<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script>
<!--
jQuery.noConflict();
-->
</script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_form.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_declaration.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
	var _pattern = 0;
	var _editIndividualId = "";
	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

	var classData = new Array();
	<c:forEach var="class" items="${ ClassListBean.classList }" varStatus="status">
	classData[<c:out value="${ status.index }" />] = new Array(
		"<c:out value="${ class.year }" />",
		"<c:out value="${ class.grade }" />",
		"<c:out value="${ class.className }" />");
	</c:forEach>

	var examData = new Array();
	<c:forEach var="exam" items="${ ExamListBean.examDataList }" varStatus="status">
	examData[<c:out value="${ status.index }" />] = new Array(
		"<c:out value="${ exam.examYear }" />",
		"<c:out value="${ exam.grade }" />",
		"<c:out value="${ exam.className }" />",
		"<c:out value="${ exam.examCd }" />",
		"<c:out value="${ exam.examName }" />",
		"<c:out value="${ exam.inpleDate }" />",
		<c:out value="${ exam.dispSequence }" />)
	</c:forEach>

	function load(actionMode, id, f) {
		document.forms[0].forward.value = "m201";
		document.forms[0].actionMode.value = actionMode;
		new Ajax.Updater(id,
				"<c:url value="M201AsyncServlet" />",
				{ postBody:Form.serialize("form"), onException:displayException, onComplete:f });
	}

	function loadStudentList(f) {
		$('StudentList').innerHTML = "<table bgcolor=#FFFFFF border=0 cellpadding=2 cellspacing=2 height=100% width=100%>"
				+ "<tr bgcolor=#DDDDDD><td align=center><span class=text12>ロード中です...</span></td></tr></table>";
		$('TakenExamList').innerHTML = "<table bgcolor=#FFFFFF border=0 cellpadding=2 cellspacing=2 width=100%>"
				+ "<tr height=143 bgcolor=#DDDDDD><td></td></tr></table>";
		$('YearLabel1').innerHTML = document.forms[0].targetYear1.value + "年度<br>";
		load("1", "StudentList", f);
	}

	function loadTakenExamList(obj) {
		$('TakenExamList').innerHTML = "<table bgcolor=#FFFFFF border=0 cellpadding=2 cellspacing=2 width=100%>"
				+ "<tr height=143 bgcolor=#DDDDDD><td align=center><span class=text12>ロード中です...</span></td></tr></table>";
		load("2", "TakenExamList");
		clearBackgroundColor($("StudentTable"));
		obj.parentNode.parentNode.style.backgroundColor = "#FFAD8C";
	}

	function loadAnswerSheetList(f) {
		$('AnswerSheetList').innerHTML = "<table bgcolor=#FFFFFF border=0 cellpadding=2 cellspacing=2 height=100% width=100%>"
				+ "<tr bgcolor=#DDDDDD><td align=center><span class=text12>ロード中です...</span></td></tr></table>";
		$('AnswerSheetDetail').innerHTML = "<table bgcolor=#FFFFFF border=0 cellpadding=2 cellspacing=2 width=100%>"
				+ "<tr height=143 bgcolor=#DDDDDD><td></td></tr></table>";
		$('YearLabel2').innerHTML = document.forms[0].targetYear2.value + "年度<br>";
		load("3", "AnswerSheetList", f);
	}

	function loadAnswerSheetDetail(obj) {
		load("4", "AnswerSheetDetail");
		clearBackgroundColor($("AnswerTable"));
		obj.parentNode.parentNode.style.backgroundColor = "#FFAD8C";
	}

	<%-- 生徒情報の表示完了時の処理 --%>
	function loadedStudentList() {
		<%-- 初期値にチェック＆スクロール --%>
		initTarget("targetStudentId", "<c:out value="${ form.targetStudentId }" />");
		<%-- 模試情報の初期ロード --%>
		loadAnswerSheetList(loadedAnswerSheetList);
	}

	<%-- 模試情報の表示完了時の処理 --%>
	function loadedAnswerSheetList() {
		<%-- 初期値にチェック＆スクロール --%>
		initTarget("targetSheetId", "<c:out value="${ form.targetSheetId }" />");
		<%-- ここまで来たらメインレイヤ表示 --%>
		showMainLayer();
	}

	function initTarget(name, value) {
		var e = document.forms[0].elements[name];
		if (e == null) {
			return;
		} else if (e.length) {
			for (var i = 0; i < e.length; i++) {
				if (e[i].value == value) {
					e[i].click();
					window.location.hash = value;
					window.scrollTo(0, 0);
					break;
				}
			}
		} else if (e.value == value) {
			e.click();
			window.location.hash = value;
			window.scrollTo(0, 0);
		}
	}

	<%-- Ajax例外は再表示（→セッションタイムアウトで発生） --%>
	function displayException(response, e) {
		submitForm("m201");
	}

	<%-- テーブルの背景色をクリアする --%>
	function clearBackgroundColor(table) {
		for (var i = 0; i < table.rows.length; i++) {
			table.rows[i].style.backgroundColor = "#F4E5D6";
		}
	}

	<%-- メインレイヤを表示する --%>
	function showMainLayer() {
		$("LoadingLayer").style.visibility = "hidden";
		$("MainLayer").style.visibility = "visible";
	}

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	<%-- 作業中宣言更新によるサブミット --%>
	function refreshWindow() {
		submitForm("m201");
	}

	<%-- 編集画面への遷移 --%>
	function submitEdit(value) {
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
		//document.forms[0].editIndividualId.value = value;
		//submitForm("m105");

		_pattern = 1;
		_editIndividualId = value;
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

		<%-- 作業中宣言アラーム --%>
		<%@ include file="/jsp/script/declaration.jsp" %>
	}

	<%-- 結合 --%>
	function submitIntegration() {
		var util = new FormUtil();
		if (util.countChecked(document.forms[0].targetStudentId) == 0
				|| util.countChecked(document.forms[0].targetSheetId) == 0) {
			alert("<kn:message id="m015a" />");
		} else {
			<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
			//showLoadingLayer();
			//submitForm("m201", "1");

			_pattern = 2;
			<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

			<%-- 作業中宣言アラーム --%>
			<%@ include file="/jsp/script/declaration.jsp" %>
		}
	}

	<%-- 生徒情報：学年のコンボを初期化する --%>
	function initGrade1(target) {
		var form =  document.forms[0];
		var year = form.targetYear1.value;
		var select = document.forms[0].targetGrade1;
		select.options.length = 0;
		var beforeGrade = null;
		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && beforeGrade != classData[i][1]) {
				var o = new Option(classData[i][1] + "年", classData[i][1]);
				o.selected = o.value == target;
				select.options[select.options.length] = o;
				beforeGrade = classData[i][1];
			}
		}
	}

	<%--  生徒情報：クラスのコンボを初期化する --%>
	function initClass1(target) {
		var form =  document.forms[0];
		var year = form.targetYear1.value;
		var grade = form.targetGrade1.value;
		var select = document.forms[0].targetClass1;
		select.options.length = 0;
		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && classData[i][1] == grade) {
				var o = new Option(classData[i][2] + "クラス", classData[i][2]);
				o.selected = o.value == target;
				select.options[select.options.length] = o;
			}
		}
		if (form.targetGrade1.value) {
			form.viewButton1.disabled = false;
		} else {
			form.viewButton1.disabled = "disabled";
		}
	}

	<%-- 模試情報：学年のコンボを初期化する --%>
	function initGrade2(target) {
		var form =  document.forms[0];
		var year = form.targetYear2.value;
		var select = document.forms[0].targetGrade2;
		select.options.length = 0;
		var beforeGrade = null;
		for (var i = 0; i < examData.length; i++) {
			if (examData[i][0] == year && beforeGrade != examData[i][1]) {
				var o = new Option(examData[i][1] + "年", examData[i][1]);
				o.selected = o.value == target;
				select.options[select.options.length] = o;
				beforeGrade = examData[i][1];
			}
		}
	}

	<%--  模試情報：クラスのコンボを初期化する --%>
	function initClass2(target) {
		var form =  document.forms[0];
		var year = form.targetYear2.value;
		var grade = form.targetGrade2.value;
		var select = document.forms[0].targetClass2;
		select.options.length = 0;
		var beforeClass = null;
		for (var i = 0; i < examData.length; i++) {
			if (examData[i][0] == year && examData[i][1] == grade
				&& examData[i][2] != beforeClass) {
				var o = new Option(examData[i][2] + "クラス", examData[i][2]);
				o.selected = o.value == target;
				select.options[select.options.length] = o;
				beforeClass = examData[i][2];
			}
		}
		if (form.targetGrade2.value) {
			form.viewButton2.disabled = false;
		} else {
			form.viewButton2.disabled = "disabled";
		}
	}

	<%--  模試情報：模試のコンボを初期化する --%>
	function initExam(target) {
		var form =  document.forms[0];
		var year = form.targetYear2.value;
		var grade = form.targetGrade2.value;
		var select = form.targetExam;
		select.options.length = 0;
		var hash = new Array();
		var array = new Array();
		for (var i = 0; i < examData.length; i++) {
			if (examData[i][0] == year && examData[i][1] == grade
					&& hash[examData[i][3]] == null) {
				array[array.length] = examData[i];
				hash[examData[i][3]] = 1;
			}
		}
		<%-- 模試は実施日の降順・表示順の昇順でソート --%>
		array.sort(function (a, b) {
			if (a[5] > b[5]) {
				return -1;
			} else if (a[5] < b[5]) {
				return 1;
			} else {
				if (a[6] > b[6]) {
					return 1;
				} else if (a[6] < b[6]) {
					return -1;
				} else {
					return 0;
				}
			}
		});
		for (var i = 0; i < array.length; i++) {
			var o = new Option(array[i][4], array[i][3]);
			o.selected = o.value == target;
			select.options[select.options.length] = o;
		}
	}

	function init() {
		startTimer();
		initGrade1("<c:out value="${ form.targetGrade1 }" />");
		initClass1("<c:out value="${ form.targetClass1 }" />");
		initGrade2("<c:out value="${ form.targetGrade2 }" />");
		initClass2("<c:out value="${ form.targetClass2 }" />");
		initExam("<c:out value="${ form.targetExam }" />");
		<c:choose>
			<%-- 初期値がある場合 --%>
			<c:when test="${ not empty form.targetStudentId || not empty form.targetSheetId }">
				loadStudentList(loadedStudentList);
			</c:when>
			<%-- 初期値がない場合 --%>
			<c:otherwise>
				showMainLayer();
			</c:otherwise>
		</c:choose>
		var message = "<c:out value="${ ErrorMessage }" />";
		if (message != "") alert(message);
	}

	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
	function workingDialogClose(val) {

		// ダイアログクローズ
		jQuery(workingDialogObj).dialog('close');
		if (val != 1) {
			return;
		}

		// 編集画面への遷移
		if (_pattern == 1) {
			document.forms[0].editIndividualId.value = _editIndividualId;
			submitForm("m105");

		// 結合
		} else {
			showLoadingLayer();
			submitForm("m201", "1");
		}

	}
	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px">
ロード中です ...
</div>

<div id="MainLayer" style="visibility:hidden;">
<form action="<c:url value="M201Servlet" />" method="POST" id="form">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m201">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="editIndividualId" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">（生徒情報管理−模試結合先変更）</b></td>
<td width="400" bgcolor="#758A98" align="right">

<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td<c:if test="${ empty DeclarationBean.time }"> bgcolor="#FFFFFF"</c:if>><input type="button" value="作業中宣言" class="text12" style="width:110px;<c:if test="${ not empty DeclarationBean.time }">background-color:#FB4733;color:#FFF;font-weight:bold;</c:if>" onclick="openDeclaration()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="10" border="0" alt=""></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="text-align:left;margin:10px;">
<span class="text14-hh">模試の結合状態に間違いがある場合は、正しい生徒情報と模試情報の組み合わせを作り（それぞれを選択）、模試結合先変更ボタンをクリックしてください。<BR>
�@画面左の生徒情報欄には、対象年度等の絞り込み条件に該当するすべての生徒情報と、結合している模試を表示します。<BR>
�A画面右の模試情報欄には、選択した模試で発生した模試情報を表示します。※その中から、対象年度等の条件でさらに絞り込みできます。</span>
</div>
<!--/説明-->

<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr>
<td width="419" valign="top">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="419">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="410">

<table border="0" cellpadding="0" cellspacing="0" width="410">
<tr valign="top">
<td colspan="4" width="410" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="410" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="410" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="410" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="396" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">生徒情報別　結合模試一覧表示</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="410" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="410" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 DEL START --%>
<%-- <td colspan="3" width="410" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="410" height="1" border="0" alt=""><br></td> --%>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 DEL END   --%>
</tr>
</table>
<!--/大タイトル-->

<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<div style="border-style:solid;border-width:0px 1px 1px 1px;border-color:#8CA9BB;width:419px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:397px;background-color:#FFFFFF;margin:10px;padding:20px 10px 20px 10px;">

<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:375px;background-color:#FFFFFF;padding:3px;">
--%>
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:417px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:375px;background-color:#FFFFFF;margin:10px;padding:20px 10px 20px 10px;">

<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:367px;background-color:#FFFFFF;padding:3px;">
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD END   --%>

<b class="text12">対象年度・クラス・氏名頭文字の選択</b>
<table border="0" cellpadding="3" cellspacing="2" width="100%">
<tr bgcolor="#E1E6EB" height="27">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">年度&nbsp;：&nbsp;</span></td>
	<td><select name="targetYear1" class="text12" onchange="initGrade1(document.forms[0].targetGrade1.value);initClass1(document.forms[0].targetClass1.value);document.forms[0].targetYear2.selectedIndex=this.selectedIndex;initGrade2(document.forms[0].targetGrade2.value);initClass2(document.forms[0].targetClass2.value);initExam(document.forms[0].targetExam.value);" style="width:60px">
	<option value="<c:out value="${ CurrentYear }" />"><c:out value="${ CurrentYear }" /></option>
	<option value="<c:out value="${ CurrentYear - 1 }" />"<c:if test="${ form.targetYear1 == CurrentYear - 1 }"> selected</c:if>><c:out value="${ CurrentYear - 1 }" /></option>
	<option value="<c:out value="${ CurrentYear - 2 }" />"<c:if test="${ form.targetYear1 == CurrentYear - 2 }"> selected</c:if>><c:out value="${ CurrentYear - 2 }" /></option>
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
	<td><span class="text12">学年&nbsp;：&nbsp;</span></td>
	<td><select name="targetGrade1" class="text12" onchange="initClass1(document.forms[0].targetClass1.value);" style="width:72px;">
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
	<td><span class="text12">クラス&nbsp;：&nbsp;</span></td>
	<td><select name="targetClass1" class="text12" style="width:80px;"></select></td>
	</tr>
	</table>
</td>
</tr>
<tr bgcolor="#E1E6EB" height="27">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">※氏名の頭文字を選択してください。&nbsp;&nbsp;</span></td>
	<td><select name="initialKana1" class="text12" style="width:80px;">
	<%@ include file="/jsp/maintenance/include/initial_kana.jsp" %>
	</select></td>
	</tr>
	</table>
</td>
</tr>
<tr bgcolor="#FFFFFF" height="27">
<td></td>
</tr>
</table>
<div style="text-align:center;margin:10px;">
	<img src="./shared_lib/img/parts/arrow_down_darkblue_s.gif" width="34" height="7" border="0" alt="↓">
</div>
<div style="text-align:center;margin:10px;">
	<input type="button" id="viewButton1" value="生徒情報表示" class="text12" style="width:120px;" onclick="loadStudentList();loadAnswerSheetList();">
</div>
</div>

<div style="text-align:center;margin:10px;">
	<img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓">
</div>

<div style="text-align:center;background-color:#CDD7DD;padding:8px;">
	<b class="text16">生徒情報選択</b>
</div>

<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2">
<tr bgcolor="#CDD7DD">
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td align="center" width="35"><span class="text12"></span></td>
<td align="center" width="116"><span class="text12" id="YearLabel1"></span><span class="text10">学年／クラス／番号</span></td>
--%>
<td align="center" width="36"><span class="text12"></span></td>
<td align="center" width="115"><span class="text12" id="YearLabel1"></span><span class="text10">学年／クラス／番号</span></td>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD END   --%>
<td align="center" width="34"><span class="text12">性別</span></td>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td align="center" width="164"><span class="text12">かな氏名</a></td>
  --%>
<td align="center" width="164"><span class="text12">かな氏名</span></td>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>
</tr>
</table>

<div id="StudentList" style="height:242px;width:100%;overflow-y:scroll;border-width:2px;border-style:inset;left;background-color:#F4E5D6;">
</div>

<BR>
<b class="text12">選択した生徒情報に結合している模試</b>

<div id="TakenExamList" style="width:100%;margin-top:10px;">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
<tr height="143" bgcolor="#DDDDDD"><td></td></tr>
</table>
</div>

</div>
</div>

</td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""></td>
<td width="419" valign="top">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="419">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="410">

<table border="0" cellpadding="0" cellspacing="0" width="410">
<tr valign="top">
<td colspan="4" width="410" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="410" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="410" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="410" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="396" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">模試情報別　受験届内容表示</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="410" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="410" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 DEL START --%>
<%-- <td colspan="3" width="410" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="410" height="1" border="0" alt=""><br></td> --%>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 DEL END   --%>
</tr>
</table>
<!--/大タイトル-->

<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<div style="border-style:solid;border-width:0px 1px 1px 1px;border-color:#8CA9BB;width:419px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:397px;background-color:#FFFFFF;margin:10px;padding:20px 10px 20px 10px;">

<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:375px;background-color:#FFFFFF;padding:3px;">
--%>
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:417px;background-color:#EFF2F3;">
<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:375px;background-color:#FFFFFF;margin:10px;padding:20px 10px 20px 10px;">

<div style="border-style:solid;border-width:1px;border-color:#8CA9BB;width:367px;background-color:#FFFFFF;padding:3px;">
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD END   --%>
<b class="text12">対象年度・クラス・氏名頭文字・模試の選択</b>
<table border="0" cellpadding="3" cellspacing="2" width="100%">
<tr bgcolor="#E1E6EB" height="27">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">年度&nbsp;：&nbsp;</span></td>
	<td><select name="targetYear2" class="text12" onchange="initGrade2(document.forms[0].targetGrade2.value);initClass2(document.forms[0].targetClass2.value);initExam(document.forms[0].targetExam.value);document.forms[0].targetYear1.selectedIndex=this.selectedIndex;initGrade1(document.forms[0].targetGrade1.value);initClass1(document.forms[0].targetClass1.value);" style="width:60px">
	<option value="<c:out value="${ CurrentYear }" />"><c:out value="${ CurrentYear }" /></option>
	<option value="<c:out value="${ CurrentYear - 1 }" />"<c:if test="${ form.targetYear2 == CurrentYear - 1 }"> selected</c:if>><c:out value="${ CurrentYear - 1 }" /></option>
	<option value="<c:out value="${ CurrentYear - 2 }" />"<c:if test="${ form.targetYear2 == CurrentYear - 2 }"> selected</c:if>><c:out value="${ CurrentYear - 2 }" /></option>
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
	<td><span class="text12">学年&nbsp;：&nbsp;</span></td>
	<td><select name="targetGrade2" class="text12" onchange="initClass2(document.forms[0].targetClass2.value);initExam(document.forms[0].targetExam.value);" style="width:72px;">
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
	<td><span class="text12">クラス&nbsp;：&nbsp;</span></td>
	<td><select name="targetClass2" class="text12" style="width:80px;"></select></td>
	</tr>
	</table>
</td>
</tr>
<tr bgcolor="#E1E6EB" height="27">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">※氏名の頭文字を選択してください。&nbsp;&nbsp;</span></td>
	<td><select name="initialKana2" class="text12" style="width:80px;">
	<%@ include file="/jsp/maintenance/include/initial_kana.jsp" %>
	</select></td>
	</tr>
	</table>
</td>
</tr>
<tr bgcolor="#E1E6EB" height="27">
<td>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">模試&nbsp;：&nbsp;</span></td>
	<td><select name="targetExam" class="text12" style="width:240px;">
	</select></td>
	</tr>
	</table>
</td>
</tr>
</table>
<div style="text-align:center;margin:10px;">
	<img src="./shared_lib/img/parts/arrow_down_darkblue_s.gif" width="34" height="7" border="0" alt="↓">
</div>
<div style="text-align:center;margin:10px;">
	<input type="button" id="viewButton2" value="模試情報表示" class="text12" style="width:120px;" onclick="loadAnswerSheetList();loadStudentList();">
</div>
</div>

<div style="text-align:center;margin:10px;">
	<img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓">
</div>

<div style="text-align:center;background-color:#CDD7DD;padding:8px;">
	<b class="text16">模試情報選択</b>
</div>

<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2">
<tr bgcolor="#CDD7DD">
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td align="center" width="35"><span class="text12"></span></td>
<td align="center" width="116"><span class="text12" id="YearLabel2"></span><span class="text10">学年／クラス／番号</span></td>
 --%>
<td align="center" width="36"><span class="text12"></span></td>
<td align="center" width="115"><span class="text12" id="YearLabel2"></span><span class="text10">学年／クラス／番号</span></td>
<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD END   --%>
<td align="center" width="34"><span class="text12">性別</span></td>
<td align="center" width="164"><span class="text12">かな氏名</a></td>
</tr>
</table>

<div id="AnswerSheetList" style="height:242px;width:100%;overflow-y:scroll;border-width:2px;border-style:inset;left;background-color:#F4E5D6;">
</div>

<BR>
<b class="text12">選択した模試情報に含まれる受験届の内容</b>

<div id="AnswerSheetDetail" style="width:100%;margin-top:10px;">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
<tr height="143" bgcolor="#DDDDDD"><td></td></tr>
</table>
</div>

</div>
</div>

</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">
<input type="button" value="模試結合先変更" class="text12" style="width:200px;" onclick="submitIntegration();">
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<input type="button" value="メンテナンス完了　（プロファイル選択画面へ）" class="text12" style="width:320px;" onclick="submitForm('w002')">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/maintenance/x011.jsp" %>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

</form>
</div>

</body>
</html>
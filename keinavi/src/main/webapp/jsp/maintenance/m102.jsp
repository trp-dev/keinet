<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/validate_string.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	var validator = new Validator();

	function submitImport() {
		var form = document.forms[1];
		if (form.file.value == "") {
			alert("<kn:message id="x003a" />");
			return;
		}
		showLoadingLayer();
		form.actionMode.value = "0";
		form.submit();
	}

	function submitRegist() {
		if (validate()) {
			showLoadingLayer();
			var form = document.forms[2];
			form.actionMode.value = "1";
			form.scrollX.value = document.body.scrollLeft;
			form.scrollY.value = document.body.scrollTop;
			form.submit();
		}
	}

	function submitCancel() {
		var form = document.forms[2];
		form.actionMode.value = "0";
		form.submit();
	}

	function validate() {

		var message = "";
		var form = document.forms[2];

		<%-- クラス --%>
		if (form.className.value == ""
				|| !validator.isAlphanumeric(form.className.value)) {
			message += "<kn:message id="x004a" />\n";
		}

		<%-- クラス番号 --%>
		if (form.classNo.value == ""
				|| !validator.isNumber(form.classNo.value)) {
			message += "<kn:message id="x005a" />\n";
		}

		<%-- かな氏名 --%>
		if (!isKanaName(form.nameKana.value)) {
			message += "<kn:message id="x006a" />\n";
		}

		<%-- 漢字氏名 --%>
		if (!isKanjiName(form.nameKanji.value)) {
			message += "<kn:message id="x007a" />\n";
		}

		<%-- 生年月日 --%>
		if (!isBirthday(form.birthYear.value, form.birthMonth.value, form.birthDate.value)) {
			message += "<kn:message id="x008a" />\n";
		}

		<%-- 電話番号 --%>
		if (!isTelNo(form.telNo.value)) {
			message += "<kn:message id="x009a" />\n";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		return true;
	}

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	function init() {
		startTimer();
		window.scrollTo(document.forms[2].scrollX.value, document.forms[2].scrollY.value);
		var message = "<c:out value="${ErrorMessage}" />";
		if (message != "") alert(message);
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0"  onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px;visibility:hidden;">
ロード中です ...
</div>

<div id="MainLayer">
<form action="<c:url value="M102Servlet" />" method="POST">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m102">
</form>

<form action="<c:url value="M104Servlet" />" method="POST" enctype="multipart/form-data" onsubmit="return false">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="forward" value="m104">
<input type="hidden" name="backward" value="m102">
<input type="hidden" name="targetYear" value="<c:out value="${form.targetYear}" />">
<input type="hidden" name="targetGrade" value="<c:out value="${form.targetGrade}" />">
<input type="hidden" name="targetClass" value="<c:out value="${form.targetClass}" />">
<input type="hidden" name="conditionSelect" value="<c:out value="${form.conditionSelect}" />">
<input type="hidden" name="conditionText" value="<c:out value="${form.conditionText}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->

<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（新規登録）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="15" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（新規登録・一括更新）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr>
<td><span class="text14-hh">「CSVファイルで一括登録・更新」または「一人ずつ登録」のどちらかの方法で生徒情報の新規登録を行ってください。<br>
ただし、登録する生徒が２，３年生の情報で前年度の生徒情報がある場合は、進級処理が行われます。<br>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="817">

<table border="0" cellpadding="0" cellspacing="0" width="817">
<tr valign="top">
<td colspan="4" width="817" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="817" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="804" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">CSVファイルで一括登録・更新</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->
<!--CSVファイルで一括登録-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="826">
<tr valign="top">
<td width="824" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="796">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="796">
<tr valign="top">
<td width="794" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="4" cellspacing="2" width="576">
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">登録用ファイル</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="file" name="file" size="50" class="text12" style="width:350px;"></td>
</tr>
</table>
</td>
</tr>
</table>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr>
<td width="572" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="572">
<tr valign="top">
<td width="570" bgcolor="#FBD49F" align="center">
<input type="button" value="キャンセル" class="text12" style="width:100px;" onClick="submitCancel()">
<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <input type="button" value="登録" class="text12" style="width:180px;" onClick="submitImport()"> --%>
<input type="button" value="登録" class="text12" style="width:180px;" onClick="submitImport()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/CSVファイルで一括登録-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</form>



<form action="<c:url value="M102Servlet" />" method="POST">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="forward" value="m101">
<input type="hidden" name="backward" value="m102">
<input type="hidden" name="targetYear" value="<c:out value="${form.targetYear}" />">
<input type="hidden" name="targetGrade" value="<c:out value="${form.targetGrade}" />">
<input type="hidden" name="targetClass" value="<c:out value="${form.targetClass}" />">
<input type="hidden" name="conditionSelect" value="<c:out value="${form.conditionSelect}" />">
<input type="hidden" name="conditionText" value="<c:out value="${form.conditionText}" />">
<input type="hidden" name="targetStudentId" value="">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="817">

<table border="0" cellpadding="0" cellspacing="0" width="817">
<tr valign="top">
<td colspan="4" width="817" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="817" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="804" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">一人ずつ登録</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->
<!--一人ずつ手動で登録-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="826">
<tr valign="top">
<td width="824" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="796">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="796">
<tr valign="top">
<td width="794" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="4" cellspacing="2" width="576">
<!--対象年度-->
<tr height="27">
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">対象年度</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ form.targetYear }" />年度</span></td>
</tr>
</table>
</td>
</tr>
<!--/対象年度-->
<!--学年-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">学年</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td>
<select name="grade" class="text12">
<option value="1"<c:if test="${ form.grade == 1 }"> selected</c:if>>1年</option>
<option value="2"<c:if test="${ form.grade == 2 }"> selected</c:if>>2年</option>
<option value="3"<c:if test="${ form.grade == 3 }"> selected</c:if>>3年</option>
<option value="4"<c:if test="${ form.grade == 4 }"> selected</c:if>>4年</option>
<option value="5"<c:if test="${ form.grade == 5 }"> selected</c:if>>5年</option>
</select>
</td>
</tr>
</table>
</td>
</tr>
<!--/学年-->
<!--クラス-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">クラス</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="3" name="className" value="<c:out value="${form.className}" />" maxlength="2"></td>
</tr>
</table>
</td>
</tr>
<!--/クラス-->
<!--クラス番号-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">クラス番号</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="6" name="classNo" value="<c:out value="${form.classNo}" />" maxlength="5"></td>
</tr>
</table>
</td>
</tr>
<!--/クラス番号-->
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="4" cellspacing="2" width="576">
<!--性別-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">性別</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><select name="sex" class="text12">
<option value="1"<c:if test="${ form.sex == 1 }"> selected</c:if>>男</option>
<option value="2"<c:if test="${ form.sex == 2 }"> selected</c:if>>女</option>
</select></td>
</tr>
</table>
</td>
</tr>
<!--/性別-->
<!--かな氏名-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">かな氏名</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="30" name="nameKana" value="<c:out value="${form.nameKana}" />" maxlength="14"></td>
</tr>
</table>
</td>
</tr>
<!--/かな氏名-->
<!--漢字氏名-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">漢字氏名</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="30" name="nameKanji" value="<c:out value="${form.nameKanji}" />" maxlength="20"></td>
</tr>
</table>
</td>
</tr>
<!--/漢字氏名-->
<!--生年月日-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">生年月日</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="4" name="birthYear" value="<c:out value="${form.birthYear}" />" maxlength="4"></td>
<td><span class="text12">&nbsp;年</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="2" name="birthMonth" value="<c:out value="${form.birthMonth}" />" maxlength="2"></td>
<td><span class="text12">&nbsp;月</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="2" name="birthDate" value="<c:out value="${form.birthDate}" />" maxlength="2"></td>
<td><span class="text12">&nbsp;日</span></td>
</tr>
</table>
</td>
</tr>
<!--/生年月日-->
<!--電話番号-->
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">電話番号</b></td>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="text" class="text12" size="13" name="telNo" value="<c:out value="${form.telNo}" />" maxlength="11"></td>
</tr>
</table>
</td>
</tr>
<!--/電話番号-->
</table>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr>
<td width="572" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="572">
<tr valign="top">
<td width="570" bgcolor="#FBD49F" align="center">
<input type="button" value="キャンセル" class="text12" style="width:100px;" onClick="submitCancel()">
<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%--	<input type="button" value="登録して一覧へ戻る" class="text12" style="width:180px;" onClick="submitRegist()">	--%>
<input type="button" value="登録して一覧へ戻る" class="text12" style="width:180px;" onClick="submitRegist()"<c:if test="${ LoginSession.trial }"> disabled</c:if>>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/一人ずつ手動で登録-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（新規登録）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</div>

</body>
</html>

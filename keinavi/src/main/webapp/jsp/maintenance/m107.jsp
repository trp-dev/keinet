<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set var="parent"><c:out value="${ param.parent }" default="${ param.backward }" /></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_form.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>
	<%@ include file="/jsp/script/validate_string.jsp" %>

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	function init() {
		startTimer();
		var message = "<c:out value="${ ErrorMessage }" />";
		if (message != "") alert(message);
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px;visibility:hidden;">
ロード中です ...
</div>

<div id="MainLayer">
<form action="<c:url value="M107Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m107">
<input type="hidden" name="parent" value="<c:out value="${ parent }" />">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="targetIndividualId" value="<c:out value="${ form.targetIndividualId }" />">
<input type="hidden" name="editFlag" value="<c:out value="${ form.editFlag }" />">
<input type="hidden" name="overwriteNameKana" value="<c:out value="${ form.overwriteNameKana }" />">
<input type="hidden" name="overwriteNameKanji" value="<c:out value="${ form.overwriteNameKanji }" />">
<input type="hidden" name="overwriteBirthday" value="<c:out value="${ form.overwriteBirthday }" />">
<input type="hidden" name="overwriteTelNo" value="<c:out value="${ form.overwriteTelNo }" />">

<input type="hidden" name="beforeLastGrade" value="<c:out value="${ form.beforeLastGrade }" />">
<input type="hidden" name="lastGrade" value="<c:out value="${ form.lastGrade }" />">
<input type="hidden" name="grade" value="<c:out value="${ form.grade }" />">
<input type="hidden" name="beforeLastClassName" value="<c:out value="${ form.beforeLastClassName }" />">
<input type="hidden" name="lastClassName" value="<c:out value="${ form.lastClassName }" />">
<input type="hidden" name="className" value="<c:out value="${ form.className }" />">
<input type="hidden" name="beforeLastClassNo" value="<c:out value="${ form.beforeLastClassNo }" />">
<input type="hidden" name="lastClassNo" value="<c:out value="${ form.lastClassNo }" />">
<input type="hidden" name="classNo" value="<c:out value="${ form.classNo }" />">
<input type="hidden" name="sex" value="<c:out value="${ form.sex }" />">
<input type="hidden" name="nameKana" value="<c:out value="${ form.nameKana }" />">
<input type="hidden" name="nameKanji" value="<c:out value="${ form.nameKanji }" />">
<input type="hidden" name="birthYear" value="<c:out value="${ form.birthYear }" />">
<input type="hidden" name="birthMonth" value="<c:out value="${ form.birthMonth }" />">
<input type="hidden" name="birthDate" value="<c:out value="${ form.birthDate }" />">
<input type="hidden" name="telNo" value="<c:out value="${ form.telNo }" />">

<input type="hidden" name="oGrade" value="<c:out value="${ form.OGrade }" />">
<input type="hidden" name="oClass" value="<c:out value="${ form.OClass }" />">
<input type="hidden" name="oLastGrade" value="<c:out value="${ form.OLastGrade }" />">
<input type="hidden" name="oLastClassName" value="<c:out value="${ form.OLastClassName }" />">
<input type="hidden" name="oBeforeLastGrade" value="<c:out value="${ form.OBeforeLastGrade }" />">
<input type="hidden" name="oBeforeLastClassName" value="<c:out value="${ form.OBeforeLastClassName }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（統合）</b></td>
<td width="400" bgcolor="#758A98" align="right"></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<div style="margin-top:24px;">
<span style="font-weight:bold;font-family:Arial Black;color:#FF8F0E;font-size:25px">以下の主生徒と従生徒を統合します。</span>
</div>

<div style="margin-top:24px;">
<table><tr><td>
<c:choose>
<c:when test="${ parent == 'm101' }">
<b class="text14">�@結合後、従生徒に結合していた模試情報は、主生徒情報に結合し直されます。<BR>�A従生徒情報を非表示にします（生徒情報一覧に表示されなくなります）。</b>
</c:when>
<c:otherwise>
<b class="text14">�@結合後、従生徒に結合していた模試情報は、主生徒情報に結合し直されます。<BR>�A従生徒情報を非表示にします（生徒情報一覧に表示されなくなります）。</b>
</c:otherwise>
</c:choose>
</td></tr></table>
</div>

<div style="margin-top:24px;">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
<tr height="27" bgcolor="#93A3AD">
<td colspan="14"><b class="text14" style="color:#FFF;">&nbsp;&nbsp;主情報</b></td>
</tr>
<tr bgcolor="#CDD7DD">
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center"><span class="text12">性別</span></td>
<td align="center"><span class="text12">かな氏名</span></td>
<td align="center"><span class="text12">漢字氏名</span></td>
<td align="center"><span class="text12">生年月日</span></td>
<td align="center"><span class="text12">電話番号</span></td>
</tr>
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><span class="text12"><c:if test="${ form.beforeLastGrade >= '0' }"><c:out value="${ form.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ form.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ form.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ form.lastGrade >= '0' }"><c:out value="${ form.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ form.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ form.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ form.grade >= '0' }"><c:out value="${ form.grade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ form.className }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ form.classNo }" /></span></td>
	<td width="5%" align="center"><span class="text12"><c:choose><c:when test="${ form.sex == '1' }">男</c:when><c:when test="${ form.sex == '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="18%"><span class="text12"><c:out value="${ form.nameKana }" /></span></td>
	<td width="14%"><span class="text12"><c:out value="${ form.nameKanji }" /></span></td>
	<td width="12%" align="center"><span class="text12"><c:if test="${ not empty form.birthYear }"><c:out value="${ form.birthYear }" />/<c:out value="${ form.birthMonth }" />/<c:out value="${ form.birthDate }" /></c:if></span></td>
	<td width="12%"><span class="text12"><c:out value="${ form.telNo }" /></span></td>
</tr>
</table>
</div>

<div style="margin-top:24px;">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">
<tr height="27" bgcolor="#93A3AD">
<td colspan="14"><b class="text14" style="color:#FFF;">&nbsp;&nbsp;従情報</b></td>
</tr>
<tr bgcolor="#CDD7DD">
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center" colspan="3"><span class="text12"><c:out value="${ CurrentYear }" />年度</span></span><br><span class="text10">学年／クラス／番号</span></td>
<td align="center"><span class="text12">性別</span></td>
<td align="center"><span class="text12">かな氏名</span></td>
<td align="center"><span class="text12">漢字氏名</span></td>
<td align="center"><span class="text12">生年月日</span></td>
<td align="center"><span class="text12">電話番号</span></td>
</tr>

<c:set var="student" value="${ SelectedStudentBean.subStudentData }" />
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.beforeLastGrade >= 0 }"><c:out value="${ student.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.lastGrade >= 0 }"><c:out value="${ student.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.currentGrade >= 0 }"><c:out value="${ student.currentGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.currentClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.currentClassNo }" /></span></td>
	<td width="5%" align="center"><span class="text12"><c:choose><c:when test="${ student.sex == '1' }">男</c:when><c:when test="${ student.sex == '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="18%"><span class="text12"><c:out value="${ student.nameHiragana }" /></span></td>
	<td width="14%"><span class="text12"><c:out value="${ student.nameKanji }" /></span></td>
	<td width="12%" align="center"><span class="text12"><kn:formatDate value="${ student.birthday }" pattern="yyyy/MM/dd" /></span></td>
	<td width="12%"><span class="text12"><c:out value="${ student.telNo }" /></span></td>
</tr>

</table>
</div>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<c:choose>
<c:when test="${ parent == 'm101' }">
<input type="button" value="キャンセル" class="text12" style="width:120px;" onclick="submitForm('m101');">
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <input type="button" value="統合" class="text12" style="width:120px;" onclick="showLoadingLayer();submitForm('m101','1');"> --%>
<input type="button" value="統合" class="text12" style="width:120px;" onclick="showLoadingLayer();submitForm('m101','1');"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
</c:when>
<c:otherwise>
<input type="button" value="キャンセル" class="text12" style="width:120px;" onclick="submitForm('m105');">
<input type="button" value="登録・統合" class="text12" style="width:120px;" onclick="showLoadingLayer();submitForm('m101','1');">
</c:otherwise>
</c:choose>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</div>

</body>
</html>
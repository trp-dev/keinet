<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_form.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_declaration.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	var classData = new Array();
	<c:forEach var="class" items="${ ClassListBean.classList }" varStatus="status">
	classData[<c:out value="${ status.index }" />] = new Array(
		"<c:out value="${ class.year }" />",
		"<c:out value="${ class.grade }" />",
		"<c:out value="${ class.className }" />");
	</c:forEach>

	function submitAction(actionMode) {
		submitForm("m108", actionMode, true);
	}

	<%-- 一覧表示 --%>
	function submitView() {
		submitAction("3");
	}

	<%-- ソート --%>
	function submitSort(value) {
		if (document.forms[0].targetGrade.value) {
			document.forms[0].sortKey.value = value;
			submitAction("5");
		}
	}

	<%-- 生徒再表示 --%>
	function submitReDisplay() {
		if (new FormUtil().countChecked(document.forms[0].individualId) == 0) {
			alert("<kn:message id="m014a" />");
			return;
		}
		if (confirm("<kn:message id="m002c" />")) {
			showLoadingLayer();
			submitForm("m101", "1");
		}
	}

	<%-- 完全に削除 --%>
	function submitDelete() {
		if (new FormUtil().countChecked(document.forms[0].individualId) == 0) {
			alert("<kn:message id="m014a" />");
			return;
		}
		if (confirm("<kn:message id="m003c" />")) {
			showLoadingLayer();
			submitForm("m101", "2");
		}
	}

	<%-- 学年のコンボを初期化する --%>
	function initGrade(target) {
		var form =  document.forms[0];
		var year = form.targetYear.value;
		var select = document.forms[0].targetGrade;
		select.options.length = 0;

		var beforeGrade = null;
		var array = new Array();
		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && beforeGrade != classData[i][1]) {
				var o = new Option(classData[i][1] + "年", classData[i][1]);
				o.selected = ("all" != target && array.length == 0) || o.value == target;
				array[array.length] = o;
				beforeGrade = classData[i][1];
			}
		}

		<%-- 全学年要素追加 --%>
		if (array.length > 0) {
			select.options[0] = new Option("全学年", "all");
			for (var i = 0; i < array.length; i++) {
				select.options[i + 1] = array[i];
			}
		}
	}

	<%--  クラスのコンボを初期化する --%>
	function initClass(target) {
		var form =  document.forms[0];
		var year = form.targetYear.value;
		var grade = form.targetGrade.value;
		var select = document.forms[0].targetClass;
		select.options.length = 0;

		if (form.targetGrade.value) {
			select.options[0] = new Option("全クラス", "all");
		}

		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && classData[i][1] == grade) {
				var o = new Option(classData[i][2] + "クラス", classData[i][2]);
				if (o.value == target || ("all" != target && select.options.length == 1)) o.selected = true;
				select.options[select.options.length] = o;
			}
		}

		if (form.targetGrade.value) {
			form.viewButton.disabled = false;
		} else {
			form.viewButton.disabled = "disabled";
		}
	}

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	function init() {
		startTimer();
		var form = document.forms[0];

		<c:if test="${ form.backward == 'm108' }">
			window.scrollTo(form.scrollX.value, form.scrollY.value);
		</c:if>

		initGrade("<c:out value="${ form.targetGrade }" />");
		initClass("<c:out value="${ form.targetClass }" />");

		var message = "<c:out value="${ErrorMessage}" />";
		if (message != "") alert(message);
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px;visibility:hidden;">
ロード中です ...
</div>

<div id="MainLayer">
<form action="<c:url value="M108Servlet" />" method="POST" onsubmit="return false;">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m108">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="scrollX" value="<c:out value="${ form.scrollX }" />">
<input type="hidden" name="scrollY" value="<c:out value="${ form.scrollY }" />">
<input type="hidden" name="sortKey" value="<c:out value="${ form.sortKey }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（非表示生徒一覧）</b></td>
<td width="400" bgcolor="#758A98" align="right">
</td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--対象年度・学年・クラスの選択-->
<div style="margin-top:10px;">
<table border="0" cellpadding="2" cellspacing="1" width="846" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="840">
	<tr height="32">
	<td width="734" bgcolor="#E1E6EB">
	<div style="margin:8px;">
	<b class="text12">対象年度・学年・クラスの選択</b>
	<table border="0" cellpadding="2" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">年度&nbsp;：&nbsp;</span></td>
	<td>
	<select name="targetYear" class="text12" onchange="initGrade(document.forms[0].targetGrade.value);initClass(document.forms[0].targetClass.value);" style="width:60px">
		<option value="<c:out value="${ CurrentYear - 0 }" />"<c:if test="${ form.targetYear == CurrentYear - 0 }"> selected</c:if>><c:out value="${ CurrentYear - 0 }" /></option>
		<option value="<c:out value="${ CurrentYear - 1 }" />"<c:if test="${ form.targetYear == CurrentYear - 1 }"> selected</c:if>><c:out value="${ CurrentYear - 1 }" /></option>
		<option value="<c:out value="${ CurrentYear - 2 }" />"<c:if test="${ form.targetYear == CurrentYear - 2 }"> selected</c:if>><c:out value="${ CurrentYear - 2 }" /></option>
	</select>
	</td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">学年&nbsp;：&nbsp;</span></td>
	<td><select name="targetGrade" class="text12" onchange="initClass(document.forms[0].targetClass.value);" style="width:72px;">
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">クラス&nbsp;：&nbsp;</span></td>
	<td><select name="targetClass" class="text12" style="width:80px">
	<option value="_">全クラス</option>
	</select></td>
	</tr>
	</table>
	</td>
	<td width="100" bgcolor="#F4E5D6" align="center"><input type="button" value="一覧表示" name="viewButton" class="text12" style="width:80px;" onclick="submitView();" disabled></td>
	</tr>
	</table>
</td>
</tr>
</table>
</div>
<!--/対象年度・学年・クラスの選択-->

<!--ツールバー-->
<div style="margin-top:6px;">
<table border="0" cellpadding="4" cellspacing="0" width="846">
<tr>
<td width="846" bgcolor="#93A3AD">

<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="dummy" onclick="new FormUtil().checkAllElements(this.checked,'individualId');"></td>
<td><b class="text12" style="color:#FFF;">すべて選択（チェックを外すとすべて解除）</b></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ツールバー-->

<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" align="846">
<tr bgcolor="#CDD7DD">
<td align="center" width="22"><span class="text12"></span></td>
<td align="center" width="102"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '1' }"><a href="#here" onclick="submitSort('11');"></c:when><c:otherwise><a href="#here" onclick="submitSort('1');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="104"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '2' }"><a href="#here" onclick="submitSort('12');"></c:when><c:otherwise><a href="#here" onclick="submitSort('2');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="104"><span class="text12"><c:out value="${ CurrentYear - 0 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '3' }"><a href="#here" onclick="submitSort('13');"></c:when><c:otherwise><a href="#here" onclick="submitSort('3');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="28"><span class="text12">性別</span></td>
<td align="center" width="139"><span class="text12"><c:choose><c:when test="${ form.sortKey == '4' }"><a href="#here" onclick="submitSort('14');"></c:when><c:otherwise><a href="#here" onclick="submitSort('4');"></c:otherwise></c:choose>かな氏名</a></span></td>
<td align="center" width="107"><span class="text12"><c:choose><c:when test="${ form.sortKey == '5' }"><a href="#here" onclick="submitSort('15');"></c:when><c:otherwise><a href="#here" onclick="submitSort('5');"></c:otherwise></c:choose>漢字氏名</a></span></td>
<td align="center" width="83"><span class="text12"><c:choose><c:when test="${ form.sortKey == '6' }"><a href="#here" onclick="submitSort('16');"></c:when><c:otherwise><a href="#here" onclick="submitSort('6');"></c:otherwise></c:choose>生年月日</a></span></td>
<td align="center" width="101"><span class="text12"><c:choose><c:when test="${ form.sortKey == '7' }"><a href="#here" onclick="submitSort('17');"></c:when><c:otherwise><a href="#here" onclick="submitSort('7');"></c:otherwise></c:choose>電話番号</a></span></td>
</tr>
</table>

<div style="height:242px;width:846;overflow-y:scroll;border-width:2px;border-style:inset;text-align:left;background-color:#F4E5D6;">
<table bgcolor="#FFFFFF" id="foo" border="0" cellpadding="2" cellspacing="2" width="100%">

<c:forEach var="student" items="${ StudentSearchBean.dispStudentList }">
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><input type="checkbox" name="individualId" value="<c:out value="${ student.individualId }" />"></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.beforeLastGrade >= 0 }"><c:out value="${ student.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.lastGrade >= 0 }"><c:out value="${ student.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.currentGrade >= 0 }"><c:out value="${ student.currentGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.currentClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.currentClassNo }" /></span></td>
	<td width="4%" align="center"><span class="text12"><c:choose><c:when test="${ student.sex == '1' }">男</c:when><c:when test="${ student.sex == '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="18%"><span class="text12"><c:out value="${ student.nameHiragana }" /></span></td>
	<td width="14%"><span class="text12"><c:out value="${ student.nameKanji }" /></span></td>
	<td width="11%" align="center"><span class="text12"><kn:formatDate value="${ student.birthday }" pattern="yyyy/MM/dd" /></span></td>
	<td width="11%"><span class="text12"><c:out value="${ student.telNo }" /></span></td>
</tr>
</c:forEach>

</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">
<input type="button" value="キャンセル" class="text12" style="width:120px;" onclick="submitForm('m101');">
<input type="button" value="生徒再表示" class="text12" style="width:120px;" onclick="submitReDisplay();">
<input type="button" value="完全に削除" class="text12" onclick="submitDelete();">
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</div>

</body>
</html>
<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/prototype.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/validate_string.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	function submitUpdate() {
		showLoadingLayer();
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "m104";
		document.forms[0].submit();
	}

	function submitBack() {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = "m101";
		document.forms[0].submit();
	}

	<%-- ロード中レイヤを表示する --%>
	function showLoadingLayer() {
		$("MainLayer").style.visibility = "hidden";
		window.scrollTo(0, 0);
		$("LoadingLayer").style.visibility = "visible";
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0"  onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div id="LoadingLayer" style="position:absolute;top:250px;left:450px;visibility:hidden;">
ロード中です ...
</div>

<div id="MainLayer">
<form action="<c:url value="M106Servlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m106">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="fileName" value="<c:out value="${ form.fileName }" />">
<input type="hidden" name="targetYear" value="<c:out value="${ form.targetYear }" />">
<input type="hidden" name="targetGrade" value="<c:out value="${ form.targetGrade }" />">
<input type="hidden" name="targetClass" value="<c:out value="${ form.targetClass }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->

<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--一括登録完了-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="15" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（新規登録・一括更新）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->


<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td align="center"><span style="font-weight:bold;font-family:Arial Black;color:#FF8F0E;font-size:25px">以下のデータを更新します</span></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">ファイル名：<c:out value="${ form.fileName }" /></b></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">【更新件数：<c:out value="${ CSVRegisterBean.updateCounter + CSVRegisterBean.deleteCounter }" />件】</b></td>
</tr>
<tr>
<td align="center"><span class="text14-hh">取り込みファイルの内容に上書きするデータが含まれています。<br>内容をご確認の上、「一括登録・更新を実施」もしくは「キャンセル」ボタンを押してください。</span></td>
</tr>
</table>
<!--/大タイトル-->

<!--ボタン-->
<div style="margin:10px">
<table border="0" cellpadding="0" cellspacing="0" width="653">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="653">
<tr valign="top">
<td bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="キャンセル" style="width:120px" onclick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="一括登録・更新を実施" style="width:160px" onclick="submitUpdate()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<table border="0" cellpadding="5" cellspacing="2" width="657">
<tr height="27">
<td bgcolor="#CDD7DD" width="60" align="center" rowspan="2"><b class="text12">行数</b></td>
<td bgcolor="#CDD7DD" width="571" align="center" colspan="2"><b class="text12">更新データ</b></td>
</tr>
<tr height="27">
<td bgcolor="#CDD7DD" width="280" align="center"><b class="text12">変更前</b></td>
<td bgcolor="#CDD7DD" width="275" align="center"><b class="text12">変更後</b></td>
</tr>
<c:forEach var="record" items="${ CSVRegisterBean.lineRecordList }">
<c:if test="${ record.updateRecord }">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ record.lineNo }" /></span></td>
<td bgcolor="#F4E5D6"><span class="text12">&nbsp;<c:if test="${ not empty record.original[1] }"><c:out value="${ record.original[1] }" />年&nbsp;</c:if><c:if test="${ not empty record.original[2] }"><c:out value="${ record.original[2] }" />クラス&nbsp;</c:if><c:if test="${ not empty record.original[3] }"><c:out value="${ record.original[3] }" />番&nbsp;</c:if>&nbsp;<c:out value="${ record.original[4] }" /></span></td>
<td bgcolor="#F4E5D6"><span class="text12">&nbsp;<c:out value="${ record.record[1] }" />年&nbsp;<c:out value="${ record.record[2] }" />クラス&nbsp;<c:out value="${ record.record[3] }" />番&nbsp;<c:out value="${ record.record[4] }" /></span></td>
</tr>
</c:if>
</c:forEach>
</table>

<!--ボタン-->
<div style="margin:10px">
<table border="0" cellpadding="0" cellspacing="0" width="653">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="653">
<tr valign="top">
<td bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="キャンセル" style="width:120px" onclick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="一括登録・更新を実施" style="width:160px" onclick="submitUpdate()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/一括登録完了-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

</form>
</div>

</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／メンテナンス</title>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_form.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
	<%@ include file="/jsp/script/open_declaration.jsp" %>
	<%@ include file="/jsp/script/ctrl_submit.jsp" %>

	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
	var _submitTarget = "";
	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

	var classData = new Array();
	<c:forEach var="class" items="${ ClassListBean.classList }" varStatus="status">
	classData[<c:out value="${ status.index }" />] = new Array(
		"<c:out value="${ class.year }" />",
		"<c:out value="${ class.grade }" />",
		"<c:out value="${ class.className }" />");
	</c:forEach>

	function submitAction(actionMode) {
		submitForm("m101", actionMode, true);
	}

	<%-- 一覧表示 --%>
	function submitView() {
		if (document.forms[0].conditionSelect.value == "1"
				&& !new Validator().isHiraganaAndSpace(document.forms[0].conditionText.value)) {
			alert("<kn:message id="m001a" />");
		} else {
			submitAction("3");
		}
	}

	<%-- ソート --%>
	function submitSort(value) {
		document.forms[0].sortKey.value = value;
		submitAction("5");
	}

	<%-- 表示形式切替 --%>
	function submitChangeStyle(value) {
		document.forms[0].displayStyle.value = value;
		submitAction("1");
	}

	<%-- 作業中宣言更新によるサブミット --%>
	function refreshWindow() {
		submitAction("2");
	}

	<%-- 新規登録 --%>
	function submitRegist() {
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
		//submitForm("m102");
		_submitTarget = "m102";
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

		<%-- 作業中宣言アラーム --%>
		<%@ include file="/jsp/script/declaration.jsp" %>
	}

	<%-- 統合 --%>
	function submitIntegration() {
		var util = new FormUtil();
		if (util.countChecked(document.forms[0].targetIndividualId)
				 + util.countChecked(document.forms[0].targetSubIndividualId) < 2) {
			alert("<kn:message id="m002a" />");
			return;
		}

		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
		//submitForm("m107");
		_submitTarget = "m107";
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

		<%-- 作業中宣言アラーム --%>
		<%@ include file="/jsp/script/declaration.jsp" %>
	}

	<%-- 編集 --%>
	function submitEdit() {
		if (new FormUtil().countChecked(document.forms[0].targetIndividualId) == 0) {
			alert("<kn:message id="m003a" />");
			return;
		}

		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
		//submitForm("m105");
		_submitTarget = "m105";
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

		<%-- 作業中宣言アラーム --%>
		<%@ include file="/jsp/script/declaration.jsp" %>
	}

	<%-- 非表示生徒一覧確認 --%>
	function submitNotDisplay() {
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD START --%>
		//submitForm("m108");
		_submitTarget = "m108";
		<%-- 2016/01/07 QQ)Nishiyama 大規模改修 UPD END   --%>

		<%-- 作業中宣言アラーム --%>
		<%@ include file="/jsp/script/declaration.jsp" %>
	}

	<%-- 受験届修正画面への遷移 --%>
	function submitAnswer(id, year, code) {
		document.forms[0].initIndividualId.value = id;
		document.forms[0].initExamYear.value = year;
		document.forms[0].initExamCd.value = code;
		submitForm("m201");
	}

	<%-- 模試コード説明 --%>
	function openExamInfo() {
		window.open("/private/meg/keinavi/guide/code_tbl_e.html","_blank",
			"resizable=yes,scrollbars=yes,status=yes,titlebar=yes,location=no,"
			+ "menubar=no,toolbar=no,top=0,left=0,width=680,height=600");
	}

	<%-- 学年のコンボを初期化する --%>
	function initGrade(target) {
		var form =  document.forms[0];
		var year = form.targetYear.value;
		var select = document.forms[0].targetGrade;
		select.options.length = 0;

		var beforeGrade = null;
		var array = new Array();
		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && beforeGrade != classData[i][1]) {
				var o = new Option(classData[i][1] + "年", classData[i][1]);
				o.selected = ("all" != target && array.length == 0) || o.value == target;
				array[array.length] = o;
				beforeGrade = classData[i][1];
			}
		}

		<%-- 全学年要素追加 --%>
		if (array.length > 0) {
			select.options[0] = new Option("全学年", "all");
			for (var i = 0; i < array.length; i++) {
				select.options[i + 1] = array[i];
			}
		}
	}

	<%--  クラスのコンボを初期化する --%>
	function initClass(target) {
		var form =  document.forms[0];
		var year = form.targetYear.value;
		var grade = form.targetGrade.value;
		var select = document.forms[0].targetClass;
		select.options.length = 0;

		if (form.targetGrade.value) {
			select.options[0] = new Option("全クラス", "all");
		}

		for (var i = 0; i < classData.length; i++) {
			if (classData[i][0] == year && classData[i][1] == grade) {
				var o = new Option(classData[i][2] + "クラス", classData[i][2]);
				if (o.value == target || ("all" != target && select.options.length == 1)) o.selected = true;
				select.options[select.options.length] = o;
			}
		}

		if (form.targetGrade.value) {
			form.viewButton.disabled = false;
		} else {
			form.viewButton.disabled = "disabled";
		}
	}

	<%-- 絞り込み条件を変えたときの処理 --%>
	function changeConditionStatus() {
		var value = document.forms[0].conditionSelect.value;
		var flag = value == '0';
		document.forms[0].conditionText.disabled = flag ? "disabled" : false;
		document.forms[0].conditionText.style.backgroundColor = flag ? "#CCCCCC" : "#FFFFFF";
		document.forms[0].conditionSubSelect.disabled = flag ? "disabled" : false;
	}

	<%-- 主生徒を選択したときの処理 --%>
	function changeSubStatus(obj) {
		var e = document.forms[0].targetSubIndividualId;
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				e[i].disabled = false;
			}
		} else {
			e.disabled = false;
		}
		<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD START --%>
		// var subObj = obj.parentNode.nextSibling.childNodes[0];
		var subObj = obj.parentNode.nextElementSibling.children[0];
		<%-- 2015/12/24 QQ)Hisakawa 大規模改修 UPD END   --%>
		subObj.disabled = "disabled";
		subObj.checked = false;
	}

	function init() {
		startTimer();
		var form = document.forms[0];

		<c:if test="${ form.backward == 'm101' }">
			window.scrollTo(form.scrollX.value, form.scrollY.value);
		</c:if>

		changeConditionStatus();
		initGrade("<c:out value="${ form.targetGrade }" />");
		initClass("<c:out value="${ form.targetClass }" />");

		var message = "<c:out value="${ErrorMessage}" />";
		if (message != "") alert(message);
	}

	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
	function workingDialogClose(val) {

		// ダイアログクローズ
		$(workingDialogObj).dialog('close');
		if (val != 1) {
			return;
		}
		// サブミット
		submitForm(_submitTarget);

	}
	<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="M101Servlet" />" method="POST" onsubmit="return false;">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="m101">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="scrollX" value="<c:out value="${ form.scrollX }" />">
<input type="hidden" name="scrollY" value="<c:out value="${ form.scrollY }" />">
<input type="hidden" name="displayStyle" value="<c:out value="${ form.displayStyle }" />">
<input type="hidden" name="sortKey" value="<c:out value="${ form.sortKey }" />">
<input type="hidden" name="pageNo" value="<c:out value="${ form.pageNo }" />">
<input type="hidden" name="initIndividualId" value="">
<input type="hidden" name="initExamYear" value="">
<input type="hidden" name="initExamCd" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="174"><img src="./mainte/img/ttl_mainte.gif" width="140" height="24" border="0" alt="メンテナンス"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="723"><img src="./mainte/img/ttl_mainte_read.gif" width="326" height="16" border="0" alt="生徒情報を一元的に管理することができます。"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<%@ include file="/jsp/admin/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="906" bgcolor="#EFF2F3" align="center">

<!--生徒情報管理（一覧）-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（新規登録・一括更新）</b></td>
<td width="400" bgcolor="#758A98" align="right">

<table border="0" cellpadding="2" cellspacing="0">
<tr>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<td<c:if test="${ empty DeclarationBean.time }"> bgcolor="#FFFFFF"</c:if>><input type="button" value="作業中宣言" class="text12" style="width:110px;<c:if test="${ not empty DeclarationBean.time }">background-color:#FB4733;color:#FFF;font-weight:bold;</c:if>" onclick="openDeclaration()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>></td>
<!--  <td<c:if test="${ empty DeclarationBean.time }"> bgcolor="#FFFFFF"</c:if>><input type="button" value="作業中宣言" class="text12" style="width:110px;<c:if test="${ not empty DeclarationBean.time }">background-color:#FB4733;color:#FFF;font-weight:bold;</c:if>" onclick="openDeclaration()"></td> -->
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="10" border="0" alt=""></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="text-align:left;margin:10px 0px 0px 10px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr>
<td width="100"><input type="button" value="生徒情報登録（新規登録・一括更新）" class="text12" onclick="submitRegist();"></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""></td>
<td width="738"><span class="text14-hh">生徒情報の新規登録、一括更新を行うには、左のボタンをクリックしてください。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="414" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">生徒情報管理（生徒情報一覧）</b></td>
<td width="400" bgcolor="#758A98" align="right">

<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td></td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="5" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->



<!--対象年度・学年・クラスの選択-->
<div style="margin-top:10px;">
<table border="0" cellpadding="2" cellspacing="1" width="846" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="840">
	<tr height="32">
	<td width="734" bgcolor="#E1E6EB">
	<div style="margin:8px;">
	<b class="text12">対象年度・学年・クラスの選択</b>
	<table border="0" cellpadding="2" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">年度&nbsp;：&nbsp;</span></td>
	<td>
	<select name="targetYear" class="text12" onchange="initGrade(document.forms[0].targetGrade.value);initClass(document.forms[0].targetClass.value);" style="width:60px">
		<option value="<c:out value="${ CurrentYear - 0 }" />"<c:if test="${ form.targetYear == CurrentYear - 0 }"> selected</c:if>><c:out value="${ CurrentYear - 0 }" /></option>
		<option value="<c:out value="${ CurrentYear - 1 }" />"<c:if test="${ form.targetYear == CurrentYear - 1 }"> selected</c:if>><c:out value="${ CurrentYear - 1 }" /></option>
		<option value="<c:out value="${ CurrentYear - 2 }" />"<c:if test="${ form.targetYear == CurrentYear - 2 }"> selected</c:if>><c:out value="${ CurrentYear - 2 }" /></option>
	</select>
	</td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">学年&nbsp;：&nbsp;</span></td>
	<td><select name="targetGrade" class="text12" onchange="initClass(document.forms[0].targetClass.value);" style="width:72px;">
	</select></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">クラス&nbsp;：&nbsp;</span></td>
	<td><select name="targetClass" class="text12" style="width:80px">
	<option value="_">全クラス</option>
	</select></td>
	</tr>
	</table>
	<b class="text12">さらに生徒情報を条件で絞り込む</b>
	<table border="0" cellpadding="2" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">条件&nbsp;：&nbsp;</span></td>
	<td><select name="conditionSelect" class="text12" onchange="changeConditionStatus();">
		<option value="0">指定なし</option>
		<option value="1"<c:if test="${ form.conditionSelect == '1' }"> selected</c:if>>かな氏名</option>
	</select>
	</td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><span class="text12">絞り込み条件を入力&nbsp;：&nbsp;</span></td>
	<td><input type="text" name="conditionText" size="50" value="<c:out value="${ form.conditionText }" />"></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td><select name="conditionSubSelect" class="text12" onchange="changeConditionStatus();">
		<option value="1"<c:if test="${ form.conditionSubSelect == '1' }"> selected</c:if>>と一致</option>
		<option value="2"<c:if test="${ form.conditionSubSelect == '2' }"> selected</c:if>>から始まる</option>
		<option value="3"<c:if test="${ form.conditionSubSelect == '3' }"> selected</c:if>>を含む</option>
		<option value="4"<c:if test="${ form.conditionSubSelect == '4' }"> selected</c:if>>あいまい検索</option>
	</select>
	</td>
	</tr>
	</table>
	</div>
	</td>
	<td width="100" bgcolor="#F4E5D6" align="center"><input type="button" value="一覧表示" name="viewButton" class="text12" style="width:80px;" onclick="submitView();" disabled></td>
	</tr>
	</table>
</td>
</tr>
<tr>
<td bgcolor="#FFFFFF">
	<table border="0" cellpadding="0" cellspacing="2" width="840">
	<tr height="32">
	<td width="836" bgcolor="#E1E6EB">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
	<td><span class="text12">表示形式&nbsp;：&nbsp;</span></td>
	<c:choose>
	<c:when test="${ form.displayStyle == '1' }">
	<td><b class="text12">生徒情報</b></td>
	<td><span class="text12" style="color:#2986B1;">&nbsp;｜&nbsp;</span></td>
	<td><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""><span class="text12">&nbsp;</span></td>
	<td><b class="text12"><a href="#here" onclick="submitChangeStyle('2');">模試結合状況</a></b></td>
	</c:when>
	<c:otherwise>
	<td><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""><span class="text12">&nbsp;</span></td>
	<td><b class="text12" style="color:#2986B1;"><a href="#here" onclick="submitChangeStyle('1');">生徒情報</a></b></td>
	<td><span class="text12" style="color:#2986B1;">&nbsp;｜&nbsp;</span></td>
	<td><b class="text12">模試結合状況</a></b></td>
	</c:otherwise>
	</c:choose>
	<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
	<%-- <td>&nbsp;&nbsp;<input type="button" value="模試結合状況絞込表示" onclick="submitForm('m109');"<c:if test="${ form.displayStyle == '1' }"> disabled</c:if>></td> --%>
	<td>&nbsp;&nbsp;<input type="button" value="模試結合状況絞込表示" onclick="submitForm('m109');"<c:if test="${ form.displayStyle == '1' || LoginSession.trial }"> disabled</c:if>></td>
	<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
	<c:if test="${ not empty ScoreExamListBean.dispExamMap }">
	<td>&nbsp;&nbsp;<b class="text12" style="color:#F00;">←模試結合状況絞込条件設定中</b></td>
	</c:if>
	</tr>
	</table>
	</td>
	</tr>
	</table>
</td>
</tr>
</table>
</div>
<!--/対象年度・学年・クラスの選択-->

<!--ツールバー-->
<div style="margin-top:6px;">
<table border="0" cellpadding="4" cellspacing="0" width="846">
<tr>
<td width="846" bgcolor="#93A3AD">

<table border="0" cellpadding="2" cellspacing="0" width="838">
<tr>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td bgcolor="#FFFFFF" width="100"><input type="button" value="CSV出力" class="text12" style="width:100px;" onclick="submitForm('m101_csv');"<c:if test="${ empty StudentSearchBean.year }"> disabled</c:if>></td> --%>
<td bgcolor="#FFFFFF" width="100"><input type="button" value="CSV出力" class="text12" style="width:100px;" onclick="submitForm('m101_csv');"<c:if test="${ empty StudentSearchBean.year || LoginSession.trial }"> disabled</c:if>></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
<c:if test="${ form.displayStyle == '1' }">
<td width="730"><img src="./shared_lib/img/parts/sp.gif" width="730" height="10" border="0" alt=""></td>
</c:if>
<c:if test="${ form.displayStyle == '2' }">
<td width="554"><img src="./shared_lib/img/parts/sp.gif" width="554" height="10" border="0" alt=""></td>
<td bgcolor="#FFFFFF" width="96"><input type="button" value="模試コード説明" class="text12" style="width:96px;" onclick="openExamInfo();"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="10" border="0" alt=""></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%--
<td bgcolor="#FFFFFF" width="24"><input type="button" value="＜" class="text12" style="width:24px;" onclick="submitAction('6');"<c:if test="${ ScoreExamListBean.firstPage }"> disabled</c:if>></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="10" border="0" alt=""></td>
<td bgcolor="#FFFFFF" width="24"><input type="button" value="＞" class="text12" style="width:24px;" onclick="submitAction('7');"<c:if test="${ ScoreExamListBean.lastPage  }"> disabled</c:if>></td>
--%>
<td bgcolor="#FFFFFF" width="24"><input type="button" value="＜" class="text12" style="width:24px;height:24px;padding:0px;" onclick="submitAction('6');"<c:if test="${ ScoreExamListBean.firstPage }"> disabled</c:if>></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="10" border="0" alt=""></td>
<td bgcolor="#FFFFFF" width="24"><input type="button" value="＞" class="text12" style="width:24px;height:24px;padding:0px;" onclick="submitAction('7');"<c:if test="${ ScoreExamListBean.lastPage  }"> disabled</c:if>></td>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
</c:if>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ツールバー-->

<c:if test="${ form.displayStyle == '1' }">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="846">
<tr bgcolor="#CDD7DD">
<td align="center" width="48"><span class="text12">主／従</span></td>
<td align="center" width="19"><span class="text12"></span></td>
<td align="center" width="102"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '1' }"><a href="#here" onclick="submitSort('11');"></c:when><c:otherwise><a href="#here" onclick="submitSort('1');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="103"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '2' }"><a href="#here" onclick="submitSort('12');"></c:when><c:otherwise><a href="#here" onclick="submitSort('2');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="103"><span class="text12"><c:out value="${ CurrentYear - 0 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '3' }"><a href="#here" onclick="submitSort('13');"></c:when><c:otherwise><a href="#here" onclick="submitSort('3');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" width="28"><span class="text12">性別</span></td>
<td align="center" width="122"><span class="text12"><c:choose><c:when test="${ form.sortKey == '4' }"><a href="#here" onclick="submitSort('14');"></c:when><c:otherwise><a href="#here" onclick="submitSort('4');"></c:otherwise></c:choose>かな氏名</a></span></td>
<td align="center" width="91"><span class="text12"><c:choose><c:when test="${ form.sortKey == '5' }"><a href="#here" onclick="submitSort('15');"></c:when><c:otherwise><a href="#here" onclick="submitSort('5');"></c:otherwise></c:choose>漢字氏名</a></span></td>
<td align="center" width="75"><span class="text12"><c:choose><c:when test="${ form.sortKey == '6' }"><a href="#here" onclick="submitSort('16');"></c:when><c:otherwise><a href="#here" onclick="submitSort('6');"></c:otherwise></c:choose>生年月日</a></span></td>
<td align="center" width="93"><span class="text12"><c:choose><c:when test="${ form.sortKey == '7' }"><a href="#here" onclick="submitSort('17');"></c:when><c:otherwise><a href="#here" onclick="submitSort('7');"></c:otherwise></c:choose>電話番号</a></span></td>
</tr>
</table>
</c:if>
<c:if test="${ form.displayStyle == '2' }">
<div style="width:846;text-align:left;">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="846">
<tr bgcolor="#CDD7DD">
<td align="center" rowspan="2" width="48"><span class="text12">主／従</span></td>
<td align="center" rowspan="2" width="104"><span class="text12"><c:out value="${ CurrentYear - 2 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '1' }"><a href="#here" onclick="submitSort('11');"></c:when><c:otherwise><a href="#here" onclick="submitSort('1');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" rowspan="2" width="101"><span class="text12"><c:out value="${ CurrentYear - 1 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '2' }"><a href="#here" onclick="submitSort('12');"></c:when><c:otherwise><a href="#here" onclick="submitSort('2');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" rowspan="2" width="101"><span class="text12"><c:out value="${ CurrentYear - 0 }" />年度</span><br><span class="text10"><c:choose><c:when test="${ form.sortKey == '3' }"><a href="#here" onclick="submitSort('13');"></c:when><c:otherwise><a href="#here" onclick="submitSort('3');"></c:otherwise></c:choose>学年／クラス／番号</a></span></td>
<td align="center" rowspan="2" width="145"><span class="text12"><c:choose><c:when test="${ form.sortKey == '4' }"><a href="#here" onclick="submitSort('14');"></c:when><c:otherwise><a href="#here" onclick="submitSort('4');"></c:otherwise></c:choose>かな氏名</a></span></td>
<c:if test="${ not empty ScoreExamListBean.dispExamList }">
	<c:set var="beforeExamYear" value="" />
	<c:set var="examCount" value="0" />
	<c:forEach var="exam" varStatus="status" items="${ ScoreExamListBean.dispExamList }">
		<c:if test="${ not empty beforeExamYear && beforeExamYear ne exam.examYear }">
			<td align="center" colspan="<c:out value="${ examCount }" />"><span class="text12"><c:out value="${ beforeExamYear }" /></span></td>
			<c:set var="examCount" value="0" />
		</c:if>
		<c:set var="beforeExamYear" value="${ exam.examYear }" />
		<c:set var="examCount" value="${ examCount + 1 }" />
	</c:forEach>
	<td align="center" colspan="<c:out value="${ examCount + 9 - ScoreExamListBean.dispExamListSize }" />"><span class="text12"><c:out value="${ beforeExamYear }" /></span></td>
</c:if>
</tr>

<tr bgcolor="#CDD7DD">
<c:set var="examCount" value="0" />
<c:forEach var="exam" varStatus="status" items="${ ScoreExamListBean.dispExamList }">
	<c:set var="examCount" value="${ examCount + 1 }" />
	<c:set var="width" value="27" />
	<c:if test="${ examCount == 9 }"><c:set var="width" value="${ width +  18 }" /></c:if>
	<td align="center" width="<c:out value="${ width }" />"><span class="text12" title="<c:out value="${ exam.examName }" />"><c:out value="${ exam.examCD }" /></span></td>
</c:forEach>
<c:if test="${ examCount < 9 }">
<c:forEach varStatus="status" begin="${ examCount + 1 }" end="9">
	<c:set var="width" value="27" />
	<c:if test="${ status.last }"><c:set var="width" value="${ width +  18 }" /></c:if>
	<td align="center" width="<c:out value="${ width }" />"></td>
</c:forEach>
</c:if>
</tr>
</table>
</div>
</c:if>

<div style="height:242px;width:846;overflow-y:scroll;border-width:2px;border-style:inset;text-align:left;background-color:#F4E5D6;">

<%-- 表示形式「生徒情報」 --%>
<c:if test="${ form.displayStyle == '1' }">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">

<c:forEach var="student" items="${ StudentList }">
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><input type="radio" name="targetIndividualId" value="<c:out value="${ student.individualId }" />" onclick="changeSubStatus(this);"<c:if test="${ student.selectedFlag == 1 }"> checked</c:if>></td>
	<td width="3%" align="center"><input type="radio" name="targetSubIndividualId" value="<c:out value="${ student.individualId }" />"<c:if test="${ student.selectedFlag == 2 }"> checked</c:if><c:if test="${ student.selectedFlag == 1 }"> disabled</c:if>></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.cmark == '1' }">C</c:if></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.beforeLastGrade >= 0 }"><c:out value="${ student.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.lastGrade >= 0 }"><c:out value="${ student.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.currentGrade >= 0 }"><c:out value="${ student.currentGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.currentClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.currentClassNo }" /></span></td>
	<td width="4%" align="center"><span class="text12"><c:choose><c:when test="${ student.sex == '1' }">男</c:when><c:when test="${ student.sex == '2' }">女</c:when><c:otherwise>不明</c:otherwise></c:choose></span></td>
	<td width="16%"><span class="text12"><c:out value="${ student.nameHiragana }" /></span></td>
	<td width="12%"><span class="text12"><c:out value="${ student.nameKanji }" /></span></td>
	<td width="10%" align="center"><span class="text12"><kn:formatDate value="${ student.birthday }" pattern="yyyy/MM/dd" /></span></td>
	<td width="10%"><span class="text12"><c:out value="${ student.telNo }" /></span></td>
</tr>
</c:forEach>

</table>
</c:if>

<%-- 表示形式「模試結合状況」 --%>
<c:if test="${ form.displayStyle == '2' }">
<table bgcolor="#FFFFFF" border="0" cellpadding="2" cellspacing="2" width="100%">

<c:forEach var="student" items="${ StudentList }">
<tr height="27" bgcolor="#F4E5D6">
	<td width="3%" align="center"><input type="radio" name="targetIndividualId" value="<c:out value="${ student.individualId }" />" onclick="changeSubStatus(this);"<c:if test="${ student.selectedFlag == 1 }"> checked</c:if>></td>
	<td width="3%" align="center"><input type="radio" name="targetSubIndividualId" value="<c:out value="${ student.individualId }" />"<c:if test="${ student.selectedFlag == 2 }"> checked</c:if><c:if test="${ student.selectedFlag == 1 }"> disabled</c:if>></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.beforeLastGrade >= 0 }"><c:out value="${ student.beforeLastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.beforeLastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.lastGrade >= 0 }"><c:out value="${ student.lastGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.lastClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.lastClassNo }" /></span></td>
	<td width="3%" align="center"><span class="text12"><c:if test="${ student.currentGrade >= 0 }"><c:out value="${ student.currentGrade }" /></c:if></span></td>
	<td width="4%" align="center"><span class="text12"><c:out value="${ student.currentClassName }" /></span></td>
	<td width="6%" align="center"><span class="text12"><c:out value="${ student.currentClassNo }" /></span></td>
	<td width="19%"><span class="text12"><c:out value="${ student.nameHiragana }" /></span></td>
	<c:set var="examCount" value="0" />
	<c:forEach var="exam" varStatus="status" items="${ ScoreExamListBean.dispExamList }">
		<c:set var="examCount" value="${ examCount + 1 }" />
		<c:set var="examId" value="${exam.examYear}${exam.examCD}" />
		<c:choose>
		<c:when test="${ student.takenExamMap[examId] }">
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
<%--		<td align="center" width="4%"><span class="text12"><a href="#here" onclick="submitAnswer('<c:out value="${ student.individualId }" />','<c:out value="${ exam.examYear }" />','<c:out value="${ exam.examCD }" />');">○</a></span></td>	--%>
			<c:choose>
			<c:when test="${ LoginSession.trial }">
				<td align="center" width="4%"><span class="text12"><font color="#2986B1"><u>○</u></font></span></td>
			</c:when>
			<c:otherwise>
				<td align="center" width="4%"><span class="text12"><a href="#here" onclick="submitAnswer('<c:out value="${ student.individualId }" />','<c:out value="${ exam.examYear }" />','<c:out value="${ exam.examCD }" />');">○</a></span></td>
			</c:otherwise>
			</c:choose>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
		</c:when>
		<c:otherwise>
			<c:set var="checked" value="" />
			<c:set var="value" value="${ examId }_${ student.individualId }" />
			<c:if test="${ TempExamineeData.tempExamineeMap[examId][student.individualId] }">
				<c:set var="checked" value=" checked" />
				<input type="hidden" name="registedTempExaminee" value="<c:out value="${ value }" />">
			</c:if>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
			<%-- <td align="center" width="4%"><input type="checkbox" name="tempExaminee" value="<c:out value="${ value }" />"<c:out value="${ checked }" />></td> --%>
			<td align="center" width="4%"><input type="checkbox" name="tempExaminee" value="<c:out value="${ value }" />"<c:out value="${ checked }" /><c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>></td>
<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
		</c:otherwise>
		</c:choose>
	</c:forEach>
	<c:if test="${ examCount < 9 }">
	<c:forEach begin="${ examCount + 1 }" end="9">
		<td align="center" width="4%"></td>
	</c:forEach>
	</c:if>
</tr>
</c:forEach>

</table>
</c:if>

</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/生徒情報管理（一覧）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
	<tr valign="top">
		<td width="906" bgcolor="#FBD49F">
			<table border="0" cellpadding="6" cellspacing="0" width="880">
				<tr>
					<td align="left" nowrap>
						<input type="button" value="主・従の統合" class="text12" style="width:150px;" onclick="submitIntegration();">
						<input type="button" value="編集／主・従の統合" class="text12" style="width:150px;" onclick="submitEdit();">
					</td>
				</tr>
				<tr>
					<td align="center" nowrap>
						<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD START --%>
						<%--
						<input type="button" value="非表示生徒一覧" class="text12" onclick="submitNotDisplay();">
						<input type="button" value="作業履歴確認用CSVファイル出力" class="text12" onclick="submitForm('m101_log');">
						<input type="button" value="仮受験者のチェックをすべて解除" class="text12" onclick="submitAction('8');">
						--%>
						<input type="button" value="非表示生徒一覧" class="text12" onclick="submitNotDisplay();"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
						<input type="button" value="作業履歴確認用CSVファイル出力" class="text12" onclick="submitForm('m101_log');"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
						<input type="button" value="仮受験者のチェックをすべて解除" class="text12" onclick="submitAction('8');"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
						<%-- 2015/12/16 QQ)Hisakawa 大規模改修 UPD END --%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="908">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">

<input type="button" value="メンテナンス完了　（プロファイル選択画面へ）" class="text12" style="width:320px;" onclick="submitForm('w002')">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/admin/footer.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/maintenance/x011.jsp" %>
<%-- 2016/01/07 QQ)Nishiyama 大規模改修 ADD END   --%>

</form>
</body>
</html>
<%@ page import="jp.co.fj.keinavi.util.KNUtil" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
	//タイムアウト時間(秒)
	var TIMELIMIT = <%=session.getMaxInactiveInterval()%>;
	var timer;

	//タイマーを開始する。
	function startTimer() {
		timer = setTimeout("_timeout()", TIMELIMIT * 1000);
	}

	//タイムアウト処理を実行する。
	function _timeout() {
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].target = "_self";
        document.forms[0].method = "POST";
        document.forms[0].action = "<%=KNUtil.getContextPath(request)%>/<c:url value="Timeout"/>";
        document.forms[0].submit();
	}

	//タイマーをリセットする。
	function _resetTimer() {
		clearTimeout(timer);
		startTimer();
	}

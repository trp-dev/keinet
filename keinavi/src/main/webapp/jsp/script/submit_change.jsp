<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

//「切替」リンク
function submitChange() {
	document.forms[0].forward.value = "w002";
	document.forms[0].backward.value = "<c:out value="${param.forward}" />";
	document.forms[0].target = "_self";

	var submitChangedFlg = true;

	if (<c:out value="${Profile.changed}" /> || document.forms[0].changed.value == "1") {

		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 DEL START --%>
		<%--
		<c:choose>
		<c:when test="${ Profile.template }">
		returnValue = showModalDialog(
				"<c:url value="Dialog" />",
				new Array("<kn:message id="w007c" />", "切替", "プロファイル保存して切替", "キャンセル"),
				"status:no;help:no;scroll:no;dialogWidth:400px;dialogHeight:220px");
		</c:when>
		<c:otherwise>
		returnValue = showModalDialog(
				"<c:url value="Dialog" />",
				new Array("<kn:message id="w004c" />", "切替", "プロファイル保存して切替", "キャンセル"),
				"status:no;help:no;scroll:no;dialogWidth:400px;dialogHeight:160px");
		</c:otherwise>
		</c:choose>
		// 切替
		if (returnValue == 1) {
			document.forms[0].submit();

		// プロファイル保存して切替
		} else if (returnValue == 2) {
			document.forms[0].forward.value = "w101";
			document.forms[0].exit.value = "w002";
			document.forms[0].submit();

		// キャンセル（デフォルト）
		} else {
			return;
		}
		--%>
		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 DEL END --%>

		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>

		submitChangedFlg = false;

		// ダイアログの設定値
		var dialogMsg = "";
		var dialogHeight = 0;

		<c:choose>
			<c:when test="${ Profile.template }">
				dialogMsg = "<kn:message id="w007c" />";
				dialogHeight = 280;
			</c:when>
			<c:otherwise>
				dialogMsg = "<kn:message id="w004c" />";
				dialogHeight = 200;
			</c:otherwise>
		</c:choose>

		// ダイアログ画面のパラメータ指定
		// メッセージ
		document.getElementById("dialogMsg").innerHTML = dialogMsg;
		// ボタンの文言
		document.getElementById("dialogBtn1").value = "切替";
		document.getElementById("dialogBtn2").value = "プロファイル保存して切替";
		document.getElementById("dialogBtn3").value = "キャンセル";

		// サイズの指定
		$( '#confirmDialogDiv' ).dialog( { width: 450, height: dialogHeight } );

		confirmDialogPattern = "submit_change";

	    // 確認ダイアログ　オープン
	    $(confirmDialogObj).dialog('open');

		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END --%>
	}

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	if (submitChangedFlg == true) {
		document.forms[0].submit();
	}
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END --%>
}


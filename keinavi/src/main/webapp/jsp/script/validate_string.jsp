<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD END --%>

	function isKanaName(value) {

		if (value == "") {
			return false;
		}

		for (var i=0; i<value.length; i++) {
			var c = value.charCodeAt(i);
			if (!((c >= 12353 && c <= 12435) || c == 12540 <%-- �Ђ炪�� �{ �[ --%>
					<%-- || (c >= 65382 && c <= 65439) --%><%-- ���p�J�^�J�i --%>
					|| (c >= 12449 && c <= 12534) <%-- �S�p�J�^�J�i --%>
					|| c == 32 || c == 12288)) { <%-- �S���p�X�y�[�X --%>
				return false;
			}
		}

		return !new Validator().isWQuotesOrComma(value);
	}

	function isKanjiName(value) {

		<%-- 20�o�C�g�܂� --%>
		return new JString(value).getLength() <= 20
				&& !new Validator().isWQuotesOrComma(value);
	}

	function isTelNo(value) {

		if (value.length == 0) {
			return true;
		}

		if (value.length > 11 || !new Validator().isNumber(value)) {
			return false;
		}

		return true;
	}

	function isBirthday(year, month, date) {

		if (!new Validator().isDate(year, month, date)) {
			return false;
		}

		if (year < "1900" || year > new Date().getFullYear()) {
			return false;
		}

		return true;
	}

<%@ page pageEncoding="MS932" %>

// 確認ダイアログ　クローズ処理
function confirmDialogClose(val) {

	// ダイアログクローズ
	$(confirmDialogObj).dialog('close');

	// 終了
	if (val == 1) {
		if (confirmDialogPattern == "submit_exit") {
			submitSweep();
		} else if (confirmDialogPattern == "submit_change") {
			document.forms[0].submit();
		} else if (confirmDialogPattern == "com_close") {
			<c:if test="${CommonForm.save == 1}">
				if (window.opener.submitClosed) window.opener.submitClosed();
			</c:if>
			window.close();
		} else if (confirmDialogPattern == "cm701" || confirmDialogPattern == "u003") {
			window.close();
		}
	// プロファイル保存して終了
	} else if (val == 2) {
		if (confirmDialogPattern == "submit_exit") {
			if (window.clearDisabled) clearDisabled();
			document.forms[0].forward.value = "w101";
			document.forms[0].backward.value = "<c:out value="${param.forward}" />";
			document.forms[0].exit.value = "logout";
			document.forms[0].submit();
			return;
		} else if (confirmDialogPattern == "submit_change") {
			if (window.clearDisabled) clearDisabled();
			document.forms[0].forward.value = "w101";
			document.forms[0].exit.value = "w002";
			document.forms[0].submit();
		} else if (confirmDialogPattern == "com_close") {
			submitRegist(1);
		} else if (confirmDialogPattern == "cm701" || confirmDialogPattern == "u003") {
			submitForm();
		}
	// キャンセル（デフォルト）
	} else {
		return;
	}

}

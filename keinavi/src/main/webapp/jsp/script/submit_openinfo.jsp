<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

    // お知らせウィンドウを開く
    function openInfoTop(div) {
        var url = "<c:url value="NewsTop" />";
        url += "?forward=n001";
        url += "&backward=<c:out value="${ param.forward }" />";
        url += "&displayDiv=" + div;
        window.open(url, "n001", "top=50,left=150,width=680,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
    }

    // お知らせ詳細ウィンドウを開く
    function openInfoDetail(div, Id) {
        var url = "<c:url value="NewsDetail" />";
        url += "?forward=n002";
        url += "&backward=<c:out value="${ param.forward }" />";
        url += "&infoId=" + Id;
        url += "&displayDiv=" + div;
        window.open(url, "n002", "top=50,left=150,width=680,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
    }


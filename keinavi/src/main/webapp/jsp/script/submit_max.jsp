<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

	// Disabledを解除した要素の入れ物
	var c = new Array();

	/**
	*
	* ダウンロード前チェック（オーバーライド）
	*/
	function validateDownload() {
		var form = document.forms[0];
		// 入力チェック
		if (!validate(1)) return false;

		// 対象模試チェック
		if (form.targetYear.selectedIndex < 0) {
			alert("模試データが無いため出力できません。");
			return false;
		}

		printDialogPattern = "submit_max";

		return true;
	}

	//「表示」「印刷」ボタン
	function submitSheet(value) {
		var form = document.forms[0];

		// 入力チェック
		if (!validate(1)) return;

		// 対象模試チェック
		if (form.targetYear.selectedIndex < 0) {
			alert("模試データが無いため出力できません。");
			return;
		}

		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
		printDialogPattern = "submit_max";
		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

		// 確認ダイアログ（印刷ボタン押下時）
		if (value == 0) {

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD START --%>
//			returnValue = showModalDialog(
//					"<c:url value="PrintDialog" />",
//					null,
//					"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

//			switch (returnValue) {
//				case 1: form.printFlag.value = 8; break;
//				case 2: form.printFlag.value = 2; break;
//				default: return;
//			}

		    // 印刷確認ダイアログ　オープン
		    $(printDialogObj).dialog('open');

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD END   --%>

		} else {
			form.printFlag.value = value;

			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
			printDialogCloseAfterProc();
			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
		}

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 DEL START --%>
		// Disabledを解除する
//		clearDisabled();

		// 出力先ウィンドウを開く
//		form.forward.value = "sheet";
//		form.backward.value = "<c:out value="${param.forward}" />";
//		form.save.value = "1";
//		form.submit();

		// フラグを戻す
//		document.forms[0].save.value = "0";

		// Disabledを復元する
//		restoreDisabled();
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 DEL END   --%>
	}

	// トップ画面の画面IDを取得する
	function getTopScreen() {
		return "<c:out value="${param.forward}" />".substring(0, 3) + "1";
	}

	//「登録して戻る」ボタン
	function submitRegist(){
		// 入力チェック
		if (!validate(0)) return;

		// Disabledは解除する
		clearDisabled();

		document.forms[0].forward.value = getTopScreen();
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].save.value = "1";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	// 戻り先はトップページで固定
	function submitBack() {
		document.forms[0].forward.value = getTopScreen();
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	// 操作不能を解除する
	function clearDisabled() {
		var e = document.forms[0].elements;
		for (var i=0; i<e.length; i++) {
			if (e[i].disabled) c[c.length] = e[i];
			e[i].disabled = false;
		}
	}

	// 解除したDisabledを復活させる
	function restoreDisabled() {
		for (var i=0; i<c.length; i++) c[i].disabled = "disabled";
		c.length = 0;
	}

	// 模試を変更したらリロード
	function changeExamSelect() {
		clearDisabled();
		document.forms[0].forward.value = "<c:out value="${param.forward}" />";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].changeExam.value = "1";
		document.forms[0].scrollX.value = document.body.scrollLeft;
		document.forms[0].scrollY.value = document.body.scrollTop;
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

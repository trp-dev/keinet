<%@ page pageEncoding="MS932" %>
(function ($) {
	$.fn.serializeAll = function () {
		var data = $(this).serialize();
		$(':disabled[name]', this).each(function () {
			if ((this.type == "radio" && this.checked == true)) {
				data += "&" + this.name + "=" + $(this).val();
			} else if (this.type == "checkbox" && this.checked == true) {
				data += "&" + this.name + "=" + $(this).val();
			} else if (this.type != "radio"  && this.type != "checkbox") {
				data += "&" + this.name + "=" + $(this).val();
			}
		});
		return data;
	}
})(jQuery);

function validateDownload() {
	return true;
}


function download_saveFlgOn() {
// 2019/10/11 QQ)Tanioka 入力エラー時にプロファイルが保存されている動作の修正 UPD START
//    document.forms[0].save.value = "1";
// 2019/10/11 QQ)Tanioka 入力エラー時にプロファイルが保存されている動作の修正 UPD END
    download();
}

function download(validateFlg) {

	var form = document.forms[0];

	// ダウンロード前チェック
	if (validateFlg == null && !validateDownload()) {
		return;
// 2019/10/11 QQ)Tanioka 入力エラー時にプロファイルが保存されている動作の修正 UPD START
//	}
	} else {
		// Disabledを解除する
		//clearDisabled();

		if(printDialogPattern != "noProfile"){
			//プロファイル保存フラグON
			document.forms[0].save.value = "1";
		}

		// Disabledを復元する
		//restoreDisabled();
	}
// 2019/10/11 QQ)Tanioka 入力エラー時にプロファイルが保存されている動作の修正 UPD END

	$.blockUI({ message: '<img src="./shared_lib/img/wait.gif" />',
		"css": {
			"background": "none", /* gif画像の背景色(#666666) */
			"border": "none"
		}
    });

	form.printFlag.value = 5;

	form.backward.value = "<c:out value="${param.forward}" />";
	document.forms[0].forward.value = "sheet";

	if (printDialogPattern == "s001") {

		document.forms[0].forward.value = "sheet";

		<c:choose>
			<c:when test="${MenuSecurity.menu['200']}">
				document.forms[0].backward.value = "b000";
			</c:when>
			<c:otherwise>
				document.forms[0].backward.value = "s000";
			</c:otherwise>
		</c:choose>

	}
<%-- 2019/09/04 QQ)Tanioka 特例成績データ申請一覧確認画面 ダウンロード障害修正 ADD START --%>
	else if(printDialogPattern == "sd206") {
		document.forms[0].forward.value = "sd206";
		form.printFlag.value = 1;
	}
<%-- 2019/09/04 QQ)Tanioka 特例成績データ申請一覧確認画面 ダウンロード障害修正 ADD END --%>
<%-- 2019/07/10 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	if (printDialogPattern == "printDialog2") {
		form.printFlag.value = 3;
	}
<%-- 2019/07/10 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<%-- 2019/07/10 QQ)nagai 個人成績分析の画面の対象帳票が出力されない不具合を修正 ADD START --%>
	else if (printDialogPattern == "printStudent") {
		form.printFlag.value = 1;
	}
<%-- 2019/07/10 QQ)nagai 個人成績分析の画面の対象帳票が出力されない不具合を修正 ADD END   --%>

	// 操作対象のフォーム要素を取得
	var $form = $(form);

	// 送信ボタンを取得
	var $button = $form.find('button');

	var data = $form.serializeAll()
    // 送信
	$.ajax({
		url: $form.attr('action'),
		type: $form.attr('method'),
		data: data,
		// 送信前
		beforeSend: function(xhr, settings) {
			// ボタンを無効化し、二重送信を防止
			$button.attr('disabled', true);
		},
		// 応答後
		complete: function(xhr, textStatus) {
			// ボタンを有効化し、再送信を許可
			$button.attr('disabled', false);
			// Disabledを復元する
			restoreDisabled();
			$.unblockUI();
        },
		// 通信成功時の処理
		success: function(result, textStatus, xhr) {
			$.unblockUI();
			if (result === "NO_DATA") {
				alert('対象データが存在しません。');
			} else {
				// ダウンロード用のサーブレットを呼び出す
				location.href = "<c:url value="DownloadServlet" />";
			}
		},
		// 通信失敗時の処理
		error: function(xhr, textStatus, error) {
			alert('NG...');
		}
	});

	// フラグを戻す
	document.forms[0].save.value = "0";
}

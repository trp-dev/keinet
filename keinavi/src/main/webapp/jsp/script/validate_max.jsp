<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

function validate(mode) {

	var form = document.forms[0];
	// チェック用クラス
	var fu = new FormUtil();
	// メッセージ
	var message = "";

	// 型・科目をチェックするかどうか
	var check = true;
	<c:if test="${ param.forward == 'c004' }">check = form.optionItem.checked;</c:if>

	// 帳票出力時のチェック
	if (mode) {
		// 共通項目設定のチェック
		message += validateCommon();
		// 型のカウント
		var typeCount = <c:out value="${ SubjectCountBean.typeCount }" />;

		// 型と科目がある場合のチェック
		if (form.commonType && form.commonCourse && check) {
			// 共に個別設定利用
			if (form.commonType[1].checked && form.commonCourse[1].checked) {
				if (fu.countChecked(form.analyzeType) == 0 && fu.countChecked(form.analyzeCourse) == 0)
					message += "<kn:message id="s013a" />\n";
			// 型のみ個別設定利用
			} else if (form.commonType[1].checked) {
				if (fu.countChecked(form.analyzeType) == 0
						&& "<kn:com item="course" />" == "設定なし")
					message += "<kn:message id="s014a" />\n";
			// 科目のみ個別設定利用
			} else if (form.commonCourse[1].checked) {
				if (("<kn:com item="type" />" == "設定なし" || typeCount == 0)
						&& fu.countChecked(form.analyzeCourse) == 0)
					message += "<kn:message id="s015a" />\n";
			}
		// 科目のみの場合のチェック
		} else if (form.commonCourse && form.commonCourse[1].checked && check) {
			if (fu.countChecked(form.analyzeCourse) == 0) {
				message += "<kn:message id="s009a" />\n";
			}
		}

	// 登録時のチェック
	} else if (check) {
		// 型と科目がある場合のチェック
		if (form.commonType && form.commonCourse) {
			<c:if test="${ not isAbilityExam }">
			if (form.commonType[1].checked && fu.countChecked(form.analyzeType) == 0) {
				message += "<kn:message id="s008a" />\n";
			}
			</c:if>
			if (form.commonCourse[1].checked && fu.countChecked(form.analyzeCourse) == 0) {
				message += "<kn:message id="s009a" />\n";
			}
		// 科目のみの場合のチェック
		} else if (form.commonCourse && form.commonCourse[1].checked) {
			if (fu.countChecked(form.analyzeCourse) == 0) {
				message += "<kn:message id="s009a" />\n";
			}
		}
	}

	// フォーマットのみ
	if (form.formatItem && !form.optionItem) {
		if (fu.countChecked(form.formatItem) == 0) {
			message += "<kn:message id="s010a" />\n";
		}
	}

	// フォーマット＆オプション
	if (form.formatItem && form.optionItem) {
	<%-- 過回比較（成績概況）のセンターリサーチは他校比較オプションは対象外 --%>
	<c:choose>
		<c:when test="${ param.forward == 's402' && isCenterResearch }">
			var count = 0;
			var e = form.optionItem;
			if (e.length) {
				for (var i = 0; i < e.length; i++) {
					if (e[i].checked && e[i].value != "CompOther") count++;
				}
			} else {
				if (e.checked && e.value != "CompOther") count++;
			}
			if (fu.countChecked(form.formatItem) == 0 && count == 0) {
				message += "<kn:message id="s011a" />\n";
			}
		</c:when>
		<c:otherwise>
			<%-- 受験学力測定テストの場合、チェックボックスが無効になっている場合があるのでその対策 --%>
			if (fu.countEnabled(form.formatItem) == 0 && fu.countEnabled(form.optionItem) == 0) {
				message += "<kn:message id="s011a" />\n";
			}
		</c:otherwise>
	</c:choose>
	}

	// 表示対象
	if (form.dispUnivStudent) {
		if (fu.countChecked(form.dispUnivStudent) == 0) {
			message += "<kn:message id="s016a" />\n";
		}
	}

	// 評価区分
	if (form.dispUnivRating) {
		if (fu.countChecked(form.dispUnivRating) == 0) {
			message += "<kn:message id="s019a" />\n";
		}
	}

	if (message != "") {
		alert(message);
		return false;
	}

	<%-- グラフ出力対象チェックは帳票出力時のみ --%>
	if (!mode) {
		return true;
	}

	var gc = new GraphChecker();

	var checked = new Array();
	for (var i = 0; i < form.elements.length; i++) {
		var e = form.elements[i];
		if (e.type == "checkbox" && e.checked && !e.disabled) {
			checked[e.value] = true;
		}
	}

	<c:choose>

	<%-- 校内成績 - 偏差値分布 --%>
	<c:when test="${ param.forward == 's003' }">
		if (checked["GraphDist"] || checked["GraphBuildup"] || checked["GraphCompRatio"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
		}
	</c:when>

	<%-- 校内成績 - 設問別成績 --%>
	<c:when test="${ param.forward == 's004' }">
		if (checked["Graph"]) {
			gc.checkCourseDetail(form.commonCourse[0].checked,
					<kn:graph item="course" />, fu.countChecked(form.graphCourse));
		}
	</c:when>

	<%-- 過年度比較 - 偏差値分布 --%>
	<c:when test="${ param.forward == 's103' }">
		if (checked["GraphDist"] || checked["GraphBuildup"] || checked["GraphCompRatio"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
		}
	</c:when>

	<%-- クラス比較 - 成績概況 --%>
	<c:when test="${ param.forward == 's202' || param.forward == 'c102' }">
		if (checked["Graph"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
			gc.checkClass(<kn:graph item="class" />);
		}
	</c:when>

	<%-- クラス比較 - 偏差値分布 --%>
	<c:when test="${ param.forward == 's203' || param.forward == 'c103' }">
		if (checked["GraphDist"] || checked["GraphBuildup"] || checked["GraphCompRatio"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
			gc.checkClass(<kn:graph item="class" />);
		}
	</c:when>

	<%-- クラス比較 - 設問別成績 --%>
	<c:when test="${ param.forward == 's204' || param.forward == 'c104' }">
		if (checked["Graph"]) {
			gc.checkCourseDetail(form.commonCourse[0].checked,
					<kn:graph item="course" />, fu.countChecked(form.graphCourse));
			gc.checkClass(<kn:graph item="class" />);
		}
	</c:when>

	<%-- 他校比較 - 成績概況 --%>
	<c:when test="${ param.forward == 's302' }">
		if (checked["Graph"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
			gc.checkSchool(<kn:graph item="school" />);
		}
	</c:when>

	<%-- 他校比較 - 偏差値分布 --%>
	<c:when test="${ param.forward == 's303' }">
		if (checked["GraphDist"] || checked["GraphBuildup"] || checked["GraphCompRatio"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
			gc.checkSchool(<kn:graph item="school" />);
		}
	</c:when>

	<%-- 他校比較 - 設問別成績 --%>
	<c:when test="${ param.forward == 's304' }">
		if (checked["Graph"]) {
			gc.checkCourseDetail(form.commonCourse[0].checked,
					<kn:graph item="course" />, fu.countChecked(form.graphCourse));
			gc.checkSchool(<kn:graph item="school" />);
		}
	</c:when>

	<%-- 過回比較 - 成績概況 --%>
	<c:when test="${ param.forward == 's402' }">
		if (checked["Graph"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
		}
		if (checked["Graph"] && checked["CompClass"]) {
			gc.checkClass(<kn:graph item="class" />);
		}
		if (checked["Graph"] && checked["CompOther"]) {
			gc.checkSchool(<kn:graph item="school" />);
		}
	</c:when>

	<%-- 過回比較 - 偏差値分布 --%>
	<c:when test="${ param.forward == 's403' }">
		if (checked["GraphDist"] || checked["GraphBuildup"] || checked["GraphCompRatio"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
		}
	</c:when>

	<%-- クラス成績概況 - 偏差値分布 --%>
	<c:when test="${ param.forward == 'c002' }">
		if (checked["GraphDist"] || checked["GraphBuildup"]) {
			gc.checkSubjectDetail(form.commonType[0].checked, form.commonCourse[0].checked,
					<kn:graph item="type" />, <kn:graph item="course" />,
					fu.countChecked(form.graphType), fu.countChecked(form.graphCourse));
		}
	</c:when>

	</c:choose>

	if (gc.hasError()) {
		alert(gc.createMessage());
		return false;
	}

	return true;
}

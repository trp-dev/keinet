<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<%--「終了」ボタン --%>
function _submitExit() {
	document.forms[0].forward.value = "logout";
	document.forms[0].backward.value = "<c:out value="${ param.forward }" />";
	document.forms[0].submit();
}

function submitExit() {

	<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
	// 処理制御用
	var submitSweepFlg = true;
	<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>


	<%-- プロファイルに変更があった場合かつ分析モード --%>
	<c:if test="${ not iCommonMap.mendanMode }">
	if (<c:out value="${Profile.changed}" /> || document.forms[0].changed.value == "1") {

		<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
		submitSweepFlg = false;
		<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>


		<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL START --%>
		<%--
		<c:choose>
		<c:when test="${ Profile.template }">
		var returnValue = showModalDialog(
				"<c:url value="Dialog" />",
				new Array("<kn:message id="w008c" />", "終了", "プロファイル保存して終了", "キャンセル"),
				"status:no;help:no;scroll:no;dialogWidth:400px;dialogHeight:220px");
		</c:when>
		<c:otherwise>
		var returnValue = showModalDialog(
				"<c:url value="Dialog" />",
				new Array("<kn:message id="w005c" />", "終了", "プロファイル保存して終了", "キャンセル"),
				"status:no;help:no;scroll:no;dialogWidth:400px;dialogHeight:160px");
		</c:otherwise>
		</c:choose>
		// 終了
		if (returnValue == 1) {
			// 2015/12/22 QQ)Nishiyama 大規模改修 ADD START
			submitSweepFlg = true;
			// 2015/12/22 QQ)Nishiyama 大規模改修 ADD END

		// プロファイル保存して終了
		} else if (returnValue == 2) {
			if (window.clearDisabled) clearDisabled();
			document.forms[0].forward.value = "w101";
			document.forms[0].backward.value = "<c:out value="${param.forward}" />";
			document.forms[0].exit.value = "logout";
			document.forms[0].submit();
			return;
		// キャンセル（デフォルト）
		} else {
			return;
		}
		  --%>
		<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL END   --%>

		<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
		var dialogMsg = "";
		var dialogHeight = 0;
		<c:choose>
			<c:when test="${ Profile.template }">
				dialogMsg = "<kn:message id="w008c" />";
				dialogHeight = 280;
			</c:when>
			<c:otherwise>
				dialogMsg = "<kn:message id="w005c" />";
				dialogHeight = 200;
			</c:otherwise>
		</c:choose>

		// ダイアログ画面のパラメータ指定
		// メッセージ
		document.getElementById("dialogMsg").innerHTML = dialogMsg;
		// ボタンの文言
		document.getElementById("dialogBtn1").value = "終了";
		document.getElementById("dialogBtn2").value = "プロファイル保存して終了";
		document.getElementById("dialogBtn3").value = "キャンセル";

		// サイズの指定
		$( '#confirmDialogDiv' ).dialog( { width: 450, height: dialogHeight } );

		confirmDialogPattern = "submit_exit";

	    // 確認ダイアログ　オープン
	    $(confirmDialogObj).dialog('open');
		<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>
	}
	</c:if>

	<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
	if (submitSweepFlg == true) {
		submitSweep();
	}
	<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2015/12/22 QQ)Nishiyama 大規模改修 DEL START --%>
//	document.forms[0].forward.value = "sweeper";
//	document.forms[0].backward.value = "<c:out value="${ param.forward }" />";
//	document.forms[0].target = "_self";
//	document.forms[0].submit();
//	setTimeout("_submitExit()", 1000 * 4);
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 DEL END   --%>

}

<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
function submitSweep() {
<%-- 2019/08/16 QQ)Nishiyama 共通テスト対応 UPD START --%>
//	document.forms[0].forward.value = "sweeper";
//	document.forms[0].backward.value = "<c:out value="${ param.forward }" />";
//	document.forms[0].target = "_self";
//	document.forms[0].submit();
//	setTimeout("_submitExit()", 1000 * 4);
	_submitExit();
<%-- 2019/08/16 QQ)Nishiyama 共通テスト対応 UPD END   --%>
}
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>

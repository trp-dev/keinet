<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

// 共通項目設定
function openCommon(mode) {
	var url = "";
	switch (mode) {
		case 0:
			url = "<c:url value="CommonTop" />?forward=cm001";
			break;
		case 1:
			url = "<c:url value="CommonType" />?forward=cm101";
			break;
		case 2:
			url = "<c:url value="CommonCourse" />?forward=cm201";
			break;
		case 3:
			url = "<c:url value="CommonCompYear" />?forward=cm301";
			break;
		case 4:
			url = "<c:url value="CommonUnivSelect" />?forward=cm401";
			break;
		case 5:
			url = "<c:url value="CommonCompClass" />?forward=cm501";
			break;
		case 6:
			url = "<c:url value="CommonCompSchoolSelect" />?forward=cm601";
			break;
		default:
			alert("不正なモード指定です");
			break;
	}

	// 対象年度
	{
		var e = document.forms[0].targetYear;
		if (e && e.options) {
			if (e.selectedIndex >= 0) {
				url += "&targetYear=" + e.options[e.selectedIndex].value;
			} else {
				alert("模試データが無いため設定できません。");
				return;
			}
		} else if (e) {
			if (e.value == null || e.value == "") {
				alert("模試データが無いため設定できません。");
				return;
			} else {
				url += "&targetYear=" + e.value;
			}
		}
	}

	// 対象模試
	{
		var e = document.forms[0].targetExam;
		if (e && e.options) {
			url += "&targetExam=" + e.options[e.selectedIndex].value;
		} else if (e) {
			url += "&targetExam=" + e.value;
		}
	}

	<c:if test="${ not empty commonCheck and commonCheck == 0 }">
		// チェックは無効にしておく
		url += "&check=0";
	</c:if>

	// 別ウィンドウ
	<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD START --%>
	<%-- window.open(url + "&backward=<c:out value="${ param.forward }" />", "CommonSetting", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920,left=50") --%>
	window.open(url + "&backward=<c:out value="${ param.forward }" />", "CommonSetting", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=920,height=870,left=50")
	<%-- 2016/02/04 QQ)Hisakawa 大規模改修 UPD END   --%>
}

function submitClosed() {
	if (window.clearDisabled) clearDisabled();
	if (document.forms[0].scrollX) document.forms[0].scrollX.value = document.body.scrollLeft;
	if (document.forms[0].scrollY) document.forms[0].scrollY.value = document.body.scrollTop;
	document.forms[0].forward.value = "<c:out value="${param.forward}" />";
	document.forms[0].backward.value = "<c:out value="${param.forward}" />";
	document.forms[0].target = "_self";
	document.forms[0].submit();
}


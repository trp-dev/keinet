<%@ page pageEncoding="MS932" %>

// 印刷確認ダイアログ　クローズ処理
function printDialogClose(val) {

  var form = document.forms[0];

  // ダイアログクローズ
  $(printDialogObj).dialog('close');

  // 印刷押下
  if (val == 0) {
    var chkRidio = $('input[name="mode"]:checked').val();

	// 印刷する
	if (chkRidio == 2) {

		if (printDialogPattern == "submit_max") {
			form.printFlag.value = 2;
		} else if (printDialogPattern == "submit_top") {
			form.printFlag.value = 3;
		// 個別
		} else if (printDialogPattern == "printDialog1") {
			document.forms[0].printStatus.value = _isBundle ? "3" : "2";
			printSheet();
			return;
		// 一括
		} else if (printDialogPattern == "printDialog2") {
			document.forms[0].printStatus.value = "3";
			printSheet();
			return;
		} else if (printDialogPattern == "u001") {
			document.forms[0].printFlag.value = 2;

		} else if (printDialogPattern == "s001") {
			document.forms[0].printFlag.value = 2;
		}


	// 印刷して保存
	} else {

		if (printDialogPattern == "submit_max") {
			form.printFlag.value = 8;
		} else if (printDialogPattern == "submit_top") {
			form.printFlag.value = 4;
		// 個別
		} else if (printDialogPattern == "printDialog1") {
			document.forms[0].printStatus.value = _isBundle ? "4" : "8";
			printSheet();
			return;
		// 一括
		} else if (printDialogPattern == "printDialog2") {
			document.forms[0].printStatus.value = "4";
			printSheet();
			return;
		} else if (printDialogPattern == "u001") {
			document.forms[0].printFlag.value = 8;

		} else if (printDialogPattern == "s001") {
			document.forms[0].printFlag.value = 8;
		}
	}

	printDialogCloseAfterProc();
  }
}

function printDialogCloseAfterProc() {

	var form = document.forms[0];

	if (printDialogPattern == "submit_max") {

		// Disabledを解除する
		clearDisabled();

		// 出力先ウィンドウを開く
		form.forward.value = "sheet";
		form.backward.value = "<c:out value="${param.forward}" />";
		form.save.value = "1";
//		form.submit();

	    // HTMLでの送信をキャンセル
//	    event.preventDefault();

	    // 操作対象のフォーム要素を取得
	    var $form = $(form);

	    // 送信ボタンを取得
	    var $button = $form.find('button');

	    // 送信
	    $.ajax({
	        url: $form.attr('action'),
	        type: $form.attr('method'),
	        data: $form.serialize(),
	        // 送信前
	        beforeSend: function(xhr, settings) {
	            // ボタンを無効化し、二重送信を防止
	            $button.attr('disabled', true);
	        },
	        // 応答後
	        complete: function(xhr, textStatus) {
	            // ボタンを有効化し、再送信を許可
	            $button.attr('disabled', false);
	            $.unblockUI();
	        },

	        // 通信成功時の処理
	        success: function(result, textStatus, xhr) {
	            // 入力値を初期化
//	            $form[0].reset();

             	location.href = "<c:url value="DownloadServlet" />";
	        },

	        // 通信失敗時の処理
	        error: function(xhr, textStatus, error) {
	            alert('NG...');
	        }
	    });
		// フラグを戻す
		document.forms[0].save.value = "0";

		// Disabledを復元する
		restoreDisabled();

	} else if (printDialogPattern == "submit_top") {

		// Disabledを解除する
		clearDisabled();

		// 出力先ウィンドウを開く
		form.forward.value = "sheet";
		form.backward.value = "<c:out value="${param.forward}" />";
		form.changed.value = "1";
		form.submit();

		// Disabledを復元する
		restoreDisabled();

	} else if (printDialogPattern == "u001") {

		document.forms[0].forward.value = "sheet";
		document.forms[0].submit();

	} else if (printDialogPattern == "s001") {

        document.forms[0].forward.value = "sheet";

        <c:choose>
            <c:when test="${MenuSecurity.menu['200']}">
                document.forms[0].backward.value = "b000";
            </c:when>
            <c:otherwise>
                document.forms[0].backward.value = "s000";
            </c:otherwise>
        </c:choose>

        document.forms[0].submit();
	}

}
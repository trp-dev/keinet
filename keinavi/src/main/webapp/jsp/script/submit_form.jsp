<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

	var c = new Array();

	// 操作不能を解除する
	function clearDisabled(e) {
		<%-- 引数がなければここまで --%>
		if (e == undefined) {
			return;
		}
		if (e.length) {
			for (var i=0; i<e.length; i++) {
				clearElementDisabled(e[i]);
			}
		} else {
			clearElementDisabled(e);
		}
	}

	function clearElementDisabled(e) {
		if (e.disabled) c[c.length] = e;
		e.disabled = false;
	}

	// 操作不能を復元する
	function restoreDisabled() {
		for (var i=0; i<c.length; i++) {
			c[i].disabled = "disabled";
		}
		c.length = 0;
	}

	<%--
		汎用的なフォームサブミット関数
			forward ... 転送先画面ID
			actionMode（デフォルト） ... アクションモード
			scrollFlag（デフォルト：false） ... 画面のスクロールを保持するかどうか
			target（デフォルト： "_selft"） ... サブミットターゲット
			form（デフォルト：document.forms[0]） ... フォームオブジェクト
	--%>
	function submitForm(forward, actionMode, scrollFlag, target, form) {

		if (form == undefined) {
			form = document.forms[0];
		}

		if (scrollFlag == undefined) {
			if (form.scrollX != undefined) form.scrollX.value = "0";
			if (form.scrollY != undefined) form.scrollY.value = "0";
		} else {
			if (form.scrollX != undefined) form.scrollX.value = document.body.scrollLeft;
			if (form.scrollY != undefined) form.scrollY.value = document.body.scrollTop;
		}

		if (form.actionMode != undefined) {
			form.actionMode.value = actionMode;
		}

		if (target == undefined) {
			form.target = "_self";
		} else {
			form.target = target;
		}

		if (form.backward.value == "") {
			form.backward.value = "<c:out value="${ form.forward }" />";
		}

		form.forward.value = forward;
		clearDisabled(form.elements);
		form.submit();
		restoreDisabled();
	}

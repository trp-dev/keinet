<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END   --%>

	// Disabledを解除した要素の入れ物
	var c = new Array();

// 2019/07/04 QQ)Hisakawa 共通テスト対応 ADD START
	/**
	*
	* ダウンロード前チェック(オーバーライド)
	*/
	function validateDownload() {

		var form = document.forms[0];
		// 入力チェック
		if (!validate(1)) return false;

		// 対象模試チェック
		if (form.targetYear.selectedIndex < 0) {
			alert("模試データが無いため出力できません。");
			return false;
		}

		<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD START --%>
		printDialogPattern = "submit_top";
		<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD END   --%>

		return true;
	}
// 2019/07/04 QQ)Hisakawa 共通テスト対応 ADD END

	//「保存」「印刷」ボタン
	function submitSheet(value) {
		var form = document.forms[0];

// 2019/07/04 QQ)Hisakawa 共通テスト対応 DEL START
//		// 入力チェック
//		if (!validate()) return;
//
//		// 対象模試チェック
//		if (form.targetYear.selectedIndex < 0) {
//			alert("模試データが無いため一括出力は行えません。");
//			return;
//		}
//
//		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
//		printDialogPattern = "submit_top";
//		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
// 2019/07/04 QQ)Hisakawa 共通テスト対応 DEL END

		// 確認ダイアログ（印刷ボタン押下時）
		if (value == 0) {
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD START --%>
//			returnValue = showModalDialog(
//					"<c:url value="PrintDialog" />",
//					null,
//					"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

//			switch (returnValue) {
//				case 1: form.printFlag.value = 4; break;
//				case 2: form.printFlag.value = 3; break;
//				default: return;
//			}

		    // 印刷確認ダイアログ　オープン
		    $(printDialogObj).dialog('open');
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD END   --%>

		} else {
			document.forms[0].printFlag.value = value;

			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
			printDialogCloseAfterProc();
			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
		}

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 DEL START --%>
		// Disabledを解除する
//		clearDisabled();

		// 出力先ウィンドウを開く
//		form.forward.value = "sheet";
//		form.backward.value = "<c:out value="${param.forward}" />";
//		form.changed.value = "1";
//		form.submit();

		// Disabledを復元する
//		restoreDisabled();
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 DEL END   --%>
	}

	// 操作不能を解除する
	function clearDisabled() {
		var e = document.forms[0].elements;
		for (var i=0; i<e.length; i++) {
			if (e[i].disabled) c[c.length] = e[i];
			e[i].disabled = false;
		}
	}

	// 解除したDisabledを復活させる
	function restoreDisabled() {
		for (var i=0; i<c.length; i++) c[i].disabled = "disabled";
		c.length = 0;
	}

	function submitBack() {
		// 戻り先は専用トップページで固定
		submitMenu('w008');
	}

	// 模試を変更したらリロード
	function changeExamSelect() {
		clearDisabled();
		document.forms[0].forward.value = "<c:out value="${param.forward}" />";
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].scrollX.value = document.body.scrollLeft;
		document.forms[0].scrollY.value = document.body.scrollTop;
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

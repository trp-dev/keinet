<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa ��K�͉��C UPD END --%>

<%-- �R���X�g���N�^ --%>
function GraphChecker() {
	this.commonTypeError = false;
	this.commonCourseError = false;
	this.commonClassError = false;
	this.commonSchoolError = false;
	this.indTypeErrorName = new Array();
	this.indCourseErrorName = new Array();
}

<%-- �G���[���b�Z�[�W���� --%>
function _createMessage() {

	var message = "<kn:message id="cm015a" />";
	message += "------------------------------\n";

	<%-- ���ʍ��ڐݒ� - �^ --%>
	if (this.commonTypeError) {
		message += "���^\n";
	}

	<%-- ���ʍ��ڐݒ� - �Ȗ� --%>
	if (this.commonCourseError) {
		message += "���Ȗ�\n";
	}

	<%-- ���ʍ��ڐݒ� - ��r�ΏۃN���X --%>
	if (this.commonClassError) {
		message += "����r�ΏۃN���X\n";
	}

	<%-- ���ʍ��ڐݒ� - ��r�Ώۍ��Z --%>
	if (this.commonSchoolError) {
		message += "����r�Ώۍ��Z\n";
	}

	<%-- �ʐݒ� - �^ --%>
	for (var i = 0; i < this.indTypeErrorName.length; i++) {
		message += "���^�i�ʐݒ�j" + this.indTypeErrorName[i] + "\n";
	}

	<%-- �ʐݒ� - �Ȗ� --%>
	for (var i = 0; i < this.indCourseErrorName.length; i++) {
		message += "���Ȗځi�ʐݒ�j" + this.indCourseErrorName[i] + "\n";
	}

	message += "------------------------------\n";

	return message;
}

function _hasError() {
	return this.commonTypeError || this.indTypeErrorName.length > 0
			|| this.commonCourseError || this.indCourseErrorName.length > 0
			|| this.commonClassError || this.commonSchoolError;
}

function _checkSubject(typeFlag, courseFlag, name) {
	if (typeFlag > 0 && courseFlag > 0) {
		this.checkType(typeFlag, name);
		this.checkCourse(courseFlag, name);
	}
}

function _checkType(flag, name) {
	if (flag == 1) {
		this.commonTypeError = true;
	} else if (flag == 2) {
		this.indTypeErrorName[this.indTypeErrorName.length] = name;
	}
}

function _checkCourse(flag, name) {
	if (flag == 1) {
		this.commonCourseError = true;
	} else if (flag == 2) {
		this.indCourseErrorName[this.indCourseErrorName.length] = name;
	}
}

function _checkClass(flag) {
	if (flag > 0) {
		this.commonClassError = true;
	}
}

function _checkSchool(flag) {
	if (flag > 0) {
		this.commonSchoolError = true;
	}
}

function _checkSubjectDetail(isCommonType, isCommonCourse, typeFlag, courseFlag, countType, countCourse) {
	var typeError = isCommonType ? typeFlag == 1 : countType == 0;
	var courseError = isCommonCourse ? courseFlag == 1 : countCourse == 0;
	if (typeError && courseError) {
		this.checkTypeDetail(isCommonType, typeFlag, countType);
		this.checkCourseDetail(isCommonCourse, courseFlag, countCourse);
	}
}

function _checkTypeDetail(isCommon, flag, count) {
	if (isCommon) {
		if (flag == 1) {
			this.commonTypeError = true;
		}
	} else {
		if (count == 0) {
			this.indTypeErrorName[this.indTypeErrorName.length] = "";
		}
	}
}

function _checkCourseDetail(isCommon, flag, count) {
	if (isCommon) {
		if (flag == 1) {
			this.commonCourseError = true;
		}
	} else {
		if (count == 0) {
			this.indCourseErrorName[this.indCourseErrorName.length] = "";
		}
	}
}

<%-- �v���g�^�C�v��` --%>
GraphChecker.prototype.createMessage = _createMessage;
GraphChecker.prototype.hasError = _hasError;
GraphChecker.prototype.checkSubject = _checkSubject;
GraphChecker.prototype.checkType = _checkType;
GraphChecker.prototype.checkCourse = _checkCourse;
GraphChecker.prototype.checkClass = _checkClass;
GraphChecker.prototype.checkSchool = _checkSchool;
GraphChecker.prototype.checkSubjectDetail = _checkSubjectDetail;
GraphChecker.prototype.checkTypeDetail = _checkTypeDetail;
GraphChecker.prototype.checkCourseDetail = _checkCourseDetail;

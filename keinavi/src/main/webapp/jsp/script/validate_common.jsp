<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

// 共通項目設定のチェック
function validateCommon() {
	var form = document.forms[0];
	var message = "";

	<c:forTokens var="screen" items="c002,c003,c004,c005,c006" delims=",">
		<c:if test="${ param.forward == screen }">
			// 担当クラス
			<c:if test="${ ChargeClass != null && empty ChargeClass }">
				message += "<kn:message id="c001a" />\n";
			</c:if>
		</c:if>
	</c:forTokens>

	// 過回模試のチェック
	if (form.pastExam1 && form.pastExam2) {
		if (form.pastExam1.options.length > 1 && form.pastExam2.options.length > 1) {
			if (form.pastExam1.selectedIndex == form.pastExam2.selectedIndex) {
				message += "<kn:message id="c003a" />\n";
			}
		}
	}

	<%-- 型と科目がある画面 --%>
	<c:forTokens var="screen" items="s003,s103,s202,s203,s302,s303,s402,s403,b002,b003,b102,b103,c002,c102,c103" delims=",">
		<c:if test="${ param.forward == screen }">
			var typeCount = <c:out value="${ SubjectCountBean.typeCount }" />;
			if (("<kn:com item="type" />" == "設定なし" || typeCount == 0)
				&& form.commonType[0].checked
				&& "<kn:com item="course" />" == "設定なし"
				&& form.commonCourse[0].checked) {
					message += "<kn:message id="s012a" />\n";
			}
		</c:if>
	</c:forTokens>

	<%-- 科目だけの画面 --%>
	<c:forTokens var="screen" items="s004,s204,s304,b004,c005,c104" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="course" />" == "設定なし" && form.commonCourse[0].checked) {
				message += "<kn:message id="s003a" />\n";
			}
		</c:if>
	</c:forTokens>

	<%-- c004は特殊 --%>
	<c:if test="${ param.forward == 'c004' }">
		var typeCount = <c:out value="${ SubjectCountBean.typeCount }" />;
		if (("<kn:com item="type" />" == "設定なし" || typeCount == 0)
			&& form.commonType[0].checked
			&& form.optionItem.checked
			&& "<kn:com item="course" />" == "設定なし"
			&& form.commonCourse[0].checked
			&& form.optionItem.checked) {
				message += "<kn:message id="s012a" />\n";
		}
	</c:if>

	<c:forTokens var="screen" items="s102,s103,s104" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="year" />" == "設定なし") {
				message += "<kn:message id="s004a" />\n";
			}
		</c:if>
	</c:forTokens>

	<c:if test="${ param.forward == 's402' }">
		if ("<kn:com item="year" />" == "設定なし"
			&& form.optionItem[0].checked) {
				message += "<kn:message id="s004a" />\n";
		}
	</c:if>

	<c:forTokens var="screen" items="s104,s205,s305,b005,c006,c105" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="univ" />" == "設定なし") {
				message += "<kn:message id="s005a" />\n";
			}
		</c:if>
	</c:forTokens>

	<c:forTokens var="screen" items="s202,s203,s204,s205,c102,c103,c104,c105" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="class" />" == "設定なし") {
				message += "<kn:message id="s006a" />\n";
			}
		</c:if>
	</c:forTokens>

	<c:if test="${ param.forward == 's402' }">
		if ("<kn:com item="class" />" == "設定なし"
			&& form.optionItem[1].checked) {
				message += "<kn:message id="s006a" />\n";
		}
	</c:if>

	<c:forTokens var="screen" items="s302,s303,s304,s305,b002,b003,b004,b005,b102" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="school" />" == "設定なし") {
				message += "<kn:message id="s007a" />\n";
			}
		</c:if>
	</c:forTokens>

	<%-- センタリサーチ以外で他校比較オプションが有効なら設定が必要 --%>
	<c:if test="${ not isCenterResearch }">
	<c:if test="${ param.forward == 's402' }">
	<c:if test="${MenuSecurity.menu['606']}">
		if ("<kn:com item="school" />" == "設定なし"
			&& form.optionItem[2].checked) {
				message += "<kn:message id="s007a" />\n";
		}
	</c:if>
	</c:if>
	</c:if>

	<c:forTokens var="screen" items="t002,t003,t004,t005" delims=",">
		<c:if test="${ param.forward == screen }">
			if ("<kn:com item="school" />" == "設定なし") {
				message += "<kn:message id="s007a" />\n";
			}
		</c:if>
	</c:forTokens>

	return message;
}


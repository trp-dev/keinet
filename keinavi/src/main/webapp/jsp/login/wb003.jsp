<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／表示メニュー選択</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    
	<%-- 終了ボタン用 --%>
	<%@ include file="/jsp/sales/common/submit_exit.jsp" %>

	function init() {
		startTimer();
		var message = "<c:out value="${ErrorMessage}" />";
		if (message != "") alert(message);
	}

	function validate() {
		// 入力チェック用オブジェクト
		var val = new Validator();
		if (document.forms[0].elements["menu"][1].checked) {
			if (!val.isAlphanumeric(document.forms[0].schoolCD.value, 5)) {
				alert("高校コードは半角英数字5桁で入力してください。");
				return false;
			}
		}
		return true;
	}

	function proceed() {
		if (validate()) {
			var menu = $('input[name=menu]:checked').val();
			if (menu == '3') {
				document.forms[0].forward.value = "sd101";
				document.forms[0].salesCode.value = document.forms[0].sectorCD.options[document.forms[0].sectorCD.selectedIndex].value;
			} else if (menu == '4') {
				document.forms[0].forward.value = "sd201";
			} else if (menu == '5') {
				document.forms[0].forward.value = "sd203";
			} else if (menu == '6') {
				document.forms[0].forward.value = "sd204";
			} else if (menu == '7') {
				document.forms[0].forward.value = "sd205";
			} else if (menu == '8') {
				document.forms[0].forward.value = "sd206";
			} else if (menu == '9') {
				document.forms[0].forward.value = "sd207";
			} else {
				document.forms[0].forward.value = "w002";
			}
			document.forms[0].submit();
		}
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="SalesSelect" />" method="POST" id="main-form">
<input type="hidden" name="forward" value="w002">
<input type="hidden" name="backward" value="wb003">
<input type="hidden" name="salesCD" value="<c:out value="${param.salesCD}" />">
<input type="hidden" name="salesCode" value="<c:out value="${param.salesCD}" />">
<!--HEADER-->
<a name="top"></a>
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr>
<td width="17" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="195" rowspan="2"><img src="./shared_lib/include/header/img/header_logo.gif" width="195" height="52" border="0" alt="Kei-Navi　(Kei-Net Step Up Navigator)"><br></td>
<td width="580" align="center" rowspan="2"><b class="text12" style="color:#626261"></b></td>
<td width="195" align="right"><div style="margin-top:8px;"><a href="javascript:submitExit()"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></div></td>
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td align="right"><span class="text10">※終了時は、ブラウザの「×」ではなく、<br>[終了]ボタンをクリックしてください。</span></td>
</tr>
</table>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_00.html"-->
<!--/ヘルプナビ-->


<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td><BR><BR><b class="text16">表示するメニューを選択してください</b></td>
	</tr>
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td><BR><BR><BR></td>
	</tr>
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td>
<!--テキスト出力-->
<table border="0" cellpadding="0" cellspacing="0" width="504">
	<tr valign="top">
		<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="504">
				<tr valign="top">
					<td width="502" bgcolor="#FFFFFF" align="center">
						<!--下の段-->
						<div style="margin-top:3px;">
							<table border="0" cellpadding="2" cellspacing="0" width="490">
								<tr>
									<td align="center">
										<table border="0"  cellpadding="2" cellspacing="0">
											<tr><td><BR><BR></td></tr>
											<tr><td>営業部選択：<select name="sectorCD" class="text12">
												<c:forEach var="sector" items="${SectorSession.sectorList}">
													<c:if test="${ sector.hsBusiDivCD == '1' }">
														<option value="<c:out value="${sector.sectorCD}" />"<c:if test="${ sector.sectorCD == param.sectorCD }"> selected</c:if>><c:out value="${sector.sectorName}" /></option>
													</c:if>
												</c:forEach>
												</select>
											</td></tr>
											<tr><td><BR></td></tr>

											<%-- メニューの初期値 --%>
											<c:set var="menuValue" value="1" />
											<c:if test="${not empty param.menu}">
												<c:set var="menuValue" value="${param.menu}" />
											</c:if>
											<tr><td><INPUT type="radio" name="menu" value="1"<c:if test="${ menuValue == '1' }"> checked</c:if>>営業部メニュー</td></tr>
											<tr><td><INPUT type="radio" name="menu" value="2"<c:if test="${ menuValue == '2' }"> checked</c:if>>高校代行メニュー</td></tr>

											<tr><td>　　　　　高校コード：<INPUT type="text" name="schoolCD" value="<c:out value="${param.schoolCD}" />" size="7" maxlength="5"></td></tr>

											<%-- 高校作成ファイルダウンロード --%>
											<tr><td><BR></td></tr>
											<tr><td><INPUT type="radio" name="menu" value="3"<c:if test="${ menuValue == '3' }"> checked</c:if>>高校作成ファイルダウンロード</td></tr>

											<%-- 特例成績データ作成承認システム --%>
											<tr><td><BR></td></tr>
											<c:if test="${MENU_APPLY}">
												<tr><td><INPUT type="radio" name="menu" value="4"<c:if test="${ menuValue == '4' }"> checked</c:if>>特例成績データ作成申請</td></tr>
											</c:if>
											<c:if test="${MENU_ACCEPT_EIGYO}">
												<tr><td><INPUT type="radio" name="menu" value="5"<c:if test="${ menuValue == '5' }"> checked</c:if>>特例成績データ作成承認(営業部)</td></tr>
											</c:if>
											<c:if test="${MENU_ACCEPT_KIKAKU}">
												<tr><td><INPUT type="radio" name="menu" value="6"<c:if test="${ menuValue == '6' }"> checked</c:if>>特例成績データ作成承認(高校事業企画部)</td></tr>
											</c:if>
											<c:if test="${MENU_ACCEPT_KANRI}">
												<tr><td><INPUT type="radio" name="menu" value="7"<c:if test="${ menuValue == '7' }"> checked</c:if>>特例成績データ作成承認(模試運用管理部)</td></tr>
												<tr><td><INPUT type="radio" name="menu" value="8"<c:if test="${ menuValue == '8' }"> checked</c:if>>特例成績データ申請一覧確認</td></tr>
											</c:if>
											<c:if test="${MENU_DOWNLOAD}">
												<tr><td><INPUT type="radio" name="menu" value="9"<c:if test="${ menuValue == '9' }"> checked</c:if>>特例成績データダウンロード</td></tr>
											</c:if>

										</table>
									</td>
								</tr>
								<tr>
									<td align="center">
										<BR><BR>
										<!--ボタン-->
										<table border="0" cellpadding="0" cellspacing="0" width="150">
											<tr valign="top">
												<td bgcolor="#8CA9BB">
													<table border="0" cellpadding="7" cellspacing="1" width="150">
														<tr valign="top">
															<td width="680" bgcolor="#FBD49F" align="center">
																<input type="button" value="&nbsp;決定&nbsp;" class="text12" style="width:120px;" onClick="proceed();">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!--/ボタン-->
										<BR>
									</td>
								</tr>
							</table>
						</div>
						<!--/下の段-->

					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br><br>
<!--/テキスト出力-->
		</td>
	</tr>

</table>
</div>
<!--/タイトル-->


<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer01.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Kei-Navi／ログイン</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript">
<!--

    function init() {
        var message = "<c:out value="${ErrorMessage}" />";
        if (message != "") alert(message);
    }

    function validate() {
        var msg = "";

        <%-- 学校／利用者ＩＤのチェック --%>
        if (document.forms[0].account.value == "") {
            msg += "<kn:message id="w026a" />\n";
        }
        if (document.forms[0].user.value == "") {
            msg += "<kn:message id="w027a" />\n";
        }

        if (msg != "") {
            alert(msg);
            return false;
        }

        document.forms[0].submit();
    }


// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<div style="position:absolute;top:0;right:0;left:0;bottom:0;margin:auto;width:697px;height:370px;">

<form action="AssetLogin" method="POST">
<input type="hidden" name="forward" value="as001">
<input type="hidden" name="backward" value="as001">
<input type="hidden" name="next" value="<c:out value="${param.next}" />">

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="697" align="center">
  <tr valign="top">
    <td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
    <td width="12"><img src="./shared_lib/img/parts/com_cnr_lt_blue.gif" width="12" height="11" border="0" alt=""><br></td>
    <td width="660" background="./shared_lib/img/parts/com_bk_t_blue.gif"><img src="./shared_lib/img/parts/sp.gif" width="660" height="11" border="0" alt=""><br></td>
    <td width="13"><img src="./shared_lib/img/parts/com_cnr_rt_blue.gif" width="13" height="11" border="0" alt=""><br></td>
  </tr>
</table>
<!--/上部　オレンジライン-->

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="697" align="center">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="310" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="668" bgcolor="#FFFFFF" align="center">

<!--ログインボックス-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="1">
<tr valign="top">
<td align="center"><p style="font-size:18px;font-weight:bold;margin-bottom:12px;">学校／利用者ＩＤとパスワードを入力し、ログインしてください。</p></td>
</tr>
<tr valign="top">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="433">
  <tr valign="top">
    <td width="2"><img src="./shared_lib/img/parts/tbl_lt_gray.gif" width="2" height="2" border="0" alt=""><br></td>
    <td width="525" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
    <td width="2"><img src="./shared_lib/img/parts/tbl_rt_gray.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
  <tr bgcolor="#E5EEF3">
    <td width="2" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
    <td width="525" align="center">
      <!--ＩＤ＆パスワード-->
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><span class="text12">学校ＩＤ</span></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><span class="text12">利用者ＩＤ</span></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><span class="text12">パスワード</span></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
        </tr>
        <tr>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><input type="text" size="20" class="text12" name="account" style="width:135px;border:2px solid #3F48CC;" value="<c:out value="${param.account}" />"></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><input type="text" size="20" class="text12" name="user" style="width:135px;border:2px solid #3F48CC;" value="<c:out value="${param.user}" />"></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
          <td><input type="password" size="20" class="text12" name="password" style="width:135px;border:2px solid #3F48CC;"></td>
          <td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
        </tr>
      </table>
      <!--/ＩＤ＆パスワード-->
    </td>
    <td width="2" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
  <tr>
    <td width="2" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
    <td width="525" align="center" bgcolor="#FFFFFF"><!--ログインボタン--><input type="button" value="&nbsp;ログイン&nbsp;" class="text12" style="width:140px;" onClick="return validate()"></td>
    <td width="2" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
  </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
  <tr valign="top">
    <td width="2"><img src="./shared_lib/img/parts/tbl_lb_gray.gif" width="2" height="2" border="0" alt=""><br></td>
    <td width="525" bgcolor="#E5EEF3"><img src="./shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
    <td width="2"><img src="./shared_lib/img/parts/tbl_rb_gray.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ログインボックス-->

<!--お問い合わせ-->
<div style="margin-top:18px;">
  <table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td valign="top" class="text14">・</td>
      <td class="text14">
        学校ID,利用者ID,パスワードにつきましては、お届けしている<br>
        「Kei-Naviご利用登録通知書」をご参照ください。<br>
        ご不明な場合は<a href="https://www.kawai-juku.ac.jp/highschool/zento/inquiry/" target="_blank">担当営業部</a>までお問い合わせください。
      </td>
    </tr>
    <tr>
      <td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""></td>
    </tr>
    <tr>
      <td valign="top" class="text14">・</td>
      <td class="text14">
        ログインできない場合は<a href="/public/guide/faqindex.html" target="_blank">「よくあるご質問（FAQ）」</a>をご参照く<br>
        ださるか、「Kei-Naviヘルプデスク」にお問い合わせください。
      </td>
    </tr>
  </table>
</div>
<!--/お問い合わせ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
  </tr>
</table>
<!--/spacer-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="697" align="center">
  <tr valign="top">
    <td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
    <td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
    <td width="660" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="660" height="15" border="0" alt=""><br></td>
    <td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
  </tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<div style="margin-top:7px;text-align:center;">
  <img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network">
</div>
<!--/FOOTER-->

</form>

</div>

</body>
</html>

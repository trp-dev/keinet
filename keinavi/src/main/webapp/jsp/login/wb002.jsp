<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／表示メニュー選択</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    
	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}
	
	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="StaffSelect" />" method="POST">
<input type="hidden" name="forward" value="wb003">
<input type="hidden" name="backward" value="wb002">
<input type="hidden" name="account" value="<c:out value="${param.account}" />">
<!--HEADER-->
<a name="top"></a>
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr>
<td width="17" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="195" rowspan="2"><img src="./shared_lib/include/header/img/header_logo.gif" width="195" height="52" border="0" alt="Kei-Navi　(Kei-Net Step Up Navigator)"><br></td>
<td width="580" align="center" rowspan="2"><b class="text12" style="color:#626261"></b></td>
<td width="195" align="right"><div style="margin-top:8px;"><a href="javascript:submitExit()"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></div></td>
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td align="right"><span class="text10">※終了時は、ブラウザの「×」ではなく、<br>[終了]ボタンをクリックしてください。</span></td>
</tr>
</table>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_00.html"-->
<!--/ヘルプナビ-->


<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td><BR><BR><b class="text16">表示するメニューを選択してください</b></td>
	</tr>
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td><BR><BR><BR></td>
	</tr>
	<tr valign="top" align="center">
		<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
		<td>
<!--テキスト出力-->
<table border="0" cellpadding="0" cellspacing="0" width="504">
	<tr valign="top">
		<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="0" cellspacing="1" width="504">
				<tr valign="top">
					<td width="502" bgcolor="#FFFFFF" align="center">
						<!--下の段-->
						<div style="margin-top:3px;">
							<table border="0" cellpadding="2" cellspacing="0" width="490">
								<tr>
									<td align="center">
										<table border="0"  cellpadding="2" cellspacing="0">
											<tr><td><BR><BR></td></tr>
											<tr><td><INPUT type="radio" name="menu" value="1" checked>営業部用</td></tr>
											<tr><td><INPUT type="radio" name="menu" value="2">校舎用</td></tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="center">
										<BR><BR>
										<!--ボタン-->
										<table border="0" cellpadding="0" cellspacing="0" width="150">
											<tr valign="top">
												<td bgcolor="#8CA9BB">
													<table border="0" cellpadding="7" cellspacing="1" width="150">
														<tr valign="top">
															<td width="680" bgcolor="#FBD49F" align="center">
																<input type="submit" value="&nbsp;決定&nbsp;" class="text12" style="width:120px;">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<!--/ボタン-->
										<BR>
									</td>
								</tr>
							</table>
						</div>
						<!--/下の段-->

					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--/テキスト出力-->
		</td>
	</tr>

</table>
</div>
<!--/タイトル-->


<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer01.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="jp.co.fj.keinavi.data.news.InformList" %>
<jsp:useBean id="InformListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ログイン</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--

    function init() {
        var message = "<c:out value="${ErrorMessage}" />";
        if (message != "") alert(message);
    }

    function validate() {
        if (document.forms[0].account.value == "") {
            alert("ＩＤを入力してください。");
            return false;
        }
        document.forms[0].target = "_self";
        document.forms[0].submit();
        //return true;
    }

    // お知らせ詳細ウィンドウ表示
    function openInfoDetail(div, Id) {
        var url = "<c:url value="NewsDetail" />";
        url += "?forward=n002";
        url += "&backward=wb001";
        url += "&infoId=" + Id;
        url += "&displayDiv=" + div;
        window.open(url, "n002", "top=50,left=150,width=680,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
    }

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="StaffLogin" method="POST">
<input type="hidden" name="forward" value="wb002">
<input type="hidden" name="backward" value="wb001">
<input type="hidden" name="infoId" value="">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF" align="center">


<!--タイトルロゴ-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><img src="./profile/img/top_logo.gif" width="467" height="96" border="0" alt="Kei-Navi (Kei-Net Step Up Navigator)"><br></td>
</tr>
</table>
</div>
<!--/タイトルロゴ-->


<!--ログインボックス-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="1">
<tr valign="top">
<td align="center"><b class="text12">ＩＤとパスワードを入力し、ログインしてください。</b></td>
</tr>
<tr valign="top">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="433">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="429" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="429" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="433">
<tr bgcolor="#F2BD74">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
<td width="429" align="center">
<!--ＩＤ＆パスワード-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">ＩＤ</span></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><span class="text12">パスワード</span></td>
</tr>
<tr>
<td><input type="text" size="20" class="text12" name="account" style="width:135px;" value="<c:out value="${param.account}" />"></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><input type="password" size="20" class="text12" name="password" style="width:135px;"></td>
</tr>
</table>
<!--/ＩＤ＆パスワード-->
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="433">
<tr>
<td width="2" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
<td width="429" align="center" bgcolor="#FFFFFF"><!--ログインボタン--><input type="button" value="&nbsp;ログイン&nbsp;" class="text12" style="width:140px;" onClick="return validate()"></td>
<td width="2" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="433">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="429" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="429" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ログインボックス-->


<!--成績分析を始めましょう！-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="859">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="859">
<tr valign="top">
<td width="857" bgcolor="#EFF2F3" align="center">

<!--タイトル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="849">
<tr valign="top">
<td bgcolor="#758A98" align="center"><img src="./profile/img/ttl_begin.gif" width="330" height="17" border="0" alt="Kei-Navi　で成績分析を始めましょう！" vspace="5"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="842">
<tr valign="bottom">
<!--左側-->
<td width="828">
                              <table border="0" cellpadding="2" cellspacing="0" width="828">
                                <tr valign="top">
<td width="828"><span class="text14">このシステムでは全統模試を利用して<br>学校、クラス、生徒の詳細な分析、データ出力などを行えます。</span></td>
</tr>
</table>

<div style="margin-top:14px;"> 
<table border="0" cellpadding="0" cellspacing="0" width="828">
  <tr valign="top"> 
    <td bgcolor="#8CA9BB"> 
      <table border="0" cellpadding="10" cellspacing="1" width="828">
        <tr valign="top"> 
          <td width="818" bgcolor="#FFFFFF"> 

            <!--お知らせ-->
            <table border="0" cellpadding="0" cellspacing="0" width="828">
              <tr valign="top"> 
                <td colspan="3"><b class="text14">システム利用者様へのお知らせ</b></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
                </td>
              </tr>
            <% 
            for (java.util.Iterator it=InformListBean.getInformList().iterator(); it.hasNext();) {
            InformList list = (InformList)it.next();
            %>
                <tr valign="top"> 
                    <td nowrap width="76"><span class="text12"><%=list.getDispDate()%></span></td>
                    <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br>
                    </td>
                    <td width="734"><span class="text12"><a href="javascript:openInfoDetail('0', '<%=list.getInfoId()%>')"><%=list.getTitle()%></a></span></td>
                </tr>
            <% } %>
            </table>
            <!--/お知らせ-->

            <!--お知らせ
            <table border="0" cellpadding="0" cellspacing="0" width="828">
              <tr valign="top"> 
                <td colspan="3"><b class="text14">システム利用者様へのお知らせ</b></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
                </td>
              </tr>
              <tr valign="top"> 
                <td nowrap width="76"><span class="text12">2004/08/01</span></td>
                <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br>
                </td>
                <td width="734"><span class="text12"><a href="#">8月13日0時〜8月15日24時の間、サービスを停止します。</a></span></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
                </td>
              </tr>
              <tr valign="top"> 
                <td nowrap width="76"><span class="text12">2004/07/25</span></td>
                <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br>
                </td>
                <td width="734"><span class="text12"><a href="#">新機能が追加されました。</a></span></td>
              </tr>
              <tr valign="top"> 
                <td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
                </td>
              </tr>
              <tr valign="top"> 
                <td nowrap width="76"><span class="text12">2004/07/17</span></td>
                <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br>
                </td>
                <td width="734"><span class="text12"><a href="#">第1回　全統記述模試の集計データをUPしました。</a></span></td>
              </tr>
            </table>
            /お知らせ-->

          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>
</td>
<!--/左側-->


</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績分析を始めましょう！-->

<!--お問い合わせ-->
<div style="margin-top:18px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td align="center"><b class="text14">「Kei-Navi」に関するお問い合わせは河合塾ヘルプデスクまで</b></td>
</tr>
<tr valign="top">
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td align="center"><span class="text14">河合塾ヘルプデスク</span></td>
</tr>
<tr valign="top">
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td align="center"><b class="text14">０１２０−０４９５８１</b></td>
</tr>
</table>
</div>
<!--/お問い合わせ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="984">
<tr valign="top">
<td width="984" align="center"><img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
</div>
<!--/FOOTER-->
</form>
</body>
</html>

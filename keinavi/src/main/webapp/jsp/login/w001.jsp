<%@page import="jp.co.fj.keinavi.servlets.login.W001Servlet"%>
<%@page import="jp.co.fj.keinavi.beans.news.BannerBean"%>
<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.news.InformList" %>
<jsp:useBean id="InformListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ログイン</title>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/js/OpenWindow.js"></script>
<script type="text/javascript">
<!--

    function init() {
        var message = "<c:out value="${ErrorMessage}" />";
        if (message != "") alert(message);
    }

    function validate() {
    var msg = "";

    <%-- 学校／利用者ＩＤのチェック --%>
    if (document.forms[0].account.value == "") {
      msg += "<kn:message id="w026a" />\n";
    }
    if (document.forms[0].user.value == "") {
      msg += "<kn:message id="w027a" />\n";
    }

    if (msg != "") {
      alert(msg);
      return false;
    }

        <c:choose>
        <c:when test="${ form.simple == 1 }">
            document.forms[0].target = "_self";
            document.forms[0].action = "DLLogin";
            document.forms[0].backward.value = "w001s";
            document.forms[0].simple.value   = "1";
            document.forms[0].submit();
        </c:when>
        <c:otherwise>
            document.forms[0].target = "_self";
            document.forms[0].action = "Login";
            document.forms[0].backward.value = "w001";
            document.forms[0].simple.value   = "0";
            document.forms[0].submit();
        </c:otherwise>
        </c:choose>
        //return true;
    }

    //2014/10 バナー広告追加対応 start
    //バナーをクリックした時のイベント
    //FWEST)LIU
    //2014/10/9
   function bannerClick(col,bannercode1,bannercode2,bannerName,url)
    {
       //alert(col);
       document.forms[0].target = "iFrame";
       document.forms[0].action = "DLLogin";
       document.forms[0].col.value = col;
       document.forms[0].bannercode1.value = bannercode1;
       document.forms[0].bannercode2.value = bannercode2;
       document.forms[0].bannerName.value = bannerName;
       document.forms[0].url.value = url;
    //    document.forms[0].backward.value = "w001s";
    //   document.forms[0].simple.value   = "1";
       document.forms[0].submit();
    }
    //2014/10 バナー広告追加対応 end

    // お知らせ詳細ウィンドウ表示
    function openInfoDetail(div, Id) {
        var url = "<c:url value="NewsDetail" />";
        url += "?forward=n002";
        url += "&backward=w001";
        url += "&infoId=" + Id;
        url += "&displayDiv=" + div;
        window.open(url, "n002", "top=50,left=150,width=680,height=600,resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
    }

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="" method="POST">
<input type="hidden" name="forward" value="w002">
<input type="hidden" name="backward" value="">
<input type="hidden" name="simple" value="">
<input type="hidden" name="infoId" value="">
<input type="hidden" name="first" value="">

<!--2014/10 バナー広告追加対応 start-->
<input type="hidden" name="col" value="-1">
<input type="hidden" name="bannercode1" value="">
<input type="hidden" name="bannercode2" value="">
<input type="hidden" name="bannerName" value="">
<input type="hidden" name="url" value="">
<!--2014/10 バナー広告追加対応 end-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_t.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_l.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF" align="center">


<!--タイトルロゴ-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<c:choose>
<c:when test="${ form.simple == '1' }">
 <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/top_DLlogo.gif" width="466" height="130" border="0" alt="Kei-Navi (Kei-Net Step Up Navigator)"><br></td>
</c:when>
<c:otherwise>
 <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/top_logo.gif" width="467" height="96" border="0" alt="Kei-Navi (Kei-Net Step Up Navigator)"><br></td>
</c:otherwise>
</c:choose>
</tr>
</table>
</div>
<!--/タイトルロゴ-->


<!--ログインボックス-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="1">
<tr valign="top">
<td align="center"><b class="text12">学校／利用者ＩＤとパスワードを入力し、ログインしてください。</b></td>
</tr>
<tr valign="top">
<td>

<table border="0" cellpadding="0" cellspacing="0" width="433">
<tr valign="top">
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_lt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<c:choose>
 <c:when test="${ form.simple == '1' }">
  <td width="525" bgcolor="#96e0a9"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
 </c:when>
 <c:otherwise>
  <td width="525" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
 </c:otherwise>
</c:choose>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_rt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
<c:choose>
 <c:when test="${ form.simple == '1' }">
  <tr bgcolor="#96e0a9">
 </c:when>
 <c:otherwise>
  <tr bgcolor="#F2BD74">
 </c:otherwise>
</c:choose>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
<td width="525" align="center">
<!--ＩＤ＆パスワード-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><span class="text12">学校ＩＤ</span></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><span class="text12">利用者ＩＤ</span></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><span class="text12">パスワード</span></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><input type="text" size="20" class="text12" name="account" style="width:135px;" value="<c:out value="${param.account}" />"></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><input type="text" size="20" class="text12" name="user" style="width:135px;" value="<c:out value="${param.user}" />"></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td><input type="password" size="20" class="text12" name="password" style="width:135px;"></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ＩＤ＆パスワード-->
</td>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="56" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
<tr>
<c:choose>
 <c:when test="${ form.simple == '1' }">
  <td width="2" bgcolor="#96e0a9"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
 </c:when>
 <c:otherwise>
  <td width="2" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
 </c:otherwise>
</c:choose>
<td width="352" align="right" bgcolor="#FFFFFF"><!--ログインボタン--><input type="button" value="&nbsp;ログイン&nbsp;" class="text12" style="width:140px;" onClick="return validate()"></td>
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="41" border="0" alt=""><br></td>

<c:choose>
 <c:when test="${ form.simple == '1' }">
  <td width="200" align="left"><font style="font-size:10px"><a href="/public/guide/faq/faq_k.html" target="_blank">ログインできないときはこちら<br>(「よくあるご質問（FAQ）」へ)</a></font></td>
 </c:when>
 <c:otherwise>
  <td width="200" align="left"><font style="font-size:10px"><a href="/public/guide/faq/faq_k.html" target="_blank">ログインできないときはこちら<br>(「よくあるご質問（FAQ）」へ)</a></font></td>
 </c:otherwise>
</c:choose>

<c:choose>
 <c:when test="${ form.simple == '1' }">
  <td width="2" bgcolor="#96e0a9"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
 </c:when>
 <c:otherwise>
  <td width="2" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="41" border="0" alt=""><br></td>
 </c:otherwise>
</c:choose>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="529">
<tr valign="top">
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_lb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<c:choose>
 <c:when test="${ form.simple == '1' }">
  <td width="525" bgcolor="#96e0a9"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
 </c:when>
 <c:otherwise>
  <td width="525" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="525" height="2" border="0" alt=""><br></td>
 </c:otherwise>
</c:choose>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_rb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ログインボックス-->

<BR>
<table border="0" cellpadding="0" cellspacing="0" width="479">
<tr valign="top"><td>
<font style="font-size:12px">
<c:choose>
 <c:when test="${ form.simple == '1' }">
  <center>※こちらは、全統模試の成績などを無料でご提供するメニューです。</center>
 </c:when>
 <c:otherwise>
  ※こちらは、有料の全統模試分析システムです。学校単位でのご契約が必要です。<BR>&nbsp;&nbsp;&nbsp;ご契約されていない場合は、「Kei-Naviダウンロードサービス」へアクセスしてください。
 </c:otherwise>
</c:choose>
</font>
</td></tr>
</table>

<!--成績分析を始めましょう！-->
<!-- 2007年度改修A項目にて追加 -->
<c:choose>
<c:when test="${ form.simple == '1' }">
<div style="margin-top:14px;">
  <table border="0" cellpadding="0" cellspacing="0" width="416">
    <tr valign="top">
    <td>
    <table border="0" cellpadding="10" cellspacing="1" width="416">
      <tr valign="top">
      <td width="414" bgcolor="#FFFFFF">

      <b class="text14">お知らせ</b>
      <iframe src="./mainte_html/NoPactNews.html" width="400" height="125" marginwidth="10"></iframe>
      </td>
      </tr>
    </table>
    </td>
    </tr>
  </table>
</div>
</c:when>
<c:otherwise>

<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="859">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="859">
<tr valign="top">
<td width="857" bgcolor="#EFF2F3" align="center">

<!--タイトル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="849">
<tr valign="top">
<td bgcolor="#758A98" align="center"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/ttl_begin.gif" width="330" height="17" border="0" alt="Kei-Navi　で成績分析を始めましょう！" vspace="5"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="842">
<tr valign="bottom">
<!--左側-->
<td width="416">
<table border="0" cellpadding="2" cellspacing="0" width="416">
<tr valign="top">
<td width="416"><span class="text14">このシステムでは全統模試を利用して<br>学校、クラス、生徒の詳細な分析、データ出力などを行えます。</span></td>
</tr>
</table>

<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="416">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="416">
<tr valign="top">
<td width="414" bgcolor="#FFFFFF">

<!--お知らせ-->
<!-- 2007年度改修A項目にて追加 -->
<b class="text14">お知らせ</b>
<iframe src="./mainte_html/PactNews.html" width="400" height="125" marginwidth="10"></iframe>

<!--/お知らせ-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

</td>
<!--/左側-->

<td width="14"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>

<!--右側-->
<td width="412">

<table border="0" cellpadding="0" cellspacing="0" width="412">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="412">
<tr valign="top">
<td width="410" bgcolor="#FFFFFF">
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="410">
<tr valign="top">
<td width="243">

<!--1段目-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="243">
<tr valign="top">
<td width="53" align="right"><!--イラスト--><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/man_face01.gif" width="35" height="41" border="0" alt="男性" vspace="4"><br></td>
<td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="175">

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="11">
<table border="0" cellpadding="0" cellspacing="0" width="11">
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="11"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_arrow_l_bw.gif" width="11" height="12" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="163">
<table border="0" cellpadding="0" cellspacing="0" width="163">
<tr valign="top">
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="147"><span class="text12">今後の指導方針を先生方に具体的に伝えたい！</span></td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/1段目-->

<!--2段目-->
<div style="margin-top:4px;">
<table border="0" cellpadding="0" cellspacing="0" width="243">
<tr valign="top">
<td width="63" align="right"><!--イラスト--><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/man_face01.gif" width="35" height="41" border="0" alt="男性" vspace="4"><br></td>
<td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="175">

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="11">
<table border="0" cellpadding="0" cellspacing="0" width="11">
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="11"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_arrow_l_bw.gif" width="11" height="12" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="163">
<table border="0" cellpadding="0" cellspacing="0" width="163">
<tr valign="top">
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="147"><span class="text12">生徒の得意、不得意を知って授業に役立てたい！</span></td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/2段目-->

<!--3段目-->
<div style="margin-top:4px;">
<table border="0" cellpadding="0" cellspacing="0" width="243">
<tr valign="top">
<td width="53" align="right"><!--イラスト--><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/man_face01.gif" width="35" height="41" border="0" alt="男性" vspace="4"><br></td>
<td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="175">

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rt_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="11">
<table border="0" cellpadding="0" cellspacing="0" width="11">
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="11"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_arrow_l_bw.gif" width="11" height="12" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="163">
<table border="0" cellpadding="0" cellspacing="0" width="163">
<tr valign="top">
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="147"><span class="text12">生徒一人一人に適切な進路指導を行いたい！</span></td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
<td width="155"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="4" border="0" alt=""><br></td>
<td width="5" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rb_bw.gif" width="5" height="5" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="155" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="155" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/3段目-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="154">
<!--右イラスト-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="134">
<tr valign="top">
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="120" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="120" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="120" bgcolor="#F9EEE5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="120" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="134">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#F9EEE5">
<table border="0" cellpadding="0" cellspacing="0" width="130">
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="110"><span class="text12">そんな時に、<br>この分析システムがお役に立ちますよ！</span></td>
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="2" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="134">
<tr valign="top">
<td width="7" rowspan="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="52" bgcolor="#F9EEE5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="52" height="5" border="0" alt=""><br></td>
<td width="15" rowspan="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_arrow_d_bp.gif" width="15" height="16" border="0" alt=""><br></td>
<td width="53" bgcolor="#F9EEE5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="53" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="52" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="52" height="2" border="0" alt=""><br></td>
<td width="53" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="53" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="52"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="52" height="9" border="0" alt=""><br></td>
<td width="53"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="53" height="9" border="0" alt=""><br></td>
</tr>
</table>
</div>
<table border="0" cellpadding="0" cellspacing="0" width="134">
<tr valign="top">
<td width="134" align="center"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/woman01.gif" width="52" height="84" border="0" alt="女性"><br></td>
</tr>
</table>
<!--/右イラスト-->
</td>
</tr>
</table>
<!--/説明-->
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
<!--/右側-->
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</c:otherwise>
</c:choose>

<!--/成績分析を始めましょう！-->

<!--お問い合わせ-->
<div style="margin-top:18px;">
<c:choose>
  <c:when test="${ form.simple != 1 }">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
    <td align="center"><b class="text14">「Kei-Navi」に関するお問い合わせはヘルプデスクまで</b></td>
    </tr>
    <!--
    <tr valign="top">
    <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
    </tr>
    <tr valign="top">
    <td align="center"><span class="text14">ヘルプデスク</span></td>
    </tr>
    <tr valign="top">
    <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
    </tr>
    <tr valign="top">
    <td align="center"><b class="text14">０１２０−０４９５８１</b></td>
    </tr>
    -->
    </table>
  </c:when>
  <c:otherwise>
    <table border="0" cellpadding="0" cellspacing="0" width="666">
    <tr valign="top">
    <td align="center"><b class="text14">「Kei-Navi」に関するお問い合わせはヘルプデスクまで</b></td>
<!--
    <td align="left"><b class="text14">現在「Kei-Naviダウンロードサービス」は一時停止しております（ログインして表示される「成績統計資料集ダウンロード」をクリックしても先に進めません）。ご迷惑をお掛けし大変申し訳ありません。<BR><BR><CENTER>11月25日頃から提供を再開する予定です。</CENTER></b></td>
-->
    </tr>
    </table>
  </c:otherwise>
</c:choose>
</div>
<!--/お問い合わせ-->


<!-- 2014/10/7 バナー広告追加対応 start -->
<c:choose>
 <c:when test="${ form.simple == '1' }">

<div style="margin-top:10px;">
 <table border="0" cellpadding="0" cellspacing="1" vspace="30">
  <tr>
  <%
      W001Servlet w001= new W001Servlet();
  try{
      BannerBean bannerArray[]=w001.readBannerInfo(request);
      int length=bannerArray.length;
      String bannerCode[][] = new String[length][2];
      for(int i=0;i<length;i++)
      {
  %>
 <% // 2014/12/24 Fw)mori Enterキー押下時の挙動不具合修正 Start %>
<!--   <td ><input type="image" height="40px" width="140px"  src=<%=bannerArray[i].getImgSrc() %>  onclick="bannerClick(<%=i %>,'<%=bannerArray[i].getBannerCode1() %>','<%=bannerArray[i].getBannerCode2()%>','<%=bannerArray[i].getBannerName() %>','<%=bannerArray[i].getUrl() %>');javascript:window.open('<%=bannerArray[i].getUrl() %>');return false;"></td>
 -->
<td >
<A HREF="#" onClick="bannerClick(<%=i %>,'<%=bannerArray[i].getBannerCode1() %>','<%=bannerArray[i].getBannerCode2()%>','<%=bannerArray[i].getBannerName() %>','<%=bannerArray[i].getUrl() %>');javascript:window.open('<%=bannerArray[i].getUrl() %>');return false;">
<img height="40px" width="140px" src="<%=bannerArray[i].getImgSrc() %>" border=0></img></A>
</td>
 <% // 2014/12/24 Fw)mori Enterキー押下時の挙動不具合修正 End %>

   <%
      }
    if(length<5)
    {
        for(int j=0;j<5-length;j++)
        {
  %>
  <td height="40px" width="140px"></td>
  <%
      }
     }
    }catch(Exception e)
      {
         System.out.println("CSVを読み込みにエラーが発生した。");
         e.printStackTrace();
     }

  %>

  </tr>
</table>
</div>
</c:when>
</c:choose>
<!-- 2014/10/7 バナー広告追加対応 end -->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="9"  background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_r.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_d.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="984">
<tr valign="top">
<td width="984" align="center"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
</div>
<!--/FOOTER-->
</form>
<iframe style="display: none" id="iFrame" name="iFrame" src="about:blank"  ></iframe>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<jsp:useBean id="hd101Form" scope="request" class="jp.co.fj.keinavi.forms.helpdesk.HD101Form" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>ファイル登録</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>

	function init() {
		startTimer();
	}

	function submitRegist() {
        // 入力ファイル
        var inFile = document.forms[0].elements["fileData"].value;
        // 必須チェック
        if (inFile.length == 0) {
            alert("入力ファイルを指定してください。");
            return ;
        }

        document.forms[0].fileName.value = inFile;
		document.forms[0].submit();
	}

//-->
</SCRIPT>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return false">
<form action="<c:url value="HelpDeskCsvRegist" />" enctype="multipart/form-data" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="hd107">
<input type="hidden" name="backward" value="hd107">
<input type="hidden" name="fileName" value="">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt="">
<b class="text16" style="color:#FFFFFF;">ファイル登録</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<table border="0" cellpadding="0" cellspacing="0" width="563">
<tr>
<td>

<!--メイン枠-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="563">
  <tr valign="top">
    <td bgcolor="#8CA9BB">
      <table border="0" cellpadding="0" cellspacing="1" width="563">
        <td bgcolor="#FFFFFF">
          <table border="0" cellpadding="0" cellspacing="0" width="561">
            <tr valign="top">
              <td width="561" align="center">

                <!--ファイル選択テーブル-->
                <div style="margin-top:10px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="543">
                    <tr valign="top">
                      <td bgcolor="#FFFFFF" width="543" align="center">
                       <input type="file" size="50" class="text12" name="fileData" style="width:500px;">
                      </td>
                    </tr>
                  </table>
                </div>
                <!--/ファイル選択テーブル-->

                <!--spacer-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr valign="top">
                    <td width="100%">
                      <img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
                    </td>
                  </tr>
                </table>
                <!--/spacer-->

              </td>
            </tr>
          </table>
        </td>
      </table>
    </td>
  </tr>
</table>
</div>
<!--/メイン枠-->

<!--キャンセル・登録ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="563">
  <tr valign="top">
    <td bgcolor="#FFFFFFF" width="563" align="center">
      <table border="0" cellpadding="0" cellspacing="0" width="563">
        <td bgcolor="#8CA9BB">
          <table border="0" cellpadding="6" cellspacing="1" width="563">
            <tr valign="top">
              <td bgcolor="#FBD49F" align="center">
                <input type="button" value="登録して閉じる" class="text12" style="width:100px;" onclick="submitRegist()">
                <input type="button" value="キャンセル"     class="text12" style="width:100px;" onclick="self.close()">
              </td>
            </tr>
          </table>
        </td>
      </table>
    </td>
  </tr>
</table>
</div>
<!--/キャンセル・登録ボタン-->

</td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="635">
<tr valign="top">
<td width="635" align="right"><img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>

<!--/FOOTER-->
</form>
</body>
</html>

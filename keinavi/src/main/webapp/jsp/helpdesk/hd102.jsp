<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START --%>
<jsp:useBean id="hd103Form" scope="request" class="jp.co.fj.keinavi.forms.helpdesk.HD103Form" />
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END   --%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプデスク−メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitBack() {
        document.forms[0].forward.value = "hd101";
        document.forms[0].submit();
    }

    function submitRegist() {

		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
        // 日付チェック
        var serverDate = "<c:out value="${serverDate}" />";
        //alert(serverDate);
        // 年月
        var md = serverDate.substring(4, 8);
        //alert(md);
        // 年次登録選択 かつ 4/1〜5/1の間
        if (getRadioVal("content") == "1" && "0401" <= md && md <= "0501") {
            alert("年次処理終了まで登録出来ません。");
            return;
        }
		// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

    	document.forms[1].forward.value = "hd103";
        document.forms[1].submit();
    }

	function init() {
		startTimer();
	}

    // 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
    // 選択されているラジオボタンの取得
    function getRadioVal(name) {
        var radioList = document.forms[1].elements[name];

        for(var i = 0; i < radioList.length; i ++){
          if(radioList[i].checked){
            var val = radioList[i].value;
            break;
          }
        }
        return val;
    }
	// 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

//-->
</SCRIPT>

</head>

<body onload="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">

<form action="<c:url value="HelpDeskRegist" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd102">
</form>

<form action="<c:url value="HelpDeskRegistEnter" />" enctype="multipart/form-data" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd102">


<!--HEADER-->
<%@ include file="/jsp/helpdesk/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="906" align="center">

<!--契約校管理（新規登録）-->
<div style="margin-top:14px;">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">ユーザ管理（一括登録）</b></td>
  --%>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">契約校マスタ更新</b></td>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<!-- 手動での登録はない
 <td width="846"><span class="text14-hh">「CSVファイルで一括登録」または「ユーザごとに手動で登録」のどちらかの方法で新規登録を行ってください。<br>
 /手動での登録はない -->
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td width="846"><span class="text14-hh">「CSVファイルで一括登録」の方法で新規登録を行ってください。<br>
  --%>
<td width="846"><span class="text14-hh">更新用ファイルの内容をもとに、契約校マスタのユーザ情報を更新します。<br>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--中タイトル-->
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 DEL START --%>
<%--
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="817">

<table border="0" cellpadding="0" cellspacing="0" width="817">
<tr valign="top">
<td colspan="4" width="817" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="817" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="804" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">CSVファイルで一括登録</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="817" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="817" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
 --%>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 DEL END   --%>
<!--/中タイトル-->
<!--CSVファイルで一括登録-->
<table border="0" cellpadding="0" cellspacing="0" width="826">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="826">
<tr valign="top">
<td width="824" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="796">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="13" cellspacing="1" width="796">
<tr valign="top">
<td width="794" bgcolor="#FFFFFF" align="center">

<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START --%>
<table border="0" cellpadding="4" cellspacing="2" width="576">
  <tr>
    <td width="20%" bgcolor="#E1E6EB">
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
          <td><b class="text12">処理の内容</b></td>
        </tr>
      </table>
    </td>

    <td width="80%"  bgcolor="#F4E5D6">
      <input type="radio" name="content" value="0" <c:if test="${ hd103Form.content == '0' }"> checked</c:if>>
        <span class="text12">ユーザ一括更新</span>
        <span class="text12">&nbsp;&nbsp;</span>
      <input type="radio" name="content" value="1" <c:if test="${ hd103Form.content == '1' }"> checked</c:if>>
        <span class="text12">契約更新ファイル登録【年次】</span>
        <span class="text12">&nbsp;&nbsp;</span>
    </td>
  </tr>
</table>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END   --%>

<table border="0" cellpadding="4" cellspacing="2" width="576">
<!-- 手動での登録はない
<tr><td>ファイルを指定してください。（一括登録／変更／削除が可能です。）</td></tr>
 /手動での登録はない -->
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<tr><td>ファイルを指定してください。（一括登録／変更が可能です。）</td></tr>
  --%>
<tr><td>※4/1〜5/1の間、「契約更新ファイル登録【年次】」は実行できません。</td></tr>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</table>
<table border="0" cellpadding="4" cellspacing="2" width="576">
<tr>
<td width="20%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td><b class="text12">登録用ファイル</b></td>
  --%>
<td><b class="text12">更新用ファイル</b></td>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
</td>
<td width="80%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><input type="file" size="50" class="text12" name="fileName" style="width:350px;"></td>
<input type="hidden" name="fileTitle" value="">
</tr>
</table>
</td>
</tr>
</table>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr>
<td width="572" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="572">
<tr valign="top">
<td width="570" bgcolor="#FBD49F" align="center">

<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<input type="button" value="登録" class="text12" style="width:180px;" onClick="javascript:submitRegist()">
 --%>
<input type="button" value="更新" class="text12" style="width:180px;" onClick="javascript:submitRegist()">
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/CSVファイルで一括登録-->

</div>
<!--/契約校管理（新規登録）-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/helpdesk/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="userDetailBean" scope="request" class="jp.co.fj.keinavi.beans.helpdesk.UserDetailBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプデスク−メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>


<SCRIPT type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>
    
	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitBack() {
        document.forms[0].forward.value = "hd101";
        document.forms[0].submit();
    }
    
    function submitForm(actionMode) {
    	document.forms[0].forward.value = "hd105";
    	document.forms[0].actionMode.value = actionMode;
    	document.forms[0].submit();
    }
   
    function initLoginId() {
    	if (confirm("<kn:message id="u013c" />")) {
    		submitForm('1');
    	}
    }
    
    function initLoginPass() {
    	if (confirm("<kn:message id="u014c" />")) {
    		submitForm('2');
    	}
    }

	function init() {
		startTimer();
	}
	
//-->
</SCRIPT>

</head>

<body onload="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="HelpDeskList" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd105">
<input type="hidden" name="userId" value="<c:out value="${userDetailBean.userId}" />">
<input type="hidden" name="actionMode" value="">


<!--HEADER-->
<%@ include file="/jsp/helpdesk/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="906" align="center">

<!--契約校管理-->
<div style="margin-top:14px;">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">ユーザ管理（詳細参照）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="846"><span class="text14-hh">以下の情報を確認してください。<br>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="4" cellspacing="2" width="612">
<!--学校コード-->
<tr>
<td width="45%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">学校コード</b></td>
</tr>
</table>
</td>
<td width="55%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.schoolCode }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/学校コード-->
<!--学校名-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">学校名</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.schoolName }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/学校名-->
<!--学校ID-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">学校ID</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.userId }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/学校ID-->
<!--利用者ID-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td width="100%"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="50%" align="left"><b class="text12">利用者ID</b></td><td width="50%" align="right"><c:if test="${not empty userDetailBean.loginId}"><button onclick="javascript:initLoginId();">初期化</button></c:if></td></tr></table></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.loginId }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/利用者ID-->
<!--初期パスワード-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">初期パスワード</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.firstPass }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/初期パスワード-->
<!--ログインパスワード-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td width="100%"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td width="50%" align="left"><b class="text12">ログインパスワード</b></td><td width="50%" align="right"><c:if test="${not empty userDetailBean.loginId}"><button onclick="javascript:initLoginPass();">初期化</button></c:if></td></tr></table></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.loginPass }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/ログインパスワード-->
<!--ユーザ区分-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">ユーザ区分</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.userKubun }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/ユーザ区分-->
<!--契約区分-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">契約区分</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.contractKubun }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/契約区分-->
<!--契約期限-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">契約期限</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.contractTerm }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/契約期限-->
<!--証明書有効期限-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">証明書有効期限</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.validityTerm }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/証明書有効期限-->
<!--発行済証明書の有効期限-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">発行済証明書の有効期限</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.finishingValidityTerm }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/発行済証明書の有効期限-->
<!--最大セション数-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">最大セション数</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.sessionNumber }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/最大セション数-->
<!--私書箱機能提供フラグ-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">私書箱機能提供</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.postFunction }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/私書箱機能提供フラグ-->
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="4" cellspacing="2" width="612">
<!--登録日-->
<tr>
<td width="45%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">登録日</b></td>
</tr>
</table>
</td>
<td width="55%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.rgdate }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/登録日-->
<!--更新日-->
<tr>
<td bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><b class="text12">更新日</b></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ userDetailBean.update }" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/更新日-->
</table>


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr>
<td width="572" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="612">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="100%">
<tr valign="top">
<td width="570" bgcolor="#FBD49F" align="center">
    <input type="button" value="戻る" class="text12" style="width:100px;" onClick="javascript:submitBack()">
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</div>
<!--/契約校管理-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/helpdesk/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

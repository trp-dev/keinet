<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<jsp:useBean id="UserListBean" scope="request" class="jp.co.fj.keinavi.beans.helpdesk.UserListBean" />
<jsp:useBean id="hd101Form" scope="request" class="jp.co.fj.keinavi.forms.helpdesk.HD101Form" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプデスク−メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD START --%>
<STYLE type="text/css">
<!--
.acdscroll {
  overflow: auto;
  width:  635px;
  height: 200px;
}
-->
</STYLE>

<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script>
// ***************************************************
// ■注意事項
//   javascriptでラジオボタンの値を変更した場合、
//   changeイベントが起きない事象が発生する。
//   上記理由により、アコーディオンの表現は廃止。
// ***************************************************
/*
$(document).ready(function(){

	// アコーディオンの表示切り替え
    function changeAccordion() {
        var val = $("input[name=tsvUser]:checked").val()
        var defined = 0;
        if(val == 0){
            $("#accordion").css("display","inline");
        }else{
            $("#accordion").css("display","none");
        }
    }

    $('input[name="tsvUser"]:radio').change(changeAccordion);

});
 */
</script>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD END   --%>

<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/ArrayList.js"></script>
<script type="text/javascript" src="./js/OrderChange.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<script type="text/javascript">
<!--
	<%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>

	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

    function submitForm() {
        document.forms[0].forward.value = "hd101";
        document.forms[0].sortMode.value = sortMode;
        // 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD START
        // 隠し項目にセット
        document.forms[0].selectUser.value = selectUserList;
        // 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD END
        document.forms[0].submit();
    }

    function submitRegist() {
        setupForm();
        document.forms[0].forward.value = "hd102";
        document.forms[0].sortMode.value = sortMode;
        document.forms[0].submit();
    }

    function submitDetail(Id) {
        setupForm();
        document.forms[0].forward.value = "hd105";
        document.forms[0].userId.value = Id;
        document.forms[0].sortMode.value = sortMode;
        // 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD START
        // 隠し項目にセット
        document.forms[0].selectUser.value = selectUserList;
        // 2015/10/21 QQ)Nishiyama デジタル証明書対応 ADD END
        document.forms[0].submit();
    }

    function submitProof() {
        var cnt = 0;
        for (var i=0; i<school.length; i++) {
            if ( certificates[school[i][2]] == true ) {
                cnt++;
                break;
            }
        }
        if ( cnt == 0 ) {
            alert("証明書発行対象の選択がされていません。");
            return ;
        }

        setupForm();
        document.forms[0].forward.value = "hd106";
        document.forms[0].sortMode.value = sortMode;
        document.forms[0].submit();
    }

    function submitBack() {
        document.forms[0].forward.value = "hd002";
        document.forms[0].sortMode.value = "";
        document.forms[0].submit();
    }

    // 契約校データ
    var school = new Array();
    <c:forEach var="school" items="${UserListBean.fullList}" varStatus="status">
        school[<c:out value="${status.index}" />] = new Array(
            "<c:out value="${school.schoolCode       }" />",
            "<c:out value="${school.schoolName       }" />",
            "<c:out value="${school.userId           }" />",
            "<c:out value="${school.userDivNm        }" />",
            "<c:out value="${school.pactDivNm        }" />",
            "<c:out value="${school.renewalDate      }" />",
            "<c:out value="${school.pactTerm         }" />",
            "<c:out value="${school.cert_BeforeDate  }" />",
            "<c:out value="${school.issued_BeforeDate}" />",
            "<c:out value="${school.changeBackColor  }" />" );
    </c:forEach>

    // チェックされたユーザID
    var certificates = new Array();

    // レイヤ操作クラス
    var sw = new SwitchLayer();

    <c:forEach var="school" items="${UserListBean.fullList}">
        <c:set var="status" value="false" />
        <c:forEach var="code" items="${hd101Form.certificates}">
            <c:if test="${ school.userId == code }"><c:set var="status" value="true" /></c:if>
        </c:forEach>
        certificates["<c:out value="${school.userId}" />"] = <c:out value="${status}" />;
    </c:forEach>

    // ソートクラス
    var oc = new OrderChange(0);
    oc.setIndex(0);
    if (school.length >0) oc.setKey(school[0][0]);
    oc.setData(school);

    var sortMode = null;
    sortMode = "<c:out value="${hd101Form.sortMode}" />";
    //alert("[" + sortMode + "]");
    if ( sortMode == null ) {
        sortMode = "高校コード";
    }

    // 行数は読み込み時の値を保持する
    var row = school.length;

    // 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START
    // 画面選択の明細を作成
    var selectUserList = new Array();
    var selectUserComma = "<c:out value="${hd101Form.selectUser}" />";
    var selectUserSplit = selectUserComma.split(",");
    if (selectUserSplit.length < 3) {
    	selectUserSplit.length = 0;
    }
    // カンマ区切りの値を配列に再セット
    for (var i = 0; i < selectUserSplit.length; i += 3) {
        var selectUser = new Array(selectUserSplit[i], selectUserSplit[i + 1], selectUserSplit[i + 2]);
        selectUserList.push(selectUser);
    }
    //alert(selectUserList);
    //alert(selectUserList.length);
    // 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END

    // 順序通りにフォームデータを作る
    function setupForm() {
        var t = document.forms[0].elements["certificates"];
    }

    function changeColor(obj) {
        // 選択状態を保持
        certificates[obj.value] = obj.checked;
    }

    // 「表示順変更」テーブルの描画
    function drawTable1(school, key) {
        var school = oc.getData();
        var key = oc.getKey();
        var table = sw.getLayer("Table1");
        for (var i=1; i<=row; i++) {
            var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
            var index = i;
            var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
            // デフォルトの背景色
            var color = "#F4E5D6";
            if (school.length >= index) {

                data[ 0] = school[index - 1][ 0];   // 高校コード
                data[ 1] = school[index - 1][ 1];   // 高校名
                data[ 2] = school[index - 1][ 2];   // ユーザID
                data[ 3] = school[index - 1][ 3];   // ユーザー区分
                data[ 4] = school[index - 1][ 4];   // 契約区分
                data[ 5] = school[index - 1][ 5];   // 更新日
                data[ 6] = school[index - 1][ 6];   // 契約期限
                data[ 7] = school[index - 1][ 7];   // 証明書有効期限
                data[ 8] = school[index - 1][ 8];   // 発行済証明書有効期限
                var checked = certificates[school[index - 1][2]] ? " checked" : "";
                data[ 9] = "<input type=\"checkbox\" name=\"certificates\" value=\""
                    + school[index - 1][ 2] + "\" onClick=\"changeColor(this)\"" + checked + ">";

                data[10] = "<a href=\"javascript:submitDetail('"
                    + school[index - 1][ 2] + "')\">詳細</a></span>"; // 詳細

                // 発行証明書チェックエラーなら背景色を変える
                if ( school[index - 1][ 9] != "0" ) {
                    color = "#FFAD8C";
                }
            }
            // 背景色
            cur.getChild( 0).style.backgroundColor = color;
            cur.getChild( 1).style.backgroundColor = color;
            cur.getChild( 2).style.backgroundColor = color;
            cur.getChild( 3).style.backgroundColor = color;
            cur.getChild( 4).style.backgroundColor = color;
            cur.getChild( 5).style.backgroundColor = color;
            cur.getChild( 6).style.backgroundColor = color;
            cur.getChild( 7).style.backgroundColor = color;
            cur.getChild( 8).style.backgroundColor = color;
            cur.getChild( 9).style.backgroundColor = color;
            cur.getChild(10).style.backgroundColor = color;
            // 値を入れる
            cur.getChild( 0).innerHTML = data[ 0];
            cur.getChild( 1).innerHTML = data[ 1];
            cur.getChild( 2).innerHTML = data[ 2];
            cur.getChild( 3).innerHTML = data[ 3];
            cur.getChild( 4).innerHTML = data[ 4];
            cur.getChild( 5).innerHTML = data[ 5];
            cur.getChild( 6).innerHTML = data[ 6];
            cur.getChild( 7).innerHTML = data[ 7];
            cur.getChild( 8).innerHTML = data[ 8];
            cur.getChild( 9).innerHTML = data[ 9];
            cur.getChild(10).innerHTML = data[10];
        }
    }

    // すべて選択
    function checkSelectAll() {
        for (var i=0; i<school.length; i++) {
            certificates[school[i][2]] = true;
        }
        drawTable1();
    }

    // すべて解除
    function checkCancelAll() {
        for (var i=0; i<school.length; i++) {
            certificates[school[i][2]] = false;
        }
        drawTable1();
    }

    // 高校コード順
    function sortSchool(index) {
        sortMode = "高校コード";
        oc.sort(
            function (a, b) {
                if (a[0] == b[0]) return 0;
                else if (a[0] > b[0]) return 1;
                else if (a[0] < b[0]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    // ユーザＩＤ順
    function sortUserId(index) {
        sortMode = "ユーザＩＤ";
        oc.sort(
            function (a, b) {
                if (a[2] == b[2]) return 0;
                else if (a[2] > b[2]) return 1;
                else if (a[2] < b[2]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    // 更新日順
    function sortRenewalDate(index) {
        sortMode = "更新日順";
        oc.sort(
            function (a, b) {
                if (a[5] == b[5]) return 0;
                else if (a[5] > b[5]) return 1;
                else if (a[5] < b[5]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    // 契約期限順
    function sortPactTerm(index) {
        sortMode = "契約期限順";
        oc.sort(
            function (a, b) {
                if (a[6] == b[6]) return 0;
                else if (a[6] > b[6]) return 1;
                else if (a[6] < b[6]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    // 証明書有効期限順
    function sortCert_BeforeDate(index) {
        sortMode = "証明書有効期限順";
        oc.sort(
            function (a, b) {
                if (a[7] == b[7]) return 0;
                else if (a[7] > b[7]) return 1;
                else if (a[7] < b[7]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    // 発行済証明書有効期限順
    function sortIssued_BeforeDate(index) {
        sortMode = "発行済証明書有効期限順";
        oc.sort(
            function (a, b) {
                if (a[8] == b[8]) return 0;
                else if (a[8] > b[8]) return 1;
                else if (a[8] < b[8]) return -1;
            }
        );
        // 再描画
        drawTable1();
    }

    function init() {
    	startTimer();
        //alert("[" + sortMode + "]");
        //drawTable1();
        if ( sortMode == "高校コード" ) {
            sortSchool();
        } else if ( sortMode == "ユーザＩＤ" ) {
            sortUserId();
        } else if ( sortMode == "更新日順" ) {
            sortRenewalDate();
        } else if ( sortMode == "契約期限順" ) {
            sortPactTerm();
        } else if ( sortMode == "証明書有効期限順" ) {
            sortCert_BeforeDate();
        } else if ( sortMode == "発行済証明書有効期限順" ) {
            sortIssued_BeforeDate();
        } else {
            sortSchool();
        }

        // 2015/10/14 QQ)Nishiyama デジタル証明書対応 ADD START
        // 画面の項目制御
        changeDisplay();

        var table = document.getElementById("tableUserSelect");
        // デフォルトの背景色
        var color = "#F4E5D6";

        // 明細行単位でループ
        //alert(selectUserList.length);
        for (var i = 0; i < selectUserList.length; i++) {
            // 末尾に行を追加
            var row = table.insertRow(-1);
            row.height = 27;

            // セルの編集
            var cells = new Array();
            for (var k = 0; k < 4; k++) {
                // セルの追加
                cells.push(row.insertCell(-1));
                // カラー
                cells[k].style.backgroundColor = color;
                // ポジション
                cells[k].align = "center";
            }

            // 値
            cells[0].innerHTML = "<span class=\"text12\">" + selectUserList[i][0]; + "</span>";  // 学校コード
            cells[1].innerHTML = "<span class=\"text12\">" + selectUserList[i][1]; + "</span>";  // 学校名
            cells[2].innerHTML = "<span class=\"text12\">" + selectUserList[i][2]; + "</span>";  // ユーザID
            cells[3].innerHTML = "<a href=\"javascript:selectRemove("
                                  + "'" + selectUserList[i][2] + "'"
                                  + ")\"><span class=\"text12\">削除</span></a>";

            //alert(selectUserList[i][2]);
        }

        // TSV作成時の終了メッセージ
        var delUserProc = <c:out value="${hd101Form.delUserProc}" />;
        if (delUserProc == "1") {
            alert("削除対象のユーザは存在しません。");
        }
        // 2015/10/14 QQ)Nishiyama デジタル証明書対応 ADD END

    }

    // 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD START
    // 選択されているラジオボタンの取得
    function getRadioVal(name) {
        var radioList = document.forms[0].elements[name];

        for(var i = 0; i < radioList.length; i ++){
          if(radioList[i].checked){
            var val = radioList[i].value;
            break;
          }
        }
        return val;
    }

    // 画面の表示切り替え
    // 引数
    //   1.処理の内容
    //   2.対象ユーザ
    function changeDisplay() {

        // ラジオボタンの選択値取得
        var process = getRadioVal("content");
        var user = getRadioVal("tsvUser");

        // 制御する画面項目名
        var name1 = "addUser";     // ユーザ追加ボタン
        var name2 = "tsvUser";     // 対象ユーザ
        var name3 = "fileRegist";  // ファイル登録ボタン

        // 対象ユーザのラジオボタンを制御
        var tsvUserDisabled = false;

        // 証明書発行（年次）を選択
        if (process == 1) {
            // 画面指定を選択不可
            document.forms[0].elements[name2][0].disabled = true;
            // ファイル指定を選択
            document.forms[0].elements[name2][1].disabled = false;
            document.forms[0].elements[name2][1].checked = true;
            user = getRadioVal(name2);

        // 証明書発行（随時）・ユーザ削除を選択
        } else  {
        	// ユーザ削除
        	if (process == 2) {
        		tsvUserDisabled = true;
        	}
            for(var i = 0; i < document.forms[0].elements[name2].length; i ++) {
                document.forms[0].elements[name2][i].disabled = tsvUserDisabled;
            }
        }

        // 一旦解除
        document.forms[0].elements[name1].disabled = false;
        document.forms[0].elements[name3].disabled = false;

        // 証明書発行（随時）
        if(process == 0) {

            // 画面選択
            if (user == 0) {
                document.forms[0].elements[name1].disabled = false;
                document.forms[0].elements[name3].disabled = true;

            // CSV指定
            } else {
                document.forms[0].elements[name1].disabled = true;
                document.forms[0].elements[name3].disabled = false;

            }

        // 証明書発行（年次）
        } else if (process == 1) {
            document.forms[0].elements[name1].disabled = true;
            document.forms[0].elements[name3].disabled = false;

        // ユーザ削除
        } else {
            document.forms[0].elements[name1].disabled = true;
            document.forms[0].elements[name3].disabled = true;

        }
    }


    // 証明書発行対象にユーザを追加する
    function addUserList() {
    	// チェックしている行番号
        var checkRowList = new Array();
        for (var i=0; i<school.length; i++) {
            if ( certificates[school[i][2]] == true ) {
                checkRowList.push(i);
            }
        }
        if ( checkRowList.length == 0 ) {
            alert("証明書発行対象のユーザが選択がされていません。");
            return ;
        }

        // テーブル取得
        var table = document.getElementById("tableUserSelect");
        var checkRow;
        var userId1;  // 選択したユーザ
        var userId2;  // 追加済みのユーザ
        var existFlg = false;
        // デフォルトの背景色
        var color = "#F4E5D6";
        for (var i = 0; i < checkRowList.length; i++) {

            // 選択行を取得
            checkRow = checkRowList[i];

            userId1 = school[checkRow][2];
            //alert("選択ユーザ：" + userId1);

            // 追加済みのチェック
            existFlg = false;
            for (var j = 0; j < selectUserList.length; j++) {
                // ユーザID
                userId2 = selectUserList[j][2];
                //alert("追加済み：" + userId2);
                if (userId1 == userId2) {
                    existFlg = true;
                    break;
                }
            }

            // 存在する場合はスキップ
            if(existFlg) {
                continue;
            }

            // 末尾に行を追加
            var row = table.insertRow(-1);
            row.height = 27;

            // セルの編集
            var cells = new Array();
            for (var k = 0; k < 4; k++) {
                // セルの追加
                cells.push(row.insertCell(-1));
                // カラー
                cells[k].style.backgroundColor = color;
                // ポジション
                cells[k].align = "center";
            }

            // 外部変数に選択内容を退避
            var selectUser = new Array(school[checkRow][0], school[checkRow][1], school[checkRow][2]);
            selectUserList.push(selectUser);

            // 値
            cells[0].innerHTML = "<span class=\"text12\">" + school[checkRow][0]; + "</span>";  // 学校コード
            cells[1].innerHTML = "<span class=\"text12\">" + school[checkRow][1]; + "</span>";  // 学校名
            cells[2].innerHTML = "<span class=\"text12\">" + school[checkRow][2]; + "</span>";  // ユーザID
            cells[3].innerHTML = "<a href=\"javascript:selectRemove("
                                  + "'" + school[checkRow][2] + "'"
                                  + ")\"><span class=\"text12\">削除</span></a>";
        }

        // 隠し項目にセット
        document.forms[0].selectUser.value = selectUserList;
    }

    // 明細の削除（１行）
    function selectRemove(userId) {

        // テーブル取得
        var table = document.getElementById("tableUserSelect");
        var compUserId;

        // 削除対象の検索
        for (var i = 0; i < selectUserList.length; i++) {
            compUserId = selectUserList[i][2];

            if (userId == compUserId) {
                // alert(compUserId);
                // 外部変数から削除
                selectUserList.splice(i, 1);
                // 画面から削除
                table.deleteRow(i + 1);
                break;
            }
        }
        // 隠し項目にセット
        document.forms[0].selectUser.value = selectUserList;
    }

    // 明細の削除（全行）
    function allRemove() {

        // テーブル取得
        var table = document.getElementById("tableUserSelect");
        // 外部変数のクリア
        selectUserList.length = 0;
        // 隠し項目にセット
        document.forms[0].selectUser.value = selectUserList;
        // 画面のクリア（ヘッダを除いて削除）
        while(table.rows.length != 1) table.deleteRow(1);
    }


    // ファイル登録画面呼び出し
    function submitUpLoad() {

    	/*
        var url = "";
        url += "<c:url value="HelpDeskCsvRegist" />";
        url += "?forward=hd107";
        url += "&backward=hd101";
		*/

	    // 隠し項目にセット
        document.forms[0].sortMode.value = sortMode;
	    document.forms[0].selectUser.value = selectUserList;

		// Disabledを解除する
		clearDisabled();

		// 空ウィンドを開く
        openWindow("", 680, 380, "DUMMY");

		// 空ウィンドに画面の情報を送信
        document.forms[0].action = "<c:url value="HelpDeskCsvRegist" />";
        document.forms[0].target = "DUMMY";
        document.forms[0].method = "POST";
        document.forms[0].submit();

        // 元に戻す
        document.forms[0].action = "<c:url value="HelpDeskList" />";
        document.forms[0].target = "";
        document.forms[0].method = "POST";

		// Disabledを復元する
		restoreDisabled();

    }

    // アップロード画面から戻った
    function submitClosed() {
        document.forms[0].sortMode.value = sortMode;
	    document.forms[0].selectUser.value = selectUserList;
        document.forms[0].forward.value = "hd101";
        document.forms[0].backward.value = "hd107";
        document.forms[0].submit();
    }

    // TSV作成処理
    function submitTsv() {

        // ラジオボタンの選択値取得
        var process = getRadioVal("content");
        var user = getRadioVal("tsvUser");
        // 入力ファイル
        var inFile = document.forms[0].elements["fileName"].value;

        //var fs = new ActiveXObject("Scripting.FileSystemObject");
        // alert(fs.FileExists(inFile));

        // 整合性チェック

        // 入力チェック
        // 証明書発行を選択
        if (process == 0 || process == 1) {
            // 画面指定
            if (user == 0) {
                // ユーザの選択チェック
                if (selectUserList.length == 0) {
                    alert("証明書発行対象ユーザが存在しません。");
                    return ;
                }
                // 画面指定ユーザをセット
                document.forms[0].selectUser.value = selectUserList;
            // ファイル指定
            } else {
                // 必須チェック
                if (inFile.length == 0) {
                    alert("入力ファイルを指定してください。");
                    return ;
                }
                // ファイル存在チェック
                /*
                if (fs.FileExists(inFile) == false) {
                    alert("入力ファイルが存在しません。");
                    return ;
                }
                */
            }
        }

        /*
        var title  = "フォルダ選択";
        var option = 0x0050;
        var root   = 0x0010;
        var shell  = new ActiveXObject("Shell.Application");
        var folder = shell.BrowseForFolder(0, title, option, root);
        if (folder != null) {
        	// デスクトップ選択時はエラーが発生する・・・
        	if (folder == "デスクトップ") {
        		var wShell = new ActiveXObject("WScript.Shell")
                document.forms[0].outFilePath.value = wShell.SpecialFolders("Desktop");
        	} else {
                document.forms[0].outFilePath.value = folder.Items().Item().Path;
        	}
        } else  {
            return ;
        }
        */
        //document.forms[0].forward.value = "hd106";
        //document.forms[0].submit();

        <%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 UPD START --%>
        //document.forms[0].sortMode.value = sortMode;
        //document.forms[0].selectUser.value = selectUserList;
        //document.forms[0].forward.value = "sheet";
        //document.forms[0].backward.value = "hd101";
        //document.forms[0].submit();
        <%-- 2019/10/28 QQ)Tanioka ダウンロード変更対応 ADD START --%>
        printDialogPattern = "noProfile";
        <%-- 2019/10/28 QQ)Tanioka ダウンロード変更対応 ADD END --%>
		download(1);
        <%-- 2019/07/08 QQ)Tanioka ダウンロード変更対応 UPD END   --%>

    }

	// Disabledを解除した要素の入れ物
	var c = new Array();

	// 操作不能を解除する
	function clearDisabled() {

		for (var i = 0; i < document.forms.length; i++) {
			var e = document.forms[0].elements;
			for (var j = 0; j < e.length; j++) {
				if (e[j].disabled) c[c.length] = e[j];
				e[j].disabled = false;
			}
		}
	}

	// 解除したDisabledを復活させる
	function restoreDisabled() {
		for (var i=0; i<c.length; i++) c[i].disabled = "disabled";
		c.length = 0;
	}

    // 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD END

// -->
</script>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">

<form action="<c:url value="HelpDeskList" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd101">
<input type="hidden" name="userId" value="">
<input type="hidden" name="sortMode" value="">
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD START --%>
<input type="hidden" name="selectUser" value="">
<input type="hidden" name="fileName" value="<c:out value="${hd101Form.fileName}" />">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="5">
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 ADD END   --%>

<!--HEADER-->
<%@ include file="/jsp/helpdesk/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="906" align="center">

<!--契約校管理-->
<div style="margin-top:14px;">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">ユーザ管理（一覧）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="846"><span class="text14-hh">表示条件を選択し、「一覧表示」ボタンを押してください。<br>
</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--表示対象の選択-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="2" cellspacing="1" width="846">
<tr>
<td width="844" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="4" width="840">
<tr>
<td><b class="text12">表示対象の選択</b></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="2" width="840">
<tr height="32">
<td width="684" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">

<tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td><span class="text12">契約区分</span></td>
    <td><span class="text12">&nbsp;：&nbsp;</span></td>
    <td>
        <c:set var="c1" value="" />
        <c:set var="c2" value="" />
        <c:set var="c3" value="" />
        <c:set var="c4" value="" />
        <c:set var="c5" value="" />
        <c:forEach var="pactDiv" items="${hd101Form.pactDiv}">
          <c:if test="${ pactDiv == '1' }"><c:set var="c1" value=" checked" /></c:if>
          <c:if test="${ pactDiv == '2' }"><c:set var="c2" value=" checked" /></c:if>
          <c:if test="${ pactDiv == '3' }"><c:set var="c3" value=" checked" /></c:if>
          <c:if test="${ pactDiv == '4' }"><c:set var="c4" value=" checked" /></c:if>
          <c:if test="${ pactDiv == '5' }"><c:set var="c5" value=" checked" /></c:if>
        </c:forEach>

        <input type="checkbox" name="pactDiv" value="1"<c:out value="${c1}"/>>契約校　
        <input type="checkbox" name="pactDiv" value="2"<c:out value="${c2}"/>>非契約校　
        <input type="checkbox" name="pactDiv" value="3"<c:out value="${c3}"/>>リサーチ参加校　
        <input type="checkbox" name="pactDiv" value="4"<c:out value="${c4}"/>>非受験校　　　　　　
        <input type="checkbox" name="pactDiv" value="5"<c:out value="${c5}"/>>ヘルプデスク
    </td>
</tr>
<tr><td><br></td></tr>
<tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td><span class="text12">ユーザ区分</span></td>
    <td><span class="text12">&nbsp;：&nbsp;</span></td>
    <td>
        <c:set var="u1" value="" />
        <c:set var="u2" value="" />
        <c:set var="u3" value="" />
        <c:set var="u4" value="" />
        <c:set var="u5" value="" />
        <c:set var="u6" value="" />
        <c:forEach var="userDiv" items="${hd101Form.userDiv}">
          <c:if test="${ userDiv == '1' }"><c:set var="u1" value=" checked" /></c:if>
          <c:if test="${ userDiv == '2' }"><c:set var="u2" value=" checked" /></c:if>
          <c:if test="${ userDiv == '3' }"><c:set var="u3" value=" checked" /></c:if>
          <c:if test="${ userDiv == '4' }"><c:set var="u4" value=" checked" /></c:if>
          <c:if test="${ userDiv == '5' }"><c:set var="u5" value=" checked" /></c:if>
          <c:if test="${ userDiv == '6' }"><c:set var="u6" value=" checked" /></c:if>
        </c:forEach>
        <input type="checkbox" name="userDiv" value="1"<c:out value="${u1}"/>>公立　
        <input type="checkbox" name="userDiv" value="2"<c:out value="${u2}"/>>私立　
        <input type="checkbox" name="userDiv" value="3"<c:out value="${u3}"/>>予備校　
        <input type="checkbox" name="userDiv" value="5"<c:out value="${u5}"/>>教育委員会　
    </td>
</tr>
<tr><td><br></td></tr>
<tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td><span class="text12">学校コード</span></td>
    <td><span class="text12">&nbsp;：&nbsp;</span></td>
    <td><input type="text" size="5" maxlength="5" class="text12" name="schoolCode" style="width:50px;" value="<c:out value="${hd101Form.schoolCode}" />" >&nbsp;（前方一致検索）</td>
</tr>
<tr><td><br></td></tr>
<tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td><span class="text12">発行済証明書</span></td>
    <td><span class="text12">&nbsp;：&nbsp;</span></td>
    <td>　<input type="radio" name="judge"  value="0"<c:if test="${ hd101Form.judge == '0' }"> checked</c:if>>
        <span class="text12">すべて</span>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>　<input type="radio" name="judge"  value="1"<c:if test="${ hd101Form.judge == '1' }"> checked</c:if>>
        <span class="text12">未発行</span>
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>　<input type="radio" name="judge"  value="2"<c:if test="${ hd101Form.judge == '2' }"> checked</c:if>>
        <span class="text12">有&nbsp;&nbsp;&nbsp;効&nbsp;&nbsp;&nbsp;</span>　→　
        <input type="text" size="4" maxlength="4" class="text12" name="certEffeYear"  style="width:40px;" value="<c:out value="${hd101Form.certEffeYear}"  />">年&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certEffeMonth" style="width:20px;" value="<c:out value="${hd101Form.certEffeMonth}" />">月&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certEffeDay"   style="width:20px;" value="<c:out value="${hd101Form.certEffeDay}"   />">日
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>　<input type="radio" name="judge"  value="3"<c:if test="${ hd101Form.judge == '3' }"> checked</c:if>>
        <span class="text12">期限切れ</span>　→　
        <input type="text" size="4" maxlength="4" class="text12" name="certOverYear"  style="width:40px;" value="<c:out value="${hd101Form.certOverYear}"  />">年&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certOverMonth" style="width:20px;" value="<c:out value="${hd101Form.certOverMonth}" />">月&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certOverDay"   style="width:20px;" value="<c:out value="${hd101Form.certOverDay}"   />">日
    </td>
</tr>
<tr>
    <td></td>
    <td></td>
    <td></td>
    <td>　<input type="radio" name="judge"  value="4"<c:if test="${ hd101Form.judge == '4' }"> checked</c:if>>
        <span class="text12">期限指定</span>　→　
        <input type="text" size="4" maxlength="4" class="text12" name="certSpecYear"  style="width:40px;" value="<c:out value="${hd101Form.certSpecYear}"  />">年&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certSpecMonth" style="width:20px;" value="<c:out value="${hd101Form.certSpecMonth}" />">月&nbsp;
        <input type="text" size="2" maxlength="2" class="text12" name="certSpecDay"   style="width:20px;" value="<c:out value="${hd101Form.certSpecDay}"   />">日
    </td>
</tr>

</table>
</td>

<!--<td width="150" bgcolor="#F4E5D6" align="center"><input type="submit" name="button" value="一覧表示" class="text12" style="width:80px;"  onClick="pushed=true"></td> -->
<td width="150" bgcolor="#F4E5D6" align="center"><input type="button" name="list" value="一覧表示" class="text12" style="width:80px;" onclick="javascrit:submitForm()"></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--表示対象の選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ツールバー-->
<table border="0" cellpadding="4" cellspacing="0" width="839">
<tr>
<td width="340" bgcolor="#93A3AD">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF">
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<input type="button" value="一括登録" class="text12" style="width:100px;" onClick="javascript:submitRegist()"></td>
  --%>
<input type="button" value="契約校マスタ更新" class="text12" style="width:150px;" onClick="javascript:submitRegist()"></td>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
</td>
<td width="500" bgcolor="#93A3AD" align="right">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td><a href="javascript:checkSelectAll()"><span class="text12">▼すべて選択</span></a>&nbsp;&nbsp;&nbsp;<a href="javascript:checkCancelAll()"><span class="text12">▼すべて解除</span></a>&nbsp;&nbsp;&nbsp;</td>
<td bgcolor="#FFFFFF"><input type="button" value="証明書発行" class="text12" style="width:100px;" onClick="javascript:submitProof()"></td>
 --%>
<td></td>
<td></td>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
</td>
</tr>
</table>
<!--/ツールバー-->

<!--項目-->
<table border="0" cellpadding="2" cellspacing="2" width="840" id="Table1">
<tr>
<td width="6%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortSchool()"><span class="text12">学校<BR>コード</span></a></td>
<td width="23%" bgcolor="#CDD7DD" align="center"><span class="text12">学校名</span></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortUserId()"><span class="text12">学校ＩＤ</span></a></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><span class="text12">ユーザ<BR>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">契約<BR>区分</span></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortRenewalDate()"><span class="text12">更新日</span></a></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortPactTerm()"><span class="text12">契約期限</span></a></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortCert_BeforeDate()"><span class="text12">証明書<BR>有効期限</span></a></td>
<td width="9%" bgcolor="#CDD7DD" align="center"><a href="javascript:sortIssued_BeforeDate()"><span class="text12">発行済<BR>証明書<BR>有効期限</span></a></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">証明書<BR>発行<BR>対象</span></td>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
</tr>

<% for ( int cnt = 0; cnt < UserListBean.getFullListSize(); cnt++ ) { %>
    <tr height="27">
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
    <td bgcolor="#F4E5D6" align="center"></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"></span>
<!-- チェックボックス・詳細
    <td bgcolor="#F4E5D6" align="center"><input type="checkbox" name="" value=""></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"><a href="#">詳細</a></span></td>
/チェックボックス・詳細 -->
    </tr>
<% } %>

</table>

<!--ツールバー-->
<table border="0" cellpadding="4" cellspacing="0" width="839">
<tr>
<td width="340" bgcolor="#93A3AD">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td bgcolor="#FFFFFF"><input type="button" value="一括登録" class="text12" style="width:100px;"  onClick="javascript:submitRegist()"></td>
 --%>
<td></td>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
</td>
<td width="500" bgcolor="#93A3AD" align="right">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td><a href="javascript:checkSelectAll()"><span class="text12">▲すべて選択</span></a>&nbsp;&nbsp;&nbsp;<a href="javascript:checkCancelAll()"><span class="text12">▲すべて解除</span></a>&nbsp;&nbsp;&nbsp;</td>
<td bgcolor="#FFFFFF"><input type="button" value="証明書発行" class="text12" style="width:100px;"  onClick="javascript:submitProof()"></td>
 --%>
<%-- 2015/11/25 QQ)Nishiyama デジタル証明書対応 DEL START --%>
<%--
<td bgcolor="#FFFFFF"><input type="button" value="証明書発行（AiCA）" class="text12" style="width:150px;" onClick="javascript:submitProof()"></td>
<td></td>
 --%>
<%-- 2015/11/25 QQ)Nishiyama デジタル証明書対応 DEL END --%>
<td bgcolor="#FFFFFF"><input type="button" value="証明書発行対象に追加" name="addUser" class="text12" style="width:150px;" onClick="javascript:addUserList()"></td>
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
</td>
</tr>
</table>
<!--/ツールバー-->


</div>
<!--/契約校管理-->

<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD START --%>
<!--デジタル証明書用TSV作成-->
<div style="margin-top:10px;">
<!--親テーブル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
  <tr>
    <!--青の枠線-->
    <td bgcolor="#8CA9BB">
      <!--テーブル：タイトル-->
      <table border="0" cellpadding="2" cellspacing="1" width="846">
        <tr>
          <!--白抜き-->
          <td width="844" bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="4" width="840">
              <tr>
                <td><b class="text12">デジタル証明書用TSV作成</b></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <table border="0" cellpadding="2" cellspacing="0" width="846">
        <tr>
          <!--↓背景水色-->
          <td width="846" bgcolor="#E1E6EB">
            <!--↓テーブル：処理の選択-->
            <table border="0" cellpadding="0" cellspacing="0" width="840">
              <tr>
                <!--青の枠線-->
                <td bgcolor="#8CA9BB">
                  <table border="0" cellpadding="2" cellspacing="1" width="840">
                    <tr height="32">
                      <!--白抜き-->
                      <td width="838" bgcolor="#FFFFFF">
                        <table border="0" cellpadding="0" cellspacing="4" width="828">
                          <tr>
                            <td width="30"></td>
                            <td colspan="2">
                              <img src="./shared_lib/img/parts/yaji_r.gif" border="0" alt="">
                              <span class="text12"><font color="#000080">処理の選択</font></span>
                            </td>
                          </tr>
                          <tr height="32">
                            <td width="30"></td>
                            <td width="100" bgcolor="#E1E6EB">
                              <span><center><b class="text12">処理の内容</b></center></span>
                            </td>
                            <td bgcolor="#F4E5D6">
                              <input type="radio" name="content" value="0" onClick="javascript:changeDisplay()" <c:if test="${ hd101Form.content == '0' }"> checked</c:if>>
                                <span class="text12">証明書発行</span>
                                <span class="text12">&nbsp;&nbsp;</span>
                              <input type="radio" name="content" value="1" onClick="javascript:changeDisplay()" <c:if test="${ hd101Form.content == '1' }"> checked</c:if>>
                                <span class="text12">証明書発行【年次】</span>
                                <span class="text12">&nbsp;&nbsp;</span>
                              <input type="radio" name="content" value="2" onClick="javascript:changeDisplay()" <c:if test="${ hd101Form.content == '2' }"> checked</c:if>>
                                <span class="text12">ユーザ削除</span>
                                <span class="text12">&nbsp;&nbsp;</span>
                            </td>
                          </tr>
                        </table>
                      </td>
                  </table>
                </td>
              </tr>
            </table>
            <!--↑テーブル：処理の選択-->

            <!--↓テーブル：対象ユーザ選択-->
            <div style="margin-top:10px;"></div>
            <table border="0" cellpadding="0" cellspacing="0" width="840">
              <tr>
                <!--↓青枠線-->
                <td bgcolor="#8CA9BB">
                  <table border="0" cellpadding="2" cellspacing="1" width="840">
                    <tr height="32">
                      <!--白抜き-->
                      <td width="838" bgcolor="#FFFFFF">
                        <table border="0" cellpadding="0" cellspacing="4" width="828">
                          <tr>
                            <td width="30" bgcolor="#FFFFFF">
                            </td>
                            <td valign="top" bgcolor="#FFFFFF">
                              <img src="./shared_lib/img/parts/yaji_r.gif" border="0" alt="">
                            </td>
                            <td valign="top" bgcolor="#FFFFFF">
                              <input id="tsvUser1" type="radio" name="tsvUser" value="0" onClick="javascript:changeDisplay()" <c:if test="${ hd101Form.tsvUser == '0' }"> checked</c:if>>
                              <span class="text12">証明書発行対象で選択したユーザ</span>
                                <div id="accordion">
                                  <!--↓開く・閉じる↓-->
                                  <div class="acdscroll">
                                    <table border="0" cellpadding="0" cellspacing="0" width="610">
                                      <tr>
                                        <!--↓青枠線-->
                                        <td bgcolor="#8CA9BB">
                                          <table border="0" cellpadding="2" cellspacing="1" width="610">
                                            <tr>
                                              <!--白抜き-->
                                              <td width="608" bgcolor="#FFFFFF">
                                                <table border="0" cellpadding="2" cellspacing="2" width="608" id="tableUserSelect">
                                                  <tr height="27">
                                                    <td width="15%" bgcolor="#CDD7DD" align="center"><span class="text12">学校<BR>コード</span></td>
                                                    <td width="50%" bgcolor="#CDD7DD" align="center"><span class="text12">学校名</span></td>
                                                    <td width="15%" bgcolor="#CDD7DD" align="center"><span class="text12">学校ＩＤ</span></td>
                                                    <td width="40%" bgcolor="#CDD7DD" align="center"><a href="javascript:allRemove()"><span class="text12">すべて削除</span></a></td>
                                                  </tr>
                                                </table>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <!--↑開く・閉じる↑-->
                                </div>
                            </td>
                          </tr>
                          <tr height="32">
                            <!--白抜き-->
                            <td width="60" colspan="2" bgcolor="#FFFFFF">
                            </td>
                            <td bgcolor="#FFFFFF">
                              <input id="tsvUser2" type="radio" name="tsvUser" value="1" onClick="javascript:changeDisplay()" <c:if test="${ hd101Form.tsvUser == '1' }"> checked</c:if>>
                              <span class="text12"><font color="#000080">ファイル指定</span>
                              <span class="text12">&nbsp;：&nbsp;</span>
                              <input type="text" value="<c:out value="${hd101Form.fileName}" />" class="text12" style="width:535px;" disabled="disabled">
                              <input type="button" value="&nbsp;ファイル登録&nbsp;" name="fileRegist" class="text12" style="width:100px;" onClick="submitUpLoad()">
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <!--↑青枠線-->
              </tr>
            </table>
            <!--↑テーブル：対象ユーザ選択-->
            <!--↓テーブル：TSV作成ボタン-->
            <table border="0" cellpadding="0" cellspacing="1" width="840">
              <tr height="32">
                <td width="720" bgcolor="#E1E6EB"></td>
                <td bgcolor="#E1E6EB">
                  <input type="button" value="&nbsp;TSV作成&nbsp;" class="text12" style="width:100px;" onClick="submitTsv()">
                </td>
              </tr>
            </table>
            <!--↑テーブル：TSV作成ボタン-->
          </td>
          <!--↑背景水色-->
        </tr>
      </table>
    </td>
  </tr>
</table>
</div>
<!--デジタル証明書用TSV作成-->
<%-- 2015/10/09 QQ)Nishiyama デジタル証明書対応 ADD END   --%>



<!--spacer-->
<%-- 2015/10/18 QQ)Nishiyama デジタル証明書対応 DEL START --%>
<%--
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
  --%>
<%-- 2015/10/18 QQ)Nishiyama デジタル証明書対応 DEL END   --%>
<!--/spacer-->
</td>
</tr>
</table>
<!--/概要-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/helpdesk/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>


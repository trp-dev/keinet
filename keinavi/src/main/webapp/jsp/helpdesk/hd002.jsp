<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／表示メニュー選択</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>

	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

    function submitForm() {

        if (!document.forms[0].elements["menu"][0].checked &&
            !document.forms[0].elements["menu"][1].checked &&
            !document.forms[0].elements["menu"][2].checked &&
            !document.forms[0].elements["menu"][3].checked &&
            !document.forms[0].elements["menu"][4].checked ) {
            alert("表示するメニューを選択してください。");
            return false;
        }

        // 高校代行メニュー
        var val = new Validator();
        if (document.forms[0].elements["menu"][0].checked) {
            if (!val.isAlphanumeric(document.forms[0].schoolCode.value, 5)) {
                alert("高校コードは半角英数字5桁で入力してください。");
                return false;
            }
            document.forms[0].forward.value  = "w002"; // 転送先をセット
        }

        // 校舎メニュー
        if (document.forms[0].elements["menu"][1].checked) {
            document.forms[0].forward.value  = ""; // 転送先をセット
            document.forms[0].backward.value = "hd002";
            alert("校舎メニューは只今工事中です．．．");
            return false;
        }

        // 営業部メニュー
        if (document.forms[0].elements["menu"][2].checked) {
            document.forms[0].forward.value  = "w002"; // 転送先をセット
        }

        // ユーザ管理
        if (document.forms[0].elements["menu"][4].checked) {
            document.forms[0].forward.value  = "hd101";
        }
        
        // 高校作成ファイルダウンロード
        if (document.forms[0].elements["menu"][3].checked) {
        	if (document.forms[0].salesCode.value.length == 0) {
        		alert("営業部が選択されていません。");
        		return false;
        	}
            document.forms[0].forward.value  = "sd101";
        }

        document.forms[0].submit();
    }

	function init() {
		startTimer();
		var message = "<c:out value="${ErrorMessage}" />";
		if (message != "") alert(message);
	}

// -->
</script>
</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()">
<form action="<c:url value="HelpDeskMenu" />" method="POST" onsubmit="return false;">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd002">

<!--HEADER-->
<%@ include file="/jsp/helpdesk/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_00.html"-->
<!--/ヘルプナビ-->


<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
    <tr valign="top" align="center">
        <td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
        <td><BR><BR><b class="text16">表示するメニューを選択してください</b></td>
    </tr>
    <tr valign="top" align="center">
        <td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
        <td><BR><BR><BR></td>
    </tr>
    <tr valign="top" align="center">
        <td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
        <td>

<!--テキスト出力-->
<table border="0" cellpadding="0" cellspacing="0" width="504">
    <tr valign="top">
        <td bgcolor="#8CA9BB">
            <table border="0" cellpadding="0" cellspacing="1" width="504">
                <tr valign="top">
                    <td width="502" bgcolor="#FFFFFF" align="center">
                        <!--下の段-->
                        <div style="margin-top:3px;">
                            <table border="0" cellpadding="2" cellspacing="0" width="490">
                                <tr>
                                    <td align="center">
                                        <table border="0"  cellpadding="2" cellspacing="0">

                                            <tr><td><INPUT type="radio" name="menu" value="1"<c:if test="${ form.menu == 1 }"> checked</c:if>>高校代行メニュー</td></tr>
                                            <tr><td>　　　　　高校コード：<INPUT type="text" name="schoolCode" value="<c:out value="${form.schoolCode}" />" size="7" maxlength="5"></td></tr>
                                            <tr><td><INPUT type="radio" name="menu" value="2"<c:if test="${ form.menu == 2 }"> checked</c:if>>校舎メニュー</td></tr>
                                            <!-- 校舎メニューリスト -->
                                            <tr><td>　　　　　
                                            <select name="buildingCode" class="text12">
                                                <c:forEach var="sector" items="${SectorSession.sectorList}">
                                                    <c:if test="${ sector.hsBusiDivCD == '2' }">
                                                        <option value="<c:out value="${sector.sectorCD}" />"><c:out value="${sector.sectorName}" /></option>
                                                    </c:if>
                                                </c:forEach>
                                                </select>
                                            </td></tr>
                                            <!-- /校舎メニューリスト-->

                                            <tr><td><INPUT type="radio" name="menu" value="3"<c:if test="${ form.menu == 3 }"> checked</c:if>>営業部メニュー</td></tr>
                                            <!-- 営業部メニューリスト -->
                                            <tr><td>　　　　　
                                            <select name="businessCode" class="text12">
                                                <c:forEach var="sector" items="${SectorSession.sectorList}">
                                                    <c:if test="${ sector.hsBusiDivCD == '1' }">
                                                        <option value="<c:out value="${sector.sectorCD}" />"><c:out value="${sector.sectorName}" /></option>
                                                    </c:if>
                                                </c:forEach>
                                                </select>
                                            </td></tr>
                                            <!-- /営業部メニューリスト-->
                                            
                                            <tr><td><INPUT type="radio" name="menu" value="4"<c:if test="${ form.menu == 4 }"> checked</c:if>>高校作成ファイルダウンロード</td></tr>
                                            <!-- 高校作成ファイルダウンロードメニューリスト -->
                                            <tr><td>　　　　　
                                            <select name="salesCode" class="text12">
                                                <c:forEach var="sector" items="${SectorSession.sectorList}">
                                                    <c:if test="${ sector.hsBusiDivCD == '1' }">
                                                    <c:if test="${ sector.downloadableCount > 0 }">
                                                        <option value="<c:out value="${sector.sectorCD}" />" <c:if test="${sector.sectorCD == param.salesCode}"> selected</c:if>><c:out value="${sector.sectorName}" /></option>
                                                    </c:if>
                                                    </c:if>
                                                </c:forEach>
                                                </select>
                                            </td></tr>
                                            <!-- /高校作成ファイルダウンロードメニューリスト-->

                                            <tr><td><BR><BR></td></tr>
                                            <tr><td>メンテナンスメニュー</td></tr>
                                            <tr><td>　<INPUT type="radio" name="menu" value="5"<c:if test="${ form.menu == 5 }"> checked</c:if>>ユーザ管理</td></tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <BR><BR>
                                        <!--ボタン-->
                                        <table border="0" cellpadding="0" cellspacing="0" width="150">
                                            <tr valign="top">
                                                <td bgcolor="#8CA9BB">
                                                    <table border="0" cellpadding="7" cellspacing="1" width="150">
                                                        <tr valign="top">
                                                            <td width="680" bgcolor="#FBD49F" align="center">
                                                                <input type="button" value="&nbsp;決定&nbsp;" class="text12" style="width:120px;"  onClick="submitForm();">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--/ボタン-->
                                        <BR>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--/下の段-->

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--/テキスト出力-->
        </td>
    </tr>

</table>
</div>
<!--/タイトル-->


<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/helpdesk/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

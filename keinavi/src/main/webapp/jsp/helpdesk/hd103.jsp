<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="jp.co.fj.keinavi.data.helpdesk.RegistError" %>

<jsp:useBean id="registError" scope="session" class="jp.co.fj.keinavi.data.helpdesk.RegistError" />
<jsp:useBean id="HelpDeskRegistBean" scope="request" class="jp.co.fj.keinavi.beans.helpdesk.HelpDeskRegistBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプデスク−メンテナンス</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>

	function submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitBack() {
        document.forms[0].forward.value = "hd101";
        document.forms[0].submit();
    }

    function init() {
    	startTimer();
    }

//-->
</SCRIPT>

</head>

<body onload="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="HelpDeskList" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="hd103">

<!--HEADER-->
<%@ include file="/jsp/helpdesk/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt=""></td>
</tr>
</table>
<!--/spacer-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="906" align="center">

<!--契約校管理-->
<div style="margin-top:14px;">

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="846">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="830">

<table border="0" cellpadding="0" cellspacing="0" width="830">
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="814" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">ユーザ管理（一括登録）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="830" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="830" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="830" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--一括登録エラー-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="878">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="15" cellspacing="0" width="878">
<tr>
<td width="876" bgcolor="#FFFFFF" align="center">

<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td align="center"><img src="./mainte/img/touroku_error.gif" width="177" height="25" border="0" alt="一括登録エラー"><br></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">[エラー発生件数　<font color="#FF6E0E"><%=HelpDeskRegistBean.getErrorList().size()%></font>件]</b></td>
</tr>
<tr>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD START --%>
<%--
<td align="center"><span class="text14-hh">CSVファイルの内容にエラーが含まれていたため、一括登録に失敗しました。<br>エラー内容をご確認の上、CSVファイルを修正し、再度一括登録処理を行ってください。</span></td>
  --%>
<c:if test="${ HelpDeskRegistBean.errPattern == '0' }">
  <td align="center"><span class="text14-hh">CSVファイルの内容にエラーが含まれていたため、証明書用TSVファイル出力に失敗しました。<br>エラー内容をご確認の上、CSVファイルを修正し、再度証明書用TSVファイル出力処理を行ってください。</span></td>
</c:if>
<c:if test="${ HelpDeskRegistBean.errPattern == '1' }">
  <td align="center"><span class="text14-hh">CSVファイルの内容にエラーが含まれていたため、一括更新に失敗しました。<br>エラー内容をご確認の上、CSVファイルを修正し、再度一括更新処理を行ってください。</span></td>
</c:if>
<c:if test="${ HelpDeskRegistBean.errPattern == '2' }">
  <td align="center"><span class="text14-hh">CSVファイルの内容にエラーが含まれていたため、契約更新ファイル登録に失敗しました。<br>エラー内容をご確認の上、CSVファイルを修正し、再度契約更新ファイル登録処理を行ってください。</span></td>
</c:if>
<%-- 2015/10/19 QQ)Nishiyama デジタル証明書対応 UPD END   --%>
</tr>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="一覧へ戻る" class="text12" style="width:180px;" onClick="javascript:submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="エラー内容を印刷する" class="text12" style="width:180px;" OnClick="javascript:window.print()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="4" cellspacing="2" width="604">
<!--項目-->
<tr bgcolor="#CDD7DD">
<td width="7%" align="center"><span class="text12">行数</span></td>
<td width="12%" align="center"><span class="text12">学校コード</span></td>
<td width="21%" align="center"><span class="text12">ユーザＩＤ</span></td>
<td width="60%" align="center"><span class="text12">エラー内容</span></td>
</tr>
<!--/項目-->

<%
 for (java.util.Iterator it=HelpDeskRegistBean.getErrorList().iterator(); it.hasNext();) {
    RegistError error = (RegistError)it.next();
%>
    <!--set-->
    <tr>
    <td bgcolor="#E1E6EB" align="right">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><span class="text12"><%=error.getLine()%></span></td>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
    </tr>
    </table>
    </td>
    <td bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
    <td><span class="text12"><%=error.getSchoolCode()%></span></td>
    </tr>
    </table>
    </td>
    <td bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
    <td><span class="text12"><%=error.getUserId()%></span></td>
    </tr>
    </table>
    </td>
    <td bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
    <td><span class="text12"><%=error.getErrorContents()%></span></td>
    </tr>
    </table>
    </td>
    </tr>
    <!--/set-->
<% } %>
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="一覧へ戻る" class="text12" style="width:180px;" onClick="javascript:submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="エラー内容を印刷する" class="text12" style="width:180px;" OnClick="javascript:window.print()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/一括登録エラー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/helpdesk/footer.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="jp.co.fj.keinavi.data.news.InformList" %>
<jsp:useBean id="InformListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ログイン</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript">
<!--
	var TIME_IN_SEC = 3;

	/**
	 * 自動再表示
	 */
	function goNow(){
		setTimeout("submitForm('no_need')", 1000 * TIME_IN_SEC );
	}
	/**
	 * サブミッション
	 */
	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
	/**
	 * 初期化
	 */
	function init(){
		goNow();
	}
	
//-->
</script>
</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()">
<form action="<c:url value="HDOnCertServlet" />" method="POST">
<input type="hidden" name="forward" value="hd106">
<input type="hidden" name="backward" value="hd106">
<input type="hidden" name="infoId" value="">
<%-- formの値を引き継ぐ必要がある --%>

<center>
<br><br>証明書の発行中です。<br>しばらくお待ちください。
</center>

</form>
</body>
</html>

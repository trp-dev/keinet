<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<jsp:useBean id="virusDetectedBean" scope="request" class="jp.co.fj.keinavi.beans.UploadFileBean" />
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／高校別データダウンロードサブメニュー</title>
<link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">
<script type="text/javascript">
<!--
	<%@ include file="/jsp/script/timer.jsp" %>
	
	function init() {
		startTimer();
		preloadImages();
	}
	
// -->
</script>
        <csscriptdict>
            <script type="text/javascript"><!--

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

function newImage(arg) {
    if (document.images) {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}
function changeImages() {
    if (document.images && (preloadFlag == true)) {
        for (var i=0; i<changeImages.arguments.length; i+=2) {
            document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
        }
    }
}

// --></script>
        </csscriptdict>
        <csactiondict>
            <script type="text/javascript"><!--
var preloadFlag = false;
function preloadImages() {
    if (document.images) {
        over_dl_icon = newImage(/*URL*/'freemenu/f004img/dl_icon2.gif');
        over_dl_icon2 = newImage(/*URL*/'freemenu/f004img/dl_icon2.gif');
        over_up_icon = newImage(/*URL*/'freemenu/f004img/up_icon2.gif');
        preloadFlag = true;
    }
}

// --></script>
        </csactiondict>
    </head>

<%
	if (request.getAttribute("NOTEXISTEXAM") != null) {		
%>
<script type="text/javascript">
<!--
		alert("受験生がいる模試データが存在しません。");
//-->
</script>
<%
	}
%>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="PostOfficeBoxSubMenu"/>" method="POST">
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->

<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

<!-- submit用JavaScript -->
<script type="text/javascript"><!--
    function submitForm(forward) {
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "f004";
        document.forms[0].submit();
    }
    
    function submitBack() {
<%
    if (session.getAttribute("FromTopMenu") == null) {
%>
        document.forms[0].forward.value = "f001";
<%
    }
    else {
%>
        document.forms[0].forward.value = "<%=(String)session.getAttribute("FromTopMenu")%>";
<%
    }
%>
        document.forms[0].backward.value = "f004";
        document.forms[0].submit();
    }
//-->
</script>

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f004img/shisho_sub_mi.gif" width="377" height="21" border="0" alt="高校別データダウンロードサブメニュー"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
    <table border="0" cellspacing="3" cellpadding="3" align="center">
        <tr>
            <td>下のメニューからご希望のメニューを選択してください。</td>
        </tr>
        <tr>
            <td><br>
                <table border="0" cellspacing="0" cellpadding="5">
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'dl_icon',/*URL*/'freemenu/f004img/dl_icon2.gif');return true" onmouseout="changeImages( /*CMP*/'dl_icon',/*URL*/'freemenu/f004img/dl_icon.gif');return true" href="javascript:submitForm('f005')"><img src="freemenu/f004img/dl_icon.gif" alt="模試成績データダウンロード" name="dl_icon" width="30" height="30" border="0"></a></td>
                        <td><a href="javascript:submitForm('f005')"><img src="freemenu/f004img/seiseki_dl_mo.gif" alt="模試成績データダウンロード" height="21" width="271" border="0"></a></td>
                    </tr>
                    <c:if test="${HSDLBean.open}">
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'dl_icon2',/*URL*/'freemenu/f004img/dl_icon2.gif');return true" onmouseout="changeImages( /*CMP*/'dl_icon2',/*URL*/'freemenu/f004img/dl_icon.gif');return true" href="javascript:submitForm('f006')"><img src="freemenu/f004img/dl_icon.gif" alt="入試結果調査システムダウンロード" name="dl_icon2" width="30" height="30" border="0"></a></td>
                        <td><a href="javascript:submitForm('f006')"><img src="freemenu/f004img/format_dl_mo.gif" alt="入試結果調査システムダウンロード" height="21" border="0"></a></td>
                    </tr>
                    </c:if>
                    <!-- 2004.11.09 F.Urakawa 仕様変更により削除-->
                    <!--
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'up_icon',/*URL*/'freemenu/f004img/up_icon2.gif');return true" onmouseout="changeImages( /*CMP*/'up_icon',/*URL*/'freemenu/f004img/up_icon.gif');return true" href="javascript:submitForm('f007')"><img src="freemenu/f004img/up_icon.gif" alt="入試結果調査データアップロード" name="up_icon" width="30" height="30" border="0"></a></td>
                        <td><a href="javascript:submitForm('f007')"><img src="freemenu/f004img/nyushi_up_mo.gif" alt="入試結果調査データアップロード" height="21" width="313" border="0"></a></td>
                    </tr>
                    -->
                    <!-- 2004.11.09 F.Urakawa-->
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'up_icon',/*URL*/'freemenu/f004img/up_icon2.gif');return true" onmouseout="changeImages( /*CMP*/'up_icon',/*URL*/'freemenu/f004img/up_icon.gif');return true" href="javascript:submitForm('f010')"><img src="freemenu/f004img/up_icon.gif" alt="ファイルアップロード" name="up_icon" width="30" height="30" border="0"></a></td>
                        <td><a href="javascript:submitForm('f010')"><img src="freemenu/f004img/upload_dl_mo.gif" alt="ファイルアップロード" border="0"></a></td>
                    </tr>
                </table>
                <br>
            </td>
        </tr>
    </table>
    <br>
    <%-- ウィルス情報/ --%>
    <c:if test="${virusDetectedBean.recordCount > 0}">
    <table align="center">
    	<tr><td colspan="4" style="align:center;width:500px;">
    	<div style="color:red">アップロードされた以下のファイルについてウィルスを検知したため、Kei-Naviサーバから削除しました。ご確認ください。</div>
		<div style="color:red">※このメッセージはウィルス検出日の１週間後まで表示されます。</div>
    	</td></tr>
    	<c:forEach var="history" items="${virusDetectedBean.recordSet}">
    	<tr>
		<td style="color:red">ファイル名：</td>
		<td style="color:red"><c:out value="${history.uploadFileName}"/></td>
		<td style="color:red">アップロード日時：</td>
		<td style="color:red"><fmt:formatDate value="${history.updateDate}" pattern="yyyy/MM/dd HH:mm" /></td>
    	</tr>
    	</c:forEach>
    </table>
    <br>
    </c:if>
    <%-- /ウィルス情報 --%>
</td>
</tr>
</table>

<%-- センターリサーチ対応 --%>
<fmt:setBundle basename="jp.co.fj.freemenu.resources.banzai" var="bundle" />
<c:set var="begin"><fmt:message key="banzai.begin" bundle="${bundle}" /></c:set>
<c:set var="end"><fmt:message key="banzai.end" bundle="${bundle}" /></c:set>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmmss" var="current" />
<c:if test="${ current >= begin && current < end }">
<div align="center">
	<table width="750" border="0" cellspacing="1" cellpadding="20" bgcolor="#ff7f50">
		<tr align="left" valign="middle" bgcolor="#ffe4c4">
			<td>
<!--
				<CENTER><span style="font-size:24px"><B>「バンザイシステム」のお知らせ</B></span></CENTER>
				<BR>
-->
				<b>2011年12月22日（木）</b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;バンザイシステム「パソコン(Windows)版 プログラム」・「マニュアル（セットアップ・基本操作・ご利用ガイド（担任編））」<BR>
				&nbsp;&nbsp;&nbsp;&nbsp;ご提供開始<BR><BR>
				<b>2012年 1月18日（水）</b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;<b><span style="font-size:16px">■Kei-Navi</span><span style="font-size:12px">＜有料：学校単位でのご契約が必要＞</span>ご契約校</b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font color="red">13時から</font></b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;「個人成績データ」「大学マスタ」ご提供開始<BR>
				&nbsp;&nbsp;&nbsp;&nbsp;<b><span style="font-size:16px">■Kei-Navi</span>ダウンロードサービス<span style="font-size:12px">＜無料＞</span>校</b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><font color="red">15時から</font></b><BR>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;「個人成績データ」「大学マスタ」ご提供開始<BR>
<!--			
				<BR>
				<HR>
				
				&nbsp;&nbsp;(1)上の「模試成績データダウンロード」をクリックし、<BR>
				&nbsp;&nbsp;(2)次に表示される画面で「2007（年度）」・「高３生」を選んで「表示」をクリックすると、<BR>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;「高３生」の一覧の下に「バンザイシステム」というタイトルの一覧が表示されますので、<BR>
				&nbsp;&nbsp;(3)そこの「DOWNLOAD」というアイコンをクリックしてダウンロードしてください。
-->
			</td>
		</tr>
	</table>
  <br>
</div>
</c:if>

<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="form" scope="request" class="jp.co.fj.freemenu.forms.F010Form" />
<jsp:useBean id="uploadFileKindBean" scope="request" class="jp.co.fj.keinavi.beans.UploadFileKindBean" />
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ファイルアップロード</title>
<link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript">
<!--
	<%@ include file="/jsp/script/timer.jsp" %>

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	function submitForm(form, forward) {
		form.target = "_self";
		form.forward.value = forward;
		form.submit();
	}
    function submitBack() {
    	submitForm(document.forms[0], "f004");
    }
    function submitData() {
    
    	var js = new JString();
    	var fu = new FormUtil();
    	var va = new Validator();
 
    	var form = document.forms[1];
    	
    	if (fu.countChecked(form.fileTypeID) == 0) {
		    alert("ファイル種類を選択してください。");
		    return;
		}
    
		if (form.file.value == '') {
		    alert("ファイル名を入力してください。");
		    return;
		}
        
        js.setString(form.comments.value);
        if (js.getLength() > 180) {
            alert('コメントは全角９０文字までで入力してください。');
            return;
        }
        
		var filename = form.file.value.substring(form.file.value.lastIndexOf('\\') + 1, form.file.value.length);
        if (va.hasJapanese(filename)) {
            alert('<kn:message id="f001a" />');
            return;
        }

        if(!confirm("指定されたファイルをアップロードします。\nよろしいですか？")) {
	    	return;
    	}
    	
        form.filename.value = form.file.value;
        submitForm(form, "f010");
    }
        
    function init() {
    	startTimer();
	<c:if test="${form.errorMessage != ''}">
		alert("<c:out value="${form.errorMessage}"/>");
	</c:if>
    }
    
// -->
</script>

</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form style="margin:0" action="<c:url value="OpenFileUpload"/>" method="POST" onsubmit="return false">
<input type="hidden" name="forward" value=""/>
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />"/>
</form>
<form style="margin:0" action="<c:url value="OpenFileUpload"/>" method="POST" enctype="multipart/form-data" onsubmit="return false">
<input type="hidden" name="forward" value=""/>
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />"/>
<input type="hidden" name="filename" value=""/>
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f010_1img/fileupload_up_mi.gif" border="0" alt="ファイルアップロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
<table border="0" cellspacing="3" cellpadding="3" align="center">
<tr align="center">
<td>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<div>アップロードするファイル種類とファイルを指定し、さらに先生・コメント欄に先生名他を入力の上、送信ボタンを押してください。</div>
<div><a href="javascript:submitForm(document.forms[0], 'f011');">過去3ヶ月間にアップロードされたファイル一覧（ウィルス検出されたものを除く）</a></div>
</td>
</tr>
<tr>
<td>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="1" cellpadding="1">
<tr valign="top">
<td>ファイル種類　：</td>
<td>
<c:forEach var="kind" items="${uploadFileKindBean.recordSet}" varStatus="status">
<input type="radio" name="fileTypeID" value="<c:out value="${kind.fileTypeID}"/>" <c:if test="${form.fileTypeID eq kind.fileTypeID}">checked</c:if>><c:out value="${kind.fileTypeName}"/><br/>
<c:if test="${'01' eq kind.fileTypeID}">
<font color="red">&nbsp;&nbsp;&nbsp;&nbsp;※必ず「*****k10cust.csv」と「*****k10.csv」の２種類を<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;　それぞれアップロードしてください。</font><br/>
</c:if>
</c:forEach>
</td>
</tr>
<tr valign="top">
<td>ファイル名　：</td>
<td><input type="file" name="file" style="width:200px; background:white;"><br/>
<font color="red">&nbsp;&nbsp;※ファイル名には半角英数字のみ使用できます。</font>
</td>
</tr>
<tr valign="top">
<td>先生名・コメント　：</td>
<td><textarea name="comments" rows="3" cols="60"><c:out value="${form.comments}"/></textarea><br/>
<font color="red">&nbsp;&nbsp;「先生名・コメント」欄には、必ず先生名を入力してください。<br/>
&nbsp;&nbsp;また、「河合塾入試結果調査システム」によるファイル以外を送信される場合は<br/>
&nbsp;&nbsp;内容に関するご説明も入力してください。<br/>
&nbsp;&nbsp;※入力された先生宛に、営業部から問い合わせをさせていただく場合が<br/>
&nbsp;&nbsp;　ありますので予めご了承ください。<br/>
&nbsp;&nbsp;※コメントには半角英数字と全角文字が使用できます。<br/>
&nbsp;&nbsp;　ただし以下（機種依存文字）は使用できません。<br/>
&nbsp;&nbsp;　「まる付き数字」「ローマ数字」「半角カナ」<br/>
&nbsp;&nbsp;　「No.やTelや単位などの記号」</font>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
</div>
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="600">
<tr valign="top">
<td width="906" bgcolor="#FBD49F" align="center">
<input type="button" value="&nbsp;送信&nbsp;" class="text12" style="width:140px;" onclick="javascript:submitData();">
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<br>
</td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／Kei-Naviダウンロードサービス</title>
<link rel="stylesheet" type="text/css" href="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/style/stylesheet.css">
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/js/OpenWindow.js"></script>
<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>

function newImage(arg) {
    if (document.images) {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}
function changeImages() {
    if (document.images && (preloadFlag == true)) {
        for (var i=0; i<changeImages.arguments.length; i+=2) {
            document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
        }
    }
}

// -->
</script>
<script type="text/javascript"><!--
var preloadFlag = false;
function preloadImages() {
    if (document.images) {
        over_taiken_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/taiken_i2.gif');
        over_seiseki_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_i2.gif');
        over_center_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i2.gif');
        over_shisho_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shisho_i2.gif');
        over_nyushi_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/nyushi_i2.gif');
        over_kenkyu_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/kenkyu_i2.gif');
        over_shusatsu_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shusatsu_i2.gif');
        over_banzaisp_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/banzaisp_i2.jpg');
        over_moshi_i3 = newImage(/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/moushikomid2.gif');
        preloadFlag = true;
    }
}

// -->
</script>

<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/submit_exit_simple.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

    function submitUserInfo() {
        openWindow("<c:url value="U101DLServlet" />" + "?forward=u101_dl&backward=f001", 680, 580);
    }

    function submitManager() {
        document.forms[0].forward.value = "u001_dl";
        document.forms[0].submit();
    }

    function init() {
    	startTimer();
    	preloadImages();
    }

// -->
</script>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="FreeMenuTop" />" method="POST">
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/com_bk_t.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/com_bk_l.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968"  align="center">
<tr valign="top">
<td width="30">&nbsp;</td>
<td width="718" align="left"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/muryo_mi.gif" width="538" height="31" border="0" alt="Kei-Naviダウンロードサービス"><br></td>
<td width="220" align="center">
	<c:if test="${ not LoginSession.pact }">
	<div>
	<input type="button" value="利用者情報確認" class="text12" style="width:180px" onClick="submitUserInfo();">
	</div>
    <c:if test="${LoginSession.cert}">
	<c:if test="${ LoginSession.manager }">
	<div style="margin-top:12px;">
	<input type="button" value="利用者管理" class="text12" style="width:180px" onClick="submitManager();">
	</div>
	</c:if>
	</c:if>
	</c:if>
</td>
</tr>
</table>
</div>
<!--/タイトル-->

<!-- submit用JavaScript -->
<script type="text/javascript"><!--
    function submitForm(forward) {
        // 体験版のみアクション先が異なる
        if (forward == 'w008') {
            document.forms[0].action = "<c:url value="Blank" />";
        }
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "f001";
        document.forms[0].submit();
    }

    <c:if test="${LoginSession.cert}">
	//模試受付システム連携用
    function submitForm1() {
    	window.open("about:blank","moshiWindow");
    	document.forms[1].target = "moshiWindow";
    	document.forms[1].action = "<c:out value="${moshiURL}" />";
    	document.forms[1].sciv.value = "<c:out value="${sciv}" />";
    	document.forms[1].scid.value = "<c:out value="${scid}" />";
    	document.forms[1].scnm.value = "<c:out value="${scnm}" />";
		document.forms[1].submit();
    }
    </c:if>

//-->
</script>

<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
    <table border="0" cellspacing="3" cellpadding="3" align="center">
    <!-- 2007年度改修A項目対応によりコメントアウト
        <tr>
            <td>下のメニューからご希望のメニューを選択してください。</td>
        </tr>
        -->
        <tr>
            <td valign="top">
                <table border="0" cellspacing="1" cellpadding="5">
                    <!-- ログインユーザによって表示するメニューを変更 -->
                    <!-- 体験版は常に表示 -->
                    <!-- 2020/02/04 QQ)Ooseto 共通テスト対応 DEL START -->
                    <!--
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'taiken_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/taiken_i2.gif');return true" onmouseout="changeImages( /*CMP*/'taiken_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/taiken_i.gif');return true" href="javascript:submitForm('w008')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/taiken_i.gif" alt="Kei-Navi体験版" name="taiken_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="javascript:submitForm('w008')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/taiken_mo.gif" alt="Kei-Navi体験版" height="22" width="160" border="0"></a></td>
                    </tr>
                    -->
                    <!-- 2020/02/04 QQ)Ooseto 共通テスト対応 DEL END -->
                    <%-- 模試受験校の場合のみ表示(証明書の有無は関係なし) --%>
                    <c:if test="${(LoginSession.pactDiv == 1 or LoginSession.pactDiv == 2)}">
                    <tr valign="middle">
                        <!-- ＰＤＦ提供まで一時的にクローズ
                        <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_i.gif" alt="成績統計資料集ダウンロード" name="seiseki_i3" height="60" width="77" border="0"></td>
                        <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_mo.gif" alt="成績統計資料集ダウンロード" height="23" width="296" border="0"></td>
                        -->
                        <td><a onmouseover="changeImages( /*CMP*/'seiseki_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_i2.gif');return true" onmouseout="changeImages( /*CMP*/'seiseki_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_i.gif');return true" href="javascript:submitForm('f002')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_i.gif" alt="成績統計資料集ダウンロード" name="seiseki_i3" height="60" width="77" border="0"></a></td>
                        <td>
						<table border="0" cellspacing="2" cellpadding="2">
							<tr><td><a href="javascript:submitForm('f002')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/seiseki_mo.gif" alt="成績統計資料集ダウンロード" height="23" width="296" border="0"></a></td></tr>
                    		<%--<tr><td><a href="/private/meg/moshidata/index.html#seiseki" target="_blank">全統模試総合成績(速報)</a></td></tr>--%>
						</table>
						</td>
                    </tr>
                    </c:if>
                    <!-- 大学入学共通テスト分析データ集ダウンロード -->
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'center_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i2.gif');return true" onmouseout="changeImages( /*CMP*/'center_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i.gif');return true" href="javascript:submitForm('f003')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i.gif" alt="大学入学共通テスト分析データ集ダウンロード" name="center_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="javascript:submitForm('f003')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_mo.gif" alt="大学入学共通テスト分析データ集ダウンロード" height="57" width="280" border="0"></a></td>
                    </tr>
                    <!-- 大学入学共通テスト分析データ集ダウンロード(BANZAIへのリンク)
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'center_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i2.gif');return true" onmouseout="changeImages( /*CMP*/'center_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i.gif');return true" href="https://banzai-pm.keinet.ne.jp/dl_data/f003.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_i.gif" alt="大学入学共通テスト分析データ集ダウンロード" name="center_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="https://banzai-pm.keinet.ne.jp/dl_data/f003.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/center_mo.gif" alt="大学入学共通テスト分析データ集ダウンロード" height="57" width="280" border="0"></a></td>
                    </tr>
                    -->
                    <%-- Kei-Navi非契約校及びリサーチ参加校(ともに証明書あり)で、高校別データダウンロード機能使用提供が必要な場合のみ表示 --%>
                    <%-- 2004.12.09 プライバシー保護学校も対象外とする --%>
                    <c:if test="${(LoginSession.pactDiv <= 3) and LoginSession.featprovMode == 1 and LoginSession.cert and not LoginSession.privacyProtection }">
                    <c:if test="${ LoginSession.pact or (not LoginSession.pact and LoginSession.knFunctionFlag == 1) }">
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'shisho_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shisho_i2.gif');return true" onmouseout="changeImages( /*CMP*/'shisho_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shisho_i.gif');return true" href="javascript:submitForm('f004')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shisho_i.gif" alt="高校別データダウンロード" name="shisho_i3" height="60" width="77" border="0"></a></td>
                        <td>
						<table border="0" cellspacing="2" cellpadding="2">
							<tr><td><a href="javascript:submitForm('f004')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shisho_mo.gif" alt="高校別データダウンロード" height="23" width="272" border="0"></a></td></tr>
						</table>
						</td>
                    </tr>
                    </c:if>
                    </c:if>
                    <%-- 模試受験校の場合のみ表示(証明書の有無は関係なし) --%>
                    <!-- Kei-Net入試情報
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'nyushi_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/nyushi_i2.gif');return true" onmouseout="changeImages( /*CMP*/'nyushi_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/nyushi_i.gif');return true" href="http://www.keinet.ne.jp/meg/index.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/nyushi_i.gif" alt="Kei-Net入試情報" name="nyushi_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="http://www.keinet.ne.jp/meg/index.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/nyushi_mo.gif" alt="Kei-Net入試情報" height="25" width="170" border="0"></a></td>
                    </tr>
                    -->
                    <!-- 河合塾 研究会・分析報告会 -->
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'kenkyu_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/kenkyu_i2.gif');return true" onmouseout="changeImages( /*CMP*/'kenkyu_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/kenkyu_i.gif');return true" href="/private/meg/kenkyu/" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/kenkyu_i.gif" alt="河合塾 研究会・分析報告会" name="kenkyu_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="/private/meg/kenkyu/" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/kenkyu_mo.gif" alt="河合塾 研究会・分析報告会" height="23" width="291" border="0"></a></td>
                    </tr>
                    <!-- 全統模試集冊版 -->
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'shusatsu_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shusatsu_i2.gif');return true" onmouseout="changeImages( /*CMP*/'shusatsu_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shusatsu_i.gif');return true" href="/private/meg/moshi_bind/index_dl.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shusatsu_i.gif" alt="全統模試集冊版" name="shusatsu_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="/private/meg/moshi_bind/index_dl.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/shusatsu_mo.gif" alt="全統模試集冊版" height="25" width="163" border="0"></a></td>
                    </tr>
                    <!-- バンザイシステムサポート -->
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'banzaisp_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/banzaisp_i2.jpg');return true" onmouseout="changeImages( /*CMP*/'banzaisp_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/banzaisp_i.jpg');return true" href="/public/banzai/index.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/banzaisp_i.jpg" alt="バンザイシステムサポート" name="banzaisp_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="/public/banzai/index.html" target="_blank"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/hanteisp_ba.png" alt="バンザイシステムサポート" height="30" width="289" border="0"></a></td>
                    </tr>
                    <!-- 模試受付システム -->
                    <c:if test="${LoginSession.cert}">
                    <c:if test="${ LoginSession.pact or (not LoginSession.pact and LoginSession.scFunctionFlag == 1) }">
                    <tr valign="middle">
                        <td><a onmouseover="changeImages( /*CMP*/'moshi_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/moushikomid2.gif');return true" onmouseout="changeImages( /*CMP*/'moshi_i3',/*URL*/'https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/moushikomid1.gif');return true" href="javascript:submitForm1()"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/moushikomid1.gif" alt="模試受付システム" name="moshi_i3" height="60" width="77" border="0"></a></td>
                        <td><a href="javascript:submitForm1()"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/f001img/moshi_mo.gif" alt="模試受付システム" border="0"></a></td>
                    </tr>
                    </c:if>
                    </c:if>
                </table>
            </td>
            <!-- 2007年度改修A項目対応につき追加 -->
	         <td  valign="top">
            <table border="0" cellspacing="3" cellpadding="3">
              <tr><td>
				<iframe src="./mainte_html/DLService.html" width="470" height="450" marginwidth="5" frameborder="0"></iframe>
              </td></tr>
            </table>
            </td>

        </tr>
    </table>
    <br>
    </td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/com_bk_r.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/com_bk_d.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
<form action="" method="POST">
<input type="hidden" name="sciv" value="">
<input type="hidden" name="scid" value="">
<input type="hidden" name="scnm" value="">
</form>
</body>
</html>

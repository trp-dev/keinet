<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<fmt:setBundle basename="jp.co.fj.freemenu.resources.banzai" var="bundle" />
<c:set var="begin"><fmt:message key="banzai.begin" bundle="${bundle}" /></c:set>
<c:set var="end"><fmt:message key="banzai.end" bundle="${bundle}" /></c:set>
<c:set var="crBegin"><fmt:message key="cr.begin" bundle="${bundle}" /></c:set>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyyMMddHHmmss" var="current" />
<c:if test="${ current >= begin && current < end }">
<tr>
  <td>
                【バンザイシステム】<br>
    <table width="590" border="0" cellspacing="1" cellpadding="5" bgcolor="#ff7f50">
      <tr align="center" valign="middle" bgcolor="#ffe4c4">
        <td width="60%"><b>バンザイシステム</b></td>
        <td width="40%" align="center"><b>掲載期間</b></td>
      </tr>
      <tr bgcolor="white">
        <td width="60%">
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td width="70%">パソコン(Windows)版　プログラム</td>
              <td align="center" width="30%">
                <a href="centerResearch/banP14.exe" onmouseover="changeImages('icon1', 'freemenu/f005img/dl_ico_2.gif');return true" onmouseout="changeImages('icon1', 'freemenu/f005img/dl_ico_1.gif');return true" ><img src="freemenu/f005img/dl_ico_1.gif" alt="" name="icon1" height="18" width="39" border="0"></a>
              </td>
            </tr>
          </table>
        </td>
        <td align="center" width="40%">2013/12/20(金)〜2014/2/3(月)</td>
      </tr>
      <tr bgcolor="white">
        <td width="60%">
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td width="70%">マニュアル<BR>(セットアップ・基本操作・<BR>&nbsp;ご利用ガイド(担任編))</td>
              <td align="center" width="30%">
                <a href="centerResearch/banM14.exe" onmouseover="changeImages('icon2', 'freemenu/f005img/dl_ico_2.gif');return true" onmouseout="changeImages('icon2', 'freemenu/f005img/dl_ico_1.gif');return true" ><img src="freemenu/f005img/dl_ico_1.gif" alt="" name="icon2" height="18" width="39" border="0"></a>
              </td>
            </tr>
          </table>
        </td>
        <td align="center" width="40%">2013/12/20(金)〜2014/2/3(月)</td>
      </tr>

      <c:if test="${ current >= crBegin }">
      <tr bgcolor="white">
        <td width="60%">
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td width="70%" valign="middle">大学マスタ
              </td>
              <td align="center" width="30%">
               <a href="centerResearch/MSTDAT14.ExE" onmouseover="changeImages('icon3', 'freemenu/f005img/dl_ico_2.gif');return true" onmouseout="changeImages('icon3', 'freemenu/f005img/dl_ico_1.gif');return true" ><img src="freemenu/f005img/dl_ico_1.gif" alt="" name="icon3" height="18" width="39" border="0"></a>
              </td>
            </tr>
          </table>
        </td>
        <td align="center" width="40%">2014/1/22(水)〜2014/2/3(月)</td>
      </tr>
      </c:if>

      </table>
    <br>
    <div align="center">
      <table border="0" cellspacing="1" cellpadding="10" bgcolor="#ff7f50" width="500">
        <tr align="left" valign="middle" bgcolor="#ffe4c4">
          <td>
                <table  border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td valign="top"><span class="text12" valign="top"></span></td>
                    <td>
                      <span class="text12" style="line-height:120%">◎ダウンロード圧縮ファイルのファイル名とサイズは以下の通りです。</span><BR>
                      <span class="text12" style="line-height:120%">&nbsp;&nbsp; デスクトップなどに解凍してご利用ください。</span><BR>
                      <span class="text12" style="line-height:100%"><BR></span>
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td colspan="2"><span class="text12" style="line-height:120%">■パソコン(Windows)版 プログラム</span></td>
                        </tr>
                        <tr>
                          <td colspan="2"><span class="text12" style="line-height:120%"> &nbsp;&nbsp; [ファイル名] banP14.exe &nbsp;&nbsp; [サイズ] 約38.3MB</span></td>
                        </tr>
                      </table>
                      <span class="text12" style="line-height:100%"><BR></span>
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td colspan="2"><span class="text12" style="line-height:120%">■マニュアル（セットアップ・基本操作・ご利用ガイド（担任編））</span></td>
                        </tr>
                        <tr>
                          <td colspan="2">
                             <span class="text12" style="line-height:120%"> &nbsp;&nbsp; [ファイル名] banM14.exe &nbsp;&nbsp; [サイズ] 約6.32MB</span><BR>
                             <span class="text12" style="line-height:120%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ※解凍後に作成されるファイル名は以下の通りです。</span><BR>
                             <span class="text12" style="line-height:120%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ・セットアップマニュアル &nbsp;&nbsp; [ファイル名] banM14a.pdf &nbsp;&nbsp; [サイズ] 約3.16MB</span><BR>
                             <span class="text12" style="line-height:120%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ・基本操作マニュアル &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [ファイル名] banM14b.pdf &nbsp;&nbsp; [サイズ] 約2.82MB</span><BR>
                             <span class="text12" style="line-height:120%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ・ご利用ガイド（担任編） &nbsp;&nbsp;&nbsp; [ファイル名] banM14c.pdf &nbsp;&nbsp; [サイズ] 約0.99MB</span><BR>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
          </td>
        </tr>
      </table>
    </div>
  </td>
</tr>
</c:if>

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--無料ダウンロードメニューinclude用jsp-->

<noscript><link rel="stylesheet" type="text/css" href="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/style/stylesheet.css"></noscript>

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/right_sub_ttl_dl_menu.gif" width="180" height="31" border="0" alt="無料ダウンロードメニューメニュー"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="180">
<tr valign="top">
<td width="178" bgcolor="#FAFAFA">

<script Language="javascript">
<!--

	function submitFreemenu(forward) {
		document.forms[0].forward.value = forward;
		document.forms[0].submit();
	}
// -->
</script>
<!--
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

-->
<!--リンク-->
<table border="0" cellpadding="0" cellspacing="0" width="158">
<!--set-->
<tr valign="top">
<td width="1%"><font class="text12-hh" color="#758A98">●</font></td>
<!-- <td width="99%"><font class="text12-hh">成績統計資料集ダウンロード</font></td> -->
<td width="99%"><font class="text12-hh"><a href="javascript:submitFreemenu('f002')">成績統計資料集ダウンロード</a></font></td>
</tr>
<tr valign="top">
<td colspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<!--set-->
<tr valign="top">
<td width="1%"><font class="text12-hh" color="#758A98">●</font></td>
<!-- 機能提供なし -->
<!--<td width="99%"><font class="text12-hh">大学入学共通テスト分析データ集ダウンロード(05/01/19より)</font></td>-->

<!-- Kei-Navi機能提供 -->
<td width="99%"><font class="text12-hh"><a href="javascript:submitFreemenu('f003')">大学入学共通テスト分析データ集ダウンロード</a></font></td>

<!-- BANZAIリンク機能提供 -->
<!-- <td width="99%"><font class="text12-hh"><a href="https://banzai-pm.keinet.ne.jp/dl_data/f003.html" target="_blank">大学入学共通テスト分析データ集ダウンロード</a></font></td> -->
</tr>

  --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 営業（代行）・非契約校・プライバシー保護学校は除く --%>
<%-- 2004.12.13 Yoshimoto KAWA - TOTEC 営業部・教育委員会も除く --%>
<%-- 2005.10.07 Yoshimoto KAWA - TOTEC 管理者のみ表示するようにする --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ not LoginSession.deputy && LoginSession.pact && not LoginSession.privacyProtection && not LoginSession.board && not LoginSession.sales && LoginSession.manager }">
<tr valign="top">
<td colspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<!--set-->
<tr valign="top">
<td width="1%"><font class="text12-hh" color="#758A98">●</font></td>
<td width="99%"><font class="text12-hh"><a href="javascript:submitFreemenu('f004')">高校別データダウンロード</font></td>
</tr>
</c:if>

</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

</div>
  --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL END --%>


<%-- 2015/12/11 QQ)Nishiyama 大規模改修 ADD START --%>
<script Language="javascript">
<!--

	function submitFreemenu(forward) {
		document.forms[0].forward.value = forward;
		document.forms[0].submit();
	}
// -->
</script>

<%-- 画像サイズの設定 --%>
<c:set var="imgWidth" value="200" />
<c:if test="${ backward == 'w008' }">
  <c:set var="imgWidth" value="170" />
</c:if>


<table border="0" cellpadding="0" cellspacing="0" >

  <!--↓タイトル-->
  <tr height="26" bgcolor="#666666">
    <td align="left" valign="top">
      <div style="margin-top:5px; margin-right:5px; margin-left:5px;">
        <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/title_head_img/headImg_green.gif" width="16" height="16" border="0" alt="" align="left">
      </div>
      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt="">
      <b class="text14"><font color="#FFFFFF">ダウンロードサービス</font></b>
    </td>
  </tr>
  <!--↑タイトル-->

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--高校別データ-->
  <tr height="65">
    <td align="left">
      <c:choose>
		<%-- 営業（代行）・非契約校・プライバシー保護学校は除く --%>
		<%-- 2004.12.13 Yoshimoto KAWA - TOTEC 営業部・教育委員会も除く --%>
		<%-- 2005.10.07 Yoshimoto KAWA - TOTEC 管理者のみ表示するようにする --%>
		<%-- 2016.01.14 Hiroyuki Nishiyama - QQ 体験版も除く --%>
        <c:when test="${ not LoginSession.deputy && LoginSession.pact && not LoginSession.privacyProtection && not LoginSession.board && not LoginSession.sales && LoginSession.manager && not LoginSession.trial }">
          <a href="javascript:submitFreemenu('f004')">
            <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl.gif'" alt="高校別データ" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:when>
<%-- 2016/07/04 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
        <c:otherwise>
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl.gif" alt="高校別データ" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:otherwise>
  --%>
        <c:otherwise>
          <c:choose>
            <%-- 2016.07.04 Hiroyuki Nishiyama - QQ 営業部は無効画像を表示 --%>
            <c:when test="${ LoginSession.sales }">
              <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl_invalid.gif" alt="高校別データ" height="60" width="<c:out value="${imgWidth}" />" border="0">
            </c:when>
            <c:otherwise>
              <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_kokodata_dl.gif" alt="高校別データ" height="60" width="<c:out value="${imgWidth}" />" border="0">
            </c:otherwise>
          </c:choose>
        </c:otherwise>
<%-- 2016/07/04 QQ)Nishiyama 大規模改修 UPD END   --%>
      </c:choose>

    </td>
  </tr>

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--成績統計資料集-->
  <tr height="65">
    <td align="left">

      <c:choose>
        <c:when test="${ LoginSession.trial }" >
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_toukei_dl.gif" alt="成績統計資料集" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:when>
        <c:otherwise>
          <a href="javascript:submitFreemenu('f002')">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_toukei_dl.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_toukei_dl_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_toukei_dl.gif'" alt="成績統計資料集" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:otherwise>
      </c:choose>

    </td>
  </tr>

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--大学入試センター-->
  <tr height="65">
    <td align="left">

      <c:choose>
        <c:when test="${ LoginSession.trial }" >
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_centerTest_dl.gif" alt="大学入学共通テスト　分析データ集" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:when>
        <c:otherwise>
          <a href="javascript:submitFreemenu('f003')">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_centerTest_dl.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_centerTest_dl_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_centerTest_dl.gif'" alt="大学入学共通テスト　分析データ集" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:otherwise>
      </c:choose>

    </td>
  </tr>

  <tr height="30"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--全統模試集冊版-->
  <tr height="65">
    <td align="left">

      <c:choose>
        <c:when test="${ LoginSession.trial }" >
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_zentouMoshiSyu.gif" alt="全統模試集冊版" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:when>
        <c:otherwise>
          <a href="/private/meg/moshi_bind/ky_index.html" target="_blank">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_zentouMoshiSyu.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_zentouMoshiSyu_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_zentouMoshiSyu.gif'" alt="全統模試集冊版" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:otherwise>
      </c:choose>

    </td>
  </tr>

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--バンザイ-->
  <tr height="65">
    <td align="left">

      <c:choose>
        <c:when test="${ LoginSession.trial }" >
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_banzaiSystem.gif" alt="バンザイシステムサポート" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:when>
        <c:otherwise>
          <a href="/public/banzai/index.html" target="_blank">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_banzaiSystem.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_banzaiSystem_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_banzaiSystem.gif'" alt="バンザイシステムサポート" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:otherwise>
      </c:choose>

    </td>
  </tr>

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--デジタルカタログ-->
  <tr height="65">
    <td align="left">

      <c:choose>
        <c:when test="${ LoginSession.trial }" >
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_schedule.gif" alt="デジタルカタログ" height="60" width="<c:out value="${imgWidth}" />" border="0">
        </c:when>
        <c:otherwise>
          <a href="/public/keinavi/schedule/index.html" target="_blank">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_schedule.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_schedule_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_schedule.gif'" alt="デジタルカタログ" height="60" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:otherwise>
      </c:choose>

    </td>
  </tr>

  <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td></tr>

  <!--全統模試申込状況確認-->
  <tr height="40">
    <td align="left">

      <c:choose>
        <c:when test="${ not LoginSession.trial && not LoginSession.business && not LoginSession.deputy && not LoginSession.helpDesk }" >
          <a href="javascript:submitForm1()">
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_mousikomiKakunin.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_mousikomiKakunin_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_mousikomiKakunin.gif'" alt="全統模試申込状況確認" height="35" width="<c:out value="${imgWidth}" />" border="0">
          </a>
        </c:when>
<%-- 2016/06/24 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
        <c:otherwise>
          <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/download service/btn_mousikomiKakunin.gif" alt="全統模試申込状況確認" height="35" width="100%" border="0">
        </c:otherwise>
  --%>
<%-- 2016/06/24 QQ)Nishiyama 大規模改修 DEL END   --%>
      </c:choose>

    </td>
  </tr>

</table>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 ADD END --%>


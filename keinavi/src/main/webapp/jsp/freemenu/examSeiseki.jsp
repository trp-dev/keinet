<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.util.KNUtil" %>
<html>
<head>
<title>模試成績データダウンロード</title>
<link rel="stylesheet" type="text/css" href="<%=KNUtil.getContextPath(request)%>/freemenu/style/stylesheet.css">
<script type="text/javascript">
<!--
	var TIME_IN_SEC = 3;

	function init(){
		<c:if test="${ not CreateStatus.error }">
			<c:choose>
				<c:when test="${CreateStatus.created}">
					window.opener.download();
					window.self.close();
				</c:when>
				<c:otherwise>
					setTimeout("document.forms[0].submit()", 1000 * TIME_IN_SEC );
				</c:otherwise>
			</c:choose>
		</c:if>
	}

//-->
</script>
</head>
<body onload="init()" bgcolor="#F4EDE2" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="/jsp/freemenu/examSeiseki.jsp" />" method="POST" target="_self">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%"><tr><td align="center" valign="middle">
<c:choose>
	<c:when test="${CreateStatus.error}">
		<p>ただいま大変混み合っております。<br>しばらくしてから再度操作してください。</p>
		<p><input type="button" value="閉じる" onclick="self.close()"></p>
	</c:when>
	<c:otherwise>
		<p>ファイル作成中です。しばらくお待ちください。</p>
	</c:otherwise>
</c:choose>
</td></tr></table>
</form>
</body>
</html>

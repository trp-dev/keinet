<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<jsp:useBean id="form" scope="request" class="jp.co.fj.freemenu.forms.F011Form" />
<jsp:useBean id="safeFileBean" scope="request" class="jp.co.fj.keinavi.beans.UploadFileBean" />
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ファイルアップロード</title>
<link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">

<script type="text/javascript">
<!--

	<%@ include file="/jsp/script/timer.jsp" %>

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].submit();
	}
    function submitBack() {
    	submitForm("f010");
    }
    
    function init() {
    	startTimer();
    }

// -->
</script>

</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form style="margin:0" action="<c:url value="OpenFileList"/>" method="POST" >
<input type="hidden" name="forward" value=""/>
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />";/>
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f010_1img/fileupload_up_mi.gif" border="0" alt="ファイルアップロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
<table border="0" cellspacing="3" cellpadding="3" align="center">
<tr align="center">
<td>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td align="center">
<div>過去にアップロードされたファイルの一覧を以下に表示します。</div>
</td>
</tr>
<tr>
<td width="100%">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
<tr>
<td width="100%">

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="900" align="center">
<!--項目-->
<tr bgcolor="#CDD7DD">
<td width="13%" align="center"><span class="text12">アップロード日時</span></td>
<td width="28%" align="center"><span class="text12">ファイル種類</span></td>
<td width="21%" align="center"><span class="text12">ファイル名</span></td>
<td width="38%" align="center"><span class="text12">コメント</span></td>
</tr>
<!--/項目-->
<!--set-->
<c:forEach var="history" items="${safeFileBean.recordSet}">
<tr>
<td bgcolor="#F4E5D6" align="center"><span class="text12"><fmt:formatDate value="${history.updateDate}" pattern="yyyy/MM/dd HH:mm" /></span></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;"><span class="text12"><c:out value="${history.fileTypeName}"/></span></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;"><span class="text12"><c:out value="${history.uploadFileName}"/></span></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;"><span class="text12"><kn:pre><c:out value="${history.uploadComment}"/></kn:pre></span></td>
</tr>
</c:forEach>
<c:if test="${safeFileBean.recordCount == 0}">
<tr>
<td colspan="4" bgcolor="#F4E5D6" align="center"><span class="text12">アップロードされたファイルが存在しません。</span></td>
</tr>
</c:if>
<!--/set-->
</table>
<!--/リスト-->

</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<br>
</td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

<%--
【修正履歴】
2005.8.11 	Totec) T.Yamada 	[1]F002_5追加

--%>
<!-- f002com.jsp -->
<script type="text/javascript">
<!--

	// 模試配列（高校別成績用）
	var exam1 = new Array();
	// 模試配列（型／科目別成績用）
	var exam2 = new Array();
	// 模試配列（高校別設問別成績一覧表用）
	var exam3 = new Array();
	// 模試配列（国公立大学学部別・高校別志望状況用）
	var exam4 = new Array();
	<%--[1] add start --%>
	// 模試配列（志望大学・学部・学科別成績分布表）
	var exam5 = new Array();
	<%--[1] add end --%>

	function newImage(arg) {
		if (document.images) {
			rslt = new Image();
			rslt.src = arg;
			return rslt;
		}
	}

	function changeImages() {
		if (document.images && (preloadFlag == true)) {
			for (var i=0; i<changeImages.arguments.length; i+=2) {
				document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
			}
		}
	}

	// 模試初期表示
	function initNextF002() {
		initCombo("<c:out value="${form.nendo}" />", "<c:out value="${form.exam}" />");
		changeObject();
	}

	// 対象年度・対象模試のオブジェクト作成
	function init() {
<%
		FileExistenceBean feb = (FileExistenceBean)session.getAttribute("ExistBean");
		AllExamBean aeb = (AllExamBean)session.getAttribute("AllExamBean");
		Map m2_1Map = feb.getMexam2_1();
		Map m2_2Map = feb.getMexam2_2();
		Map m2_3Map = feb.getMexam2_3();
		Map m2_4Map = feb.getMexam2_4();
		Map m2_5Map = feb.getMexam2_5();//[1] add
		FileExistenceUtil feu = new FileExistenceUtil();
		StringUtil su = new StringUtil();
%>
		<c:forEach var="year" items="${AllExamBean.years}">
			<c:forEach var="exam" items="${AllExamBean.examMap[year]}">
			<%
				String examYear = (String) pageContext.getAttribute("year");
				String examCd = ((ExamData) pageContext.getAttribute("exam")).getExamCD();
			%>
			<%
				if (feu.isExistExam(examYear, examCd, m2_1Map)) {
			%>
					<%--[1] del start
					exam1[exam1.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>");
					[1] del end --%>
					<%--[1] add start --%>
					exam1[exam1.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>", "<c:out value="${exam.examTypeCD}" />");
					<%--[1] add end --%>   
			<%
				}
			%>
			<%
				if (feu.isExistExam(examYear, examCd, m2_2Map)) {
			%>
					<%--[1] del start
					exam2[exam2.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>");
					[1] del end --%>
					<%--[1] add start--%>
					exam2[exam2.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>", "<c:out value="${exam.examTypeCD}" />");
					<%--[1] add end --%>
			<%
				}
			%>
			<%
				if (feu.isExistExam(examYear, examCd, m2_3Map)) {
			%>
					<%--[1] del start
					exam3[exam3.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>");
					[1] del end --%>
					<%--[1] add start --%>
					exam3[exam3.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>", "<c:out value="${exam.examTypeCD}" />");
					<%--[1] add end --%>
			<%
				}
			%>
			<%
				if (feu.isExistExam(examYear, examCd, m2_4Map)) {
			%>
					<%--[1] del start
					exam4[exam4.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>");
					[1] del end --%>
					<%--[1] add start --%>
					exam4[exam4.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>", "<c:out value="${exam.examTypeCD}" />");
					<%--[1] add end --%>					
			<%
				}
			%>
			<%--[1] add start --%>
			<%
				if (feu.isExistExam(examYear, examCd, m2_5Map)) {
			%>
					exam5[exam5.length] = new Array("<c:out value="${exam.examYear}" />", "<c:out value="${exam.examCD}" />", "<%= su.editExamName(((ExamData)pageContext.getAttribute("exam")).getExamName()) %>", "<c:out value="${exam.examTypeCD}" />");
			<%
				}
			%>
			<%--[1] add end --%>
			</c:forEach>
		</c:forEach>
	}

	<%-- セレクトボックスを初期化する --%>
	function initCombo(year, examCd) {
		<%-- 模試の配列を決定 --%>
		var exam = getExamData();
		<%-- 年度のセレクトボックスを初期化する --%>
		document.forms[0].nendo.options.length = 0;
		var before = "";
		var index = 0;
		for (var i=0; i<exam.length; i++) {
			if (exam[i][0] != before) {
				document.forms[0].nendo.options[index] = new Option(exam[i][0], exam[i][0]);
				if (year == exam[i][0]) {
					document.forms[0].nendo.options[index].selected = true;
				}
				index++;
				before = exam[i][0];
			}
		}
		<%-- 模試のセレクトボックスも初期化する --%>
		initExamCombo(examCd);
	}

	<%-- 模試のセレクトボックスを初期化する --%>
	function initExamCombo(examCd) {
		<%-- 模試の配列を決定 --%>
		var exam = getExamData();
		document.forms[0].exam.options.length = 0;
		var year = document.forms[0].nendo.value;
		var index = 0;
		for (var i=0; i<exam.length; i++) {
			if (year != exam[i][0]) continue;
			document.forms[0].exam.options[index] = new Option(exam[i][2], exam[i][1]);
			if (examCd == exam[i][1]) {
				document.forms[0].exam.options[index].selected = true;
			}
			index++;
		}
		<%--[1] add start --%>
		changeObject();
		<%--[1] add end --%>
	}

	<%-- 選択されている対象データの模試配列を取得する --%>
	function getExamData() {
		// 高校別総合成績一覧表を選択
		if (document.forms[0].datatype.value == "F002_1") {
			return exam1;
		}
		// 型／科目別成績一覧表を選択
		else if (document.forms[0].datatype.value == "F002_2") {
			return exam2;
		}
		// 高校別設問別成績一覧表を選択
		else if (document.forms[0].datatype.value == "F002_3") {
			return exam3;
		}
		else if (document.forms[0].datatype.value == "F002_4") {
		// 国公立大学学部別・高校別志望状況
			return exam4;
		}
		<%--[1] add start --%>
		else if (document.forms[0].datatype.value == "F002_5") {
		// 志望大学・学部・学科別成績分布表
			return exam5;
		}
		<%--[1] add end --%>
		return new Array();
	}

	// 対象地区オブジェクトの表示／非表示
	function changeObject() {
		// 型／科目別・高校別総合成績一覧表(F002_2)、高校別設問別成績一覧表(F002_3)の場合には、
		// selectボックスを表示する
		if (document.forms[0].datatype.value == "F002_2"
				|| document.forms[0].datatype.value == "F002_3") {
			document.getElementById("tiku").style.display = "inline";
		}
		else {
			document.getElementById("tiku").style.display = "none";
		}
		<%--[1] add start --%>
		if(document.forms[0].datatype.value == 'F002_5'){
			//模試が一切ない場合は、マーク用を表示する
			var examTypeCd = '01';
			if(exam5.length != 0){			
				for (var i=0; i<exam5.length; i++) {
					if (document.forms[0].nendo.value == exam5[i][0] && document.forms[0].exam.value == exam5[i][1]) {
						examTypeCd = exam5[i][3];
						break;
					}
				}
			}
			createPPList(examTypeCd);
			document.getElementById('divisionCD').style.display = 'inline';
		}else{
			document.getElementById('divisionCD').style.display = 'none';
		}
		<%--[1] add end --%>
	}
	<%--[1] add start--%>
	/**
	 * "F002_5"(志望大学・学部・学科別成績分布表)選択時に
	 * 公私区分リストを表示する
	 */
	function createPPList(examTypeCd){
		//以前に作成したリストをクリアする
		document.forms[0].divisionCD.options.length = 0;
		//リストを作成する
		switch(examTypeCd){
			case '02':
			case '03':
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('国公立', '06');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('私立', '07');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('短大その他', '08');
				break;		
			default:
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('国公立', '01');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('センター私大', '02');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('センター短大', '03');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('私立', '04');
				document.forms[0].divisionCD.options[document.forms[0].divisionCD.options.length] = new Option('短大その他', '05');
				break;
		}
		for(var i=0; i<document.forms[0].divisionCD.options.length; i++){
			if("<c:out value="${form.divisionCD}" />" == document.forms[0].divisionCD.options[i].value){
				document.forms[0].divisionCD.options[i].selected = true;
			}
		}
	}
	<%--[1] add end--%>
//-->
</script>

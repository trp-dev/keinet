<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<a name="top"></a>
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr>
<td width="17" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="195" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/header_logo.gif" width="195" height="52" border="0" alt="Kei-Navi　(Kei-Net Step Up Navigator)"><br></td>
<td width="580" align="center" rowspan="2"><b class="text12" style="color:#626261"><c:out value="${LoginSession.userName}"/></b></td>
<td width="195" align="right"><div style="margin-top:8px;"><a href="<c:url value="Logout" />?forward=logout&backward=<c:out value="${ param.forward }" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></div></td>
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td align="right"><span style="font-size:8pt;line-height:120%;">※終了時は、ブラウザの「×」ではなく、<br>[終了]ボタンをクリックしてください。</span></td>
</tr>
</table>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<form action="<c:url value="UploadSuccess"/>" method="POST">

<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／入試結果調査データアップロード</title>
<link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">

<script type="text/javascript">
<!--

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

// -->
</script>

</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f008_9img/nyushidata_up_mi.gif" width="313" height="21" border="0" alt="入試結果調査データアップロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
    <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr align="center">
            <td><img src="freemenu/f008_9img/kanryo_mo.gif" alt="" height="23" width="298" border="0"></td>
        </tr>
        <tr align="center">
            <td><br>
                入試結果調査データの登録が完了しました。<br>
                <br>
            </td>
        </tr>
        <tr align="center">
            <td><input type="button" name="backpostmenu" onClick="javascript:submitData()" value="高校別データダウンロードサブメニューへ戻る"></td>
        </tr>
    </table>
    <br>
</td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<% /*隠しオブジェクト*/ %>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<script Language="javascript">
<!--
    function submitBack() {
        document.forms[0].forward.value = "f007";
        document.forms[0].backward.value = "f008";
        document.forms[0].submit();
    }
    
    function submitData() {
        document.forms[0].forward.value = "f004";
        document.forms[0].backward.value = "f008";
        document.forms[0].submit();
    }
// -->
</script>

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

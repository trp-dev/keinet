<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／大学入学共通テスト分析データ集ダウンロード</title>
        <link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">

<script type="text/javascript">
<!--

	<%@ include file="/jsp/script/timer.jsp" %>

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>

	function init() {
		startTimer();
	}

// -->
</script>

</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="" method="POST">
<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->

<script type="text/javascript"><!--
function newImage(arg) {
    if (document.images) {
        rslt = new Image();
        rslt.src = arg;
        return rslt;
    }
}
function changeImages() {
    if (document.images && (preloadFlag == true)) {
        for (var i=0; i<changeImages.arguments.length; i+=2) {
            document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
        }
    }
}

// --></script>
<script type="text/javascript">
<!--
    var preloadFlag = true;
    function preloadImages() {
        if (document.images) {
            over_pdficon_1 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_2 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_3 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_4 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_5 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_6 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_7 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_8 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_9 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_10 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_11 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            over_pdficon_12 = newImage(/*URL*/'freemenu/f002img/pdficon_3.gif');
            preloadFlag = true;
        }
    }

    function ExecDownloadPdf(decnendo, divisionCD, areaCD ) {
        var url = "";
        url = "<c:url value="FileDownload" />";
        url = url + "?flag=f003_1";
        url = url + "&type=pdf";
        url = url + "&decnendo=" + decnendo;
        url = url + "&divisionCD=" + divisionCD;
        url = url + "&areaCD=" + areaCD;
        url = url + "&forward=f003_1";
        url = url + "&backward=f003_1";
        document.forms[0].action = url;
        document.forms[0].submit();
    }
// -->
</script>

<% /*隠しオブジェクト*/ %>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f003img/center_mi.gif" width="460" height="21" border="0" alt="大学入学共通テスト分析データ集ダウンロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
    <table border="0" cellspacing="3" cellpadding="3" align="center">
        <tr align="center">
            <td>
                <table border="0" cellspacing="0" cellpadding="2">
                    <tr>
                        <td nowrap width="70">国私区分</td>
                        <td align="center" width="12">：</td>
                        <td width="100">
                            <select name="division" size="1">
                                <option value="01"<c:if test="${form.division == '01'}">selected</c:if>>国公立</option>
                                <option value="02"<c:if test="${form.division == '02'}">selected</c:if>>私立</option>
                                <option value="03"<c:if test="${form.division == '03'}">selected</c:if>>短大</option>
                            </select>
                        </td>
                        <td align="center" valign="middle" width="150">
                            <a href="javascript:submitData()"><img src="freemenu/f003img/hyouji_w.gif" alt="" height="35" width="114" border="0"></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td><br>
                <table border="0" cellspacing="1" cellpadding="5" bgcolor="#ff7f50">
                    <tr align="center" valign="middle" bgcolor="#ffe4c4">
                        <td width="100"><b>地区</b></td>
                        <td width="75"><b>ＰＤＦ</b></td>
                    </tr>
                    <c:set var="cnt" value="0"/>
                    <c:choose>
                    <c:when test="${form.division == '01' || form.division == '02'}" >
                        <c:forEach var="data" items="${AreaBean.areaData}" varStatus="status">
                            <tr bgcolor="white">
                            <td align="center" width="100"><c:out value="${data[1]}" /></td>
                            <td align="center" valign="middle" width="75">
                                <c:if test="${FileExist[status.index][0]}"><a href="javascript:ExecDownloadPdf('<c:out value="${form.targetYear}"/>','<c:out value="${form.division}"/>','<c:out value="${data[0]}"/>')" onmouseover="changeImages( /*CMP*/'pdficon_<c:out value="${status.index + 1}"/>',/*URL*/'freemenu/f002img/pdficon_3.gif');return true" onmouseout="changeImages( /*CMP*/'pdficon_<c:out value="${status.index + 1}"/>',/*URL*/'freemenu/f002img/pdficon_2.gif');return true" href="(EmptyReference!)"><img src="freemenu/f002img/pdficon_2.gif" alt="" name="pdficon_<c:out value="${status.index + 1}"/>" height="23" width="65" border="0"></a></c:if>
                            </td>
                            </tr>
                            <c:set var="cnt" value="${status.index}"/>
                        </c:forEach>
                        
                        <% //全国版圧縮版の地区コードは「00」 %>
                        <tr bgcolor="white">
                            <td align="center" width="100">全地区圧縮版</td>
                            <td align="center" valign="middle" width="75">
                                <c:if test="${FileExist[cnt+1][0]}"><a href="javascript:ExecDownloadPdf('<c:out value="${form.targetYear}"/>','<c:out value="${form.division}"/>','00')" onmouseover="changeImages( /*CMP*/'pdficon_12',/*URL*/'freemenu/f002img/pdficon_3.gif');return true" onmouseout="changeImages( /*CMP*/'pdficon_12',/*URL*/'freemenu/f002img/pdficon_2.gif');return true"><img src="freemenu/f002img/pdficon_2.gif" alt="" name="pdficon_12" height="23" width="65" border="0"></a></c:if>
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach var="a" items="${AreaBean.areaData}" varStatus="status">
                            <c:set var="cnt" value="${status.index}"/>
                        </c:forEach>
                        <% //全国版の地区コードは「00」 %>
                        <tr bgcolor="white">
                            <td align="center" width="100">全国</td>
                            <td align="center" valign="middle" width="75">
                                <c:if test="${FileExist[cnt+1][0]}"><a href="javascript:ExecDownloadPdf('<c:out value="${form.targetYear}"/>','<c:out value="${form.division}"/>','00')" onmouseover="changeImages( /*CMP*/'pdficon_12',/*URL*/'freemenu/f002img/pdficon_3.gif');return true" onmouseout="changeImages( /*CMP*/'pdficon_12',/*URL*/'freemenu/f002img/pdficon_2.gif');return true"><img src="freemenu/f002img/pdficon_2.gif" alt="" name="pdficon_12" height="23" width="65" border="0"></a></c:if>
                            </td>
                        </tr>
                    </c:otherwise>
                    </c:choose>
                </table>
                <br>
            </td>
        </tr>
    </table>
    <br>
</td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<script Language="javascript">
<!--
    // submit
    function submitData() {
        document.forms[0].action = "<c:url value="AnalysisDataDownload" />";
        document.forms[0].forward.value = "f003_1";
        document.forms[0].backward.value = "f003_1";
        document.forms[0].submit();
    }

    function submitBack() {
<%
    if (session.getAttribute("FromTopMenu") == null) {
%>
        document.forms[0].action = "<c:url value="AnalysisDataDownload1" />";
        document.forms[0].forward.value = "f001";
<%
    }
    else {
%>
        document.forms[0].action = "<c:url value="AnalysisDataDownload1" />";
        document.forms[0].forward.value = "<%=(String)session.getAttribute("FromTopMenu")%>";
<%
    }
%>
        document.forms[0].backward.value = "f003_1";
        document.forms[0].submit();
    }
// -->
</script>

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/freemenu/footer.jsp" %>
<!--/FOOTER-->

</form>
</body>
</html>

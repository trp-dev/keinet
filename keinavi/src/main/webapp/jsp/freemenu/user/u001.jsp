<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／利用者管理</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript">
<!--

	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- フォーム操作クラス --%>
	var util = new FormUtil();

	<%-- 利用者編集 --%>
	function submitEdit(id) {
		document.forms[0].targetLoginId.value = id;
		submitMenu('u002_dl');
	}

	<%-- 利用者参照編集 --%>
	function submitRef(id) {
		document.forms[0].targetLoginId.value = id;
		submitMenu('u004_dl');
	}

	<%-- パスワード初期化 --%>
	function submitInitPassword(id) {
		if (confirm("<kn:message id="u002c" />")) {
			document.forms[0].actionMode.value = "2";
			document.forms[0].targetLoginId.value = id;
			submitMenu('u001_dl');
		}
	}

	<%-- 状態変更 --%>
	function submitSwitch(id, mode) {
		document.forms[0].forward.value = "u001_dl";
		document.forms[0].backward.value = "u001_dl";
		document.forms[0].actionMode.value = mode;
		document.forms[0].targetLoginId.value = id;
		document.forms[0].scrollX.value = document.body.scrollLeft;
		document.forms[0].scrollY.value = document.body.scrollTop;
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	function init() {
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U001DLServlet" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="u001_dl">
<input type="hidden" name="actionMode" value="">
<input type="hidden" name="targetLoginId" value="">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="scrollX" value="<c:out value="${param.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${param.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br>
		</td>
		<td width="8" background="./shared_lib/img/parts/com_bk_l.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br>
		</td>
		<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help01.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top">
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
		<td width="908">


<!--タイトル-->
<div style="margin-top:23px;">
	<table border="0" cellpadding="0" cellspacing="0" width="908">
		<tr>
			<td width="174">
				<img src="./user/img/ttl_user.gif" width="128" height="24" border="0" alt="利用者管理"><br>
			</td>
			<td width="1" bgcolor="#657681">
				<img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br>
			</td>
			<td width="10">
				<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br>
			</td>
			<td width="723">
				<img src="./user/img/ttl_user_read.gif" width="364" height="16" border="0" alt="利用者情報を一元的に管理することができます。"><br>
			</td>
		</tr>
	</table>
</div>
<!--/タイトル-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->


<!--タイトルバーkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td width="2" bgcolor="#8CA9BB">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br>
		</td>
		<td width="892">
			<table border="0" cellpadding="0" cellspacing="0" width="892">
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr>
					<td width="5" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br>
					</td>
					<td width="7" bgcolor="#FF8F0E">
						<img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br>
					</td>
					<td width="4" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br>
					</td>
					<td width="576" bgcolor="#758A98">
						<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
						<b class="text16" style="color:#FFFFFF;">利用者一覧</b>
					</td>
					<td width="300" bgcolor="#758A98" align="right"><br></td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
			</table>
		</td>
		<td width="14">
			<img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/タイトルバーkkk-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="18"><br></td>
<td width="890">

<!--現在の利用者はテキストkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="890">
	<tr valign="top">
		<td bgcolor="#FFFFFF">
			<table border="0" cellpadding="6" cellspacing="1" width="880">
				<tr valign="top">
					<td bgcolor="#FFFFFF" align="left">
						<span class="text14-hh">現在の利用者は以下の通りです。先頭に管理者ＩＤを表示しています。<br></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--/現在の利用者はテキストkkk-->

<table border="0" cellpadding="0" cellspacing="5" width="890">
	<tr>
		<td width="110"></td>
		<td width="110"></td>
		<td width="760" align="right"><span class="text14-hh">○：可　　×：不可</span></td>
	</tr>
</table>

</td>
</tr>
<table>

<!--ユーザー 一覧-->
<TABLE border="0" cellspacing="2" cellpadding="4">
	<TR bgcolor="#E1E6EB">
		<TH nowrap rowspan="3"><b class="text12">状態</b></TH>
		<TH nowrap rowspan="3"><b class="text12">切替</b></TH>
		<TH nowrap rowspan="3"><b class="text12">利用者ID</b></TH>
		<TH nowrap rowspan="3" align="center"><b class="text12">利用者名</b></TH>
		<TH nowrap rowspan="3"><b class="text12">初期PW</b></TH>
		<TH nowrap rowspan="3"><b class="text12">初期PW<br>変更</b></TH>
		<TH height="40" nowrap colspan="2"><b class="text12">機能権限</b></TH>
		<TH nowrap rowspan="3" colspan="2"><span class="text12"><br></span></TH>
	</TR>
	<TR bgcolor="#E1E6EB">
		<TH height="40" nowrap colspan="2"><b class="text12">Kei-Navi<br />ダウンロードサービス</b></TH>
	</TR>
	<TR bgcolor="#E1E6EB">
		<TH height="40"><b class="text12">高校別データ<br />ダウンロード</b></TH>
		<TH><b class="text12">全統模試申込状況確認・<br />センター試験問題集申込</b></TH>
	</TR>

	<c:forEach var="user" items="${ LoginUserBean.loginUserList }">
	<c:choose>
	<c:when test="${ user.manager }">
	<TR bgcolor="#F4E5D6">
		<TD width="35" height="40" align="center"></TD>
		<TD width="100" align="center"></TD>
		<TD width="70" align="center"><font color="Red"><b class="text12"><c:out value="${ user.loginId }" /></b></font></TD>
		<TD width="120"><font color="Red"><b class="text12">システム管理者</b></font></TD>
		<TD width="70" align="center"><font color="Red"><b class="text12"><c:out value="${ user.defaultPwd }" /></b></font></TD>
		<TD width="60" align="center"><font color="Red"><b class="text12"><c:choose><c:when test="${ user.changedPwd }">有</c:when><c:otherwise>無</c:otherwise></c:choose></b></font></TD>
		<TD width="115" align="center"><font color="Red"><b class="text12">○</b></font></TD>
		<TD width="159" align="center"><font color="Red"><b class="text12">○</b></font></TD>
		<TD width="42" align="center"><a href="javascript:submitRef('<c:out value="${ user.loginId }" />')"><span class="text12">通知書</span></a></TD>
		<TD width="35" align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD>
	</TR>
	</c:when>
	<c:otherwise>
	<TR bgcolor="<c:choose><c:when test="${ user.maintainer }">#F4E5D6</c:when><c:otherwise>#DDDDDD</c:otherwise></c:choose>">
		<TD height="40" rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.maintainer }">有効</c:when><c:otherwise>無効</c:otherwise></c:choose></span></TD>
		<TD rowspan="2" align="center"><c:choose><c:when test="${ user.maintainer }"><input type="button" value="無効にする" class="text12" onclick="submitSwitch('<c:out value="${ user.loginId }" />', '4');"></c:when><c:otherwise><input type="button" value="有効にする" class="text12" onclick="submitSwitch('<c:out value="${ user.loginId }" />', '3');"></c:otherwise></c:choose></TD>
		<TD rowspan="2" align="center"><span class="text12"><c:out value="${ user.loginId }" /></span></TD>
		<TD rowspan="2"><span class="text12"><c:out value="${ user.loginName }" /></span></TD>
		<TD rowspan="2" align="center"><span class="text12"><c:out value="${ user.defaultPwd }" /></span></TD>
		<TD rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.changedPwd }">有</c:when><c:otherwise>無</c:otherwise></c:choose></span></TD>
		<TD rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.knFunctionFlag == 1 }">○</c:when><c:when test="${ user.knFunctionFlag == 2 }">▲</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>
		<TD rowspan="2" align="center"><span class="text12"><c:choose><c:when test="${ user.scFunctionFlag == 1 }">○</c:when><c:when test="${ user.scFunctionFlag == 2 }">▲</c:when><c:otherwise>×</c:otherwise></c:choose></span></TD>
		<TD align="center"><a href="javascript:submitRef('<c:out value="${ user.loginId }" />')"><span class="text12">通知書</span></a></TD>
		<TD align="center"><a href="javascript:submitEdit('<c:out value="${ user.loginId }" />')"><span class="text12">編集</span></a></TD>
	</TR>
	<TR bgcolor="<c:choose><c:when test="${ user.maintainer }">#F4E5D6</c:when><c:otherwise>#DDDDDD</c:otherwise></c:choose>">
		<TD colspan="2" align="center"><a href="javascript:submitInitPassword('<c:out value="${ user.loginId }" />')"><span class="text12">PW初期化</span></a></TD>
	</TR>
	</c:otherwise>
	</c:choose>
	</c:forEach>
</TABLE>
<!--/ユーザー 一覧-->

<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="18"><br></td>
<td width="890">

<table border="0" cellpadding="0" cellspacing="5" width="890">
	<tr>
		<td width="110"></td>
		<td width="110"></td>
		<td width="760" align="right"><span class="text14-hh">○：可　　×：不可</span></td>
	</tr>
</table>

</td>
</tr>
<table>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr>
		<td width="908" height="28" align="center">
			<img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br>
		</td>
	</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td bgcolor="#8CA9BB">
			<table border="0" cellpadding="6" cellspacing="1" width="908">
				<tr valign="top">
					<td width="906" bgcolor="#FBD49F" align="center">
						<input type="button" value="利用者管理完了　（メニュー画面へ）" class="text12" style="width:320px;" onclick="submitMenu('f001')">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->

		</td>
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/コンテンツ-->



		</td>
		<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_d.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/下部　ドロップシャドウ-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="997">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<table border="0" cellpadding="0" cellspacing="0" width="984">
<tr>
<td rowspan="5" width="155" align="right">
<c:if test="${ isLogin }">
<input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()">
</c:if>
</td>
<td width="829" align="right">
<!--先頭へ戻る-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#top"><img src="freemenu/img/arrow_top.gif" width="10" height="8" border="0" alt="↑" hspace="4"></a></td>
<td><span class="text12"><a href="#top">このページの先頭へ</a></span></td>
</tr>
</table>
<!--/先頭へ戻る-->
</td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><a href="<c:url value="Logout" />"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><img src="shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
<img src="freemenu/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
<!--/FOOTER-->

</form>
</body>
</html>

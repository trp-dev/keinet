<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／利用者管理</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--

	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 日本語用クラス --%>
	var jstr = new JString();
	<%-- フォーム操作クラス --%>
	var util = new FormUtil();
	<%-- 検査用クラス --%>
	var validator = new Validator();

	function switchStatus(e, flag) {

		var disabled = flag ? "disabled" : false;

		for (var i=0; i<e.length; i++) {
			if (e[i].value == 1) {
				if (flag) e[i].checked = true;
				continue;
			}
			e[i].disabled = disabled;
		}
	}

	function submitRegist() {

		var form = document.forms[0];
		var message = "";

		if (form.newLoginId) {
			var value = form.newLoginId.value;
			if (value.length == 0) {
				message += "<kn:message id="u002a" />\n";
			} else if (value.length > 8 || !validator.isAlphanumeric(value)) {
				message += "<kn:message id="u003a" />\n";
			}
		}

		jstr.setString(form.loginName.value);
		var length = jstr.getLength();
		if (length == 0) {
			message += "<kn:message id="u004a" />\n";
		} else if (length > 20) {
			message += "<kn:message id="u005a" />\n";
		}

		if (message != "") {
			alert(message);
			return;
		}

		document.forms[0].forward.value = "u004_dl";
		document.forms[0].backward.value = "u002_dl";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	function init() {
		var errorMessage = "<c:out value="${ ErrorMessage }" />";
		if (errorMessage != "") alert(errorMessage);
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="U002DLServlet" />" method="POST" onsubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />">
<input type="hidden" name="targetLoginId" value="<c:out value="${param.targetLoginId}" />">
<input type="hidden" name="authKn" value="<c:out value="${LoginUserData.knFunctionFlag}" />">
<input type="hidden" name="authSc" value="<c:out value="${LoginUserData.scFunctionFlag}" />">
<input type="hidden" name="authEe" value="<c:out value="${LoginUserData.eeFunctionFlag}" />">
<input type="hidden" name="authAb" value="<c:out value="${LoginUserData.abFunctionFlag}" />">

<c:forEach var="loginId" items="${paramValues.loginId}">
<input type="hidden" name="loginId" value="<c:out value="${loginId}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_t.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/上部　オレンジライン-->

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br>
		</td>
		<td width="8" background="./shared_lib/img/parts/com_bk_l.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br>
		</td>
		<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help01.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
	<tr valign="top">
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
		<td width="908" align="center">



<!--タイトル-->
<div style="margin-top:23px;">
	<table border="0" cellpadding="0" cellspacing="0" width="908">
		<tr>
			<td width="174">
				<img src="./user/img/ttl_user.gif" width="128" height="24" border="0" alt="利用者管理"><br>
			</td>
			<td width="1" bgcolor="#657681">
				<img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br>
			</td>
			<td width="10">
				<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br>
			</td>
			<td width="723">
				<img src="./user/img/ttl_user_read.gif" width="364" height="16" border="0" alt="利用者情報を一元的に管理することができます。"><br>
			</td>
		</tr>
	</table>
</div>
<!--/タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="17" border="0" alt="">
		</td>
	</tr>
</table>
<!--/spacer-->

<!--タイトルバーkkk-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td width="2" bgcolor="#8CA9BB">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br>
		</td>
		<td width="892">
			<table border="0" cellpadding="0" cellspacing="0" width="892">
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr>
					<td width="5" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br>
					</td>
					<td width="7" bgcolor="#FF8F0E">
						<img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br>
					</td>
					<td width="4" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br>
					</td>
					<td width="576" bgcolor="#758A98">
						<img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt="">
						<b class="text16" style="color:#FFFFFF;">利用者情報登録・編集</b>
					</td>
					<td width="300" bgcolor="#758A98" align="right"><br></td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#EFF2F3">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="3" border="0" alt=""><br>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="5" width="892" bgcolor="#8CA9BB">
						<img src="./shared_lib/img/parts/sp.gif" width="892" height="2" border="0" alt=""><br>
					</td>
				</tr>
			</table>
		</td>
		<td width="14">
			<img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br>
		</td>
	</tr>
</table>



<!--利用者情報を設定してくださいテキスト-->
<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr>
		<td align="center">
			<table border="0" cellpadding="6" cellspacing="1" width="678">
				<tr>
					<td>
						<b class="text14-hh">利用者情報を設定してください。<br></b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/利用者情報を設定してくださいテキスト-->



<!--メイン枠-->
<div style="margin-top:8px;">
<table bgcolor="#8CA9BB" border="0" cellpadding="0" cellspacing="0" width="678">
<tr>
<td>
	<table border="0" cellpadding="20" cellspacing="1" width="678">
	<tr>
	<td align="center" bgcolor="#FFFFFF" width="636">


<!--利用者ＩＤテキストボックス-->
<div>
<table border="0" cellpadding="3" cellspacing="0" width="636">
	<tr>
		<td width="75"><b class="text12">利用者ID : </b></td>
		<td width="150"><input type="text" maxlength="8" size="12" class="text12" name="newLoginId" style="width:100px;" value="<c:out value="${ LoginUserData.loginId }" />"><input type="hidden" name="originalLoginId" value="<c:out value="${ LoginUserData.loginId }" />"></td>
		<td width="250"><span class="text12">英数字8文字以内<br><b style="color:#F00">※学校IDと同一のIDは登録出来ません。</b></span></td>
		<td width="161"><br></td>
	</tr>
</table>
</div>
<!--/利用者ＩＤテキストボックス-->




<!--利用者名テキストボックス-->
<div style="margin-top:3px;">
<table border="0" cellpadding="3" cellspacing="0" width="636">
	<tr>
		<td width="75"><b class="text12">利用者名 : </b></td>
		<c:choose>
			<c:when test="${ LoginUserData.manager }">
				<td width="150"><span class="text12"><c:out value="${ LoginUserData.loginName }" /></span><input type="hidden" name="loginName" value="<c:out value="${ LoginUserData.loginName }" />"></td>
				<td width="150"><span class="text12"><br></span></td>
			</c:when>
			<c:otherwise>
				<td width="150"><input type="text" maxlength="20" size="30" class="text12" name="loginName" style="width:135px;" value="<c:out value="${ LoginUserData.loginName }" />"></td>
				<td width="150"><span class="text12">全角10文字以内</span></td>
			</c:otherwise>
		</c:choose>
		<td width="261"></td>
	</tr>
</table>
</div>
<!--/利用者名テキストボックス-->



<!--利用権限ラベル-->
<div style="margin-top:17px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td width="600"><b class="text12">利用権限 : </b></td>
	</tr>
</table>
</div>
<!--/利用権限ラベル-->



<!--機能権限テーブル-->
<div style="margin-top:3px;">
<table border="0" cellpadding="5" cellspacing="2" width="600">
	<tr align="center">
		<td width="100" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">機能権限</b></td>
		<td width="100" bgcolor="#E1E6EB" rowspan= "2"><b class="text12">Kei-Navi<br />ダウンロード<br />サービス</b></td>
		<td width="170" bgcolor="#E1E6EB"><b class="text12">高校別データ<br />ダウンロード</b></td>
		<td bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.knFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
	</tr>
	<tr align="center">
		<td width="170" bgcolor="#E1E6EB"><b class="text12">全統模試申込状況確認・<br />センター試験問題集申込</b></td>
		<td bgcolor="#F4E5D6"><span class="text12"><c:choose><c:when test="${ LoginUserData.scFunctionFlag == 1 }">可</c:when><c:otherwise>不可</c:otherwise></c:choose></span></td>
	</tr>
</table>
</div>
<!--/機能権限テーブル-->

	</td>
	</tr>
	</table>
</td>
</tr>
</table>
</div>
<!--/メイン枠-->



<!--キャンセル・登録ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="908">
	<tr valign="top">
		<td bgcolor="#FFFFFFF" width="908" align="center">
			<table border="0" cellpadding="0" cellspacing="0" width="678">
				<td bgcolor="#8CA9BB">
					<table border="0" cellpadding="6" cellspacing="1" width="678">
						<tr>
							<td bgcolor="#FBD49F" align="center">
								<table border="0" cellpadding="5" cellspacing="0">
									<tr>
										<td>
											<input type="button" value="キャンセル" class="text12" style="width:120px;" onclick="submitMenu('u001_dl')">
										</td>
										<td><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""></td>
										<td>
											<input type="button" value="登録" class="text12" style="width:120px;" onclick="submitRegist()">
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</table>
		</td>
	</tr>
</table>
</div>
<!--/キャンセル・登録ボタン-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td width="100%">
			<img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->



		</td>
		<td width="30">
			<img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/コンテンツ-->





		</td>
		<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="12">
			<img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br>
		</td>
		<td width="12">
			<img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br>
		</td>
		<td width="960" background="./shared_lib/img/parts/com_bk_d.gif">
			<img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br>
		</td>
		<td width="13">
			<img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/下部　ドロップシャドウ-->



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
	<tr valign="top">
		<td width="997">
			<img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br>
		</td>
	</tr>
</table>
<!--/spacer-->


<!--FOOTER-->
<table border="0" cellpadding="0" cellspacing="0" width="984">
<tr>
<td rowspan="5" width="155" align="right">
<c:if test="${ isLogin }">
<input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()">
</c:if>
</td>
<td width="829" align="right">
<!--先頭へ戻る-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#top"><img src="freemenu/img/arrow_top.gif" width="10" height="8" border="0" alt="↑" hspace="4"></a></td>
<td><span class="text12"><a href="#top">このページの先頭へ</a></span></td>
</tr>
</table>
<!--/先頭へ戻る-->
</td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><a href="<c:url value="Logout" />"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><img src="shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
<img src="freemenu/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
<!--/FOOTER-->

</form>
</body>
</html>


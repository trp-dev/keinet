<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<c:set var="isLogin" value="${ not empty LoginSession }" />
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／成績統計資料集ダウンロード</title>
<link rel="stylesheet" type="text/css" href="freemenu/style/stylesheet.css">
<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>

	var functionMap = new Array();
	<c:forEach var="function" items="${ F002FunctionListBean.functionList }">
		functionMap["<c:out value="${ function.functionId }" />"] = "<c:out value="${ function.functionName }" />";
	</c:forEach>

	var examList = new Array();
	var examMap = new Array();
	<c:forEach var="exam" items="${ ExamList }">
		examList[examList.length] = new Array(
			"<c:out value="${exam.examYear}" />",
			"<c:out value="${exam.examCD}" />",
			"<c:out value="${exam.examName}" />",
			"<c:out value="${exam.examTypeCD}" />",
			new Array(<c:forEach var="functionId" items="${ exam.functionList }" varStatus="status"><c:if test="${ status.index > 0 }">,</c:if>"<c:out value="${ functionId }" />"</c:forEach>));
		examMap["<c:out value="${exam.examId}" />"] = examList[examList.length - 1];
	</c:forEach>

	function newImage(arg) {
		if (document.images) {
			rslt = new Image();
			rslt.src = arg;
			return rslt;
		}
	}

	function changeImages() {
		if (document.images) {
			for (var i=0; i<changeImages.arguments.length; i+=2) {
				document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
			}
		}
	}

	if (document.images) {
		newImage('freemenu/f002img/pdficon_2.gif');
		newImage('freemenu/f002img/csvicon_2.gif');
		newImage('freemenu/f002img/pdficon_3.gif');
		newImage('freemenu/f002img/csvicon_3.gif');
	}

	<%@ include file="/jsp/script/submit_help.jsp" %>

	function submitBack() {
		<c:choose>
		<c:when test="${ empty FromTopMenu }">
			document.forms[0].forward.value = "f001";
		</c:when>
		<c:otherwise>
			document.forms[0].forward.value = "<c:out value="${ FromTopMenu }" />";
		</c:otherwise>
		</c:choose>
		document.forms[0].submit();
	}

	function submitData() {
		if (document.forms[0].targetYear.length == 0) {
			alert("模試が存在しません。");
		} else {
			document.forms[0].forward.value = "f002";
			document.forms[0].submit();
		}
	}

	function submitDownload(distCd, subCd) {
		if (distCd) document.forms[0].downDistCd.value = distCd;
		if (subCd) document.forms[0].downSubCd.value = subCd;
		document.forms[0].forward.value = "f002_download";
		document.forms[0].submit();
	}

	function initYearBox(targetExamYear) {
		var select = document.forms[0].targetYear;
		select.options.length = 0;
		var beforeYear = "";
		for (var i = 0; i < examList.length; i++) {
			var year = examList[i][0];
			if (year != beforeYear) {
				var selected = year == targetExamYear;
				select.options[select.options.length] = new Option(year, year, selected, selected);
				beforeYear = year;
			}
		}
	}

	function initExamBox(targetExamCd) {
		var targetYear = document.forms[0].targetYear.value;
		var select = document.forms[0].targetExam;
		select.options.length = 0;
		for (var i = 0; i < examList.length; i++) {
			if (examList[i][0] == targetYear) {
				var examCd = examList[i][1];
				var selected = examCd == targetExamCd;
				select.options[select.options.length] = new Option(examList[i][2], examCd, selected, selected);
			}
		}
	}

	function initDataBox(targetDataType) {
		var select = document.forms[0].dataType;
		select.options.length = 0;
		var functionList = getSelectedExam()[4];
		for (var i = 0; i < functionList.length; i++) {
			var id = functionList[i];
			var selected = id == targetDataType;
			select.options[select.options.length] = new Option(functionMap[id], id, selected, selected);
		}
	}

	function getSelectedExam() {
		var targetYear = document.forms[0].targetYear.value;
		var targetExam = document.forms[0].targetExam.value;
		return examMap[targetYear + targetExam];
	}

	function changeDataType() {

		var dataType = document.forms[0].dataType.value;
		<%-- 以下の機能を選択しているなら地区リストボックスを表示する --%>
		<%-- F002_2：型／科目別・高校別成績一覧表 --%>
		<%-- F002_3：高校別設問別成績一覧表 --%>
		if (dataType == "F002_2" || dataType == "F002_3") {
			document.getElementById("dist").style.display = "inline";
		} else {
			document.getElementById("dist").style.display = "none";
		}

		<%-- 以下の機能を選択しているなら国私区分リストボックスを表示する --%>
		<%-- F002_5：型／科目別・高校別成績一覧表 --%>
		<%-- F002_3：高校別設問別成績一覧表 --%>
		<%-- F002_16：合格可能性評価基準一覧 --%>
		if (dataType == "F002_5" || dataType == "F002_16") {
			initUnivDivBox();
			document.getElementById("univDiv").style.display = 'inline';
		}else{
			document.getElementById("univDiv").style.display = 'none';
		}
	}

	function initUnivDivBox(){

		var select = document.forms[0].divisionCd;
		var dataType = document.forms[0].dataType.value;
		select.options.length = 0;
		switch (getSelectedExam()[3]) {
			case "02":
			case "03":
			case "91":
				select.options[select.options.length] = new Option("国公立", "01");
				select.options[select.options.length] = new Option("私立", "04");
				select.options[select.options.length] = new Option("短大その他", "05");
				break;
			default:
				select.options[select.options.length] = new Option("国公立", "01");
				// 2019/08/28 Hics)Ueta 共通テスト対応 UPD START
				//select.options[select.options.length] = new Option("センター私大", "02");
				//select.options[select.options.length] = new Option("センター短大", "03");
				select.options[select.options.length] = new Option("共通テスト私大", "02");
				select.options[select.options.length] = new Option("共通テスト短大", "03");
				// 2019/08/28 Hics)Ueta 共通テスト対応 UPD END
				select.options[select.options.length] = new Option("私立", "04");
				select.options[select.options.length] = new Option("短大その他", "05");
		}

		for (var i = 0; i < select.options.length; i++) {
			if ("<c:out value="${ form.divisionCd }" />" == select.options[i].value) {
				select.options[i].selected = true;
				break;
			}
		}
	}

	function init() {
		startTimer();
		initYearBox("<c:out value="${ form.targetYear }" />");
		initExamBox("<c:out value="${ form.targetExam }" />");
		initDataBox("<c:out value="${ form.dataType }" />");
		changeDataType();
	}

//-->
</script>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ResultStatisticDownload" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="f002">
<input type="hidden" name="downDataType" value="<c:out value="${ form.dataType }" />">
<input type="hidden" name="downExamYear" value="<c:out value="${ form.targetYear }" />">
<input type="hidden" name="downExamCd" value="<c:out value="${ form.targetExam }" />">
<input type="hidden" name="downDistCd" value="<c:out value="${ form.distCd }" />">
<input type="hidden" name="downSubCd" value="">
<input type="hidden" name="downDivCd" value="<c:out value="${ form.divisionCd }" />">

<!--HEADER-->
<%@ include file="/jsp/freemenu/header.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->



<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="freemenu/f002img/seiseki_mi.gif" width="271" height="21" border="0" alt="成績統計資料集ダウンロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
	<table border="0" cellspacing="3" cellpadding="3" align="center">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td nowrap width="70">対象年度</td>
						<td align="center" width="12">：</td>
						<td width="250">
							<select name="targetYear" style="width:70px;" onChange="initExamBox(document.forms[0].targetExam.value);initDataBox();changeDataType();"></select>
						</td>
						<td rowspan="3" align="center" valign="bottom" width="150"><a href="#here" onclick="submitData();"><img src="freemenu/f002img/hyouji_w.gif" alt="表示" height="35" width="114" border="0"></a></td>
						<%--<td rowspan="3" align="center" valign="bottom" width="150"><a href="#here" onClick="javascript:window.open('<c:out value="${hyo_ref}" />', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes');"><img src="freemenu/f002img/hyouji_mi.gif" alt="表の見方" height="35" width="114" border="0"></a></span></td> --%>
					</tr>
					<tr>
						<td nowrap width="70">対象模試</td>
						<td align="center" width="12">：</td>
						<td width="250">
							<select name="targetExam" style="width:220px;" onChange="initDataBox();changeDataType();"></select>
						</td>
					</tr>
					<tr>
						<td nowrap width="70">対象データ</td>
						<td align="center" width="12">：</td>
						<td width="250">
							<select name="dataType" style="width:260px;" onChange="changeDataType();"></select>
						</td>
					</tr>
					<tr id="dist" style="display:none;">
						<td nowrap width="70">対象地区</td>
						<td align="center" width="12">：</td>
						<td width="250">
							<select name="distCd">
							<c:forEach var="area" items="${ AreaList }">
							  <option value="<c:out value="${area[0]}" />"<c:if test="${ area[0] == form.distCd }"> selected</c:if>><c:out value="${ area[1] }" /></option>
							</c:forEach>
							</select>
						</td>
					</tr>
					<tr id="univDiv" style="display:none;">
						<td nowrap width="70">国私区分</td>
						<td align="center" width="12">：</td>
						<td width="250">
							<select name="divisionCd"></select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<c:choose>
			<c:when test="${ form.dataType == 'F002_1'}">
				<%@ include file="/jsp/freemenu/f002_1.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_2'}">
				<%@ include file="/jsp/freemenu/f002_2.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_3'}">
				<%@ include file="/jsp/freemenu/f002_3.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_4'}">
				<%@ include file="/jsp/freemenu/f002_4.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_5'}">
				<%@ include file="/jsp/freemenu/f002_5.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_6'}">
				<%@ include file="/jsp/freemenu/f002_6.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_7'}">
				<%@ include file="/jsp/freemenu/f002_7.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_8'}">
				<%@ include file="/jsp/freemenu/f002_8.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_9'}">
				<%@ include file="/jsp/freemenu/f002_9.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_10'}">
				<%@ include file="/jsp/freemenu/f002_10.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_11'}">
				<%@ include file="/jsp/freemenu/f002_11.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_12'}">
				<%@ include file="/jsp/freemenu/f002_12.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_13'}">
				<%@ include file="/jsp/freemenu/f002_13.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_14'}">
				<%@ include file="/jsp/freemenu/f002_14.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_15'}">
				<%@ include file="/jsp/freemenu/f002_15.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_16'}">
				<%@ include file="/jsp/freemenu/f002_16.jsp" %>
			</c:when>
			<c:when test="${ form.dataType == 'F002_17'}">
				<%@ include file="/jsp/freemenu/f002_17.jsp" %>
			</c:when>
		</c:choose>
	</table>
	<br>
</td>
</tr>
</table>

<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<table border="0" cellpadding="0" cellspacing="0" width="984">
<tr>
<td rowspan="5" width="155" align="right">
<c:if test="${ isLogin }">
<input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()">
</c:if>
</td>
<td width="829" align="right">
<!--先頭へ戻る-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="#top"><img src="freemenu/img/arrow_top.gif" width="10" height="8" border="0" alt="↑" hspace="4"></a></td>
<td><span class="text12"><a href="#top">このページの先頭へ</a></span></td>
</tr>
</table>
<!--/先頭へ戻る-->
</td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><a href="<c:url value="Logout" />"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></td>
</tr>
<tr valign="top">
<td width="829"><img src="freemenu/img/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="829" align="right"><img src="shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
<img src="freemenu/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
<!--/FOOTER-->
</form>
</body>
</html>

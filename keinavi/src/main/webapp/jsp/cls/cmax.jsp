<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="OnepointBean" scope="request" class="jp.co.fj.keinavi.beans.help.OnepointBean" />
<c:set value="class" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／クラス成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/submit_max.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/open_charge.jsp" %>
    <%@ include file="/jsp/script/validate_common.jsp" %>
    <%@ include file="/jsp/script/validate_max.jsp" %>
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/graph_checker.jsp" %>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

	// フォームユーティリティ
	var util = new FormUtil();

    function changeState(obj) {
        // 分析対象の状態によりグラフ表示対象も変化させる
        var disabled = obj.checked ? false : "disabled";
        var cur  = new DOMUtil(obj);
     	<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD START --%>
        // var checkbox = cur.getParent(2).childNodes[2].childNodes[0];
        var checkbox = cur.getParent(2).children[2].children[0];
     	<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD END   --%>
        checkbox.disabled = disabled;
        // 分析対象がチェックされていなければグラフ表示対象も外す
		<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
        //if (!obj.checked) checkbox.checked = false;
		if (obj.checked) {
			checkbox.checked = true;
		} else {
			checkbox.checked = false;
		}
		<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
    }

    function changeAllState(name) {
		var e = document.forms[0].elements[name];
		if (e == null) {
			return;
		}
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				changeState(e[i]);
			}
		} else {
			changeState(e)
		}
	}

    //型・科目別比較の際に、型・科目の設定を表示する
    function changeCompPrev( obj ) {
        // チェックされていれば表示する
        var dispYes = "none";
        var dispNo = "none";
        // 共通項目設定を利用する
        if (obj.checked) dispYes = "inline";
            else dispNo = "inline";// 共通項目設定を利用しない
        document.getElementById("CourseTypeSelect1").style.display = dispYes;
        document.getElementById("CourseTypeSelect2").style.display = dispNo;
        document.getElementById("CourseTypeSelectInfo").style.display = dispYes;
        if( obj.checked ){
            var typeDisp = document.getElementById("TypeInfo2").style.display;
            var courseDisp = document.getElementById("CourseInfo2").style.display;
            document.getElementById("SelectType").style.display = typeDisp;
            document.getElementById("SelectCourse").style.display = courseDisp;
        } else {
            document.getElementById("SelectType").style.display = "none";
            document.getElementById("SelectCourse").style.display = "none";
        }
        //一般のchangeも呼ぶ
        change(obj);
    }

    function change(obj) {

                if (obj.name == "formatItem") {
            // チェックされていれば表示する
            var display = obj.checked ? false : true;
            var elem,i,chkd;
            if (obj.value == "GraphDist") {
                elem = document.forms[0].elements["axis"];
                if( elem != null ){
                    chkd = false;
                    //動作をOffに
                    for( i=0; i<elem.length; i++){
                        elem[i].disabled = display;
                        chkd = chkd || elem[i].checked;
                    }
                    //初期値の設定
                    if( (!display) && (!chkd) ){
                        for( i=0; i<elem.length; i++){
                            if( elem[i].value == "number"){
                                elem[i].checked = true;
                            }
                        }
                    }
                }
            } else if (obj.value == "Chart") {
                elem = document.forms[0].elements["dispRatio"];
                chkd = false;
                if( elem != null ){
                    for( i=0; i<elem.length; i++){
                        elem[i].disabled = display;
                        chkd = chkd || elem[i].checked;
                    }
                    //初期値の設定
                    if( (!display) && (!chkd) ){
                        for( i=0; i<elem.length; i++){
                            if( elem[i].value == "yes"){
                                elem[i].checked = true;
                            }
                        }
                    }
                }
            }
        // オプション
        } else if (obj.name == "optionItem") {
            // チェックされていれば表示する
            var display = obj.checked ? false : true;
            var elem,i,chkd;
            if (obj.value == "CompPrev") {
                elem = document.forms[0].elements["compCount"];
                chkd = false;
                //動作をOffに
                for( i=0; i<elem.length; i++){
                    elem[i].disabled = display;
                    chkd = chkd || elem[i].checked;
                }
                //初期値の設定
                if( (!display) && (!chkd) ){
                    for( i=0; i<elem.length; i++){
                        if( elem[i].value == "1"){
                            elem[i].checked = true;
                        }
                    }
                }
            } else if (obj.value == "GraphCompRatio") {
                elem = document.forms[0].elements["pitchCompRatio"];
                chkd = false;
                for( i=0; i<elem.length; i++){
                    elem[i].disabled = display;
                    chkd = chkd || elem[i].checked;
                }
                //初期値の設定
                if( (!display) && (!chkd) ){
                    for( i=0; i<elem.length; i++){
                        if( elem[i].value == "1"){
                            elem[i].checked = true;
                        }
                    }
                }
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START --%>
    		<%-- 2019/09/06 QQ)Tanioka 共通テスト対応 ADD START --%>
			// 共通テスト英語認定試験CEFR取得状況
<%--			} else if (obj.value == "OptionCheckBox") { --%>
<%--				<c:if test="${ !isCenterResearch and !isMarkExam }">display = true;</c:if> --%>
<%--				elem = document.forms[0].elements["targetCheckBox"]; --%>
<%--				chkd = false; --%>
<%--				elem.disabled = display; --%>
<%--				chkd = chkd || elem.checked; --%>
				//初期値の設定
<%--				if( (!display) && (!chkd) ){ --%>
<%--					for( i=0; i<elem.length; i++){ --%>
<%--						if( elem.value == "0"){ --%>
<%--							elem.checked = false; --%>
<%--						} --%>
<%--					} --%>
<%--				} --%>
    		<%-- 2019/09/06 QQ)Tanioka 共通テスト対応 ADD END   --%>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END --%>
            }  else if (obj.value == "RankOrder") {
                elem = document.forms[0].elements["orderPitch"];
                chkd = false;
                for( i=0; i<elem.length; i++){
                    elem[i].disabled = display;
                    chkd = chkd || elem[i].checked;
                }
                //初期値の設定
                if( (!display) && (!chkd) ){
                    for( i=0; i<elem.length; i++){
                        if( elem[i].value == "1"){
                            elem[i].checked = true;
                        }
                    }
                }

            } else if (obj.value == "GraphBuildup") {
                elem = document.forms[0].elements["divPitch"];
                chkd = false;
                for( i=0; i<elem.length; i++){
                    elem[i].disabled = display;
                    chkd = chkd || elem[i].checked;
                }
                //初期値の設定
                if( (!display) && (!chkd) ){
                    for( i=0; i<elem.length; i++){
                        if( elem[i].value == "1"){
                            elem[i].checked = true;
                        }
                    }
                }
            }
        // 分析対象
        } else if (obj.name == "analyzeItem") {
            var display = obj.checked ? false : true;
            if( obj.value == "univ" ){
                document.forms[0].elements["univ"] = display;
                //ラジオボタンをdisable
                var elems = document.forms[0].elements["choiceUnivMode"];
                for( var i=0; i<elems.length; i++){
                    elems[i].disabled = display;
                }
                //志願者数・評価基準をdisable
                if( display ){
                    document.forms[0].elements["candRange"].disabled = display;
                    var elems2 = document.forms[0].elements["evaluation"];
                    for( var i=0; i<elems2.length; i++){
                        elems2[i].disabled = display;
                    }
                } else {
                    for( var i=0; i<elems.length; i++){
                        change(elems[i]);
                    }
                }
            } else if( obj.value == "deviation"){
                document.forms[0].elements["deviation"].disabled = display;
            }
        // 志望大学の分析モード
        } else if (obj.name == "choiceUnivMode"){
            var elems = document.forms[0].elements["choiceUnivMode"];
            for( var i=0; i<elems.length; i++){
                if( elems[i].value == '2' ){
                    var display = elems[i].checked ? false : true;
                    document.forms[0].elements["candRange"].disabled = display;
                } else if( elems[i].value == '3' ){
                    var display = elems[i].checked ? false : true;
                    var elems2 = document.forms[0].elements["evaluation"];
                    for( var i=0; i<elems2.length; i++){
                        elems2[i].disabled = display;
                    }
                }
            }
        // 型の選択・科目の選択
        } else {
            // チェックされていなければ評価しない
            if (obj.checked) {
                var dispYes = "none";
                var dispNo = "none";
                // 共通項目設定を利用する
                if (obj.value == "1") dispYes = "inline";
                // 共通項目設定を利用しない
                else dispNo = "inline";
                // 型
                if (obj.name == "commonType") {
                    document.getElementById("SelectType").style.display = dispNo;
                    document.getElementById("TypeInfo1").style.display = dispYes;
                    document.getElementById("TypeInfo2").style.display = dispNo;
                } else if (obj.name == "commonCourse") {
                    document.getElementById("SelectCourse").style.display = dispNo;
                    document.getElementById("CourseInfo1").style.display = dispYes;
                    document.getElementById("CourseInfo2").style.display = dispNo;
                }
            }
        }
    }

    function init() {
    	startTimer();
        // スクロール位置を初期化する
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

        // オプション設定
        var optionItem = document.forms[0].elements["optionItem"];
        if( optionItem != null ){
            var i;
            if( optionItem.length != null ){
                for ( i=0; i<optionItem.length; i++) {
                    // 過回の３つなら処理を行う
                    if (   (optionItem[i].value == "GraphBuildup")
                        || (optionItem[i].value == "GraphCompRatio")
                        || (optionItem[i].value == "RankOrder")
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%--                        || (optionItem[i].value == "OptionCheckBox")) { --%>
                    	) {
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END --%>
                        change(optionItem[i]);
                    }
                }
            } else {
                if (   (optionItem.value == "GraphBuildup")
                    || (optionItem.value == "GraphCompRatio")
                	|| (optionItem.value == "RankOrder") ) {
                    change(optionItem);
                }
            }
        }
        // 分析対象
        var analyzeItem = document.forms[0].elements["analyzeItem"];
        if( analyzeItem != null ){
            var i;
            if( analyzeItem.length != null ){
                for ( i=0; i<analyzeItem.length; i++) {
                    // 過回の３つなら処理を行う
                    if (   (analyzeItem[i].value == "univ"||
                        analyzeItem[i].value == "deviation" ) ) {
                        change(analyzeItem[i]);
                    }
                }
            } else {
                if (   (analyzeItem.value == "univ" ||
                    analyzeItem.value == "deviation") ) {
                    change(analyzeItem);
                }
            }
        }

        // 型
        var commonType = document.forms[0].elements["commonType"];
        if (commonType) {
            change(commonType[0]);
            change(commonType[1]);
        }
        // 科目
        var commonCourse = document.forms[0].elements["commonCourse"];
        if (commonCourse) {
            change(commonCourse[0]);
            change(commonCourse[1]);
        }
        // 型・科目設定
        var compPrev = document.getElementById("CompPrev");
        if (compPrev) {
            changeCompPrev( compPrev );
        } else {
            document.getElementById("CourseTypeSelect1").style.display = "inline";
            document.getElementById("CourseTypeSelect2").style.display = "none";
        }
        // 過年度の表示
        var formatItem = document.forms[0].elements["formatItem"];
        if( formatItem != null ){
            var i;
            if( formatItem.length != null ){
                for ( i=0; i<formatItem.length; i++) {
                    // 過回の３つなら処理を行う
                    if (   (formatItem[i].value == "Chart")
                        || (formatItem[i].value == "GraphDist") ) {
                        change(formatItem[i]);
                    }
                }
            } else {
                if (   (formatItem.value == "Chart")
                    || (formatItem.value == "GraphDist") ) {
                    change(formatItem);
                }
            }
        }
		// 日程
		{
			var e = document.forms[0].totalUnivDiv;
			if (e && e.length) {
				for (var i=0; i<e.length; i++) {
					if (e[i].checked) {
					<%-- 高1・2模試の場合は無条件で無効とする --%>
					<c:choose>
					<c:when test="${ isUniv12Exam }">
						document.forms[0].dispUnivSchedule.disabled = "disabled";
					</c:when>
					<c:otherwise>
						if (e[i].value == "2" || e[i].value == "4") {
							document.forms[0].dispUnivSchedule.disabled = "disabled";
						}
					</c:otherwise>
					</c:choose>
					}
				}
			}
		}
	}

	<%-- 志望大学評価別人数・過年度の表示のチェック --%>
	function dispUnivYearChecked(obj) {
		var count = 0;
		var e = document.forms[0].dispUnivYear;
		<%-- 4つ以下ならここまで --%>
		if (!e.length || e.length <= 4) return;
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) count++;
		}
		if (count > 4) {
			obj.checked = false;
			alert("<kn:message id="s017a" />");
			return;
		}
	}

	<%-- 志望大学評価別人数・大学集計区分の変更 --%>
	function dispUnivCountChecked(obj) {
		<%-- 高1・2模試の場合は無条件で無効とする --%>
		<c:choose>
		<c:when test="${ isUniv12Exam }">
			document.forms[0].dispUnivSchedule.disabled = "disabled";
		</c:when>
		<c:otherwise>
			if (obj.value == "1" || obj.value == "3" || obj.value == "5") {
				document.forms[0].dispUnivSchedule.disabled = false;
			} else {
				document.forms[0].dispUnivSchedule.disabled = "disabled";
			}
		</c:otherwise>
		</c:choose>
	}

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ClassDetail" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeExam" value="0">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp"%>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp"%>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="36" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="887">
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="887">
<tr>
<td width="737">
	<table border="0" cellpadding="0" cellspacing="0" width="737">
	<tr valign="top">
	<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
	<td width="721">

	<table border="0" cellpadding="0" cellspacing="0" width="721">
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="721" height="2" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="721" height="3" border="0" alt=""><br></td>
	</tr>
	<tr>
	<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
	<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
	<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
	<td width="705" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">
	<c:choose>
	  <c:when test="${param.forward == 'c002'}">クラス成績概況（偏差値分布）
	  </c:when>
	  <c:when test="${param.forward == 'c003'}">クラス成績概況（個人成績）
	  </c:when>
	  <c:when test="${param.forward == 'c004'}">クラス成績概況（個人成績推移）
	  </c:when>
	  <c:when test="${param.forward == 'c005'}">クラス成績概況（設問別個人成績）
	  </c:when>
	  <c:when test="${param.forward == 'c006'}">クラス成績概況（志望大学別個人評価）
	  </c:when>
	  <c:when test="${param.forward == 'c102'}">クラス比較（成績概況）
	  </c:when>
	  <c:when test="${param.forward == 'c103'}">クラス比較（偏差値分布）
	  </c:when>
	  <c:when test="${param.forward == 'c104'}">クラス比較（設問別成績）
	  </c:when>
	  <c:when test="${param.forward == 'c105'}">クラス比較（志望大学別評価人数）
	  </c:when>
	</c:choose>
	</b></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="721" height="3" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="721" height="2" border="0" alt=""><br></td>
	</tr>
	</table>

	</td>
	<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
	</tr>
	</table>
</td>
<td align="center" width="150"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
</table>
<!--/大タイトル-->
</td>
<td width="41"><img src="./shared_lib/img/parts/sp.gif" width="41" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->




<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="689" align="center">
<!--●●●左側●●●-->
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="689">
<tr>
<td width="669"><span class="text14">このメニューでの出力項目を以下の手順に従って設定してください。</span></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/説明-->

<!--ボタン-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<c:forTokens var="screen" items="c002,c003,c004,c005,c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--中タイトル-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr valign="top">
    <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
    <td width="641">

    <table border="0" cellpadding="0" cellspacing="0" width="641">
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
    </tr>
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
    <td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
    <td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
    <td width="628" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">担当クラス</b></td>
    </tr>
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
    </tr>
    </table>

    </td>
    <td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/中タイトル-->

    <!--担当クラスの設定-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="650">
    <tr valign="top">
    <td width="648" bgcolor="#EFF2F3" align="center">
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="9" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF" align="center"><b class="text14"><c:choose>
      <c:when test="${ empty ChargeClass.classList }">設定なし</c:when>
      <c:otherwise>
        <c:out value="${ChargeClass.year}" />年度
        <c:forEach var="class" items="${ChargeClass.classList}">
          &nbsp;<c:out value="${class.grade}" />年<c:out value="${class.className}" />
        </c:forEach>
      </c:otherwise>
    </c:choose></b></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <div style="margin-top:8px;">
    <!--設定ボタン--><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:120px;" onClick="openCharge()">
    </div>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/担当クラスの設定-->


    <!--矢印-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr>
    <td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
    </tr>
    </table>
    <!--/矢印-->
  </c:if>
</c:forTokens>



<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step1.gif" width="86" height="24" border="0" alt="STEP1"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--対象模試の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--年度＆模試-->
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF" align="center">

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象年度&nbsp;：&nbsp;</span></td>
<td><select name="targetYear" class="text12" style="width:60px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象模試&nbsp;：&nbsp;</span></td>
<td><select name="targetExam" class="text12" style="width:220px;" onChange="changeExamSelect()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/年度＆模試-->

<!--比較したい過回の選択-->
<c:forTokens var="screen" items="c004" delims=",">
  <c:if test="${ param.forward == screen }">
    <div style="margin-top:10px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="30"><img src="./shared_lib/img/parts/icon_year.gif" width="30" height="30" border="0" alt="比較対象年度の選択" align="absmiddle"></td>
    <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td width="580"><b class="text14" style="color:#657681;">比較したい過回の選択</b></td>
    </tr>
    <tr>
    <td colspan="3" width="620"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <div style="margin-top:14px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr valign="top">
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <table border="0" cellpadding="8" cellspacing="2" width="576">
    <!--過回模試1-->
    <tr>
    <td width="34%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><b class="text12">過回模試1</b></td>
    </tr>
    </table>
    </td>
    <td width="66%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""></td>
    <td><select name="pastExam1" class="text12" style="width:220px;">
    <c:forEach var="exam" items="${PastExam1}">
      <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.pastExam1 == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
    </c:forEach>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <!--/過回模試1-->
    <!--過回模試2-->
    <tr>
    <td width="34%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><b class="text12">過回模試2</b></td>
    </tr>
    </table>
    </td>
    <td width="66%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""></td>
    <td><select name="pastExam2" class="text12" style="width:220px;">
    <c:forEach var="exam" items="${PastExam2}">
      <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.pastExam2 == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
    </c:forEach>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
    <!--/過回模試2-->
    </table>

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/比較したい過回の選択-->
    <!--注意-->
    <div style="margin-top:3px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td width="620"><span class="text12">※STEP1の対象模試と年度、模試形式が同じものが選択可能になっています。</span></td>
    </tr>
    </table>
    </div>
    <!--/注意-->
  </c:if>
</c:forTokens>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/対象模試の設定-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step2.gif" width="86" height="24" border="0" alt="STEP2"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--共通項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">

 <!-- 型・科目設定あり(javaScriptで on/off) -->
<td width="618" bgcolor="#FFFFFF">
<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top" id="CourseTypeSelect1">
<c:choose>
  <c:when test="${ param.forward == 'c003' }">
    <td align="center"><span class="text14-hh">※この帳票では共通項目を使用しません。</span></td>
  </c:when>
  <c:otherwise>
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <table border="0" cellpadding="0" cellspacing="0" width="572">
    <tr valign="top">
    <td><span class="text14-hh">この分析で利用する共通項目です。<br>
    ※「型」・「科目」で共通項目を利用しない場合は下記で切り替えることができます。</span></td>
    </tr>
    <tr valign="top">
    <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
    </tr>
    </table>



    <table border="0" cellpadding="0" cellspacing="2" width="576">
    <tr bgcolor="#CDD7DD">
    <td colspan="3" width="572" align="right" >
    <table border="0" cellpadding="6" cellspacing="0">
    <tr>
    <td><span class="text12">共通項目設定の最終更新日 ： <fmt:formatDate value="${Profile.updateDate}" pattern="yyyy/MM/dd" /></span></td>
    </tr>
    </table>
    </td>
    </tr>

    <!--型の選択-->
    <c:forTokens var="screen" items="c002,c004,c102,c103" delims=",">
      <c:if test="${ param.forward == screen }">
        <tr height="56">
        <td width="34%" bgcolor="#E1E6EB">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><img src="./shared_lib/img/parts/icon_kata.gif" width="30" height="30" border="0" alt="型の選択" align="absmiddle" hspace="6"></td>
        <td><b class="text12">型の選択</b></td>
        </tr>
        </table>
        </td>
        <td width="33%" bgcolor="#F4E5D6">
        <table border="0" cellpadding="0" cellspacing="3">
            <tr>
            <td><input type="radio" name="commonType" value="1" onClick="javascript:change(this)" <c:if test="${form.commonType == '1'}">checked</c:if>></td>
            <td><span class="text12">共通項目設定を利用する</span></td>
            </tr>
            <tr>
            <td><input type="radio" name="commonType" value="0" onClick="javascript:change(this)" <c:if test="${form.commonType == '0'}">checked</c:if>></td>
            <td><span class="text12">共通項目設定を利用しない</span></td>
            </tr>
        </table>
        </td>
        <td width="33%" bgcolor="#F4E5D6" align="center" id="TypeInfo1" nowrap><span class="text12"><a href="javascript:openCommon(1)"><kn:com item="type" /></a></span></td>
        <td width="33%" bgcolor="#F4E5D6" align="center" id="TypeInfo2" style="display:none"><b class="text12" style="color:#FF6E0E;">STEP3より選択</b></td>
        </tr>
      </c:if>
    </c:forTokens>
    <!--/型の選択-->

    <!--科目の選択-->
    <c:forTokens var="screen" items="c002,c004,c005,c102,c103,c104" delims=",">
      <c:if test="${ param.forward == screen }">
        <tr height="56">
        <td width="34%" bgcolor="#E1E6EB">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><img src="./shared_lib/img/parts/icon_kamoku.gif" width="30" height="30" border="0" alt="科目の選択" align="absmiddle" hspace="6"></td>
        <td><b class="text12">科目の選択</b></td>
        </tr>
        </table>
        </td>
        <td width="33%" bgcolor="#F4E5D6">
        <table border="0" cellpadding="0" cellspacing="3">
            <tr>
            <td><input type="radio" name="commonCourse" value="1" onClick="javascript:change(this)" <c:if test="${form.commonCourse == '1'}">checked</c:if>></td>
            <td><span class="text12">共通項目設定を利用する</span></td>
            </tr>
            <tr>
            <td><input type="radio" name="commonCourse" value="0" onClick="javascript:change(this)" <c:if test="${form.commonCourse == '0'}">checked</c:if>></td>
            <td><span class="text12">共通項目設定を利用しない</span></td>
            </tr>
        </table>
        </td>
        <td width="33%" bgcolor="#F4E5D6" align="center" id="CourseInfo1"><span class="text12"><a href="javascript:openCommon(2)"><kn:com item="course" /></a></span></td>
        <td width="33%" bgcolor="#F4E5D6" align="center" id="CourseInfo2" style="display:none"><b class="text12" style="color:#FF6E0E;">STEP3より選択</b></td>
        </tr>
      </c:if>
    </c:forTokens>
    <!--/科目の選択-->

    <!--志望大学-->
    <c:forTokens var="screen" items="c006,c105" delims=",">
      <c:if test="${ param.forward == screen }">
        <tr height="36">
        <td width="34%" bgcolor="#E1E6EB">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><img src="./shared_lib/img/parts/icon_shibou.gif" width="30" height="30" border="0" alt="志望大学" align="absmiddle" hspace="6"></td>
        <td><b class="text12">志望大学</b></td>
        </tr>
        </table>
        </td>
        <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(4)"><kn:com item="univ" /></a></span></td>
        </tr>
      </c:if>
    </c:forTokens>
    <!--/比志望大学-->

    <!--比較対象クラス-->
    <c:forTokens var="screen" items="c102,c103,c104,c105" delims=",">
      <c:if test="${ param.forward == screen }">
        <tr height="36">
        <td width="34%" bgcolor="#E1E6EB">
        <table border="0" cellpadding="0" cellspacing="0">
        <tr>
        <td><img src="./shared_lib/img/parts/icon_class.gif" width="30" height="30" border="0" alt="比較対象クラス" align="absmiddle" hspace="6"></td>
        <td><b class="text12">比較対象クラス</b></td>
        </tr>
        </table>
        </td>
        <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(5)"><kn:com item="class" /></a></span></td>
        </tr>
      </c:if>
    </c:forTokens>
    <!--/比較対象クラス-->

    </table>
    </td>
  </c:otherwise>
</c:choose>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
<!-- 型・科目設定有り -->

<!-- 型・科目設定無し -->
  <tr valign="top" id="CourseTypeSelect2">
  <td align="center"><span class="text14-hh">※この帳票では共通項目を使用しません。</span></td>
  </tr>
<!-- /型・科目設定無し -->

</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/共通項目の設定-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step3.gif" width="86" height="24" border="0" alt="STEP3"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">個別項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--個別項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">


<!-- 型の選択-->
<c:forTokens var="screen" items="c002,c004,c102,c103" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr valign="top" id="SelectType" style="display:none">
    <td width="648" bgcolor="#EFF2F3" align="center">

    <div style="margin-top:7px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="42"><img src="./shared_lib/img/parts/icon_kata.gif" width="30" height="30" border="0" alt="型の選択" hspace="3"></td>
    <td width="578"><b class="text14" style="color:#657681;">型の選択</b></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <div style="margin-top:8px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr valign="top">
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <table border="0" cellpadding="0" cellspacing="0" width="572">
    <tr valign="top">
    <td><span class="text14-hh">分析対象とする型を選択し、さらにグラフ表示をさせたい型を選択してください。<BR>※分析対象の選択をせずにグラフ表示に設定することはできません。</span></td>
    </tr>
    </table>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="4" cellspacing="0" width="572">
	<tr>
	<td width="741" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'analyzeType');changeAllState('analyzeType');"></td>
	--%>
	<td><input type="checkbox" name="dummy1" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyzeType');changeAllState('analyzeType');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">分析対象をすべて選択</b></td>
	<td>&nbsp;&nbsp;</td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graphType');"></td>
	--%>
	<td><input type="checkbox" name="dummy1" value="" onclick="util.checkAllElements(this.checked, 'graphType');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

    <!--リスト-->
    <table border="0" cellpadding="0" cellspacing="0" width="576">
    <tr valign="top">
    <td width="283">
    <!--左側-->
    <table border="0" cellpadding="2" cellspacing="2" width="283">
    <tr bgcolor="#CDD7DD">
    <td width="13%">&nbsp;</td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">分析<br>対象</span></td>
    </tr>
    </table>
    </td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">グラフ<br>表示</span></td>
    </tr>
    </table>
    </td>
    <td width="55%">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12">型名</span></td>
    <td align="right" nowrap><span class="text12">受験人数</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <c:set var="begin" value="0" />
    <%@ include file="/jsp/shared_lib/cm_type.jsp" %>
    <!--/set-->
    </table>
    <!--/左側-->
    </td>
    <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td width="283">
    <!--右側-->
    <table border="0" cellpadding="2" cellspacing="2" width="283">
    <tr bgcolor="#CDD7DD">
    <td width="13%">&nbsp;</td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">分析<br>対象</span></td>
    </tr>
    </table>
    </td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">グラフ<br>表示</span></td>
    </tr>
    </table>
    </td>
    <td width="55%">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12">型名</span></td>
    <td align="right" nowrap><span class="text12">受験人数</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <c:set var="begin" value="${TypeBean.fullListHalfSize}" />
    <%@ include file="/jsp/shared_lib/cm_type.jsp" %>
    </table>
    <!--/右側-->
    </td>
    </tr>
    </table>
    <!--/リスト-->

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <!--注意-->
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td width="620" align="right">
    <div style="margin-top:6px;">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    </table>
    <!--/注意-->
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!-- /型の選択-->

<!-- 科目の選択-->
<c:forTokens var="screen" items="c002,c004,c005,c102,c103,c104" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr valign="top" id="SelectCourse" style="display:none">
    <td width="648" bgcolor="#EFF2F3" align="center">

    <div style="margin-top:7px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="42"><img src="./shared_lib/img/parts/icon_kamoku.gif" width="30" height="30" border="0" alt="科目の選択" hspace="3"></td>
    <td width="578"><b class="text14" style="color:#657681;">科目の選択</b></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <div style="margin-top:8px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr valign="top">
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <table border="0" cellpadding="0" cellspacing="0" width="572">
    <tr valign="top">
    <td><span class="text14-hh">分析対象とする科目を選択し、さらにグラフ表示をさせたい科目を選択してください。<BR>※分析対象の選択をせずにグラフ表示に設定することはできません。</span></td>
    </tr>
    </table>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="4" cellspacing="0" width="572">
	<tr>
	<td width="741" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'analyzeCourse');changeAllState('analyzeCourse');"></td>
	--%>
	<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyzeCourse');changeAllState('analyzeCourse');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">分析対象をすべて選択</b></td>
	<td>&nbsp;&nbsp;</td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graphCourse');"></td>
	--%>
	<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, 'graphCourse');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

    <!--リスト-->
    <table border="0" cellpadding="0" cellspacing="0" width="576">
    <tr valign="top">
    <td width="283">
    <!--左側-->
    <table border="0" cellpadding="2" cellspacing="2" width="283">
    <tr bgcolor="#CDD7DD">
    <td width="13%">&nbsp;</td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">分析<br>対象</span></td>
    </tr>
    </table>
    </td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">グラフ<br>表示</span></td>
    </tr>
    </table>
    </td>
    <td width="55%">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12">科目名</span></td>
    <td align="right" nowrap><span class="text12">受験人数</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <c:set var="begin" value="0" />
    <%@ include file="/jsp/shared_lib/cm_course.jsp" %>
    </table>
    <!--/左側-->
    </td>
    <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
    <td width="283">
    <!--右側-->
    <table border="0" cellpadding="2" cellspacing="2" width="283">
    <tr bgcolor="#CDD7DD">
    <td width="13%">&nbsp;</td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">分析<br>対象</span></td>
    </tr>
    </table>
    </td>
    <td width="16%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><span class="text12">グラフ<br>表示</span></td>
    </tr>
    </table>
    </td>
    <td width="55%">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12">科目名</span></td>
    <td align="right" nowrap><span class="text12">受験人数</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <c:set var="begin" value="${CourseBean.fullListHalfSize}" />
    <%@ include file="/jsp/shared_lib/cm_course.jsp" %>
    </table>
    <!--/右側-->
    </td>
    </tr>
    </table>
    <!--/リスト-->

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <!--注意-->
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td width="620" align="right">
    <div style="margin-top:6px;">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている科目" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    </table>
    <!--/注意-->
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!-- /科目の選択-->



<!--表示・印刷形式の選択-->

<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">

<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr>
<td width="42"><img src="./shared_lib/img/parts/icon_hyoji.gif" width="30" height="30" border="0" alt="表示・印刷形式の選択" hspace="3"></td>
<td width="378"><b class="text14" style="color:#657681;">表示形式の選択</b></td>
<td width="200" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('<c:out value="${param.forward}" />')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('<c:out value="${param.forward}" />')">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<!--フォーマットの選択-->
<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="576" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td><b class="text14-hh">フォーマットの選択</b></td>
</tr>
</table>

<c:forEach var="formItem" items="${form.formatItem}">
  <c:if test="${formItem == 'Chart'}"><c:set var="Chart" value=" checked "/></c:if>
  <c:if test="${formItem == 'GraphDist'}"><c:set var="GraphDist" value=" checked "/></c:if>
  <c:if test="${formItem == 'Graph'}"><c:set var="Graph" value=" checked "/></c:if>
</c:forEach>
<c:forTokens var="screen" items="c002,c003,c004,c005,c006,c102,c104,c105" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    <!--一段目-->
    <table border="0" cellpadding="0" cellspacing="0" width="573">
    <tr valign="top">
    <td width="187">
    <!--表-->
    <table border="0" cellpadding="0" cellspacing="0" width="187">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="187">
    <tr>
    <td width="185" height="26" bgcolor="#E5EEF3">
    <table border="0" cellpadding="0" cellspacing="3">
    <tr>
    <td>
    <c:forTokens var="screen" items="c002,c003,c004,c102,c103,c104" delims=",">
      <c:if test="${ param.forward == screen }">
        <input type="checkbox" name="formatItem" value="Chart" <c:out value="${Chart}"/> >
      </c:if>
    </c:forTokens>
    <%--
    <c:forTokens var="screen" items="c005,c006,c105" delims=",">
      <c:if test="${ param.forward == screen }">
        <input type="hidden" name="formatItem" value="Chart" >
      </c:if>
    </c:forTokens>
    --%>
    </td>
    <td><span class="text12">表</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr valign="top">
    <td width="185" bgcolor="#FFFFFF" align="center">

    <div style="margin-top:9px;">
    <table border="0" cellpadding="0" cellspacing="0" width="171">
    <tr valign="top">
    <td width="93"><!--イラスト--><img src="./class/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
    <td width="78"></td>
    </tr>
    </table>
    </div>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/表-->
    </td>
    <c:forTokens var="screen" items="c002" delims=",">
      <c:if test="${ param.forward == screen }">
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187">
        <!--偏差値帯別度数分布グラフ-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="26" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
        <td>
            <input type="checkbox" name="formatItem" value="GraphDist" <c:out value="${GraphDist}"/> >
        </td>
        <td><span class="text12">偏差値帯別度数分布グラフ</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph06.gif" width="85" height="64" border="0" alt="偏差値帯別度数分布グラフ"><br></td>
        <td width="78"></td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        </td>
      </c:if>
    </c:forTokens>
  <!--/偏差値帯別度数分布グラフ-->

    <c:forTokens var="screen" items="c102,c104" delims=",">
      <c:if test="${ param.forward == screen }">
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187">
        <!--グラフ-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="26" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
            <td><input type="checkbox" name="formatItem" value="Graph" <c:out value="${Graph}"/> ></td>
        <td><span class="text12">グラフ</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./school/img/graph02.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
        <td width="78"></td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
      </c:if>
    </c:forTokens>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="187"></td>
    </tr>
    </table>
    <!--/一段目-->
  </c:if>
</c:forTokens>

<!--二段目-->
<c:forTokens var="screen" items="c103" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="573">
    <tr valign="top">
    <td width="187">
    <!--表-->
    <table border="0" cellpadding="0" cellspacing="0" width="187">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="187">
    <tr>
    <td width="185" height="26" bgcolor="#E5EEF3">
    <table border="0" cellpadding="0" cellspacing="3">
    <tr>
    <td>
        <input type="checkbox" name="formatItem" value="Chart" onClick="javascript:change(this)" <c:out value="${Chart}"/>>
    </td>
    <td><span class="text12">表</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr valign="top">
    <td width="185" bgcolor="#FFFFFF" align="center">

    <div style="margin-top:9px;">
    <table border="0" cellpadding="0" cellspacing="0" width="171">
    <tr valign="top">
    <td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
    <td width="78">
    <table border="0" cellpadding="0" cellspacing="0" width="78">
    <tr>
    <td colspan="2" width="78"><span class="text12">構成比表示：</span></td>
    </tr>
    <tr>
    <td width="22"><input type="radio" name="dispRatio" value="1" <c:if test="${form.dispRatio == '1'}">checked</c:if> ></td>
    <td width="56"><span class="text12">あり</span></td>
    </tr>
    <tr>
    <td width="22"><input type="radio" name="dispRatio" value="2" <c:if test="${form.dispRatio == '2'}">checked</c:if> ></td>
    <td width="56"><span class="text12">なし</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </div>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/表-->
    </td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="187">
    <!--グラフ-->
    <table border="0" cellpadding="0" cellspacing="0" width="187">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="187">
    <tr>
    <td width="185" height="26" bgcolor="#E5EEF3">
    <table border="0" cellpadding="0" cellspacing="3">
    <tr>
    <td>
        <input type="checkbox" name="formatItem" value="GraphDist" onClick="javascript:change(this)" <c:out value="${GraphDist}"/> >
    </td>
    <td><span class="text12">偏差値帯別度数分布グラフ</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr valign="top">
    <td width="185" bgcolor="#FFFFFF" align="center">

    <div style="margin-top:9px;">
    <table border="0" cellpadding="0" cellspacing="0" width="171">
    <tr valign="top">
    <td width="93"><!--イラスト--><img src="./class/img/graph09.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
    <td width="78">
    <table border="0" cellpadding="0" cellspacing="0" width="78">
    <tr>
    <td colspan="2" width="78"><span class="text12">グラフ軸：</span></td>
    </tr>
    <tr>
    <td width="22"><input type="radio" name="axis" value="1" <c:if test="${form.axis == '1'}">checked</c:if> ></td>
    <td width="56"><span class="text12">人数</span></td>
    </tr>
    <tr>
    <td width="22"><input type="radio" name="axis" value="2" <c:if test="${form.axis == '2'}">checked</c:if> ></td>
    <td width="56"><span class="text12">構成比</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </div>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/グラフ-->
    </td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="187"></td>
    </tr>
    </table>
    <!--/二段目-->
  </c:if>
</c:forTokens>



</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/フォーマットの選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<c:forEach var="optItem" items="${form.optionItem}">
  <c:if test="${optItem == 'GraphBuildup'}"><c:set var="GraphBuildup" value=" checked "/></c:if>
  <c:if test="${optItem == 'CompPrev'}"><c:set var="CompPrev" value=" checked "/></c:if>
  <c:if test="${optItem == 'GraphCompRatio'}"><c:set var="GraphCompRatio" value=" checked "/></c:if>
  <c:if test="${optItem == 'RankOrder'}"><c:set var="RankOrder" value=" checked "/></c:if>
</c:forEach>

<c:forTokens var="screen" items="c002,c003,c004,c103" delims=",">
  <c:if test="${ param.forward == screen }">

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <!--オプションの選択-->
    <div style="margin-top:8px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr valign="top">
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <table border="0" cellpadding="0" cellspacing="0" width="572">
    <tr valign="top">
    <td><b class="text14-hh">オプションの選択</b></td>
    </tr>
    </table>

    <c:forTokens var="screen" items="c002" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->

        <!--一段目-->
        <table border="0" cellpadding="0" cellspacing="0" width="573">
        <tr valign="top">
        <td width="187">
        <!--偏差値帯別人数積み上げグラフ-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="36" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
        <td>
            <input type="checkbox" name="optionItem" value="GraphBuildup" onClick="javascript:change(this)" <c:out value="${GraphBuildup}"/> >
        </td>
        <td><span class="text12">偏差値帯別人数積み上げ<br>グラフ</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph03.gif" width="85" height="64" border="0" alt="偏差値帯別人数積み上げグラフ"><br></td>
        <td width="78">
        <table border="0" cellpadding="0" cellspacing="0" width="78">
		<c:choose>
		<c:when test="${NewExam}">
        <tr>
        <td colspan="2" width="78"><span class="text12">得点ピッチ：</span></td>
        </tr>
        <tr>

            <td width="22"><input type="radio" name="divPitch" value="1" <c:if test="${form.divPitch == '1'}">checked</c:if> ></td>

        <td width="56"><span class="text12">10</span></td>
        </tr>
        <tr>
            <td width="22"><input type="radio" name="divPitch" value="2" <c:if test="${form.divPitch == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">20</span></td>
        </tr>
        <tr>
            <td width="22"><input type="radio" name="divPitch" value="3" <c:if test="${form.divPitch == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">40</span></td>
        </tr>
		</c:when>
		<c:otherwise>
        <tr>
        <td colspan="2" width="78"><span class="text12">偏差値ピッチ：</span></td>
        </tr>
        <tr>

            <td width="22"><input type="radio" name="divPitch" value="1" <c:if test="${form.divPitch == '1'}">checked</c:if> ></td>

        <td width="56"><span class="text12">2.5</span></td>
        </tr>
        <tr>
            <td width="22"><input type="radio" name="divPitch" value="2" <c:if test="${form.divPitch == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">5.0</span></td>
        </tr>
        <tr>
            <td width="22"><input type="radio" name="divPitch" value="3" <c:if test="${form.divPitch == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">10.0</span></td>
        </tr>
		</c:otherwise>
		</c:choose>
        </table>
        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187"></td>
        </tr>
        </table>
        <!--/一段目-->
      </c:if>
    </c:forTokens>

    <c:forTokens var="screen" items="c004" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->

        <!--一段目-->
        <table border="0" cellpadding="0" cellspacing="0" width="573">
        <tr valign="top">
        <td width="187">
        <!--型・科目別比較-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="36" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
            <td><input type="checkbox" name="optionItem" value="CompPrev" id="CompPrev" onClick="javascript:changeCompPrev(this)" <c:out value="${CompPrev}"/> ></td>
        <td><span class="text12">型・科目別比較</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph04.gif" width="85" height="64" border="0" alt="型・科目別比較"><br></td>
        <td width="78">
        <table border="0" cellpadding="0" cellspacing="0" width="78">
        <tr>
        <td colspan="2" width="78"><span class="text12">比較模試数：</span></td>
        </tr>

            <tr>
            <td width="22"><input type="radio" name="compCount" value="1" <c:if test="${form.compCount == '1' }">checked</c:if>></td>
            <td width="56"><span class="text12">2回比較</span></td>
            </tr>
            <tr>
            <td width="22"><input type="radio" name="compCount" value="2" <c:if test="${form.compCount == '2' }">checked</c:if>></td>
            <td width="56"><span class="text12">3回比較</span></td>
            </tr>

        </table>
        </td>
        </tr>
        <tr valign="top">
        <td colspan="2" width="171"><img src="./shared_lib/img/parts/dot_blue.gif" width="171" height="1" border="0" alt="" vspace="8"><br></td>
        </tr>
        <tr valign="top" id="CourseTypeSelectInfo">
        <td colspan="2" width="171" height="45"><span class="text12">
        <font color="#FF6E0E">設定項目が追加されました。<br></font>
        STEP2：<br>
        <b>「型の選択、科目の選択」</b><br>
        </span></td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187"></td>
        </tr>
        </table>
        <!--/一段目-->
      </c:if>
    </c:forTokens>



    <c:forTokens var="screen" items="c103" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->

        <!--一段目-->
        <table border="0" cellpadding="0" cellspacing="0" width="573">
        <tr valign="top">
        <td width="187">
        <!--グラフ-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="36" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
        <td>
            <input type="checkbox" name="optionItem" value="GraphBuildup" onClick="javascript:change(this)" <c:out value="${GraphBuildup}"/> >
        </td>
        <td><span class="text12">偏差値帯別人数積み上げ<br>グラフ</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./school/img/graph04.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
        <td width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%--        <table border="0" cellpadding="0" cellspacing="0" width="78" height="72"> --%>
        <table border="0" cellpadding="0" cellspacing="0" width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END --%>
		<c:choose>
		<c:when test="${NewExam}">
        <tr>
        <td colspan="2" width="78"><span class="text12">得点ピッチ：</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="1" <c:if test="${form.divPitch == '1'}">checked</c:if> ></td>
        <td width="56"><span class="text12">10</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="2" <c:if test="${form.divPitch == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">20</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="3" <c:if test="${form.divPitch == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">40</span></td>
        </tr>
		</c:when>
		<c:otherwise>
        <tr>
        <td colspan="2" width="78"><span class="text12">偏差値ピッチ：</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="1" <c:if test="${form.divPitch == '1'}">checked</c:if> ></td>
        <td width="56"><span class="text12">2.5</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="2" <c:if test="${form.divPitch == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">5.0</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="divPitch" value="3" <c:if test="${form.divPitch == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">10.0</span></td>
        </tr>
		</c:otherwise>
		</c:choose>
        </table>
        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187">
        <!--グラフ-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="36" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
        <td>
            <input type="checkbox" name="optionItem" value="GraphCompRatio" onClick="javascript:change(this)" <c:out value="${GraphCompRatio}"/> >
        </td>
        <td><span class="text12">偏差値帯別構成比グラフ</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph08.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
        <td width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%--        <table border="0" cellpadding="0" cellspacing="0" width="78" height="72"> --%>
        <table border="0" cellpadding="0" cellspacing="0" width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END --%>
		<c:choose>
		<c:when test="${NewExam}">
        <tr>
        <td colspan="2" width="78"><span class="text12">得点ピッチ：</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="1" <c:if test="${form.pitchCompRatio == '1'}">checked</c:if> ></td>
        <td width="56"><span class="text12">10</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="2" <c:if test="${form.pitchCompRatio == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">20</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="3" <c:if test="${form.pitchCompRatio == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">40</span></td>
        </tr>
		</c:when>
		<c:otherwise>
        <tr>
        <td colspan="2" width="78"><span class="text12">偏差値ピッチ：</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="1" <c:if test="${form.pitchCompRatio == '1'}">checked</c:if> ></td>
        <td width="56"><span class="text12">2.5</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="2" <c:if test="${form.pitchCompRatio == '2'}">checked</c:if> ></td>
        <td width="56"><span class="text12">5.0</span></td>
        </tr>
        <tr>
        <td width="22"><input type="radio" name="pitchCompRatio" value="3" <c:if test="${form.pitchCompRatio == '3'}">checked</c:if> ></td>
        <td width="56"><span class="text12">10.0</span></td>
        </tr>
		</c:otherwise>
		</c:choose>
        </table>
        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
<%--        <td width="187"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" width="187"> --%>
<%--			<tr valign="top"> --%>
<%--			<td bgcolor="#8CA9BB"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="1" width="187"> --%>
<%--			<tr> --%>
<%--			<td width="185" height="35" bgcolor="#E5EEF3"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="3"> --%>
<%--			<tr> --%>
<%--			<td> --%>
<%--			<!--<input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)" <c:out value="${OptionCheckBox}"/><c:if test="${form.targetCheckBox == '11' || form.targetCheckBox == '12'}"> checked</c:if><c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> -->--%>
<%--			<c:set value="false" var="flag"/> --%>
<%--			<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%--			  <c:if test="${optItem == 'OptionCheckBox'}"> --%>
<%--			  <c:set value="true" var="flag"/> --%>
<%--				<input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)" checked<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--			  </c:if> --%>
<%--			</c:forEach> --%>
<%--			<c:if test="${flag == false}"> --%>
<%--			  <input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--			</c:if> --%>
<%--			</td> --%>
<%--			<td><span class="text12">共通テスト英語認定試験<br>CEFR取得状況</span></td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			<tr valign="top"> --%>
<%--			<c:if test="${ !isCenterResearch and !isMarkExam }"> --%>
<%--				<td width="185" bgcolor="#e1e1e1" align="center"> --%>
<%--			</c:if> --%>
<%--			<c:if test="${ isCenterResearch or isMarkExam}"> --%>
<%--				<td width="185" bgcolor="#FFFFFF" align="center"> --%>
<%--			</c:if> --%>
<%--			<div style="margin-top:9px;"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" width="171"> --%>
<%--			<tr valign="top"> --%>
<%--			<td width="88"> --%><!--イラスト--><%--<img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="グラフ"><br></td> --%>
<%--			<td width="83"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" height="72"> --%>
<%--			<tr valign="top"> --%>
<%--			<td> --%>
<%--			<input type="checkbox" name="targetCheckBox" style="margin:0px 2px 0px 0px" value="11" <c:if test="${form.targetCheckBox == '1' || form.targetCheckBox == '11'}">checked</c:if>> --%>
<%--			</td> --%>
<%--			<td><span class="text10">自校受験者がいる試験を対象にする</span></td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</div> --%>

			<!--spacer-->
<%--			<table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%--			<tr valign="top"> --%>
<%--			<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td> --%>
<%--			</tr> --%>
<%--			</table> --%>
			<!--/spacer-->
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--        </td> --%>
        <td width="187"></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 UPD END --%>
        </tr>
        </table>
        <!--/一段目-->
      </c:if>
    </c:forTokens>


    <c:forTokens var="screen" items="c003" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->

        <!--一段目-->
        <table border="0" cellpadding="0" cellspacing="0" width="573">
        <tr valign="top">
        <td width="187">
        <!--型・科目別成績順位-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="36" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
            <td><input type="checkbox" name="optionItem" value="RankOrder"  onclick="javascript:change(this)" <c:out value="${RankOrder}"/>  ></td>
        <td><span class="text12">型・科目別成績順位</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph05.gif" width="85" height="64" border="0" alt="表"><br></td>
        <td width="78">
        <table border="0" cellpadding="0" cellspacing="0" width="78">
        <tr><td colspan="2" width ="78"><span class="text12">順位付け:</span></td></tr>
        <tr><td width ="22"><input type="radio" name="orderPitch" value="1" <c:if test="${form.orderPitch == '0' || form.orderPitch == '1'}">checked</c:if>></td>
        <td width ="56"><span class="text12">クラス単位</span></td></tr>
        <tr><td width ="22"><input type="radio" name="orderPitch" value="2"<c:if test="${form.orderPitch == '2'}">checked</c:if>></td>
        <td width ="56"><span class="text12">選択クラス全員</span></td></tr></table>
        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/グラフ-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="187"></td>
        </tr>
        </table>
        <!--/一段目-->
      </c:if>
    </c:forTokens>
    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--/オプションの選択-->

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
  </c:if>
</c:forTokens>

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<!--表示項目の選択-->
<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="576" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td><b class="text14-hh">表示項目の選択</b></td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="576">

<!--set-->
<c:forTokens var="screen" items="c003" delims=",">
  <c:if test="${ param.forward == screen }">
	<%-- 新テストは非表示 --%>
	<c:choose>
		<c:when test="${NewExam}">
			<input type="hidden" name="dispChoiceUniv" value="<c:out value="${form.dispChoiceUniv}" />">
		</c:when>
		<c:otherwise>
			<tr>
			<td width="22%" bgcolor="#E1E6EB">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td><b class="text12">志望大学表示</b></td>
			</tr>
			</table>
			</td>
			<td width="78%" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
			<td width="33%">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="22"><input type="radio" name="dispChoiceUniv" value="1" <c:if test="${form.dispChoiceUniv == '1'}">checked</c:if> ></td>
			<td><span class="text12">あり</span></td>
			</tr>
			</table>
			</td>
			<td width="33%">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="22"><input type="radio" name="dispChoiceUniv" value="2" <c:if test="${form.dispChoiceUniv == '2'}">checked</c:if> ></td>
			<td><span class="text12">なし</span></td>
			</tr>
			</table>
			</td>
			<td width="34%"></td>
			</tr>
			</table>
			</td>
			</tr>
		</c:otherwise>
	</c:choose>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">国私区分</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="printUnivDiv" value="1" <c:if test="${form.printUnivDiv == '1'}">checked</c:if> ></td>
    <td><span class="text12">すべて</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="printUnivDiv" value="2" <c:if test="${form.printUnivDiv == '2'}">checked</c:if> ></td>
    <td><span class="text12">国公立大</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="printUnivDiv" value="3" <c:if test="${form.printUnivDiv == '3'}">checked</c:if> ></td>
    <td><span class="text12">私大・短大・その他</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c004" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">生徒の表示順序</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="strudentOrder" value="1" <c:if test="${form.strudentOrder == '1'}">checked</c:if> ></td>
    <td><span class="text12">番号順</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="strudentOrder" value="2" <c:if test="${form.strudentOrder == '2'}">checked</c:if> ></td>
    <td><span class="text12">成績順</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="strudentOrder" value="3" <c:if test="${form.strudentOrder == '3'}">checked</c:if> ></td>
    <td><span class="text12">変動順</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="3"><span class="text12">&nbsp;※フォーマットの選択で「表」を選んだ場合、<br>&nbsp;「成績順」「変動順」は、英語の成績を基準としています。</span></td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c005" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">生徒の表示順序</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="strudentOrder" value="1" <c:if test="${form.strudentOrder == '1'}">checked</c:if> ></td>
    <td><span class="text12">番号順</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="strudentOrder" value="2" <c:if test="${form.strudentOrder == '2'}">checked</c:if> ></td>
    <td><span class="text12">成績順</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><!--<input type="radio" name="" value="">--></td>
    <td><!--<span class="text12">変動順</span>--></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->


<!--set-->
<c:forTokens var="screen" items="c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">志望者の表示順序</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="candidateOrder" value="1" <c:if test="${form.candidateOrder == '1'}">checked</c:if> ></td>
    <td><span class="text12">クラス番号順</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="candidateOrder" value="2" <c:if test="${form.candidateOrder == '2'}">checked</c:if> ></td>
    <td><span class="text12">評価順</span></td>
    </tr>
    </table>
    </td>
    <td width="34%"></td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">大学の表示順序</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="univOrder" value="3" <c:if test="${form.univOrder == '3'}">checked</c:if> ></td>
    <td><span class="text12">志望者数順</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="univOrder" value="1" <c:if test="${form.univOrder == '1'}">checked</c:if> ></td>
    <td><span class="text12">選択順</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="univOrder" value="2" <c:if test="${form.univOrder == '2'}">checked</c:if> ></td>
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
    <td><span class="text12">コード順</span></td>
	  --%>
    <td><span class="text12">大学区分・名称順</span></td>
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c002,c003,c004,c005,c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">対象クラス</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt="">
    <select name="printClass" class="text12">
        <option value=""<c:if test="${ empty form.printClass }"> selected</c:if>>担当クラスすべて</option>
        <c:forEach var="class" items="${ChargeClass.classList}">
          <option value="<c:out value="${class.key}" />"<c:if test="${form.printClass == class.key}"> selected</c:if>><c:out value="${class.grade}" />年<c:out value="${class.className}" /></option>
        </c:forEach>
    </select></td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->

<!--set-->
<c:forTokens var="screen" items="c004" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">変動幅</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
	<c:choose>
	<c:when test="${NewExam}">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="0" <c:if test="${form.fluctRange == '0'}">checked</c:if> ></td>
    <td><span class="text12">10以上</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="1" <c:if test="${form.fluctRange == '1'}">checked</c:if> ></td>
    <td><span class="text12">20以上</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="2" <c:if test="${form.fluctRange == '2'}">checked</c:if> ></td>
    <td><span class="text12">40以上</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
	</c:when>
	<c:otherwise>
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="0" <c:if test="${form.fluctRange == '0'}">checked</c:if> ></td>
    <td><span class="text12">2.5以上</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="1" <c:if test="${form.fluctRange == '1'}">checked</c:if> ></td>
    <td><span class="text12">5.0以上</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="fluctRange" value="2" <c:if test="${form.fluctRange == '2'}">checked</c:if> ></td>
    <td><span class="text12">10.0以上</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
	</c:otherwise>
	</c:choose>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->
<c:forTokens var="screen" items="c105" delims=",">
  <c:if test="${ param.forward == screen && CurrentYear - ExamData.examYear < 6 && not HideUnivYear1 }">
	<c:set var="checked1" value="" />
	<c:set var="checked2" value="" />
	<c:set var="checked3" value="" />
	<c:set var="checked4" value="" />
	<c:set var="checked5" value="" />
	<c:set var="checked6" value="" />
	<c:forEach var="value" items="${form.dispUnivYear}">
		<c:if test="${ value == 1 }"><c:set var="checked1" value=" checked" /></c:if>
		<c:if test="${ value == 2 }"><c:set var="checked2" value=" checked" /></c:if>
		<c:if test="${ value == 3 }"><c:set var="checked3" value=" checked" /></c:if>
		<c:if test="${ value == 4 }"><c:set var="checked4" value=" checked" /></c:if>
		<c:if test="${ value == 5 }"><c:set var="checked5" value=" checked" /></c:if>
		<c:if test="${ value == 6 }"><c:set var="checked6" value=" checked" /></c:if>
	</c:forEach>
	<tr>
	<td width="22%" bgcolor="#E1E6EB" valign="top">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><b class="text12">過年度の表示<BR>（最大4つまで）</b></td>
	</tr>
	</table>
	</td>
	<td width="78%" bgcolor="#F4E5D6">
	<table border="0" cellpadding="0" cellspacing="2" width="100%">
	<tr>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td width="22"><input type="checkbox" name="dispUnivYear" value="1" onclick="dispUnivYearChecked(this)"<c:out value="${checked1}" />></td>
	<td><span class="text12"><c:out value="${ ExamData.examYear - 1 }" />年度</span></td>
	</tr>
	</table>
	</td>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<c:choose>
		<c:when test="${ CurrentYear - ExamData.examYear < 5 && not HideUnivYear2 }">
			<td width="22"><input type="checkbox" name="dispUnivYear" value="2" onclick="dispUnivYearChecked(this)"<c:out value="${checked2}" />></td>
			<td><span class="text12"><c:out value="${ ExamData.examYear - 2 }" />年度</span></td>
		</c:when>
		<c:otherwise>
			<td width="22"><br></td>
			<td><br></td>
		</c:otherwise>
	</c:choose>
	</tr>
	</table>
	</td>
	<td width="34%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<c:choose>
		<c:when test="${ CurrentYear - ExamData.examYear < 4 && not HideUnivYear3 }">
			<td width="22"><input type="checkbox" name="dispUnivYear" value="3" onclick="dispUnivYearChecked(this)"<c:out value="${checked3}" />></td>
			<td><span class="text12"><c:out value="${ ExamData.examYear - 3 }" />年度</span></td>
		</c:when>
		<c:otherwise>
			<td width="22"><br></td>
			<td><br></td>
		</c:otherwise>
	</c:choose>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<c:choose>
		<c:when test="${ CurrentYear - ExamData.examYear < 3 && not HideUnivYear4 }">
			<td width="22"><input type="checkbox" name="dispUnivYear" value="4" onclick="dispUnivYearChecked(this)"<c:out value="${checked4}" />></td>
			<td><span class="text12"><c:out value="${ ExamData.examYear - 4 }" />年度</span></td>
		</c:when>
		<c:otherwise>
			<td width="22"><br></td>
			<td><br></td>
		</c:otherwise>
	</c:choose>
	</tr>
	</table>
	</td>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<c:choose>
		<c:when test="${ CurrentYear - ExamData.examYear < 2 && not HideUnivYear5 }">
			<td width="22"><input type="checkbox" name="dispUnivYear" value="5" onclick="dispUnivYearChecked(this)"<c:out value="${checked5}" />></td>
			<td><span class="text12"><c:out value="${ ExamData.examYear - 5 }" />年度</span></td>
		</c:when>
		<c:otherwise>
			<td width="22"><br></td>
			<td><br></td>
		</c:otherwise>
	</c:choose>
	</tr>
	</table>
	</td>
	<td width="34%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<c:choose>
		<c:when test="${ CurrentYear - ExamData.examYear < 1 && not HideUnivYear6 }">
			<td width="22"><input type="checkbox" name="dispUnivYear" value="6" onclick="dispUnivYearChecked(this)"<c:out value="${checked6}" />></td>
			<td><span class="text12"><c:out value="${ ExamData.examYear - 6 }" />年度</span></td>
		</c:when>
		<c:otherwise>
			<td width="22"><br></td>
			<td><br></td>
		</c:otherwise>
	</c:choose>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
  </c:if>
</c:forTokens>
<!--set-->
<c:forTokens var="screen" items="c105" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">大学の表示順序</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="univOrder" value="1" <c:if test="${form.univOrder == '1'}">checked</c:if> ></td>
    <td><span class="text12">選択順</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="univOrder" value="2" <c:if test="${form.univOrder == '2'}">checked</c:if> ></td>
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
    <td><span class="text12">コード順</span></td>
	  --%>
    <td><span class="text12">大学区分・名称順</span></td>
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
    </tr>
    </table>
    </td>
    <td width="34%"></td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->
<!--set-->
<c:forTokens var="screen" items="c105" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB" valign="top">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">大学集計区分</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <%-- 2019/07/18 QQ)kitaoka システム企画 UPD START --%>
    <%--
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="1" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '1'}"> checked</c:if><c:if test="${ isUniv12Exam }"> disabled=disabled</c:if>></td>
    <td><span class="text12">大学（日程あり）</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="3" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '3'}"> checked</c:if><c:if test="${ isUniv12Exam }"> disabled=disabled</c:if>></td>
    <td><span class="text12">学部（日程あり）</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="5" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '5'}"> checked</c:if> ></td>
    <td><span class="text12">学科</span></td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="2" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '2'}"> checked</c:if> ></td>
    <td><span class="text12">大学（日程なし）</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="4" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '4'}"> checked</c:if> ></td>
    <td><span class="text12">学部（日程なし）</span></td>
    </tr>
    </table>
    </td>
    <td width="34%"></td>
    </tr>
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
    </tr>--%>
    <%-- 2019/07/18 QQ)kitaoka システム企画 UPD END --%>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="3" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '3'}"> checked</c:if>></td>
    <td><span class="text12">学部</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="radio" name="totalUnivDiv" value="5" onclick="dispUnivCountChecked(this)"<c:if test="${form.totalUnivDiv == '5'}"> checked</c:if>></td>
    <td><span class="text12">学科</span></td>
    </tr>
    </table>
    </td>
    <td width="34%"></td>
    </tr>
    <tr>
    <td colspan="3">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><br></td>
    <td width="22"><input type="checkbox" name="dispUnivSchedule" value="1"<c:if test="${ form.dispUnivSchedule == '1' }"> checked</c:if>></td>
    <td><span class="text12">国公立大は前期日程のみ表示</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--/set-->
<c:forTokens var="screen" items="c105" delims=",">
  <c:if test="${ param.forward == screen }">
    <tr>
    <td width="22%" bgcolor="#E1E6EB">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
    <td><b class="text12">評価区分</b></td>
    </tr>
    </table>
    </td>
    <td width="78%" bgcolor="#F4E5D6">
    <table border="0" cellpadding="0" cellspacing="2" width="100%">
    <tr>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="checkbox" name="dispUnivRating" value="1"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
    <td>
    <span class="text12">共通テスト</span></td>
    </tr>
    </table>
    </td>
    <td width="33%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="checkbox" name="dispUnivRating" value="2"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
    <td><span class="text12">２次・私大</span></td>
    </tr>
    </table>
    </td>
    <td width="34%">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="22"><input type="checkbox" name="dispUnivRating" value="3"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '3' }"> checked</c:if></c:forEach>></td>
    <td><span class="text12">総合</span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
  </c:if>
</c:forTokens>
<!--set-->
<c:forTokens var="screen" items="c105" delims=",">
  <c:if test="${ param.forward == screen }">
	<tr>
	<td width="22%" bgcolor="#E1E6EB">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><b class="text12">表示対象</b></td>
	</tr>
	</table>
	</td>
	<td width="78%" bgcolor="#F4E5D6">
	<table border="0" cellpadding="0" cellspacing="2" width="100%">
	<tr>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td width="22"><input type="checkbox" name="dispUnivStudent" value="1"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
	<td><span class="text12">現役生</span></td>
	</tr>
	</table>
	</td>
	<td width="33%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td width="22"><input type="checkbox" name="dispUnivStudent" value="2"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
	<td><span class="text12">高卒生</span></td>
	</tr>
	</table>
	</td>
	<td width="34%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td width="22"><input type="checkbox" name="dispUnivStudent" value="0"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '0' }"> checked</c:if></c:forEach>></td>
	<td><span class="text12">現役生＋高卒生</span></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
  </c:if>
</c:forTokens>
<!--/set-->
<!--set-->
<c:forTokens var="screen" items="c102,c103,c104,c105" delims=",">
  <c:if test="${ param.forward == screen }">
     <tr>
     <td width="22%" bgcolor="#E1E6EB">
     <table border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
     <td><b class="text12">クラスの表示順序</b></td>
     </tr>
     </table>
     </td>
     <td width="78%" bgcolor="#F4E5D6">
     <table border="0" cellpadding="0" cellspacing="2" width="100%">
     <tr>
     <td width="33%">
     <table border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td width="22">
         <input type="radio" name="classOrder" value="1" <c:if test="${form.classOrder == '1'}">checked</c:if> >
     </td>
     <td><span class="text12">選択順</span></td>
     </tr>
     </table>
     </td>
     <td width="33%">
     <table border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td width="22">
         <input type="radio" name="classOrder" value="2" <c:if test="${form.classOrder == '2'}">checked</c:if> >
     </td>
     <td><span class="text12">コード順</span></td>
     </tr>
     </table>
     </td>
     <td width="34%"></td>
     </tr>
     </table>
     </td>
     </tr>
  </c:if>
</c:forTokens>
<!--/set-->
</table>
<!--/リスト-->

</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/表示項目の選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
<!--/表示・印刷形式の選択-->

<c:forEach var="anaItem" items="${form.analyzeItem}">
  <c:if test="${anaItem == 'deviation'}"><c:set var="deviation" value=" checked "/></c:if>
  <c:if test="${anaItem == 'univ'}"><c:set var="univ" value=" checked "/></c:if>
</c:forEach>

<c:forTokens var="screen" items="c004,c006" delims=",">
  <c:if test="${ param.forward == screen && not NewExam }">
    <!--分析対象の絞込み-->
    <tr valign="top">
    <td width="648" bgcolor="#EFF2F3" align="center">

    <div style="margin-top:7px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="42"><img src="./shared_lib/img/parts/icon_hyoji.gif" width="30" height="30" border="0" alt="分析対象の絞込み" hspace="3"></td>
    <td width="578"><b class="text14" style="color:#657681;">分析対象の絞込み</b></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">
    <!---->
    <div style="margin-top:15px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr valign="top">
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576" align="center">

    <c:forTokens var="screen" items="c004" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--一段目-->
        <table border="0" cellpadding="0" cellspacing="0" width="573">
        <tr valign="top">
        <td width="187">
        <!--偏差値-->
        <table border="0" cellpadding="0" cellspacing="0" width="187">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="187">
        <tr>
        <td width="185" height="26" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
            <td><input type="checkbox" name="analyzeItem" value="deviation" onClick="javascript:change(this)" <c:out value="${deviation}"/> ></td>
        <td><span class="text12">偏差値</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="185" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:9px;">
        <table border="0" cellpadding="0" cellspacing="0" width="171">
        <tr valign="top">
        <td width="93"><!--イラスト--><img src="./class/img/graph06.gif" width="85" height="63" border="0" alt="偏差値"><br></td>
        <td width="78">
        <select name="deviation" class="text12" style="width:78px;">
            <option value="1" <c:if test="${form.deviation == '1'}">selected</c:if> >40.0以上</option>
            <option value="2" <c:if test="${form.deviation == '2'}">selected</c:if> >45.0以上</option>
            <option value="3" <c:if test="${form.deviation == '3'}">selected</c:if> >50.0以上</option>
            <option value="4" <c:if test="${form.deviation == '4'}">selected</c:if> >55.0以上</option>
            <option value="5" <c:if test="${form.deviation == '5'}">selected</c:if> >60.0以上</option>
            <option value="6" <c:if test="${form.deviation == '6'}">selected</c:if> >65.0以上</option>
            <option value="7" <c:if test="${form.deviation == '7'}">selected</c:if> >70.0以上</option>
            <option value="8" <c:if test="${form.deviation == '8'}">selected</c:if> >75.0以上</option>
        </select>
        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/偏差値-->
        </td>
        <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
        <td width="380" colspan="3" valign="middle"><span class="text12">表示対象を英語の偏差値で絞り込むことができます。</span></td>
        </tr>
        </table>
        <!--/一段目-->

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
      </c:if>
    </c:forTokens>

    <c:forTokens var="screen" items="c006" delims=",">
      <c:if test="${ param.forward == screen }">
        <!--志望大学-->
        <table border="0" cellpadding="0" cellspacing="0" width="571">
        <tr valign="top">
        <td bgcolor="#8CA9BB">
        <table border="0" cellpadding="0" cellspacing="1" width="571">
        <tr>
        <td width="569" height="26" bgcolor="#E5EEF3">
        <table border="0" cellpadding="0" cellspacing="3">
        <tr>
        <td>
        <c:set value="false" var="flag"/>
            <input type="checkbox" name="analyzeItem" value="univ" onClick="javascript:change(this)" <c:out value="${univ}"/> >
        </td>
        <td><span class="text12">志望大学</span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr valign="top">
        <td width="569" bgcolor="#FFFFFF" align="center">

        <div style="margin-top:7px;">
        <table border="0" cellpadding="0" cellspacing="0" width="569">
        <tr valign="top">
        <td width="9"><img src="./shared_lib/img/parts/sp.gif" width="9" height="1" border="0" alt=""><br></td>
        <td width="93"><!--イラスト--><img src="./class/img/graph07.gif" width="85" height="63" border="0" alt="志望大学" vspace="2"><br></td>
        <td width="467">

        <table border="0" cellpadding="0" cellspacing="2" width="458">
        <tr height="27">
        <td bgcolor="#E1E6EB"><input type="radio" name="choiceUnivMode" value="1" onClick="javascript:change(this)" <c:if test="${form.choiceUnivMode == '1'}">checked</c:if> ></td>
        <td bgcolor="#E1E6EB"><b class="text12">&nbsp;志望上位10校</b></td>
        <td bgcolor="#F4E5D6"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><span class="text12">※志望者が多かった順に10校表示されます。</span></td>
        </tr>
        <tr height="27">
        <td bgcolor="#E1E6EB"><input type="radio" name="choiceUnivMode" value="2" onClick="javascript:change(this)" <c:if test="${form.choiceUnivMode == '2'}">checked</c:if> ></td>
        <td bgcolor="#E1E6EB"><b class="text12">&nbsp;志望者数（範囲指定）</b></td>
        <td bgcolor="#F4E5D6"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><select name="candRange" class="text12">
        <option value="1" <c:if test="${form.candRange == '1'}">selected</c:if> >5人以上</option>
        <option value="2" <c:if test="${form.candRange == '2'}">selected</c:if> >10人以上</option>
        <option value="3" <c:if test="${form.candRange == '3'}">selected</c:if> >15人以上</option>
        <option value="4" <c:if test="${form.candRange == '4'}">selected</c:if> >20人以上</option>
        <option value="0" <c:if test="${form.candRange == '0'}">selected</c:if> >全て</option>
        </select></td>
        </tr>
        <tr height="27">
        <td bgcolor="#E1E6EB"><input type="radio" name="choiceUnivMode" value="3" onClick="javascript:change(this)" <c:if test="${form.choiceUnivMode == '3'}">checked</c:if> ></td>
        <td bgcolor="#E1E6EB"><b class="text12">&nbsp;評価基準</b></td>
        <td bgcolor="#F4E5D6">

        <c:forEach var="evaItem" items="${form.evaluation}">
          <c:if test="${evaItem == 'A'}"><c:set var="evaA" value=" checked " /></c:if>
          <c:if test="${evaItem == 'B'}"><c:set var="evaB" value=" checked " /></c:if>
          <c:if test="${evaItem == 'C'}"><c:set var="evaC" value=" checked " /></c:if>
          <c:if test="${evaItem == 'D'}"><c:set var="evaD" value=" checked " /></c:if>
          <c:if test="${evaItem == 'E'}"><c:set var="evaE" value=" checked " /></c:if>
        </c:forEach>

        <table border="0" cellpadding="2" cellspacing="0">
        <tr>
        <td><input type="checkbox" name="evaluation" value="A"<c:out value="${evaA}" />></td>
        <td><span class="text12">A評価</span></td>
        <td><input type="checkbox" name="evaluation" value="B"<c:out value="${evaB}" />></td>
        <td><span class="text12">B評価</span></td>
        <td><input type="checkbox" name="evaluation" value="C"<c:out value="${evaC}" />></td>
        <td><span class="text12">C評価</span></td>
        <td><input type="checkbox" name="evaluation" value="D"<c:out value="${evaD}" />></td>
        <td><span class="text12">D評価</span></td>
        <td><input type="checkbox" name="evaluation" value="E"<c:out value="${evaE}" />></td>
        <td><span class="text12">E評価</span></td>
        </tr>
        </table>

        </td>
        </tr>
        </table>

        </td>
        </tr>
        </table>
        </div>

        <!--spacer-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr valign="top">
        <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
        </tr>
        </table>
        <!--/spacer-->
        </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
        <!--/志望大学-->
      </c:if>
    </c:forTokens>

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--/-->
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    <!--/分析対象の絞込み-->
  </c:if>
</c:forTokens>

<%-- c004: 新テストで分析対象の絞込みが消えるので補完作業 --%>
<c:if test="${ param.forward ==  'c004' && NewExam }">
	<c:forEach var="anaItem" items="${form.analyzeItem}">
		<c:if test="${anaItem == 'deviation'}"><input type="hidden" name="analyzeItem" value="deviation"></c:if>
	</c:forEach>
	<input type="hidden" name="deviation" value="<c:out value="${form.deviation}" />">
</c:if>

<c:forTokens var="screen" items="c002,c102,c103,c104,c105" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--セキュリティスタンプの選択1-->
    <tr valign="top">
    <td width="648" bgcolor="#EFF2F3" align="center">

    <div style="margin-top:7px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
    <td width="578"><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <div style="margin-top:8px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr>
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576">

    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="stamp" value="1" <c:if test="${form.stamp == '1'}">checked</c:if> ></td>
    <td width="88"><span class="text12">機密</span></td>
        <td width="22"><input type="radio" name="stamp" value="2" <c:if test="${form.stamp == '2'}">checked</c:if> ></td>
    <td width="88"><span class="text12">部内限り</span></td>
        <td width="22"><input type="radio" name="stamp" value="3" <c:if test="${form.stamp == '3'}">checked</c:if> ></td>
    <td width="88"><span class="text12">校内限り</span></td>
        <td width="22"><input type="radio" name="stamp" value="4" <c:if test="${form.stamp == '4'}">checked</c:if> ></td>
    <td width="88"><span class="text12">取扱注意</span></td>
        <td width="22"><input type="radio" name="stamp" value="5" <c:if test="${form.stamp == '5'}">checked</c:if> ></td>
    <td width="88"><span class="text12">なし</span></td>
    </tr>
    </table>

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    <!--/セキュリティスタンプの選択1-->
  </c:if>
</c:forTokens>

<c:forTokens var="screen" items="c003,c004,c005,c006" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--セキュリティスタンプの選択2-->
    <tr valign="top">
    <td width="648" bgcolor="#EFF2F3" align="center">

    <div style="margin-top:7px;">
    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr>
    <td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
    <td width="578"><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF">

    <div style="margin-top:8px;">
    <table border="0" cellpadding="0" cellspacing="0" width="618">
    <tr>
    <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
    <td width="576">

    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="22"><input type="radio" name="stamp" value="1" <c:if test="${form.stamp == '1'}">checked</c:if> ></td>
    <td width="88"><span class="text12">機密</span></td>
        <td width="22"><input type="radio" name="stamp" value="5" <c:if test="${form.stamp == '5'}">checked</c:if> ></td>
    <td width="88"><span class="text12">なし</span></td>
    </tr>
    </table>

    </td>
    <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </div>
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    <!--/セキュリティスタンプの選択2-->
  </c:if>
</c:forTokens>

</table>
</td>
</tr>
</table>
<!--/個別項目の設定-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="javascript:download_saveFlgOn()"><img src="./shared_lib/img/btn/hozon_w.gif" width="114" height="35" border="0" alt="保存"></a><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="175">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="175"><img src="./shared_lib/img/illust/one_point.gif" width="173" height="84" border="0" alt="ワンポイントアドバイス！!"><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ワンポイントアドバイス-->
<%@ include file="/jsp/shared_lib/Onepoint.jsp" %>
<!--/ワンポイントアドバイス-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="43"><img src="./shared_lib/img/parts/sp.gif" width="43" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

</form>
</body>
</html>

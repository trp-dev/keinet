<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set value="class" var="category"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／クラス成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/submit_top.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/open_charge.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/graph_checker.jsp" %>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

    function validate() {
        if (count == 0) {
            alert("<kn:message id="s001a" />");
            return false;
        }

        if (flag) {
            alert("設定完了ボタンを押してください。");
            return false;
        }

        var message = "";

        // 担当クラス
        <c:if test="${ empty ChargeClass.classList }">
            message += "<kn:message id="c001a" />\n";
        </c:if>

		<c:choose>
			<%-- 科目だけの画面を含む場合 --%>
			<c:when test="${ Profile.categoryMap['030104']['1301'] == 1 || Profile.categoryMap['030203']['1301'] == 1 }">
				if ("<c:out value="${ComSetStatusBean.statusMap['course']}" />" == "02") {
					message += "<kn:message id="s003a" />\n";
				}
			</c:when>
			<%-- それ以外 --%>
			<c:otherwise>
				var typeCount = <c:out value="${ SubjectCountBean.typeCount }" />;
				if (("<c:out value="${ComSetStatusBean.statusMap['type']}" />" == "02" || typeCount == 0)
						&& "<c:out value="${ComSetStatusBean.statusMap['course']}" />" == "02") {
					message += "<kn:message id="s012a" />\n";
				}
			</c:otherwise>
		</c:choose>

        if ("<c:out value="${ComSetStatusBean.statusMap['univ']}" />" == "02") {
            message += "<kn:message id="s005a" />\n";
        }

        if ("<c:out value="${ComSetStatusBean.statusMap['class']}" />" == "02") {
            message += "<kn:message id="s006a" />\n";
        }

        if (message != "") {
            alert(message);
            return false;
        }

		<%-- グラフ出力対象チェック --%>
		var gc = new GraphChecker();
		<%-- クラス成績概況 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="c002" item="type" />, <kn:graph id="c002" item="course" />, "<kn:message id="gcC002" />");
		<%-- クラス比較 - 成績概況 --%>
		gc.checkSubject(<kn:graph id="c102" item="type" />, <kn:graph id="c102" item="course" />, "<kn:message id="gcC102" />");
		gc.checkClass(<kn:graph id="c102" item="class" />);
		<%-- クラス比較 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="c103" item="type" />, <kn:graph id="c103" item="course" />, "<kn:message id="gcC103" />");
		gc.checkClass(<kn:graph id="c103" item="class" />);
		<%-- クラス比較 - 設問別成績 --%>
		gc.checkCourse(<kn:graph id="c104" item="course" />, "<kn:message id="gcC104" />");
		gc.checkClass(<kn:graph id="c104" item="class" />);

		if (gc.hasError()) {
			alert(gc.createMessage());
			return false;
		}

        return true;
    }

    // 画面表示段階でのチェック数
    var count = 0;
    <c:if test="${ Profile.categoryMap['030101']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030102']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030103']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030104']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030105']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030201']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030202']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030203']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['030204']['1301'] == 1 }">count++;</c:if>
    // チェックを加えたかどうか
    var flag = false;

    function change(obj) {
        // 変更フラグ
        document.forms[0].changed.value = "1";

        // カウントを増減
        if (obj.checked) {
            count++;
            flag = true;
        } else {
            count--;
        }
    }

    function init() {
    	startTimer();
        // スクロール位置を初期化する
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
    }

// -->
</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ClassTop" />"method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="590">
<!--●●●左側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="21" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="184"><img src="./class/img/ttl_class.gif" width="173" height="25" border="0" alt="クラス成績分析"><br></td>
<td width="1"><!-- <img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""> --><br></td>
<td width="10"><!-- <img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""> --><br></td>
<td width="405"><!-- <img src="./class/img/ttl_class_read.gif" width="261" height="16" border="0" alt="クラス単位での成績分析を行います。"> --><br></td>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--コンテンツメニュー-->
<table border="0" cellpadding="0" cellspacing="0" width="590" height="38">
<tr>
<%-- メニュー --%>
<%-- クラス成績概況 --%>
<c:choose>
  <c:when test="${param.forward == 'c001'}">
    <td width="114"><img src="./shared_lib/img/btn/cur/class_menu_01.gif" width="114" height="38" border="0" alt="クラス成績概況"></td>
  </c:when>
  <c:otherwise>
    <td width="114"><a href="javascript:submitMenu('c001')"><img src="./shared_lib/img/btn/def/class_menu_01.gif" width="114" height="38" border="0" alt="クラス成績概況"></a></td>
  </c:otherwise>
</c:choose>
<td width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
<%-- クラス比較 --%>
<c:choose>
  <c:when test="${param.forward == 'c101'}">
    <td width="114"><img src="./shared_lib/img/btn/cur/class_menu_02.gif" width="114" height="38" border="0" alt="クラス比較"></td>
  </c:when>
  <c:otherwise>
    <td width="114"><a href="javascript:submitMenu('c101')"><img src="./shared_lib/img/btn/def/class_menu_02.gif" width="114" height="38" border="0" alt="クラス比較"></a></td>
  </c:otherwise>
</c:choose>
<td width="357" background="./shared_lib/img/parts/contents_menu_span.gif"><img src="./shared_lib/img/parts/sp.gif" width="357" height="38" border="0" alt=""></td>
</tr>
</table>
<!--/コンテンツメニュー-->





<c:forEach var="item" items="${form.outItem}">
    <c:if test="${item == 'CondDev'}"><c:set var="CondDev" value=" checked "/></c:if>
    <c:if test="${item == 'CondPersonal'}"><c:set var="CondPersonal" value=" checked "/></c:if>
    <c:if test="${item == 'CondTrans'}"><c:set var="CondTrans" value=" checked "/></c:if>
    <c:if test="${item == 'CondQue'}"><c:set var="CondQue" value=" checked "/></c:if>
    <c:if test="${item == 'CondUniv'}"><c:set var="CondUniv" value=" checked "/></c:if>
</c:forEach>

<!--概要-->
<c:if test="${ param.forward == 'c001'}">
  <table border="0" cellpadding="0" cellspacing="0" width="590">
  <tr>
  <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
  <td width="588" bgcolor="#EFF2F3" align="center">

  <!--説明-->
  <div style="margin-top:10px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td><span class="text14">クラス全体や個人の成績を幅広く分析します。</span></td>
  </tr>
  </table>
  </div>
  <!--/説明-->

  <!--担当クラス-->
  <div style="margin-top:10px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="2" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">

  <table border="0" cellpadding="0" cellspacing="0" width="554">
  <tr>
  <td width="90" bgcolor="#758A98" align="center"><b class="text12" style="color:#FFFFFF">担当クラス</b></td>
  <td width="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
  <td width="462" bgcolor="#F4E5D6">
  <table border="0" cellpadding="3" cellspacing="0" width="462">
  <tr>
  <td><span class="text12"><c:choose>
    <c:when test="${ empty ChargeClass.classList }">設定なし</c:when>
    <c:otherwise>
      <c:out value="${ChargeClass.year}" />年度
      <c:forEach var="class" items="${ChargeClass.classList}">
        &nbsp;<c:out value="${class.grade}" />年<c:out value="${class.className}" />
      </c:forEach>
    </c:otherwise>
  </c:choose></span></td>
  <td align="right"><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:75px;" onClick="openCharge()"></td>
  </tr>
  </table>
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/担当クラス-->

  <!--偏差値分布-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_hensachi.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">偏差値分布</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030101']['0103'] == 0 || Profile.categoryMap['030101']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c002" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c002" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
  <input type="checkbox" name="outItem" value="CondDev" onClick="change(this);" <c:out value="${CondDev}"/>>
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c002" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c002')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c002')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c002')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/偏差値分布-->

  <!--個人成績-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_kojin.gif" width="140" height="84" border="0" alt="個人成績"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271"><b class="text16" style="color:#657681;">個人成績</b></td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c003" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c003" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CondPersonal" onClick="change(this)" <c:out value="${CondPersonal}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c003" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">該当クラスの成績を一覧表で確認します。<BR><BR></span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c003')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c003')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c003')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/個人成績-->

  <!--個人成績推移-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_suii.gif" width="140" height="84" border="0" alt="個人成績推移"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">個人成績推移</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030103']['0103'] == 0 || Profile.categoryMap['030103']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c004_top" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c004_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c004_top" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CondTrans" onClick="change(this)" <c:out value="${CondTrans}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c004_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c004_top" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c004_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c004_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">該当クラスの生徒の成績推移を一覧表で確認します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c004')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c004')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c004')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/個人成績推移-->

  <!--設問別個人成績-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_setsumon.gif" width="140" height="84" border="0" alt="設問別個人成績"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">設問別個人成績</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030104']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c005" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c005" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CondQue" onClick="change(this)" <c:out value="${CondQue}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c005" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">各科目の設問毎の得点、得点率を表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c005')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c005')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c005')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/設問別個人成績-->

  <!--志望大学別個人評価-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_daigaku.gif" width="140" height="84" border="0" alt="志望大学別個人評価"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271"><b class="text16" style="color:#657681;">志望大学別個人評価</b></td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c006" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c006" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c006" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CondUniv" onClick="change(this)" <c:out value="${CondUniv}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c006" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c006" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c006" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c006" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">大学別の志望者と評価を表示します。<BR><BR></span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c006')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c006')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c006')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/志望大学別個人評価-->

  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/概要-->
  <table border="0" cellpadding="0" cellspacing="0" width="590">
  <tr>
  <td width="590" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="590" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
</c:if>






<!--概要（クラス成績概況）-->

<c:forEach var="item" items="${form.outItem}">
    <c:if test="${item == 'CompScore'}"><c:set var="CompScore" value=" checked "/></c:if>
    <c:if test="${item == 'CompDev'}"><c:set var="CompDev" value=" checked "/></c:if>
    <c:if test="${item == 'CompQue'}"><c:set var="CompQue" value=" checked "/></c:if>
    <c:if test="${item == 'CompUniv'}"><c:set var="CompUniv" value=" checked "/></c:if>
</c:forEach>

<!--概要-->
<c:if test="${ param.forward == 'c101' }">
  <table border="0" cellpadding="0" cellspacing="0" width="590">
  <tr>
  <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
  <td width="588" bgcolor="#EFF2F3" align="center">

  <!--説明-->
  <div style="margin-top:10px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td><span class="text14">クラス間で成績を比較分析します。</span></td>
  </tr>
  </table>
  </div>
  <!--/説明-->

  <%-- 中項目「クラス成績概況」選択時のみ出力する。
  <!--担当クラス-->
  <div style="margin-top:10px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="2" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">

  <table border="0" cellpadding="0" cellspacing="0" width="554">
  <tr>
  <td width="90" bgcolor="#758A98" align="center"><b class="text12" style="color:#FFFFFF">担当クラス</b></td>
  <td width="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
  <td width="462" bgcolor="#F4E5D6">
  <table border="0" cellpadding="3" cellspacing="0" width="462">
  <tr>
  <td><span class="text12">&nbsp;3年A組&nbsp;3年B組</span></td>
  <td align="right"><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:75px;"></td>
  </tr>
  </table>
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/担当クラス-->--%>

  <!--成績概況-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_gaikyo2.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">成績概況</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030201']['0103'] == 0 || Profile.categoryMap['030201']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c102" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c102" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CompScore" onClick="change(this)" <c:out value="${CompScore}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c102" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c102')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c102')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c102')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/成績概況-->

  <!--偏差値分布-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_hensachi2.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">偏差値分布</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030202']['0103'] == 0 || Profile.categoryMap['030202']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c103" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c103" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CompDev" onClick="change(this)" <c:out value="${CompDev}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c103" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c103')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c103')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c103')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/個人成績-->

  <!--設問別成績-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_setsumon2.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271">
    <b class="text16" style="color:#657681;">設問別成績</b>
    <%-- 個別設定アイコン--%>
    <c:if test="${ Profile.categoryMap['030203']['0203'] == 0 }">
      <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
    </c:if>
  </td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c104" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c104" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CompQue" onClick="change(this)" <c:out value="${CompQue}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c104" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">各科目の設問毎の受験人数、平均得点率を表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c104')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c104')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c104')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/個人成績推移-->

  <!--志望大学評価別人数-->
  <div style="margin-top:9px;">
  <table border="0" cellpadding="0" cellspacing="0" width="560">
  <tr>
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="9" cellspacing="1" width="560">
  <tr>
  <td width="558" bgcolor="#FFFFFF">


  <table border="0" cellpadding="0" cellspacing="0" width="540">
  <tr>
  <td width="140"><!--画像--><img src="./class/img/g_shibou2.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  <td width="389">
  <!--タイトル-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
  <td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
  <td width="271"><b class="text16" style="color:#657681;">志望大学評価別人数</b></td>
  <td width="105" valign="top">
  <!--一括出力対象-->
  <table border="0" cellpadding="0" cellspacing="0" width="105">
  <tr bgcolor="<kn:exam screen="c105" out="color" />" valign="top">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="c105" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td rowspan="2" width="101">

  <table border="0" cellpadding="0" cellspacing="0" width="101">
  <tr bgcolor="<kn:exam screen="c105" out="color" />" valign="top">
  <td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
  <td width="100">
      <input type="checkbox" name="outItem" value="CompUniv" onClick="change(this)" <c:out value="${CompUniv}"/> >
  <span class="text12-hh">一括出力対象</span></td>
  </tr>
  </table>

  </td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="c105" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  <tr bgcolor="<kn:exam screen="c105" out="color" />" valign="bottom">
  <td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="c105" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  <td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="c105" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/一括出力対象-->
  </td>
  </tr>
  </table>
  <!--/タイトル-->

  <!--ボーダー-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/ボーダー-->

  <!--下部-->
  <table border="0" cellpadding="0" cellspacing="0" width="389">
  <tr>
  <td width="270">

  <table border="0" cellpadding="0" cellspacing="0" width="270">
  <tr>
  <td width="270"><span class="text14">大学別の志望者数、評価別人数を表示します。</span></td>
  </tr>
  <tr>
  <td width="270">
  <div style="margin-top:5px;">
  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="12"><a href="javascript:openSample('c105')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
  <td><span class="text12"><a href="javascript:openSample('c105')">サンプルの表示</a></span></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  </table>

  </td>
  <td width="119" align="right"><a href="javascript:submitMenu('c105')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
  </tr>
  </table>
  <!--/下部ー-->
  </td>
  </tr>
  </table>


  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
  </div>
  <!--/志望大学評価人数-->


  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/概要-->
  <table border="0" cellpadding="0" cellspacing="0" width="590">
  <tr>
  <td width="590" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="590" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
</c:if>


<!---->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="301">
<tr>
<td width="95"><img src="./shared_lib/img/parts/icon_set_waku.gif" width="87" height="25" border="0" alt="個別設定中"><br></td>
<td width="206"><span class="text10">上記メニューの中にこのマークがある場合、共通設定を利用しない項目を含みます。</span></td>
</tr>
</table>
</div>
<!---->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td width="289">

<!--●●●右側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="62" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--一括出力-->
<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td><img src="./shared_lib/img/parts/ttl_lump.gif" width="289" height="38" border="0" alt="一括出力"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="286" bgcolor="#F3F5F5" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="286">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="224"><div style="margin-top:8px;"><span class="text12">以下の手順に従って一括出力ができます。</span></div></td>
<td width="50"><img src="./shared_lib/img/parts/ttl_lump_parts.gif" width="50" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td nowrap><span class="text12">対象年度 ： </span></td>
<td><select name="targetYear" class="text12" style="width:55px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr>
<td nowrap><span class="text12">対象模試 ： </span></td>
<td><select name="targetExam" class="text12" style="width:200px;" onChange="changeExamSelect()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step1-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">出力メニューの設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="29"><img src="./shared_lib/img/parts/yaji_l.gif" width="24" height="26" border="0" alt="←"><br></td>
<td width="220"><span class="text12">左のメニューで一括出力対象を確認の上、下のボタンを押してください。</span></td>
</tr>
<tr>
<td colspan="2"><div style="margin-top:5px;"><a href="javascript:changeExamSelect()"><img src="./shared_lib/img/btn/finish.gif" width="249" height="24" border="0" alt="設定完了"></a></div></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step2-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step3_cur.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<!--ボタン-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="81"><a href="javascript:openCommon(1)"><img src="./shared_lib/img/btn/com_kata_<c:out value="${ComSetStatusBean.statusMap['type']}" />.gif" width="81" height="22" border="0" alt="型"></a><br></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><a href="javascript:openCommon(2)"><img src="./shared_lib/img/btn/com_kamoku_<c:out value="${ComSetStatusBean.statusMap['course']}" />.gif" width="81" height="22" border="0" alt="科目"></a><br></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><a href="javascript:openCommon(4)"><img src="./shared_lib/img/btn/com_daigaku_<c:out value="${ComSetStatusBean.statusMap['univ']}" />.gif" width="81" height="22" border="0" alt="志望大学"></a><br></td>
</tr>
<tr>
<td colspan="5" width="249"><img src="./shared_lib/img/parts/sp.gif" width="3" height="6" border="0" alt=""><br></td>
</tr>
<tr>
<td width="81"><a href="javascript:openCommon(5)"><img src="./shared_lib/img/btn/com_class_<c:out value="${ComSetStatusBean.statusMap['class']}" />.gif" width="81" height="22" border="0" alt="比較対象クラス"></a><br></td>
<td width="3"></td>
<td width="81"></td>
<td width="3"></td>
<td width="81"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="18"><img src="./shared_lib/img/parts/color_box_00.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">設定済み</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_01.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">要確認</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_02.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">未設定</span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step3-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step4_cur.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">個別項目の確認</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249"><span class="text12">各詳細画面で個別項目の設定を確認してください。</span></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step4-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step5-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step5_cur.gif" width="46" height="12" border="0" alt="STEP5"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">一括出力</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="116"><a href="javascript:download()"><img src="./shared_lib/img/btn/hozon.gif" width="116" height="37" border="0" alt="保存"></a><br></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step5-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="289" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="289" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="31"><img src="./shared_lib/img/parts/sp.gif" width="31" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp"%>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

</form>
</body>
</html>

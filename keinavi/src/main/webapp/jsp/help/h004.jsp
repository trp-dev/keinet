<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<jsp:useBean id="SearchBean" scope="request" class="jp.co.fj.keinavi.beans.help.HelpSearchBean" />
<jsp:useBean id="HelpListBean" scope="request" class="jp.co.fj.keinavi.beans.help.HelpListBean" />


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitTop() {
        document.forms[0].target = "_self";
        document.forms[0].action="<c:url value="HelpTop" />";
        document.forms[0].method = "POST";
        document.forms[0].forward.value = "h001";
        document.forms[0].backward.value = "";
        document.forms[0].submit();
    }

    function submitPage(page) {
        document.forms[0].forward.value = "h004";
        document.forms[0].backward.value = "h004";
        document.forms[0].freeWord.value = "<c:out value="${SearchBean.freeWord}" />";
        document.forms[0].category.value = "";
        document.forms[0].helpId.value = "";
        document.forms[0].page.value = page;
        document.forms[0].submit();
    }

    function submitNext(formId, helpId) {
        document.forms[0].forward.value = formId;
        document.forms[0].backward.value = "h004";
        document.forms[0].freeWord.value = "<c:out value="${SearchBean.freeWord}" />";
        document.forms[0].category.value = "";
        document.forms[0].helpId.value = helpId;
        document.forms[0].page.value = "<%=SearchBean.getPageNo()%>";
        document.forms[0].submit();
    }

    function submitBack(formId) {
        document.forms[0].forward.value = formId;
        document.forms[0].backward.value = "h004";
        document.forms[0].freeWord.value = "<c:out value="${SearchBean.freeWord}" />";
        document.forms[0].category.value = "";
        document.forms[0].helpId.value = "";
        document.forms[0].page.value = "1";
        document.forms[0].submit();
    }

//-->
</SCRIPT>

</head>


<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="HelpResult" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="freeWord" value="">
<input type="hidden" name="category" value="">
<input type="hidden" name="helpId" value="">
<input type="hidden" name="detailBack" value="h004">
<input type="hidden" name="page" value="">


<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_h001.html"-->
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="621">
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="621" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"  onClick="javascript:submitBack('h001')"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570"><img src="./help/img/ttl_help.gif" width="78" height="24" border="0" alt="ヘルプ"><br></td>
</tr>
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</table>
</div>
<!--/タイトル-->

<!-- パンくず -->
<%@ include file="/jsp/help/pankuzu.jsp" %>
<!--/パンくず -->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">フリーワード：<c:out value="${SearchBean.freeWord}" /></b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td width="550"><span class="text14">入力されたフリーワードが含まれているヘルプの一覧です。<br>探している内容に最も近いものをクリックしてください。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--上ページング-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#EFF2F3" align="center">
<!--表示-->
<table border="0" cellpadding="0" cellspacing="0" width="536">
<tr>
<td width="536" align="center"><span class="text12">
全<b><c:out value="${ SearchBean.recordCount }" /></b>件中　<c:out value="${ SearchBean.beginCount }" />〜<c:out value="${ SearchBean.endCount }" />件表示
</span></td>
</tr>
<c:if test="${ SearchBean.pageCount > 1 }">
<tr>
<td width="536"><img src="./shared_lib/img/parts/dot_blue.gif" width="536" height="1" border="0" alt="" vspace="6"><br></td>
</tr>
<tr>
<td width="536" align="center">
<!--ページングナビ-->
<%@ include file="/jsp/help/page.jsp" %>
<!--/ページングナビー-->
</td>
</tr>
</c:if>
</table>
<!--/表示-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/上ページング-->

<!--リスト-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr>
<td width="550" bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="550">
<!--set-->
<c:forEach var="help" items="${SearchBean.recordSet}">
    <tr>
        <td width="548" bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="540">
                <tr>
                    <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
                    <td width="535"><span class="text12-hh">
                    <a href="javascript:submitNext('h003', '<c:out value="${help.helpId}" />')"><c:out value="${help.titleStr}" /></a><br>
                    <c:out value="${help.explan}" />
                    </span></td>
                </tr>
            </table>
        </td>
    </tr>
</c:forEach>
<!--/set-->
</table>
</td>
</tr>
</table>
</div>
<!--/リスト-->

<!--下ページング-->
<c:if test="${ SearchBean.pageCount > 1 }">
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#EFF2F3" align="center">
<!--ページングナビ-->
<%@ include file="/jsp/help/page.jsp" %>
<!--/ページングナビー-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</c:if>
<!--/下ページング-->

<!--ボーダー-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->

<!--ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw_03.html"-->
<!--/FOOTER-->
</form>
</body>
</html>


<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／お問い合わせ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/JString.js"></script>
<SCRIPT type="text/javascript">
<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitConfirm() {

        // タイトル[必須]
        if ( document.forms[0].title.value.length == 0 ) {
            alert("必須項目を入力してください。");
            return false;
        }
        // 本文[必須]
        if ( document.forms[0].text.value.length == 0 ) {
            alert("必須項目を入力してください。");
            return false;
        }
        // 本文[全角３００文字以内]
		var js = new JString();
		js.setString(document.forms[0].text.value);
        if (js.getLength() > 600) {
            alert("本文は全角300文字までです。");
            return false;
        }
        // お名前[必須]
        if ( document.forms[0].name.value.length == 0 ) {
            alert("必須項目を入力してください。");
            return false;
        }

        // ご連絡先[必須] 但し営業部のみ
        <c:if test="${ LoginSession.userMode % 10 == 2 }">
            if ( document.forms[0].mailAddr.value.length == 0 ) {
                alert("必須項目を入力してください。");
                return false;
            }
        </c:if>

        document.forms[0].forward.value = "h102";
        document.forms[0].backward.value = "h101";
        document.forms[0].target = "_self";
        document.forms[0].action="<c:url value="InquireConfirm" />";
        document.forms[0].method = "POST";
        document.forms[0].submit();
    }

 
//-->
</SCRIPT>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="form01" />">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="actionMode" value="">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_h101.html"-->
<!--/ヘルプナビ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">お問い合わせ（入力フォーム）</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--説明-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="570"><span class="text14-hh">このページからKei-Naviに関するお問い合わせができます。<br>
※お問い合わせへの回答は数日かかることがございます。あらかじめご了承ください。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--説明-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td width="550"><span class="text14">下の各項目を記入し、「確認」ボタンを押してください。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--問い合わせフォーム-->
<table border="0" cellpadding="6" cellspacing="2" width="554">
<tr>
<td width="25%" bgcolor="#E1E6EB"><span class="text12">タイトル&nbsp;&nbsp;<font style="color:#FF6E0E">[必須］</font></span></td>
<td width="75%" bgcolor="#F4E5D6"><input type="text" size="40" class="text12" name="title" value="<c:out value="${form.title}"/>" style="width:390px;"></td>
</tr>
<tr>
<td bgcolor="#E1E6EB"><span class="text12">本文&nbsp;&nbsp;<font style="color:#FF6E0E">[必須］</font></span></td>
<td bgcolor="#F4E5D6"><textarea rows="6" cols="40" name="text" style="width:390px;"><c:out value="${form.text}"/></textarea><br>
<span class="text12">※全角300文字まで</span></td>
</tr>

<c:set var="name" value="" />
<c:if test="${ LoginSession.userMode % 10 == 1 }"><c:set var="name" value="学校名" /></c:if>
<c:if test="${ LoginSession.userMode % 10 == 2 }"><c:set var="name" value="営業部名" /></c:if>
<c:if test="${ LoginSession.userMode % 10 == 3 }"><c:set var="name" value="校舎名" /></c:if>
<tr>
<td bgcolor="#E1E6EB"><span class="text12"><c:out value="${name}"/></span></td>
<td bgcolor="#F4E5D6"><span class="text12"><c:out value="${LoginSession.userName}" /></span></td>
</tr>
<tr>
<td bgcolor="#E1E6EB"><span class="text12">お名前&nbsp;&nbsp;<font style="color:#FF6E0E">[必須］</font></span></td>
<td bgcolor="#F4E5D6"><input type="text" size="40" class="text12" name="name" value="<c:out value="${form.name}"/>"></td>
</tr>
<tr>
<td bgcolor="#E1E6EB"><span class="text12">ご連絡先電話番号</span></td>
<td bgcolor="#F4E5D6"><input type="text" size="40" class="text12" name="telNumber" value="<c:out value="${form.telNumber}"/>"></td>
</tr>

<c:set var="mode" value="" />
<c:if test="${ LoginSession.userMode % 10 == 2 }"><c:set var="mode" value="[必須]" /></c:if>
<tr>
<td bgcolor="#E1E6EB"><span class="text12">ご連絡先<br>メールアドレス&nbsp;&nbsp;<font style="color:#FF6E0E"><c:out value="${mode}"/></font></span></td>
<td bgcolor="#F4E5D6"><input type="text" size="40" class="text12" name="mailAddr" value="<c:out value="${form.mailAddr}"/>"></td>
</tr>
</table>
<!--/問い合わせフォーム-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="キャンセル" class="text12" style="width:100px;" onClick="window.close()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="確認" class="text12" style="width:100px;" onClick="javascript:submitConfirm()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、送信せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>

<jsp:useBean id="HelpListBean" scope="request" class="jp.co.fj.keinavi.beans.help.HelpListBean" />
<jsp:useBean id="OnepointBean" scope="request" class="jp.co.fj.keinavi.beans.help.OnepointBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitCategory(id) {
        document.forms[0].forward.value = "h002";
        document.forms[0].backward.value = "h001";
        document.forms[0].category.value = id;
        document.forms[0].page.value = "1";
        document.forms[0].submit();
    }

    <%@ include file="/jsp/script/submit_help.jsp" %>

    // ワンポイント詳細画面を開いて表示
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>

    function submitSearch() {
        //フリーワード入力必須の場合
        //if (document.forms[0].freeWord.value == "") {
        //  alert("フリーワードを入力してください。");
        //  return false;
        //}
        document.forms[0].forward.value = "h004";
        document.forms[0].backward.value = "h001";
        document.forms[0].category.value = "";
        document.forms[0].page.value = "1";
        document.forms[0].submit();
    }

//-->
</SCRIPT>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" >

<!-- フリーワード検索とカテゴリー一覧の切り分けを「category」で行う。-->
<form action="<c:url value="HelpTop" />" method="POST" onSubmit='return false;'>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="category" value="">
<input type="hidden" name="page" value="">


<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_h001.html"-->
<!--/ヘルプナビ-->

<!--タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570"><img src="./help/img/ttl_help.gif" width="78" height="24" border="0" alt="ヘルプ"><br></td>
</tr>
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!--ケイコさん-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="446">
<tr valign="top">
<td width="62"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="384">

<table border="0" cellpadding="0" cellspacing="0" width="384">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="360" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="360" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="360" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="360" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="384">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="370" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="370">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="340"><span class="text14">フリーワードもしくはカテゴリーからお進みください。</span></td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="384">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="360" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="360" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="360" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="360" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">フリーワードで探す</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--フリーワードで探す-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#EFF2F3" align="center">

<!--検索-->
<table border="0" cellpadding="0" cellspacing="0" width="548">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="548">
<tr valign="top">
<td width="548" bgcolor="#FFFFFF">

<!--ワンポイントアドバイス-->
<% 
 for (java.util.Iterator it=OnepointBean.getHelpList().iterator(); it.hasNext();) {
    HelpList list = (HelpList)it.next();
%>
    <div style="margin-top:5px;">
    <table border="0" cellpadding="0" cellspacing="0" width="180">
    <tr valign="top">
    <td><b class="text12"><%=list.getUpdate()%></b></td>
    </tr>
    <tr valign="top">
    <td><span class="text12"><font color="#758A98">●</font><a href="javascript:openOnepoin('<%=list.getHelpId()%>')"><%=list.getTitleStr()%></a></span></td>
    </tr>
    </table>
    </div>
<% } %>
<!--/ワンポイントアドバイス-->


<table border="0" cellpadding="5" cellspacing="0">
<tr>
<td><input type="text" size="40" class="text12" name="freeWord" style="width:200px;" value="<c:out value="${HelpListBean.freeWord}" />"></td>
<td><input type="submit" value="検索" name="retrieve" class="text12" style="width:50px;" onClick="submitSearch()"></td>
<td><span class="text12">※半角カタカナは使用しないで下さい。</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/検索-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/フリーワードで探す-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">カテゴリーから探す</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--カテゴリーから探す-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="570">
<tr valign="top">
<td width="568" bgcolor="#EFF2F3" align="center">

<%
    // ヘルプリストからカテゴリーを抽出する
    int intPos = 0;
    int intListSize = HelpListBean.getHelpListSize();       // リストサイズ
    String[] strCategory = new String[intListSize];
    for (java.util.Iterator it=HelpListBean.getHelpLists().iterator(); it.hasNext();) {
        HelpList list = (HelpList)it.next();
        strCategory[intPos] = list.getCategory();
        intPos++;
    }
    intPos=0;

%>

<% for(int intCnt=0; intCnt < (intListSize+1 ) / 2; intCnt++) { %>
<% intPos = intCnt*2; %>

    <!--1段目-->
    <table border="0" cellpadding="0" cellspacing="0" width="548">
    <tr valign="top">
    <td width="269">
    <!--左-->
    <table border="0" cellpadding="0" cellspacing="0" width="269">
        <tr valign="top">
            <td bgcolor="#8CA9BB">
                <table border="0" cellpadding="8" cellspacing="1" width="269">
                    <tr valign="top">
                        <td width="267" bgcolor="#FFFFFF">
                            <span class="text12">
                                <font color="#758A98">■</font>
                                <a href="javascript:submitCategory('<%=strCategory[intPos]%>')"><%=strCategory[intPos]%></a>
                            </span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--/左-->
    </td>
    <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
    <td width="269">
    <!--右-->
    <%if( intPos + 1 < intListSize ){%>
        <table border="0" cellpadding="0" cellspacing="0" width="269">
            <tr valign="top">
                <td bgcolor="#8CA9BB">
                    <table border="0" cellpadding="8" cellspacing="1" width="269">
                        <tr valign="top">
                            <td width="267" bgcolor="#FFFFFF">
                                <span class="text12">
                                    <font color="#758A98">■</font>
                                    <a href="javascript:submitCategory('<%=strCategory[intPos+1]%>')"><%=strCategory[intPos+1]%></a>
                                </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    <%}%>
    <!--/右-->
    </td>
    </tr>
    </table>
    <!--/1段目-->
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

<% } %>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/カテゴリーから探す-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="3" cellspacing="0">
<tr>
<td><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();"><img src="./shared_lib/include/content/img/icon_manual.gif" width="16" height="16" border="0" alt="マニュアル"></a></td>
<td><span class="text12"><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();">マニュアル</a></span></td>
<td><span class="text12">・・・オンラインマニュアルはこちらから参照できます。</span></td>
</tr>
<tr>
<td><a href="javascript:openHelp('Inquire')"><img src="./shared_lib/include/content/img/icon_inq.gif" width="16" height="16" border="0" alt="お問い合わせ"></a></td>
<td><span class="text12"><a href="javascript:openHelp('Inquire')">お問い合わせ</a></span></td>
<td><span class="text12">・・・ヘルプで解決できなかった場合はこちらからお問い合わせ下さい。</span></td>
</tr>
</table>

<!--ボーダー-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->
<!--ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onclick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

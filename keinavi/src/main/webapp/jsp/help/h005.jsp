<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>

<jsp:useBean id="HelpListBean" scope="request" class="jp.co.fj.keinavi.beans.help.HelpListBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<SCRIPT type="text/javascript">

<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitTop() {
        document.forms[0].target = "_self";
        document.forms[0].action="<c:url value="HelpTop" />";
        document.forms[0].method = "POST";
        document.forms[0].forward.value = "h001";
        document.forms[0].backward.value = "";
        document.forms[0].submit();
    }

    function submitBack() {
        document.forms[0].forward.value = "h003";
        document.forms[0].backward.value = "h005";
        document.forms[0].submit();
    }

//-->
</SCRIPT>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form action="<c:url value="HelpDetail" />"; method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="category" value="<c:out value="${HelpListBean.category}" />">
<input type="hidden" name="freeWord" value="<c:out value="${HelpListBean.freeWord}" />">
<input type="hidden" name="helpId" value="<c:out value="${HelpListBean.helpId}" />">
<input type="hidden" name="useful" value="<c:out value="${HelpListBean.useful}" />">
<input type="hidden" name="comment" value="<c:out value="${HelpListBean.comment}" />">
<input type="hidden" name="name" value="<c:out value="${HelpListBean.name}" />">
<input type="hidden" name="detailBack" value="<c:out value="${HelpListBean.detailBack}" />">
<input type="hidden" name="page" value="<c:out value="${HelpListBean.page}" />">


<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_h001.html"-->
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="621">
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="621" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"  onClick="javascript:submitBack()"></td>
<!--- <td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"  onClick="history.back()"></td> -->
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570"><img src="./help/img/ttl_help.gif" width="78" height="24" border="0" alt="ヘルプ"><br></td>
</tr>
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!-- パンくず -->
<%@ include file="/jsp/help/pankuzu.jsp" %>
<!--/パンくず -->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--送信完了-->
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><img src="./help/img/finish.gif" width="105" height="24" border="0" alt="送信完了"><br></td>
</tr>
</table>
<!--/送信完了-->


<!--送信完了-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><b class="text14">以下の内容で送信しました。</b></td>
</tr>
</table>
</div>
<!--/送信完了-->


<!--意見フォーム-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="550">
<tr valign="top">
<td width="20%" bgcolor="#E5EEF3"><b class="text12-hh">送信内容</b></td>
<td width="80%" bgcolor="#FFFFFF"><span class="text12-hh">ヘルプについてのご意見（ヘルプID:<c:out value="${HelpListBean.helpId}" />）</span></td>
</tr>
<tr valign="top">
<td width="20%" bgcolor="#E5EEF3"><b class="text12-hh">評価</b></td>
<td width="80%" bgcolor="#FFFFFF"><span class="text12-hh"><c:out value="${HelpListBean.useful}" /></span></td>
</tr>
<tr valign="top">
<td width="20%" bgcolor="#E5EEF3"><b class="text12-hh">コメント</b></td>
<td width="80%" bgcolor="#FFFFFF"><span class="text12-hh"><kn:pre><c:out value="${HelpListBean.comment}" /></kn:pre></span></td>
</tr>
<tr valign="top">
<td width="20%" bgcolor="#E5EEF3"><b class="text12-hh">お名前</b></td>
<td width="80%" bgcolor="#FFFFFF"><span class="text12-hh"><c:out value="${HelpListBean.name}" /></span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/意見フォーム-->

<!--ボーダー-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->
<!--ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw_03.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

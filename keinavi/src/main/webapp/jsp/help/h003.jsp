<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<jsp:useBean id="HelpListBean" scope="request" class="jp.co.fj.keinavi.beans.help.HelpListBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ヘルプ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>

<SCRIPT type="text/javascript">
<!--
    /********************************************************************************
    *** SUBMITION *******************************************************************
    ********************************************************************************/
    function submitTop() {
        document.forms[0].target = "_self";
        document.forms[0].action="<c:url value="HelpTop" />";
        document.forms[0].method = "POST";
        document.forms[0].forward.value = "h001";
        document.forms[0].backward.value = "h003";
        document.forms[0].submit();
    }

    function submitTransmit() {
        document.forms[0].forward.value = "h005";
        document.forms[0].backward.value = "h003";
        // コメント文字数をチェック（200バイトを超えたらエラー）
        if (getByteCount(document.forms[0].comment.value) > 200 ) {
            alert("コメントは全角１００文字までです。");
            return false;
        }

        if (confirm("ご意見を送信しますか？")) {
            document.forms[0].submit();
            //return true;
        } else {
            return false;
        }
    }

    /**
     * 全角であるかをチェック
     *
     * @param  チェックする値
     * @return ture : 全角 / flase : 全角以外
     */
    function checkIsZenkaku(value) {
        for (var i = 0; i < value.length; ++i) {
            var c = value.charCodeAt(i);
            //  半角カタカナは不許可
            if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
              return false;
            }
        }
        return true;
    }

    /**
     * 文字列のバイト数を取得
     * 全角を2バイト、半角を1バイトとしてカウント
     *
     * @param  バイトを取得する値
     * @return 取得したバイト数
     */
    function getByteCount(value) {
        var count = 0;
        for ( var i = 0; i < value.length; ++i ) {
            var sub = value.substring(i, i + 1);
            //全角の場合２バイト追加。
            if( checkIsZenkaku(sub) ){
                count += 2;
            } else {
                count += 1;
            }
        }
        return count;
    }


    function submitBack(formId) {
        document.forms[0].forward.value = formId;
        document.forms[0].backward.value = "h003";
        document.forms[0].submit();
    }

    function submitTitleStr(id) {
        document.forms[0].forward.value = "h003";
        document.forms[0].backward.value = "h003";
        document.forms[0].helpId.value = id;
        document.forms[0].submit();
    }

//-->
</SCRIPT>

</head>

<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" >
<!-- <form action="HelpTransmit" method="POST"> -->
<form action="<c:url value="HelpDetail" />" method="POST" onSubmit='return false;' >
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="category" value="<%=HelpListBean.getCategory()%>">
<input type="hidden" name="freeWord" value="<c:out value="${HelpListBean.freeWord}" />">
<input type="hidden" name="helpId" value="<c:out value="${HelpListBean.helpId}" />">
<input type="hidden" name="detailBack" value="<c:out value="${HelpListBean.detailBack}" />">
<input type="hidden" name="page" value="<c:out value="${HelpListBean.page}" />">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<!--# include virtual="./shared_lib/include/content/help_link_h001.html"-->
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="621">
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="621" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<!-- <td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="history.back()"></td> -->
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack('<c:out value="${HelpListBean.detailBack}" />')"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#828282">
<td width="621"><img src="./shared_lib/img/parts/sp.gif" width="621" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--タイトル-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570"><img src="./help/img/ttl_help.gif" width="78" height="24" border="0" alt="ヘルプ"><br></td>
</tr>
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</table>
</div>
<!--/タイトル-->

<!-- パンくず -->
<%@ include file="/jsp/help/pankuzu.jsp" %>
<!--/パンくず -->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="561">

<table border="0" cellpadding="0" cellspacing="0" width="561">
<tr valign="top">
<td colspan="4" width="561" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="561" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;"><%=HelpListBean.getCategory()%></b></td>
</tr>
<tr valign="top">
<td colspan="4" width="561" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="561" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="570" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--一覧へ戻る-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="568" bgcolor="#EFF2F3">
<table border="0" cellpadding="4" cellspacing="1" width="568">
<tr valign="top">
<td width="566"><span class="text12">
<!-- <a href="javascript:history.back(-1)">＜＜一覧へ戻る</a> -->
<a href="javascript:submitBack('<c:out value="${HelpListBean.detailBack}" />')">＜＜一覧へ戻る</a>
</span></td>
</tr>
</table>
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/一覧へ戻る-->


<!--質問＆回答-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<!--質問-->
<tr valign="top">
<td width="550"><b class="text14"><%=HelpListBean.getTitleStr()%></b></td>
</tr>
<!--/質問-->
<tr valign="top">
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<!--回答-->
<tr valign="top">
<td width="550"><span class="text12-hh"><%=HelpListBean.getExplan()%><br>
</span></td>
</tr>
<!--/回答-->
</table>
</div>
<!--/質問＆回答-->


<!--意見フォーム-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#E5EEF3" align="right"><span class="text12">
ヘルプID:<c:out value="${HelpListBean.helpId}" />
</span></td>
</tr>
<tr valign="top">
<td width="548" bgcolor="#FFFFFF" align="center">
<!--1-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="528">
<tr>
<td width="528"><span class="text12">■このヘルプは役に立ちましたか？</span></td>
</tr>
<tr>
<td width="528">
<table border="0" cellpadding="0" cellspacing="0">
<tr>

<% String checked = ""; %>
<% if (HelpListBean.getUseful().equals("") || HelpListBean.getUseful() == null) checked = " checked" ;  %>
<% if (HelpListBean.getUseful().equals("役に立った")) checked = " checked" ;  %>
<td><input type="radio" name="useful" value="役に立った" <%=checked%> ></td>
<td><span class="text12">役に立った</span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>

<% checked = ""; %>
<% if (HelpListBean.getUseful().equals("役に立たなかった")) checked = " checked"; %>
<td><input type="radio" name="useful" value="役に立たなかった" <%=checked%> ></td>
<td><span class="text12">役に立たなかった</span></td>

<% checked = ""; %>
<% if (HelpListBean.getUseful().equals("どちらとも言えない")) checked = " checked"; %>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="radio" name="useful" value="どちらとも言えない" <%=checked%> ></td>
<td><span class="text12">どちらとも言えない</span></td>

</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/1-->
<!--2-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="528">
<tr>
<td width="528"><span class="text12">■何かコメントがあればご記入ください。（全角100文字まで）</span></td>
</tr>
<tr>
<td width="528"><textarea rows="4" cols="40" name="comment" style="width:500px;" value=""><c:out value="${HelpListBean.comment}" /></textarea></td>
</tr>
</table>
</div>
<!--/2-->
<!--3-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="528">
<tr>
<td width="528"><span class="text12">■お名前（任意）</span></td>
</tr>
<tr>
<td width="528"><input type="text" size="40" class="text12" name="name" value="<c:out value="${HelpListBean.name}" />"></td>
</tr>
</table>
</div>
<!--/3-->

<!--ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="528">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="528">
<tr valign="top">
<td width="526" bgcolor="#FBD49F" align="center">

<input type="submit" value="送信" class="text12" style="width:80px;" onClick="return submitTransmit()">
<!-- <input type="button" value="送信" class="text12" style="width:80px;" onClick="javascript:submitTransmit()"> -->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/意見フォーム-->

<!--ボーダー-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->

<!--ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onclick="window.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw_03.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／特例成績データダウンロード</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
<!--

	<%@ include file="/jsp/script/timer.jsp" %>

	<%-- ヘルプ・お問い合わせ用 --%>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 終了ボタン用 --%>
	<%@ include file="/jsp/sales/common/submit_exit.jsp" %>

	function init() {
		startTimer();
		var message = "<kn:errorMessage form="SD207Form" separator="" />";
		if (message) {
			alert(message);
		}
	}

	function submitBack() {
		$('#main-form').children('input[name=forward]').val('wb003').end().submit();
	}

	function submitSort(sortKey) {
		$('#main-form').children('input[name=operation]').val('index').end().children('input[name=sortKey]').val(sortKey).end().submit();
		return false;
	}

	function submitCancel(appId) {
		if (confirm('申請を取り消します。\r\nよろしいですか？')) {
			$('#main-form').children('input[name=operation]').val('cancel').end().children('input[name=cancelAppId]').val(appId).end().submit();
		}
	}

	function submitDownload() {
		if ($('input[name=appId]:checked').size() == 0) {
			alert('ダウンロードする申請データを選択してください。');
			return;
		}
		$('#main-form').children('input[name=operation]').val('download').end().submit();
	}


// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<form action="<c:url value="SD207Servlet" />" method="POST" id="main-form">
<input type="hidden" name="operation" value="index">
<input type="hidden" name="forward" value="sd207">
<input type="hidden" name="backward" value="sd207">
<input type="hidden" name="sectorCD" value="<c:out value="${LoginSession.sectorCd}" />">
<input type="hidden" name="menu" value="9">
<input type="hidden" name="sortKey" value="<c:out value="${SD207Form.sortKey}" />">
<input type="hidden" name="cancelAppId" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="240" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><h2 class="text24"><B>特例成績データダウンロード</B></h2></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%">
    <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr align="left">
        <td><span class="text12">
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         特例成績データの作成申請の一覧です。<BR>
        </span></td>
        </tr>
        <tr align="center">
            <td>

<!--契約校管理-->
<div style="margin-top:14px;">


<table border="0" cellpadding="2" cellspacing="2" width="900" id="Table1">
<tr>
 <td width="150"><span class="text12">対象年度：</span>
  <select name="examYear" class="text12" onchange="submitSort(this.form.sortKey.value);">
   <option value="<c:out value="${currentYear}" />"><c:out value="${currentYear}" /></option>
   <option value="<c:out value="${currentYear - 1}" />"<c:if test="${ SD207Form.examYear eq (currentYear - 1) }"> selected</c:if>><c:out value="${currentYear - 1}" /></option>
  </select>
 </td>
 <td><span class="text12">絞込み条件：</span>
  <select name="condition" class="text12" onchange="submitSort(this.form.sortKey.value);">
   <option value="1">未ダウンロード</option>
   <option value="2"<c:if test="${ SD207Form.condition eq '2' }"> selected</c:if>>全て</option>
  </select>
 </td>
</tr>
</table>

<!--項目-->
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD START --%>
<table border="0" cellpadding="2" cellspacing="2" width="900" id="Table1"  style="word-break:break-all;">
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD END --%>
    <tr>
    <td width="3%" bgcolor="#CDD7DD" align="center"><span class="text12"><BR></span></td>
    <td width="4%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(1);">対象<BR>年度</A></span></td>
    <td width="20%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(2);">対象模試</A></span></td>
    <td width="4%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(3);">学校<BR>コード</A></span></td>
    <td width="10%" bgcolor="#CDD7DD" align="center"><span class="text12">学校名</span></td>
    <td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">契約<BR>区分</span></td>
    <td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">担当<BR>先生名</span></td>
    <td width="18%" bgcolor="#CDD7DD" align="center"><span class="text12">申請理由</span></td>
    <td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(4);">申請日時</A></span></td>
    <td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(5);">最終<BR>ダウンロード日時</A></span></td>
    <td width="11%" bgcolor="#CDD7DD" align="center"><span class="text12"><A HREF="javascript:void(0)" onclick="return submitSort(6);">状態</A></span></td>
    </tr>

    <c:if test="${empty results}">
    <tr height="27">
    <td bgcolor="#F4E5D6" align="center" colspan="12">申請データは存在しません。</td>
    </tr>
    </c:if>

    <c:forEach var="data" items="${results}">
    <tr height="27">
    <td bgcolor="#F4E5D6" align="center"><c:if test="${ data.statusCd eq '99' and empty data.openDate }"><input type="radio" name="appId" value="<c:out value="${data.appId}" />"></c:if></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.examYear}" /></span></td>
    <td bgcolor="#F4E5D6" align="left"><span class="text12"><c:out value="${data.examName}" /></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${data.schoolCd}" /></span></td>
    <td bgcolor="#F4E5D6" align="left"><span class="text12"><c:out value="${data.schoolName}" /></span></td>
    <td bgcolor="#F4E5D6" align="left"><span class="text12"><c:out value="${data.pactDivName}" /></span></td>
    <td bgcolor="#F4E5D6" align="left"><span class="text12"><c:out value="${data.teacherName}" /></span></td>
    <td bgcolor="#F4E5D6" align="left"><span class="text12"><kn:pre><c:out value="${data.appComment}" /></kn:pre></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"><fmt:formatDate value="${data.appDate}" pattern="yyyy/MM/dd HH:mm" /></span></td>
    <td bgcolor="#F4E5D6" align="center"><span class="text12"><fmt:formatDate value="${data.downloadDate}" pattern="yyyy/MM/dd HH:mm" /></span></td>
    <td bgcolor="#F4E5D6" align="center">
        <span class="text12"><c:out value="${data.statusName}" /></span>
        <c:if test="${ data.statusCd eq '99' and not empty data.openDate }">
            <span class="text12"><br>（<fmt:formatDate value="${data.openDate}" pattern="yyyy/MM/dd" />からダウンロード可能）</span>
        </c:if>
        <c:if test="${ data.statusCd ne '99' }">
            <br><input type="button" value="取消" class="text12" style="width:50px;" onclick="submitCancel('<c:out value="${data.appId}" />')">
        </c:if>
    </td>
    </tr>
    </c:forEach>

</table>


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;ダウンロード&nbsp;" class="text12" style="width:120px;" onClick="submitDownload();"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->



</div>
<!--/契約校管理-->

            </td>
        </tr>
    </table>
    <br>
</td>
</tr>
</table>

<!--/spacer-->


<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>

</body>
</html>

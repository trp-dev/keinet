<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<jsp:useBean id="form" scope="request" class="jp.co.fj.keinavi.forms.sales.SD101Form" />
<jsp:useBean id="downloadFileBean" scope="request" class="jp.co.fj.keinavi.beans.UploadFileBean" />
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／ファイルダウンロード</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<style type="text/css">
	th.header {
	    cursor: pointer;
	    font-weight: none;
	    /*
	    background-repeat: no-repeat;
	    background-position: center right;
	    padding-left: 20px;
	    border-right: 1px solid #dad9c7;
	    margin-left: -1px;
	    */
	}
	th.headerSortUp {
    	/*　background-image: url(./sales/sd101_img/asc.gif); */
    }
    th.headerSortDown {
	    /* background-image: url(./sales/sd101_img/desc.gif); */
	}
</style>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/OrderChange.js"></script>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>

	<%-- ヘルプ・お問い合わせ用 --%>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 終了ボタン用 --%>
	<%@ include file="/jsp/sales/common/submit_exit.jsp" %>

	//進む
	function submitForm(forward) {
		document.forms[0].target = "_self";
		document.forms[0].forward.value = forward;
		document.forms[0].submit();
	}
	//戻る
    function submitBack() {
    	<c:choose>
    	<c:when test="${LoginSession.helpDesk}">
    	submitForm("hd002");
    	</c:when>
    	<c:otherwise>
    	submitForm("wb003");
    	</c:otherwise>
    	</c:choose>
    }
	//ダウンロード実行
	function download() {

		var fu = new FormUtil();
		var va = new Validator();

		if (fu.countChecked(document.forms[0].elements['uploadFileID']) == 0) {
			alert("ダウンロード対象ファイルを選択（チェック）してください。");
			return;
		}

		if (document.forms[0].zipFileName.value == '') {
			alert("ダウンロードファイル名を入力してください。");
			return;
		}

        if (va.hasJapanese(document.forms[0].zipFileName.value)) {
            alert('<kn:message id="f003a" />');
            return;
        }

		document.forms[0].actionMode.value = "1";
		submitForm("<c:out value="${param.forward}" />");
	}
	//全て選択
	function checkAllElements(obj){

		var fu = new FormUtil();

		if (obj.checked) {
			document.forms[0].checkall[0].checked = true;
			document.forms[0].checkall[1].checked = true;
		} else {
			document.forms[0].checkall[0].checked = false;
			document.forms[0].checkall[1].checked = false;
		}

		//uploadFileIDをすべてチェックする
		fu.checkAllElements(obj.checked,　'uploadFileID');
	}

	function checkself(obj) {
		if (!obj.checked) {
			document.forms[0].checkall[0].checked = false;
			document.forms[0].checkall[1].checked = false;
		}
	}

	//ソート
	function sortTable(index) {
		if (document.forms[0].sortIndex.value == index) {
			index = "-" + index;
		}

		document.forms[0].actionMode.value = "2";
		document.forms[0].sortIndex.value = index;
		submitForm("sd101");
	}
	//初期化
    function init() {
    	startTimer();
	<c:if test="${form.errorMessage != ''}">
		alert("<c:out value="${form.errorMessage}"/>");
	</c:if>
    }

// -->
</script>

</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form style="margin:0" action="<c:url value="OpenFileList"/>" method="POST" onsubmit="return false">
<input type="hidden" name="forward" value=""/>
<input type="hidden" name="backward" value="<c:out value="${param.forward}" />"/>
<input type="hidden" name="actionMode" value="0"/>
<input type="hidden" name="salesCode" value="<c:out value="${param.salesCode}" />"/>
<input type="hidden" name="sectorCD" value="<c:out value="${param.salesCode}" />"/>
<input type="hidden" name="sortIndex" value="<c:out value="${form.sortIndex}" />"/>
<input type="hidden" name="menu" value="<c:out value="${form.menu}" />"/>
<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_t.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="8" background="freemenu/img/parts/com_bk_l.gif"><img src="freemenu/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプナビ--><!--コンテンツ--><!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="sales/sd101_img/filedownload_up_mi.gif" border="0" alt="ファイルダウンロード"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><br>
<table border="0" cellspacing="3" cellpadding="3" align="center">
<tr align="center">
<td>
<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>
<div class="text14">高等学校がアップロードしたファイルの一覧を以下に表示します。</div>
</td>
</tr>
<tr>
<td>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
<tr>
<td>
<!--リスト-->
<!--ツールバー-->
<table border="0" cellpadding="2" cellspacing="2" width="900" align="center">
<tr bgcolor="#93A3AD">
<td colspan="8" width="100%">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input id="checkall1" type="checkbox" name="checkall" value="1" onclick="javascript:checkAllElements(this);" <c:if test="${form.checkall[0] eq '1'}">checked</c:if>></td>
<td><b class="text12" style="color:#FFF;">すべて選択（チェックを外すとすべて解除）</b></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ツールバー-->
<table border="0" cellpadding="2" cellspacing="2" width="900" align="center" id="listTable" class="tablesorter">
<!--項目-->
<thead>
<tr bgcolor="#CDD7DD" align="center">
<th width="3%" align="center"><span class="text12"></span></th>
<th width="5%" align="center" style="font-weight:normal"><span class="text12" ><a href="javascript:sortTable('1');">学校<BR>コード</span></a></th>
<th width="12%" align="center" style="font-weight:normal"><span class="text12"><a href="javascript:sortTable('2');">学校名</a></span></th>
<th width="11%" align="center" style="font-weight:normal"><span class="text12"><a href="javascript:sortTable('3');">登録日時</a></span></th>
<th width="13%" align="center" style="font-weight:normal"><span class="text12"><a href="javascript:sortTable('4');">ファイル種類</a></span></th>
<th width="18%" align="center" style="font-weight:normal"><span class="text12">ファイル名</span></th>
<th width="27%" align="center" style="font-weight:normal"><span class="text12">コメント</span></th>
<th width="11%" align="center" style="font-weight:normal"><span class="text12"><a href="javascript:sortTable('7');">最終<BR>ダウンロード日時</a></span></th>
</tr>
</thead>
<!--/項目-->
<!--set-->
<tbody>
<c:forEach var="history" items="${downloadFileBean.recordSet}" varStatus="status">
<tr>
<c:set var="checked" value="" />
<c:forEach var="uploadFileID" items="${form.uploadFileID}">
<c:if test="${history.uploadFileID eq uploadFileID}">
<c:set var="checked" value="checked" />
</c:if>
</c:forEach>
<td bgcolor="#E1E6EB" align="center"><input type="checkbox" id="<c:out value="${status.index}"/>" name="uploadFileID" value="<c:out value="${history.uploadFileID}"/>" <c:out value="${checked}"/> onclick="javascript:checkself(this);"></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;word-wrap:break-word;overflow:auto;" align="center" sortKey="<c:out value="${history.schoolCD}"/>+<fmt:formatDate value="${history.updateDate}" pattern="yyyyMMddHHmm" />"><span class="text12"><c:out value="${history.schoolCD}"/></span></td>
<td bgcolor="#F4E5D6" sortKey="<c:out value="${history.schoolNameKana}"/>+<fmt:formatDate value="${history.updateDate}" pattern="yyyyMMddHHmm" />"><span class="text12"><c:out value="${history.schoolName}"/></span></td>
<td bgcolor="#F4E5D6" align="center" sortKey="<fmt:formatDate value="${history.updateDate}" pattern="yyyyMMddHHmm" />"><span class="text12"><fmt:formatDate value="${history.updateDate}" pattern="yyyy/MM/dd HH:mm" /></span></td>
<td bgcolor="#F4E5D6" sortKey="<c:out value="${history.fileTypeID}"/>+<fmt:formatDate value="${history.updateDate}" pattern="yyyyMMddHHmm" />"><span class="text12"><c:out value="${history.fileTypeAbbrname}"/></span></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;word-wrap:break-word;overflow:auto;"><span class="text12"><c:out value="${history.uploadFileName}"/></span></td>
<td bgcolor="#F4E5D6" style="word-break:break-all;word-wrap:break-word;overflow:auto;"><span class="text12"><kn:pre><c:out value="${history.uploadComment}"/></kn:pre></span></td>
<c:set var="lastDownloadDateSortKey" value="" />
<c:set var="lastDownloadDateString" value="-" />
<c:if test="${not empty history.lastDownloadDate}">
<c:set var="lastDownloadDateSortKey"><fmt:formatDate value="${history.lastDownloadDate}" pattern="yyyyMMddHHmm" /></c:set>
<c:set var="lastDownloadDateString"><fmt:formatDate value="${history.lastDownloadDate}" pattern="yyyy/MM/dd HH:mm" /></c:set>
</c:if>
<td bgcolor="#F4E5D6" align="center" sortKey="<c:out value="${lastDownloadDateSortKey}"/>+<fmt:formatDate value="${history.updateDate}" pattern="yyyyMMddHHmm" />"><span class="text12"><c:out value="${lastDownloadDateString}"/></span></td>
</tr>
</c:forEach>
</tbody>
<c:if test="${downloadFileBean.recordCount == 0}">
<tr>
<td colspan="8" bgcolor="#F4E5D6" align="center"><span class="text12">アップロードされたファイルが存在しません。</span></td>
</tr>
</c:if>
<!--/set-->
</table>
<!--ツールバー-->
<table border="0" cellpadding="2" cellspacing="2" width="900" align="center">
<tr bgcolor="#93A3AD">
<td colspan="7" width="100%">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input id="checkall2" type="checkbox" name="checkall" value="1" onclick="javascript:checkAllElements(this);" <c:if test="${form.checkall[1] eq '1'}">checked</c:if>></td>
<td><b class="text12" style="color:#FFF;">すべて選択（チェックを外すとすべて解除）</b></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ツールバー-->
<!--/リスト-->
</td>
</tr>
</table>
<c:if test="${downloadFileBean.recordCount > 0}">
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="908">
<tr>
<td width="908" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td align="center">チェックされたファイルを１つのzip圧縮ファイルとしてダウンロードします。</td>
</tr>
<tr><td><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td></tr>
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="500">
<tr valign="top">
<td width="800" bgcolor="#FBD49F" align="center">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td style="padding-left:10px;" class="text14">ダウンロードファイル名 :</td>
<td><input type="text" name="zipFileName" style="width:300px;"></td>
</tr>
<tr><td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""></td></tr>
<tr><td style="padding-left:10px;" colspan="2">&nbsp;&nbsp;・ファイル名のみ指定してください（ドライブ名、フォルダ名、拡張子不可）。</td></tr>
<tr><td style="padding-left:10px;" colspan="2">&nbsp;&nbsp;・ファイル名には半角英数字のみ使用できます。</td></tr>
<tr><td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""></td></tr>
<tr>
<td></td>
<td><input type="button" value="&nbsp;ダウンロード&nbsp;" class="text12" style="width:140px;" onclick="javascript:download();"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
<tr><td><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td></tr>
<tr>
<td align="center" class="text14">※ダウンロードしたファイルに対して、必ずウィルスチェックを実行してください。</td>
</tr>
</table>
<!--/ボタン-->
</c:if>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<br>
</td>
</tr>
</table>
<!--/spacer--><!--/コンテンツ--></td>
<td width="9"  background="freemenu/img/parts/com_bk_r.gif"><img src="freemenu/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="freemenu/img/com_cnr_lb_w.gif" width="12" height="14" border="0" alt=""><br></td>
<td width="960" background="freemenu/img/parts/com_bk_d.gif"><img src="freemenu/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br>
</td>
<td width="13"><img src="freemenu/img/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="freemenu/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

</form>

<form action="<c:url value="OpenFileList"/>" method="POST" id="main-form">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="sd101">
</form>

</body>
</html>

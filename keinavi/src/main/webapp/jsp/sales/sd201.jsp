<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／特例成績データ作成申請</title>
<style type="text/css">
	.error {
		color: red;
	}
</style>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
<!--

    <%@ include file="/jsp/script/timer.jsp" %>
    
	<%-- ヘルプ・お問い合わせ用 --%>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 終了ボタン用 --%>
	<%@ include file="/jsp/sales/common/submit_exit.jsp" %>

	var examMap = Array();
	<c:forEach var="entry" items="${exam.examMap}">
		examMap['<c:out value="${entry.key}" />'] = Array();
		<c:forEach var="data" items="${entry.value}">
			examMap['<c:out value="${entry.key}" />'][examMap['<c:out value="${entry.key}" />'].length] = new Option("<c:out value="${data.examName}" />","<c:out value="${data.examCd}" />");
		</c:forEach>
	</c:forEach>

	$(function(){
		$('#main-form input[name=schoolCd]')
			.focus(function(){
				jQuery.data(this, 'oldValue', this.value);
			})
			.blur(function(){
				if (jQuery.data(this, 'oldValue') == this.value) {
					return;
				}
				var form = $('#ajax-form');
				form.children('input[name=schoolCd]').val(this.value);
			    $.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(data) {
						$('#schoolName').html(data);
					}
				});
			});
	});

	function init() {
		startTimer();
		initExamListBox("<c:out value="${param.examCd}" />");
		var message = "<kn:errorMessage form="SD201Form" field="schoolCd" /><kn:errorMessage form="SD201Form" field="common" />";
		if (message) {
			alert(message);
		}
	}

	function initExamListBox(examCd) {
		var options = $('select[name=examCd]').get(0).options;
		options.length = 0;
		var examList =  examMap[$('select[name=examYear]').val()];
		for (var i = 0; i <  examList.length; i++) {
			examList[i].selected = examCd == examList[i].value;
			options[i] = examList[i];
		}
	}

	function submitBack() {
		$('#main-form').children('input[name=forward]').val('wb003').end().submit();
	}

	function submitApply() {
		if (confirm("指定された「対象年度」「対象模試」「学校コード」で特例成績データの作成を申請します。\r\nよろしいですか？")) {
			$('#main-form').children('input[name=forward]').val('sd201').end().submit();
		}
		return false;
	}

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init();" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

<form action="<c:url value="SD201Servlet" />" method="POST" id="main-form">
<input type="hidden" name="operation" value="apply">
<input type="hidden" name="forward" value="sd201">
<input type="hidden" name="backward" value="sd201">
<input type="hidden" name="sectorCD" value="<c:out value="${LoginSession.sectorCd}" />">
<input type="hidden" name="menu" value="4">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/freemenu_help.jsp" %>
<!--/ヘルプナビ-->

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="freemenu/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="freemenu/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><h2 class="text24"><B>特例成績データ作成申請</B></h2></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%">
    <table width="100%" border="0" cellspacing="3" cellpadding="3" align="center">
        <tr align="left">
        <td><span class="text12">
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         特例成績データを申請する「対象年度」「対象模試」「学校コード」「担当先生名」「申請理由」を指定し、申請ボタンを押してください。<BR>
        </span></td>
        </tr>
        <tr align="center">
            <td>

<!--契約校管理-->
<div style="margin-top:14px;">

<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
<td>

<table width="600" border="0" cellspacing="0" cellpadding="2">
<tr>
 <td><span class="text12">対象年度：</span></td>
 <td align="left">
  <select name="examYear" class="text12" onchange="initExamListBox();">
    <c:forEach var="year" items="${exam.years}">
      <option value="<c:out value="${year}" />"<c:if test="${year eq param.examYear}"> selected</c:if>><c:out value="${year}" /></option>
    </c:forEach>
  </select>
 </td>
</tr>
<tr>
 <td><span class="text12">対象模試：</span></td>
 <td align="left">
  <select name=examCd class="text12">
  </select>
 </td>
</tr>
<tr>
 <td width="80"><span class="text12">学校コード：</span></td>
 <td align="left">
 <input type="text" size="10" maxlength="5" name="schoolCd" value="<c:out value="${SD201Form.schoolCd}" />">
 <span class="text12">&nbsp;</span>
 <span class="text12" id="schoolName"><c:out value="${schoolName}" /></span></td>
</tr>
<tr>
 <td width="80"><span class="text12">担当先生名：</span></td>
 <td align="left">
 <input type="text" size="30" maxlength="20" name="teacherName" value="<c:out value="${SD201Form.teacherName}" />"></td>
</tr>
<tr>
 <td width="80"><span class="text12">申請理由：</span></td>
 <td align="left">
 <textarea name="comments" rows="3" cols="60"><c:out value="${SD201Form.comments}" /></textarea></td>
</tr>
</table>

</td>
</tr>
</table>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;申請&nbsp;" class="text12" style="width:120px;" onclick="return submitApply();"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

</div>
<!--/契約校管理-->

            </td>
        </tr>
    </table>
    <br>
</td>
</tr>
</table>

<!--/spacer-->

<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->

<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>

<form action="<c:url value="SD201Servlet" />" method="POST" id="ajax-form">
<input type="hidden" name="operation" value="getSchoolName">
<input type="hidden" name="forward" value="sd201">
<input type="hidden" name="backward" value="sd201">
<input type="hidden" name="schoolCd" value="">
</form>

</body>
</html>

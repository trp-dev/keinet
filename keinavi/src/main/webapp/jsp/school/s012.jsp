<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／サンプル画面</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    
	function selectScreen(obj) {
		var form = document.forms[0];

		if (obj.options) {
			form.screen.value = obj.options[obj.selectedIndex].value;
		} else {
			form.screen.value = obj;
		}

		form.category.value = "";
		form.submit();
	}

	function selectCategory(category) {
		var form = document.forms[0];
		form.category.value = category;
		form.screen.value = "";
		form.submit();
	}
	
	function init() {
		startTimer();
	}
	
// -->
</script>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="Sample" />">
<input type="hidden" name="screen" value="<c:out value="${form.screen}" />">
<input type="hidden" name="category" value="<c:out value="${form.category}" />">
<input type="hidden" name="forward" value="s012">
<input type="hidden" name="backward" value="s012">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ナビゲーション-->
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr valign="top">
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="2" width="871">
<tr>
<td width="867" height="28" bgcolor="#EDEFF0">

<table border="0" cellpadding="0" cellspacing="0" width="867">
<tr>
<td nowrap>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td><b class="text12">サンプル画面&nbsp;：&nbsp;</b></td>
<td>
<span class="text12">

<c:set var="first" value="true" />

<c:if test="${DispSchool}">
<c:choose>
	<c:when test="${ form.category == 's' }">校内成績分析</c:when>
	<c:otherwise><a href="javascript:selectCategory('s')">校内成績分析</a></c:otherwise>
</c:choose>
<c:set var="first" value="false" />
</c:if>

<c:if test="${DispClass}">
	<c:if test="${ not first }">｜</c:if>
	<c:choose>
		<c:when test="${ form.category == 'c' }">クラス成績分析</c:when>
		<c:otherwise><a href="javascript:selectCategory('c')">クラス成績分析</a></c:otherwise>
	</c:choose>
	<c:set var="first" value="false" />
</c:if>

<c:if test="${DispIndividual}">
	<c:if test="${ not first }">｜</c:if>
	<c:choose>
		<c:when test="${ form.category == 'i' }">個人成績分析</c:when>
		<c:otherwise><a href="javascript:selectCategory('i')">個人成績分析</a></c:otherwise>
	</c:choose>
	<c:set var="first" value="false" />
</c:if>

<c:if test="${DispBusiness}">
	<c:if test="${ not first }">｜</c:if>
	<c:choose>
		<c:when test="${ form.category == 'b' }">高校成績分析</c:when>
		<c:otherwise><a href="javascript:selectCategory('b')">高校成績分析</a></c:otherwise>
	</c:choose>
	<c:set var="first" value="false" />
</c:if>

</span>
</td>
</tr>
</table>
</td>
<td align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">サンプル切替&nbsp;：&nbsp;</span></td>
<td>

<select class="text12" onchange="selectScreen(this)">
<c:forEach var="target" items="${Targets}">
<option value="<c:out value="${target}" />"<c:if test="${ target == form.screen}"> selected</c:if>><c:out value="${ScreenName[target]}" /></option>
</c:forEach>
</select>

</td>
<td><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="./shared_lib/img/parts/sp.gif" width="867" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ナビゲーション-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr valign="top">
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="570"><b class="text16" style="color:#FFFFFF;"><c:out value="${ScreenName[form.screen]}" /></b></td>
<td width="204"><font class="text12" style="color:#FFFFFF;"><a href="javascript:selectScreen('<c:out value="${Backward}" />')" style="color:#FF9;"><b>＜前のサンプル</b></a>│<a href="javascript:selectScreen('<c:out value="${Forward}" />')" style="color:#FF9;"><b>次のサンプル＞</b></a></font></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->




<!--↓↓編集領域↓↓-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td>

<c:forEach var="image" items="${Images}">
	<c:choose>
		<c:when test="${ image == 'i101-sample1.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;「表示する学年：現学年のみ」の場合</FONT>
		</c:when>
		<c:when test="${ image == 'i101-sample2.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;「表示する学年：全ての学年」の場合</FONT>
		</c:when>
		<c:when test="${ image == 'i102-sample1.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;「表示する学年：現学年のみ」の場合</FONT>
		</c:when>
		<c:when test="${ image == 'i102-sample2.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;「表示する学年：全ての学年」の場合</FONT>
		</c:when>
		<c:when test="${ image == 'i302-sample1.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;【先生用】<BR>&nbsp;&nbsp;&nbsp;※生徒用では、「電話番号」の表示がありません。</FONT>
		</c:when>
		<c:when test="${ image == 'i305-sample1.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;【先生用】<BR>&nbsp;&nbsp;&nbsp;※生徒用では、「電話番号」の表示がありません。</FONT>
		</c:when>
		<c:when test="${ image == 'i306-sample1.png' }">
			<FONT COLOR="red">&nbsp;&nbsp;&nbsp;【先生用】<BR>&nbsp;&nbsp;&nbsp;※生徒用では、「電話番号」の表示がありません。</FONT>
		</c:when>
		<c:otherwise></c:otherwise>
	</c:choose>
<p align="center"><img src="./sample/<c:out value="${image}" />" border="0" alt=""></p>
</c:forEach>

</td>
</tr>
</table>
<!--↑↑編集領域↑↑-->




<!--ボーダー-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="820" bgcolor="#868686"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー--->
<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onclick="self.close()"></td>
</tr>
</table>
</div>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw_02.html"-->
<!--/FOOTER-->
</form>
</body>
</html>

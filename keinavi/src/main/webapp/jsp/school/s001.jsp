<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set value="school" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／校内成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	<%@ include file="/jsp/script/download.jsp" %>
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/submit_top.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_sample.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/graph_checker.jsp" %>

	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
    <%@ include file="/jsp/script/printDialog_close.jsp" %>
	 var printDialogPattern = "";
	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

    // 全国総合成績を出力
    function submitTotalScore(value) {

    	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
		printDialogPattern = "s001";
    	<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

		// 確認ダイアログ（印刷ボタン押下時）
		if (value == 0) {
			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD START --%>
			//returnValue = showModalDialog(
			//		"<c:url value="PrintDialog" />",
			//		null,
			//		"status:no;help:no;scroll:no;dialogWidth:665px;dialogHeight:450px");

			//switch (returnValue) {
			//	case 1: document.forms[0].printFlag.value = 8; break;
			//	case 2: document.forms[0].printFlag.value = 2; break;
			//	default: return;
			//}

		    // 印刷確認ダイアログ　オープン
		    $(printDialogObj).dialog('open');
			<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD END   --%>
		} else {

			<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 UPD START --%>
			//document.forms[0].printFlag.value = value;
			//<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
			//printDialogCloseAfterProc();
			//<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>
			download(value);
			<%-- 2019/07/04 QQ)Tanioka ダウンロード変更対応 UPD END   --%>
		}

		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD START --%>
		<%--
        document.forms[0].forward.value = "sheet";

        <c:choose>
            <c:when test="${MenuSecurity.menu['200']}">
                document.forms[0].backward.value = "b000";
            </c:when>
            <c:otherwise>
                document.forms[0].backward.value = "s000";
            </c:otherwise>
        </c:choose>

        document.forms[0].submit();
		  --%>
		<%-- 2016/01/08 QQ)Nishiyama 大規模改修 UPD END   --%>
    }

    // 画面表示段階でのチェック数
    var count = 0;
    <c:if test="${ Profile.categoryMap['020100']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020201']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020202']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020203']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020301']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020302']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020303']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020401']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020402']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020403']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020404']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020501']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020502']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020503']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020504']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020601']['1301'] == 1 }">count++;</c:if>
    <c:if test="${ Profile.categoryMap['020602']['1301'] == 1 }">count++;</c:if>
    // チェックを加えたかどうか
    var flag = false;

    function change(obj) {
        // 変更フラグ
        document.forms[0].changed.value = "1";

        // カウントを増減
        if (obj.checked) {
            count++;
            flag = true;
        } else {
            count--;
        }
    }

    function validate() {
        if (count == 0) {
            alert("<kn:message id="s001a" />");
            return false;
        }

        if (flag) {
            alert("設定完了ボタンを押してください。");
            return false;
        }

        var message = "";

		<c:choose>
			<%-- 科目だけの画面を含む場合 --%>
			<c:when test="${ Profile.categoryMap['020203']['1301'] == 1 || Profile.categoryMap['020403']['1301'] == 1 || Profile.categoryMap['020503']['1301'] == 1 }">
				if ("<c:out value="${ComSetStatusBean.statusMap['course']}" />" == "02") {
					message += "<kn:message id="s003a" />\n";
				}
			</c:when>
			<%-- それ以外 --%>
			<c:otherwise>
				var typeCount = <c:out value="${ SubjectCountBean.typeCount }" />;
				if (("<c:out value="${ComSetStatusBean.statusMap['type']}" />" == "02" || typeCount == 0)
						&& "<c:out value="${ComSetStatusBean.statusMap['course']}" />" == "02") {
					message += "<kn:message id="s012a" />\n";
				}
			</c:otherwise>
		</c:choose>

        if ("<c:out value="${ComSetStatusBean.statusMap['year']}" />" == "02") {
            message += "<kn:message id="s004a" />\n";
        }

        if ("<c:out value="${ComSetStatusBean.statusMap['univ']}" />" == "02") {
            message += "<kn:message id="s005a" />\n";
        }

        if ("<c:out value="${ComSetStatusBean.statusMap['class']}" />" == "02") {
            message += "<kn:message id="s006a" />\n";
        }

        if ("<c:out value="${ComSetStatusBean.statusMap['school']}" />" == "02") {
            message += "<kn:message id="s007a" />\n";
        }

        if (message != "") {
            alert(message);
            return false;
        }

		<%-- グラフ出力対象チェック --%>
		var gc = new GraphChecker();
		<%-- 校内成績 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="s003" item="type" />, <kn:graph id="s003" item="course" />, "<kn:message id="gcS003" />");
		<%-- 校内成績 - 設問別成績 --%>
		gc.checkCourse(<kn:graph id="s004" item="course" />, "<kn:message id="gcS004" />");
		<%-- 過年度比較 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="s103" item="type" />, <kn:graph id="s103" item="course" />, "<kn:message id="gcS103" />");
		<%-- クラス比較 - 成績概況 --%>
		gc.checkSubject(<kn:graph id="s202" item="type" />, <kn:graph id="s202" item="course" />, "<kn:message id="gcS202" />");
		gc.checkClass(<kn:graph id="s202" item="class" />);
		<%-- クラス比較 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="s203" item="type" />, <kn:graph id="s203" item="course" />, "<kn:message id="gcS203" />");
		gc.checkClass(<kn:graph id="s203" item="class" />);
		<%-- クラス比較 - 設問別成績 --%>
		gc.checkCourse(<kn:graph id="s204" item="course" />, "<kn:message id="gcS204" />");
		gc.checkClass(<kn:graph id="s204" item="class" />);
		<%-- 他校比較 - 成績概況 --%>
		gc.checkSubject(<kn:graph id="s302" item="type" />, <kn:graph id="s302" item="course" />, "<kn:message id="gcS302" />");
		gc.checkSchool(<kn:graph id="s302" item="school" />);
		<%-- 他校比較 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="s303" item="type" />, <kn:graph id="s303" item="course" />, "<kn:message id="gcS303" />");
		gc.checkSchool(<kn:graph id="s303" item="school" />);
		<%-- 他校比較 - 設問別成績 --%>
		gc.checkCourse(<kn:graph id="s304" item="course" />, "<kn:message id="gcS304" />");
		gc.checkSchool(<kn:graph id="s304" item="school" />);
		<%-- 過回比較 - 成績概況 --%>
		gc.checkSubject(<kn:graph id="s402" item="type" />, <kn:graph id="s402" item="course" />, "<kn:message id="gcS402" />");
		gc.checkClass(<kn:graph id="s402" item="class" />);
		gc.checkSchool(<kn:graph id="s402" item="school" />);
		<%-- 他校比較 - 偏差値分布 --%>
		gc.checkSubject(<kn:graph id="s403" item="type" />, <kn:graph id="s403" item="course" />, "<kn:message id="gcS403" />");

		if (gc.hasError()) {
			alert(gc.createMessage());
			return false;
		}

		return true;
    }

    function init() {
    	startTimer();
        // スクロール位置を初期化する
        window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
    }

// -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="SchoolTop" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="590">
<!--●●●左側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="21" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>

<c:choose>
<c:when test="${MenuSecurity.menu['200']}">
<td width="160"><img src="./business/img/koko_seiseki.gif" width="150" height="25" border="0" alt="高校成績分析"><br></td>
</c:when>
<c:otherwise>
<td width="160"><img src="./school/img/ttl_school.gif" width="149" height="25" border="0" alt="校内成績分析"><br></td>
</c:otherwise>
</c:choose>

<td width="1"><!-- <img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""> --><br></td>
<td width="10"><!-- <img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""> --><br></td>
<td width="419"><!-- <img src="./school/img/ttl_school_read.gif" width="377" height="16" border="0" alt="地域・学校・学年などの視点から成績分析を行います。"> --><br></td>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--コンテンツメニュー-->
<table border="0" cellpadding="0" cellspacing="0" width="590" height="38">
<tr>
<%-- タブ --%>
<jsp:useBean id="stab" class="java.util.LinkedList" />
<%-- 校内成績分析・校内成績 --%>
<c:if test="${MenuSecurity.menu['101']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${ param.forward == 's001'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/contents_menu_01.gif" width="114" height="38" border="0" alt="校内成績"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('s001')"><img src="./shared_lib/img/btn/def/contents_menu_01.gif" width="114" height="38" border="0" alt="校内成績"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 校内成績分析・過年度比較 --%>
<c:if test="${MenuSecurity.menu['102']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 's101'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/contents_menu_02.gif" width="114" height="38" border="0" alt="過年度比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('s101')"><img src="./shared_lib/img/btn/def/contents_menu_02.gif" width="114" height="38" border="0" alt="過年度比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 校内成績分析・クラス比較 --%>
<c:if test="${MenuSecurity.menu['103']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 's201'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/contents_menu_03.gif" width="114" height="38" border="0" alt="クラス比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('s201')"><img src="./shared_lib/img/btn/def/contents_menu_03.gif" width="114" height="38" border="0" alt="クラス比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 校内成績分析・他校比較 --%>
<c:if test="${MenuSecurity.menu['104']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 's301'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/contents_menu_04.gif" width="114" height="38" border="0" alt="他校比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('s301')"><img src="./shared_lib/img/btn/def/contents_menu_04.gif" width="114" height="38" border="0" alt="他校比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 校内成績分析・過回比較 --%>
<c:if test="${MenuSecurity.menu['105']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 's401'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/contents_menu_05.gif" width="114" height="38" border="0" alt="過回比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('s401')"><img src="./shared_lib/img/btn/def/contents_menu_05.gif" width="114" height="38" border="0" alt="過回比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 高校成績分析・高校間比較 --%>
<c:if test="${MenuSecurity.menu['201']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 'b001'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/business_menu_01.gif" width="114" height="38" border="0" alt="高校間比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('b001')"><img src="./shared_lib/img/btn/def/business_menu_01.gif" width="114" height="38" border="0" alt="高校間比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- 高校成績分析・過回比較 --%>
<c:if test="${MenuSecurity.menu['202']}">
	<kn:listAdd var="stab">
		<c:choose>
			<c:when test="${param.forward == 'b101'}">
				<td width="114"><img src="./shared_lib/img/btn/cur/business_menu_02.gif" width="114" height="38" border="0" alt="過回比較"></td>
			</c:when>
			<c:otherwise>
				<td width="114"><a href="javascript:submitMenu('b101')"><img src="./shared_lib/img/btn/def/business_menu_02.gif" width="114" height="38" border="0" alt="過回比較"></a></td>
			</c:otherwise>
		</c:choose>
	</kn:listAdd>
</c:if>
<%-- SPACER --%>
<kn:listPad var="stab" size="5">
	<td width="114"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="114" height="38" border="0" alt=""></td>
</kn:listPad>
<kn:listSpacer var="stab">
	<td width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
</kn:listSpacer>
<%-- 出力 --%>
<kn:listOut var="stab" />
</tr>
</table>
<!--/コンテンツメニュー-->

<!-- 概要（校内成績）-->
<c:if test="${ param.forward == 's001' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">全統模試の校内成績をさまざまな角度から分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_gaikyo.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s002" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s002" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="SchoolScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020201']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s002" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s002')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s002')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s002')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_hensachi.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020202']['0103'] == 0 || Profile.categoryMap['020202']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s003" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s003" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="SchoolDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020202']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s003" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s003')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s003')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s003')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_setsumon.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">設問別成績</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020203']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s004" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s004" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="SchoolQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020203']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s004" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各科目の設問毎の受験人数、平均得点率を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s004')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s004')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s004')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「校内成績」には「評価別人数」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（校内成績）-->

<!-- 概要（過年度比較）-->
<c:if test="${ param.forward == 's101' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">過去の同学年の成績と比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_gaikyo2.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s102" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s102" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PrevScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020301']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s102" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s102')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s102')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s102')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_hensachi2.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020302']['0103'] == 0 || Profile.categoryMap['020302']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s103" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s103" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PrevDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020302']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s103" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s103')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s103')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s103')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_setsumon2.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過年度比較」には「設問別成績」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou2.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">志望大学評価別人数</b>
<!--個別設定アイコン--></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s104" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s104" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PrevUniv" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020303']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s104" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">大学別の志望者数、評価別人数を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s104')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s104')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s104')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（過年度比較）-->

<!-- 概要（クラス比較）-->
<c:if test="${ param.forward == 's201' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">クラス間で成績を比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_gaikyo3.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020401']['0103'] == 0 || Profile.categoryMap['020401']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s202" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s202" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s202" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="ClassScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020401']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s202" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s202" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s202" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s202" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s202')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s202')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s202')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_hensachi3.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020402']['0103'] == 0 || Profile.categoryMap['020402']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s203" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s203" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s203" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="ClassDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020402']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s203" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s203" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s203" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s203" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s203')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s203')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s203')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_setsumon3.gif" width="140" height="84" border="0" alt="設問別成績"><br>
</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">設問別成績</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020403']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s204" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s204" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s204" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="ClassQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020403']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s204" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s204" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s204" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s204" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各科目の設問毎の受験人数、平均得点率を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s204')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s204')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s204')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou3.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">志望大学評価別人数</b>
<!--個別設定アイコン-->
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s205" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s205" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s205" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="ClassUniv" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020404']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s205" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s205" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s205" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s205" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">大学別の志望者数、評価別人数を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s205')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s205')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s205')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（クラス比較）-->

<!-- 概要（他校比較）-->
<c:if test="${ param.forward == 's301' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">他の学校の成績と比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_gaikyo4.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020501']['0103'] == 0 || Profile.categoryMap['020501']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s302" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s302" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s302" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020501']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s302" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s302" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s302" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s302" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s302')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s302')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s302')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_hensachi4.gif" width="140" height="84" border="0" alt="偏差値分布"><br>
</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020502']['0103'] == 0 || Profile.categoryMap['020502']['0203'] == 0 }">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s303" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s303" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s303" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020502']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s303" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s303" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s303" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s303" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s303')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s303')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s303')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_setsumon4.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">設問別成績</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020503']['0203'] == 0 }">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s304" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s304" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s304" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020503']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s304" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s304" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s304" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s304" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各科目の設問毎の受験人数、平均得点率を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s304')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s304')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s304')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou4.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">志望大学評価別人数</b>
<!--個別設定アイコン-->
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s305" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s305" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s305" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherUniv" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020504']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s305" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s305" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s305" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s305" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">大学別の志望者数、評価別人数を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s305')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s305')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s305')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（他校比較）-->

<!-- 概要（過回比較）-->
<c:if test="${ param.forward == 's401' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">同系統模試の成績を比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_gaikyo5.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020601']['0103'] == 0 || Profile.categoryMap['020601']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s402_top" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s402_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s402_top" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PastScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020601']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s402_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s402_top" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s402_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s402_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s402')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s402')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s402')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_hensachi5.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020602']['0103'] == 0 || Profile.categoryMap['020602']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="s403_top" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="s403_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="s403_top" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PastDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020602']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="s403_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="s403_top" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="s403_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="s403_top" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('s403')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('s403')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('s403')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_setsumon5.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過回比較」には「設問別成績」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou5.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過回比較」には「評価別人数」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（過回比較）-->

<!-- 概要（営業・高校間比較）-->
<c:if test="${ param.forward == 'b001' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">高校間の模試成績を比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_gaikyo.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020501']['0103'] == 0 || Profile.categoryMap['020501']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="b002" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="b002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="b002" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020501']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="b002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="b002" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="b002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="b002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('b002')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('b002')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('b002')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_hensachi.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">偏差値分布</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020502']['0103'] == 0 || Profile.categoryMap['020502']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="b003" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="b003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="b003" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherDev" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020502']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="b003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="b003" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="b003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="b003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の偏差値分布を表やグラフで表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('b003')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('b003')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('b003')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_setsumon.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">設問別成績</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020503']['0103'] == 0 || Profile.categoryMap['020503']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="b004" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="b004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="b004" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020503']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="b004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="b004" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="b004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="b004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各科目の設問毎の受験人数、平均得点率を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('b004')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('b004')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('b004')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./school/img/g_shibou2.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">志望大学評価別人数</b><!--個別設定アイコン--></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="b005" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="univ" out="b005" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="b005" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="OtherUniv" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020504']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="univ" out="b005" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="b005" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="univ" out="b005" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="univ" out="b005" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">大学別の志望者数、評価別人数を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('b005')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('b005')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('b005')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（営業高校間比較）-->

<!-- 概要（営業・過回比較）-->
<c:if test="${ param.forward == 'b101' }">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td><span class="text14">高校間の模試成績を過回比較分析します。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--成績概況-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_gaikyo2.gif" width="140" height="84" border="0" alt="成績概況"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="271"><b class="text16" style="color:#657681;">成績概況</b>
<!--個別設定アイコン-->
<c:if test="${ Profile.categoryMap['020601']['0103'] == 0 || Profile.categoryMap['020601']['0203'] == 0}">
 <img src="./shared_lib/img/parts/icon_set.gif" width="78" height="17" border="0" alt="個別設定中" align="absmiddle" hspace="7">
</c:if>
</td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="b102" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="b102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="b102" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="PastScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['020601']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="b102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="b102" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="b102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="b102" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="389" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="389"><img src="./shared_lib/img/parts/sp.gif" width="389" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="389">
<tr>
<td width="270">

<table border="0" cellpadding="0" cellspacing="0" width="270">
<tr>
<td width="270"><span class="text14">各型・科目の受験人数、平均点、平均偏差値を表示します。</span></td>
</tr>
<tr>
<td width="270">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('b102')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('b102')">サンプルの表示</a></span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>

</td>
<td width="119" align="right"><a href="javascript:submitMenu('b102')"><img src="./shared_lib/img/btn/shousai.gif" width="95" height="36" border="0" alt="詳細" hspace="4"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/成績概況-->

<!--偏差値分布-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_hensachi2.gif" width="140" height="84" border="0" alt="偏差値分布"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過回比較」には「偏差値分布」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/偏差値分布-->

<!--設問別成績-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_setsumon2.gif" width="140" height="84" border="0" alt="設問別成績"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過回比較」には「設問別成績」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/設問別成績-->

<!--志望大学評価別人数-->
<div style="margin-top:9px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">


<table border="0" cellpadding="0" cellspacing="0" width="540">
<tr>
<td width="140"><!--画像--><img src="./business/img/g_shibou2.gif" width="140" height="84" border="0" alt="志望大学評価別人数"><br></td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<td width="389"><span class="text14">※「過回比較」には「評価別人数」の分析メニューはありません</span></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/志望大学評価別人数-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
</c:if>
<!-- /概要（営業・過回比較）-->

<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="590" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="590" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!---->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="95"><img src="./shared_lib/img/parts/icon_set_waku.gif" width="87" height="25" border="0" alt="個別設定中"><br></td>
<td width="206"><span class="text10">上記メニューの中にこのマークがある場合、共通設定を利用しない項目を含みます。</span></td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
<td width="285">

<table border="0" cellpadding="0" cellspacing="0" width="285" height="32">
<tr>
<td rowspan="3" width="94"><img src="./school/img/ttl_zenkoku.gif" width="94" height="32" border="0" alt="全国総合成績"><br></td>
<td width="190" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="190" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="32" border="0" alt=""><br></td>
</tr>
<tr>
<td width="190" bgcolor="FAFAFA">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="checkbox" name="outItem" value="ScoreTotal" onClick="change(this)"<c:if test="${ Profile.categoryMap['020100']['1301'] == 1 }"> checked</c:if>></td>
<td><span class="text12">一括出力対象</span></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="10"></td>
<c:set var="flag"><kn:exam screen="s000" /></c:set>
<c:choose>
<c:when test="${ flag == '1' }">
<td><a href="javascript:submitTotalScore(1)"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" hspace="5" align="absmiddle"><span class="text12">保存</span></a></td>
</c:when>
<c:otherwise>
<td><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" hspace="5" align="absmiddle"><span class="text12">保存</span></td>
</c:otherwise>
</c:choose>

</tr>
</table>
</td>
</tr>
<tr>
<td width="190" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="190" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!---->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td width="289">

<!--●●●右側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="62" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--一括出力-->
<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td><img src="./shared_lib/img/parts/ttl_lump.gif" width="289" height="38" border="0" alt="一括出力"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="286" bgcolor="#F3F5F5" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="286">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="224"><div style="margin-top:8px;"><span class="text12">以下の手順に従って一括出力ができます。</span></div></td>
<td width="50"><img src="./shared_lib/img/parts/ttl_lump_parts.gif" width="50" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td nowrap><span class="text12">対象年度 ： </span></td>
<td><select name="targetYear" class="text12" style="width:55px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr>
<td nowrap><span class="text12">対象模試 ： </span></td>
<td><select name="targetExam" class="text12" style="width:200px;" onChange="changeExamSelect()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step1-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">出力メニューの設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="29"><img src="./shared_lib/img/parts/yaji_l.gif" width="24" height="26" border="0" alt="←"><br></td>
<td width="220"><span class="text12">左のメニューで一括出力対象を確認の上、下のボタンを押してください。</span></td>
</tr>
<tr>
<td colspan="2"><div style="margin-top:5px;"><a href="javascript:changeExamSelect()"><img src="./shared_lib/img/btn/finish.gif" width="249" height="24" border="0" alt="設定完了"></a></div></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step2-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step3_cur.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<!--ボタン-->
<div style="margin-top:7px;">
<jsp:useBean id="common" class="java.util.ArrayList" />
<%-- 比較対象年度 --%>
<c:if test="${MenuSecurity.menu['603']}">
	<kn:listAdd var="common">
		<a href="javascript:openCommon(3)"><img src="./shared_lib/img/btn/com_nendo_<c:out value="${ComSetStatusBean.statusMap['year']}" />.gif" width="81" height="22" border="0" alt="比較対象年度"></a><br>
	</kn:listAdd>
</c:if>
<%-- 志望大学 --%>
<kn:listAdd var="common">
	<a href="javascript:openCommon(4)"><img src="./shared_lib/img/btn/com_daigaku_<c:out value="${ComSetStatusBean.statusMap['univ']}" />.gif" width="81" height="22" border="0" alt="志望大学"></a><br>
</kn:listAdd>
<%-- 比較対象クラス --%>
<c:if test="${MenuSecurity.menu['605']}">
	<kn:listAdd var="common">
		<a href="javascript:openCommon(5)"><img src="./shared_lib/img/btn/com_class_<c:out value="${ComSetStatusBean.statusMap['class']}" />.gif" width="81" height="22" border="0" alt="比較対象クラス"></a><br>
	</kn:listAdd>
</c:if>
<%-- 比較対象高校 --%>
<c:if test="${MenuSecurity.menu['606']}">
	<kn:listAdd var="common">
		<a href="javascript:openCommon(6)"><img src="./shared_lib/img/btn/com_koukou_<c:out value="${ComSetStatusBean.statusMap['school']}" />.gif" width="81" height="22" border="0" alt="比較対象高校"></a><br>
	</kn:listAdd>
</c:if>
<%-- SPACER --%>
<kn:listPad var="common" size="4">
</kn:listPad>
<%-- 出力 --%>
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="81"><a href="javascript:openCommon(1)"><img src="./shared_lib/img/btn/com_kata_<c:out value="${ComSetStatusBean.statusMap['type']}" />.gif" width="81" height="22" border="0" alt="型"></a><br></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><a href="javascript:openCommon(2)"><img src="./shared_lib/img/btn/com_kamoku_<c:out value="${ComSetStatusBean.statusMap['course']}" />.gif" width="81" height="22" border="0" alt="科目"></a><br></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><c:out value="${common[0]}" escapeXml="false" /></td>
</tr>
<tr>
<td colspan="5" width="249"><img src="./shared_lib/img/parts/sp.gif" width="3" height="6" border="0" alt=""><br></td>
</tr>
<tr>
<td width="81"><c:out value="${common[1]}" escapeXml="false" /></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><c:out value="${common[2]}" escapeXml="false" /></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><c:out value="${common[3]}" escapeXml="false" /></td>
</tr>
</table>
</div>
<!--/ボタン-->

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="18"><img src="./shared_lib/img/parts/color_box_00.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">設定済み</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_01.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">要確認</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_02.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">未設定</span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step3-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step4_cur.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">個別項目の確認</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249"><span class="text12">各詳細画面で個別項目の設定を確認してください。</span></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step4-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step5-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step5_cur.gif" width="46" height="12" border="0" alt="STEP5"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">一括出力</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="116"><a href="javascript:download()"><img src="./shared_lib/img/btn/hozon.gif" width="116" height="37" border="0" alt="保存"></a><br></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step5-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="289" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="289" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="31"><img src="./shared_lib/img/parts/sp.gif" width="31" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>
<%-- 2016/01/08 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

</form>
</body>
</html>

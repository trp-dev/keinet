<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="OnepointBean" scope="request" class="jp.co.fj.keinavi.beans.help.OnepointBean" />
<c:set value="school" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／校内成績分析</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	<%@ include file="/jsp/script/download.jsp" %>
	<%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_max.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_sample.jsp" %>
	<%@ include file="/jsp/script/validate_common.jsp" %>
	<%@ include file="/jsp/script/validate_max.jsp" %>
	<%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/graph_checker.jsp" %>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var printDialogPattern = "";
	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

	// フォームユーティリティ
	var util = new FormUtil();

	function changeState(obj) {
		// 分析対象の状態によりグラフ表示対象も変化させる
		var disabled = obj.checked ? false : "disabled";
		var cur  = new DOMUtil(obj);
     	<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD START --%>
		// var checkbox = cur.getParent(2).childNodes[2].childNodes[0];
        var checkbox = cur.getParent(2).children[2].children[0];
     	<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD END   --%>
		checkbox.disabled = disabled;
		// 分析対象がチェックされていなければグラフ表示対象も外す
		<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
		//if (!obj.checked) checkbox.checked = false;
		if (obj.checked) {
			checkbox.checked = true;
		} else {
			checkbox.checked = false;
		}
		<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	}

	function changeAllState(name) {
		var e = document.forms[0].elements[name];
		if (e == null) {
			return;
		}
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				changeState(e[i]);
			}
		} else {
			changeState(e)
		}
	}

	//型・科目別比較の際に、型・科目の設定を表示する
	function changeCompPrev( obj ) {
		// チェックされていれば表示する
		var dispYes = "none";
		var dispNo = "none";
		// 共通項目設定を利用する
		if (obj.checked) dispYes = "inline";
			else dispNo = "inline";// 共通項目設定を利用しない
		document.getElementById("CourseTypeSelect1").style.display = dispYes;
		document.getElementById("CourseTypeSelect2").style.display = dispNo;
		document.getElementById("CourseTypeSelectInfo").style.display = dispYes;
		if( obj.checked ){
			var typeDisp = document.getElementById("TypeInfo2").style.display;
			var courseDisp = document.getElementById("CourseInfo2").style.display;
			document.getElementById("SelectType").style.display = typeDisp;
			document.getElementById("SelectCourse").style.display = courseDisp;
		} else {
			document.getElementById("SelectType").style.display = "none";
			document.getElementById("SelectCourse").style.display = "none";
		}
		//一般のchangeも呼ぶ
		change(obj);
	}

	function change(obj) {
		if (obj.name == "formatItem") {
			// チェックされていれば表示する
			var display = obj.checked ? false : true;
			var elem,i,chkd;
			// 表
			if (obj.value == "Chart") {
				elem = document.forms[0].elements["compRatio"];
				chkd = false;
				if( elem != null ){
					<c:if test="${ isAbilityExam }">display = true;</c:if>
					for( i=0; i<elem.length; i++){
						elem[i].disabled = display;
						chkd = chkd || elem[i].checked;
					}
					//初期値の設定
					if( (!display) && (!chkd) ){
						for( i=0; i<elem.length; i++){
							if( elem[i].value == "11"){
								elem[i].checked = true;
							}
						}
					}
				}
				<%-- 校内成績分析・他校比較（偏差値分布） --%>
				<c:if test="${ param.forward == 's303' }">
					// 表チェック時のみ「過年度の表示」を表示する
					var e = document.getElementById("DispPast");
					if (!obj.checked) e.style.display = "none";
					else e.style.display = "inline";
				</c:if>
			// グラフ
			} else if (obj.value == "Graph") {
				elem = document.forms[0].elements["axis"];
				if( elem != null ){
					chkd = false;
					//動作をOffに
					for( i=0; i<elem.length; i++){
						elem[i].disabled = display;
						chkd = chkd || elem[i].checked;
					}
					//初期値の設定
					if( (!display) && (!chkd) ){
						for( i=0; i<elem.length; i++){
							if( elem[i].value == "number"){
								elem[i].checked = true;
							}
						}
					}
				}
			// 偏差値帯別度数分布グラフ
			} else if (obj.value == "GraphDist") {
				<c:if test="${ isAbilityExam }">display = true;</c:if>
				elem = document.forms[0].elements["axis"];
				if( elem != null ){
					chkd = false;
					//動作をOffに
					for( i=0; i<elem.length; i++){
						elem[i].disabled = display;
						chkd = chkd || elem[i].checked;
					}
					//初期値の設定
					if( (!display) && (!chkd) ){
						for( i=0; i<elem.length; i++){
							if( elem[i].value == "11"){
								elem[i].checked = true;
							}
						}
					}
				}

			}

		// オプション
		} else if (obj.name == "optionItem") {
			// チェックされていれば表示する
			var display = obj.checked ? false : true;
			var elem,i,chkd;
			// 偏差値帯別人数積み上げグラフ
			if (obj.value == "GraphBuildup") {
				<c:if test="${ isAbilityExam }">display = true;</c:if>
				elem = document.forms[0].elements["pitchBuildup"];
				chkd = false;
				for( i=0; i<elem.length; i++){
					elem[i].disabled = display;
					chkd = chkd || elem[i].checked;
				}
				//初期値の設定
				if( (!display) && (!chkd) ){
					for( i=0; i<elem.length; i++){
						if( elem[i].value == "2.5"){
							elem[i].checked = true;
						}
					}
				}
			// 偏差値帯別構成比グラフ
			} else if (obj.value == "GraphCompRatio") {
				<c:if test="${ isAbilityExam }">display = true;</c:if>
				elem = document.forms[0].elements["pitchCompRatio"];
				chkd = false;
				for( i=0; i<elem.length; i++){
					elem[i].disabled = display;
					chkd = chkd || elem[i].checked;
				}
				//初期値の設定
				if( (!display) && (!chkd) ){
					for( i=0; i<elem.length; i++){
						if( elem[i].value == "11"){
							elem[i].checked = true;
						}
					}
				}
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START --%>
			<%-- 2019/07/17 QQ)Tanioka 共通テスト対応 ADD START --%>
			// 共通テスト英語認定試験CEFR取得状況
<%--			} else if (obj.value == "OptionCheckBox") {
<%--				<c:if test="${ !isCenterResearch and !isMarkExam }">display = true;</c:if>
<%--				elem = document.forms[0].elements["targetCheckBox"];
<%--				chkd = false;
<%--				if(elem.length){
<%--					for( i=0; i<elem.length; i++){
<%--						elem[i].disabled = display;
<%--						chkd = chkd || elem[i].checked;
<%--					}
<%--				}else{
<%--					elem.disabled = display;
<%--					chkd = chkd || elem.checked;
<%--				}
				//初期値の設定
<%--				if( (!display) && (!chkd) ){
<%--					for( i=0; i<elem.length; i++){
<%--						if( elem[i].value == "0"){
<%--							elem[i].checked = false;
<%--						}
<%--					}
<%--				}
			<%-- 2019/07/17 QQ)Tanioka 共通テスト対応 ADD END   --%>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END --%>
			// 過回・過年度比較
			} else if (obj.value == "CompPrev") {
				var display = obj.checked ? "inline" : "none";
				document.getElementById("CommonCompYear").style.display = display;
				document.getElementById("InfoCompYear").style.display = display;
			// 過回・クラス比較
			} else if (obj.value == "CompClass") {
				var display = obj.checked ? "inline" : "none";
				document.getElementById("CommonCompClass").style.display = display;
				document.getElementById("InfoCompClass").style.display = display;
				document.getElementById("DispClassOrder").style.display = display;
				// 表示項目
				var dispItems = "none";
				var e = document.forms[0].optionItem;
				for (var i=0; i<e.length; i++) {
					if (e[i].checked && e[i].value != "CompPrev") {
						dispItems = "inline";
						break;
					}
				}
				document.getElementById("DispItems").style.display = dispItems;
			// 過回・他校比較
			} else if (obj.value == "CompOther") {
				var display = obj.checked ? "inline" : "none";
				document.getElementById("CommonCompSchool").style.display = display;
				document.getElementById("InfoCompSchool").style.display = display;
				document.getElementById("DispSchoolOrder").style.display = display;
				// 表示項目
				var dispItems = "none";
				var e = document.forms[0].optionItem;
				for (var i=0; i<e.length; i++) {
					if (e[i].checked && e[i].value != "CompPrev") {
						dispItems = "inline";
						break;
					}
				}
				document.getElementById("DispItems").style.display = dispItems;
			}

		// 分析対象
		} else if (obj.name == "analyzeItem") {
			var display = obj.checked ? false : true;
			if( obj.value == "univ" ){
				document.forms[0].elements["univ"] = display;
				//ラジオボタンをdisable
				var elems = document.forms[0].elements["choiceUnivMode"];
				for( var i=0; i<elems.length; i++){
					elems[i].disabled = display;
				}
				//志願者数・評価基準をdisable
				if( display ){
					document.forms[0].elements["candRange"].disabled = display;
					var elems2 = document.forms[0].elements["evaluation"];
					for( var i=0; i<elems2.length; i++){
						elems2[i].disabled = display;
					}
				} else {
					for( var i=0; i<elems.length; i++){
						change(elems[i]);
					}
				}
			}

		// 志望大学の分析モード
		} else if (obj.name == "choiceUnivMode"){
			var elems = document.forms[0].elements["choiceUnivMode"];
			for( var i=0; i<elems.length; i++){
				if( elems[i].value == 'number' ){
					var display = elems[i].checked ? false : true;
					document.forms[0].elements["candRange"].disabled = display;
				} else if( elems[i].value == 'evaluation' ){
					var display = elems[i].checked ? false : true;
					var elems2 = document.forms[0].elements["evaluation"];
					for( var i=0; i<elems2.length; i++){
						elems2[i].disabled = display;
					}
				}
			}

		// 型の選択・科目の選択
		} else {
			// チェックされていなければ評価しない
			if (obj.checked) {
				var dispYes = "none";
				var dispNo = "none";
				// 共通項目設定を利用する
				if (obj.value == "1") dispYes = "inline";
				// 共通項目設定を利用しない
				else dispNo = "inline";
				// 型
				if (obj.name == "commonType") {
					document.getElementById("SelectType").style.display = dispNo;
					document.getElementById("TypeInfo1").style.display = dispYes;
					document.getElementById("TypeInfo2").style.display = dispNo;
				} else if (obj.name == "commonCourse") {
					document.getElementById("SelectCourse").style.display = dispNo;
					document.getElementById("CourseInfo1").style.display = dispYes;
					document.getElementById("CourseInfo2").style.display = dispNo;
				}
			}

		}
	}

	function init() {
		startTimer();
		// スクロール位置を初期化する
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);

		// オプション設定
		var optionItem = document.forms[0].elements["optionItem"];
		if (optionItem != null) {
			// 複数
			if(optionItem.length){
				for (var i=0; i<optionItem.length; i++) change(optionItem[i]);

			// 単数
			} else {
				change(optionItem);
			}
		}

		// 分析対象
		var analyzeItem = document.forms[0].elements["analyzeItem"];
		if( analyzeItem != null ){
			var i;
			if( analyzeItem.length != null ){
				for ( i=0; i<analyzeItem.length; i++) {
					// 過回の３つなら処理を行う
					if (   (analyzeItem[i].value == "univ") ) {
						change(analyzeItem[i]);
					}
				}
			} else {
				if (   (analyzeItem.value == "univ") ) {
					change(analyzeItem);
				}
			}
		}

		// 型
		var commonType = document.forms[0].elements["commonType"];
		if (commonType) {
			change(commonType[0]);
			change(commonType[1]);
		}
		// 科目
		var commonCourse = document.forms[0].elements["commonCourse"];
		if (commonCourse) {
			change(commonCourse[0]);
			change(commonCourse[1]);
		}
		// 型・科目設定
		var compPrev = document.getElementById("CompPrev");
		//if (compPrev) {
		//  changeCompPrev( compPrev );
		//} else {
		//  document.getElementById("CourseTypeSelect1").style.display = "inline";
		//  document.getElementById("CourseTypeSelect2").style.display = "none";
		//}
		// 過年度の表示
		var formatItem = document.forms[0].elements["formatItem"];
		if( formatItem != null ){
			var i;
			if( formatItem.length != null ){
				for ( i=0; i<formatItem.length; i++) {
					// 過回の３つなら処理を行う
					if (   (formatItem[i].value == "Chart")
						|| (formatItem[i].value == "GraphDist") ) {
						change(formatItem[i]);
					}
				}
			} else {
				if (   (formatItem.value == "Chart")
					|| (formatItem.value == "GraphDist") ) {
					change(formatItem);
				}
			}
		}

		<%-- 過回比較・成績概況のみの処理 --%>
		<c:if test="${ param.forward == 's402' }">
			// オプション
			var dispItems = "none";
			var e = document.forms[0].optionItem;
			for (var i=0; i<e.length; i++) {
				// 過回・過年度比較
				if (e[i].value == "CompPrev") {
					// 過年度比較出来なければチェック無効
					if (<kn:exam screen="prev" /> == 0) e[i].disabled = "disabled";

				//「表示項目の選択 」の表示・非表示
				// 過回・クラス比較
				// 過回・他校比較
				} else if (e[i].checked) {
					dispItems = "inline";

				}
			}
			document.getElementById("DispItems").style.display = dispItems;
		</c:if>

		// 日程
		{
			var e = document.forms[0].dispUnivCount;
			if (e && e.length) {
				for (var i=0; i<e.length; i++) {
					if (e[i].checked) {
					<%-- 高1・2模試の場合は無条件で無効とする --%>
					<c:choose>
					<c:when test="${ isUniv12Exam }">
						document.forms[0].dispUnivSchedule.disabled = "disabled";
					</c:when>
					<c:otherwise>
						if (e[i].value == "2" || e[i].value == "4") {
							document.forms[0].dispUnivSchedule.disabled = "disabled";
						}
					</c:otherwise>
					</c:choose>
					}
				}
			}
		}
	}


  function checkType(i){
	i = i*1;
	var analyzeT = document.forms[0].analyzeType;
	var graphT = document.forms[0].graphType;

	if(analyzeT[i].checked == false){
	   graphT[i].checked = false;
	}
  }

  function checkCourse(i){
	i = i*1;
	var analyzeC = document.forms[0].analyzeCourse;
	var graphC = document.forms[0].graphCourse;

	if(analyzeC[i].checked == false){
	   graphC[i].checked = false;
	}
  }

	<%-- 志望大学評価別人数・過年度の表示のチェック --%>
	function dispUnivYearChecked(obj) {
		var count = 0;
		var e = document.forms[0].dispUnivYear;
		<%-- 4つ以下ならここまで --%>
		if (!e.length || e.length <= 4) return;
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) count++;
		}
		if (count > 4) {
			obj.checked = false;
			alert("<kn:message id="s017a" />");
			return;
		}
	}

	<%-- 志望大学評価別人数・大学集計区分の変更 --%>
	function dispUnivCountChecked(obj) {
		<%-- 高1・2模試の場合は無条件で無効とする --%>
		<c:choose>
		<c:when test="${ isUniv12Exam }">
			document.forms[0].dispUnivSchedule.disabled = "disabled";
		</c:when>
		<c:otherwise>
			if (obj.value == "1" || obj.value == "3" || obj.value == "5") {
				document.forms[0].dispUnivSchedule.disabled = false;
			} else {
				document.forms[0].dispUnivSchedule.disabled = "disabled";
			}
		</c:otherwise>
		</c:choose>
	}

 -->
</script>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onLoad="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" /> ">
<form action="<c:url value="SchoolDetail" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changeExam" value="0">
<input type="hidden" name="save" value="0">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="36" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="887">
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="887">
<tr>
<td width="737">
	<table border="0" cellpadding="0" cellspacing="0" width="737">
	<tr valign="top">
	<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
	<td width="721">

	<table border="0" cellpadding="0" cellspacing="0" width="711">
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="721" height="2" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="721" height="3" border="0" alt=""><br></td>
	</tr>
	<tr>
	<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
	<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
	<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>

	<td width="705" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt="">
	 <b class="text16" style="color:#FFFFFF;">


	<c:choose>
	  <c:when test="${ param.forward == 's002' }">校内成績（成績概況）</c:when>
	  <c:when test="${ param.forward == 's003' }">校内成績（偏差値分布）</c:when>
	  <c:when test="${ param.forward == 's004' }">校内成績（設問別成績）</c:when>
	  <c:when test="${ param.forward == 's102' }">過年度比較（成績概況）</c:when>
	  <c:when test="${ param.forward == 's103' }">過年度比較（偏差値分布）</c:when>
	  <c:when test="${ param.forward == 's104' }">過年度比較（志望大学評価別人数）</c:when>
	  <c:when test="${ param.forward == 's202' }">クラス比較（成績概況）</c:when>
	  <c:when test="${ param.forward == 's203' }">クラス比較（偏差値分布）</c:when>
	  <c:when test="${ param.forward == 's204' }">クラス比較（設問別成績）</c:when>
	  <c:when test="${ param.forward == 's205' }">クラス比較（志望大学評価別人数）</c:when>
	  <c:when test="${ param.forward == 's302' }">他校比較（成績概況）</c:when>
	  <c:when test="${ param.forward == 's303' }">他校比較（偏差値分布）</c:when>
	  <c:when test="${ param.forward == 's304' }">他校比較（設問別成績）</c:when>
	  <c:when test="${ param.forward == 's305' }">他校比較（志望大学評価別人数）</c:when>
	  <c:when test="${ param.forward == 'b002' }">高校間比較（成績概況）</c:when>
	  <c:when test="${ param.forward == 'b003' }">高校間比較（偏差値分布）</c:when>
	  <c:when test="${ param.forward == 'b004' }">高校間比較（設問別成績）</c:when>
	  <c:when test="${ param.forward == 'b005' }">高校間比較（志望大学評価別人数）</c:when>
	  <c:when test="${ param.forward == 's402' || param.forward == 'b102'}">過回比較（成績概況）</c:when>
	  <c:when test="${ param.forward == 's403' || param.forward == 'b103'}">過回比較（偏差値分布）</c:when>
	</c:choose>
	</b>
	</td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="721" height="3" border="0" alt=""><br></td>
	</tr>
	<tr valign="top">
	<td colspan="4" width="721" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="721" height="2" border="0" alt=""><br></td>
	</tr>
	</table>

	</td>
	<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
	</tr>
	</table>
</td>
<td align="center" width="150"><input type="button" class="text12" onclick="openSample('<c:out value="${param.forward}" />');" value="サンプルの表示"></td>
</tr>
</table>
<!--/大タイトル-->
</td>
<td width="41"><img src="./shared_lib/img/parts/sp.gif" width="41" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->




<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="689" align="center">
<!--●●●左側●●●-->
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="689">
<tr>
<td width="669"><span class="text14">このメニューでの出力項目を以下の手順に従って設定してください。</span></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/説明-->

<!--ボタン-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step1.gif" width="86" height="24" border="0" alt="STEP1"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--対象模試の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象年度 ： </span></td>
<td><select name="targetYear" class="text12" style="width:60px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象模試 ： </span></td>
<td><select name="targetExam" class="text12" style="width:220px;" onChange="changeExamSelect()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/対象模試の設定-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step2.gif" width="86" height="24" border="0" alt="STEP2"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--共通項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">

<c:choose>
  <c:when test="${ param.forward == 's002' }">
	<td align="center"><span class="text14-hh">※この帳票では共通項目を使用しません。</span></td>
  </c:when>
  <c:otherwise>
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">
	  <table border="0" cellpadding="0" cellspacing="0" width="572">
	  <tr valign="top">
	  <td><span class="text14-hh">この分析で利用する共通項目です。<br>
	  <c:forTokens var="screen" items="s003,s103,s202,s203,s302,s303,s402,s403,b002,b003,b102,b103" delims=",">
		<c:if test="${ param.forward == screen }">
		  ※「型」・「科目」で共通項目を利用しない場合は下記で切り替えることができます。
		</c:if>
	  </c:forTokens>
	  <c:forTokens var="screen" items="s004,s204,s304" delims=",">
		<c:if test="${ param.forward == screen }">
		  ※「科目」で共通項目を利用しない場合は下記で切り替えることができます。
		</c:if>
	  </c:forTokens>
	  </span></td>
	  </tr>
	  <tr valign="top">
	  <td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
	  </tr>
	  <tr valign="top">
	  <td align="right"><span class="text12">共通項目設定の最終更新日 ： <fmt:formatDate value="${Profile.updateDate}" pattern="yyyy/MM/dd" /></span></td>
	  </tr>
	  </table>

	  <table border="0" cellpadding="0" cellspacing="2" width="576">
	  <tr valign="top" bgcolor="#CDD7DD">
	  <td colspan="3" width="574" height="24"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
	  </tr>
	  <!-- 型の選択-->
	  <c:forTokens var="screen" items="s003,s103,s202,s203,s302,s303,s402,s403,b002,b003,b102,b103" delims=",">
		<c:if test="${ param.forward == screen }">
		  <tr height="56">
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_kata.gif" width="30" height="30" border="0" alt="型の選択" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">型の選択</b></td>
		  </tr>
		  </table>
		  </td>
		  <td width="33%" bgcolor="#F4E5D6">
		  <table border="0" cellpadding="0" cellspacing="3">
		  <tr>
		  <td><input type="radio" name="commonType" value="1" onClick="javascript:change(this)"<c:if test="${ form.commonType == '1' }"> checked</c:if><c:if test="${ isAbilityExam }"> disabled</c:if>></td>
		  <td><span class="text12">共通項目設定を利用する</span></td>
		  </tr>
		  <tr>
		  <td><input type="radio" name="commonType" value="0" onClick="javascript:change(this)"<c:if test="${ form.commonType == '0' }"> checked</c:if><c:if test="${ isAbilityExam }"> disabled</c:if>></td>
		  <td><span class="text12">共通項目設定を利用しない</span></td>
		  </tr>
		  </table>
		  </td>
		  <td width="33%" bgcolor="#F4E5D6" align="center" id="TypeInfo1"><span class="text12"><a href="javascript:openCommon(1)"><kn:com item="type" /></a></span></td>
		  <td width="33%" bgcolor="#F4E5D6" align="center" id="TypeInfo2" style="display:none"><b class="text12" style="color:#FF6E0E;">STEP3より選択</b></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /型の選択-->
	  <!-- 科目の選択-->
	  <c:forTokens var="screen" items="s003,s004,s103,s202,s203,s204,s302,s303,s304,s402,s403,b002,b003,b004,b102,b103" delims=",">
		<c:if test="${ param.forward == screen }">
		  <tr height="56">
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_kamoku.gif" width="30" height="30" border="0" alt="科目の選択" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">科目の選択</b></td>
		  </tr>
		  </table>
		  </td>
		  <td width="33%" bgcolor="#F4E5D6">
		  <table border="0" cellpadding="0" cellspacing="3">
		  <tr>
		  <td><input type="radio" name="commonCourse" value="1" onClick="javascript:change(this)" <c:if test="${form.commonCourse == '1'}">checked</c:if>></td>
		  <td><span class="text12">共通項目設定を利用する</span></td>
		  </tr>
		  <tr>
		  <td><input type="radio" name="commonCourse" value="0" onClick="javascript:change(this)" <c:if test="${form.commonCourse == '0'}">checked</c:if>></td>
		  <td><span class="text12">共通項目設定を利用しない</span></td>
		  </tr>
		  </table>
		  </td>
		  <td width="33%" bgcolor="#F4E5D6" align="center" id="CourseInfo1"><span class="text12"><a href="javascript:openCommon(2)"><kn:com item="course" /></a></span></td>
		  <td width="33%" bgcolor="#F4E5D6" align="center" id="CourseInfo2" style="display:none"><b class="text12" style="color:#FF6E0E;">STEP3より選択</b></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /科目の選択-->
	  <!-- 比較対象年度の選択-->
	  <c:forTokens var="screen" items="s102,s103,s104,s402" delims=",">
		<c:if test="${ param.forward == screen }">
		  <%-- s402の場合は初期非表示 --%>
		  <tr height="36"<c:if test="${ param.forward == 's402' }"> style="display:none" id="CommonCompYear"</c:if>>
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_year.gif" width="30" height="30" border="0" alt="比較対象年度の選択" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">比較対象年度の選択</b></td>
		  </tr>
		  </table>
		  </td>
		  <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(3)"><kn:com item="year" /></a></span></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /比較対象年度の選択-->
	  <!-- 志望大学-->
	  <c:forTokens var="screen" items="s104,s205,s305,b005" delims=",">
		<c:if test="${ param.forward == screen }">
		  <tr height="36">
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_shibou.gif" width="30" height="30" border="0" alt="志望大学" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">志望大学</b></td>
		  </tr>
		  </table>
		  </td>
		  <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(4)"><kn:com item="univ" /></a></span></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /比志望大学-->
	  <!-- 比較対象クラス-->
	  <c:forTokens var="screen" items="s202,s203,s204,s205,s402" delims=",">
		<c:if test="${ param.forward == screen }">
		  <%-- s402の場合は初期非表示 --%>
		  <tr height="36"<c:if test="${ param.forward == 's402' }"> style="display:none" id="CommonCompClass"</c:if>>
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_class.gif" width="30" height="30" border="0" alt="比較対象クラス" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">比較対象クラス</b></td>
		  </tr>
		  </table>
		  </td>
		  <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(5)"><kn:com item="class" /></a></span></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /比較対象クラス-->
	  <!-- 比較対象高校-->
	  <c:forTokens var="screen" items="s302,s303,s304,s305,s402,b002,b003,b004,b005,b102" delims=",">
		<c:if test="${ param.forward == screen }">
		  <%-- s402の場合は初期非表示 --%>
		  <tr height="36"<c:if test="${ param.forward == 's402' }"> style="display:none" id="CommonCompSchool"</c:if>>
		  <td width="34%" bgcolor="#E1E6EB">
		  <table border="0" cellpadding="0" cellspacing="0">
		  <tr>
		  <td><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校" align="absmiddle" hspace="6"></td>
		  <td><b class="text12">比較対象高校</b></td>
		  </tr>
		  </table>
		  </td>
		  <td colspan="2" width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(6)"><kn:com item="school" /></a></span></td>
		  </tr>
		</c:if>
	  </c:forTokens>
	  <!-- /比較対象高校-->
	  </table>
	</td>
  </c:otherwise>
</c:choose>

<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/共通項目の設定-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step3.gif" width="86" height="24" border="0" alt="STEP3"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="547" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">個別項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--個別項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">

<!-- 型の選択-->
<c:forTokens var="screen" items="s003,s103,s202,s203,s302,s303,s402,s403,b002,b003,b102,b103" delims=",">
  <c:if test="${ param.forward == screen }">
	<tr valign="top" id="SelectType" style="display:none">
	<td width="648" bgcolor="#EFF2F3" align="center">

	<div style="margin-top:7px;">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr>
	<td width="42"><img src="./shared_lib/img/parts/icon_kata.gif" width="30" height="30" border="0" alt="型の選択" hspace="3"></td>
	<td width="578"><b class="text14" style="color:#657681;">型の選択</b></td>
	</tr>
	</table>
	</div>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><span class="text14-hh">分析対象とする型を選択し、さらにグラフ表示をさせたい型を選択してください。<BR>※分析対象の選択をせずにグラフ表示に設定することはできません。</span></td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<table border="0" cellpadding="4" cellspacing="0" width="572">
	<tr>
	<td width="741" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'analyzeType');changeAllState('analyzeType');"></td>
	--%>
	<td><input type="checkbox" name="dummy1" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyzeType');changeAllState('analyzeType');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">分析対象をすべて選択</b></td>
	<td>&nbsp;&nbsp;</td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graphType');"></td>
	--%>
	<td><input type="checkbox" name="dummy1" value="" onclick="util.checkAllElements(this.checked, 'graphType');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

	<!--リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="576">
	<tr valign="top">
	<td width="283">
	<!--左側-->
	<table border="0" cellpadding="2" cellspacing="2" width="283">
	<tr bgcolor="#CDD7DD">
	<td width="13%">&nbsp;</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">分析<br>対象</span></td>
	</tr>
	</table>
	</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">グラフ<br>表示</span></td>
	</tr>
	</table>
	</td>
	<td width="55%">
	<table border="0" cellpadding="4" cellspacing="0" width="100%">
	<tr>
	<td><span class="text12">型名</span></td>
	<td align="right" nowrap><span class="text12">受験人数</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<c:set var="begin" value="0" />
	<c:set var="SubjectBean" value="${TypeBean}" />
	<%@ include file="/jsp/shared_lib/cm_type.jsp" %>
	</table>
	<!--/左側-->
	</td>
	<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
	<td width="283">
	<!--右側-->
	<table border="0" cellpadding="2" cellspacing="2" width="283">
	<tr bgcolor="#CDD7DD">
	<td width="13%">&nbsp;</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">分析<br>対象</span></td>
	</tr>
	</table>
	</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">グラフ<br>表示</span></td>
	</tr>
	</table>
	</td>
	<td width="55%">
	<table border="0" cellpadding="4" cellspacing="0" width="100%">
	<tr>
	<td><span class="text12">型名</span></td>
	<td align="right" nowrap><span class="text12">受験人数</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<c:set var="begin" value="${TypeBean.fullListHalfSize}" />
	<%@ include file="/jsp/shared_lib/cm_type.jsp" %>
	</table>
	<!--/右側-->
	</td>
	</tr>
	</table>
	<!--/リスト-->

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

	<!--注意-->
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td width="620" align="right">
	<div style="margin-top:6px;">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
	</tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	<!--/注意-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
  </c:if>
</c:forTokens>
<!-- /型の選択-->

<!-- 科目の選択-->
<c:forTokens var="screen" items="s003,s004,s103,s202,s203,s204,s302,s303,s304,s402,s403,b002,b003,b004,b102,b103" delims=",">
  <c:if test="${ param.forward == screen }">
	<tr valign="top" id="SelectCourse" style="display:none">
	<td width="648" bgcolor="#EFF2F3" align="center">

	<div style="margin-top:7px;">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr>
	<td width="42"><img src="./shared_lib/img/parts/icon_kamoku.gif" width="30" height="30" border="0" alt="科目の選択" hspace="3"></td>
	<td width="578"><b class="text14" style="color:#657681;">科目の選択</b></td>
	</tr>
	</table>
	</div>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><span class="text14-hh">分析対象とする科目を選択し、さらにグラフ表示をさせたい科目を選択してください。<BR>※分析対象の選択をせずにグラフ表示に設定することはできません。</span></td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<table border="0" cellpadding="4" cellspacing="0" width="572">
	<tr>
	<td width="741" bgcolor="#93A3AD">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr height="27">
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'analyzeCourse');changeAllState('analyzeCourse');"></td>
	--%>
	<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyzeCourse');changeAllState('analyzeCourse');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">分析対象をすべて選択</b></td>
	<td>&nbsp;&nbsp;</td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graphCourse');"></td>
	--%>
	<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, 'graphCourse');"></td>
	<%-- 2016/03/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

	<!--リスト-->
	<table border="0" cellpadding="0" cellspacing="0" width="576">
	<tr valign="top">
	<td width="283">
	<!--左側-->
	<table border="0" cellpadding="2" cellspacing="2" width="283">
	<tr bgcolor="#CDD7DD">
	<td width="13%">&nbsp;</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">分析<br>対象</span></td>
	</tr>
	</table>
	</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">グラフ<br>表示</span></td>
	</tr>
	</table>
	</td>
	<td width="55%">

	<table border="0" cellpadding="4" cellspacing="0" width="100%">
	<tr>
	<td><span class="text12">科目名</span></td>
	<td align="right" nowrap><span class="text12">受験人数</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<c:set var="begin" value="0" />
	<c:set var="SubjectBean" value="${CourseBean}" />
	<%@ include file="/jsp/shared_lib/cm_course.jsp" %>
	</table>
	<!--/左側-->
	</td>
	<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
	<td width="283">
	<!--右側-->
	<table border="0" cellpadding="2" cellspacing="2" width="283">
	<tr bgcolor="#CDD7DD">
	<td width="13%">&nbsp;</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">分析<br>対象</span></td>
	</tr>
	</table>
	</td>
	<td width="16%">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
	<td><span class="text12">グラフ<br>表示</span></td>
	</tr>
	</table>
	</td>
	<td width="55%">
	<table border="0" cellpadding="4" cellspacing="0" width="100%">
	<tr>
	<td><span class="text12">科目名</span></td>
	<td align="right" nowrap><span class="text12">受験人数</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<c:set var="begin" value="${CourseBean.fullListHalfSize}" />
	<%@ include file="/jsp/shared_lib/cm_course.jsp" %>
	</table>
	<!--/右側-->
	</td>
	</tr>
	</table>
	<!--/リスト-->

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>

	<!--注意-->
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td width="620" align="right">
	<div style="margin-top:6px;">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている科目" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
	</tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	<!--/注意-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
  </c:if>
</c:forTokens>
<!-- /科目の選択-->


<!--表示・印刷形式の選択-->
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">

<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr>
<td width="42"><img src="./shared_lib/img/parts/icon_hyoji.gif" width="30" height="30" border="0" alt="表示形式の選択" hspace="3"></td>
<td width="378"><b class="text14" style="color:#657681;">表示形式の選択</b></td>
<td width="200" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="12"><a href="javascript:openSample('<c:out value="${param.forward}" />')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→"></a></td>
<td><span class="text12"><a href="javascript:openSample('<c:out value="${param.forward}" />')">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<!--フォーマットの選択-->
<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="576" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td><b class="text14-hh">フォーマットの選択</b></td>
</tr>
</table>

<!-- フォーマットの選択-->
<%-- チェックを復元する --%>
<c:set var="c1" value="" />
<c:set var="c2" value="" />
<c:forEach var="format" items="${form.formatItem}">
  <c:if test="${ format == 'Chart' }"><c:set var="c1" value=" checked" /></c:if>
  <c:if test="${ format == 'Graph' }"><c:set var="c2" value=" checked" /></c:if>
</c:forEach>
<c:forTokens var="screen" items="s002,s004,s102,s104,s202,s204,s205,s302,s304,s305,s402" delims=",">
  <c:if test="${ param.forward == screen }">
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	<!--一段目-->
	<table border="0" cellpadding="0" cellspacing="0" width="573">
	<tr valign="top">
	<td width="187">
	<!--表-->
	<table border="0" cellpadding="0" cellspacing="0" width="187">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="187">
	<tr>
	<td width="185" height="26" bgcolor="#E5EEF3">
	<table border="0" cellpadding="0" cellspacing="3">
	<tr>
	<td>
	<c:forTokens var="screen" items="s002,s004,s102,s202,s204,s302,s304,s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<input type="checkbox" name="formatItem" value="Chart"<c:out value="${c1}" />>
	  </c:if>
	</c:forTokens>
	</td>
	<td><span class="text12">表</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr valign="top">
	<td width="185" bgcolor="#FFFFFF" align="center">

	<div style="margin-top:9px;">
	<table border="0" cellpadding="0" cellspacing="0" width="171">
	<tr valign="top">
	<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
	<td width="78"></td>
	</tr>
	</table>
	</div>

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/表-->
	</td>
	<c:forTokens var="screen" items="s002,s004,s102,s202,s204,s302,s304,s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="26" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td><input type="checkbox" name="formatItem" value="Graph"<c:out value="${c2}" />></td>
		<td><span class="text12">グラフ</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト-->
		  <c:choose>
			<c:when test="${ screen == 's402' }">
			  <img src="./school/img/graph03.gif" width="85" height="64" border="0" alt="グラフ"><br>
			</c:when>
			<c:otherwise>
			  <img src="./school/img/graph02.gif" width="85" height="64" border="0" alt="グラフ"><br>
			</c:otherwise>
		  </c:choose>
		</td>
		<td width="78"></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
	  </c:if>
	</c:forTokens>
	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td width="187"></td>
	</tr>
	</table>
	<!--/一段目-->
  </c:if>
</c:forTokens>


<!--二段目-->
<c:forTokens var="screen" items="s003,s103,s203,s303,s403" delims=",">
  <c:if test="${ param.forward == screen }">
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="573">
	<tr valign="top">
	<td width="187">
	<!--表-->
	<table border="0" cellpadding="0" cellspacing="0" width="187">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="187">
	<tr>
	<td width="185" height="26" bgcolor="#E5EEF3">
	<table border="0" cellpadding="0" cellspacing="3">
	<tr>
	<td>
	<c:set value="false" var="flag"/>
	<c:forEach var="formItem" items="${form.formatItem}">
	  <c:if test="${formItem == 'Chart'}">
		<c:set value="true" var="flag"/>
		<input type="checkbox" name="formatItem" value="Chart" onClick="javascript:change(this)" checked>
	  </c:if>
	</c:forEach>
	<c:if test="${flag == false}">
	  <input type="checkbox" name="formatItem" value="Chart" onClick="javascript:change(this)">
	</c:if>
	</td>
	<td><span class="text12">表</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr valign="top">
	<td width="185" bgcolor="#FFFFFF" align="center">

	<div style="margin-top:9px;">
	<table border="0" cellpadding="0" cellspacing="0" width="171">
	<tr valign="top">
	<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
	<td width="78">
	<table border="0" cellpadding="0" cellspacing="0" width="78">
	<tr>
	<td colspan="2" width="78"><span class="text12">構成比表示：</span></td>
	</tr>
	<tr>
	<td width="22"><input type="radio" name="compRatio" value="11" <c:if test="${form.compRatio == '1' || form.compRatio == '11'}">checked</c:if>></td>
	<td width="56"><span class="text12">あり</span></td>
	</tr>
	<tr>
	<td width="22"><input type="radio" name="compRatio" value="12" <c:if test="${form.compRatio == '2' || form.compRatio == '12'}">checked</c:if>></td>
	<td width="56"><span class="text12">なし</span></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/表-->
	</td>
	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td width="187">
	<!--グラフ-->
	<table border="0" cellpadding="0" cellspacing="0" width="187">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="187">
	<tr>
	<td width="185" height="26" bgcolor="#E5EEF3">
	<table border="0" cellpadding="0" cellspacing="3">
	<tr>
	<td>
	<c:set value="false" var="flag"/>
	<c:forEach var="formItem" items="${form.formatItem}">
	  <c:if test="${formItem == 'GraphDist'}">
		<c:set value="true" var="flag"/>
		<input type="checkbox" name="formatItem" value="GraphDist" onClick="javascript:change(this)" checked<c:if test="${ isAbilityExam }"> disabled</c:if>>
	  </c:if>
	</c:forEach>
	<c:if test="${flag == false}">
	   <input type="checkbox" name="formatItem" value="GraphDist" onClick="javascript:change(this)"<c:if test="${ isAbilityExam }"> disabled</c:if>>
	</c:if>
	</td>
	<td><span class="text12">偏差値帯別度数分布グラフ</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr valign="top">
	<td width="185" bgcolor="#FFFFFF" align="center">

	<div style="margin-top:9px;">
	<table border="0" cellpadding="0" cellspacing="0" width="171">
	<tr valign="top">
	<td width="93"><!--イラスト-->
		  <c:choose>
			<c:when test="${ screen == 's003' }">
			  <img src="./school/img/graph03.gif" width="85" height="64" border="0" alt="グラフ"><br>
			</c:when>
			<c:otherwise>
			  <img src="./school/img/graph02.gif" width="85" height="64" border="0" alt="グラフ"><br>
			</c:otherwise>
		  </c:choose>
	</td>
	<td width="78">
	<table border="0" cellpadding="0" cellspacing="0" width="78">
	<tr>
	<td colspan="2" width="78"><span class="text12">グラフ軸：</span></td>
	</tr>
	<tr>
	<td width="22"><input type="radio" name="axis" value="11" <c:if test="${ form.axis == '1' || form.axis == '11' }">checked</c:if>></td>
	<td width="56"><span class="text12">人数</span></td>
	</tr>
	<tr>
	<td width="22"><input type="radio" name="axis" value="22" <c:if test="${ (form.axis == '2' || form.axis == '22') }">checked</c:if>></td>
	<td width="56"><span class="text12">構成比</span></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/グラフ-->
	</td>
	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td width="187"></td>
	</tr>
	</table>
	<!--/二段目-->
  </c:if>
</c:forTokens>
<!-- /フォーマットの選択-->

<!-- フォーマットの選択・営業-->
<c:forTokens var="screen" items="b002,b004,b005,b102" delims=",">
  <c:if test="${ param.forward == screen }">
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	<!--一段目-->
	<table border="0" cellpadding="0" cellspacing="0" width="573">
	<tr valign="top">
	<td width="187">
	<!--表-->
	<table border="0" cellpadding="0" cellspacing="0" width="187">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="187">
	<tr>
	<td width="185" height="26" bgcolor="#E5EEF3">
	<table border="0" cellpadding="0" cellspacing="3">
	<tr>
	<td>
	  <c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<input type="checkbox" name="formatItem" value="Chart"<c:out value="${c1}" />>
	  </c:if>
	</c:forTokens>
	</td>

	<td><span class="text12">表</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr valign="top">
	<td width="185" bgcolor="#FFFFFF" align="center">

	<div style="margin-top:9px;">
	<table border="0" cellpadding="0" cellspacing="0" width="171">
	<tr valign="top">
	<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
	<td width="78"></td>
	</tr>
	</table>
	</div>

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/表-->
	 <c:forTokens var="screen" items="s002,s004,s102,s202,s204,s302,s304,s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="26" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td><input type="checkbox" name="formatItem" value="Graph"<c:out value="${c2}" />></td>
		<td><span class="text12">グラフ</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph02.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
		<td width="78"></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
	  </c:if>
	</c:forTokens>

	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
	<td width="187"></td>
	</tr>
	</table>
	<!--/一段目-->
  </c:if>
</c:forTokens>


<!--二段目-->
<c:forTokens var="screen" items="b003" delims=",">
  <c:if test="${ param.forward == screen }">
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="573">
	<tr valign="top">
	<td width="187">
	<!--表-->
	<table border="0" cellpadding="0" cellspacing="0" width="187">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="187">
	<tr>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--	<td width="185" height="35" bgcolor="#E5EEF3">--%>
	<td width="185" height="26" bgcolor="#E5EEF3">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
	<table border="0" cellpadding="0" cellspacing="3">
	<tr>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--	<td>--%>
<%--	<c:set value="false" var="flag"/>--%>
<%--	<c:forEach var="formItem" items="${form.formatItem}">--%>
<%--	  <c:if test="${formItem == 'Chart'}">--%>
<%--		<c:set value="true" var="flag"/>--%>
<%--		<input type="checkbox" name="formatItem" value="Chart" onClick="javascript:change(this)" checked>--%>
<%--	  </c:if>--%>
<%--	</c:forEach>--%>
<%--	<c:if test="${flag == false}">--%>
<%--		<input type="checkbox" name="formatItem" value="Chart" onClick="javascript:change(this)">--%>
<%--	</c:if>--%>
<%--	</td>--%>
	<td><br></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
	<td><span class="text12">表</span></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr valign="top">
	<td width="185" bgcolor="#FFFFFF" align="center">

	<div style="margin-top:9px;">
	<table border="0" cellpadding="0" cellspacing="0" width="171">
	<tr valign="top">
	<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td>
	<td width="78">
	<table border="0" cellpadding="0" cellspacing="0" width="78">
	<tr>
	<td colspan="2" width="78"><span class="text12">構成比表示：</span></td>
	</tr>
	<tr>
	<%-- 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
	<%--<td width="22"><input type="radio" name="compRatio" value="11" <c:if test="${form.compRatio == '1' || form.compRatio == '11'}">checked</c:if>></td> --%>
	<td width="22"><input type="radio" name="compRatio" value="11"<c:if test="${form.compRatio == '1' || form.compRatio == '11'}"> checked</c:if><c:if test="${ isAbilityExam }"> disabled</c:if>></td>
	<%-- 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
	<td width="56"><span class="text12">あり</span></td>
	</tr>
	<tr>
	<%-- 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD START --%>
	<%--<td width="22"><input type="radio" name="compRatio" value="12" <c:if test="${form.compRatio == '2' || form.compRatio == '12'}">checked</c:if>></td> --%>
	<td width="22"><input type="radio" name="compRatio" value="12"<c:if test="${form.compRatio == '2' || form.compRatio == '12'}"> checked</c:if><c:if test="${ isAbilityExam }"> disabled</c:if>></td>
	<%-- 2019/11/21 QQ)Tanioka 英語認定試験延期対応 UPD END   --%>
	<td width="56"><span class="text12">なし</span></td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--/表-->
	</td>
	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL START --%>
<%--	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>--%>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 DEL END --%>
	<td width="187"></td>
	</tr>
	</table>
	<!--/二段目-->
  </c:if>
</c:forTokens>
<!-- /フォーマットの選択・営業-->

</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/フォーマットの選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START --%>
<%-- <c:forTokens var="screen" items="s003,s103,s203,s204,s303,s402,s403" delims=","> --%>
<c:forTokens var="screen" items="s003,s103,s203,s303,s402,s403" delims=",">
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END --%>
  <c:if test="${ param.forward == screen }">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<!-- オプションの選択-->
	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><b class="text14-hh">オプションの選択</b></td>
	</tr>
	</table>

	<c:forTokens var="screen" items="s003,s103,s203,s303,s403" delims=",">
	  <c:if test="${ param.forward == screen }">
		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->

		<!--一段目-->
		<table border="0" cellpadding="0" cellspacing="0" width="573">
		<tr valign="top">
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="36" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
		<c:set value="false" var="flag"/>
		<c:forEach var="optItem" items="${form.optionItem}">
		  <c:if test="${optItem == 'GraphBuildup'}">
	  <c:set value="true" var="flag"/>
			<input type="checkbox" name="optionItem" value="GraphBuildup" onClick="javascript:change(this)" checked<c:if test="${ isAbilityExam }"> disabled</c:if>>
		  </c:if>
		</c:forEach>
		<c:if test="${flag == false}">
		  <input type="checkbox" name="optionItem" value="GraphBuildup" onClick="javascript:change(this)"<c:if test="${ isAbilityExam }"> disabled</c:if>>
		</c:if>
		</td>
		<td><span class="text12">偏差値帯別人数積み上げ<br>グラフ</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph04.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
		<td width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--		<table border="0" cellpadding="0" cellspacing="0" width="78" height="72"> --%>
		<table border="0" cellpadding="0" cellspacing="0" width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
		<c:choose>
		<c:when test="${NewExam}">
		<tr>
		<td colspan="2" width="78"><span class="text12">得点ピッチ：</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="11" <c:if test="${ form.pitchBuildup == '1' || form.pitchBuildup == '11'}">checked</c:if>></td>
		<td width="56"><span class="text12">10</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="12" <c:if test="${ form.pitchBuildup == '2' || form.pitchBuildup == '12'}">checked</c:if>></td>
		<td width="56"><span class="text12">20</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="13" <c:if test="${ form.pitchBuildup == '3' || form.pitchBuildup == '13'}">checked</c:if>></td>
		<td width="56"><span class="text12">40</span></td>
		</tr>
		</c:when>
		<c:otherwise>
		<tr>
		<td colspan="2" width="78"><span class="text12">偏差値ピッチ：</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="11" <c:if test="${ form.pitchBuildup == '1' || form.pitchBuildup == '11'}">checked</c:if>></td>
		<td width="56"><span class="text12">2.5</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="12" <c:if test="${ form.pitchBuildup == '2' || form.pitchBuildup == '12'}">checked</c:if>></td>
		<td width="56"><span class="text12">5.0</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchBuildup" value="13" <c:if test="${ form.pitchBuildup == '3' || form.pitchBuildup == '13'}">checked</c:if>></td>
		<td width="56"><span class="text12">10.0</span></td>
		</tr>
		</c:otherwise>
		</c:choose>
		</table>
		</td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="36" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
		<c:set value="false" var="flag"/>
		<c:forEach var="optItem" items="${form.optionItem}">
		  <c:if test="${optItem == 'GraphCompRatio'}">
	  <c:set value="true" var="flag"/>
			<input type="checkbox" name="optionItem" value="GraphCompRatio" onClick="javascript:change(this)" checked<c:if test="${ isAbilityExam }"> disabled</c:if>>
		  </c:if>
		</c:forEach>
		<c:if test="${flag == false}">
		  <input type="checkbox" name="optionItem" value="GraphCompRatio" onClick="javascript:change(this)"<c:if test="${ isAbilityExam }"> disabled</c:if>>
		</c:if>
		</td>
		<td><span class="text12">偏差値帯別構成比グラフ</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph07.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
		<td width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--		<table border="0" cellpadding="0" cellspacing="0" width="78" height="72"> --%>
		<table border="0" cellpadding="0" cellspacing="0" width="78">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
		<c:choose>
		<c:when test="${NewExam}">
		<tr>
		<td colspan="2" width="78"><span class="text12">得点ピッチ：</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="11" <c:if test="${form.pitchCompRatio == '1' || form.pitchCompRatio == '11'}">checked</c:if> ></td>
		<td width="56"><span class="text12">10</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="12" <c:if test="${form.pitchCompRatio == '2' || form.pitchCompRatio == '12'}">checked</c:if> ></td>
		<td width="56"><span class="text12">20</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="13" <c:if test="${form.pitchCompRatio == '3' || form.pitchCompRatio == '13'}">checked</c:if> ></td>
		<td width="56"><span class="text12">40</span></td>
		</tr>
		</c:when>
		<c:otherwise>
		<tr>
		<td colspan="2" width="78"><span class="text12">偏差値ピッチ：</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="11" <c:if test="${form.pitchCompRatio == '1' || form.pitchCompRatio == '11'}">checked</c:if> ></td>
		<td width="56"><span class="text12">2.5</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="12" <c:if test="${form.pitchCompRatio == '2' || form.pitchCompRatio == '12'}">checked</c:if> ></td>
		<td width="56"><span class="text12">5.0</span></td>
		</tr>
		<tr>
		<td width="22"><input type="radio" name="pitchCompRatio" value="13" <c:if test="${form.pitchCompRatio == '3' || form.pitchCompRatio == '13'}">checked</c:if> ></td>
		<td width="56"><span class="text12">10.0</span></td>
		</tr>
		</c:otherwise>
		</c:choose>
		</table>
		</td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--		<td width="187"> --%>
		<!--グラフ-->
<%--		<c:if test="${ screen != 's303' }"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" width="187"> --%>
<%--			<tr valign="top"> --%>
<%--			<td bgcolor="#8CA9BB"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="1" width="187"> --%>
<%--			<tr> --%>
<%--			<td width="185" height="36" bgcolor="#E5EEF3"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="3"> --%>
<%--			<tr> --%>
<%--			<td> --%>
<%--			<c:set value="false" var="flag"/> --%>
<%--			<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%--			  <c:if test="${optItem == 'OptionCheckBox'}"> --%>
<%--			  <c:set value="true" var="flag"/> --%>
<%--				<input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)" checked<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--			  </c:if> --%>
<%--			</c:forEach> --%>
<%--			<c:if test="${flag == false}"> --%>
<%--			  <input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--			</c:if> --%>
<%--			</td> --%>
<%--			<td><span class="text12">共通テスト英語認定試験<br>CEFR取得状況</span></td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			<tr valign="top"> --%>
<%--			<c:if test="${ !isCenterResearch and !isMarkExam }"> --%>
<%--				<td width="185" bgcolor="#e1e1e1" align="center"> --%>
<%--			</c:if> --%>
<%--			<c:if test="${ isCenterResearch or isMarkExam}"> --%>
<%--				<td width="185" bgcolor="#FFFFFF" align="center"> --%>
<%--			</c:if> --%>
<%--			<div style="margin-top:9px;"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" width="171"> --%>
<%--			<tr valign="top"> --%>
<%--			<td width="88"> --%><!--イラスト--><%--<img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="グラフ"><br></td> --%>
<%--			<td width="83"> --%>
<%--			<table border="0" cellpadding="0" cellspacing="0" height="72"> --%>
<%--			<tr valign="top"> --%>
<%--			<td> --%>
<%--			<input type="checkbox" name="targetCheckBox" style="margin:0px 2px 0px 0px" value="11" <c:if test="${form.targetCheckBox == '1' || form.targetCheckBox == '11'}">checked</c:if> > --%>
<%--			</td> --%>
<%--			<c:choose> --%>
<%--				<c:when test="${ screen == 's103' }"> --%>
<%--					<td><span class="text10">当年度自校受験者がいる試験を対象にする</span></td> --%>
<%--				</c:when> --%>
<%--				<c:when test="${ screen == 's403' }"> --%>
<%--					<td><span class="text10">当該回自校受験者がいる試験を対象にする</span></td> --%>
<%--				</c:when> --%>
<%--				<c:otherwise> --%>
<%--					<td><span class="text10">自校受験者がいる試験を対象にする</span></td> --%>
<%--				</c:otherwise> --%>
<%--			</c:choose> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</div> --%>

			<!--spacer-->
<%--			<table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%--			<tr valign="top"> --%>
<%--			<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td> --%>
<%--			</tr> --%>
<%--			</table> --%>
			<!--/spacer-->
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--			</td> --%>
<%--			</tr> --%>
<%--			</table> --%>
<%--		</c:if> --%>
		<!--/グラフ-->
<%--		</td> --%>
		<td width="187"></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
		</tr>
		</table>
		<!--/一段目-->
	   </c:if>
	</c:forTokens>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL START --%>
<%-- 	<c:forTokens var="screen" items="s204" delims=","> --%>
<%-- 	  <c:if test="${ param.forward == screen }"> --%>
<%-- 		<!--spacer--> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%-- 		<tr valign="top"> --%>
<%-- 		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		<!--/spacer--> --%>

<%-- 		<!--一段目--> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" width="573"> --%>
<%-- 		<tr valign="top"> --%>
<%-- 		<td width="280"> --%>
<%-- 		<!--グラフ--> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" width="280"> --%>
<%-- 		<tr valign="top"> --%>
<%-- 		<td bgcolor="#8CA9BB"> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="1" width="280"> --%>
<%-- 		<tr> --%>
<%-- 		<td width="278" height="26" bgcolor="#E5EEF3"> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="3"> --%>
<%-- 		<tr> --%>
<%-- 		<td> --%>
<%-- 		<c:set value="false" var="flag"/> --%>

<%-- 		</td> --%>
<%-- 		<td><span class="text12">共通テスト模試記述式問題詳細分析</span></td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		</td> --%>
<%-- 		</tr> --%>
<%-- 		<tr valign="top"> --%>
<%-- 		<c:if test="${ !isCenterResearch and !isMarkExam }"> --%>
<%-- 			<td width="278" bgcolor="#e1e1e1" align="center"> --%>
<%-- 		</c:if> --%>
<%-- 		<c:if test="${ isCenterResearch or isMarkExam }"> --%>
<%-- 			<td width="278" bgcolor="#FFFFFF" align="center"> --%>
<%-- 		</c:if> --%>

<%-- 		<div style="margin-top:9px;"> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" width="264"> --%>
<%-- 		<tr> --%>
<%-- 		<td width="93">イラスト<img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="表"><br></td> --%>
<%-- 		<td width="178"> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" height="46"> --%>
<%-- 		<tr> --%>
<%-- 		<c:set var="checked" value="" /> --%>
<%-- 		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%-- 		  <c:if test="${optItem == 'targetCheckKokugo'}"> --%>
<%-- 			<c:set var="checked" value=" checked" /> --%>
<%-- 		  </c:if> --%>
<%-- 		</c:forEach> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD START --> --%>
<%-- 		<%-- <td width="22"><input type="checkbox" name="optionItem" value="targetCheckKokugo"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if><c:out value="${checked}" />></td> --%>
<%-- 		<td width="22"><input type="checkbox" name="optionItem" value="targetCheckKokugo" disabled=disabled <c:out value="${checked}" />></td> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD END --> --%>
<%-- 		<td width="156"><span class="text12">国語 評価別人数</span></td> --%>
<%-- 		</tr> --%>
<%-- 		<tr> --%>
<%-- 		<c:set var="checked" value="" /> --%>
<%-- 		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%-- 		  <c:if test="${optItem == 'targetCheckStatus'}"> --%>
<%-- 			<c:set var="checked" value=" checked" /> --%>
<%-- 		  </c:if> --%>
<%-- 		</c:forEach> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD START --> --%>
<%-- 		<%-- <td width="22"><input type="checkbox" name="optionItem" value="targetCheckStatus"<c:if test="${ !(isCenterP or isMarkExam) or isCenterResearch }"> disabled=disabled</c:if><c:out value="${checked}" />></td> --%>
<%-- 		<td width="22"><input type="checkbox" name="optionItem" value="targetCheckStatus" disabled=disabled <c:out value="${checked}" />></td> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD END --> --%>
<%-- 		<td width="156"><span class="text12">国語 小設問別正答状況</span></td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		</td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		</div> --%>

<%-- 		<!--spacer--> --%>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%-- 		<tr valign="top"> --%>
<%-- 		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		<!--/spacer--> --%>
<%-- 		</td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		</td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		<!--/グラフ--> --%>
<%-- 		</td> --%>
<%-- 		<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td> --%>
<%-- 		<td width="280"> --%>
<%-- 		<!--グラフ--> --%>

<%-- 		<!--/グラフ--> --%>
<%-- 		</td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		<!--/一段目--> --%>
<%-- 	  </c:if> --%>
<%-- 	</c:forTokens> --%>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 DEL END --%>
	<c:forTokens var="screen" items="s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->

		<!--三段目-->
		<table border="0" cellpadding="0" cellspacing="0" width="573">
		<tr valign="top">
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="26" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
		<c:set value="false" var="flag"/>
		<c:forEach var="optionItem" items="${form.optionItem }">
		  <c:if test="${optionItem == 'CompPrev'}">
			<c:set value="true" var="flag"/>
			<input type="checkbox" name="optionItem" value="CompPrev" onClick="javascript:change(this)" checked>
		  </c:if>
		</c:forEach>
		<c:if test="${flag == false}">
		   <input type="checkbox" name="optionItem" value="CompPrev" onClick="javascript:change(this)">
		</c:if>
		</td>
		<td><span class="text12">過回・過年度比較</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph02.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
		<td width="78"></td>
		</tr>
		<tr valign="top">
		<td colspan="2" width="171"><img src="./shared_lib/img/parts/dot_blue.gif" width="171" height="1" border="0" alt="" vspace="8"><br></td>
		</tr>
		<tr valign="top">
		<td colspan="2" width="171" height="45"><span class="text12" id="InfoCompYear">
		<font color="#FF6E0E">設定項目が追加されました。<br></font>
		STEP2：<br>
		<b>「比較対象年度」</b><br>
		</span></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>

		<jsp:useBean id="common" class="java.util.LinkedList" />
		<%-- 比較対象クラス --%>
		<c:if test="${MenuSecurity.menu['605']}">
			<kn:listAdd var="common">
				<td width="187"><%@ include file="/jsp/school/smax_past_class.jsp" %><br></td>
			</kn:listAdd>
		</c:if>
		<%-- 比較対象高校 --%>
		<c:if test="${MenuSecurity.menu['606']}">
			<kn:listAdd var="common">
				<td width="187"><%@ include file="/jsp/school/smax_past_other.jsp" %><br></td>
			</kn:listAdd>
		</c:if>
		<%-- SPACER --%>
		<kn:listPad var="common" size="2">
			<td width="187"><img src="./shared_lib/img/parts/sp.gif" width="187" height="1" border="0" alt=""><br></td>
		</kn:listPad>
		<kn:listSpacer var="common">
			<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		</kn:listSpacer>
		<%-- 出力 --%>
		<kn:listOut var="common" />

		</tr>
		</table>
		<!--/三段目-->
	  </c:if>
	</c:forTokens>

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!-- /オプションの選択-->

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
  </c:if>
</c:forTokens>

<c:choose>
  <c:when test="${ param.forward == 's004'}">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<!-- オプションの選択-->
	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><b class="text14-hh">オプションの選択</b></td>
	</tr>
	</table>

	<c:forTokens var="screen" items="s004" delims=",">
	  <c:if test="${ param.forward == screen }">
		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->

		<!--二段目-->
		<table border="0" cellpadding="0" cellspacing="0" width="573">
		<tr valign="top">
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="40" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START --%>
		<td>
		<c:set var="checked" value="" />
		<c:forEach var="optItem" items="${form.optionItem}">
		  <c:if test="${optItem == 'targetCheckRecord'}">
			<c:set var="checked" value=" checked" />
		  </c:if>
		</c:forEach>
		<input type="checkbox"  name="optionItem" value="targetCheckRecord" onClick="javascript:change(this)"<c:if test="${ !isWrtnExam2 and !isPStage }"> disabled=disabled</c:if><c:out value="${checked}" />>
		</td>
		<%-- <td><span class="text12">記述式問題詳細分析</span></td> --%>
		<td><span class="text12">記述系模試小設問別成績</span></td>
		<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END --%>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD START --%>
<%-- 	<td width="185" height="105" bgcolor="#FFFFFF" align="center"> --%>
		<c:if test="${ !isWrtnExam2 and !isPStage }">
			<td width="185" height="105" bgcolor="#e1e1e1" align="center">
		</c:if>
		<c:if test="${ isWrtnExam2 or isPStage }">
			<td width="185" height="105" bgcolor="#FFFFFF" align="center">
		</c:if>
		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<%-- <td width="48" valign="top"><!--イラスト--><img src="./school/img/graph01.gif" width="42" height="32" border="0" alt="記述式問題詳細分析"><br></td> --%>
		<%-- <td width="123"> --%>
		<tr valign="top">
		<td width="93" valign="top"><!--イラスト--><img src="./school/img/graph01.gif" width="84" height="64" border="0" alt="記述式問題詳細分析"><br></td>
		<td width="78"></td>
		</tr>
		</table>
		</div>
<%-- 		<table border="0" cellpadding="0" cellspacing="0" height="72"> --%>
<%-- 		<tr> --%>
<%-- 		<td colspan="2"><span class="text12">■共通テスト模試</span></td> --%>
<%-- 		</tr> --%>
<%-- 		<tr> --%>
<%-- 		<c:set var="checked" value="" /> --%>
<%-- 		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%-- 		  <c:if test="${optItem == 'targetCheckKokugo'}"> --%>
<%-- 			<c:set var="checked" value=" checked" /> --%>
<%-- 		  </c:if> --%>
<%-- 		</c:forEach> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD START --> --%>
<%-- 		<%-- <td width="22"><input type="checkbox"  name="optionItem" value="targetCheckKokugo" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if><c:out value="${checked}" />></td> --> --%>
<%-- 		<td width="22"><input type="checkbox"  name="optionItem" value="targetCheckKokugo" onClick="javascript:change(this)" disabled=disabled <c:out value="${checked}" />></td> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD END --> --%>
<%-- 		<td width="101"><span class="text12">国語 評価別人数</span></td> --%>
<%-- 		</tr> --%>
<%-- 		<tr> --%>
<%-- 		<c:set var="checked" value="" /> --%>
<%-- 		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%-- 		  <c:if test="${optItem == 'targetCheckStatus'}"> --%>
<%-- 			<c:set var="checked" value=" checked" /> --%>
<%-- 		  </c:if> --%>
<%-- 		</c:forEach> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD START --> --%>
<%-- 		<%-- <td width="22"><input type="checkbox"  name="optionItem" value="targetCheckStatus" onClick="javascript:change(this)"<c:if test="${ !(isCenterP or isMarkExam) or isCenterResearch }"> disabled=disabled</c:if><c:out value="${checked}" />></td> --> --%>
<%-- 		<td width="22"><input type="checkbox"  name="optionItem" value="targetCheckStatus" onClick="javascript:change(this)" disabled=disabled <c:out value="${checked}" />></td> --%>
<%-- 		<%-- 2020/1/7 QQ)Ooseto 記述延期対応 UPD END --> --%>
<%-- 		<td width="101"><span class="text12">小設問別正答状況</span></td> --%>
<%-- 		</tr> --%>
<%-- 		<tr> --%>
<%-- 		<td colspan="2"><span class="text12">■記述系模試</span></td> --%>
<%-- 		</tr> --%>
<%-- 		<tr> --%>
<%-- 		<c:set var="checked" value="" /> --%>
<%-- 		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%-- 		  <c:if test="${optItem == 'targetCheckRecord'}"> --%>
<%-- 			<c:set var="checked" value=" checked" /> --%>
<%-- 		  </c:if> --%>
<%-- 		</c:forEach> --%>
<%-- 		<td width="22"><input type="checkbox"  name="optionItem" value="targetCheckRecord" onClick="javascript:change(this)"<c:if test="${ !isWrtnExam2 and !isPStage }"> disabled=disabled</c:if><c:out value="${checked}" />></td> --%>
<%-- 		<td width="101"><span class="text12">小設問別成績</span></td> --%>
<%-- 		</tr> --%>
<%-- 		</table> --%>
<%-- 		</td> --%>
<%-- 		</table> --%>
<%-- 		</div> --%>
<%-- 2020/03/30 QQ)Ooseto 2020年度 Kei-Navi共通テスト対応改修 UPD END --%>
		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>

		<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>

		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="40" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
		<c:set var="checked" value="" />
		<c:forEach var="optItem" items="${form.optionItem}">
		  <c:if test="${optItem == 'MarkExam'}">
			<c:set var="checked" value=" checked" />
		  </c:if>
		</c:forEach>
		<input type="checkbox" name="optionItem" value="MarkExam" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if><c:out value="${checked}" />>
		</td>
		<td><span class="text12">共通テスト模試正答状況</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<c:if test="${ !isCenterResearch and !isMarkExam }">
			<td width="185" height="105" bgcolor="#e1e1e1" align="center">
		</c:if>
		<c:if test="${ isCenterResearch or isMarkExam }">
			<td width="185" height="105" bgcolor="#FFFFFF" align="center">
		</c:if>

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="共通テスト模試正答状況"><br></td>
		<td width="78"></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>

		</tr>
		</table>
		<!--/グラフ-->
		</td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<c:if test="${ExamData.examYear >= 2010}">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="40" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
		<c:set var="checked" value="" />
		<c:forEach var="optItem" items="${form.optionItem}">
		  <c:if test="${optItem == 'MarkExamArea'}">
			<c:set var="checked" value=" checked" />
		  </c:if>
		</c:forEach>
		<input type="checkbox" name="optionItem" value="MarkExamArea" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if><c:out value="${checked}" />>
		</td>
		<td><span class="text12">共通テスト模試レベル別<br>設問別正答率</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<c:if test="${ !isCenterResearch and !isMarkExam }">
			<td width="185" height="105" bgcolor="#e1e1e1" align="center">
		</c:if>
		<c:if test="${ isCenterResearch or isMarkExam }">
			<td width="185" height="105" bgcolor="#FFFFFF" align="center">
		</c:if>

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="共通テスト模試レベル別設問別正答率"><br></td>
		<td width="78"></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</c:if>

		</tr>
		</table>
		<!--/二段目-->
	  </c:if>
	</c:forTokens>

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!-- /オプションの選択-->

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
  </c:when>
</c:choose>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--<c:forTokens var="screen" items="b002,b003" delims=","> --%>
<c:forTokens var="screen" items="b002" delims=",">
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
  <c:if test="${ param.forward == screen }">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<!-- オプションの選択・営業-->
	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><b class="text14-hh">オプションの選択</b></td>
	</tr>
	</table>

	<c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->

		<!--四段目-->
		<table border="0" cellpadding="0" cellspacing="0" width="573">
		<tr valign="top">
		<td width="187">
		<!--グラフ-->
		<table border="0" cellpadding="0" cellspacing="0" width="187">
		<tr valign="top">
		<td bgcolor="#8CA9BB">
		<table border="0" cellpadding="0" cellspacing="1" width="187">
		<tr>
		<td width="185" height="26" bgcolor="#E5EEF3">
		<table border="0" cellpadding="0" cellspacing="3">
		<tr>
		<td>
	<c:set value="false" var="flag"/>
		<c:forEach var="optionItem" items="${form.optionItem}">
		  <c:if test="${optionItem == 'RankOrder'}">
			<c:set value="true" var="flag"/>
			<input type="checkbox" name="optionItem" value="RankOrder" onClick="javascript:change(this)" checked>
		  </c:if>
		</c:forEach>
		<c:if test="${flag == false}">
		   <input type="checkbox" name="optionItem" value="RankOrder" onClick="javascript:change(this)">
		</c:if>
	</td>
		<td><span class="text12">型・科目別成績順位</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr valign="top">
		<td width="185" bgcolor="#FFFFFF" align="center">

		<div style="margin-top:9px;">
		<table border="0" cellpadding="0" cellspacing="0" width="171">
		<tr valign="top">
		<td width="93"><!--イラスト--><img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="グラフ"><br></td>
		<td width="78"></td>
		</tr>
		</table>
		</div>

		<!--spacer-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr valign="top">
		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
		</tr>
		</table>
		<!--/spacer-->
		</td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		<!--/グラフ-->
		</td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<td width="187"></td>
		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
		<td width="187"></td>
		</tr>
		</table>
		<!--/四段目-->
	  </c:if>
	</c:forTokens>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD START --%>
<%--	<c:forTokens var="screen" items="b003" delims=","> --%>
<%--	<c:if test="${ param.forward == screen }"> --%>
		<!--spacer-->
<%--		<table border="0" cellpadding="0" cellspacing="0" width="100%"> --%>
<%--		<tr valign="top"> --%>
<%--		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td> --%>
<%--		</tr> --%>
<%--		</table> --%>
		<!--/spacer-->
		<!--CEFR-->
<%--		<table border="0" cellpadding="0" cellspacing="0" width="573"> --%>
<%--		<tr valign="top"> --%>
<%--		<td width="187"> --%>
		<!--グラフ-->
<%--		<table border="0" cellpadding="0" cellspacing="0" width="187"> --%>
<%--		<tr valign="top"> --%>
<%--		<td bgcolor="#8CA9BB"> --%>
<%--		<table border="0" cellpadding="0" cellspacing="1" width="187"> --%>
<%--		<tr> --%>
<%--		<td width="185" height="26" bgcolor="#E5EEF3"> --%>
<%--		<table border="0" cellpadding="0" cellspacing="3"> --%>
<%--		<tr> --%>
<%--		<td> --%>
<%--		<c:set value="false" var="flag"/> --%>
<%--		<c:forEach var="optItem" items="${form.optionItem}"> --%>
<%--		<c:if test="${optItem == 'OptionCheckBox'}"> --%>
<%--		<c:set value="true" var="flag"/> --%>
<%--			<input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)" checked<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--		</c:if> --%>
<%--		</c:forEach> --%>
<%--		<c:if test="${flag == false}"> --%>
<%--			<input type="checkbox" name="optionItem" value="OptionCheckBox" onClick="javascript:change(this)"<c:if test="${ !isCenterResearch and !isMarkExam }"> disabled=disabled</c:if>> --%>
<%--		</c:if> --%>
<%--		</td> --%>
<%--		<td><span class="text12">共通テスト英語認定試験<br>CEFR取得状況</span></td> --%>
<%--		</tr> --%>
<%--		</table> --%>
<%--		</td> --%>
<%--		</tr> --%>
<%--		<tr valign="top"> --%>
<%--		<c:if test="${ !isCenterResearch and !isMarkExam }"> --%>
<%--			<td width="185" bgcolor="#e1e1e1" align="center"> --%>
<%--		</c:if> --%>
<%--		<c:if test="${ isCenterResearch or isMarkExam}"> --%>
<%--			<td width="185" bgcolor="#FFFFFF" align="center"> --%>
<%--		</c:if> --%>
<%--		<div style="margin-top:9px;"> --%>
<%--		<table border="0" cellpadding="0" cellspacing="0" width="171"> --%>
<%--		<tr valign="top"> --%>
<%--		<td width="88">--%><!--イラスト--><%--<img src="./school/img/graph01.gif" width="85" height="64" border="0" alt="グラフ"><br></td> --%>
<%--		<td width="83"> --%>
<%--		<table border="0" cellpadding="0" cellspacing="0"> --%>
<%--		<tr valign="top"> --%>
<%--		<td> --%>
<%--		<input type="checkbox" name="targetCheckBox" style="margin:0px 2px 0px 0px" value="11" <c:if test="${form.targetCheckBox == '1' || form.targetCheckBox == '11'}">checked</c:if> > --%>
<%--		</td> --%>
<%--		<td><span class="text10">受験者がいる試験を対象にする</span></td> --%>
<%--		</tr>
<%--		</table>
<%--		</td> --%>
<%--		</tr> --%>
<%--		</table> --%>
<%--		</div> --%>

		<!--spacer-->
<%--		<table border="0" cellpadding="0" cellspacing="0" width="100%">
<%--		<tr valign="top">
<%--		<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
<%--		</tr>
<%--		</table>
		<!--/spacer-->
<%--		</td>
<%--		</tr>
<%--		</table>
<%--		</td>
<%--		</tr>
<%--		</table>
		<!--/グラフ-->
<%--		</td>
<%--		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%--		<td width="187"></td>
<%--		<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%--		<td width="187"></td>
<%--		</tr>
<%--		</table>
		<!--/CEFR-->
<%--	<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<%--	</c:if>
<%--	</c:forTokens>
<%--	</td>
	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
<%-- 2019/11/20 QQ)Tanioka 英語認定試験延期対応 MOD END --%>
	</tr>
	</table>
	</div>
	<!-- /オプションの選択・営業-->

	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="16" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
  </c:if>
</c:forTokens>

<c:forTokens var="screen" items="s104,s202,s203,s204,s205,s302,s303,s304,s305,s402" delims=",">
  <c:if test="${ param.forward == screen }">
	<table border="0" cellpadding="0" cellspacing="0" width="620" id="DispItems">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<!-- 表示項目の選択-->
	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><b class="text14-hh">表示項目の選択</b></td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<!--リスト-->

	<table border="0" cellpadding="2" cellspacing="2" width="576">
	<!--set-->
	<c:forTokens var="screen" items="s303" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr id="DispPast">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td><b class="text12">過年度の表示</b></td>
			</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="1"<c:if test="${ form.dispPast == '1' || CurrentYear - ExamData.examYear == 6 }"> checked</c:if>></td>
			<td><span class="text12">対象年度のみ</span></td>
			</tr>
		<c:if test="${ CurrentYear - ExamData.examYear < 6 }">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="2"<c:if test="${ form.dispPast == '2' || ( CurrentYear - ExamData.examYear == 5 && form.dispPast == '3' ) }"> checked</c:if>></td>
			<td><span class="text12">対象年度の前年度まで</span></td>
			</tr>
		</c:if>
		<c:if test="${ CurrentYear - ExamData.examYear < 5 }">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="3"<c:if test="${ form.dispPast == '3' }"> checked</c:if>></td>
			<td><span class="text12">対象年度の前々年度まで</span></td>
			<tr>
		</c:if>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<c:forTokens var="screen" items="s205,s305" delims=",">
	  <c:if test="${ param.forward == screen && CurrentYear - ExamData.examYear < 6 && not HideUnivYear1 }">
		<c:set var="checked1" value="" />
		<c:set var="checked2" value="" />
		<c:set var="checked3" value="" />
		<c:set var="checked4" value="" />
		<c:set var="checked5" value="" />
		<c:set var="checked6" value="" />
		<c:forEach var="value" items="${form.dispUnivYear}">
			<c:if test="${ value == 1 }"><c:set var="checked1" value=" checked" /></c:if>
			<c:if test="${ value == 2 }"><c:set var="checked2" value=" checked" /></c:if>
			<c:if test="${ value == 3 }"><c:set var="checked3" value=" checked" /></c:if>
			<c:if test="${ value == 4 }"><c:set var="checked4" value=" checked" /></c:if>
			<c:if test="${ value == 5 }"><c:set var="checked5" value=" checked" /></c:if>
			<c:if test="${ value == 6 }"><c:set var="checked6" value=" checked" /></c:if>
		</c:forEach>
		<tr>
		<td width="22%" bgcolor="#E1E6EB" valign="top">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">過年度の表示<BR>（最大4つまで）</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivYear" value="1" onclick="dispUnivYearChecked(this)"<c:out value="${checked1}" />></td>
		<td><span class="text12"><c:out value="${ ExamData.examYear - 1 }" />年度</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<c:choose>
			<c:when test="${ CurrentYear - ExamData.examYear < 5 && not HideUnivYear2 }">
				<td width="22"><input type="checkbox" name="dispUnivYear" value="2" onclick="dispUnivYearChecked(this)"<c:out value="${checked2}" />></td>
				<td><span class="text12"><c:out value="${ ExamData.examYear - 2 }" />年度</span></td>
			</c:when>
			<c:otherwise>
				<td width="22"><br></td>
				<td><br></td>
			</c:otherwise>
		</c:choose>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<c:choose>
			<c:when test="${ CurrentYear - ExamData.examYear < 4 && not HideUnivYear3 }">
				<td width="22"><input type="checkbox" name="dispUnivYear" value="3" onclick="dispUnivYearChecked(this)"<c:out value="${checked3}" />></td>
				<td><span class="text12"><c:out value="${ ExamData.examYear - 3 }" />年度</span></td>
			</c:when>
			<c:otherwise>
				<td width="22"><br></td>
				<td><br></td>
			</c:otherwise>
		</c:choose>
		</tr>
		</table>
		</td>
		</tr>
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<c:choose>
			<c:when test="${ CurrentYear - ExamData.examYear < 3 && not HideUnivYear4 }">
				<td width="22"><input type="checkbox" name="dispUnivYear" value="4" onclick="dispUnivYearChecked(this)"<c:out value="${checked4}" />></td>
				<td><span class="text12"><c:out value="${ ExamData.examYear - 4 }" />年度</span></td>
			</c:when>
			<c:otherwise>
				<td width="22"><br></td>
				<td><br></td>
			</c:otherwise>
		</c:choose>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<c:choose>
			<c:when test="${ CurrentYear - ExamData.examYear < 2 && not HideUnivYear5 }">
				<td width="22"><input type="checkbox" name="dispUnivYear" value="5" onclick="dispUnivYearChecked(this)"<c:out value="${checked5}" />></td>
				<td><span class="text12"><c:out value="${ ExamData.examYear - 5 }" />年度</span></td>
			</c:when>
			<c:otherwise>
				<td width="22"><br></td>
				<td><br></td>
			</c:otherwise>
		</c:choose>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<c:choose>
			<c:when test="${ CurrentYear - ExamData.examYear < 1 && not HideUnivYear6 }">
				<td width="22"><input type="checkbox" name="dispUnivYear" value="6" onclick="dispUnivYearChecked(this)"<c:out value="${checked6}" />></td>
				<td><span class="text12"><c:out value="${ ExamData.examYear - 6 }" />年度</span></td>
			</c:when>
			<c:otherwise>
				<td width="22"><br></td>
				<td><br></td>
			</c:otherwise>
		</c:choose>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--set-->
	<c:forTokens var="screen" items="s104,s205,s305" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">大学の表示順序</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivOrder" value="1" <c:if test="${ form.dispUnivOrder == '1' }"> checked</c:if>></td>
		<td><span class="text12">選択順</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
		<td width="22"><input type="radio" name="dispUnivOrder" value="2" <c:if test="${ form.dispUnivOrder == '2' }"> checked</c:if>></td>
		<%--
		<td><span class="text12">コード順</span></td>
		  --%>
		<td><span class="text12">大学区分・名称順</span></td>
		<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="s104,s205,s305" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB" valign="top">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">大学集計区分</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>

		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivCount" value="3" onclick="dispUnivCountChecked(this)"<c:if test="${ form.dispUnivCount == '3' }"> checked</c:if>></td>
		<td><span class="text12">学部</span></td>
		</tr>
		</table>
		</td>
		<td width="67%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivCount" value="5" onclick="dispUnivCountChecked(this)"<c:if test="${ form.dispUnivCount == '5' }"> checked</c:if>></td>
		<td><span class="text12">学科</span></td>
		</tr>
		</table>
		</td>
		</tr>
		<tr></tr>
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
		</tr>
		<tr>
		<td colspan="3">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><br></td>
		<td width="22"><input type="checkbox" name="dispUnivSchedule" value="1"<c:if test="${ form.dispUnivSchedule == '1' }"> checked</c:if>></td>
		<td><span class="text12">国公立大は前期日程のみ表示</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<c:forTokens var="screen" items="s104,s205,s305" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">評価区分</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="1"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">共通テスト</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="2"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">２次・私大</span></td>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="3"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '3' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">総合</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--set-->
	<c:forTokens var="screen" items="s104,s205,s305" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">表示対象</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="1"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">現役生</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="2"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">高卒生</span></td>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="0"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '0' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">現役生＋高卒生</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="s302,s303,s304,s305,s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr id="DispSchoolOrder">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">高校の表示順序</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispSchoolOrder" value="1" <c:if test="${ form.dispSchoolOrder == '1' }"> checked</c:if>></td>
		<td><span class="text12">選択順</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispSchoolOrder" value="2" <c:if test="${ form.dispSchoolOrder == '2' }"> checked</c:if>></td>
		<td><span class="text12">コード順</span></td>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="s202,s203,s204,s205,s402" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr id="DispClassOrder">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">クラスの表示順序</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispClassOrder" value="1" <c:if test="${ form.dispClassOrder == '1' }"> checked</c:if>></td>
		<td><span class="text12">選択順</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispClassOrder" value="2" <c:if test="${ form.dispClassOrder == '2' }"> checked</c:if>></td>
		<td><span class="text12">コード順</span></td>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	</table>
	<!--/リスト-->

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!-- /表示項目の選択-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
  </c:if>
</c:forTokens>

<c:forTokens var="screen" items="b002,b003,b004,b005,b102" delims=",">
  <c:if test="${ param.forward == screen }">
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td bgcolor="#8CA9BB">
	<table border="0" cellpadding="0" cellspacing="1" width="620">
	<tr valign="top">
	<td width="618" bgcolor="#FFFFFF">

	<!-- 表示項目の選択・営業-->
	<div style="margin-top:8px;">
	<table border="0" cellpadding="0" cellspacing="0" width="618">
	<tr valign="top">
	<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td width="576" align="center">

	<table border="0" cellpadding="0" cellspacing="0" width="572">
	<tr valign="top">
	<td><b class="text14-hh">表示項目の選択</b></td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<!--リスト-->
	<table border="0" cellpadding="2" cellspacing="2" width="576">
	<!--set-->
	<c:forTokens var="screen" items="b002,b003,b005" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">過年度の表示</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="1"<c:if test="${ form.dispPast == '1' || CurrentYear - ExamData.examYear == 6 || HideUnivYear1 }"> checked</c:if>></td>
			<td><span class="text12">対象年度のみ</span></td>
			</tr>
		<c:if test="${ CurrentYear - ExamData.examYear < 6 && not HideUnivYear1 }">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="2"<c:if test="${ form.dispPast == '2' || ( CurrentYear - ExamData.examYear == 5 && form.dispPast == '3' ) || (HideUnivYear2 && form.dispPast == '3') }"> checked</c:if>></td>
			<td><span class="text12">対象年度の前年度まで</span></td>
			</tr>
		</c:if>
		<%-- b002は非表示 --%>
		<c:if test="${ screen != 'b002' }">
		<c:if test="${ CurrentYear - ExamData.examYear < 5 && not HideUnivYear2 }">
			<tr>
			<td width="22"><input type="radio" name="dispPast" value="3"<c:if test="${ form.dispPast == '3' }"> checked</c:if>></td>
			<td><span class="text12">対象年度の前々年度まで</span></td>
			<tr>
		</c:if>
		</c:if>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">全体平均表示</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispTotalAvg" value="1" <c:if test="${ form.dispTotalAvg == '1' }"> checked</c:if>></td>
		<td><span class="text12">あり</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispTotalAvg" value="0" <c:if test="${ form.dispTotalAvg == '0' }"> checked</c:if>></td>
		<td><span class="text12">なし</span></td>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">現役全体平均表示</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispActiveAvg" value="1" <c:if test="${ form.dispActiveAvg == '1' }"> checked</c:if>></td>
		<td><span class="text12">あり</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispActiveAvg" value="0" <c:if test="${ form.dispActiveAvg == '0' }"> checked</c:if>></td>
		<td><span class="text12">なし</span></td>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">高卒全体平均表示</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispGradAvg" value="1" <c:if test="${ form.dispGradAvg == '1' }"> checked</c:if>></td>
		<td><span class="text12">あり</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispGradAvg" value="0" <c:if test="${ form.dispGradAvg == '0' }"> checked</c:if>></td>
		<td><span class="text12">なし</span></td>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b005" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">大学の表示順序</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivOrder" value="1" <c:if test="${ form.dispUnivOrder == '1' }"> checked</c:if>></td>
		<td><span class="text12">選択順</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivOrder" value="2" <c:if test="${ form.dispUnivOrder == '2' }"> checked</c:if>></td>
		<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
		<%--
		<td><span class="text12">コード順</span></td>
		  --%>
		<td><span class="text12">大学区分・名称順</span></td>
		<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
		</tr>
		</table>
		</td>
		<td width="34%"></td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b005" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB" valign="top">
		<table border="0" cellpadding="0" cellspacing="2">
		<tr>
		<td><b class="text12">大学集計区分</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivCount" value="3" onclick="dispUnivCountChecked(this)"<c:if test="${ form.dispUnivCount == '3' }"> checked</c:if>></td>
		<td><span class="text12">学部</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="radio" name="dispUnivCount" value="5" onclick="dispUnivCountChecked(this)"<c:if test="${ form.dispUnivCount == '5' }"> checked</c:if>></td>
		<td><span class="text12">学科</span></td>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"></td>
		<td></td>
		</tr>
		</table>
		</td>
		</tr>

		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
		</tr>
		<tr>
		<td colspan="3">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><br></td>
		<td width="22"><input type="checkbox" name="dispUnivSchedule" value="1"<c:if test="${ form.dispUnivSchedule == '1' }"> checked</c:if>></td>
		<td><span class="text12">国公立大は前期日程のみ表示</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<c:forTokens var="screen" items="b005" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">評価区分</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="1"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">共通テスト</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="2"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">２次・私大</span></td>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivRating" value="3"<c:forEach var="item" items="${form.dispUnivRating}"><c:if test="${ item == '3' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">総合</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--set-->
	<c:forTokens var="screen" items="b002" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td><b class="text12">表示高校数</b></td>
			</tr>
			</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt="">
				<select name="dispSchoolNum" class="text12" style="width:75px;">
					<option value="1" <c:if test="${ form.dispSchoolNum == '1' }"> selected</c:if>>50校</option>
					<option value="2" <c:if test="${ form.dispSchoolNum == '2' }"> selected</c:if>>100校</option>
					<option value="3" <c:if test="${ form.dispSchoolNum == '3' }"> selected</c:if>>150校</option>
					<option value="4" <c:if test="${ form.dispSchoolNum == '4' }"> selected</c:if>>200校</option>
					<option value="5" <c:if test="${ form.dispSchoolNum == '5' }"> selected</c:if>>250校</option>
					<option value="6" <c:if test="${ form.dispSchoolNum == '6' }"> selected</c:if>>300校</option>
				</select>
			</td>
			</tr>
			<tr>
			<td style="font-size:10px;"><img src="./shared_lib/img/parts/sp.gif" width="15" height="2" border="0" alt=""><FONT COLOR="red"><B>※この項目はオプションの「型・科目別成績順位」のみに適用されます。</B></FONT></td>
			</tr>
			</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b005" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">表示対象</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="2" width="100%">
		<tr>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="1"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '1' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">現役生</span></td>
		</tr>
		</table>
		</td>
		<td width="33%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="2"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '2' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">高卒生</span></td>
		</tr>
		</table>
		</td>
		<td width="34%">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="22"><input type="checkbox" name="dispUnivStudent" value="0"<c:forEach var="item" items="${form.dispUnivStudent}"><c:if test="${ item == '0' }"> checked</c:if></c:forEach>></td>
		<td><span class="text12">現役生＋高卒生</span></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b002,b003,b004,b005,b102" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
		<td><b class="text12">高校の表示順序</b></td>
		</tr>
		</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="2" width="100%">
				<tr>
					<td width="33%">
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="22"><input type="radio" name="dispSchoolOrder" value="1" <c:if test="${ form.dispSchoolOrder == '1' }"> checked</c:if>></td>
							<td><span class="text12">選択順</span></td>
						</tr>
						</table>
					</td>
					<td width="33%">
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="22"><input type="radio" name="dispSchoolOrder" value="2" <c:if test="${ form.dispSchoolOrder == '2' }"> checked</c:if>></td>
							<td><span class="text12">コード順</span></td>
						</tr>
						</table>
					</td>
					<td width="34%" style="font-size:10px;"></td>
				</tr>
				<%-- b002のみ表示 --%>
				<c:if test="${ screen == 'b002' }">
					<tr>
					<td COLSPAN="3" style="font-size:10px;"><img src="./shared_lib/img/parts/sp.gif" width="15" height="2" border="0" alt=""><FONT COLOR="red"><B>※この項目はオプションの「型・科目別成績順位」には適用されません。</B></FONT></td>
					</tr>
				</c:if>
			</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->
	<!--set-->
	<c:forTokens var="screen" items="b002,b004" delims=",">
	  <c:if test="${ param.forward == screen }">
		<tr>
		<td width="22%" bgcolor="#E1E6EB">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
			<td><b class="text12">上位校の＊印表示</b></td>
			</tr>
			</table>
		</td>
		<td width="78%" bgcolor="#F4E5D6">
			<table border="0" cellpadding="0" cellspacing="2" width="100%">
			<tr>
				<td width="33%">
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td width="22"><input type="radio" name="dispUpperMark" value="1" <c:if test="${ form.dispUpperMark == '1' }"> checked</c:if>></td>
					<td><span class="text12">10校まで</span></td>
					</tr>
					</table>
				</td>
				<td width="33%">
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td width="22"><input type="radio" name="dispUpperMark" value="2" <c:if test="${ form.dispUpperMark == '2' }"> checked</c:if>></td>
					<td><span class="text12">5校まで</span></td>
					</tr>
					</table>
				</td>
				<td width="34%">
					<table border="0" cellpadding="0" cellspacing="0">
					<tr>
					<td width="22"><input type="radio" name="dispUpperMark" value="3" <c:if test="${ form.dispUpperMark == '3' }"> checked</c:if>></td>
					<td><span class="text12">なし</span></td>
					</tr>
					</table>
				</td>
			</tr>
			<%-- b002のみ表示 --%>
			<c:if test="${ screen == 'b002' }">
				<tr>
				<td COLSPAN="3" style="font-size:10px;"><img src="./shared_lib/img/parts/sp.gif" width="15" height="2" border="0" alt=""><FONT COLOR="red"><B>※この項目はオプションの「型・科目別成績順位」には適用されません。</B></FONT></td>
				</tr>
			</c:if>
			</table>
		</td>
		</tr>
	  </c:if>
	</c:forTokens>
	<!--/set-->

	</table>
	<!--/リスト-->

	</td>
	<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
	</tr>
	</table>
	</div>
	<!-- /表示項目の選択・営業-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->
  </c:if>
</c:forTokens>

</td>
</tr>
<!--/表示・印刷形式の選択-->


<!-- セキュリティスタンプの選択1-->
<c:if test="${!LoginSession.sales}">
  <tr valign="top">
  <td width="648" bgcolor="#EFF2F3" align="center">

  <div style="margin-top:7px;">
  <table border="0" cellpadding="0" cellspacing="0" width="620">
  <tr>
  <td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
  <td width="578"><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
  </tr>
  </table>
  </div>
  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->

  <table border="0" cellpadding="0" cellspacing="0" width="620">
  <tr valign="top">
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="0" cellspacing="1" width="620">
  <tr valign="top">
  <td width="618" bgcolor="#FFFFFF">

  <div style="margin-top:8px;">
  <table border="0" cellpadding="0" cellspacing="0" width="618">
  <tr>
  <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
  <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
  <td width="576">

  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="22"><input type="radio" name="stamp" value="1"<c:if test="${ form.stamp == '1' }"> checked</c:if>></td>
  <td width="88"><span class="text12">機密</span></td>
  <td width="22"><input type="radio" name="stamp" value="2"<c:if test="${ form.stamp == '2' }"> checked</c:if>></td>
  <td width="88"><span class="text12">部内限り</span></td>
  <td width="22"><input type="radio" name="stamp" value="3"<c:if test="${ form.stamp == '3' }"> checked</c:if>></td>
  <td width="88"><span class="text12">校内限り</span></td>
  <td width="22"><input type="radio" name="stamp" value="4"<c:if test="${ form.stamp == '4' }"> checked</c:if>></td>
  <td width="88"><span class="text12">取扱注意</span></td>
  <td width="22"><input type="radio" name="stamp" value="5"<c:if test="${ form.stamp == '5' }"> checked</c:if>></td>
  <td width="88"><span class="text12">なし</span></td>
  </tr>
  </table>

  </td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
  </div>
  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  </tr>
</c:if>
<!-- /セキュリティスタンプの選択1-->


<!-- セキュリティスタンプの選択2-->
<c:if test="${LoginSession.sales}">
  <tr valign="top">
  <td width="648" bgcolor="#EFF2F3" align="center">

  <div style="margin-top:7px;">
  <table border="0" cellpadding="0" cellspacing="0" width="620">
  <tr>
  <td width="42"><img src="./shared_lib/img/parts/icon_secu.gif" width="30" height="30" border="0" alt="セキュリティスタンプの選択" hspace="3"></td>
  <td width="578"><b class="text14" style="color:#657681;">セキュリティスタンプの選択</b></td>
  </tr>
  </table>
  </div>
  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->

  <table border="0" cellpadding="0" cellspacing="0" width="620">
  <tr valign="top">
  <td bgcolor="#8CA9BB">
  <table border="0" cellpadding="0" cellspacing="1" width="620">
  <tr valign="top">
  <td width="618" bgcolor="#FFFFFF">

  <div style="margin-top:8px;">
  <table border="0" cellpadding="0" cellspacing="0" width="618">
  <tr>
  <td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
  <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
  <td width="576">

  <table border="0" cellpadding="0" cellspacing="0">
  <tr>
  <td width="22"><input type="radio" name="stamp" value="11" <c:if test="${ form.stamp == '11' }"> checked</c:if>></td>
  <td width="88"><span class="text12">機密</span></td>
  <td width="22"><input type="radio" name="stamp" value="12" <c:if test="${ form.stamp == '12' }"> checked</c:if>></td>
  <td width="88"><span class="text12">部外秘</span></td>
  <td width="22"><input type="radio" name="stamp" value="13" <c:if test="${ form.stamp == '13' }"> checked</c:if>></td>
  <td width="88"><span class="text12">塾外秘</span></td>
  <td width="22"><input type="radio" name="stamp" value="14" <c:if test="${ form.stamp == '14' }"> checked</c:if>></td>
  <td width="88"><span class="text12">取扱注意</span></td>
  </tr>
  </table>

  </td>
  <td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
  </tr>
  </table>
  </div>
  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </table>

  <!--spacer-->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr valign="top">
  <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
  </tr>
  </table>
  <!--/spacer-->
  </td>
  </tr>
</c:if>
<!-- /セキュリティスタンプの選択2-->


</table>
</td>
</tr>
</table>
<!--/個別項目の設定-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="javascript:download_saveFlgOn()"><img src="./shared_lib/img/btn/hozon_w.gif" width="114" height="35" border="0" alt="保存"></a><br></td>
</tr>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="175">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="175"><img src="./shared_lib/img/illust/one_point.gif" width="173" height="84" border="0" alt="ワンポイントアドバイス！!"><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ワンポイントアドバイス-->
<%@ include file="/jsp/shared_lib/Onepoint.jsp" %>
<!--/ワンポイントアドバイス-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="43"><img src="./shared_lib/img/parts/sp.gif" width="43" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>

<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

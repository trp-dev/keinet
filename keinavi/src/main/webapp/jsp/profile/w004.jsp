<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル新規作成</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	
	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>
	
	<c:set var="commonCheck" value="0" />
	<%@ include file="/jsp/script/open_common.jsp" %>

	function submitNext() {
		var message = "";

		// 入力チェック
		<c:set var="cm1" value="false" /><%-- 型 --%>
		<c:set var="cm2" value="false" /><%-- 科目 --%>
		<c:set var="cm3" value="false" /><%-- 比較対象年度 --%>
		<c:set var="cm4" value="false" /><%-- 志望大学 --%>
		<c:set var="cm5" value="false" /><%-- 比較対象クラス --%>
		<c:set var="cm6" value="false" /><%-- 比較対象高校 --%>
		<c:forEach var="function" items="${form.function}">
			<c:if test="${ function == 'school' }">
				<c:set var="cm1" value="true" />
				<c:set var="cm2" value="true" />
				<c:set var="cm3" value="true" />
				<c:set var="cm4" value="true" />
				<c:set var="cm5" value="true" />
				<c:set var="cm6" value="true" />
			</c:if>
			<c:if test="${ function == 'class' }">
				<c:set var="cm1" value="true" />
				<c:set var="cm2" value="true" />
				<c:set var="cm4" value="true" />
				<c:set var="cm5" value="true" />
			</c:if>
			<c:if test="${ function == 'text' }">
				<c:set var="cm6" value="true" />
			</c:if>
		</c:forEach>

		<%-- 比較対象年度 --%>
		<c:if test="${ not MenuSecurity.menu['603'] }">
			<c:set var="cm3" value="false" />
		</c:if>
		<%-- 比較対象クラス --%>
		<c:if test="${ not MenuSecurity.menu['605'] }">
			<c:set var="cm5" value="false" />
		</c:if>
		<%-- 比較対象高校 --%>
		<c:if test="${ not MenuSecurity.menu['606'] }">
			<c:set var="cm6" value="false" />
		</c:if>

		<%-- 型 --%>
		<c:if test="${cm1}">
			if ("<kn:com item="type" />" == "設定なし") {
				message += "<kn:message id="w009a" />\n";
			}
		</c:if>
		<%-- 科目 --%>
		<c:if test="${cm2}">
			if ("<kn:com item="course" />" == "設定なし") {
				message += "<kn:message id="w010a" />\n";
			}
		</c:if>
		<%-- 比較対象年度 --%>
		<c:if test="${cm3}">
			if ("<kn:com item="year" />" == "設定なし") {
				message += "<kn:message id="w011a" />\n";
			}
		</c:if>
		<%-- 志望大学 --%>
		<c:if test="${cm4}">
			if ("<kn:com item="univ" />" == "設定なし") {
				message += "<kn:message id="w012a" />\n";
			}
		</c:if>
		<%-- 比較対象クラス --%>
		<c:if test="${cm5}">
			if ("<kn:com item="class" />" == "設定なし") {
				message += "<kn:message id="w013a" />\n";
			}
		</c:if>
		<%-- 比較対象高校 --%>
		<c:if test="${cm6}">
			if ("<kn:com item="school" />" == "設定なし") {
				message += "<kn:message id="w014a" />\n";
			}
		</c:if>

		if (message != "" && !confirm(message)) return;

		<c:choose>
			<%-- 担当クラスなし --%>
			<c:when test="${ not MenuSecurity.menu['607'] }">
				document.forms[0].forward.value = "w006";
			</c:when>
			<%-- 担当クラスあり --%>
			<c:otherwise>
				document.forms[0].forward.value = "w005";
			</c:otherwise>
		</c:choose>

		document.forms[0].submit();
	}

	function submitReload() {
		document.forms[0].forward.value = "w004";
		document.forms[0].submit();
	}

	function submitBack() {
		document.forms[0].forward.value = "w003";
		document.forms[0].submit();
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileCreate2" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w004">
<input type="hidden" name="current" value="<c:out value="${form.current}" />">
<input type="hidden" name="countingDivCode" value="<c:out value="${form.countingDivCode}" />">

<c:forEach var ="function" items="${form.function}">
  <input type="hidden" name="function" value="<c:out value="${function}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="./profile/img/ttl_profile_shinki.gif" width="239" height="25" border="0" alt="プロファイル新規作成"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="219">
<!--●●●左側●●●-->

<!--現在の集計区分-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td><img src="./profile/img/tbl_top_syukei.gif" width="219" height="11" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="217" bgcolor="#4694AF">

<%-- 集計区分 --%>
<%@ include file="/jsp/profile/countingDiv.jsp" %>

</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/現在の集計区分-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="216" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">分析機能の選択</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130"><b class="text12" style="color:#FFFFFF;">共通設定</b></td>
</tr>
</table>
<!--/step2-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->

<c:choose>
<%-- 担当クラスなし --%>
<c:when test="${ not MenuSecurity.menu['607'] }">
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step3_def.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step3-->
</c:when>
<%-- 担当クラスあり --%>
<c:otherwise>
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step3_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">担当クラスの選択</b></td>
</tr>
</table>
<!--/step3-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step4_def.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step4-->
</c:otherwise>
</c:choose>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--完了-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#98ACB7">
<td width="198" align="center"><b class="text12" style="color:#FFFFFF;">完了</b></td>
</tr>
</table>
<!--/完了-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="219" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="219" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->

</td>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="682">

<!--●●●右側●●●-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="673">

<table border="0" cellpadding="0" cellspacing="0" width="673">
<tr valign="top">
<td colspan="4" width="673" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="673" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step2.gif" width="86" height="24" border="0" alt="STEP2"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="579" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">共通設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--共通設定-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="652">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="652">
<tr valign="top">
<td width="650" bgcolor="#FFFFFF" align="center">

<!--ケイコさん-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="596">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="514">

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="56" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="500" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="470"><span class="text14">対象となる模試を選択の上、各分析で利用される共通設定を設定してください。なお、設定された内容は今回対象とした模試以外にも利用できます。<br></span>

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><b class="text12" style="color:#FF6E0E;">※</b></td>
<td><b class="text12" style="color:#FF6E0E;">下記で背景色が<img src="./profile/img/color_box_gr.gif" width="27" height="11" border="0" alt="□" align="absmiddle" hspace="4">になっている部分はあなたの分析範囲では設定の必要はありません。</b></td>
</tr>
</table>
</div>

</td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--参照年度＆参照模試-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="616" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="616" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr bgcolor="#F2BD74">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" border="0" alt=""><br></td>
<td width="610" align="center">
<!--選択-->
<table border="0" cellpadding="0" cellspacing="8">
<tr>
<td><b class="text14">参照年度：&nbsp;</b></td>
<td><select name="targetYear" class="text12" onChange="submitReload()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<td><b class="text14">参照模試：&nbsp;</b></td>
<td><select name="targetExam" class="text12" onChange="submitReload()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select>
</td>
</tr>
</table>
<!--/選択-->
</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="616" bgcolor="#F2BD74"><img src="./shared_lib/img/parts/sp.gif" width="616" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/参照年度＆参照模試-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--型の選択＆科目の選択-->
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td width="304"><%@ include file="/jsp/profile/w004_type.jsp" %></td>
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="304"><%@ include file="/jsp/profile/w004_course.jsp" %></td>
</tr>
</table>
<!--/型の選択＆科目の選択-->

<jsp:useBean id="common" class="java.util.ArrayList" />
<%-- 比較対象年度 --%>
<c:if test="${MenuSecurity.menu['603']}">
	<kn:listAdd var="common">
		<%@ include file="/jsp/profile/w004_year.jsp" %>
	</kn:listAdd>
</c:if>
<%-- 志望大学 --%>
<kn:listAdd var="common">
	<%@ include file="/jsp/profile/w004_univ.jsp" %>
</kn:listAdd>
<%-- 比較対象クラス --%>
<c:if test="${MenuSecurity.menu['605']}">
	<kn:listAdd var="common">
		<%@ include file="/jsp/profile/w004_class.jsp" %>
	</kn:listAdd>
</c:if>
<%-- 比較対象高校 --%>
<c:if test="${MenuSecurity.menu['606']}">
	<kn:listAdd var="common">
		<%@ include file="/jsp/profile/w004_school.jsp" %>
	</kn:listAdd>
</c:if>
<%-- SPACER --%>
<kn:listPad var="common" size="4">
</kn:listPad>
<%-- 出力 --%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td width="304">
	<c:out value="${common[0]}" escapeXml="false" />
</td>
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="304">
	<c:choose>
		<c:when test="${ empty common[1] }">
			<img src="./shared_lib/img/parts/sp.gif" width="304" height="1" border="0" alt=""><br>
		</c:when>
		<c:otherwise>
			<c:out value="${common[1]}" escapeXml="false" />
		</c:otherwise>
	</c:choose>
</td>
</tr>
</table>
<c:if test="${ not empty common[2] }">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
	</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" width="620">
	<tr valign="top">
	<td width="304">
		<c:out value="${common[2]}" escapeXml="false" />
	</td>
	<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
	<td width="304">
		<c:choose>
			<c:when test="${ empty common[3] }">
				<img src="./shared_lib/img/parts/sp.gif" width="304" height="1" border="0" alt=""><br>
			</c:when>
			<c:otherwise>
				<c:out value="${common[3]}" escapeXml="false" />
			</c:otherwise>
		</c:choose>
	</td>
	</tr>
	</table>
</c:if>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/共通設定-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;次のSTEPへ&nbsp;" class="text12" style="width:120px;" onClick="submitNext()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル新規作成</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	
	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>
	
	function submitNext() {
		document.forms[0].forward.value = "w008";
		document.forms[0].submit();
	}

/* 削除
	function submitBack() {
		document.forms[0].forward.value = "w006";
		document.forms[0].forward.backward = "";
		document.forms[0].submit();
	}
*/

	function init() {
		startTimer();
	}

// -->
</script>
</head>

<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileCreateComplete" />">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w007">
<input type="hidden" name="countingDivCode" value="<c:out value="${form.countingDivCode}" />">
<input type="hidden" name="current" value="<c:out value="${form.current}" />">
<c:forEach var ="function" items="${form.function}">
  <input type="hidden" name="function" value="<c:out value="${function}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="./profile/img/ttl_profile_shinki.gif" width="239" height="25" border="0" alt="プロファイル新規作成"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="219">
<!--●●●左側●●●-->

<!--現在の集計区分-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td><img src="./profile/img/tbl_top_syukei.gif" width="219" height="11" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="217" bgcolor="#4694AF">

<%-- 集計区分 --%>
<%@ include file="/jsp/profile/countingDiv.jsp" %>

</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/現在の集計区分-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="216" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">分析機能の選択</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">共通設定</b></td>
</tr>
</table>
<!--/step2-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->

<c:choose>
<%-- 担当クラスなし --%>
<c:when test="${ not MenuSecurity.menu['607'] }">
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step3_def.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step3-->
</c:when>
<%-- 担当クラスあり --%>
<c:otherwise>
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step3_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">担当クラスの選択</b></td>
</tr>
</table>
<!--/step3-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step4_def.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step4-->
</c:otherwise>
</c:choose>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--完了-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#FF8F0E">
<td width="198" align="center"><b class="text12" style="color:#FFFFFF;">完了</b></td>
</tr>
</table>
<!--/完了-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="219" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="219" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->

</td>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="682">

<!--●●●右側●●●-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="673">

<table border="0" cellpadding="0" cellspacing="0" width="673">
<tr valign="top">
<td colspan="4" width="673" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="673" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="658" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">完了</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--完了-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="652">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="652">
<tr valign="top">
<td width="650" bgcolor="#FFFFFF" align="center">

<!--ケイコさん-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="596">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="514">

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="500" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="470"><span class="text14">保存名と保存場所を確認して、「完了」ボタンを押せば新規プロファイルの作成は完了します。<br></span>
</td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--確認-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="600">
<!--プロファイル作成日-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル作成日</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><fmt:formatDate value="${Profile.createDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
<!--/プロファイル作成日-->
<!--プロファイル名-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル名</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.profileName}" /></span></td>
</tr>
<!--/プロファイル名-->
<!--コメント-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">コメント</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><kn:pre><c:out value="${Profile.comment}" /></kn:pre></span></td>
</tr>
<!--/コメント-->
<!--保存先フォルダ-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">保存先フォルダ</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.folderName}" /></span></td>
</tr>
<!--/保存先フォルダ-->
<!--作成者-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">作成者</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.userName}" /></span></td>
</tr>
<!--/作成者-->
</table>
</td>
</tr>
</table>
<!--/確認-->


<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td width="600" align="center"><b class="text16-hh">設定が保存されました。<br>「完了」ボタンを押して専用トップページへ</b></td>
</tr>
</table>
</div>

<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;完了&nbsp;" class="text12" style="width:120px;" onClick="submitNext()">

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/完了-->





<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

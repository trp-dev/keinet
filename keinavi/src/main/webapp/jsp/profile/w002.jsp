<%@page import="jp.co.fj.keinavi.beans.security.MenuSecurityBean"%>
<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.news.InformList" %>
<jsp:useBean id="InformListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />
<jsp:useBean id="KeinetListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル選択</title>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/js/Browser.js"></script>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/js/SwitchLayer.js"></script>
<script type="text/javascript" src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/js/OpenWindow.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_exit_simple.jsp" %>
    <%@ include file="/jsp/script/submit_help.jsp" %>
    <%@ include file="/jsp/script/submit_openinfo.jsp" %>

    function submitMenu(forward) {
		for (var i = 0; i < document.forms[0].elements.length; i++) {
			var e = document.forms[0].elements[i];
			if (e.type && e.type == 'button') {
				e.disabled = true;
			}
		}
		<%-- プロファイルがMAXなら別名保存できないのでアラート --%>
		if (<kn:profile item="profilecount" /> >= 100) {
			alert("<kn:message id="w031a" />");
			document.forms[0].forward.value = "w002";
			document.forms[0].tabId.value = "1";
		<%-- フォルダがMAXなら別名保存できないのでアラート --%>
		} else if (<kn:profile item="foldercount" /> >= 20) {
			alert("<kn:message id="w032a" />");
			document.forms[0].forward.value = "w002";
			document.forms[0].tabId.value = "1";
		<%-- エラーなし --%>
		} else {
			document.forms[0].forward.value = forward;
		}
        document.forms[0].operation.value = 0;
        document.forms[0].profileID.value = "";
        document.forms[0].submit();
    }

    function submitAdd() {
        <%@ include file="/jsp/script/count_profile.jsp" %>
        document.forms[0].forward.value = "w003";
        document.forms[0].operation.value = 0;
        document.forms[0].current.value = current;
        document.forms[0].submit();
    }

    function submitUserInfo() {
		openWindow("<c:url value="U101Servlet" />" + "?forward=u101&backward=w002", 680, 580);
    }

    function submitManager() {
        document.forms[0].forward.value = "u001";
        document.forms[0].submit();
    }

    function submitAdmin() {
        document.forms[0].forward.value = "m001";
        document.forms[0].submit();
    }

    function submitProfile(id) {
        // 上書き禁止モード
        if (mode[id]) {
            if (!confirm("<kn:message id="w024a" />")) {
                return;
            }
        }
        document.getElementById("MainLayer").style.visibility = "hidden";
        document.getElementById("MainLayer").style.top = -2000;
        document.getElementById("LoadingLayer").style.visibility = "visible";
        document.forms[0].forward.value = "w008";
        document.forms[0].operation.value = 0;
        document.forms[0].profileID.value = id;
        document.forms[0].submit();
    }

    function submitCountingDiv(value) {
        document.forms[0].forward.value = "w002";
        document.forms[0].changeCountingDivFlag.value = value;
        document.forms[0].operation.value = 0;
        document.forms[0].current.value = "";
        document.forms[0].scrollX.value = document.body.scrollLeft;
        document.forms[0].scrollY.value = document.body.scrollTop;

        document.forms[0].submit();
    }

    function submitFolderDelete() {
        // 読み取り専用プロファイルを含むフォルダ
        var realonly = new Array();
        <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
            <c:forEach var="profile" items="${folder.profileList}">
                <c:if test="${ profile.overwriteMode == 1 }">
                    realonly["<c:out value="${folder.folderID}" />"] = true;
                </c:if>
            </c:forEach>
        </c:forEach>

        // 上書き禁止チェック
        if (realonly[current]) {
            alert("<kn:message id="w021a" />");
            return;
        }

        // 自分が所有しているフォルダ
        var owner = new Array();
        <c:forEach var="folder" items="${ProfileFolderBean.ownFolderList}">
            owner["<c:out value="${folder.folderID}" />"] = true;
        </c:forEach>

        if (!owner[current]) {
            alert("フォルダを削除する権限がありません。");
            return;
        }

        if (confirm("<kn:message id="w001c" />")) {
            document.forms[0].forward.value = "w002";
            document.forms[0].operation.value = 1;
            document.forms[0].current.value = current;
            document.forms[0].submit();
        }
    }

    function submitCopy(id) {
        <%@ include file="/jsp/script/count_profile.jsp" %>

        document.forms[0].forward.value = "w102";
        document.forms[0].exit.value ="w002";
        document.forms[0].operation.value = 2;
        document.forms[0].copyFlag.value = "1";
        document.forms[0].current.value = current;
        document.forms[0].profileID.value = id;
        document.forms[0].submit();
    }

    function submitProfileDelete(id) {
        // 上書き禁止チェック
        if (mode[id]) {
            alert("<kn:message id="w023a" />");
            return;
        }

        // 自分が所有しているプロファイル
        var owner = new Array();

        <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
            <c:forEach var="profile" items="${folder.ownProfileList}">
                owner["<c:out value="${profile.profileID}" />"] = true;
            </c:forEach>
        </c:forEach>

        if (!owner[id]) {
            alert("プロファイルを削除する権限がありません。");
            return;
        }

        if (confirm("<kn:message id="w002c" />")) {
            document.forms[0].forward.value = "w002";
            document.forms[0].operation.value = 3;
            document.forms[0].current.value = current;
            document.forms[0].profileID.value = id;
            document.forms[0].submit();
        }
    }

    // Kei-Net欄ページ替え（１ページ固定）
    function submitKeiPage(page) {
        document.forms[0].forward.value = "w002";
        document.forms[0].keiPage.value = 1;
        document.forms[0].submit();
    }

    //模試受付システム連携用
	<%-- 2016/01/28 QQ)Nishiyama 大規模改修 DEL START --%>
	<%--
	<c:if test="${ not LoginSession.trial && not LoginSession.business && not LoginSession.deputy && not LoginSession.helpDesk }">
	  --%>
	<%-- 2016/01/28 QQ)Nishiyama 大規模改修 DEL END   --%>

	    function submitForm1() {
	    	window.open("about:blank","moshiWindow");
	    	document.forms[1].target = "moshiWindow";
	    	document.forms[1].action = "<c:out value="${moshiURL}" />";
	    	document.forms[1].sciv.value = "<c:out value="${sciv}" />";
	    	document.forms[1].scid.value = "<c:out value="${scid}" />";
	    	document.forms[1].scnm.value = "<c:out value="${scnm}" />";
			document.forms[1].submit();
	    }

	<%-- 2016/01/28 QQ)Nishiyama 大規模改修 DEL START --%>
	<%--
	</c:if>
	  --%>
	<%-- 2016/01/28 QQ)Nishiyama 大規模改修 DEL END   --%>

    // 上書き禁止モードを保持する配列
    var mode = new Array();
    <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
        <c:forEach var="profile" items="${folder.profileList}">
            mode["<c:out value="${profile.profileID}" />"] =
            <c:choose>
                <c:when test="${ profile.overwriteMode == 0 }">false;</c:when>
                <c:otherwise>true;</c:otherwise>
            </c:choose>
        </c:forEach>
    </c:forEach>

    <%-- プロファイルフォルダ操作用JavaScript --%>
    <%@ include file="/jsp/profile/profile.jsp" %>

    // フォルダ編集ウィンドウを開く
    function openFolderEditor() {
        // 自分が所有しているフォルダ
        var owner = new Array();

        <c:forEach var="folder" items="${ProfileFolderBean.ownFolderList}">
            owner["<c:out value="${folder.folderID}" />"] = true;
        </c:forEach>

        if (!owner[current]) {
            alert("フォルダを編集する権限がありません。");
            return;
        }

        var url = "<c:url value="ProfileFolderEdit" />";
        url += "?folderID=" + current;
        url += "&forward=w009";
        url += "&backward=w002";
        url += "&countingDivCode=" + getSelectedCountingDiv();
        openWindow(url,  680);
    }

    // プロファイル編集ウィンドウを開く
    function openProfileEditor(id) {
        // 上書き禁止チェック
        if (mode[id]) {
            alert("<kn:message id="w022a" />");
            return;
        }

        // 自分が所有しているプロファイル
        var owner = new Array();

        <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
            <c:forEach var="profile" items="${folder.ownProfileList}">
                owner["<c:out value="${profile.profileID}" />"] = true;
            </c:forEach>
        </c:forEach>

        if (!owner[id]) {
            alert("プロファイルを編集する権限がありません。");
            return;
        }

        var url = "<c:url value="ProfileEdit" />";
        url += "?profileID=" + id;
        url += "&forward=w010";
        url += "&backward=w002";
        url += "&current=" + current;
        url += "&countingDivCode=" + getSelectedCountingDiv();
        <%-- 2016/01/20 QQ)Hisakawa 大規模改修 UPD START --%>
        // openWindow(url,  680);
        openWindow(url,  680,  700);
        <%-- 2016/01/20 QQ)Hisakawa 大規模改修 UPD END   --%>
    }

    // 選択されている集計区分を返す
    function getSelectedCountingDiv() {
        var e = document.forms[0].countingDivCode;

        if (e == null) return null;
        else if (e.type == "hidden") return e.value;
        else return e.options[e.selectedIndex].value;
    }

    function submitClosed() {
        document.forms[0].forward.value = "w002";
        document.forms[0].current.value = current;
        document.forms[0].submit();
    }

    function init() {
    	startTimer();

		<%-- タブIDの決定 --%>
		<c:set var="tabId" value="${ form.tabId }" />

		<%-- 制限エラーありなら「選択」 --%>
		<c:if test="${ not empty ErrorMessage }">
			<c:set var="tabId" value="1" />
		</c:if>

		<%-- 初期値：営業は「選択」 --%>
		<c:if test="${ empty tabId && LoginSession.sales }">
			<c:set var="tabId" value="1" />
		</c:if>

		<%-- 初期値：営業以外は「成績分析」 --%>
		<c:if test="${ empty tabId && not LoginSession.sales }">
			<c:set var="tabId" value="3" />
		</c:if>

		<%-- 選択なのにフォルダがない：営業は「新規」 --%>
		<%-- 2015/12/14 QQ)Nishiyama 大規模改修 DEL START --%>
		<%--
		<c:if test="${ tabId eq '1' && empty ProfileFolderBean.folderList && LoginSession.sales }">
			<c:set var="tabId" value="2" />
		</c:if>
		  --%>
		<%-- 2015/12/14 QQ)Nishiyama 大規模改修 DEL END --%>

		<%-- 選択なのにフォルダがない：営業以外は「成績分析」 --%>
		<%-- 2015/12/14 QQ)Nishiyama 大規模改修 DEL START --%>
		<%--
		<c:if test="${ tabId eq '1' && empty ProfileFolderBean.folderList && not LoginSession.sales }">
			<c:set var="tabId" value="3" />
		</c:if>
		  --%>
		<%-- 2015/12/14 QQ)Nishiyama 大規模改修 DEL END --%>

        <%-- プロファイル選択タブならフォルダを変更する --%>
        <%-- 2015/12/14 QQ)Nishiyama 大規模改修 UPD START --%>
        <%--
        <c:if test="${ tabId == '1' }">
		  --%>
        <c:if test="${ tabId == '1' && not empty ProfileFolderBean.folderList}">
        <%-- 2015/12/11 QQ)Nishiyama 大規模改修 UPD END --%>

            <%-- デフォルトのカレントフォルダは一番上 --%>
            <c:set var="current" value="${ProfileFolderBean.folderList[0].folderID}" />
            <%-- フォームから渡されたカレントフォルダが存在すればそれにする --%>
            <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
                <c:if test="${folder.folderID == form.current}">
                    <c:set var="current" value="${form.current}" />
                </c:if>
            </c:forEach>
            changeFolder("<c:out value="${current}" />");
        </c:if>

        <c:if test="${ param.backward == 'w002' }">
        window.scrollTo("<c:out value="${form.scrollX}" />", "<c:out value="${form.scrollY}" />");
        </c:if>

        // エラーメッセージ
        var message = "<c:out value="${ErrorMessage}" />";
        if (message != "") alert(message);
    }

// -->
</script>
</head>
<!-- <body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return false"> -->
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="LoadingLayer" style="position:absolute;visibility:hidden;top:250px;left:450px;">
ロード中です ...
</div>
<div id="MainLayer" style="position:absolute;top:0px;left:0px;">
<form action="<c:url value="ProfileSelect" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w002">
<input type="hidden" name="exit" value="">
<input type="hidden" name="current" value="">
<input type="hidden" name="profileID" value="">
<input type="hidden" name="operation" value="">
<input type="hidden" name="actionMode" value="1">
<input type="hidden" name="copyFlag" value="">
<input type="hidden" name="tabId" value="<c:out value="${ tabId }" />">
<input type="hidden" name="scrollX" value="">
<input type="hidden" name="scrollY" value="">
<input type="hidden" name="changeCountingDivFlag" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_t.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->

<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_l.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<!--●●●左側●●●-->
<td width="168" bgcolor="#E5EEF3" align="center">


<!-- Kei-Net注目情報表示-->
<%@ include file="/jsp/profile/keinet.jsp" %>
<!--/Kei-Net注目情報表示-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<!--/●●●左側●●●-->

<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<!--●●●中央●●●-->
<td width="530">
<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/ttl_profile_select_new.gif" width="377" height="60" border="0" alt="成績分析開始 プロファイル（分析パターン）選択"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->

<!--ケイコさん-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="476">
<tr>
<td width="62"><!--イラスト--><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="414">

<table border="0" cellpadding="0" cellspacing="0" width="414">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="390" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="390" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="390" bgcolor="#F9EEE5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="390" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="414">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="64" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="64" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="400" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="400">
<tr>
<td width="15"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="370"><span class="text14">Kei-Navi にようこそ！<br>はじめに、<c:if test="${ not LoginSession.sales }">下の「成績分析開始」</c:if>「設定済みプロファイル選択」<br>「プロファイル新規作成」のいずれかを選んでください。<br>Kei-Naviでは帳票の表示・印刷の条件を「プロファイル」とい<br>う分析パターンとして保存し、呼び出すことができます。<br><c:if test="${ not LoginSession.sales }">「成績分析開始」を選ぶと、プロファイルを作成・利用しないで<br>始めることができます。</c:if></span></td>
<td width="15"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="414">
<tr valign="top">
<td width="10" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="390" bgcolor="#F9EEE5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="390" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="390" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="390" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<br>
 --%>
<%-- 営業は非表示 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ not LoginSession.sales }">
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 単数でも非表示 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:choose>
<c:when test="${ CountingDivBean.countingDivListSize == 1 }">
<input type="hidden" name="countingDivCode" value="<c:out value="${CountingDivBean.countingDivList[0].countingDivCode}" />">
</c:when>
<c:otherwise>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<!--集計区分の選択-->
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_lt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="526" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="429" height="2" border="0" alt=""><br></td>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_rt_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr bgcolor="#F2BD74">
<td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" border="0" alt=""><br></td>
<td width="520">
<!--選択-->
<table border="0" cellpadding="0" cellspacing="0" width="520">
<tr>
<td width="125"><b class="text14">集計区分の選択：</b></td>
<td width="230">
<select name="countingDivCode" class="text12" style="width:220px;" onChange="submitCountingDiv('1')">
<c:forEach var="countingDiv" items="${CountingDivBean.countingDivList}">
  <c:set var="selected" value="" />
  <c:if test="${ countingDiv.countingDivCode == form.countingDivCode }"><c:set var="selected" value=" selected" /></c:if>
  <option value="<c:out value="${countingDiv.countingDivCode}" />"<c:out value="${selected}" />><c:out value="${countingDiv.countingDivCode}" />　<c:out value="${countingDiv.countingDivName}" /></option>
</c:forEach>
</select>
</td>
<td width="180">
<table border="0" cellpadding="2" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<td><span class="text12">集計区分ごとにプロファイルを<br>作成する必要があります。</span></td>
</tr>
</table>
</td>
</tr>
</table>
<!--/選択-->
</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_lb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="526" bgcolor="#F2BD74"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="526" height="2" border="0" alt=""><br></td>
<td width="2"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_rb_ora.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/集計区分の選択-->
<br>
</c:otherwise>
</c:choose>
</c:if>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<c:if test="${ not LoginSession.sales }">
<td width="172">
	<table border="0" cellpadding="5" cellspacing="1" width="172" bgcolor="#8CA9BB">
	<tr>
	<c:choose>
	<c:when test="${ tabId == '3' }">
	<td bgcolor="#FF8F0E" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14" style="color:#FFF;">成績分析開始</b></td>
		</tr>
		</table>
	</td>
	</c:when>
	<c:otherwise>
	<td bgcolor="#EFF2F3" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14"><a href="#here" onclick="document.forms[0].tabId.value='3';submitCountingDiv();">成績分析開始</a></b></td>
		</tr>
		</table>
	</td>
	</c:otherwise>
	</c:choose>
	</tr>
	<tr>
	<td bgcolor="#FFFFFF" height="100" valign="top"><span class="text12">下のメニューを選んで開始します。表示・印刷の条件は画面のメッセージに沿って選びます。<br>選んだ条件は、Kei-Navi終了時にプロファイル（分析パターン）として保存、再利用できます。</span></td>
	</tr>
	</table>
</td>
<td width="7"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><br></td>
</c:if>
<c:if test="${ not empty ProfileFolderBean.folderList }">
<td width="172">
	<table border="0" cellpadding="5" cellspacing="1" width="172" bgcolor="#8CA9BB">
	<tr>
	<c:choose>
	<c:when test="${ tabId == '1' }">
	<td bgcolor="#FF8F0E" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14" style="color:#FFF;">設定済みプロファイル<br>（分析パターン）選択</b></td>
		</tr>
		</table>
	</td>
	</c:when>
	<c:otherwise>
	<td bgcolor="#EFF2F3" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14"><a href="#here" onclick="document.forms[0].tabId.value='1';submitCountingDiv();">設定済みプロファイル<br>（分析パターン）選択</a></b></td>
		</tr>
		</table>
	</td>
	</c:otherwise>
	</c:choose>
	</tr>
	<tr>
	<td bgcolor="#FFFFFF" height="100" valign="top"><span class="text12">設定済みのプロファイルを選んで開始します。予め登録されている「ひな型プロファイル」（よく使われる分析パターンとして河合塾がご用意したもの）も選択できます。</span></td>
	</tr>
	</table>
</td>
<td width="7"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><br></td>
</c:if>
<td width="172">
	<table border="0" cellpadding="5" cellspacing="1" width="172" bgcolor="#8CA9BB">
	<tr>
	<c:choose>
	<c:when test="${ tabId == '2' }">
	<td bgcolor="#FF8F0E" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14" style="color:#FFF;">プロファイル（分析<br>パターン）新規作成</b></td>
		</tr>
		</table>
	</td>
	</c:when>
	<c:otherwise>
	<td bgcolor="#EFF2F3" height="52">
		<table border="0" cellpadding="3" cellspacing="0">
		<tr>
		<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt=""></td>
		<td><b class="text14"><a href="#here" onclick="document.forms[0].tabId.value='2';submitCountingDiv();">プロファイル（分析<br>パターン）新規作成</a></b></td>
		</tr>
		</table>
	</td>
	</c:otherwise>
	</c:choose>
	</tr>
	<tr>
	<td bgcolor="#FFFFFF" height="100" valign="top"><span class="text12">画面のメッセージに沿って新しいプロファイルを作成します。</span></td>
	</tr>
	</table>
</td>
</tr>
</table>


<br>


<!--新しくプロファイルを作成される方-->
<c:if test="${ tabId == '2' }">
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="521">

<table border="0" cellpadding="0" cellspacing="0" width="521">
<tr valign="top">
<td colspan="4" width="521" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="507" bgcolor="#758A98"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">プロファイル（分析パターン）新規作成</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<table border="0" cellpadding="0" cellspacing="1" width="530" bgcolor="#8CA9BB">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="528">
<tr valign="top">
<td width="528" bgcolor="#EFF2F3" align="center">

<!--ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="500">
<tr valign="top">
<td width="498" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><a href="javascript:submitAdd()"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/btn_new_profile.gif" width="205" height="37" border="0" alt="プロファイル新規作成"></a></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</c:if>
<!--/新しくプロファイルを作成される方-->


<c:if test="${ tabId == '1' }">
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<%-- フォルダリストがなければ表示しない --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ ProfileFolderBean.folderListSize > 0 }">
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<!--大タイトル-->
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="521">

<table border="0" cellpadding="0" cellspacing="0" width="521">
<tr valign="top">
<td colspan="4" width="521" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="507" bgcolor="#758A98"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">設定済みプロファイル（分析パターン）選択</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--プロファイル選択-->
<table border="0" cellpadding="0" cellspacing="1" width="530" bgcolor="#8CA9BB">
<tr>
<td width="528">
<table border="0" cellpadding="0" cellspacing="0" width="528">
<tr valign="top">
<td width="528" bgcolor="#EFF2F3" align="center">

<!--フォルダリスト-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="500">
<tr valign="top">
<td width="498" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="473">
<tr valign="top">
<td width="18"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→" vspace="5"><br></td>
<td width="217" bgcolor="#EBEFF2">
<!--左リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="217">

<!--タイトル-->
<tr>
<td width="215"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/ttl_folder_list.gif" width="215" height="25" border="0" alt="フォルダリスト"><br></td>
<td width="2" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
</tr>
<!--/タイトル-->

<tr>
<td colspan="2" width="217" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
</tr>


<c:forEach var="folder" items="${ProfileFolderBean.folderList}">
  <tr>
  <td width="215" bgcolor="#EBEFF2" valign="center" id="COLOR1-<c:out value="${folder.folderID}" />">
  <table border="0" cellpadding="0" cellspacing="0" width="215">
  <tr>
  <td width="22" align="right"><!--フォルダアイコン--><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_close.gif" width="13" height="16" border="0" name="FOLDER-<c:out value="${folder.folderID}" />"></a></td>
  <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
  <td width="172"><!--フォルダ名--><span class="text12"><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><c:out value="${folder.folderName}" /></a></span></td>
  <td width="15"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" name="ARROW-<c:out value="${folder.folderID}" />"></td>
  </tr>
  </table>
  </td>
  <td width="2" bgcolor="#FFFFFF" id="COLOR2-<c:out value="${folder.folderID}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td colspan="2" width="217" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
  </tr>
</c:forEach>


</table>
<!--/左リスト-->

</td>
<td width="238" bgcolor="#FFAD8C">

<!--右リスト-->
<table border="0" cellpadding="5" cellspacing="0" width="238">
<tr>
<td width="238" bgcolor="#FFAD8C" align="center">
<!--タブ-->
<table border="0" cellpadding="0" cellspacing="0" width="217">
<tr valign="top">
<td width="101" bgcolor="#EBEFF2">
<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr>
<td width="21" align="right"><!--フォルダアイコン--><a href="javascript:openFolderEditor()" title="フォルダ編集"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_open.gif" width="16" height="16" border="0"></a></td>
<td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="20" border="0" alt=""><br></td>
<td width="75"><span class="text12"><a href="javascript:openFolderEditor()" title="フォルダ編集">フォルダ編集</a></span></td>
</tr>
</table>
</td>
<td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_tab_r.gif" width="6" height="20" border="0" alt=""><br></td>
<td width="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="3" height="20" border="0" alt=""><br></td>
<td width="101" bgcolor="#EBEFF2">
<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr>
<td width="21" align="right"><!--フォルダアイコン--><a href="javascript:submitFolderDelete()" title="フォルダ削除"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_open.gif" width="16" height="16" border="0"></a></td>
<td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="20" border="0" alt=""><br></td>
<td width="75"><span class="text12"><a href="javascript:submitFolderDelete()" title="フォルダ削除">フォルダ削除</a></span></td>
</tr>
</table>
</td>
<td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_tab_r.gif" width="6" height="20" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="5" bgcolor="#FFAD8C"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="217" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/タブ-->


<c:forEach var="folder" items="${ProfileFolderBean.folderList}">
  <div id="PL-<c:out value="${folder.folderID}" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
  <c:forEach var="profile" items="${folder.profileList}">
    <table border="0" cellpadding="0" cellspacing="0" width="219">
    <tr valign="top">
    <td width="219" bgcolor="#FFAD8C">
    <table border="0" cellpadding="0" cellspacing="1" width="219">
    <tr height="40" bgcolor="#FFFFFF">
    <td width="143">
    <table border="0" cellpadding="0" cellspacing="0" width="143">
    <tr>
    <td width="19" align="right"><!--ファイルアイコン--><a href="javascript:submitProfile('<c:out value="${profile.profileID}" />')" title="<c:out value="${profile.comment}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_file.gif" width="11" height="13" border="0"></a></td>
    <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="118"><span class="text12"><a href="javascript:submitProfile('<c:out value="${profile.profileID}" />')" title="<c:out value="${profile.comment}" />"><c:out value="${profile.profileName}" /><c:if test="${ MenuSecurity.menu['801'] }"><br>（<c:out value="${profile.userName}" />）</c:if></a></span></td>
    </tr>
    </table>
    </td>
    <td width="36" align="center"><span class="text12">
    <a href="javascript:openProfileEditor('<c:out value="${profile.profileID}" />')">編集</a><br>
    <a href="javascript:submitCopy('<c:out value="${profile.profileID}" />')">複製</a><br>
    </span></td>
    <td width="36" align="center"><span class="text12"><a href="javascript:submitProfileDelete('<c:out value="${profile.profileID}" />')">削除</a><br></span></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
  </c:forEach>
  </div>
</c:forEach>


</td>
</tr>
</table>
<!--/右リスト-->
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/フォルダリスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/プロファイル選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</c:if>
</c:if>


<c:if test="${ tabId == '3' }">
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="521">

<table border="0" cellpadding="0" cellspacing="0" width="521">
<tr valign="top">
<td colspan="4" width="521" bgcolor="#8CA9BB"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="507" bgcolor="#758A98"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">成績分析開始</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<table border="0" cellpadding="0" cellspacing="1" width="530" bgcolor="#8CA9BB">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="528">
<tr valign="top">
<td width="528" bgcolor="#EFF2F3" align="center">

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 機能リスト --%>
<%-- タブ --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<jsp:useBean id="function" class="java.util.LinkedList" />
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 校内成績分析 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${MenuSecurity.menu['100']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_school.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 高校成績分析 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${MenuSecurity.menu['200']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_business.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- クラス成績分析 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${MenuSecurity.menu['300']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_class.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 個人成績分析 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${MenuSecurity.menu['400']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_individual.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- テキスト出力 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${MenuSecurity.menu['500']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_text.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- SPACER --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<kn:listPad var="function" size="4">
	<td width="125"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="125" height="1" border="0" alt=""><br></td>
</kn:listPad>
<kn:listSpacer var="function">
	<td width="4"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><br></td>
</kn:listSpacer>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>
<%-- 出力 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<kn:listOut var="function" />
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</c:if>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<!--/●●●中央●●●-->

<td width="30"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>

<!--●●●右側●●●-->
<td width="180">
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<%-- 利用者情報確認 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ MenuSecurity.menu['801'] || LoginSession.trial }">
<div style="margin-top:12px;">
<input type="button" value="利用者情報確認" class="text12" style="width:180px" onClick="submitUserInfo()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
</div>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<%-- 利用者管理 --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ (LoginSession.manager && MenuSecurity.menu['800']) || LoginSession.trial }">
<div style="margin-top:12px;">
<input type="button" value="利用者管理" class="text12" style="width:180px" onClick="submitManager()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
</div>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<%-- メンテナンス --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ (LoginSession.maintainer && MenuSecurity.menu['700']) || LoginSession.trial }">
<div style="margin-top:12px;">
<input type="button" value="メンテナンス" class="text12" style="width:180px" onClick="submitAdmin()"<c:if test="${ LoginSession.trial }"> disabled=disabled</c:if>>
</div>
</c:if>
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<!--各種ダウンロードメニュー(体験版は非表示)-->
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ not LoginSession.trial }">
<%@ include file="/jsp/freemenu/f000.jsp" %>
</c:if>
<!--/各種ダウンロードメニュー-->
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>

<%-- 模試受付システム --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ not LoginSession.trial && not LoginSession.business && not LoginSession.deputy && not LoginSession.helpDesk }">
<div style="margin-top:23px;">
<a href="javascript:submitForm1()"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/moushikomi.gif" width="179px" border="0" alt="模試受付システム"></a>
</div>
<br>
</c:if>

<!--お役立ちリンク-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/right_sub_ttl_link.gif" width="180" height="31" border="0" alt="お役立ちリンク"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="180">
<tr valign="top">
<td width="178" bgcolor="#FAFAFA">

<!--リンク-->
<%@ include file="/jsp/shared_lib/usefulLink.jsp" %>
<!--/リンク-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/お役立ちリンクー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<!--/●●●右側●●●-->

<td width="29"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="29" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->
  --%>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 DEL END --%>


<%-- 2015/12/11 QQ)Nishiyama 大規模改修 ADD START --%>
<!--コンテンツ-->
<img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br>
<table border="0" cellpadding="0" cellspacing="0" width="968">
  <tr valign="top">
    <td width="20"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
    <td width="62" valign="bottom"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
    <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>

    <!--↓お知らせ-->
    <td bgcolor="#FFFFFF" width="544">
      <%@ include file="/jsp/profile/notice.jsp" %>
    </td>
    <!--↑お知らせ-->

    <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>

    <!--↓システム設定-->
    <td bgcolor="#FFFFFF" width="308">
      <table border="0" cellpadding="2" cellspacing="0" width="100%">
        <!--↓タイトル-->
        <tr height="26" bgcolor="#666666"">
          <td align="left" valign="top">
            <div style="margin-top:5px; margin-right:5px; margin-left:5px;">
              <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/title_head_img/headImg_gray.gif" width="16" height="16" border="0" alt="" align="left">
            </div>
            <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt="">
            <b class="text16"><font color="#FFFFFF">システム設定</font></b>
          </td>
        </tr>
        <!--↑タイトル-->
        <!--↓ボタン-->
        <tr height="100" valign="top">
          <td width="100%" bgcolor="#EFF2F3">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr valign="middle">
                <td>
                  <c:if test="${ MenuSecurity.menu['801'] || LoginSession.trial }">
                    <div style="margin-top:6px;">
                      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                      <input type="button" value="利用者情報確認" class="text10" style="width:110px" onClick="submitUserInfo()">
                    </div>
                  </c:if>
                </td>
                <td>
                  <c:if test="${ MenuSecurity.menu['801'] || LoginSession.trial }">
                    <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                    <span class="text10">パスワード変更／利用権限の確認</span>
                  </c:if>
                </td>
              </tr>
              <tr valign="middle">
                <td>
                  <c:if test="${ (LoginSession.manager && MenuSecurity.menu['800']) || LoginSession.trial }">
                    <div style="margin-top:6px;">
                      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                      <input type="button" value="利用者管理" class="text10" style="width:110px" onClick="submitManager()">
                    </div>
                  </c:if>
                </td>
                <td>
                  <c:if test="${ (LoginSession.manager && MenuSecurity.menu['800']) || LoginSession.trial }">
                    <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                    <span class="text10">利用者ＩＤ追加／利用権限の設定<br>　（システム管理者のみ）</span>
                  </c:if>
                </td>
              </tr>
              <tr valign="middle">
                <td>
                  <c:if test="${ (LoginSession.maintainer && MenuSecurity.menu['700']) || LoginSession.trial }">
                    <div style="margin-top:6px;">
                      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                      <input type="button" value="メンテナンス" class="text10" style="width:110px" onClick="submitAdmin()">
                    </div>
                  </c:if>
                </td>
                <td>
                  <c:if test="${ (LoginSession.maintainer && MenuSecurity.menu['700']) || LoginSession.trial }">
                    <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif">
                    <span class="text10">生徒情報登録・修正／複合クラス作成</span>
                  </c:if>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!--↑ボタン-->
      </table>
    </td>
    <!--↑システム設定-->

    <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>

  </tr>
</table>
<!--/コンテンツ-->

<img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>

    <!--↓ダウンロードサービス-->
    <td valign="top" bgcolor="#FFFFFF" width="200">
      <%@ include file="/jsp/freemenu/f000.jsp" %>
    </td>
    <!--↑ダウンロードサービス-->

    <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" border="0" alt=""></td>

    <td valign="top" width="500">

      <!--↓集計区分の選択-->
      <%-- 営業は非表示 --%>
      <c:if test="${ not LoginSession.sales }">
        <%-- 単数は非表示 --%>
        <c:choose>
          <c:when test="${ CountingDivBean.countingDivListSize == 1 }">
            <input type="hidden" name="countingDivCode" value="<c:out value="${CountingDivBean.countingDivList[0].countingDivCode}" />">
          </c:when>
        <c:otherwise>
          <table width="500" border="0" cellpadding="0" cellspacing="0">
            <tr height="34" bgcolor="#FFB074">
              <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" border="0" alt=""><br></td>
              <td>
                <!--↓選択-->
                <b class="text14">集計区分の選択：</b>
                <select name="countingDivCode" class="text12" style="width:220px;" onChange="submitCountingDiv('1')">
                  <c:forEach var="countingDiv" items="${CountingDivBean.countingDivList}">
                    <c:set var="selected" value="" />
                    <c:if test="${ countingDiv.countingDivCode == form.countingDivCode }"><c:set var="selected" value=" selected" /></c:if>
                    <option value="<c:out value="${countingDiv.countingDivCode}" />"<c:out value="${selected}" />><c:out value="${countingDiv.countingDivCode}" />　<c:out value="${countingDiv.countingDivName}" /></option>
                  </c:forEach>
                </select>
                <!--↑選択-->
              </td>
            </tr>
            <tr height="6"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""></td></tr>
          </table>
          </c:otherwise>
        </c:choose>
      </c:if>
      <!--↑集計区分の選択-->

      <!--↓成績分析開始-->
      <table width="500" border="0" cellpadding="0" cellspacing="0">
        <!--↓成績分析開始：タイトル-->
        <tr height="26" bgcolor="#666666">
          <td align="left" valign="top">
            <div style="margin-top:5px; margin-right:5px; margin-left:5px;">
              <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/title_head_img/headImg_orange.gif" width="16" height="16" border="0" alt="" align="left">
            </div>
            <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt="">
            <c:choose>
              <%-- 成績分析 --%>
              <c:when test="${ tabId == '3' }" >
                <b class="text16"><font color="#FFFFFF">成績分析開始</font></b>
              </c:when>
              <%-- プロファイル選択 --%>
              <c:otherwise>
                <b class="text16"><font color="#FFFFFF">設定済みプロファイル（分析パターン）選択</font></b>
              </c:otherwise>
            </c:choose>
          </td>
        </tr>
        <!--↑成績分析開始：タイトル-->
        <!--↓成績分析開始：ボタン-->
        <tr>
          <td>
            <!--↓成績分析開始：枠-->
            <table width="500" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
              <c:choose>
                <%-- ↓プロファイル選択 --%>
                <c:when test="${ tabId == '1' }" >
                  <tr valign="top" height="450" bgcolor="#EFF2F3">
                    <td width="500">
                      <table border="0" cellpadding="0" cellspacing="0">
                        <tr height="10"><td colspan="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>
                        <tr>
                          <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                          <td>
                            <!--↓枠線（中枠）-->
                            <table border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
                              <tr valign="top" height="430" bgcolor="#FFFFFF">
                                <td>
                                  <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <!--↓左・右リスト-->
                                      <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="473">
                                          <tr height="10"><td colspan="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>
                                          <tr>
                                            <!--↓ボタン-->
                                            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                                            <td width="18"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                                            <td width="190" align="left">
                                              <c:if test="${ not LoginSession.sales }">
                                                <input type="button" value="＜前のページに戻る" onclick="document.forms[0].tabId.value='3';submitCountingDiv();" class="text12" style="width:140px">
                                              </c:if>
                                            </td>
                                            <td width="238" align="right">
                                              <a href="javascript:onclick=submitAdd();">
                                                <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_sinkiSakusei.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_sinkiSakusei_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_sinkiSakusei.gif'" alt="新規作成" height="30" width="100" border="0">
                                              </a>
                                            </td>
                                            <!--↑ボタン-->
                                            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                                          </tr>

                                          <tr height="10"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>

                                          <c:if test="${ not empty ProfileFolderBean.folderList}">
                                            <tr valign="top">
                                              <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                                              <td width="18" align="right"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→" vspace="5"><br></td>
                                              <!--↓左リスト-->
                                              <td width="190" bgcolor="#EBEFF2">
                                                <table border="0" cellpadding="0" cellspacing="0" width="190">
                                                  <!--↓タイトル-->
                                                  <tr>
                                                    <td width="190"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/ttl_folder_list.gif" width="190" height="25" border="0" alt="フォルダリスト"><br></td>
                                                    <td width="2" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
                                                  </tr>
                                                  <!--↑タイトル-->
                                                  <!--↓フォルダ-->
                                                  <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
                                                    <tr>
                                                      <td colspan="2" width="190" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="190" height="2" border="0" alt=""><br></td>
                                                    </tr>
                                                    <tr>
                                                      <td width="190" bgcolor="#EBEFF2" valign="center" id="COLOR1-<c:out value="${folder.folderID}" />">
                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                          <tr>
                                                            <td width="22" align="right"><!--フォルダアイコン--><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_close.gif" width="13" height="16" border="0" name="FOLDER-<c:out value="${folder.folderID}" />"></a></td>
                                                            <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
                                                            <td width="147"><!--フォルダ名--><span class="text12"><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><c:out value="${folder.folderName}" /></a></span></td>
                                                            <td width="15"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" name="ARROW-<c:out value="${folder.folderID}" />"></td>
                                                          </tr>
                                                        </table>
                                                      </td>
                                                      <td width="2" bgcolor="#FFFFFF" id="COLOR2-<c:out value="${folder.folderID}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
                                                    </tr>
                                                    <tr>
                                                      <td colspan="2" width="190" bgcolor="#FFFFFF"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="190" height="2" border="0" alt=""><br></td>
                                                    </tr>
                                                  </c:forEach>
                                                  <!--↑フォルダ-->
                                                </table>
                                              </td>
                                              <!--↑左リスト-->

                                              <!--↓右リスト-->
                                              <td width="238" bgcolor="#FFAD8C">
                                                <table border="0" cellpadding="5" cellspacing="0" width="238">
                                                  <tr>
                                                    <td width="238" bgcolor="#FFAD8C" align="center">
                                                      <!--↓タブ-->
                                                      <table border="0" cellpadding="0" cellspacing="0" width="217">
                                                        <c:if test="${ not empty ProfileFolderBean.folderList}">
                                                          <tr valign="top">
                                                            <td width="101" bgcolor="#EBEFF2">
                                                              <table border="0" cellpadding="0" cellspacing="0" width="101">
                                                                <tr>
                                                                  <td width="21" align="right"><!--フォルダアイコン--><a href="javascript:openFolderEditor()" title="フォルダ編集"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_open.gif" width="16" height="16" border="0"></a></td>
                                                                  <td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="20" border="0" alt=""><br></td>
                                                                  <td width="75"><span class="text12"><a href="javascript:openFolderEditor()" title="フォルダ編集">フォルダ編集</a></span></td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                            <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_tab_r.gif" width="6" height="20" border="0" alt=""><br></td>
                                                            <td width="3"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="3" height="20" border="0" alt=""><br></td>
                                                            <td width="101" bgcolor="#EBEFF2">
                                                              <table border="0" cellpadding="0" cellspacing="0" width="101">
                                                                <tr>
                                                                  <td width="21" align="right"><!--フォルダアイコン--><a href="javascript:submitFolderDelete()" title="フォルダ削除"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_open.gif" width="16" height="16" border="0"></a></td>
                                                                  <td width="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="5" height="20" border="0" alt=""><br></td>
                                                                  <td width="75"><span class="text12"><a href="javascript:submitFolderDelete()" title="フォルダ削除">フォルダ削除</a></span></td>
                                                                </tr>
                                                              </table>
                                                            </td>
                                                            <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/tbl_tab_r.gif" width="6" height="20" border="0" alt=""><br></td>
                                                          </tr>
                                                          <tr>
                                                            <td colspan="5" bgcolor="#FFAD8C"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="217" height="1" border="0" alt=""><br></td>
                                                          </tr>
                                                        </c:if>
                                                      </table>
                                                      <!--↑タブ-->
                                                      <!--↓プロファイル-->
                                                      <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
                                                        <div id="PL-<c:out value="${folder.folderID}" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
                                                        <c:forEach var="profile" items="${folder.profileList}">
                                                          <table border="0" cellpadding="0" cellspacing="0" width="219">
                                                            <tr valign="top">
                                                              <td width="219" bgcolor="#FFAD8C">
                                                                <table border="0" cellpadding="0" cellspacing="1" width="219">
                                                                  <tr height="40" bgcolor="#FFFFFF">
                                                                    <td width="143">
                                                                      <table border="0" cellpadding="0" cellspacing="0" width="143">
                                                                        <tr>
                                                                          <td width="19" align="right"><!--ファイルアイコン--><a href="javascript:submitProfile('<c:out value="${profile.profileID}" />')" title="<c:out value="${profile.comment}" />"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_file.gif" width="11" height="13" border="0"></a></td>
                                                                          <td width="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
                                                                          <td width="118"><span class="text12"><a href="javascript:submitProfile('<c:out value="${profile.profileID}" />')" title="<c:out value="${profile.comment}" />"><c:out value="${profile.profileName}" /><c:if test="${ MenuSecurity.menu['801'] }"><br>（<c:out value="${profile.userName}" />）</c:if></a></span></td>
                                                                        </tr>
                                                                      </table>
                                                                    </td>
                                                                    <td width="36" align="center">
                                                                      <span class="text12">
                                                                        <a href="javascript:openProfileEditor('<c:out value="${profile.profileID}" />')">編集</a><br>
                                                                        <a href="javascript:submitCopy('<c:out value="${profile.profileID}" />')">複製</a><br>
                                                                      </span>
                                                                    </td>
                                                                    <td width="36" align="center">
                                                                      <span class="text12">
                                                                        <a href="javascript:submitProfileDelete('<c:out value="${profile.profileID}" />')">削除</a><br>
                                                                      </span>
                                                                    </td>
                                                                  </tr>
                                                                </table>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </c:forEach>
                                                        </div>
                                                      </c:forEach>
                                                      <!--↑プロファイル-->
                                                    </td>
                                                  </tr>
                                                </table>
                                              </td>
                                              <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                                              <!--↑右リスト-->
                                            </tr>
                                          </c:if>

                                        </table>
                                      </td>
                                      <!--↑左右リスト-->

                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                            <!--↑枠線（中枠）-->
                          </td>
                          <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                        </tr>
                        <tr height="10"><td colspan="5"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>
                      </table>
                    </td>
                  </tr>
                </c:when>
                <%-- ↓成績分析開始 --%>
                <c:otherwise>
                  <tr valign="top" height=<c:choose><c:when test="${ not LoginSession.sales }">"260"</c:when><c:otherwise>"130"</c:otherwise></c:choose> bgcolor="#FFFFFF">
                    <td>
                      <!--↓成績分析開始：ボタン-->
                      <table border="0" cellpadding="0" cellspacing="0">
                        <%-- 営業は非表示 --%>
                        <c:if test="${ not LoginSession.sales }">
                          <tr height="10"><td colspan="6"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>

                          <tr height="112">
                            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                            <!--↓校内成績-->
                            <td width="235">
                              <c:if test="${MenuSecurity.menu['100']}">
                                <a href="javascript:submitMenu('s001')">
                                  <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kounai.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kounai_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kounai.gif'" alt="校内成績" height="112" width="233" border="0">
                                </a>
                              </c:if>
                            </td>
                            <!--↑校内成績-->
                            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                            <!--↓クラス成績-->
                            <td width="235">
                              <c:if test="${MenuSecurity.menu['300']}">
                                <a href="javascript:submitMenu('c001')">
                                  <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_class.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_class_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_class.gif'" alt="クラス成績" height="112" width="233" border="0">
                                </a>
                              </c:if>
                            </td>
                            <!--↑クラス成績-->
                            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                          </tr>
                        </c:if>

                        <tr height="10"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>

                        <tr height="112">
                          <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                          <!--↓個人成績-->
                          <td width="235">
                            <%-- 個人成績分析 --%>
                            <c:if test="${MenuSecurity.menu['400']}">
                              <a href="javascript:submitMenu('i001')">
                                <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kojin.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kojin_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_kojin.gif'" alt="個人成績" height="112" width="233" border="0">
                              </a>
                            </c:if>
                          </td>
                          <!--↑個人成績-->

                          <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>

                          <!--↓答案閲覧-->
                          <td width="235">
                            <%-- K-Webへのリンクを持つ権限を判定する --%>
                            <kn:hasKwebLink>
                              <a href="javascript:submitExLink('k-web');">
                                <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_touan.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_touan_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/seiseki_bunseki/btn_touan.gif'" alt="答案閲覧" height="112" width="233" border="0">
                              </a>
                            </kn:hasKwebLink>
                          </td>
                          <!--↑答案閲覧-->
                          <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                        </tr>

                        <tr height="10"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>

                      </table>
                      <!--↑成績分析開始：ボタン-->
                    </td>
                  </tr>
                </c:otherwise>
              </c:choose>
            </table>
            <!--↑成績分析開始：枠-->
          </td>
        </tr>
        <!--↑成績分析開始：ボタン-->

      </table>
      <!--↑成績分析開始-->

      <%-- 成績分析開始のみ表示 --%>
      <c:if test="${ tabId == '3' }">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr height="9"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""></td></tr>
        </table>

        <!--↓分析・データ出力-->
        <table border="0" cellpadding="0" cellspacing="0" width="500">
          <tr>
            <td>
              <table border="0" cellpadding="0" cellspacing="0" width="245">
                <!--↓分析プロファイル：タイトル-->
                <tr height="26" bgcolor="#666666">
                  <td align="left" valign="top">
                    <div style="margin-top:5px; margin-right:5px; margin-left:5px;">
                      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/title_head_img/headImg_orange.gif" width="16" height="16" border="0" alt="" align="left">
                    </div>
                    <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt="">
                    <b class="text16"><font color="#FFFFFF">分析プロファイルを使う</font></b>
                  </td>
                </tr>
                <!--↑分析プロファイル：タイトル-->
              </table>
            </td>

            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>

            <td>
              <table border="0" cellpadding="0" cellspacing="0" width="245">
                <!--↓データ出力：タイトル-->
                <tr height="26" bgcolor="#666666">
                  <td align="left" valign="top">
                    <div style="margin-top:5px; margin-right:5px; margin-left:5px;">
                      <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/title_head_img/headImg_orange.gif" width="16" height="16" border="0" alt="" align="left">
                    </div>
                    <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt="">
                    <b class="text16"><font color="#FFFFFF">データ出力</font></b>
                  </td>
                </tr>
                <!--↑データ出力：タイトル-->
              </table>
            </td>
          </tr>

          <tr>
            <td>
              <!--↓分析プロファイル：枠-->
              <table width="245" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
                <tr valign="top" height="153" bgcolor="#FFFFFF">
                  <td>
                    <!--↓分析プロファイル：ボタン-->
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr height="10"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>
                      <tr valign="top">
                        <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                        <td>
                          <a href="javascript:onclick=document.forms[0].tabId.value='1';submitCountingDiv();">
                            <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_profile.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_profile_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/bunseki_profile/btn_profile.gif'" alt="分析プロファイル" height="112" width="215" border="0">
                          </a>
                        </td>
                      </tr>

                    </table>
                    <!--↑分析プロファイル：ボタン-->
                  </td>
                </tr>
              </table>
              <!--↑分析プロファイル：枠-->
            </td>

            <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>

            <td>
              <!--↓データ出力：枠-->
              <table width="245" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
                <tr valign="top" height="153" bgcolor="#FFFFFF">
                  <td>
                    <!--↓データ出力：ボタン-->
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr height="10"><td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td></tr>
                      <tr valign="top">
                        <td width="10"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif"></td>
                        <td>
                          <c:if test="${MenuSecurity.menu['500']}">
                            <a href="javascript:submitMenu('t001')">
                              <img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/data_syuturyoku/btn_text.gif" onmouseover="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/data_syuturyoku/btn_text_on.gif'" onmouseout="this.src='https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/profile/img/data_syuturyoku/btn_text.gif'" alt="データ出力" height="112" width="215" border="0">
                            </a>
                          </c:if>
                        </td>
                      </tr>
                    </table>
                    <!--↑データ出力：ボタン-->
                  </td>
                </tr>
              </table>
              <!--↑データ出力：枠-->
            </td>
          </tr>


        </table>
        <!--↑分析・データ出力-->
      </c:if>

    </td>

    <td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="10" border="0" alt=""></td>

    <td valign="top">
      <!--↓入試情報-->
      <table border="0" cellpadding="0" cellspacing="0" width="230">
        <tr>
          <td>
            <!--枠線-->
            <table border="0" cellpadding="0" cellspacing="1" bgcolor="#666666" width="230">
              <tr height="520" valign="top" bgcolor="#FFFFFF">
                <td>
                  <%@ include file="/jsp/profile/keinet.jsp" %>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--↑入試情報-->
    </td>
  </tr>
</table>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 ADD END --%>


</td>
<td width="9"  background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_r.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="177"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_lb_b.gif" width="177" height="15" border="0" alt=""><br></td>
<td width="795" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_d.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="795" height="15" border="0" alt=""><br></td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
  --%>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_bk_d.gif"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
<%-- 2015/12/11 QQ)Nishiyama 大規模改修 UPD END --%>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--先頭へ戻る-->
<%@ include file="/jsp/shared_lib/page_top.jsp" %>
<!--/先頭へ戻る-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer01.jsp" %>
<!--/FOOTER-->
</form>
<form action="" method="POST">
<input type="hidden" name="sciv" value="">
<input type="hidden" name="scid" value="">
<input type="hidden" name="scnm" value="">
</form>
</div>
</body>
</html>

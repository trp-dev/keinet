<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%-- クラス・個人のみ処理を切り替えるフラグ（入力チェックと説明文） --%>
<c:set var="check" value="0" />
<c:forEach var="function" items="${form.function}">
	<c:if test="${ function == 'individual' }"><c:set var="check" value="1" /></c:if>
	<c:if test="${ function == 'class' }"><c:set var="check" value="1" /></c:if>
	<c:if test="${ function == 'text' }"><c:set var="check" value="1" /></c:if>
</c:forEach>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル新規作成</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>
	
	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>
	
	function submitNext() {
		// 入力チェック
		<c:if test="${ check == 1 }">
			var message = "";

			var count = util.countChecked(document.forms[0].classes);

			if (count == 0) {
				alert("<kn:message id="w015a" />");
				return;
			} else if (count > 40) {
				alert("<kn:message id="w016a" />");
				return;
			}
		</c:if>

		document.forms[0].forward.value = "w006";
		document.forms[0].submit();
	}

	function submitBack() {
		document.forms[0].forward.value = "w004";
		document.forms[0].submit();
	}


	<%-- レイヤ操作クラス --%>
	var sw = new SwitchLayer();
	<%-- フォームユーティリティ --%>
	var util = new FormUtil();

	<%-- 年度別の学年リストとテーブルIDリスト --%>
	var gradeList = new Array();
	var tableList = new Array();
	<c:forEach var="year" items="${ ClassBean.yearList }">
		gradeList["<c:out value="${year}" />"] = new Array(
		<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }" varStatus="status">
			"<c:out value="${grade.grade}" />"<c:if test="${ not status.last }">,</c:if>
		</c:forEach>
		);
		<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }" varStatus="status">
			tableList[tableList.length] = "Table-<c:out value="${year}" />-<c:out value="${grade.grade}" />";
		</c:forEach>
	</c:forEach>

	function changeYear() {
		if (confirm("<kn:message id="w025a" />")) {
			initGrade();

			var e = document.forms[0].classes;
			if (e.length) {
				for (var i=0; i<e.length; i++) {
					e[i].checked = false;
				}
			} else {
				e.checked = false;
			}

		} else {
			var options = document.forms[0].year.options;
			if (options.length) {
				for (var i=0; i<options.length; i++) {
					if (options[i].value == "<c:out value="${form.year}" />") {
						options[i].selected = true;
						return;
					}
				}
			}
		}
	}

	<%-- 表示するテーブルを切り替える --%>
	function selectTable() {

		var current = "Table-"
				+ document.forms[0].year.value
				+ "-" + document.forms[0].grade.value;

		for (var i=0; i<tableList.length; i++) {
			if (current == tableList[i]) {
				sw.showLayer(tableList[i]);
			} else {
				sw.hideLayer(tableList[i]);
			}
		}

		<%-- 全選択のチェックは外す --%>
		document.forms[0].ctrl.checked = false;
	}

	<%-- 学年のセレクトボックスを初期化する --%>
	function initGrade() {

		var options = document.forms[0].grade.options;
		options.length = 0;
		var e = gradeList[document.forms[0].year.value];
		for (var i=0; i<e.length; i++) {
			options[i] = new Option(e[i] + "年", e[i]);
		}

		selectTable();
	}

	function selectAll(checked) {
		var target = document.forms[0].year.value + "," + document.forms[0].grade.value;
		var e = document.forms[0].classes;
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				if (target == e[i].value.substring(0, target.length)) {
					e[i].checked = checked;
				}
			}
		} else {
			if (target == e.value.substring(0, target.length)) {
				e.checked = checked;
			}
		}
	}

	<%-- ページ初期化処理 --%>
	function init() {
		startTimer();
		initGrade();
	}

// -->
</script>
</head>

<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileCreate3" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w005">
<input type="hidden" name="targetYear" value="<c:out value="${form.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${form.targetExam}" />">
<input type="hidden" name="current" value="<c:out value="${form.current}" />">
<input type="hidden" name="countingDivCode" value="<c:out value="${form.countingDivCode}" />">

<c:forEach var ="function" items="${form.function}">
  <input type="hidden" name="function" value="<c:out value="${function}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="./profile/img/ttl_profile_shinki.gif" width="239" height="25" border="0" alt="プロファイル新規作成"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="219">
<!--●●●左側●●●-->

<!--現在の集計区分-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td><img src="./profile/img/tbl_top_syukei.gif" width="219" height="11" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="217" bgcolor="#4694AF">

<%-- 集計区分 --%>
<%@ include file="/jsp/profile/countingDiv.jsp" %>

</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/現在の集計区分-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="216" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">分析機能の選択</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">共通設定</b></td>
</tr>
</table>
<!--/step2-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step3_cur.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130"><b class="text12" style="color:#FFFFFF;">担当クラスの選択</b></td>
</tr>
</table>
<!--/step3-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step4_def.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step4-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--完了-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#98ACB7">
<td width="198" align="center"><b class="text12" style="color:#FFFFFF;">完了</b></td>
</tr>
</table>
<!--/完了-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="219" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="219" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->

</td>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="682">

<!--●●●右側●●●-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="673">

<table border="0" cellpadding="0" cellspacing="0" width="673">
<tr valign="top">
<td colspan="4" width="673" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="673" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step3.gif" width="86" height="24" border="0" alt="STEP3"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="579" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">担当クラスの選択</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--担当クラスの選択-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="652">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="652">
<tr valign="top">
<td width="650" bgcolor="#FFFFFF" align="center">

<!--ケイコさん-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="596">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="514">

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="42" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="500" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="470"><span class="text14">対象学年を選択後、担当しているクラスにチェックしてください。チェックした後、下の「選択」ボタンを押すことで選択が決定します。（全40クラス選択可）</span>

<%-- 説明文の切替 --%>
<c:if test="${ check == '0' }">
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><b class="text12" style="color:#FF6E0E;">※</b></td>
<td><b class="text12" style="color:#FF6E0E;">下記の設定はあなたの分析範囲では設定の必要はありません。</b></td>
</tr>
</table>
</div>
</c:if>

</td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--対象学年-->
<table border="0" cellpadding="0" cellspacing="2" width="578">
<tr>
<td width="60"><span class="text12">対象年度：</span></td>
<td width="80">
<select name="year" class="text12" style="width:60px;" onChange="changeYear()">
<c:forEach var="year" items="${ClassBean.yearList}">
	<option value="<c:out value="${year}" />"<c:if test="${ year == form.year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select>
</td>
<td width="60"><span class="text12">対象学年：</span></td>
<td>
<select name="grade" class="text12" style="width:60px;" onChange="selectTable()"></select>
</td>
</tr>
</table>
<!--/対象学年-->

<table border="0" cellpadding="2" cellspacing="0" width="574">
<tr>
<td width="574" bgcolor="#93A3AD">

<table border="0" cellpadding="3" cellspacing="0">
<tr height="27">
<td><input type="checkbox" name="ctrl" value="" onclick="selectAll(this.checked);"></td>
<td><b class="text12" style="color:#FFF;">担当クラスをすべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>

</td>
</tr>
</table>

<c:if test="${ not empty ClassBean.classYearMap }">
<c:forEach var="year" items="${ ClassBean.yearList }">
	<c:forEach var="grade" items="${ ClassBean.classYearMap[year] }">
		<div id="Table-<c:out value="${ year }" />-<c:out value="${ grade.grade }" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
		<table border="0" cellpadding="4" cellspacing="2" width="578">
		<tr>
		<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
		<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
		<td width="8%" bgcolor="#CDD7DD" align="center"><b class="text12">学年</span></td>
		<td width="18%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><b class="text12">クラス名</span></td>
		</tr>
		</table>
		</td>
		<td width="11%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><b class="text12">人数</span></td>
		</tr>
		</table>
		</td>
		<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
		<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
		<td width="8%" bgcolor="#CDD7DD" align="center"><span class="text12">&nbsp;</span></td>
		<td width="7%" bgcolor="#CDD7DD" align="center"><b class="text12">学年</span></td>
		<td width="19%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><b class="text12">クラス名</span></td>
		</tr>
		</table>
		</td>
		<td width="11%" bgcolor="#CDD7DD">
		<table border="0" cellpadding="4" cellspacing="0">
		<tr>
		<td><b class="text12">人数</span></td>
		</tr>
		</table>
		</td>
		</tr>

		<c:forEach begin="0" end="${ grade.halfSize - 1 }" varStatus="status">
			<c:set var="leftClass" value="${ grade.classList[status.index] }" />
			<c:set var="leftKey" value="${ leftClass.year },${ leftClass.grade },${ leftClass.key }" />
			<c:choose>
				<c:when test="${ CheckedMap[leftKey] }">
					<c:set var="leftColor" value="#FFAD8C" />
				</c:when>
				<c:otherwise>
					<c:set var="leftColor" value="#F4E5D6" />
				</c:otherwise>
			</c:choose>
			<tr height="27">
			<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count }" /></span></td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="center"><input type="checkbox" name="classes" value="<c:out value="${ leftKey }" />" onclick=""<c:if test="${ CheckedMap[leftKey] }"> checked</c:if>></td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="center"><span class="text12"><c:out value="${ leftClass.grade }" />年</span></td>
			<td bgcolor="<c:out value="${ leftColor }" />">
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ leftClass.className }" /></span></td>
			</tr>
			</table>
			</td>
			<td bgcolor="<c:out value="${ leftColor }" />" align="right" nowrap>
			<table border="0" cellpadding="4" cellspacing="0">
			<tr>
			<td><span class="text12"><c:out value="${ leftClass.examinees }" />人</span></td>
			</tr>
			</table>
			</td>
			<td bgcolor="#FFFFFF">&nbsp;</td>
			<c:choose>
				<c:when test="${ grade.size % 2 == 0 || not status.last }">
					<c:set var="rightClass" value="${ grade.classList[status.index + grade.halfSize ] }" />
					<c:set var="rightKey" value="${ rightClass.year },${ rightClass.grade },${ rightClass.key }" />
					<c:choose>
						<c:when test="${ CheckedMap[rightKey] }">
							<c:set var="rightColor" value="#FFAD8C" />
						</c:when>
						<c:otherwise>
							<c:set var="rightColor" value="#F4E5D6" />
						</c:otherwise>
					</c:choose>
					<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count + grade.halfSize }" /></span></td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="center"><input type="checkbox" name="classes" value="<c:out value="${ rightKey }" />" onclick=""<c:if test="${ CheckedMap[rightKey] }"> checked</c:if>></td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="center"><span class="text12"><c:out value="${ rightClass.grade }" />年</span></td>
					<td bgcolor="<c:out value="${ rightColor }" />">
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"><c:out value="${ rightClass.className }" /></span></td>
					</tr>
					</table>
					</td>
					<td bgcolor="<c:out value="${ rightColor }" />" align="right" nowrap>
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"><c:out value="${ rightClass.examinees }" />人</span></td>
					</tr>
					</table>
					</td>
				</c:when>
				<c:otherwise>
					<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ status.count + grade.halfSize }" /></span></td>
					<td bgcolor="#F4E5D6" align="center"></td>
					<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
					<td bgcolor="#F4E5D6">
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"></span></td>
					</tr>
					</table>
					</td>
					<td bgcolor="#F4E5D6" align="right" nowrap>
					<table border="0" cellpadding="4" cellspacing="0">
					<tr>
					<td><span class="text12"></span></td>
					</tr>
					</table>
					</td>
				</c:otherwise>
			</c:choose>
			</tr>
		</c:forEach>
		</table>
		</div>
	</c:forEach>
</c:forEach>
</c:if>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/担当クラスの選択-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;次のSTEPへ&nbsp;" class="text12" style="width:120px;" onClick="submitNext()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

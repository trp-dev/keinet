<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／プロファイル編集</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>

	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var printDialogPattern = "";
	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

	function submitOverwrite() {
		<%-- 上書き禁止モードなら進めない --%>
		<c:if test="${ Profile.overwriteMode == 1 }">
			alert("<kn:message id="w024a" />");
			return;
		</c:if>

		// 自分が所有しているプロファイル
		var owner = new Array();

		<c:forEach var="folder" items="${ProfileFolderBean.folderList}">
			<c:forEach var="profile" items="${folder.ownProfileList}">
				owner["<c:out value="${profile.profileID}" />"] = true;
			</c:forEach>
		</c:forEach>

		if (!owner["<c:out value="${Profile.profileID}" />"]) {
			alert("プロファイルを上書き保存する権限がありません。");
			return;
		}

		if (confirm("<kn:message id="w006c" />")) {
			document.forms[0].forward.value = "w103";
			document.forms[0].submit();
		}
	}

	function submitAlias() {
		<%@ include file="/jsp/script/count_profile.jsp" %>

		document.forms[0].forward.value = "w102";
		document.forms[0].submit();
	}

	function submitBack() {
		document.forms[0].forward.value = document.forms[0].origin.value;
		document.forms[0].submit();
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileSaveSelect" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w101">
<input type="hidden" name="origin" value="<c:out value="${form.origin}" />">
<input type="hidden" name="exit" value="<c:out value="${param.exit}" />">
<input type="hidden" name="targetYear" value="<c:out value="${Exam.examYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${Exam.examCD}" />">
<input type="hidden" name="targetPersonId" value="<c:out value="${param.targetPersonId}" />">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908"><img src="./profile/img/ttl_profile_hozon.gif" width="188" height="24" border="0" alt="プロファイル保存"><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="3" width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="159"><img src="./shared_lib/img/parts/sp.gif" width="159" height="1" border="0" alt=""><br></td>
<td width="610" align="center">

<!--ケイコさん-->
<table border="0" cellpadding="0" cellspacing="0" width="460">
<tr valign="top">
<td width="56"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="404">

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="390" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="390">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="365"><span class="text12"><b>保存方法の選択</b><br>
プロファイルを保存します。現在利用中のプロファイルを確認し、「上書き保存」または、「別名保存」※を選択してください。</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ケイコさん-->
<table border="0" cellpadding="0" cellspacing="0" width="460">
<tr valign="top">
<td width="65"><img src="./shared_lib/img/parts/sp.gif" width="65" height="2" border="0" alt=""><br></td>
<td nowrap><span class="text12"><b>※別名保存・・・</b></span></td>
<td><span class="text12">現在利用中のプロファイルは変更前の状態で保存されつつ、変更後の状態が新しいプロファイルとして追加保存されます。</span></td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="610">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="601">

<table border="0" cellpadding="0" cellspacing="0" width="601">
<tr valign="top">
<td colspan="4" width="601" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="601" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="601" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="601" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="587" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="587">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">現在利用中のプロファイル</b></td>
<td align="right"><font class="text12" color="#FFFFFF">今日の日付 ： <fmt:formatDate value="${now}" pattern="yyyy/MM/dd" /></font><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="601" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="601" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--現在利用中のプロファイル-->
<table border="0" cellpadding="0" cellspacing="0" width="610">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="610">
<tr valign="top">
<td width="608" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="580">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="580">
<tr valign="top">
<td width="578" bgcolor="#FFFFFF" align="center">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="538">
<tr valign="top">
<td width="273">
<!--左側-->
<table border="0" cellpadding="2" cellspacing="0">
<tr valign="top">
<td nowrap><b class="text12-hh">プロファイル作成日：&nbsp;</b></td>
<td nowrap><span class="text12-hh"><fmt:formatDate value="${Profile.createDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
<tr valign="top">
<td nowrap><b class="text12-hh">最終分析日：&nbsp;</b></td>
<td nowrap><span class="text12-hh"><fmt:formatDate value="${Profile.updateDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
</table>
<table border="0" cellpadding="2" cellspacing="0">
<tr valign="top">
<td nowrap><b class="text12-hh">格納フォルダ：&nbsp;</b></td>
<td nowrap><span class="text12-hh"><c:out value="${Profile.folderName}" /></span></td>
</tr>
<tr valign="top">
<td nowrap><b class="text12">プロファイル名：&nbsp;</b></td>
<td nowrap><span class="text12-hh"><c:out value="${Profile.profileName}" /></span></td>
</tr>
<c:if test="${ MenuSecurity.menu['801'] }">
<tr valign="top">
<td nowrap><b class="text12">作成者：&nbsp;</b></td>
<td nowrap><span class="text12-hh"><c:out value="${Profile.userName}" /></span></td>
</tr>
</c:if>
</table>
<!--/左側-->
</td>
<td width="17"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="17"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="230">
<!--右側コメント-->
<table border="0" cellpadding="2" cellspacing="0">
<tr valign="top">
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td><span class="text12-hh"><b class="text12">コメント：&nbsp;</b><br><kn:pre><c:out value="${Profile.comment}" /></kn:pre></span></td>	--%>
<td style="word-break:break-all;"><span class="text12-hh"><b class="text12">コメント：&nbsp;</b><br><kn:pre><c:out value="${Profile.comment}" /></kn:pre></span></td>
<%-- 2015/12/21 QQ)Hisakawa 大規模改修 UPD END --%>
</tr>
</table>
<!--/右側コメント-->
</td>
</tr>
</table>


<!--対象模試-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="542">
<tr height="22" bgcolor="#8CA9BB">
<td colspan="2" width="538">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">対象模試</b></td>
</tr>
</table>
</td>
</tr>
<!--set-->
<tr height="22">
<td width="24%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">対象年度</b></td>
</tr>
</table>
</td>
<td width="76%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><c:out value="${Exam.examYear}" />年度</span></td>
</tr>
</table>
</td>
</tr>
<!--/set-->
<!--set-->
<tr height="22">
<td width="24%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">対象模試</b></td>
</tr>
</table>
</td>
<td width="76%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><c:out value="${Exam.examName}" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/set-->
<c:if test="${ not LoginSession.sales && CountingDivBean.countingDivListSize > 1 }">
<!--set-->
<tr height="22">
<td width="24%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">集計区分</b></td>
</tr>
</table>
</td>
<td width="76%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><c:out value="${Profile.bundleName}" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/set-->
</c:if>
</table>
</td>
</tr>
</table>
</div>
<!--/対象模試-->


<!--共通項目設定状況-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="542">
<tr height="22" bgcolor="#8CA9BB">
<td colspan="4" width="538">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">共通項目設定状況</b></td>
</tr>
</table>
</td>
</tr>
<!--set-->
<tr height="22">

<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">型</b></td>
</tr>
</table>
</td>

<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><kn:com item="type" /></span></td>
</tr>
</table>
</td>

<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">科目</b></td>
</tr>
</table>
</td>

<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><kn:com item="course" /></span></td>
</tr>
</table>
</td>

</tr>
<!--/set-->

<jsp:useBean id="common" class="java.util.ArrayList" />
<%-- 比較対象年度 --%>
<c:if test="${MenuSecurity.menu['603']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象年度</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><kn:com item="year" /></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- 志望大学 --%>
<kn:listAdd var="common">
	<td width="22%" bgcolor="#E1E6EB">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td><b class="text12">志望大学</b></td>
	</tr>
	</table>
	</td>
	<td width="28%" bgcolor="#F4E5D6">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
	<td><span class="text12"><kn:com item="univ" /></span></td>
	</tr>
	</table>
	</td>
</kn:listAdd>
<%-- 比較対象クラス --%>
<c:if test="${MenuSecurity.menu['605']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象クラス</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><kn:com item="class" /></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- 比較対象高校 --%>
<c:if test="${MenuSecurity.menu['606']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象高校</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><kn:com item="school" /></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- SPACER --%>
<kn:listPad var="common" size="4">
</kn:listPad>
<%-- 出力 --%>
<tr height="22">
	<c:out value="${common[0]}" escapeXml="false" />
	<c:choose>
		<c:when test="${ empty common[1] }">
			<td width="22%" bgcolor="#E1E6EB"></td>
			<td width="28%" bgcolor="#F4E5D6"></td>
		</c:when>
		<c:otherwise>
			<c:out value="${common[1]}" escapeXml="false" />
		</c:otherwise>
	</c:choose>
</tr>
<c:if test="${ not empty common[2] }">
	<tr height="22">
		<c:out value="${common[2]}" escapeXml="false" />
		<c:choose>
			<c:when test="${ empty common[3] }">
				<td width="22%" bgcolor="#E1E6EB"></td>
				<td width="28%" bgcolor="#F4E5D6"></td>
			</c:when>
			<c:otherwise>
				<c:out value="${common[3]}" escapeXml="false" />
			</c:otherwise>
		</c:choose>
	</tr>
</c:if>

</table>
</td>
</tr>
</table>
</div>
<!--/共通項目設定状況-->

<c:if test="${MenuSecurity.menu['607']}">
<!--担当クラス-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="542">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="542">
<tr height="22" bgcolor="#8CA9BB">
<td width="538">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">担当クラス</b></td>
</tr>
</table>
</td>
</tr>
<tr height="22" bgcolor="#F4E5D6">
<td width="538">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><kn:com item="charge" /></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/担当クラス-->
</c:if>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/現在利用中のプロファイル-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->



<!--保存方法の選択-->
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="4" cellspacing="1" width="530">

<tr valign="top">
<td width="522" bgcolor="#EFF2F3">
<table border="0" cellpadding="3" cellspacing="0" width="522">
<tr>
<td bgcolor="#758A98" align="center"><b class="text12" style="color:#FFFFFF">保存方法の選択</b>
</td>
</tr>
</table>
</td>
</tr>

<tr valign="top">
<td width="522" bgcolor="#FFFFFF" align="center">
<!--ボタン-->
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="512">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="512">
<tr valign="top">
<td bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onClick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>

<%-- 2015/12/17 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="button" value="&nbsp;上書き保存&nbsp;" class="text12" style="width:120px;" onClick="submitOverwrite()"></td>
  --%>
<td><input type="button" value="&nbsp;上書き保存&nbsp;" class="text12" style="width:120px;" onClick="submitOverwrite();" <c:if test="${ Profile.template }"><c:out value="disabled" /></c:if>></td>
<%-- 2015/12/17 QQ)Nishiyama 大規模改修 UPD END --%>

<td><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;別名保存&nbsp;" class="text12" style="width:120px;" onClick="submitAlias()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br>
<!--/ボタン-->
</td>
</tr>

</table>
</td>
</tr>
</table>
<!--/保存方法の選択-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、保存せずに前のページに戻ります。</span></td>
</tr>
</table>
</div>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="199"><img src="./shared_lib/img/parts/sp.gif" width="199" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>

<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

<!--********************お知らせ情報表示＆お知らせ詳細表示********************-->
<!--お知らせ-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="149">
<tr valign="top">
<td><img src="./profile/img/left_sub_ttl_news.gif" width="149" height="18" border="0" alt="お知らせ"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="149">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="2" cellspacing="1" width="149">
<tr valign="top">
<td width="147" bgcolor="#FAFAFA">

<table border="0" cellpadding="0" cellspacing="5" width="143">
<!-- お知らせset-->
<% 
 for (java.util.Iterator it=InformListBean.getInformList().iterator(); it.hasNext();) {
    InformList list = (InformList)it.next();

    String divcolor = "#4997BF";
    if ( list.getDispDiv().equals("一般") ) {
        divcolor = "#4997BF";
    } else if ( list.getDispDiv().equals("高校別") ) {
        divcolor = "#FF8F0E";
    } else if ( list.getDispDiv().equals("県別") ) {
        divcolor = "#FF99FF";
    }
%>
    <tr valign="top">
    <td width="133"><span class="text12-hh">
    <b>［<font color="<%=divcolor%>"><%=list.getDispDiv()%></font>］ <%=list.getDispDate()%></b><br>
    <a href="javascript:openInfoDetail('0', '<%=list.getInfoId()%>')"><%=list.getTitle()%></td>
    </tr>
    <tr valign="top">
    <td width="133"><img src="./shared_lib/img/parts/dot_blue.gif" width="133" height="1" border="0" alt=""><br></td>
    </tr>
<% } %>
<!--/お知らせset-->

<!--一般set--
<tr valign="top">
<td width="133"><span class="text12-hh">
<b>［<font color="#4997BF">一般</font>］ 2004/08/01</b><br>
<a href="#">8月13日0時〜8月15日24時の間、サービスを停止します。</a></span></td>
</tr>
<tr valign="top">
<td width="133"><img src="./shared_lib/img/parts/dot_blue.gif" width="133" height="1" border="0" alt=""><br></td>
</tr>
--/一般set-->
<!--高校別set--
<tr valign="top">
<td width="133"><span class="text12-hh">
<b>［<font color="#FF8F0E">高校別</font>］ 2004/08/01</b><br>
<a href="#">8月13日0時〜8月15日24時の間、サービスを停止します。</a></span></td>
</tr>
<tr valign="top">
<td width="133"><img src="./shared_lib/img/parts/dot_blue.gif" width="133" height="1" border="0" alt=""><br></td>
</tr>
--/高校別set-->
<!--一般set--
<tr valign="top">
<td width="133"><span class="text12-hh">
<b>［<font color="#4997BF">一般</font>］ 2004/08/01</b><br>
<a href="#">8月13日0時〜8月15日24時の間、サービスを停止します。</a></span></td>
</tr>
<tr valign="top">
<td width="133"><img src="./shared_lib/img/parts/dot_blue.gif" width="133" height="1" border="0" alt=""><br></td>
</tr>
--/一般set-->

<tr valign="top">
<td width="133" align="right"><span class="text10">
<a href="javascript:openInfoTop('0')">more &gt;&gt;</a>
</span></td>
</tr></table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/お知らせ-->

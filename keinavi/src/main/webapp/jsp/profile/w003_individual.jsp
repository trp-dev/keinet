<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--個人成績分析-->

<%-- チェック --%>
<c:set var="checked" value="" />
<c:forEach var ="function" items="${form.function}">
  <c:if test="${ function == 'individual' }"><c:set var="checked" value=" checked" /></c:if>
</c:forEach>

<table border="0" cellpadding="0" cellspacing="0" width="304">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="304">
<tr valign="top">
<td width="302" height="36" bgcolor="#E5EEF3">

<table border="0" cellpadding="0" cellspacing="8" width="302">
<tr>
<td><input type="checkbox" name="function" value="individual"<c:out value="${checked}" />> <b class="text14">個人成績分析</b></td>
<td align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><a href="javascript:openSample('i101')"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="0" alt="→" hspace="5"></a></td>
<td><span class="text12"><a href="javascript:openSample('i101')">サンプルの表示</a></span></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
<tr valign="top">
<td width="302" bgcolor="#FFFFFF" align="center">

<!--上の段-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="290">
<tr>
<td rowspan="2" width="93" align="right" valign="top"><!--イラスト--><img src="./profile/img/icon_individual.gif" width="88" height="92" border="0" alt="個人成績分析"><br></td>
<td rowspan="2" width="10" valign="top"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="187" valign="top">
<table border="0" cellpadding="0" cellspacing="0" width="187">
<tr>
<td><span class="text12-hh">・個人分析</span></td>
<td><span class="text12-hh">・志望大学判定</span></td>
</tr>
<tr>
<td><span class="text12-hh">・個人手帳</span></td>
<td><span class="text12-hh">&nbsp;</span></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="187" valign="bottom">
<table border="0" cellpadding="0" cellspacing="0" width="187">
<tr valign="top">
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="3" width="187">
<tr valign="top">
<td width="1%"><b class="text12">★</b></td>
<td width="99%"><span class="text12"><b>こんな方にオススメ</b><br>→ クラス担任</span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
<!--/上の段-->

<table border="0" cellpadding="0" cellspacing="0" width="290">
<tr>
<td bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="290" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--下の段-->
<div style="margin-top:3px;">
<table border="0" cellpadding="2" cellspacing="0" width="290">
<tr>
<td><span class="text12-hh">生徒一人ひとりの詳細な分析を行い、進路指導・個人面談などの実施に役立てます。</span></td>
</tr>
</table>
</div>
<!--/下の段-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/個人成績分析-->

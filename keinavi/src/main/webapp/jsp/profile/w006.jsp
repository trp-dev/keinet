<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル新規作成</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>

	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>

	function submitBack() {

		<c:choose>
			<%-- 担当クラスなし --%>
			<c:when test="${ not MenuSecurity.menu['607'] }">
				document.forms[0].forward.value = "w004";
			</c:when>
			<%-- 担当クラスあり --%>
			<c:otherwise>
				document.forms[0].forward.value = "w005";
			</c:otherwise>
		</c:choose>

		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

	function submitSave() {
		var message = "";

		// 入力チェック（プロファイル名）
		if (document.forms[0].profileName.value == "") {
			message += "<kn:message id="w018a" />\n";
		}

		var js = new JString();
		// 入力チェック（プロファイル名の文字数）
		js.setString(document.forms[0].profileName.value);
		if (js.getLength() > 30) {
			message += "プロファイル名は全角15文字までです。\n";
		}

		// 入力チェック（コメントの文字数）
		js.setString(document.forms[0].comment.value);
		if (js.getLength() > 100) {
			message += "<kn:message id="w019a" />\n";
		}

		// フォルダ
		if (current == "") {
			message += "保存先フォルダを選択してください。\n";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		document.forms[0].forward.value = "w007";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

	function submitCancel() {
		document.forms[0].forward.value = "w002";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

<%-- プロファイルフォルダ操作用JavaScript --%>
<%@ include file="/jsp/profile/profile.jsp" %>

	// フォルダ編集ウィンドウを開く
	function openFolderEditor() {
		<%@ include file="/jsp/script/count_folder.jsp" %>

		var url = "<c:url value="ProfileFolderEdit" />";
		// 新規なのでフォルダIDはない
		url += "?folderID=" + "";
		url += "&forward=w009";
		url += "&backward=<c:out value="${ param.forward }" />";
		url += "&countingDivCode=<c:out value="${form.countingDivCode}" />";
		openWindow(url,  680);
	}

	function submitClosed() {
		document.forms[0].forward.value = "w006";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

	function init() {
		startTimer();
		<c:choose>
			<%-- プロファイルのフォルダID --%>
			<c:when test="${!empty(Profile.folderID)}">
				changeFolder("<c:out value="${Profile.folderID}" />");
			</c:when>
			<%-- 前画面からのカレントフォルダ指定 --%>
			<c:when test="${!empty(form.current)}">
				changeFolder(document.forms[0].current.value);
			</c:when>
			<%-- デフォルトは一番上 --%>
			<c:otherwise>
				<c:if test="${ ProfileFolderBean.folderListSize > 0 }">
					changeFolder("<c:out value="${ProfileFolderBean.folderList[0].folderID}" />");
				</c:if>
			</c:otherwise>
		</c:choose>
	}

// -->
</script>
</head>

<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileCreate4" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w006">
<input type="hidden" name="targetYear" value="<c:out value="${form.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${form.targetExam}" />">
<input type="hidden" name="countingDivCode" value="<c:out value="${form.countingDivCode}" />">
<input type="hidden" name="current" value="<c:out value="${form.current}" />">
<c:forEach var ="function" items="${form.function}">
  <input type="hidden" name="function" value="<c:out value="${function}" />">
</c:forEach>

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td><img src="./profile/img/ttl_profile_shinki.gif" width="239" height="25" border="0" alt="プロファイル新規作成"><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="219">
<!--●●●左側●●●-->

<!--現在の集計区分-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td><img src="./profile/img/tbl_top_syukei.gif" width="219" height="11" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="217" bgcolor="#4694AF">

<%-- 集計区分 --%>
<%@ include file="/jsp/profile/countingDiv.jsp" %>

</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/現在の集計区分-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="219">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="216" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">分析機能の選択</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">共通設定</b></td>
</tr>
</table>
<!--/step2-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->

<c:choose>
<%-- 担当クラスなし --%>
<c:when test="${ not MenuSecurity.menu['607'] }">
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step3_cur.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step3-->
</c:when>
<%-- 担当クラスあり --%>
<c:otherwise>
<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step3_def.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">担当クラスの選択</b></td>
</tr>
</table>
<!--/step3-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step4_cur.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="130"><b class="text12" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
</table>
<!--/step4-->
</c:otherwise>
</c:choose>

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr valign="top">
<td width="198" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->
<!--完了-->
<table border="0" cellpadding="0" cellspacing="0" width="198">
<tr height="22" bgcolor="#98ACB7">
<td width="198" align="center"><b class="text12" style="color:#FFFFFF;">完了</b></td>
</tr>
</table>
<!--/完了-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="219" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="219" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->

</td>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="682">

<!--●●●右側●●●-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="673">

<table border="0" cellpadding="0" cellspacing="0" width="673">
<tr valign="top">
<td colspan="4" width="673" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="673" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="24" border="0" alt=""><br></td>

<c:choose>
<%-- 担当クラスなし --%>
<c:when test="${ not MenuSecurity.menu['607'] }">
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step3.gif" width="86" height="24" border="0" alt="STEP3"><br></td>
</c:when>
<%-- 担当クラスあり --%>
<c:otherwise>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step4.gif" width="86" height="24" border="0" alt="STEP4"><br></td>
</c:otherwise>
</c:choose>

<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="579" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">プロファイルの保存</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="673" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="673" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--プロファイルの保存-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="652">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="652">
<tr valign="top">
<td width="650" bgcolor="#FFFFFF" align="center">

<!--ケイコさん-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="596">
<tr valign="top">
<td width="72" align="right"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="514">

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="15" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="15" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="500" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>

<c:choose>
<%-- 担当クラスなし --%>
<c:when test="${ not MenuSecurity.menu['607'] }">
<td width="470"><span class="text14">STEP1から2で設定されたプロファイルを保存します。プロファイル名を記入し、保存先フォルダを選択してください。<br></span>
</c:when>
<%-- 担当クラスあり --%>
<c:otherwise>
<td width="470"><span class="text14">STEP1から3で設定されたプロファイルを保存します。プロファイル名を記入し、保存先フォルダを選択してください。<br></span>
</c:otherwise>
</c:choose>

</td>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="514">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="490" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="490" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="490" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="490" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="600">
<!--プロファイル作成日-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル作成日</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><fmt:formatDate value="${now}" pattern="yyyy/MM/dd" /></span></td>
</tr>
<!--/プロファイル作成日-->
<!--プロファイル名-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル名</b></td>
<td width="450" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="text" size="40" maxlength="30" class="text12" name="profileName" value="<c:out value="${Profile.profileName}" />"></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><span class="text12">※全角15文字まで</span></td>
</tr>
</table>

</td>
</tr>
<!--/プロファイル名-->
<!--コメント-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">コメント</b></td>
<td width="450" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><textarea rows="4" cols="40" name="comment" style="width:400px;"><c:out value="${Profile.comment}" /></textarea></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><span class="text12">※全角50文字まで</span></td>
</tr>
</table>

</td>
</tr>
<!--/コメント-->
<!--保存先フォルダ-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">保存先フォルダ</b></td>
<td width="450" bgcolor="#FFFFFF">


<%-- フォルダリストがなければ表示しない --%>
<c:if test="${ ProfileFolderBean.folderListSize > 0 }">

  <!--フォルダリスト-->
  <table border="0" cellpadding="0" cellspacing="0" width="450">
  <tr valign="top">
  <td width="18"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→" vspace="5"><br></td>
  <td width="217" bgcolor="#EBEFF2">
  <!--左リスト-->
  <table border="0" cellpadding="0" cellspacing="0" width="217">

  <!--タイトル-->
  <tr>
  <td width="215"><img src="./shared_lib/img/parts/ttl_folder_list.gif" width="215" height="25" border="0" alt="フォルダリスト"><br></td>
  <td width="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
  </tr>
  <!--/タイトル-->
  <tr>
  <td colspan="2" width="217" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
  </tr>

  <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
    <tr>
    <td width="215" bgcolor="#EBEFF2" id="COLOR1-<c:out value="${folder.folderID}" />">
    <table border="0" cellpadding="0" cellspacing="0" width="215">
    <tr>
    <td width="22" align="right"><!--フォルダアイコン--><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><img src="./shared_lib/img/parts/icon_folder_close.gif" width="13" height="16" border="0" name="FOLDER-<c:out value="${folder.folderID}" />"></a></td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="172"><!--フォルダ名--><span class="text12"><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><c:out value="${folder.folderName}" /></a></span></td>
    <td width="15"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" name="ARROW-<c:out value="${folder.folderID}" />"></td>
    </tr>
    </table>
    </td>
    <td width="2" bgcolor="#FFFFFF" id="COLOR2-<c:out value="${folder.folderID}" />"><img src="./shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td colspan="2" width="217" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
    </tr>
  </c:forEach>

  </table>
  <!--/左リスト-->

  </td>
  <td width="215" bgcolor="#FFAD8C">

  <!--右リスト-->
  <table border="0" cellpadding="5" cellspacing="0" width="215">
  <tr>
  <td width="215" bgcolor="#FFAD8C" align="center">

  <c:forEach var="folder" items="${ProfileFolderBean.folderList}">
    <div id="PL-<c:out value="${folder.folderID}" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
    <c:forEach var="profile" items="${folder.profileList}">
      <table border="0" cellpadding="0" cellspacing="0" width="197">
      <tr valign="top">
      <td width="197" bgcolor="#FFAD8C">
      <table border="0" cellpadding="0" cellspacing="1" width="197">
      <tr height="40" bgcolor="#FFFFFF">
      <td width="195">
      <table border="0" cellpadding="0" cellspacing="0" width="195">
      <tr>
      <td width="19" align="right"><!--ファイルアイコン--><img src="./shared_lib/img/parts/icon_file.gif" width="11" height="13" border="0" alt="<c:out value="${profile.comment}" />"></td>
      <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
      <td width="164"><span class="text12" title="<c:out value="${profile.comment}" />"><c:out value="${profile.profileName}" /><c:if test="${ MenuSecurity.menu['801'] }"><br>（<c:out value="${profile.userName}" />）</c:if></span></td>
      <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
      </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
      </table>
    </c:forEach>
    </div>
  </c:forEach>

  </td>
  </tr>
  </table>
  <!--/右リスト-->
  </td>
  </tr>
  </table>
  <!--/フォルダリスト-->

</c:if>


<!--ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="450">
<tr valign="top">
<td width="450" align="right"><input type="button" value="&nbsp;新規フォルダ作成&nbsp;" class="text12" style="width:130px;" onClick="openFolderEditor()"></td>
</tr>
</table>
</div>
<!--/ボタン-->
</td>
</tr>
<!--/保存先フォルダ-->
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/プロファイルの保存-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="682">
<tr valign="top">
<td width="680" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onClick="submitCancel()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;保存&nbsp;" class="text12" style="width:120px;" onClick="submitSave()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="682">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、保存せずにプロファイル選択画面に戻ります。</span></td>
</tr>
</table>
</div>



<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

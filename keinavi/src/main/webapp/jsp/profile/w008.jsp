<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.news.InformList" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="InformListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />
<jsp:useBean id="KeinetListBean" scope="request" class="jp.co.fj.keinavi.beans.news.InformListBean" />
<jsp:useBean id="OnepointBean" scope="request" class="jp.co.fj.keinavi.beans.help.OnepointNewBean" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
<title>Kei-Navi／専用トップ</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/24 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    <%@ include file="/jsp/script/submit_change.jsp" %>
    <%@ include file="/jsp/script/submit_exit.jsp" %>
    <%@ include file="/jsp/script/submit_save.jsp" %>
    <%@ include file="/jsp/script/submit_menu.jsp" %>
    <%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_charge.jsp" %>

    // ヘルプ・問い合わせ
    <%@ include file="/jsp/script/submit_help.jsp" %>
    // お知らせ詳細画面を開いて表示
    <%@ include file="/jsp/script/submit_openinfo.jsp" %>
    // ワンポイント詳細画面を開いて表示
    <%@ include file="/jsp/script/submit_openOnepoint.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2016/01/28 QQ)Nishiyama 大規模改修 ADD START --%>
    function submitForm1() {
    	window.open("about:blank","moshiWindow");
    	document.forms[1].target = "moshiWindow";
    	document.forms[1].action = "<c:out value="${moshiURL}" />";
    	document.forms[1].sciv.value = "<c:out value="${sciv}" />";
    	document.forms[1].scid.value = "<c:out value="${scid}" />";
    	document.forms[1].scnm.value = "<c:out value="${scnm}" />";
		document.forms[1].submit();
    }
    <%-- 2016/01/28 QQ)Nishiyama 大規模改修 ADD END   --%>

    function init() {
    	startTimer();

    	<%-- 2015/12/15 QQ)Nishiyama 大規模改修 ADD START --%>
    	<c:set var="backward" value="w008" />
    	<%-- 2015/12/15 QQ)Nishiyama 大規模改修 ADD END --%>
    }

// -->
</script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
</head>

<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileTop" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w008">
<input type="hidden" name="exit" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="168" bgcolor="#E5EEF3" align="center">
  --%>
<!--●●●左側●●●-->
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" align="center">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>

<!-- お知らせ情報表示-->
<!-- 2007年度改修A項目対応にてお知らせ削除

-->
<!--/お知らせ情報表示-->

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<!-- Kei-Net注目情報表示-->
<%@ include file="/jsp/profile/keinet.jsp" %>
<!--/Kei-Net注目情報表示-->
  --%>
<!--↓ダウンロードサービス-->
<%@ include file="/jsp/freemenu/f000.jsp" %>
<!--↑ダウンロードサービス-->
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>


<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
  --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL END --%>
<!--/●●●左側●●●-->
</td>

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
  --%>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>
<td width="530">

<!--●●●中央●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="521">

<table border="0" cellpadding="0" cellspacing="0" width="521">
<tr valign="top">
<td colspan="4" width="521" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="521" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="24" border="0" alt=""><br></td>
<td width="507" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">「<c:out value="${Profile.profileName}" />」専用トップページ</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="521" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="521" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->

<!--「××××××」専用トップページ-->
<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="530">
<tr valign="top">
<td width="528" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="14" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="500">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="500">
<tr valign="top">
<td width="498" bgcolor="#FFFFFF" align="center">
<!--ケイコさん-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="458">
<tr valign="top">
<td width="56"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="402">

<table border="0" cellpadding="0" cellspacing="0" width="402">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="378" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="378" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="378" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="378" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="402">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="388" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="388">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="363">

<table border="0" cellpadding="0" cellspacing="0" width="363">
<tr>
<td width="363"><span class="text12">現在の設定状況はご覧の通りです。詳細の確認は各リンクからどうぞ。</span></td>
</tr>
<tr>
<td width="363"><img src="./shared_lib/img/parts/dot_blue.gif" width="363" height="1" border="0" alt="" vspace="5"><br></td>
</tr>
<tr>
<td width="363"><span class="text12"><b>プロファイル作成日 ：</b> <fmt:formatDate value="${Profile.createDate}" pattern="yyyy/MM/dd" /></span>
<img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt="">
<span class="text12"><b>最終分析日 ：</b> <fmt:formatDate value="${Profile.updateDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
</table>

</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="402">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="378" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="378" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="378" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="378" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--対象模試-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="462">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="462">
<tr height="22" bgcolor="#8CA9BB">
<td width="458">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">対象模試</b></td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#F4E5D6">
<td width="458" align="center">

<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="2" width="442">
<tr>
<td width="70"><b class="text12">対象年度</b></td>
<td width="230"><b class="text12">対象模試</b></td>
<td width="130">
<c:if test="${ not LoginSession.sales && CountingDivBean.countingDivListSize > 1 }">
<b class="text12">対象区分</b>
</c:if>
</td>
</tr>
<tr>
<td width="70"><select name="targetYear" class="text12" style="width:60px;" onChange="submitMenu('w008')">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
<td width="230"><select name="targetExam" class="text12" style="width:220px;" onChange="submitMenu('w008')">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
<td width="130">
<c:if test="${ not LoginSession.sales && CountingDivBean.countingDivListSize > 1 }">
<span class="text12"><c:out value="${Profile.bundleName}" /></span>
</c:if>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/対象模試-->

<!--共通設定状況-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="462">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="462">
<tr height="22" bgcolor="#8CA9BB">
<td colspan="4" width="458">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">共通設定状況</b></td>
</tr>
</table>
</td>
</tr>

<%-- 型・科目 --%>
<tr height="22">
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">型</b></td>
</tr>
</table>
</td>
<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><a href="javascript:openCommon(1)"><kn:com item="type" /></a></span></td>
</tr>
</table>
</td>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">科目</b></td>
</tr>
</table>
</td>
<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><a href="javascript:openCommon(2)"><kn:com item="course" /></a></span></td>
</tr>
</table>
</td>
</tr>

<jsp:useBean id="common" class="java.util.ArrayList" />
<%-- 比較対象年度 --%>
<c:if test="${MenuSecurity.menu['603']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象年度</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><a href="javascript:openCommon(3)"><kn:com item="year" /></a></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- 志望大学 --%>
<kn:listAdd var="common">
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text12">志望大学</b></td>
</tr>
</table>
</td>
<td width="28%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><a href="javascript:openCommon(4)"><kn:com item="univ" /></a></span></td>
</tr>
</table>
</td>
</kn:listAdd>
<%-- 比較対象クラス --%>
<c:if test="${MenuSecurity.menu['605']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象クラス</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><a href="javascript:openCommon(5)"><kn:com item="class" /></a></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- 比較対象高校 --%>
<c:if test="${MenuSecurity.menu['606']}">
	<kn:listAdd var="common">
		<td width="22%" bgcolor="#E1E6EB">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><b class="text12">比較対象高校</b></td>
		</tr>
		</table>
		</td>
		<td width="28%" bgcolor="#F4E5D6">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
		<td><span class="text12"><a href="javascript:openCommon(6)"><kn:com item="school" /></a></span></td>
		</tr>
		</table>
		</td>
	</kn:listAdd>
</c:if>
<%-- SPACER --%>
<kn:listPad var="common" size="4">
</kn:listPad>
<%-- 出力 --%>
<tr height="22">
	<c:out value="${common[0]}" escapeXml="false" />
	<c:choose>
		<c:when test="${ empty common[1] }">
			<td width="22%" bgcolor="#E1E6EB"></td>
			<td width="28%" bgcolor="#F4E5D6"></td>
		</c:when>
		<c:otherwise>
			<c:out value="${common[1]}" escapeXml="false" />
		</c:otherwise>
	</c:choose>
</tr>
<c:if test="${ not empty common[2] }">
	<tr height="22">
		<c:out value="${common[2]}" escapeXml="false" />
		<c:choose>
			<c:when test="${ empty common[3] }">
				<td width="22%" bgcolor="#E1E6EB"></td>
				<td width="28%" bgcolor="#F4E5D6"></td>
			</c:when>
			<c:otherwise>
				<c:out value="${common[3]}" escapeXml="false" />
			</c:otherwise>
		</c:choose>
	</tr>
</c:if>

</table>
</td>
</tr>
</table>
</div>
<!--/共通設定状況-->

<c:if test="${MenuSecurity.menu['607']}">
<!--担当クラス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="462">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="1" cellspacing="2" width="462">
<tr height="22" bgcolor="#8CA9BB">
<td width="458">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><b class="text14" style="color:#FFFFFF">担当クラス</b></td>
</tr>
</table>
</td>
</tr>
<tr height="22" bgcolor="#F4E5D6">
<td width="458">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12"><a href="javascript:openCharge()"><kn:com item="charge" /></a></span></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/担当クラス-->
</c:if>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/「××××××」専用トップページ-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="530">
<tr valign="top">
<%-- 機能リスト --%>
<%-- タブ --%>
<jsp:useBean id="function" class="java.util.LinkedList" />
<%-- 校内成績分析 --%>
<c:if test="${MenuSecurity.menu['100']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_school.jsp" %></td>
	</kn:listAdd>
</c:if>
<%-- 高校成績分析 --%>
<c:if test="${MenuSecurity.menu['200']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_business.jsp" %></td>
	</kn:listAdd>
</c:if>
<%-- クラス成績分析 --%>
<c:if test="${MenuSecurity.menu['300']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_class.jsp" %></td>
	</kn:listAdd>
</c:if>
<%-- 個人成績分析 --%>
<c:if test="${MenuSecurity.menu['400']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_individual.jsp" %></td>
	</kn:listAdd>
</c:if>
<%-- テキスト出力 --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<c:if test="${MenuSecurity.menu['500']}">
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_text.jsp" %></td>
	</kn:listAdd>
</c:if>
  --%>
<%-- 答案閲覧 --%>
<kn:hasKwebLink>
	<kn:listAdd var="function">
		<td width="125"><%@ include file="/jsp/profile/w008_touan.jsp" %></td>
	</kn:listAdd>
</kn:hasKwebLink>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>
<%-- SPACER --%>
<kn:listPad var="function" size="4">
	<td width="125"><img src="./shared_lib/img/parts/sp.gif" width="125" height="1" border="0" alt=""><br></td>
</kn:listPad>
<kn:listSpacer var="function">
	<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</kn:listSpacer>
<%-- 出力 --%>
<kn:listOut var="function" />
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●中央●●●-->

</td>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="180">
  --%>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="235">
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>

<!--●●●右側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<!--ワンポイントアドバイス-->
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><img src="./profile/img/right_sub_ttl_advice.gif" width="180" height="39" border="0" alt="新着　ワンポイントアドバイス！"><br></td>
</tr>
</table>
<!--set-->
<!-- ワンポイントアドバイス-->
<%@ include file="/jsp/shared_lib/Onepoint.jsp" %>
<!--/ワンポイントアドバイス-->
  --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL END --%>
<!--/set-->
<!--set-->
<!--
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><b class="text12"><font color="#758A98">●</font>2004/05/09</b></td>
</tr>
<tr valign="top">
<td><span class="text12">「クラス成績分析／クラス成績／成績概況」</span></td>
</tr>
<tr valign="top">
<td><span class="text12"><a href="#">ヘルプテキストヘルプテキスト</a></span></td>
</tr>
</table>
</div>
-->
<!--/set-->
<!--set-->
<!--
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><b class="text12"><font color="#758A98">●</font>2004/05/09</b></td>
</tr>
<tr valign="top">
<td><span class="text12">「全般」</span></td>
</tr>
<tr valign="top">
<td><span class="text12"><a href="#">ヘルプテキストヘルプテキストヘルプテキスト</a></span></td>
</tr>
</table>
</div>
-->
<!--/set-->
<!--/ワンポイントアドバイスー-->

<!--各種ダウンロードメニュー(体験版は非表示)-->
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${LoginSession.trialMode == 0}">
<%@ include file="/jsp/freemenu/f000.jsp" %>
</c:if>
  --%>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 DEL END --%>
<!--/各種ダウンロードメニュー-->

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<!--お役立ちリンク-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td><img src="./profile/img/right_sub_ttl_link.gif" width="180" height="31" border="0" alt="お役立ちリンク"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="180">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="180">
<tr valign="top">
<td width="178" bgcolor="#FAFAFA">

<!--リンク-->
<%@ include file="/jsp/shared_lib/usefulLink.jsp" %>
<!--/リンク-->

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/お役立ちリンクー-->
  --%>

<table border="0" cellpadding="0" cellspacing="0" width="230">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="230">
<tr valign="top">
<td width="230" bgcolor="#FAFAFA">

<!--リンク-->
<%@ include file="/jsp/profile/keinet.jsp" %>
<!--/リンク-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 ADD START --%>
<table border="0" cellpadding="0" cellspacing="0" width="230">
  <tr valign="top"><td><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td></tr>
  <tr valign="top">
    <%-- テキスト出力 --%>
    <c:if test="${MenuSecurity.menu['500']}">
      <td width="125"><%@ include file="/jsp/profile/w008_text.jsp" %></td>
    </c:if>
  </tr>
</table>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 ADD END --%>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
<!--/●●●右側●●●-->

<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="29"><img src="./shared_lib/img/parts/sp.gif" width="29" height="1" border="0" alt=""><br></td>
  --%>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="177"><img src="./shared_lib/img/parts/com_cnr_lb_b.gif" width="177" height="15" border="0" alt=""><br></td>
<td width="795" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="795" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
  --%>
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
<%-- 2015/12/15 QQ)Nishiyama 大規模改修 UPD END --%>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--先頭へ戻る-->
<%@ include file="/jsp/shared_lib/page_top.jsp" %>
<!--/先頭へ戻る-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer01.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

</form>

<%-- 2016/01/28 QQ)Nishiyama 大規模改修 ADD START --%>
<form action="" method="POST">
<input type="hidden" name="sciv" value="">
<input type="hidden" name="scid" value="">
<input type="hidden" name="scnm" value="">
</form>
<%-- 2016/01/28 QQ)Nishiyama 大規模改修 ADD END   --%>

</body>
</html>

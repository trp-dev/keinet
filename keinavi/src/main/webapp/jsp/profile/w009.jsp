<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル編集</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/JString.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
    
	function submitForm() {
		var message = "";

		// 入力チェック（フォルダ名）
		if (document.forms[0].folderName.value == "") {
			message += "<kn:message id="w017a" />\n";
		}

		var js = new JString();

		// 入力チェック（フォルダ名の文字数）
		js.setString(document.forms[0].folderName.value);
		if (js.getLength() > 30) {
			message += "フォルダ名は全角15文字までです。\n";
		}

		// 入力チェック（コメントの文字数）
		js.setString(document.forms[0].comment.value);
		if (js.getLength() > 100) {
			message += "<kn:message id="w019a" />\n";
		}

		if (message != "") {
			alert(message);
			return;
		}

		document.forms[0].submit();
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>
<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileFolderEdit" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="w009">
<input type="hidden" name="backward" value="w009">
<input type="hidden" name="folderID" value="<c:out value="${form.folderID}" />">
<input type="hidden" name="countingDivCode" value="<c:out value="${form.countingDivCode}" />">

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="570">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="554">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="538" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><b class="text16" style="color:#FFFFFF;">プロファイル詳細編集</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="554" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="554" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="554" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<div style="margin-top:5px;">
<table border="0" cellpadding="2" cellspacing="0" width="570">
<tr valign="top">
<td><span class="text14">選択中のフォルダまたは、プロファイルを確認した上で詳細編集を行ってください。<br>
「登録して閉じる」ボタンを押すと編集内容を登録し、このウィンドウを閉じます。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--選択中のフォルダ-->
<table border="0" cellpadding="0" cellspacing="0" width="469">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="2" width="469">
<tr>
<td width="465" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="455">
<tr>
<td><b class="text12">選択中のフォルダ</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="455">
<tr>
<td width="233" rowspan="3" bgcolor="#FFAD8C">
<!--左側-->
<table border="0" cellpadding="0" cellspacing="0" width="233">
<tr>
<td width="9"><img src="./shared_lib/img/parts/sp.gif" width="9" height="1" border="0" alt=""><br></td>
<td width="21"><img src="./shared_lib/img/parts/icon_folder_open.gif" width="16" height="16" border="0" alt="<c:out value="${folder.folderName}" />"><br></td>
<td width="203"><b class="text12"><c:out value="${folder.folderName}" /></b></td>
</tr>
</table>
<!--/左側-->
</td>
<td width="6">
<table border="0" cellpadding="0" cellspacing="0" width="6">
<tr bgcolor="#EBEFF2">
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="4" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="216" height="25" bgcolor="#EBEFF2">
<!--右側-->
<table border="0" cellpadding="0" cellspacing="0" width="216">
<tr>
<td width="9"><img src="./shared_lib/img/parts/sp.gif" width="9" height="1" border="0" alt=""><br></td>
<td width="21"><img src="./shared_lib/img/parts/icon_files.gif" width="12" height="14" border="0" alt="その他プロファイル4個格納中"><br></td>
<td width="186"><span class="text12">プロファイル<c:out value="${folder.profileListSize}" />個格納中</span></td>
</tr>
</table>
<!--/右側-->
</td>
</tr>

<tr>
<td colspan="2" width="222"><img src="./shared_lib/img/parts/sp.gif" width="222" height="2" border="0" alt=""><br></td>
</tr>

<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="216" height="25">&nbsp;</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/選択中のフォルダ-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--編集-->
<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr>
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="8" cellspacing="2" width="554">
<!--作成日-->
<tr>
<td width="23%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><b class="text12">作成日</b></td>
</tr>
</table>
</td>
<td width="77%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><span class="text12"><fmt:formatDate value="${folder.createDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/作成日-->
<!--更新日-->
<tr>
<td width="23%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><b class="text12">更新日</b></td>
</tr>
</table>
</td>
<td width="77%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><span class="text12"><fmt:formatDate value="${folder.updateDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
</table>
</td>
</tr>
<!--/更新日-->
<!--名称-->
<tr>
<td width="23%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><b class="text12">名称</b></td>
</tr>
</table>
</td>
<td width="77%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><input type="text" size="40" maxlength="30" class="text12" name="folderName" style="width:215px;" value="<c:out value="${folder.folderName}" />"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td><span class="text12">※全角15文字まで</span></td>
</tr>
</table>
</td>
</tr>
<!--/名称-->
<!--コメント-->
<tr valign="top">
<td width="23%" bgcolor="#E1E6EB">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><b class="text12">コメント</b></td>
</tr>
</table>
</td>
<td width="77%" bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><textarea rows="4" cols="40" name="comment" style="width:400px;"><c:out value="${folder.comment}" /></textarea></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td><span class="text12">※全角50文字まで</span></td>
</tr>
</table>
</td>
</tr>
<!--/コメント-->
</table>
</td>
</tr>
</table>
<!--/編集-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="550">
<tr valign="top">
<td width="548" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onClick="window.close()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;登録して閉じる&nbsp;" class="text12" style="width:120px;" onClick="submitForm()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="550">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、保存せずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->

<!--FOOTER-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="635">
<tr valign="top">
<td width="635" align="right"><img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network"><br></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br>
<!--/FOOTER-->
</form>
</body>
</html>

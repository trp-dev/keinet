<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<c:if test="${ CountingDivBean.countingDivListSize > 1 }">
  <c:forEach var="countingDiv" items="${CountingDivBean.countingDivList}">
    <c:if test="${ countingDiv.countingDivCode == form.countingDivCode }">
      <div style="margin-top:4px;">
      <table border="0" cellpadding="0" cellspacing="0" width="217">
      <tr valign="top">
      <td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
      <td width="202">
      <b class="text14" style="color:#FFFFFF;">現在の集計区分は、</b><br>
      <div style="margin-top:4px;">
      <table border="0" cellpadding="0" cellspacing="0" width="202">
      <tr>
      <td width="156" height="27" bgcolor="#EFF2F3" align="center">
      <!--区分--><b class="text14"><c:out value="${countingDiv.countingDivCode}" />　<c:out value="${countingDiv.countingDivName}" /></b>
      </td>
      <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
      <td width="40"><b class="text14" style="color:#FFFFFF;">です。</b></td>
      </tr>
      </table>
      </div>
      <div style="margin-top:10px;">
      <table border="0" cellpadding="0" cellspacing="0" width="202">
      <tr>
      <td width="202"><font class="text12" color="#FFFFFF">集計区分をまたいでのプロファイル利用はできませんので、ご注意下さい。</font>
      </td>
      </tr>
      </table>
      </div>
      <!--spacer-->
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr valign="top">
      <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
      </tr>
      </table>
      <!--/spacer-->
      </td>
      <td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
      </tr>
      </table>
      </div>
    </c:if>
  </c:forEach>
</c:if>

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

	// イメージオブジェクト
	var folderOpen = new Image();
	folderOpen.src = "https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_open.gif";
	var folderClose = new Image()
	folderClose.src = "https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/icon_folder_close.gif";
	var arrowWhite  = new Image();
	arrowWhite.src  = "https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_white.gif";
	var arrowDarkblue  = new Image();
	arrowDarkblue.src  = "https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/shared_lib/img/parts/arrow_right_darkblue.gif";

	// カレントフォルダ
	var current = "";
	// レイヤ操作クラス
	var sw = new SwitchLayer();

	function changeFolder(id) {
		// カレントフォルダが同じなら何もしない
		if (id == current) return;
		// 新しいカレントフォルダ
		document.getElementById("COLOR1-" + id).style.backgroundColor = "#FFAD8C";
		document.getElementById("COLOR2-" + id).style.backgroundColor = "#FFAD8C";
		document.images["FOLDER-" + id].src = folderOpen.src;
		document.images["ARROW-" + id].src = arrowWhite.src;
		sw.showLayer("PL-" + id);
		// 古いカレントフォルダ
		if (current != "") {
			document.getElementById("COLOR1-" + current).style.backgroundColor = "#EBEFF2";
			document.getElementById("COLOR2-" + current).style.backgroundColor = "#FFFFFF";
			document.images["FOLDER-" + current].src = folderClose.src;
			document.images["ARROW-" + current].src = arrowDarkblue.src;
			sw.hideLayer("PL-" + current);
		}
		// カレントフォルダを変更
		current = id;
	}

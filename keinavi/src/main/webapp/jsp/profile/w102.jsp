<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／プロファイル保存</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript" src="./js/JString.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>

	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var printDialogPattern = "";
	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

	function submitSave() {
		var message = "";

		// 入力チェック（プロファイル名）
		if (document.forms[0].profileName.value == "") {
			message += "<kn:message id="w018a" />\n";
		}

		var js = new JString();

		// 入力チェック（プロファイル名の文字数）
		js.setString(document.forms[0].profileName.value);
		if (js.getLength() > 30) {
			message += "プロファイル名は全角15文字までです。";
		}

		// 入力チェック（コメントの文字数）
		js.setString(document.forms[0].comment.value);
		if (js.getLength() > 100) {
			message += "<kn:message id="w019a" />";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		document.forms[0].forward.value = "w103";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

	// フォルダ編集ウィンドウを開く
	function openFolderEditor() {
		<%@ include file="/jsp/script/count_folder.jsp" %>

		var url = "<c:url value="ProfileFolderEdit" />";
		// 新規なのでフォルダIDはない
		url += "?folderID=" + "";
		url += "&forward=w009";
		url += "&backward=<c:out value="${ param.forward }" />";
		url += "&countingDivCode=<c:out value="${Profile.bundleCD}" />";
		openWindow(url,  680);
	}

	function submitClosed() {
		document.forms[0].forward.value = "w102";
		document.forms[0].backward.value = "<c:out value="${param.backward}" />";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

	function submitBack() {
		document.forms[0].forward.value = "<c:out value="${param.backward}" />";
		document.forms[0].current.value = current;
		document.forms[0].submit();
	}

<%-- プロファイルフォルダ操作用JavaScript --%>
<%@ include file="/jsp/profile/profile.jsp" %>

	function init() {
		startTimer();
		changeFolder("<c:out value="${Profile.folderID}" />");
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileSaveAlias" />" method="POST" onSubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="w102">
<input type="hidden" name="origin" value="<c:out value="${form.origin}" />">
<input type="hidden" name="exit" value="<c:out value="${param.exit}" />">
<input type="hidden" name="current" value="">
<input type="hidden" name="targetYear" value="<c:out value="${form.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${form.targetExam}" />">
<input type="hidden" name="targetPersonId" value="<c:out value="${param.targetPersonId}" />">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">
<input type="hidden" name="tabId" value="<c:out value="${ param.tabId }" />">
<input type="hidden" name="copyFlag" value="<c:out value="${ param.copyFlag }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908"><img src="./profile/img/ttl_profile_hozon.gif" width="188" height="24" border="0" alt="プロファイル保存"><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="3" width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="159"><img src="./shared_lib/img/parts/sp.gif" width="159" height="1" border="0" alt=""><br></td>
<td width="610" align="center">

<!--ケイコさん-->
<table border="0" cellpadding="0" cellspacing="0" width="460">
<tr valign="top">
<td width="56"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="404">

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="390" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="390">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="365"><span class="text12"><b>別名保存</b><br>
現在の設定状況を別名で保存します。<br>プロファイル名を変更し、保存先フォルダを選択してください。</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ケイコさん-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="600">
<!--プロファイル作成日-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル作成日</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><fmt:formatDate value="${now}" pattern="yyyy/MM/dd" /></span></td>
</tr>
<!--/プロファイル作成日-->
<!--プロファイル名-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル名</b></td>
<td width="450" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="text" size="40" class="text12" name="profileName" value="<c:out value="${Profile.profileName}" />"></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><span class="text12">※全角15文字まで</span></td>
</tr>
</table>

</td>
</tr>
<!--/プロファイル名-->
<!--コメント-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">コメント</b></td>
<td width="450" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><textarea rows="4" cols="40" name="comment" style="width:400px;"><c:out value="${Profile.comment}" /></textarea></td>
</tr>
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td><span class="text12">※全角50文字まで</span></td>
</tr>
</table>

</td>
</tr>
<!--/コメント-->
<!--保存先フォルダ-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">保存先フォルダ</b></td>
<td width="450" bgcolor="#FFFFFF">

<!--フォルダリスト-->
<table border="0" cellpadding="0" cellspacing="0" width="450">
<tr valign="top">
<td width="18"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→" vspace="5"><br></td>
<td width="217" bgcolor="#EBEFF2">
<!--左リスト-->
<table border="0" cellpadding="0" cellspacing="0" width="217">

<!--タイトル-->
<tr>
<td width="215"><img src="./shared_lib/img/parts/ttl_folder_list.gif" width="215" height="25" border="0" alt="フォルダリスト"><br></td>
<td width="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
</tr>
<!--/タイトル-->
<tr>
<td colspan="2" width="217" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
</tr>

<c:forEach var="folder" items="${ProfileFolderBean.folderList}">
  <tr>
  <td width="215" bgcolor="#EBEFF2" id="COLOR1-<c:out value="${folder.folderID}" />">
  <table border="0" cellpadding="0" cellspacing="0" width="215">
  <tr>
  <td width="22" align="right"><!--フォルダアイコン--><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><img src="./shared_lib/img/parts/icon_folder_close.gif" width="13" height="16" border="0" name="FOLDER-<c:out value="${folder.folderID}" />"></a></td>
  <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
  <td width="172"><!--フォルダ名--><span class="text12"><a href="javascript:changeFolder('<c:out value="${folder.folderID}" />')" title="<c:out value="${folder.comment}" />"><c:out value="${folder.folderName}" /></a></span></td>
  <td width="15"><img src="./shared_lib/img/parts/arrow_right_darkblue.gif" width="7" height="9" border="→" name="ARROW-<c:out value="${folder.folderID}" />"></td>
  </tr>
  </table>
  </td>
  <td width="2" bgcolor="#FFFFFF" id="COLOR2-<c:out value="${folder.folderID}" />"><img src="./shared_lib/img/parts/sp.gif" width="2" height="25" border="0" alt=""><br></td>
  </tr>
  <tr>
  <td colspan="2" width="217" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="217" height="2" border="0" alt=""><br></td>
  </tr>
</c:forEach>

</table>
<!--/左リスト-->

</td>
<td width="215" bgcolor="#FFAD8C">

<!--右リスト-->
<table border="0" cellpadding="5" cellspacing="0" width="215">
<tr>
<td width="215" bgcolor="#FFAD8C" align="center">

<c:forEach var="folder" items="${ProfileFolderBean.folderList}">
  <div id="PL-<c:out value="${folder.folderID}" />" style="position:absolute;visibility:hidden;top:0px;left:0px">
  <c:forEach var="profile" items="${folder.profileList}">
    <table border="0" cellpadding="0" cellspacing="0" width="197">
    <tr valign="top">
    <td width="197" bgcolor="#FFAD8C">
    <table border="0" cellpadding="0" cellspacing="1" width="197">
    <tr height="40" bgcolor="#FFFFFF">
    <td width="195">
    <table border="0" cellpadding="0" cellspacing="0" width="195">
    <tr>
    <td width="19" align="right"><!--ファイルアイコン--><img src="./shared_lib/img/parts/icon_file.gif" width="11" height="13" border="0" alt="<c:out value="${profile.comment}" />"></td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    <td width="164"><span class="text12" title="<c:out value="${profile.comment}" />"><c:out value="${profile.profileName}" /><c:if test="${ MenuSecurity.menu['801'] }"><br>（<c:out value="${profile.userName}" />）</c:if></span></td>
    <td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
  </c:forEach>
  </div>
</c:forEach>

</td>
</tr>
</table>
<!--/右リスト-->
</td>
</tr>
</table>
<!--/フォルダリスト-->

<!--ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="450">
<tr valign="top">
<td width="450" align="right"><input type="button" value="&nbsp;新規フォルダ作成&nbsp;" class="text12" style="width:130px;" onClick="openFolderEditor()"></td>
</tr>
</table>
</div>
<!--/ボタン-->
</td>
</tr>
<!--/保存先フォルダ-->
</table>
</td>
</tr>
</table>
<!--/概要-->


<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;キャンセル&nbsp;" class="text12" style="width:120px;" onClick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;保存&nbsp;" class="text12" style="width:120px;" onClick="submitSave()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td align="center"><span class="text12">※キャンセルを押すと、保存せずに前のページに戻ります。</span></td>
</tr>
</table>
</div>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="199"><img src="./shared_lib/img/parts/sp.gif" width="199" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>

<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

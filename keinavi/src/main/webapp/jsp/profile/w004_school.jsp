<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!-- 比較対象高校-->
<%-- 背景色 --%>
<c:set var="color" value="#EDEFF0" />
<c:forEach var="function" items="${form.function}">
  <c:if test="${ function == 'school' }"><c:set var="color" value="#FFFFFF" /></c:if>
  <c:if test="${ function == 'text' }"><c:set var="color" value="#FFFFFF" /></c:if>
</c:forEach>
<table border="0" cellpadding="0" cellspacing="0" width="304">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="304">
<tr valign="top">
<td width="302" bgcolor="<c:out value="${color}" />" align="center">
<!--上の段-->
<table border="0" cellpadding="0" cellspacing="5" width="296">
<tr>
<td><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校" hspace="5" align="absmiddle"><b class="text14">比較対象高校</b></td>
<td align="right"><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:70px;" onClick="openCommon(6)"></td>
</tr>
</table>
<!--/上の段-->
<table border="0" cellpadding="0" cellspacing="0" width="290">
<tr>
<td bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="290" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--下の段-->
<div style="margin-top:3px;">
<table border="0" cellpadding="2" cellspacing="0" width="290">
<tr>
<td height="50" valign="top"><span class="text12-hh"><c:choose><c:when test="${ not LoginSession.sales }">自校と</c:when></c:choose>比較したい高校を選択してください。</span></td>
</tr>
<tr>
<td align="right"><span class="text12-hh">設定状況 ： <a href="javascript:openCommon(6)"><kn:com item="school" /></a></span></td>
</tr>
</table>
</div>
<!--/下の段-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="3" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!-- /比較対象高校-->

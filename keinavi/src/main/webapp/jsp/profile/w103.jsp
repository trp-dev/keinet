<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／プロファイル保存</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_exit_simple.jsp" %>

	// ヘルプ・問い合わせ
	<%@ include file="/jsp/script/submit_help.jsp" %>

	function _submitExit() {
		document.forms[0].forward.value = "logout";
		document.forms[0].submit();
	}

	function submitForm() {
		<c:choose>
		<c:when test="${ param.exit == 'logout' }">
		<%-- 2019/08/17 QQ)Nishiyama 共通テスト対応 UPD START --%>
		//	document.forms[0].forward.value = "sweeper";
		//	document.forms[0].submit();
		//	setTimeout("_submitExit()", 1000 * 4);
		_submitExit();
		<%-- 2019/08/17 QQ)Nishiyama 共通テスト対応 UPD END   --%>
		</c:when>
		<c:otherwise>
			document.forms[0].submit();
		</c:otherwise>
		</c:choose>

	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>
<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="ProfileSaveComplete" />" method="POST">
<input type="hidden" name="forward" value="<c:out value="${param.exit}" />">
<input type="hidden" name="backward" value="w103">
<input type="hidden" name="targetYear" value="<c:out value="${param.targetYear}" />">
<input type="hidden" name="targetExam" value="<c:out value="${param.targetExam}" />">
<input type="hidden" name="tabId" value="<c:out value="${ param.tabId }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header01.jsp" %>
<!--/HEADER-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="968" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->

<!--タイトル-->
<div style="margin-top:23px;">
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908"><img src="./profile/img/ttl_profile_hozon.gif" width="188" height="24" border="0" alt="プロファイル保存"><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="3" width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="10" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="908" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="908" height="1" border="0" alt=""><br></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="159"><img src="./shared_lib/img/parts/sp.gif" width="159" height="1" border="0" alt=""><br></td>
<td width="610" align="center">

<!--ケイコさん-->
<table border="0" cellpadding="0" cellspacing="0" width="460">
<tr valign="top">
<td width="56"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="404">

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="390" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="390">
<tr>
<td width="15"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="365"><span class="text12"><b>保存完了</b><br>
保存名と保存場所を確認して、「完了」ボタンを押せば新規プロファイルの作成は完了します。</span></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="404">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="380" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="380" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="380" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="380" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ケイコさん-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--確認-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="10" cellspacing="1" width="600">
<!--プロファイル作成日-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル作成日</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><fmt:formatDate value="${Profile.createDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
<!--/プロファイル作成日-->
<!--プロファイル名-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">プロファイル名</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.profileName}" /></span></td>
</tr>
<!--/プロファイル名-->
<!--コメント-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">コメント</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><kn:pre><c:out value="${Profile.comment}" /></kn:pre></span></td>
</tr>
<!--/コメント-->
<!--保存先フォルダ-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">保存先フォルダ</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.folderName}" /></span></td>
</tr>
<!--/保存先フォルダ-->
<c:if test="${ MenuSecurity.menu['801'] }">
<!--作成者-->
<tr valign="top">
<td width="147" bgcolor="#E5EEF3"><b class="text12">作成者</b></td>
<td width="450" bgcolor="#FFFFFF"><span class="text12"><c:out value="${Profile.userName}" /></span></td>
</tr>
<!--/作成者-->
</c:if>
</table>
</td>
</tr>
</table>
<!--/確認-->


<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td width="600" align="center">
<b class="text16-hh">
<c:choose>
<c:when test="${ param.exit == 'w002' }">
	設定が保存されました。<br>「完了」ボタンを押してプロファイル選択画面へ
</c:when>
<c:when test="${ param.exit == 'w008' }">
	設定が保存されました。<br>「完了」ボタンを押して専用トップページへ
</c:when>
<c:when test="${ param.exit == 'logout' }">
	設定が保存されました。<br>「完了」ボタンを押してログイン画面へ
</c:when>
</c:choose>
</b>
</td>
</tr>
</table>
</div>

<!--ボタン-->
<div style="margin-top:20px;">
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;完了&nbsp;" class="text12" style="width:120px;" onclick="submitForm()">

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="40" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="199"><img src="./shared_lib/img/parts/sp.gif" width="199" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

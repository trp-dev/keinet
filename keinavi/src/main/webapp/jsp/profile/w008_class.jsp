<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--クラス成績分析-->
<table border="0" cellpadding="0" cellspacing="0" width="125">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="125">
<tr valign="top">
<td>
<!--ボタン-->
<table border="0" cellpadding="8" cellspacing="0" width="123">
<tr valign="top">
<td width="123" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;クラス成績分析&nbsp;" class="text12" style="width:107px;" onClick="submitMenu('c001')">

</td>
</tr>
</table>
<!--/ボタン-->
</td>
</tr>
<tr valign="top">
<td width="123" bgcolor="#FFFFFF" align="center">
<div style="margin-top:10px;"><!--イラスト--><img src="./profile/img/icon_class.gif" width="88" height="92" border="0" alt="クラス成績分析"></div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="110">
<tr>
<td bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="110" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->
<div style="margin-top:3px;">
<table border="0" cellpadding="2" cellspacing="0" width="110">
<tr valign="top">
<td height="115"><span class="text12-hh">担当しているクラス内の成績を分布・推移などから分析、または他クラスと比較します。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/クラス成績分析-->

<%@ page contentType="text/html;charset=MS932" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／エラー</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
</head>
<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return false">

<form>

<table border="0" cellspacing="0"cellSpacing="0" cellPadding="0" width="100%">
	<tr>
	<td><img height="15" alt="" src="./shared_lib/img/parts/sp.gif" width="2" border="0"><br></td>
	</tr>
</table>

<!--上部　オレンジライン-->
<table cellspacing="0" cellPadding="0" width="100%" border="0">
	<tr>
	<td><img alt=""  border="0" height="11" width="12" src="./shared_lib/img/parts/sp.gif"></td>
	<td><img alt=""  border="0" height="11" width="12" src="./shared_lib/img/parts/com_cnr_lt.gif"></td>
	<td width="100%" background="./shared_lib/img/parts/com_bk_t.gif"><img alt=""  border="0" height="11" width="12" src="./shared_lib/img/parts/sp.gif"></td>
	<td><img alt=""  border="0" height="11" width="13"src="./shared_lib/img/parts/com_cnr_rt.gif"></td>
	<td><img alt=""  border="0" height="11" width="11" src="./shared_lib/img/parts/sp.gif"></td>
	</tr>
</table>
<!--/上部　オレンジライン-->
<!--MAIN-->
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr valign="top">
	<td><img alt="" border="0" height="400" width="12" src="./shared_lib/img/parts/sp.gif"></td>
	<td background="./shared_lib/img/parts/com_bk_l.gif"><img alt="" border="0" height="2" width="8" src="./shared_lib/img/parts/sp.gif"></td>
	<td align="center" width="100%" bgcolor="#FFFFFF">

		<!--タイトルロゴ-->
		<div style="margin-top:20px">
			<img height="96" alt="Kei-Navi (Kei-Net Step Up Navigator)" src="./profile/img/top_logo.gif" width="467" border="0">
		</div>
		<!--/タイトルロゴ-->

		<div style="margin-top: 30px">
		<table cellspacing="0" cellpadding="0" width="80%" border="0">
		<tr>
		<td bgcolor="#8CA9BB">
			<table cellspacing="1" cellpadding="10" width="100%" border="0">
			<tr>
			<td align="center" bgcolor="#EFF2F3">
				<b class="text14">
				<c:choose>
					<c:when test="${ KNServletException.showMessage }"><c:out value="${ KNServletException.errorMessage }" /></c:when>
					<c:otherwise>ただいま大変混み合っております。しばらくしてから再度操作してください。</c:otherwise>
				</c:choose>
				</b>

				<table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
					<td><img height="7" alt="" src="./shared_lib/img/parts/sp.gif" width="1" border="0"></td>
					</tr>
				</table>

				<c:choose>
					<c:when test="${ KNServletException.close }"><input type="button" value="　閉じる　" onClick="window.opener=null;window.close();" style="width:120px"></c:when>
					<c:otherwise><input type="button" value="　戻　る　" onClick="history.back()" style="width:120px"></c:otherwise>
				</c:choose>
			</td>
			</tr>
			</table>
		</td>
		</tr>
		</table>
	</td>
	<td background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""></td>
	<td><img alt=""  border="0" height="11" width="11" src="./shared_lib/img/parts/sp.gif"></td>
</tr>
</table>
<!--/MAIN-->
<!--下部　ドロップシャドウ-->
<table cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr>
	<td><img height="1" alt="" src="./shared_lib/img/parts/sp.gif" width="12" border="0"></td>
	<td><img height="15" alt="" src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" border="0"></td>
	<td width="100%" background="./shared_lib/img/parts/com_bk_d.gif"><img alt=""  border="0" height="11" width="12" src="./shared_lib/img/parts/sp.gif"></td>
	<td><img height="15" alt="" src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" border="0"></td>
	<td><img alt=""  border="0" height="11" width="11" src="./shared_lib/img/parts/sp.gif"></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<div style="margin-top:7px;width:100%;text-align:center">
<img src="./shared_lib/include/footer/img/copyright.gif" width="380" height="20" border="0" alt="Copyright: Kawaijuku Educational Information Network">
</div>
<!--/FOOTER-->

</form>
</body>
</html>

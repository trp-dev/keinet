<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<a name="top"></a>
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr>
<td width="17" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="17" height="1" border="0" alt=""><br></td>
<td width="195" rowspan="2"><img src="./shared_lib/include/header/img/header_logo.gif" width="195" height="52" border="0" alt="Kei-Navi　Kei-Net Step Up Navigator"><br></td>
<td width="437" align="right" rowspan="2">
	<table border="0" cellpadding="5" cellspacing="0">
	<tr>
	<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
	<td nowrap><span class="text12" style="color:#626261"><c:out value="${LoginSession.userName}" /></span></td>
	<td><b class="text12" style="color:#626261">［<c:out value="${Profile.profileName}" />］専用ページ</b></td>
	<c:if test="${ not iCommonMap.mendanMode }">
	<td nowrap><b class="text14" style="color:#626261">［<a href="javascript:submitChange()" style="color:#FF8F0E;">成績分析開始画面へ</a>］</b></td>
	</c:if>
	</tr>
	</table>
</td>
<td width="15" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="109"><c:if test="${not iCommonMap.mendanMode}"><div style="margin-top:8px;"><a href="javascript:openCommon(0)"><img src="./shared_lib/include/header/img/btn_common.gif" width="107" height="24" border="0" alt="共通項目設定"></a></div></c:if></td>
<td width="138"><c:if test="${not iCommonMap.mendanMode}"><div style="margin-top:8px;"><a href="javascript:submitSave()"><img src="./shared_lib/include/header/img/btn_profile.gif" width="136" height="24" border="0" alt="プロファイル保存"></a></div></c:if></td>
<td width="76"><div style="margin-top:8px;"><a href="javascript:submitExit()"><img src="./shared_lib/include/header/img/btn_end.gif" width="76" height="24" border="0" alt="終了"></a></div></td>
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" align="right"><span class="text10">※終了時は、ブラウザの「×」ではなく、<br>[終了]ボタンをクリックしてください。</span></td>
</tr>
</table>

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td align="center"><img src="./mainte/img/touroku_finish.gif" width="153" height="24" border="0" alt="一括登録完了"><br></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">ファイル名：<c:out value="${ form.fileName }" /></b></td>
</tr>
<tr>
<td align="center"><span class="text14-hh">ファイルの取込が完了しました。下記の取込み結果を確認してください。</span></td>
</tr>
</table>
<!--/大タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="4" cellspacing="2" width="400">
	<tr>
	<td width="50%" bgcolor="#E1E6EB">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
	<td><span class="text12">新規に登録されたデータ</span></td>
	</tr>
	</table>
	</td>
	<td width="50%" bgcolor="#F4E5D6" align="right">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${ CSVRegisterBean.insertCounter }" />件</span></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td width="50%" bgcolor="#E1E6EB">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
	<td><span class="text12">変更処理されたデータ</span></td>
	</tr>
	</table>
	</td>
	<td width="50%" bgcolor="#F4E5D6" align="right">
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td><span class="text12"><c:out value="${ CSVRegisterBean.updateCounter + CSVRegisterBean.deleteCounter }" />件</span></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""></td>
	</tr>
	</table>
	</td>
	</tr>
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<input type="button" value="戻る" class="text12" style="width:120px;" onClick="submitBack()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<%@ page contentType="text/html;charset=MS932" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／印刷</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">

	function submitForm() {
		if (document.forms[0].mode[0].checked) returnValue = 1;
		else returnValue = 2;
		self.close();
	}

</script>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">
<form>
<a name="top"></a>
<!--spacer
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="650"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
-->
<!--/spacer-->
  --%>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ↓ダイアログの定義 -->
<div id="printDialogDiv">
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="200" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="621" bgcolor="#FFFFFF" align="center">


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./print/img/icon_exclam.gif" width="52" height="46" border="0" alt="!" hspace="6"><br></td>
<td><b class="text16" style="color:#FF6E0E">印刷を開始します</b></td>
</tr>
</table>
<!--/タイトル-->

<!--ケイコさん-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="460">
<tr>
<td width="62"><!--イラスト--><img src="./shared_lib/img/illust/woman02.gif" width="52" height="84" border="0" alt="ケイコさん"><br></td>
<td width="398">

<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="374" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="374" height="2" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rt_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="374" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="374" height="5" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr>
<td width="12">
<table border="0" cellpadding="0" cellspacing="0" width="12">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="60" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="2" width="12"><img src="./shared_lib/img/parts/wb_arrow_l_bp.gif" width="12" height="14" border="0" alt=""><br></td>
</tr>
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="60" border="0" alt=""><br></td>
</tr>
</table>
</td>
<td width="384" bgcolor="#F9EEE5">
<!--文言-->
<table border="0" cellpadding="0" cellspacing="0" width="384">
<tr>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
<td width="324"><span class="text14-hh">現在の設定内容で印刷を開始します。<br>
よく内容をご確認の上、印刷をして下さい。<br>
※印刷・出力に際して「セキュリティの警告」としてExcelマクロに関する警告が表示されたら、「マクロを有効にする」を選んでください。詳しくは「ヘルプ−帳票について」をご参照ください。</span></td>
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="15" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/文言-->
</td>
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="398">
<tr valign="top">
<td width="10" rowspan="2"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_lb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
<td width="374" bgcolor="#F9EEE5"><img src="./shared_lib/img/parts/sp.gif" width="374" height="5" border="0" alt=""><br></td>
<td width="7" rowspan="2"><img src="./shared_lib/img/parts/wb_rb_bp.gif" width="7" height="7" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="374" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="374" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/ケイコさん-->

<!--印刷-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="radio" name="mode" value="" checked></td>
  --%>
<td><input type="radio" name="mode" value="1" checked></td>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>

<td><span class="text12">印刷と同時に保存</span></td>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td rowspan="2"><a href="#" onClick="submitForm()"><img src="./shared_lib/img/btn/print_begin.gif" width="121" height="35" border="0" alt="印刷開始" hspace="10"></a><br></td>
  --%>
<td rowspan="2"><a onClick="printDialogClose(0);"><img src="./shared_lib/img/btn/print_begin.gif" width="121" height="35" border="0" alt="印刷開始" hspace="10"></a><br></td>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>

</tr>
<tr>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="radio" name="mode" value=""></td>
  --%>
<td><input type="radio" name="mode" value="2"></td>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>

<td><span class="text12">印刷のみ</span></td>
</tr>
</table>
</div>
<!--/印刷-->

<!--ボーダー-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="5" width="570">
<tr valign="top">
<td width="570" bgcolor="#CDD7DD"><img src="./shared_lib/img/parts/sp.gif" width="570" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--/ボーダー-->

<!--ボタン-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="button" value="キャンセル" class="text12" style="width:120px;" onClick="returnValue=3;self.close()"></td>
  --%>
<td><input type="button" value="キャンセル" class="text12" style="width:120px;" onClick="printDialogClose(9);"></td>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 UPD END   --%>

</tr>
</table>
</div>
<!--/ボタン-->

<!--注意-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">※キャンセルを押すと、保存も印刷もせずにウィンドウを閉じます。</span></td>
</tr>
</table>
</div>
<!--/注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="613" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
</div>
<!-- ↑ダイアログの定義 -->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>

<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<!--/下部　ドロップシャドウ-->
<!--FOOTER-->
<!--# include virtual="./shared_lib/include/footer/footer_sw.html"-->
<!--/FOOTER-->
</form>
</body>
</html>
  --%>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 DEL END   --%>

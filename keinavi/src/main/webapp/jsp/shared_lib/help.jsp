<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="2" width="968">
<tr valign="top">
<td width="964" bgcolor="#EDEFF0">

<table border="0" cellpadding="0" cellspacing="0" width="964">
<tr>
<td width="374">
<!--パンくず-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td>
<span class="text12">

<%-- 面談モード時は出さない --%>
<c:if test="${ not iCommonMap.mendanMode }">
<c:choose>
  <c:when test="${category == 'school'}">
    <a href="javascript:submitMenu('w008')">HOME</a> ＞
    <c:forTokens var="screen" items="s001,s101,s201,s301,s401" delims=",">
      <c:if test="${ param.forward == screen }">校内成績分析</c:if>
    </c:forTokens>
    <c:forTokens var="screen" items="b001,b101" delims=",">
      <c:if test="${ param.forward == screen }">高校成績分析</c:if>
    </c:forTokens>
    <c:forTokens var="screen" items="s002,s003,s004,s102,s103,s104,s202,s203,s204,s205,s302,s303,s304,s305,s402,s403" delims=",">
      <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('s001')">校内成績分析</a> ＞</c:if>
    </c:forTokens>
    <c:forTokens var="screen" items="b002,b003,b004,b005,b102,b103" delims=",">
      <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('b001')">高校成績分析</a> ＞</c:if>
    </c:forTokens>
    <c:if test="${ param.forward == 's002' }">校内成績（成績概況）</c:if>
    <c:if test="${ param.forward == 's003' }">校内成績（偏差値分布）</c:if>
    <c:if test="${ param.forward == 's004' }">校内成績（設問別成績）</c:if>
    <c:if test="${ param.forward == 's102' }">過年度比較（成績概況）</c:if>
    <c:if test="${ param.forward == 's103' }">過年度比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 's104' }">過年度比較（志望大学評価別人数）</c:if>
    <c:if test="${ param.forward == 's202' }">クラス比較（成績概況）</c:if>
    <c:if test="${ param.forward == 's203' }">クラス比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 's204' }">クラス比較（設問別成績）</c:if>
    <c:if test="${ param.forward == 's205' }">クラス比較（志望大学評価別人数）</c:if>
    <c:if test="${ param.forward == 's302' }">他校比較（成績概況）</c:if>
    <c:if test="${ param.forward == 's303' }">他校比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 's304' }">他校比較（設問別成績）</c:if>
    <c:if test="${ param.forward == 's305' }">他校比較（志望大学評価別人数）</c:if>
    <c:if test="${ param.forward == 's402' }">過回比較（成績概況）</c:if>
    <c:if test="${ param.forward == 's403' }">過回比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 'b002' }">高校間比較（成績概況）</c:if>
    <c:if test="${ param.forward == 'b003' }">高校間比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 'b004' }">高校間比較（設問別成績）</c:if>
    <c:if test="${ param.forward == 'b005' }">高校間比較（志望大学評価別人数）</c:if>
    <c:if test="${ param.forward == 'b102' }">過回比較（成績概況）</c:if>
    <c:if test="${ param.forward == 'b103' }">過回比較（偏差値分布）</c:if>
  </c:when>
  <c:when test="${category == 'class'}">
    <a href="javascript:submitMenu('w008')">HOME</a> ＞
    <c:forTokens var="screen" items="c001,c101" delims=",">
      <c:if test="${ param.forward == screen }">クラス成績分析</c:if>
    </c:forTokens>
    <c:forTokens var="screen" items="c002,c003,c004,c005,c006,c102,c103,c104,c105" delims=",">
      <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('c001')">クラス成績分析</a> ＞</c:if>
    </c:forTokens>
    <c:if test="${ param.forward == 'c002' }">クラス成績概況（偏差値分布）</c:if>
    <c:if test="${ param.forward == 'c003' }">クラス成績概況（個人成績）</c:if>
    <c:if test="${ param.forward == 'c004' }">クラス成績概況（個人成績推移）</c:if>
    <c:if test="${ param.forward == 'c005' }">クラス成績概況（設問別個人成績）</c:if>
    <c:if test="${ param.forward == 'c006' }">クラス成績概況（志望大学別個人評価）</c:if>
    <c:if test="${ param.forward == 'c102' }">クラス比較（成績概況）</c:if>
    <c:if test="${ param.forward == 'c103' }">クラス比較（偏差値分布）</c:if>
    <c:if test="${ param.forward == 'c104' }">クラス比較（設問別成績）</c:if>
    <c:if test="${ param.forward == 'c105' }">クラス比較（志望大学評価別人数）</c:if>
  </c:when>
  <c:when test="${category == 'individual'}"><a href="javascript:submitMenu('w008')">HOME</a> ＞ 個人成績分析</c:when>
  <c:when test="${category == 'text'}">
    <c:forTokens var="screen" items="t001,t101,t201" delims=",">
      <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('w008')">HOME</a> ＞ テキスト出力</c:if>
    </c:forTokens>
    <c:forTokens var="screen" items="t002,t003,t004,t005,t102,t103,t104,t106,t202" delims=",">
      <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('w008')">HOME</a> ＞
      		<a href="javascript:submitMenu('t001')">テキスト出力</a> ＞
      </c:if>
    </c:forTokens>
    <c:if test="${ param.forward == screen }"><a href="javascript:submitMenu('w008')">HOME</a> ＞</c:if>
    <c:if test="${ param.forward == 't002' }">集計データ（成績概況）</c:if>
    <c:if test="${ param.forward == 't003' }">集計データ（偏差値分布）</c:if>
    <c:if test="${ param.forward == 't004' }">集計データ（設問別成績）</c:if>
    <c:if test="${ param.forward == 't005' }">集計データ（志望大学評価別人数）</c:if>
    <c:if test="${ param.forward == 't103' }">個人データ（設問別成績）</c:if>
    <c:if test="${ param.forward == 't104' }">個人データ（模試別成績データ）</c:if>
    <c:if test="${ param.forward == 't106' }">個人データ（学力要素別成績データ）</c:if>
    <c:if test="${ param.forward == 't202' }">入試結果調査票データ</c:if>
  </c:when>
  <c:otherwise>HOME</c:otherwise>
</c:choose>
</c:if>

</span>
</td>
</tr>
</table>
<!--/パンくず-->
</td>
<td align="right">
<!--ヘルプ-->
<table border="0" cellpadding="0" cellspacing="0">
<tr>

<kn:hasKwebLink>
<td><a href="javascript:submitExLink('k-web');"><img src="./shared_lib/include/content/img/icon_kounai.gif" width="16" height="16" border="0" alt="答案閲覧システム" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:submitExLink('k-web');">答案閲覧システム</a></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</kn:hasKwebLink>

<%-- 2015/12/22 QQ)Nishiyama 大規模改修 DEL START --%>
<%--
<c:if test="${ not LoginSession.trial }">
<td><a href="https://navi.keinet.ne.jp/kounai/Login.html" target="_blank"><img src="./shared_lib/include/content/img/icon_kounai.gif" width="16" height="16" border="0" alt="校内成績処理システム" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="https://navi.keinet.ne.jp/kounai/Login.html" target="_blank">校内成績処理システム</a></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</c:if>
  --%>
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 DEL END   --%>

<td><a href="javascript:openHelp('Inquire')"><img src="./shared_lib/include/content/img/icon_inq.gif" width="16" height="16" border="0" alt="お問い合わせ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:openHelp('Inquire')">お問い合わせ</a></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>

<!--一時的にマニュアル・ヘルプを閉鎖2004.11.22 sugishita-->
<td><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();"><img src="./shared_lib/include/content/img/icon_manual.gif" width="16" height="16" border="0" alt="マニュアル" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();">マニュアル</a></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>

<!--
<td><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();"><img src="./shared_lib/include/content/img/icon_help.gif" width="17" height="16" border="0" alt="ヘルプ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();">ヘルプ</a></span></td>
-->

<!-- 一時的にヘルプを閉鎖2004.11.22 sugishita => 2005.05.24 open -->
<td><a href="javascript:openHelp('HelpTop')"><img src="./shared_lib/include/content/img/icon_help.gif" width="17" height="16" border="0" alt="ヘルプ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:openHelp('HelpTop')">ヘルプ</a></span></td>

<td><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/ヘルプ-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
<tr>
<td bgcolor="#BABABA"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
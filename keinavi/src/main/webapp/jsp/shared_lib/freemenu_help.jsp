<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--ヘルプナビ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="2" width="968">
<tr valign="top">
<td width="964" bgcolor="#EDEFF0" align="right">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<c:if test="${ not empty LoginSession }">
<td><a href="javascript:openHelp('Inquire')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/icon_inq.gif" width="16" height="16" border="0" alt="お問い合わせ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:openHelp('Inquire')">お問い合わせ</a></span></td>
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>

<!-- 一時的にマニュアル・ヘルプを閉鎖2004.11.22 sugishita-->
<td><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/icon_manual.gif" width="16" height="16" border="0" alt="マニュアル" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();">マニュアル</a></span></td>
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>

<!--
<td><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/icon_help.gif" width="17" height="16" border="0" alt="ヘルプ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:window.open('underconstruction.html', '_blank', 'resizable=yes,scrollbars=yes,status=yes,titlebar=yes,width=800,height=600');function noop(){} noop();">ヘルプ</a></span></td>
-->

<!-- 一時的にヘルプを閉鎖2004.11.22 sugishita => 2005.05.24 open -->
<td><a href="javascript:openHelp('HelpTop')"><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/icon_help.gif" width="17" height="16" border="0" alt="ヘルプ" hspace="3" vspace="1"></a></td>
<td><span class="text12"><a href="javascript:openHelp('HelpTop')">ヘルプ</a></span></td>
</c:if>
<td><img src="https://kjp-pm.oo.kawai-juku.ac.jp/hantei/app/freemenu/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
</table>


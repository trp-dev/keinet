<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="10">
<tr>
<td align="center"><img src="./mainte/img/touroku_error.gif" width="177" height="25" border="0" alt="一括登録エラー"><br></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">ファイル名：<c:out value="${ form.fileName }" /></b></td>
</tr>
<tr>
<td align="center"><b class="text14-hh">[エラー発生件数　<font color="#FF6E0E"><c:out value="${ CSVRegisterBean.errorSize }" /></font>件]</b></td>
</tr>
<tr>
<td align="center"><span class="text14-hh">取込みファイルの内容にエラーが含まれていたため、ファイル取込みに失敗しました。<br>エラー内容をご確認の上、取込みファイルを修正し再度登録処理を行ってください。</span></td>
</tr>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="戻る" class="text12" style="width:120px;" onClick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="エラー内容を印刷する" class="text12" style="width:180px;" onclick="window.print()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--リスト-->
<table border="0" cellpadding="4" cellspacing="2" width="604">
<!--項目-->
<tr bgcolor="#CDD7DD">
<td width="7%" align="center"><span class="text12">行数</span></td>
<td width="93%" align="center"><span class="text12">エラー内容</span></td>
</tr>
<!--/項目-->
<!--set-->
<c:forEach var="record" items="${ CSVRegisterBean.lineRecordList }">
<c:forEach var="error" items="${ record.errorDataList }">
<tr>
<td bgcolor="#E1E6EB" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${ error.lineNo }" /></span></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""></td>
<td><span class="text12"><c:out value="${ error.errorContent }" /></span></td>
</tr>
</table>
</td>
</tr>
</c:forEach>
</c:forEach>
<!--/set-->
</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="600">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="600">
<tr valign="top">
<td width="598" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="戻る" class="text12" style="width:120px;" onClick="submitBack()"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="エラー内容を印刷する" class="text12" style="width:180px;" onclick="window.print()"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

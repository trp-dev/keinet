<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set value="text" var="category" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／テキスト出力</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<%-- 2015/12/22 QQ)Nishiyama 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_top.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
	<%@ include file="/jsp/script/open_charge.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var printDialogPattern = "";
	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

    // 画面表示段階でのチェック数
	var count = 0;
	<c:if test="${ Profile.categoryMap['050201']['1301'] == 1 }">count++;</c:if>
	<c:if test="${ Profile.categoryMap['050202']['1301'] == 1 }">count++;</c:if>
	<c:if test="${ Profile.categoryMap['050203']['1301'] == 1 }">count++;</c:if>
	<c:if test="${ Profile.categoryMap['050204']['1301'] == 1 }">count++;</c:if>
	<%-- 個人データは権限があるなら対象となる --%>
	<c:if test="${ MenuSecurity.menu['504'] }">
		<c:if test="${ Profile.categoryMap['050302']['1301'] == 1 }">count++;</c:if>
		<c:if test="${ Profile.categoryMap['050303']['1301'] == 1 }">count++;</c:if>
		<c:if test="${ Profile.categoryMap['050304']['1301'] == 1 }">count++;</c:if>
	</c:if>

	// チェックを加えたかどうか
	var flag = false;

	function change(obj) {
		// 変更フラグ
		document.forms[0].changed.value = "1";

		// カウントを増減
		if (obj.checked) {
			count++;
			flag = true;
		} else {
			count--;
		}
	}

	// 入力チェック
	function validate() {

		if (count == 0) {
			alert("<kn:message id="s001a" />");
			return false;
		}

		if (flag) {
			alert("設定完了ボタンを押してください。");
			return false;
		}

		var message = "";

		<%-- 担当クラス --%>
		<%-- 個人成績・入試結果調査のみチェック --%>
		<c:if test="${ empty ChargeClass.classList && MenuSecurity.menu['504'] }">
			var check = false;
			<c:if test="${ Profile.categoryMap['050302']['1301'] == 1 }">check = true;</c:if>
			<c:if test="${ Profile.categoryMap['050303']['1301'] == 1 }">check = true;</c:if>
			<c:if test="${ Profile.categoryMap['050304']['1301'] == 1 }">check = true;</c:if>

			if (check) {
				message += "<kn:message id="c001a" />\n";
			}
		</c:if>

		<%-- 比較対象高校 --%>
		if ("<c:out value="${ComSetStatusBean.statusMap['school']}" />" == "02") {
			message += "<kn:message id="s007a" />";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		return true;
	}

	function init() {
		startTimer();
		// スクロール位置を初期化する
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
	}

// -->
</script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="TextTop" />" method="POST">
<input type="hidden" name="out" value="0">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<input type="hidden" name="exit" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="printFlag" value="">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--コンテンツ-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="30"><img src="./shared_lib/img/parts/sp.gif" width="30" height="1" border="0" alt=""><br></td>
<td width="590">
<!--●●●左側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="21" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="149"><img src="./jsp/txt_out/img/ttl_text.gif" width="140" height="23" border="0" alt="テキスト出力"><br></td>
<td width="1" bgcolor="#657681"><img src="./shared_lib/img/parts/sp.gif" width="1" height="35" border="0" alt=""><br></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="430"><img src="./jsp/txt_out/img/ttl_text_read.gif" width="357" height="16" border="0" alt="必要なデータを編集してファイルを一括出力します。"><br></td>
</tr>
</table>
<!--/タイトル-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--コンテンツメニュー-->
<table border="0" cellpadding="0" cellspacing="0" width="590" height="38">
<tr>
<%-- 集計データ --%>
<c:choose>
	<c:when test="${param.forward == 't001'}">
		<td width="114"><img src="./shared_lib/img/btn/cur/txt_menu_01.gif" width="114" height="38" border="0" alt="集計データ"></td>
	</c:when>
	<c:otherwise>
		<td width="114"><a href="javascript:submitMenu('t001')"><img src="./shared_lib/img/btn/def/txt_menu_01.gif" width="114" height="38" border="0" alt="集計データ"></a></td>
	</c:otherwise>
</c:choose>
<td width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
<%-- 個人データ --%>
<c:if test="${MenuSecurity.menu['504']}">
	<c:choose>
		<c:when test="${param.forward == 't101'}">
			<td width="114"><img src="./shared_lib/img/btn/cur/txt_menu_02.gif" width="114" height="38" border="0" alt="個人データ"></td>
			<td width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:when>
		<c:otherwise>
			<td width="114"><a href="javascript:submitMenu('t101')"><img src="./shared_lib/img/btn/def/txt_menu_02.gif" width="114" height="38" border="0" alt="個人データ"></a></td>
			<td width="5"><img src="./shared_lib/img/parts/contents_menu_span.gif" width="5" height="38" border="0" alt=""></td>
		</c:otherwise>
	</c:choose>
</c:if>
<%-- 入試結果調査 --%>
<c:if test="${MenuSecurity.menu['505']}">
	<c:choose>
		<c:when test="${param.forward == 't201'}">
			<td width="114"><img src="./shared_lib/img/btn/cur/txt_menu_03.gif" width="114" height="38" border="0" alt="入試結果調査票データ"></td>
		</c:when>
		<c:otherwise>
			<td width="114"><a href="javascript:submitMenu('t201')"><img src="./shared_lib/img/btn/def/txt_menu_03.gif" width="114" height="38" border="0" alt="入試結果調査票データ"></a></td>
		</c:otherwise>
	</c:choose>
</c:if>
<td background="./shared_lib/img/parts/contents_menu_span.gif"><img src="./shared_lib/img/parts/sp.gif" width="233" height="38" border="0" alt=""></td>
</tr>
</table>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="588" bgcolor="#EFF2F3" align="center">

<!--説明-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
 <c:choose>
  <c:when test="${param.forward == 't001'}">
	<td><span class="text14">御校全体・クラス単位の集計成績データを出力します。</span></td>
  </c:when>
  <c:when test="${param.forward == 't101'}">
	<td><span class="text14">御校の模試受験生個々人の成績データを出力します。</span></td>
  </c:when>
  <c:when test="${param.forward == 't201'}">
	<td><span class="text14">「個人成績分析」で生徒さんの「受験予定大学」が登録されていれば、これをKei-Netサービスウェア「入試結果調査システム」に取り込めるデータとして出力します。</span></td>
  </c:when>
 </c:choose>
</tr>
</table>
</div>
<!--/説明-->

<c:if test="${ param.forward == 't101' || param.forward == 't201' }">
<!--担当クラス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="2" cellspacing="1" width="560">
<tr>
<td width="558" bgcolor="#FFFFFF">

<table border="0" cellpadding="0" cellspacing="0" width="554">
<tr>
<td width="90" bgcolor="#758A98" align="center"><b class="text12" style="color:#FFFFFF">担当クラス</b></td>
<td width="2" bgcolor="#FFFFFF"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="462" bgcolor="#F4E5D6">
<table border="0" cellpadding="3" cellspacing="0" width="462">
<tr>
<td><span class="text12"><c:choose>
  <c:when test="${ empty ChargeClass.classList }">設定なし</c:when>
  <c:otherwise>
    <c:out value="${ChargeClass.year}" />年度
    <c:forEach var="class" items="${ChargeClass.classList}">
      &nbsp;<c:out value="${class.grade}" />年<c:out value="${class.className}" />
    </c:forEach>
  </c:otherwise>
</c:choose></span></td>
<td align="right"><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:75px;" onClick="openCharge()"></td>
</tr>
</table>
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/担当クラス-->
</c:if>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td width="275">
<!--成績概況-->
<c:choose>
<c:when test="${param.forward == 't001'}">

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">成績概況</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t002" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t002" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="CountScore" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050201']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t002" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t002" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--オプション-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253" height="20" bgcolor="#CDD7DD" align="center">


<table border="0" cellpadding="0" cellspacing="0">
<tr valign="middle">
<td><span class="text12">オプション&nbsp;：</span></td>
<c:if test="${MenuSecurity.menu['502']}">
	<td><input type="checkbox" name="outItem" value="CountScoreSchool" onclick="document.forms[0].changed.value = 1;flag=true" <c:if test="${ Profile.categoryMap['050201']['0403'] == 1 }"> checked</c:if><c:if test="${ isCenterResearch }"> disabled=disabled</c:if>></td>
	<td><span class="text12">他校比較</span></td>
	<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</c:if>
<c:if test="${MenuSecurity.menu['503']}">
	<td><input type="checkbox" name="outItem" value="CountScoreClass" onclick="document.forms[0].changed.value = 1"<c:if test="${ Profile.categoryMap['050201']['0503'] == 1 }"> checked</c:if>></td>
	<td><span class="text12">クラス比較</span></td>
</c:if>
</tr>
</table>


</td>
</tr>
</table>
<!--/オプション-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t002'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t002']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050201']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
	</c:forEach>
	  </c:when>
	</c:choose>

</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t002')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/成績概況-->
</c:when>
<c:when test="${param.forward == 't101'}">
<!--模試別成績データ-->

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">模試別成績データ</b></td>
<td width="105" valign="bottom">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t104" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t104" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="IndExam2" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050303']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t104" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t104" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t104'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t104']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050303']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
		</c:forEach>
	  </c:when>
	</c:choose>

</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t104')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/模試別成績データ-->
</c:when>
<c:when test="${param.forward == 't201'}">
<!--入試結果調査-->
<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="240"><b class="text16" style="color:#657681;">入試結果調査票データ</b></td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t202'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t202']}">
			<option value=""><c:out value="${items[1]}" /></option>
		<%-- 2016/01/12 QQ)Hisakawa 大規模改修 DEL START --%>
		<%-- </c:forTokens> --%>
		<%-- 2016/01/12 QQ)Hisakawa 大規模改修 DEL END   --%>
	</c:forEach>
	  </c:when>
	</c:choose>

</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t202')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/入試結果調査-->
</c:when>
</c:choose>
</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="275">
<!--偏差値分布-->
<c:choose>
<c:when test="${param.forward == 't001'}">

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">偏差値分布</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t003" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t003" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="CountDiv" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050202']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t003" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t003" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--オプション-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253" height="20" bgcolor="#CDD7DD" align="center">



<table border="0" cellpadding="0" cellspacing="0">
<tr valign="middle">
<td><span class="text12">オプション&nbsp;：</span></td>
<c:if test="${MenuSecurity.menu['502']}">
	<td><input type="checkbox" name="outItem" value="CountDivSchool" onclick="document.forms[0].changed.value = 1;flag=true" <c:if test="${ Profile.categoryMap['050202']['0403'] == 1 }"> checked</c:if><c:if test="${ isCenterResearch }"> disabled=disabled</c:if>></td>
	<td><span class="text12">他校比較</span></td>
</c:if>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<c:if test="${MenuSecurity.menu['503']}">
	<td><input type="checkbox" name="outItem" value="CountDivClass" onclick="document.forms[0].changed.value = 1;"<c:if test="${ Profile.categoryMap['050202']['0503'] == 1 }"> checked</c:if>></td>
	<td><span class="text12">クラス比較</span></td>
</c:if>
</tr>
</table>

</td>
</tr>
</table>


<!--/オプション-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t003a'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t003a']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050202']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
	</c:forEach>
	  </c:when>
	</c:choose>
</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t003')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>


<!--/下部ー-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>


</c:when>
</c:choose>
<!--/偏差値分布-->

<!--個人設問別成績-->
<c:choose>
<c:when test="${param.forward == 't101'}">

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">設問別成績</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t103" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t103" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="IndQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050302']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t103" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t103" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t103'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t103']}">
		 <c:set var="proItems" value="${ Profile.categoryMap['050302']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
	</c:forEach>
	  </c:when>
	</c:choose>

</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t103')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</c:when>
</c:choose>
<!--/個人設問別成績-->
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="560">
<tr>
<td width="275">

<!--設問別成績-->
<c:choose>
<c:when test="${param.forward == 't001'}">

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">設問別成績</b></td>
<td width="105" valign="top">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t004" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t004" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="CountQue" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050203']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t004" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t004" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--オプション-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253" height="20" bgcolor="#CDD7DD" align="center">


<table border="0" cellpadding="0" cellspacing="0">
<tr valign="middle">
<td><span class="text12">オプション&nbsp;：</span></td>
<c:if test="${MenuSecurity.menu['502']}">
	<td><input type="checkbox" name="outItem" value="CountQueSchool" onclick="document.forms[0].changed.value = 1;flag=true" <c:if test="${ Profile.categoryMap['050203']['0403'] == 1 }"> checked</c:if><c:if test="${ isCenterResearch }"> disabled=disabled</c:if>></td>
	<td><span class="text12">他校比較</span></td>
</c:if>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<c:if test="${MenuSecurity.menu['503']}">
	<td><input type="checkbox" name="outItem" value="CountQueClass" onclick="document.forms[0].changed.value = 1;"<c:if test="${ Profile.categoryMap['050203']['0503'] == 1 }"> checked</c:if>></td>
	<td><span class="text12">クラス比較</span></td>
</c:if>
</tr>
</table>


</td>
</tr>
</table>
<!--/オプション-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">
	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t004'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t004']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050203']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
	</c:forEach>
	  </c:when>
	</c:choose>
</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t004')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/設問別成績-->
</c:when>
<c:when test="${param.forward == 't101'}">
<%--模試別成績データ（旧形式）廃止--%>
<!--（記述系模試）学力要素別成績データ-->
<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">（記述系模試）<br>学力要素別成績<br>データ</b></td>
<td width="105" valign="bottom">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t106_top" out="!color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t106_top" out="!image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t106_top" out="!color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="IndDescExam" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050304']['1301'] == 1 }"> checked</c:if><c:if test="${ !isWrtnExam2 and !isPStage }"> disabled</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t106_top" out="!image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t106_top" out="!color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t106_top" out="!image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t106_top" out="!image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">

	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t106'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t106']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050304']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
		</c:forEach>
	  </c:when>
	</c:choose>

</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>

		<td width="253" align="center"><a href="javascript:submitMenu('t106')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>

</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/（記述系模試）学力要素別成績データ-->
</c:when>
</c:choose>
</td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="275">
<!--志望大学評価別人数-->
<c:choose>
<c:when test="${param.forward == 't001'}">

<table border="0" cellpadding="0" cellspacing="0" width="275">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="275">
<tr>
<td width="273" bgcolor="#FFFFFF" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253">
<!--タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="22" border="0" alt=""><br></td>
<td width="8"><img src="./shared_lib/img/parts/sp.gif" width="8" height="1" border="0" alt=""><br></td>
<td width="135"><b class="text16" style="color:#657681;">志望大学<br>評価別人数</b></td>
<td width="105" valign="bottom">
<!--一括出力対象-->
<table border="0" cellpadding="0" cellspacing="0" width="105">
<tr bgcolor="<kn:exam screen="t005" out="color" />" valign="top">
<td width="2"><img src="./shared_lib/img/parts/tbl_lt_<kn:exam screen="t005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td rowspan="2" width="101">

<table border="0" cellpadding="0" cellspacing="0" width="101">
<tr bgcolor="<kn:exam screen="t005" out="color" />" valign="top">
<td width="1"><img src="./shared_lib/img/parts/sp.gif" width="1" border="0" alt=""><br></td>
<td width="100"><input type="checkbox" name="outItem" value="CountRating" onClick="javascript:change(this)"<c:if test="${ Profile.categoryMap['050204']['1301'] == 1 }"> checked</c:if>><span class="text12-hh">一括出力対象</span></td>
</tr>
</table>

</td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rt_<kn:exam screen="t005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
<tr bgcolor="<kn:exam screen="t005" out="color" />" valign="bottom">
<td width="2"><img src="./shared_lib/img/parts/tbl_lb_<kn:exam screen="t005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
<td width="2"><img src="./shared_lib/img/parts/tbl_rb_<kn:exam screen="t005" out="image" />.gif" width="2" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力対象-->
</td>
</tr>
</table>
<!--/タイトル-->

<!--ボーダー-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="2" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="253" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="253" height="4" border="0" alt=""><br></td>
</tr>
</table>
<!--/ボーダー-->

<!--オプション-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253" height="20" bgcolor="#CDD7DD" align="center">


<table border="0" cellpadding="0" cellspacing="0">
<tr valign="middle">
<td><span class="text12">オプション&nbsp;：</span></td>
<c:if test="${MenuSecurity.menu['502']}">
	<td><input type="checkbox" name="outItem" value="CountRatingSchool" onclick="document.forms[0].changed.value = 1;flag=true" <c:if test="${ Profile.categoryMap['050204']['0403'] == 1 }"> checked</c:if><c:if test="${ isCenterResearch }"> disabled=disabled</c:if>></td>
	<td><span class="text12">他校比較</span></td>
</c:if>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<c:if test="${MenuSecurity.menu['503']}">
	<td><input type="checkbox" name="outItem" value="CountRatingClass" onclick="document.forms[0].changed.value = 1;"<c:if test="${ Profile.categoryMap['050204']['0503'] == 1 }"> checked</c:if>></td>
	<td><span class="text12">クラス比較</span></td>
</c:if>
</tr>
</table>


</td>
</tr>
</table>

<!--/オプション-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下部-->
<table border="0" cellpadding="0" cellspacing="0" width="253">
<tr>
<td width="253"><select size="6" name="" class="text12" style="width:253px;">
	<c:choose>
	  <c:when test="${TextInfoBean.outputTableMap['t005'] != null}">
		<c:forEach var="items" items="${TextInfoBean.outputTableMap['t005']}">
		<c:set var="proItems" value="${ Profile.categoryMap['050204']['1403']}"/>
		<c:forTokens var="id" items="${proItems}" delims=",">
		<c:if test="${ id == items[0] }"><option value=""><c:out value="${items[1]}" /></option></c:if>
		</c:forTokens>
	</c:forEach>
	  </c:when>
	</c:choose>
</select></td>
</tr>
<tr>
<td width="253"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
<tr>
<td width="253" align="center"><a href="javascript:submitMenu('t005')"><img src="./shared_lib/img/btn/hensyu.gif" width="95" height="36" border="0" alt="編集"></a><br></td>
</tr>
</table>
<!--/下部ー-->
</td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/志望大学評価別人数-->
</c:when>
</c:choose>
</td>
</tr>
</table>













<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->
<table border="0" cellpadding="0" cellspacing="0" width="590">
<tr>
<td width="590" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="590" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="28"><img src="./shared_lib/img/parts/sp.gif" width="28" height="1" border="0" alt=""><br></td>
<td width="289">

<!--●●●右側●●●-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="62" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--一括出力-->
<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td><img src="./shared_lib/img/parts/ttl_lump.gif" width="289" height="38" border="0" alt="一括出力"><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="289">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="286" bgcolor="#F3F5F5" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="286">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="224"><div style="margin-top:8px;"><span class="text12">以下の手順に従って一括出力ができます。</span></div></td>
<td width="50"><img src="./shared_lib/img/parts/ttl_lump_parts.gif" width="50" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td nowrap><span class="text12">対象年度 ： </span></td>
<td><select name="targetYear" class="text12" style="width:55px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="2"><img src="./shared_lib/img/parts/sp.gif" width="1" height="7" border="0" alt=""><br></td>
</tr>
<tr>
<td nowrap><span class="text12">対象模試 ： </span></td>
<td><select name="targetExam" class="text12" style="width:200px;" onChange="changeExamSelect()">
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:out value="${exam.examName}" /></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step1-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">出力メニューの設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="29"><img src="./shared_lib/img/parts/yaji_l.gif" width="24" height="26" border="0" alt="←"><br></td>
<td width="220"><span class="text12">左のメニューで一括出力対象を確認の上、下のボタンを押してください。</span></td>
</tr>
<tr>
<td colspan="2"><div style="margin-top:5px;"><a href="javascript:changeExamSelect()"><img src="./shared_lib/img/btn/finish.gif" width="249" height="24" border="0" alt="設定完了"></a></div></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step2-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step3-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step3_cur.gif" width="46" height="12" border="0" alt="STEP3"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<!--ボタン-->
<div style="margin-top:7px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>

<c:if test="${MenuSecurity.menu['502']}">
	<td width="81"><a href="javascript:openCommon(6)"><img src="./shared_lib/img/btn/com_koukou_<c:out value="${ComSetStatusBean.statusMap['school']}" />.gif" width="81" height="22" border="0" alt="比較対象高校"></a><br></td>
</c:if>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><br></td>
<td width="3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="1" border="0" alt=""><br></td>
<td width="81"><br></td>
</tr>
</table>
</div>
<!--/ボタン-->

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249" align="right">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="18"><img src="./shared_lib/img/parts/color_box_00.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">設定済み</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_01.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">要確認</span></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="18"><img src="./shared_lib/img/parts/color_box_02.gif" width="15" height="11" border="0" alt="□"><br></td>
<td nowrap><span class="text12">未設定</span></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step3-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step4-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step4_cur.gif" width="46" height="12" border="0" alt="STEP4"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">出力対象データの確認</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="249"><span class="text12">必要に応じて各項目の編集を行ってください。</span></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="8" border="0" alt=""><br>
</td>
</tr>
</table>
<!--/step4-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr valign="top">
<td width="262" height="18" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓" vspace="4"><br></td>
</tr>
</table>
<!--/矢印-->


<!--step5-->
<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr height="22">
<td width="61" bgcolor="#FF8F0E" align="center"><img src="./shared_lib/img/step/step5_cur.gif" width="46" height="12" border="0" alt="STEP5"><br></td>
<td width="6" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
<td width="195" bgcolor="#98ACB7"><b class="text14" style="color:#FFFFFF;">一括出力</b></td>
</tr>
<tr>
<td colspan="3" width="262"><img src="./shared_lib/img/parts/sp.gif" width="262" height="1" border="0" alt=""><br></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="262">
<tr>
<td width="262" bgcolor="#CDD7DD" align="center">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="249">
<tr>
<td width="120"><span class="text12">ファイル形式の選択：</span></td>
<td width="129"><select name="packageTxtType" class="text12" onchange="document.forms[0].changed.value=1">
<option value="1">CSV</option>
<option value="2"<c:if test="${ Profile.categoryMap['050100']['1402'] == 2 }"> selected</c:if>>テキスト（固定長）</option>
</select></td>
</tr>
<tr>
<td colspan="2" width="249" align="center"><a href="javascript:download()"><img src="./shared_lib/img/btn/hozon.gif" width="114" height="39" border="0" alt="保存" vspace="6"></a><br></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<!--/step5-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="9" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="289" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="289" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/一括出力-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="31"><img src="./shared_lib/img/parts/sp.gif" width="31" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>

<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

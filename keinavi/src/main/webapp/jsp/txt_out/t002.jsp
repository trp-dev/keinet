<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.keinavi.data.help.HelpList" %>
<jsp:useBean id="OnepointBean" scope="request" class="jp.co.fj.keinavi.beans.help.OnepointBean" />
<c:set value="text" var="category" />
<%-- 出力形式 --%>
<c:set var="txtType" value="1" />
<c:choose>
<c:when test="${ param.forward == 't002' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050201']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't003' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050202']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't004' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050203']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't005' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050204']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't103' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050302']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't104' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050303']['1402'] }" />
</c:when>
<c:when test="${ param.forward == 't106' }">
	<c:set var="txtType" value="${ Profile.categoryMap['050304']['1402'] }" />
</c:when>
</c:choose>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／テキスト出力</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
<script type="text/javascript" src="./js/jquery.blockUI.js"></script>
<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
<script type="text/javascript">
<!--

	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD START --%>
	<%@ include file="/jsp/script/download.jsp" %>
	<%-- 2019/07/05 QQ)Tanioka ダウンロード変更対応 ADD END   --%>
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_change.jsp" %>
	<%@ include file="/jsp/script/submit_save.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_max.jsp" %>
	<%@ include file="/jsp/script/open_common.jsp" %>
    <%@ include file="/jsp/script/open_charge.jsp" %>
	<%@ include file="/jsp/script/validate_common.jsp" %>
	<%@ include file="/jsp/script/submit_openOnepoint.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/printDialog_close.jsp" %>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var printDialogPattern = "";
	var confirmDialogPattern = "";
	<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>

	function openSubWindow(url) {
		window.open(url,"_blank","resizable=yes,scrollbars=yes,status=yes,titlebar=yes,location=no,menubar=no,toolbar=no,top=0,left=0,width=680,height=600");
	}

	// 入力チェック
	function validate(mode) {
		var form = document.forms[0];

		// メッセージ
		var message = "";

		// 共通項目設定のチェック
		// 他校比較オプションが有効 && センターリサーチ以外なら設定が必要
		var flag = false;
		<c:if test="${ not isCenterResearch }">
			<c:if test="${ param.forward == 't002' && Profile.categoryMap['050201']['0403'] == 1 }">flag = true;</c:if>
			<c:if test="${ param.forward == 't003' && Profile.categoryMap['050202']['0403'] == 1 }">flag = true;</c:if>
			<c:if test="${ param.forward == 't004' && Profile.categoryMap['050203']['0403'] == 1 }">flag = true;</c:if>
			<c:if test="${ param.forward == 't005' && Profile.categoryMap['050204']['0403'] == 1 }">flag = true;</c:if>
		</c:if>
		if (mode && flag) {
			message += validateCommon();
		}

		// チェックはひとつ以上
		var e = form.outTarget;
		var count = 0;
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) count++;
		}
		if (count == 0) {
			message += "出力対象項目を選択してください。\n";
		}

		if (message != "") {
			alert(message);
			return false;
		}

		return true;
	}


	// 全て選択
	function allChecked(){
		   for(i = 0; i < document.forms[0].outTarget.length; i++) {
			document.forms[0].outTarget[i].checked = true;
		   }
	}

	function init() {
		startTimer();
		// スクロール位置を初期化する
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
	}

// -->
</script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
</head>

<body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="TextDetail" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<input type="hidden" name="top" value="<c:out value="${param.backward}" />">
<input type="hidden" name="changeExam" value="0">
<input type="hidden" name="exit" value="">
<input type="hidden" name="out" value="0">
<input type="hidden" name="save" value="">
<input type="hidden" name="printFlag" value="">
<input type="hidden" name="changed" value="<c:choose><c:when test="${Profile.changed}">1</c:when><c:otherwise>0</c:otherwise></c:choose>">
<input type="hidden" name="txtType" value="<c:out value="${ txtType }" />">

<!--HEADER-->
<%@ include file="/jsp/shared_lib/header02.jsp" %>
<!--/HEADER-->
<!--グローバルナビゲーション-->
<%@ include file="/jsp/shared_lib/global.jsp" %>
<!--/グローバルナビゲーション-->
<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="500" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="968" bgcolor="#FFFFFF">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="968" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="javascript:submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr bgcolor="#828282">
<td width="968"><img src="./shared_lib/img/parts/sp.gif" width="968" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="36" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="887">
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="887">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="871">

<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr valign="top">
<td colspan="4" width="871" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="871" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="871" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="871" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="855" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt="">
<b class="text16" style="color:#FFFFFF;">
<c:choose>
  <c:when test="${ param.forward == 't002' }">集計データ（成績概況）</c:when>
  <c:when test="${ param.forward == 't003' }">集計データ（偏差値分布）</c:when>
  <c:when test="${ param.forward == 't004' }">集計データ（設問別成績）</c:when>
  <c:when test="${ param.forward == 't005' }">集計データ（志望大学評価別人数）</c:when>
  <c:when test="${ param.forward == 't103' }">個人データ（設問別成績）</c:when>
  <c:when test="${ param.forward == 't104' }">個人データ（模試別成績データ）</c:when>
  <c:when test="${ param.forward == 't106' }">個人データ（学力要素別成績データ）</c:when>
  <c:when test="${ param.forward == 't202' }">入試結果調査票データ</c:when>
</c:choose>
</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="871" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="871" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="871" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="871" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
</td>
<td width="41"><img src="./shared_lib/img/parts/sp.gif" width="41" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->




<table border="0" cellpadding="0" cellspacing="0" width="968">
<tr valign="top">
<td width="40"><img src="./shared_lib/img/parts/sp.gif" width="40" height="1" border="0" alt=""><br></td>
<td width="689" align="center">
<!--●●●左側●●●-->
<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="689">
<tr>
<td width="669"><span class="text14">
このメニューでの出力項目を以下の手順に従って設定してください。
</span></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/説明-->

<!--ボタン-->
<div style="margin-top:14px;">
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" name="regist" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="return submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">ファイル形式の選択：</span></td>
<td><select name="txtType1" class="text12" onchange="document.forms[0].txtType.value=this.value;document.forms[0].txtType2.selectedIndex=this.selectedIndex;">
<option value="1" <c:if test="${ txtType == '1' }"> selected</c:if>>CSV</option>
<option value="2" <c:if test="${ txtType == '2' }"> selected</c:if>>テキスト（固定長）</option>
</select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><a href="javascript:download_saveFlgOn()"><img src="./shared_lib/img/btn/hozon_w.gif" width="116" height="37" border="0" alt="保存"></a><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="27" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<c:forTokens var="screen" items="t102,t103,t104,t106,t202" delims=",">
  <c:if test="${ param.forward == screen }">
    <!--中タイトル-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr valign="top">
    <td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
    <td width="641">

    <table border="0" cellpadding="0" cellspacing="0" width="641">
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
    </tr>
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
    </tr>
    <tr>
    <td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
    <td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="24" border="0" alt=""><br></td>
    <td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
    <td width="628" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">担当クラス</b></td>
    </tr>
    <tr valign="top">
    <td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
    </tr>
    </table>

    </td>
    <td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/中タイトル-->

    <!--担当クラスの設定-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="0" cellspacing="1" width="650">
    <tr valign="top">
    <td width="648" bgcolor="#EFF2F3" align="center">
    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->

    <table border="0" cellpadding="0" cellspacing="0" width="620">
    <tr valign="top">
    <td bgcolor="#8CA9BB">
    <table border="0" cellpadding="9" cellspacing="1" width="620">
    <tr valign="top">
    <td width="618" bgcolor="#FFFFFF" align="center"><b class="text14"><c:choose>
      <c:when test="${ empty ChargeClass.classList }">設定なし</c:when>
      <c:otherwise>
        <c:out value="${ChargeClass.year}" />年度
        <c:forEach var="class" items="${ChargeClass.classList}">
          &nbsp;<c:out value="${class.grade}" />年<c:out value="${class.className}" />
        </c:forEach>
      </c:otherwise>
    </c:choose></b></td>
    </tr>
    </table>
    </td>
    </tr>
    </table>

    <div style="margin-top:8px;">
    <!--設定ボタン--><input type="button" value="&nbsp;設定&nbsp;" class="text12" style="width:120px;" onClick="openCharge()">
    </div>

    <!--spacer-->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr valign="top">
    <td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
    </tr>
    </table>
    <!--/spacer-->
    </td>
    </tr>
    </table>
    </td>
    </tr>
    </table>
    <!--/担当クラスの設定-->


    <!--矢印-->
    <table border="0" cellpadding="0" cellspacing="0" width="650">
    <tr>
    <td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
    </tr>
    </table>
    <!--/矢印-->
  </c:if>
</c:forTokens>

<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step1.gif" width="86" height="24" border="0" alt="STEP1"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">対象模試の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--対象模試の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF" align="center">

<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象年度&nbsp;：&nbsp;</span></td>
<td><select name="targetYear" class="text12" style="width:60px;" onChange="changeExamSelect()">
<c:forEach var="year" items="${ExamSession.years}">
  <option value="<c:out value="${year}" />"<c:if test="${ form.targetYear == year }"> selected</c:if>><c:out value="${year}" /></option>
</c:forEach>
</select></td>
</tr>
<tr>
<td colspan="3"><img src="./shared_lib/img/parts/sp.gif" width="1" height="6" border="0" alt=""><br></td>
</tr>
<tr>
<td width="21"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td nowrap><span class="text14">対象模試&nbsp;：&nbsp;</span></td>
<td><select name="targetExam" class="text12" style="width:220px;" onChange="changeExamSelect()"<c:if test="${ param.forward == 't202' }"> disabled</c:if>>
<c:forEach var="exam" items="${ExamSession.examMap[form.targetYear]}">
  <option value="<c:out value="${exam.examCD}" />"<c:if test="${ form.targetExam == exam.examCD }"> selected</c:if>><c:if test="${ param.forward != 't202' }"><c:out value="${exam.examName}" /></c:if></option>
</c:forEach>
</select></td>
</tr>
</table>
</div>
<img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br>
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/対象模試の設定-->
<c:choose>
  <c:when test="${ (param.forward == 't002' || param.forward == 't003' || param.forward == 't004' || param.forward == 't005') && MenuSecurity.menu['502'] }">

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step2.gif" width="86" height="24" border="0" alt="STEP2"><br></td>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">共通項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--共通項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="576" align="center">

<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td><span class="text14-hh">この分析で利用する共通項目です。<br></span></td>
</tr>
<tr valign="top">
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>



<table border="0" cellpadding="0" cellspacing="2" width="576">
<tr bgcolor="#CDD7DD">
<td colspan="2" width="100%" align="right">
<table border="0" cellpadding="6" cellspacing="0">
<tr>
<td><span class="text12">共通項目設定の最終更新日 ： <fmt:formatDate value="${Profile.updateDate}" pattern="yyyy/MM/dd" /></span></td>
</tr>
</table>
</td>
</tr>
<!--比較対象高校-->
<tr height="36">
<td width="34%" bgcolor="#E1E6EB">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校" align="absmiddle" hspace="6"></td>
<td><b class="text12">比較対象高校</b></td>
</tr>
</table>

</td>
<td width="66%" bgcolor="#F4E5D6" align="center"><span class="text12"><a href="javascript:openCommon(6)"><kn:com item="school" /></a></span></td>

</tr>
<!--/比較対象高校-->
</table>

</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/共通項目の設定-->
  </c:when>
</c:choose>
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->


<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="641">

<table border="0" cellpadding="0" cellspacing="0" width="641">
<tr valign="top">
<td colspan="4" width="641" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="641" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<c:choose>
  <c:when test="${ (param.forward == 't002' || param.forward == 't003' || param.forward == 't004' || param.forward == 't005') && MenuSecurity.menu['502'] }">
	<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step3.gif" width="86" height="24" border="0" alt="STEP3"><br></td>
  </c:when>
  <c:otherwise>
	<td width="86" bgcolor="#FF8F0E"><img src="./shared_lib/img/step/ttl_step2.gif" width="86" height="24" border="0" alt="STEP2"><br></td>
  </c:otherwise>
</c:choose>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="549" bgcolor="#758A98"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""><b class="text14" style="color:#FFFFFF;">出力対象項目の設定</b></td>
</tr>
<tr valign="top">
<td colspan="4" width="641" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="641" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
</table>
<!--/中タイトル-->

<!--出力対象項目の設定-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#EFF2F3" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="620">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="620">
<tr valign="top">
<td width="618" bgcolor="#FFFFFF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="618">
<tr valign="top">
<td width="26" align="right"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="576" align="center">

<!--説明-->
<table border="0" cellpadding="0" cellspacing="0" width="572">
<tr valign="top">
<td width="572"><span class="text14-hh">このデータで出力される項目の一覧と説明が表示されています。<br>一覧から、出力の対象にしたい項目を選択してください。</span></td>
</tr>
<tr valign="top">
<td><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/説明-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="576">
<tr bgcolor="#93A3AD">
<td colspan="5" width="100%">
<table border="0" cellpadding="2" cellspacing="2">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="すべて選択" class="text12" style="width:100px;" onClick="allChecked()"<c:if test="${ param.forward == 't202' }"> disabled</c:if>></td>
</tr>
</table>
</td>
</tr>
<!--項目-->
<tr bgcolor="#CDD7DD" align="center">
<td width="6%"><span class="text12">出力<br>対象</span></td>
<td width="6%"><span class="text12">No.</span></td>
<td width="20%"><span class="text12">項目名</span></td>
<td width="10%"><span class="text12">最大<br>バイト数</span></td>
<td width="58%"><span class="text12">備考</span></td>
</tr>
<!--/項目-->
<!--set-->

<c:choose>
<%-- 成績概況 --%>
<c:when test="${param.forward == 't002'}">
<c:set var="proItems" value="${ Profile.categoryMap['050201']['1403']}"/>
</c:when>
<%-- 偏差値分布 --%>
<c:when test="${param.forward == 't003'}">
<c:set var="proItems" value="${ Profile.categoryMap['050202']['1403']}"/>
</c:when>
<%-- 設問別成績 --%>
<c:when test="${param.forward == 't004'}">
<c:set var="proItems" value="${ Profile.categoryMap['050203']['1403']}"/>
</c:when>
<%-- 志望大学評価別人数 --%>
<c:when test="${param.forward == 't005'}">
<c:set var="proItems" value="${ Profile.categoryMap['050204']['1403']}"/>
</c:when>
<%-- 個人模試別成績データ（旧形式） --%>
<c:when test="${param.forward == 't102'}">
<c:set var="proItems" value="${ Profile.categoryMap['050301']['1403']}"/>
</c:when>
<%-- 個人設問別成績データ --%>
<c:when test="${param.forward == 't103'}">
<c:set var="proItems" value="${ Profile.categoryMap['050302']['1403']}"/>
</c:when>
<%-- 個人模試別成績データ（新形式） --%>
<c:when test="${param.forward == 't104'}">
<c:set var="proItems" value="${ Profile.categoryMap['050303']['1403']}"/>
</c:when>
<%-- （記述系模試）学力要素別成績データ --%>
<c:when test="${param.forward == 't106'}">
<c:set var="proItems" value="${ Profile.categoryMap['050304']['1403']}"/>
</c:when>
</c:choose>

<%-- 画面表示内容を切り替える --%>
<c:choose>
	<%-- t003 --%>
	<c:when test="${ param.forward == 't003'}">
		<c:choose>
			<%-- 新テストの場合 --%>
			<c:when test="${NewExam}">
				<c:set var="id" value="t003b" />
			</c:when>
			<%-- それ以外 --%>
			<c:otherwise>
				<c:set var="id" value="t003a" />
			</c:otherwise>
		</c:choose>
	</c:when>
	<%-- それ以外 --%>
	<c:otherwise>
		<c:set var="id" value="${param.forward}" />
	</c:otherwise>
</c:choose>

<c:forEach var="tableItems" items="${TextInfoBean.outputTableMap[id]}" varStatus="status">
<tr>
<td bgcolor="#E1E6EB" align="center"><input type="checkbox" name="outTarget" value="<c:out value="${tableItems[0]}" />"

<c:choose>
<%-- 入試結果調査 --%>
<c:when test="${ param.forward == 't202' }">checked disabled></c:when>
<%-- それ以外 --%>
<c:otherwise>
<c:forTokens var="name" items="${proItems}" delims=","><c:if test="${ name == tableItems[0] }">checked></c:if></c:forTokens>
</c:otherwise>
</c:choose>

</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${status.count}"/></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${tableItems[1]}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="right">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${tableItems[3]}" /></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td><span class="text12"><c:out value="${tableItems[4]}" escapeXml="false" /></span></td>
</tr>
</table>
</td>
</tr>
</c:forEach>
<!--/set-->

</table>
<!--/リスト-->

</td>
<td width="11"><img src="./shared_lib/img/parts/sp.gif" width="11" height="1" border="0" alt=""><br></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="12" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/出力対象項目の設定-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td width="650" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="650">
<tr valign="top">
<td width="648" bgcolor="#FBD49F" align="center">

<input type="button" name="regist" value="&nbsp;＜ 登録して戻る&nbsp;" class="text12" style="width:110px;" onClick="return submitRegist()">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="22" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="650">
<tr>
<td align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">ファイル形式の選択：</span></td>
<td><select name="txtType2" class="text12" onchange="document.forms[0].txtType.value=this.value;document.forms[0].txtType1.selectedIndex=this.selectedIndex;">
<option value="1" <c:if test="${ txtType == '1' }"> selected</c:if>>CSV</option>
<option value="2" <c:if test="${ txtType == '2' }"> selected</c:if>>テキスト（固定長）</option>
</select></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><a href="javascript:download_saveFlgOn()"><img src="./shared_lib/img/btn/hozon_w.gif" width="116" height="37" border="0" alt="保存"></a><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<%-- センターリサーチコメント --%>
<c:choose>
<c:when test="${ param.forward == 't102' }">
<table border="0" cellpadding="10" cellspacing="1" width="550" bgcolor="#8CA9BB">
  <tr>
    <td bgcolor="#F9EEE5">
      <span class="text12">出力された成績ファイルは以下の場所・名前で保存されます。</span>
      <table border="0" cellpadding="2" cellspacing="0" style="margin:5px 0px 0px 10px">
        <tr>
          <td nowrap><span class="text12">[保存場所]</span></td>
          <td nowrap><span class="text12">Windows98・Me</span></td>
          <td nowrap><span class="text12">：</span></td>
          <td nowrap><span class="text12">Cドライブの「Kei-Net」フォルダに保存されます。</span></td>
        </tr>
        <tr>
          <td><br></td>
          <td valign="top" nowrap><span class="text12">Windows2000・XP</span></td>
          <td valign="top" nowrap><span class="text12">：</span></td>
          <td><span class="text12">Cドライブの「Documents and Settings」フォルダの、「ログオンユーザ名」フォルダの、「Ke-Net」フォルダに保存されます。</span></td>
        </tr>
      </table>
      <table border="0" cellpadding="2" cellspacing="0" style="margin:5px 0px 0px 10px">
        <tr>
          <td valign="top" nowrap><span class="text12">[ファイル名]</span></td>
          <td nowrap>
            <span class="text12">
              zbf610_(模試コード)_(出力年月日時分秒).csvの名称で保存されます。<br>
              ※模試コードは以下の通りです。<br>
              センター・リサーチ自己採点 = 「38」<br>
              第3回全統記述模試 = 「07」<br>
<!--
              第3回全統マーク模試 = 「03」<br>
              全統センター試験プレテスト = 「04」<br>
              第3回全統高1模試 = 「73」<br>
              第3回全統高2模試 = 「63」<br>
              第4回全統高1模試 = 「74」<br>
              全統マーク高2模試 = 「66」<br>
              全統記述高2模試 = 「67」<br>
-->
            </span>
          </td>
        </tr>
      </table>
      <dl style="margin:5px 0px 0px 0px">
        <dt class="text12">例：WindowsXPパソコンに「shinro」という「ユーザ名」でログオンし、センター・リサーチ自己採点成績データを出力した場合</dt>
        <dd class="text12">「Cドライブ」の「Documents and Settings」の「shinro」の「Kei-Net」に「zbf610_38_20050119133210.csv」という名称のファイルが保存されます。</dd>
      </dl>
    </td>
  </tr>
</table>
</c:when>
<c:when test="${ param.forward == 't104' || param.forward == 't106' }">
<table border="0" cellpadding="10" cellspacing="1" width="550" bgcolor="#8CA9BB">
  <tr>
    <td bgcolor="#F9EEE5">
      <table border="0" cellpadding="2" cellspacing="0" style="margin:5px 0px 0px 10px">
        <tr>
          <td valign="top" nowrap><span class="text12">
              ※模試コードは画面右上のヘルプから「J(解説)用語/模試・科目コード他」の「模試コード/型・科目コード」をご参照ください。
          </span></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</c:when>
</c:choose>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●左側●●●-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="175">
<!--●●●右側●●●-->
<table border="0" cellpadding="0" cellspacing="0" width="175">
<tr valign="top">
<td width="175"><img src="./shared_lib/img/illust/one_point.gif" width="173" height="84" border="0" alt="ワンポイントアドバイス！!"><br></td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--ワンポイントアドバイス-->
<%@ include file="/jsp/shared_lib/Onepoint.jsp" %>
<!--/ワンポイントアドバイス-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/●●●右側●●●-->
</td>
<td width="43"><img src="./shared_lib/img/parts/sp.gif" width="43" height="1" border="0" alt=""><br></td>
</tr>
</table>


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->

</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="960" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="960" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="997">
<tr valign="top">
<td width="997"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer02.jsp" %>
<!--/FOOTER-->

<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/print_dialog.jsp" %>

<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/12 QQ)Hisakawa 大規模改修 ADD END   --%>
</form>
</body>
</html>

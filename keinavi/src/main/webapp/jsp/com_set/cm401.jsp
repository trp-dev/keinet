<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/ArrayList.js"></script>
<script type="text/javascript" src="./js/OrderChange.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/com_close.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	function submitAdd() {
		// 大学数のチェック
		if (oc.getData().length >= 20) {
			alert("<kn:message id="cm002a" />");
			return;
		}

		setupForm();
		document.forms[0].forward.value = "cm404";
		document.forms[0].backward.value = "cm401";
		document.forms[0].submit();
	}

	function DelAll() {

		if (!confirm("<kn:message id="cm012c" />")) {
			return;
		}

		var data = oc.getData();
		for (var i=0; i<data.length; i++) {
			// 削除実行
			oc.deleteElement(data[i][0]);
		}
		// 更新フラグ
		formChanged();
		// 再描画
		drawTable1();
		drawTable2();

	}

	// 大学データ
	var univ = new Array();
	<c:forEach var="univ" items="${UnivBean2.fullList}" varStatus="status">
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
	<%--
		univ[<c:out value="${status.index}" />] = new Array(
			"<c:out value="${univ.univCode}" />",
			"<c:out value="${univ.univName}" />",
			"<c:out value="${univ.choiceNum}" />",
			"<c:out value="${UnivDivBean.univDivMap[univ.univDiv]}" />",
			"<c:out value="${PrefBean.prefMap[univ.location]}" />");
  	  --%>
		univ[<c:out value="${status.index}" />] = new Array(
				"<c:out value="${univ.univCode}" />",
				"<c:out value="${univ.univName}" />",
				"<c:out value="${univ.choiceNum}" />",
				"<c:out value="${UnivDivBean.univDivMap[univ.univDiv]}" />",
				"<c:out value="${PrefBean.prefMap[univ.location]}" />",
				"<c:out value="${univ.univDiv}" />",
				"<c:out value="${univ.univNameKana}" />");

		<%-- 大学区分を国私区分に置き換える --%>
		if (univ[<c:out value="${status.index}" />][5] == "01") {
			univ[<c:out value="${status.index}" />][5] = "02";
		}
	<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
	</c:forEach>

	// レイヤ操作クラス
	var sw = new SwitchLayer();
	// ソートクラス
	var oc = new OrderChange(0);
	oc.setIndex(0);
	if (univ.length > 0) oc.setKey(univ[0][0]);
	oc.setData(univ);

	// 個別選択の大学コードをフォームにセットする
	function setupForm() {
		var data = oc.getData();
		var univ = document.forms[0].elements["univ"];

		// なければ中止
		if (univ == null) return;

		// 複数の場合
		if (univ.length) {
			for (var i=0; i<univ.length; i++) {
				if (data.length > i) {
					univ[i].value = data[i][0];
					univ[i].disabled = false;
				} else {
					univ[i].disabled = "disabled";
				}
			}

		// 単数の場合
		} else {
			// データがあれば入れる
			if (data.length > 0) {
				univ.value = data[0][0];
				univ.disabled = false;
			// なければ無効
			} else {
				univ.disabled = "disabled";
			}
		}
	}

	function change(obj) {
		var color1 = "FAFAFA";
		var color2 = "FAFAFA";
		if (obj.value == "1") {
			color1 = "#FFAD8C";
			sw.showLayer("Layer1");
			sw.hideLayer("Layer2");
			sw.hideLayer("Layer3");
		} else if (obj.value == "2") {
			color2 = "#FFAD8C";
			sw.hideLayer("Layer1");
			sw.showLayer("Layer2");
			sw.hideLayer("Layer3");
			// テーブルを描画
			drawTable1();
		}
		var cur  = new DOMUtil(new DOMUtil(obj).getParent(2));
		cur.getChild(2).style.backgroundColor = color1;
		cur.getChild(6).style.backgroundColor = color2;
	}

	//「対象選択」テーブルの描画
	function drawTable1() {
		var univ = oc.getData();
		var table = sw.getLayer("Table1");
		var row = <c:out value="${UnivBean2.fullListHalfSize}" />;
		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
				if (univ.length >= index) {
					data[0] = univ[index - 1][0]; // 大学コード
					data[1] = univ[index - 1][1]; // 志望大学名
					data[2] = univ[index - 1][2] + "人"; // 志望人数
					data[3] = univ[index - 1][3]; // 国私区分
					data[4] = univ[index - 1][4]; // 所在地
					data[5] = "<a href=\"javascript:deleteUniv('" + univ[index - 1][0] + "')\">削除</a>"; // 削除
				}
				var gap = 6 * j;
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[1];
				cur.getChild(gap, 0, 0, 0, 1, 0).innerHTML = data[2];
				cur.getChild(++gap, 0).innerHTML = data[3];
				cur.getChild(++gap, 0).innerHTML = data[4];
				cur.getChild(++gap, 0).innerHTML = data[5];
			}
		}
	}

	// 「表示順変更」テーブルの描画
	function drawTable2() {
		var univ = oc.getData();
		var key = oc.getKey();
		var table = sw.getLayer("Table2");
		var row = <c:out value="${UnivBean2.fullListHalfSize}" />;
		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
				if (univ.length >= index) {
					// ラジオボタン
					var checked = univ[index - 1][0] == key ? " checked" : "";
					data[0] = "<input type=\"radio\" name=\"order\" value=\"" + univ[index - 1][0]
						+ "\" onClick=\"oc.setKey(this.value)\"" + checked + ">";
					data[1] = univ[index - 1][0]; // 大学コード
					data[2] = univ[index - 1][1]; // 志望大学名
					data[3] = univ[index - 1][2] + "人"; // 志望人数
					data[4] = univ[index - 1][3]; // 国私区分
					data[5] = univ[index - 1][4]; // 所在地
				}
				var gap = 6 * j;
				cur.getChild(++gap, 0).innerHTML = data[0];
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[2];
				cur.getChild(gap, 0, 0, 0, 1, 0).innerHTML = data[3];
				cur.getChild(++gap, 0).innerHTML = data[4];
				cur.getChild(++gap, 0).innerHTML = data[5];
			}
		}
	}

	// 大学を削除する
	function deleteUniv(id) {

		// 削除対象になる大学名を取得する
		var name = "";
		var data = oc.getData();
		for (var i=0; i<data.length; i++) {
			if (data[i][0] == id) {
				name = data[i][1];
				break;
			}
		}

		// 確認ダイアログ
		if (!confirm(name + "<kn:message id="cm009c" />")) {
			return;
		}

		// 削除実行
		oc.deleteElement(id);
		// デフォルトのチェックは最初の要素に再設定
		if (oc.getData().length) oc.setKey((oc.getData())[0][0]);
		// 更新フラグ
		formChanged();
		// 再描画
		drawTable1();

	}

	// 大学コード順
	function sortUniv() {
		oc.sort(
			function (a, b) {
				<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
				<%--
				if (a[0] == b[0]) return 0;
				else if (a[0] > b[0]) return 1;
				else if (a[0] < b[0]) return -1;
				  --%>

				// 1.大学区分
				if (a[5] > b[5]) return 1;
				else if (a[5] < b[5]) return -1;

				// 2.カナ
				if (a[6] > b[6]) return 1;
				else if (a[6] < b[6]) return -1;

				// 3.大学コード
				if (a[0] > b[0]) return 1;
				else if (a[0] < b[0]) return -1;

				return 0;
				<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
			}
		);
		// 再描画
		drawTable2();
	}

	// 志望人数順
	function sortChoice() {
		oc.sort(
			function (a, b) {
				<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
				<%--
				if (a[2] - 0 == b[2] - 0) return 0;
				else if (a[2] - 0 < b[2] - 0) return 1;
				else if (a[2] - 0 > b[2] - 0) return -1;
				  --%>

				// 1.大学区分
				if (a[5] > b[5]) return 1;
				else if (a[5] < b[5]) return -1;

				// 2.志望人数（降順）
				if (a[2] - 0 < b[2] - 0) return 1;
				else if (a[2] - 0 > b[2] - 0) return -1;

				// 3.カナ
				if (a[6] > b[6]) return 1;
				else if (a[6] < b[6]) return -1;

				// 4.大学コード
				if (a[0] > b[0]) return 1;
				else if (a[0] < b[0]) return -1;

				return 0;
				<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
			}
		);
		// 再描画
		drawTable2();
	}

	//「対象選択」「表示順変更」の切替
	function changeTab(mode) {
		if (mode) {
			document.forms[0].selection[0].disabled = "disabled";
			drawTable2();
			sw.showLayer("Layer3");
			sw.hideLayer("Layer2");
		} else {
			document.forms[0].selection[0].disabled = false;
			drawTable1();
			sw.showLayer("Layer2");
			sw.hideLayer("Layer3");
		}
	}

	function up () {
		oc.up();
		// 再描画
		drawTable2();
	}

	function down() {
		oc.down();
		// 再描画
		drawTable2();
	}

	function init() {
		startTimer();
		// 選択方法
		var e = document.forms[0].elements["selection"];
		// 一般
		if (e.length) {
			if (e[0].checked) change(e[0]);
			else if (e[1].checked) change(e[1]);
		// 営業
		} else {
			change(e)
		}
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonUnivSelect" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">

<%-- 大学コードの入れ物 --%>
<c:forEach var="univ" items="${CommonForm.actionForms.CM401Form.univ}">
  <input type="hidden" name="univ" value="<c:out value="${univ}" />">
</c:forEach>

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_shibou.gif" width="30" height="30" border="0" alt="志望大学"><br></td>
<td width="160"><b class="text16" style="color:#657681">志望大学</b></td>
<td width="591">
<!--選択方法-->
<c:if test="${LoginSession.sales}"><div style="visibility:hidden"></c:if>
<table border="0" cellpadding="0" cellspacing="0" width="591" height="38">
<tr>
<td rowspan="3" width="101"><img src="./com_set/img/ttl_select.gif" width="101" height="38" border="0" alt="選択方法"><br></td>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="489" bgcolor="FAFAFA"> --%>
<td width="489" height="36" bgcolor="FAFAFA">
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
<table border="0" cellpadding="0" cellspacing="0" width="489">
<tr height="30">
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="1" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM401Form.selection == 1 }"> checked</c:if><c:if test="${LoginSession.sales}"> disabled</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="217" bgcolor="#FFAD8C">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="215"><span class="text12-lh">模試で記入された志望大学の<BR>志望者数上位20校を対象にする</span></td>
</tr>
</table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="2" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM401Form.selection == 2 }"> checked</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="218">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="216"><span class="text12-lh">志望大学を選択する</span></td>
</tr>
</table>
</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
</tr>
</table>
<c:if test="${LoginSession.sales}"></div></c:if>
<!--/選択方法-->
</td>
</tr>
</table>
</div>
<!--/中タイトル-->


<!-- レイヤ１ -->
<div id="Layer1" style="visibility:hidden">

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">

<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">模試で記入された志望大学の一覧です。すべてを志望大学に設定しています。<br>選択する場合は「選択方法」を切り替えてください。<br></span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">
<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745">
<tr>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="31%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="26%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
</tr>


<c:forEach var="i" begin="1" end="10">
  <tr height="27">
  <td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
  <c:choose>
    <c:when test="${ UnivBean1.fullListSize >= i && not empty UnivBean1.fullList[i - 1].univCode && UnivBean1.fullList[i - 1].choiceNum > 0 }">
      <td bgcolor="#F4E5D6">
      <table border="0" cellpadding="4" cellspacing="0" width="100%">
      <tr>
      <td><span class="text12"><c:out value="${ UnivBean1.fullList[i - 1].univName }" /></span></td>
      <td align="right" nowrap><span class="text12"><c:out value="${ UnivBean1.fullList[i - 1].choiceNum }" />人</span></td>
      </tr>
      </table>
      </td>
      <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${UnivDivBean.univDivMap[UnivBean1.fullList[i - 1].univDiv]}" /></span></td>
      <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PrefBean.prefMap[UnivBean1.fullList[i - 1].location]}" /></span></td>
    </c:when>
    <c:otherwise>
      <td bgcolor="#F4E5D6"><br></td>
      <td bgcolor="#F4E5D6"><br></td>
      <td bgcolor="#F4E5D6"><br></td>
    </c:otherwise>
  </c:choose>
  <td bgcolor="#FFFFFF">&nbsp;</td>
  <td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + 10 }" /></span></td>
  <c:choose>
    <c:when test="${ UnivBean1.fullListSize >= i + 10 && not empty UnivBean1.fullList[i + 10 - 1].univCode && UnivBean1.fullList[i + 10 - 1].choiceNum > 0}">
      <td bgcolor="#F4E5D6">
      <table border="0" cellpadding="4" cellspacing="0" width="100%">
      <tr>
      <td><span class="text12"><c:out value="${ UnivBean1.fullList[i + 10 - 1].univName }" /></span></td>
      <td align="right" nowrap><span class="text12"><c:out value="${ UnivBean1.fullList[i + 10 - 1].choiceNum }" />人</span></td>
      </tr>
      </table>
      </td>
      <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${UnivDivBean.univDivMap[UnivBean1.fullList[i + 10 - 1].univDiv]}" /></span></td>
      <td bgcolor="#F4E5D6" align="center"><span class="text12"><c:out value="${PrefBean.prefMap[UnivBean1.fullList[i + 10 - 1].location]}" /></span></td>
      </tr>
    </c:when>
    <c:otherwise>
      <td bgcolor="#F4E5D6"><br></td>
      <td bgcolor="#F4E5D6"><br></td>
      <td bgcolor="#F4E5D6"><br></td>
    </c:otherwise>
  </c:choose>
</c:forEach>


</table>
<!--/リスト-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>

</td>
</tr>
</table>
</div>
<!--/概要-->

<!--注意-->
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="790">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD START --%>
<%-- <td><span class="text12">全志望大学の中から国公立大・私立大それぞれで志望者数の多い順に合わせて20件表示しています。<br>また、印刷時には国公立大、私立大、短大、その他が別シートにて20件印刷されます。</span></td> --%>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD END --%>
<td><span class="text12">全志望大学の中から国公立大・私立大それぞれで志望者数の多い順に合わせて20件表示しています。<br>また、国公立大、私立大、短大、その他が別シートにて出力されます。</span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<!--/注意-->

</div>
<!-- /レイヤ１ -->

<!-- レイヤ２ -->
<div id="Layer2" style="visibility:hidden">

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">志望大学が選択されていることを確認し、必要に応じて新規追加、もしくは表示順の変更を行ってください。<br>（全20校登録可能）<br></span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">
<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr>
<td width="110">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;新規追加&nbsp;" class="text12" style="width:100px;" onClick="submitAdd()"></td>
</tr>
</table>
</td>
<td width="413"><font class="text12" style="color:#FFFFFF;">※検索画面に遷移します。</font></td>
<td width="200"><font class="text12" style="color:#FFFFFF;">
<b>対象選択</b>
&nbsp;｜&nbsp;
<a href="javascript:changeTab(1)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>表示順変更</b></a>
</font></td>
<td align="right">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;全削除&nbsp;" class="text12" style="width:60px;" onClick="DelAll()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table1">
<tr>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="26%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="27%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
</tr>

<c:if test="${ UnivBean2.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${UnivBean2.fullListHalfSize}">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + UnivBean2.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--注意-->
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="790">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD START --%>
<%-- <td><span class="text12">印刷時には国公立大、私立大、短大、その他が別シートにて印刷されます。</span></td> --%>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD END --%>
<td><span class="text12">国公立大、私立大、短大、その他が別シートにて出力されます。</span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<!--/注意-->

</div>
<!-- /レイヤ２ -->

<!-- レイヤ３ -->
<div id="Layer3" style="visibility:hidden">
<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">志望大学が選択されていることを確認し、必要に応じて新規追加、もしくは表示順の変更を行ってください。<br>（全20校登録可能）<br></span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr>
<td width="80" align="center"><b class="text12" style="color:#FFFFFF;">表示順変更</b></td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▲" class="text12" style="width:35px;" onClick="up();formChanged()"></td>
</tr>
</table>
</td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▼" class="text12" style="width:35px;" onClick="down();formChanged()"></td>
</tr>
</table>
</td>
<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td width="110">
  --%>
<td width="130">
<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td bgcolor="#FFFFFF"><input type="button" value="大学コード順" class="text12" style="width:100px;" onClick="sortUniv();formChanged()"></td>
  --%>
<td bgcolor="#FFFFFF"><input type="button" value="大学区分・名称順" class="text12" style="width:120px;" onClick="sortUniv();formChanged()"></td>
<%-- 2016/01/18 QQ)Nishiyama 大規模改修 UPD END   --%>
</tr>
</table>
</td>
<td width="243">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="志望人数順" class="text12" style="width:100px;" onClick="sortChoice();formChanged()"></td>
</tr>
</table>
</td>
<td width="200"><font class="text12" style="color:#FFFFFF;">
<a href="javascript:changeTab(0)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>対象選択</b></a>
&nbsp;｜&nbsp;
<b>表示順変更</b>
</font></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;全削除&nbsp;" class="text12" style="width:60px;" onClick="DelAll()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table2">
<tr>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">表示順<br>変更</span></td>
<!-- <td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">大学<br>コード</span></td>-->
<td width="25%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">表示順<br>変更</span></td>
<td width="26%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">志望大学名</span></td>
<td align="right" nowrap><span class="text12">志望人数</span></td>
</tr>
</table>
</td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">国私<br>区分</span></td>
<td width="7%" bgcolor="#CDD7DD" align="center"><span class="text12">所在地</span></td>
</tr>

<c:if test="${ UnivBean2.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${UnivBean2.fullListHalfSize}">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + UnivBean2.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--注意-->
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="790">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD START --%>
<%-- <td><span class="text12">印刷時には国公立大、私立大、短大、その他が別シートにて印刷されます。</span></td> --%>
<%-- 2019/07/19 QQ)Kitaoka システム企画 UPD END --%>
<td><span class="text12">国公立大、私立大、短大、その他が別シートにて出力されます。</span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
<!--/注意-->

</div>
<!-- /レイヤ３ -->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;設定完了（すべての共通項目設定を反映します）&nbsp;" class="text12" style="width:340px;" onClick="submitRegist(0)">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="windowClose()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</body>
</html>

<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/ArrayList.js"></script>
<script type="text/javascript" src="./js/OrderChange.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/com_close.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	function submitAdd() {
		// 高校数のチェック
		<c:choose>
			<%-- 営業 --%>
			<c:when test="${LoginSession.sales}">
				if (oc.getData().length >= 300) {
					alert("<kn:message id="cm008a" />");
					return;
				}
			</c:when>
			<%-- その他 --%>
			<c:otherwise>
				if (oc.getData().length >= 20) {
					alert("<kn:message id="cm007a" />");
					return;
				}
			</c:otherwise>
		</c:choose>

		setupForm();
		document.forms[0].forward.value = "cm603";
		document.forms[0].backward.value = "cm601";
		document.forms[0].submit();
	}

	function DelAll() {

		if (!confirm("<kn:message id="cm012c" />")) {
			return;
		}

		var data = oc.getData();
		for (var i=0; i<data.length; i++) {
			oc.deleteElement(data[i][0]);
			// チェック状態もfalseに
			graph[data[i][0]] = false;
			bg[data[i][0]] = false;
		}
		// 更新フラグ
		formChanged();
		// 再描画
		drawTable1();
		drawTable2();

	}

	// 学校データ
	var school = new Array();
	<c:forEach var="school" items="${SchoolBean.fullList}" varStatus="status">
		school[<c:out value="${status.index}" />] = new Array(
			"<c:out value="${school.schoolCode}" />",
			"<c:out value="${school.schoolName}" />",
			"<c:if test="${LoginSession.sales}"><c:out value="${school.rank}" /></c:if>",
			<c:out value="${school.examinees}" />,
			"<c:out value="${PrefBean.prefMap[school.location]}" />");
	</c:forEach>

	// 選択状態を保持する配列
	var graph = new Array();
	// 背景色を保持する配列
	var bg = new Array();
	<c:forEach var="school" items="${SchoolBean.fullList}">
		<c:set var="status" value="false" />
		<c:forEach var="code" items="${CommonForm.actionForms.CM601Form.graph}">
			<c:if test="${ school.schoolCode == code }"><c:set var="status" value="true" /></c:if>
		</c:forEach>
		graph["<c:out value="${school.schoolCode}" />"] = <c:out value="${status}" />;
		bg["<c:out value="${school.schoolCode}" />"] = <c:out value="${status}" />;
	</c:forEach>

	// レイヤ操作クラス
	var sw = new SwitchLayer();
	// ソートクラス
	var oc = new OrderChange(0);
	oc.setIndex(0);
	if (school.length >0) oc.setKey(school[0][0]);
	oc.setData(school);

	// 行数は読み込み時の値を保持する
	var row = Math.round(school.length / 2);

	// 順序通りにフォームデータを作る
	function setupForm() {
		var data = oc.getData();
		var s = document.forms[0].elements["school"];
		var g = document.forms[0].elements["graph"];

		// 無ければ中止
		if (s == null) return;

		// 比較対象高校が複数の場合
		if (s.length) {
			for (var i=0; i<s.length; i++) {
				if (data.length > i) {
					s[i].value = data[i][0];
					s[i].disabled = false;
				} else {
					s[i].disabled = "disabled";
				}
			}

		// 比較対象高校が単数の場合
		} else {
			// データがあれば入れる
			if (data.length > 0) {
				s.value = data[0][0];
				s.disabled = false;
			// なければ無効
			} else {
				s.disabled = "disabled";
			}
		}
	}

	//「対象選択」「表示順変更」の切替
	function changeTab(mode) {
		if (mode) {
			drawTable2();
			sw.showLayer("Layer2");
			sw.hideLayer("Layer1");
		} else {
			drawTable1();
			sw.showLayer("Layer1");
			sw.hideLayer("Layer2");
		}
	}

	function changeColor(obj) {
/*
		var color = obj.checked ? "#FFAD8C" : "#F4E5D6";
		var cur  = new DOMUtil(obj);
		cur.getParent(2).childNodes[1].style.backgroundColor = color;
		cur.getParent(2).childNodes[2].style.backgroundColor = color;
		cur.getParent(2).childNodes[3].style.backgroundColor = color;
		cur.getParent(2).childNodes[4].style.backgroundColor = color;
		cur.getParent(2).childNodes[5].style.backgroundColor = color;
		cur.getParent(2).childNodes[6].style.backgroundColor = color;
		cur.getParent(2).childNodes[7].style.backgroundColor = color;
*/
		// 選択状態を保持
		graph[obj.value] = obj.checked;
	}

	//「対象選択」テーブルの描画
	function drawTable1() {
		var school = oc.getData();
		var table = sw.getLayer("Table1");
		//var row = Math.round(school.length / 2);
		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
				// デフォルトの背景色
				var color = "#F4E5D6";
				if (school.length >= index) {
					var checked = graph[school[index - 1][0]] ? " checked" : "";
					data[0] = "<input type=\"checkbox\" name=\"graph\" value=\""
						+ school[index - 1][0] + "\" onClick=\"changeColor(this);formChanged()\"" + checked + ">";

					// 高校コード
					// 全国と都道府県は非表示
					if (school[index - 1][0].substring(2, 7) == '999') {
						data[1] = "";
					} else {
						data[1] = school[index - 1][0];
					}

					data[2] = school[index - 1][1]; // 高校名
					data[3] = school[index - 1][2]; // ランク
					data[4] = school[index - 1][3] + "人"; // 受験人数
					data[5] = school[index - 1][4]; // 所在地
					data[6] = "<a href=\"javascript:deleteSchool('"
						+ school[index - 1][0] + "')\">削除</a></span>"; // 削除
					// グラフ表示対象ならアクティブ
					if (bg[school[index - 1][0]]) color = "#FFAD8C";
				}
				<c:choose>
					<c:when test="${LoginSession.sales}">var gap = 9 * j;</c:when>
					<c:otherwise>var gap = 8 * j;</c:otherwise>
				</c:choose>
				// 背景色
				cur.getChild(gap + 1).style.backgroundColor = color;
				cur.getChild(gap + 2).style.backgroundColor = color;
				cur.getChild(gap + 3).style.backgroundColor = color;
				cur.getChild(gap + 4).style.backgroundColor = color;
				cur.getChild(gap + 5).style.backgroundColor = color;
				cur.getChild(gap + 6).style.backgroundColor = color;
				<c:if test="${LoginSession.sales}">cur.getChild(gap + 7).style.backgroundColor = color;</c:if>
				// 値を入れる
				cur.getChild(++gap).innerHTML = data[0];
				cur.getChild(++gap, 0).innerHTML = data[1];
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[2];
				<c:if test="${LoginSession.sales}">cur.getChild(++gap, 0).innerHTML = data[3];</c:if>
				cur.getChild(++gap, 0).innerHTML = data[4];
				cur.getChild(++gap, 0).innerHTML = data[5];
				cur.getChild(++gap, 0).innerHTML = data[6];
			}
		}
	}

	// 「表示順変更」テーブルの描画
	function drawTable2(school, key) {
		var school = oc.getData();
		var key = oc.getKey();
		var table = sw.getLayer("Table2");
		//var row = Math.round(school.length / 2);
		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
				<c:choose>
					<c:when test="${LoginSession.sales}">var gap = 8 * j;</c:when>
					<c:otherwise>var gap = 7 * j;</c:otherwise>
				</c:choose>
				// デフォルトの背景色
				var color = "#F4E5D6";
				if (school.length >= index) {
					// ラジオボタン
					var checked = school[index - 1][0] == key ? " checked" : "";
					data[0] = "<input type=\"radio\" name=\"order\" value=\"" + school[index - 1][0]
						+ "\" onClick=\"oc.setKey(this.value)\"" + checked + ">";

					// 高校コード
					// 全国と都道府県は非表示
					if (school[index - 1][0].substring(2, 7) == '999') {
						data[1] = "";
					} else {
						data[1] = school[index - 1][0];
					}

					data[2] = school[index - 1][1]; // 高校名
					data[3] = school[index - 1][2]; // ランク
					data[4] = school[index - 1][3] + "人"; // 受験人数
					data[5] = school[index - 1][4]; // 所在地
					// グラフ表示対象ならアクティブ
					if (bg[school[index - 1][0]]) color = "#FFAD8C";
				}
				// 背景色
				cur.getChild(gap + 1).style.backgroundColor = color;
				cur.getChild(gap + 2).style.backgroundColor = color;
				cur.getChild(gap + 3).style.backgroundColor = color;
				cur.getChild(gap + 4).style.backgroundColor = color;
				cur.getChild(gap + 5).style.backgroundColor = color;
				<c:if test="${LoginSession.sales}">cur.getChild(gap + 6).style.backgroundColor = color;</c:if>
				// データを入れる
				cur.getChild(++gap).innerHTML = data[0];
				cur.getChild(++gap, 0).innerHTML = data[1];
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[2];
				<c:if test="${LoginSession.sales}">cur.getChild(++gap, 0).innerHTML = data[3];</c:if>
				cur.getChild(++gap, 0).innerHTML = data[4];
				cur.getChild(++gap, 0).innerHTML = data[5];
			}
		}
	}

	// 高校を削除する
	function deleteSchool(id) {
		// 削除対象になる高校名を取得する
		var name = "";
		var data = oc.getData();
		for (var i=0; i<data.length; i++) {
			if (data[i][0] == id) {
				name = data[i][1];
				break;
			}
		}

		// 全国・県でなければ高校を付加
		if (id.substring(2, 7) != '999') name += "高校";

		// 確認ダイアログ
		if (!confirm(name + "<kn:message id="cm010c" />")) {
			return;
		}

		oc.deleteElement(id);

		// チェック状態もfalseに
		graph[id] = false;
		bg[id] = false;

		// デフォルトのチェックは最初の要素に再設定
		if (oc.getData().length) oc.setKey((oc.getData())[0][0]);
		// 更新フラグ
		formChanged();
		// 再描画
		drawTable1();

	}

	// 高校コード順
	function sortSchool(index) {
		oc.sort(
			function (a, b) {
				if (a[0] == b[0]) return 0;
				else if (a[0] > b[0]) return 1;
				else if (a[0] < b[0]) return -1;
			}
		);
		// 再描画
		drawTable2();
	}

	// 志望人数順
	function sortChoice(index) {
		oc.sort(
			function (a, b) {
				if (a[3] == b[3]) return 0;
				else if (a[3] < b[3]) return 1;
				else if (a[3] > b[3]) return -1;
			}
		);
		// 再描画
		drawTable2();
	}

	function up () {
		oc.up();
		// 再描画
		drawTable2();
	}

	function down() {
		oc.down();
		// 再描画
		drawTable2();
	}

	function selectGraph(obj) {
		for (var key in graph) {
			graph[key] = obj.checked;
		}
		drawTable1();
	}

	function init() {
		startTimer();
		sw.showLayer("Layer1");
		sw.hideLayer("Layer2");
		drawTable1();
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonCompSchoolSelect" />" method="POST">
<input type="hidden" name="forward" value="<c:out value="${param.forward}" />">
<input type="hidden" name="backward" value="<c:out value="${param.backward}" />">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">

<%-- 比較対象高校 --%>
<c:forEach var="school" items="${SchoolBean.fullList}">
  <input type="hidden" name="school" value="<c:out value="${school.schoolCode}" />">
</c:forEach>

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--比較対象高校-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校"><br></td>
<td width="751"><b class="text16" style="color:#657681">比較対象高校</b></td>
</tr>
</table>
</div>
<!--/中タイトル-->


<!-- レイヤ１ -->
<div id="Layer1" style="visibility:hidden">

<!--概要-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">グラフ表示対象の選択、分析対象の新規追加がこの画面から行えます。（全<c:choose><c:when test="${ not LoginSession.sales }">20</c:when><c:otherwise>300</c:otherwise></c:choose>校登録可能）</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr>
<td width="500">
<table border="0" cellpadding="2" cellspacing="0">
<tr height="27">
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;新規追加&nbsp;" class="text12" style="width:100px;" onClick="submitAdd()"></td>
<td nowrap>&nbsp;&nbsp;</td>
<td><input type="checkbox" name="dummy" value="" onclick="selectGraph(this);formChanged();"></td>
<td nowrap><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>
</td>
<td width="200"><font class="text12" style="color:#FFFFFF;">
<b>対象選択</b>
&nbsp;｜&nbsp;
<a href="javascript:changeTab(1)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>表示順変更</b></a>
</font></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr height="27">
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;全削除&nbsp;" class="text12" style="width:60px;" onClick="DelAll()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table1">
<tr align="center">
<td width="3%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">グラフ<br>表示</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">高校<br>コード</span></td>
<td width="11%" bgcolor="#CDD7DD"><span class="text12">高校名</span></td>
<c:if test="${LoginSession.sales}"><td width="6%" bgcolor="#CDD7DD"><span class="text12">ランク</span></td></c:if>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">受験<br>人数</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">所在地</span></td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="3%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">グラフ<br>表示</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">高校<br>コード</span></td>
<td width="12%" bgcolor="#CDD7DD"><span class="text12">高校名</span></td>
<c:if test="${LoginSession.sales}"><td width="6%" bgcolor="#CDD7DD"><span class="text12">ランク</span></td></c:if>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">受験<br>人数</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">所在地</span></td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
</tr>

<c:if test="${ SchoolBean.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${SchoolBean.fullListHalfSize}">
<tr>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
</tr>
</table>
</td>
<c:if test="${LoginSession.sales}"><td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td></c:if>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + SchoolBean.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
</tr>
</table>
</td>
<c:if test="${LoginSession.sales}"><td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td></c:if>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

</div>
<!-- /レイヤ１ -->


<!-- レイヤ２ -->
<div id="Layer2" style="visibility:hidden">

<!--概要-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">グラフ表示対象の選択、分析対象の新規追加がこの画面から行えます。（全<c:choose><c:when test="${ not LoginSession.sales }">20</c:when><c:otherwise>300</c:otherwise></c:choose>校登録可能）</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->



<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr>
<td width="80" align="center"><b class="text12" style="color:#FFFFFF;">表示順変更</b></td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▲" class="text12" style="width:35px;" onClick="up();formChanged()"></td>
</tr>
</table>
</td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▼" class="text12" style="width:35px;" onClick="down();formChanged()"></td>
</tr>
</table>
</td>
<td width="110">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="高校コード順" class="text12" style="width:100px;" onClick="sortSchool();formChanged()"></td>
</tr>
</table>
</td>
<td width="243">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="受験人数順" class="text12" style="width:100px;" onClick="sortChoice();formChanged()"></td>
</tr>
</table>
</td>
<td width="200"><font class="text12" style="color:#FFFFFF;">
<a href="javascript:changeTab(0)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>対象選択</b></a>
&nbsp;｜&nbsp;
<b>表示順変更</b>
</font></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr height="27">
<td bgcolor="#FFFFFF"><input type="button" value="&nbsp;全削除&nbsp;" class="text12" style="width:60px;" onClick="DelAll()"></td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table2">
<tr align="center">
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">表示順<br>変更</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">高校<br>コード</span></td>
<td width="15%" bgcolor="#CDD7DD"><span class="text12">高校名</span></td>
<c:if test="${LoginSession.sales}"><td width="6%" bgcolor="#CDD7DD"><span class="text12">ランク</span></td></c:if>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">受験<br>人数</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">所在地</span></td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="4%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">表示順<br>変更</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">高校<br>コード</span></td>
<td width="16%" bgcolor="#CDD7DD"><span class="text12">高校名</span></td>
<c:if test="${LoginSession.sales}"><td width="6%" bgcolor="#CDD7DD"><span class="text12">ランク</span></td></c:if>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">受験<br>人数</span></td>
<td width="6%" bgcolor="#CDD7DD"><span class="text12">所在地</span></td>
</tr>

<c:if test="${ SchoolBean.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${SchoolBean.fullListHalfSize}">
<tr>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
</tr>
</table>
</td>
<c:if test="${LoginSession.sales}"><td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td></c:if>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + SchoolBean.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
</tr>
</table>
</td>
<c:if test="${LoginSession.sales}"><td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td></c:if>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
<td bgcolor="#F4E5D6" align="center" nowrap><span class="text12"></span></td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

</div>
<!-- /レイヤ２ -->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/比較対象高校-->


<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;設定完了（すべての共通項目設定を反映します）&nbsp;" class="text12" style="width:340px;" onClick="submitRegist(0)">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="windowClose()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</body>
</html>

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

	// 都道府県データ
	var data = new Array();
	<c:forEach var="block" items="${ PrefBean.blockCodeList }">
		data["<c:out value="${block}" />"] = new Array(
		<c:forEach var="pref" items="${ PrefBean.blockMap[block] }" varStatus="status">
			<c:if test="${ status.index > 0 }">,</c:if>
			"<c:out value="${pref}" />"
		</c:forEach>
		);
	</c:forEach>

	function checkAll() {
		checkElemtns("block", true);
		checkElemtns("pref", true);
	}

	function removeCheckAll() {
		checkElemtns("block", false);
		checkElemtns("pref", false);
	}

	function checkElemtns(name, value) {
		var form = document.forms[0];
		var e = form.elements[name];
		for (var i=0; i<e.length; i++) e[i].checked = value;
	}

	function checkBlock(obj) {
		var form = document.forms[0];
		var block = data[obj.value];
		var elements = form.elements["pref"];
		for (var i=0; i<elements.length; i++) {
			for (var j=0; j<block.length; j++) {
				if (block[j] == elements[i].value) elements[i].checked = obj.checked;
			}
		}
	}

	function checkPref(obj) {
		var form = document.forms[0];
		var e1 = form.elements["block"];
		var e2 = form.elements["pref"];
		var block = "";
		// チェックされた都道府県がある地区を探す
		for (var i=0; i<e1.length; i++) {
			var pref = data[e1[i].value];
			for (var j=0; j<pref.length; j++) {
				if (pref[j] == obj.value) {
					block = e1[i].value;
					break;
				}
			}
		}
		// 同じ地区の都道府県が全てチェックされているかどうか
		var flag = true;
		for (var i=0; i<e2.length; i++) {
			var pref = data[block];
			for (var j=0; j<pref.length; j++) {
				if (pref[j] == e2[i].value) {
					if (!e2[i].checked) flag = false;
				}
			}
		}
		// 地区のチェック状態を変更
		for (var i=0; i<e1.length; i++) {
			if (e1[i].value == block) e1[i].checked = flag;
		}
	}

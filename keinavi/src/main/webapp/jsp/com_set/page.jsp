<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<span class="text12">
<%-- 現在のページが２ページ目以降なら表示する --%>
<c:if test="${ SearchBean.pageNo > 1 }">
  <a href="javascript:submitPage(1)">＜＜</a>&nbsp;
  <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo - 1 }" />)">＜</a>&nbsp;
  ・・・&nbsp;
</c:if>
<%-- 現在のページ〜５つ先まで表示 --%>
<c:if test="${ SearchBean.pageNo - 2 > 0 }">
  <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo - 2 }" />)"><c:out value="${ SearchBean.pageNo - 2 }" /></a>&nbsp;
</c:if>
<c:if test="${ SearchBean.pageNo - 1 > 0 }">
  <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo - 1 }" />)"><c:out value="${ SearchBean.pageNo - 1 }" /></a>&nbsp;
</c:if>
<c:out value="${SearchBean.pageNo}" />&nbsp;
<c:if test="${ SearchBean.pageNo + 1 <= SearchBean.pageCount }">
  <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo + 1 }" />)"><c:out value="${ SearchBean.pageNo + 1 }" /></a>&nbsp;
</c:if>
<c:if test="${ SearchBean.pageNo + 2 <= SearchBean.pageCount }">
  <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo + 2 }" />)"><c:out value="${ SearchBean.pageNo + 2 }" /></a>&nbsp;
</c:if>
<%-- 現在のページが最終ページでなければ表示する --%>
<c:if test="${ SearchBean.pageCount > SearchBean.pageNo }">
  ・・・&nbsp;
  <c:if test="${ SearchBean.pageCount > 1 }">
    <a href="javascript:submitPage(<c:out value="${ SearchBean.pageNo + 1 }" />)">＞</a>&nbsp;
  </c:if>
  <c:if test="${ SearchBean.pageCount > 1 }">
    <a href="javascript:submitPage(<c:out value="${ SearchBean.pageCount }" />)">＞＞</a>
  </c:if>
</c:if>
</span>
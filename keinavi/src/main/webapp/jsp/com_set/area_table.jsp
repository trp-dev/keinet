<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>
<%
	// 地区のチェックマップ
	java.util.HashMap c1 = new java.util.HashMap();
	pageContext.setAttribute("c1", c1);
	// 県のチェックマップ
	java.util.HashMap c2 = new java.util.HashMap();
	pageContext.setAttribute("c2", c2);
%>
<c:forEach var="block" items="${form.block}">
<% c1.put(pageContext.getAttribute("block"), " checked"); %>
</c:forEach>
<c:forEach var="pref" items="${form.pref}">
<% c2.put(pageContext.getAttribute("pref"), " checked"); %>
</c:forEach>
<table border="0" cellpadding="5" cellspacing="2" width="547">
<tr>
<td colspan="2" width="100%" bgcolor="#93A3AD">
<table border="0" cellpadding="3" cellspacing="0">
<tr height="27">
<td><input type="checkbox" name="dummy" value="" onclick="this.checked ? checkAll() : removeCheckAll();"></td>
<td><b class="text12" style="color:#FFF;">すべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>
</td>
</tr>
<!--北海道-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="01" onClick="checkBlock(this)"<c:out value="${c1['01']}" />></td>
<td><span class="text12">北海道地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="01" onClick="checkPref(this)"<c:out value="${c2['01']}" />></td>
<td width="40"><span class="text12">北海道</span></td>
</tr>
</table>
</td>
</tr>
<!--/北海道-->
<!--東北地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="02" onClick="checkBlock(this)"<c:out value="${c1['02']}" />></td>
<td><span class="text12">東北地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="02" onClick="checkPref(this)"<c:out value="${c2['02']}" />></td>
<td width="40"><span class="text12">青森</span></td>
<td width="20"><input type="checkbox" name="pref" value="03" onClick="checkPref(this)"<c:out value="${c2['03']}" />></td>
<td width="40"><span class="text12">岩手</span></td>
<td width="20"><input type="checkbox" name="pref" value="04" onClick="checkPref(this)"<c:out value="${c2['04']}" />></td>
<td width="40"><span class="text12">宮城</span></td>
<td width="20"><input type="checkbox" name="pref" value="05" onClick="checkPref(this)"<c:out value="${c2['05']}" />></td>
<td width="40"><span class="text12">秋田</span></td>
<td width="20"><input type="checkbox" name="pref" value="06" onClick="checkPref(this)"<c:out value="${c2['06']}" />></td>
<td width="40"><span class="text12">山形</span></td>
<td width="20"><input type="checkbox" name="pref" value="07" onClick="checkPref(this)"<c:out value="${c2['07']}" />></td>
<td width="40"><span class="text12">福島</span></td>
</tr>
</table>
</td>
</tr>
<!--/東北地区-->
<!--関東地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="03" onClick="checkBlock(this)"<c:out value="${c1['03']}" />></td>
<td><span class="text12">関東地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="08" onClick="checkPref(this)"<c:out value="${c2['08']}" />></td>
<td width="40"><span class="text12">茨城</span></td>
<td width="20"><input type="checkbox" name="pref" value="09" onClick="checkPref(this)"<c:out value="${c2['09']}" />></td>
<td width="40"><span class="text12">栃木</span></td>
<td width="20"><input type="checkbox" name="pref" value="10" onClick="checkPref(this)"<c:out value="${c2['10']}" />></td>
<td width="40"><span class="text12">群馬</span></td>
<td width="20"><input type="checkbox" name="pref" value="11" onClick="checkPref(this)"<c:out value="${c2['11']}" />></td>
<td width="40"><span class="text12">埼玉</span></td>
<td width="20"><input type="checkbox" name="pref" value="12" onClick="checkPref(this)"<c:out value="${c2['12']}" />></td>
<td width="40"><span class="text12">千葉</span></td>
<td width="20"><input type="checkbox" name="pref" value="13" onClick="checkPref(this)"<c:out value="${c2['13']}" />></td>
<td width="40"><span class="text12">東京</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="pref" value="14" onClick="checkPref(this)"<c:out value="${c2['14']}" />></td>
<td width="40"><span class="text12">神奈川</span></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
</tr>
</table>
</td>
</tr>
<!--/関東地区-->
<!--甲信越地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="04" onClick="checkBlock(this)"<c:out value="${c1['04']}" />></td>
<td><span class="text12">甲信越地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="15" onClick="checkPref(this)"<c:out value="${c2['15']}" />></td>
<td width="40"><span class="text12">新潟</span></td>
<td width="20"><input type="checkbox" name="pref" value="19" onClick="checkPref(this)"<c:out value="${c2['19']}" />></td>
<td width="40"><span class="text12">山梨</span></td>
<td width="20"><input type="checkbox" name="pref" value="20" onClick="checkPref(this)"<c:out value="${c2['20']}" />></td>
<td width="40"><span class="text12">長野</span></td>
</tr>
</table>
</td>
</tr>
<!--/甲信越地区-->
<!--北陸地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="05" onClick="checkBlock(this)"<c:out value="${c1['05']}" />></td>
<td><span class="text12">北陸地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="16" onClick="checkPref(this)"<c:out value="${c2['16']}" />></td>
<td width="40"><span class="text12">富山</span></td>
<td width="20"><input type="checkbox" name="pref" value="17" onClick="checkPref(this)"<c:out value="${c2['17']}" />></td>
<td width="40"><span class="text12">石川</span></td>
<td width="20"><input type="checkbox" name="pref" value="18" onClick="checkPref(this)"<c:out value="${c2['18']}" />></td>
<td width="40"><span class="text12">福井</span></td>
</tr>
</table>
</td>
</tr>
<!--/北陸地区-->
<!--東海地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="06" onClick="checkBlock(this)"<c:out value="${c1['06']}" />></td>
<td><span class="text12">東海地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="21" onClick="checkPref(this)"<c:out value="${c2['21']}" />></td>
<td width="40"><span class="text12">岐阜</span></td>
<td width="20"><input type="checkbox" name="pref" value="22" onClick="checkPref(this)"<c:out value="${c2['22']}" />></td>
<td width="40"><span class="text12">静岡</span></td>
<td width="20"><input type="checkbox" name="pref" value="23" onClick="checkPref(this)"<c:out value="${c2['23']}" />></td>
<td width="40"><span class="text12">愛知</span></td>
<td width="20"><input type="checkbox" name="pref" value="24" onClick="checkPref(this)"<c:out value="${c2['24']}" />></td>
<td width="40"><span class="text12">三重</span></td>
</tr>
</table>
</td>
</tr>
<!--/東海地区-->
<!--近畿地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="07" onClick="checkBlock(this)"<c:out value="${c1['07']}" />></td>
<td><span class="text12">近畿地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="25" onClick="checkPref(this)"<c:out value="${c2['25']}" />></td>
<td width="40"><span class="text12">滋賀</span></td>
<td width="20"><input type="checkbox" name="pref" value="26" onClick="checkPref(this)"<c:out value="${c2['26']}" />></td>
<td width="40"><span class="text12">京都</span></td>
<td width="20"><input type="checkbox" name="pref" value="27" onClick="checkPref(this)"<c:out value="${c2['27']}" />></td>
<td width="40"><span class="text12">大阪</span></td>
<td width="20"><input type="checkbox" name="pref" value="28" onClick="checkPref(this)"<c:out value="${c2['28']}" />></td>
<td width="40"><span class="text12">兵庫</span></td>
<td width="20"><input type="checkbox" name="pref" value="29" onClick="checkPref(this)"<c:out value="${c2['29']}" />></td>
<td width="40"><span class="text12">奈良</span></td>
<td width="20"><input type="checkbox" name="pref" value="30" onClick="checkPref(this)"<c:out value="${c2['30']}" />></td>
<td width="40"><span class="text12">和歌山</span></td>
</tr>
</table>
</td>
</tr>
<!--/近畿地区-->
<!--中国地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="08" onClick="checkBlock(this)"<c:out value="${c1['08']}" />></td>
<td><span class="text12">中国地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="31" onClick="checkPref(this)"<c:out value="${c2['31']}" />></td>
<td width="40"><span class="text12">鳥取</span></td>
<td width="20"><input type="checkbox" name="pref" value="32" onClick="checkPref(this)"<c:out value="${c2['32']}" />></td>
<td width="40"><span class="text12">島根</span></td>
<td width="20"><input type="checkbox" name="pref" value="33" onClick="checkPref(this)"<c:out value="${c2['33']}" />></td>
<td width="40"><span class="text12">岡山</span></td>
<td width="20"><input type="checkbox" name="pref" value="34" onClick="checkPref(this)"<c:out value="${c2['34']}" />></td>
<td width="40"><span class="text12">広島</span></td>
<td width="20"><input type="checkbox" name="pref" value="35" onClick="checkPref(this)"<c:out value="${c2['35']}" />></td>
<td width="40"><span class="text12">山口</span></td>
</tr>
</table>
</td>
</tr>
<!--/中国地区-->
<!--四国地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="09" onClick="checkBlock(this)"<c:out value="${c1['09']}" />></td>
<td><span class="text12">四国地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="36" onClick="checkPref(this)"<c:out value="${c2['36']}" />></td>
<td width="40"><span class="text12">徳島</span></td>
<td width="20"><input type="checkbox" name="pref" value="37" onClick="checkPref(this)"<c:out value="${c2['37']}" />></td>
<td width="40"><span class="text12">香川</span></td>
<td width="20"><input type="checkbox" name="pref" value="38" onClick="checkPref(this)"<c:out value="${c2['38']}" />></td>
<td width="40"><span class="text12">愛媛</span></td>
<td width="20"><input type="checkbox" name="pref" value="39" onClick="checkPref(this)"<c:out value="${c2['39']}" />></td>
<td width="40"><span class="text12">高知</span></td>
</tr>
</table>
</td>
</tr>
<!--/四国地区-->
<!--九州・沖縄地区-->
<tr>
<td width="22%" bgcolor="#E1E6EB">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td><input type="checkbox" name="block" value="10" onClick="checkBlock(this)"<c:out value="${c1['10']}" />></td>
<td><span class="text12">九州・沖縄地区</span></td>
</tr>
</table>
</td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="checkbox" name="pref" value="40" onClick="checkPref(this)"<c:out value="${c2['40']}" />></td>
<td width="40"><span class="text12">福岡</span></td>
<td width="20"><input type="checkbox" name="pref" value="41" onClick="checkPref(this)"<c:out value="${c2['41']}" />></td>
<td width="40"><span class="text12">佐賀</span></td>
<td width="20"><input type="checkbox" name="pref" value="42" onClick="checkPref(this)"<c:out value="${c2['42']}" />></td>
<td width="40"><span class="text12">長崎</span></td>
<td width="20"><input type="checkbox" name="pref" value="43" onClick="checkPref(this)"<c:out value="${c2['43']}" />></td>
<td width="40"><span class="text12">熊本</span></td>
<td width="20"><input type="checkbox" name="pref" value="44" onClick="checkPref(this)"<c:out value="${c2['44']}" />></td>
<td width="40"><span class="text12">大分</span></td>
<td width="20"><input type="checkbox" name="pref" value="45" onClick="checkPref(this)"<c:out value="${c2['45']}" />></td>
<td width="40"><span class="text12">宮崎</span></td>
</tr>
<tr>
<td width="20"><input type="checkbox" name="pref" value="46" onClick="checkPref(this)"<c:out value="${c2['46']}" />></td>
<td width="40"><span class="text12">鹿児島</span></td>
<td width="20"><input type="checkbox" name="pref" value="47" onClick="checkPref(this)"<c:out value="${c2['47']}" />></td>
<td width="40"><span class="text12">沖縄</span></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
<td width="20"></td>
<td width="40"></td>
</tr>
</table>
</td>
</tr>
<!--/九州・沖縄地区-->
</table>

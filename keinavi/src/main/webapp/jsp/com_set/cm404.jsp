<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set var="form" value="${CommonForm.actionForms.CM406Form}" scope="request" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/area_script.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	// ボタンを押したらTRUEにする
	var pushed = false;

	function submitBack() {
		document.forms[0].forward.value = document.forms[0].backward.value;
		document.forms[0].backward.value = "cm404";
		document.forms[0].submit();
	}

	function submitSearchChar(obj) {
		document.forms[0].forward.value = "cm406";
		document.forms[0].backward.value = "cm404";
		document.forms[0].searchChar.value = obj.value;
		document.forms[0].submit();
	}

	function check() {
		// 入力チェック用オブジェクト
		var val = new Validator();

		// 大学名はひらがな
		if (!val.isHiragana(document.forms[0].searchStr.value)) {
			alert("<kn:message id="cm003a" />");
			return false;
		}

		// ボタンは押されたことになる
		pushed = true;
	}

	function init() {
		startTimer();
		<c:if test="${ param.searchconf != null }">
			//大学名・大学コード検索後の再検索の場合
			if(<c:out value="${param.searchconf}" /> == 1) {
				window.location = '#colsearch';
			}
		</c:if>
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonUnivSearch" />" method="POST" onSubmit="return pushed">
<input type="hidden" name="forward" value="cm406">
<input type="hidden" name="backward" value="cm404">
<input type="hidden" name="searchChar" value="">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--概要-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_shibou.gif" width="30" height="30" border="0" alt="志望大学"><br></td>
<td width="160"><b class="text16" style="color:#657681">志望大学</b></td>
<td width="591">
<!--選択方法-->
<c:if test="${LoginSession.sales}"><div style="visibility:hidden"></c:if>
<table border="0" cellpadding="0" cellspacing="0" width="591" height="38">
<tr>
<td rowspan="3" width="101"><img src="./com_set/img/ttl_select.gif" width="101" height="38" border="0" alt="選択方法"><br></td>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<td width="489" bgcolor="FAFAFA">
<table border="0" cellpadding="0" cellspacing="0" width="489">
<tr height="30">
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="" value=""></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="217">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="215"><span class="text12-lh">模試で記入された志望大学の<BR>志望者数上位20校を対象にする</span></td>
</tr>
</table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="" value="" checked></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="218" bgcolor="#FFAD8C">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="216"><span class="text12-lh">志望大学を選択する</span></td>
</tr>
</table>
</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
</tr>
</table>
<c:if test="${LoginSession.sales}"></div></c:if>
<!--/選択方法-->
</td>
</tr>
</table>
</div>
<!--/中タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<table border="0" cellpadding="6" cellspacing="1" width="183" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FBD49F" align="center"><input type="button" value="一覧に戻る" onclick="submitMenu('cm401');"></td>
</tr>
</table>

<!--新規追加-->
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./com_set/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="160">
<b class="text14" style="color:#FFFFFF;">新規追加</b><br>
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="160">
<tr>
<td width="160" height="27" bgcolor="#EFF2F3" align="center">
<%-- カウント --%>
<c:set var="count" value="20" />
<c:forEach var="univ" items="${CommonForm.actionForms.CM401Form.univ}">
  <c:set var="count" value="${ count - 1}" />
</c:forEach>
<!--文言--><span class="text12">あと<b style="color:#FF8F0E;"><c:out value="${count}" />校</b>の登録が可能です</span>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</div>

</td>
</tr>
</table>
</div>
<!--/新規追加-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">検　索</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">選　択</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="16"><img src="./shared_lib/img/parts/sp.gif" width="16" height="1" border="0" alt=""><br></td>
<td width="591">

<!--●●●右側●●●-->
<%-- 営業のみ --%>
<c:choose>
<c:when test="${LoginSession.sales}">
<div style="margin-top:3px;">
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td width="591"><b class="text14-hh">※下記のいずれかの方法で検索してください。</b></td>
</tr>
</table>
</div>
<!--地区・国私区分選択-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><span class="text14-hh">大学の所在地（地区）と国私区分で検索します</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="551"><b class="text12">&nbsp;1.対象地区の選択</b><br>
<!--対象地区の選択-->
<%@ include file="/jsp/com_set/area_table.jsp" %>
<!--/対象地区の選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<b class="text12">&nbsp;2.国私区分の選択</b><br>

<!--国私区分の選択-->
<table border="0" cellpadding="5" cellspacing="2" width="547">
<tr>
<td width="22%" bgcolor="#E1E6EB"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><span class="text12">国私区分</span></td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<c:set var="c1" value="" />
<c:set var="c2" value="" />
<c:set var="c3" value="" />
<c:set var="c4" value="" />
<c:forEach var="univDiv" items="${form.univDiv}">
  <c:if test="${ univDiv == '01' }"><c:set var="c1" value=" checked" /></c:if>
  <c:if test="${ univDiv == '02' }"><c:set var="c2" value=" checked" /></c:if>
  <c:if test="${ univDiv == '03' }"><c:set var="c3" value=" checked" /></c:if>
  <c:if test="${ univDiv == '04' }"><c:set var="c4" value=" checked" /></c:if>
</c:forEach>
<td width="20"><input type="checkbox" name="univDiv" value="01"<c:out value="${c1}" />></td>
<td width="40"><span class="text12">国公立</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="02"<c:out value="${c2}" />></td>
<td width="40"><span class="text12">私立</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="03"<c:out value="${c3}" />></td>
<td width="40"><span class="text12">短大</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="04"<c:out value="${c4}" />></td>
<td width="40"><span class="text12">その他</span></td>
</tr>
</table>
</td>
</tr>
</table>
<!--国私区分の選択-->

<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="7" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#FBD49F" align="center">
<input type="submit" name="button" value="上記の条件で検索" class="text12" style="width:189px;" onClick="pushed=true">
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/地区・国私区分選択-->

</c:when>

<%-- 一般 --%>

<c:otherwise>

<!--国私区分選択-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><span class="text14-hh">国私区分を選択後、「模試・志望大学検索」をしてください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="551"><b class="text14-hh">&nbsp;国私区分の選択</b><br>
<!--選択-->
<table border="0" cellpadding="5" cellspacing="2" width="547">
<tr>
<td width="22%" bgcolor="#E1E6EB"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><span class="text12">国私区分</span></td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<c:set var="c1" value="" />
<c:set var="c2" value="" />
<c:set var="c3" value="" />
<c:set var="c4" value="" />
<c:forEach var="univDiv" items="${form.univDiv}">
  <c:if test="${ univDiv == '01' }"><c:set var="c1" value=" checked" /></c:if>
  <c:if test="${ univDiv == '02' }"><c:set var="c2" value=" checked" /></c:if>
  <c:if test="${ univDiv == '03' }"><c:set var="c3" value=" checked" /></c:if>
  <c:if test="${ univDiv == '04' }"><c:set var="c4" value=" checked" /></c:if>
</c:forEach>
<td width="20"><input type="checkbox" name="univDiv" value="01"<c:out value="${c1}" />></td>
<td width="40"><span class="text12">国公立</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="02"<c:out value="${c2}" />></td>
<td width="40"><span class="text12">私立</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="03"<c:out value="${c3}" />></td>
<td width="40"><span class="text12">短大</span></td>
<td width="20"><input type="checkbox" name="univDiv" value="04"<c:out value="${c4}" />></td>
<td width="40"><span class="text12">その他</span></td>
</tr>
</table>
</td>
</tr>
</table>
<!--選択-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
<!-- 
</td>
</tr>
</table>
</td>
</tr> -->
<!-- </table> -->
<!--/国私区分選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%" bgcolor="white" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/spacer-->

<!--模試・志望大学検索-->
<!-- <table border="0" cellpadding="0" cellspacing="0" width="591"> -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<!-- <tr>
<td bgcolor="#8CA9BB">
 <table border="0" cellpadding="9" cellspacing="1" width="591"> -->
 <tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><b class="text14-hh">模試・志望大学検索</b></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="549">
<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="3" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="4" cellspacing="0">
<tr>
<td align="center"><input type="submit" name="button" value="模試・志望大学検索" class="text12" style="width:189px;" onClick="pushed=true"></td>
</tr>
<tr valign="top">
<td align="center"><span class="text12">※生徒が模試で志望している大学の一覧を表示します。</span></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<!--/模試・志望大学検索-->

</c:otherwise>

</c:choose>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<a name="colsearch"></a>
<!--大学名・大学コード検索-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><span class="text14-hh">大学名で検索します</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--検索-->
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="184" valign="top">
<!--直接入力-->
<table border="0" cellpadding="5" cellspacing="0" width="184">
<tr>
<td width="184" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名直接入力</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="184" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="184">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="184">
<tr valign="top">
<td width="182" height="195" bgcolor="#FBD49F" align="center">
<!--大学名orコード-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td><input type="hidden" name="searchMode" value="name">&nbsp;</td>
<!--
<td width="23"><input type="radio" name="searchMode" value="name"<c:if test="${ form.searchMode == 'name' }"> checked</c:if>></td>
<td width="59"><span class="text12">大学名</span></td>
<td width="23"><input type="radio" name="searchMode" value="code"<c:if test="${ form.searchMode == 'code' }"> checked</c:if>></td>
<td width="60"><span class="text12">大学コード</span></td>
-->
</tr>
</table>
</div>
<!--/大学名orコード-->
<!--テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td width="165"><input type="text" size="20" class="text12" name="searchStr" style="width:165px;" value="<c:out value="${form.searchStr}" />"></td>
</tr>
</table>
</div>
<!--/テキストボックス-->
<!--検索ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td align="center"><input type="submit" name="button" value="検索" class="text12" style="width:85px;" onClick="return check()">
</td>
</tr>
</table>
</div>
<!--/検索ボタン-->
<!--注意-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<td><span class="text12">大学名はひらがなで<br>入力してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/直接入力-->
</td>
<td width="22" align="center"><b class="text12">or</b></td>
<td width="337" valign="top">
<!--大学名頭文字指定-->
<table border="0" cellpadding="5" cellspacing="0" width="337">
<tr>
<td width="337" bgcolor="#CDD7DD"><b class="text12">&nbsp;大学名頭文字指定</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="337" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="337">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="337">
<tr valign="top">
<td width="335" height="195" bgcolor="#FBD49F" align="center">
<!--リスト-->
<%@ include file="/jsp/com_set/searchChar.jsp" %>
<!--/リスト-->
<!--注意-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="335">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="325"><span class="text12">※大学名の頭文字を選択してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大学名頭文字指定-->
</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--検索-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大学名・大学コード検索-->
<!--/●●●右側●●●-->
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

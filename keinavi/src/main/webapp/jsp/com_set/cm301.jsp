<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/com_close.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	// チェック数のチェック
	function compYearChecked(obj) {
		var count = 0;
		var e = document.forms[0].compYear;
		if (e.length) {
			for (var i=0; i<e.length; i++) {
				if (e[i].checked) count++;
			}
			if (count > 4) {
				obj.checked = false;
				alert("<kn:message id="cm001a" />");
				return;
			}
		}
	}

	function init() {
		startTimer();
	}

// -->
</script>
</head>
<body onload="init();" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonCompYear" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">
<input type="hidden" name="last1" value="<c:out value="${CompYearBean.compYearList[0].year}" />">
<input type="hidden" name="last2" value="<c:out value="${CompYearBean.compYearList[1].year}" />">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--比較対象年度の選択-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_year.gif" width="30" height="30" border="0" alt="比較対象年度の選択"><br></td>
<td width="751"><b class="text16" style="color:#657681">比較対象年度の選択</b></td>
</tr>
</table>
</div>
<!--/中タイトル-->


<!--概要-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<%-- 比較対象年度がない場合 --%>
<c:choose>
<c:when test="${ CompYearBean.compYearList[0].year < CurrentYear - 6 }">
	<!--説明-->
	<div style="margin-top:6px;">
	<table border="0" cellpadding="0" cellspacing="0" width="770">
	<tr valign="top">
	<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="748"><b class="text14-hh" style="color:#F33"><kn:message id="cm014a" /></b></td>
	</tr>
	</table>
	</div>
	<!--/説明-->
</c:when>
<%-- 比較対象年度がある場合 --%>
<c:otherwise>
	<!--説明-->
	<div style="margin-top:6px;">
	<table border="0" cellpadding="0" cellspacing="0" width="770">
	<tr valign="top">
	<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
	<td width="748"><span class="text14-hh">本年度と合わせて比較したい年度を選択してください。（全4年度まで選択可能）</span></td>
	</tr>
	</table>
	</div>
	<!--/説明-->
	<!--spacer-->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
	<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
	</tr>
	</table>
	<!--/spacer-->

	<table border="0" cellpadding="0" cellspacing="0" width="770">
	<tr valign="top">
	<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
	<td width="750">
	<!--リスト-->
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
	<c:set var="c0" value="" />
	<c:set var="c1" value="" />
	<c:set var="c2" value="" />
	<c:set var="c3" value="" />
	<c:set var="c4" value="" />
	<c:set var="c5" value="" />
	<%-- NULLならはじめの２つがチェック  --%>
	<c:if test="${ CommonForm.actionForms.CM301Form.compYear == null }">
		<c:set var="c0" value=" checked" />
		<c:set var="c1" value=" checked" />
	</c:if>
	<c:forEach var="year" items="${CommonForm.actionForms.CM301Form.compYear}">
		<c:if test="${ year == 1 }"><c:set var="c0" value=" checked" /></c:if>
		<c:if test="${ year == 2 }"><c:set var="c1" value=" checked" /></c:if>
		<c:if test="${ year == 3 }"><c:set var="c2" value=" checked" /></c:if>
		<c:if test="${ year == 4 }"><c:set var="c3" value=" checked" /></c:if>
		<c:if test="${ year == 5 }"><c:set var="c4" value=" checked" /></c:if>
		<c:if test="${ year == 6 }"><c:set var="c5" value=" checked" /></c:if>
	</c:forEach>
	<td><input type="checkbox" name="compYear" value="1" onClick="compYearChecked(this);formChanged()"<c:out value="${c0}" />></td>
	<td><span class="text12"><c:out value="${ CompYearBean.compYearList[0].year }年度（${ CompYearBean.compYearList[0].examinees }人）" /></span></td>
	<td><c:if test="${ CompYearBean.compYearList[1].year >= CurrentYear - 6 }"><input type="checkbox" name="compYear" value="2" onClick="compYearChecked(this);formChanged()"<c:out value="${c1}" />></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[1].year >= CurrentYear - 6 }"><span class="text12"><c:out value="${ CompYearBean.compYearList[1].year }年度（${ CompYearBean.compYearList[1].examinees }人）" /></span></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[2].year >= CurrentYear - 6 }"><input type="checkbox" name="compYear" value="3" onClick="compYearChecked(this);formChanged()"<c:out value="${c2}" />></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[2].year >= CurrentYear - 6 }"><span class="text12"><c:out value="${ CompYearBean.compYearList[2].year }年度（${ CompYearBean.compYearList[2].examinees }人）" /></span></c:if></td>
	</tr>
	<tr>
	<td><c:if test="${ CompYearBean.compYearList[3].year >= CurrentYear - 6 }"><input type="checkbox" name="compYear" value="4" onClick="compYearChecked(this);formChanged()"<c:out value="${c3}" />></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[3].year >= CurrentYear - 6 }"><span class="text12"><c:out value="${ CompYearBean.compYearList[3].year }年度（${ CompYearBean.compYearList[3].examinees }人）" /></span></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[4].year >= CurrentYear - 6 }"><input type="checkbox" name="compYear" value="5" onClick="compYearChecked(this);formChanged()"<c:out value="${c4}" />></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[4].year >= CurrentYear - 6 }"><span class="text12"><c:out value="${ CompYearBean.compYearList[4].year }年度（${ CompYearBean.compYearList[4].examinees }人）" /></span></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[5].year >= CurrentYear - 6 }"><input type="checkbox" name="compYear" value="6" onClick="compYearChecked(this);formChanged()"<c:out value="${c5}" />></c:if></td>
	<td><c:if test="${ CompYearBean.compYearList[5].year >= CurrentYear - 6 }"><span class="text12"><c:out value="${ CompYearBean.compYearList[5].year }年度（${ CompYearBean.compYearList[5].examinees }人）" /></span></c:if></td>
	</tr>
	</table>
	<!--/リスト-->
	</td>
	</tr>
	</table>
</c:otherwise>
</c:choose>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--注意-->
<c:if test="${ CompYearBean.compYearList[0].year >= CurrentYear - 6 }">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="790">
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><span class="text12">※本年度分は自動的に出力します。（）内は受験者数。</span></td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</c:if>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/比較対象年度の選択-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;設定完了（すべての共通項目設定を反映します）&nbsp;" class="text12" style="width:340px;" onClick="submitRegist(0)">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="windowClose()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</body>
</html>

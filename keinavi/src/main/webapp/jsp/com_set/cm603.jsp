<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set var="form" value="${CommonForm.actionForms.CM604Form}" scope="request" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/Validator.js"></script>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/area_script.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	// ボタンを押したらTRUEにする
	var pushed = false;

	function check() {
		// 入力チェック用オブジェクト
		var val = new Validator();

		// 高校名はひらがな
		if (document.forms[0].searchMode[0].checked) {
			if (!val.isHiragana(document.forms[0].searchStr.value)) {
				alert("<kn:message id="cm009a" />");
				return false;
			}

		// 高校コードは５桁の数字
		} else {
			if (!val.isNumber(document.forms[0].searchStr.value, 5)) {
				alert("<kn:message id="cm010a" />");
				return false;
			}
		}

		// ボタンは押されたことになる
		pushed = true;
	}

	function init() {
		startTimer();
		<c:if test="${ param.searchconf != null }">
			if(<c:out value="${param.searchconf}" /> == 1) {
				window.location = '#colsearch';
			}
		</c:if>
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonCompSchoolSearch" />" method="POST" onSubmit="return pushed">
<input type="hidden" name="forward" value="cm604">
<input type="hidden" name="backward" value="cm603">
<input type="hidden" name="searchChar" value="">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--比較対象高校-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校"><br></td>
<td width="751"><b class="text16" style="color:#657681">比較対象高校</b></td>
</tr>
</table>
</div>
<!--/中タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<table border="0" cellpadding="6" cellspacing="1" width="183" bgcolor="#8CA9BB">
<tr>
<td bgcolor="#FBD49F" align="center"><input type="button" value="一覧に戻る" onclick="submitMenu('cm601');"></td>
</tr>
</table>

<!--新規追加-->
<div style="margin-top:12px;">
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./com_set/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="160">
<b class="text14" style="color:#FFFFFF;">新規追加</b><br>
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="160">
<tr>
<td width="160" height="27" bgcolor="#EFF2F3" align="center">
<%-- カウント --%>
<c:set var="count" value="20" />
<%-- 営業は300まで --%>
<c:if test="${LoginSession.sales}"><c:set var="count" value="300" /></c:if>
<c:forEach items="${CommonForm.actionForms.CM601Form.school}">
  <c:set var="count" value="${ count - 1}" />
</c:forEach>
<!--文言--><span class="text12">あと<b style="color:#FF8F0E;"><c:out value="${count}" />校</b>の登録が可能です</span>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</div>

</td>
</tr>
</table>
</div>
<!--/新規追加-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step1_cur.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">検　索</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step2_def.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">選　択</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="16"><img src="./shared_lib/img/parts/sp.gif" width="16" height="1" border="0" alt=""><br></td>
<td width="591">

<!--●●●右側●●●-->
<!--地域・国私区分選択-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><span class="text14-hh">地域・公私区分を選択後、下記いずれかの方法で検索してください。</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="551"><b class="text14-hh">&nbsp;1.対象地区の選択</b><br>
<!--対象地区の選択-->
<%@ include file="/jsp/com_set/area_table.jsp" %>
<!--/対象地区の選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<b class="text14-hh">&nbsp;2.その他絞り込み条件の選択</b><br>

<!--その他絞り込み条件の選択-->
<table border="0" cellpadding="5" cellspacing="2" width="547">
<!--公私区分-->
<tr>
<td width="22%" bgcolor="#E1E6EB"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><span class="text12">公私区分</span></td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<c:set var="c1" value="" />
<c:set var="c2" value="" />
<c:forEach var="schoolDiv" items="${form.schoolDiv}">
  <c:if test="${ schoolDiv == '01' }"><c:set var="c1" value=" checked" /></c:if>
  <c:if test="${ schoolDiv == '02' }"><c:set var="c2" value=" checked" /></c:if>
</c:forEach>
<td width="20"><input type="checkbox" name="schoolDiv" value="01"<c:out value="${c1}" />></td>
<td width="55"><span class="text12">国公立</span></td>
<td width="20"><input type="checkbox" name="schoolDiv" value="02"<c:out value="${c2}" />></td>
<td width="55"><span class="text12">私立</span></td>
<td width="20"></td>
<td width="55"></td>
<td width="20"></td>
<td width="55"></td>
<td width="20"></td>
<td width="55"></td>
</tr>
</table>
</td>
</tr>
<!--/公私区分-->

<%-- 営業のみ --%>
<!--ランク-->
<c:if test="${LoginSession.sales}">
<tr>
<td width="22%" bgcolor="#E1E6EB"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><span class="text12">ランク</span></td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<c:set var="r1" value="" />
<c:set var="r2" value="" />
<c:set var="r3" value="" />
<c:set var="r4" value="" />
<c:set var="r5" value="" />
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD START --%>
<c:set var="r6" value="" />
<c:set var="r7" value="" />
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD END --%>
<c:forEach var="rank" items="${form.rank}">
  <c:if test="${ rank == 'A' }"><c:set var="r1" value=" checked" /></c:if>
  <c:if test="${ rank == 'B' }"><c:set var="r2" value=" checked" /></c:if>
  <c:if test="${ rank == 'C' }"><c:set var="r3" value=" checked" /></c:if>
  <c:if test="${ rank == 'D' }"><c:set var="r4" value=" checked" /></c:if>
  <c:if test="${ rank == 'E' }"><c:set var="r5" value=" checked" /></c:if>
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD START --%>
  <c:if test="${ rank == 'N' }"><c:set var="r6" value=" checked" /></c:if>
  <c:if test="${ rank == 'なし' }"><c:set var="r7" value=" checked" /></c:if>
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD END --%>
</c:forEach>
<td width="20"><input type="checkbox" name="rank" value="A"<c:out value="${r1}" />></td>
<td width="55"><span class="text12">A</span></td>
<td width="20"><input type="checkbox" name="rank" value="B"<c:out value="${r2}" />></td>
<td width="55"><span class="text12">B</span></td>
<td width="20"><input type="checkbox" name="rank" value="C"<c:out value="${r3}" />></td>
<td width="55"><span class="text12">C</span></td>
<td width="20"><input type="checkbox" name="rank" value="D"<c:out value="${r4}" />></td>
<td width="55"><span class="text12">D</span></td>
<td width="20"><input type="checkbox" name="rank" value="E"<c:out value="${r5}" />></td>
<td width="55"><span class="text12">E</span></td>
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD START --%>
<td width="20"><input type="checkbox" name="rank" value="N"<c:out value="${r6}" />></td>
<td width="55"><span class="text12">N</span></td>
<td width="20"><input type="checkbox" name="rank" value="なし"<c:out value="${r7}" />></td>
<td width="55"><span class="text12">なし</span></td>
<%-- 2019/07/19 QQ)Kitaoka システム企画 ADD END --%>
</tr>
</table>
</td>
</tr>
<!--/ランク-->
</c:if>

<!--受験人数-->
<tr>
<td width="22%" bgcolor="#E1E6EB"><img src="./shared_lib/img/parts/sp.gif" width="4" height="1" border="0" alt=""><span class="text12">受験人数</span></td>
<td width="78%" bgcolor="#F4E5D6">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td width="20"><input type="radio" name="examinees" value="all"<c:if test="${ form.examinees == 'all' }"> checked</c:if>></td>
<td width="55"><span class="text12">すべて</span></td>
<td width="20"><input type="radio" name="examinees" value="10"<c:if test="${ form.examinees == '10' }"> checked</c:if>></td>
<td width="55"><span class="text12">10人以上</span></td>
<td width="20"><input type="radio" name="examinees" value="50"<c:if test="${ form.examinees == '50' }"> checked</c:if>></td>
<td width="55"><span class="text12">50人以上</span></td>
<td width="20"><input type="radio" name="examinees" value="100"<c:if test="${ form.examinees == '100' }"> checked</c:if>></td>
<td width="55"><span class="text12">100人以上</span></td>
<td width="20"><input type="radio" name="examinees" value="200"<c:if test="${ form.examinees == '200' }"> checked</c:if>></td>
<td width="55"><span class="text12">200人以上</span></td>
</tr>
</table>
</td>
</tr>
<!--/受験人数-->
</table>
<!--その他絞り込み条件の選択-->

<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="5" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="3" cellspacing="0">
<tr>
<td width="20"><input type="radio" name="view" value="all"<c:if test="${ form.view == 'all' }"> checked</c:if>></td>
<td width="80"><span class="text12">すべて表示</span></td>
<td width="20"><input type="radio" name="view" value="school"<c:if test="${ form.view == 'school' }"> checked</c:if>></td>
<td width="100"><span class="text12">高校のみ表示</span></td>
<td width="20"><input type="radio" name="view" value="average"<c:if test="${ form.view == 'average' }"> checked</c:if>></td>
<td width="130"><span class="text12">都道府県平均のみ表示</span></td>
</tr>
<tr>
<td colspan="6" align="center"><input type="submit" name="button" value="上記の条件で検索" class="text12" style="width:189px;" onClick="pushed=true"></td>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/地域・国私区分選択-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<a name="colsearch"></a>
<!--高校名・高校コード検索-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><b class="text14-hh">高校名・高校コード検索</b>　<span class="text12">※下記いずれかの方法で検索してください</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--検索-->
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="22"><img src="./shared_lib/img/parts/sp.gif" width="22" height="1" border="0" alt=""><br></td>
<td width="184" valign="top">
<!--直接入力-->
<table border="0" cellpadding="5" cellspacing="0" width="184">
<tr>
<td width="184" bgcolor="#CDD7DD"><b class="text12">&nbsp;高校名・高校コード直接入力</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="184" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="184">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="184">
<tr valign="top">
<td width="182" height="195" bgcolor="#FBD49F" align="center">
<!--高校名orコード-->
<div style="margin-top:30px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td width="23"><input type="radio" name="searchMode" value="name"<c:if test="${ form.searchMode == 'name' }"> checked</c:if>></td>
<td width="59"><span class="text12">高校名</span></td>
<td width="23"><input type="radio" name="searchMode" value="code"<c:if test="${ form.searchMode == 'code' }"> checked</c:if>></td>
<td width="60"><span class="text12">高校コード</span></td>
</tr>
</table>
</div>
<!--/高校名orコード-->
<!--テキストボックス-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td width="165"><input type="text" size="20" class="text12" name="searchStr" style="width:165px;" value="<c:out value="${form.searchStr}" />"></td>
</tr>
</table>
</div>
<!--/テキストボックス-->
<!--検索ボタン-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="165">
<tr>
<td align="center"><input type="submit" name="button" value="検索" class="text12" style="width:85px;" onClick="return check()">
</td>
</tr>
</table>
</div>
<!--/検索ボタン-->
<!--注意-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td><span class="text12">※</span></td>
<td><span class="text12">高校名はひらがなで<br>入力してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/直接入力-->
</td>
<td width="22" align="center"><b class="text12">or</b></td>
<td width="337" valign="top">
<!--高校名頭文字指定-->
<table border="0" cellpadding="5" cellspacing="0" width="337">
<tr>
<td width="337" bgcolor="#CDD7DD"><b class="text12">&nbsp;高校名頭文字指定</b></td>
</tr>
</table>
<img src="./shared_lib/img/parts/sp.gif" width="337" height="2" border="0" alt=""><br>

<table border="0" cellpadding="0" cellspacing="0" width="337">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="0" cellspacing="1" width="337">
<tr valign="top">
<td width="335" height="195" bgcolor="#FBD49F" align="center">
<!--リスト-->
<%@ include file="/jsp/com_set/searchChar.jsp" %>
<!--/リスト-->
<!--注意-->
<div style="margin-top:5px;">
<table border="0" cellpadding="0" cellspacing="0" width="335">
<tr>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="325"><span class="text12">※高校名の頭文字を選択してください</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/高校名頭文字指定-->
</td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--検索-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/高校名・高校コード検索-->
<!--/●●●右側●●●-->
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/比較対象高校-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

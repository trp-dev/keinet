<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/ArrayList.js"></script>
<script type="text/javascript" src="./js/OrderChange.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/com_close.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	<%-- 2015/12/16 QQ)Nishiyama 大規模改修 ADD START --%>
	// フォームユーティリティ
	var util = new FormUtil();
	<%-- 2015/12/16 QQ)Nishiyama 大規模改修 ADD END   --%>

	// クラスデータ（全選択）
	var sc1 = new Array();
	<c:forEach var="class" items="${ClassBean.partList}" varStatus="status">
		sc1[<c:out value="${status.index}" />] = new Array(
			"<c:out value="${class.key}" />",
			"<c:out value="${class.className}" />",
			<c:out value="${class.grade}" />,
			<c:out value="${class.examinees}" />);
	</c:forEach>

	// クラスデータ（個別選択）
	var sc2 = new Array();
	<c:forEach var="class" items="${ClassBean.fullList}" varStatus="status">
		sc2[<c:out value="${status.index}" />] = new Array(
			"<c:out value="${class.key}" />",
			"<c:out value="${class.className}" />",
			<c:out value="${class.grade}" />,
			<c:out value="${class.examinees}" />);
	</c:forEach>

	// 選択状態を保持する配列
	var comp2 = new Array();
	var graph1 = new Array();
	var graph2 = new Array();
	// 背景色を保持する配列
	var bg1 = new Array();
	var bg2 = new Array();

	<c:forEach var="class" items="${ClassBean.partList}">
		<c:set var="s1" value="false" />
		<c:forEach var="key" items="${CommonForm.actionForms.CM501Form.graph1}">
			<c:if test="${ class.key == key }"><c:set var="s1" value="true" /></c:if>
		</c:forEach>
		graph1["<c:out value="${class.key}" />"] = <c:out value="${s1}" />;
		bg1["<c:out value="${class.key}" />"] = <c:out value="${s1}" />;
	</c:forEach>

	<c:forEach var="class" items="${ClassBean.fullList}">
		<c:set var="s1" value="false" />
		<c:set var="s2" value="false" />
		<c:forEach var="key" items="${CommonForm.actionForms.CM501Form.compare2}">
			<c:if test="${ class.key == key }"><c:set var="s1" value="true" /></c:if>
		</c:forEach>
		<c:forEach var="key" items="${CommonForm.actionForms.CM501Form.graph2}">
			<c:if test="${ class.key == key }"><c:set var="s2" value="true" /></c:if>
		</c:forEach>
		comp2["<c:out value="${class.key}" />"] = <c:out value="${s1}" />;
		graph2["<c:out value="${class.key}" />"] = <c:out value="${s2}" />;
		bg2["<c:out value="${class.key}" />"] = <c:out value="${s2}" />;
	</c:forEach>

	// レイヤ操作クラス
	var sw = new SwitchLayer();

	// ソートクラス（全選択）
	var oc1 = new OrderChange(0);
	oc1.setIndex(0);
	if (sc1.length > 0) oc1.setKey(sc1[0][0]);
	oc1.setData(sc1);

	// ソートクラス（個別選択）
	var oc2 = new OrderChange(0);
	if (sc2.length > 0) oc2.setKey(sc2[0][0]);
	oc2.setData(sc2);

	function addHidden(name, value) {
		var body = document.getElementsByTagName("FORM").item(0);

		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD START --%>
        var input = document.createElement("input");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", name);
        input.setAttribute("value", value);

		// body.appendChild(document.createElement("<input type=\"hidden\" name=\"" + name + "\" value=\"" + value + "\">"));
		body.appendChild(input);
		<%-- 2015/12/18 QQ)Hisakawa 大規模改修 UPD END   --%>
	}

	function disableElements(name) {
		var e =  document.forms[0].elements[name];
		if (e == null) return;

		if (e.length) {
			for (var i=0; i<e.length; i++) {
				e[i].disabled = "disabled";
			}
		} else {
			e.disabled = "disabled";
		}
	}

	// 順序通りにフォームデータを作る
	function setupForm() {
		initSelectData();

		var form = document.forms[0];
		var data = null;

		disableElements("compare1");
		disableElements("compare2");
		disableElements("graph1");
		disableElements("graph2");

		// 全選択
		data = oc1.getData();
		for (var i=0; i<data.length; i++) {
			// 比較対象
			addHidden("compare1", data[i][0]);
			// グラフ表示対象
			if (graph1[data[i][0]]) {
				addHidden("graph1", data[i][0]);
			}
		}

		// 個別選択
		var count = 0;
		data = oc2.getData();
		for (var i=0; i<data.length; i++) {
			// 比較対象
			if (comp2[data[i][0]]) {
				addHidden("compare2", data[i][0]);
				count++;
			}
			// グラフ表示対象
			if (graph2[data[i][0]]) {
				addHidden("graph2", data[i][0]);
			}
		}
	}

	//「対象選択」「表示順変更」の切替
	function changeTab(mode) {
		initSelectData();

		if (mode) {
			if (document.forms[0].elements["selection"][0].checked) drawTable2All();
			else drawTable2Select();
			sw.showLayer("Layer2");
			sw.hideLayer("Layer1");

			<%-- 2016/02/12 QQ)Hisakawa 大規模改修 ADD START --%>
			sw.hideLayer("ctrlTable1");
			sw.hideLayer("ctrlTable2");
			<%-- 2016/02/12 QQ)Hisakawa 大規模改修 ADD END   --%>
		} else {
			<%-- 2016/02/12 QQ)Hisakawa 大規模改修 UPD START --%>
			// if (document.forms[0].elements["selection"][0].checked) drawTable1All();
			// else drawTable1Select();

			if (document.forms[0].elements["selection"][0].checked) {
				drawTable1All();
				sw.showLayer("ctrlTable1");
			} else {
				drawTable1Select();
				sw.showLayer("ctrlTable2");
			}
			<%-- 2016/02/12 QQ)Hisakawa 大規模改修 UPD END   --%>

			sw.showLayer("Layer1");
			sw.hideLayer("Layer2");
		}
	}

	function change(obj) {
		initSelectData();

		var color1 = "#FAFAFA";
		var color2 = "#FAFAFA";
		if (obj.value == "1") {
			color1 = "#FFAD8C";
			drawTable1All();
			drawTable2All();
			sw.getLayer("Info").innerHTML = "受験者のいる全クラスを比較対象クラスに設定しています。この中からグラフ表示にするクラスを選択してください。<br>比較対象クラスを限定したり順番を変更する場合、複合クラスを使用する場合は「選択方法」を切り替えてください。";
			sw.hideLayer("ctrlTable2");
			sw.showLayer("ctrlTable1");
		} else if (obj.value == "2") {
			color2 = "#FFAD8C";
			drawTable1Select();
			drawTable2Select();
			sw.getLayer("Info").innerHTML = "比較対象とするクラスを選択し、さらにグラフ表示をさせたいクラスを選択してください。<br>また必要に応じて、順番変更をすることもできます。（※比較対象の選択をせずにグラフ表示に設定することはできません）";
			sw.hideLayer("ctrlTable1");
			sw.showLayer("ctrlTable2");
		}

		<%-- 2016/02/12 QQ)Hisakawa 大規模改修 ADD START --%>
		if($("#Layer2").css("visibility") == "visible") {
			sw.hideLayer("ctrlTable1");
			sw.hideLayer("ctrlTable2");
		}
		<%-- 2016/02/12 QQ)Hisakawa 大規模改修 ADD END   --%>

		var cur  = new DOMUtil(obj);
		<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD START --%>
		//cur.getParent(2).childNodes[2].style.backgroundColor = color1;
		//cur.getParent(2).childNodes[6].style.backgroundColor = color2;

		cur.getParent(2).children[2].style.backgroundColor = color1;
		cur.getParent(2).children[6].style.backgroundColor = color2;
		<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD END   --%>
	}

	function changeState(obj) {
		// 制限数のチェック
		if (obj.checked) {
			var count = 0;
			for (var i=0; i<sc2.length; i++) {
				if (comp2[sc2[i][0]]) count++;
			}
			if (count >= 40) {
				obj.checked = false;
				alert("<kn:message id="cm006a" />");
				return;
			}
		}

		// 比較対象の状態によりグラフ表示対象も変化させる
		var disabled = obj.checked ? false : "disabled";
//		var checkbox = new DOMUtil(obj).getParent(2).childNodes[2].childNodes[0];
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
//		var checkbox = new DOMUtil(obj).getParent(1).nextSibling.childNodes[0];
//		var checkbox = new DOMUtil(obj).getParent(1).nextSibling.children[0];
		var checkbox = new DOMUtil(obj).getParent(1).nextElementSibling.childNodes[0];
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END   --%>
		checkbox.disabled = disabled;

		// 分析対象がチェックされていなければグラフ表示対象も外す
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
		//if (!obj.checked) checkbox.checked = false;
		if (obj.checked) {
			checkbox.checked = true;
		} else {
			checkbox.checked = false;
		}
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END   --%>

		// 背景色も変える
		changeColor(checkbox);
		// 選択状態を保持
		comp2[obj.value] = obj.checked;
	}

	function changeColor(obj) {
/*
		var color = obj.checked ? "#FFAD8C" : "#F4E5D6";
		var cur  = new DOMUtil(obj);
		cur.getParent(2).childNodes[1].style.backgroundColor = color;
		cur.getParent(2).childNodes[2].style.backgroundColor = color;
		cur.getParent(2).childNodes[3].style.backgroundColor = color;
		cur.getParent(2).childNodes[4].style.backgroundColor = color;
*/
		// 選択状態を保持
		if (obj.name == "graph") graph1[obj.value] = obj.checked;
		else graph2[obj.value] = obj.checked;
	}

	//「対象選択」テーブルの描画
	function drawTable1(school) {
		var mode = document.forms[0].elements["selection"][0].checked;
		var table = sw.getLayer("Table1");
		var row = <c:out value="${ClassBean.fullListHalfSize}" />;
		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>", "<BR>");
				// デフォルトの背景色
				var color = "#F4E5D6";
				if (school.length >= index) {
					// 全クラスの場合
					if (mode) {
						var checked = graph1[school[index - 1][0]] ? " checked" : "";
						data[0] = "<input type=\"checkbox\" checked disabled=disabled>"; // 比較対象
						data[1] = "<input type=\"checkbox\" name=\"graph\" value=\""
							+ school[index - 1][0] + "\" onClick=\"changeColor(this);formChanged()\"" + checked + ">"; // グラフ表示
						// 背景色
						if (bg1[school[index - 1][0]]) color = "#FFAD8C";
					// 選択の場合
					} else {
						var checked1 = comp2[school[index - 1][0]] ? " checked" : "";
						data[0] = "<input type=\"checkbox\" name=\"comp2\" value=\""
							+ school[index - 1][0] + "\" onClick=\"changeState(this);formChanged()\"" + checked1 + ">"; // 比較対象
						// 分析対象でなければ操作不可
						var disabled = checked1 == "" ? " disabled=disabled" : "";
						var checked2 = graph2[school[index - 1][0]] ? " checked" : "";
						data[1] = "<input type=\"checkbox\" value=\""
							+ school[index - 1][0] + "\" onClick=\"changeColor(this);formChanged()\"" + checked2 + disabled + ">"; // グラフ表示
						// 背景色
						if (bg2[school[index - 1][0]]) color = "#FFAD8C";
					}
					data[2] = school[index - 1][1]; // クラス名
					data[3] = school[index - 1][2] + "年"; // 学年
					data[4] = school[index - 1][3] + "人"; // 受験人数
				}
				var gap = 6 * j;
				// 背景色
				cur.getChild(gap + 1).style.backgroundColor = color;
				cur.getChild(gap + 2).style.backgroundColor = color;
				cur.getChild(gap + 3).style.backgroundColor = color;
				cur.getChild(gap + 4).style.backgroundColor = color;
				// データを入れる
				cur.getChild(++gap).innerHTML = data[0];
				cur.getChild(++gap).innerHTML = data[1];
				cur.getChild(++gap, 0).innerHTML = data[3];
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[2];
				cur.getChild(gap, 0, 0, 0, 1, 0).innerHTML = data[4];
			}
		}
	}

	function drawTable1All() {
		drawTable1(oc1.getData());
	}

	function drawTable1Select() {
		drawTable1(oc2.getData());
	}

	// 比較対象クラスのデータを初期化する
	function initSelectData() {
		var data = oc2.getData();
		var container = new ArrayList();

		// 選択データを詰める
		var selected = new Array();
		for (var i=0; i<data.length; i++) {
			container.add(data[i]);
			selected[data[i][0]] = true;
		}

		for (var i=0; i<sc2.length; i++) {
			if (!selected[sc2[i][0]]) container.add(sc2[i]);
		}

		oc2.setData(container.toArray());
	}

	// 比較対象クラスのデータを取得する
	function getSelectData() {
		var container = new Array(0);
		var data = oc2.getData();
		var index = 0;

		for (var i=0; i<data.length; i++) {
			if (comp2[data[i][0]]) {
				container[index++] = data[i];
			}
		}

		return container;
	}

	// 「表示順変更」テーブルの描画
	function drawTable2(school, key) {
		var mode = document.forms[0].elements["selection"][0].checked;
		var table = sw.getLayer("Table2");
		var row = <c:out value="${ClassBean.fullListHalfSize}" />;

		for (var i=1; i<=row; i++) {
			var cur = new DOMUtil(new DOMUtil(table).getChild(0, i));
			for (var j=0; j<2; j++) {
				var index = i + row * j;
				var data = new Array("<BR>", "<BR>", "<BR>", "<BR>");
				var gap = 5 * j;
				var color = "#F4E5D6";
				if (school.length >= index) {
					// ラジオボタン
					var checked = school[index - 1][0] == key ? " checked" : "";
					var oc = mode ? "oc1" : "oc2";
					data[0] = "<input type=\"radio\" name=\"order\" value=\"" + school[index - 1][0]
						+ "\" onClick=\""+ oc + ".setKey(this.value)\"" + checked + ">";
					data[1] = school[index - 1][1]; // クラス名
					data[2] = school[index - 1][2] + "年"; // 学年
					data[3] = school[index - 1][3] + "人"; // 受験人数
					// グラフ表示対象の場合の背景色
					if (mode) {
						if (bg1[school[index - 1][0]]) color = "#FFAD8C";
					} else {
						if (bg2[school[index - 1][0]]) color = "#FFAD8C";
					}
				}
				cur.getChild(gap + 1).style.backgroundColor = color;
				cur.getChild(gap + 2).style.backgroundColor = color;
				cur.getChild(gap + 3).style.backgroundColor = color;
				cur.getChild(++gap).innerHTML = data[0];
				cur.getChild(++gap, 0).innerHTML = data[2];
				cur.getChild(++gap, 0, 0, 0, 0, 0).innerHTML = data[1];
				cur.getChild(gap, 0, 0, 0, 1, 0).innerHTML = data[3];
			}
		}
	}

	function drawTable2All() {
		drawTable2(oc1.getData(), oc1.getKey());
	}

	function drawTable2Select() {
		oc2.setData(getSelectData());
		if (oc2.getData().length > 0) oc2.setKey(oc2.getData()[0][0]);
		drawTable2(oc2.getData(), oc2.getKey());
	}

	function up () {
		if (document.forms[0].elements["selection"][0].checked) {
			oc1.up();
			drawTable2(oc1.getData(), oc1.getKey());
		} else {
			oc2.up();
			drawTable2(oc2.getData(), oc2.getKey());
		}
	}

	function down() {
		if (document.forms[0].elements["selection"][0].checked) {
			oc1.down();
			drawTable2(oc1.getData(), oc1.getKey());
		} else {
			oc2.down();
			drawTable2(oc2.getData(), oc2.getKey());
		}
	}

	function selectGraph1(obj) {
		for (var key in graph1) {
			graph1[key] = obj.checked;
		}
		drawTable1All();
	}

	function selectGraph2(obj) {
		initSelectData();
		for (var key in graph2) {
			graph2[key] = comp2[key] ? obj.checked : false;
		}
		drawTable1Select();
	}

	function selectComp2(obj) {
		// 制限数のチェック
		if (obj.checked) {
			var count = 0;
			for (var key in comp2) {
				count++;
			}
			if (count > 40) {
				alert("<kn:message id="cm006a" />");
				return;
			}
		}
		initSelectData();
		var e = document.forms[0].elements["comp2"];
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				e[i].checked = obj.checked;
				changeState(e[i]);
			}
		} else {
			<%-- 2016/03/02 QQ)Nishiyama 大規模改修 ADD START --%>
			// ヘッダのチェック状態をセットする
			e.checked = obj.checked;
			<%-- 2016/03/02 QQ)Nishiyama 大規模改修 ADD END   --%>
			changeState(e);
		}
		drawTable1Select();
	}

	function init() {
		startTimer();
		sw.showLayer("Layer1");
		sw.hideLayer("Layer2");
		// 選択方法
		var elements = document.forms[0].elements["selection"];
		if (elements[0].checked) {
			change(elements[0]);
		} else if (elements[1].checked) {
			change(elements[1]);
		}
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonCompClass" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--比較対象クラス-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_class.gif" width="30" height="30" border="0" alt="比較対象クラス"><br></td>
<td width="160"><b class="text16" style="color:#657681">比較対象クラス</b></td>
<td width="591">
<!--選択方法-->
<table border="0" cellpadding="0" cellspacing="0" width="591" height="38">
<tr>
<td rowspan="3" width="101"><img src="./com_set/img/ttl_select.gif" width="101" height="38" border="0" alt="選択方法"><br></td>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="489" bgcolor="FAFAFA"> --%>
<td width="489" height="36" bgcolor="FAFAFA">
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
<table border="0" cellpadding="0" cellspacing="0" width="489">
<tr height="30">
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="1" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM501Form.selection == 1 }"> checked</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="217" bgcolor="#FFAD8C">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="215"><span class="text12-lh">受験者のいる全クラスを対象にする<br>（※複合クラスは対象外）</span></td>
</tr>
</table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="2" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM501Form.selection == 2 }"> checked</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="218">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="216"><span class="text12-lh">対象クラスを選択する</span></td>
</tr>
</table>
</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/選択方法-->
</td>
</tr>
</table>
</div>
<!--/中タイトル-->

<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh" id="Info">受験者のいる全クラスを比較対象クラスに設定しています。この中からグラフ表示にするクラスを選択してください。<br>比較対象クラスを限定したり順番を変更する場合、複合クラスを使用する場合は「選択方法」を切り替えてください。<br></span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!-- レイヤ１ -->
<div id="Layer1" style="visibility:hidden">


<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr height="27">
<td>
	<div id="ctrlTable1" style="visibility:hidden">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
	<td><input type="checkbox" name="dummy" value="" onclick="selectGraph1(this);formChanged();"></td>
  --%>
	<td><input type="checkbox" name="dummy1" value="" onclick="selectGraph1(this);formChanged();"></td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td nowrap><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</div>
	<div id="ctrlTable2" style="visibility:hidden">
	<table border="0" cellpadding="3" cellspacing="0">
	<tr>
<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
	<td><input type="checkbox" name="dummy" value="" onclick="selectComp2(this);formChanged();"></td>
  --%>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
	<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, this.name);selectComp2(this);formChanged();"></td>
  --%>
	<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, this.name);selectComp2(this);formChanged();"></td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END --%>
	<td nowrap><b class="text12" style="color:#FFF;">比較対象をすべて選択</b></td>
	<td nowrap>&nbsp;&nbsp;</td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
	<td><input type="checkbox" name="dummy" value="" onclick="selectGraph2(this);formChanged();"></td>
  --%>
	<td><input type="checkbox" name="dummy2" value="" onclick="selectGraph2(this);formChanged();"></td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
	<td nowrap><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
	</tr>
	</table>
	</div>
</td>
<td align="right" nowrap><font class="text12" style="color:#FFFFFF;">
<b>対象選択</b>
&nbsp;｜&nbsp;
<a href="javascript:changeTab(1)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>表示順変更</b></a>
</font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table1">
<tr>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">比較<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
<td width="26%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">クラス名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">比較<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
<td width="27%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">クラス名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
</tr>

<c:if test="${ ClassBean.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${ClassBean.fullListHalfSize}">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + ClassBean.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td width="745" align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>


</div>
<!-- /レイヤ１ -->

<!-- レイヤ２ -->
<div id="Layer2" style="visibility:hidden">

<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<!---->
<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="0" cellspacing="0" width="733">
<tr>
<td width="80" align="center"><b class="text12" style="color:#FFFFFF;">表示順変更</b></td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▲" class="text12" style="width:35px;" onClick="up();formChanged()"></td>
</tr>
</table>
</td>
<td width="45">
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#FFFFFF"><input type="button" value="▼" class="text12" style="width:35px;" onClick="down();formChanged()"></td>
</tr>
</table>
</td>
<td width="353"><font class="text12" style="color:#FFFFFF;">※ラジオボタンで選択後▲▼で移動します。</font></td>
<td width="200" align="right"><font class="text12" style="color:#FFFFFF;">
<a href="javascript:changeTab(0)" style="color:#FF9;"><img src="./shared_lib/img/parts/arrow_right_white_g.gif" width="7" height="9" border="0" alt="→" align="absmiddle" hspace="4"><b>対象選択</b></a>
&nbsp;｜&nbsp;
<b>表示順変更</b>
</font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/-->

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table2">
<tr>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">表示順<br>変更</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
<td width="32%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">クラス名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">表示順<br>変更</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">学年</span></td>
<td width="33%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">クラス名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
</tr>

<c:if test="${ ClassBean.fullListHalfSize > 0 }">
<c:forEach var="i" begin="1" end="${ClassBean.fullListHalfSize}">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${i}" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + ClassBean.fullListHalfSize }" /></span></td>
<td bgcolor="#F4E5D6" align="center"></td>
<td bgcolor="#F4E5D6" align="center"><span class="text12"></span></td>
<td bgcolor="#F4E5D6">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"></span></td>
<td align="right" nowrap><span class="text12"></span></td>
</tr>
</table>
</td>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td width="745" align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->

</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>

<!-- /レイヤ２ -->


</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<!--/概要-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/比較対象クラス-->

<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;設定完了（すべての共通項目設定を反映します）&nbsp;" class="text12" style="width:340px;" onClick="submitRegist(0)">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="windowClose()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</body>
</html>

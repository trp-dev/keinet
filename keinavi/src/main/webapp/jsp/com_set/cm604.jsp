<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<c:set var="form" value="${CommonForm.actionForms.CM604Form}" scope="request" />
<%-- カウント --%>
<c:set var="count" value="20" />
<%-- 営業は300まで --%>
<c:if test="${LoginSession.sales}"><c:set var="count" value="300" /></c:if>
<c:set var="registed" value="0" />
<c:forEach items="${CommonForm.actionForms.CM601Form.school}">
  <c:set var="count" value="${ count - 1}" />
</c:forEach>
<c:forEach items="${CommonForm.actionForms.CM604Form.school}">
  <c:set var="count" value="${ count - 1}" />
  <c:set var="registed" value="${ registed + 1}" />
</c:forEach>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	// ページ表示段階での残カウント
	var count = <c:out value="${count}" />;
	// ページ表示段階でのチェック数
	var registed = <c:out value="${registed}" />;

	function submitPage(page) {
		document.forms[0].forward.value = "cm604";
		document.forms[0].backward.value = "cm604";
		document.forms[0].save.value = "0";
		document.forms[0].page.value = page;
		document.forms[0].submit();
	}

	function submitRegist(forward) {
		// 上限値チェック
		if (count < 0) {
			<c:choose>
				<%-- 営業 --%>
				<c:when test="${LoginSession.sales}">
					alert("<kn:message id="cm008a" />");
				</c:when>
				<%-- その他 --%>
				<c:otherwise>
					alert("<kn:message id="cm007a" />");
				</c:otherwise>
			</c:choose>
			return;
		}

		// 未選択チェック	
		if (registed == 0) {
			alert("<kn:message id="cm011a" />");
			return;
		}

		document.forms[0].changed.value = "1";
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "cm604";
		document.forms[0].save.value = "1";
		document.forms[0].submit();
	}

	function submitUnRegist(forward) {
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "cm604";
		document.forms[0].save.value = "0";
		document.forms[0].submit();
	}

	// 残カウントを増減させる
	function setCount(obj) {
		if (obj.checked) {
			count--;
			registed++;
		} else {
			count++;
			registed--;
		}
	}

	function selectAll(checked) {
		var e = document.forms[0].school;
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				e[i].checked = checked;
				setCount(e[i]);
			}
		} else {
			e.checked = checked;
			setCount(e);
		}
	}

	function init() {
		startTimer();
		<c:choose>
			<c:when test="${ form.button == '上記の条件で検索' }">
				document.forms[0].searchconf.value = "0";
			</c:when>
			<c:when test="${ form.button == '検索' }">
				<c:if test="${ form.searchMode == 'name' }">
					document.forms[0].searchconf.value = "1";
				</c:if>
				<c:if test="${ form.searchMode == 'code' }">
					document.forms[0].searchconf.value = "1";
				</c:if>
		</c:when>
		<c:otherwise>
			document.forms[0].searchconf.value = "1";
		</c:otherwise>
	</c:choose>
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonCompSchoolAdd" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">
<input type="hidden" name="page" value="<c:out value="${CommonForm.actionForms['CM604Form'].page}" />">
<input type="hidden" name="searchconf" value="">

<%-- チェック状態の判断のために表示している高校コードを渡す --%>
<c:forEach var="school" items="${SearchBean.recordSet}">
  <input type="hidden" name="master" value="<c:out value="${school.schoolCode}" />">
</c:forEach>

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--比較対象高校-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_school.gif" width="30" height="30" border="0" alt="比較対象高校"><br></td>
<td width="751"><b class="text16" style="color:#657681">比較対象高校</b></td>
</tr>
</table>
</div>
<!--/中タイトル-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr valign="top">
<td width="183">
<!--●●●左側●●●-->

<!--新規追加-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td colspan="2" width="171" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="171" height="1" border="0" alt=""><br></td>
<td rowspan="2" width="12" background="./com_set/img/tbl_cnr_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="170" bgcolor="#4694AF">

<div style="margin-top:8px;">
<table border="0" cellpadding="0" cellspacing="0" width="170">
<tr valign="top">
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td width="160">
<b class="text14" style="color:#FFFFFF;">新規追加</b><br>
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="160">
<tr>
<td width="160" height="27" bgcolor="#EFF2F3" align="center">
<!--文言--><span class="text12">あと<b style="color:#FF8F0E;"><c:choose><c:when test="${ count > 0 }"><c:out value="${count}" /></c:when><c:otherwise>0</c:otherwise></c:choose>校</b>の登録が可能です</span>
</td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>
</div>

</td>
</tr>
</table>
<!--/新規追加-->

<!--STEP-->
<table border="0" cellpadding="0" cellspacing="0" width="183">
<tr valign="top">
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="1" bgcolor="#4694AF"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="180" bgcolor="#F3F5F5" align="center">
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="13" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--step1-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22">
<td width="5" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
<td width="50" bgcolor="#98ACB7" align="right"><img src="./shared_lib/img/step/step1_def.gif" width="46" height="12" border="0" alt="STEP1"><br></td>
<td width="13" bgcolor="#98ACB7"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94" bgcolor="#98ACB7"><b class="text12" style="color:#FFFFFF;">検　索</b></td>
</tr>
</table>
<!--/step1-->
<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr>
<td width="162" height="20" align="center"><img src="./shared_lib/img/parts/arrow_down_blue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->
<!--step2-->
<table border="0" cellpadding="0" cellspacing="0" width="162">
<tr height="22" bgcolor="#FF8F0E">
<td width="55" align="right"><img src="./shared_lib/img/step/step2_cur.gif" width="46" height="12" border="0" alt="STEP2"><br></td>
<td width="13"><img src="./shared_lib/img/parts/sp.gif" width="13" height="1" border="0" alt=""><br></td>
<td width="94"><b class="text12" style="color:#FFFFFF;">選　択</b></td>
</tr>
</table>
<!--/step2-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="4" width="183" bgcolor="#B9B9B9"><img src="./shared_lib/img/parts/sp.gif" width="183" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/STEP-->
<!--/●●●左側●●●-->
</td>
<td width="16"><img src="./shared_lib/img/parts/sp.gif" width="16" height="1" border="0" alt=""><br></td>
<td width="591">

<!--●●●右側●●●-->
<!--大学選択-->
<table border="0" cellpadding="0" cellspacing="0" width="591">
<tr>
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="9" cellspacing="1" width="591">
<tr>
<td width="589" bgcolor="#FFFFFF" align="center">

<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="549"><span class="text14-hh">新規追加したい高校を選択し、登録ボタンを押してください。<br>（※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="□" hspace="5" align="absmiddle">は登録済みのため、選択できません。）</span></td>
</tr>
</table>
</div>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<table border="0" cellpadding="0" cellspacing="0" width="571">
<tr>
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="551">
<!--中タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="31" border="0" alt=""><br></td>
<td width="534">

<table border="0" cellpadding="0" cellspacing="0" width="534">
<tr valign="top">
<td colspan="2" width="534" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="534" height="1" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="2" width="534" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="534" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="3" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="3" height="24" border="0" alt=""><br></td>
<td width="531" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="531">
<tr>
<td width="7"><img src="./shared_lib/img/parts/sp.gif" width="7" height="1" border="0" alt=""></td>
<td width="300"><b class="text16" style="color:#FFFFFF;">現在の検索条件</b></td>
<td width="218" align="right"></td>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="1" border="0" alt=""></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="2" width="534" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="534" height="3" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="8"><img src="./shared_lib/img/parts/tbl_ttl_r.gif" width="8" height="31" border="0" alt=""><br></td>
</tr>
<tr>
<td colspan="3" width="543" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="543" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>
<!--/中タイトル-->
<!--条件-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="393"><span class="text12">
<c:choose>
  <c:when test="${ form.button == '上記の条件で検索' }">
    <b><nobr>［表示対象］</nobr></b>
    <c:if test="${ form.view == 'all' }"><nobr>すべて表示</nobr></c:if>
    <c:if test="${ form.view == 'school' }"><nobr>高校のみ表示</nobr></c:if>
    <c:if test="${ form.view == 'average' }"><nobr>都道府県平均のみ表示</nobr></c:if>

    <c:if test="${ not empty form.pref }">
      <b><nobr>［所在地］</nobr></b>
      <c:forEach var="pref" items="${form.pref}">
        <nobr><c:out value="${PrefBean.prefMap[pref]}" /></nobr>
      </c:forEach>
    </c:if>

    <c:if test="${ form.view != 'average' }">
      <c:if test="${ not empty form.schoolDiv }">
        <b><nobr>［公私区分］</nobr></b>
        <c:forEach var="schoolDiv" items="${form.schoolDiv}">
          <c:if test="${ schoolDiv == '01' }"><nobr>国公立</nobr></c:if>
          <c:if test="${ schoolDiv == '02' }"><nobr>私立</nobr></c:if>
        </c:forEach>
      </c:if>
      <c:if test="${ not empty form.rank }">
        <b><nobr>［ランク］</nobr></b>
        <c:forEach var="rank" items="${form.rank}">
          <c:out value="${rank}" />
        </c:forEach>
      </c:if>
    </c:if>

    <b><nobr>［受験人数］</nobr></b>
    <c:forEach var="examinees" items="${form.examinees}">
      <c:if test="${ examinees == 'all' }"><nobr>すべて</nobr></c:if>
      <c:if test="${ examinees == '10' }"><nobr>10人以上</nobr></c:if>
      <c:if test="${ examinees == '50' }"><nobr>50人以上</nobr></c:if>
      <c:if test="${ examinees == '100' }"><nobr>100人以上</nobr></c:if>
      <c:if test="${ examinees == '200' }"><nobr>200人以上</nobr></c:if>
    </c:forEach>
  </c:when>
 
 <c:when test="${ form.button == '検索' }">
    <c:if test="${ form.searchMode == 'name' }">
      <b><nobr>［高校名］</nobr></b>
    </c:if>
    <c:if test="${ form.searchMode == 'code' }">
      <b><nobr>［高校コード］</nobr></b>
    </c:if>
    <nobr><c:out value="${form.searchStr}" /></nobr>
  </c:when>

  <c:otherwise>
    <b><nobr>［高校名頭文字］</nobr></b>
    <c:out value="${form.button}" />
  </c:otherwise>
</c:choose>
</span></td>
<td width="150" align="right"><input type="button" value="検索条件の変更" onclick="submitMenu('cm603');">
</tr>
</table>
</div>
<!--/条件-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--上ページング-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#EFF2F3" align="center">
<!--表示-->
<table border="0" cellpadding="0" cellspacing="0" width="516">
<tr>
<td width="516" align="center"><span class="text12">
全<b><c:out value="${ SearchBean.recordCount }" /></b>件中　<c:out value="${ SearchBean.beginCount }" />〜<c:out value="${ SearchBean.endCount }" />件表示
</span></td>
</tr>
<tr>
<td width="516"><img src="./shared_lib/img/parts/dot_blue.gif" width="516" height="1" border="0" alt="" vspace="6"><br></td>
</tr>
<tr>
<td width="516" align="center">
<!--ページングナビ-->
<%@ include file="/jsp/com_set/page.jsp" %>
<!--/ページングナビー-->
</td>
</tr>
</table>
<!--/表示-->
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/上ページング-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="2" cellspacing="2" width="547">
<tr>
<td bgcolor="#93A3AD">

<table border="0" cellpadding="3" cellspacing="0">
<tr height="27">
<td><input type="checkbox" name="dummy" value="" onclick="selectAll(this.checked);"></td>
<td><b class="text12" style="color:#FFF;">すべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>

</td>
</tr>
</table>

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="547">
<tr bgcolor="#CDD7DD">
<td width="7%">&nbsp;</td>
<td width="12%" align="center"><span class="text12">高校コード</span></td>
<td width="46%">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">高校名</span></td>
</tr>
</table>
</td>
<%-- 営業のみ --%>
<c:if test="${LoginSession.sales}">
<td width="10%" align="center"><span class="text12">ランク</span></td>
</c:if>

<td width="13%" align="center"><span class="text12">受験人数</span></td>
<td width="12%" align="center"><span class="text12">所在地</span></td>
</tr>

<c:forEach var="school" items="${SearchBean.recordSet}">
  <c:set var="checked" value="" />
  <c:forEach var="selected" items="${CommonForm.actionForms['CM604Form'].school}">
    <c:if test="${ school.schoolCode == selected }">
      <c:set var="checked" value=" checked" />
    </c:if>
  </c:forEach>
  <c:choose>
    <c:when test="${school.registed}"><c:set var="color" value="#FFAD8C" /></c:when>
    <c:otherwise><c:set var="color" value="#F4E5D6" /></c:otherwise>
  </c:choose>
  <tr height="27">
  <td bgcolor="#E1E6EB" align="center"><c:if test="${not school.registed}"><input type="checkbox" name="school" value="<c:out value="${school.schoolCode}" />"<c:out value="${checked}" /> onClick="setCount(this)"></c:if></td>
  <td bgcolor="<c:out value="${color}" />" align="center" nowrap>
    <span class="text12">
      <c:if test="${HiddenCodeMap[school.schoolCode] != '1'}">
        <c:out value="${school.schoolCode}" />
      </c:if>
    </span></td>
  <td bgcolor="<c:out value="${color}" />">
  <table border="0" cellpadding="4" cellspacing="0" width="100%">
  <tr>
  <td><span class="text12"><c:out value="${school.schoolName}" /></span></td>
  </tr>
  </table>
  </td>

  <%-- 営業のみ --%>
  <c:if test="${LoginSession.sales}">
  <td bgcolor="<c:out value="${color}" />" align="center"><span class="text12"><c:out value="${school.rank}" /></span></td>
  </c:if>

  <td bgcolor="<c:out value="${color}" />" align="right" nowrap>
  <table border="0" cellpadding="4" cellspacing="0">
  <tr>
  <td><span class="text12"><c:out value="${school.examinees}" />人</span></td>
  </tr>
  </table>
  </td>
  <td bgcolor="<c:out value="${color}" />" align="center"><span class="text12"><c:out value="${PrefBean.prefMap[school.location]}" /></span></td>
  </tr>
</c:forEach>

</table>
<!--/リスト-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<!--下ページング-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#EFF2F3" align="center">
<!--ページングナビ-->
<%@ include file="/jsp/com_set/page.jsp" %>
<!--/ページングナビー-->
</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/下ページング-->

<!--矢印-->
<div align="center">
<table border="0" cellpadding="0" cellspacing="10">
<tr valign="top">
<td><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
</div>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="545">
<tr valign="top">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="543">

<table border="0" cellpadding="0" cellspacing="0" width="543">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="543">
<tr valign="top">
<td width="541" bgcolor="#FBD49F" align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td><input type="button" value="&nbsp;登録して再検索&nbsp;" class="text12" style="width:130px;" onClick="submitRegist('cm603')"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;登録して一覧に戻る&nbsp;" class="text12" style="width:130px;" onClick="submitRegist('cm601')"></td>
<td><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
<td><input type="button" value="&nbsp;登録せず一覧に戻る&nbsp;" class="text12" style="width:130px;" onClick="submitUnRegist('cm601')"></td>
</tr>
</table>

</td>
</tr>
</table>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--/ボタン-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
</tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/大学選択-->
<!--/●●●右側●●●-->
</td>
</tr>
</table>

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/比較対象高校-->





<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="25" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!-- /MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
</form>
</body>
</html>

<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD START --%>
<%@ page pageEncoding="MS932" %>
<%-- 2016/1/6 QQ)Hisakawa 大規模改修 UPD END --%>

function submitRegist(mode) {

	var form = document.forms[0];
	form.mode.value = mode;

	// あれば呼ぶ
	if (window.setupForm) setupForm();

	// フォーム用クラス
	var fu = new FormUtil();

	// 警告メッセージ
	var alertMsg = "";
	// エラーメッセージ
	var errorMsg = "";

	// 入力チェック
	<%-- プロファイル新規作成フロー以外からのアクセスなら調べる --%>
	<c:if test="${ CommonForm.check == 1 }">

		<%-- 型 --%>
		<c:choose>
			<c:when test="${ param.forward == 'cm101' }">
				// 個別選択の場合
				if (form.selection[1].checked) {
					// 分析対象がなければ警告
					if(fu.countChecked(form.elements["analyze2"]) == 0) {
						alertMsg += "<kn:message id="cm003c" />\n";
					}
				}
			</c:when>
			<c:otherwise>
				<%-- 個別選択の場合 --%>
				<c:if test="${ CommonForm.actionForms.CM101Form.selection == 2 }">
					<%-- 分析対象がなければ警告 --%>
					<c:if test="${ empty CommonForm.actionForms.CM101Form.analyze2 }">
						alertMsg += "<kn:message id="cm003c" />\n";
					</c:if>
				</c:if>
			</c:otherwise>
		</c:choose>

		<%-- 科目 --%>
		<c:choose>
			<c:when test="${ param.forward == 'cm201' }">
				// 個別選択の場合
				if (form.selection[1].checked) {
					// 分析対象がなければ警告
					if(fu.countChecked(form.elements["analyze2"]) == 0) {
						alertMsg += "<kn:message id="cm004c" />\n";
					}
				}
			</c:when>
			<c:otherwise>
				<%-- 個別選択の場合 --%>
				<c:if test="${ CommonForm.actionForms.CM201Form.selection == 2 }">
					<%-- 分析対象がなければ警告 --%>
					<c:if test="${ empty CommonForm.actionForms.CM201Form.analyze2 }">
						alertMsg += "<kn:message id="cm004c" />\n";
					</c:if>
				</c:if>
			</c:otherwise>
		</c:choose>

		<%-- 比較対象年度 --%>
		<c:if test="${MenuSecurity.menu['603']}">
		<c:choose>
			<c:when test="${ param.forward == 'cm301' }">
				var count = fu.countChecked(form.elements["compYear"])
				if (count == 0) {
					alertMsg += "<kn:message id="cm005c" />\n";
				}
			</c:when>
			<c:otherwise>
				<%-- NULLならチェックしない --%>
				<c:if test="${ CommonForm.actionForms.CM301Form.compYear != null }">
					<c:set var="count" value="0" />
					<c:forEach items="${CommonForm.actionForms.CM301Form.compYear}">
						<c:set var="count" value="${ count + 1 }" />
					</c:forEach>
					<c:if test="${ count == 0 }">
						alertMsg += "<kn:message id="cm005c" />\n";
					</c:if>
				</c:if>
			</c:otherwise>
		</c:choose>
		</c:if>

		<%-- 志望大学 --%>
		<c:choose>
			<c:when test="${ param.forward == 'cm401' }">
				// 個別選択の場合
				if (form.selection[1].checked) {
					if (oc.getData().length == 0) {
						alertMsg += "<kn:message id="cm006c" />\n";
					}
				}
			</c:when>
			<c:otherwise>
				<%-- 個別選択の場合 --%>
				<c:if test="${ CommonForm.actionForms.CM401Form.selection == 2 }">
					<c:if test="${ empty CommonForm.actionForms.CM401Form.univ }">
						alertMsg += "<kn:message id="cm006c" />\n";
					</c:if>
				</c:if>
			</c:otherwise>
		</c:choose>

		<%-- 比較対象クラス --%>
		<c:if test="${MenuSecurity.menu['605']}">
		<c:choose>
			<c:when test="${ param.forward == 'cm501' }">
				// 個別選択の場合
				if (form.selection[1].checked) {
					var count = fu.count(form.elements["compare2"]);
					if (count == 0) {
						alertMsg += "<kn:message id="cm007c" />\n";
					}
					if (count > 40) {
						errorMsg += "<kn:message id="cm006a" />\n";
					}
				}
			</c:when>
			<c:otherwise>
				<%-- 個別選択の場合 --%>
				<c:if test="${ CommonForm.actionForms.CM501Form.selection == 2 }">
					<c:set var="count" value="0" />
					<c:forEach items="${CommonForm.actionForms.CM501Form.compare2}">
						<c:set var="count" value="${ count + 1 }" />
					</c:forEach>
					<c:if test="${ count == 0 }">
						alertMsg += "<kn:message id="cm007c" />\n";
					</c:if>
					<c:if test="${ count > 40 }">
						errorMsg += "<kn:message id="cm006a" />\n";
					</c:if>
				</c:if>
			</c:otherwise>
		</c:choose>
		</c:if>

		<%-- 比較対象高校 --%>
		<c:if test="${MenuSecurity.menu['606']}">
		<c:choose>
			<c:when test="${ param.forward == 'cm601' }">
				if (oc.getData().length == 0) {
					alertMsg += "<kn:message id="cm008c" />\n";
				}
			</c:when>
			<c:otherwise>
				<c:if test="${ empty CommonForm.actionForms.CM601Form.school }">
					alertMsg += "<kn:message id="cm008c" />\n";
				</c:if>
			</c:otherwise>
		</c:choose>
		</c:if>

	</c:if>

	// エラー
	if (errorMsg != "") {
		alert(errorMsg);
		return;
	}

	// 警告
	if (alertMsg != "") {
		if (confirm(alertMsg)) {
			submitMenu('cm002');
		}
	} else {
		submitMenu('cm002');
	}

}

function windowClose() {

	if (document.forms[0].changed.value == 0) {
		<c:if test="${CommonForm.save == 1}">
			if (window.opener.submitClosed) window.opener.submitClosed();
		</c:if>
		window.close();
	} else {

		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 DEL START --%>
		<%--
		var arg = new Array("<kn:message id="cm002c" />", "閉じる", "設定して閉じる", "キャンセル");

		returnValue = showModalDialog(
				"<c:url value="Dialog" />",
				arg,
				"status:no;help:no;scroll:no;dialogWidth:360px;dialogHeight:160px");

		// 閉じる
		if (returnValue == 1) {
			<c:if test="${CommonForm.save == 1}">
				if (window.opener.submitClosed) window.opener.submitClosed();
			</c:if>
			window.close();

		// 設定して閉じる
		} else if (returnValue == 2) {
			submitRegist(1);
		}
		--%>
		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 DEL END   --%>

		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
		var dialogMsg = "<kn:message id="cm002c" />";

		// ダイアログ画面のパラメータ指定
		// メッセージ
		document.getElementById("dialogMsg").innerHTML = dialogMsg;
		// ボタンの文言
		document.getElementById("dialogBtn1").value = "閉じる";
		document.getElementById("dialogBtn2").value = "設定して閉じる";
		document.getElementById("dialogBtn3").value = "キャンセル";

		confirmDialogPattern = "com_close";

		// サイズの指定
		$( '#confirmDialogDiv' ).dialog( { width: 410, height: 200 } );

	    // 確認ダイアログ　オープン
	    $(confirmDialogObj).dialog('open');
		<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>
	}
}

function formChanged() {
	document.forms[0].changed.value = "1";
}


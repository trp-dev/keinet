<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD START --%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<%-- 2016/01/07 QQ)Hisakawa 大規模改修 ADD END   --%>
<title>Kei-Navi／共通項目設定</title>
<script type="text/javascript" src="./shared_lib/style/stylesheet.js"></script>
<noscript><link rel="stylesheet" type="text/css" href="./shared_lib/style/stylesheet.css"></noscript>
<script type="text/javascript" src="./js/DOMUtil.js"></script>
<script type="text/javascript" src="./js/Browser.js"></script>
<script type="text/javascript" src="./js/SwitchLayer.js"></script>
<script type="text/javascript" src="./js/FormUtil.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD START --%>
<link rel="stylesheet" type="text/css" href="./shared_lib/style/jquery-ui.css">
<script type="text/javascript" src="./js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<script type="text/javascript" src="./js/Dialog.js"></script>
<%-- 2015/12/25 QQ)Hisakawa 大規模改修 ADD END   --%>
<script type="text/javascript">
<!--
    <%@ include file="/jsp/script/timer.jsp" %>
	<%@ include file="/jsp/com_set/com_close.jsp" %>
	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_help.jsp" %>

	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD START --%>
	<%@ include file="/jsp/script/dialog_close.jsp" %>

	var confirmDialogPattern = "";
	<%-- 2016/01/08 QQ)Hisakawa 大規模改修 ADD END   --%>

	// レイヤ操作クラス
	var sw = new SwitchLayer();
	// フォームユーティリティ
	var util = new FormUtil();

	function change(obj) {
		var color1 = "FAFAFA";
		var color2 = "FAFAFA";
		if (obj.value == "1") {
			color1 = "#FFAD8C";
			sw.showLayer("Layer1");
			sw.hideLayer("Layer2");
		} else if (obj.value == "2") {
			color2 = "#FFAD8C";
			sw.showLayer("Layer2");
			sw.hideLayer("Layer1");
		}
		var cur  = new DOMUtil(obj);
		<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD START --%>
		// cur.getParent(2).childNodes[2].style.backgroundColor = color1;
		// cur.getParent(2).childNodes[6].style.backgroundColor = color2;

		cur.getParent(2).children[2].style.backgroundColor = color1;
		cur.getParent(2).children[6].style.backgroundColor = color2;
		<%-- 2015/12/22 QQ)Hisakawa 大規模改修 UPD END   --%>
	}

	function changeState(obj) {
		// 分析対象の状態によりグラフ表示対象も変化させる
		var disabled = obj.checked ? false : "disabled";
		var cur  = new DOMUtil(obj);
//		var checkbox = cur.getParent(2).childNodes[2].childNodes[0];
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
//		var checkbox = cur.getParent(1).nextSibling.childNodes[0];
		var checkbox = cur.getParent(1).nextElementSibling.childNodes[0];
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END   --%>
		checkbox.disabled = disabled;

		// 分析対象がチェックされていなければグラフ表示対象も外す
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
		//if (!obj.checked) checkbox.checked = false;
		if (obj.checked) {
			checkbox.checked = true;
		} else {
			checkbox.checked = false;
		}
		<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END   --%>
	}

	function changeAllState(name) {
		var e = document.forms[0].elements[name];
		if (e.length) {
			for (var i = 0; i < e.length; i++) {
				changeState(e[i]);
			}
		} else {
			changeState(e)
		}
	}

	function init() {
		startTimer();
		// 選択方法
		var e = document.forms[0].elements["selection"];
		// 一般
		if (e.length) {
			if (e[0].checked) change(e[0]);
			else if (e[1].checked) change(e[1]);
		// 営業
		} else {
			change(e)
		}
	}

// -->
</script>
</head>
<body onLoad="init()" bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="<c:url value="CommonType" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="<c:out value="${CommonForm.changed}" />">

<%-- 全選択の分析対象 --%>
<c:forEach var="subject" items="${SubjectBean.partList}">
  <input type="hidden" name="analyze1" value="<c:out value="${subject.subCD}" />">
</c:forEach>

<a name="top"></a>
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="17" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--上部　オレンジライン-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lt.gif" width="12" height="11" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_t.gif"><img src="./shared_lib/img/parts/sp.gif" width="863" height="11" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rt.gif" width="13" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/上部　オレンジライン-->


<!--MAIN-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="300" border="0" alt=""><br></td>
<td width="8" background="./shared_lib/img/parts/com_bk_l.gif"><img src="./shared_lib/img/parts/sp.gif" width="8" height="2" border="0" alt=""><br></td>
<td width="871" bgcolor="#FFFFFF" align="center">

<!--ヘルプナビ-->
<%@ include file="/jsp/shared_lib/help_sw00.jsp" %>
<!--/ヘルプナビ-->

<!--戻るボタン
<table border="0" cellpadding="0" cellspacing="0" width="871">
<tr bgcolor="#828282">
<td width="871"><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
<tr bgcolor="#98A7B1">
<td width="871" height="32">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="6"><img src="./shared_lib/img/parts/sp.gif" width="6" height="32" border="0" alt=""><br></td>
<td>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
<td bgcolor="#F2F4F4"><input type="button" value="&nbsp;＜前のページに戻る&nbsp;" class="text12" style="width:138px;" onClick="submitBack()"></td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr width="871" bgcolor="#828282">
<td><img src="./shared_lib/img/parts/sp.gif" width="871" height="1" border="0" alt=""><br></td>
</tr>
</table>
/戻るボタン-->

<!--コンテンツ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--大タイトル-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td width="2" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="2" height="42" border="0" alt=""><br></td>
<td width="804">

<table border="0" cellpadding="0" cellspacing="0" width="804">
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr>
<td width="5" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="5" height="32" border="0" alt=""><br></td>
<td width="7" bgcolor="#FF8F0E"><img src="./shared_lib/img/parts/sp.gif" width="7" height="32" border="0" alt=""><br></td>
<td width="4" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="4" height="32" border="0" alt=""><br></td>
<td width="788" bgcolor="#758A98">
<table border="0" cellpadding="0" cellspacing="0" width="788">
<tr>
<td width="14"><img src="./shared_lib/img/parts/sp.gif" width="14" height="1" border="0" alt=""><br></td>
<td width="200"><b class="text16" style="color:#FFFFFF;">共通項目設定</b></td>
<td width="564" align="right"><font class="text12" style="color:#FFFFFF;"><c:out value="${LoginSession.userName}" />　｜　<c:out value="${Profile.profileName}" />　｜　<c:out value="${Exam.examName}" /></font></td>
<td width="10"><img src="./shared_lib/img/parts/sp.gif" width="10" height="1" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#EFF2F3"><img src="./shared_lib/img/parts/sp.gif" width="804" height="3" border="0" alt=""><br></td>
</tr>
<tr valign="top">
<td colspan="4" width="804" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="804" height="2" border="0" alt=""><br></td>
</tr>
</table>

</td>
<td width="14"><img src="./shared_lib/img/parts/tbl_ttl_r_l.gif" width="14" height="42" border="0" alt=""><br></td>
</tr>
</table>
<!--/大タイトル-->
<!--説明-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td><span class="text14">現在登録されている共通項目です。この画面で設定すると全画面に反映されます。</span></td>
</tr>
</table>
</div>
<!--/説明-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="2" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->


<!--コンテンツメニュー-->
<%@ include file="/jsp/com_set/menu.jsp" %>
<!--/コンテンツメニュー-->


<!--型の選択-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
<td width="818" bgcolor="#EFF2F3" align="center">

<!--中タイトル-->
<div style="margin-top:15px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td width="39"><img src="./shared_lib/img/parts/icon_kata.gif" width="30" height="30" border="0" alt="型の選択"><br></td>
<td width="160"><b class="text16" style="color:#657681">型の選択</b></td>
<td width="591">
<!--選択方法-->
<%-- 営業は非表示 --%>
<c:if test="${LoginSession.sales}"><div style="visibility:hidden"></c:if>
<table border="0" cellpadding="0" cellspacing="0" width="591" height="38">
<tr>
<td rowspan="3" width="101"><img src="./com_set/img/ttl_select.gif" width="101" height="38" border="0" alt="選択方法"><br></td>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
<td rowspan="3" width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="38" border="0" alt=""><br></td>
</tr>
<tr>
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD START --%>
<%-- <td width="489" bgcolor="FAFAFA"> --%>
<td width="489" height="36" bgcolor="FAFAFA">
<%-- 2016/04/28 QQ)Hisakawa 大規模改修 UPD END   --%>
<table border="0" cellpadding="0" cellspacing="0" width="489">
<tr height="30">
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="1" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM101Form.selection == 1 }"> checked</c:if><c:if test="${LoginSession.sales}"> disabled</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="217" bgcolor="#FFAD8C">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="215"><span class="text12-lh">受験生がいる全種類の型を対象にする</span></td>
</tr>
</table>
</td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="22" bgcolor="#E1E6EB"><input type="radio" name="selection" value="2" onClick="change(this);formChanged()"<c:if test="${ CommonForm.actionForms.CM101Form.selection == 2 }"> checked</c:if>></td>
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="30" border="0" alt=""><br></td>
<td width="218">
<table border="0" cellpadding="2" cellspacing="0" width="217">
<tr height="30">
<td width="2"><img src="./shared_lib/img/parts/sp.gif" width="2" height="1" border="0" alt=""><br></td>
<td width="216"><span class="text12-lh">型を選択する</span></td>
</tr>
</table>
</td>
<td width="4"><img src="./shared_lib/img/parts/sp.gif" width="4" height="30" border="0" alt=""><br></td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="489" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="489" height="1" border="0" alt=""><br></td>
</tr>
</table>
<c:if test="${LoginSession.sales}"></div></c:if>
<!--/選択方法-->
</td>
</tr>
</table>
</div>
<!--/中タイトル-->


<!--概要-->
<div style="margin-top:10px;">
<table border="0" cellpadding="0" cellspacing="0" width="790">
<tr>
<td bgcolor="#8CA9BB">

<!-- レイヤ１ -->

<div id="Layer1" style="visibility:hidden">

<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">受験者のいる全種類の型を分析対象に設定しています。この中からグラフ表示にする型を選択してください。<BR>自分で型を指定したい場合は「選択方法」を切り替えてください。</span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="3" cellspacing="0">
<tr height="27">
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graph1');formChanged();"></td>
  --%>
<td><input type="checkbox" name="dummy1" value="" onclick="util.checkAllElements(this.checked, 'graph1');formChanged();"></td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>

</td>
</tr>
</table>

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table1">
<tr>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">分析<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="32%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">型名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">分析<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="33%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">型名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
</tr>

<c:if test="${ SubjectBean.partListHalfSize > 0 }">
<c:forEach var="i" begin="0" end="${ SubjectBean.partListHalfSize - 1 }">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + 1 }" /></span></td>
<c:set var="color" value="#F4E5D6" />
<c:set var="checked" value="" />
<c:forEach var="code" items="${CommonForm.actionForms.CM101Form.graph1}">
  <c:if test="${ SubjectBean.partList[i].subCD == code }">
    <c:set var="color" value="#FFAD8C" />
    <c:set var="checked" value=" checked" />
  </c:if>
</c:forEach>
<td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" disabled=disabled checked></td>
<td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="graph1" value="<c:out value="${SubjectBean.partList[i].subCD}" />" onClick="formChanged()"<c:out value="${disabled}" /><c:out value="${checked}" />></td>
<td bgcolor="<c:out value="${color}" />">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"><c:out value="${SubjectBean.partList[i].subName}" /></span></td>
<td align="right" nowrap><span class="text12"><c:out value="${SubjectBean.partList[i].examinees}" />人</span></td>
</tr>
</table>
</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + 1 + SubjectBean.partListHalfSize }" /></span></td>
<c:choose>
  <c:when test="${ SubjectBean.partListSize > i + SubjectBean.partListHalfSize }">
    <c:set var="color" value="#F4E5D6" />
    <c:set var="checked" value="" />
    <c:forEach var="code" items="${CommonForm.actionForms.CM101Form.graph1}">
      <c:if test="${ SubjectBean.partList[i + SubjectBean.partListHalfSize].subCD == code }">
        <c:set var="color" value="#FFAD8C" />
        <c:set var="checked" value=" checked" />
      </c:if>
    </c:forEach>
    <td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" disabled=disabled checked></td>
    <td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="graph1" value="<c:out value="${SubjectBean.partList[i + SubjectBean.partListHalfSize].subCD}" />" onClick="formChanged()"<c:out value="${disabled}" /><c:out value="${checked}" />></td>
    <td bgcolor="<c:out value="${color}" />">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12"><c:out value="${SubjectBean.partList[i + SubjectBean.partListHalfSize].subName}" /></span></td>
    <td align="right" nowrap><span class="text12"><c:out value="${SubjectBean.partList[i + SubjectBean.partListHalfSize].examinees}" />人</span></td>
    </tr>
    </table>
    </td>
  </c:when>
  <c:otherwise>
    <td bgcolor="#F4E5D6" align="center"><br></td>
    <td bgcolor="#F4E5D6" align="center"><br></td>
    <td bgcolor="#F4E5D6">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><br></td>
    <td><br></td>
    </tr>
    </table>
    </td>
  </c:otherwise>
</c:choose>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>

</div>
<!-- /レイヤ１ -->

<!-- レイヤ２ -->
<div id="Layer2" style="visibility:hidden">

<table border="0" cellpadding="9" cellspacing="1" width="790">
<tr>
<td width="788" bgcolor="#FFFFFF" align="center">

<!--説明-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="22"><img src="./shared_lib/img/parts/yaji_r.gif" width="16" height="16" border="0" alt="→"><br></td>
<td width="748"><span class="text14-hh">分析対象とする型を選択し、さらにグラフ表示をさせたい型を選択してください。<BR>（※分析対象の選択をせずにグラフ表示に設定することはできません） </span></td>
</tr>
</table>
</div>
<!--/説明-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="10" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->

<table border="0" cellpadding="0" cellspacing="0" width="770">
<tr valign="top">
<td width="20"><img src="./shared_lib/img/parts/sp.gif" width="20" height="1" border="0" alt=""><br></td>
<td width="745" align="center">

<table border="0" cellpadding="4" cellspacing="0" width="741">
<tr>
<td width="741" bgcolor="#93A3AD">

<table border="0" cellpadding="3" cellspacing="0">
<tr height="27">
<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'analyze2');changeAllState('analyze2');formChanged();"></td>
  --%>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyze2');changeAllState('analyze2');formChanged();"></td>
  --%>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, this.name);util.checkAllElements(this.checked, 'analyze2');changeAllState('analyze2');formChanged();"></td>
<%-- 2015/12/16 QQ)Nishiyama 大規模改修 UPD END --%>
<td><b class="text12" style="color:#FFF;">分析対象をすべて選択</b></td>
<td>&nbsp;&nbsp;</td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD START --%>
<%--
<td><input type="checkbox" name="dummy" value="" onclick="util.checkAllElements(this.checked, 'graph2');formChanged();"></td>
  --%>
<td><input type="checkbox" name="dummy2" value="" onclick="util.checkAllElements(this.checked, 'graph2');formChanged();"></td>
<%-- 2016/06/23 QQ)Nishiyama 大規模改修 UPD END   --%>
<td><b class="text12" style="color:#FFF;">グラフ表示をすべて選択　　（チェックを外すとすべて解除）</b></td>
</tr>
</table>

</td>
</tr>
</table>

<!--リスト-->
<table border="0" cellpadding="2" cellspacing="2" width="745" id="Table1">
<tr>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">分析<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="32%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">型名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
<td width="1%" bgcolor="#FFFFFF">&nbsp;</td>
<td width="5%" bgcolor="#CDD7DD">&nbsp;</td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">分析<br>対象</span></td>
<td width="6%" bgcolor="#CDD7DD" align="center"><span class="text12">グラフ<br>表示</span></td>
<td width="33%" bgcolor="#CDD7DD">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12">型名</span></td>
<td align="right" nowrap><span class="text12">受験人数</span></td>
</tr>
</table>
</td>
</tr>

<c:if test="${ SubjectBean.fullListHalfSize > 0 }">
<c:forEach var="i" begin="0" end="${ SubjectBean.fullListHalfSize - 1 }">
<tr height="27">
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + 1 }" /></span></td>
<c:set var="checked1" value="" />
<c:set var="disabled" value=" disabled=disabled" />
<c:forEach var="code" items="${CommonForm.actionForms.CM101Form.analyze2}">
  <c:if test="${ SubjectBean.fullList[i].subCD == code }">
    <c:set var="checked1" value=" checked" />
    <c:set var="disabled" value=" dsiabled=false" />
  </c:if>
</c:forEach>
<c:set var="color" value="#F4E5D6" />
<c:set var="checked2" value="" />
<c:forEach var="code" items="${CommonForm.actionForms.CM101Form.graph2}">
  <c:if test="${ SubjectBean.fullList[i].subCD == code }">
    <c:set var="color" value="#FFAD8C" />
    <c:set var="checked2" value=" checked" />
  </c:if>
</c:forEach>
<td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="analyze2" value="<c:out value="${SubjectBean.fullList[i].subCD}" />"<c:out value="${checked1}" /> onClick="changeState(this);formChanged()"></td>
<td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="graph2" value="<c:out value="${SubjectBean.fullList[i].subCD}" />" onClick="formChanged()"<c:out value="${disabled}" /><c:out value="${checked2}" />></td>
<td bgcolor="<c:out value="${color}" />">
<table border="0" cellpadding="4" cellspacing="0" width="100%">
<tr>
<td><span class="text12"><c:out value="${SubjectBean.fullList[i].subName}" /></span></td>
<td align="right" nowrap><span class="text12"><c:out value="${SubjectBean.fullList[i].examinees}" />人</span></td>
</tr>
</table>
</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#E1E6EB" align="center"><span class="text12"><c:out value="${ i + 1 + SubjectBean.fullListHalfSize }" /></span></td>
<c:choose>
  <c:when test="${ SubjectBean.fullListSize > i + SubjectBean.fullListHalfSize }">
    <c:set var="checked1" value="" />
    <c:set var="disabled" value=" disabled=disabled" />
    <c:forEach var="code" items="${CommonForm.actionForms.CM101Form.analyze2}">
      <c:if test="${ SubjectBean.fullList[i + SubjectBean.fullListHalfSize].subCD == code }">
        <c:set var="checked1" value=" checked" />
        <c:set var="disabled" value=" dsiabled=false" />
      </c:if>
    </c:forEach>
    <c:set var="color" value="#F4E5D6" />
    <c:set var="checked2" value="" />
    <c:forEach var="code" items="${CommonForm.actionForms.CM101Form.graph2}">
      <c:if test="${ SubjectBean.fullList[i + SubjectBean.fullListHalfSize].subCD == code }">
        <c:set var="color" value="#FFAD8C" />
        <c:set var="checked2" value=" checked" />
      </c:if>
    </c:forEach>
    <td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="analyze2" value="<c:out value="${SubjectBean.fullList[i + SubjectBean.fullListHalfSize].subCD}" />"<c:out value="${checked1}" /> onClick="changeState(this);formChanged()"></td>
    <td bgcolor="<c:out value="${color}" />" align="center"><input type="checkbox" name="graph2" value="<c:out value="${SubjectBean.fullList[i + SubjectBean.fullListHalfSize].subCD}" />" onClick="formChanged()"<c:out value="${disabled}" /><c:out value="${checked2}" />></td>
    <td bgcolor="<c:out value="${color}" />">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><span class="text12"><c:out value="${SubjectBean.fullList[i + SubjectBean.fullListHalfSize].subName}" /></span></td>
    <td align="right" nowrap><span class="text12"><c:out value="${SubjectBean.fullList[i + SubjectBean.fullListHalfSize].examinees}" />人</span></td>
    </tr>
    </table>
    </td>
  </c:when>
  <c:otherwise>
    <td bgcolor="#F4E5D6" align="center"><br></td>
    <td bgcolor="#F4E5D6" align="center"><br></td>
    <td bgcolor="#F4E5D6">
    <table border="0" cellpadding="4" cellspacing="0" width="100%">
    <tr>
    <td><br></td>
    <td><br></td>
    </tr>
    </table>
    </td>
  </c:otherwise>
</c:choose>
</tr>
</c:forEach>
</c:if>

</table>
<!--/リスト-->

<!--注意-->
<div style="margin-top:6px;">
<table border="0" cellpadding="0" cellspacing="0" width="745">
<tr>
<td align="right"><span class="text12">※<img src="./shared_lib/img/parts/color_box_03.gif" width="27" height="11" border="0" alt="赤く選択されている型" hspace="5" align="absmiddle">はグラフ表示対象</span></td>
</tr>
</table>
</div>
<!--/注意-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="11" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="5"><img src="./shared_lib/img/parts/sp.gif" width="5" height="1" border="0" alt=""><br></td>
</tr>
</table>

</td>
</tr>
</table>

</div>
<!-- /レイヤ２ -->

</td>
</tr>
</table>
</div>
<!--/概要-->


<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
</td>
<td width="1" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="1" height="1" border="0" alt=""><br></td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" bgcolor="#8CA9BB"><img src="./shared_lib/img/parts/sp.gif" width="820" height="1" border="0" alt=""><br></td>
</tr>
</table>
<!--/型の選択-->



<!--矢印-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td width="820" height="28" align="center"><img src="./shared_lib/img/parts/arrow_down_darkblue.gif" width="44" height="9" border="0" alt="↓"><br></td>
</tr>
</table>
<!--/矢印-->

<!--ボタン-->
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr valign="top">
<td bgcolor="#8CA9BB">
<table border="0" cellpadding="6" cellspacing="1" width="820">
<tr valign="top">
<td width="818" bgcolor="#FBD49F" align="center">

<input type="button" value="&nbsp;設定完了（すべての共通項目設定を反映します）&nbsp;" class="text12" style="width:340px;" onClick="submitRegist(0)">

</td>
</tr>
</table>
</td>
</tr>
</table>
<!--/ボタン-->

<!--ボタン-->
<div style="margin-top:25px;">
<table border="0" cellpadding="0" cellspacing="0" width="820">
<tr>
<td align="center"><input type="button" value="&nbsp;閉じる&nbsp;" class="text12" style="width:138px;" onClick="windowClose()"></td>
</tr>
</table>
</div>
<!--/ボタン-->

<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr valign="top">
<td width="100%"><img src="./shared_lib/img/parts/sp.gif" width="1" height="20" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--/コンテンツ-->
</td>
<td width="9"  background="./shared_lib/img/parts/com_bk_r.gif"><img src="./shared_lib/img/parts/sp.gif" width="9" height="2" border="0" alt=""><br></td>
</tr>
</table>
<!--/MAIN -->



<!--下部　ドロップシャドウ-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="12"><img src="./shared_lib/img/parts/sp.gif" width="12" height="1" border="0" alt=""><br></td>
<td width="12"><img src="./shared_lib/img/parts/com_cnr_lb_w.gif" width="12" height="15" border="0" alt=""><br></td>
<td width="863" background="./shared_lib/img/parts/com_bk_d.gif"><img src="./shared_lib/img/parts/sp.gif" width="613" height="15" border="0" alt=""><br></td>
<td width="13"><img src="./shared_lib/img/parts/com_cnr_rb.gif" width="13" height="15" border="0" alt=""><br></td>
</tr>
</table>
<!--/下部　ドロップシャドウ-->
<!--spacer-->
<table border="0" cellpadding="0" cellspacing="0" width="900">
<tr valign="top">
<td width="900"><img src="./shared_lib/img/parts/sp.gif" width="2" height="5" border="0" alt=""><br></td>
</tr>
</table>
<!--/spacer-->
<!--FOOTER-->
<%@ include file="/jsp/shared_lib/footer_sw02.jsp" %>
<!--/FOOTER-->
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD START --%>
<!-- ダイアログの定義 -->
<%@ include file="/jsp/shared_lib/dialog.jsp" %>
<%-- 2016/01/06 QQ)Nishiyama 大規模改修 ADD END   --%>
</form>
</body>
</html>

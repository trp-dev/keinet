
function openWindow(url, width, height, name) {
	var style = "resizable=yes,scrollbars=yes,status=yes,titlebar=yes";
	if (width != undefined) style += ",width=" + width;
    // 2016/01/21 QQ)Hisakawa 大規模改修 UPD START
	// if (height!= undefined) style += ",height=" + height;
	if (height!= undefined || height!= null) {
		style += ",height=" + height;
	} else {
		style += ",height=680";
	}
	// 2016/01/21 QQ)Hisakawa 大規模改修 UPD END
	if (name == undefined) name = "_blank";
	return window.open(url, name, style);
}

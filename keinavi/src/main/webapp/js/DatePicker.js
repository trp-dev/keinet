/**
 * 月・日を作成する連動コンボボックス
 * 注意：これは年度単位なので、１〜３月は翌年計算をする
   修正：上記の１〜３を１〜４に変更
 * @author 
 * @version 1.0
 * 使い方：
 	//new DatePicker(年度, 初期月, 初期日, 月用のselectID, 日用のselectID)
	var ps;
	function init(){
		ps = new Array(
			new DatePicker(2004, 12, 10, 'MM_9999,99,99,01,0202,0204,=,2', 'DD_9999,99,99,01,0202,0204,=,2'),
			new DatePicker(2004, 2, 11, 'MM_9999,99,99,01,0202,0204,=,3', 'DD_9999,99,99,01,0202,0204,=,3'),
			new DatePicker(2004, 5, 18, 'MM_9999,99,99,01,0202,0204,=,4', 'DD_9999,99,99,01,0202,0204,=,4')
		);
	
	}
 */
function DatePicker(y, m, d, MM, DD){

	var DATE_A = [["1","1日"],["2","2日"],["3","3日"],["4","4日"],["5","5日"],["6","6日"],["7","7日"],["8","8日"],["9","9日"],["10","10日"],["11","11日"],["12","12日"],["13","13日"],["14","14日"],["15","15日"],["16","16日"],["17","17日"],["18","18日"],["19","19日"],["20","20日"],["21","21日"],["22","22日"],["23","23日"],["24","24日"],["25","25日"],["26","26日"],["27","27日"],["28","28日"],["29","29日"],["30","30日"],["31","31日"]];
	//var DATE_S = [30,31,30,31,31,30,31,30,31,31,28,31];
	var DATE_S = [30,31,30,31,31,30,31,30,31,31,28,31];
	//var DATE_M = [["4","4月"],["5","5月"],["6","6月"],["7","7月"],["8","8月"],["9","9月"],["10","10月"],["11","11月"],["12","12月"],["1","1月"],["2","2月"],["3","3月"]];
	var DATE_M = [["4","4月"],["5","5月"],["6","6月"],["7","7月"],["8","8月"],["9","9月"],["10","10月"],["11","11月"],["12","12月"],["1","1月"],["2","2月"],["3","3月"]];

	//var DATE_S = [31,28,31,30,31,30,31,31,30,31,30,31];
	//var DATE_M = [["1","1月"],["2","2月"],["3","3月"],["4","4月"],["5","5月"],["6","6月"],["7","7月"],["8","8月"],["9","9月"],["10","10月"],["11","11月"],["12","12月"]];

	this.WDAY;
	this.WMON;
	this.WYEAR;
	this.THISYEAR;
	this.THISMONTH;
	this.THISDATE;

	DatePicker.prototype.MonList = MonList;
	DatePicker.prototype.DayList = DayList;
	DatePicker.prototype.ChgDay = ChgDay;
	DatePicker.prototype.ChgMon = ChgMon;
	DatePicker.prototype.initDATE_Select = initDATE_Select;

	initDATE_Select(y, m, d, MM, DD);

	function MonList(nameID, wMon) {	
	//オプションの名前を指定する
		var o = document.getElementById(nameID);
	
		for (var i=0;i<12;i++) {
			if (!o[i]) {
				var op = document.createElement("OPTION");
				op.text = DATE_M[i][1];
				op.value= DATE_M[i][0];
				o.options[i] = op;
			}
			if (wMon == 1 || wMon == 2 || wMon == 3){
				if((wMon+9) == (i+1))
					o.options[i].selected = true;
			}else{
				if((wMon-3) == (i+1))
					o.options[i].selected = true;
			}
		}
	}
	function DayList(nameID, wYear, wMon, wDay) {
	
		if(wMon == 1 || wMon == 2 || wMon == 3){
			wYear ++;//ローカル変数だから大丈夫
		}
				
		var o = document.getElementById(nameID);
		var x = 0;
	
		wMon  = wMon  - 0;
		//月をシフト
		if(wMon >= 1 && wMon < 4){
			wMon = wMon + 9;
		}else{
			wMon = wMon -3;
		}
		wYear = wYear - 0;
		if (((wYear % 100)==0 && (wYear % 400) != 0)){
			x = DATE_S[wMon-1];
		}else{
			if ((wYear % 4) == 0 && wMon == 11)
				x = 29;
			else
				x = DATE_S[wMon-1];
		}
		for (var i=0;i<x;i++) {
			if (!o[i]) {
				var op = document.createElement("OPTION");
				op.text = DATE_A[i][1];
				op.value= DATE_A[i][0];
				o.options[i] = op;
			}
			if (wDay == (i+1))
				o.options[i].selected = true;
		}
		var Max = i;
		for ( ;i<31;i++) {
			o.options[i] = null;
		}
		o.length = Max;
	}
	function ChgDay(ID) {
		//WDAY = document.getElementById(ID).value-0;
	}
	function ChgMon(ID, ID2) {
		WMON = document.getElementById(ID).value-0;
		WYEAR = THISYEAR;
		WDAY = 1;
		DayList(ID2, WYEAR, WMON, WDAY);
	}
	function initDATE_Select(y, m, d, MM, DD) {
		THISYEAR = y;
		THISMONTH = m;
		THISDATE = d;
		WYEAR = THISYEAR;
		WMON  = THISMONTH;
		WDAY  = THISDATE;

		DayList(DD, WYEAR, WMON, WDAY);
		MonList(MM, WMON);
		
	}
}
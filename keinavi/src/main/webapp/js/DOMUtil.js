/**
 * DOMユーティリティクラス
 * 要素へのアクセスを楽にしたもの
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function DOMUtil(obj) {
	this.obj = obj;
}

function _getParent(index) {
	var parent = this.obj;
	for (var i=0; i<index; i++) {
		parent = parent.parentNode;
	}
	return parent;
}

// 2015/12/22 QQ)Hisakawa 大規模改修 UPD START
/*
function _getChild() {
	var child = this.obj;
	for (var i=0; i<arguments.length; i++) {
		child = child.childNodes[arguments[i]];
	}
	return child;
}
*/

function _getChild() {
	var child = this.obj;
	//********************************************************************
	var elementChildren = "";
	//********************************************************************
	for (var i=0; i<arguments.length; i++) {
	//********************************************************************
	  elementChildren = child.children;
	  child = elementChildren[arguments[i]];
	//********************************************************************
	  //child = child.childNodes[arguments[i]];
	}
	return child;
}
// 2015/12/22 QQ)Hisakawa 大規模改修 UPD END

// public
DOMUtil.prototype.getParent = _getParent;
DOMUtil.prototype.getChild = _getChild;


/**
 * ソートライブラリ
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */

function OrderChange() {
	// データ配列
	this.data = null;
	// フラグキー
	this.key = null;
	// キーとして扱う配列のインデックス
	this.index = 0;
}

// アクセサ
function _setKey(key) { this.key = key }
function _setData(data) { this.data = data }
function _getData() { return this.data }
function _setIndex(index) { this.index = index }
function _getIndex() { return this.index }
function _getKey() { return this.key }

function _up() {
	this.data.reverse();
	this.down();
	this.data.reverse();
}

function _down() {
	var container = new ArrayList();
	var match = new ArrayList();
	for (var i=0; i<this.data.length; i++) {
		if (this.data[i][this.index] == this.key) {
			match.add(this.data[i]);
		} else {
			container.add(this.data[i]);
			container.addAll(match);
			match.clear();
		}
	}
	container.addAll(match);
	this.data = container.toArray();
}

function _deleteElement(id) {
	var container = new Array();
	for (var i=0; i<this.data.length; i++) {
		if (this.data[i][this.index] != id) {
			container[container.length] = this.data[i];
		}
	}
	this.data = container;
}

function _sort(func) {
	this.data.sort(func);
}

// public
OrderChange.prototype.up = _up;
OrderChange.prototype.down = _down;
OrderChange.prototype.deleteElement = _deleteElement;
OrderChange.prototype.sort = _sort;
OrderChange.prototype.setKey = _setKey;
OrderChange.prototype.getKey = _getKey;
OrderChange.prototype.setData = _setData;
OrderChange.prototype.getData = _getData;
OrderChange.prototype.setIndex = _setIndex;
OrderChange.prototype.getIndex = _getIndex;

/**
 * メンテナンス画面入力チェック用クラス
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function AdminValidator() {
	this.validator = new Validator();
}

function _validateClass(value) {
	return this.validator.isAlphanumeric(value);
}

function _validateClassNo(value) {
	return this.validator.isNumber(value);
}


// public
AdminValidator.prototype.validateClass = _validateClass;
AdminValidator.prototype.validateClassNo = _validateClassNo;


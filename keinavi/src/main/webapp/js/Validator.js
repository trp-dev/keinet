/**
 * 入力チェック用クラス
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function Validator() {
}

function _isNumber(string, digit) {
	if (!_isPositiveInteger(string)) return false;
	if (digit) {
		return string.length == digit;
	} else {
		return true;
	}
}

function _isHiragana(string) {
	if (string == null || string.length == 0) return false;
	for (var i=0; i<string.length; i++) {
		var c = string.charCodeAt(i);
		if (!((c >= 12353 && c <= 12435) || c == 12540)) return false;
	}
	return true;
}

function _isHiraganaAndSpace(string) {
	if (string == null || string.length == 0) {
		return false;
	}
	for (var i=0; i<string.length; i++) {
		var c = string.charCodeAt(i);
		if (!((c >= 12353 && c <= 12435) || c == 12540 || c == 32 || c == 12288)) return false;
	}
	return true;
}

function _isAlphanumeric(string, digit) {
	for (var i=0; i<string.length; i++) {
		var c = string.substring(i, i + 1);
		if ((c >= "a" && c <= "z")
			|| (c >= "A" && c <= "Z")
			|| (c >= "0" && c <= "9")) {} else return false;
    }
	if (digit) {
		return string.length == digit;
	} else {
		return true;
	}
}

function _isDate(year, month, date) {

	if (year == "" || month == "" || date == "") {
		return false;
	}

	if (!_isNumber(year)
			|| !_isNumber(month)
			|| !_isNumber(date)) {
		return false;
	}

	var d = new Date(year, month - 1, date);
	if (d.getFullYear() != year
			|| d.getMonth() + 1 != month
			|| d.getDate() != date) {
		return false;
	}

	return true;
}

function _isWQuotesOrComma(value) {
	return value.indexOf('"') >= 0 || value.indexOf(',') >= 0 || value.indexOf('\\') >= 0;
}

function _isPositiveInteger(value) {
	if (value == null || value.length == 0) {
		return false;
	}
	for (var i=0; i<value.length; i++) {
		var c = value.charCodeAt(i);
		if (!(c >= 48 && c <= 57)) {
			return false;
		}
	}
	return true;
}

/**
 * 日本語を含んでいるかどうかを返す。
 */
function _hasJapanese(value) {
    return value.match(/[0-9a-zA-Z\+\-\_\*\,\.\ ]+/g) != value;
}

// public
Validator.prototype.isNumber = _isNumber;
Validator.prototype.isHiragana = _isHiragana;
Validator.prototype.isHiraganaAndSpace = _isHiraganaAndSpace;
Validator.prototype.isAlphanumeric = _isAlphanumeric;
Validator.prototype.isDate = _isDate;
Validator.prototype.isWQuotesOrComma = _isWQuotesOrComma;
Validator.prototype.isPositiveInteger = _isPositiveInteger;
Validator.prototype.hasJapanese = _hasJapanese;

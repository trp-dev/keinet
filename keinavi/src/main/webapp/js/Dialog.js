/**
*
* 確認ダイアログ定義
*
*/
var confirmDialogObj;
jQuery(function(){

	confirmDialogObj = jQuery("#confirmDialogDiv").dialog({
        title         : "確認ダイアログ" // タイトル
      , autoOpen      : false            // 手動表示
      , modal         : true             // モーダルモード
      , resizable     : false            // リサイズ不可
      , closeOnEscape : false            // エスケープキーで閉じない
      // , position      : {my: "center center", at: "center top+200", of: window}
      , position	  : {my: "center center", at: "center top+" + (window.innerHeight / 2), of: window}
      , buttons: {
        }
    });
});

/**
*
* 印刷ダイアログ定義
*
*/
var printDialogObj;
jQuery(function(){
    printDialogObj = jQuery("#printDialogDiv").dialog({
        title         : "Kei-Navi／印刷" // タイトル
      , autoOpen      : false            // 手動表示
      , modal         : true             // モーダルモード
      , resizable     : false            // リサイズ不可
      , closeOnEscape : false            // エスケープキーで閉じない
      , width         : 680
      , height        : 500
      // , position      : {my: "center center", at: "center top+200", of: window}
      , position      : {my: "center center", at: "center top+" + (window.innerHeight / 2), of: window}
      , buttons: {
        }
    });
});

/**
*
* 作業中宣言　確認ダイアログ定義
*
*/
var workingDialogObj;
jQuery(function(){
	workingDialogObj = jQuery("#workingDialogDiv").dialog({
        title         : "作業中宣言"     // タイトル
      , autoOpen      : false            // 手動表示
      , modal         : true             // モーダルモード
      , resizable     : false            // リサイズ不可
      , closeOnEscape : false            // エスケープキーで閉じない
      , width         : 450
      , height        : 280
      // , position      : {my: "center center", at: "center top+200", of: window}
      , position      : {my: "center center", at: "center top+" + (window.innerHeight / 2), of: window}
      , buttons: {
        }
    });
});

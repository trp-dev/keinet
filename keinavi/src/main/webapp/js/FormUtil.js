/**
 * フォーム用クラス
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function FormUtil() {
}

// チェックされている数を返す
function _countChecked(e) {
	if (e == null) return 0;
	var count = 0;
	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) count++;
		}
	} else {
		if (e.checked) count++;
	}
	return count;
}

// 有効なチェック数を返す
function _countEnabled(e) {
	if (e == null) {
		return 0;
	}
	var count = 0;
	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (e[i].checked && !e[i].disabled) {
				count++;
			}
		}
	} else {
		if (e.checked && !e.disabled) {
			count++;
		}
	}
	return count;
}

// 有効な数を返す
function _count(e) {
	if (e == null) return 0;
	var count = 0;
	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (!e[i].disabled) count++;
		}
	} else {
		if (!e.disabled) count++;
	}
	return count;
}

// チェック状態を変更する
function _changeChecked(e, flag) {

	if (e == null) return;

	if (e.length) {
		for (var i=0; i<e.length; i++) {
			e[i].checked = flag;
		}
	} else {
		e.checked = flag;
	}
}

// チェックされている値を取得する
function _getCheckedValue(e) {

	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) return e[i].value;
		}
	} else {
		return e.value;
	}
}

// チェックを付ける
function _checkAllElements(checked, name) {
	var e = document.forms[0].elements[name];
	if (e == null) {
		return;
	} else if (e.length) {
		for (var i = 0; i < e.length; i++) {
			e[i].checked = e[i].disabled ? false : checked;
		}
	} else {
		e.checked = e.disabled ? false : checked;
	}
}

// public
FormUtil.prototype.countChecked = _countChecked;
FormUtil.prototype.countEnabled = _countEnabled;
FormUtil.prototype.count = _count;
FormUtil.prototype.changeChecked = _changeChecked;
FormUtil.prototype.getCheckedValue = _getCheckedValue;
FormUtil.prototype.checkAllElements = _checkAllElements;


$(function(){
	var parents = $(':checkbox.cb_all');
	parents.not('.processed').addClass('processed').each(function(){
		var self = $(this);
		var children = $(':checkbox.cb_one');
		self.click(function(){
			$.each(children, function(){
				$(this).attr('checked', self.attr('checked'));
			});
			parents.attr('checked', self.attr('checked'));
		});
		$.each(children, function(){
			$(this).click(function(){
				self.attr('checked', children.size() == children.filter(':checked').size());
			});
		});
	});
});

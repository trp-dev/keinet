/**
 * 年・月・日を作成する連動コンボボックス
 * @author http://www.gac.jp/article/index.php?stats=question&category=9&id=7470&command=msg
 * @version 1.0
 */
	var DateA = [
	"1","2","3","4","5","6","7","8","9","10",
	"11","12","13","14","15","16","17","18","19","20",
	"21","22","23","24","25","26","27","28","29","30",
	"31"];
	
	var DateS = [31,28,31,30,31,30,31,31,30,31,30,31];
	
	function MonList(nameID,wMon) {	//オプションの名前を指定する
		var o = document.getElementById(nameID);
	
		for (var i=0;i<12;i++) {
			if (!o[i]) {
				var op = document.createElement("OPTION");
				op.text = DateA[i]+"月";
				op.value= DateA[i];
				o.options[i] = op;	//.add(i,op);
			}
			if (wMon == (i+1))
				o.options[i].selected = true;
		}
	}
	function DayList(nameID,wYear,wMon,wDay) {
		var o = document.getElementById(nameID);
		var x = 0;
	
		wMon  = wMon  - 0;
		wYear = wYear - 0;
		if (((wYear % 100)==0 && (wYear % 400) != 0))
			x = DateS[wMon-1];
		else
		if ((wYear % 4) == 0 && wMon == 2)
			x = 29;
		else
			x = DateS[wMon-1];
		
		for (var i=0;i<x;i++) {
			if (!o[i]) {
				var op = document.createElement("OPTION");
				op.text = DateA[i]+"日";
				op.value= DateA[i];
				o.options[i] = op;	//add(i,op);
			}
			if (wDay == (i+1))
				o.options[i].selected = true;
		}
		var Max = i;
		for ( ;i<31;i++) {
			o.options[i] = null;	//o.remove(i);
		}
		o.length = Max;
	}
	function YearList(nameID,wYear,pYear) {
		var o = document.getElementById(nameID);
	
		var yBgn = wYear - pYear;
		var yEnd = wYear + 1;// + pYear;
	
		for (var i=0;(yBgn+i) < yEnd;i++) {
			if (!o[i]) {
				var op = document.createElement("OPTION");
				op.text = ""+(yBgn+i)+"年";
				op.value= ""+(yBgn+i);
				o.options[i] = op;	//add(i,op);
			}
			if (wYear == (yBgn+i))
				o.options[i].selected=true;
		}
	}
	var WDAY,WMON,WYEAR;
	function ChgDay() {
		WDAY = document.getElementById("DD").value-0;
	}
	function ChgMon() {
		WMON = document.getElementById("MM").value-0;
		DayList('DD',WYEAR,WMON,WDAY);
	}
	function ChgYear() {
		WYEAR = document.getElementById("YY").value-0;
		DayList('DD',WYEAR,WMON,WDAY);
	}
/**
 * 日本語文字列用クラス
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function JString(pString) {
	this.string = pString;
}

function _setString(string) {
	this.string = string;
	return this;
}

function _getLength() {
	var len = 0;
	for (var i = 0; i < this.string.length; ++i) {
		var c = this.string.charCodeAt(i);
		if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
			len++;
		} else {
			len += 2;
		}
	}
	return len;
}

// public
JString.prototype.setString = _setString;
JString.prototype.getLength = _getLength;


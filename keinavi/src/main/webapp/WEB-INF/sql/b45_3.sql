SELECT
	'�i' || p.prefname || '�j',
	NVL(rp.numbers, -999),
	NVL(rp.avgpnt, -999),
	NVL(rp.avgdeviation, -999)
FROM
	prefecture p

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = ? /* 1 */
AND
	st.cursubcd = ? /* 2 */
AND
	st.examcd = ? /* 3 */

LEFT OUTER JOIN
	subrecord_p rp
ON
	rp.examyear = st.examyear
AND
	rp.examcd = st.examcd
AND
	rp.subcd = st.subcd
AND
	rp.prefcd = p.prefcd

WHERE
	p.prefcd = ? /* 4 */

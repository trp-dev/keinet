SELECT
  E.EXAMYEAR
  , E.EXAMCD
  , E.EXAMNAME
  , E.EXAMTYPECD
FROM
  (
    SELECT
      EXAMYEAR
      , EXAMCD
      , EXAMNAME
      , EXAMTYPECD
      , DISPSEQUENCE
    FROM
      EXAMINATION
    WHERE
      EXAMCD IN (
        '01'
        , '02'
        , '03'
        , '04'
        , '05'
        , '06'
        , '07'
        , '09'
        , '10'
        , '28'
        , '38'
        , '61'
        , '62'
        , '63'
        , '65'
        , '66'
        , '67'
        , '68'
        , '71'
        , '72'
        , '73'
        , '74'
        , '75'
        , '76'
        , '77'
        , '78'
      )
    UNION ALL
    SELECT
      EXAMYEAR
      , EXAMCD
      , EXAMNAME || '�i�l���сj'
      , EXAMTYPECD
      , DISPSEQUENCE
    FROM
      EXAMINATION
    WHERE
      EXAMTYPECD = '32'
    UNION ALL
    SELECT
      EXAMYEAR
      , EXAMCD || '_1'
      , EXAMNAME || '�i�����񓚂P�j'
      , EXAMTYPECD
      , DISPSEQUENCE
    FROM
      EXAMINATION
    WHERE
      EXAMTYPECD = '32'
    UNION ALL
    SELECT
      EXAMYEAR
      , EXAMCD || '_2'
      , EXAMNAME || '�i�����񓚂Q�j'
      , EXAMTYPECD
      , DISPSEQUENCE
    FROM
      EXAMINATION
    WHERE
      EXAMTYPECD = '32'
  ) E
WHERE
  E.EXAMYEAR = ?
  OR E.EXAMYEAR = TO_CHAR(TO_NUMBER(?) - 1)
ORDER BY
  E.EXAMYEAR DESC
  , E.DISPSEQUENCE
  , E.EXAMCD

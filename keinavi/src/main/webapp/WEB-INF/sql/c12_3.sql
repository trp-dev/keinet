SELECT /*+ ORDERED INDEX(cr PK_CANDIDATERATING) */
	uniname_abbr univname_abbr,
	facultyname_abbr,
	deptname_abbr,
	NVL(cr.totalcandidatenum, -999),
	NVL(cr.totalcandidaterank, -999),
	NVL(cr.ratingcvsscore, -999),
	cr.centerrating,
	NVL(cr.ratingdeviation, -999),
	cr.secondrating,
	NVL(cr.ratingpoint, -999),
	cr.totalrating,
	/*  2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/* 2019/09/26 QQ)Oosaki 共通テスト対応 ADD START */
	/*cr.centerrating_engpt,*/
	/* 2019/09/26 QQ)Oosaki 共通テスト対応 ADD END */
	/* 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD START */
	/*cr.app_qual_judge,*/
	/*cr.app_qual_notice,*/
	/* 2019/09/19 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/09/24 QQ)Tanouchi 共通テスト対応 ADD START */
	/*NVL(cr.ratingcvsscore_engpt, -999),*/
	/*cr.cenfull,*/
	/* 2020/02/27 QQ)Ooseto 共通テスト対応 UPD START */
	/*cr.cenfull*/
	NVL(cr.cenfull, -999)
	/* 2020/02/27 QQ)Oosetoi 共通テスト対応 UPD END */
	/*cr.cenfull_engpt*/
	/* 2019/09/24 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
FROM
	candidaterating cr,
	univmaster_basic u
WHERE
	cr.individualid = ? /* 1 */
AND
	cr.examyear = ? /* 2 */
AND
	cr.examcd = ? /* 3 */
AND
	u.eventyear(+) = cr.examyear
AND
	u.examdiv(+) = ? /* 4 */
AND
	u.univcd(+) = cr.univcd
AND
	u.facultycd(+) = cr.facultycd
AND
	u.deptcd(+) = cr.deptcd
ORDER BY
	cr.candidaterank

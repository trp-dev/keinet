DELETE /*+ ORDERED */ FROM 
	subdistrecord_c dc
WHERE
	(dc.examyear, dc.examcd, dc.bundlecd, dc.grade, dc.class, dc.subcd) IN (
		SELECT
			re.examyear,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class,
			re.combisubcd
		FROM
			v_subrecount re
		WHERE
			re.examyear = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

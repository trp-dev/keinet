SELECT /*+ ORDERED USE_NL(b h) USE_NL(b qi) INDEX(b BASICINFO_IDX) INDEX(qi PK_QRECORD_I) INDEX(ri PK_SUBRECORD_I) */
	qi.individualid individualid,
	qi.examyear,
	qi.examcd examcd,
	qi.bundlecd bundlecd,
	b.homeschoolcd,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana,
	qi.subcd subcd,
	es.suballotpnt,
	ri.score,
	ri.a_deviation,
	ri.s_deviation,
	qi.question_no question_no,
	eq.qallotpnt,
	qi.qscore,
	qi.a_deviation,
	eq.display_no display_no
FROM
	basicinfo b,
	historyinfo h,
	countchargeclass c,
	qrecord_i qi,
	examquestion eq,
	subrecord_i ri,
	v_i_qexamsubject es
WHERE
	b.schoolcd = ?
AND
	h.individualid = b.individualid
AND
	h.year = ?
AND
	h.grade = c.grade
AND
	h.class = c.class
AND
	qi.individualid = h.individualid
AND
	qi.examyear = h.year
AND
	qi.examcd = ?
AND
	eq.examyear = qi.examyear
AND
	eq.examcd = qi.examcd
AND
	eq.subcd = qi.subcd
AND
	eq.question_no = qi.question_no
AND
	ri.individualid = qi.individualid
AND
	ri.examyear = qi.examyear
AND
	ri.examcd = qi.examcd
AND
	ri.subcd = qi.subcd
AND
	es.examyear = qi.examyear
AND
	es.examcd = qi.examcd
AND
	es.subcd = qi.subcd

ORDER BY
	bundlecd,
	grade,
	class,
	class_no,
	individualid,
	subcd,
	display_no,
	question_no

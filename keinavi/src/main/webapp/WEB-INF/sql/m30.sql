INSERT INTO
	historyinfo(individualid, year, grade, class, class_no)
SELECT
	?,
	h.year,
	h.grade,
	h.class,
	h.class_no
FROM
	historyinfo h
WHERE
	h.individualid = ?
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			historyinfo a
		WHERE
			a.individualid = ?
		AND
			a.year = h.year)
AND
	EXISTS (
		SELECT
			1
		FROM
			individualrecord ri
		WHERE
			ri.s_individualid = h.individualid
		AND
			ri.examyear = h.year)

SELECT
	ri.examyear,
	ri.examcd
FROM
	subrecord_i ri
WHERE
	ri.individualid = ?
GROUP BY
	ri.examyear,
	ri.examcd

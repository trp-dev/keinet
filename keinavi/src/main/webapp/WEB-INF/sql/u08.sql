UPDATE
	loginid_manage m
SET
	m.loginpwd = ?
WHERE
	m.schoolcd = ?
AND
	m.loginpwd = ?
AND
	m.loginid = (
	SELECT
		MIN(a.loginid)
	FROM
		loginid_manage a
	WHERE
		a.schoolcd = m.schoolcd
	AND
		a.manage_flg = '1')

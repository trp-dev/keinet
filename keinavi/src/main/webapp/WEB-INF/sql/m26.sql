SELECT
	ri.grade,
	ri.class
FROM
	individualrecord ri
WHERE
	ri.examyear = ?
AND
	ri.examcd = ?
AND
	ri.answersheet_no = ?
AND
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ri.examyear
		AND
			e.examcd = ri.examcd
		AND
			e.examtypecd = '32'
		)
AND
	EXISTS (
		SELECT
			1
		FROM
			historyinfo h
		WHERE
			h.individualid = ?
		AND
			h.year = ri.examyear
		AND
			(h.grade <> ri.grade OR h.grade <> ri.cntgrade OR h.class <> ri.class OR h.class <> ri.cntclass))
UNION ALL
SELECT
	h.grade,
	h.class
FROM
	historyinfo h
WHERE
	h.individualid = ?
AND
	h.year = ?
AND
	EXISTS (
		SELECT
			1
		FROM
			individualrecord ri
		WHERE
			ri.examyear = h.year
		AND
			ri.examcd = ?
		AND
			ri.answersheet_no = ?
		AND
			NOT EXISTS (
				SELECT
					'1'
				FROM
					examination e
				WHERE
					e.examyear = ri.examyear
				AND
					e.examcd = ri.examcd
				AND
					e.examtypecd = '32'
				)
		AND
			(h.grade <> ri.grade OR h.grade <> ri.cntgrade OR h.class <> ri.class OR h.class <> ri.cntclass))

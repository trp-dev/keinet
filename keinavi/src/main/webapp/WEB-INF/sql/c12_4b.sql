SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	SUM(rc.numbers),
	es.dispsequence
FROM
	v_i_cmexamsubject es

INNER JOIN
	class_group cg
ON
	cg.classgcd = ? /* 1 */

LEFT OUTER JOIN
	subrecord_c rc
ON
	rc.examyear = es.examyear
AND
	rc.examcd = es.examcd
AND
	rc.subcd = es.subcd
AND
	rc.bundlecd = ? /* 2 */
AND
	rc.grade = cg.grade
AND
	rc.class = cg.class

WHERE
	es.examyear = ? /* 3 */
AND
	es.examcd = ? /* 4 */

GROUP BY
	es.subcd,
	es.subname,
	es.suballotpnt,
	es.dispsequence

ORDER BY
	es.dispsequence

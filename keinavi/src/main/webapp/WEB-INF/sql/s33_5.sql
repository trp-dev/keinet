SELECT
	CC.GRADE,
	SUBSTR(CC.CLASS, 1, 2),
	CC.DISPGRAPH,
	NVL(RC.NUMBERS, -999),
	NVL(RC.AVGSCORERATE, -999),
	CC.DISPSEQUENCE DISPSEQUENCE
FROM
	SUBRECORD_C RC,
	COUNTCOMPCLASS CC
WHERE
	RC.EXAMYEAR(+) = ? AND /* 1 */
	RC.EXAMCD(+) = ? AND /* 2 */
	RC.BUNDLECD(+) = ? AND /* 3 */
	RC.SUBCD(+) = ? AND /* 4 */
	RC.GRADE(+) = CC.GRADE AND
	RC.CLASS(+) = CC.CLASS AND
	LENGTH(NVL(TRIM(CC.CLASS), '  ')) <= 2

UNION ALL 
SELECT
    cc.grade,
    cn.classgname,
    cc.dispgraph,
    NVL(SUM(rc.numbers), -999) numbers,
    NVL(ROUND(ROUND(SUM(rc.avgpnt * rc.numbers) / SUM(rc.numbers), 1) / es.suballotpnt * 100, 1), -999) avgscorerate,
    cc.dispsequence
FROM
    countcompclass cc
INNER JOIN
    classgroup cn
ON
    cc.class = cn.classgcd
LEFT JOIN
    class_group cg
ON
    cg.classgcd = cn.classgcd
LEFT JOIN
    examsubject es
ON
    es.examyear = ?
AND
    es.examcd = ?
AND 
    es.subcd = ?
LEFT JOIN
    subrecord_c rc
ON
    es.examyear = rc.examyear
AND
    es.examcd = rc.examcd
AND
    es.subcd = rc.subcd
AND
    rc.bundlecd = cg.bundlecd
AND
    rc.grade = cg.grade
AND
    rc.class = cg.class
AND
    rc.bundlecd = ?
GROUP BY
    cc.grade,
    cn.classgname,
    cc.dispgraph,
    es.suballotpnt,
    cc.dispsequence
ORDER BY
    dispsequence
/* e± */
SELECT
    CASE
        WHEN head.LEVELFLG = 0 THEN TRIM(head.ENGPTNAME_ABBR)
        ELSE                        TRIM(head.ENGPTNAME_ABBR) || ' ' || detl.ENGPTLEVELNAME_ABBR
    END as SHORT_NAME
  , info.CERFLEVELNAME_ABBR
  , head.ENGPTCD
  , this.NUMBERS   AS NUMBERS_S
  , this.COMPRATIO AS COMPRATIO_S
  , pref.NUMBERS   AS NUMBERS_P
  , pref.COMPRATIO AS COMPRATIO_P
  , zenk.NUMBERS   AS NUMBERS_A
  , zenk.COMPRATIO AS COMPRATIO_A
FROM
    EXAMINATION       mosi     /* Í}X^ */
INNER JOIN
    (
        SELECT
             EVENTYEAR
           , EXAMDIV
           , CEFRLEVELCD
           , CASE
                 WHEN CEFRLEVELCD = '99' THEN 'Èµ'
                 ELSE                         CERFLEVELNAME_ABBR
             END CERFLEVELNAME_ABBR
           , SORT
        FROM
             CEFR_INFO
        UNION ALL
        SELECT
             EVENTYEAR
           , EXAMDIV
           , 'ZZ'
           , 'ZZ'
           , 999
        FROM
             CEFR_INFO
        GROUP BY
             EVENTYEAR
           , EXAMDIV
    ) info                     /* CEFRîñ */
    ON    mosi.EXAMYEAR     = info.EVENTYEAR
    AND   mosi.EXAMDIV      = info.EXAMDIV
INNER JOIN
    ENGPTINFO         head     /* Fè±îñ}X^ */
    ON    mosi.EXAMYEAR     = head.EVENTYEAR
    AND   mosi.EXAMDIV      = head.EXAMDIV
INNER JOIN
    ENGPTDETAILINFO   detl     /* Fè±Ú×îñ}X^ */
    ON    head.EVENTYEAR    = detl.EVENTYEAR
    AND   head.EXAMDIV      = detl.EXAMDIV
    AND   head.ENGPTCD      = detl.ENGPTCD
LEFT OUTER JOIN
    ENGPT_CEFRACQSTATUS_S this /* pêFè±ÊEvCEFRæ¾óµiZj */
    ON    mosi.EXAMYEAR     = this.EXAMYEAR
    AND   mosi.EXAMCD       = this.EXAMCD
    AND   info.CEFRLEVELCD  = this.CEFRLEVELCD
    AND   head.ENGPTCD      = this.ENGPTCD
    AND   detl.ENGPTLEVELCD = this.ENGPTLEVELCD
    AND   this.BUNDLECD     = ? /* 1 */
LEFT OUTER JOIN
    ENGPT_CEFRACQSTATUS_P pref /* pêFè±ÊEvCEFRæ¾óµi§j */
    ON    mosi.EXAMYEAR     = pref.EXAMYEAR
    AND   mosi.EXAMCD       = pref.EXAMCD
    AND   info.CEFRLEVELCD  = pref.CEFRLEVELCD
    AND   head.ENGPTCD      = pref.ENGPTCD
    AND   detl.ENGPTLEVELCD = pref.ENGPTLEVELCD
    AND   pref.PREFCD       = ? /* 2 */
LEFT OUTER JOIN
    ENGPT_CEFRACQSTATUS_A zenk /* pêQÁ±ÊEvCEFRæ¾óµiSj */
    ON    mosi.EXAMYEAR     = zenk.EXAMYEAR
    AND   mosi.EXAMCD       = zenk.EXAMCD
    AND   info.CEFRLEVELCD  = zenk.CEFRLEVELCD
    AND   head.ENGPTCD      = zenk.ENGPTCD
    AND   detl.ENGPTLEVELCD = zenk.ENGPTLEVELCD
WHERE
        mosi.EXAMYEAR       = ? /* 3 */
    AND mosi.EXAMCD        = ? /* 4 */
ORDER BY
     head.SORT ASC
   , detl.SORT ASC
   , info.SORT ASC

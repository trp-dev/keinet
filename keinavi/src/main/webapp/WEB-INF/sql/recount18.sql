INSERT INTO ratingnumber_c (
	examyear, examcd, bundlecd, grade, class,
	ratingdiv, univcountingdiv, univcd, facultycd, deptcd,
	agendacd, studentdiv, totalcandidatenum, firstcandidatenum, ratingnum_a,
	ratingnum_b, ratingnum_c, ratingnum_d, ratingnum_e
) VALUES (
	?, ?, ?, ?, ?,
	?, ?, ?, ?, ?,
	?, ?, ?, ?, ?,
	?, ?, ?, ?
)

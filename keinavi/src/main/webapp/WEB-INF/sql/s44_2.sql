SELECT
	TRIM('（全国）'),
	et.examyear,
	NVL(ra.totalcandidatenum, -999),
	NVL(ra.firstcandidatenum, -999),
	NVL(ra.ratingnum_a, -999),
	NVL(ra.ratingnum_b, -999),
	NVL(ra.ratingnum_c, -999),
	NVL(ra.ratingnum_d, -999),
	NVL(ra.ratingnum_e, -999)
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(ra.eng_ratingnum_a, -999), */
	/* NVL(ra.eng_ratingnum_b, -999), */
	/* NVL(ra.eng_ratingnum_c, -999), */
	/* NVL(ra.eng_ratingnum_d, -999), */
	/* NVL(ra.eng_ratingnum_e, -999)  */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
LEFT OUTER JOIN
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_a ra */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
	ratingnumber_a ra
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	ra.examyear = et.examyear
AND
	ra.examcd = et.examcd
AND
	ra.studentdiv = ? /* 1 */
AND
	ra.univcountingdiv = ? /* 2 */
AND
	ra.univcd = ? /* 3 */
AND
	ra.facultycd = ? /* 4 */
AND
	ra.deptcd = ? /* 5 */
AND
	ra.agendacd = ? /* 6 */
AND
	ra.ratingdiv = ? /* 7 */
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 8 */
ORDER BY
	et.examyear DESC

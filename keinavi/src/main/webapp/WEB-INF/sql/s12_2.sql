SELECT
	NVL(d.highlimit, -999),
	NVL(d.lowlimit, -999) lowlimit,
	NVL(s.numbers, -999),
	NVL(s.compratio, -999)
FROM
	deviationzone d,
	subdistrecord_s s
WHERE
	s.examyear(+) = ? /* 1 */
AND
	s.examcd(+) = ? /* 2 */
AND
	s.bundlecd(+) = ? /* 3 */
AND
	s.subcd(+) = ? /* 4 */
AND
	d.devzonecd = s.devzonecd(+)
ORDER BY
	lowlimit DESC

SELECT
	 /* CEFRレベルコード */
	 M1.cefrlevelcd
	 /* 英語認定試験別・合計CEFR取得状況（高校）.人数 */
	,NVL(ecass.numbers, 0) AS numbers
	 /* 英語認定試験別・合計CEFR取得状況（高校）.構成比 */
	,NVL(ecass.compratio, 0) AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN ecass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
 FROM
	(
	SELECT
	 ex.examyear AS examyear
	,ex.examcd AS examcd
	,ci.cefrlevelcd AS cefrlevelcd
	,CASE WHEN ci.cefrlevelcd = '99' THEN 'なし' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr
	,ci.sort AS sort
	 FROM
		examination ex
		INNER JOIN
			cefr_info ci
			ON
				ci.eventyear = ex.examyear
			AND
				ci.examdiv = ex.examdiv
	 WHERE
		ex.examyear = ? /* '1' */
	 AND
	    ex.examcd = ? /* '2' */

	UNION ALL

	SELECT
	 ex.examyear AS examyear
	,ex.examcd AS examcd
	,'ZZ' AS cerflevelcd
	,'延人数' AS cerflevelname_abbr
	,0 AS sort
	 FROM
		examination ex
	 WHERE
		ex.examyear = ? /* '3' */
	 AND
	    ex.examcd = ? /* '4' */
	) M1
	LEFT JOIN
		(
		select count(*) AS cnt
		 from engpt_cefracqstatus_s ecass
			where
				ecass.examyear = ? /* '5' */
			AND
				ecass.examcd = ? /* '6' */
			AND
				ecass.bundlecd = ? /* '7' */
		) S1
		ON 1 = 1
	LEFT JOIN
		/* 英語認定試験別・合計CEFR取得状況（学校）TBL */
		engpt_cefracqstatus_s ecass
		ON
			ecass.examyear = M1.examyear
		AND
			ecass.examcd = M1.examcd
		AND
			ecass.engptcd = ? /* '8' */
		AND
			ecass.engptlevelcd = ? /* '9' */
		AND
			ecass.cefrlevelcd = ? /* '10' */
		AND
			ecass.bundlecd = ? /* '11' */
 WHERE
	M1.cefrlevelcd = ? /* '12' */

INSERT INTO
	historyinfo (individualid, year, grade, class, class_no)
SELECT
	?, ?, ?, ?, ?
FROM
	DUAL
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			historyinfo h
		WHERE
			h.individualid = ?
		AND
			h.year = ?)

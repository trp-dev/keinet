SELECT
	eventyear,
	engptcd,
	engptname_abbr,
	levelflg,
	engptinfo_sort,
	engptlevelcd,
	engptlevelname_abbr,
	engptdetailinfo_sort,
	cefrlevelcd,
	cerflevelname_abbr,
	cefrinfo_sort,
	numbers1,
	compratio1,
	flg1,
	examyear2,
	numbers2,
	compratio2,
	examyear3,
	numbers3,
	compratio3,
	examyear4,
	numbers4,
	compratio4,
	examyear5,
	numbers5,
	compratio5
FROM (
	/* eCEFRxÌlA\¬ä */
	SELECT
		t1.eventyear,
		t1.engptcd,
		t1.engptname_abbr,
		t1.levelflg,
		t1.engptinfo_sort,
		t1.engptlevelcd,
		t1.engptlevelname_abbr,
		t1.engptdetailinfo_sort,
		t1.cefrlevelcd,
		t1.cerflevelname_abbr,
		t1.cefrinfo_sort,
		t1.numbers AS numbers1,
		t1.compratio AS compratio1,
		t1.flg AS flg1,
		? /* '1' */ AS examyear2,
		t2.numbers AS numbers2,
		t2.compratio AS compratio2,
		? /* '2' */ AS examyear3,
		t3.numbers AS numbers3,
		t3.compratio AS compratio3,
		? /* '3' */ AS examyear4,
		t4.numbers AS numbers4,
		t4.compratio AS compratio4,
		? /* '4' */ AS examyear5,
		t5.numbers AS numbers5,
		t5.compratio AS compratio5
	FROM
		/* Nx */
		(SELECT
			/* ÍNx */
			ei.eventyear,
			/* pêFè±R[h */
			ei.engptcd,
			/* Fè±xZk¼ */
			ei.engptname_abbr,
			/* x ètO */
			ei.levelflg,
			/* ÀÑ */
			ei.sort AS engptinfo_sort,
			/* Fè±xR[h */
			edi.engptlevelcd,
			/* Fè±xZk¼ */
			edi.engptlevelname_abbr,
			/* ÀÑ */
			edi.sort AS engptdetailinfo_sort,
			/* bdeqxR[h */
			ci.cefrlevelcd,
			/* bdeqxZk¼ */
			CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
			/* ÀÑ */
			ci.sort AS cefrinfo_sort,
			/* l */
			ecass.numbers,
			/* \¬ä */
			ecass.compratio,
			/* NxÌó±L³tO */
			CASE WHEN ecass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
		FROM
			/* Fè±îñ}X^TBL */
			engptinfo ei
			/* Fè±Ú×îñ}X^TBL */
			INNER JOIN engptdetailinfo edi
			ON
				edi.eventyear = ei.eventyear
			AND
				edi.examdiv = ei.examdiv
			AND
				edi.engptcd = ei.engptcd
			/* CEFRîñTBL */
			INNER JOIN cefr_info ci
			ON
				ci.eventyear = ei.eventyear
			AND
				ci.examdiv = ei.examdiv
			/* Í}X^TBL */
			INNER JOIN examination en
			ON
				en.examyear = ei.eventyear
			AND
				en.examdiv = ei.examdiv
			AND
				en.examcd = ? /* '5' */
			/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
			LEFT JOIN engpt_cefracqstatus_s ecass
			ON
				ecass.examyear = ei.eventyear
			AND
				ecass.examcd = en.examcd
			AND
				ecass.engptcd = ei.engptcd
			AND
				ecass.engptlevelcd = edi.engptlevelcd
			AND
				ecass.cefrlevelcd = ci.cefrlevelcd
			AND
				ecass.bundlecd = ? /* '6' */
		WHERE
			ei.eventyear = ? /* '7' */
		ORDER BY
			ei.sort,
			edi.sort,
			ci.sort
		) t1
		/* ßNx1 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* ÍR[h */
				ecass.examcd,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '8' */
			AND
				ecass.examyear = ? /* '9' */
		) t2
		ON
			t2.engptcd = t1.engptcd
		AND
			t2.engptlevelcd = t1.engptlevelcd
		AND
			t2.cefrlevelcd = t1.cefrlevelcd
		/* ßNx2 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* ÍR[h */
				ecass.examcd,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '10' */
			AND
				ecass.examyear = ? /* '11' */
		) t3
		ON
			t3.engptcd = t1.engptcd
		AND
			t3.engptlevelcd = t1.engptlevelcd
		AND
			t3.cefrlevelcd = t1.cefrlevelcd
		/* ßNx3 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* ÍR[h */
				ecass.examcd,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '12' */
			AND
				ecass.examyear = ? /* '13' */
		) t4
		ON
			t4.engptcd = t1.engptcd
		AND
			t4.engptlevelcd = t1.engptlevelcd
		AND
			t4.cefrlevelcd = t1.cefrlevelcd
		/* ßNx4 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* ÍR[h */
				ecass.examcd,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '14' */
			AND
				ecass.examyear = ? /* '15' */
		) t5
		ON
			t5.engptcd = t1.engptcd
		AND
			t5.engptlevelcd = t1.engptlevelcd
		AND
			t5.cefrlevelcd = t1.cefrlevelcd
) UNION ALL (
	/* læ¾ */
	SELECT
		t1.examyear AS eventyear,
		t1.engptcd,
		t1.engptname_abbr,
		t1.levelflg,
		t1.engptinfo_sort,
		t1.engptlevelcd,
		t1.engptlevelname_abbr,
		t1.engptdetailinfo_sort,
		t1.cefrlevelcd,
		t1.cerflevelname_abbr,
		t1.cefrinfo_sort,
		t1.numbers AS numbers1,
		t1.compratio AS compratio1,
		t1.flg AS flg1,
		? /* '16' */ AS examyear2,
		t2.numbers AS numbers2,
		t2.compratio AS compratio2,
		? /* '17' */ AS examyear3,
		t3.numbers AS numbers3,
		t3.compratio AS compratio3,
		? /* '18' */ AS examyear4,
		t4.numbers AS numbers4,
		t4.compratio AS compratio4,
		? /* '19' */ AS examyear5,
		t5.numbers AS numbers5,
		t5.compratio AS compratio5
	FROM
		/* Nx */
		(SELECT
			/* ÍNx */
			ecass.examyear,
			/* pêFè±R[h */
			ecass.engptcd,
			/* Fè±xZk¼ */
			NVL(ei.engptname_abbr, ' ') AS engptname_abbr,
			/* x ètO */
			NVL(ei.levelflg, ' ') AS levelflg,
			/* ÀÑ */
			NVL(ei.sort, 0) AS engptinfo_sort,
			/* Fè±xR[h */
			ecass.engptlevelcd,
			/* Fè±xZk¼ */
			NVL(edi.engptlevelname_abbr, ' ') AS engptlevelname_abbr,
			/* ÀÑ */
			NVL(edi.sort, 0) AS engptdetailinfo_sort,
			/* bdeqxR[h */
			ecass.cefrlevelcd,
			/* bdeqxZk¼ */
			'  ' AS cerflevelname_abbr,
			/* ÀÑ */
			999 AS cefrinfo_sort,
			/* l */
			ecass.numbers,
			/* \¬ä */
			ecass.compratio,
			/* NxÌó±L³tO */
			CASE WHEN ecass.bundlecd IS NULL THEN '0' ELSE '1' END AS flg
		FROM
			/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
			engpt_cefracqstatus_s ecass
			/* Í}X^TBL */
			LEFT JOIN examination en
			ON
				en.examyear = ecass.examyear
			AND
				en.examcd = ecass.examcd
			/* Fè±îñ}X^TBL */
			LEFT JOIN engptinfo ei
			ON
				ei.eventyear = ecass.examyear
			AND
				ei.examdiv = en.examdiv
			AND
				ei.engptcd = ecass.engptcd
			/* Fè±Ú×îñ}X^TBL */
			LEFT JOIN engptdetailinfo edi
			ON
				edi.eventyear = ecass.examyear
			AND
				edi.examdiv = en.examdiv
			AND
				edi.engptcd = ecass.engptcd
			AND
				edi.engptlevelcd = ecass.engptlevelcd
		WHERE
			ecass.bundlecd = ? /* '20' */
		AND
			ecass.examyear = ? /* '21' */
		AND
			/* bdeqxR[h = ZZ:vR[h */
			ecass.cefrlevelcd = 'ZZ'
		) t1
		/* ßNx1 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '22' */
			AND
				ecass.examyear = ? /* '23' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				ecass.cefrlevelcd = 'ZZ'
		) t2
		ON
			t2.engptcd = t1.engptcd
		AND
			t2.engptlevelcd = t1.engptlevelcd
		AND
			t2.cefrlevelcd = t1.cefrlevelcd
		/* ßNx2 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '24' */
			AND
				ecass.examyear = ? /* '25' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				ecass.cefrlevelcd = 'ZZ'
		) t3
		ON
			t3.engptcd = t1.engptcd
		AND
			t3.engptlevelcd = t1.engptlevelcd
		AND
			t3.cefrlevelcd = t1.cefrlevelcd
		/* ßNx3 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '26' */
			AND
				ecass.examyear = ? /* '27' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				ecass.CEFRLEVELCD = 'ZZ'
		) t4
		ON
			t4.engptcd = t1.engptcd
		AND
			t4.engptlevelcd = t1.engptlevelcd
		AND
			t4.cefrlevelcd = t1.cefrlevelcd
		/* ßNx4 */
		LEFT JOIN (
			SELECT
				/* ÍNx */
				ecass.examyear,
				/* pêFè±R[h */
				ecass.engptcd,
				/* Fè±xR[h */
				ecass.engptlevelcd,
				/* bdeqxR[h */
				ecass.cefrlevelcd,
				/* l */
				ecass.numbers,
				/* \¬ä */
				ecass.compratio
			FROM
				/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
				engpt_cefracqstatus_s ecass
			WHERE
				ecass.bundlecd = ? /* '28' */
			AND
				ecass.examyear = ? /* '29' */
			AND
				/* bdeqxR[h = ZZ:vR[h */
				ecass.CEFRLEVELCD = 'ZZ'
		) t5
		ON
			t5.engptcd = t1.engptcd
		AND
			t5.engptlevelcd = t1.engptlevelcd
		AND
			t5.cefrlevelcd = t1.cefrlevelcd
)
ORDER BY
	engptinfo_sort,
	engptdetailinfo_sort,
	cefrinfo_sort

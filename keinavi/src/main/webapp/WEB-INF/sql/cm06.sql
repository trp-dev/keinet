SELECT
	rc.grade || ':' || rc.class,
	rc.grade,
	rc.class || 'クラス' classname,
	NVL(rc.numbers, 0),
	DECODE (rc.grade, 3, 1, 1, 3, rc.grade) grade_seq
FROM
	examtakenum_c rc
WHERE
	rc.examyear = ? /* 1 */
AND
	rc.examcd = ? /* 2 */
AND
	rc.bundlecd = ? /* 3 */

UNION ALL 

SELECT /*+ ORDERED */
	TO_NUMBER(cg.grade) || ':' || cg.classgcd,
	TO_NUMBER(cg.grade),
	cn.classgname || 'クラス',
	NVL(SUM(rc.numbers), 0),
	DECODE (cg.grade, 3, 1, 1, 3, cg.grade) grade_seq
FROM
	class_group cg,
	classgroup cn,
	examtakenum_c rc
WHERE
	cg.year = ? /* 4 */
AND
	cg.bundlecd = ? /* 5 */
AND
	cn.classgcd = cg.classgcd
AND
	rc.examyear(+) =  cg.year
AND
	rc.examcd(+) = ? /* 6 */
AND
	rc.bundlecd(+) = ? /* 7 */
AND
	rc.grade(+) = cg.grade
AND
	rc.class(+) = cg.class
GROUP BY
	cg.grade,
	cg.classgcd,
	cn.classgname

ORDER BY
	grade_seq,
	classname

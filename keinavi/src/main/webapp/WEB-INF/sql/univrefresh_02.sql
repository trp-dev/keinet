UPDATE
	examplanuniv ep
SET
	ep.univcd = ?,
	ep.facultycd = ?,
	ep.deptcd = ?,
	ep.year = ?,
	ep.examdiv = ?
WHERE
	ep.individualid = ?
AND
	ep.univcd = ?
AND
	ep.facultycd = ?
AND
	ep.deptcd = ?
AND
	ep.entexammodecd = ?
AND
	ep.entexamdiv1 = ?
AND
	ep.entexamdiv2 = ?
AND
	ep.entexamdiv3 = ?
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			examplanuniv ep2
		WHERE
			ep2.individualid = ep.individualid
		AND
			ep2.univcd = ?
		AND
			ep2.facultycd = ?
		AND
			ep2.deptcd = ?
		AND
			ep2.entexammodecd = ep.entexammodecd
		AND
			ep2.entexamdiv1 = ep.entexamdiv1
		AND
			ep2.entexamdiv2 = ep.entexamdiv2
		AND
			ep2.entexamdiv3 = ep.entexamdiv3
	)

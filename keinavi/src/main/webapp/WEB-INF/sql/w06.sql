SELECT
	e.examyear examyear,
	e.examcd examcd,
	e.examname examname,
	e.examname_abbr examname_abbr,
	e.examtypecd examtypecd,
	e.examst examst,
	e.examdiv examdiv,
	e.tergetgrade tergetgrade,
	e.inpledate inpledate,
	e.out_dataopendate out_dataopendate,
	e.dockingexamcd dockingexamcd,
	e.dockingtype dockingtype,
	e.dispsequence dispsequence,
	e.masterdiv masterdiv
FROM
	school s
INNER JOIN
	examination e
ON
	e.out_dataopendate >= NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000')
AND
	e.out_dataopendate < NVL2(s.stopdate, s.stopdate || '0000', '999999999999')
WHERE
	s.bundlecd = ?
AND
	e.examyear = ?
AND
	SUBSTR(e.out_dataopendate, 1, 8) <= TO_CHAR(SYSDATE, 'YYYYMMDD')
AND (
	EXISTS (
		SELECT /*+ FULL(cc) */
			1
		FROM
			countchargeclass cc
		INNER JOIN
			examtakenum_c rc
		ON
			rc.grade = cc.grade
		AND
			rc.class = cc.class
		WHERE
			rc.examyear = e.examyear
		AND
			rc.examcd = e.examcd
		AND
			rc.bundlecd = s.bundlecd)
OR
	EXISTS (
		SELECT /*+ ORDERED FULL(cc) */
			1
		FROM
			countchargeclass cc
		INNER JOIN
			class_group cg
		ON
			cg.grade = cc.grade
		AND
			cg.classgcd = cc.class
		INNER JOIN
			examtakenum_c rc
		ON
			rc.examyear = cg.year
		AND
			rc.grade = cg.grade
		AND
			rc.class = cg.class
		WHERE
			cg.year = e.examyear
		AND
			cg.bundlecd = ?
		AND
			rc.examcd = e.examcd
		AND
			rc.bundlecd = s.bundlecd)
OR
	EXISTS (
		SELECT
			1
		FROM
			examtakenum_s rs
		WHERE
			rs.examyear = e.examyear
		AND
			rs.examcd = e.examcd
		AND
			rs.bundlecd = s.bundlecd
		AND
			e.examtypecd = '32'))
ORDER BY
	examyear DESC,
	dispsequence

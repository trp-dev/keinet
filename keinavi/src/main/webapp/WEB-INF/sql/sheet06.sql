INSERT INTO
	examcdtrans
SELECT
	c.examyear,
	c.examcd,
	c.curexamcd
FROM (
	SELECT
		ayear examyear,
		aexamcd examcd,
		? curexamcd
	FROM (
		SELECT
			e.examyear ayear,
			e.examcd aexamcd,
			TO_CHAR(e.examyear - 1) byear,
			NVL(et.oldexamcd, e.examcd) bexamcd
		FROM
			examination e
		LEFT OUTER JOIN
			examcodetrans et
		ON
			et.examyear = e.examyear
		AND
			et.newexamcd = e.examcd)
	START WITH
		ayear = ?
	AND
		aexamcd = ?
	CONNECT BY
		PRIOR byear = ayear
	AND
		PRIOR bexamcd = aexamcd) c
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			examcdtrans et
		WHERE
			et.examyear = c.examyear
		AND
			et.examcd = c.examcd)

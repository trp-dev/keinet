INSERT INTO
	qrecord_c
SELECT /*+ USE_NL(qi eq) */
	qi.examyear,
	qi.examcd,
	qi.bundlecd,
	qi.grade,
	qi.class,
	qi.subcd,
	qi.question_no,
	ROUND(AVG(qi.qscore), 1),
	COUNT(qi.individualid),
	ROUND(ROUND(AVG(qi.qscore), 1) / eq.qallotpnt * 100, 1)
FROM (
	SELECT
		qi.individualid individualid,
		qi.examyear examyear,
		qi.examcd examcd,
		qi.bundlecd bundlecd,
		re.grade grade,
		re.class class,
		re.combisubcd subcd,
		qi.question_no question_no,
		qi.qscore qscore
	FROM
		v_subrecount re
	INNER JOIN
		qrecord_i qi
	ON
		qi.examyear = re.examyear
	AND
		qi.examcd = re.examcd
	AND
		qi.subcd = re.subcd
	AND
		qi.bundlecd = re.bundlecd
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?
	AND
		EXISTS (
			SELECT
				1
			FROM
				historyinfo hi
			WHERE
				hi.individualid = qi.individualid
			AND
				hi.year = qi.examyear
			AND
				hi.grade = re.grade
			AND
				hi.class = re.class)
	UNION ALL
	SELECT
		pp.answersheet_no,
		pp.examyear,
		pp.examcd,
		pp.schoolcd,
		pp.grade,
		pp.class,
		re.combisubcd,
		pp.question_no,
		pp.qscore
	FROM
		v_subrecount re
	INNER JOIN
		qrecord_pp pp
	ON
		pp.examyear = re.examyear
	AND
		pp.examcd = re.examcd
	AND
		pp.subcd = re.subcd
	AND
		pp.schoolcd = re.bundlecd
	AND
		pp.grade = re.grade
	AND
		pp.class = re.class
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?) qi
LEFT OUTER JOIN
	examquestion eq
ON
	eq.examyear = qi.examyear
AND
	eq.examcd = qi.examcd
AND
	eq.subcd = qi.subcd
AND
	eq.question_no = qi.question_no
GROUP BY
	qi.examyear,
	qi.examcd,
	qi.bundlecd,
	qi.grade,
	qi.class,
	qi.subcd,
	qi.question_no,
	eq.qallotpnt

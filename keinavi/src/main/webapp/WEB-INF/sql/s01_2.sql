SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(a.avgpnt, -999),
	NVL(b.avgpnt, -999),
	NVL(c.avgpnt, -999),
	NVL(a.stddeviation, -999),
	NVL(a.avgdeviation, -999),
	NVL(b.avgdeviation, -999),
	NVL(c.avgdeviation, -999),
	NVL(a.highestpnt, -999),
	NVL(a.lowestpnt, -999),
	NVL(a.numbers, -999),
	NVL(b.numbers, -999),
	NVL(c.numbers, -999)
FROM
	v_sm_cmexamsubject es,
	(SELECT
		subcd,
		avgpnt,
		stddeviation,
		avgdeviation,
		highestpnt,
		lowestpnt,
		numbers
	FROM
		subrecord_a
	WHERE
		examyear = ?
	AND
		examcd = ?
	AND
		studentdiv = '0') a,
	(SELECT
		subcd,
		avgpnt,
		stddeviation,
		avgdeviation,
		highestpnt,
		lowestpnt,
		numbers
	FROM
		subrecord_a
	WHERE
		examyear = ?
	AND
		examcd = ?
	AND
		studentdiv = '1') b,
	(SELECT
		subcd,
		avgpnt,
		stddeviation,
		avgdeviation,
		highestpnt,
		lowestpnt,
		numbers
	FROM
		subrecord_a
	WHERE
		examyear = ?
	AND
		examcd = ?
	AND
		studentdiv = '2') c
WHERE
	es.examyear = ?
AND
	es.examcd = ?
AND
	a.subcd(+) = es.subcd
AND
	b.subcd(+) = es.subcd
AND
	c.subcd(+) = es.subcd
ORDER BY es.dispsequence

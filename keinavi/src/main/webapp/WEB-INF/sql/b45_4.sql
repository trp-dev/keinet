SELECT
	s.bundlename,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	examination e

INNER JOIN
	school s
ON
	s.bundlecd = ? /* 1 */

LEFT OUTER JOIN
	school b
ON
	b.bundlecd = s.bundlecd
AND
	NVL(b.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(b.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = e.examyear
AND
	st.examcd = e.examcd
AND
	st.cursubcd = ? /* 2 */

LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = b.bundlecd

WHERE
	e.examyear = ? /* 3 */
AND
	e.examcd = ? /* 4 */

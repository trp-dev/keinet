INSERT INTO
	subcdtrans
SELECT
	ayear,
	?,
	asubcd,
	?
FROM (
	SELECT
		es.examyear ayear,
		es.subcd asubcd,
		es.examyear - 1 byear,
		NVL(st.oldsubcd, es.subcd) bsubcd
	FROM
		examcdtrans et
	INNER JOIN
		examsubject es
	ON
		es.examyear = et.examyear
	AND
		es.examcd = et.examcd
	LEFT OUTER JOIN
		subjecttrans st
	ON
		st.examyear = es.examyear
	AND
		st.examcd = es.examcd
	AND
		st.newsubcd = es.subcd
	WHERE
		et.curexamcd = ?)
START WITH
	ayear = ?
AND
	asubcd = ?
CONNECT BY
	PRIOR byear = ayear
AND
	PRIOR bsubcd = asubcd

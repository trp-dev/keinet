INSERT INTO
	loginid_manage
SELECT
	?, ?, ?, ?, ?, '2', ?, ?, ?, ?, ?
FROM
	DUAL
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			loginid_manage
		WHERE
			schoolcd = ?
		AND
			loginid = ?)
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			pactschool
		WHERE
			schoolcd = ?
		AND
			userid = ?)

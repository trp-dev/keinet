SELECT
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation,  -999)
FROM
	subrecord_s rs,
	subcdtrans st
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
AND
	rs.subcd = st.subcd
AND
	st.examyear = rs.examyear
AND
	st.examcd = rs.examcd
AND
	st.cursubcd = ?

SELECT
	TRIM('�i�S���j') bundlename,
	et.examyear examyear,
	NVL(ra.numbers, -999) numbers,
	NVL(ra.avgpnt, -999) avgpnt,
	NVL(ra.avgdeviation, -999) avgdeviation,
	ra.subcd subcd,
	ra.examcd examcd
FROM
	examcdtrans et
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = et.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = et.examyear
AND
	ra.examcd = et.examcd
AND
	ra.subcd = st.subcd
AND
	ra.studentdiv = ?
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ?
ORDER BY
	examyear DESC

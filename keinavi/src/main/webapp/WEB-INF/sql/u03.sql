UPDATE
	loginid_manage m
SET
	m.loginpwd = (
		SELECT
			t.defaultpwd
		FROM
			loginid_manage t
		WHERE
			t.schoolcd = m.schoolcd
		AND
			t.loginid = m.loginid)
WHERE
	m.schoolcd = ?
AND
	m.loginid = ?
AND
	m.manage_flg <> '1'

SELECT
	cc.grade,
	SUBSTR(cc.class, 1, 2),
	SUBSTR(cc.class, 1, 2),
	NVL(rc.numbers, -999),
	NVL(rc.avgpnt, -999),
	NVL(rc.avgdeviation, -999),
	cc.dispgraph,
	cc.dispsequence dispsequence
FROM
	subrecord_c rc,
	countcompclass cc
WHERE
	rc.examyear(+) = ? /* 1 */
AND
	rc.examcd(+) = ? /* 2 */
AND
	rc.bundlecd(+) = ? /* 3 */
AND
	rc.subcd(+) = ? /* 4 */
AND
	rc.grade(+) = cc.grade
AND
	rc.class(+) = SUBSTR(cc.class, 1, 2)
AND
	LENGTH(NVL(TRIM(cc.class), '  ')) <= 2

UNION ALL

SELECT
	cc.grade,
	cn.classgname,
	cc.class,
	NVL(SUM(rc.numbers), -999),
	NVL(ROUND(SUM(rc.avgpnt*rc.numbers) / SUM(rc.numbers), 1), -999),
	NVL(ROUND((ROUND(SUM(rc.avgpnt*rc.numbers) / SUM(rc.numbers), 1) - ra.avgpnt) / ra.stddeviation * 10 + 50, 1), -999),
	cc.dispgraph,
	cc.dispsequence
FROM
	subrecord_c rc,
	class_group cg,
	countcompclass cc,
	classgroup cn,
	subrecord_a ra,
	(
		SELECT
			MIN(ra.studentdiv) studentdiv
		FROM
			subrecord_a ra
		WHERE
			ra.examyear = ? /* 5 */
		AND
			ra.examcd = ? /* 6 */
	) a
WHERE
	rc.examyear(+) = ? /* 7 */
AND
	rc.examcd(+) = ? /* 8 */
AND
	rc.bundlecd(+) = ? /* 9 */
AND
	rc.subcd(+) = ? /* 10 */
AND
	rc.grade(+) = cg.grade
AND
	rc.class(+) = cg.class
AND
	ra.examyear(+) = ? /* 11 */
AND
	ra.examcd(+) = ? /* 12 */
AND
	ra.subcd(+) = ? /* 13 */
AND
	ra.studentdiv(+) = a.studentdiv
AND
	cg.classgcd = cc.class
AND
	cn.classgcd = cg.classgcd
GROUP BY
	cc.grade,
	cn.classgname,
	cc.class,
	ra.avgpnt,
	ra.stddeviation,
	cc.dispgraph,
	cc.dispsequence

ORDER BY
	dispsequence
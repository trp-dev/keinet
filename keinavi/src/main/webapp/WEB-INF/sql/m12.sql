UPDATE
	historyinfo
SET
	grade = ?,
	class = ?,
	class_no = ?
WHERE
	individualid = ?
AND
	year = ?
AND
	NOT EXISTS (
		SELECT /*+ ORDERED INDEX(bi BASICINFO_IDX) USE_NL(bi hi) */
			1
		FROM
			basicinfo bi
		INNER JOIN
			historyinfo hi
		ON
			hi.individualid = bi.individualid
		AND
			hi.year = ?
		WHERE
			bi.schoolcd = ?
		AND
			bi.individualid <> ?
		AND
			hi.grade = ?
		AND
			hi.class = ?
		AND
			hi.class_no = ?
	)

SELECT
	univcd
FROM (
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b basicinfo_idxa) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd,
		cr.centerrating,
		cr.secondrating
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL START */
		/* 2019/09/06 QQ)���� ���ʃe�X�g�Ή� ADD START */
/*		,cr.centerrating_engpt */
/*		,cr.app_qual_judge */
/*		,cr.app_qual_notice */
		/* 2019/09/06 QQ)���� ���ʃe�X�g�Ή� ADD END */
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL END */
	FROM
		basicinfo b,
		historyinfo h,
		candidaterating cr
	WHERE
		b.schoolcd = ? /* 1 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 2 */
	AND
		h.grade = ? /* 3 */
	AND
		h.class = ? /* 4 */
	AND
		cr.individualid = h.individualid
	AND
		cr.examyear = ? /* 5 */
	AND
		cr.examcd = ? /* 6 */

	UNION ALL

	SELECT /*+ ORDERED USE_NL(b h) INDEX(b basicinfo_idxa) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd,
		cr.centerrating,
		cr.secondrating
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL START */
		/* 2019/09/06 QQ)���� ���ʃe�X�g�Ή� ADD START */
/*		,cr.centerrating_engpt */
/*		,cr.app_qual_judge */
/*		,cr.app_qual_notice */
		/* 2019/09/06 QQ)���� ���ʃe�X�g�Ή� ADD END */
/* 2019/11/21 QQ)Tanioka �p��F�莎�������Ή� DEL END */
	FROM
		basicinfo b,
		historyinfo h,
		class_group cg,
		candidaterating cr
	WHERE
		b.schoolcd = ? /* 8 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 9 */
	AND
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = h.grade
	AND
		cg.class = h.class
	AND
		cg.classgcd = ? /* 10 */
	AND
		cr.individualid = h.individualid
	AND
		cr.examyear = ? /* 11 */
	AND
		cr.examcd = ? /* 12 */
)

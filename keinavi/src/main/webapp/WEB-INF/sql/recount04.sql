INSERT INTO
	recountqueue
SELECT
	?, SYSDATE
FROM
	DUAL
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			recountqueue re
		WHERE
			re.schoolcd = ?)

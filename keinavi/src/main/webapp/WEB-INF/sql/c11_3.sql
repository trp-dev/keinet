SELECT
	NVL(highlimit, -999),
	NVL(lowlimit, -999) lowlimit,
	MAX(numbers)
FROM (
	SELECT
		highlimit,
		lowlimit,
		-999 numbers
	FROM
		deviationzone
	
	UNION ALL
	
	SELECT
		d.highlimit,
		d.lowlimit,
		dc.numbers
	FROM
		deviationzone d,
		subdistrecord_c dc
	WHERE
		dc.examyear = ? /* 1 */
	AND
		dc.examcd = ? /* 2 */
	AND
		dc.bundlecd = ? /* 3 */
	AND
		dc.subcd = ? /* 4 */
	AND
		dc.grade = ? /* 5 */
	AND
		dc.class = ? /* 6 */
	AND
		dc.devzonecd = d.devzonecd
	
	UNION ALL
	
	SELECT
		d.highlimit,
		d.lowlimit,
		SUM(dc.numbers)
	FROM
		deviationzone d,
		subdistrecord_c dc,
		class_group cg
	WHERE
		dc.examyear = ? /* 7 */
	AND
		dc.examcd = ? /* 8 */
	AND
		dc.bundlecd = ? /* 9 */
	AND
		dc.subcd = ? /* 10 */
	AND
		dc.grade = cg.grade
	AND
		dc.class = cg.class
	AND
		cg.classgcd = ? /* 11 */
	AND
		d.devzonecd = dc.devzonecd
	GROUP BY
		d.highlimit,
		d.lowlimit
)
GROUP BY
	highlimit,
	lowlimit
ORDER BY
	lowlimit DESC

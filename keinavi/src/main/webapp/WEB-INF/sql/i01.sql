SELECT
	individualid,
	year,
	grade,
	class,
	TRIM(class_no),
	TRIM(name_kana),
	TRIM(name_kanji),
	grade_seq,
	NVL(sex, 9)
FROM (
	SELECT /*+ ORDERED USE_NL(b h) USE_NL(h cc) INDEX(b BASICINFO_IDX) FULL(cc) */
		h.individualid individualid,
		h.year year,
		h.grade grade,
		h.class class,
		h.class_no class_no,
		b.name_kana name_kana,
		b.name_kanji name_kanji,
		DECODE (h.grade, 3, 1, 1, 3, h.grade) grade_seq,
		b.sex sex 
	FROM
		basicinfo b
	INNER JOIN
		historyinfo h
	ON
		h.individualid = b.individualid
	AND
		h.year = ?
	INNER JOIN
		countchargeclass cc
	ON
		h.grade = cc.grade
	AND
		h.class = cc.class
	WHERE
		b.schoolcd = ?
	UNION
	SELECT /*+ ORDERED USE_NL(b h) USE_NL(h cc) INDEX(b BASICINFO_IDX) FULL(cc) */
		h.individualid,
		h.year,
		h.grade,
		h.class,
		h.class_no,
		b.name_kana,
		b.name_kanji,
		DECODE (h.grade, 3, 1, 1, 3, h.grade),
		b.sex 
	FROM
		basicinfo b
	INNER JOIN
		historyinfo h
	ON
		h.individualid = b.individualid
	AND
		h.year = ?
	INNER JOIN
		countchargeclass cc
	ON
		cc.grade = h.grade
	AND
		LENGTH(cc.class) > 2
	INNER JOIN
		class_group cg
	ON
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = cc.grade
	AND
		cg.class = h.class
	AND
		cg.classgcd = cc.class
	WHERE
		b.schoolcd = ?
	ORDER BY
		grade_seq,
		class,
		class_no
)

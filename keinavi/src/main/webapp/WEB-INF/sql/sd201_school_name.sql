SELECT
  BUNDLENAME || '�@' || DECODE(PACTDIV, 1, '�i�_��Z�j', '�i��_��Z�j')
FROM
  (
    SELECT
      BUNDLENAME
      , (
        SELECT
          1
        FROM
          DUAL
        WHERE
          EXISTS(
            SELECT
              1
            FROM
              PACTSCHOOL P
            WHERE
              (
                P.SCHOOLCD = S.BUNDLECD
                OR P.SCHOOLCD = S.PARENTSCHOOLCD
              )
              AND P.PACTDIV = '1'
              AND (
                P.PACTTERM IS NULL
                OR TO_DATE(P.PACTTERM, 'YYYYMMDD') >= SYSDATE
              )
          )
      ) AS PACTDIV
    FROM
      SCHOOL S
    WHERE
      S.BUNDLECD = ?
  )

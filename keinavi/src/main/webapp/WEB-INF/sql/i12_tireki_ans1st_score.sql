SELECT
	COURSE.COURSENAME,
	NVL(B.A_DEVIATION, -999),
	SUBCD,
	B.ACADEMICLEVEL,
	B.ANS1ST
FROM COURSE
LEFT JOIN (
	SELECT
		/*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */
		1 ANS1ST,
		SUBSTR(SUBRECORD_I.SUBCD, 1, 1) || '000' COURSECD,
		SUBRECORD_I.SUBCD SUBCD,
		SUBRECORD_I.A_DEVIATION A_DEVIATION,
		SUBRECORD_I.ACADEMICLEVEL
	FROM COURSE
	LEFT JOIN SUBRECORD_I
		ON COURSE.YEAR = SUBRECORD_I.EXAMYEAR
		AND SUBSTR(COURSE.COURSECD, 0, 1) = SUBSTR(SUBRECORD_I.SUBCD, 0, 1)
	INNER JOIN V_I_CMEXAMSUBJECT ES
		ON ES.EXAMYEAR = SUBRECORD_I.EXAMYEAR
		AND ES.EXAMCD = SUBRECORD_I.EXAMCD
		AND ES.SUBCD = SUBRECORD_I.SUBCD
	WHERE COURSE.COURSECD >= '5000'
		AND COURSE.COURSECD < '6000'
		AND SUBRECORD_I.INDIVIDUALID = ?	/* 1 */
		AND SUBRECORD_I.EXAMYEAR = ?		/* 2 */
		AND SUBRECORD_I.EXAMCD = ?			/* 3 */
		AND SUBRECORD_I.SUBCD >= '5000'
		AND SUBRECORD_I.SUBCD < '6000'
		AND SUBRECORD_I.SCOPE=1

	UNION

	SELECT
		A.ANS1ST,
		A.COURSECD,
		A.SUBCD,
		NVL(ROUND(AVG(A.A_DEVIATION),1), -999),
		A.ACADEMICLEVEL
	FROM (
		SELECT
			/*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */
			0 ANS1ST,
			SUBSTR(SUBRECORD_I.SUBCD, 1, 1) || '000' COURSECD,
			SUBSTR(SUBRECORD_I.SUBCD, 1, 1) || '000' SUBCD,
			SUBRECORD_I.A_DEVIATION A_DEVIATION,
			NULL ACADEMICLEVEL
		FROM COURSE
		LEFT JOIN SUBRECORD_I
			ON COURSE.YEAR = SUBRECORD_I.EXAMYEAR
			AND SUBSTR(COURSE.COURSECD, 0, 1) = SUBSTR(SUBRECORD_I.SUBCD, 0, 1)
		INNER JOIN V_I_CMEXAMSUBJECT ES
			ON ES.EXAMYEAR = SUBRECORD_I.EXAMYEAR
			AND ES.EXAMCD = SUBRECORD_I.EXAMCD
			AND ES.SUBCD = SUBRECORD_I.SUBCD
		WHERE COURSE.COURSECD >= '5000'
			AND COURSE.COURSECD < '6000'
			AND SUBRECORD_I.INDIVIDUALID = ?	/* 4 */
			AND SUBRECORD_I.EXAMYEAR = ?		/* 5 */
			AND SUBRECORD_I.EXAMCD = ?			/* 6 */
			AND SUBRECORD_I.SUBCD >= '5000'
			AND SUBRECORD_I.SUBCD < '6000'
			AND (SUBRECORD_I.SCOPE=9 OR SUBRECORD_I.SCOPE IS NULL)
	) A
	GROUP BY ANS1ST,COURSECD,SUBCD,ACADEMICLEVEL
) B
	ON B.COURSECD = COURSE.COURSECD
WHERE
	(COURSE.YEAR = ?) AND						/* 7 */
	COURSE.COURSECD = '5000'
ORDER BY COURSE.COURSECD ASC

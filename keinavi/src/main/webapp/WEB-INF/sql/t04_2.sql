SELECT /*+ USE_NL(rp rs) */
	0,
	rp.examyear,
	rp.examcd,
	e.examname,
	2,
	rp.prefcd,
	p.prefname,
	rp.prefcd || '999',
	TRIM('(' || p.prefname || ')'),
	99,
	'99',
	rp.ratingdiv RATINGDIV,
	rp.univcountingdiv UNIVCOUNTINGDIV,
	rp.univcd UNIVCD,
	rp.facultycd FACULTYCD,
	rp.deptcd DEPTCD,
	u.choicecd5 CHOICECD5,
	DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uniname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
		 ),
		u.uniname_abbr
	) univname_abbr,
	DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
			AND
				u.facultycd = rp.facultycd),
		u.facultyname_abbr
	),
	u.deptname_abbr,
	rp.agendacd AGENDACD,
	rp.studentdiv STUDENTDIV,
	rp.totalcandidatenum,
	rp.firstcandidatenum,
	rp.ratingnum_a,
	rp.ratingnum_b,
	rp.ratingnum_c,
	rp.ratingnum_d,
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	rp.ratingnum_e, */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD START */
/*	rp.eng_ratingnum_a, */
/*	rp.eng_ratingnum_b, */
/*	rp.eng_ratingnum_c, */
/*	rp.eng_ratingnum_d, */
/*	rp.eng_ratingnum_e */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD END */
	rp.ratingnum_e
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD START */
	/* ����w�P�� */
	/* ��w�敪 */
	, DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.unidiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
		 ),
		u.unidiv
	) unidiv_sort,
	/* ��w���J�i */
	DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.univname_kana)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
		 ),
		u.univname_kana
	) univname_kana_sort,
	/* ���w���P�� */
	/* ��w��ԋ敪 */
	DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uninightdiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
			AND
				u.facultycd = rp.facultycd
		 ),
		u.uninightdiv
	) uninightdiv_sort,
	/* �w�����e�R�[�h */
	DECODE(
		rp.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyconcd)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rp.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rp.univcd
			AND
				u.facultycd = rp.facultycd
		 ),
		u.facultyconcd
	) facultyconcd_sort
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_s rs */
/*	v_ratingnumber_s rs */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
/*INNER JOIN */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_p rp */
/*	v_ratingnumber_p rp */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
	ratingnumber_s rs
INNER JOIN
	ratingnumber_p rp
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
ON
	rp.examyear = rs.examyear
AND
	rp.examcd = rs.examcd
AND
	rp.ratingdiv = rs.ratingdiv
AND
	rp.univcountingdiv = rs.univcountingdiv
AND
	rp.univcd = rs.univcd
AND
	rp.facultycd = rs.facultycd
AND
	rp.deptcd = rs.deptcd
AND
	rp.agendacd = rs.agendacd
AND
	rp.studentdiv = rs.studentdiv
AND
	rp.prefcd = ?
INNER JOIN
	examination e
ON
	e.examyear = rs.examyear
AND
	e.examcd = rs.examcd
INNER JOIN
	prefecture p
ON
	p.prefcd = rp.prefcd
LEFT OUTER JOIN
	univmaster_basic u
ON
	u.eventyear = rp.examyear
AND
	u.examdiv = e.examdiv
AND
	u.univcd = rp.univcd
AND
	u.facultycd = rp.facultycd
AND
	u.deptcd = rp.deptcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
ORDER BY
	RATINGDIV,
	UNIVCOUNTINGDIV,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	unidiv_sort,
	univname_kana_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	UNIVCD,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* FACULTYCD, */
	uninightdiv_sort,
	facultyconcd_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	NVL(u.UNIGDIV, ''),
	NVL(u.DEPTSERIAL_NO, ''),
	NVL(u.SCHEDULESYS, ''),
	NVL(u.SCHEDULESYSBRANCHCD, ''),
	NVL(u.DEPTNAME_KANA, ''),
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	DEPTCD,
	AGENDACD,
	STUDENTDIV

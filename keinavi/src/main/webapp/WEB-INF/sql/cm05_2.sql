SELECT
	univcd,
	univname_abbr,
	univdivcd,
	prefcd_hq,
	totalcandidatenum
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
	 , univname_kana
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
FROM (
	SELECT
		u.univcd univcd,
		u.uniname_abbr univname_abbr,
		u.unidiv univdivcd,
		u.prefcd_examho prefcd_hq,
		NVL(rs.totalcandidatenum, 0) totalcandidatenum,
		u.facultycd facultycd1,
		FIRST_VALUE (u.facultycd) OVER (PARTITION BY u.univcd) facultycd2,
		u.deptcd deptcd1,
		FIRST_VALUE (u.deptcd) OVER (PARTITION BY u.univcd) deptcd2
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		univmaster_basic u,
		ratingnumber_s rs
	WHERE
		u.eventyear = ? /* 1 */
	AND
		u.examdiv = ? /* 2 */
	AND
		u.univcd IN (#)
	AND
		rs.examyear(+) = u.eventyear
	AND
		rs.examcd(+) = ? /* 3 */
	AND
		rs.bundlecd(+) = ? /* 4 */
	AND
		rs.ratingdiv(+) = ? /* 5 */
	AND
		rs.univcountingdiv(+) = '0'
	AND
		rs.studentdiv(+) = '0'
	AND
		rs.univcd(+) = u.univcd
)
WHERE
	facultycd1 = facultycd2
AND
	deptcd1 = deptcd2
	
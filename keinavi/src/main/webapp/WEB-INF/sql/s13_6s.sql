SELECT
  ma.subcd           /* 科目コード */
, es.subname         /* 科目名 */
, ltrim(ma.question_no, '0')      /* 設問番号 */
, qn.shoquestionname /* 設問名称 */
, qn.shoquestion_pt  /* 配点 */
, ms.avgpnt          /* 校内平均点 */
, ma.avgpnt          /* 全国平均点 */
FROM
	mathdescque_a ma                /* 数学記述式設問（全国） */

LEFT JOIN mathdescque_s ms          /* 数学記述式設問（高校） */

ON
	ms.examyear = ma.examyear

AND
	ms.examcd = ma.examcd

AND
	ms.subcd = ma.subcd

AND
	ms.question_no = ma.question_no

AND
	ms.bundlecd = ? /* 一括コード */

LEFT JOIN v_sm_qexamsubject es      /* 模試科目マスタ */

ON
	es.examyear = ma.examyear

AND
	es.examcd = ma.examcd

AND
	es.subcd = ma.subcd

LEFT JOIN questionname qn           /* 設問名称マスタ */

ON
	qn.eventyear = ma.examyear

AND
	qn.examcd = ma.examcd

AND
	qn.subcd = ma.subcd

AND
	qn.question_no = ms.question_no

WHERE ma.examyear = ? /* 対象年度 */

AND
	ma.examcd = ?     /* 模試コード */

ORDER BY
ms.subcd
,ms.question_no

SELECT /*+ ORDERED INDEX(ri PK_SUBRECORD_I) */
    c.coursecd,
    c.coursename,
    es.subcd,
    es.subaddrname,
    NVL(es.suballotpnt, -999),
    NVL(ri.score, -999),
    NVL(ri.s_deviation, -999),
    NVL(ri.a_deviation, -999),
    NVL(NVL(ts.numbers, rs.numbers), -999) ns,
    NVL(NVL(ta.numbers, ra.numbers), -999) na,
    NVL(ri.cvsscore, -999),
    NVL(ri.s_rank, -999),
    NVL(ri.a_rank, -999),
    ri.academiclevel,
    NVL(ri.scope, -999),
    getcharflagvalue(es.notdisplayflg, 5) basicflg,
    /* 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START */
    ri.individualid,
    ri.examyear,
    ri.examcd,
    ex.examtypecd,
    trim(kd_i.kokugo_desc_q1_rating),
    trim(kd_i.kokugo_desc_q2_rating),
    trim(kd_i.kokugo_desc_q3_rating),
    trim(kd_i.kokugo_desc_comp_rating)
    /* 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END */
FROM
    examsubject es

INNER JOIN
    course c
ON
    c.coursecd = SUBSTR(es.subcd, 1, 1) || '000'
AND
    c.year = es.examyear

INNER JOIN
    subrecord_i ri
ON
    ri.individualid = ? /* 1 */
AND
    ri.examyear = es.examyear
AND
    ri.examcd = es.examcd
AND
    ri.subcd = es.subcd

LEFT OUTER JOIN
    subrecord_s rs
ON
    rs.examyear = es.examyear
AND
    rs.examcd = es.examcd
AND
    rs.subcd = es.subcd
AND
    rs.bundlecd = ? /* 2 */

LEFT OUTER JOIN
    subrecord_s ts
ON
    ts.examyear = es.examyear
AND
    ts.examcd = es.examcd
AND
    ts.subcd = es.fusion_subcd
AND
    ts.bundlecd = ? /* 3 */

LEFT OUTER JOIN
    (
        SELECT
            ra.examyear examyear,
            ra.examcd examcd,
            MIN(ra.studentdiv) studentdiv
        FROM
            subrecord_a ra
        GROUP BY
            ra.examyear,
            ra.examcd
    ) a
ON
    a.examyear = es.examyear
AND
    a.examcd = es.examcd

LEFT OUTER JOIN
    subrecord_a ra
ON
    ra.examyear = a.examyear
AND
    ra.examcd = a.examcd
AND
    ra.subcd = es.subcd
AND
    ra.studentdiv = a.studentdiv

LEFT OUTER JOIN
    subrecord_a ta
ON
    ta.examyear = a.examyear
AND
    ta.examcd = a.examcd
AND
    ta.subcd = es.fusion_subcd
AND
    ta.studentdiv = a.studentdiv
/* 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD START */
INNER JOIN
	examination	ex
ON
	ex.examyear = es.examyear
AND
	ex.examcd = es.examcd
LEFT JOIN
	kokugo_desc_i kd_i
ON
	kd_i.individualid = ri.individualid
AND
	kd_i.examyear = es.examyear
AND
	kd_i.examcd = es.examcd
AND
	kd_i.subcd = es.subcd
/* 2019/09/20 QQ)Tanouchi 共通テスト対応 ADD END */
WHERE
    es.examyear = ? /* 4 */
AND
    es.examcd = ? /* 5 */
AND
    getcharflagvalue(es.notdisplayflg, 3) = '0'

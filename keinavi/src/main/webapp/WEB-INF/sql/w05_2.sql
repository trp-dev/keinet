SELECT
	e.examyear,
	e.examcd,
	e.examname,
	e.examname_abbr,
	e.examtypecd,
	e.examst,
	e.examdiv,
	e.tergetgrade,
	e.inpledate,
	e.out_dataopendate,
	e.dockingexamcd,
	e.dockingtype,
	e.dispsequence,
	e.masterdiv
FROM
	examination e,
	school s
WHERE
	e.examyear >= ? /* 1 */
AND
	SUBSTR(e.out_dataopendate, 1, 8) <= ? /* 2 */
AND
	s.bundlecd = ? /* 3 */
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
AND EXISTS (
	SELECT
		rs.numbers
	FROM
		examtakenum_s rs
	WHERE
		rs.examyear = e.examyear
	AND
		rs.examcd = e.examcd
	AND
		rs.bundlecd = s.bundlecd
)
ORDER BY
	e.dispsequence

SELECT
	s.bundlename,
	et.examyear,
	NVL(rs.totalcandidatenum, -999),
	NVL(rs.firstcandidatenum, -999),
	NVL(rs.ratingnum_a, -999),
	NVL(rs.ratingnum_b, -999),
	NVL(rs.ratingnum_c, -999),
	NVL(rs.ratingnum_d, -999),
	NVL(rs.ratingnum_e, -999)
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(rs.eng_ratingnum_a, -999), */
	/* NVL(rs.eng_ratingnum_b, -999), */
	/* NVL(rs.eng_ratingnum_c, -999), */
	/* NVL(rs.eng_ratingnum_d, -999), */
	/* NVL(rs.eng_ratingnum_e, -999)  */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
INNER JOIN
	examination e
ON
	e.examyear = et.examyear
AND
	e.examcd = et.examcd
LEFT OUTER JOIN
	school s
ON
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
LEFT OUTER JOIN
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_s rs */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
	ratingnumber_s rs
/* 2019/11/25 QQ)nagai 英語認定試験延期対応 DEL END   */
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.studentdiv = ? /* 1 */
AND
	rs.univcountingdiv = ? /* 2 */
AND
	rs.univcd = ? /* 3 */
AND
	rs.facultycd = ? /* 4 */
AND
	rs.deptcd = ? /* 5 */
AND
	rs.agendacd = ? /* 6 */
AND
	rs.ratingdiv = ? /* 7 */
AND
	rs.bundlecd = s.bundlecd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 8 */
AND
	s.bundlecd = ? /* 9 */
ORDER BY
	et.examyear DESC

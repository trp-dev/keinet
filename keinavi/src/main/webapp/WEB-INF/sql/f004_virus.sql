SELECT 
	uf.uploadfile_id, 
	uf.schoolcd, 
	uf.loginid, 
	uf.update_date, 
	uf.uploadfilename, 
	uf.serverfilename, 
	uf.filetype_id, 
	uf.upload_comment, 
	uf.status,
	uf.last_download_date
FROM 
	uploadfile uf
WHERE 
	uf.schoolcd = ? 
AND 
	uf.status = '9' 
AND
	uf.update_date > (SYSDATE - ?)
ORDER BY 
	uf.update_date DESC

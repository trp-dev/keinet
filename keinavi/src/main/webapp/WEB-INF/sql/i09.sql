SELECT
    n.uniname_abbr univname_abbr,
    n.deptname_abbr,
    n.facultyname_abbr,
    n.univcd,
    n.deptcd,
    n.facultycd,
    nvl(n.deptsortkey, ' ') deptsortkey,
    n.unigdiv schedulecd,
    n.borderscorerate2 cen_scorerate,
    decode(n.unidiv, '01', '02', n.unidiv) as natlpvtdiv,
    n.uninightdiv,
    n.facultyconcd,
    n.deptserial_no,
    n.schedulesys,
    n.schedulesysbranchcd,
    n.deptname_kana,
    i.kana_num,
    r.rankllimits,
    d.SCHEDULEBRANCHNAME,
    d.ENTEXAMINPLEDATE1_1,
    d.ENTEXAMINPLEDATE1_2,
    d.ENTEXAMINPLEDATE1_3,
    d.ENTEXAMINPLEDATE1_4,
    d.ENTEXAMINPLEDATE2_1,
    d.ENTEXAMINPLEDATE2_2,
    d.ENTEXAMINPLEDATE2_3,
    d.ENTEXAMINPLEDATE2_4,
    d.PLEDATEDIV1_1,
    d.PLEDATEDIV1_2,
    d.PLEDATEDIV1_3,
    d.PLEDATEUNITEDIV,
    d.PLEDATEDIV2_1,
    d.PLEDATEDIV2_2,
    d.PLEDATEDIV2_3,
    d.ENTEXAMAPPLIDEADLINE,
    d.ENTEXAMAPPLIDEADLINEDIV,
    d.sucanndate,
    d.procedeadline,
    decode(a.entexammodecd, '01', '���', '02', '���E', '03', 'ao', '04', '�����E���', '05', '���̑�', null) entexammodename,
    a.entexammodecd,
    a.entexamdiv1,
    a.entexamdiv2,
    TRIM(a.entexamdiv3) entexamdiv3,
    a.examplandate1,
    a.examplandate2,
    a.examplandate3,
    a.examplandate4,
    a.examplandate5,
    a.examplandate6,
    a.examplandate7,
    a.examplandate8,
    a.examplandate9,
    a.examplandate10,
    a.proventexamsite1,
    a.proventexamsite2,
    a.proventexamsite3,
    a.proventexamsite4,
    a.proventexamsite5,
    a.proventexamsite6,
    a.proventexamsite7,
    a.proventexamsite8,
    a.proventexamsite9,
    a.proventexamsite10,
    a.year,
    a.examdiv 
FROM
    (SELECT
        p.individualid,
        p.year,
        p.examdiv,
        p.entexammodecd,
        p.entexamdiv1,
        p.entexamdiv2,
        p.entexamdiv3,
        p.univcd final_univcd,
        p.deptcd final_deptcd,
        p.facultycd final_facultycd,
        p.examplandate1,
        p.examplandate2,
        p.examplandate3,
        p.examplandate4,
        p.examplandate5,
        p.examplandate6,
        p.examplandate7,
        p.examplandate8,
        p.examplandate9,
        p.examplandate10,
        p.proventexamsite1,
        p.proventexamsite2,
        p.proventexamsite3,
        p.proventexamsite4,
        p.proventexamsite5,
        p.proventexamsite6,
        p.proventexamsite7,
        p.proventexamsite8,
        p.proventexamsite9,
        p.proventexamsite10 
    FROM
        examplanuniv p 
    WHERE
        p.year = (SELECT
                        max(eventyear)
                    FROM
                        univmaster_basic
                    WHERE
                        univcd = p.univcd 
					AND
                        facultycd = p.facultycd 
					AND
                        deptcd = p.deptcd)     
	AND
        p.examdiv = (SELECT
                        max (examdiv)
                    FROM
                        univmaster_basic
                    WHERE
                        eventyear = (SELECT
                                        max(eventyear)
                                    FROM
                                        univmaster_basic
                                    WHERE
                                        univcd = p.univcd 
									AND
                                        facultycd = p.facultycd 
									AND
                                        deptcd = p.deptcd) 
					AND
                        univcd = p.univcd 
					AND
                        facultycd = p.facultycd 
					AND
                        deptcd = p.deptcd)) a    
	INNER JOIN 
		univmaster_basic n  
	ON 
		n.univcd = a.final_univcd  
	AND 
		n.deptcd = a.final_deptcd    
	AND 
		n.facultycd = a.final_facultycd    
	AND 
		n.eventyear = a.year
	AND 
		n.examdiv = a.examdiv
	LEFT JOIN 
		examscheduledetail d
	ON 
		d.year = n.eventyear
	AND
		d.examdiv = n.examdiv
	AND 
		d.univcd = n.univcd
	AND 
		d.deptcd = n.deptcd
	AND 
		d.facultycd = n.facultycd
	AND 
		d.entExamDiv_1_2order = a.entexamdiv1
	AND 
		d.schoolProventDiv = a.entexamdiv2
	AND 
		d.entExamDiv_1_2term = a.entexamdiv3
	LEFT JOIN 
		rank r  
	ON 
		n.entexamrank = r.rankcd
	LEFT JOIN
		univmaster_info i
	ON
		i.eventyear = n.eventyear
	AND 
		i.univcd = n.univcd
WHERE
	a.individualid = ?
ORDER BY 
	n.univcd, 
	n.deptcd, 
	n.facultycd, 
	a.entexammodecd, 
	a.entexamdiv1, 
	a.entexamdiv2, 
	a.entexamdiv3

SELECT
	s.bundlename,
	NVL(a.numbers, -999),
	NVL(a.avgpnt, -999),
	NVL(a.avgdeviation, -999)
FROM
	school s,
	examination e,
	(
		SELECT
			rs.bundlecd bundlecd,
			rs.numbers numbers,
			rs.avgpnt avgpnt,
			rs.avgdeviation avgdeviation
		FROM
			subrecord_s rs,
			school s,
			examination e,
			subcdtrans st
		WHERE
			rs.examyear = ? /* 1 */
		AND
			rs.examcd = ? /* 2 */
		AND
			rs.subcd = st.subcd
		AND
			st.examyear = rs.examyear
		AND
			st.examcd = rs.examcd
		AND
			st.cursubcd = ? /* 3 */
		AND
			rs.bundlecd = ? /* 4 */
		AND
			s.bundlecd = rs.bundlecd
		AND
			e.examyear = rs.examyear
		AND
			e.examcd = rs.examcd
		AND
			NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
		AND
			NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
	) a
WHERE
	a.bundlecd(+) = s.bundlecd
AND
	s.bundlecd = ? /* 5 */
AND
	e.examyear = ? /* 6 */
AND
	e.examcd = ? /* 7 */
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)

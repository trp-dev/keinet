SELECT
	bundlecd,
	bundlename,
	bundlename_kana,
	rank,
	facultycd,
	NVL(numbers, 0) numbers
FROM
	(
		SELECT
			'99999' bundlecd,
			TRIM('�i�S���j') bundlename,
			NULL bundlename_kana,
			NULL rank,
			'00' facultycd,
			NVL(ra.numbers, 0) numbers,
			1 dispsequence
		FROM
			DUAL
		LEFT OUTER JOIN
			(
				SELECT
					ra.examyear examyear,
					ra.examcd examcd,
					ra.numbers numbers
				FROM
					examtakenum_a ra
				INNER JOIN
					(
						SELECT
							MIN(a.studentdiv) studentdiv
						FROM
							examtakenum_a a
						WHERE
							a.examyear = ?
						AND
							a.examcd = ?
					) a
				ON
					a.studentdiv = ra.studentdiv
			) ra
		ON
			ra.examyear = ?
		AND
			ra.examcd = ?

		UNION ALL

		SELECT
			p.prefcd || '999',
			'�i' || p.prefname || '�j',
			NULL,
			NULL,
			p.prefcd,
			NVL(ep.numbers, 0),
			2
		FROM
			prefecture p
		LEFT OUTER JOIN
			examtakenum_p ep
		ON
			ep.examyear = ? /* 3 */
		AND
			ep.examcd = ? /* 4 */
		AND
			ep.prefcd = p.prefcd
		
		UNION ALL
		
		SELECT
			s.bundlecd,
			s.bundlename,
			s.bundlename_kana,
			s.rank,
			s.facultycd,
			rs.numbers,
			3
		FROM
			school s
		LEFT OUTER JOIN
			examtakenum_s rs
		ON
			rs.examyear = ? /* 5 */
		AND
			rs.examcd = ? /* 6 */
		AND
			rs.bundlecd = s.bundlecd
		WHERE
			NVL(s.newgetdate, '00000000') <= TO_CHAR(SYSDATE, 'YYYYMMDD')
		AND
			NVL(s.stopdate, '99999999') > TO_CHAR(SYSDATE, 'YYYYMMDD')
		AND
			s.bundlecd NOT IN ('27819')
		# /* �i���ݏ��� */
	)
WHERE
	1 = 1

SELECT
	studentdiv,
	univcd,
	univname_abbr,
	facultycd,
	facultyname_abbr,
	deptcd,
	deptname_abbr,
	agendacd,
	schedulename,
	ratingdiv
FROM (
	SELECT
		ra.studentdiv studentdiv,
		ra.univcd univcd,
		u.uniname_abbr univname_abbr,
		ra.facultycd facultycd,
		u.facultyname_abbr,
		ra.deptcd deptcd,
		NULL deptname_abbr,
		ra.agendacd agendacd,
		s.schedulename schedulename,
		ra.ratingdiv ratingdiv,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		u.deptcd deptcd1,
		FIRST_VALUE(u.deptcd) OVER (PARTITION BY ra.univcd, ra.facultycd) deptcd2
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		 , u.uninightdiv
		 , u.facultyconcd
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
		/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
		/* v_ratingnumber_a ra, */
		/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
		ratingnumber_a ra,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
		schedule s,
		countuniv c,
		univmaster_basic u
	WHERE
		ra.examyear = ? /* 1 */
	AND
		ra.examcd = ? /* 2 */
	AND
		ra.univcountingdiv = ? /* 3 */
	AND
		s.schedulecd = ra.agendacd
	AND
		c.univcd = ra.univcd
	AND
		u.univcd = c.univcd
	AND
		u.facultycd = ra.facultycd
	AND
		u.eventyear = ra.examyear
	AND
		u.examdiv = ? /* 4 */
	AND
		ra.studentdiv IN (#)
	AND
		ra.ratingdiv IN (#)
	/* SCHEDULE CONDITION_A */
)
WHERE
	deptcd1 = deptcd2
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	facultycd,
	agendacd,
	ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

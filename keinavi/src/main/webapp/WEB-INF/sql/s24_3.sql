SELECT
	studentdiv,
	univcd,
	univname_abbr,
	facultycd,
	facultyname_abbr,
	deptcd,
	deptname_abbr,
	agendacd,
	schedulename,
	ratingdiv
FROM (
	SELECT
		rs.studentdiv studentdiv,
		rs.univcd univcd,
		u.uniname_abbr univname_abbr,
		rs.facultycd facultycd,
		u.facultyname_abbr,
		rs.deptcd deptcd,
		NULL deptname_abbr,
		rs.agendacd agendacd,
		s.schedulename schedulename,
		rs.ratingdiv ratingdiv,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		u.deptcd deptcd1,
		FIRST_VALUE(u.deptcd) OVER (PARTITION BY rs.univcd, rs.facultycd) deptcd2
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		 , u.uninightdiv
		 , u.facultyconcd
		/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		/* 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD START */
		/* 2019/09/02 QQ)Tanouchi ���ʃe�X�g�Ή� UPD START */
		ratingnumber_s rs,
		/*v_ratingnumber_s rs,*/
		/* 2019/09/02 QQ)Tanouchi ���ʃe�X�g�Ή� UPD END */
		/* 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD END */
		schedule s,
		countuniv c,
		univmaster_basic u
	WHERE
		rs.examyear = ? /* 1 */
	AND
		rs.examcd = ? /* 2 */
	AND
		rs.bundlecd = ? /* 3 */
	AND
		rs.univcountingdiv = ? /* 4 */
	AND
		s.schedulecd = rs.agendacd
	AND
		c.univcd = rs.univcd
	AND
		u.univcd = c.univcd
	AND
		u.facultycd = rs.facultycd
	AND
		u.eventyear = rs.examyear
	AND
		u.examdiv = ? /* 5 */
	AND
		rs.studentdiv IN (#)
	AND
		rs.ratingdiv IN (#)
	/* SCHEDULE CONDITION */
)
WHERE
	deptcd1 = deptcd2
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	facultycd,
	agendacd,
	ratingdiv
 */
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL END */

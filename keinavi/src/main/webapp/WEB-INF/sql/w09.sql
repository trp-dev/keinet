SELECT
	1
FROM
	login_charge c
WHERE
	c.schoolcd = ? /* 1 */
AND
	c.loginid = ?
AND
	c.year = ?
AND
	c.grade = ?
AND
	c.class = ?

UNION ALL

SELECT
	1
FROM
	DUAL
WHERE
	(
		SELECT
			DECODE(count(cg.rowid), 0, -1, count(cg.rowid))
		FROM
			class_group cg
		WHERE
			cg.year = ? /* 6 */
		AND
			cg.bundlecd = ?
		AND
			cg.grade = ?
		AND
			cg.classgcd = ?
	) = (
		SELECT
			count(cg.rowid)
		FROM
			class_group cg
		INNER JOIN
			login_charge c
		ON
			c.schoolcd = cg.bundlecd
		AND
			c.year = cg.year
		AND
			c.loginid = ?
		AND
			c.grade = cg.grade
		AND
			c.class = cg.class
		WHERE
			cg.year = ? /* 11 */
		AND
			cg.bundlecd = ?
		AND
			cg.grade = ?
		AND
			cg.classgcd = ?
	)

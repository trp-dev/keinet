SELECT
	s.bundlename bundlename,
	et.examyear examyear,
	NVL(rs.totalcandidatenum, -999) totalcandidatenum,
	NVL(rs.firstcandidatenum, -999) firstcandidatenum,
	NVL(rs.ratingnum_a, -999) ratingnum_a,
	NVL(rs.ratingnum_b, -999) ratingnum_b,
	NVL(rs.ratingnum_c, -999) ratingnum_c,
	NVL(rs.ratingnum_d, -999) ratingnum_d,
	NVL(rs.ratingnum_e, -999) ratingnum_e
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL START */
	/* 2019/09/30 QQ) 共通テスト対応 ADD START */
	/* NVL(rs.eng_ratingnum_a, -999) eng_ratingnum_a, */
	/* NVL(rs.eng_ratingnum_b, -999) eng_ratingnum_b, */
	/* NVL(rs.eng_ratingnum_c, -999) eng_ratingnum_c, */
	/* NVL(rs.eng_ratingnum_d, -999) eng_ratingnum_d, */
	/* NVL(rs.eng_ratingnum_e, -999) eng_ratingnum_e */
	/* 2019/09/30 QQ) 共通テスト対応 ADD END */
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 DEL END   */
FROM
	examcdtrans et
INNER JOIN
	school s
ON
	s.bundlecd = ? /* 1 */
LEFT OUTER JOIN
	examination e
ON
	e.examyear = et.examyear
AND
	e.examcd = et.examcd
AND
	NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
AND
	NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
LEFT OUTER JOIN
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 UPD START */
	/* 2019/09/30 QQ) 共通テスト対応 UPD START */
	/* v_ratingnumber_s rs */
	/* 2019/09/30 QQ) 共通テスト対応 UPD END */
	ratingnumber_s rs
/* 2019/11/26 QQ)nagai 英語認定試験延期対応 UPD END   */
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.ratingdiv = ? /* 2 */
AND
	rs.univcountingdiv = ? /* 3 */
AND
	rs.univcd = ? /* 4 */
AND
	rs.facultycd = ? /* 5 */
AND
	rs.deptcd = ? /* 6 */
AND
	rs.agendacd = ? /* 7 */
AND
	rs.studentdiv = ? /* 8 */
AND
	rs.bundlecd = s.bundlecd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 9 */
ORDER BY
	examyear DESC,
	totalcandidatenum DESC

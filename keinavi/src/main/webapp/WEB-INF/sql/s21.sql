SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	e.examyear,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	examination e

INNER JOIN
	v_sm_cmexamsubject es
ON
	es.examyear = ? /* 1 */
AND
	es.examcd = ? /* 2 */
AND
	es.subcd IN (#)

LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = e.examyear
AND
	ee.examcd = e.examcd

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = ee.examyear
AND
	st.examcd = es.examcd
AND
	st.cursubcd = es.subcd

LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = ? /* 3 */

WHERE
	e.examyear IN (#)
AND
	e.examcd = (SELECT ex.examcd
				FROM examcdtrans ex
				WHERE ex.examyear = e.examyear
				AND ex.curexamcd = ?) /* 4 */
ORDER BY
	es.dispsequence,
	e.examyear DESC

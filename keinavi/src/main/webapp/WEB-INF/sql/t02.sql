SELECT /*+ ORDERED USE_NL(b h) USE_NL(b cr) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
	cr.individualid individualid,
	cr.candidaterank candidaterank,
	cr.univcd || cr.facultycd || cr.deptcd,
	' ' superabbrname,
	cr.facultycd,
	cr.deptcd,
	cr.ratingcvsscore,
	cr.ratingdeviation,
	cr.centerrating,
	cr.secondrating,
	cr.ratingpoint,
	cr.totalrating,
	u.unigdiv schedulecd,
	u.uniname_abbr univname_abbr,
	u.facultyname_abbr,
	u.deptname_abbr,
	/* 2020/1/7 QQ)Ooseto �L�q�����Ή� UPD START */
	/*u.choicecd5,*/
	u.choicecd5
	/* 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� UPD START */
	/*cr.cenfull*/
	/*cr.cenfull,*/
	/*cr.cenfull_engpt,*/
	/*cr.ratingcvsscore_engpt,*/
	/*cr.centerrating_engpt,*/
	/*cr.app_qual_judge,*/
	/*cr.app_qual_notice*/
	/* 2019/11/19 QQ)Ooseto �p��F�莎�������Ή� UPD END */
	/* 2020/1/7 QQ)Ooseto �L�q�����Ή� UPD END */
FROM
	basicinfo b,
	historyinfo h,
	candidaterating cr,
	univmaster_basic u
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	cr.individualid = h.individualid
AND
	cr.examyear = h.year
AND
	cr.examcd = ? /* 3 */
AND
	u.eventyear(+) = cr.examyear
AND
	u.examdiv(+) = ? /* 4 */
AND
	u.univcd(+) = cr.univcd
AND
	u.facultycd(+) = cr.facultycd
AND
	u.deptcd(+) = cr.deptcd
ORDER BY
	individualid,
	candidaterank

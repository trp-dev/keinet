SELECT /*+ ORDERED USE_NL(b h) USE_NL(a h) USE_NL(c h) INDEX(b BASICINFO_IDX) */
	h.individualid,
	a.year,
	a.grade,
	UPPER(a.class),
	a.class_no,
	c.year,
	c.grade,
	UPPER(c.class),
	c.class_no,
	h.year,
	h.grade,
	UPPER(h.class),
	h.class_no,
	DECODE(b.sex, '1', '1', '2', '2', '9'),
	REPLACE(TRIM(b.name_kana), ' ', '　'),
	TRIM(b.name_kanji),
	CASE
		WHEN
			LENGTH(b.birthday) = 8
		AND
			SUBSTR(b.birthday, 5, 2) <= 12 
		AND
			SUBSTR(b.birthday, 7, 2) <= 31 
		THEN
			TO_CHAR(TO_DATE(b.birthday,'YYYYMMDD'), 'YYYY/MM/DD')
		ELSE
			NULL
	END CASE,
	TRIM(b.tel_no)
FROM
	basicinfo b,
	historyinfo h,
	(
		/* 前々年度 */
		SELECT
			h.year year,
			h.individualid individualid,
			h.grade grade,
			h.class class,
			h.class_no class_no
		FROM
			historyinfo h
		WHERE
			h.year = ? /* 1 */
	) a,
	(
		/* 前年度 */
		SELECT
			h.year year,
			h.individualid individualid,
			h.grade grade,
			h.class class,
			h.class_no class_no
		FROM
			historyinfo h
		WHERE
			h.year = ? /* 2 */
	) c
WHERE
	h.individualid = b.individualid
AND
	h.year = ? /* 3 */
AND
	b.schoolcd = ? /* 4 */
AND
	a.individualid(+) = h.individualid
AND
	c.individualid(+) = h.individualid

AND h.grade = ? /* 5 */

AND UPPER(h.class) = ? /* 6 */

ORDER BY
	h.class,
	h.class_no

/*�Ȗڕʐ��сi���j*/

SELECT 
	'�i' || p.prefname ||'�j',
	NVL(rp.numbers, -999),
	NVL(rp.avgscorerate, -999)
FROM
	SUBRECORD_P RP,
	PREFECTURE P
WHERE
	RP.EXAMYEAR(+) = ? AND /* 1 */
	RP.EXAMCD(+) = ? AND /* 2 */
	RP.PREFCD(+) = P.PREFCD AND
	RP.SUBCD(+) = ? AND /* 3 */
	P.PREFCD = ? /* 4 */

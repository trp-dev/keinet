SELECT
	/* 認定試験情報マスタ.参加試験コード */
	ei.engptcd,
	/* 認定試験情報マスタ.参加試験短縮名 */
	ei.engptname_abbr,
	/* 認定試験情報マスタ.レベルありフラグ */
	ei.levelflg,
	/* 認定試験情報マスタ.並び順 */
	ei.sort AS ei_sort,
	/* 認定試験詳細情報マスタ.参加試験レベルコード */
	edi.engptlevelcd,
	/* 認定試験詳細情報マスタ.参加試験レベル短縮名 */
	edi.engptlevelname_abbr,
	/* 認定試験詳細情報マスタ.並び順 */
	edi.sort AS edi_sort
FROM
	/* 認定試験情報マスタTBL */
	engptinfo ei
	/* 認定試験詳細情報マスタTBL */
	INNER JOIN engptdetailinfo edi
	ON
		edi.eventyear = ei.eventyear
	AND
		edi.examdiv = ei.examdiv
	AND
		edi.engptcd = ei.engptcd
WHERE
	ei.eventyear = ? /* '1' */
AND
	ei.examdiv = ? /* '2' */
ORDER BY
	ei_sort,
	edi_sort

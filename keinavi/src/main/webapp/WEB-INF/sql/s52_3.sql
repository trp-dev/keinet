SELECT
	rs.subcd subcd,
	NVL(rs.avgpnt, -999) avgpnt,
	NVL(rs.numbers, -999) numbers
FROM
	subcdtrans st
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = st.examyear
AND
	rs.examcd = st.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = ?
WHERE
	st.examyear = ?
AND
	st.examcd = ?
AND
	st.cursubcd = ?

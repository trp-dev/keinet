SELECT
	cr.examyear,
	cr.examcd,
	cr.bundlecd,
	cr.grade,
	cr.class,
	'2',
	cr.univcd,
	cr.facultycd,
	'ZZZZ',
	'Z',
	DECODE(cr.grade, 1, '1', 2, '1', 3, '1', '2') SDIV,
	COUNT(*) TC,
	SUM(DECODE(cr.candidaterank, 1, 1, 0)) FC,
	SUM(DECODE(cr.centerrating, NULL, 0, 1)) CFLAG,
	SUM(DECODE(cr.centerrating, 'A', 1, 0)) CA,
	SUM(DECODE(cr.centerrating, 'B', 1, 0)) CB,
	SUM(DECODE(cr.centerrating, 'C', 1, 0)) CC,
	SUM(DECODE(cr.centerrating, 'D', 1, 0)) CD,
	SUM(DECODE(cr.centerrating, 'E', 1, 0)) CE,
	SUM(DECODE(cr.secondrating, NULL, 0, 1)) SFLAG,
	SUM(DECODE(cr.secondrating, 'A', 1, 0)) SA,
	SUM(DECODE(cr.secondrating, 'B', 1, 0)) SB,
	SUM(DECODE(cr.secondrating, 'C', 1, 0)) SC,
	SUM(DECODE(cr.secondrating, 'D', 1, 0)) SD,
	SUM(DECODE(cr.secondrating, 'E', 1, 0)) SE,
	SUM(DECODE(cr.totalrating, NULL, 0, 1)) TFLAG,
	SUM(DECODE(cr.totalrating, 'A', 1, 0)) TA,
	SUM(DECODE(cr.totalrating, 'B', 1, 0)) TB,
	SUM(DECODE(cr.totalrating, 'C', 1, 0)) TC,
	SUM(DECODE(cr.totalrating, 'D', 1, 0)) TD,
	SUM(DECODE(cr.totalrating, 'E', 1, 0)) TE
FROM
	rnc_recount_work cr
WHERE
	EXISTS (
		SELECT
			1
		FROM
			ratingnumberrecount re
		WHERE
			re.year = cr.examyear
		AND
			re.examcd = cr.examcd
		AND
			re.bundlecd = cr.bundlecd
		AND
			re.grade = cr.grade
		AND
			re.class = cr.class
		AND
			re.univcd = cr.univcd)
GROUP BY
	cr.examyear,
	cr.examcd,
	cr.bundlecd,
	cr.grade,
	cr.class,
	cr.univcd,
	cr.facultycd

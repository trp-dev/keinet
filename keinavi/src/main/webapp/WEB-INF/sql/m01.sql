SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) */ DISTINCT 
	h.year year,
	h.grade grade,
	UPPER(h.class) class,
	DECODE(h.grade, 3, 1, 1, 3, h.grade) grade_seq
FROM
	basicinfo b,
	historyinfo h
WHERE
	b.individualid = h.individualid
AND
	b.schoolcd = ?
ORDER BY
	year DESC,
	grade_seq,
	class

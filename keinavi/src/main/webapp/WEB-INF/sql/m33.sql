UPDATE
	basicinfo b
SET
	b.notdisplayflg = '1'
WHERE
	b.individualid = ?
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			individualrecord ri
		WHERE
			ri.s_individualid = b.individualid)

/* 全試験情報取得 */
SELECT
    info.CERFLEVELNAME_ABBR
  , this.NUMBERS   AS NUMBERS_S
  , this.COMPRATIO AS COMPRATIO_S
  , pref.NUMBERS   AS NUMBERS_P
  , pref.COMPRATIO AS COMPRATIO_P
  , zenk.NUMBERS   AS NUMBERS_A
  , zenk.COMPRATIO AS COMPRATIO_A
FROM
    EXAMINATION mosi  /* 模試マスタ */
INNER JOIN
    (
        SELECT
             EVENTYEAR
           , EXAMDIV
           , CEFRLEVELCD
           , CASE
                 WHEN CEFRLEVELCD = '99' THEN 'なし'
                 ELSE                         CERFLEVELNAME_ABBR
             END CERFLEVELNAME_ABBR
           , SORT
        FROM
             CEFR_INFO
        UNION ALL
        SELECT
             EVENTYEAR
           , EXAMDIV
           , 'ZZ'
           , 'ZZ'
           , 999
        FROM
             CEFR_INFO
        GROUP BY
             EVENTYEAR
           , EXAMDIV
    ) info  /* CEFR情報 */
    ON    mosi.EXAMYEAR    = info.EVENTYEAR
    AND   mosi.EXAMDIV     = info.EXAMDIV
LEFT OUTER JOIN
    CEFRACQUISITIONSTATUS_S this /* CEFR取得状況（高校） */
    ON    mosi.EXAMYEAR    = this.EXAMYEAR
    AND   mosi.EXAMCD      = this.EXAMCD
    AND   info.CEFRLEVELCD = this.CEFRLEVELCD
    AND   this.BUNDLECD     = ? /* 1 */
LEFT OUTER JOIN
    CEFRACQUISITIONSTATUS_P pref /* CEFR取得状況（県） */
    ON    mosi.EXAMYEAR    = pref.EXAMYEAR
    AND   mosi.EXAMCD      = pref.EXAMCD
    AND   info.CEFRLEVELCD = pref.CEFRLEVELCD
    AND   pref.PREFCD       = ? /* 2 */
LEFT OUTER JOIN
    CEFRACQUISITIONSTATUS_A zenk /* CEFR取得状況（全国） */
    ON    mosi.EXAMYEAR    = zenk.EXAMYEAR
    AND   mosi.EXAMCD      = zenk.EXAMCD
    AND   info.CEFRLEVELCD = zenk.CEFRLEVELCD
WHERE     mosi.EXAMYEAR    = ? /* 3 */
    AND   mosi.EXAMCD      = ? /* 4 */
ORDER BY
    info.SORT ASC

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) INDEX(qi PK_QRECORD_I) */
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	b.sex,
	NVL(qi.qscore, -999),
	NVL(qi.scorerate, -999),
	ri.s_rank
FROM
	basicinfo b,
	historyinfo h,
	subrecord_i ri,
	qrecord_i qi
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	h.grade = ? /* 3 */
AND
	h.class = ? /* 4 */
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 5 */
AND
	ri.examcd = ? /* 6 */
AND
	ri.subcd = ? /* 7 */
AND
	qi.individualid(+) = ri.individualid
AND
	qi.examyear(+) = ri.examyear
AND
	qi.examcd(+) = ri.examcd
AND
	qi.subcd(+) = ri.subcd
AND
	qi.question_no(+) = ? /* 8 */

UNION ALL

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) INDEX(qi PK_QRECORD_I) */
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	b.sex,
	NVL(qi.qscore, -999),
	NVL(qi.scorerate, -999),
	ri.s_rank
FROM
	basicinfo b,
	historyinfo h,
	class_group cg,
	subrecord_i ri,
	qrecord_i qi
WHERE
	b.schoolcd = ? /* 9 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 10 */
AND
	cg.year = h.year
AND
	cg.bundlecd = b.schoolcd
AND
	cg.grade = h.grade
AND
	cg.class = h.class
AND
	cg.classgcd = ? /* 11 */
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 12 */
AND
	ri.examcd = ? /* 13 */
AND
	ri.subcd = ? /* 14 */
AND
	qi.individualid(+) = ri.individualid
AND
	qi.examyear(+) = ri.examyear
AND
	qi.examcd(+) = ri.examcd
AND
	qi.subcd(+) = ri.subcd
AND
	qi.question_no(+) = ? /* 15 */

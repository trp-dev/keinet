insert into SUBRECOUNT
select
		  a.EXAMYEAR
		, a.EXAMCD
		, a.BUNDLECD
		, a.GRADE
		, a.CLASS
		, a.SUBCD
	from
		(
			select distinct
					  c.EXAMYEAR
					, c.EXAMCD
					, c.BUNDLECD
					, c.GRADE
					, c.CLASS
					, c.SUBCD
				from
					SUBRECORD_I i
					inner join SUBRECORD_C c
						on i.EXAMYEAR = c.EXAMYEAR
						and i.EXAMCD = c.EXAMCD
						and i.BUNDLECD = c.BUNDLECD
				where
					c.EXAMYEAR = ?
					and c.EXAMCD = ?
					and c.GRADE = ?
					and c.CLASS = ?
			union
			select distinct
					  i.EXAMYEAR
					, i.EXAMCD
					, i.BUNDLECD
					, c.GRADE
					, c.CLASS
					, i.SUBCD
				from
					SUBRECORD_I i
					inner join SUBRECORD_C c
						on i.EXAMYEAR = c.EXAMYEAR
						and i.EXAMCD = c.EXAMCD
						and i.BUNDLECD = c.BUNDLECD
				where
					c.EXAMYEAR = ?
					and c.EXAMCD = ?
					and c.GRADE = ?
					and c.CLASS = ?
		) a
	where
		exists (
			select
					  1
				from
					SUBRECORD_I ii
				where
					a.EXAMYEAR = ii.EXAMYEAR
					and a.EXAMCD = ii.EXAMCD
					and a.BUNDLECD = ii.BUNDLECD
					and ii.INDIVIDUALID = ?
		)
		and not exists (
			select
					  1
				from
					SUBRECOUNT re
				where
					re.EXAMYEAR = a.EXAMYEAR
					and re.EXAMCD = a.EXAMCD
					and re.BUNDLECD = a.BUNDLECD
					and re.GRADE = a.GRADE
					and re.CLASS = a.CLASS
					and re.SUBCD = a.SUBCD
		)

SELECT
	m.univcd univcd,
	m.univname_abbr,
	m.univdivcd,
	m.prefcd_hq,
	NVL(ra.totalcandidatenum, 0) totalcandidatenum
FROM 
	(
		SELECT
			u.eventyear examyear,
			u.univcd univcd,
			u.uniname_abbr univname_abbr,
			u.unidiv univdivcd,
			u.prefcd_examho prefcd_hq,
			u.facultycd facultycd1,
			u.deptcd deptcd1,
			FIRST_VALUE (u.facultycd) OVER (PARTITION BY u.univcd) facultycd2,
			FIRST_VALUE (u.deptcd) OVER (PARTITION BY u.univcd) deptcd2
			/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
			 , u.univname_kana
			/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
		FROM
			univmaster_basic u
		WHERE
			u.eventyear = ? /* 1 */
		AND
			u.examdiv = ? /* 2 */
		# /* �i���ݏ��� */
	) m,
	ratingnumber_a ra
WHERE
	m.facultycd1 = m.facultycd2
AND
	m.deptcd1 = m.deptcd2
AND
	ra.univcd(+) = m.univcd
AND
	ra.examyear(+) = m.examyear
AND
	ra.examcd(+) = ?
AND
	ra.ratingdiv(+) = ?
AND
	ra.univcountingdiv(+) = '0'
AND
	ra.studentdiv(+) = '0'
AND
	ra.facultycd(+) = 'ZZ'
AND
	ra.deptcd(+) = 'ZZZZ'
AND
	ra.agendacd(+) = 'Z'

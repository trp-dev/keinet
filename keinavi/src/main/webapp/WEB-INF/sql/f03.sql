SELECT
	e.examyear,
	e.examcd,
	e.examname,
	TO_DATE(SUBSTR(e.out_dataopendate, 1, 8), 'YYYYMMDD'),
	e.tergetgrade,
	s.bundlecd,
	e.examtypecd
FROM
	examination e,
	school s
WHERE
	SUBSTR(e.out_dataopendate, 1, 8) <= TO_CHAR(SYSDATE, 'YYYYMMDD')
AND
	e.out_dataopendate >= '200411150000'
AND (		
		e.examtypecd = '32'
	OR
		e.examcd IN ('01', '02', '03', '04', '05', '06', '07', '09', '10', '28', '37', '38', '61', '62', '63', '65', '66', '67', '68', '69', '71', '72', '73', '74', '75', '76', '77', '78', '79')
	)
AND
	(s.bundlecd = ? OR s.parentschoolcd = ?)
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)
ORDER BY
	e.examyear DESC,
	e.dispsequence,
	s.bundlecd


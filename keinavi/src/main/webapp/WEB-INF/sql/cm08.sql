SELECT
	univcd,
	univname_abbr,
	univdivcd,
	prefcd_hq,
	totalcandidatenum
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
	 , univname_kana
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
FROM (
	SELECT /*+ ORDERED USE_NL(u rs) INDEX(u PK_UNIVMASTER_BASIC) */
		u.univcd univcd,
		u.uniname_abbr univname_abbr,
		u.unidiv univdivcd,
		u.prefcd_examho prefcd_hq,
		rs.totalcandidatenum totalcandidatenum,
		u.facultycd facultycd1,
		FIRST_VALUE (u.facultycd) OVER (PARTITION BY u.univcd) facultycd2,
		u.deptcd deptcd1,
		FIRST_VALUE (u.deptcd) OVER (PARTITION BY u.univcd) deptcd2,
		DECODE(u.unidiv, '01', '02', u.unidiv) sortkey1
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		ratingnumber_s rs
	INNER JOIN
		univmaster_basic u
	ON
		u.eventyear = rs.examyear
	AND
		u.examdiv = ? /* 1 */
	AND
		u.univcd = rs.univcd
	WHERE
		rs.examyear = ? /* 2 */
	AND
		rs.examcd = ? /* 3 */
	AND
		rs.bundlecd = ? /* 4 */
	AND
		rs.ratingdiv = ? /* 5 */
	AND
		rs.univcountingdiv = '0'
	AND
		rs.studentdiv = '0'
	AND
		rs.facultycd = 'ZZ'
	AND
		rs.deptcd = 'ZZZZ'
	AND
		rs.agendacd = 'Z'
	AND
		rs.totalcandidatenum is not null
)
WHERE
	facultycd1 = facultycd2
AND
	deptcd1 = deptcd2
ORDER BY
	sortkey1,
	totalcandidatenum DESC,
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD START */
	univname_kana ,
	/* 2016/01/18 QQ)Nishiyama ��K�͉��C ADD END */
	univcd

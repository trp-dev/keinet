SELECT
	c.year year,
	c.grade grade,
	c.class class,
	DECODE(c.grade, 1, 3, 3, 1, c.grade) grade_seq
FROM
	login_charge c
WHERE
	c.schoolcd = ?
AND
	c.loginid = ?
ORDER BY
	year DESC,
	grade_seq,
	class

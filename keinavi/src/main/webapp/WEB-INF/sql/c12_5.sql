SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
	NVL(ri.s_rank, -999) s_rank,
	h.grade grade,
	h.class class,
	h.class_no class_no,
	b.name_kana,
	b.sex,
	NVL(ri.score, -999),
	NVL(ri.s_deviation, -999),
	NVL(ri.a_deviation, -999),
	ri.academiclevel
FROM
	basicinfo b,
	historyinfo h,
	subrecord_i ri
WHERE
	b.schoolcd = ? /* 1 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 2 */
AND
	h.grade = ? /* 3 */
AND
	h.class = ? /* 4 */
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 5 */
AND
	ri.examcd = ? /* 6 */
AND
	ri.subcd = ? /* 7 */

UNION ALL

SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
	NVL(ri.s_rank, -999),
	h.grade,
	h.class,
	h.class_no,
	b.name_kana,
	b.sex,
	NVL(ri.score, -999),
	NVL(ri.s_deviation, -999),
	NVL(ri.a_deviation, -999),
	ri.academiclevel
FROM
	basicinfo b,
	historyinfo h,
	class_group cg,
	subrecord_i ri
WHERE
	b.schoolcd = ? /* 8 */
AND
	h.individualid = b.individualid
AND
	h.year = ? /* 9 */
AND
	cg.year = h.year
AND
	cg.bundlecd = b.schoolcd
AND
	cg.grade = h.grade
AND
	cg.class = h.class
AND
	cg.classgcd = ? /* 10 */
AND
	ri.individualid = h.individualid
AND
	ri.examyear = ? /* 11 */
AND
	ri.examcd = ? /* 12 */
AND
	ri.subcd = ? /* 13 */

ORDER BY
	s_rank,
	grade DESC,
	class,
	class_no

select count(*) AS cnt
 from engpt_cefracqstatus_s ecass
	where
		ecass.examyear = ? /* '1' */
	AND
		ecass.examcd IN (#) /*  */
	AND
		ecass.bundlecd = ? /* 2 */
	AND
		ecass.engptcd = ? /* 3 */
	AND
		ecass.engptlevelcd = ? /* 4 */

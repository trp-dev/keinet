SELECT
	et.examyear examyear,
	NVL(rc.totalcandidatenum, -999),
	NVL(rc.firstcandidatenum, -999),
	NVL(rc.ratingnum_a, -999),
	NVL(rc.ratingnum_b, -999),
	/* 2019/11/19 QQ)Ooseto 英語認定試験延期対応 UPD START*/
	/*NVL(rc.ratingnum_c, -999),*/
	NVL(rc.ratingnum_c, -999)
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD START */
	/*NVL(rc.eng_totalcandidatenum, -999),*/
	/*NVL(rc.eng_firstcandidatenum, -999),*/
	/*NVL(rc.eng_ratingnum_a, -999),*/
	/*NVL(rc.eng_ratingnum_b, -999),*/
	/*NVL(rc.eng_ratingnum_c, -999)*/
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/11/19 QQ)Ooseto 英語認定試験延期対応 UPD END*/
FROM
	examcdtrans et

LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd

LEFT OUTER JOIN
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD START */
	ratingnumber_c rc
	/*v_ratingnumber_c rc*/
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END */
ON
	rc.examyear = ee.examyear
AND
	rc.examcd = ee.examcd
AND
	rc.bundlecd = ? /* 1 */
AND
	rc.studentdiv = ? /* 2 */
AND
	rc.univcountingdiv = ? /* 3 */
AND
	rc.univcd = ? /* 4 */
AND
	rc.facultycd = ? /* 5 */
AND
	rc.deptcd = ? /* 6 */
AND
	rc.agendacd = ? /* 7 */
AND
	rc.ratingdiv = ? /* 8 */
AND
	rc.grade = ? /* 9 */
AND
	rc.class = ? /* 10 */
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 11 */

ORDER BY
	examyear DESC

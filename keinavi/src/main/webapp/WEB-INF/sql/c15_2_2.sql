SELECT
	univcd
FROM (
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd univcd,
		h.individualid individualid
	FROM
		basicinfo b,
		historyinfo h,
		candidaterating cr
	WHERE
		b.schoolcd = ? /* 1 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 2 */
	AND
		h.grade = ? /* 3 */
	AND
		h.class = ? /* 4 */
	AND
		cr.examyear = ? /* 5 */
	AND
		cr.examcd = ? /* 6 */
	AND
		cr.individualid = b.individualid
	
	UNION ALL
	
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd,
		h.individualid
	FROM
		basicinfo b,
		historyinfo h,
		class_group cg,
		candidaterating cr
	WHERE
		b.schoolcd = ? /* 8 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 9 */
	AND
		cg.year = cr.examyear
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = h.grade
	AND
		cg.class = h.class
	AND
		cg.classgcd = ? /* 10 */
	AND
		cr.individualid = b.individualid
	AND
		cr.examyear = ? /* 11 */
	AND
		cr.examcd = ? /* 12 */
)
GROUP BY
	univcd

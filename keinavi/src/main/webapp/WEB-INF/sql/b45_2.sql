SELECT
	TRIM('�i�S���j'),
	NVL(ra.numbers, -999),
	NVL(ra.avgpnt, -999),
	NVL(ra.avgdeviation, -999)
FROM
	examination e

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = e.examyear
AND
	st.examcd = e.examcd
AND
	st.cursubcd = ? /* 1 */

LEFT OUTER JOIN
	(
		SELECT
			ra.examyear examyear,
			ra.examcd examcd,
			ra.subcd subcd,
			MIN(studentdiv) studentdiv
		FROM
			subrecord_a ra
		GROUP BY
			ra.examyear,
			ra.examcd,
			ra.subcd
	) a
ON
	a.examyear = e.examyear
AND
	a.examcd = e.examcd
AND
	a.subcd = st.subcd

LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = a.examyear
AND
	ra.examcd = a.examcd
AND
	ra.subcd = a.subcd
AND
	ra.studentdiv = a.studentdiv

WHERE
	e.examyear = ? /* 2 */
AND
	e.examcd = ? /* 3 */

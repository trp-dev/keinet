SELECT
	TRIM('�i�S���j'),
	NVL(ra.numbers, -999),
	NVL(ra.avgpnt, -999),
	NVL(ra.avgdeviation, -999)
FROM
	subrecord_a ra,
	(
		SELECT
			MIN(a.studentdiv) studentdiv
		FROM
			subrecord_a a
		WHERE
			a.examyear = ? /* 1 */
		AND
			a.examcd = ? /* 2 */
	) a
WHERE
	ra.examyear(+) = ? /* 3 */
AND
	ra.examcd(+) = ? /* 4 */
AND
	ra.studentdiv(+) = a.studentdiv
AND
	ra.subcd(+) = ? /* 5 */

SELECT
	TRIM('�i�S���j'),
	et.examyear examyear,
	ra.subcd subcd,
	NVL(ra.numbers, -999) numbers,
	NVL(ra.avgpnt, -999) avgpnt,
	NVL(ra.avgdeviation, -999) avgdevitaion,
	et.examcd examcd
FROM
	examcdtrans et
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = et.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = et.examyear
AND
	ra.examcd = et.examcd
AND
	ra.studentdiv = ?
AND
	ra.subcd = st.subcd
WHERE
	et.examyear IN(#)
AND
	et.curexamcd = ?
ORDER BY
	examyear DESC

INSERT INTO
	examtakenum_c
SELECT
	examyear,
	examcd,
	bundlecd,
	grade,
	class,
	SUM(numbers)
FROM (
	SELECT
		ri.examyear examyear,
		ri.examcd examcd,
		ri.bundlecd bundlecd,
		re.grade grade,
		re.class class,
		COUNT(DISTINCT ri.individualid) numbers
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_i ri
	ON
		ri.examyear = re.examyear
	AND
		ri.examcd = re.examcd
	AND
		ri.bundlecd = re.bundlecd
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?
	AND
		EXISTS (
			SELECT
				1
			FROM
				historyinfo hi
			WHERE
				hi.individualid = ri.individualid
			AND
				hi.year = ri.examyear
			AND
				hi.grade = re.grade
			AND
				hi.class = re.class)
	GROUP BY
		ri.examyear,
		ri.examcd,
		ri.bundlecd,
		re.grade,
		re.class
	UNION ALL
	SELECT
		pp.examyear,
		pp.examcd,
		pp.schoolcd,
		pp.grade,
		pp.class,
		COUNT(DISTINCT pp.answersheet_no)
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_pp pp
	ON
		pp.examyear = re.examyear
	AND
		pp.examcd = re.examcd
	AND
		pp.schoolcd = re.bundlecd
	AND
		pp.grade = re.grade
	AND
		pp.class = re.class
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?
	GROUP BY
		pp.examyear,
		pp.examcd,
		pp.schoolcd,
		pp.grade,
		pp.class)
GROUP BY
	examyear,
	examcd,
	bundlecd,
	grade,
	class

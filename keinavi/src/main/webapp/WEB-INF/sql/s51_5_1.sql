SELECT
	TRIM('�i�S���j'),
	NVL(ra.numbers, -999),
	NVL(ra.avgpnt, -999),
	NVL(ra.avgdeviation, -999)
FROM
	subrecord_a ra,
	(
		SELECT
			a.examyear examyear,
			a.examcd examcd,
			a.subcd subcd,
			MIN(a.studentdiv) studentdiv
		FROM
			subrecord_a a,
			subcdtrans st
		WHERE
			a.examyear = ? /* 1 */
		AND
			a.examcd = ? /* 2 */
		AND
			a.subcd = st.subcd
		AND
			st.examyear = a.examyear
		AND
			st.examcd = a.examcd
		AND
			st.cursubcd = ? /* 3 */
		GROUP BY
			a.examyear,
			a.examcd,
			a.subcd
	) a
WHERE
	ra.examyear(+) = a.examyear
AND
	ra.examcd(+) = a.examcd
AND
	ra.studentdiv(+) = a.studentdiv
AND
	ra.subcd(+) = a.subcd

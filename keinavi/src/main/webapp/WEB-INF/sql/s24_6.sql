SELECT /*+ ORDERED */
	et.examyear examyear,
	NVL(ra.totalcandidatenum, -999),
	NVL(rs.totalcandidatenum, -999),
	NVL(rs.firstcandidatenum, -999),
	NVL(ra.avgscorerate, -999),
	NVL(rs.avgscorerate, -999),
	NVL(ra.avgdeviation, -999),
	NVL(rs.avgdeviation, -999),
	NVL(rs.ratingnum_a, -999),
	NVL(rs.ratingnum_b, -999),
	NVL(rs.ratingnum_c, -999),
	NVL(rs.ratingnum_d, -999),
	/* 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/*NVL(rs.ratingnum_e, -999),*/
	NVL(rs.ratingnum_e, -999)
	/* 2019/09/30 共通テスト対応 ADD START */
	/*NVL(rs.eng_ratingnum_a, -999),*/
	/*NVL(rs.eng_ratingnum_b, -999),*/
	/*NVL(rs.eng_ratingnum_c, -999),*/
	/*NVL(rs.eng_ratingnum_d, -999),*/
	/*NVL(rs.eng_ratingnum_e, -999)*/
	/* 2019/09/30 共通テスト対応 ADD END */
	/* 2019/11/18 QQ)Ooseto 英語認定試験延期対応 UPD END */
FROM
	examcdtrans et
LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd
LEFT OUTER JOIN
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/* 2019/09/30 共通テスト対応 UPD START */
	ratingnumber_a ra
	/*v_ratingnumber_a ra*/
	/* 2019/09/30 共通テスト対応 UPD END */
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END */
ON
	ra.examyear = ee.examyear
AND
	ra.examcd = ee.examcd
AND
	ra.studentdiv = ? /* 1 */
AND
	ra.univcountingdiv = ? /* 2 */
AND
	ra.univcd = ? /* 3 */
AND
	ra.facultycd = ? /* 4 */
AND
	ra.deptcd = ? /* 5 */
AND
	ra.agendacd = ? /* 6 */
AND
	ra.ratingdiv = ? /* 7 */
LEFT OUTER JOIN
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/* 2019/09/30 共通テスト対応 UPD START */
	ratingnumber_s rs
	/*v_ratingnumber_s rs*/
	/* 2019/09/30 共通テスト対応 UPD END */
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END */
ON
	rs.examyear = ee.examyear
AND
	rs.examcd = ee.examcd
AND
	rs.bundlecd = ? /* 8 */
AND
	rs.studentdiv = ? /* 9 */
AND
	rs.univcountingdiv = ? /* 10 */
AND
	rs.univcd = ? /* 11 */
AND
	rs.facultycd = ? /* 12 */
AND
	rs.deptcd = ? /* 13 */
AND
	rs.agendacd = ? /* 14 */
AND
	rs.ratingdiv = ? /* 15 */
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 16 */
ORDER BY
	examyear DESC

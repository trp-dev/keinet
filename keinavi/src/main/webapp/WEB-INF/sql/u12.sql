UPDATE
	loginid_manage m
SET
	m.loginid = NVL((
		SELECT
			?
		FROM
			admin_oldstyle ao
		WHERE 	
			m.schoolcd = ao.schoolcd
			),
		'admin'	
	)
WHERE				
	m.manage_flg = '1'
AND
	m.schoolcd = (
		SELECT
			p.schoolcd
		FROM
			pactschool p
		WHERE 	
			p.userid = ?
)

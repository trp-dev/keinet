SELECT /*+ USE_NL(rs rc) */
	0,
	rc.examyear,
	rc.examcd,
	e.examname,
	4,
	p.prefcd,
	p.prefname,
	s.bundlecd,
	s.bundlename,
	rc.grade GRADE,
	rc.class CLASS,
	rc.ratingdiv RATINGDIV,
	rc.univcountingdiv UNIVCOUNTINGDIV,
	rc.univcd UNIVCD,
	rc.facultycd FACULTYCD,
	rc.deptcd DEPTCD,
	u.choicecd5 CHOICECD5,
	DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uniname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
		 ),
		u.uniname_abbr
	) univname_abbr,
	DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyname_abbr)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
			AND
				u.facultycd = rc.facultycd),
		u.facultyname_abbr
	),
	u.deptname_abbr,
	rc.agendacd AGENDACD,
	rc.studentdiv STUDENTDIV,
	rc.totalcandidatenum,
	rc.firstcandidatenum,
	rc.ratingnum_a,
	rc.ratingnum_b,
	rc.ratingnum_c,
	rc.ratingnum_d,
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	rc.ratingnum_e, */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD START */
/*	rc.eng_ratingnum_a, */
/*	rc.eng_ratingnum_b, */
/*	rc.eng_ratingnum_c, */
/*	rc.eng_ratingnum_d, */
/*	rc.eng_ratingnum_e */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� ADD END */
	rc.ratingnum_e
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD START */
	/* ����w�P�� */
	/* ��w�敪 */
	, DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.unidiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
		 ),
		u.unidiv
	) unidiv_sort,
	/* ��w���J�i */
	DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.univname_kana)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
		 ),
		u.univname_kana
	) univname_kana_sort,
	/* ���w���P�� */
	/* ��w��ԋ敪 */
	DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.uninightdiv)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
			AND
				u.facultycd = rc.facultycd
		 ),
		u.uninightdiv
	) uninightdiv_sort,
	/* �w�����e�R�[�h */
	DECODE(
		rc.deptcd,
		'ZZZZ',
		(
			SELECT /*+ INDEX(h PK_UNIVMASTER_BASIC) */
				MAX(u.facultyconcd)
			FROM
				univmaster_basic u
			WHERE
				u.eventyear = rc.examyear
			AND
				u.examdiv = e.examdiv
			AND
				u.univcd = rc.univcd
			AND
				u.facultycd = rc.facultycd
		 ),
		u.facultyconcd
	) facultyconcd_sort
	/* 2016/03/02 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_s rs */
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	v_ratingnumber_s rs */
	ratingnumber_s rs
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
INNER JOIN
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD START */
	/* ratingnumber_c rc */
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD START */
/*	v_ratingnumber_c rc */
	ratingnumber_c rc
/* 2019/11/20 QQ)Tanioka �p��F�莎�������Ή� MOD END */
/* 2019/08/26 QQ)nagai ���ʃe�X�g�Ή� UPD END */
ON
	rc.examyear = rs.examyear
AND
	rc.examcd = rs.examcd
AND
	rc.ratingdiv = rs.ratingdiv
AND
	rc.univcountingdiv = rs.univcountingdiv
AND
	rc.univcd = rs.univcd
AND
	rc.facultycd = rs.facultycd
AND
	rc.deptcd = rs.deptcd
AND
	rc.agendacd = rs.agendacd
AND
	rc.studentdiv = rs.studentdiv
AND
	rc.bundlecd = rs.bundlecd
INNER JOIN
	school s
ON
	s.bundlecd = rc.bundlecd
INNER JOIN
	prefecture p
ON
	p.prefcd = s.facultycd
INNER JOIN
	examination e
ON
	e.examyear = rc.examyear
AND
	e.examcd = rc.examcd
LEFT OUTER JOIN
	univmaster_basic u
ON
	u.eventyear = rc.examyear
AND
	u.examdiv = e.examdiv
AND
	u.univcd = rc.univcd
AND
	u.facultycd = rc.facultycd
AND
	u.deptcd = rc.deptcd
WHERE
	rs.examyear = ?
AND
	rs.examcd = ?
AND
	rs.bundlecd = ?
ORDER BY
	GRADE,
	CLASS,
	RATINGDIV,
	UNIVCOUNTINGDIV,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	unidiv_sort,
	univname_kana_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	UNIVCD,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* FACULTYCD, */
	uninightdiv_sort,
	facultyconcd_sort,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	NVL(u.UNIGDIV, ''),
	NVL(u.DEPTSERIAL_NO, ''),
	NVL(u.SCHEDULESYS, ''),
	NVL(u.SCHEDULESYSBRANCHCD, ''),
	NVL(u.DEPTNAME_KANA, ''),
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	DEPTCD,
	AGENDACD,
	STUDENTDIV

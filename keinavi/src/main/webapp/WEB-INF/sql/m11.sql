SELECT /*+ ORDERED USE_NL(bi h1) USE_NL(bi h2) USE_NL(bi h3) */
	bi.individualid,
	bi.sex key_sex,
	TRIM(bi.name_kana) key_name_kana,
	TRIM(bi.name_kanji),
	CONVERT_BIRTHDAY(bi.birthday),
	TRIM(bi.tel_no),
	NVL(h1.grade, -1),
	h1.class key_class1,
	h1.class_no key_class_no1,
	NVL(h2.grade, -1),
	h2.class key_class2,
	h2.class_no key_class_no2,
	NVL(h3.grade, -1),
	h3.class key_class3,
	h3.class_no key_class_no3,
	DECODE(h1.grade, 3, 1, 1, 3, h1.grade) key_grade1,
	DECODE(h2.grade, 3, 1, 1, 3, h2.grade) key_grade2,
	DECODE(h3.grade, 3, 1, 1, 3, h3.grade) key_grade3
FROM
	basicinfo bi
LEFT OUTER JOIN
	historyinfo h1
ON
	h1.individualid = bi.individualid
AND
	h1.year = ?
LEFT OUTER JOIN
	historyinfo h2
ON
	h2.individualid = bi.individualid
AND
	h2.year = ?
LEFT OUTER JOIN
	historyinfo h3
ON
	h3.individualid = bi.individualid
AND
	h3.year = ?
WHERE
	bi.schoolcd = ?

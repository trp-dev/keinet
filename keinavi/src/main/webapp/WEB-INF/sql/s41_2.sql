SELECT
	'�i' || p.prefname ||'�j',
	NVL(rp.numbers, -999),
	NVL(rp.avgpnt, -999),
	NVL(rp.avgdeviation, -999)
FROM
	subrecord_p rp,
	prefecture p
WHERE
	rp.examyear(+) = ? /* 1 */
AND
	rp.examcd(+) = ? /* 2 */
AND
	rp.prefcd(+) = p.prefcd
AND
	rp.subcd(+) = ? /* 3 */
AND
	p.prefcd = ? /* 4 */

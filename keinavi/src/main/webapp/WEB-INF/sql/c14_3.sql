SELECT
	qa.subcd, 
	NVL(rc.numbers, - 999), 
	NVL(qa.avgpnt, - 999), 
	NVL(qp.avgpnt, - 999), 
	NVL(qs.avgpnt, - 999), 
	NVL(qc.avgpnt, - 999) 
FROM 
(
	SELECT
		x.examyear,
		x.examcd,
		x.subcd,
		x.studentdiv,
		x.avgpnt,
		CASE
			WHEN e.examtypecd IN ('02','91') THEN NVL(t2.combisubcd, x.subcd)
			ELSE x.subcd
		END rplsubcd 
	FROM
		subrecord_a x
	INNER JOIN
		examination e
	ON
		x.examyear = e.examyear
	AND
		x.examcd = e.examcd
	LEFT JOIN
	    	subjectcombi t1
	ON
	    	t1.subcd = x.subcd
	LEFT JOIN
		subjectcombi t2
	ON
		t2.combisubcd = t1.combisubcd	
) qa 
LEFT OUTER JOIN subrecord_p qp 
	ON qp.examyear = qa.examyear
AND
	qp.examcd = qa.examcd
AND
	qp.subcd = qa.subcd
AND
	qp.prefcd = ? /* 1 */
LEFT OUTER JOIN subrecord_s qs 
	ON qs.examyear = qa.examyear
AND
	qs.examcd = qa.examcd
AND
	qs.subcd = qa.rplsubcd 
AND
	qs.bundlecd = ? /* 2 */
LEFT OUTER JOIN 
( 
	SELECT
		rc.subcd subcd, 
		rc.numbers numbers 
	FROM
		subrecord_c rc 
	WHERE
		rc.examyear = ? /* 3 */
	AND
		rc.examcd = ? /* 4 */
	AND
		rc.bundlecd = ? /* 5 */
	AND
		rc.grade = ? /* 6 */
	AND
		rc.CLASS = ? /* 7 */
	UNION ALL 
	SELECT
		subcd, 
		numbers 
	FROM
	( 
		SELECT
			rc.subcd subcd, 
			SUM(rc.numbers) OVER(PARTITION BY rc.subcd) numbers, 
			rc.grade grade1, 
			FIRST_VALUE(rc.grade) OVER(PARTITION BY rc.subcd) grade2, 
			rc.CLASS class1, 
			FIRST_VALUE(rc.CLASS) OVER(PARTITION BY rc.subcd) class2 
		FROM
			subrecord_c rc, 
			class_group cg 
		WHERE
			rc.examyear = ? /* 8 */
		AND
			rc.examcd = ? /* 9 */
		AND
			rc.bundlecd = ? /* 10 */
		AND
			rc.grade = cg.grade 
		AND
			rc.CLASS = cg.CLASS 
		AND
			cg.classgcd = ? /* 11 */
	) 
	WHERE
		grade1 = grade2 
	AND
		class1 = class2
) rc 
ON
	rc.subcd = qa.rplsubcd
LEFT OUTER JOIN ( 
	SELECT
		qc.subcd subcd,
		qc.avgpnt avgpnt 
	FROM
		subrecord_c qc 
	WHERE
		qc.examyear = ? /* 12 */
	AND
		qc.examcd = ? /* 13 */
	AND
		qc.bundlecd = ? /* 14 */
	AND
		qc.grade = ? /* 15 */
	AND
		qc.CLASS = ? /* 16 */
	UNION ALL 
	SELECT
		subcd,
		avgpnt 
	FROM
	( 
		SELECT
			qc.subcd subcd,
			ROUND( 
				SUM(qc.numbers * qc.avgpnt) OVER(PARTITION BY qc.subcd) / SUM(qc.numbers) OVER(PARTITION BY qc.subcd), 
				2
			) avgpnt, 
			qc.grade grade1, 
			FIRST_VALUE(qc.grade) OVER(PARTITION BY qc.subcd) grade2, 
			qc.CLASS class1, 
			FIRST_VALUE(qc.CLASS) OVER(PARTITION BY qc.subcd) class2 
		FROM
			subrecord_c qc, 
			class_group cg 
		WHERE
			qc.examyear = ? /* 17 */
		AND
			qc.examcd = ? /* 18 */
		AND
			qc.bundlecd = ? /* 19 */
		AND
			qc.grade = cg.grade 
		AND
			qc.CLASS = cg.CLASS 
		AND
			cg.classgcd = ? /* 20 */
	) 
	WHERE
		grade1 = grade2 
	AND
		class1 = class2
) qc 
	ON qc.subcd = qa.rplsubcd
WHERE
	qa.examyear = ? /* 21 */
AND
	qa.examcd = ? /* 22 */
AND
	qa.subcd = ? /* 23 */
AND
	qa.studentdiv = 
	(
		SELECT
			MIN(STUDENTDIV)
		FROM
			SUBRECORD_A
		WHERE
			EXAMYEAR = ? /* 24 */
		AND
			EXAMCD = ? /* 25 */
	)
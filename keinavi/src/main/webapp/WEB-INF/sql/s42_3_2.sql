SELECT
	'99999' bundlecd,
	TRIM('�i�S���j') bundlename,
	st.examyear examyear,
	ra.subcd subcd,
	NVL(ra.avgpnt, -999) avgpnt,
	NVL(ra.numbers, -999) numbers
FROM
	subcdtrans st
LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = st.examyear
AND
	ra.examcd = st.examcd
AND
	ra.subcd = st.subcd
AND
	ra.studentdiv = ?
WHERE
	st.examyear IN (#)
AND
	st.examcd = ?
AND
	st.cursubcd = ?
ORDER BY
	examyear DESC

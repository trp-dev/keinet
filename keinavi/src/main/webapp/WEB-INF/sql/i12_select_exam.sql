SELECT DISTINCT /*+ INDEX(SUBRECORD_I PK_SUBRECORD_I) */
	si.examyear examyear
	,si.examcd examcd
	,ex.examname_abbr examname_abbr
	,ex.inpledate inpledate
	,ex.in_dataopendate in_dataopendate
	,ex.out_dataopendate out_dataopendate
FROM 
	EXAMINATION ex
INNER JOIN 
	SUBRECORD_I si
ON 
	ex.EXAMYEAR = si.EXAMYEAR
AND 
	ex.EXAMCD = si.EXAMCD
AND 
	si.INDIVIDUALID = ?
WHERE 
	ex.examyear = ? 
AND
	ex.examcd IN (#)
ORDER BY
	ex.out_dataopendate DESC, si.examcd DESC
	
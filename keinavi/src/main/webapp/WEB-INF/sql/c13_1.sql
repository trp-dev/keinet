SELECT /*+ ORDERED */
	a.individualid,
	a.grade grade,
	a.class class,
	a.class_no class_no,
	a.name_kana,
	a.sex,
	a.a_deviation deviation,
	NVL(a.a_deviation - b.a_deviation, 0) diff,
	a.score score
FROM (
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
		b.individualid individualid,
		h.grade grade,
		h.class class,
		h.class_no class_no,
		b.name_kana name_kana,
		b.sex sex,
		ri.a_deviation a_deviation,
		ri.subcd subcd1,
		FIRST_VALUE(es.subcd) OVER (PARTITION BY b.individualid ORDER BY es.dispsequence) subcd2,
		ri.score score
	FROM
		basicinfo b,
		historyinfo h,
		subrecord_i ri,
		v_i_cmexamsubject es
	WHERE
		ri.individualid = h.individualid
	AND
		ri.examyear = ? /* 1 */
	AND
		ri.examcd = ? /* 2 */
	AND
		b.schoolcd = ? /* 3 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 4 */
	AND
		h.grade = ? /* 5 */
	AND
		h.class = ? /* 6 */
	AND
		es.examyear = ri.examyear
	AND
		es.examcd = ri.examcd
	AND
		es.subcd = ri.subcd

	UNION ALL

	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
		b.individualid individualid,
		h.grade grade,
		h.class class,
		h.class_no class_no,
		b.name_kana name_kana,
		b.sex sex,
		ri.a_deviation,
		ri.subcd subcd1,
		FIRST_VALUE(es.subcd) OVER (PARTITION BY b.individualid ORDER BY es.dispsequence) subcd2,
		ri.score
	FROM
		basicinfo b,
		historyinfo h,
		class_group cg,
		subrecord_i ri,
		v_i_cmexamsubject es
	WHERE
		ri.individualid = h.individualid
	AND
		ri.examyear = ? /* 7 */
	AND
		ri.examcd = ? /* 8 */
	AND
		b.schoolcd = ? /* 9 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 10 */
	AND
		h.grade = cg.grade
	AND
		h.class = cg.class
	AND
		cg.classgcd = ? /* 11 */
	AND
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		es.examyear = ri.examyear
	AND
		es.examcd = ri.examcd
	AND
		es.subcd = ri.subcd
) a,
(
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
		ri.individualid,
		ri.a_deviation,
		ri.subcd subcd
	FROM
		basicinfo b,
		historyinfo h,
		subrecord_i ri,
		v_i_cmexamsubject es,
		existsexam ee
	WHERE
		ri.individualid = h.individualid
	AND
		ri.examyear = ? /* 12 */
	AND
		ri.examcd = ? /* 13 */
	AND
		b.schoolcd = ? /* 14 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 15 */
	AND
		h.grade = ? /* 16 */
	AND
		h.class = ? /* 17 */
	AND
		es.examyear = ri.examyear
	AND
		es.examcd = ri.examcd
	AND
		es.subcd = ri.subcd
	AND
		ee.examyear = es.examyear
	AND
		ee.examcd = es.examcd
	
	UNION ALL

	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(ri PK_SUBRECORD_I) */
		ri.individualid,
		ri.a_deviation,
		ri.subcd subcd
	FROM
		basicinfo b,
		historyinfo h,
		class_group cg,
		subrecord_i ri,
		v_i_cmexamsubject es,
		existsexam ee
	WHERE
		ri.individualid = h.individualid
	AND
		ri.examyear = ? /* 18 */
	AND
		ri.examcd = ? /* 19 */
	AND
		b.schoolcd = ? /* 20 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 21 */
	AND
		h.grade = cg.grade
	AND
		h.class = cg.class
	AND
		cg.classgcd = ? /* 22 */
	AND
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		es.examyear = ri.examyear
	AND
		es.examcd = ri.examcd
	AND
		es.subcd = ri.subcd
	AND
		ee.examyear = es.examyear
	AND
		ee.examcd = es.examcd
	) b
WHERE
	a.subcd2 = a.subcd1
AND
	b.individualid(+) = a.individualid
AND
	b.subcd(+) = a.subcd2

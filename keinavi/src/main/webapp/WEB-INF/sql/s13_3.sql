SELECT
	es.subcd, 
	es.subname, 
	es.suballotpnt, 
	NVL(rs.numbers, - 999), 
	NVL(rs.avgscorerate, - 999), 
	NVL(rp.numbers, - 999), 
	NVL(rp.avgscorerate, - 999), 
	NVL(ra.numbers, - 999), 
	NVL(ra.avgscorerate, - 999), 
	NVL(zs.numbers, - 999), 
	NVL(zs.avgscorerate, - 999), 
	NVL(za.numbers, - 999), 
	NVL(za.avgscorerate, - 999), 
	NVL(zb.numbers, - 999), 
	NVL(zb.avgscorerate, - 999), 
	NVL(zc.numbers, - 999), 
	NVL(zc.avgscorerate, - 999), 
	NVL(zd.numbers, - 999), 
	NVL(zd.avgscorerate, - 999), 
	NVL(ze.numbers, - 999), 
	NVL(ze.avgscorerate, - 999), 
	NVL(zf.numbers, - 999), 
	NVL(zf.avgscorerate, - 999), 
	NVL(ys.numbers, - 999), 
	NVL(ys.avgscorerate, - 999), 
	NVL(ya.numbers, - 999), 
	NVL(ya.avgscorerate, - 999), 
	NVL(yb.numbers, - 999), 
	NVL(yb.avgscorerate, - 999), 
	NVL(yc.numbers, - 999), 
	NVL(yc.avgscorerate, - 999), 
	NVL(yd.numbers, - 999), 
	NVL(yd.avgscorerate, - 999), 
	NVL(ye.numbers, - 999), 
	NVL(ye.avgscorerate, - 999), 
	NVL(yf.numbers, - 999), 
	NVL(yf.avgscorerate, - 999) 
FROM
	v_sm_qexamsubject es 
	LEFT OUTER JOIN (
		SELECT
			examyear,
			examcd,
			subcd,
			numbers,
			avgscorerate
		FROM
			subrecord_a
		WHERE
			STUDENTDIV = (
			SELECT 
				MIN(STUDENTDIV)
			FROM
				SUBRECORD_A
			WHERE
				EXAMYEAR = ? AND /* 1 */
				EXAMCD = ? /* 2 */
		)
	) ra
		ON ra.examyear = es.examyear AND
		ra.examcd = es.examcd AND
		ra.subcd = es.subcd
	LEFT OUTER JOIN subrecord_p rp 
		ON rp.examyear = es.examyear AND
		rp.examcd = es.examcd AND
		rp.subcd = es.subcd AND
		rp.prefcd = ? /* 3 */
	LEFT OUTER JOIN subrecord_s rs 
		ON rs.examyear = es.examyear AND
		rs.examcd = es.examcd AND
		rs.subcd = es.subcd AND
		rs.bundlecd = ? /* 4 */
	LEFT OUTER JOIN SUBDEVZONERECORD_S zs 
		ON zs.examyear = rs.examyear AND
		zs.examcd = rs.examcd AND
		zs.subcd = rs.subcd AND
		zs.bundlecd = rs.bundlecd AND
		zs.devzonecd = 'S' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S za 
		ON za.examyear = rs.examyear AND
		za.examcd = rs.examcd AND
		za.subcd = rs.subcd AND
		za.bundlecd = rs.bundlecd AND
		za.devzonecd = 'A' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S zb 
		ON zb.examyear = rs.examyear AND
		zb.examcd = rs.examcd AND
		zb.subcd = rs.subcd AND
		zb.bundlecd = rs.bundlecd AND
		zb.devzonecd = 'B' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S zc 
		ON zc.examyear = rs.examyear AND
		zc.examcd = rs.examcd AND
		zc.subcd = rs.subcd AND
		zc.bundlecd = rs.bundlecd AND
		zc.devzonecd = 'C' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S zd 
		ON zd.examyear = rs.examyear AND
		zd.examcd = rs.examcd AND
		zd.subcd = rs.subcd AND
		zd.bundlecd = rs.bundlecd AND
		zd.devzonecd = 'D' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S ze 
		ON ze.examyear = rs.examyear AND
		ze.examcd = rs.examcd AND
		ze.subcd = rs.subcd AND
		ze.bundlecd = rs.bundlecd AND
		ze.devzonecd = 'E' 
	LEFT OUTER JOIN SUBDEVZONERECORD_S zf 
		ON zf.examyear = rs.examyear AND
		zf.examcd = rs.examcd AND
		zf.subcd = rs.subcd AND
		zf.bundlecd = rs.bundlecd AND
		zf.devzonecd = 'F' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A ys 
		ON ys.examyear = es.examyear AND
		ys.examcd = es.examcd AND
		ys.subcd = es.subcd AND
		ys.devzonecd = 'S' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A ya 
		ON ya.examyear = es.examyear AND
		ya.examcd = es.examcd AND
		ya.subcd = es.subcd AND
		ya.devzonecd = 'A' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A yb 
		ON yb.examyear = es.examyear AND
		yb.examcd = es.examcd AND
		yb.subcd = es.subcd AND
		yb.devzonecd = 'B' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A yc 
		ON yc.examyear = es.examyear AND
		yc.examcd = es.examcd AND
		yc.subcd = es.subcd AND
		yc.devzonecd = 'C' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A yd 
		ON yd.examyear = es.examyear AND
		yd.examcd = es.examcd AND
		yd.subcd = es.subcd AND
		yd.devzonecd = 'D' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A ye 
		ON ye.examyear = es.examyear AND
		ye.examcd = es.examcd AND
		ye.subcd = es.subcd AND
		ye.devzonecd = 'E' 
	LEFT OUTER JOIN SUBDEVZONERECORD_A yf 
		ON yf.examyear = es.examyear AND
		yf.examcd = es.examcd AND
		yf.subcd = es.subcd AND
		yf.devzonecd = 'F' 
WHERE
	es.examyear = ? AND /* 5 */
	es.examcd = ? AND /* 6 */
	es.subcd = ? /* 7 */

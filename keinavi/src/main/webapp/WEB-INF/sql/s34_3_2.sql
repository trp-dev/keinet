SELECT
	et.examyear examyear,
	NVL(SUM(rc.totalcandidatenum), -999),
	NVL(SUM(rc.firstcandidatenum), -999),
	NVL(SUM(rc.ratingnum_a), -999),
	NVL(SUM(rc.ratingnum_b), -999),
	/* 2019/11/19 QQ)Ooseto 英語認定試験延期対応 UPD START*/
	/*NVL(SUM(rc.ratingnum_c), -999),*/
	NVL(SUM(rc.ratingnum_c), -999)
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD START */
	/*NVL(SUM(rc.eng_totalcandidatenum), -999),*/
	/*NVL(SUM(rc.eng_firstcandidatenum), -999),*/
	/*NVL(SUM(rc.eng_ratingnum_a), -999),*/
	/*NVL(SUM(rc.eng_ratingnum_b), -999),*/
	/*NVL(SUM(rc.eng_ratingnum_c), -999)*/
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/11/19 QQ)Ooseto 英語認定試験延期対応 UPD END*/
FROM
	examcdtrans et

INNER JOIN
	class_group cg
ON
	cg.classgcd = ? /* 1 */

LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd

LEFT OUTER JOIN
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD START */
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD START */
	ratingnumber_c rc
	/*v_ratingnumber_c rc*/
	/* 2019/09/02 QQ)Tanouchi 共通テスト対応 ADD END */
	/* 2019/11/25 QQ)Ooseto 英語認定試験延期対応 UPD END */
ON
	rc.examyear = ee.examyear
AND
	rc.examcd = ee.examcd
AND
	rc.bundlecd = ? /* 2 */
AND
	rc.studentdiv = ? /* 3 */
AND
	rc.univcountingdiv = ? /* 4 */
AND
	rc.univcd = ? /* 5 */
AND
	rc.facultycd = ? /* 6 */
AND
	rc.deptcd = ? /* 7 */
AND
	rc.agendacd = ? /* 8 */
AND
	rc.ratingdiv = ? /* 9 */
AND
	rc.grade = cg.grade
AND
	rc.class = cg.class

WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ? /* 10 */
GROUP BY
	et.examyear

ORDER BY
	examyear DESC

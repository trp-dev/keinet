UPDATE						
	loginid_manage m					
SET						
	m.loginpwd = m.defaultpwd					
WHERE						
	m.manage_flg = '1'					
AND						
	m.schoolcd = (					
		SELECT 				
			p.schoolcd 			
		FROM				
			pactschool p	
		WHERE 								
			p.userid = ?			
)						

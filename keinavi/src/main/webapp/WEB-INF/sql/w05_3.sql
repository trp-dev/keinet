SELECT
	NVL(et.oldexamcd, e.examcd)
FROM
	examination e
LEFT OUTER JOIN
	examcodetrans et
ON
	et.examyear = e.examyear
AND
	et.newexamcd = e.examcd
WHERE
	e.examyear = ? /* 1 */
AND
	SUBSTR(e.out_dataopendate, 1, 8) <= ? /* 2 */
ORDER BY
	e.dispsequence

SELECT
	cc.grade,
	SUBSTR(cc.class, 1, 2),
	cc.dispgraph,
	NVL(rc.numbers, -999),
	NVL(rc.avgscorerate, -999),
	cc.dispsequence dispsequence
FROM
	qrecord_c rc,
	countcompclass cc
WHERE
	rc.examyear(+) = ? /* 1 */
AND
	rc.examcd(+) = ? /* 2 */
AND
	rc.bundlecd(+) = ? /* 3 */
AND
	rc.subcd(+) = ? /* 4 */
AND
	rc.question_no(+) = ? /* 5 */
AND
	rc.grade(+) = cc.grade
AND
	rc.class(+) = cc.class
AND
	LENGTH(NVL(TRIM(cc.class), '  ')) <= 2

UNION ALL 

SELECT
	cc.grade,
	cn.classgname,
	cc.dispgraph,
	NVL(SUM(rc.numbers), -999) numbers,
	NVL(ROUND(ROUND(SUM(rc.avgpnt * rc.numbers) / SUM(rc.numbers), 1) / eq.qallotpnt * 100, 1), -999) avgscorerate,
	cc.dispsequence
FROM
	qrecord_c rc,
	examination e,
	class_group cg,
	classgroup cn,
	examquestion eq,
	countcompclass cc
WHERE
	rc.examyear(+) = ? /* 6 */
AND
	rc.examcd(+) = ? /* 7 */
AND
	rc.bundlecd(+) = ? /* 8 */
AND
	rc.subcd(+) = ? /* 9 */
AND
	rc.question_no(+) = ? /* 10 */
AND
	rc.grade(+) = cg.grade
AND
	rc.class(+) = cg.class
AND
	cg.classgcd = cc.class
AND
	cn.classgcd = cg.classgcd
AND
	e.examyear = ? /* 11 */
AND
	e.examcd = ? /* 12 */
AND
	eq.examyear(+) = e.examyear
AND
	eq.examcd(+) = e.examcd
AND
	eq.subcd(+) = ? /* 13 */
AND
	eq.question_no(+) = ? /* 14 */
GROUP BY
	cc.grade,
	cn.classgname,
	cc.dispgraph,
	eq.qallotpnt,
	cc.dispsequence

ORDER BY
	dispsequence

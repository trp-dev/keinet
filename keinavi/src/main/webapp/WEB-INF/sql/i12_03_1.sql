SELECT 
	/*+ INDEX(QI PK_QRECORD_I) */
	ES.SUBCD,
	ES.SUBNAME,
	EQ.DISPLAY_NO,
	EQ.QUESTIONNAME1,
	EQ.QUESTION_NO,
	EQ.QALLOTPNT,
	NVL(QI.QSCORE, -999) QSCORE,
	NVL(QI.SCORERATE, -999.0) SCORERATE,
	NVL(QS.AVGPNT, -999.0) AVGPNT_S,
	NVL(QS.AVGSCORERATE, -999.0) AVGSCORERATE_S,
	NVL(QA.AVGPNT, -999.0) AVGPNT_A,
	NVL(QA.AVGSCORERATE, -999.0) AVGSCORERATE_A
FROM 
	V_I_QEXAMSUBJECT ES
	
	INNER JOIN
	EXAMQUESTION EQ
	ON EQ.EXAMYEAR = ES.EXAMYEAR
	AND EQ.EXAMCD = ES.EXAMCD
	AND EQ.SUBCD = ES.SUBCD
	AND EQ.SUBCD in (
		SELECT /*+ INDEX(QRECORD_I PK_QRECORD_I) */
		SUBCD 
		FROM QRECORD_I
		WHERE INDIVIDUALID = ? /* ΒlID */
			AND EXAMYEAR = ? /* ΞΫΝNx */
			AND EXAMCD = ? /* ΞΫΝR[h */
			AND SUBCD < '7000'
		GROUP BY SUBCD
	)
	AND EQ.EXAMYEAR = ? /* ΝNx */
	AND EQ.EXAMCD = ? /* ΝR[h */
	
	LEFT JOIN
	QRECORD_I QI
	ON QI.EXAMYEAR = EQ.EXAMYEAR
	AND QI.EXAMCD = EQ.EXAMCD
	AND QI.SUBCD = EQ.SUBCD
	AND QI.QUESTION_NO = EQ.QUESTION_NO
	AND QI.INDIVIDUALID = ? /* ΒlID */

	LEFT JOIN QRECORD_S QS
	ON QS.EXAMYEAR = EQ.EXAMYEAR
	AND QS.EXAMCD = EQ.EXAMCD
	AND QS.SUBCD = EQ.SUBCD
	AND QS.QUESTION_NO = EQ.QUESTION_NO
	AND QS.BUNDLECD = ? /* κR[h */
	
	LEFT JOIN QRECORD_A QA
	ON QA.EXAMYEAR = EQ.EXAMYEAR
	AND QA.EXAMCD = EQ.EXAMCD
	AND QA.SUBCD = EQ.SUBCD
	AND QA.QUESTION_NO = EQ.QUESTION_NO
ORDER BY
	SUBSTR(ES.SUBCD, 1, 1), 
	ES.DISPSEQUENCE,
	DISPLAY_NO,
	QUESTION_NO

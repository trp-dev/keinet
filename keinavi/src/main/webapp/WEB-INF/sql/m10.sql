SELECT /*+ ORDERED USE_NL(bi hi) INDEX(bi BASICINFO_IDX) INDEX(hi PK_HISTORYINFO) */ 
	hi.year year,
	hi.grade grade,
	UPPER(hi.class) class,
	DECODE(hi.grade, 3, 1, 1, 3, hi.grade) grade_seq
FROM
	basicinfo bi
INNER JOIN
	historyinfo hi
ON
	hi.individualid = bi.individualid
WHERE
	bi.schoolcd = ?

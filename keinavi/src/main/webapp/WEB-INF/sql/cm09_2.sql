SELECT
	es.subcd,
	es.subname,
	NVL(rs.numbers, 0)
FROM
	v_sm_cmexamsubject es,
	subrecord_s rs
WHERE
	es.examyear = ?
AND
	es.examcd = ?
AND
	rs.examyear(+) = es.examyear
AND
	rs.examcd(+) = es.examcd
AND
	rs.subcd(+) = es.subcd
AND
	rs.bundlecd(+) = ?

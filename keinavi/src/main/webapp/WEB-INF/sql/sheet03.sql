SELECT
	e.examyear,
	e.examcd,
	e.examname_abbr,
	e.tergetgrade,
	e.inpledate,
	e.dispsequence,
	e.out_dataopendate
FROM
	examination e,
	existsexam ee
WHERE
	e.examtypecd = ?
AND
	(
			(e.examyear = ? AND e.tergetgrade = ?)
		OR
			(e.examyear = ? AND e.tergetgrade = ?)
		OR
			(e.examyear = ? AND e.tergetgrade = ?)
	)
AND
	e.out_dataopendate <= ?
AND
	ee.examyear = e.examyear
AND
	ee.examcd = e.examcd
ORDER BY
	e.examyear DESC,
	e.out_dataopendate DESC

SELECT
	iv.individualid,
	iv.ko_individualid,
	iv.firstregistrydt,
	iv.createdate,
	iv.renewaldate,
	iv.title,
	iv.text
FROM
	interview iv
WHERE
	iv.individualid = ?
OR
	iv.ko_individualid IN (
		SELECT /*+ INDEX(bi BASICINFO_IDX) */
			bi.individualid
		FROM
			ko_basicinfo bi
		WHERE
			bi.schoolcd = ?
		AND
			bi.navi_individualid = ?)
ORDER BY
	createdate asc,
	renewaldate asc,
	firstregistrydt asc

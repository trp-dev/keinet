SELECT
	/* CEFRîñ.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRîñ.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRîñ.ÀÑ */
	ci.sort AS ci_sort,
	/* §}X^.§¼ */
	'i' || pre.prefname || 'j' AS bundlename,
	/* pêFè±ÊEvCEFRæ¾óµi§j.l */
	ecasp.numbers AS numbers,
	/* pêFè±ÊEvCEFRæ¾óµi§j.\¬ä */
	ecasp.compratio AS compratio
FROM
	/* Í}X^TBL */
	examination en
	/* §}X^ */
	INNER JOIN prefecture pre
	ON
		pre.prefcd = ? /* 1 */
	/* CEFRîñTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'v' AS cerflevelname_abbr,
			-1 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		ci.eventyear = en.EXAMYEAR
	AND
		ci.examdiv = en.examdiv
	/* pêFè±ÊEvCEFRæ¾óµi§jTBL */
	LEFT JOIN engpt_cefracqstatus_p ecasp
	ON
		ecasp.examyear = en.EXAMYEAR
	AND
		ecasp.examcd = en.examcd
	AND
		ecasp.engptcd = ? /* 2 */
	AND
		ecasp.engptlevelcd = ? /* 3 */
	AND
		ecasp.cefrlevelcd = ci.cefrlevelcd
	AND
		ecasp.prefcd = pre.prefcd
WHERE
		en.EXAMYEAR = ? /* '4' */
	AND
		en.EXAMCD = ? /* '5' */
ORDER BY
	ci_sort

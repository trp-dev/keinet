SELECT
	e.examyear examyear,
	e.examcd examcd,
	e.examname examname,
	e.inpledate inpledate,
	e.dispsequence dispsequence
FROM
	examination e
WHERE
	e.examyear >= TO_CHAR(ADD_MONTHS(SYSDATE, -27), 'YYYY')
AND
	e.out_dataopendate <= TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000'
AND
	EXISTS (
		SELECT /*+ ORDERED */
			1
		FROM
			basicinfo b
		INNER JOIN
			individualrecord ri
		ON
			ri.s_individualid = b.individualid
		WHERE
			b.individualid = ?
		AND
			b.schoolcd = ?
		AND
			ri.examyear = e.examyear
		AND
			ri.examcd = e.examcd)
ORDER BY
	examyear DESC,
	inpledate DESC,
	dispsequence

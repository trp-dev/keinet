SELECT
	e.examyear,
	e.examcd,
	e.examname,
	e.examname_abbr,
	e.examtypecd,
	e.examst,
	e.examdiv,
	e.tergetgrade,
	e.inpledate,
	e.out_dataopendate,
	e.dockingexamcd,
	e.dockingtype,
	e.dispsequence,
	e.masterdiv
FROM
	examination e
WHERE
	e.examyear >= ? /* 1 */
AND
	SUBSTR(e.out_dataopendate, 1, 8) <= ? /* 2 */
ORDER BY
	e.dispsequence

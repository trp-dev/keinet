SELECT
	e.examyear examyear,
	e.examcd examcd,
	e.examname examname,
	e.inpledate inpledate,
	e.dispsequence dispsequence
FROM
	examination e
WHERE
	e.examyear >= TO_CHAR(ADD_MONTHS(SYSDATE, -27), 'YYYY')
AND
	e.out_dataopendate <= TO_CHAR(SYSDATE, 'YYYYMMDD') || '0000'
AND
	EXISTS (
		SELECT
			1
		FROM
			school s
		INNER JOIN
			examtakenum_s ns
		ON
			ns.bundlecd = s.bundlecd
		WHERE
			(s.bundlecd = ? OR s.parentschoolcd = ?)
		AND
			NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000') <= e.out_dataopendate
		AND
			NVL2(s.stopdate, s.stopdate || '0000', '999999999999') > e.out_dataopendate
		AND
			ns.examyear = e.examyear
		AND
			ns.examcd = e.examcd)
ORDER BY
	examyear DESC,
	inpledate DESC,
	dispsequence

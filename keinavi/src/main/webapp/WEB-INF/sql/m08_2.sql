INSERT INTO
	ratingnumberrecount
SELECT DISTINCT
	cr.examyear,
	cr.examcd,
	cr.bundlecd,
	?,
	?,
	cr.univcd,
	cr.facultycd,
	cr.deptcd
FROM
	candidaterating cr
WHERE
	cr.examyear = ?
AND
	cr.examcd = ?
AND
	cr.individualid = ?
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			ratingnumberrecount re
		WHERE
			re.year = cr.examyear
		AND
			re.examcd = cr.examcd
		AND
			re.bundlecd = cr.bundlecd
		AND
			re.grade = ?
		AND
			re.class = ?
		AND
			re.univcd = cr.univcd
		AND
			re.facultycd = cr.facultycd
		AND
			re.deptcd = cr.deptcd)

SELECT
	rs.studentdiv,
	rs.univcd,
	u.uniname_abbr univname_abbr,
	rs.facultycd,
	u.facultyname_abbr,
	rs.deptcd,
	u.deptname_abbr,
	rs.agendacd,
	NULL,
	rs.ratingdiv
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD START */
	 , c.univdiv
	/* 2016/01/20 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	/* 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD START */
	/* 2019/09/02 QQ)Tanouchi ���ʃe�X�g�Ή� UPD START */
	ratingnumber_s rs,
	/*v_ratingnumber_s rs,*/
	/* 2019/09/02 QQ)Tanouchi ���ʃe�X�g�Ή� UPD END */
	/* 2019/11/25 QQ)Ooseto �p��F�莎�������Ή� UPD END */
	countuniv c,
	univmaster_basic u
WHERE
	rs.examyear = ? /* 1 */
AND
	rs.examcd = ? /* 2 */
AND
	rs.bundlecd = ? /* 3 */
AND
	rs.univcountingdiv = ? /* 4 */
AND
	rs.univcd = c.univcd
AND
	u.univcd = c.univcd
AND
	u.facultycd = rs.facultycd
AND
	u.deptcd = rs.deptcd
AND
	u.eventyear = rs.examyear
AND
	u.examdiv = ? /* 5 */
AND
	rs.studentdiv IN (#)
AND
	rs.ratingdiv IN (#)
/* SCHEDULE CONDITION */
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	rs.studentdiv,
	c.univdiv,
	c.dispsequence,
	rs.facultycd,
	u.deptsortkey,
	rs.ratingdiv
 */
/* 2016/01/20 QQ)Nishiyama ��K�͉��C DEL END */

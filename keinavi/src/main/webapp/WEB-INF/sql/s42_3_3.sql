SELECT
	p.prefcd || '999' bundlecd,
	'�i' || p.prefname || '�j' bundlename,
	st.examyear examyear,
	rp.subcd subcd,
	NVL(rp.avgpnt, -999) avgpnt,
	NVL(rp.numbers, -999) numbers
FROM
	prefecture p
CROSS JOIN
	subcdtrans st
LEFT OUTER JOIN
	subrecord_p rp
ON
	rp.examyear = st.examyear
AND
	rp.examcd = st.examcd
AND
	rp.subcd = st.subcd
AND
	rp.prefcd = p.prefcd
WHERE
	p.prefcd = ?
AND
	st.examyear IN (#)
AND
	st.examcd = ?
AND
	st.cursubcd = ?
ORDER BY
	examyear DESC

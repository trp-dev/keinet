SELECT
	v.subcd,
	v.subname,
	v.suballotpnt,
	eq.display_no,
	eq.questionname1,
	eq.qallotpnt,
	eq.question_no,
	NVL(rc.numbers, -999),
	NVL(qa.avgpnt, -999),
	NVL(qp.avgpnt, -999),
	NVL(qs.avgpnt, -999),
	NVL(qc.avgpnt, -999)
FROM
	v_i_qexamsubject v

LEFT OUTER JOIN
	examquestion eq
ON
	eq.examyear = v.examyear
AND
	eq.examcd = v.examcd
AND
	eq.subcd = v.subcd

LEFT OUTER JOIN
	qrecord_a qa
ON
	qa.examyear = eq.examyear
AND
	qa.examcd = eq.examcd
AND
	qa.subcd = eq.subcd
AND
	qa.question_no = eq.question_no

LEFT OUTER JOIN
	qrecord_p qp
ON
	qp.examyear = eq.examyear
AND
	qp.examcd = eq.examcd
AND
	qp.subcd = eq.subcd
AND
	qp.question_no = eq.question_no
AND
	qp.prefcd = ? /* 1 */

LEFT OUTER JOIN
	qrecord_s qs
ON
	qs.examyear = eq.examyear
AND
	qs.examcd = eq.examcd
AND
	qs.subcd = eq.subcd
AND
	qs.question_no = eq.question_no
AND
	qs.bundlecd = ? /* 2 */

LEFT OUTER JOIN
	(
		SELECT
			rc.subcd subcd,	
			rc.numbers numbers
		FROM
			subrecord_c rc
		WHERE
			rc.examyear = ? /* 3 */
		AND
			rc.examcd = ? /* 4 */
		AND
			rc.bundlecd = ? /* 5 */
		AND
			rc.grade = ? /* 6 */
		AND
			rc.class = ? /* 7 */
		AND
			rc.subcd < '7000'
		
		UNION ALL
	
		SELECT
			subcd,
			numbers
		FROM
			(
				SELECT
					rc.subcd subcd,	
					SUM(rc.numbers) OVER (PARTITION BY rc.subcd) numbers,
					rc.grade grade1,
					FIRST_VALUE(rc.grade) OVER (PARTITION BY rc.subcd) grade2,
					rc.class class1,
					FIRST_VALUE(rc.class) OVER (PARTITION BY rc.subcd) class2
				FROM
					subrecord_c rc,
					class_group cg
				WHERE
					rc.examyear = ? /* 8 */
				AND
					rc.examcd = ? /* 9 */
				AND
					rc.bundlecd = ? /* 10 */
				AND
					rc.subcd < '7000'
				AND
					rc.grade = cg.grade
				AND
					rc.class = cg.class
				AND
					cg.classgcd = ? /* 11 */
			)
		WHERE
			grade1 = grade2
		AND
			class1 = class2
	) rc
ON
	rc.subcd = eq.subcd

LEFT OUTER JOIN
	(
		SELECT
			qc.subcd subcd,
			qc.question_no question_no,
			qc.avgpnt avgpnt
		FROM
			qrecord_c qc
		WHERE
			qc.examyear = ? /* 12 */
		AND
			qc.examcd = ? /* 13 */
		AND
			qc.bundlecd = ? /* 14 */
		AND
			qc.grade = ? /* 15 */
		AND
			qc.class = ? /* 16 */
		AND
			qc.subcd < '7000'

		UNION ALL

		SELECT
			subcd,
			question_no,
			avgpnt
		FROM
			(
				SELECT	
					qc.subcd subcd,
					qc.question_no question_no,	
					ROUND(
						SUM(qc.numbers * qc.avgpnt)
							 OVER (PARTITION BY qc.subcd, qc.question_no)
						/ SUM(qc.numbers)
							OVER (PARTITION BY qc.subcd, qc.question_no), 2
					) avgpnt,
					qc.grade grade1,
					FIRST_VALUE (qc.grade)
						OVER (PARTITION BY qc.subcd, qc.question_no) grade2,
					qc.class class1,
					FIRST_VALUE (qc.class)
						OVER (PARTITION BY qc.subcd, qc.question_no) class2
				FROM
					qrecord_c qc,
					class_group cg
				WHERE
					qc.examyear = ? /* 17 */
				AND
					qc.examcd = ? /* 18 */
				AND
					qc.bundlecd = ? /* 19 */
				AND
					qc.subcd < '7000'
				AND
					qc.grade = cg.grade
				AND
					qc.class = cg.class
				AND
					cg.classgcd = ? /* 20 */
			)
		WHERE
			grade1 = grade2
		AND
			class1 = class2
	) qc
ON
	qc.subcd = eq.subcd
AND
	qc.question_no = eq.question_no

WHERE
	v.examyear = ? /* 21 */
AND
	v.examcd = ? /* 22 */
AND
	v.subcd < '7000'
AND
	(
			(v.subcd IN (#)
				AND NOT EXISTS (
					SELECT es.rowid FROM examsubject es
					WHERE es.examyear = v.examyear
					AND es.examcd = v.examcd
					AND es.fusion_subcd = v.subcd))
		OR
			v.fusion_subcd IN(#)
	)

ORDER BY
	v.dispsequence,
	eq.display_no,
	eq.question_no

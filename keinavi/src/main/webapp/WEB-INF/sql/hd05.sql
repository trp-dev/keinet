INSERT INTO
	ko_subject
SELECT
	?,
	d.subcd,
	d.subname,
	d.subaddrname,
	d.suballotpnt,
	NULL, NULL
FROM
	ko_defaultsubject d
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			ko_subject a
		WHERE
			a.schoolcd = ?
		AND
			a.subcd = d.subcd)

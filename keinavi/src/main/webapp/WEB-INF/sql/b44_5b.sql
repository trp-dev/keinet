SELECT DISTINCT
	rp.studentdiv studentdiv,
	rp.univcd univcd,
	u.uniname_abbr univname_abbr,
	rp.facultycd facultycd,
	u.facultyname_abbr facultyname_abbr,
	rp.deptcd deptcd,
	u.deptname_abbr deptname_abbr,
	rp.agendacd agendacd,
	NULL schedulename,
	rp.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence,
	u.deptsortkey deptsortkey
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	, u.univname_kana
	, u.uninightdiv
	, u.facultyconcd
	, u.unigdiv
	, u.deptserial_no
	, u.schedulesys
	, u.schedulesysbranchcd
	, u.deptname_kana
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	univmaster_basic u,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_p rp, */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
	ratingnumber_p rp,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
	countuniv c
WHERE
	rp.examyear = ? /* 1 */
AND
	rp.examcd = ? /* 2 */
AND
	rp.univcountingdiv = ? /* 3 */
AND
	rp.univcd = c.univcd
AND
	u.univcd = rp.univcd
AND
	u.facultycd = rp.facultycd
AND
	u.deptcd = rp.deptcd
AND
	u.eventyear = rp.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rp.studentdiv IN (#)
AND
	rp.prefcd IN (#)
AND
	rp.ratingdiv IN (#)
/* SCHEDULE CONDITION */

UNION

SELECT DISTINCT
	rs.studentdiv studentdiv,
	rs.univcd univcd,
	u.uniname_abbr univname_abbr,
	rs.facultycd facultycd,
	u.facultyname_abbr facultyname_abbr,
	rs.deptcd deptcd,
	u.deptname_abbr deptname_abbr,
	rs.agendacd agendacd,
	NULL schedulename,
	rs.ratingdiv ratingdiv,
	c.univdiv univdiv,
	c.dispsequence dispsequence,
	u.deptsortkey deptsortkey
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	 , u.univname_kana
	 , u.uninightdiv
	 , u.facultyconcd
	 , u.unigdiv
	 , u.deptserial_no
	 , u.schedulesys
	 , u.schedulesysbranchcd
	 , u.deptname_kana
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM
	univmaster_basic u,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_s rs, */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
	ratingnumber_s rs,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
    countuniv c
WHERE
	rs.examyear = ? /* 1 */
AND
	rs.examcd = ? /* 2 */
AND
	rs.univcountingdiv = ? /* 3 */
AND
	rs.univcd = c.univcd
AND
	u.univcd = rs.univcd
AND
	u.facultycd = rs.facultycd
AND
	u.deptcd = rs.deptcd
AND
	u.eventyear = rs.examyear
AND
	u.examdiv = ? /* 4 */
AND
	rs.studentdiv IN (#)
AND
	rs.bundlecd IN (#)
AND
	rs.ratingdiv IN (#)
/* SCHEDULE CONDITION */

/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	studentdiv,
	univdiv,
	dispsequence,
	facultycd,
	deptsortkey,
	ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

SELECT 
	COUNT(*) count 
FROM 
	examplanuniv 
WHERE  
	individualid = ? 
AND 
	univcd = ? 
AND 
	facultycd = ? 
AND 
	deptcd = ? 
AND 
	entexammodecd = ? 

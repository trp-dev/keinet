SELECT
	/* CEFRīń.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRīń.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Čµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRīń.ĄŃ */
	ci.sort AS ci_sort,
	/* ¼Ģ */
	'iSj' AS bundlename,
	/* CEFRę¾óµiSj.l */
	casa.numbers AS s_numbers,
	/* CEFRę¾óµiSj.\¬ä */
	casa.compratio AS s_compratio
FROM
	/* Ķ}X^TBL */
	examination en
	/* CEFRīńTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'v' AS cerflevelname_abbr,
			-1 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		en.examyear = ci.eventyear
	AND
		en.examdiv = ci.examdiv
	/* CEFRę¾óµiSjTBL */
	LEFT JOIN cefracquisitionstatus_a casa
	ON
		casa.examyear = ci.eventyear
	AND
		casa.examcd = en.examcd
	AND
		casa.cefrlevelcd = ci.cefrlevelcd
WHERE
		en.examyear = ? /* 1 */
	AND
		en.examcd   = ? /* 2 */
ORDER BY
	ci.sort

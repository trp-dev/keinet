INSERT INTO
	subrecount
SELECT
	ri.examyear,
	ri.examcd,
	ri.bundlecd,
	?,
	?,
	ri.subcd
FROM
	subrecord_i ri
WHERE
	ri.individualid = ?
AND
	ri.examyear = ?
AND	
	NOT EXISTS (
		SELECT
			'1'
		FROM
			examination e
		WHERE
			e.examyear = ri.examyear
		AND
			e.examcd = ri.examcd
		AND
			e.examtypecd = '32'
		)
AND
	NOT EXISTS (
		SELECT
			1
		FROM
			subrecount re
		WHERE
			re.examyear = ri.examyear
		AND
			re.examcd = ri.examcd
		AND
			re.bundlecd = ri.bundlecd
		AND
			re.grade = ?
		AND
			re.class = ?
		AND
			re.subcd = ri.subcd
	)

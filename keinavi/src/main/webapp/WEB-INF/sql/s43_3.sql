SELECT
	s.bundlename,
	NVL(rs.numbers, -999),
	NVL(rs.avgscorerate, -999)
FROM
	qrecord_s rs,
	examination e,
	school s
WHERE
	rs.examyear(+) = ? /* 1 */
AND
	rs.examcd(+) = ? /* 2 */
AND
	rs.bundlecd(+) = s.bundlecd
AND
	rs.subcd(+) = ? /* 3 */
AND
	rs.question_no(+) = ? /* 4 */
AND
	s.bundlecd= ? /* 5 */
AND
	e.examyear = ? /* 6 */
AND
	e.examcd = ? /* 7 */
AND
	NVL(s.newgetdate, '00000000') <= SUBSTR(e.out_dataopendate, 1, 8)
AND
	NVL(s.stopdate, '99999999') > SUBSTR(e.out_dataopendate, 1, 8)

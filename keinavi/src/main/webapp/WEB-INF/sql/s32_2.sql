SELECT
	*
FROM
(
/*--- 校内全体 ---*/
SELECT
	 /* 集計用比較対象クラス.クラス */
	 '校内全体' AS class
	 /* クラス表示順 */
	,0 AS dispsequence
	,cass.numbers AS numbers
	,cass.compratio AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN cass.EXAMYEAR IS NULL THEN '0' ELSE '1' END AS flg
FROM
	examination ex
LEFT JOIN
	/* CEFR取得状況（学校）TBL */
	cefracquisitionstatus_s cass
ON
	cass.EXAMYEAR = ex.EXAMYEAR
AND
	cass.EXAMCD = ex.EXAMCD
AND
	cass.BUNDLECD = ? /* 1 */
AND
	cass.CEFRLEVELCD = ? /* 2 */
WHERE
	ex.EXAMYEAR = ? /* 3 */
AND
	ex.EXAMCD = ? /* 4 */

UNION ALL
/*--- 選択している個別クラス ---*/

SELECT
	 /* 集計用比較対象クラス.クラス */
	 TRIM(ccc.CLASS) AS class
	 /* クラス表示順 */
	,ccc.DISPSEQUENCE AS dispsequence
	,casc.numbers AS numbers
	,casc.compratio AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN casc.EXAMYEAR IS NULL THEN '0' ELSE '1' END AS flg
FROM
	countcompclass ccc
INNER JOIN
	examination ex
ON
	ex.EXAMYEAR = ? /* 5 */
AND
	ex.EXAMCD = ? /* 6 */
LEFT JOIN
	/* CEFR取得状況（クラス）TBL */
	cefracquisitionstatus_c casc
ON
	casc.EXAMYEAR = ex.EXAMYEAR
AND
	casc.EXAMCD = ex.EXAMCD
AND
	casc.BUNDLECD = ? /* 7 */
AND
	casc.CEFRLEVELCD = ? /* 8 */
AND
	casc.GRADE = ccc.GRADE
AND
	casc.CLASS = ccc.CLASS
WHERE NOT EXISTS (
	SELECT *
	FROM
		class_group cg
	WHERE
		cg.YEAR = ex.EXAMYEAR
	AND cg.BUNDLECD = ? /* 9 */
	AND cg.GRADE = ccc.GRADE
	AND cg.CLASSGCD = ccc.CLASS
)

UNION ALL

/*--- 選択している複合クラス ---*/
SELECT
	 /* 集計用比較対象クラス.クラス */
	 TRIM(MAX(gp.CLASSGNAME)) AS class
	 /* クラス表示順 */
	,MAX(ccc.DISPSEQUENCE) AS dispsequence
	 /* 英語参加試験別・合計CEFR取得状況（高校）.人数 */
	,SUM(casc.numbers) AS numbers
	 /* 英語参加試験別・合計CEFR取得状況（高校）.構成比 */
	,SUM(casc.compratio) AS compratio
	 /* 受験有無フラグ */
	,CASE WHEN MAX(casc.EXAMYEAR) IS NULL THEN '0' ELSE '1' END AS flg
FROM
	countcompclass ccc
INNER JOIN
	examination ex
ON
	ex.EXAMYEAR = ? /* 10 */
AND
	ex.EXAMCD = ? /* 11 */
INNER JOIN
	class_group cg
ON
	cg.YEAR = ex.EXAMYEAR
AND
	cg.BUNDLECD = ? /* 12 */
AND
	cg.GRADE = ccc.GRADE
AND
	cg.CLASSGCD = ccc.CLASS
INNER JOIN
	classgroup gp
ON
	gp.CLASSGCD = cg.CLASSGCD
LEFT JOIN
	/* CEFR取得状況（クラス）TBL */
	cefracquisitionstatus_c casc
ON
	casc.EXAMYEAR = ex.EXAMYEAR
AND
	casc.EXAMCD = ex.EXAMCD
AND
	casc.BUNDLECD = ? /* 13 */
AND
	casc.CEFRLEVELCD = ? /* 14 */
AND
	casc.GRADE = ccc.GRADE
AND
	casc.CLASS = cg.CLASS
GROUP BY
	 ccc.GRADE
	,ccc.CLASS
ORDER BY
	dispsequence
)
WHERE
	/* 校内全体を含めて21まで出力 */
	ROWNUM <= 21

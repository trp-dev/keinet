SELECT
	'�i' || p.prefname || '�j',
	NVL(rp.numbers, -999),
	NVL(rp.avgscorerate, -999)
FROM
	prefecture p

LEFT OUTER JOIN
	qrecord_p rp
ON
	rp.examyear = ? /* 1 */
AND
	rp.examcd = ? /* 2 */
AND
	rp.subcd = ? /* 3 */
AND
	rp.question_no = ? /* 4 */
AND
	rp.prefcd = p.prefcd
	
WHERE
	p.prefcd = ? /* 5 */

SELECT
	'99999',
	TRIM('(�S��)'),
	NULL,
	NULL,
	NVL(ra.numbers, 0)
FROM
	DUAL
LEFT OUTER JOIN
	(
		SELECT
			ra.examyear examyear,
			ra.examcd examcd,
			ra.numbers numbers
		FROM
			examtakenum_a ra
		INNER JOIN
			(
				SELECT
					MIN(a.studentdiv) studentdiv
				FROM
					examtakenum_a a
				WHERE
					a.examyear = ?
				AND
					a.examcd = ?
			) a
		ON
			a.studentdiv = ra.studentdiv
	) ra
ON
	ra.examyear = ?
AND
	ra.examcd = ?
UNION ALL

SELECT
	p.prefcd || '999',
	'�i' || p.prefname || '�j',
	NULL,
	NULL,
	NVL(ep.numbers, 0)
FROM
	prefecture p
LEFT OUTER JOIN
	examtakenum_p ep
ON
	ep.examyear = ? /* 5 */
AND
	ep.examcd = ? /* 6 */
AND
	ep.prefcd = p.prefcd

UNION ALL

SELECT
	s.bundlecd,
	s.bundlename,
	s.rank,
	s.facultycd,
	NVL(rs.numbers, 0)
FROM
	school s
LEFT OUTER JOIN
	examtakenum_s rs
ON
	rs.examyear = ? /* 7 */
AND
	rs.examcd = ? /* 8 */
AND
	rs.bundlecd = s.bundlecd
WHERE
	s.bundlecd IN (#)
AND
	NVL(s.newgetdate, '00000000') <= TO_CHAR(SYSDATE, 'YYYYMMDD')
AND
	NVL(s.stopdate, '99999999') > TO_CHAR(SYSDATE, 'YYYYMMDD')

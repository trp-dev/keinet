SELECT
	candidaterank1
FROM (
	SELECT /*+ ORDERED INDEX(cr PK_CANDIDATERATING) */
		cr.candidaterank candidaterank1,
		MIN(cr.candidaterank) OVER (PARTITION BY u.unigdiv) candidaterank2
	FROM
		candidaterating cr,
		univmaster_basic u
	WHERE
		cr.examyear = ? /* 1 */
	AND
		cr.examcd = ? /* 2 */
	AND
		u.eventyear = cr.examyear
	AND
		u.examdiv = ? /* 3 */
	AND
		u.univcd = cr.univcd
	AND
		u.facultycd = cr.facultycd
	AND
		u.deptcd = cr.deptcd
	AND
		cr.individualid = ? /* 4 */
	AND
		u.unigdiv IN ('C', 'D', 'E')
)
WHERE
	candidaterank1 = candidaterank2

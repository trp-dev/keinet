INSERT
INTO EXDATA_APP(
  APPLICATION_ID
  , APP_ID
  , EXAMYEAR
  , EXAMCD
  , BUNDLECD
  , TEACHER_NAME
  , APP_COMMENT
  , APP_DATE
  , APP_STATUS_CODE
)
SELECT
  ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , ?
  , SYSTIMESTAMP
  , '00'
FROM
  DUAL
WHERE
  NOT EXISTS(
    SELECT
      1
    FROM
      EXDATA_APP A
      INNER JOIN EXDATA_APP_SECT S
        ON S.APPLICATION_ID = A.APPLICATION_ID
        AND S.APP_SECTORCD IN (#)
    WHERE
      A.EXAMYEAR = ?
      AND A.EXAMCD = ?
      AND A.BUNDLECD = ?
      AND A.CANCEL_FLG IS NULL
  )

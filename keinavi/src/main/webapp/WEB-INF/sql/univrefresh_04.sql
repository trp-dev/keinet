UPDATE
	examplanuniv ep
SET
	ep.year = ?,
	ep.examdiv = ?
WHERE
	ep.individualid = ?
AND
	ep.univcd = ?
AND
	ep.facultycd = ?
AND
	ep.deptcd = ?
AND
	ep.entexammodecd = ?
AND
	ep.entexamdiv1 = ?
AND
	ep.entexamdiv2 = ?
AND
	ep.entexamdiv3 = ?

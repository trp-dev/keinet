SELECT
	/* CEFRîñ.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRîñ.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRîñ.ÀÑ */
	ci.sort AS ci_sort,
	/* wZ}X^.ê¼ */
	sc.bundlename,
	/* pêFè±ÊEvCEFRæ¾óµiZj.l */
	ecass.numbers AS numbers,
	/* pêFè±ÊEvCEFRæ¾óµiZj.\¬ä */
	ecass.compratio AS compratio
FROM
	/* Í}X^TBL */
	examination en
	/* wZ}X^ */
	INNER JOIN school sc
	ON
		sc.bundlecd = ? /* '1' */
	/* CEFRîñTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'v' AS cerflevelname_abbr,
			-1 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
	ON
		ci.eventyear = en.examyear
	AND
		ci.examdiv = en.examdiv
	/* pêFè±ÊEvCEFRæ¾óµiwZjTBL */
	LEFT JOIN engpt_cefracqstatus_s ecass
	ON
		ecass.examyear = en.examyear
	AND
		ecass.examcd = en.examcd
	AND
		ecass.engptcd = ? /* 2 */
	AND
		ecass.engptlevelcd = ? /* 3 */
	AND
		ecass.cefrlevelcd = ci.cefrlevelcd
	AND
		ecass.bundlecd = sc.bundlecd
WHERE
		en.examyear = ? /* '4' */
	AND
		en.examcd = ? /* '5' */
ORDER BY
	ci_sort

SELECT
	m.loginid,
	m.loginname,
	m.loginpwd,
	m.defaultpwd,
	m.manage_flg,
	m.mainte_flg,
	m.func1_flg,
	m.func2_flg,
	m.func3_flg,
	NVL(m.func4_flg, '3') func4_flg
FROM
	loginid_manage m
WHERE
	m.schoolcd = ?

SELECT
	univcd,
	univname_abbr,
	facultycd,
	facultyname_abbr,
	schedulecd,
	schedulename,
	univdivcd,
	dispsequence,
	univdiv,
	count
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	 , univname_kana
	 , uninightdiv
	 , facultyconcd
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM (
	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd univcd,
		u.uniname_abbr univname_abbr,
		cr.facultycd facultycd,
		u.facultyname_abbr facultyname_abbr,
		' ' schedulecd,
		' ' schedulename,
		u.unidiv univdivcd,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		COUNT(b.individualid) OVER (PARTITION BY cr.univcd) count,
		b.individualid individualid1,
		FIRST_VALUE(b.individualid) OVER (PARTITION BY cr.univcd, cr.facultycd) individualid2,
		cr.deptcd deptcd1,
		FIRST_VALUE(cr.deptcd) OVER (PARTITION BY cr.univcd, cr.facultycd) deptcd2,
		cr.candidaterank rank1,
		FIRST_VALUE(cr.candidaterank) OVER (PARTITION BY cr.univcd, cr.facultycd) rank2
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		 , u.uninightdiv
		 , u.facultyconcd
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		basicinfo b,
		historyinfo h,
		candidaterating cr,
		countuniv c,
		h12_univmaster u
	WHERE
		b.schoolcd = ? /* 1 */
	AND
		h.individualid = b.individualid
	AND
		h.year = ? /* 2 */
	AND
		h.grade = ? /* 3 */
	AND
		h.class = ? /* 4 */
	AND
		cr.individualid = h.individualid
	AND
		cr.examyear = ? /* 5 */
	AND
		cr.examcd = ? /* 6 */
	AND
		cr.univcd = c.univcd
	AND
		u.eventyear = cr.examyear
	AND
		u.examdiv = ? /* 8 */
	AND
		u.univcd = cr.univcd
	AND
		u.facultycd = cr.facultycd
	AND
		u.deptcd = cr.deptcd

	UNION ALL

	SELECT /*+ ORDERED USE_NL(b h) INDEX(b BASICINFO_IDX) INDEX(cr PK_CANDIDATERATING) */
		cr.univcd univcd,
		u.uniname_abbr univname_abbr,
		cr.facultycd facultycd,
		u.facultyname_abbr facultyname_abbr,
		' ' schedulecd,
		' ' schedulename,
		u.unidiv univdivcd,
		c.dispsequence dispsequence,
		c.univdiv univdiv,
		COUNT(b.individualid) OVER (PARTITION BY cr.univcd) count,
		b.individualid individualid1,
		FIRST_VALUE(b.individualid) OVER (PARTITION BY cr.univcd, cr.facultycd) individualid2,
		cr.deptcd deptcd1,
		FIRST_VALUE(cr.deptcd) OVER (PARTITION BY cr.univcd, cr.facultycd) deptcd2,
		cr.candidaterank rank1,
		FIRST_VALUE(cr.candidaterank) OVER (PARTITION BY cr.univcd, cr.facultycd) rank2
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
		 , u.univname_kana
		 , u.uninightdiv
		 , u.facultyconcd
		/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
	FROM
		basicinfo b,
		historyinfo h,
		class_group cg,
		candidaterating cr,
		countuniv c,
		h12_univmaster u
	WHERE
		b.schoolcd = ? /* 9 */
	AND
		h.year = ? /* 10 */
	AND
		h.individualid = b.individualid
	AND
		cg.year = h.year
	AND
		cg.bundlecd = b.schoolcd
	AND
		cg.grade = h.grade
	AND
		cg.class = h.class
	AND
		cg.classgcd = ? /* 11 */
	AND
		cr.individualid = b.individualid
	AND
		cr.examyear = ? /* 12 */
	AND
		cr.examcd = ? /* 13 */
	AND
		cr.univcd = c.univcd
	AND
		u.eventyear = cr.examyear
	AND
		u.examdiv = ? /* 15 */
	AND
		u.univcd = cr.univcd
	AND
		u.facultycd = cr.facultycd
	AND
		u.deptcd = cr.deptcd
)
WHERE
	individualid1 = individualid2
AND
	deptcd1 = deptcd2
AND
	rank1 = rank2

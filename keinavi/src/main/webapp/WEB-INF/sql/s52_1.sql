SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	es.dispsequence,
	st.subcd,
	NVL(rs.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rs.avgdeviation, -999)
FROM
	examsubject es

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = ? /* 1 */
AND
	st.examcd = ? /* 2 */
AND
	st.cursubcd = es.subcd

LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = st.examyear
AND
	rs.examcd = st.examcd
AND
	rs.subcd = st.subcd
AND
	rs.bundlecd = ? /* 3 */

WHERE
	es.examyear = ? /* 4 */
AND
	es.examcd = ? /* 5 */
AND
	es.subcd IN (#)

SELECT
	es.subcd,
	es.subname,
	es.dispsequence,
	es.subaddrname
FROM
	examsubject es
WHERE
	es.examyear = ?
AND
	es.examcd = ?
AND
	es.subcd < '7000'
ORDER BY
	es.dispsequence

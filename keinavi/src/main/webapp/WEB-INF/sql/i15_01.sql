SELECT 
    ep.univcd, 
    ep.facultycd, 
    ep.deptcd, 
    hs.uniname_abbr univname_abbr, 
    hs.facultyname_abbr facultyname_abbr, 
    hs.deptname_abbr deptname_abbr, 
    hs.unidiv univdivcd, 
    hs.unigdiv schedulecd, 
    ep.examplandate1, 
    ep.examplandate2, 
    ep.examplandate3, 
    ep.examplandate4, 
    ep.examplandate5, 
    ep.examplandate6, 
    ep.examplandate7, 
    ep.examplandate8, 
    ep.examplandate9, 
    ep.examplandate10, 
    ep.proventexamsite1, 
    ep.proventexamsite2, 
    ep.proventexamsite3, 
    ep.proventexamsite4, 
    ep.proventexamsite5, 
    ep.proventexamsite6, 
    ep.proventexamsite7, 
    ep.proventexamsite8, 
    ep.proventexamsite9, 
    ep.proventexamsite10, 
    SUBSTR(sd.entexamapplideadline, 5, 4) applideadline, 
    SUBSTR(sd.sucanndate, 5, 4) sucanndate, 
    SUBSTR(sd.procedeadline, 5, 4) procedeadline, 
    nvl(hs.borderscorerate2, -999.0) cen_scorerate, 
    decode(r.rankcd, '15', 0, nvl(r.rankllimits, -999.0)) rankllimits, 
    r.rankname rankname, 
    nvl(ep.remarks, ep2.remarks) as remarks, 
    decode(hs.unidiv, '01', '02', hs.unidiv) as natlpvtdiv 
FROM 
	univmaster_basic hs 
INNER JOIN 
	examplanuniv ep 
ON 
	hs.eventyear = ep.year 
AND 
	hs.examdiv = ep.examdiv 
AND 
	hs.univcd = ep.univcd 
AND 
	hs.facultycd = ep.facultycd 
AND 
	hs.deptcd = ep.deptcd 
INNER JOIN 
	rank r 
ON 
	hs.entexamrank = r.rankcd 
LEFT JOIN 
	examscheduledetail sd 
ON 
	sd.year = hs.eventyear 
AND 
	sd.examdiv = hs.examdiv
AND 
	sd.univcd = hs.univcd 
AND 
	sd.facultycd = hs.facultycd 
AND 
	sd.deptcd = hs.deptcd 
AND 
	sd.schoolproventdiv = ep.entexamdiv2 
AND 
	sd.entexamdiv_1_2term = ep.entexamdiv3 
AND 
	sd.entexamdiv_1_2order = ep.entexamdiv1 
AND 
	ep.entexammodecd = '01' 
LEFT JOIN 
	examplanuniv ep2 
ON 
	ep2.individualid = ep.individualid 
AND 
	ep2.univcd = ep.univcd 
AND 
	ep2.facultycd = ep.facultycd 
AND 
	ep2.deptcd = ep.deptcd 
AND 
	ep2.entexammodecd = '01' 
AND 
	ep2.entexamdiv1 = '1' 
AND 
	ep2.entexamdiv2 = '1' 
AND 
	ep2.entexamdiv3 = '01' 
LEFT JOIN 
	univmaster_info ui 
ON 
	ui.eventyear = hs.eventyear 
AND 
	ui.univcd = hs.univcd 
WHERE 
	ep.individualid = ? 
ORDER BY
     hs.eventyear, 
     natlpvtdiv, 
     ui.kana_num, 
     hs.univcd, 
     hs.uninightdiv, 
     hs.facultyconcd, 
     hs.unigdiv, 
     hs.deptserial_no, 
     hs.schedulesys, 
     hs.schedulesysbranchcd, 
     hs.deptname_kana, 
     hs.deptcd, 
     hs.examdiv 

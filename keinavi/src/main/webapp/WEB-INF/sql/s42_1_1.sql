SELECT
	et.examyear examyear,
	rs.subcd subcd,
	NVL(rs.numbers, -999) numbers,
	NVL(rs.avgpnt, -999) avgpnt,
	NVL(rs.avgdeviation, -999) avgdeviation,
	et.examcd examcd
FROM
	examcdtrans et
LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = et.examyear
AND
	ee.examcd = et.examcd
LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = et.examyear
AND
	st.examcd = et.curexamcd
AND
	st.cursubcd = ?
LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = ee.examyear
AND
	rs.examcd = ee.examcd
AND
	rs.bundlecd = ?
AND
	rs.subcd = st.subcd
WHERE
	et.examyear IN (#)
AND
	et.curexamcd = ?
ORDER BY
	examyear DESC

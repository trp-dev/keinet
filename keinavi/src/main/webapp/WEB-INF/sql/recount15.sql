INSERT INTO
	subrecord_c
SELECT /*+ USE_NL(ri es) USE_NL(es ra) */
	ri.examyear,
	ri.examcd,
	ri.bundlecd,
	ri.grade,
	ri.class,
	ri.subcd,
	ROUND(AVG(ri.score), 1),
	CASE
		WHEN
			ri.subcd < '7000'
		THEN
			ROUND((ROUND(AVG(ri.score), 1) - ra.avgpnt) / ra.stddeviation * 10 + 50, 1)
		ELSE
			ROUND(AVG(ri.a_deviation), 1)
	END,
	ROUND(ROUND(AVG(ri.score), 1) / es.suballotpnt * 100, 1),
	COUNT(ri.individualid)
FROM (
	SELECT
		ri.individualid individualid,
		ri.examyear examyear,
		ri.examcd examcd,
		ri.bundlecd bundlecd,
		re.grade grade,
		re.class class,
		re.combisubcd subcd,
		ri.score score,
		ri.a_deviation a_deviation
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_i ri
	ON
		ri.examyear = re.examyear
	AND
		ri.examcd = re.examcd
	AND
		ri.subcd = re.subcd
	AND
		ri.bundlecd = re.bundlecd
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?
	AND
		EXISTS (
			SELECT
				1
			FROM
				historyinfo hi
			WHERE
				hi.individualid = ri.individualid
			AND
				hi.year = ri.examyear
			AND
				hi.grade = re.grade
			AND
				hi.class = re.class)
	UNION ALL
	SELECT
		pp.answersheet_no,
		pp.examyear,
		pp.examcd,
		pp.schoolcd,
		pp.grade,
		pp.class,
		re.combisubcd,
		pp.score,
		pp.a_deviation
	FROM
		v_subrecount re
	INNER JOIN
		subrecord_pp pp
	ON
		pp.examyear = re.examyear
	AND
		pp.examcd = re.examcd
	AND
		pp.subcd = re.subcd
	AND
		pp.schoolcd = re.bundlecd
	AND
		pp.grade = re.grade
	AND
		pp.class = re.class
	WHERE
		re.examyear = ?
	AND
		re.examcd = ?
	AND
		re.bundlecd = ?) ri
LEFT OUTER JOIN
	examsubject es
ON
	es.examyear = ri.examyear
AND
	es.examcd = ri.examcd
AND
	es.subcd = ri.subcd
LEFT OUTER JOIN
	subrecord_a ra
ON
	ra.examyear = es.examyear
AND
	ra.examcd = es.examcd
AND
	ra.subcd = es.subcd
AND
	ra.studentdiv = ?
GROUP BY
	ri.examyear,
	ri.examcd,
	ri.bundlecd,
	ri.grade,
	ri.class,
	ri.subcd,
	es.suballotpnt,
	ra.avgpnt,
	ra.stddeviation

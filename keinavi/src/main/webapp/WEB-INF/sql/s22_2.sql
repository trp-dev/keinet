SELECT /*+ ordered use_nl(st rs) */
	e.examyear examyear,
	NVL(highlimit, -999),
	NVL(lowlimit, -999) lowlimit,
	NVL(rs.numbers, -999),
	NVL(rs.compratio, -999)
FROM
	deviationzone d

INNER JOIN
	examcdtrans e
ON
	e.examyear IN (#)
AND
	e.curexamcd = ?/* 1 */

LEFT OUTER JOIN
	existsexam ee
ON
	ee.examyear = e.examyear
AND
	ee.examcd = e.examcd

LEFT OUTER JOIN
	subcdtrans st
ON
	st.examyear = ee.examyear
AND
	st.examcd = e.curexamcd
AND
	st.cursubcd = ? /* 2 */

LEFT OUTER JOIN
	subdistrecord_s rs
ON
	rs.examyear = e.examyear
AND
	rs.examcd = e.examcd
AND
	rs.subcd = st.subcd
AND
	rs.devzonecd = d.devzonecd
AND
	rs.bundlecd = ? /* 3 */

ORDER BY
	examyear DESC,
	lowlimit DESC

SELECT
	es.subcd,
	es.subname,
	es.suballotpnt,
	NVL(rs.numbers, -999),
	ma.answer_no,
	NVL(mq.perfrecordflg, -999),
	NVL(mq.perfqflg, -999),
	ma.allotpnt,
	mq.correctanswer,
	NVL(mq.s_scorerate, -999),
	NVL(mq.a_scorerate, -999),
	mq.err1_answer,
	NVL(mq.err1_markrate, -999),
	mq.err2_answer,
	NVL(mq.err2_markrate, -999),
	mq.err3_answer,
	NVL(mq.err3_markrate, -999),
	ma.bigquestion
FROM
	v_sm_qexamsubject es

LEFT OUTER JOIN
	subrecord_s rs
ON
	rs.examyear = es.examyear
AND
	rs.examcd = es.examcd
AND
	rs.subcd = es.subcd
AND
	rs.bundlecd = ? /* 1 */

LEFT OUTER JOIN
	markanswer_no ma
ON
	ma.examyear = es.examyear
AND
	ma.examcd = es.examcd
AND
	ma.subcd = es.subcd

LEFT OUTER JOIN
	markqscorerate mq
ON
	mq.examyear = es.examyear
AND
	mq.examcd = es.examcd
AND
	mq.bundlecd = rs.bundlecd
AND
	mq.subjectcd = es.subcd
AND
	mq.answer_no = ma.answer_no

WHERE
	es.examyear = ? /* 2 */
AND
	es.examcd = ? /* 3 */
AND
	es.subcd < '7000'
AND
	es.subcd IN (#)

ORDER BY
	es.dispsequence,
	mq.sortorder

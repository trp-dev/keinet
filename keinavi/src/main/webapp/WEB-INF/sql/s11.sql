SELECT
	rs.subcd,
	es.subname,
	es.suballotpnt,
	NVL(rs.numbers, -999),
	NVL(rp.numbers, -999),
	NVL(ra.numbers, -999),
	NVL(rs.avgpnt, -999),
	NVL(rp.avgpnt, -999),
	NVL(ra.avgpnt, -999),
	NVL(rs.avgdeviation, -999),
	NVL(rp.avgdeviation, -999),
	NVL(ra.avgdeviation, -999)
FROM
	v_sm_cmexamsubject es,
	subrecord_s rs,
	subrecord_p rp,
	subrecord_a ra
WHERE
	rs.examyear = ? /* 1 */
AND
	rs.examcd = ? /* 2 */
AND
	rs.bundlecd = ? /* 3 */
AND
	es.examyear = rs.examyear
AND
	es.examcd = rs.examcd
AND
	es.subcd = rs.subcd
AND
	rp.examyear = rs.examyear
AND
	rp.examcd = rs.examcd
AND
	rp.prefcd = ? /* 4 */
AND
	rp.subcd = rs.subcd
AND
	ra.examyear = rs.examyear
AND
	ra.examcd = rs.examcd
AND
	ra.subcd = rs.subcd
AND
	ra.studentdiv = (
		SELECT
			MIN(a.studentdiv)
		FROM
			subrecord_a a
		WHERE
			a.examyear = rs.examyear
		AND
			a.examcd = rs.examcd
	)
ORDER BY
	es.dispsequence

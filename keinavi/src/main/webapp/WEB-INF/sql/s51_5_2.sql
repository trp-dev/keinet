SELECT
	'�i' || p.prefname  ||'�j',
	NVL(a.numbers, -999),
	NVL(a.avgpnt, -999),
	NVL(a.avgdeviation, -999)
FROM
	prefecture p,
	(
		SELECT
			rp.prefcd prefcd,
			rp.numbers numbers,
			rp.avgpnt avgpnt,
			rp.avgdeviation avgdeviation
		FROM
			subrecord_p rp,
			subcdtrans st
		WHERE
			rp.examyear = ? /* 1 */
		AND
			rp.examcd = ? /* 2 */
		AND
			rp.subcd = st.subcd
		AND
			st.examyear = rp.examyear
		AND
			st.examcd = rp.examcd
		AND
			st.cursubcd = ? /* 3 */
		AND
			rp.prefcd = ? /* 4 */
	) a
WHERE
	a.prefcd(+) = p.prefcd
AND
	p.prefcd = ? /* 5 */

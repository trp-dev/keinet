SELECT
	ra.studentdiv,
	ra.univcd,
	u.uniname_abbr univname_abbr,
	ra.facultycd,
	u.facultyname_abbr,
	ra.deptcd,
	u.deptname_abbr,
	ra.agendacd,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD START */
	/* NULL, */
	NULL schedulename,
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C UPD END */
	ra.ratingdiv
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD START */
	 , u.univname_kana
	/* 2016/01/21 QQ)Nishiyama ��K�͉��C ADD END */
FROM
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD START */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD START */
	/* v_ratingnumber_a ra, */
	/* 2019/09/30 QQ) ���ʃe�X�g�Ή� UPD END */
	ratingnumber_a ra,
/* 2019/11/26 QQ)nagai �p��F�莎�������Ή� UPD END   */
	countuniv c,
	univmaster_basic u
WHERE
	ra.examyear = ? /* 1 */
AND
	ra.examcd = ? /* 2 */
AND
	ra.univcountingdiv = ? /* 3 */
AND
	ra.univcd = c.univcd
AND
	u.univcd = c.univcd
AND
	u.facultycd = ra.facultycd
AND
	u.deptcd = ra.deptcd
AND
	u.eventyear = ra.examyear
AND
	u.examdiv = ? /* 4 */
AND
	ra.studentdiv IN (#)
AND
	ra.ratingdiv IN (#)
/* SCHEDULE CONDITION */

/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL START */
/*
ORDER BY
	ra.studentdiv,
	c.univdiv,
	c.dispsequence,
	ra.facultycd,
	u.deptsortkey,
	ra.ratingdiv
 */
/* 2016/01/21 QQ)Nishiyama ��K�͉��C DEL END */

SELECT
	p.profileid,
	p.profilename,
	p.profilefid,
	p.com,
	p.createdate,
	p.renewaldate,
	p.updngflg,
	p.userid,
	p.bundlecd,
	p.sectorsortingcd,
	NVL2(p.userid, NVL(m.loginname, '---'), '�͍��m')
FROM
	profile p
INNER JOIN
	profileparam o
ON
	o.profileid = p.profileid
LEFT OUTER JOIN
	loginid_manage m
ON
	m.schoolcd = ?
AND
	m.loginid = p.userid
WHERE
	p.profilefid = ?

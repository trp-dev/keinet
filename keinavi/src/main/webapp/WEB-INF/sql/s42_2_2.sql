SELECT
	NVL(d.highlimit, -999) highlimit,
	NVL(d.lowlimit, -999) lowlimit,
	NVL(ra.numbers, -999) numbers,
	NVL(ra.compratio, -999) compratio
FROM
	deviationzone d
LEFT OUTER JOIN
	subdistrecord_a ra
ON
	ra.examyear = ?
AND
	ra.examcd = ?
AND
	ra.devzonecd = d.devzonecd
AND
	ra.subcd = ?
ORDER BY
	lowlimit DESC

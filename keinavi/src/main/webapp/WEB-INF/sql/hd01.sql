INSERT INTO
	loginid_manage
SELECT
	?, ?, 'システム管理者', ?, ?, '1', '1', '1', '1', '1', '1'
FROM
	DUAL
WHERE
	NOT EXISTS (
		SELECT
			1
		FROM
			loginid_manage
		WHERE
			schoolcd = ?
		AND
			manage_flg = '1')

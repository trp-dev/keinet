DELETE /*+ ORDERED */ FROM
	qrecord_c qc
WHERE
	(qc.examyear, qc.examcd, qc.bundlecd, qc.grade, qc.class, qc.subcd) IN (
		SELECT
			re.examyear,
			re.examcd,
			re.bundlecd,
			re.grade,
			re.class,
			re.combisubcd
		FROM
			v_subrecount re
		WHERE
			re.examyear = ?
		AND
			re.examcd = ?
		AND
			re.bundlecd = ?)

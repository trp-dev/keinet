SELECT
  ks.correct_ansrate_1 /* 高校正答率1 */
, ka.correct_ansrate_1 /* 全国正答率1 */
, ks.correct_ansrate_2 /* 高校正答率2 */
, ka.correct_ansrate_2 /* 全国正答率2 */
, ks.correct_ansrate_3 /* 高校正答率3 */
, ka.correct_ansrate_3 /* 全国正答率3 */
, ks.correct_ansrate_4 /* 高校正答率4 */
, ka.correct_ansrate_4 /* 全国正答率4 */
, ks.correct_ansrate_5 /* 高校正答率5 */
, ka.correct_ansrate_5 /* 全国正答率5 */
, ks.correct_ansrate_6 /* 高校正答率6 */
, ka.correct_ansrate_6 /* 全国正答率6 */
, ks.correct_ansrate_7 /* 高校正答率7 */
, ka.correct_ansrate_7 /* 全国正答率7 */
FROM
	KKG_ANSRT_EVALNUMS_A ka               /* 国語設問別条件別正答率・設問別評価人数（全国） */

LEFT JOIN KKG_ANSRT_EVALNUMS_S ks        /* 国語設問別条件別正答率・設問別評価人数（高校） */
ON
	ka.examyear = ks.examyear
AND
	ka.examcd = ks.examcd
AND
	ka.subcd = ks.subcd
AND
	ka.question_no = ks.question_no
AND
	ks.bundlecd = ?  /* 一括コード */
WHERE
	ka.examyear = ?  /* 対象年度 */
AND
	ka.examcd = ?    /* 模試コード */
AND
	ka.subcd = ?     /* 科目コード */

ORDER BY
ka.question_no       /* 設問番号 */
SELECT
	/* Fè±îñ}X^.QÁ±R[h */
	ei.engptcd,
	/* Fè±îñ}X^.QÁ±Zk¼ */
	ei.engptname_abbr,
	/* Fè±îñ}X^.x ètO */
	ei.levelflg,
	/* Fè±îñ}X^.ÀÑ */
	ei.sort AS ei_sort,
	/* Fè±Ú×îñ}X^.QÁ±xR[h */
	edi.engptlevelcd,
	/* Fè±Ú×îñ}X^.QÁ±xZk¼ */
	edi.engptlevelname_abbr,
	/* Fè±Ú×îñ}X^.ÀÑ */
	edi.sort AS edi_sort,
	/* CEFRîñ.bdeqxR[h */
	ci.cefrlevelcd,
	/* CEFRîñ.bdeqxZk¼ */
	CASE WHEN ci.cefrlevelcd = '99' THEN 'Èµ' ELSE ci.cerflevelname_abbr END AS cerflevelname_abbr,
	/* CEFRîñ.ÀÑ */
	ci.sort AS ci_sort
FROM
	/* Fè±îñ}X^TBL */
	engptinfo ei
	/* Fè±Ú×îñ}X^TBL */
	INNER JOIN engptdetailinfo edi
	ON
		edi.eventyear = ei.eventyear
	AND
		edi.examdiv = ei.examdiv
	AND
		edi.engptcd = ei.engptcd
	/* CEFRîñTBL */
	INNER JOIN (
		SELECT
			ci.eventyear,
			ci.examdiv,
			ci.cefrlevelcd,
			ci.cerflevelname_abbr,
			ci.sort
		FROM
			cefr_info ci
		UNION ALL
		SELECT
			ci.eventyear,
			ci.examdiv,
			'ZZ' AS cefrlevelcd,
			'l' AS cerflevelname_abbr,
			0 AS sort
		FROM
			cefr_info ci
		GROUP BY
			ci.eventyear,
			ci.examdiv) ci
		ON
			ci.eventyear = ei.eventyear
		AND
			ci.examdiv = ei.examdiv
WHERE
	ei.eventyear = ? /* '1' */
AND
	ei.examdiv = ? /* '2' */
ORDER BY
	ei_sort,
	edi_sort,
	ci_sort

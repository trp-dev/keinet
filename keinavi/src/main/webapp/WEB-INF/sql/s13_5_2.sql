SELECT
	KCS.EVAL_A_NUMBERS,
	KCS.EVAL_B_NUMBERS,
	KCS.EVAL_C_NUMBERS,
	KCS.EVAL_D_NUMBERS,
	KCS.EVAL_E_NUMBERS,
	KCS.EVAL_A_COMPRATIO,
	KCS.EVAL_B_COMPRATIO,
	KCS.EVAL_C_COMPRATIO,
	KCS.EVAL_D_COMPRATIO,
	KCS.EVAL_E_COMPRATIO,
	KCA.EVAL_A_NUMBERS,
	KCA.EVAL_B_NUMBERS,
	KCA.EVAL_C_NUMBERS,
	KCA.EVAL_D_NUMBERS,
	KCA.EVAL_E_NUMBERS,
	KCA.EVAL_A_COMPRATIO,
	KCA.EVAL_B_COMPRATIO,
	KCA.EVAL_C_COMPRATIO,
	KCA.EVAL_D_COMPRATIO,
	KCA.EVAL_E_COMPRATIO
FROM
	KOKUGODESC_COMPEVAL_S KCS
LEFT OUTER JOIN
	KOKUGODESC_COMPEVAL_A KCA
ON
	KCS.EXAMYEAR = KCA.EXAMYEAR
AND
	KCS.EXAMCD = KCA.EXAMCD
AND
	KCS.SUBCD = KCA.SUBCD
WHERE
	KCS.SUBCD = ?
AND
	KCS.BUNDLECD = ?
AND
	KCS.EXAMYEAR = ?
AND
	KCS.EXAMCD = ?
SELECT
	e.examyear,
	e.examcd,
	e.examname_abbr,
	e.inpledate,
	e.out_dataopendate
FROM
	examination e
WHERE
	e.examtypecd = ? /* 1 */
AND(
	(e.examyear = ? AND e.tergetgrade = ?) /* 2, 3 */
OR
	(e.examyear = ? AND e.tergetgrade = ?) /* 4, 5 */
OR
	(e.examyear = ? AND e.tergetgrade = ?) /* 6, 7 */
)
AND
	e.out_dataopendate <= ? /* 8 */
ORDER BY
	e.examyear DESC,
	e.out_dataopendate DESC

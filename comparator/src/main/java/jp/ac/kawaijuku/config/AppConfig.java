package jp.ac.kawaijuku.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class AppConfig {

    @Autowired
    private Environment env;

    /**
     * プロパティを取得します。
     * @param key
     * @return プロパティ値
     */
    public String getProp(String key) {
        return env.getProperty(key);
    }

    /**
     * 接頭語を指定して、プロパティを取得します。
     * @param key
     * @param prefix
     * @return プロパティ値
     */
    public String getProp(String key, String prefix) {
        return env.getProperty(prefix + key);
    }

    /**
     * キーの存在有無を返却します。
     * @param key
     * @return
     */
    public boolean containsKey(String key) {
        return env.containsProperty(key);
    }

}

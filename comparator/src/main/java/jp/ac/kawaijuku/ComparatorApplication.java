package jp.ac.kawaijuku;

import jp.ac.kawaijuku.comparator.JudgementComparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComparatorApplication implements CommandLineRunner {

    @Autowired
    private JudgementComparator jc;

    private static final Logger logger = LogManager.getLogger(ComparatorApplication.class);

    /**
     * メインメソッドです。
     *
     * @param args 引数
     */
    public static void main(String[] args) {
        SpringApplication.run(ComparatorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("App Start");
        String[] restricts = null;
        if (args.length > 1) {
            for (int i = 0; i < args.length; i++) {
                // コマンドライン引数から制限大学のリストを取得
                if (args[i].indexOf("restricts") >= 0) {
                    logger.info("Arguments: " + args[i]);
                    String[] tempArgs = args[i].split("=", -1);
                    restricts = tempArgs[1].split(",", -1);
                }
            }
        }
        // 初期化
        jc.initialinze();
        // 判定チェック
        jc.compare(restricts);
        logger.info("App End");
    }

}

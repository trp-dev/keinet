package jp.ac.kawaijuku.comparator;

import jp.ac.kawaijuku.comparator.HostFile.Target;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.data.eval.Evaluation;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 比較用判定結果クラスです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JCJudgedResult {

    /** 評価得点の無効値 */
    private static final String INVALID_SCORE = "9999";

    /** 評価偏差値の無効値 */
    private static final String INVALID_DEVIATION = "9999";

    /** 総合ポイントの無効値 */
    private static final String INVALID_POINT_TOTAL = "999";

    /** 結果値：評価得点 */
    private String score;

    /** 結果値：評価偏差値 */
    private String deviation;

    /** 結果値：総合ポイント */
    private String totalPoint;

    /** 結果値：センタ評価 */
    private String centerJudge;

    /** 結果値：二次評価 */
    private String secondJudge;

    /** 結果値：総合評価 */
    private String totalJudge;

    /**
     * 判定を実行しなかった場合のコンストラクタです。
     *
     * @param target 志望校データ
     */
    public JCJudgedResult(Target target) {
        this.score = target.getScore();
        this.deviation = target.getDeviation();
        this.totalPoint = target.getTotalPoint();
        this.centerJudge = target.getCenterJudge();
        this.secondJudge = target.getSecondJudge();
        this.totalJudge = target.getTotalJudge();
    }

    /**
     * 判定を実行した場合のコンストラクタです。
     *
     * @param isWritten 記述模試フラグ
     * @param compareTotalFlg 総合評価比較フラグ
     * @param result 判定結果
     * @param target 志望校データ
     */
    public JCJudgedResult(boolean isWritten, boolean compareTotalFlg, JudgedResult result, Target target) {
        this(target);
        overWrite(isWritten, compareTotalFlg, result, target);
    }

    /**
     * 結果値をゼロ詰めします。
     *
     * @param value 結果値
     * @param length ゼロ詰めした結果の文字列長
     * @return ゼロ詰めした結果値
     */
    private String fillZero(String value, int length) {
        return StringUtils.leftPad(value, length, '0');
    }

    /**
     * 判定結果を比較用に変換します。
     *
     * @param value 判定結果
     * @return 変換した判定結果
     */
    private String toJudgeLetter(String value) {
        return StringUtils.rightPad(value, 2, ' ');
    }

    /**
     * 判定結果で結果値を上書きします。
     *
     * @param isWritten 記述模試フラグ
     * @param compareTotalFlg 総合評価比較フラグ
     * @param result 判定結果
     * @param can 志望校データ
     */
    private void overWrite(boolean isWritten, boolean compareTotalFlg, JudgedResult result, Target can) {

        /* センタ評価を上書きする */
        if (!isWritten) {
            if (result.getC_scoreStr().trim().length() > 0) {
                this.score = fillZero(result.getC_scoreStr().trim(), INVALID_SCORE.length());
            } else {
                this.score = INVALID_SCORE;
            }
            this.centerJudge = toJudgeLetter(result.getCLetterJudgement());
        }

        /* 二次評価を上書きする */
        if (isWritten || result.getScheduleCd().equals(JudgementConstants.Univ.SCHEDULE_NML)) {
            if (result.getS_scoreStr().trim().length() > 0) {
                this.deviation =
                        fillZero(Integer.toString((int) (result.getS_score() * 10)), INVALID_DEVIATION.length());
            } else {
                this.deviation = INVALID_DEVIATION;
            }
            this.secondJudge = toJudgeLetter(result.getSLetterJudgement());
        }

        if (compareTotalFlg) {

            Judgement bean = result.getBean();

            Univ univ = bean.getUniv();

            Evaluation evalTotal;
            if (!isWritten) {
                /* ファイル値から二次ポイントを抽出する */
                int[] decoded = decodeJudgement(can.getSecondJudge().trim());
                int sJudgement = decoded[0];
                int sGhjudgement = decoded[1];
                double sScore = Double.parseDouble(can.getDeviation()) / 10;
                int sPoint = bean.calcSPoint(sJudgement, univ.getRank(), sScore);
                /* 総合ポイントを算出する */
                int tPoint =
                        bean.calcTPoint(bean.getCPoint(), sPoint, result.getDetail().getCAllotPointRate(), result
                                .getDetail().getSAllotPointRate(), bean.getCJudgement(), sJudgement);
                /* 総合評価を算出する */
                evalTotal =
                        Evaluation.total(univ.isDockingNeeded(), centerJudge, secondJudge,
                                bean.calcTJudgement(tPoint, bean.getCJudgement(), sJudgement),
                                bean.calcTGHJudgement(bean.getCGhJudgement(), sGhjudgement), tPoint);
            } else {
                /* ファイル値からセンタポイントを算出する */
                int[] decoded = decodeJudgement(can.getCenterJudge().trim());
                int cJudgement = decoded[0];
                int cGhjudgement = decoded[1];
                int cScore = Integer.parseInt(can.getScore());
                int cPoint =
                        bean.calcCPoint(cJudgement, univ.getCertainPoint(), univ.getBorderPoint(),
                                univ.getWarningPoint(), cScore);
                /* 総合ポイントを算出する */
                int tPoint =
                        bean.calcTPoint(cPoint, bean.getSPoint(), result.getDetail().getCAllotPointRate(), result
                                .getDetail().getSAllotPointRate(), cJudgement, bean.getSJudgement());
                /* 総合評価を算出する */
                evalTotal =
                        Evaluation.total(univ.isDockingNeeded(), centerJudge, secondJudge,
                                bean.calcTJudgement(tPoint, cJudgement, bean.getSJudgement()),
                                bean.calcTGHJudgement(cGhjudgement, bean.getSGhJudgement()), tPoint);
            }

            /* 総合ポイントを設定する */
            if (evalTotal.hasPoint()) {
                this.totalPoint = fillZero(evalTotal.getPointStr().trim(), INVALID_POINT_TOTAL.length());
            } else {
                this.totalPoint = INVALID_POINT_TOTAL;
            }

            /* 総合評価を設定する */
            this.totalJudge = toJudgeLetter(evalTotal.getLetter());
        }
    }

    /**
     * 判定結果文字列を数値に戻して返します。
     *
     * @param result 判定結果文字列
     * @return 判定結果
     */
    private int[] decodeJudgement(String result) {

        int[] judged = new int[2];

        if (result.length() == 0) {
            return new int[] { -1, 0 };
        } else if (result.length() == 1) {
            if (result.equals(JudgementConstants.Judge.JUDGE_STR_G)) {
                return new int[] { -1, JudgementConstants.Judge.JUDGE_G };
            } else {
                judged[1] = 0;
            }
        } else {
            String gh = result.substring(1, 2);
            if (JudgementConstants.Judge.JUDGE_STR_H.equals(gh)) {
                /* H付き判定 */
                judged[1] = JudgementConstants.Judge.JUDGE_H;
            } else if (JudgementConstants.Judge.JUDGE_STR_G.equals(gh)) {
                /* G付き判定 */
                judged[1] = JudgementConstants.Judge.JUDGE_G;
            } else if (JudgementConstants.Judge.JUDGE_STR_SHARP.equals(gh)) {
                /* #付き判定 */
                judged[1] = JudgementConstants.Judge.JUDGE_SHARP;
            }
        }

        String letter = result.substring(0, 1);
        if (letter.equals(JudgementConstants.JUDGE_STR_A)) {
            judged[0] = JudgementConstants.JUDGE_A;
        } else if (letter.equals(JudgementConstants.JUDGE_STR_B)) {
            judged[0] = JudgementConstants.JUDGE_B;
        } else if (letter.equals(JudgementConstants.JUDGE_STR_C)) {
            judged[0] = JudgementConstants.JUDGE_C;
        } else if (letter.equals(JudgementConstants.JUDGE_STR_D)) {
            judged[0] = JudgementConstants.JUDGE_D;
        } else if (letter.equals(JudgementConstants.JUDGE_STR_E)) {
            judged[0] = JudgementConstants.JUDGE_E;
        } else if (letter.equals(JudgementConstants.JUDGE_STR_X)) {
            judged[0] = JudgementConstants.JUDGE_NP;
        }

        return judged;
    }

    /**
     * 評価得点を返します。
     *
     * @return 評価得点
     */
    public String getScore() {
        return score;
    }

    /**
     * 評価偏差値を返します。
     *
     * @return 評価偏差値
     */
    public String getDeviation() {
        return deviation;
    }

    /**
     * 総合ポイントを返します。
     *
     * @return 評価偏差値
     */
    public String getTotalPoint() {
        return totalPoint;
    }

    /**
     * センタ評価を返します。
     *
     * @return センタ評価
     */
    public String getCenterJudge() {
        return centerJudge;
    }

    /**
     * 二次評価を返します。
     *
     * @return 二次評価
     */
    public String getSecondJudge() {
        return secondJudge;
    }

    /**
     * 総合評価を返します。
     *
     * @return 総合評価
     */
    public String getTotalJudge() {
        return totalJudge;
    }

}

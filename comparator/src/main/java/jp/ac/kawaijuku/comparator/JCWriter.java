package jp.ac.kawaijuku.comparator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * 判定結果ファイル書き込みクラスです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JCWriter {

    /** Logger */
    private static final Logger logger = LogManager.getLogger(JCWriter.class);

    /** BufferedWriter */
    private BufferedWriter writer;

    /**
     * コンストラクタです。
     */
    protected JCWriter() {
    }

    /**
     * コンストラクタです。
     *
     * @param encoding ファイルエンコーディング
     * @param path 出力ファイルパス
     * @param format 出力ファイル名フォーマット
     * @param ext 出力ファイル拡張子
     * @throws FileNotFoundException ファイルが見つからなかったとき
     * @throws UnsupportedEncodingException 未サポートエンコーディング例外
     */
    public JCWriter(String encoding, String path, String format, String ext) throws FileNotFoundException,
            UnsupportedEncodingException {
        open(encoding, path, format, ext);
    }

    /**
     * ファイルを開きます。
     *
     * @param encoding ファイルエンコーディング
     * @param path 出力ファイルパス
     * @param format 出力ファイル名フォーマット
     * @param ext 出力ファイル拡張子
     * @throws FileNotFoundException ファイルが見つからなかったとき
     * @throws UnsupportedEncodingException 未サポートエンコーディング例外
     */
    protected void open(String encoding, String path, String format, String ext) throws FileNotFoundException,
            UnsupportedEncodingException {
        File file = createNewFile(path, format, ext);
        writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding));
        logger.info(file.getAbsolutePath() + "へ書き込みをします");
    }

    /**
     * 文字列を出力します。
     *
     * @param string 文字列
     * @throws IOException IO例外
     */
    public void write(String string) throws IOException {
        writer.write(string);
    }

    /**
     * 改行を出力します。
     *
     * @throws IOException IO例外
     */
    public void line() throws IOException {
        writer.write("\n");
        writer.flush();
    }

    /**
     * 出力ファイルを生成します。
     *
     * @param path 出力ファイルパス
     * @param format 出力ファイル名フォーマット
     * @param ext 出力ファイル拡張子
     * @return 出力ファイル
     */
    protected File createNewFile(String path, String format, String ext) {
        StringBuffer buff = new StringBuffer();
        buff.append(new SimpleDateFormat(format).format(new Date()));
        buff.append(".");
        buff.append(ext);
        return new File(path, buff.toString());
    }

    /**
     * ファイルを閉じます。
     */
    public void close() {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException e) {
            logger.error("ファイルのcloseに失敗しました。", e);
        }
    }

}

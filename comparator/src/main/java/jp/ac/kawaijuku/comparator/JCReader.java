package jp.ac.kawaijuku.comparator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * 比較用ホストファイル読み込みクラスです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JCReader {

    /** Logger */
    private static final Logger logger = LogManager.getLogger(JCReader.class);

    /** BufferedReader */
    private final BufferedReader reader;

    /** 現在の行番号 */
    private int lineNo;

    /**
     * コンストラクタです。
     *
     * @param encoding ファイルエンコーディング
     * @param path 入力ファイルパス
     * @throws FileNotFoundException ファイルが見つからなかったとき
     * @throws UnsupportedEncodingException 未サポートエンコーディング例外
     */
    public JCReader(String encoding, String path) throws FileNotFoundException, UnsupportedEncodingException {
        File file = new File(path);
        reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        logger.info(file.getAbsolutePath() + "から読み込みします");
    }

    /**
     * 次行を読み出します。
     *
     * @return 行文字列
     * @throws IOException IO例外
     */
    public String getNextLine() throws IOException {
        String line = reader.readLine();
        if (line != null) {
            lineNo++;
        }
        return line;
    }

    /**
     * 現在の行番号を返します。
     *
     * @return 現在の行番号
     */
    public int getLineNo() {
        return lineNo;
    }

    /**
     * ファイルを閉じます。
     */
    public void close() {
        try {
            reader.close();
        } catch (IOException e) {
            logger.error("ファイルのcloseに失敗しました。", e);
        }
    }

}

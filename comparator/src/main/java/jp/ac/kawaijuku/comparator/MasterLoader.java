package jp.ac.kawaijuku.comparator;

import java.util.Map;

import jp.ac.kawaijuku.config.AppConfig;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;

/**
 *
 * マスタローダIFです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface MasterLoader {

    /**
     * 数学①の全国成績を取得します。
     *
     * @param config 設定ファイル情報
     * @param exam 対象模試
     * @return {@link SubRecordABean}
     * @throws Exception 例外
     */
    SubRecordAllBean findAllMathOneBean(AppConfig config, Examination exam) throws Exception;

    /**
     * 大学マスタをロードします。
     *
     * @param config 設定ファイル情報
     * @param restricts マスタ展開の対象大学コード配列
     * @return {@link FactoryManager}
     * @throws Exception 例外
     */
    FactoryManager loadUnivMaster(AppConfig config, String[] restricts) throws Exception;

    /**
     *  センター得点換算表をロードします。
     * @param config 設定ファイル情報
     * @param exam 対象模試
     * @return {@link CvsScoreBean}
     * @throws Exception 例外
     */
    Map<String, Map<String, int[]>> loadCvsScore(AppConfig config, Examination exam) throws Exception;
}

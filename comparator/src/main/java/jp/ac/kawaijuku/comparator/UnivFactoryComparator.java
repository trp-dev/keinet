package jp.ac.kawaijuku.comparator;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.fujitsu.keinet.mst.db.factory.UnivFactoryDB;

/**
*
* DB用大学マスタ展開処理
*
*
* @author TOTEC)YAMADA.Tomohisa
* @author TOTEC)KAWAI.Yoshimoto
*
*/
public class UnivFactoryComparator extends UnivFactoryDB {

    public UnivFactoryComparator(Connection con) throws Exception {
        super(con);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Map loadSubjectInfo() throws Exception {
        Map datas = new HashMap();
        datas.put(ExamSubjectBean.class.getName(), deployExamSubject());

        return datas;
    }
}

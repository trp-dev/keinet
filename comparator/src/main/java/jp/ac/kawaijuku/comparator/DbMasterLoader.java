package jp.ac.kawaijuku.comparator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.ac.kawaijuku.config.AppConfig;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;
import jp.co.fj.kawaijuku.judgement.factory.UnivFactory;
import jp.co.fj.kawaijuku.judgement.util.JudgementProperties;

import org.apache.commons.dbutils.DbUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * DB用の {@link MasterLoader} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DbMasterLoader implements MasterLoader {

    /** Logger */
    private static final Logger logger = LogManager.getLogger(DbMasterLoader.class);

    /**
     * {@inheritDoc}
     */
    public SubRecordAllBean findAllMathOneBean(AppConfig config, Examination exam) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = getConnection(config);
            ps = con.prepareStatement("SELECT SUBCD, AVGPNT, STDDEVIATION FROM SUBRECORD_A " + "WHERE EXAMYEAR = ? AND EXAMCD = ? AND SUBCD = ? AND STUDENTDIV = '0'");
            ps.setString(1, exam.getExamYear());
            ps.setString(2, exam.getExamCd());
            ps.setString(3, JudgementProperties.getInstance().getSubRepcd("mark.subcd.mathOne"));
            rs = ps.executeQuery();
            if (rs.next()) {
                SubRecordAllBean bean = new SubRecordAllBean();
                bean.setSubCd(rs.getString(1));
                bean.setAvgPnt(rs.getDouble(2));
                bean.setStdDeviation(rs.getDouble(3));
                return bean;
            } else {
                throw new RuntimeException("数学①の全国成績が見つかりませんでした。");
            }
        } finally {
            DbUtils.closeQuietly(con, ps, rs);
        }
    }

    /**
     * {@inheritDoc}
     */
    public FactoryManager loadUnivMaster(AppConfig config, String[] restricts) throws Exception {
        Connection con = null;
        try {
            con = getConnection(config);

            logger.info("大学マスタの展開：開始");
            logger.info("大学マスタをDBからロードします。");

            UnivFactory factory = new UnivFactoryComparator(con);

            if (restricts != null) {
                logger.info(restricts.length + "大学がマスタ展開用に指定されています。");
                for (int i = 0; i < restricts.length; i++) {
                    factory.restrict(restricts[i]);
                }
            }

            FactoryManager manager = new FactoryManager(factory);
            manager.load();

            manager.loadSubjectInfo();
            logger.info("大学マスタの展開：完了");

            @SuppressWarnings("unchecked")
            List<Univ> datas = manager.getDatas();
            if (datas.isEmpty()) {
                throw new RuntimeException("大学展開数は0です。");
            }

            logger.info("大学展開数は" + datas.size() + "です。");

            return manager;
        } finally {
            DbUtils.closeQuietly(con);
        }
    }

    @Override
    public Map<String, Map<String, int[]>> loadCvsScore(AppConfig config, Examination exam) throws Exception {

        final Map<String, Map<String, int[]>> map = new HashMap<String, Map<String, int[]>>();

        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = getConnection(config)){
            // クエリを設定
            ps = con.prepareStatement("SELECT EXAMCD, SUBCD, RAWSCORE, CVSSCORE FROM CENTERCVSSCORE WHERE EXAMYEAR=? ORDER BY RAWSCORE DESC");
            // パラメータを設定
            ps.setString(1, exam.getExamYear().substring(2));
            // SQL実行
            rs = ps.executeQuery();
            while (rs.next()) {
                // 値を取得
                int rawScore = rs.getInt("RAWSCORE");
                int cvsScore = rs.getInt("CVSSCORE");
                String examCd = rs.getString("EXAMCD");
                String subCd = rs.getString("SUBCD");
                // 模試コードをキーにMapを作成
                Map<String, int[]> subMap = map.get(examCd);
                if (subMap == null) {
                    subMap = new HashMap<String, int[]>();
                    map.put(examCd, subMap);
                }

                // 模試コード単位に科目コードをキーにMapを作成
                int[] table = subMap.get(subCd);
                if (table == null) {
                    if (rawScore <= 50) {
                        table = new int[51];
                    } else if (rawScore <= 100) {
                        table = new int[101];
                    } else {
                        table = new int[201];
                    }
                    subMap.put(subCd, table);
                }

                // 模試コード、科目コード単位に得点換算値を設定
                table[rawScore] = cvsScore;

            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return map;
    }

    /**
     * Connectionを取得します。
     *
     * @param config 設定ファイル情報
     * @return {@link Connection}
     * @throws Exception 例外
     */
    private Connection getConnection(AppConfig config) throws Exception {
        Class.forName(config.getProp("DBDriver"));
        return DriverManager.getConnection(config.getProp("DBURL"), config.getProp("DBConnUserID"), config.getProp("DBConnUserPass"));
    }

}

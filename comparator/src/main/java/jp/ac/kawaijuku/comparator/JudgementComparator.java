package jp.ac.kawaijuku.comparator;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.ac.kawaijuku.comparator.HostFile.Record;
import jp.ac.kawaijuku.comparator.HostFile.Target;
import jp.ac.kawaijuku.config.AppConfig;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.JudgementExecutor;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.beans.score.ExtScore;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * ホスト判定とKei-Navi判定ロジックによる判定の比較バッチです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Component
public class JudgementComparator {

    /** Logger */
    private static final Logger logger = LogManager.getLogger(JudgementComparator.class);

    /** マーク模試コード配列 */
    private static final String[] MAKR_EXAM_CD_ARRAY = new String[] { JudgementConstants.Exam.Code.SECONDGRADE_MARK, JudgementConstants.Exam.Code.MARK1, JudgementConstants.Exam.Code.MARK2, JudgementConstants.Exam.Code.MARK3,
            JudgementConstants.Exam.Code.CENTERPRE, JudgementConstants.Exam.Code.CENTERRESEARCH };

    /** 設定ファイル */
    @Autowired
    private AppConfig config;

    /** 読み込みファイル */
    private JCReader fileReader;

    /** 書き込みファイル */
    private JCWriter fileWriter;

    /** 比較ログファイル */
    private JCLogWriter logWriter;

    /** マスタローダ */
    private MasterLoader loader;

    /** 換算得点マップ */
    private Map<String, Map<String, int[]>> cvsScoreMap;

    /**
     * MasterLoaderを生成します。
     *
     * @return {@link MasterLoader}
     */
    protected MasterLoader createMasterLoader() {
        return new DbMasterLoader();
    }

    /**
     * 初期化
     *
     * @throws Exception
     */
    public void initialinze() throws Exception {

        this.fileReader = new JCReader(config.getProp("ENCODE_IN"), config.getProp("URL_FILE_IN"));

        this.fileWriter = new JCWriter(config.getProp("ENCODE_OUT"), config.getProp("URL_DIR_OUT"), config.getProp("FORMAT_NAME_FILE"), config.getProp("EXT_FILE_OUT"));

        this.logWriter = new JCLogWriter(config.getProp("ENCODE_LOG"), config.getProp("URL_FILE_LOG"), config.getProp("OUTPUT_NAME_FILE"), config.getProp("EXT_FILE_LOG"));

        if (config.containsKey("IS_APPEND_INFILENAME") && Boolean.valueOf(config.getProp("IS_APPEND_INFILENAME")).booleanValue()) {
            logWriter.appendReadingFileName(new File(config.getProp("URL_FILE_IN")).getName());
        }

        if ("ON".equals(config.getProp("COMPARE_LOG"))) {
            logWriter.activate();
        }

        this.loader = createMasterLoader();


    }

    /**
     * 判定結果を比較します。
     *
     * @throws Exception 例外
     */
    public void compare(String[] restricts) throws Exception {

        logger.info("判定チェック：開始");

        /* ファイルの一行目の対象模試を取得する */
        Examination exam = readExamination();

        // センター得点換算表を読み込む
        this.cvsScoreMap = loader.loadCvsScore(config, exam);

        /* 記述模試フラグを設定する */
        boolean isWritten = !ArrayUtils.contains(MAKR_EXAM_CD_ARRAY, exam.getExamCd());

        /* 模試種類をログに出力する */
        logger.info(!isWritten ? "マーク模試で比較を行います" : "記述模試で比較を行います");

        /* 対象模試がマーク模試の場合は、数学①の科目別成績（全国）を取得する */
        SubRecordAllBean allMathOneBean = !isWritten ? loader.findAllMathOneBean(config, exam) : null;

        /*
         * 総合評価比較フラグを設定する。
         * 記述模試とセンタープレ・リサーチのみ総合評価を比較する。
         */
        boolean compareTotalFlg = isWritten || exam.getExamCd().equals(JudgementConstants.Exam.Code.CENTERPRE) || exam.getExamCd().equals(JudgementConstants.Exam.Code.CENTERRESEARCH);

        /* 大学マスタをロードする */
        FactoryManager manager = loader.loadUnivMaster(config, restricts);

        logger.info("判定結果書き込み：開始");

        String line;
        while ((line = fileReader.getNextLine()) != null) {
            HostFile hostFile = new HostFile(line, fileReader.getLineNo(), config.getProp("ENCODE_IN"));
            compareLine(manager, allMathOneBean, isWritten, compareTotalFlg, hostFile);
        }

        logger.info("判定結果書き込み：完了");
        logger.info("判定チェック：完了");
    }

    /**
     * ホストファイル1行の比較処理です。
     *
     * @param manager {@link FactoryManager}
     * @param allMathOneBean 数学①の全国成績
     * @param isWritten 記述模試フラグ
     * @param compareTotalFlg 総合評価比較フラグ
     * @param hostFile ホストファイル1行分のデータ
     * @throws Exception 例外
     */
    private void compareLine(FactoryManager manager, SubRecordAllBean allMathOneBean, boolean isWritten, boolean compareTotalFlg, HostFile hostFile) throws Exception {

        /* そのまま書き出す部分を出力する */
        fileWriter.write(hostFile.getNonSense());

        /* 成績インスタンスを生成する */
        Score score = createScore(hostFile, isWritten, allMathOneBean, manager.getExamSubject());

        Target[] targets = hostFile.get9Targets();
        for (int i = 0; i < targets.length; i++) {
            Target target = targets[i];

            /* 判定対象となる大学情報をメモリから取得する */
            Univ univ = manager.searchUniv(target.getUnivcd(), target.getFacultycd(), target.getDeptcd());

            JCJudgedResult nice;
            if (univ == null) {
                /* 大学が存在しないか無効値の場合 */
                nice = new JCJudgedResult(target);
                String univKey = target.getUnivcd() + target.getFacultycd() + target.getDeptcd();
                if (!StringUtils.containsOnly(univKey, "9")) {
                    logger.warn("大学マスタの取得に失敗しました。" + univKey);
                }
            } else {
                // 共通テストリサーチでの一般私大判定の場合、成績データをクリアする
                boolean isCenPrivate = isCenterPrivate(score, univ);
                Score judgeScore = score;
                if (isCenPrivate) {
                    judgeScore = new Score(null, null, null, null);
                    judgeScore.setEngSSScoreDatas(new ArrayList<EngSSScoreData>());
                }
                /* 判定する */
                JudgementExecutor ex = new JudgementExecutor(univ, judgeScore);
                ex.execute();
                JudgedResult result = new JudgedResult(ex.getJudgement());
                result.execute();
                nice = new JCJudgedResult(isWritten, compareTotalFlg, result, target);
            }

            /* 判定結果を出力する */
            fileWriter.write(target.getUnivcd());
            fileWriter.write(target.getFacultycd());
            fileWriter.write(target.getDeptcd());
            fileWriter.write(target.getUniv5Code());
            fileWriter.write(target.getWishRank());
            fileWriter.write(nice.getScore());
            fileWriter.write(nice.getDeviation());
            fileWriter.write(nice.getTotalPoint());
            fileWriter.write(nice.getCenterJudge());
            fileWriter.write(nice.getSecondJudge());
            fileWriter.write(nice.getTotalJudge());
            fileWriter.write(target.getTotalCandidateCount());
            fileWriter.write(target.getTotalRank());

            /* 比較ログファイルを出力する */
            if (univ != null) {
                logWriter.evaluate(nice, target);
            }
        }

        fileWriter.line();
    }

    /**
     * 共通テストリサーチでの一般私大判定かどうか判定します。
     *
     * @param score 成績データ
     * @param univ 大学キインスタンス
     * @return
     */
    private boolean isCenterPrivate(Score score, Univ univ) {

        return score.getCExam() != null && score.getCExam().isCenterResearch() && score.getSExam() == null && JudgementConstants.Univ.SCHEDULE_NML.equals(univ.getSchedule());
    }

    /**
     * 得点インスタンスを生成します。
     *
     * @param hostFile ホストデータ
     * @param isWritten 記述系フラグ
     * @param allMathOneBean 数学①の全国成績
     * @param examSubject 模試科目情報
     * @return {@link Score}
     * @throws Exception 例外
     */
    private Score createScore(HostFile hostFile, boolean isWritten, SubRecordAllBean allMathOneBean, Map<String, ExamSubjectBean> examSubject) throws Exception {

        Examination exam = new Examination(hostFile.getExamYear(), hostFile.getExamCode());

        Score score;
        if (!isWritten) {
            score = new ExtScore(exam, createSubrecords(hostFile, isWritten, examSubject), null, null, allMathOneBean);
        } else {
            score = new ExtScore(null, null, exam, createSubrecords(hostFile, isWritten, examSubject), allMathOneBean);
        }

        score.execute();

        return score;
    }

    /**
     * 科目別成績情報を判定用に変換します。
     *
     * @param hostFile ホストファイル
     * @param isWritten 記述系フラグ
     * @return 科目別成績リスト
     */
    private List<SubjectRecord> createSubrecords(HostFile hostFile, boolean isWritten, Map<String, ExamSubjectBean> examSubject) {
        Examination exam = new Examination(hostFile.getExamYear(), hostFile.getExamCode());

        Record[] records22 = hostFile.get22Records();
        List<SubjectRecord> list = new ArrayList<SubjectRecord>(records22.length);
        for (int i = 0; i < records22.length; i++) {
            Record record = records22[i];
            if ("9999".equals(record.getSubcd())) {
                /* 無効レコードは飛ばす */
                continue;
            }
            SubjectRecord rec = new SubjectRecord();
            rec.setSubCd(record.getSubcd().trim());
            rec.setScore(record.getScore().trim());
            rec.setCvsScore(record.getCvsScore().trim());
            rec.setCDeviation(Double.toString(Double.parseDouble(record.getNationalDeviation().trim()) / 10));
            rec.setScope(record.getScope());
            rec.setSubjectAllotPnt(getSubjectAllotPnt(isWritten ? "02" : "01", rec.getSubCd(), examSubject));
            list.add(rec);
        }

        /* センター・リサーチのみ、未受験の国語構成科目（現代文・古文・漢文）の0点成績を生成する */
        if (JudgementConstants.Exam.Code.CENTERRESEARCH.equals(hostFile.getExamCode())) {
            Set<String> set = new HashSet<String>();
            set.add(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE);
            set.add(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD);
            set.add(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE);
            for (Iterator<SubjectRecord> ite = list.iterator(); ite.hasNext();) {
                SubjectRecord rec = (SubjectRecord) ite.next();
                set.remove(rec.getSubCd());
                if (set.isEmpty()) {
                    break;
                }
            }
            if (set.size() < 3) {
                for (Iterator<String> ite = set.iterator(); ite.hasNext();) {
                    String subCd = (String) ite.next();
                    SubjectRecord rec = new SubjectRecord();
                    rec.setSubCd(subCd);
                    rec.setScore("0");
                    rec.setScope(Integer.toString(JudgementConstants.Score.ANSWER_NO_NA));
                    rec.setSubjectAllotPnt(getSubjectAllotPnt(isWritten ? "02" : "01", subCd, examSubject));
                    list.add(rec);
                }
            }
        }

        // センター得点換算を行う
        if (!isWritten) {
            setCvsScore(exam, list);
        }

        return list;
    }

    /**
     * 科目成績に換算得点をセットします。
     *
     * @param exam 対象模試
     * @param list 科目成績リスト
     */
    private void setCvsScore(Examination exam, List<SubjectRecord> list) {

        Map<String, int[]> map = this.cvsScoreMap.get(exam.getExamCd());
        if (map == null) {
            throw new RuntimeException(String.format("センター得点換算表データがありません。[模試コード=%s]", exam.getExamCd()));
        }

        for (SubjectRecord rec : list) {
            String subCd = rec.getSubCd();
            if (subCd.compareTo("7000") < 0 && !subCd.equals(SUBCODE_CENTER_DEFAULT_JKIJUTSU)) {
                int[] table = map.get(subCd);
                if (table != null) {
                    rec.setCvsScore(Integer.toString(table[Integer.parseInt(rec.getScore())]));
                }
            }
        }
    }

    /**
     * 模試科目マスタから教科配点を取得します。
     *
     * @param examTypeCd 模試種別
     * @param subCd 科目コード
     * @param examSubject 模試科目マスタ情報
     * @return
     */
    private String getSubjectAllotPnt(String examTypeCd, String subCd, Map<String, ExamSubjectBean> examSubject) {
        String result = String.valueOf(Detail.ALLOT_NA);
        if (examSubject.containsKey(examTypeCd + subCd)) {
            result = String.valueOf(examSubject.get(examTypeCd + subCd).getSubAllotPnt());
        }
        return result;
    }

    /**
     * 一行目に定義されている模試情報を取得します。
     *
     * @return {@link Examination}
     * @throws IOException IO例外
     */
    private Examination readExamination() throws IOException {
        JCReader reader = null;
        try {
            reader = new JCReader(config.getProp("ENCODE_IN"), config.getProp("URL_FILE_IN"));
            String line = reader.getNextLine();
            if (line != null) {
                HostFile hostFile = new HostFile(line, reader.getLineNo(), config.getProp("ENCODE_IN"));
                return new Examination(hostFile.getExamYear(), hostFile.getExamCode());
            } else {
                throw new RuntimeException("模試情報が見つかりませんでした。");
            }
        } finally {
            reader.close();
        }
    }

    /**
     * ファイルを閉じます。
     */
    void close() {
        try {
            fileReader.close();
        } finally {
            try {
                fileWriter.close();
            } finally {
                logWriter.close();
            }
        }
    }

}

package jp.ac.kawaijuku.comparator;

import java.io.File;
import java.io.IOException;

import jp.ac.kawaijuku.comparator.HostFile.Target;

/**
 *
 * 比較ログ書き込みクラスです。
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JCLogWriter extends JCWriter {

    /** ヘッダ文字列：行番号 */
    private static final String HEADER_ROW = "行番号";

    /** ヘッダ文字列：志望順位 */
    private static final String HEADER_WISHNO = "志望順位";

    /** ヘッダ文字列：大学コード */
    private static final String HEADER_UNIVCD = "大学コード";

    /** ヘッダ文字列：学部コード */
    private static final String HEADER_FACULTYCD = "学部コード";

    /** ヘッダ文字列：学科コード */
    private static final String HEADER_DEPTCD = "学科コード";

    /** ヘッダ文字列：ホストセンタ評価 */
    private static final String HEADER_HOST_CENTER_JUDGE = "ホストセンタ評価";

    /** ヘッダ文字列：Kei-Naviセンタ評価 */
    private static final String HEADER_KEINAVI_CENTER_JUDGE = "Kei-Naviセンタ評価";

    /** ヘッダ文字列：ホスト二次評価 */
    private static final String HEADER_HOST_SECOND_JUDGE = "ホスト二次評価";

    /** ヘッダ文字列：Kei-Navi二次評価 */
    private static final String HEADER_KEINAVI_SECOND_JUDGE = "Kei-Navi二次評価";

    /** ヘッダ文字列：ホスト総合評価 */
    private static final String HEADER_HOST_TOTAL_JUDGE = "ホスト総合評価";

    /** ヘッダ文字列：Kei-Navi総合評価 */
    private static final String HEADER_KEINAVI_TOTAL_JUDGE = "Kei-Navi総合評価";

    /** ヘッダ文字列：ホスト評価得点 */
    private static final String HEADER_HOST_SCORE = "ホスト評価得点";

    /** ヘッダ文字列：Kei-Navi評価得点 */
    private static final String HEADER_KEINAVI_SCORE = "Kei-Navi評価得点";

    /** ヘッダ文字列：ホスト評価偏差値 */
    private static final String HEADER_HOST_DEVIATION = "ホスト評価偏差値";

    /** ヘッダ文字列：Kei-Navi評価偏差値 */
    private static final String HEADER_KEINAVI_DEVIATION = "Kei-Navi評価偏差値";

    /** ヘッダ文字列：ホスト総合ポイント */
    private static final String HEADER_HOST_TOTAL_POINT = "ホスト総合ポイント";

    /** ヘッダ文字列：Kei-Navi総合ポイント */
    private static final String HEADER_KEINAVI_TOTAL_POINT = "Kei-Navi総合ポイント";

    /** ヘッダ文字列：備考 */
    private static final String HEADER_REMARKS = "備考";

    /** ログ出力フラグ */
    private boolean activated;

    /** ファイルエンコーディング */
    private final String encoding;

    /** 出力ファイルパス */
    private final String path;

    /** 出力ファイル名フォーマット */
    private final String format;

    /** 出力ファイル拡張子 */
    private final String ext;

    /** 読み込み元ファイル名 */
    private String readingFile;

    /**
     * コンストラクタです。
     *
     * @param encoding ファイルエンコーディング
     * @param path 出力ファイルパス
     * @param format 出力ファイル名フォーマット
     * @param ext 出力ファイル拡張子
     */
    public JCLogWriter(String encoding, String path, String format, String ext) {
        this.encoding = encoding;
        this.path = path;
        this.format = format;
        this.ext = ext;
    }

    /**
     * 読み込み元ファイル名をセットします。
     *
     * @param readingFile 読み込み元ファイル名
     */
    public void appendReadingFileName(String readingFile) {
        this.readingFile = readingFile;
    }

    /**
     * 出力ファイルを生成します。
     *
     * @param path 出力ファイルパス
     * @param format 出力ファイル名フォーマット
     * @param ext 出力ファイル拡張子
     * @return 出力ファイル
     */
    protected File createNewFile(String path, String format, String ext) {
        StringBuffer buff = new StringBuffer();
        buff.append(format);
        if (readingFile != null) {
            buff.append("_" + getBaseName(readingFile));
        }
        buff.append(".");
        buff.append(ext);
        return new File(path, buff.toString());
    }

    /**
     * ファイル名から拡張子を取り除いた名前を返します。
     *
     * @param name ファイル名
     * @return ファイル名から拡張子を取り除いた名前
     */
    private String getBaseName(String name) {
        int point = name.lastIndexOf(".");
        if (point > -1) {
            return name.substring(0, point);
        }
        return name;
    }

    /**
     * 文字列を出力します。
     *
     * @param string 文字列
     * @throws IOException IO例外
     */
    public void write(String string) throws IOException {
        if (activated) {
            super.write(string);
            super.write(",");
        }
    }

    /**
     * 改行を出力します。
     *
     * @throws IOException IO例外
     */
    public void line() throws IOException {
        if (activated) {
            super.line();
        }
    }

    /**
     * ログ出力を有効化します。
     *
     * @throws IOException IO例外
     */
    public void activate() throws IOException {
        activated = true;
        open(encoding, path, format, ext);
        createHeader();
    }

    /**
     * ヘッダを出力します。
     *
     * @throws IOException IO例外
     */
    private void createHeader() throws IOException {
        write(HEADER_ROW);
        write(HEADER_WISHNO);
        write(HEADER_UNIVCD);
        write(HEADER_FACULTYCD);
        write(HEADER_DEPTCD);
        write(HEADER_HOST_CENTER_JUDGE);
        write(HEADER_KEINAVI_CENTER_JUDGE);
        write(HEADER_HOST_SECOND_JUDGE);
        write(HEADER_KEINAVI_SECOND_JUDGE);
        write(HEADER_HOST_TOTAL_JUDGE);
        write(HEADER_KEINAVI_TOTAL_JUDGE);
        write(HEADER_HOST_SCORE);
        write(HEADER_KEINAVI_SCORE);
        write(HEADER_HOST_DEVIATION);
        write(HEADER_KEINAVI_DEVIATION);
        write(HEADER_HOST_TOTAL_POINT);
        write(HEADER_KEINAVI_TOTAL_POINT);
        write(HEADER_REMARKS);
        line();
    }

    /**
     * 比較結果を書き出します。
     *
     * @param nice 判定結果
     * @param target 志望校データ
     * @throws IOException IO例外
     */
    public void evaluate(JCJudgedResult nice, Target target) throws IOException {

        /* 備考を設定する */
        StringBuffer remarks = new StringBuffer();

        if (!nice.getCenterJudge().equals(target.getCenterJudge())) {
            /* センター判定が異なる */
            remarks.append(HEADER_KEINAVI_CENTER_JUDGE);
            remarks.append(" | ");
        }

        if (!nice.getSecondJudge().equals(target.getSecondJudge())) {
            /* 二次判定が異なる */
            remarks.append(HEADER_KEINAVI_SECOND_JUDGE);
            remarks.append(" | ");
        }

        if (!nice.getTotalJudge().equals(target.getTotalJudge())) {
            /* 総合判定が異なる */
            remarks.append(HEADER_KEINAVI_TOTAL_JUDGE);
            remarks.append(" | ");
        }

        if (!nice.getScore().equals(target.getScore())) {
            /* センター得点が異なる */
            remarks.append(HEADER_KEINAVI_SCORE);
            remarks.append(" | ");
        }

        if (!nice.getDeviation().equals(target.getDeviation())) {
            /* 二次得点が異なる */
            remarks.append(HEADER_KEINAVI_DEVIATION);
            remarks.append(" | ");
        }

        if (!nice.getTotalPoint().equals(target.getTotalPoint())) {
            /* 総合ポイントが異なる */
            remarks.append(HEADER_KEINAVI_TOTAL_POINT);
            remarks.append(" | ");
        }

        if (remarks.length() > 0) {
            /* 相違があるならログ出力する */
            write(Integer.toString(target.getLineNo()));
            write(target.getWishRank());
            write(target.getUnivcd());
            write(target.getFacultycd());
            write(target.getDeptcd());
            write(target.getCenterJudge());
            write(nice.getCenterJudge());
            write(target.getSecondJudge());
            write(nice.getSecondJudge());
            write(target.getTotalJudge());
            write(nice.getTotalJudge());
            write(target.getScore());
            write(nice.getScore());
            write(target.getDeviation());
            write(nice.getDeviation());
            write(target.getTotalPoint());
            write(nice.getTotalPoint());
            write(remarks.toString());
            line();
        }
    }

}

package jp.ac.kawaijuku.comparator;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.StringUtils;

/**
 *
 * ホストファイル一行分データを保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class HostFile {

    /** 基本情報レコード長定義 */
    private static final int[] BASIC_LEN = new int[] {
    /* 模試年度 */
    4,
    /* 模試コード */
    2,
    /* 解答用紙番号 */
    6,
    /* 在卒校コード */
    5,
    /* 一括コード */
    5,
    /* 学年 */
    2,
    /* クラス */
    2,
    /* クラス番号 */
    5,
    /* 受講学年 */
    2,
    /* カナ氏名 */
    14,
    /* 漢字氏名 */
    20,
    /* 性別 */
    1,
    /* 生年月日 */
    6,
    /* 電話番号 */
    11,
    /* 顧客番号 */
    11,
    /* 学籍番号 */
    6,
    /* 所属地区 */
    1,
    /* 所属校舎 */
    3,
    /* 申し込み区分 */
    1,
    /* プライバシー保護フラグ */
    1 };

    /** 科目成績レコード長定義 */
    private static final int[] SUBREC_LEN = new int[] {
    /* 科目コード */
    4,
    /* 得点 */
    3,
    /* 換算得点 */
    3,
    /* 校内順位 */
    4,
    /* 全国順位 */
    8,
    /* 校内偏差値 */
    4,
    /* 全国偏差値/到達レベル */
    4,
    /* 範囲区分 */
    1,
    /* 学力レベル */
    1 };

    private static final int QRECORD_LEN = 2840;

    /** 志望校レコード長定義 */
    private static final int[] CANDIDATE_LEN = new int[] {
    /* 大学コード */
    4,
    /* 学部コード */
    2,
    /* 学科コード */
    4,
    /* 志望校コード5桁 */
    5,
    /* 志望順位 */
    1,
    /* 評価得点 */
    4,
    /* 評価偏差値 */
    4,
    /* 総合評価ポイント */
    3,
    /* センター評価 */
    2,
    /* 2次評価 */
    2,
    /* 総合評価 */
    2,
    /* 総志望者数 */
    8,
    /* 総志望者内順位 */
    8, };

    /** 基本情報配列 */
    private final String[] basic = new String[BASIC_LEN.length];

    /** 科目成績データ配列 */
    private final Record[] records = new Record[22];

    /** 志望校データ配列 */
    private final Target[] candidates = new Target[9];

    /** 行番号 */
    private final int lineNo;

    /**
     * コンストラクタです。
     *
     * @param line 行データ
     * @param lineNo 行番号
     * @param encoding エンコーディング
     * @throws UnsupportedEncodingException 未サポートエンコーディング例外
     */
    public HostFile(String line, int lineNo, String encoding) throws UnsupportedEncodingException {

        this.lineNo = lineNo;

        byte[] bytes = line.getBytes("Windows-31J");

        int start = 0;
        for (int i = 0; i < BASIC_LEN.length; i++) {
            basic[i] = new String(bytes, start, BASIC_LEN[i], encoding);
            start += BASIC_LEN[i];
        }

        for (int i = 0; i < records.length; i++) {
            String[] data = new String[SUBREC_LEN.length];
            for (int j = 0; j < SUBREC_LEN.length; j++) {
                data[j] = new String(bytes, start, SUBREC_LEN[j]);
                start += SUBREC_LEN[j];
            }
            records[i] = new Record(data);
        }

        start += QRECORD_LEN;

        for (int i = 0; i < candidates.length; i++) {
            String[] data = new String[CANDIDATE_LEN.length];
            for (int j = 0; j < CANDIDATE_LEN.length; j++) {
                data[j] = new String(bytes, start, CANDIDATE_LEN[j]);
                start += CANDIDATE_LEN[j];
            }
            candidates[i] = new Target(data);
        }
    }

    /**
     * 行番号を返します。
     *
     * @return 行番号
     */
    public int getLineNo() {
        return lineNo;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return basic[0];
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCode() {
        return basic[1];
    }

    /**
     * 科目成績データ配列を返します。
     *
     * @return 科目成績データ配列
     */
    public Record[] get22Records() {
        return records;
    }

    /**
     * 志望校データ配列を返します。
     *
     * @return 志望校データ配列
     */
    public Target[] get9Targets() {
        return candidates;
    }

    /**
     * そのまま書き出す部分を返します。
     *
     * @return 先頭～志望校データの直前まで
     */
    public String getNonSense() {

        StringBuffer sb = new StringBuffer();
        sb.append(StringUtils.join(basic));
        for (int i = 0; i < records.length; i++) {
            sb.append(StringUtils.join(records[i].data));
        }
        return sb.toString();
    }

    /** 科目成績データクラス */
    public class Record {

        /** データ配列 */
        private String[] data;

        /**
         * コンストラクタです。
         *
         * @param data データ配列
         */
        public Record(String[] data) {
            this.data = data;
        }

        /**
         * 科目コードを返します。
         *
         * @return 科目コード
         */
        public String getSubcd() {
            return data[0];
        }

        /**
         * 得点を返します。
         *
         * @return 得点
         */
        public String getScore() {
            return data[1];
        }

        /**
         * 換算得点を返します。
         *
         * @return 換算得点
         */
        public String getCvsScore() {
            return data[2];
        }

        /**
         * 校内順位を返します。
         *
         * @return 校内順位
         */
        public String getSchoolRank() {
            return data[3];
        }

        /**
         * 全国順位を返します。
         *
         * @return 全国順位
         */
        public String getNationalRank() {
            return data[4];
        }

        /**
         * 校内偏差値を返します。
         *
         * @return 校内偏差値
         */
        public String getSchoolDeviation() {
            return data[5];
        }

        /**
         * 全国偏差値を返します。
         *
         * @return 全国偏差値
         */
        public String getNationalDeviation() {
            return data[6];
        }

        /**
         * 範囲を返します。
         *
         * @return 範囲
         */
        public String getScope() {
            return data[7];
        }

        /**
         * 学力レベルを返します。
         *
         * @return 学力レベル
         */
        public String getLevel() {
            return data[8];
        }

        /**
         * {@inheritDoc}
         */
        public String toString() {
            return "[subCd=" + getSubcd() + "/cvs=" + getCvsScore() + "/score=" + getScore() + "/dev="
                    + getNationalDeviation() + "]";
        }

    }

    /** 志望校データクラス */
    public class Target {

        /** データ配列 */
        private String[] data;

        /**
         * コンストラクタです。
         *
         * @param data データ配列
         */
        public Target(String[] data) {
            this.data = data;
        }

        /**
         * 行番号を返します。
         *
         * @return 行番号
         */
        public int getLineNo() {
            return lineNo;
        }

        /**
         * 大学コードを返します。
         *
         * @return 大学コード
         */
        public String getUnivcd() {
            return data[0];
        }

        /**
         * 学部コードを返します。
         *
         * @return 学部コード
         */
        public String getFacultycd() {
            return data[1];
        }

        /**
         * 学科コードを返します。
         *
         * @return 学科コード
         */
        public String getDeptcd() {
            return data[2];
        }

        /**
         * 大学５桁コードを返します。
         *
         * @return 大学５桁コード
         */
        public String getUniv5Code() {
            return data[3];
        }

        /**
         * 志望順位を返します。
         *
         * @return 志望順位
         */
        public String getWishRank() {
            return data[4];
        }

        /**
         * 評価得点を返します。
         *
         * @return 評価得点
         */
        public String getScore() {
            return data[5];
        }

        /**
         * 評価偏差値を返します。
         *
         * @return 評価偏差値
         */
        public String getDeviation() {
            return data[6];
        }

        /**
         * 総合評価ポイントを返します。
         *
         * @return 総合評価ポイント
         */
        public String getTotalPoint() {
            return data[7];
        }

        /**
         * センタ評価を返します。
         *
         * @return センタ評価
         */
        public String getCenterJudge() {
            return data[8];
        }

        /**
         * 二次評価を返します。
         *
         * @return 二次評価
         */
        public String getSecondJudge() {
            return data[9];
        }

        /**
         * 総合評価を返します。
         *
         * @return 総合評価
         */
        public String getTotalJudge() {
            return data[10];
        }

        /**
         * 総志望者数を返します。
         *
         * @return 総志望者数
         */
        public String getTotalCandidateCount() {
            return data[11];
        }

        /**
         * 総志望者内順位を返します。
         *
         * @return 総志望者内順位
         */
        public String getTotalRank() {
            return data[12];
        }

    }

}

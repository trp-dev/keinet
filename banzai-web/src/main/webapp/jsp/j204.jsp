<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8; IE=EmulateIE10; IE=EmulateIE11">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
    <title>判定結果 バンザイシステム</title>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j204pc.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j204sp.css" media="(max-width:640px)">
    <!-- [if IE 9]>
    <script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery.tablehover.min.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/FormUtil.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/DOMUtil.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/Browser.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/SwitchLayer.js"></script>
<!--     <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/footerFixed.js"></script> -->
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/more.js"></script>
<%-- テーブル関連スクリプト --%>
<c:set var="bean" value="${useListBean}" />
<%@ include file="/jsp/include/result_script.jsp" %>
<script type="text/javascript">
<!--
jQuery(function($) {
	$('th[data-href]').addClass('clickable')
		.click(function(e) {
			if(!$(e.target).is('a')){
				window.location = $(e.target).closest('th').data('href');
			};
		});
});
jQuery( function() {
    jQuery( '.jquery-tablehover' ) . tableHover();
} );

	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/open_detail.jsp" %>

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

	<%-- GLOBAL --%>
	var form;
	var util = new FormUtil();
	var sw = new SwitchLayer();

	<%-- 登録済み大学の情報 --%>
	var registed = new Array();
	<c:set var="left" value="${RegistMax}" />
	<c:forEach var="univ" items="${UnivList}">
		registed["<c:out value="${univ}" />"] = true;
		<c:set var="left" value="${ left - 1 }" />
	</c:forEach>
	var left = <c:out value = "${left}" />;

	<%-- 戻る --%>
	function submitBack() {
		<c:choose>
			<c:when test="${ JudgeMode == '1' }">
				submitMenuBack("j202");
			</c:when>
			<c:otherwise>
				submitMenuBack("j203");
			</c:otherwise>
		</c:choose>
	}

	<%-- 一覧に追加 --%>
	function submitRegist() {
		<%-- 入力チェック --%>
		var count = util.countChecked(form.univValue);
		if (count == 0) {
			alert("<kn:message id="j011a" />");
			return;
		} else if (count > left) {
			alert("<kn:message id="j014a" />" + (-(left - count)) + "<kn:message id="j014b" />");
			return;
		}
		form.button.value = "add";
		alert("<kn:message id="j015a" />");
		submitMenu("j204");
	}

	<%-- チェック状態を変更する --%>
	function chenge(flag) {
		var e = form.univValue;
		if (!e) {
			return;
		}
		if (e.length) {
			for (var i=0; i<e.length; i++) {
				e[i].checked  = flag;
				checked[e[i].value] = flag;
			}
		} else {
			e.checked = flag;
			checked[e.value] = flag;
		}
		renewRegistButton();
	}

	<%-- チェックしたら呼ばれる --%>
	function selectUniv(obj) {
		checked[obj.value] = obj.checked;
		renewRegistButton();
	}

	<%-- 登録ボタンの状態を変える --%>
	function renewRegistButton() {
		if (util.countChecked(form.univValue) == 0) {
            form.regist.id="tsuika-not-btn";
			form.regist.disabled = "disabled";
		} else {
            form.regist.id="tsuika-btn";
			form.regist.disabled = false;
		}
	}

	function init() {
		form = document.forms[0];
		execSort(form.sortKey.value);
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		sortinit1(form.sortKey.value);
	}

	<%-- 画面遷移 --%>
	function submitMenu(forward) {
		if (forward =="<c:out value="${param.forward}" />") {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = document.body.scrollLeft;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = 0;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = 0;
		}
		document.forms[0].forward.value = forward;
		if (forward == "j203") {
			document.forms[0].backward.value = "j101";
		} else {
			document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		}
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	<%-- 画面遷移(戻るボタン押下時) --%>
	function submitMenuBack(forward) {
		if (forward =="<c:out value="${param.forward}" />") {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = document.body.scrollLeft;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = 0;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = 0;
		}
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

    // トップ画面への遷移
    function submitTop(forward) {
    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
    		location.href = "<c:out value="${topUrl}" />";
		}

	}
 // 2015/09/15 CCC堀畑 add start
 //ソート時に該当項目の背景色を変更する
 function chgColor(c){
 var num = c;
  if(document.getElementById)
  {
  	switch (num) {
  	case 0:
    document.getElementById("col0").className = "link01 name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
    break;
  	case 1:
    document.getElementById('col1').className = "link01 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
    break;
    case 2:
    document.getElementById("col2").className = "link01 mytable-link1 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
    break;
    case 3:
    document.getElementById("col3").className = "link01 mytable-sp clickable overall link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
    break;
    case 4:
    document.getElementById("col4").className = "link01 mytable-sp clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
    break;
    case 5:
    document.getElementById("col5").className = "link01 mytable-sp mytable-link1 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    break;
  	}
  }
}
// 2015/09/15 CCC堀畑 add end

//「並び順を戻す」ボタンが押下された場合、ソート順、ソート項目の背景色を初期化
// 2015/09/07 CCC堀畑 add start
function sortreset() {
	execSort(0);
	document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "mytable-sp clickable overall link-width";
    document.getElementById("col4").className = "mytable-sp clickable link-width";
    document.getElementById("col5").className = "mytable-sp mytable-link1 clickable link-width";
}

function sortinit1(num){
	var sortnum = num
	if(sortnum == 0){
	}else if(sortnum == 1){
		document.getElementById("col0").className = "link01 name-title clickable";
	}else if(sortnum == 2){
		document.getElementById('col1').className = "link01 clickable link-width";
	}else if(sortnum == 3){
		document.getElementById("col2").className = "link01 mytable-link1 clickable link-width";
	}else if(sortnum == 4){
		document.getElementById("col3").className = "link01 mytable-sp clickable overall link-width";
	}else if(sortnum == 5){
		document.getElementById("col4").className = "link01 mytable-sp clickable link-width";
	}else if(sortnum == 6){
		document.getElementById("col5").className = "link01 mytable-sp mytable-link1 clickable link-width";
	}
}
//2015/09/07 CCC堀畑 add end

// -->
</script>
</head>
<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="UnivJudge" />?sid=<c:out value="${serverId}" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="uniqueKey" value="">
<input type="hidden" name="sortKey" value="<c:out value="${form.sortKey}" default="0" />">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<div id="wrapper"></div>

<%-- HEADER --%>
<%@ include file="/jsp/include/header.jsp" %>
<%-- /HEADER --%>

<!-- コンテンツここから -->
<div class="container">
    <div class="contents">

    <c:choose>
    <c:when test="${ empty useListBean.recordSet }">
    <!-- 検索 0件 -->
        <!-- 大見出し ここから -->
        <h3 class="title01">判定結果</h3>
        <!-- 大見出し ここまで -->
        <style type="text/css">
        <!--
        .pagetop { visibility: hidden; }
        .container{ min-height: calc(100vh - 439px); }
        @media screen and (max-width:640px) {
        	.container{ min-height: calc(100vh - 350px); }
        }
        -->
        </style>

        <div class="contents-inner">
            <div class="text-inner error-text">
                <p>指定された検索条件に一致する大学はありませんでした。もう一度検索条件を設定しなおしてください。</p>
            </div><!-- /.text-inner -->
            <div class="back-btn btn">
                <input type="button" value="" onclick="submitBack()" id="back-btn" />
            </div><!-- /.back-btn .btn -->
        </div><!-- /.contents-inner -->
    </c:when>
    <c:when test="${ useListBean.recordCount > DispMax }">
    <!-- 最大表示件数越え -->
        <!-- 大見出し ここから -->
        <h3 class="title01">判定結果</h3>
        <!-- 大見出し ここまで -->
        <style type="text/css">
        <!--
        .pagetop { visibility: hidden; }
        .container{ min-height: calc(100vh - 439px); }
        @media screen and (max-width:640px) {
        	.container{ min-height: calc(100vh - 350px); }
        }
        -->
        </style>

        <div class="contents-inner">
            <div class="text-inner error-text">
                <p>検索結果が<c:out value="${DispMax}" />件を超えたため結果を表示できません。<br>前のページに戻り、検索条件を変更してください。</p>
            </div><!-- /.text-inner -->
            <div class="back-btn btn">
                <input type="button" value="" onclick="submitBack()" id="back-btn" />
            </div><!-- /.back-btn .btn -->
        </div><!-- /.contents-inner -->
    </c:when>
    <c:otherwise>
    <!-- 通常  -->
        <!-- 大見出し ここから -->
        <h3 class="title01">判定結果（<c:out value="${bean.recordCount}" />件）</h3>
        <!-- 大見出し ここまで -->

        <div class="contents-inner info">
            <div class="text-inner204">
				<p>大学名をクリックすると、詳細情報が確認できます。</p>
				<p>[！] マークが表示されている大学は、出願要件や判定方法などについて注意が必要な大学です。</p>
				<p>大学名をクリックして詳細画面をご確認いただくとともに、入試科目等は必ず大学発表の学生募集要項でご確認ください。</p>
				<p class="cl-pc">また、「選択」にチェックを入れ、「登録大学に追加」をクリックすると、最大４０件まで登録できます。</p>
				<p class="cl-pc">大学の比較・検討にご利用ください。</p>
				<p class="cl-pc">あと<c:out value="${left}" />件登録が可能です。</p>
            </div><!-- /.text-inner -->
            <div class="btn allcheck-btn cl-pc">
            	<p>
                <input type="button" name="" value="" onClick="chenge(true)" id="all-check01" />
                <input type="button" style="margin-left:10px;" name="" value="" onClick="chenge(false)" id="all-reset01" />
                <input type="button"  style="margin-left:520px;" name="" value="" onClick="sortreset()" id="sort-reset01" />
            	</p>
            </div><!-- /.btn.allcheck-btn.cl-pc -->

            <table class="mytable jquery-tablehover" id="dataMyTable">
                <thead>
                    <tr>
                        <th rowspan="2" class="mytable-sp list01">選択</th>
                        <th rowspan="2" class="name-title clickable" data-href="javascript:execSort(1)" onclick="chgColor(0)" id="col0"><a href="javascript:execSort(1)">大　学</a></th>
                        <th colspan="2" class="mytable-sp">共通テスト</th>
                        <th colspan="2" class="mytable-sp">2次・個別</th>
                        <th colspan="2" class="right-line">評価</th>
                        <th class="mytable-sp">&nbsp;</th>
                        <th colspan="2" class="mytable-sp">配点比率</th>
                        <th rowspan="2" class="mytable-sp list06">第1段階選抜</th>
                    </tr>
                    <tr>
                        <th class="mytable-sp list02">ボーダー<br>得点(率)</th>
                        <th class="mytable-sp list03">あなたの<br>得点</th>
                        <th class="mytable-sp list04">ボーダー<br>偏差値</th>
                        <th class="mytable-sp list05">あなたの<br>偏差値</th>
                        <th class="clickable link-width" data-href="javascript:execSort(2)" onclick="chgColor(1)" id="col1"><a href="javascript:execSort(2)">共通<br>テスト</a></th>
                        <th class="mytable-link1 clickable link-width" data-href="javascript:execSort(3)" onclick="chgColor(2)" id="col2"><a href="javascript:execSort(3)">2次・<br>個別</a></th>
                        <th class="mytable-sp clickable overall link-width" data-href="javascript:execSort(4)" onclick="chgColor(3)" id="col3"><a href="javascript:execSort(4)">総合</a></th>
                        <th class="mytable-sp clickable link-width" data-href="javascript:execSort(5)" onclick="chgColor(4)" id="col4"><a href="javascript:execSort(5)">共通<br>テスト</a></th>
                        <th class="mytable-sp mytable-link1 clickable link-width" data-href="javascript:execSort(6)" onclick="chgColor(5)" id="col5"><a href="javascript:execSort(6)">2次・<br>個別</a></th>
                    </tr>
                </thead>

                <tbody id="ListTBody">
                <c:forEach var="univ" items="${bean.recordSetAll}">
                    <tr>
                        <td class="mytable-sp"><span class="text12"></span></td>
                        <td class="mytable-title"><span class="text12"></span></td>
                        <td class="mytable-sp"><span class="text12 "></span></td>
                        <td class="mytable-sp"><strong><span class="text12"></span></strong></td>
                        <td class="mytable-sp"><span class="text12"></span></td>
                        <td class="mytable-sp"><strong><span class="text12"></span></strong></td>
                        <td><strong><span class="text12"></span></strong></td>
                        <td><strong><span class="text12"></span></strong></td>
                        <td class="mytable-sp"><strong><span class="text12"></span></strong></td>
                        <td class="mytable-sp"><span class="text12"></span></td>
                        <td class="mytable-sp"><span class="text12"></span></td>
                        <td class="mytable-sp"><span class="text12"></span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table><!-- /.mytable -->

        </div><!-- /.contents-inner -->

        <div class="contents-inner info">
            <div class="cl-pc">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr valign="top">
                    <td width="100%"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/parts/sp.gif" width="1" height="18" border="0" alt=""><br/></td>
                </tr>
                </table>
            </div>
            <div class="btn allcheck-btn cl-pc">
                <input type="button" name="" value="" onClick="chenge(true)" id="all-check02" />
                <input type="button" style="margin-left:10px;" name="" value="" onClick="chenge(false)" id="all-reset02" />
                <input type="button" style="margin-left:520px;" name="" value="" onClick="sortreset()" id="sort-reset02" />
            </div><!-- /.btn .allcheck-btn .cl-pc -->
        </div><!-- /.contents-inner -->

        <div class="contents-inner">
            <div class="submit-btn btn cl-pc">
                <input type="button" name="regist" value="" disabled=disabled onclick="submitRegist();" id="tsuika-not-btn" />
            </div><!-- /.submit-btn.btn. cl-pc -->

            <div class="back-btn btn">
                <input type="button" value="" onclick="submitBack()" id="back-btn" />
            </div><!-- /.back-btn .btn -->
        </div><!-- /.contents-inner -->
    </c:otherwise>
    </c:choose>

    </div><!-- /.contents -->
</div><!-- /.container -->
<!-- コンテンツここまで -->

<%-- FOOTER --%>
<%@ include file="/jsp/include/footer.jsp" %>
<%-- /FOOTER --%>

</form>
</body>
</html>

<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<jsp:useBean id="form" scope="request" class="jp.co.fj.inet.forms.J203Form" />
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
	<title>大学選択（名称指定）	バンザイシステム</title>
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j203pc.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j203b.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j203c.css">
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j203sp.css" media="(max-width:640px)">
	<!--[if lt IE 9]>
	<script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
	<![endif]-->
	<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js"></script>
	<script src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js" type="text/javascript"></script>
	<script src="<c:out value="${cacheServerUrl}" />js/jquery.corner.js" type="text/javascript"></script>
	<script type="text/javascript">
<!--

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

	//角丸
	$('#searchBox').corner("6px");

	/*
	 * 画面遷移
	 */
	function submitMenu(forward) {
		if (forward =="<c:out value="${param.forward}" />") {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = document.body.scrollLeft;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = document.body.scrollTop;
		} else {
			if (document.forms[0].scrollX) document.forms[0].scrollX.value = 0;
			if (document.forms[0].scrollY) document.forms[0].scrollY.value = 0;
		}
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "<c:out value="${param.forward}" />";
		if (document.forms[0].searchStr) {
			if (document.forms[0].searchStr.value == "大学名(漢字・かな)の一部を入力") {
				document.forms[0].searchStr.value = "";
			}
		}
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
	<%@ include file="/jsp/script/submit_exit.jsp" %>

	/**
	 * 頭文字サブミッション
	 */
	function submitChar(c){
		document.forms[0].searchStr.value = "";
		document.forms[0].button.value = 'char';
		document.forms[0].charValue.value = c;
		submitMenu('j203');
	}

	/**
	 * 大学名・大学コードサブミッション
	 */
	function submitSearch(){
		if (check()) {
			document.forms[0].button.value = 'search';
			submitMenu('j203');
		}
	}

	/**
	 * 判定サブミッション
	 */
	function submitJudge(){
		if(document.forms[0].elements['univValue'] == null || document.forms[0].elements['univValue'].length == 0){
			window.alert("<kn:message id="j020a" />");
			return;
		}
		submitMenu('j204');
	}

	/**
	 * 入力チェック
	 */
	function check() {
		var c = document.forms[0].searchStr.value;
		if (c == "大学名(漢字・かな)の一部を入力") {
			c == "";
		}
		if (!isStrCheck(c)) {
			alert("<kn:message id="j017a" />");
			return false;
		}else{
			return true;
		}
	}

	/*
	* 文字列が漢字・ひらがな・全角カナのみで構成されているかをチェック
	*/
	function isStrCheck(str) {
		if (str == null || str.length == 0) return false;
			for (var i=0; i<str.length; i++) {
				var c = str.charCodeAt(i);
				if (!((c >= 12353 && c <= 12435) || c == 12540) && !((c >= 19968  && c <= 40911) || (c >= 13312 && c <= 19903) || (c >= 131072  && c <= 173791) || (c >= 63744  && c <= 64223) || (c >= 194560  && c <= 195103)) && !(c >= 12448  && c <= 12543)) return false;
				}
			return true;
	}

	/**
	 * 初期化
	 */
	function init(){

		// 判定結果一覧から戻るボタンで戻ってきた場合
		<% if (request.getAttribute("univValueList") != null) {
			if (!request.getAttribute("facultyStatusFlg").toString().equals("true")) { %>
				document.getElementById("univSection").style.display = "none";
				document.getElementById("facultySection").style.display = "block";
				restoreCheck();
			<% } %>
		<% } %>

		<%@ include file="/jsp/script/loading.jsp" %>
	}

	/*
	 * 名称指定③に戻る場合のチェック状態復元処理
	 */
	function restoreCheck() {
		// 選択済み学科のリストを作成
		var selectedDeptList = new Array(
			<c:forEach var="univValue" items="${univValueList}" varStatus="status">
				<c:if test="${ status.index > 0 }">,</c:if>
					"<c:out value="${univValue}" />"
				</c:forEach>
		);

		// 選択済み学部のリストを作成
		var selectedFacList = new Array();
		var lastFacultyCd = "";
		for (var i = 0; i < selectedDeptList.length; i++) {
			if (lastFacultyCd != selectedDeptList[i].substring(0, 7)) {
				selectedFacList.push(selectedDeptList[i].substring(0, 7));
				lastFacultyCd = selectedDeptList[i].substring(0, 7);
			}
		}

		var selectedUnivCd = selectedDeptList[0].substring(0, 4);
		var selectedUnivName;
		for (var i = 0; i < univString.length; i++) {
			if (univString[i][0] == selectedUnivCd) {
				selectedUnivName = univString[i][1];
			}
		}
		// 学部一覧、学科一覧を作成
		univSelect(selectedUnivCd, selectedUnivName);

		// 学部のチェック状態を復元
		for (var i = 0; i < selectedFacList.length; i++) {
			for(var j = 0; j < document.forms[0].fac.length; j++){
				var el = document.forms[0].fac[j];
				if (el.value == selectedFacList[i]) {
					el.checked = true;
					checkdept(el);
				}
			}
		}
	}

	/*
	 * 全学部指定時の処理
	 */
	function checkALL(obj) {
		// 学部
		if (document.forms[0].fac.length) {
			for(var i = 0; i < document.forms[0].fac.length; i++){
				document.forms[0].fac[i].checked = obj.checked;
			}
		} else if (document.forms[0].univValue) {
			document.forms[0].fac.checked = obj.checked;
		}

		//　学科
		if (document.forms[0].univValue.length) {
			for(var i = 0; i < document.forms[0].univValue.length; i++){
				document.forms[0].univValue[i].checked = obj.checked;
			}
		} else if (document.forms[0].univValue) {
			document.forms[0].univValue.checked = obj.checked;
		}
	}

	/*
	 * 学部と学科(未表示項目)のチェックボックスの連動
	 */
	function checkdept(obj) {

		if (document.forms[0].univValue.length) {
			for(var i = 0; i < document.forms[0].univValue.length; i++){
				var el = document.forms[0].univValue[i];
				var index = el.value.indexOf(obj.value);
				if (index >= "0") {
					el.checked = obj.checked;
				}
			}
		} else if (document.forms[0].univValue) {
			document.forms[0].univValue.checked = obj.checked;
		}

		checkZengakubu();
	}

	/*
	 * 全学部のチェック状態を変更
	 */
	function checkZengakubu() {
		var allcheckFlg = true;
		// 学部のチェック状態をチェック
		if (document.forms[0].fac.length) {
			for(var i = 0; i < document.forms[0].fac.length; i++){
				if (!document.forms[0].fac[i].checked) {
					allcheckFlg = false;
				}
			}
		} else if (document.forms[0].fac) {
			if (!document.forms[0].fac.checked) {
				allcheckFlg = false;
			}
		}

		// 学科のチェック状態をチェック
		if (document.forms[0].univValue.length) {
			for(var i = 0; i < document.forms[0].univValue.length; i++){
				if (!document.forms[0].univValue[i].checked) {
					allcheckFlg = false;
				}
			}
		} else if (document.forms[0].univValue) {
			if (!document.forms[0].univValue.checked) {
				allcheckFlg = false;
			}
		}

		// 全学部にチェックを付けるかの判定
		if (allcheckFlg) {
			document.forms[0].zengakubu.checked = true;
		} else {
			document.forms[0].zengakubu.checked = false;
		}
	}

	/*
	 * 大学を選択時の処理
	 */
	function univSelect(cdValue, name) {
		document.all.univName.innerHTML = name;
		var univIndex;
		for (var i = 0; i < univString.length; i++) {
			if (univString[i][0] == cdValue) {
				univIndex = i;
			}
		}
		var sinFacultyflg = false;
		// 学部一覧・学科一覧を削除
		$("#faculty li").remove();
		$("#dept li").remove();
		$('ul#faculty').append('<li><label><input type="checkbox" name="zengakubu" value="0" onclick="checkALL(this)">全学部</label></li>');
		// 学部が1件しか存在しない場合
		if (facultyString[univIndex].length == "1") {
			sinFacultyflg = true;
		}
		for (var i = 0; i < facultyString[univIndex].length; i++) {
			var facultyname = facultyString[univIndex][i][1];
			var facultyvalue = univString[univIndex][0] + "," + facultyString[univIndex][i][0];
			// 学部の一覧を作成
			$('ul#faculty').append('<li><label><input type="checkbox" name="fac" value="' + facultyvalue + '" onclick="checkdept(this)">' + facultyname + '</label></li>');
			for (var j = 0; j < deptString[univIndex][i].length; j++) {
				var deptname = deptString[univIndex][i][j][1];
				var deptvalue = univString[univIndex][0] + "," + facultyString[univIndex][i][0] + "," + deptString[univIndex][i][j][0];
				// 学科の一覧(非表示)を作成
				if (sinFacultyflg) {
					// 学部の一覧が存在しない場合は、チェックボックスがONの状態で学科一覧を作成
					$('ul#dept').append('<li><label><input type="checkbox" name="univValue" value="' + deptvalue + '" checked>' + deptname + '</label></li>');
				} else {
					$('ul#dept').append('<li><label><input type="checkbox" name="univValue" value="' + deptvalue + '">' + deptname + '</label></li>');
				}
			}
		}

		// 学部が1件しか場合は判定結果一覧に遷移
		if (sinFacultyflg) {
			document.forms[0].facultyStatusFlg.value = 'True';
			submitJudge();
		}　else {
			// 画面表示を切替
			document.getElementById("univSection").style.display = "none";
			document.getElementById("facultySection").style.display = "block";
			document.forms[0].facultyStatusFlg.value = 'False';
			window.scrollTo(0, 0);
		}
	}

	/*
	 * 学部選択画面で戻るボタン押下時の処理
	 */
	function backAction() {
		// 画面表示を切替
		document.getElementById("univSection").style.display = "block";
		document.getElementById("facultySection").style.display = "none";
		// 学部一覧・学科一覧を削除
		$("#faculty li").remove();
		$("#dept li").remove();
	}

	/*
	 * 判定学部未選択チェック
	 */
	function boxChecked(){
		var checkedflg = false;
		// 学科の選択状態をチェック
		if (document.forms[0].univValue.length) {
			for(var count = 0; count < document.forms[0].univValue.length; count++){
				if (document.forms[0].univValue[count].checked) {
					checkedflg = true;
				}
			}
		} else if (document.forms[0].univValue) {
			if (document.forms[0].univValue.checked) {
				checkedflg = true;
			}
		}

		if (checkedflg) {
			submitJudge();
		} else {
			alert("<kn:message id="j031a" />");
		}
	}

	// 名称指定②から名称指定①に戻る場合の処理
	function submitBack() {
		document.forms[0].forward.value = "j203";
		document.forms[0].backward.value = "j101";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}

	/**
	 * 大学コンボ
	 */
	<%@ include file="/jsp/include/univ_combox_203.jsp" %>

		$(window).bind('load', function() {
				var sf = document.getElementById('search');
				if (sf && checkuserAgent() != "firefox") {
					if(sf.value=="") {sf.value="大学名(漢字・かな)の一部を入力";sf.style.color="#A9A9A9";}

					if(sf.value=="大学名(漢字・かな)の一部を入力") {sf.style.color="#A9A9A9";}

					$('#search').focus( function() {
						var sf = document.getElementById('search');
						if(sf.value=="大学名(漢字・かな)の一部を入力") {sf.value="";sf.style.color="#000000";}
					});

					$('#search').blur( function() {
						var sf = document.getElementById('search');
						if(sf.value=="") {sf.value="大学名(漢字・かな)の一部を入力";sf.style.color="#A9A9A9";}
					});
				}
		});

		// 使用しているブラウザの判定
		function checkuserAgent() {
			var userAgent = window.navigator.userAgent.toLowerCase();

			if (userAgent.indexOf('opera') != -1) {
	  			return "opera";
			} else if (userAgent.indexOf('msie') != -1) {
	  			return "ie";
			} else if (userAgent.indexOf('chrome') != -1) {
	  			return "chrome";
			} else if (userAgent.indexOf('safari') != -1) {
	  			return "safari";
			} else if (userAgent.indexOf('firefox') != -1) {
	  			return "firefox";
	  		} else if (userAgent.indexOf('trident') != -1) {
	  			return "trident";
			} else {
	  			return false;
			}

		}

	    // トップ画面への遷移
	    function submitTop(forward) {
	    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
	    		location.href = "<c:out value="${topUrl}" />";
			}

		}
// -->
</script>
</head>
<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="UnivSpec" />?sid=<c:out value="${serverId}" />" method="POST" onsubmit="return false">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="charValue" value="">
<input type="hidden" name="button" value="">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<input type="hidden" name="searchMode" value="name">
<input type="hidden" name="facultyStatusFlg" value="">
<div id="wrapper">
<%-- HEADER --%>
<%@ include file="/jsp/include/header.jsp" %>
<%-- /HEADER --%>
<div id="LoadingLayer">
		<!-- コンテンツここから -->
			<div class="container">
				<h3 class="title01">ロード中</h3>
				<div class="contents">
					<div class="contents-inner">
						<div class="onjudge">
							<p>ロード中です ...</p>
						<!-- /.onjudge --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.container --></div>
		<%-- FOOTER --%>
		<%@ include file="/jsp/include/footer.jsp" %>
		<%-- /FOOTER --%>
</div>
<div id="MainLayer" style="display:none;">
	<c:choose>
	<c:when test="${ empty nestedUnivListSize }">
<!-- 大学名称指定部分 -->
	<!-- コンテンツここから -->
			<div class="container">
				<div class="contents">
					<h3 class="title01">判定大学を指定する</h3>
					<div class="contents-inner">
						<div class="text-inner">
							<p class="cl-sp">大学名を直接入力</p>
						<!-- /.text-inner --></div>
						<div class="search-container">
							<div class="search-form">
								<h4 class="title02 search-title cl-pc">大学名を直接入力</h4>
									<div id="searchBox">
										<input id="search" type="search" name="searchStr" value="<c:out value="${form.searchStr}" />" placeholder="大学名(漢字・かな)の一部を入力">
									<!-- /#searchBox --></div>
									<div class="search-btn-container">
										<div class="search-btn btn">
											<input type="submit" name="search" value="" onclick="submitSearch()" id="search-btn">
										<!-- /.submit-btn.btn --></div>
									<!-- /.search-btn-container --></div>
							<!-- /.search-form --></div>
						<div class="cl-pc initial">
							<h4 class="title02 search-title cl-pc">大学名の頭文字を指定</h4>
							<div class="initial-inner cl-pc">
								<table>
									<tr>
										<td><input type="button" value="あ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="か" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="さ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="た" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="な" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="は" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ま" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="や" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ら" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="わ" onclick="submitChar(this.value)"></td>
									</tr>
									<tr>
										<td><input type="button" value="い" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="き" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="し" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ち" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="に" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ひ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="み" onclick="submitChar(this.value)"></td>
										<td><br></td>
										<td><input type="button" value="り" onclick="submitChar(this.value)"></td>
										<td><br></td>
									</tr>
									<tr>
										<td><input type="button" value="う" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="く" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="す" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="つ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ぬ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ふ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="む" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ゆ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="る" onclick="submitChar(this.value)"></td>
										<td><br></td>
									</tr>
									<tr>
										<td><input type="button" value="え" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="け" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="せ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="て" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ね" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="へ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="め" onclick="submitChar(this.value)"></td>
										<td><br></td>
										<td><input type="button" value="れ" onclick="submitChar(this.value)"></td>
									<td><br></td>
									</tr>
									<tr>
										<td><input type="button" value="お" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="こ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="そ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="と" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="の" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ほ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="も" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="よ" onclick="submitChar(this.value)"></td>
										<td><input type="button" value="ろ" onclick="submitChar(this.value)"></td>
										<td><br></td>
									</tr>
								</table>
							<!-- /.initial-inner --></div>
						<!-- /cl-pc.initial --></div>
						<!-- /.search-container --></div>

					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.container --></div>
	<!-- コンテンツここまで -->
	<%-- FOOTER --%>
	<%@ include file="/jsp/include/footer.jsp" %>
	<%-- /FOOTER --%>
<!-- 大学名称指定部分ここまで -->
</c:when>
<c:when test="${ nestedUnivListSize > 0 }">

	<!-- 大学選択部分 -->
	<div id="univSection">
		<div class="container">

				<div class="contents">
					<h3 class="title01">判定大学を指定する</h3>
					<div class="contents-inner">
						<div class="text-inner203b">

						<c:choose>
  						<c:when test="${ not empty form.searchStr}">
						<h4><strong><c:out value="${form.searchStr}" /></strong>を含む大学</h4>
						</c:when>
						<c:otherwise>
						<h4><strong><c:out value="${form.charValue}" /></strong>で始まる大学</h4>
						</c:otherwise>
						</c:choose>
							<p>検索結果：<strong><c:out value="${nestedUnivListSize}" /></strong>件</p>
						<!-- /.text-inner --></div>
							<div class="next">
								<ul>
									<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
										<li value="<c:out value="${nestedUnivData.univCd}" />" onclick="univSelect('<c:out value="${nestedUnivData.univCd}" />', '<kn:univType><c:out value="${nestedUnivData.univCd}" /></kn:univType><c:out value="${nestedUnivData.univKanjiName}" />')"><a><span class="search-name"><kn:univType><c:out value="${nestedUnivData.univCd}" /></kn:univType><c:out value="${nestedUnivData.univKanjiName}" /></span></a></li>
									</c:forEach>
								</ul>
							<!-- /.next --></div>

						<div class="back-btn btn">
							<input type="button" value="" onclick="submitBack()" id="back-btn">
						<!-- /.back-btn.btn --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.container --></div>
	</div>
	<!-- コンテンツここまで -->
	<!-- 大学選択部分ここまで -->

	<!-- コンテンツここから -->
	<!-- 学部選択部分 -->
	<div id="facultySection" style="display: none">

			<div class="container">
				<div class="contents">
					<h3 class="title01">判定学部を指定する</h3>
					<div class="contents-inner">
						<h4 class="title02" id="univName"></h4>
						<div class="text-inner">
							<div class="next1">
								<ul id="faculty">

								</ul>
								<ul id="dept" style="display:none">

								</ul>
							<!-- /.next --></div>

						<!-- /.text-inner --></div>
						<div class="submit-btn btn">
							<input type="button" value="" onclick="boxChecked()" id="hantei-btn" class="cl-pc">
							<input type="button" value="" onclick="boxChecked()" class="hantei-btn cl-sp">
						<!-- /.back-btn.btn --></div>
						<div class="back-btn btn">
							<input type="button" value="" onclick="backAction()" id="back-btn">
						<!-- /.back-btn.btn --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.container --></div>
	</div>
	<!-- 学部選択部分ここまで -->
	<!-- コンテンツここまで -->
	<%-- FOOTER --%>
	<%@ include file="/jsp/include/footer.jsp" %>
	<%-- /FOOTER --%>
</c:when>
<c:when test="${ not empty nestedUnivListSize }">
	<style type="text/css">
        <!--
        .pagetop { visibility: hidden; }
        -->
    </style>
	<%@ include file="/jsp/include/j203error.jsp" %>
</c:when>
</c:choose>
</div>
</div>
</form>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.kawaijuku.judgement.util.JudgementUtil" %>
<jsp:useBean id="Judge" scope="request" type="jp.co.fj.banzai.beans.BZDetailPageBean" />
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
    <title>結果詳細 バンザイシステム</title>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j205pc.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j205sp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/graph.css">
    <style type="text/css">
    <!--
        div.g-nav { display: none; }
        div.g-nav-sp { display: none; }
    -->
    </style>
    <!--[if lt IE 9]>
    <script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/footerFixed.js"></script>
    <script type="text/javascript">
    <!--

    <%@ include file="/jsp/script/submit_menu.jsp" %>

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

 	// トップ画面への遷移
    function submitTop(forward) {
    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
    		location.href = "<c:out value="${topUrl}" />";
		}

	}

    //画面読み込み時に実行 2015/09/08 mizo add start
    function init(){
    	borderlinePrediction();
    	admittedNumber();
    }

    //大学区分が01、02以外のとき、第1段階選抜予想ラインの表示を無効にするmizo
    function borderlinePrediction(){

    	var univDivCd = new String("<%=Judge.getUnivDivCd()%>");

    	if (univDivCd == '01' || (univDivCd == '02')){
    		document.getElementById('borderline-prediction').style.display = 'block';
    	} else {
    		document.getElementById('borderline-prediction').style.display = 'none';
    	}
    }

    //入試定員信頼性が「非公表(8)」または募集人員が0名の場合
    //募集人員を非表示にする(2015/09/03)
    function admittedNumber(){

    	var admittedNum = new String("<%= changeNOS(Judge.getExamCapaRel(), Judge.getCapacity()) %>");
    	if (admittedNum == "--"){
    		document.getElementById('boshu').style.display = 'none';
    	} else {
    		document.getElementById('boshu').style.display = '';
    	}
    }
	//画面読み込み時に実行 2015/09/08 mizo add end

    /**
     * サブミッション
     */
    function submitForm(forward) {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 公表科目サブミッション
     */
    function submitOpenSub() {
        var form = document.forms[0];
        // 出力先ウィンドウを開く
        if (window.navigator.userAgent.indexOf('iPhone OS') == -1 && window.navigator.userAgent.indexOf('Edge') == -1 && window.navigator.userAgent.indexOf('Android') == -1) {
        	op = window.open("", "j206", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
        	op.focus();
        }
        form.forward.value = "j206";
        form.backward.value = "<c:out value="${param.forward}" />";
        if (window.navigator.userAgent.indexOf('iPhone OS') == -1 && window.navigator.userAgent.indexOf('Edge') == -1 && window.navigator.userAgent.indexOf('Android') == -1) {
            form.target = "j206";
        } else {
            form.target = "_blank";
        }
        form.submit();
    }
    /**
     * 枝番設定
     */
    function setBranchInfo(branch){
        document.forms[0].selectBranch.value = branch;
    }



    //-->
    </script>
</head>
<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />" onLoad="init();">
<div id="wrapper">
<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="UnivDetail" />?sid=<c:out value="${serverId}" />" method="post">
<%-- hiddens --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="judgeNo" value="<c:out value="${param.judgeNo}" />">
<!-- 隠れフィールド　大学・学部・学科情報 -->
<input type="hidden" name="uniqueKey" value="<%=encTableNBSP(Judge.getUniqueKey())%>">
<!-- 隠れフィールド　選択された枝番 -->
<input type="hidden"name="selectBranch" value="">
<%-- /hiddens --%>

<%-- HEADER --%>
<%@ include file="/jsp/include/header.jsp" %>
<%-- /HEADER --%>

<!-- コンテンツここから -->
<div class="container">
    <div class="contents">

        <!-- 大見出し ここから -->
        <h3 class="title01">判定結果詳細</h3>
        <!-- 大見出し ここまで -->

        <div class="contents-inner">
            <h4 class="title02"><kn:univType><c:out value="${Judge.univCd}"/></kn:univType><c:out value="${Judge.univName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${Judge.deptName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${Judge.subName}"/></h4>
            <div class="text-inner">
                <p>本部所在地：<c:out value="${Judge.prefName}"/>&nbsp;&nbsp;&nbsp;キャンパス所在地：<c:out value="${Judge.prefName_sh}"/></p>
                <p><span id="boshu">募集人員：<%= changeNOS(Judge.getExamCapaRel(), Judge.getCapacity()) %>　</span>出願予定者：<%= encTableHAI(Judge.getCandidates()) %>名<br class="br"><span class="bairitsu-indent">前年度倍率：<%= encTableHAI(Judge.getPreMagnification()) %>倍</span></p>
                <!-- 2016.10.13 CCC.FUJISAWA ADD START -->
                	<c:choose>
                		<c:when test="${Judge.typeFlg == '1' }"><p><span class="exclamation1">［！］</span><span class="comment"><c:out value="${Judge.commentStatement}"></c:out></span></p></c:when>
                		<c:when test="${Judge.typeFlg == '2' }"><p><span class="exclamation2">［！］</span><span class="comment"><c:out value="${Judge.commentStatement}"></c:out></span></p></c:when>
                		<c:otherwise><p><span class="comment"><c:out value="${Judge.commentStatement}"></c:out></span></p></c:otherwise>
                	</c:choose>
                <!-- 2016.10.13 CCC.FUJISAWA ADD END -->
            </div><!-- /.text-inner -->
            <div class="btn link-btn">
            	<p>
               	<input type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01" class="cl-pc">
                <c:if test="${ not (Judge.univDivCd == '08' || Judge.univDivCd == '09')}">
                	<input type="button" name="" value="" onclick="window.open('https://search.keinet.ne.jp/outline/department/<c:out value="${Judge.univCd}"/>')" id="j205-btn02" class="cl-pc">
            	</c:if>
            	</p>
            </div><!-- /.btn.link-btn -->
           <div class="btn link-btn01">
            	<p>
            	<input style="position: absolute; right: 160px;" type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01-sp" class="cl-sp1">
                <c:if test="${ not (Judge.univDivCd == '08' || Judge.univDivCd == '09')}">
                	<input style="position: absolute; right: 10px;" type="button" name="" value="" onclick="window.open('https://search.keinet.ne.jp/outline/department_sp/<c:out value="${Judge.univCd}"/>')" id="j205-btn02-sp" class="cl-sp1">
            	</c:if>
            	<div class="contents-inner01"></div>
            	</p>
            </div><!-- /.btn.link-btn01 -->

            <div class="section-container">
                <div id="section01">
                    <h4 class="section-title">共通テスト&nbsp;判定&nbsp;<%=Judge.getCLetterJudgement()%></h4>

                    <div class="text-inner">
                        <div class="section-inner">
                            <div class="panel">
                            	<div class="cl-pc">
	                            	<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getCLetterJudgement().trim().length() > 0 ? Judge.getCLetterJudgement().trim()=="*" ? "kome" : Judge.getCLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>.png" />
	                            </div>
	                            <div class="cl-sp">
                            		<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getCLetterJudgement().trim().length() > 0 ? Judge.getCLetterJudgement().trim()=="*" ? "kome" : Judge.getCLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>_x2.png" style="width:100px; height:100px" />
                            	</div>
                            </div>
                            <div class="myscore">
                                <h5 class="point">あなたの得点</h5>
                                <p><span class="text-19"><%= encTableHAI(Judge.getC_scoreStr()) %>点</span><span class="text-16">（<%= encTableHAI(Judge.cFullPoint) %>点中）</span></p>
                            </div><!-- /.myscore -->
                        </div><!-- /.section-inner -->
                        <a><span style="visibility: hidden;">&nbsp;</span></a>
                            <table class="section-table">
                                <thead>
                                    <tr>
                                        <td width="82" height="21" class="full01" style="background-color:#7b95d8;color:#fff;">教科</td>
                                        <th width="70" height="21">英</th>
                                        <th width="70" height="21">数</th>
                                        <th width="70" height="21">国</th>
                                        <th width="70" height="21">理</th>
                                        <th width="70" height="21">地公</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="82" height="21" class="full01" style="background-color:#7b95d8;color:#fff;">得点</td>
                                        <td class="full2"><c:out value="${Judge.score1English}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Mat}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Jap}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Sci}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Soc}"/></td>
                                    </tr>
                                    <tr>
                                        <td width="82" height="21" class="full02" style="background-color:#7b95d8;color:#fff;">配点</td>
                                        <td class="full3"><c:out value="${Judge.allot1English}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Mat}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Jap}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Sci}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Soc}"/></td>
                                    </tr>
                                </tbody>
                            </table>

                            <hr style="background-color:#fff"><hr>


                        <h5 class="clear point">ボーダー得点・評価基準点</h5>
                        <p><span class="text-16">ボーダー得点</span>&nbsp;<span class="text-19"><%= encTableHAI(Judge.getBorderPointStr()) %>点</span></p>
                        <ul class="rank-marks">
                            <li><%=encTableNBSP(Judge.getCDLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCCLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCBLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCALine()) %><li>
                        </ul>
                        <ul class="rank-meter">
                            <li class="rank-meter-e">E</li>
                            <li class="rank-meter-d">D</li>
                            <li class="rank-meter-c">C</li>
                            <li class="rank-meter-b">B</li>
                            <li class="rank-meter-a">A</li>
                        </ul>
                        <ul class="myrank">
                            <c:forEach var="i" begin="1" end="5" step="1">
                            <li <c:if test="${Judge.c_judgement == i}">id="myRankCurrent"</c:if>>★</li>
                            </c:forEach>
                        </ul>
                        <!-- グラフへボタン ここから -->
                        <div class="clear bunpu">
                            <p><a href="#distribution">&nbsp;</a></p>
                        </div>
                        <!-- グラフへボタン ここまで -->



                            <div id="borderline-prediction"><!-- mizo -->
                            <hr class="full">
                            <h5 class="point">第1段階選抜予想ライン</h5>
                            <p><span class="senbatsu-indent"><c:out value="${Judge.pickedLine}"/></span></p>
                            </div>



                    </div><!-- /.text-inner -->
                    <c:if test="${ Judge.scheduleCd == ' ' }">
                    <div id="notCovered">
                        <p>課さない</p>
                    </div><!-- /#notCovered -->
                    </c:if>
                </div><!-- /#section01 -->

                <div id="section02">
                    <h4 class="section-title">2次・個別試験&nbsp;判定&nbsp;<%=Judge.getSLetterJudgement()%></h4>
                    <div class="text-inner">
                            <div class="section-inner">
                            	<div class="panel">
                            		<div class="cl-pc">
  	                            		<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getSLetterJudgement().trim().length() > 0 ? Judge.getSLetterJudgement().trim()=="*" ? "kome" : Judge.getSLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>.png" />
  	                            	</div>
  	                            	<div class="cl-sp">
  	                            		<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getSLetterJudgement().trim().length() > 0 ? Judge.getSLetterJudgement().trim()=="*" ? "kome" : Judge.getSLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>_x2.png" style="width:100px; height:100px" />
  	                            	</div>
  	                            </div>
                                <div class="myscore">
                                    <h5 class="point">あなたの偏差値</h5>
                                    <p><span class="text-19"><%= encTableHAI(Judge.getS_scoreStr()) %></span></p>
                                </div><!-- /.myscore -->
                            </div><!-- /section-inner -->
                        <a><span style="visibility: hidden;">&nbsp;</span></a>
                        <table class="section-table">
                            <thead>
                                <tr>
                                    <td width="72" height="21" class="full01" style="background-color:#7b95d8;color:#fff;">教科</td>
                                    <th width="60" height="21">英</th>
                                    <th width="60" height="21">数</th>
                                    <th width="60" height="21">国</th>
                                    <th width="60" height="21">理</th>
                                    <th width="60" height="21">地公</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="72" height="21" class="full01" style="background-color:#7b95d8;color:#fff;">偏差値</td>
                                    <td class="full2"><c:out value="${Judge.score2Eng}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Mat}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Jap}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Sci}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Soc}"/></td>
                                </tr>
                                <tr>
                                    <td width="72" height="21" class="full02" style="background-color:#7b95d8;color:#fff;">配点</td>
                                    <td class="full3"><c:out value="${Judge.allot2Eng}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Mat}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Jap}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Sci}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Soc}"/></td>
                                </tr>
                            </tbody>
                        </table>

                        <hr style="background-color:#fff"><hr>

                        <h5 class="clear point">ボーダー偏差値・評価基準偏差値</h5>
                        <p>
                            <span class="text-16">ボーダー偏差値</span>
                            <span class="text-19">
                            <c:if test="${Judge.rankName == ' -- ' && Judge.rank != '-99'}">--</c:if>
                            <c:if test="${Judge.rankName != ' -- ' && Judge.rank != '-99'}">
                            <%= Judge.getRankLLimitsStr().trim().length() > 0 ? Judge.getRankLLimitsStr()+" &#65374; "+Judge.getRankHLimitsStr() : "--" %>
                            </c:if>
                            <c:if test="${Judge.rank == '-99'}">BF</c:if>
                            </span>
                        </p>
                        <ul class="deviation-marks">
                            <li><%=encTableNBSP(Judge.getSDLine())%><li>
                            <li><%=encTableNBSP(Judge.getSCLine())%><li>
                            <li><%=encTableNBSP(Judge.getSBLine())%><li>
                            <li><%=encTableNBSP(Judge.getSALine())%><li>
                        </ul>
                        <ul class="deviation-meter">
                            <li class="deviation-meter-e">E</li>
                            <li class="deviation-meter-d">D</li>
                            <li class="deviation-meter-c">C</li>
                            <li class="deviation-meter-b">B</li>
                            <li class="deviation-meter-a">A</li>
                        </ul>
                        <ul class="mydeviation clear">
                            <c:forEach var="i" begin="1" end="5" step="1">
                            <li <c:if test="${Judge.s_judgement == i}">id="myDeviationCurrent"</c:if>>★</li>
                            </c:forEach>
                        </ul>

                        <hr class="full" style="visibility:hidden">


                    </div><!-- /.text-inner -->
                </div><!-- /#section02 -->

<c:if test="${ Judge.scheduleCd != ' ' }">
                <div id="section03">
                    <h4 class="section-title">総合&nbsp;判定&nbsp;<%=Judge.getTLetterJudgement()%></h4>
                    <div class="text-inner">
                        <div class="section-inner">
                        	<div class="panel">
                        		<div class="cl-pc">
	                            	<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getTLetterJudgement().trim().length() > 0 ? Judge.getTLetterJudgement().trim()=="*" ? "kome" : Judge.getTLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>.png" />
	                            </div>
	                        	<div class="cl-sp">
	                            	<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/hantei_<%= Judge.getTLetterJudgement().trim().length() > 0 ? Judge.getTLetterJudgement().trim()=="*" ? "kome" : Judge.getTLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>_x2.png" style="width:100px; height:100px" />
	                           	</div>
	                        </div>
                            <div class="myscore">
                                <p><span class="point">総合ポイント</span><span class="text-19 total-indent01"><%= encTableHAI(Judge.getT_scoreStr()) %></span></p>
                                <p><span class="point">配点ウェイト（点）</span><br><span class="text-16"><span class="total-indent03">共通テスト</span>：<c:out value="${Judge.CAllotPntRateAll}"/><br><span class="total-indent03">２次・個別</span>：<c:out value="${Judge.SAllotPntRateAll}"/></span></p>
                            </div><!-- /.myscore -->
                        </div><!-- /.section-inner -->
                    </div><!-- /.text-inner -->
                </div><!-- /#section03 -->
</c:if>
            </div><!-- /.section-container -->

<c:if test="${ Judge.scheduleCd != ' ' }">
            <div id="distribution">
                <h4 class="section-title">共通テスト&nbsp;&nbsp;得点分布</h4>
                <div id="tableID" class="text-inner">
                    <!--ここにグラフを表示します-->
                    <c:if test="${ not empty Judge.candidates }">
                    	<%@ include file="/jsp/include/j205graph.jsp" %>
                    </c:if>
                </div><!-- /.text-inner -->
                <p style="margin:10px 0px 20px 20px;"><span class="point">出願予定者</span>&nbsp;&nbsp;&nbsp;<span class="text-19"><%= encTableHAI(Judge.getCandidates()) %>名</span><span class="text-16">（平均点<%= encTableHAI(Judge.getAverage()) %>点）</span></p>
            </div><!-- /#distribution -->
</c:if>
            <div class="text-inner notes">
                <p>入試科目等は、必ず大学発表の学生募集要項で確認してください。</p>
                <p class="small"><span class="kome">※</span><span class="indent">大学の指定科目・配点が複数パターンある場合、最も高い成績になるパターンで判定を行っています。</span></p>
                <p class="small"><span class="kome">※</span><span class="indent">小論文や総合問題等を課す場合、全統模試では他の教科で代替して判定を行っているケースがあります。</span></p>
            </div>

        </div><!-- /.contents-inner -->
        <div class="contents-inner">
            <div class="close-btn btn">
                <input type="button" name="" value="" onclick="javascript:window.close()" id="close-btn">
            </div><!-- /.submit-btn.btn -->
        </div><!-- /.contents-inner -->

    </div><!-- /.contents -->
</div><!-- /.container -->
<!-- コンテンツここまで -->

<%-- FOOTER --%>
<%@ include file="/jsp/include/footer.jsp" %>
<%-- /FOOTER --%>

</form>
</div>
</body>
</html>
<%!
/**
 * 表の中に””（空文字）があるときに"--"を設定してくれるメソッド（ＮＮ対応の場合は必ずとおす）
 * encは実行済み
 * @param	value  表内に設定する値Value値
 * @return	"value" または "&nbsp;"
 */
public String encTableHAI(String value){
    if(value == null || value.equals("")){
        return ("--");
      }else{
        return (enc(value));
      }
}
 /**
  * 募集人員の表示内容を引数の値によって変更させるメソッド
  * encは実行済み
  * @param	examCapaRel  入試定員信頼性
  * @param	capacity  募集人員
  * @return	募集人員
  */
 public String changeNOS(String examCapaRel, String capacity){
 	if (examCapaRel.equals("8") || capacity.equals("0")) {
 		return "--";
 	} else if (examCapaRel.equals("9")) {
 		return "推定" + enc(capacity) + "名";
 	/**} else if (capacity.equals("0") && !examCapaRel.equals("8")) {
		return "若干";*/
 	} else {
 		return enc(capacity) + "名";
 	}
 }
private static String getAction(int Flg){
 if(Flg == 1){
   return("BranchfavoriteDetail");
  }else{
   return("BranchlistDetail");
  }
}
public String getBranchNumber(int index){
  return (JudgementUtil.fillZero(index+1));
}
/**
 *
 * @param	strSrc
 * @return
 */
public static final String enc(String strSrc){
  int				nLen;
  if(strSrc == null || (nLen = strSrc.length()) <= 0)
  return "";

  StringBuffer	sbEnc = new StringBuffer(nLen * 2);

  for(int i = 0; i < nLen; i++){
    char	c;
    switch(c = strSrc.charAt(i)){
      case '<':	sbEnc.append("&lt;");	break;
      case '>':	sbEnc.append("&gt;");	break;
      case '&':	sbEnc.append("&amp;");	break;
      case '"':	sbEnc.append("&quot;");	break;
      case '\'':sbEnc.append("&#39;");	break;
      case '\\':sbEnc.append("&yen;");	break;
      default: sbEnc.append(c); break;
    }
  }
  return sbEnc.toString();
}
/**
 *
 * @param	value
 * @return	"value"
 */
public static final String encTableNBSP(String value){
  if(value.equals("")){
    return ("&nbsp;");
  }else{
    return (enc(value));
  }
}
%>

<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld"%>
<c:set var="simpleLayout" value="true" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<title>バンザイシステム</title>
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j001pc.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j001sp.css" media="(max-width:640px)">

<!--[if lt IE 9]>
		<script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
		<![endif]-->
<script src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js" type="text/javascript"></script>
<script src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js" type="text/javascript"></script>
<script src="<c:out value="${cacheServerUrl}" />js/footerFixed.js" type="text/javascript"></script>
<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/OpenWindow.js"></script>
<script type="text/javascript">
	//SP用バナーランダム表示jQuery
	$(function() {
		$.fn.extend({
			randomdisplay : function(num) {
				return this.each(function() {
					var chn = $(this).children().hide().length;
					for (var i = 0; i < num && i < chn; i++) {
						var r = parseInt(Math.random() * (chn - i)) + i;
						$(this).children().eq(r).show().prependTo($(this));
					}
				});
			}
		});
		$(function() {
			$("[randomdisplay]").each(function() {
				$(this).randomdisplay($(this).attr("randomdisplay"));
			});
		});
	});
	var form;
<%@ include file="/jsp/script/submit_exit.jsp" %>
	function init() {
		var msg = "<c:out value="${ErrorMessage}" />";
		if (msg != "")
			alert(msg);
	}
<%@ include file="/jsp/script/submit_menu.jsp" %>
	// Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
	if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
		document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
	}
</script>
</head>

<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">

	<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="Top" />" method="POST">
		<c:set var="submitTop" value="false" />
		<input type="hidden" name="forward" value="j101" />
		<input type="hidden" name="backward" value="j001" />
		<input type="hidden" name="sslFlg" value="true" />
		<!-- ヘッダーここから -->
		<%@ include file="/jsp/include/header.jsp"%>
		<!-- ヘッダーここまで -->


			<!-- コンテンツここから -->
			<div class="container">
				<div class="contents line">
					<div class="contents-inner">
						<div class="text-inner pickup">
						<p>大学入学共通テストの自己採点得点を入力して、志望大学の合格可能性判定や、合格可能性の高い大学の検索ができます。</p>
						<p style="color:#FF0000">1/20(金)に発表された大学入学共通テストの得点調整にともない、一部の大学のボーダーラインが変更となる可能性があります。</p>
						<p style="color:#FF0000">変更後のボーダーラインは、1/21(土)夕方に改めて掲載をする予定です。</p>
					</div>
						<div class="btn submit-btn">
							<input type="submit" name="" value="" id="start-btn">
							<div class="chuui">
								<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/chuui_icon.png"> <a href="https://www.keinet.ne.jp/center/banzai/info.pdf" target="_blank">注意が必要な大学</a>
							</div>
						</div>
						<!-- /.contents-inner -->
					</div>
					<%-- banner--%>
					<%@ include file="/jsp/include/banner.jsp"%>
					<%-- banner --%>
					<div class="contents-inner next cl-sp">
						<div class="text-inner">
							<h3>
								<img src="<c:out value="${cacheServerUrl}" />shared_lib/img/sakura_index_icon.png" height="20" width="20" alt="見出し桜アイコン">&nbsp;共通テスト関連コンテンツ
							</h3>
							<!-- /.text-inner -->
						</div>
						<ul>
							<li><a href="https://www.keinet.ne.jp/sp/center/">大学入学共通テスト特集</a></li>
							<li><a href="https://www.keinet.ne.jp/sp/rank/">入試難易予想ランキング表</a></li>
							<li><a href="https://www.keinet.ne.jp/sp/mail/">メールマガジン&nbsp;Success&nbsp;Mail</a></li>
						</ul>
						<!-- /.contents-inner -->
					</div>
					<!-- /.contents -->
				</div>

				<!-- /.clear .container -->
			</div>
			<!-- コンテンツここまで -->
			<!-- フッターここから -->
			<%@ include file="/jsp/include/footer.jsp"%>
			<!-- フッターここまで -->

			<!-- /#wrapper -->
		</div>
	</form>
</body>
</html>

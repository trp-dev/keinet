<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8; IE=EmulateIE10; IE=EmulateIE11">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
    <title>登録大学 バンザイシステム</title>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j301pc.css">
    <!-- [if IE 9]>
    <script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery.tablehover.min.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/footerFixed.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/DOMUtil.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/Browser.js"></script>
    <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/SwitchLayer.js"></script>
<%-- テーブル関連スクリプト --%>
<c:set var="bean" value="${useListBean2}" />
<%@ include file="/jsp/include/result_script.jsp" %>
<script type="text/javascript">
<!--
jQuery(function($) {
	$('th[data-href]').addClass('clickable')
		.click(function(e) {
			if(!$(e.target).is('a')){
				window.location = $(e.target).closest('th').data('href');
			};
		});
});
jQuery( function() {
    jQuery( '.jquery-tablehover' ) . tableHover();
} );

	<%@ include file="/jsp/script/submit_menu.jsp" %>
	<%@ include file="/jsp/script/submit_exit.jsp" %>
	<%@ include file="/jsp/script/open_detail.jsp" %>

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

	<%-- GLOBAL --%>
	var form;
	var sw = new SwitchLayer();

	function deleteUniv(key) {
		if (confirm("<kn:message id="j009a" />")) {
			form.mode.value = "1";
			form.uniqueKey.value = key;
			submitMenu("j301");
		}
	}

	function deleteAllUniv() {
		if (confirm("<kn:message id="j010a" />")) {
			form.mode.value = "2";
			submitMenu("j301");
		}
	}

    // トップ画面への遷移
    function submitTop(forward) {
    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
    		location.href = "<c:out value="${topUrl}" />";
		}

	}
	// 2015/09/08 CCC堀畑 add start
	// 各ソート項目が押下された場合に該当するソート項目の背景色を変更する
 	function chgColor(c){
 		var num = c;
 		if(document.getElementById){
  		switch (num) {
  	case 0:
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "clickable overall link-width";
    document.getElementById("col4").className = "clickable link-width";
    document.getElementById("col0").className = "link01 name-title clickable";
    document.getElementById("col5").className = "mytable-link1 clickable link-width";
    break;
  	case 1:
    document.getElementById('col1').className = "link01 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "clickable overall link-width";
    document.getElementById("col4").className = "clickable link-width";
    document.getElementById("col5").className = "mytable-link1 clickable link-width";
    break;
    case 2:
    document.getElementById("col2").className = "link01 mytable-link1 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col3").className = "clickable overall link-width";
    document.getElementById("col4").className = "clickable link-width";
    document.getElementById("col5").className = "mytable-link1 clickable link-width";
    break;
    case 3:
    document.getElementById("col3").className = "link01 clickable overall link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col4").className = "clickable link-width";
    document.getElementById("col5").className = "mytable-link1 clickable link-width";
    break;
    case 4:
    document.getElementById("col4").className = "link01 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "clickable overall link-width";
    document.getElementById("col5").className = "mytable-link1 clickable link-width";
    break;
    case 5:
    document.getElementById("col5").className = "link01 mytable-link1 clickable link-width";
    document.getElementById("col0").className = "name-title clickable";
    document.getElementById("col1").className = "clickable link-width";
    document.getElementById("col2").className = "mytable-link1 clickable link-width";
    document.getElementById("col3").className = "clickable overall link-width";
    document.getElementById("col4").className = "clickable link-width";
    break;
  	}
  }
}
	// 2015/09/07 CCC堀畑 add end

	// 2015/09/07 CCC堀畑 add start
	//「並び順を戻す」ボタンが押下された場合、ソート順、ソート項目の背景色を初期化
	function sortreset() {
		execSort(0);
		document.getElementById("col0").className = "name-title clickable";
    	document.getElementById("col1").className = "clickable link-width";
    	document.getElementById("col2").className = "mytable-link1 clickable link-width";
    	document.getElementById("col3").className = "clickable overall link-width";
    	document.getElementById("col4").className = "clickable link-width";
    	document.getElementById("col5").className = "mytable-link1 clickable link-width";
	}
	// 2015/09/08 CCC堀畑 add end
	function init() {
		form = document.forms[0];
		execSort(form.sortKey.value);
		window.scrollTo(document.forms[0].scrollX.value, document.forms[0].scrollY.value);
		sortinit1(form.sortKey.value);
	}

//画面遷移時にソート実行中の項目の背景色を変更する
	function sortinit1(num){
		var sortnum = num
		if(sortnum == 0){
		}else if(sortnum == 1){
			document.getElementById("col0").className = "link01 name-title clickable";
		}else if(sortnum == 2){
			document.getElementById('col1').className = "link01 clickable link-width";
		}else if(sortnum == 3){
			document.getElementById("col2").className = "link01 mytable-link1 clickable link-width";
		}else if(sortnum == 4){
			document.getElementById("col3").className = "link01 clickable overall link-width";
		}else if(sortnum == 5){
			document.getElementById("col4").className = "link01 clickable link-width";
		}else if(sortnum == 6){
			document.getElementById("col5").className = "link01 mytable-link1 clickable link-width";
		}
	}
// 2015/09/15 CCC堀畑 add end

	<%-- 画面遷移(戻るボタン押下時) --%>
	function submitMenuBack(forward) {
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "j301";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
// -->
</script>
</head>
<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="UnivList" />?sid=<c:out value="${serverId}" />" method="POST">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="uniqueKey" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="judgeNo" value="2">
<input type="hidden" name="sortKey" value="<c:out value="${form.sortKey}" default="0" />">
<input type="hidden" name="scrollX" value="<c:out value="${form.scrollX}" />">
<input type="hidden" name="scrollY" value="<c:out value="${form.scrollY}" />">
<div id="wrapper"></div>

<%-- HEADER --%>
<%@ include file="/jsp/include/header.jsp" %>
<%-- /HEADER --%>

<!-- コンテンツここから -->
<div class="container">
    <div class="contents">

        <!-- 大見出し ここから -->
        <h3 class="title01">登録大学</h3>
        <!-- 大見出し ここまで -->

	<c:choose>
	<c:when test="${ empty UnivList }">
    <!-- 検索 0件 -->

        <style type="text/css">
        <!--
        .pagetop { display: none; }
        .contents-inner{
    		padding: 10px;
		}
        -->
        </style>
        <div class="contents-inner">
            <div class="text-inner error-text">
                <p>現在登録されている大学はありません。</p>
            </div><!-- /.text-inner -->
        </div><!-- /.contents-inner -->

	</c:when>
	<c:otherwise>
    <!-- 通常  -->

        <div class="contents-inner info">

            <div class="text-inner301">
				<p>大学名をクリックすると、詳細情報が確認できます。</p>
				<p>[！] マークが表示されている大学は、出願要件や判定方法などについて注意が必要な大学です。</p>
				<p>大学名をクリックして詳細画面をご確認いただくとともに、入試科目等は必ず大学発表の学生募集要項でご確認ください。</p>
				<p class="attention">※登録大学は、バンザイシステムご利用中のみ有効です。バンザイシステムを終了すると、リセットされます。</p>
            </div><!-- /.text-inner -->
            <div>
            	<p>
                <input type="button" " style="margin-left:819px;" name="" value="" onClick="sortreset()" id="sort-reset01" />
            	</p>
            </div><!-- /.btn.allcheck-btn.cl-pc -->
            <table class="mytable jquery-tablehover">
                <thead>
                    <tr>
                    <th rowspan="2" class="name-title clickable" data-href="javascript:execSort(1)"onclick="chgColor(0)" id="col0" ><a href="javascript:execSort(1)">大　学</a></th>
                    <th colspan="2">共通テスト</th>
                    <th colspan="2">2次・個別</th>
                    <th colspan="2" class="right-line">評価</th>
                    <th>&nbsp;</th>
                    <th colspan="2">配点比率</th>
                    <th rowspan="2" class="list06">第1段階選抜</th>
                    <th rowspan="2" class="list07">削除</th>
                    </tr>
                    <tr>
                    <th class="list02">ボーダー<br>得点(率)</th>
                    <th class="list03">あなたの<br>得点</th>
                    <th class="list04">ボーダー<br>偏差値</th>
                    <th class="list05">あなたの<br>偏差値</th>
                    <th class="clickable link-width" data-href="javascript:execSort(2)" onclick="chgColor(1)" id="col1"><a href="javascript:execSort(2)">共通<br>テスト</a></th>
                    <th class="mytable-link1 clickable link-width" data-href="javascript:execSort(3)" onclick="chgColor(2)" id="col2"><a href="javascript:execSort(3)">2次・<br>個別</a></th>
                    <th class="clickable overall link-width" data-href="javascript:execSort(4)" onclick="chgColor(3)" id="col3"><a href="javascript:execSort(4)">総合</a></th>
                    <th class="clickable link-width" data-href="javascript:execSort(5)" onclick="chgColor(4)" id="col4"><a href="javascript:execSort(5)">共通<br>テスト</a></th>
                    <th class="clickable mytable-link1 link-width" data-href="javascript:execSort(6)" onclick="chgColor(5)" id="col5"><a href="javascript:execSort(6)">2次・<br>個別</a></th>
                    </tr>
                </thead>
                <tbody id="ListTBody">
                <c:forEach var="univ" items="${bean.recordSetAll}">
                    <tr>
                    <td class="mytable-title"><span class="text12"></span></td>
                    <td><span class="text12"></span></td>
                    <td><strong><span class="text12"></span></strong></td>
                    <td><span class="text12"></span></td>
                    <td><strong><span class="text12"></span></strong></td>
                    <td><strong><span class="text12"></span></strong></td>
                    <td><strong><span class="text12"></span></strong></td>
                    <td><strong><span class="text12"></span></strong></td>
                    <td><span class="text12"></span></td>
                    <td><span class="text12"></span></td>
                    <td><span class="text12"></span></td>
                    <td><span class="text12"></span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

        </div><!-- /.contents-inner -->

        <div class="btn alldelete-btn">
            <input type="button" name="" value="" onclick="deleteAllUniv()" id="all-delete-btn">
        </div><!-- /.btn.allcheck-btn.cl-pc -->

	</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="${ param.backward == 'j204' }">
			<div class="back-btn btn">
				<input type="button" name="" onclick="javascript:submitMenuBack('j204');" id="back-btn">
			<!-- /.submit-btn.btn --></div>
		</c:when>
	</c:choose>
    </div><!-- /.contents -->
</div><!-- /.container -->
<!-- コンテンツここまで -->

<%-- FOOTER --%>
<%@ include file="/jsp/include/footer.jsp" %>
<%-- /FOOTER --%>

</form>
</body>
</html>

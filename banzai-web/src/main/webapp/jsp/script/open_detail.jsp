<%-- 詳細画面を開く --%>
function openDetail(key) {
	if ((window.navigator.userAgent.indexOf('iPhone OS') == -1) && (window.navigator.userAgent.indexOf('Edge') == -1) && (window.navigator.userAgent.indexOf('Android') == -1)) {
		var op = window.open("", "j205", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
		op.focus();
	}
	document.forms[0].forward.value = "j205";
	document.forms[0].backward.value = "<c:out value="${param.forward}" />";
	document.forms[0].uniqueKey.value = key;
	if (window.navigator.userAgent.indexOf('iPhone OS') == -1 && window.navigator.userAgent.indexOf('Edge') == -1 && window.navigator.userAgent.indexOf('Android') == -1) {
		document.forms[0].target = "j205";
	} else {
		document.forms[0].target = "_blank";
	}
	document.forms[0].submit();
}

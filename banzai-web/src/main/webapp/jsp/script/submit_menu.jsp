<%-- （汎用的）次の画面への遷移 --%>
function submitMenu(forward) {
	if (forward =="<c:out value="${param.forward}" />") {
		if (document.forms[0].scrollX) document.forms[0].scrollX.value = document.body.scrollLeft;
		if (document.forms[0].scrollY) document.forms[0].scrollY.value = document.body.scrollTop;
	} else {
		if (document.forms[0].scrollX) document.forms[0].scrollX.value = 0;
		if (document.forms[0].scrollY) document.forms[0].scrollY.value = 0;
	}
	document.forms[0].forward.value = forward;
	document.forms[0].backward.value = "<c:out value="${param.forward}" />";
	document.forms[0].target = "_self";
	document.forms[0].submit();
}

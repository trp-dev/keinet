<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%-- マーク系の模試名称 --%>
<c:choose>
<c:when test="${ExamBean.centerPre}"><c:set var="MarkExamName" value="全統共通テストプレテスト" /></c:when>
<c:otherwise><c:set var="MarkExamName" value="全統マーク模試" /></c:otherwise>
</c:choose>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
		<title>成績入力 バンザイシステム</title>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j101pc.css">
		<!--[if IE 8]>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j101pcie8.css"/>
		<![endif]-->
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j101sp.css" media="(max-width:640px)">
		<!--[if lt IE 9]>
		<script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
		<![endif]-->
		<script src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js" type="text/javascript"></script>
		<script src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js" type="text/javascript"></script>

		<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/Browser.js"></script>
		<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/SwitchLayer.js"></script>
		<script src="./js/j101.js" type="text/javascript"></script>
	<script type="text/javascript">
	<!--

    <%@ include file="/jsp/script/submit_exit.jsp" %>

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

    <%-- GLOBAL --%>
    var form;
    var valid = true;
    var sw = new SwitchLayer();
    var max = new Array();
    max["t1En1Score"] = 100;
    max["t1En2Score"] = 100;
    max["t1Ma1Score"] = 100;
    max["t1Ma2Score"] = 100;
    max["t1Ja1Score"] = 100;
    max["t1Ja2Score"] = 50;
    max["t1Ja3Score"] = 50;
    max["t1Sc1Score"] = 100;
    max["t1Sc2Score"] = 100;
    max["t1Sc3Score"] = 50;
    max["t1Sc4Score"] = 50;
    max["t1Gh1Score"] = 100;
    max["t1Gh2Score"] = 100;

    // 入力チェック
    function validate() {
        var t1 = getFormElements("t1");
        var t2 = getFormElements("t2")
        valid = true;

        // エラーメッセージはクリアしておく
         clearErrorMessage(t1);
         clearErrorMessage(t2);
        // テーブル１のチェック
        checkSubject(t1)
        checkScore(t1);
		/* checkJpnDesc(); */
        // 科目重複チェック
        if (isConflict("t1Sc3Subject", "t1Sc4Subject")) {
            setErrorMessage("t1Sc4Subject", "<kn:message id="j004a" />");
            setErrorMessage("t1Sc4SubjectSP", "<kn:message id="j004a" />");
        }
        if (isConflict("t1Sc2Subject", "t1Sc1Subject")) {
            setErrorMessage("t1Sc2Subject", "<kn:message id="j004a" />");
            setErrorMessage("t1Sc2SubjectSP", "<kn:message id="j004a" />");
        }
        if (isConflict("t1Gh2Subject", "t1Gh1Subject")) {
            setErrorMessage("t1Gh2Subject", "<kn:message id="j004a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j004a" />");
        }


        // 理科基礎科目選択チェック
        checkBasic();

        checSciOldCourse();

        // 1科目め入力チェック
        checkAns1stSc();
        checkAns1stGh();

         // 同一名称を含む科目チェック(地歴・公民)
    	checkKamokuNameGh();

        // テーブル２のチェック
        checkSubject(t2)
        checkDeviation(t2);


        // 科目重複チェック
        if (isConflict("t2Sc2Subject", "t2Sc1Subject", false)) {
           	setErrorMessage("t2Sc2Subject", "<kn:message id="j004a" />");
           	setErrorMessage("t2Sc2SubjectSP", "<kn:message id="j004a" />");
        }

        if (isConflict("t2Gh2Subject", "t2Gh1Subject")) {
            setErrorMessage("t2Gh2Subject", "<kn:message id="j004a" />");
            setErrorMessage("t2Gh2SubjectSP", "<kn:message id="j004a" />");
        }



        // リスニングフラグチェック
        if (form.listeningFlag.checked && form.t2EnDev.value == "" && !form.listeningFlag.disabled) {
            setErrorMessage("t2EnDev", "<kn:message id="j002a" />");
            setErrorMessage("t2EnDevSP", "<kn:message id="j002a" />");
        }

        return valid;
    }



    // 引数が数値であるかどうか
    function isNumber(value) {
        return value.match(/^(([0-9]+\.[0-9]*)|(\.[0-9]+)|([0-9]+))$/);
    }

    // 引数が整数であるかどうか
    function isInteger(value) {
        return value.match(/^[0-9]+$/);
    }

    // 科目をチェックする
    function checkSubject(c) {

        for (var i=0; i<c.length; i++) {
            if (c[i].name.length < "Subject".length) {
                continue;
            }

		if (c[i].name.substring(c[i].name.length - "Subject".length) == "Subject" && !c[i].disabled) {
                var prefix = c[i].name.substring(0, c[i].name.length - "Subject".length);
                var score = form.elements[prefix + "Score"];
                var dev = form.elements[prefix + "Dev"];

                // 未選択チェック
                // 科目の得点または偏差値に入力があるならエラー
                var e1 = form.elements[c[i].name];
                if (e1[0].checked) {
                    if ((score && score.value != "") || (dev && !dev.disabled && dev.value != "")) {
                        setErrorMessage(c[i].name, "<kn:message id="j003a" />");
                        setErrorMessage(c[i].name + "SP", "<kn:message id="j003a" />");
                        var obj = form.elements[c[i].name + "SP1"];
                        if (obj) {
                        	setErrorMessage(c[i].name + "SP1", "<kn:message id="j003a" />");
                        }
                    }
                // 未入力チェック
                } else {
                    if ((!score || (score && score.value == "")) && (!dev || dev.disabled || (dev && dev.value == ""))) {
                        if (score) {
                        	setErrorMessage(score.name, "<kn:message id="j002a" />");
                        	setErrorMessage(score.name + "SP", "<kn:message id="j002a" />");
                        }
                        if (dev && !dev.disabled) {
                        	setErrorMessage(dev.name, "<kn:message id="j002a" />");
                        	setErrorMessage(dev.name + "SP", "<kn:message id="j002a" />");
                        }
                    }
                }
            }
        }
    }

    // 使用しているブラウザの判定
	function checkuserAgent() {
		var userAgent = window.navigator.userAgent.toLowerCase();

		if (userAgent.indexOf('opera') != -1) {
  			return "opera";
		} else if (userAgent.indexOf('msie') != -1) {
  			return "ie";
		} else if (userAgent.indexOf('chrome') != -1) {
  			return "chrome";
		} else if (userAgent.indexOf('safari') != -1) {
  			return "safari";
		} else if (userAgent.indexOf('firefox') != -1) {
  			return "firefox";
  		} else if (userAgent.indexOf('trident') != -1) {
  			return "trident";
		} else {
  			return false;
		}

	}

    // 得点をチェックする
    function checkScore(c) {
        for (var i=0; i<c.length; i++) {
            if (c[i].name.length < "Score".length) {
                continue;
            }
            if (checkuserAgent() != "ie") {
            	if (c[i].value == "" && c[i].validity.valid) {
                	continue;
            	}
            } else {
            	if (c[i].value == "") {
                	continue;
            	}
            }
            if (c[i].name.substring(c[i].name.length - "Score".length) == "Score" && !c[i].disabled) {
                if (isInteger(c[i].value)) {
                    if (c[i].value > max[c[i].name]) {
                        setErrorMessage(c[i].name, max[c[i].name] + "<kn:message id="j005a" />");
                        setErrorMessage(c[i].name + "SP", max[c[i].name] + "<kn:message id="j005a" />");
                    } else if (c[i].value < 0) {
                        setErrorMessage(c[i].name, "<kn:message id="j012a" />");
                        setErrorMessage(c[i].name + "SP", "<kn:message id="j012a" />");
                    }
                } else {
                    setErrorMessage(c[i].name, "<kn:message id="j002a" />");
                    setErrorMessage(c[i].name + "SP", "<kn:message id="j002a" />");
                }
            }

            if (c[i].name.substring(c[i].name.length - "ScoreSP".length) == "ScoreSP" && document.forms[0].device.value == "SP") {
                if (isInteger(c[i].value)) {
                    if (c[i].value > max[c[i].name]) {
                        setErrorMessage(c[i].name, max[c[i].name] + "<kn:message id="j005a" />");
                    } else if (c[i].value < 0) {
                        setErrorMessage(c[i].name, "<kn:message id="j012a" />");
                    }
                } else {
                    setErrorMessage(c[i].name, "<kn:message id="j002a" />");
                }
            }

        }
    }

    // 偏差値をチェックする
    function checkDeviation(c) {
        for (var i=0; i<c.length; i++) {
            if (c[i].name.length < "Dev".length) {
                continue;
            }
            if (checkuserAgent() != "ie") {
            	if (c[i].value == "" && c[i].validity.valid) {
                	continue;
            	}
            } else {
            	if (c[i].value == "") {
                	continue;
            	}
            }
            if (c[i].name.substring(c[i].name.length - "Dev".length) == "Dev" && !c[i].disabled) {
            	if (isNumber(c[i].value)) {
                    if (c[i].value > 199.9) {
                        setErrorMessage(c[i].name, "199.9" + "<kn:message id="j005a" />");
                        setErrorMessage(c[i].name + "SP", "199.9" + "<kn:message id="j005a" />");
                    } else if (c[i].value < 0) {
                        setErrorMessage(c[i].name, "<kn:message id="j012a" />");
                        setErrorMessage(c[i].name + "SP", "<kn:message id="j012a" />");
                    }
                } else {
                    setErrorMessage(c[i].name, "<kn:message id="j002a" />");
                    setErrorMessage(c[i].name + "SP", "<kn:message id="j002a" />");
                }
            }

            if (c[i].name.substring(c[i].name.length - "DevSP".length) == "DevSP" && document.forms[0].device.value == "SP") {
            	if (isNumber(c[i].value)) {
                    if (c[i].value > 199.9) {
                        setErrorMessage(c[i].name, "199.9" + "<kn:message id="j005a" />");
                    } else if (c[i].value < 0) {
                        setErrorMessage(c[i].name, "<kn:message id="j012a" />");
                    }
                } else {
                    setErrorMessage(c[i].name, "<kn:message id="j002a" />");
                }
            }
        }
    }

    // 重複しているかどうか
    function isConflict(name1, name2, checkNameFlg) {

        var e1 = form.elements[name1];
        var e2 = form.elements[name2];

        var e1checkval = 0;
        var e2checkval = 0;

        for(var i=0;i<e1.length;i++) {
        	if (e1[i].checked) {
        		e1checkval = i;
        	}
        }

        for(var i=0;i<e2.length;i++) {
        	if (e2[i].checked) {
        		e2checkval = i;
        	}
        }

        // 共に未選択なら重複はしていない
        if (e1checkval == 0 || e2checkval == 0) {
            return false;
        }
        // どちらかが無効なら重複はしていないこととする
        if (e1[0].disabled || e2[0].disabled) {
            return false;
        }

        // 科目名称の重複をチェックする
        if (checkNameFlg) {
            var name1 = e1[e1checkval].text;
            var name2 = e2[e2checkval].text;
            return name1.substring(0, 2) == name2.substring(0, 2);
        }
        return e1checkval == e2checkval;
    }

    // 理科基礎科目選択チェック
    function checkBasic() {
    	var sc1 = form.elements["t1Sc1Subject"];
    	var sc2 = form.elements["t1Sc2Subject"];
    	var sc3 = form.elements["t1Sc3Subject"];
    	var sc4 = form.elements["t1Sc4Subject"];

        if (!sc1[0].checked && !sc2[0].checked
                && (!sc3[0].checked || !sc4[0].checked)) {
            setErrorMessage("t1Sc2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Sc2SubjectSP", "<kn:message id="j030a" />");
        }
    }

      function checSciOldCourse() {
        var errorFlg = false;

        var oldCourseFlg1 = isOldSci("t1Sc1Subject");
        var oldCourseFlg2 = isOldSci("t1Sc2Subject");
        if (oldCourseFlg1 && isNewSci("t1Sc2Subject")) {
            errorFlg = true;
            setErrorMessage("t1Sc2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Sc2SubjectSP", "<kn:message id="j030a" />");
        }

        if (oldCourseFlg2 && isNewSci("t1Sc1Subject")) {
            errorFlg = true;
            setErrorMessage("t1Sc1Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Sc1SubjectSP", "<kn:message id="j030a" />");
        }

		var e1 = form.elements["t1Sc3Subject"];
   		var aindex = 0;
    	for(var i=0;i<e1.length;i++) {
        	if (e1[i].checked) {
        		aindex = e1[i].value;
        	}
        }

        var e2 = form.elements["t1Sc4Subject"];
   		var bindex = 0;
    	for(var j=0;j<e2.length;j++) {
        	if (e2[j].checked) {
        		bindex = e2[j].value;
        	}
        }



        if (oldCourseFlg1 || oldCourseFlg2) {
            if (aindex > 0) {
                errorFlg = true;
                setErrorMessage("t1Sc3Subject", "<kn:message id="j030a" />");
                setErrorMessage("t1Sc3SubjectSP", "<kn:message id="j030a" />");
            }
            if (bindex > 0) {
                errorFlg = true;
                setErrorMessage("t1Sc4Subject", "<kn:message id="j030a" />");
                setErrorMessage("t1Sc4SubjectSP", "<kn:message id="j030a" />");
            }
        }

        if (errorFlg) {
            if (oldCourseFlg1) {
                setErrorMessage("t1Sc1Subject", "<kn:message id="j030a" />");
                setErrorMessage("t1Sc1SubjectSP", "<kn:message id="j030a" />");
            }
            if (oldCourseFlg2) {
                setErrorMessage("t1Sc2Subject", "<kn:message id="j030a" />");
                setErrorMessage("t1Sc2SubjectSP", "<kn:message id="j030a" />");
            }
        }
    }


    function isOldSci(subject) {
   		var e1 = form.elements[subject];
   		var aval = 0;
    	for(var i=0;i<e1.length;i++) {
        	if (e1[i].checked) {
        		aval = e1[i].value;
        	}
        }
        return aval >= 5;
    }


    function isNewSci(subject) {
    	var e1 = form.elements[subject];
   		var aindex = 0;
    	for(var i=0;i<e1.length;i++) {
        	if (e1[i].checked) {
        		aindex = e1[i].value;
        	}
        }
        return aindex > 0 && !isOldSci(subject);
    }

    // 1科目め入力チェック（理科）
    function checkAns1stSc() {
    	var sc1 = form.elements["t1Sc1Subject"];
    	var sc2 = form.elements["t1Sc2Subject"];
        if (!sc2[0].checked && form.t1Sc2Score.value != "") {
            if (sc1[0].checked && form.t1Sc1Score.value == "") {
                setErrorMessage("t1Sc1Subject", "<kn:message id="j029a" />");
                setErrorMessage("t1Sc1SubjectSP", "<kn:message id="j029a" />");
            }
        }
    }

    // 1科目め入力チェック（地歴・公民）
    function checkAns1stGh() {
    	var gh1 = form.elements["t1Gh1Subject"];
    	var gh2 = form.elements["t1Gh2Subject"];
        if (!gh2[0].checked && form.t1Gh2Score.value != "") {
            if (gh1[0].checked && form.t1Gh1Score.value == "") {
                setErrorMessage("t1Gh1Subject", "<kn:message id="j029a" />");
                setErrorMessage("t1Gh1SubjectSP", "<kn:message id="j029a" />");
            }
        }
    }

	 // 同一名称を含む科目チェック(地歴・公民)
	function checkKamokuNameGh() {
		var gh1 = form.elements["t1Gh1Subject"];
    	var gh2 = form.elements["t1Gh2Subject"];

		// 世界史Aと世界史Bを選択していた場合
		if ((gh1[1].checked && gh2[2].checked) || (gh1[2].checked && gh2[1].checked)) {
			setErrorMessage("t1Gh2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j030a" />");
		}

		// 日本史Aと日本史Bを選択していた場合
		if ((gh1[3].checked && gh2[4].checked) || (gh1[4].checked && gh2[3].checked)) {
			setErrorMessage("t1Gh2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j030a" />");
		}

		// 地理Aと地理Bを選択していた場合
		if ((gh1[5].checked && gh2[6].checked) || (gh1[6].checked && gh2[5].checked)) {
			setErrorMessage("t1Gh2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j030a" />");
		}

		// 倫理と倫理，政治・経済を選択していた場合
		if ((gh1[9].checked && gh2[7].checked) || (gh1[7].checked && gh2[9].checked)) {
			setErrorMessage("t1Gh2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j030a" />");
		}

		// 政治・経済と倫理，政治・経済を選択していた場合
		if ((gh1[10].checked && gh2[7].checked) || (gh1[7].checked && gh2[10].checked)) {
			setErrorMessage("t1Gh2Subject", "<kn:message id="j030a" />");
            setErrorMessage("t1Gh2SubjectSP", "<kn:message id="j030a" />");
		}
	}

    // エラーメッセージをセットする
    function setErrorMessage(id, msg) {
    	if (!form.elements[id].disabled) {
	    	var indexSP = id.indexOf("SP");
    		var indexSubject = id.indexOf("Subject");
    		var indexDev = id.indexOf("Dev");
    		var indexScore = id.indexOf("Score");
    		var indext1Ma = id.indexOf("t1Ma");
    		var indext1Sc = id.indexOf("t1Sc");
    		var indext1Gh = id.indexOf("t1Gh");
    		var indext2Ma = id.indexOf("t2Ma");
    		var indext2Sc = id.indexOf("t2Sc");
    		var indext2Gh = id.indexOf("t2Gh");
    		var indexAA1 = id.indexOf("t2JaDev");
    		var indexAA2 = id.indexOf("t1Ma3Dev");
    		var indexAA3 = id.indexOf("t1Ma4Dev");
    		// 部品によってエラーメッセージの表示位置を調整
    		if (indexSP >= "0") {
    			if (indexSubject >= "0") {
	 		   		document.getElementById(id + "Error").style.padding="0px 26px";
	 		   	}
    		} else {
    			if (indexAA1 >= "0") {
    				document.getElementById(id + "Error").style.padding="0px 0px 12px 10px";
    			} else if (indexAA2 >= "0" || indexAA3 >= "0") {
    				document.getElementById(id + "Error").style.padding="0px 0px 12px 0px";
    			} else if ((indexDev >= "0" || indexScore >= "0" ) && indext1Ma < "0" && indext1Sc < "0" && indext1Gh < "0" && indext2Ma < "0" && indext2Sc < "0" && indext2Gh < "0") {
    				document.getElementById(id + "Error").style.padding="0px 0px 12px 0px";
    			} else {
    				document.getElementById(id + "Error").style.padding="0px 0px 12px 10px";
    			}
    		}

        	sw.getLayer(id + "Error").innerHTML = msg;
        	sw.getLayer(id + "Error").style.color = 'red';
        	sw.showLayer(id + "Error");

        	valid = false;
    	}
    }

    // フォームをクリアする
    function clearFormValue(prefix) {
        if (confirm("データをクリアしますか？")) {
            var c = getFormElements(prefix);
            for (var i=0; i<c.length; i++) {
                if (c[i].type == "text" || c[i].type == "number") {
                    c[i].value = "";
                } else if (c[i].type == "radio") {
                	var e1 = form.elements[c[i].name];
                	e1[0].checked = true;
                } else {
                    c[i].selectedIndex = 0;
                }
            }
            /* if (prefix == 't1') {
            	var pre = $("#pc").is(":visible") ? "pc-" : "sp-";
            	for (var i = 1; i <= 3; i++) {
            		var name = pre + "q" + i;
            		$('input[name=' + name + ']:eq(0)').prop('checked', true);
            	}
            	$("div.jpndesc").text("");
            	createPostData();
            } */
            clearErrorMessage(c);
            if (prefix == "t2") {
            	form.listeningFlag.checked = false;
            	form.listeningFlagSP.checked = false;
            }
            form.changed.value = "true";
        }
    }

 	// フォームをクリアする (SPでの画面遷移時のみ使用)
    function clearFormValueSP(prefix, key) {
 		if (key == "") {
 	      	var c = getFormElements(prefix);
 		} else {
 			var c = getFormElementslimited(prefix, key);
 		}
        for (var i=0; i<c.length; i++) {
        	if (c[i].type == "text" || c[i].type == "number") {
            	c[i].value = "";
            } else if (c[i].type == "radio") {
               	var e1 = form.elements[c[i].name];
               	e1[0].checked = true;
          	} else {
               	c[i].selectedIndex = 0;
            }
            if (prefix == 't1') {
            	createPostData();
            }
        	clearErrorMessage(c);
        	if (prefix == "t2") {
           		form.listeningFlag.checked = false;
           		form.listeningFlagSP.checked = false;
       		}
        	form.changed.value = "true";
 		}
    }

    // エラーメッセージをクリアする
    function clearErrorMessage(c) {
        for (var i=0; i<c.length; i++) {
            	sw.hideLayer(c[i].name + "Error");
        }
        /* $(".Ja0ScoreError").text(""); */
    }

    // テーブルのフォーム要素を取得する
    function getFormElements(prefix) {
        var c = new Array();
        var e = form.elements;
        for (var i=0; i<e.length; i++) {
        	if (e[i].type == "text" || e[i].type == "number" || e[i].type == "select-one" || e[i].type == "radio") {
                if (e[i].name.length >= 2) {
                    if (prefix == e[i].name.substring(0, 2)) {
                        c[c.length] = e[i];
                    }
                }
            }
        }
        return c;
    }

    // 指定したnameの条件に該当するテーブルのフォーム要素を取得する
    function getFormElementslimited(prefix, key) {
        var c = new Array();
        var e = form.elements;
        for (var i=0; i<e.length; i++) {
        	if (e[i].type == "text" || e[i].type == "number" || e[i].type == "select-one" || e[i].type == "radio") {
        		 var index = e[i].name.indexOf(key);
                if (e[i].name.length >= 2) {
                    if (prefix == e[i].name.substring(0, 2) && index >= "0") {
                        c[c.length] = e[i];
                    }
                }
            }
        }
        return c;
    }

    // 値を変更したときのアクション
    function changeForm(obj) {

        form.changed.value = "true";

        // PCとSPの値の同期
        var objname = obj.name;
        var indexSP = objname.indexOf("SP");
        var indexSP1 = objname.indexOf("SP1");
        var obj1 = "";
        var obj2 = "";
        //　反映元がPCかSPかを判定し、反映先の情報を取得
		if (indexSP >= 0) {
        	// 判定元がSPでnameに"SP"が含まれる場合
        	var obj1name = objname.split("SP").join("");
        	obj1 = form.elements[obj1name];
        } else {
        	// 判定元がPCの場合
        	obj1 = form.elements[obj.name + "SP"];
        }

        // 部品の種類で分岐
        if (obj.type == "radio") {
        	// ラジオボタンの場合
        	var objre = form.elements[objname];
			if (obj1) {
	        	for(var i=0;i<objre.length;i++) {
	        		obj1[i].checked = objre[i].checked;
	        	}
			}

		} else if (obj.type == "checkbox") {
			// チェックボックスの場合
			if (obj1) {
            	obj1.checked = obj.checked;
        	}

        } else {
        	// 上記以外の場合
        	if (obj1) {
            	obj1.value = obj.value;
        	}
        }
    }

    // 偏差値入力を有効化する
    function enableDeviation(c) {
        changeDevStyle(c, "#FFFFFF", false);
    }

    // 偏差値入力を無効化する
    function disableDeviation(c) {
        changeDevStyle(c, "#CCCCCC", "disabled");
    }

    // 科目・偏差値のスタイルを変える
    function changeDevStyle(c, bgColor, flag) {
        for (var i=0; i<c.length; i++) {
            if (c[i].name.substring(c[i].name.length - "Dev".length) == "Dev"
                || c[i].name.substring(c[i].name.length - "Subject".length) == "Subject") {
                    // 科目で得点があるものは除外する
                    if (!form.elements[c[i].name.substring(0, c[i].name.length - "Subject".length) + "Score"]) {
                        if (c[i].type != "radio") {
                    		c[i].style.backgroundColor = bgColor;
                        }
                        c[i].disabled = flag;
                        sw.hideLayer(c[i].name + "Error");
                    }
            }
        }
    }

    <%-- 初期化処理 --%>
    function init() {
        form = document.forms[0];
    }

 	// 次の画面への遷移 (共通)
    function submitMenuCommon(forward) {
        if (validate()) {
            document.forms[0].mode.value = "2";
            document.forms[0].forward.value = forward;
            document.forms[0].backward.value = "j101";
            document.forms[0].target = "_self";
            document.forms[0].submit();
        } else {
            alert("<kn:message id="j008a" />");
        }
    }

 	// 次の画面への遷移(PC)
    function submitMenu(forward) {
    	document.forms[0].device.value = "PC";
    	submitMenuCommon(forward);
    }

 	// 次の画面への遷移(SP)
    function submitMenuSP(forward) {
		document.forms[0].device.value = "SP";
		submitMenuCommon(forward);
    }

    // トップ画面への遷移(PC)
    function submitTop(forward) {
    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
    		location.href = "<c:out value="${topUrl}" />";
		}

	}

    // トップ画面への遷移(SP)
    function submitTopSP(forward) {
    	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
    		location.href = "<c:out value="${topUrl}" />";
		}

	}

// -->
</script>
</head>
<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="wrapper">
<input type="hidden" name="message" value='<c:out value="${applicationScope.message}" />'>
<form action="https://${sslServerAddress}${pageContext.request.contextPath}/<c:url value="InputScore" />?sid=<c:out value="${serverId}" />" method="POST" onsubmit="return false;">
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="mode" value="">
<input type="hidden" name="changed" value="false">
<input type="hidden" name="device" value="pc">
<input type="hidden" name="t1JaDescQ1" id="t1JaDescQ1" value='<c:out value="${J101Form.t1JaDescQ1}" />'>
<input type="hidden" name="t1JaDescQ2" id="t1JaDescQ2" value='<c:out value="${J101Form.t1JaDescQ2}" />'>
<input type="hidden" name="t1JaDescQ3" id="t1JaDescQ3" value='<c:out value="${J101Form.t1JaDescQ3}" />'>
<input type="hidden" name="t1JaDescQ4" id="t1JaDescQ4" value='<c:out value="${J101Form.t1JaDescQ4}" />'>

<%-- HEADER --%>
	<!-- ヘッダーここから -->
	<%@ include file="/jsp/include/header.jsp"%>
	<!-- ヘッダーここまで -->
<%-- /HEADER --%>

<div class="container">

<!-- PC用コンテンツここから -->
	<div id="pc" class="contents cl-pc">

		<div id="section01">
			<h3 class="title01">共通テスト　自己採点得点入力</h3>
				<div class="contents-inner">

					<div class="text-inner101">
						<p>共通テストの自己採点得点を<strong>半角数字で</strong>入力してください。</p>
					<!-- /.text-inner101 --></div>

					<div class="btn clear-btn">
						<p>
						<input type="button" name="" value="" onclick="clearFormValue('t1')" id="clear-btn01">
						</p>
					</div>

							<table class="table-pc">
								<thead>
									<tr>
										<th class="table-pc-title">教科</th>
										<th>科目</th>
										<th class="table-pc-title">共通テスト<br>自己採点得点</th>
									</tr>
								</thead>
								<tbody>
									<tr><!-- 英語01 -->
										<th rowspan="2">英語</th>
										<td class="table-pc-subjects"><p>リーディング</p></td>
										<td>
											<input type="number" maxlength="3" min="0" max="100" name="t1En1Score" value="<c:out value="${J101Form.t1En1Score}" />" onchange="changeForm(this)">
											<div id="t1En1ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 英語02 -->
										<td class="table-pc-subjects"><p>リスニング</p></td>
										<td>
											<input type="number" maxlength="3" min="0" max="100" name="t1En2Score" value="<c:out value="${J101Form.t1En2Score}" />" onchange="changeForm(this)">
											<div id="t1En2ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 数学01 -->
										<th rowspan="4">数学</th>
										<td class="table-pc-line table-pc-subjects">
                                            <p>数学【1】</p>
											<label><input type="radio" name="t1Ma1Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Ma1Subject" value="1"<c:if test="${ J101Form.t1Ma1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">数学I</label>
											<label><input type="radio" name="t1Ma1Subject" value="2"<c:if test="${ J101Form.t1Ma1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">数学I・数学A</label>
                                            <%-- Mizobata Edit Start --%>
                                            <%--  <label><input type="radio" name="t1Ma1Subject" value="3"<c:if test="${ J101Form.t1Ma1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">旧数学I</label>
											<label><input type="radio" name="t1Ma1Subject" value="4"<c:if test="${ J101Form.t1Ma1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">旧数学I・数学A</label>
											--%>
											<%-- Mizobata Edit End --%>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Ma1Score" value="<c:out value="${J101Form.t1Ma1Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Ma1SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Ma1ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 数学02 -->
										<td class="table-pc-line table-pc-subjects">
                                            <p>数学【2】</p>
											<label><input type="radio" name="t1Ma2Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Ma2Subject" value="1"<c:if test="${ J101Form.t1Ma2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">数学II</label>
											<label><input type="radio" name="t1Ma2Subject" value="2"<c:if test="${ J101Form.t1Ma2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">数学II・数学B</label>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <label><input type="radio" name="t1Ma2Subject" value="3"<c:if test="${ J101Form.t1Ma2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">旧数学II・数学B</label>
											--%>
											<%-- Mizobata Edit End --%>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Ma2Score" value="<c:out value="${J101Form.t1Ma2Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Ma2SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Ma2ScoreError" class="error"></div>
										</td>
									</tr>
									<tr>
										<th rowspan="3">国語</th>
										<!-- 国語01 -->
										<td class="table-pc-subjects">
											<p>現代文</p>
										</td>
										<td>
											<input type="number" maxlength="3" min="0" max="100" name="t1Ja1Score" value="<c:out value="${J101Form.t1Ja1Score}" />" onchange="changeForm(this)">
											<div id="t1Ja1ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 国語02 -->
										<td class="table-pc-subjects">
											<p>古文</p>
										</td>
										<td>
											<input type="number" maxlength="2" min="0" max="50" name="t1Ja2Score" value="<c:out value="${J101Form.t1Ja2Score}" />" onchange="changeForm(this)">
											<div id="t1Ja2ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 国語03 -->
										<td class="table-pc-subjects">
											<p>漢文</p>
										</td>
										<td>
											<input type="number" maxlength="2" min="0" max="50" name="t1Ja3Score" value="<c:out value="${J101Form.t1Ja3Score}" />" onchange="changeForm(this)">
											<div id="t1Ja3ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科【1】01 -->
										<th rowspan="4">理科【1】</th>
										<td class="table-pc-line table-pc-subjects">
											<p>【1科目め】</p>
											<label><input type="radio" name="t1Sc3Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Sc3Subject" value="7"<c:if test="${ J101Form.t1Sc3Subject == '7' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label>
											<label><input type="radio" name="t1Sc3Subject" value="8"<c:if test="${ J101Form.t1Sc3Subject == '8' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label>
											<label><input type="radio" name="t1Sc3Subject" value="9"<c:if test="${ J101Form.t1Sc3Subject == '9' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label>
											<label><input type="radio" name="t1Sc3Subject" value="10"<c:if test="${ J101Form.t1Sc3Subject == '10' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="2" min="0" max="50" name="t1Sc3Score" value="<c:out value="${J101Form.t1Sc3Score}" />" onchange="changeForm(this)">
											</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Sc3SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Sc3ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科【1】02 -->
										<td class="table-pc-line table-pc-subjects">
											<p>【2科目め】</p>
											<label><input type="radio" name="t1Sc4Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Sc4Subject" value="7"<c:if test="${ J101Form.t1Sc4Subject == '7' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label>
											<label><input type="radio" name="t1Sc4Subject" value="8"<c:if test="${ J101Form.t1Sc4Subject == '8' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label>
											<label><input type="radio" name="t1Sc4Subject" value="9"<c:if test="${ J101Form.t1Sc4Subject == '9' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label>
											<label><input type="radio" name="t1Sc4Subject" value="10"<c:if test="${ J101Form.t1Sc4Subject == '10' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="2" min="0" max="50" name="t1Sc4Score" value="<c:out value="${J101Form.t1Sc4Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Sc4SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Sc4ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科【2】01 -->
										<th rowspan="4">理科【2】</th>
										<td class="table-pc-line table-pc-subjects">
											<p>【1科目め】</p>
											<label><input type="radio" name="t1Sc1Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Sc1Subject" value="1"<c:if test="${ J101Form.t1Sc1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理</label>
											<label><input type="radio" name="t1Sc1Subject" value="2"<c:if test="${ J101Form.t1Sc1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">化学</label>
											<label><input type="radio" name="t1Sc1Subject" value="3"<c:if test="${ J101Form.t1Sc1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">生物</label>
											<label><input type="radio" name="t1Sc1Subject" value="4"<c:if test="${ J101Form.t1Sc1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">地学</label>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <label><input type="radio" name="t1Sc1Subject" value="11"<c:if test="${ J101Form.t1Sc1Subject == '11' }"> checked</c:if> onclick="changeForm(this)">旧物理I</label>
											<label><input type="radio" name="t1Sc1Subject" value="12"<c:if test="${ J101Form.t1Sc1Subject == '12' }"> checked</c:if> onclick="changeForm(this)">旧化学I</label>
											<label><input type="radio" name="t1Sc1Subject" value="13"<c:if test="${ J101Form.t1Sc1Subject == '13' }"> checked</c:if> onclick="changeForm(this)">旧生物I</label>
											<label><input type="radio" name="t1Sc1Subject" value="14"<c:if test="${ J101Form.t1Sc1Subject == '14' }"> checked</c:if> onclick="changeForm(this)">旧地学I</label>
											<label><input type="radio" name="t1Sc1Subject" value="5"<c:if test="${ J101Form.t1Sc1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">理科総合A</label>
											<label><input type="radio" name="t1Sc1Subject" value="6"<c:if test="${ J101Form.t1Sc1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">理科総合B</label>
											--%>
											<%-- Mizobata Edit End --%>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Sc1Score" value="<c:out value="${J101Form.t1Sc1Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Sc1SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Sc1ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科【2】02 -->
										<td class="table-pc-line table-pc-subjects">
											<p>【2科目め】</p>
											<label><input type="radio" name="t1Sc2Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Sc2Subject" value="1"<c:if test="${ J101Form.t1Sc2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理</label>
											<label><input type="radio" name="t1Sc2Subject" value="2"<c:if test="${ J101Form.t1Sc2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">化学</label>
											<label><input type="radio" name="t1Sc2Subject" value="3"<c:if test="${ J101Form.t1Sc2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">生物</label>
											<label><input type="radio" name="t1Sc2Subject" value="4"<c:if test="${ J101Form.t1Sc2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">地学</label>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <label><input type="radio" name="t1Sc2Subject" value="11"<c:if test="${ J101Form.t1Sc2Subject == '11' }"> checked</c:if> onclick="changeForm(this)">旧物理I</label>
											<label><input type="radio" name="t1Sc2Subject" value="12"<c:if test="${ J101Form.t1Sc2Subject == '12' }"> checked</c:if> onclick="changeForm(this)">旧化学I</label>
											<label><input type="radio" name="t1Sc2Subject" value="13"<c:if test="${ J101Form.t1Sc2Subject == '13' }"> checked</c:if> onclick="changeForm(this)">旧生物I</label>
											<label><input type="radio" name="t1Sc2Subject" value="14"<c:if test="${ J101Form.t1Sc2Subject == '14' }"> checked</c:if> onclick="changeForm(this)">旧地学I</label>
											<label><input type="radio" name="t1Sc2Subject" value="5"<c:if test="${ J101Form.t1Sc2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">理科総合A</label>
											<label><input type="radio" name="t1Sc2Subject" value="6"<c:if test="${ J101Form.t1Sc2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">理科総合B</label>
											--%>
											<%-- Mizobata Edit End --%>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Sc2Score" value="<c:out value="${J101Form.t1Sc2Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Sc2SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Sc2ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 地歴・公民01 -->
										<th rowspan="4">地歴・公民</th>
										<td class="table-pc-line table-pc-subjects">
											<p>【1科目め】</p>
											<label><input type="radio" name="t1Gh1Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Gh1Subject" value="3"<c:if test="${ J101Form.t1Gh1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">世界史A</label>
											<label><input type="radio" name="t1Gh1Subject" value="4"<c:if test="${ J101Form.t1Gh1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">世界史B</label>
											<label><input type="radio" name="t1Gh1Subject" value="1"<c:if test="${ J101Form.t1Gh1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史A</label>
											<label><input type="radio" name="t1Gh1Subject" value="2"<c:if test="${ J101Form.t1Gh1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">日本史B</label>
											<label><input type="radio" name="t1Gh1Subject" value="5"<c:if test="${ J101Form.t1Gh1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">地理A</label>
											<label><input type="radio" name="t1Gh1Subject" value="6"<c:if test="${ J101Form.t1Gh1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">地理B</label>
											<label><input type="radio" name="t1Gh1Subject" value="10"<c:if test="${ J101Form.t1Gh1Subject == '10' }"> checked</c:if> onclick="changeForm(this)">倫理，政治・経済</label>
											<label><input type="radio" name="t1Gh1Subject" value="9"<c:if test="${ J101Form.t1Gh1Subject == '9' }"> checked</c:if> onclick="changeForm(this)">現代社会</label>
											<label><input type="radio" name="t1Gh1Subject" value="7"<c:if test="${ J101Form.t1Gh1Subject == '7' }"> checked</c:if> onclick="changeForm(this)">倫理</label>
											<label><input type="radio" name="t1Gh1Subject" value="8"<c:if test="${ J101Form.t1Gh1Subject == '8' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Gh1Score" value="<c:out value="${J101Form.t1Gh1Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t1Gh1SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t1Gh1ScoreError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 地歴・公民02 -->
										<td class="table-pc-line table-pc-subjects">
											<p>【2科目め】</p>
											<label><input type="radio" name="t1Gh2Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t1Gh2Subject" value="3"<c:if test="${ J101Form.t1Gh2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">世界史A</label>
											<label><input type="radio" name="t1Gh2Subject" value="4"<c:if test="${ J101Form.t1Gh2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">世界史B</label>
											<label><input type="radio" name="t1Gh2Subject" value="1"<c:if test="${ J101Form.t1Gh2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史A</label>
											<label><input type="radio" name="t1Gh2Subject" value="2"<c:if test="${ J101Form.t1Gh2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">日本史B</label>
											<label><input type="radio" name="t1Gh2Subject" value="5"<c:if test="${ J101Form.t1Gh2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">地理A</label>
											<label><input type="radio" name="t1Gh2Subject" value="6"<c:if test="${ J101Form.t1Gh2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">地理B</label>
											<label><input type="radio" name="t1Gh2Subject" value="10"<c:if test="${ J101Form.t1Gh2Subject == '10' }"> checked</c:if> onclick="changeForm(this)">倫理，政治・経済</label>
											<label><input type="radio" name="t1Gh2Subject" value="9"<c:if test="${ J101Form.t1Gh2Subject == '9' }"> checked</c:if> onclick="changeForm(this)">現代社会</label>
											<label><input type="radio" name="t1Gh2Subject" value="7"<c:if test="${ J101Form.t1Gh2Subject == '7' }"> checked</c:if> onclick="changeForm(this)">倫理</label>
											<label><input type="radio" name="t1Gh2Subject" value="8"<c:if test="${ J101Form.t1Gh2Subject == '8' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="3" min="0" max="100" name="t1Gh2Score" value="<c:out value="${J101Form.t1Gh2Score}" />" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align: top;">
											<div id="t1Gh2SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align: top;">
											<div id="t1Gh2ScoreError" class="error"></div>
										</td>
									</tr>
								</tbody>
							<!-- /.table-pc --></table>
						<!-- /.contents-inner --></div>
					<!-- /#section01 --></div>

					<div id="section02">
						<h3 class="title01">全統記述模試　成績入力</h3>
						<div class="contents-inner">
							<div class="text-inner">
								<p>全統記述模試の成績を<strong>半角数字で</strong>入力してください。共通テスト・２次のドッキング判定ができます。</p>
								<p>※全統記述模試を受験されていない方は未入力のまま<strong><a href="#section03">大学選択</a></strong>へ進んでください。</p>
							<!-- /.text-inner --></div>

							<div class="btn clear-btn">
								<p>
								<input type="button" name="" value="" onclick="clearFormValue('t2')"id="clear-btn02">
								</p>
							</div>

							<table class="table-pc">
								<thead>
									<tr>
										<th class="table-pc-title">教科</th>
										<th>科目</th>
										<th class="table-pc-title">偏差値</th>
									</tr>
								</thead>
								<tbody>
									<tr><!-- 英語01 -->
										<th>英語</th>
										<td class="table-pc-subjects">
											<p>英語</p>
											<p><label><input type="checkbox" name="listeningFlag" value="1" onchange="changeForm(this)" <c:if test="${ J101Form.listeningFlag == '1' }"> checked</c:if>>リスニング受験</label></p>
										</td>
										<td>
											<input type="number" name="t2EnDev" maxlength="5"  min="0" max="199.9" value="<c:out value="${J101Form.t2EnDev}" />" step="0.1" onchange="changeForm(this)">
											<div id="t2EnDevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 数学01 -->
										<th rowspan="2">数学</th>
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2MaSubject" value="0" checked onclick="changeForm(this)">未選択</label>
											<!-- <label><input type="radio" name="t2MaSubject" value="1"<c:if test="${ J101Form.t2MaSubject == '1' }"> checked</c:if> onclick="changeForm(this)">数学I</label> -->
											<label><input type="radio" name="t2MaSubject" value="2"<c:if test="${ J101Form.t2MaSubject == '2' }"> checked</c:if> onclick="changeForm(this)">数学I型</label>
											<label><input type="radio" name="t2MaSubject" value="3"<c:if test="${ J101Form.t2MaSubject == '3' }"> checked</c:if> onclick="changeForm(this)">数学II型</label>
											<!-- <label><input type="radio" name="t2MaSubject" value="4"<c:if test="${ J101Form.t2MaSubject == '4' }"> checked</c:if> onclick="changeForm(this)">数学II B</label> -->
											<label><input type="radio" name="t2MaSubject" value="5"<c:if test="${ J101Form.t2MaSubject == '5' }"> checked</c:if> onclick="changeForm(this)">数学III型</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9" name="t2MaDev" value="<c:out value="${J101Form.t2MaDev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2MaSubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2MaDevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 国語01 -->
										<th rowspan="2">国語</th>
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2JaSubject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t2JaSubject" value="3"<c:if test="${ J101Form.t2JaSubject == '3' }"> checked</c:if> onclick="changeForm(this)">現代文</label>
											<label><input type="radio" name="t2JaSubject" value="2"<c:if test="${ J101Form.t2JaSubject == '2' }"> checked</c:if> onclick="changeForm(this)">現・古</label>
											<label><input type="radio" name="t2JaSubject" value="1"<c:if test="${ J101Form.t2JaSubject == '1' }"> checked</c:if> onclick="changeForm(this)">現・古・漢</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9" name="t2JaDev" value="<c:out value="${J101Form.t2JaDev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2JaSubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2JaDevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科01 -->
										<th rowspan="4">理科</th>
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2Sc1Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t2Sc1Subject" value="2"<c:if test="${ J101Form.t2Sc1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">物理</label>
											<label><input type="radio" name="t2Sc1Subject" value="4"<c:if test="${ J101Form.t2Sc1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">化学</label>
											<label><input type="radio" name="t2Sc1Subject" value="6"<c:if test="${ J101Form.t2Sc1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">生物</label>
											<label><input type="radio" name="t2Sc1Subject" value="8"<c:if test="${ J101Form.t2Sc1Subject == '8' }"> checked</c:if> onclick="changeForm(this)">地学</label>
                                           <%-- Mizobata Add Start --%>
											<div style="display:inline-block; height:27px;">
												<label><input type="radio" name="t2Sc1Subject" value="1"<c:if test="${ J101Form.t2Sc1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label>
												<label><input type="radio" name="t2Sc1Subject" value="3"<c:if test="${ J101Form.t2Sc1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label>
												<label><input type="radio" name="t2Sc1Subject" value="5"<c:if test="${ J101Form.t2Sc1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label>
												<label><input type="radio" name="t2Sc1Subject" value="7"<c:if test="${ J101Form.t2Sc1Subject == '7' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label>
											</div>
											<%-- Mizobata Add End --%>
										</td>

										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9" name="t2Sc1Dev" value="<c:out value="${J101Form.t2Sc1Dev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2Sc1SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2Sc1DevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 理科02 -->
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2Sc2Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t2Sc2Subject" value="2"<c:if test="${ J101Form.t2Sc2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">物理</label>
											<label><input type="radio" name="t2Sc2Subject" value="4"<c:if test="${ J101Form.t2Sc2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">化学</label>
											<label><input type="radio" name="t2Sc2Subject" value="6"<c:if test="${ J101Form.t2Sc2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">生物</label>
											<label><input type="radio" name="t2Sc2Subject" value="8"<c:if test="${ J101Form.t2Sc2Subject == '8' }"> checked</c:if> onclick="changeForm(this)">地学</label>
                                            <%-- Mizobata Add Start --%>
											<div style="display:inline-block; height:27px;">
												<label><input type="radio" name="t2Sc2Subject" value="1"<c:if test="${ J101Form.t2Sc2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label>
												<label><input type="radio" name="t2Sc2Subject" value="3"<c:if test="${ J101Form.t2Sc2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label>
												<label><input type="radio" name="t2Sc2Subject" value="5"<c:if test="${ J101Form.t2Sc2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label>
												<label><input type="radio" name="t2Sc2Subject" value="7"<c:if test="${ J101Form.t2Sc2Subject == '7' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label>
											</div>
											<%-- Mizobata Add End --%>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9" name="t2Sc2Dev" value="<c:out value="${J101Form.t2Sc2Dev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2Sc2SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2Sc2DevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 地歴・公民01 -->
										<th rowspan="4">地歴・公民</th>
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2Gh1Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t2Gh1Subject" value="2"<c:if test="${ J101Form.t2Gh1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">世界史B</label>
											<label><input type="radio" name="t2Gh1Subject" value="1"<c:if test="${ J101Form.t2Gh1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史B</label>
											<label><input type="radio" name="t2Gh1Subject" value="3"<c:if test="${ J101Form.t2Gh1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">地理B</label>
											<label><input type="radio" name="t2Gh1Subject" value="4"<c:if test="${ J101Form.t2Gh1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">倫理</label>
											<label><input type="radio" name="t2Gh1Subject" value="5"<c:if test="${ J101Form.t2Gh1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9"  name="t2Gh1Dev" value="<c:out value="${J101Form.t2Gh1Dev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2Gh1SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2Gh1DevError" class="error"></div>
										</td>
									</tr>
									<tr><!-- 地歴・公民02 -->
										<td class="table-pc-line table-pc-subjects">
											<label><input type="radio" name="t2Gh2Subject" value="0" checked onclick="changeForm(this)">未選択</label>
											<label><input type="radio" name="t2Gh2Subject" value="2"<c:if test="${ J101Form.t2Gh2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">世界史B</label>
											<label><input type="radio" name="t2Gh2Subject" value="1"<c:if test="${ J101Form.t2Gh2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史B</label>
											<label><input type="radio" name="t2Gh2Subject" value="3"<c:if test="${ J101Form.t2Gh2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">地理B</label>
											<label><input type="radio" name="t2Gh2Subject" value="4"<c:if test="${ J101Form.t2Gh2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">倫理</label>
											<label><input type="radio" name="t2Gh2Subject" value="5"<c:if test="${ J101Form.t2Gh2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label>
										</td>
										<td class="table-pc-line">
											<input type="number" maxlength="5" min="0" max="199.9" name="t2Gh2Dev" value="<c:out value="${J101Form.t2Gh2Dev}" />" step="0.1" onchange="changeForm(this)">
										</td>
									</tr>
									<tr>
										<td style="padding:0px 0px; vertical-align:top;">
											<div id="t2Gh2SubjectError" class="error"></div>
										</td>
										<td style="padding:0px 10px 0px 0px; vertical-align:top;">
											<div id="t2Gh2DevError" class="error"></div>
										</td>
									</tr>
								</tbody>
							<!-- /.table-pc --></table>
						<!-- /.contents-inner --></div>
					<!-- /#section02 --></div>

					<div id="section03">
						<h4 class="title01">合格可能性評価を行う大学の選択</h4>

						<div class="contents-inner">
							<div class="btn">
								<input type="button" name="" value="" onClick="javascript:submitMenu('j203')" id="j101-btn01">
								<input type="button" name="" value="" onClick="javascript:submitMenu('j202')" id="j101-btn02">
								<c:choose>
								<c:when test="${ empty UnivList }">
									<input type="button" name="" value="" disabled="disabled" id="j101-btn04">
								</c:when>
								<c:otherwise>
									<input type="button" name="" value="" onClick="javascript:submitMenu('j301')" id="j101-btn03">
								</c:otherwise>
								</c:choose>
							<!-- /.btn --></div>

						<!-- /.contents-inner --></div>
					<!-- /#section03 --></div>
				<!-- /.contents.cl-pc --></div>





	<!-- SP用コンテンツここから -->
				<div class="contents cl-sp">
					<div id="#sectionSp01">
						<h3 class="title01">共通テスト　自己採点得点入力</h3>
						<div class="contents-inner">
							<div class="text-inner">
								<p>▼各教科の「共通テスト自己採点得点」を入力してください</p>
							<!-- /.text-inner --></div>
							<div class="btn">
								<input type="button" name="" value="" onclick="clearFormValue('t1')" id="clear-btn03">
							</div>
							<table class="table-sp">
								<tr>
									<th colspan="2" class="table-sp-title">英語</th>
								</tr>
								<tr><!-- 英語01 -->
									<th>リーディング</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1En1ScoreSP" value="<c:out value="${J101Form.t1En1Score}" />" onchange="changeForm(this)">
										<div id="t1En1ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 英語02 -->
									<th>リスニング</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1En2ScoreSP" value="<c:out value="${J101Form.t1En2Score}" />" onchange="changeForm(this)">
										<div id="t1En2ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 数学01 -->
									<th colspan="2" class="table-sp-title">数学</th>
								</tr>
								<tr>
									<th>
                                        <p>数学【1】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Ma1SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Ma1SubjectSP" value="1"<c:if test="${ J101Form.t1Ma1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">数学I</label></li>
    										<li><label><input type="radio" name="t1Ma1SubjectSP" value="2"<c:if test="${ J101Form.t1Ma1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">数学I・数学A</label></li>
    										<%-- Mizobata Edit Start --%>
    										<%-- <li><label><input type="radio" name="t1Ma1SubjectSP" value="3"<c:if test="${ J101Form.t1Ma1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">旧数学I</label></li>
    										<li><label><input type="radio" name="t1Ma1SubjectSP" value="4"<c:if test="${ J101Form.t1Ma1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">旧数学I・数学A</label></li>
    										--%>
    										<%-- Mizobata Edit End --%>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Ma1ScoreSP" value="<c:out value="${J101Form.t1Ma1Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Ma1SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Ma1ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 数学02 -->
									<th>
                                        <p>数学【2】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Ma2SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Ma2SubjectSP" value="1"<c:if test="${ J101Form.t1Ma2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">数学II</label></li>
    										<li><label><input type="radio" name="t1Ma2SubjectSP" value="2"<c:if test="${ J101Form.t1Ma2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">数学II・数学B</label></li>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <li><label><input type="radio" name="t1Ma2SubjectSP" value="3"<c:if test="${ J101Form.t1Ma2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">旧数学II・数学B</label></li>
    										--%>
    										<%-- Mizobata Edit End --%>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Ma2ScoreSP" value="<c:out value="${J101Form.t1Ma2Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Ma2SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Ma2ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr>
									<th colspan="2" class="table-sp-title">国語</th>
								</tr>
								<tr><!-- 国語01 -->
									<th>現代文</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Ja1ScoreSP" value="<c:out value="${J101Form.t1Ja1Score}" />" onchange="changeForm(this)">
										<div id="t1Ja1ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 国語02 -->
									<th>古文</th>
									<td>
										<input type="number" maxlength="2" min="0" max="50" name="t1Ja2ScoreSP" value="<c:out value="${J101Form.t1Ja2Score}" />" onchange="changeForm(this)">
										<div id="t1Ja2ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 国語03 -->
									<th>漢文</th>
									<td>
										<input type="number" maxlength="2" min="0" max="50" name="t1Ja3ScoreSP" value="<c:out value="${J101Form.t1Ja3Score}" />" onchange="changeForm(this)">
										<div id="t1Ja3ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科【1】01 -->
									<th colspan="2" class="table-sp-title">理科【1】</th>
								</tr>
								<tr>
									<th>
										<p>【1科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Sc3SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Sc3SubjectSP" value="7"<c:if test="${ J101Form.t1Sc3Subject == '7' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label></li>
    										<li><label><input type="radio" name="t1Sc3SubjectSP" value="8"<c:if test="${ J101Form.t1Sc3Subject == '8' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label></li>
    										<li><label><input type="radio" name="t1Sc3SubjectSP" value="9"<c:if test="${ J101Form.t1Sc3Subject == '9' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label></li>
    										<li><label><input type="radio" name="t1Sc3SubjectSP" value="10"<c:if test="${ J101Form.t1Sc3Subject == '10' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="2" min="0" max="50" name="t1Sc3ScoreSP" value="<c:out value="${J101Form.t1Sc3Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Sc3SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Sc3ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科【1】02 -->
									<th>
										<p>【2科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Sc4SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Sc4SubjectSP" value="7"<c:if test="${ J101Form.t1Sc4Subject == '7' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label></li>
    										<li><label><input type="radio" name="t1Sc4SubjectSP" value="8"<c:if test="${ J101Form.t1Sc4Subject == '8' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label></li>
    										<li><label><input type="radio" name="t1Sc4SubjectSP" value="9"<c:if test="${ J101Form.t1Sc4Subject == '9' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label></li>
    										<li><label><input type="radio" name="t1Sc4SubjectSP" value="10"<c:if test="${ J101Form.t1Sc4Subject == '10' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="2" min="0" max="50" name="t1Sc4ScoreSP" value="<c:out value="${J101Form.t1Sc4Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Sc4SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Sc4ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科【2】01 -->
									<th colspan="2" class="table-sp-title">理科【2】</th>
								</tr>
								<tr>
									<th>
										<p>【1科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="1"<c:if test="${ J101Form.t1Sc1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="2"<c:if test="${ J101Form.t1Sc1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">化学</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="3"<c:if test="${ J101Form.t1Sc1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">生物</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="4"<c:if test="${ J101Form.t1Sc1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">地学</label></li>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <li><label><input type="radio" name="t1Sc1SubjectSP" value="11"<c:if test="${ J101Form.t1Sc1Subject == '11' }"> checked</c:if> onclick="changeForm(this)">旧物理I</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="12"<c:if test="${ J101Form.t1Sc1Subject == '12' }"> checked</c:if> onclick="changeForm(this)">旧化学I</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="13"<c:if test="${ J101Form.t1Sc1Subject == '13' }"> checked</c:if> onclick="changeForm(this)">旧生物I</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="14"<c:if test="${ J101Form.t1Sc1Subject == '14' }"> checked</c:if> onclick="changeForm(this)">旧地学I</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="5"<c:if test="${ J101Form.t1Sc1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">理科総合A</label></li>
    										<li><label><input type="radio" name="t1Sc1SubjectSP" value="6"<c:if test="${ J101Form.t1Sc1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">理科総合B</label></li>
    										--%>
    										<%-- Mizobata Edit End --%>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Sc1ScoreSP" value="<c:out value="${J101Form.t1Sc1Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Sc1SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Sc1ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科【2】02 -->
									<th>
										<p>【2科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="1"<c:if test="${ J101Form.t1Sc2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="2"<c:if test="${ J101Form.t1Sc2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">化学</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="3"<c:if test="${ J101Form.t1Sc2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">生物</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="4"<c:if test="${ J101Form.t1Sc2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">地学</label></li>
                                            <%-- Mizobata Edit Start --%>
                                            <%-- <li><label><input type="radio" name="t1Sc2SubjectSP" value="11"<c:if test="${ J101Form.t1Sc2Subject == '11' }"> checked</c:if> onclick="changeForm(this)">旧物理I</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="12"<c:if test="${ J101Form.t1Sc2Subject == '12' }"> checked</c:if> onclick="changeForm(this)">旧化学I</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="13"<c:if test="${ J101Form.t1Sc2Subject == '13' }"> checked</c:if> onclick="changeForm(this)">旧生物I</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="14"<c:if test="${ J101Form.t1Sc2Subject == '14' }"> checked</c:if> onclick="changeForm(this)">旧地学I</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="5"<c:if test="${ J101Form.t1Sc2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">理科総合A</label></li>
    										<li><label><input type="radio" name="t1Sc2SubjectSP" value="6"<c:if test="${ J101Form.t1Sc2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">理科総合B</label></li>
    										--%>
    										<%-- Mizobata Edit End --%>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Sc2ScoreSP" value="<c:out value="${J101Form.t1Sc2Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Sc2SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Sc2ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 地歴・公民01 -->
									<th colspan="2" class="table-sp-title">地歴・公民</th>
								</tr>
								<tr>
									<th>
										<p>【1科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="3"<c:if test="${ J101Form.t1Gh1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">世界史A</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="4"<c:if test="${ J101Form.t1Gh1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">世界史B</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="1"<c:if test="${ J101Form.t1Gh1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史A</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="2"<c:if test="${ J101Form.t1Gh1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">日本史B</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="5"<c:if test="${ J101Form.t1Gh1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">地理A</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="6"<c:if test="${ J101Form.t1Gh1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">地理B</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="10"<c:if test="${ J101Form.t1Gh1Subject == '10' }"> checked</c:if> onclick="changeForm(this)">倫理，政治・経済</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="9"<c:if test="${ J101Form.t1Gh1Subject == '9' }"> checked</c:if> onclick="changeForm(this)">現代社会</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="7"<c:if test="${ J101Form.t1Gh1Subject == '7' }"> checked</c:if> onclick="changeForm(this)">倫理</label></li>
    										<li><label><input type="radio" name="t1Gh1SubjectSP" value="8"<c:if test="${ J101Form.t1Gh1Subject == '8' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Gh1ScoreSP" value="<c:out value="${J101Form.t1Gh1Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Gh1SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Gh1ScoreSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 地歴・公民02 -->
									<th>
										<p>【2科目め】</p>
                                        <ul>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="3"<c:if test="${ J101Form.t1Gh2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">世界史A</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="4"<c:if test="${ J101Form.t1Gh2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">世界史B</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="1"<c:if test="${ J101Form.t1Gh2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史A</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="2"<c:if test="${ J101Form.t1Gh2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">日本史B</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="5"<c:if test="${ J101Form.t1Gh2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">地理A</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="6"<c:if test="${ J101Form.t1Gh2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">地理B</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="10"<c:if test="${ J101Form.t1Gh2Subject == '10' }"> checked</c:if> onclick="changeForm(this)">倫理，政治・経済</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="9"<c:if test="${ J101Form.t1Gh2Subject == '9' }"> checked</c:if> onclick="changeForm(this)">現代社会</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="7"<c:if test="${ J101Form.t1Gh2Subject == '7' }"> checked</c:if> onclick="changeForm(this)">倫理</label></li>
    										<li><label><input type="radio" name="t1Gh2SubjectSP" value="8"<c:if test="${ J101Form.t1Gh2Subject == '8' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="3" min="0" max="100" name="t1Gh2ScoreSP" value="<c:out value="${J101Form.t1Gh2Score}" />" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t1Gh2SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t1Gh2ScoreSPError" class="error"></div>
									</td>
								</tr>
							<!-- /.table-sp --></table>
						<!-- /.contents-inner --></div>
					<!-- /#section-sp01 --></div>

					<div id="#sectionSp03">
						<div class="contents-inner">
							<h3 class="title01">全統記述模試&nbsp;成績入力</h3>
							<div class="text-inner">
								<p>▼各教科の「偏差値」を入力してください</p>
								<p>※全統記述模試を受験されていない方は未入力のまま<strong><a href="#sectionSp04">大学選択</a></strong>へ進んでください</p>
							<!-- /.text-inner --></div>
							<div class="btn">
								<input type="button" name="" value="" onclick="clearFormValue('t2')" id="clear-btn05">
							</div>
							<table class="table-sp">
								<tr><!-- 英語01 -->
									<th colspan="2" class="table-sp-title">英語</th>
								</tr>
								<tr>
									<th>
										<p>英語</p>
                                        <ul>
    										<li><label><input type="checkbox" name="listeningFlagSP" value="1"<c:if test="${ J101Form.listeningFlag == '1' }"> checked</c:if> onchange="changeForm(this)">リスニング受験</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="5" name="t2EnDevSP" min="0" max="199.9" value="<c:out value="${J101Form.t2EnDev}" />" step="0.1" onchange="changeForm(this)">
										<div id="t2EnDevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 数学01 -->
									<th colspan="2" class="table-sp-title">数学</th>
								</tr>
								<tr>
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2MaSubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<!-- <li><label><input type="radio" name="t2MaSubjectSP" value="1"<c:if test="${ J101Form.t2MaSubject == '1' }"> checked</c:if> onclick="changeForm(this)">数学I</label></li> -->
    										<li><label><input type="radio" name="t2MaSubjectSP" value="2"<c:if test="${ J101Form.t2MaSubject == '2' }"> checked</c:if> onclick="changeForm(this)">数学I型</label></li>
    										<li><label><input type="radio" name="t2MaSubjectSP" value="3"<c:if test="${ J101Form.t2MaSubject == '3' }"> checked</c:if> onclick="changeForm(this)">数学II型</label></li>
    										<!-- <li><label><input type="radio" name="t2MaSubjectSP" value="4"<c:if test="${ J101Form.t2MaSubject == '4' }"> checked</c:if> onclick="changeForm(this)">数学IIB</label></li> -->
    										<li><label><input type="radio" name="t2MaSubjectSP" value="5"<c:if test="${ J101Form.t2MaSubject == '5' }"> checked</c:if> onclick="changeForm(this)">数学III型</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2MaDevSP" value="<c:out value="${J101Form.t2MaDev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2MaSubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2MaDevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 国語01 -->
									<th colspan="2" class="table-sp-title">国語</th>
								</tr>
								<tr>
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2JaSubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t2JaSubjectSP" value="3"<c:if test="${ J101Form.t2JaSubject == '3' }"> checked</c:if> onclick="changeForm(this)">現代文</label></li>
    										<li><label><input type="radio" name="t2JaSubjectSP" value="2"<c:if test="${ J101Form.t2JaSubject == '2' }"> checked</c:if> onclick="changeForm(this)">現・古</label></li>
    										<li><label><input type="radio" name="t2JaSubjectSP" value="1"<c:if test="${ J101Form.t2JaSubject == '1' }"> checked</c:if> onclick="changeForm(this)">現・古・漢</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2JaDevSP" value="<c:out value="${J101Form.t2JaDev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2JaSubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2JaDevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科01 -->
									<th colspan="2" class="table-sp-title">理科</th>
								</tr>
								<tr>
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="2"<c:if test="${ J101Form.t2Sc1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">物理</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="4"<c:if test="${ J101Form.t2Sc1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">化学</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="6"<c:if test="${ J101Form.t2Sc1Subject == '6' }"> checked</c:if> onclick="changeForm(this)">生物</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="8"<c:if test="${ J101Form.t2Sc1Subject == '8' }"> checked</c:if> onclick="changeForm(this)">地学</label></li>
    									</ul>
                                        <%-- Mizobata Add Start --%>
                                        <ul>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="1"<c:if test="${ J101Form.t2Sc1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="3"<c:if test="${ J101Form.t2Sc1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="5"<c:if test="${ J101Form.t2Sc1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label></li>
    										<li><label><input type="radio" name="t2Sc1SubjectSP" value="7"<c:if test="${ J101Form.t2Sc1Subject == '7' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label></li>
                                        </ul>
                                        <%-- Mizobata Add End --%>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2Sc1DevSP" value="<c:out value="${J101Form.t2Sc1Dev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2Sc1SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2Sc1DevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 理科02 -->
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="2"<c:if test="${ J101Form.t2Sc2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">物理</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="4"<c:if test="${ J101Form.t2Sc2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">化学</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="6"<c:if test="${ J101Form.t2Sc2Subject == '6' }"> checked</c:if> onclick="changeForm(this)">生物</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="8"<c:if test="${ J101Form.t2Sc2Subject == '8' }"> checked</c:if> onclick="changeForm(this)">地学</label></li>
    									</ul>
                                        <%-- Mizobata Add Start --%>
                                        <ul>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="1"<c:if test="${ J101Form.t2Sc2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">物理基礎</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="3"<c:if test="${ J101Form.t2Sc2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">化学基礎</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="5"<c:if test="${ J101Form.t2Sc2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">生物基礎</label></li>
    										<li><label><input type="radio" name="t2Sc2SubjectSP" value="7"<c:if test="${ J101Form.t2Sc2Subject == '7' }"> checked</c:if> onclick="changeForm(this)">地学基礎</label></li>
                                        </ul>
                                        <%-- Mizobata Add End --%>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2Sc2DevSP" value="<c:out value="${J101Form.t2Sc2Dev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2Sc2SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2Sc2DevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 地歴・公民01 -->
									<th colspan="2" class="table-sp-title">地歴・公民</th>
								</tr>
								<tr>
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="2"<c:if test="${ J101Form.t2Gh1Subject == '2' }"> checked</c:if> onclick="changeForm(this)">世界史B</label></li>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="1"<c:if test="${ J101Form.t2Gh1Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史B</label></li>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="3"<c:if test="${ J101Form.t2Gh1Subject == '3' }"> checked</c:if> onclick="changeForm(this)">地理B</label></li>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="4"<c:if test="${ J101Form.t2Gh1Subject == '4' }"> checked</c:if> onclick="changeForm(this)">倫理</label></li>
    										<li><label><input type="radio" name="t2Gh1SubjectSP" value="5"<c:if test="${ J101Form.t2Gh1Subject == '5' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2Gh1DevSP" value="<c:out value="${J101Form.t2Gh1Dev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2Gh1SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2Gh1DevSPError" class="error"></div>
									</td>
								</tr>
								<tr><!-- 地歴・公民02 -->
									<th>
                                        <ul>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="0" checked onclick="changeForm(this)">未選択</label></li>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="2"<c:if test="${ J101Form.t2Gh2Subject == '2' }"> checked</c:if> onclick="changeForm(this)">世界史B</label></li>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="1"<c:if test="${ J101Form.t2Gh2Subject == '1' }"> checked</c:if> onclick="changeForm(this)">日本史B</label></li>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="3"<c:if test="${ J101Form.t2Gh2Subject == '3' }"> checked</c:if> onclick="changeForm(this)">地理B</label></li>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="4"<c:if test="${ J101Form.t2Gh2Subject == '4' }"> checked</c:if> onclick="changeForm(this)">倫理</label></li>
    										<li><label><input type="radio" name="t2Gh2SubjectSP" value="5"<c:if test="${ J101Form.t2Gh2Subject == '5' }"> checked</c:if> onclick="changeForm(this)">政治・経済</label></li>
                                        </ul>
									</th>
									<td>
										<input type="number" maxlength="5" min="0" max="199.9" name="t2Gh2DevSP" value="<c:out value="${J101Form.t2Gh2Dev}" />" step="0.1" onchange="changeForm(this)">
									</td>
								</tr>
								<tr>
									<th style="padding:0px 0px;">
										<div id="t2Gh2SubjectSPError" class="error"></div>
									</th>
									<td style="padding:0px 20px 0px 0px;">
										<div id="t2Gh2DevSPError" class="error"></div>
									</td>
								</tr>
							<!-- /.table-sp --></table>
						<!-- /.contents-inner --></div>
					<!-- /#section-sp03 --></div>

					<div id="sectionSp04">
						<div class="contents-inner next">
							<h4 class="title01">合格可能性評価を行う大学の選択</h4>
							<ul>
								<li><a href="javascript:submitMenuSP('j203')">大学選択（名称指定）</a></li>
								<li><a href="javascript:submitMenuSP('j202')">大学選択（条件指定）</a></li>
							</ul>
						<!-- /.contents-inner.next --></div>
					<!-- /#section-sp04 --></div>


				<!-- /.contents .cl-sp --></div>
	<!-- SP用コンテンツここまで -->

			<!-- /.clear .container --></div>
	<!-- コンテンツここまで -->

	<!-- フッターここから -->
	<%@ include file="/jsp/include/footer.jsp"%>
	<!-- フッターここまで -->
</form>
</div>
</body>
</html>

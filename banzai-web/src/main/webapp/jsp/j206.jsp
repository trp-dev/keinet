<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="java.util.List" %>
<%@ page import="jp.co.fj.kawaijuku.judgement.data.UnivInfoBean"%>
<jsp:useBean id="pageBean" scope="request" type="jp.co.fj.keinavi.beans.individual.judgement.SubjectPageBean" />
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
		<title>公表科目情報 バンザイシステム</title>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j206pc.css">
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/j206sp.css" media="(max-width:640px)">
		<!--[if lt IE 9]>
		<script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
		<![endif]-->
		<script src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js" type="text/javascript"></script>
		<script src="<c:out value="${cacheServerUrl}" />js/jquery.smoothScroll.js" type="text/javascript"></script>
		<script type="text/javascript">
		<!--
			<%@ include file="/jsp/script/submit_menu.jsp" %>

            // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
            if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
                document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
            }

            // トップ画面への遷移
            function submitTop(forward) {
            	if (confirm("トップページに戻ります。これまでの入力や選択内容はクリアされますがよろしいですか？")) {
            		location.href = "<c:out value="${topUrl}" />";
        		}

        	}
		// -->
		</script>
	</head>
<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<form>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<div id="wrapper"></div>
<%-- HEADER --%>
	<%@ include file="/jsp/include/header.jsp" %>
<%-- /HEADER --%>

	<!-- コンテンツここから -->
			<div class="container">

				<div class="contents">

	<!-- 大見出し ここから -->
					<h3 class="title01">大学公表科目</h3>
	<!-- 大見出し ここまで -->

					<div class="contents-inner">
						<h4 class="title02"><c:out value="${pageBean.univName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${pageBean.facultyName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${pageBean.deptName}"/><%= changeNOS(pageBean.getExamCapaRel(), pageBean.getCapacity()) %></h4>
						<c:forEach var="detail" items="${pageBean.detailPageList}" varStatus="status">
							<div class="section">
							<!-- 2015/09/10 CCC堀畑 mod start -->
							<% if (pageBean.getDetailPageList().size() > 1) {%>
								<% if (edabanCHECK(pageBean.getDetailPageList())) {%>
								<h5 class="section-title01">科目・配点【<c:out value="${status.count}"/>】</h5>
								<% } %>
							<% } if (pageBean.getDetailPageList().size() > 1) { %>
								<% if (!edabanCHECK(pageBean.getDetailPageList())) {%>
								<c:if test="${!empty detail.branchName}">
								<h5 class="section-title01">科目・配点【<c:out value="${detail.branchName}"/>】</h5>
								</c:if>
								<c:if test="${empty detail.branchName}">
								<h5 class="section-title01">科目・配点</h5>
								</c:if>
								<% } %>
							<% } else { %>
								<h5 class="section-title01">科目・配点</h5>
							<% } %>
							<!-- 2015/09/10 CCC堀畑 mod end -->
									<c:if test="${detail.CExist}">
									<div class="text-inner">
										<h6 class="point">共通テスト</h6>
										<table id="myTBL">
											<thead>
												<tr>
													<th style="background-color:#7b95d8;color:#fff;">教科数</th>
													<td class="full" style="background-color:#fff;color:#333;"><c:out value="${detail.cenCourseNum}"/></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th style="background-color:#7b95d8;color:#fff;">試験科目</th>
													<td class="full"><c:out value="${detail.c_subString}"/><c:if test="${detail.firstAnsFlgSci == 'true'}"><span class="full1" id="rika1">&nbsp;理1&nbsp;</span></c:if><c:if test="${detail.firstAnsFlgSci != 'true'}"></c:if><c:if test="${detail.firstAnsFlgSoc == 'true'}"><span class="full2" id="tiko1">&nbsp;地公1&nbsp;</span></c:if><c:if test="${detail.firstAnsFlgSci != 'true'}"></c:if></td>
												</tr>
												<tr>
													<th class="full0" style="background-color:#7b95d8;color:#fff;">配点</th>
													<td class="full" id="haiten01"><c:out value="${detail.allot1}"/></td>
												</tr>
											</tbody>
										</table>
									</div>
									</c:if>
									<c:if test="${detail.SExist}">
									<div class="text-inner">
										<h6 class="point">2次・個別試験</h6>
										<table id="myTBL_niji">
											<thead>
												<tr>
													<th style="background-color:#7b95d8;color:#fff;">試験科目</th>
													<td class="full" style=background-color:#fff;color:#2b2b2b;"><c:out value="${detail.s_subString}"/></td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th class="full0" style="background-color:#7b95d8;color:#fff;">配点</th>
													<td class="full2" id="haiten02"><c:out value="${detail.allot2}"/></td>
												</tr>
											</tbody>
										</table>
									</div>
									</c:if>
						<!-- /.section --></div>
						</c:forEach>

						<div class="text-inner notes">
							<p>入試科目等は、必ず大学発表の学生募集要項で確認してください。</p>
							<p>試験科目の★は、英語資格・検定試験における出願要件があることを表します。</p>
						<!-- /.text-inner --></div>

					<!-- /.contents-inner --></div>
					<div class="contents-inner">
						<div class="close-btn btn">
							<input type="button" name="" value="" onclick="javascript:window.close()" id="close-btn">
						<!-- /.submit-btn.btn --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.clear .container --></div>
	<!-- コンテンツここまで -->
<%-- FOOTER --%>
<%@ include file="/jsp/include/footer.jsp" %>
<%-- /FOOTER --%>

</form>
</body>
</html>
<%!
/**
 * 募集人員の表示内容を引数の値によって変更させるメソッド
 * encは実行済み
 * @param	examCapaRel  入試定員信頼性
 * @param	capacity  募集人員
 * @return	募集人員
 */
public String changeNOS(String examCapaRel, Integer capacity){
	if (examCapaRel.equals("8") || capacity == 0) {
		return "";
	} else if (examCapaRel.equals("9")) {
		return "（募集人員 推定" + capacity + "名）";
	/**} else if (capacity == 0 && !examCapaRel.equals("8")) {
		return "（募集人員 若干）";*/
	} else {
		return "（募集人員" + capacity + "名）";
	}
}
/**
 * 科目のパターンが複数ある場合に枝番が1つ以上設定されているか判定するメソッド
 * @param	listlength  科目パターン数
 * @param	edabanname  枝番名称
 * @return	枝番名称有無（0:無 1:有）
 */
public boolean edabanCHECK(List DetailPageList){
	int i;
	int hyoji_edaban = DetailPageList.size();
	boolean hyoji_edaban1=false;

		for (i=0 ; i<hyoji_edaban ; i++){
			UnivInfoBean UnivInforBean = (UnivInfoBean) DetailPageList.get(i);
			if(UnivInforBean.getBranchName().equals("")){
			//枝番が設定されていない場合
				hyoji_edaban1 = true;
			}else{
			//枝番が1つでも設定されている場合
				hyoji_edaban1 = false;
				break;
			}
		}
		return hyoji_edaban1;
}
%>
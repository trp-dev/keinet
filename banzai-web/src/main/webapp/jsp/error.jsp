<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld"%>
<c:set var="simpleLayout" value="true" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<c:choose>
	<c:when test="${ ex.timeout }">
		<title>タイムアウト バンザイシステム</title>
	</c:when>
	<c:otherwise>
		<title>エラー バンザイシステム</title>
	</c:otherwise>
</c:choose>
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/normalize.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/common.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/button.css">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/buttonsp.css" media="(max-width:640px)">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/sp.css" media="(max-width:640px)">
<link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/jerror.css">
<!--[if lt IE 9]>
 	<script src="<c:out value="${cacheServerUrl}" />js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:out value="${cacheServerUrl}" />shared_lib/style/ie8.css">
    <![endif]-->
<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/footerFixed.js"></script>
<!--     <script type="text/javascript" src="<c:out value="${cacheServerUrl}" />shared_lib/style/stylesheet.js"></script> -->
<!-- <noscript><link rel="stylesheet" type="text/css" href="<c:out value="${cacheServerUrl}" />shared_lib/style/stylesheet.css"></noscript> -->
<script type="text/javascript">
<!--

<%@ include file="/jsp/script/submit_menu.jsp" %>
<%@ include file="/jsp/script/submit_exit.jsp" %>
	// Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
	if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
		document
				.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
	}

	// トップ画面への遷移
	function submitTop(forward) {
		location.href = "<c:out value="${topUrl}" />";
	}
// -->
</script>
</head>
<%-- <body bgcolor="#F4EDE2" text="#2B2C2E" link="#2986B1" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />"> --%>
<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
	<form>
		<input type="hidden" name="forward" value="j101">
		<input type="hidden" name="backward" value="j001">

		<%-- HEADER --%>
		<!-- ヘッダーここから -->
		<%@ include file="/jsp/include/header.jsp"%>
		<!-- ヘッダーここまで -->
		<%-- /HEADER --%>

		<!-- コンテンツここから -->
		<div class="container">
			<div class="contents">
				<!-- 大見出し ここから -->
				<c:choose>
					<c:when test="${ ex.timeout }">
						<h3 class="title01">タイムアウトしました</h3>
					</c:when>
					<c:otherwise>
						<h3 class="title01">
							<c:choose>
								<c:when test="${ param.forward == 'j101' }">成績入力</c:when>
								<c:when test="${ param.forward == 'j202' }">大学選択（条件指定）</c:when>
								<c:when test="${ param.forward == 'j203' }">大学選択（名称指定）</c:when>
								<c:when test="${ param.forward == 'j204' }">判定結果</c:when>
								<c:when test="${ param.forward == 'j301' }">登録大学</c:when>
								<c:otherwise>エラー</c:otherwise>
							</c:choose>
						</h3>
					</c:otherwise>
				</c:choose>
				<!-- 大見出し ここまで -->

				<div class="contents-inner">
					<div class="text-inner error-text">
						<c:choose>
							<c:when test="${ ex.showMessage }">
								<c:out value="${ ex.errorMessage }" />
							</c:when>
							<c:otherwise>ただいま大変混み合っております。しばらくしてから再度操作してください。</c:otherwise>
						</c:choose>
					</div>
					<!-- /.text-inner -->
					<c:choose>
						<c:when test="${ ex.close }">
							<style type="text/css">
								<!--
									/* .footer { margin-bottom: 5px; } */
									.g-nav, .g-nav-sp { display: none; }
								-->
							</style>
							<div class="close-btn btn">
							<c:if test="${ ex.timeout || ex.errorCode=='1003' }">
								<input type="button" value="" onClick="javascript:submitTop('j001')" id="top-btn" />
							</c:if>
								<input type="button" value="" onClick="submitExit();" id="close-btn" />
							</div>
							<!-- /.close-btn .btn -->
						</c:when>
						<c:when test="${ not ex.showMessage}">
							<style type="text/css">
								<!--
									/* .footer { margin-bottom: 5px; } */
									.g-nav, .g-nav-sp { display: none; }
								-->
							</style>
							<div class="close-btn btn">
							<c:if test="${ ex.timeout || ex.errorCode=='1003' }">
								<input type="button" value="" onClick="javascript:submitTop('j001')" id="top-btn" />
							</c:if>
								<input type="button" value="" onClick="submitExit();" id="close-btn" />
							</div>
							<!-- /.close-btn .btn -->
						</c:when>
						<c:otherwise>
							<div class="back-btn btn">
								<input type="button" value="" onClick="history.back()" id="back-btn" />
							</div>
							<!-- /.back-btn .btn -->
						</c:otherwise>
					</c:choose>
				</div>
				<!-- /.contents-inner -->

			</div>
			<!-- /.contents -->
		</div>
		<!-- /.container -->
		<!-- コンテンツここまで -->

		<%-- FOOTER --%>
		<%@ include file="/jsp/include/footer.jsp"%>
		<%-- /FOOTER --%>

	</form>
</body>
</html>

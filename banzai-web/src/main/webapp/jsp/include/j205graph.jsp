<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="graphPage" scope="request" type="jp.co.fj.banzai.beans.DetailGraphBean" />

<%
    // 初期化処理
    String cacheServerUrl = (String)request.getAttribute("cacheServerUrl");
    String graphHtml   = "";
    double graphHeight = 15;
    double graphWidth  = 0;
    String graphImage  = cacheServerUrl + "shared_lib/img/graph_a.gif"; /* グラフの初期カラー（赤：判定A） */
    double num;             /* 人数軸の目盛り */
    String tdPoint    = "<td>&#160;</td>";
    String tdScore;
    String tdPerson;
    String tdGraph;
    int person = 0;

    int getY_Axis  = 20; /* 人数軸の数（※ 0-20なので実際は 20+1） */

%>
<%!
    /**
     * 内容 : 人数により棒グラフの長さ（width）を決める(Firefox以外用)<br/>
     * 引数 : pCount 人数, pParcent 1マスの人数<br/>
     * 戻値 : 棒グラフの長さ width (％)<br/>
     * 説明 : 1マス目が通常のマスの半分の長さなので、97.5％と 2.5％に分けて長さを計算する<br/>
     * 作成 : 2014-08-25 ccc tatokubo<br/>
     */
    double funGraphWidthSet(double pCount, double pParcent) {
        double gWidth = 0;
        double gMax   = pParcent * 19;

        // 人数が 1マス分より多い
        if (pCount > pParcent) {
            // 人数より 1マス分引く（ 2.5％）
            pCount -= pParcent;
            // 人数のパーセント計算（97.5％）
            gWidth  = (double)Math.round((pCount/gMax) *0.975 *1000) / 10;
            // 最後に 2.5％を足す
            gWidth = gWidth + 2.5;

        // 人数が 1マス分より少ない
        } else {
            gWidth = (pCount >= pParcent) ? 2.5 : (double)Math.round((pCount/pParcent) *2.5 *100) / 100;

        }
        gWidth = (gWidth > 100) ? 105 : gWidth;
        return gWidth;
    }

     /**
      * 内容 : 人数により棒グラフの長さ（width）を決める(Firefox用)<br/>
      * 引数 : pCount 人数, pParcent 1マスの人数<br/>
      * 戻値 : 棒グラフの長さ width (％)<br/>
      * 説明 : 1マス目が通常のマスの半分の長さなので、97.5％と 2.5％に分けて長さを計算する<br/>
      * 作成 : 2014-08-25 ccc tatokubo<br/>
      */
     double funGraphWidthSetFF(double pCount, double pParcent) {
         double gWidth = 0;
         double gMax   = pParcent * 19;

         // 人数が 1マス分より多い
         if (pCount > pParcent) {
             // 人数より 1マス分引く（ 2.5％）
             pCount -= pParcent;
             // 人数のパーセント計算（97.5％）
             gWidth  = (double)Math.round((pCount/gMax) *0.975 *1000) / 10;
             // 最後に 2.5％を足し、計算結果の小数点以下を切り上げ
             gWidth = (double)Math.ceil(gWidth + 2.5);

         // 人数が 1マス分より少ない
         } else {
        	 // 棒グラフの長さを計算し、計算結果の小数点以下を切り上げ
             gWidth = (pCount >= pParcent) ? 2.5 : (double)Math.ceil((double)Math.round((pCount/pParcent) *2.5 *100) / 100);

         }
         gWidth = (gWidth > 100) ? 105 : gWidth;
         return gWidth;
     }

%>

<!-- グラフ外枠 -->
<table class="graph">
<thead>
<tr>
<td class="cell-r cell-b" rowspan="2" colspan="2">得&#12288;&#12288;点</td>
<td class="cellparson-first cell-b" rowspan="2">人数<br/>（累計）</td>
<td class="cell cell-l" colspan="21"><span class="my-position">人数（<span class="star">★</span>：あなたの位置）</span></td></tr><tr>
<%
    for(int i=1; i<=getY_Axis; i++){
        num = i * graphPage.getParcent();
        // 人数軸の目盛りが 2の倍数の場合、表示する
        if(num % 2 == 0){
            // スマホは、10番目、20番目以外は表示しない
            if(i%10 == 0) {
%>
                <td class="cell cell-ten cell-b"><span><%= (int)num %></span></td>
<%
            } else {
%>
                <td class="cell cell-b"><span class="cl-pc"><%= (int)num %></span></td>
<%
            }
        } else {
            // スマホは、10番目、20番目を必ず表示する
            if(i%10 == 0) {
%>
                <td class="cell cell-ten cell-b"><span class="cl-sp"><%= (int)num %></span></td>
<%
            } else {
%>
                 <td class="cell cell-b">&#160;</td>
<%
            }
        }
    }
%>

<td class="cell cell-b"><span class="cl-pc">（人）</span></td>
</tr>
</thead>

<%
    // グラフを生成
    graphHtml = "<tbody>";
    for(int i=(graphPage.getX_Axis()-1); i>=0; i--){
        num = graphPage.getStartingPoint() + (i - 1) * graphPage.getInterval();

        // 1列目 ボーダー（ランク）
        if (graphPage.getBorderPoint() == 9999) {
            tdPoint = "<td>&#160;</td>";
            graphImage  = cacheServerUrl + "shared_lib/img/graph_e.gif";
        } else if (graphPage.getJudge_a()[i] == 1) {
            tdPoint = "<td class=\"color-a\">A<span class=\"cl-pc\">&#160;("+graphPage.getaPoint()+")</span></td>";
        } else if (graphPage.getJudge_b()[i] == 1) {
            tdPoint = "<td class=\"color-b\">B<span class=\"cl-pc\">&#160;("+graphPage.getbPoint()+")</span></td>";
        } else if (graphPage.getJudge_c()[i] == 1) {
            tdPoint = "<td class=\"color-c\">C<span class=\"cl-pc\">&#160;("+graphPage.getcPoint()+")</span></td>";
        } else if (graphPage.getJudge_d()[i] == 1) {
            tdPoint = "<td class=\"color-d\">D<span class=\"cl-pc\">&#160;("+graphPage.getdPoint()+")</span></td>";
        } else if (graphPage.getJudge_border()[i] == 1) {
            tdPoint = "<td class=\"color-bl\">ボ<span class=\"cl-pc\">&#160;("+graphPage.getBorderPoint()+")</span></td>";
        } else {
            tdPoint = "<td>&#160;</td>";
        }

        // 2列目 得点
        if (i == (graphPage.getX_Axis()-1)) {
            tdScore = "<td class=\"cellpoint\">"+(int)num+"&#65374;&nbsp;</td>";
        } else if(i == 0) {
            num += graphPage.getAdjust();
            tdScore = "<td class=\"cellpoint\">&#65374;"+(int)num+"&#12288;&nbsp;</td>";
        } else {
            tdScore = "<td class=\"cellpoint\">"+(int)num+"&#12288;&nbsp;</td>";
        }

        // 3列目 人数
        person += graphPage.getDetails()[i];
        if(i == 0) {
            tdPerson = "<td class=\"cellparson-last\">"+person+"</td>";
        } else {
            tdPerson = "<td class=\"cellparson\">"+person+"</td>";
        }

        // 4列目 棒グラフ
       	graphWidth = funGraphWidthSet(graphPage.getDetails()[i], graphPage.getParcent());
       	if(graphWidth <= 0) {
            tdGraph = "<td class=\"cellgraph cell-l\" colspan=\"21\"><p class=\"cellgraph\">&#160;</p></td>";
        }else{
            tdGraph = "<td class=\"cellgraph cell-l\" colspan=\"21\"><p class=\"cellgraph\"><img class=\"graph-bar\" src=\""+graphImage+"\" height=\""+graphHeight+"px\" width=\""+graphWidth+"%\"></p></td>";
        }

// 自分のポジションを設定 start -----
         if (graphPage.getJudge()[i] == 1 && !Judge.getCLetterJudgement().equals("G") && !Judge.getCLetterJudgement().equals("X")) {
        	if(i == 0) {
            	tdPerson = "<td class=\"cellparson-last starcolor\">"+person+"</td>";
        	} else {
        		tdPerson = "<td class=\"cellparson starcolor\">"+person+"</td>";
        	}
        	if(graphWidth <= 0) {
            	tdGraph  = "<td class=\"cellgraph cell-l\" colspan=\"21\"><p class=\"cellgraph\">";
            	tdGraph += "<img class=\"absolute-star\" style=\"left:"+graphWidth+"%;\" src=\"" + cacheServerUrl + "shared_lib/img/star.png\"></p></td>";
        	} else {
        		tdGraph  = "<td class=\"cellgraph cell-l\" colspan=\"21\"><p class=\"cellgraph\"><img src=\""+graphImage+"\" height=\""+graphHeight+"px\" width=\""+graphWidth+"%\">";
            	tdGraph += "<img class=\"absolute-star\" style=\"left:"+graphWidth+"%;\" src=\"" + cacheServerUrl + "shared_lib/img/star.png\"></p></td>";
        	}
        }
// 自分のポジションを設定 end -----

        // 行を追加
        graphHtml += "<tr>" + tdPoint + tdScore + tdPerson + tdGraph + "</tr>";

        // ランクボーダーを過ぎた場合、画像の色を変更する
        if (graphPage.getJudge_a()[i] == 1) { graphImage = cacheServerUrl + "shared_lib/img/graph_b.gif"; }
        if (graphPage.getJudge_b()[i] == 1) { graphImage = cacheServerUrl + "shared_lib/img/graph_c.gif"; }
        if (graphPage.getJudge_c()[i] == 1) { graphImage = cacheServerUrl + "shared_lib/img/graph_d.gif"; }
        if (graphPage.getJudge_d()[i] == 1) { graphImage = cacheServerUrl + "shared_lib/img/graph_e.gif"; }

    }
    graphHtml += "</tbody>";
    out.print(graphHtml);

%>
</table>

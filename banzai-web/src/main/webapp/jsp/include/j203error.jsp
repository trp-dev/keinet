<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<script type="text/javascript" src="<c:out value="${cacheServerUrl}" />js/footerFixed.js"></script>
<script type="text/javascript">

<%-- 画面遷移(戻るボタン押下時) --%>
	function submitBack(forward) {
		document.forms[0].forward.value = forward;
		document.forms[0].backward.value = "";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
</script>

<div class="container">

	<div class="contents">

		<!-- 大見出し ここから -->
		<h3 class="title01">判定大学を指定する</h3>
		<!-- 大見出し ここまで -->

		<div class="contents-inner">
			<div class="text-inner error-text">
				<p>指定された検索条件に一致する大学はありませんでした。もう一度検索条件を設定しなおしてください。</p>
				<!-- /.text-inner -->
			</div>
			<div class="back-btn btn">
				<input type="button" name="" value="" onclick="submitBack('j203')" id="back-btn">
				<!-- /.submit-btn.btn -->
			</div>
			<!-- /.contents-inner -->
		</div>
		<!-- /.contents -->
	</div>

	<!-- /.container -->
</div>

<%-- FOOTER --%>
<%@ include file="/jsp/include/footer.jsp"%>
<%-- /FOOTER --%>


<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
	//【※※要注意※※】
	//    配列インデックス1に#(ハッシュフラグメント)付きのURLの場合、URLの画面区分、機器区分パラメータを#の前に入れる事。
	//PCのバナー用 画面区分(+top)＋機器区分(+pc)
	String pcURLPara = "+top+pc";
	//SPのバナー用 画面区分(+top)＋機器区分(+sp)
	String spURLPara = "+top+sp";

	// JSTLでの処理用に保存
	pageContext.setAttribute("pcURLPara", pcURLPara);
	pageContext.setAttribute("spURLPara", spURLPara);
%>
					<div class="contents-inner">

					<!-- PC用バナーここから -->
						<div class="banner cl-pc">
								<div class="banner01">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?https://www.kochi-tech.ac.jp/entrance_info/admission/bachelors/exam05.html<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/01kochi-tech.gif" width="140" height="40" border="0" alt="高知工科"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.tokyo-fukushi.ac.jp/jukennavi/index.html<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/02tokyo-fukushi.gif" width="140" height="40" border="0" alt="東京福祉"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.toyo.ac.jp/nyushi/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/03toyo.gif" width="140" height="40" border="0" alt="東洋"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.tcu.ac.jp/examinee/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/04tcu.gif" width="140" height="40" border="0" alt="東京都市"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://ouen.meisei-u.ac.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/05meisei-u.gif" width="140" height="40" border="0" alt="明星"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.sist.ac.jp/ex/ex01/examination-fee/index.html<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/06sist.gif" width="140" height="40" border="0" alt="静岡理工科"></a></li>
									</ul>
								</div>
								<div class="banner02">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?http://www.sugiyama-u.ac.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/07sugiyama-u.gif" width="140" height="40" border="0" alt="椙山女学園"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://adm.chubu.ac.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/08chubu.gif" width="140" height="40" border="0" alt="中部"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.mizuho-c.ac.jp/student/index.html<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/09mizuho-c.gif" width="140" height="40" border="0" alt="愛知みずほ"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.navi.tokaigakuen-u.ac.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/10tokaigakuen-u.gif" width="140" height="40" border="0" alt="東海学園"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.keiho-u.ac.jp/lp/nyushi/ippan_zenki/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/11keiho-u.gif" width="140" height="40" border="0" alt="大阪経済法科"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.nyusi.kansai-u.ac.jp/2019_navi/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/12kansai-u.gif" width="140" height="40" border="0" alt="関西"></a></li>
									</ul>
								</div>
								<div class="banner03">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?http://www.setsunan.ac.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/13setsunan.jpg" width="140" height="40" border="0" alt="摂南"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.milive-plus.net/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/14milive-plus.jpg" width="140" height="40" border="0" alt="みらいぶ＋"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.keinet.ne.jp/search.html<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/15search.gif" width="140" height="40" border="0" alt="大学検索"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.keinet.ne.jp/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/16regist.gif" width="140" height="40" border="0" alt="会員登録"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.kawai-juku.ac.jp/admission/spc/fgh/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/17fgh.jpg" width="140" height="40" border="0" alt="応援サイト"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.kawai-juku.ac.jp/winter/stc/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/18winter.jpg" width="140" height="40" border="0" alt="直前講習"></a></li>
									</ul>
								</div>
								<div class="banner04">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?https://www.keinet.ne.jp/center/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/19keinet.gif" width="432" height="40" border="0" alt="Kei-Net会員登録"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.52school.com/net/<c:out value="${pcURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/20kei.gif" width="432" height="40" border="0" alt="KEIアドバンス"></a></li>
									</ul>
								</div>
						<!-- .banner.cl-pc --></div>
					<!-- PC用バナーここまで -->

					<!-- SP用バナーここから -->
						<div class="banner cl-sp">
							<ul randomdisplay="1">
								<li>
								<div class="banner01">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?http://www.fukuchiyama.ac.jp/admission/examination/examination01/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s01_fukuchiyama.gif" width="140" height="40" border="0" alt="福知山公立"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.osakafu-u.ac.jp/admission/college/current/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s02osakafu-u.gif" width="140" height="40" border="0" alt="大阪府立"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://think.komazawa-u.ac.jp/admission/app-net<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s03komazawa-u.gif" width="140" height="40" border="0" alt="駒澤"></a></li>
										<li><a href="../../cgi-bin/ad1.cgi?http://www.daito.ac.jp/cross/admissions/topics/new.html?utm_source=kei-net&utm_medium=banner&utm_campaign=shutsugan2018k<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s04daito.png" width="140" height="40" border="0" alt="大東文化"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.guide.52school.com/guidance/net-u-tokai/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s05u-tokai.gif" width="140" height="40" border="0" alt="東海"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.tku.ac.jp/exam_summary/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s06tku.gif" width="140" height="40" border="0" alt="東京経済"></a></li>
									</ul>
								</div>
								</li>
								<li>
								<div class="banner02">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?https://www.wako.ac.jp/nyushi/special/special.html<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s07wako.gif" width="140" height="40" border="0" alt="和光"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.komajo.ac.jp/uni/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s08komajo.gif" width="140" height="40" border="0" alt="駒沢女子"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.sanno.ac.jp/exam/index.html<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s09sanno.gif" width="140" height="40" border="0" alt="産業能率"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.shotoku.ac.jp/nyushi/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s10shotoku.gif" width="140" height="40" border="0" alt="岐阜聖徳学園"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.aasa.ac.jp/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s11aasa.jpg" width="140" height="40" border="0" alt="愛知淑徳"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.sugiyama-u.ac.jp/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s12sugiyama-u.gif" width="140" height="40" border="0" alt="椙山女学園"></a></li>
									</ul>
								</div>
								</li>
								<li>
								<div class="banner03">
									<ul>
										<li><a href="../../cgi-bin/ad.cgi?http://adm.chubu.ac.jp/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s13chubu.gif" width="140" height="40" border="0" alt="中部"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.doho.ac.jp/examinee/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s14doho.gif" width="140" height="40" border="0" alt="同朋"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?https://www.nagoyagaidai.com/nyushi/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s15nagoyagaidai.gif" width="140" height="40" border="0" alt="名古屋外国語"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.nyusi.kansai-u.ac.jp/2019_navi/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s16kansai-u.gif" width="140" height="40" border="0" alt="関西"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.setsunan.ac.jp/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s17setsunan.jpg" width="140" height="40" border="0" alt="摂南"></a></li>
										<li><a href="../../cgi-bin/ad.cgi?http://www.yamato-u.ac.jp/<c:out value="${spURLPara}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/banner/s18yamato-u.gif" width="140" height="40" border="0" alt="大和"></a></li>
									</ul>
								</div>
								</li>
							</ul>
						<!-- .banner.cl-sp --></div>
					<!-- SP用バナーここまで -->

					<!-- /.contents-inner --></div>

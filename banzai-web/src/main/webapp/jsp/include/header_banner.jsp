<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%

	//PCのバナー用 画面区分(+top)＋機器区分(+pc)
	String pcURLPara = "+header+pc";
	//SPのバナー用 画面区分(+top)＋機器区分(+sp)
	String spURLPara = "+header+sp";

	// バナーリスト
	List<String[]> bannerListPc = new ArrayList<String[]>();
	List<String[]> bannerListSp = new ArrayList<String[]>();

	// ＰＣ用バナー定義（★要素数は必ず3の倍数で定義すること）
	// 配列インデックス0：大学名（画像のalt属性）
	// 配列インデックス1：リンク先URL
	// 配列インデックス2：画像ファイルURL
	// 【※※要注意※※】
	//    配列インデックス1に#(ハッシュフラグメント)付きのURLの場合、URLの画面区分、機器区分パラメータを#の前に入れる事。
	bannerListPc.add(new String[]{"高知工科", "../../cgi-bin/ad.cgi?https://www.kochi-tech.ac.jp/entrance_info/admission/bachelors/exam05.html"+pcURLPara, "shared_lib/img/banner/h01kochi-tech.gif"});
	bannerListPc.add(new String[]{"日本工業", "../../cgi-bin/ad.cgi?https://www.nit.ac.jp/lp/"+pcURLPara, "shared_lib/img/banner/h02nit.gif"});
	bannerListPc.add(new String[]{"青山学院", "../../cgi-bin/ad.cgi?https://www.aoyama.ac.jp/admission/undergraduate/request/request.html"+pcURLPara, "shared_lib/img/banner/h03aoyama.jpg"});

	bannerListPc.add(new String[]{"中京", "../../cgi-bin/ad2.cgi?https://nc.chukyo-u.ac.jp/nyushi/net_shutugan/?id=banzai2018"+pcURLPara, "shared_lib/img/banner/h04chukyo-u.gif"});
	bannerListPc.add(new String[]{"名古屋外国語", "../../cgi-bin/ad.cgi?https://www.nagoyagaidai.com/nyushi/"+pcURLPara, "shared_lib/img/banner/h05nagoyagaidai.gif"});
	bannerListPc.add(new String[]{"大阪工業", "../../cgi-bin/ad.cgi?http://www.oit.ac.jp/japanese/juken/index.html"+pcURLPara, "shared_lib/img/banner/h06oit.gif"});

	// スマートフォン用バナー定義
	// 配列インデックス0：大学名（画像のalt属性）
	// 配列インデックス1：リンク先URL
	// 配列インデックス2：画像ファイルURL
	// 【※※要注意※※】
	//    配列インデックス1に#(ハッシュフラグメント)付きのURLの場合、URLの画面区分、機器区分パラメータを#の前に入れる事。
	bannerListSp.add(new String[]{"高知工科", "../../cgi-bin/ad.cgi?https://www.kochi-tech.ac.jp/entrance_info/admission/bachelors/exam05.html"+spURLPara, "shared_lib/img/banner/h01kochi-tech.gif"});
	bannerListSp.add(new String[]{"日本工業", "../../cgi-bin/ad.cgi?https://www.nit.ac.jp/lp/"+spURLPara, "shared_lib/img/banner/h02nit.gif"});
	bannerListSp.add(new String[]{"青山学院", "../../cgi-bin/ad.cgi?https://www.aoyama.ac.jp/admission/undergraduate/request/request.html"+spURLPara, "shared_lib/img/banner/h03aoyama.jpg"});

	bannerListSp.add(new String[]{"中京", "../../cgi-bin/ad2.cgi?https://nc.chukyo-u.ac.jp/nyushi/net_shutugan/?id=banzai2018"+spURLPara, "shared_lib/img/banner/h04chukyo-u.gif"});
	bannerListSp.add(new String[]{"名古屋外国語", "../../cgi-bin/ad.cgi?https://www.nagoyagaidai.com/nyushi/"+spURLPara, "shared_lib/img/banner/h05nagoyagaidai.gif"});
	bannerListSp.add(new String[]{"大阪工業", "../../cgi-bin/ad.cgi?http://www.oit.ac.jp/japanese/juken/index.html"+spURLPara, "shared_lib/img/banner/h06oit.gif"});

	// PCのバナー表示開始位置を決める
	int pcBannerBegin = (int)(Math.random() * Math.ceil((double)bannerListPc.size() / 3)) * 3;

	// SPのバナー表示開始位置を決める
	int spBannerBegin = (int)(Math.random() * bannerListSp.size());

	// JSTLでの処理用に変数を保存しておく
	pageContext.setAttribute("bannerListPc", bannerListPc);
	pageContext.setAttribute("bannerListSp", bannerListSp);
	pageContext.setAttribute("pcBannerBegin", pcBannerBegin);
	pageContext.setAttribute("pcBannerEnd", pcBannerBegin + 2);
	pageContext.setAttribute("spBannerBegin", spBannerBegin);

%>
<style type="text/css">
.header-banner ul {
    font-size: 0;
    text-align: center;
}
.header-banner li {
    display: inline-block;
    padding: 8px;
}
</style>
<div class="header-banner cl-pc">
  <ul>
    <c:forEach var="banner" items="${bannerListPc}" begin="${pcBannerBegin}" end="${pcBannerEnd}">
       <li><a href="<c:out value="${banner[1]}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" /><c:out value="${banner[2]}" />" width="300" height="50" border="0" alt="<c:out value="${banner[0]}" />"></a></li>
    </c:forEach>
</div>
<div class="header-banner cl-sp">
  <ul>
    <c:forEach var="banner" items="${bannerListSp}" begin="${spBannerBegin}" end="${spBannerBegin}">
       <li><a href="<c:out value="${banner[1]}" />" target="_blank"><img src="<c:out value="${cacheServerUrl}" /><c:out value="${banner[2]}" />" width="300" height="50" border="0" alt="<c:out value="${banner[0]}" />"></a></li>
    </c:forEach>
  </ul>
</div>

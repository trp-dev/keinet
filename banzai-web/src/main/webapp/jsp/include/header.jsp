<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"%>
<!-- insert start -->
<div id="wrapper">
	<!-- ヘッダーここから -->
	<header class="header">
		<div class="header-logo cl-sp">
			<h1>
				<a href="https://www.keinet.ne.jp/"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/img_header_logo.png" alt="河合塾が運営する大学情報サイト　Kei-net"></a>
			</h1>
			<p>
				<span class="cl-pc"><a href="https://www.kawaijuku.jp/" target="_blank"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/header_group.png" alt="河合塾グループ"></a></span><a href="https://www.kawai-juku.ac.jp/" target="_blank"><img
					src="<c:out value="${cacheServerUrl}" />shared_lib/img/kawai_logo2.gif" alt="河合塾グループ"></a>
			</p>
			<!-- /.header-logo -->
		</div>
		<div id="gHeader" class="gStyle cl-pc">
			<div class="gStyleIn">
				<p class="groupLogo2">
					<a href="https://www.kawai-juku.ac.jp/" target="_blank"><img src="./shared_lib/img/kawai_logo.gif" class="normal" alt="河合塾" width="62" height="13" /></a>
				</p>
				<p class="groupLogo">
					<a href="https://www.kawaijuku.jp/jp/" target="_blank"><img src="./shared_lib/img/header_group.png" alt="河合塾グループ" width="65" height="13" class="normal" /></a>
				</p>
				<!-- / class gStyleIn -->
			</div>
			<!-- / id gHeader -->
		</div>
		<div class="header-logo2 cl-pc">
			<h1>
				<a href="https://www.keinet.ne.jp/"><img src="./shared_lib/img/img_header_logo.png" alt="河合塾が運営する大学情報サイト　Kei-net"></a>
			</h1>
			<a href="https://www.keinet.ne.jp/center/" style="position: absolute; right: 0px; top: 4px;"><img src="<c:out value="${cacheServerUrl}" />shared_lib/img/sakura_tab.png" alt="大学入試共通テスト特集"></a>
			<!-- /.header-logo2 -->
		</div>

		<%-- ヘッダバナー --%>
		<c:choose>
			<c:when test="${ ex.close || param.forward == 'j101' || param.forward == 'j202' || param.forward == 'j203' || param.forward == 'j204' || param.forward == 'j205' || param.forward == 'j206' || param.forward == 'j301' }">
				<%@ include file="/jsp/include/header_banner.jsp"%>
			</c:when>
		</c:choose>

		<div class="header-line">
			<div class="header-line-inner">
				<h2 class="header-title">
					<c:choose>
						<c:when test="${ param.forward == '' || submitTop == 'false' || ex.close }">
							<img alt="共通テスト・リサーチ／バンザイシステム" src="<c:out value="${cacheServerUrl}" />shared_lib/img/banzai_title.png">
						</c:when>
						<c:otherwise>
							<a href="javascript:submitTop('j001')"><img alt="共通テスト・リサーチ／バンザイシステム" src="<c:out value="${cacheServerUrl}" />shared_lib/img/banzai_title.png"></a>
						</c:otherwise>
					</c:choose>
				</h2>
				<p class="help">
					<a href="https://www.keinet.ne.jp/center/banzai/help.html" target="new"></a>
				</p>
				<!-- /.header-line-inner -->
			</div>
			<!-- /.header-line -->
		</div>

		<%-- グローバルナビゲーション --%>
		<div class="g-nav cl-pc" <c:if test="${ param.forward == null || param.forward == '' || param.forward == 'j001' || param.forward == 'j206' || ErrorCode == '1005' }">style="display:none;"</c:if>>
			<ul>
				<c:choose>
					<c:when test="${ param.forward == 'j101' || param.forward == 'j102' }">
						<li class="g-nav-current">成績入力</li>
					</c:when>
					<c:otherwise>
						<li><a href="javascript:submitMenu('j101')">成績入力</a></li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${ param.forward == 'j203' }">
						<li class="g-nav-current">大学選択（名称指定）</li>
					</c:when>
					<c:otherwise>
						<li><a href="javascript:submitMenu('j203')">大学選択（名称指定）</a></li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${ param.forward == 'j202' }">
						<li class="g-nav-current">大学選択（条件指定）</li>
					</c:when>
					<c:otherwise>
						<li><a href="javascript:submitMenu('j202')">大学選択（条件指定）</a></li>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${ param.forward == 'j301' }">
						<li class="g-nav-current">登録大学</li>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${ empty UnivList }">
								<li>登録大学</li>
							</c:when>
							<c:otherwise>
								<li><a href="javascript:submitMenu('j301')">登録大学</a></li>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</header>
</div>
<%-- ここからコンテンツ --%>

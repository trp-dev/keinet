<%-- 表示最大数・登録最大数の取得 --%>
<%@ page pageEncoding="UTF-8" %>
<fmt:setBundle basename="jp.co.fj.keinavi.util.individual.iprops" var="bundle" />
<c:set var="DispMax"><fmt:message key="DISP_MAX_RESULT" bundle="${bundle}" /></c:set>
<c:set var="RegistMax"><fmt:message key="REGIST_MAX_RESULT" bundle="${bundle}" /></c:set>
<%-- /表示最大数・登録最大数の取得 --%>
<script type="text/javascript">
<!--

	<%-- チェック状態を保持する配列 --%>
	var checked = new Array();

	<%-- ソートキーインデックス --%>
<%-- 	var IDX_UNIV_NAME = 13; 大学名カナ --%>
<%-- 	var IDX_CODE = 14; 大学コード＋学部コード＋日程コード＋学科コード --%>
<%-- 	var IDX_RATE = 15; 配点比率 --%>
<%-- 	var IDX_TOTAL_POINT = 16; 総合評価ポイント --%>
<%-- 	var IDX_RATING_CENTER = 17; 共通テスト評価 --%>
<%-- 	var IDX_RATING_SECOND = 18; ２次評価 --%>
<%-- 	var IDX_BORDER = 19; 共通テストボーダー得点率 --%>
<%-- 	var IDX_RANK = 20; ２次ランク偏差値 --%>
<%-- 	var IDX_SORT_KEY = 21; 学科ソートキー --%>
    var IDX_UNIV_NAME = 12; <%-- 大学名カナ --%>
    var IDX_CODE = 13; <%-- 大学コード＋学部コード＋日程コード＋学科コード --%>
    var IDX_RATE = 14; <%-- 配点比率 --%>
    var IDX_TOTAL_POINT = 8; <%-- 総合評価 --%>
    var IDX_RATING_CENTER = 16; <%-- 共通テスト評価 --%>
    var IDX_RATING_SECOND = 17; <%-- ２次評価 --%>
    var IDX_BORDER = 18; <%-- 共通テストボーダー得点率 --%>
    var IDX_RANK = 19; <%-- ２次ランク偏差値 --%>
    var IDX_SORT_KEY = 20; <%-- 学科ソートキー --%>
    var IDX_DIV_CD = 21; <%-- 大学区分 --%>
    var IDX_PREF_CD = 22; <%-- 受験校本部県コード --%>
    var IDX_KANA_NUM = 23; <%-- カナ付き50音順番号 --%>
    var IDX_UNIVNIGHT_DIV = 24; <%-- 大学夜間区分 --%>
    var IDX_FACCON_CD = 25; <%-- 学部内容コード --%>
    var IDX_SCEDULE = 26; <%-- 大学グループ区分 --%>
    var IDX_DEPT_NO = 27; <%-- 学科通番 --%>
    var IDX_SCSYS = 28; <%-- 日程方式 --%>
    var IDX_SCSYS_BRNO = 29; <%-- 日程方式枝番 --%>
    var IDX_DEPT_NAME = 30; <%-- 学科カナ名 --%>
    var IDX_DEPT_CD = 31; <%-- 学科コード --%>

	<%-- 制限数範囲内なら結果データを作る --%>
	<%--
		0  ... ユニークキー
		1  ... 大学名＋学部名＋学科名
		2  ... 共通テストボーダ+共通テストボーダー得点率(2015/09/08 CCC堀畑 mod 共通テストボーダー得点率追加)
		3  ... 共通テスト得点
        4  ... ２次ランク
		5  ... ２次偏差値
		6  ... 共通テスト評価
		7  ... ２次評価
		8  ... 総合評価
		9  ... 配点（共通テスト）
		10 ... 配点（２次）
		11 ... 第一段階選抜
		12 ... 大学名カナ（ソート用）
		13 ... 大学コード＋学部コード＋日程コード＋学科コード（ソート用）
		14 ... 配点比率（ソート用）
		15 ... 総合評価ポイント（ソート用）
		16 ... 共通テスト評価（ソート用）
		17 ... ２次評価（ソート用）
		18 ... 共通テストボーダー得点率（ソート用）
		19 ... ２次ランク偏差値（ソート用）
		20 ... 学科ソートキー（ソート用）
		21 ... 大学区分（ソート用）
		22 ... 受験校本部県コード（ソート用）
		23 ... カナ付き50音順番号（ソート用）
		24 ... 大学夜間区分（ソート用）
		25 ... 学部内容コード（ソート用）
		26 ... 大学グループ区分（ソート用）
		27 ... 学科通番（ソート用）
		28 ... 日程方式（ソート用）
		29 ... 日程方式枝番（ソート用）
		30 ... 学科カナ名（ソート用）
		31 ... 学科コード（ソート用）
		32 ... 注釈文マスタ．種別フラグ（2016.10.14 CCC.KASAI ADD）
	--%>
	var data = new Array();
	// 2016.10.14 CCC.KASAI MOD START
	//<c:if test="${ bean.recordCount <= DispMax }">
	//	<c:forEach var="univ" items="${bean.recordSetAll}" varStatus="status">data[<c:out value="${status.index}" />] = new Array("<c:out value="${univ.uniqueKey}" />", "<kn:univType><c:out value="${univ.univCd}" /></kn:univType><c:out value="${univ.univName}" /> <c:out value="${univ.deptName}" /> <c:out value="${univ.subName}" />",  "<c:if test="${univ.borderPointStr != ''}"><c:out value="${univ.borderPointStr}" />(<c:out value="${univ.cenScoreRate}" />)</c:if>", "<c:out value="${univ.c_scoreStr}" />", "<c:if test="${univ.rankName == ' -- ' && univ.univ.rank != '-99'}"> -- </c:if><c:if test="${univ.univ.rank == '-99'}"> BF </c:if><c:if test="${univ.rankName != ' -- ' && univ.univ.rank != '-99'}"><c:out value="${univ.rankLLimitsStr}"/>-<c:out value="${univ.rankHLimitsStr}" /><c:if test="${ univ.rankHLimitsStr == '' }">　</c:if></c:if>", "<c:out value="${univ.s_scoreStr}" />", "<c:out value="${univ.CLetterJudgement}" />", "<c:out value="${univ.SLetterJudgement}" />", "<c:out value="${univ.TLetterJudgement}" />", <c:if test="${univ.CAllotPntRateAll != ' - '}">"<c:out value="${univ.CAllotPntRateAll}" />"</c:if><c:if test="${univ.CAllotPntRateAll == ' - '}">""</c:if>, <c:if test="${univ.SAllotPntRateAll != ' - '}">"<c:out value="${univ.SAllotPntRateAll}" />"</c:if><c:if test="${univ.SAllotPntRateAll == ' - '}">""</c:if>, "<c:out value="${univ.pickedLine}" />", "<c:out value="${univ.univNameKana}" />", "<c:out value="${univ.univCd}" /><c:out value="${univ.facultyCd}" /><kn:sortKey target="schedule" /><c:out value="${univ.deptSortKey}" />", <kn:sortKey target="rate" />, <c:out value="${univ.t_score}" />, "<kn:sortKey target="center" />", "<kn:sortKey target="second" />", <kn:sortKey target="border" />, <kn:sortKey target="rank" />, "<c:out value="${univ.deptSortKey}" />", "<c:out value="${univ.univDivCd}" />", "<c:out value="${univ.prefCd}" />", "<c:out value="${univ.kanaNum}" />", "<c:out value="${univ.uniNightDiv}" />", "<c:out value="${univ.facultyConCd}" />", "<kn:sortKey target="schedule" />", "<c:out value="${univ.deptSerialNo}" />", "<c:out value="${univ.scheduleSys}" />", "<c:out value="${univ.scheduleSysBranchCd}" />", "<c:out value="${univ.deptNameKana}" />", "<c:out value="${univ.deptCd}" />");
	//	</c:forEach>
	//</c:if>
	<c:if test="${ bean.recordCount <= DispMax }">
		<c:forEach var="univ" items="${bean.recordSetAll}" varStatus="status">data[<c:out value="${status.index}" />] = new Array("<c:out value="${univ.uniqueKey}" />", "<kn:univType><c:out value="${univ.univCd}" /></kn:univType><c:out value="${univ.univName}" /> <c:out value="${univ.deptName}" /> <c:out value="${univ.subName}" />",  "<c:if test="${univ.borderPointStr != ''}"><c:out value="${univ.borderPointStr}" />(<c:out value="${univ.cenScoreRate}" />)</c:if>", "<c:out value="${univ.c_scoreStr}" />", "<c:if test="${univ.rankName == ' -- ' && univ.univ.rank != '-99'}"> -- </c:if><c:if test="${univ.univ.rank == '-99'}"> BF </c:if><c:if test="${univ.rankName != ' -- ' && univ.univ.rank != '-99'}"><c:out value="${univ.rankLLimitsStr}"/>-<c:out value="${univ.rankHLimitsStr}" /><c:if test="${ univ.rankHLimitsStr == '' }">　</c:if></c:if>", "<c:out value="${univ.s_scoreStr}" />", "<c:out value="${univ.CLetterJudgement}" />", "<c:out value="${univ.SLetterJudgement}" />", "<c:out value="${univ.TLetterJudgement}" />", <c:if test="${univ.CAllotPntRateAll != ' - '}">"<c:out value="${univ.CAllotPntRateAll}" />"</c:if><c:if test="${univ.CAllotPntRateAll == ' - '}">""</c:if>, <c:if test="${univ.SAllotPntRateAll != ' - '}">"<c:out value="${univ.SAllotPntRateAll}" />"</c:if><c:if test="${univ.SAllotPntRateAll == ' - '}">""</c:if>, "<c:out value="${univ.pickedLine}" />", "<c:out value="${univ.univNameKana}" />", "<c:out value="${univ.univCd}" /><c:out value="${univ.facultyCd}" /><kn:sortKey target="schedule" /><c:out value="${univ.deptSortKey}" />", <kn:sortKey target="rate" />, <c:out value="${univ.t_score}" />, "<kn:sortKey target="center" />", "<kn:sortKey target="second" />", <kn:sortKey target="border" />, <kn:sortKey target="rank" />, "<c:out value="${univ.deptSortKey}" />", "<c:out value="${univ.univDivCd}" />", "<c:out value="${univ.prefCd}" />", "<c:out value="${univ.kanaNum}" />", "<c:out value="${univ.uniNightDiv}" />", "<c:out value="${univ.facultyConCd}" />", "<kn:sortKey target="schedule" />", "<c:out value="${univ.deptSerialNo}" />", "<c:out value="${univ.scheduleSys}" />", "<c:out value="${univ.scheduleSysBranchCd}" />", "<c:out value="${univ.deptNameKana}" />", "<c:out value="${univ.deptCd}" />", "<c:out value="${univ.typeFlg}" />");
		</c:forEach>
	</c:if>
	// 2016.10.14 CCC.KASAI MOD END

	<%-- テーブルを描画する --%>
	function drawTable() {
		$(sw.getLayer("ListTBody")).children().each(function(i, tr){
		if (data.length > i) {
			var color = '#F4E5D6';
			<c:if test="${param.forward == 'j204'}">
			if (registed[data[i][0]]) {
				if (registed[data[i][0]]) {
	    	        $(tr).addClass("selected");
				} else {
					$(tr).removeClass("selected");
				}
			} else {
					$(tr).removeClass("selected");
			}
			</c:if>
			$(tr).children().each(function(j, td){

				var span = $(td).children(":first");


		<c:choose>
			<c:when test="${ param.forward == 'j204' }">
				if (j == 0) {
					<%-- ■チェックボックス--%>
					if (registed[data[i][0]]) {
						if (registed[data[i][0]]) {
							$(span).html("<input type=\"checkbox\" name=\"dummy\" style=\"visibility:hidden\">");
						} else {
							$(span).html("<input type=\"checkbox\" name=\"univValue\" value=\"" + data[i][0] + "\" onclick=\"selectUniv(this)\""+ (checked[data[i][0]] ? " checked" : "") + ">");
						}
					} else {
						$(span).html("<input type=\"checkbox\" name=\"univValue\" value=\"" + data[i][0] + "\" onclick=\"selectUniv(this)\"" + (checked[data[i][0]] ? " checked" : "") + ">");
					}
				} else if (j == 1) {
					<%-- ■詳細 --%>
					// 2016.10.14 CCC.KASAI MOD START
					//$(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
					switch (data[i][32]) {
					case '1':
						$(span).html("<span class=\"exclamation1\">［！］</span><a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
						break;
					case '2':
						$(span).html("<span class=\"exclamation2\">［！］</span><a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
						break;
					default:
						$(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
					}
					// 2016.10.14 CCC.KASAI MOD END
				} else {
					// 2015/09/08 CCC堀畑 mod start
					var border = data[i][2];
					var arr = new Array();
					if(border != null){
						border1 = border.replace(/\.0/g, '');
						data[i][2]=border1;
				}
					// 2015/09/08 CCC堀畑 mod end
				$(span).text(data[i][j]);
			}
			</c:when>
			<c:when test="${ param.forward == 'j301' }">
				if (j == 0) {
					<%-- ■詳細 --%>
					// 2016.10.14 CCC.KASAI MOD START
					//$(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
					switch (data[i][32]) {
					case '1':
						$(span).html("<span class=\"exclamation1\">［！］</span><a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
						break;
					case '2':
						$(span).html("<span class=\"exclamation2\">［！］</span><a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
						break;
					default:
						$(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
					}
					// 2016.10.14 CCC.KASAI MOD END
				}
                else if (j == 11) {
					<%-- ■削除 --%>
                    $(span).html("<input type=\"button\" value=\"削除\" onclick=\"javascript:deleteUniv('" + data[i][0] + "');\">");
				}
				else {
					/* 2015/09/07 CCC堀畑 mod start 「ボーダー得点率」追加対応*/
					var border = data[i][2];
					var arr = new Array();
					if(border != null){
					var border1 =border.replace(/\.0/g, '');
					data[i][2]=border1;
					}
					$(span).text(data[i][j+1]);
					/* 2015/09/07 CCC堀畑 mod end 「ボーダー得点率」追加対応*/
				}
			</c:when>
		</c:choose>
			});
		}
		});
	}

	<%-- 昇順ソート --%>
	function sortByAsc(a, b, index) {
		if (a[index] == b[index]) return 0;
		else if (a[index] > b[index]) return 1;
		else if (a[index] < b[index]) return -1;
	}

	<%-- 降順ソート --%>
	function sortByDesc(a, b, index) {
		return -(sortByAsc(a, b, index));
	}

	<%-- 昇順ソート(総合評価用) --%>
	function sortByAscTotal(a, b, index) {
		var avalue = convertRating(a[index]);
		var bvalue = convertRating(b[index]);
		if (avalue == bvalue) return 0;
		else if (avalue > bvalue) return 1;
		else if (avalue < bvalue) return -1;
	}

	<%-- 降順ソート(総合評価用) --%>
	function sortByDescTotal(a, b, index) {
		return -(sortByAscTotal(a, b, index));
	}

	<%-- ソート実行 --%>
	function execSort(mode) {
		form.sortKey.value = mode;
		switch (parseInt(mode)) {
			<%-- 大学コード --%>
			case 0:
				data.sort(
					function (a, b) {
						<%-- 大学区分（昇順）--%>
						var value = sortByAsc(a, b, IDX_DIV_CD);
						if (value == 0) {
							<%-- 受験校本部県コード（昇順）--%>
							var value = sortByAsc(a, b, IDX_PREF_CD);
							if (value == 0) {
								<%-- カナ付き50音順番号（昇順）--%>
								var value = sortByAsc(a, b, IDX_KANA_NUM);
								if (value == 0) {
									<%-- 大学夜間区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
									if (value == 0) {
										<%-- 学部内容コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_FACCON_CD);
										if (value == 0) {
											<%-- 大学グループ区分（昇順）--%>
											var value = sortByAsc(a, b, IDX_SCEDULE);
											if (value == 0) {
												<%-- 学科通番（昇順）--%>
												var value = sortByAsc(a, b, IDX_DEPT_NO);
												if (value == 0) {
													<%-- 日程方式（昇順）--%>
													var value = sortByAsc(a, b, IDX_SCSYS);
													if (value == 0) {
														<%-- 日程方式枝番（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
														if (value == 0) {
															<%-- 学科カナ名（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NAME);
															if (value == 0) {
																<%-- 学科コード（昇順）--%>
																return sortByAsc(a, b, IDX_DEPT_CD);
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);
				break;

			<%-- 大学名 --%>
			case 1:
				data.sort(
					function (a, b) {
						<%-- カナ付き50音順番号（昇順）--%>
						var value = sortByAsc(a, b, IDX_KANA_NUM);
						if (value == 0) {
							<%-- 大学区分（昇順）--%>
							var value = sortByAsc(a, b, IDX_DIV_CD);
							if (value == 0) {
								<%-- 受験校本部県コード（昇順）--%>
								var value = sortByAsc(a, b, IDX_PREF_CD);
								if (value == 0) {
									<%-- 大学夜間区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
									if (value == 0) {
										<%-- 学部内容コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_FACCON_CD);
										if (value == 0) {
											<%-- 大学グループ区分（昇順）--%>
											var value = sortByAsc(a, b, IDX_SCEDULE);
											if (value == 0) {
												<%-- 学科通番（昇順）--%>
												var value = sortByAsc(a, b, IDX_DEPT_NO);
												if (value == 0) {
													<%-- 日程方式（昇順）--%>
													var value = sortByAsc(a, b, IDX_SCSYS);
													if (value == 0) {
														<%-- 日程方式枝番（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
														if (value == 0) {
															<%-- 学科カナ名（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NAME);
															if (value == 0) {
																<%-- 学科コード（昇順）--%>
																return sortByAsc(a, b, IDX_DEPT_CD);
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 共通テスト評価 --%>
			case 2:
				data.sort(
					function (a, b) {
						<%-- 共通テスト評価（昇順）--%>
						var value = sortByAsc(a, b, IDX_RATING_CENTER);
						if (value == 0) {
							<%-- 共通テストボーダー得点率（降順）--%>
							var value = sortByDesc(a, b, IDX_BORDER);
							if (value == 0) {
								<%-- 配点比率（降順）--%>
								var value = sortByDesc(a, b, IDX_RATE);
								if (value == 0) {
									<%-- 大学区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- 受験校本部県コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- カナ付き50音順番号（昇順）--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- 大学夜間区分（昇順）--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- 学部内容コード（昇順）--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- 大学グループ区分（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- 学科通番（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- 日程方式（昇順）--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- 日程方式枝番（昇順）--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- 学科カナ名（昇順）--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- 学科コード（昇順）--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 2次評価 --%>
			case 3:
				data.sort(
					function (a, b) {
						<%-- ２次評価（昇順）--%>
						var value = sortByAsc(a, b, IDX_RATING_SECOND);
						if (value == 0) {
							<%-- ２次ランク偏差値（降順）--%>
							var value = sortByDesc(a, b, IDX_RANK);
							if (value == 0) {
								<%-- 配点比率（昇順）--%>
								var value = sortByAsc(a, b, IDX_RATE);
								if (value == 0) {
									<%-- 大学区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- 受験校本部県コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- カナ付き50音順番号（昇順）--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- 大学夜間区分（昇順）--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- 学部内容コード（昇順）--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- 大学グループ区分（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- 学科通番（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- 日程方式（昇順）--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- 日程方式枝番（昇順）--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- 学科カナ名（昇順）--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- 学科コード（昇順）--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);


				break;
			<%-- 総合評価 --%>
			case 4:
				data.sort(
					function (a, b) {
						<%-- 総合評価(昇順)--%>
						var value = sortByAscTotal(a, b, IDX_TOTAL_POINT);
						if (value == 0) {
							<%-- 大学区分（昇順）--%>
							var value = sortByAsc(a, b, IDX_DIV_CD);
							if (value == 0) {
								<%-- 受験校本部県コード（昇順）--%>
								var value = sortByAsc(a, b, IDX_PREF_CD);
								if (value == 0) {
									<%-- カナ付き50音順番号（昇順）--%>
									var value = sortByAsc(a, b, IDX_KANA_NUM);
									if (value == 0) {
										<%-- 大学夜間区分（昇順）--%>
										var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
										if (value == 0) {
											<%-- 学部内容コード（昇順）--%>
											var value = sortByAsc(a, b, IDX_FACCON_CD);
											if (value == 0) {
												<%-- 大学グループ区分（昇順）--%>
												var value = sortByAsc(a, b, IDX_SCEDULE);
												if (value == 0) {
													<%-- 学科通番（昇順）--%>
													var value = sortByAsc(a, b, IDX_DEPT_NO);
													if (value == 0) {
														<%-- 日程方式（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCSYS);
														if (value == 0) {
															<%-- 日程方式枝番（昇順）--%>
															var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
															if (value == 0) {
																<%-- 学科カナ名（昇順）--%>
																var value = sortByAsc(a, b, IDX_DEPT_NAME);
																if (value == 0) {
																	<%-- 学科コード（昇順）--%>
																	return sortByAsc(a, b, IDX_DEPT_CD);
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 共通テスト比高 --%>
			case 5:
				data.sort(
					function (a, b) {
						<%-- 配点比率（降順）--%>
						var value = sortByDesc(a, b, IDX_RATE);
						if (value == 0) {
							<%-- 共通テスト評価（昇順）--%>
							var value = sortByAsc(a, b, IDX_RATING_CENTER);
							if (value == 0) {
								<%-- 共通テストボーダー得点率（降順）--%>
								var value = sortByDesc(a, b, IDX_BORDER);
								if (value == 0) {
									<%-- 大学区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- 受験校本部県コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- カナ付き50音順番号（昇順）--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- 大学夜間区分（昇順）--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- 学部内容コード（昇順）--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- 大学グループ区分（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- 学科通番（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- 日程方式（昇順）--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- 日程方式枝番（昇順）--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- 学科カナ名（昇順）--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- 学科コード（昇順）--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 2次比高 --%>
			case 6:
				data.sort(
					function (a, b) {
						<%-- 配点比率（昇順）--%>
						var value = sortByAsc(a, b, IDX_RATE);
						if (value == 0) {
							<%-- ２次評価（昇順）--%>
							var value = sortByAsc(a, b, IDX_RATING_SECOND);
							if (value == 0) {
								<%-- ２次ランク偏差値（降順）--%>
								var value = sortByDesc(a, b, IDX_RANK);
								if (value == 0) {
									<%-- 大学区分（昇順）--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- 受験校本部県コード（昇順）--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- カナ付き50音順番号（昇順）--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- 大学夜間区分（昇順）--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- 学部内容コード（昇順）--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- 大学グループ区分（昇順）--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- 学科通番（昇順）--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- 日程方式（昇順）--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- 日程方式枝番（昇順）--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- 学科カナ名（昇順）--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- 学科コード（昇順）--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 不明 --%>
			default:
				alert("ソートエラー：" + mode);
		}
		<%-- 再描画 --%>
		drawTable();
	}

	<%-- 通常のソート --%>
	function sortNormal(index) {
		oc.sort(
			function (a, b) {
				if (a[index] == b[index]) return 0;
				else if (a[index] > b[index]) return 1;
				else if (a[index] < b[index]) return -1;
			}
		);
		// 再描画
		drawTable2();
	}

	function convertRating(c) {
		switch (c){
		  case "A":
		    return "10";
		  case "B":
			return "11";
		  case "C":
			return "12";
		  case "D":
			return "13";
		  case "E":
			return "14";
		  case "AH":
			return "30";
		  case "BH":
		    return "31";
	      case "CH":
			return "32";
	      case "DH":
			return "33";
		  case "EH":
			return "34";
		  case "A#":
			return "40";
		  case "B#":
		    return "41";
		  case "C#":
			return "42";
		  case "D#":
		    return "43";
	      case "E#":
		    return "44";
		  case "AG":
			return "50";
	      case "BG":
		    return "51";
		  case "CG":
		    return "52";
		  case "DG":
		    return "53";
		  case "EG":
		    return "54";
		  case "H":
		    return "70";
		  case "G":
			return "71";
		  case "X":
			return "72";
	      case "*":
			return "90";
		  case "  ":
			return "91";
		}
	}
// -->
</script>

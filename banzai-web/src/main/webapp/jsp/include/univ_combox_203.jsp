	/********************************************************************************
	*** EXAM COMBO BOX **************************************************************
	********************************************************************************/
	var univString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array('<c:out value="${nestedUnivData.univCd}"/>','<kn:univType><c:out value="${nestedUnivData.univCd}" /></kn:univType><c:out value="${nestedUnivData.univKanjiName}" />')
		</c:forEach>
	);
	var facultyString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array('<c:out value="${facultyData.facultyCd}"/>','<c:out value="${facultyData.facultyNameAbbr}"/>')
				</c:forEach>
			)
		</c:forEach>
	);
	var deptString = new Array(
		<c:forEach var="nestedUnivData" items="${nestedUnivList}" varStatus="status">
			<c:if test="${status.index != 0}">,</c:if>
			new Array(
				<c:forEach var="facultyData" items="${nestedUnivData.facultyList}" varStatus="status2">
					<c:if test="${status2.index != 0}">,</c:if>
					new Array(
						<c:forEach var="deptData" items="${facultyData.deptList}" varStatus="status3">
							<c:if test="${status3.index != 0}">,</c:if>
							new Array('<c:out value="${deptData.deptCd}"/>','<c:out value="${deptData.deptNameAbbr}"/>')
						</c:forEach>
					)
				</c:forEach>
			)
		</c:forEach>
	);
	function loadUnivList(){
		createList(document.forms[0].univCd, univString);
		loadFacultyList();
	}
	function loadFacultyList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){ 
				createList(document.forms[0].facultyCd, facultyString[i]); 
			} 
		}
		loadDeptList();
	}
	function loadDeptList(){
		for(var i=0; i<univString.length; i++){
			if(document.forms[0].univCd.options[document.forms[0].univCd.selectedIndex].value == univString[i][0]){ 
				for(var j=0; j<facultyString[i].length; j++){
					if(document.forms[0].facultyCd.options[document.forms[0].facultyCd.selectedIndex].value == facultyString[i][j][0]){
						createList(document.forms[0].deptCd, deptString[i][j]); 
					}
				}
			}
		}
	
	}
	function createList(list, datas) { 
		$(list).children().remove().end();
		$.each(datas, function(index, data) {
			var option = $('<option>');
			if (index == 0) option = $('<option selected>');
			$(list).append(option
				.css('background-color', '#ffffff')
				.attr({value: data[0]}).text(data[1]));
		});
		//下記はIEで動作しないためループ内で初期設定
		//$(list).children(':first').attr('selected', 'selected');
	}
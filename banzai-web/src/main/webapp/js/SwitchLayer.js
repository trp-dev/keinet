/**
 * DOM/レイヤ用ライブラリ
 * @author Yoshimoto KAWAI - Totec
 * @version 1.2
 *
 * 1.0 -> 1.1 - 2004/06/29
 *   ・スクロール対策として非表示にした場合の位置を(0,0)に固定した
 * 1.1 -> 1.2 - 2004/06/30
 *   ・getLayer()をpublicにした
 */

// コンストラクタ
function SwitchLayer() {
	this.config = new Array();
	this.addConfig = function (obj) { this.config[obj.id] = obj }
}

function getLayer(id) {
	if (NN4) return document.layers[id];
	else if (IE6) return document.getElementById(id);
	else if (IE4) return document.all.item(id);
}

function showLayer(obj) { this._changeLayerVisibility(this.getLayer(obj), VISIBLE) }
function hideLayer(obj) { this._changeLayerVisibility(this.getLayer(obj), HIDDEN) }

function _changeLayerVisibility(obj, state) {
	position = state == VISIBLE ? "static" : "absolute";
	if (NN4) {
		var flag = false;
		var gap = state == HIDDEN ? 0 - gap : obj.clip.height;
		for (var i=0;i<document.layers.length;i++){
			if (flag) document.layers[i].top = document.layers[i].top + gap;
			if (document.layers[i].id == obj.id) flag = true;
		}
		obj.visibility = state;
	} else if (IE4 || IE6) {
		obj.style.position = position;
		obj.style.visibility = state;
		// version 1.1 fix
		if (position == "absolute") {
			obj.style.top = 0;
			obj.style.left = 0;
		}
	}
}

function changeLayer(checkbox) {
	var show = this.config[checkbox.name].showList[checkbox.value];
	var hide = this.config[checkbox.name].hideList[checkbox.value];
	for (var i=0; i<show.length; i++) { this.showLayer(show[i]) }
	for (var i=0; i<hide.length; i++) { this.hideLayer(hide[i]) }
}

// public
SwitchLayer.prototype.changeLayer = changeLayer;
SwitchLayer.prototype.hideLayer = hideLayer;
SwitchLayer.prototype.showLayer = showLayer;
SwitchLayer.prototype.getLayer = getLayer;
// private
SwitchLayer.prototype._changeLayerVisibility = _changeLayerVisibility;


/**
 * 設定を格納するオブジェクト
 */
function LayerConfig(id) {
	this.id = id;
	this.showList = new Array();
	this.hideList = new Array();
}

function addShowList() {
	this.showList[arguments[0]] = new Array();
	for (var i=0; i<arguments.length-1; i++) {
		this.showList[arguments[0]][i] = arguments[i + 1];
	}
}

function addHideList() {
	this.hideList[arguments[0]] = new Array();
	for (var i=0; i<arguments.length-1; i++) {
		this.hideList[arguments[0]][i] = arguments[i + 1];
	}
}

LayerConfig.prototype.addShowList = addShowList;
LayerConfig.prototype.addHideList = addHideList;

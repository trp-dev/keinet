$(function(){

	/* 国語記述評価：クリック */
	$(".jpndesc").change(function() {
		/* POSTデータを生成 */
		var postData = "forward=&"+ createPostData();

		//ajaxでservletにリクエストを送信
		$.ajax({
			type    : "POST",          //GET / POST
			url     : "InputJpnDesc",  //送信先のServlet URL
			data    : postData,        //リクエストJSON
			async   : true,           //true:非同期(デフォルト), false:同期
			success : function(data, dataType) {
				showJpnDesc(data);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("リクエスト時にエラーが発生しました。");
			}
		});
	});
});

function createPostData() {
	var pre = $("#pc").is(":visible") ? "pc-" : "sp-";
	var q1, q2, q3, q4
	q1 = $("input[name=" + pre + "q1]:checked").val();
	q2 = $("input[name=" + pre + "q2]:checked").val();
	q3 = $("input[name=" + pre + "q3]:checked").val();
	q4 = $("#" + pre + "JpnDesc").text();
	$("#t1JaDescQ1").val(q1);
	$("#t1JaDescQ2").val(q2);
	$("#t1JaDescQ3").val(q3);
	$("#t1JaDescQ4").val(q4);
	var data = $("#t1JaDescQ1,#t1JaDescQ2,#t1JaDescQ3,#t1JaDescQ4").serialize();
	return data;
}

/**
 * サーバーから返却された情報を画面に設定します。
 *
 * @param {responseデータ} data
 */
function showJpnDesc(data) {
	var pre = $("#pc").is(":visible") ? "#pc-" : "#sp-";
	$(".Ja0ScoreError").text("");
	if (data === null) {
		$(pre + "JpnDesc").text("");
		$("#t1JaDescQ4").val("");
	} else {
		$("#t1JaDescQ4").val(data.totalEvaluation);
		$(pre + "JpnDesc").text(data.totalEvaluation);
	}
}

/**
 * 国語記述式の入力チェックを行います。
 *
 */
function checkJpnDesc() {
	$(".Ja0ScoreError").text("");
	var q1, q2, q3;
	var pre = $("#pc").is(":visible") ? "pc" : "sp";
	q1 = $("input[name=" + pre + "-q1]:checked").val();
	q2 = $("input[name=" + pre + "-q2]:checked").val();
	q3 = $("input[name=" + pre + "-q3]:checked").val();

	/* すべて未選択はOK */
	if (q1 === "" && q2 === "" && q3 === "") {
		return true;
	}
	/* すべて選択はOK */
	if (q1 !== "" && q2 !== "" && q3 !== "") {
		return true;
	}
	/* 上記以外の場合、エラー */
	$(".Ja0ScoreError").text(getMessage("j034a"));
	valid = false;
}

/**
 * メッセージを取得します。
 *
 * @param {メッセージID} id
 */
function getMessage(id) {
	var result = "";
	$("input[name='message']").val().split(";").forEach(function(value) {
		var msg = value.split(":");
		if (id == msg[0]) {
			result = msg[1];
			return;
		}
	});
	return result;
}

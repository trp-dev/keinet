/**
 * フォーム用クラス
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function FormUtil() {
}

// チェックされている数を返す
function _countChecked(e) {
	if (e == null) return 0;
	var count = 0;
	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (e[i].checked) count++;
		}
	} else {
		if (e.checked) count++;
	}
	return count;
}

// 有効な数を返す
function _count(e) {
	if (e == null) return 0;
	var count = 0;
	if (e.length) {
		for (var i=0; i<e.length; i++) {
			if (!e[i].disabled) count++;
		}
	} else {
		if (!e.disabled) count++;
	}
	return count;
}

// public
FormUtil.prototype.countChecked = _countChecked;
FormUtil.prototype.count = _count;


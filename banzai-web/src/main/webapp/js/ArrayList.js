/**
 * ArrayList���ǂ�
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
function ArrayList() {
	this.array = new Array();
	this.add = new Function("obj", "this.array[this.array.length]=obj");
	this.addAll = new Function("obj", "for (var i=0; i<obj.size(); i++) { this.array[this.array.length] = obj.get(i) }");
	this.clear = new Function("this.array.length = 0;");
	this.get = new Function("index", "return this.array[index]");
	this.size = new Function("return this.array.length");
	this.sort = new Function("obj", "return this.array.sort(obj)");
	this.toArray = new Function("return this.array");
}

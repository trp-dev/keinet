package profilePatch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonInclude.Value;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;

import lombok.AllArgsConstructor;
import lombok.Data;
import oracle.sql.BLOB;

/**
 * @author SRS)kurimoto
 *
 */
public final class App {

    private static final Logger logger = Logger.getLogger("root");
    private static final Logger bLogger = Logger.getLogger("before");
    private static final Logger aLogger = Logger.getLogger("after");
    private static final Properties props = new Properties();
    private static final ObjectMapper om = new ObjectMapper();
    private static Connection con = null;

    private static final String PROPS_PATH = "config/common.properties";
    private static final String LOG4J_PATH = "config/log4j.properties";
    private static final String CAT_KEY = "050303";
    private static final String ITEM_KEY = "1403";
    private static final int ARRAY_LENGTH = 247;
    private static final List<Integer> defList = new ArrayList<Integer>() {
        private static final long serialVersionUID = 5400516431064914260L;
        {
            for (int i = 0; i < ARRAY_LENGTH; i++) {
                add(i + 1);
            }
            Collections.sort(this);
        }
    };
    private static final String def = String.join(",", defList.stream().map(e -> String.valueOf(e)).collect(Collectors.toList()));
    private boolean isUpdate;

    /**
     * メインメソッドです。
     *
     * @param args 引数
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        App app = new App();
        int exitCode = 0;
        try {
            /* 初期処理 */
            app.init(args);
            /* パッチ処理 */
            app.exec();
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
            logger.fatal("致命的なエラー", e);
            exitCode = -1;
        } finally {
            /* 終了処理 */
            app.end();
        }
        System.exit(exitCode);
    }

    /**
     * 初期処理
     *
     * @throws Exception
     */
    private void init(String[] args) throws Exception {
        try {
            PropertyConfigurator.configure(LOG4J_PATH);
            logger.debug("start");
            logger.info(String.format("PROFILEPARAM Update Start - CATEGORY:[%s] ITEM[%s]", CAT_KEY, ITEM_KEY));
            props.load(Files.newBufferedReader(Paths.get(PROPS_PATH), StandardCharsets.UTF_8));
            Class.forName(props.getProperty("Driver"));
            con = DriverManager.getConnection(props.getProperty("URL"), props.getProperty("UID"), props.getProperty("PWD"));
            con.setAutoCommit(false);
            om.configOverride(Map.class).setInclude(Value.construct(JsonInclude.Include.NON_NULL, JsonInclude.Include.NON_NULL));
            om.getSerializerProvider().setNullKeySerializer(new MyNullKeySerializer());
            om.setSerializationInclusion(Include.NON_NULL);
            isUpdate = "1".equals(props.getProperty("UPDATE"));
            if (args.length > 0 && "1".equals(args[0])) {
                isUpdate = true;
            }
        } catch (IOException e) {
            throw new IOException(String.format("common.properties read faild. path:[%s]", PROPS_PATH));
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException(String.format("driver is not found. Driver:[%s]", props.getProperty("Driver")));
        } catch (SQLException e) {
            throw new SQLException(String.format("database connection faild. URL:[%s]UID:[%s]PWD:[%s]", props.getProperty("URL"), props.getProperty("UID"), props.getProperty("PWD")));
        } finally {
            logger.debug("end");
        }
    }

    /**
     * パッチ処理
     *
     * @throws Exception
     *
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void exec() throws Exception {
        logger.debug("start");
        int updCnt = 0;
        int readCnt = 0;
        List<Json> orgJson = new ArrayList();
        List<Json> newJson = new ArrayList();

        // クエリ設定
        try (PreparedStatement read = con.prepareStatement(createReadQuery()); PreparedStatement write = con.prepareStatement(createWriteQuery())) {
            // PROFILEPARAMの読み込み
            read.setString(1, props.getProperty("TARGET_DATE"));
            read.setString(2, props.getProperty("TARGET_DATE"));
            ResultSet rs = read.executeQuery();
            // ループ
            while (rs.next()) {
                // 読込件数をカウントアップ
                readCnt++;
                // 読込結果より値を取得
                String id = rs.getString("PROFILEID");
                BLOB blob = (BLOB) rs.getBlob("PARAMETER");

                // パラメーターが未設定の場合、スキップ
                if (blob == null) {
                    logger.info(String.format("PROFILEID:[%s] PARAMETER IS NULL", id));
                    continue;
                }

                // BLOBフィールドの読み込み
                Map newParam = getBlobValue(blob);
                Map orgParam = getBlobValue(blob);

                // カテゴリを取得
                Map cat = (Map) newParam.get(CAT_KEY);

                // カテゴリが未設定の場合
                if (cat == null) {
                    // カテゴリにアイテムを設定
                    cat = new HashMap();
                    cat.put(ITEM_KEY, def);
                    // TODO:他のアイテムをどうする？
                    // パラメーターにカテゴリを追加
                    newParam.put(CAT_KEY, (Map) cat);
                    logger.info(String.format("PROFILEID:[%s] CATEGORY IS NULL - UPDATE", id));
                    logger.debug(String.format("{\"size\":%d, \"value\":%s}", defList.size(), om.writeValueAsString(cat)));
                } else {
                    String item = (String) cat.get(ITEM_KEY);
                    if (item == null) {
                        // アイテムが未設定の場合、デフォルト設定する
                        cat.put(ITEM_KEY, def);
                        logger.info(String.format("PROFILEID:[%s] ITEM IS NULL     - UPDATE", id));
                        logger.debug(String.format("{\"size\":%d, \"value\":%s}", defList.size(), om.writeValueAsString(cat)));
                    } else {
                        // アイテムをリスト化
                        List<String> orgList = Arrays.asList(item.split(",", -1));

                        String status = "";
                        // アイテム数が既定値どおりの場合
                        if (orgList.size() == ARRAY_LENGTH) {
                            status = "NORMAL";
                        }
                        // アイテムが既定値より少ない場合
                        if (orgList.size() < ARRAY_LENGTH) {
                            status = "SMALL ";
                        }
                        // アイテムが既定値より多い場合
                        if (orgList.size() > ARRAY_LENGTH) {
                            status = "LARGE ";
                        }
                        // 不要な要素を除去
                        List<String> newList = cleanItem(orgList);
                        // カテゴリにアイテムを設定
                        String newItem = String.join(",", newList);
                        cat.put(ITEM_KEY, newItem);
                        // パラメーターにカテゴリを設定
                        newParam.put(CAT_KEY, (Map) cat);

                        // 同じ場合は更新しない
                        if (item.equals(newItem)) {
                            continue;
                        }
                        // ログ出力用
                        orgJson.add(new Json(id, orgParam));
                        newJson.add(new Json(id, newParam));
                        logger.info(String.format("PROFILEID:[%s] ITEM IS %s   - UPDATE", id, status));
                    }
                }

                // 更新件数をカウントアップ
                updCnt++;

                if (isUpdate()) {
                    // クエリーパラメーターを設定
                    write.setBlob(1, setBlobValue(newParam));
                    write.setString(2, id);
                    // UPDATE文を追加
                    write.addBatch();
                }
            }

            // 一括更新
            if (isUpdate()) {
                write.executeBatch();
            }

            // ログ出力
            om.enable(SerializationFeature.INDENT_OUTPUT);
            bLogger.debug(om.writeValueAsString(orgJson));
            aLogger.debug(om.writeValueAsString(newJson));
            logger.info(String.format("read   count:%3d", readCnt));
            logger.info(String.format(isUpdate() ? "update count:%3d" : "target count:%3d", updCnt));
        } finally {
            logger.debug("end");
        }
    }

    /**
     *
     * @return
     */
    private boolean isUpdate() {
        return isUpdate;
    }

    /**
     *
     * @param blob
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private Map getBlobValue(BLOB blob) throws Exception {
        try (ObjectInputStream ois = new ObjectInputStream(blob.getBinaryStream())) {
            return (Map) ois.readObject();
        }
    }

    /**
     *
     * @param param
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
    private ByteArrayInputStream setBlobValue(Map param) throws Exception {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(bos);) {
            oos.writeObject(param);
            return new ByteArrayInputStream(bos.toByteArray());
        }
    }

    /**
     *
     * @return
     */
    private String createReadQuery() {
        StringBuffer sql = new StringBuffer();
        sql.append("select p.PROFILEID");
        sql.append("     , p.PARAMETER");
        sql.append("  from PROFILE f");
        sql.append(" inner join PROFILEPARAM p");
        sql.append("    on f.PROFILEID = p.PROFILEID");
        sql.append(" where p.PARAMETER is not null");
        sql.append("   and (f.CREATEDATE > ? or f.RENEWALDATE > ?)");
        sql.append(" order by p.PROFILEID");
        return sql.toString();
    }

    /**
     *
     * @return
     */
    private String createWriteQuery() {
        StringBuffer sql = new StringBuffer();
        sql.append("update PROFILEPARAM");
        sql.append("   set PARAMETER = ?");
        sql.append(" where PROFILEID = ?");
        return sql.toString();
    }

    /**
     *
     * @param orgList
     * @return
     */
    private List<String> cleanItem(List<String> orgList) {
        List<String> newList = new ArrayList<String>();
        // 配列分ループして、不要な要素を除去
        for (int i = 0; i < orgList.size(); i++) {
            try {
                String value = orgList.get(i);
                // 未設定もしくは既定値より大きい場合は無視
                if (value == null || "".equals(value) || Integer.parseInt(value) > ARRAY_LENGTH) {
                    continue;
                }
                // 値が存在しない場合、配列に追加
                if (!newList.contains(value)) {
                    newList.add(value);
                }
            } catch (Exception e) {
                // 例外は無視
            }
        }
        /*for (int i = newList.size(); i < ARRAY_LENGTH; i++) {
            newList.add("");
        }*/
        return newList;
    }

    /**
     * 終了処理
     *
     * @throws SQLException
     */
    private void end() throws SQLException {
        try {
            logger.debug("start");
            logger.info("PROFILEPARAM Update End.");
            if (con != null) {
                // コミット
                con.commit();
                con.close();
                con = null;
            }
        } finally {
            logger.debug("end");
        }
    }

    /**
     *
     * @author U02
     *
     */
    class MyNullKeySerializer extends JsonSerializer<Object> {
        @Override
        public void serialize(Object nullKey, JsonGenerator jsonGenerator, SerializerProvider unused) throws IOException, JsonProcessingException {
            jsonGenerator.writeFieldName("");
        }
    }

    /**
     *
     * @author U02
     *
     */
    @Data
    @SuppressWarnings("rawtypes")
    @AllArgsConstructor
    class Json {
        private String profileId;
        private Map parameter;
    }
}

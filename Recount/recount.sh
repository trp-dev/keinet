#!/bin/sh

export JAVA_HOME=/usr/java/j2sdk1.4.2_19

export LIB_DIR=lib
export CP=bin
export CP=$CP:$LIB_DIR/keinavi.jar
export CP=$CP:$LIB_DIR/totec.jar
export CP=$CP:$LIB_DIR/classes12.jar
export CP=$CP:$LIB_DIR/commons-configuration-1.1.jar
export CP=$CP:$LIB_DIR/commons-logging.jar
export CP=$CP:$LIB_DIR/log4j-1.2.8.jar
export CP=$CP:$LIB_DIR/commons-lang-2.1.jar
export CP=$CP:$LIB_DIR/commons-dbutils-1.0.jar
export CP=$CP:$LIB_DIR/JSPDev.jar

cd /keinavi/bat/Recount/
$JAVA_HOME/bin/java -cp $CP recount.RecountBatch


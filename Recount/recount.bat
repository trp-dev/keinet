
@echo off

SET LIB_DIR=lib

SET CP=bin
SET CP=%CP%;%LIB_DIR%\keinavi.jar
SET CP=%CP%;%LIB_DIR%\totec.jar;
SET CP=%CP%;%LIB_DIR%\classes12.jar
SET CP=%CP%;%LIB_DIR%\commons-configuration-1.1.jar
SET CP=%CP%;%LIB_DIR%\commons-logging.jar
SET CP=%CP%;%LIB_DIR%\log4j-1.2.8.jar
SET CP=%CP%;%LIB_DIR%\commons-lang-2.1.jar
SET CP=%CP%;%LIB_DIR%\commons-dbutils-1.0.jar
SET CP=%CP%;%LIB_DIR%\JSPDev.jar

java -cp %CP% recount.RecountBatch


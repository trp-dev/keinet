package recount;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import jp.co.fj.keinavi.beans.recount.RecountChannel;
import jp.co.fj.keinavi.beans.recount.data.RecountRequest;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.co.fj.keinavi.util.sql.QueryLoader;
import jp.co.totec.config.ConfigResolver;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.xml.DOMConfigurator;

/**
 *
 * 再集計バッチ
 * 
 * 2006.02.15	[新規作成]
 * 
 * 2007.08.09	校内成績処理システムより移植
 * 
 *
 * @author Yoshimoto KAWAI - TOTEC
 * @version 1.0
 * 
 */
public final class RecountBatch {

	// ログインスタンス
	private static final Log LOG;

	private static final String DB_DRIVER;
	private static final String DB_URL;
	private static final String DB_USER_ID;
	private static final String DB_PASSWORD;
	
	static {
		System.out.print("再集計バッチ初期化中 ... ");
		
		final String base = System.getProperty("user.dir");
		// リソースファイルのパスをセット
		KNPropertyCtrl.FilePath = base + File.separator + "config";
		// SQLファイルがあるパスをセット
		QueryLoader.getInstance().setPath(base + File.separator + "sql");
		// 設定ファイルディレクトリをクラスパスに含める
		try {
			ConfigResolver.getInstance().init(base + File.separator + "config");
		} catch (final Exception e) {
			final InternalError i = new InternalError(e.getMessage());
			i.initCause(e);
			throw i;
		}
		// log4j設定
		DOMConfigurator.configure(ConfigResolver.getInstance(
				).findResource("log4j.xml"));

		LOG = LogFactory.getLog(RecountBatch.class);
		DB_DRIVER = KNCommonProperty.getStringValue("NDBDriver");
		DB_URL = KNCommonProperty.getStringValue("NDBURL");
		DB_USER_ID = KNCommonProperty.getStringValue("NDBConnUserID");
		DB_PASSWORD = KNCommonProperty.getStringValue("NDBConnUserPass");
		
		System.out.println("完了しました。");
		
        // 再集計処理スレッド開始
		RecountChannel.getInstance().init(
				DB_DRIVER, DB_URL, DB_USER_ID, DB_PASSWORD);
	}
	
	private RecountBatch() {
	}
	
	/**
	 * @param args 起動パラメータ
	 */
	public static void main(final String[] args) {

		// dummy ...
		args.getClass();

		LOG.info("再集計バッチ処理を開始します。");
		LOG.info("再集計対象の登録を開始します ...");
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("USER_DIR: " + System.getProperty("user.dir"));
			LOG.debug("DB_DRIVER: " + DB_DRIVER);
			LOG.debug("DB_URL: " + DB_URL);
			LOG.debug("DB_USER_ID: " + DB_USER_ID);
			LOG.debug("DB_PASSWORD: " + DB_PASSWORD);
		}

		// 再集計対象登録処理
		final List container = new LinkedList();
		Connection con = null;
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs1 = null;
		try {
			con = getConnection();
			
			ps1 = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount_bat01"));
			
			ps2 = con.prepareStatement(
					QueryLoader.getInstance().getQuery("recount_bat02"));
			
			rs1 = ps1.executeQuery();
			while (rs1.next()) {
				final String schoolCd = rs1.getString(1);
				// RECOUNTQUEUEに登録
				ps2.setString(1, schoolCd);
				ps2.setString(2, schoolCd);
				// 成功なら再集計対象とする
				if (ps2.executeUpdate() > 0) {
					container.add(new RecountRequest(schoolCd));
				}
			}
			
			con.commit();
			
		} catch (final Throwable e) {
			// ロールバック
			try {
				con.rollback();
			} catch (final SQLException s) {
				LOG.error("ロールバックに失敗しました。", s);
			}
			// 再集計スレッド停止
			RecountChannel.getInstance().terminate();
			LOG.error("再集計対象の登録に失敗しました。", e);
			// バッチ終了
			return;
		} finally {
			DbUtils.closeQuietly(ps2);
			DbUtils.closeQuietly(con, ps1, rs1);
		}

		// 再集計スレッドに登録
		for (final Iterator ite = container.iterator(); ite.hasNext();) {
			RecountChannel.getInstance(
					).putRequest((RecountRequest) ite.next());
		}
		
		LOG.info("再集計対象の登録が完了しました。" + container.size() + "件");
		
		// 監視ループ
		int size;
		while ((size = RecountChannel.getInstance().getQueueSize()) > 0) {
			
			LOG.info("再集計処理中です ... 残り" + size + "件");
			
			try {
				Thread.sleep(1000 * 10);
			} catch (final InterruptedException e) {
				throw new InternalError(e.getMessage());
			}
		}

		RecountChannel.getInstance().terminate();
		
		LOG.info("再集計バッチ処理が完了しました。");
	}

	private static Connection getConnection() throws Exception {
		Class.forName(DB_DRIVER);
		return DriverManager.getConnection(DB_URL, DB_USER_ID, DB_PASSWORD);
	}
	
}

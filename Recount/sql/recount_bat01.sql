SELECT
	s.bundlecd
FROM
	school s
WHERE
	s.bundlecd IN (
		SELECT
			re.bundlecd
		FROM
			subrecount re
		UNION ALL
		SELECT
			re.bundlecd
		FROM
			ratingnumberrecount re)
AND
	parentschoolcd IS NULL

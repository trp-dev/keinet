INSERT INTO
	rnc_recount_work
SELECT
	cr.individualid,
	cr.examyear,
	cr.examcd,
	RANK() OVER(PARTITION BY cr.individualid, decode(u.unidiv, '01', '1', '2') ORDER BY cr.candidaterank),
	cr.univcd,
	cr.facultycd,
	cr.deptcd,
	cr.bundlecd,
	hi.grade,
	hi.class,
	TRIM(cr.centerrating),
	TRIM(cr.secondrating),
	TRIM(cr.totalrating),
	u.unigdiv
FROM
	candidaterating cr
INNER JOIN
	historyinfo hi
ON
	hi.individualid = cr.individualid
AND
	hi.year = cr.examyear
INNER JOIN
	univmaster_basic u
ON
	u.eventyear = cr.examyear
AND
	u.examdiv = ?
AND
	u.univcd = cr.univcd
AND
	u.facultycd = cr.facultycd
AND
	u.deptcd = cr.deptcd
WHERE
	cr.examyear = ?
AND
	cr.examcd = ?
AND
	cr.bundlecd = ?
AND
	cr.candidaterank = (
		SELECT
			MIN(t.candidaterank)
		FROM
			candidaterating t
		WHERE
			t.individualid = cr.individualid
		AND
			t.examyear = cr.examyear
		AND
			t.examcd = cr.examcd
		AND
			t.univcd = cr.univcd
		AND
			t.facultycd = cr.facultycd
		AND
			t.deptcd = cr.deptcd)
AND
	EXISTS (
		SELECT
			1
		FROM
			ratingnumberrecount re
		WHERE
			re.year = cr.examyear
		AND
			re.examcd = cr.examcd
		AND
			re.bundlecd = cr.bundlecd
		AND
			re.grade = hi.grade
		AND
			re.class = hi.class)
UNION ALL
SELECT
	'ZZZZ' || pp.answersheet_no,
	pp.examyear,
	pp.examcd,
	RANK() OVER(PARTITION BY pp.answersheet_no, decode(u.unidiv, '01', '1', '2') ORDER BY pp.candidaterank),
	pp.univcd,
	pp.facultycd,
	pp.deptcd,
	pp.schoolcd,
	pp.grade,
	pp.class,
	TRIM(pp.centerrating),
	TRIM(pp.secondrating),
	TRIM(pp.totalrating),
	u.unigdiv
FROM
	candidaterating_pp pp
INNER JOIN
	univmaster_basic u
ON
	u.eventyear = pp.examyear
AND
	u.examdiv = ?
AND
	u.univcd = pp.univcd
AND
	u.facultycd = pp.facultycd
AND
	u.deptcd = pp.deptcd
WHERE
	pp.examyear = ?
AND
	pp.examcd = ?
AND
	pp.schoolcd = ?
AND
	pp.candidaterank = (
		SELECT
			MIN(t.candidaterank)
		FROM
			candidaterating_pp t
		WHERE
			t.answersheet_no = pp.answersheet_no
		AND
			t.examyear = pp.examyear
		AND
			t.examcd = pp.examcd
		AND
			t.univcd = pp.univcd
		AND
			t.facultycd = pp.facultycd
		AND
			t.deptcd = pp.deptcd)
AND
	EXISTS (
		SELECT
			1
		FROM
			ratingnumberrecount re
		WHERE
			re.year = pp.examyear
		AND
			re.examcd = pp.examcd
		AND
			re.bundlecd = pp.schoolcd
		AND
			re.grade = pp.grade
		AND
			re.class = pp.class)

SELECT
	e.examyear,
	e.examcd,
	e.examtypecd,
	e.examdiv,
	e.masterdiv,
	re.bundlecd
FROM
	examination e
INNER JOIN
	subrecount re
ON
	re.examyear = e.examyear
AND
	re.examcd = e.examcd
WHERE
	re.bundlecd IN (
		SELECT
			bundlecd
		FROM
			school s
		WHERE
			(s.bundlecd = ? OR s.parentschoolcd = ?)
		AND
			e.out_dataopendate >= NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000')
		AND
			e.out_dataopendate < NVL2(s.stopdate, s.stopdate || '0000', '999999999999'))
UNION
SELECT
	e.examyear,
	e.examcd,
	e.examtypecd,
	e.examdiv,
	e.masterdiv,
	re.bundlecd
FROM
	examination e
INNER JOIN
	ratingnumberrecount re
ON
	re.year = e.examyear
AND
	re.examcd = e.examcd
WHERE
	re.bundlecd IN (
		SELECT
			bundlecd
		FROM
			school s
		WHERE
			(s.bundlecd = ? OR s.parentschoolcd = ?)
		AND
			e.out_dataopendate >= NVL2(s.newgetdate, s.newgetdate || '0000', '000000000000')
		AND
			e.out_dataopendate < NVL2(s.stopdate, s.stopdate || '0000', '999999999999'))

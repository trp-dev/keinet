import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import jp.co.fj.freemenu.beans.MakeStatsDeleteBean;
import jp.co.fj.freemenu.beans.MakeStatsSearchBean;
import jp.co.fj.freemenu.data.MakeStatsData;
import jp.co.fj.freemenu.util.DBUtil;
import jp.co.fj.freemenu.util.FileUtil;
import jp.co.fj.freemenu.util.PDFUtil;
import jp.co.fj.freemenu.util.StringUtil;
import jp.co.fj.keinavi.beans.individual.ExamSearchBean;
import jp.co.fj.keinavi.data.individual.ExaminationData;
import jp.co.fj.keinavi.util.CollectionUtil;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.fujitsu.systemwalker.outputassist.connector.FormBase;
 
/**
 * @author Totec) T.Yamada
 * 
 * KeiNaviダウンロードサービス
 * 志望大学・学部・学科別成績分布表
 * 
 * [大学区分コード]区分	  ：[処理タイプ]出力方法		:[定義体名]定義体
 * -------------------------------------------------------------------------------------------------
 * 1. [01]国公立大　　　　：[A]マーク(国公立)			:[f002_5_1]国公立・センター参加用
 * 2. [02]センター参加私大：[B]マーク(センター参加私大)	:[f002_5_1]国公立・センター参加用
 * 3. [03]センター参加短大：[B]マーク(センター参加私大)	:[f002_5_1]国公立・センター参加用
 * 4. [04]一般私大　　　　：[C]マーク(私立)				:[f002_5_2]一般私大用
 * 5. [05]一般短大・その他：[C]マーク(私立)				:[f002_5_2]一般私大用
 * 6. [06]国公立大　　　　：[D]記述(国公立)				:[f002_5_3]一般私大用
 * 7. [07]一般私大　　　　：[E]記述(私立)				:[f002_5_4]一般私大用
 * 8. [08]一般短大・その他：[E]記述(私立)				:[f002_5_4]一般私大用
 * 
 * DSUD+地区+区分→ure+模試区分+地区+.pdf
 * 
 * 2005.8.8 	Totec) T.Yamada 	作成
 * 2005.9.13 	Totec) T.Yamada 	[1]マーク国公立の換算得点のビット位置とサイズ修正
 * 2005.9.15 	Totec) T.Yamada 	[2]マークセンター参加私大の換算得点のビット位置とサイズ修正
 * 2005.9.15 	Totec) T.Yamada 	[3]その大学に学部（その学部に学科）が存在しないときの対処
 * 2005.9.15 	Totec) T.Yamada 	[4]大学区分01、02、03(A、Ｂ)の大学名と学部名の流用ロジックを削除
 * 2020.2.14 	FJ) Y.Nagai	 	[5]共通テスト対応による文言変更（センター → 共テ）
 * 
 */
public class F002_5Batch {

	/**
	 * プロパティファイル名
	 */
	private static final String PROPERTIES = "f002_5batch.properties";

	/**
	 * 出力先ディレクトリ
	 */
	private static final String FILE_OUTPUT_DIR;
		
	/**
	 * 入力データファイル
	 */
	private static final String INPUT_DATA_DIR;
	
	/**
	 * 帳票定義体ディレクトリ
	 */
	private static final String PDF_SCRIPT_DIR;
	
	/**
	 * 出力用ファイルプレフィックス
	 */
	private static final String OUT_FILE_PREFIX;

	/**
	 * 入力文字コードセット
	 */
	private static final String INPUT_CHARSET;
	
	/**
	 * 出力文字コードセット
	 */
	private static final String OUTPUT_CHARSET;
	
	/**
	 * 区切り用文字
	 */
	private static final String OUT_DELIMITER;
		
	/**
	 * バッチ処理を許可する模試コードの配列
	 */
	private static final String[] EXAMCDS_ALLOWED;
	
	/**
	 * 統計種類
	 */
	private static final int STATISTICS_TYPE;
	
	/**
	 * ログインスタンス
	 */
	private static final Logger logger = Logger.getLogger(F002_5Batch.class);
	
	/**
	 * ファイルを削除するX年前
	 */
	private static final int DELETE_YEARSAGO;
	
	/**
	 * DB接続情報群
	 */
	private static final String DB_DRIVER;
	private static final String DB_URL;
	private static final String DB_USERNAME;
	private static final String DB_PASSWORD;
	
	/**
	 * PDF出力項目
	 */
	private static final String PDF_AUTHER;
	private static final String PDF_TITLE;

	/**
	 * 設定を読み込む
	 */
	static {

		Properties props = new Properties();

		try {
			props.load(new FileInputStream(new File(System.getProperty("user.dir") + File.separator + "conf" + File.separator + PROPERTIES)));
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("プロパティファイルが見つかりません。");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		FILE_OUTPUT_DIR = (String) props.get("FileOutputDir");
		INPUT_DATA_DIR = (String) props.get("InputDataDir");
		PDF_SCRIPT_DIR = (String) props.get("PDFScriptDir");
		OUT_FILE_PREFIX = (String) props.get("OutFilePrefix");
		INPUT_CHARSET = (String) props.get("InputCharset");
		OUTPUT_CHARSET = (String) props.get("OutputCharset");
		OUT_DELIMITER = (String) props.get("OutDelimiter");
		EXAMCDS_ALLOWED = CollectionUtil.deconcatComma((String) props.get("ExamCdsAllowed"));
		STATISTICS_TYPE = Integer.parseInt((String) props.get("StatisticsType"));
		DELETE_YEARSAGO = Integer.parseInt((String) props.get("DelYearsAgo"));
		
		DB_DRIVER = (String) props.get("DBDriver");
		DB_URL = (String) props.get("DBUrl");
		DB_USERNAME = (String) props.get("DBUserName");
		DB_PASSWORD = (String) props.get("DBPassWord");
		
		PDF_AUTHER = (String) props.get("PdfAuther");
		PDF_TITLE = (String) props.get("PdfTitle");
		
		DOMConfigurator.configure((String) props.get("Log4jConfig"));
		
	}
	
	/**
	 * 
	 * @param string
	 * @param start
	 * @param num
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String getByteString(String string, int start, int num) throws UnsupportedEncodingException{
		return StringUtil.getByteString(string, start, num, INPUT_CHARSET);
	}
	
	/**
	 * 
	 * @param string
	 * @return
	 */
	private String zen2Han(String string){
		return StringUtil.zen2Han(string);
	}
	
	/**
	 * 出力先ディレクトリパスを返す
	 * [ディレクトリパス]+[年度]+[模試コード]
	 * @param fileOutputDir ディレクトリパス
	 * @param examYear 模試年度
	 * @param examCd 模試コード
	 * @return
	 */
	private static String getOutDirPath(String fileOutputDir, String examYear, String examCd){
		return fileOutputDir + File.separator + examYear + File.separator + examCd;
	}
	
	/**
	 * 出力先ファイルリパスを返す
	 * [ディレクトリパス]+[模試コード]+[大学区分コード]+[地区コード]
	 * @param outFileDir
	 * @param examCd
	 * @param univExamDiv
	 * @param areaDiv
	 * @param extension
	 */
	private static String getOutFilePath(String outFileDir, String examCd, String univExamDiv, String areaCd, String extension){
		
		// 記述・私大系模試の場合は大学区分コードを変換する
		if ("06".equals(univExamDiv)) univExamDiv = "01";
		if ("07".equals(univExamDiv)) univExamDiv = "04";
		if ("08".equals(univExamDiv)) univExamDiv = "05";
		
		return outFileDir + File.separator + OUT_FILE_PREFIX + examCd + univExamDiv + areaCd + "."+ extension;
	}
	
	/**
     * KEYPOINT メインメソッド
     * @param args
	 */
	public static void main(String[] args) {
		logger.info("バッチ処理を開始します。");

		Connection con = null;
		
		//0:正常終了 1:異常終了
		int errorFlag = 0;
		
		//バッチレベル
		try {

			//DBから模試情報の取得
			con = DBUtil.openConnection(DB_DRIVER, DB_URL, DB_USERNAME, DB_PASSWORD);
			con.setAutoCommit(false);
			
			/*
			 * ■MakeStatsの取得 
			 * ※対象の模試を取得
			 */
			MakeStatsSearchBean sb = new MakeStatsSearchBean();
			sb.setConnection("", con);
			sb.setStatisticsType(STATISTICS_TYPE);
			sb.execute();
			List targetExams = sb.getRecordSetAll();
			if(targetExams.size() == 0){
				//正常終了とする
				logger.info("テーブル内に情報がありません。"+targetExams.size());
			}
			
			//テーブルにある模試の数だけ実行する
			for(int j=0; j<targetExams.size(); j++){
				
				//ファイル単位でエラーが起こったかどうか
				boolean isError = false;
				
				MakeStatsData stat = null;
				
				//模試レベル
				try{
					
					stat = (MakeStatsData) targetExams.get(j);
					
					//許可される模試コードでないのでテーブルから削除して、次の模試へ進む
					if(!Arrays.asList(EXAMCDS_ALLOWED).contains(stat.getExamCd())){
						MakeStatsDeleteBean sdb = new MakeStatsDeleteBean();
						sdb.setConnection("", con);
						sdb.setStatisticsType(STATISTICS_TYPE);
						sdb.setExamYear(stat.getExamYear());
						sdb.setExamCd(stat.getExamCd());
						sdb.execute();
						con.commit();
						throw new Exception("バッチ処理の許可されない模試なので、ＤＢから削除します。"+stat.getExamYear()+":"+stat.getExamCd());
					}
					
					/*
					 * ■Examinationの取得 
					 * ※模試名を取得
					 */
					ExamSearchBean eb = new ExamSearchBean();
					eb.setConnection("", con);
					eb.setExamYear(stat.getExamYear());
					eb.setExamCd(stat.getExamCd());
					eb.execute();
					List ebs = eb.getRecordSetAll();
					if(ebs.size() != 1){
						throw new Exception("EXAMINATIONテーブルのこの模試の情報が正しくありません。"+stat.getExamYear()+":"+stat.getExamCd());
					}
					ExaminationData exam = (ExaminationData) ebs.get(0);
		
					logger.info("模試を実行します。" + exam.getExamYear()+":"+exam.getExamCd());
				
					/*
					 * ■ファイル処理
					 * ※PDF作成
					 */
					F002_5Batch batch = new F002_5Batch();
					//ホストファイルのある場所
					String inputDataPath = INPUT_DATA_DIR + File.separator + exam.getExamYear() + File.separator + exam.getExamCd();
					File dir = new File(inputDataPath);
					if (dir.isDirectory()) {
						File[] file = dir.listFiles();
						if(file.length == 0){
							throw new Exception("指定の模試のホストファイルは存在しません。"+exam.getExamYear()+":"+exam.getExamNameAbbr());
						}
						for (int i=0; file!=null && i<file.length; i++) {
							//ディレクトリは無視する
							if (file[i].isDirectory()) continue;
							
							try{
								logger.info("ファイル処理: " + file[i].getPath());
								//ファイル処理実行
								batch.execute(file[i], exam.getExamYear(), exam.getExamCd(), exam.getExamName(), getOutDirPath(FILE_OUTPUT_DIR, exam.getExamYear(), exam.getExamCd()));
							}catch(Exception e){
								
								//ファイル処理に失敗したら、次のファイルに進む
								logger.fatal("ファイル処理に失敗しました。"+e);
								e.printStackTrace();
								
								//エラーフラグを立てる
								isError = true;
							}
						}
					}else{
						throw new Exception("指定の模試のホストファイルは存在しません。"+dir.getPath());			
					}
		
					/*
					 * ■全地区のPDFファイルをまとめたzipファイルを作成する
					 * ure[模試コード][大学区分コード]00pdf.zip
					 */
					String outDirPath = getOutDirPath(FILE_OUTPUT_DIR, exam.getExamYear(), exam.getExamCd());
					File[] temps = new File(outDirPath).listFiles();
					Map map = new HashMap();
					for(int i=0; i<temps.length; i++){
						//このファイル名
						String fileName = temps[i].getName();
						//PDFファイルである
						if(fileName.substring(fileName.indexOf("."), fileName.length()).equals(".pdf")){
							String thisUnivExamDiv = fileName.substring(5, 7);
							//初めての大学区分
							if(map.get(thisUnivExamDiv) == null){
								map.put(thisUnivExamDiv, new ArrayList());
							}
							//大学区分キーのリストにこのファイルを追加
							((List)map.get(thisUnivExamDiv)).add(temps[i]);
						}
					}
					//大学区分の数だけループする
					for(Iterator it = map.entrySet().iterator(); it.hasNext();) {
						Map.Entry entry = (Map.Entry)it.next();
						String univExamDiv = (String) entry.getKey();
						List pdfFiles = (List) entry.getValue();
						//短大でないときのみzipファイルを作成する
						if(!Arrays.asList(new String[]{"05", "08", "03"}).contains(univExamDiv)){
							String zippedFilePath = outDirPath + File.separator +"ure"+exam.getExamCd()+univExamDiv+"00pdf.zip";
							//System.out.println("-- ZIP File Path : "+zippedFilePath);
							//System.out.println("公私区分は "+univExamDiv+" です。");
							FileUtil.zipFiles(zippedFilePath, (File[])pdfFiles.toArray(new File[pdfFiles.size()]));
						}
					}
					
					//ファイル処理でエラーがひとつもなかったときのみ、以下を実行する
					if(!isError){

						/*
						 * ■バッチ処理対象となった模試のX年前の同じ模試コードのPDFファイルを模試ディレクトリごと削除する
						 */
						//削除対象年度＆削除対象模試以下を削除
						String targetYearAgo = Integer.toString(Integer.parseInt(exam.getExamYear()) - DELETE_YEARSAGO);
						String delDir = FILE_OUTPUT_DIR + File.separator + targetYearAgo + File.separator + exam.getExamCd();
						//logger.info(targetYearAgo+"年模試コード:"+exam.getExamCd()+"のファイルをすべて削除。以下のファイル: "+delDir);
						File examDir = new File(delDir);
						//recursive delete
						FileUtil.deleteDir(examDir);
						//削除した模試ディレクトリの親である年ディレクトリが空ならば、その年ディレクトリも削除する
						if(examDir.getParentFile().delete()){
							//logger.info("年ディレクトリ："+targetYearAgo+"は空なので削除しました。");
						}
								 
						/*
						 * ■バッチ対象模試データをMAKESTATISTICSテーブルより削除する
						 */				 
						MakeStatsDeleteBean sdb = new MakeStatsDeleteBean();
						sdb.setConnection("", con);
						sdb.setStatisticsType(STATISTICS_TYPE);
						sdb.setExamYear(exam.getExamYear());
						sdb.setExamCd(exam.getExamCd());
						sdb.execute();
						
						//DB開放
						con.commit();
						
					}
					//ファイル処理にひとつでも失敗していれば、異常終了とする
					else{
						//エラーフラグを異常終了にする
						errorFlag = 1;
						
					}
				
				} catch(Exception e){
					
					//次の模試に進む
					logger.fatal("模試処理に失敗しました。"+e);
					e.printStackTrace();
					
					//ロールバック
					try{
						con.rollback();
					}catch(SQLException sqle){
						logger.fatal("ロールバックに失敗しました。");
					}
					
					//エラーフラグを異常終了にセット
					errorFlag = 1;
				}
					
			}
			
		} catch (Exception e) {
			logger.fatal("バッチ処理中に例外が発生しました。"+e);
			errorFlag = 1;
			
		} finally{
			try {
				DBUtil.closeConnection(con);
			} catch (Exception e1) {
				logger.fatal("DBのクローズ処理中に例外が発生しました。", e1);
				errorFlag = 1;
			}
		}

		logger.info("バッチ処理を終了します。("+errorFlag+")");
		System.exit(errorFlag);
	}
	
	/**
	 * KEIPOINT execute 実行部
	 * @param file 入力ファイル
	 * @throws Exception
	 */
	public void execute(File inFile, String examYear, String examCd, String examName, String outFileDir) throws Exception {
		
		//ファイル名
		String fileName = inFile.getName();
		//国私区分:マーク（国公立)・マーク（私立）・記述（国公立）・記述（私立）
		String univExamDiv = fileName.substring(fileName.length()-2, fileName.length());
		//地区コード
		String areaCd = fileName.substring(fileName.length()-4, fileName.length()-2);
		
		//国私区分名(出力用)
		String univExamDivName = "";
		switch(Integer.parseInt(univExamDiv)){
			case 1 : univExamDivName = "国公立";
				break;
			case 2 : univExamDivName = "共テ私大";
				break;
			case 3 : univExamDivName = "共テ短大";
				break;
			case 4 : univExamDivName = "私立";
				break;
			case 5 : univExamDivName = "短大その他";
				break;
			case 6 : univExamDivName = "国公立";
				break;
			case 7 : univExamDivName = "私立";
				break;
			case 8 : univExamDivName = "短大その他";
				break;
			default : 
				throw new Exception("国私区分が正しくありません。"+univExamDiv);
		}
		
		//地区名(出力用)
		String areaName = "";
		switch(Integer.parseInt(areaCd)){
			case 1: areaName = "北海道";
				break;
			case 2: areaName = "東北";
				break;
			case 3: areaName = "関東";
				break;
			case 4: areaName = "甲信越";
				break;
			case 5: areaName = "北陸";
				break;
			case 6: areaName = "東海";
				break;
			case 7: areaName = "近畿";
				break;
			case 8: areaName = "中国";
				break;
			case 9: areaName = "四国";
				break;
			case 10: areaName = "九州・沖縄";
				break;
			case 11: areaName = "東京";
				break;
			case 0: areaName = "全国";
				break;
			default : 
				throw new Exception("地区が正しくありません。"+areaCd);
		}
		
		//書き出し用ファイル格納ディレクトリ作成
		new File(outFileDir).mkdirs();
		//書き出し用データファイル作成
		String outFilePath = getOutFilePath(outFileDir, examCd, univExamDiv, areaCd, "dat");
		File outFile = new File(outFilePath);
		//System.out.println("-- Dat File Path : "+outFile);
		
		//帳票定義体名
		String scriptName = null;
		
		BufferedReader reader = null;
		BufferedWriter writer = null;
		try{
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), INPUT_CHARSET));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), OUTPUT_CHARSET));
			
			//区分別に入出力を行う
			//TypeA
			if(univExamDiv.equals("01")){
				writeTypeA(writer, readTypeA(reader), examYear, examName, univExamDivName, areaName);
				scriptName = "f002_5_1";
			}
			//TypeB
			else if(univExamDiv.equals("02") || univExamDiv.equals("03")){
				writeTypeB(writer, readTypeB(reader), examYear, examName, univExamDivName, areaName);
				scriptName = "f002_5_1";
			}
			//TypeC
			else if(univExamDiv.equals("04") || univExamDiv.equals("05")){
				writeTypeC(writer, readTypeC(reader), examYear, examName, univExamDivName, areaName);
				scriptName = "f002_5_2";
			}
			//TypeD
			else if(univExamDiv.equals("06")){
				writeTypeD(writer, readTypeD(reader), examYear, examName, univExamDivName, areaName);
				scriptName = "f002_5_3";
			}
			//TypeE
			else if(univExamDiv.equals("07") || univExamDiv.equals("08")){
				writeTypeE(writer, readTypeE(reader), examYear, examName, univExamDivName, areaName);
				scriptName = "f002_5_4";
			}
			//エラー
			else{
				throw new IllegalArgumentException("不明な区分情報です。 "+univExamDiv);	
			}
			
		}finally{
			if(reader != null) reader.close();
			if(writer != null) writer.close();
		}
		
		//PDFファイルフルパス
		String pdfFilePath = getOutFilePath(outFileDir, examCd, univExamDiv, areaCd, "pdf");
		//System.out.println("-- PDF File Path : "+pdfFilePath);
		//PDFの出力処理
		PDFUtil.createPDF(pdfFilePath, outFile.getPath(), PDF_SCRIPT_DIR, scriptName, OUT_DELIMITER, FormBase.CODE_UTF8, PDF_AUTHER, PDF_TITLE);		
				
		//PDFを作成し終えたら、使用したデータファイルを削除
		//System.out.println("-- Deleting Dat File : "+outFile.getPath());
		outFile.delete();
      
	}
	
	/**
	 * KEYPOINT writeTypeA マーク（国公立）
	 * @param writer
	 * @param list
	 * @throws IOException
	 */
	private void writeTypeA(BufferedWriter writer, List list, String examYear, String examName, String univExamDivName, String areaName) throws IOException{
		for(int i=0; i<list.size(); i++){
			Map m = (Map)list.get(i);
			
			writer.write(examYear);
			writer.write(OUT_DELIMITER);
			writer.write(examName);
			writer.write(OUT_DELIMITER);
			writer.write(univExamDivName+"（"+areaName+"）");
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange1")).trim());//半角に直す zen2Han(string); 空白は削除 trim();
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange6")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange7")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange8")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange9")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange10")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange17")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange18")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange19")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("scheduleName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("univName"));			
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder1"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder2"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder3"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder4"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder5"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder6"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder7"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder8"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder9"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder10"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder11"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder12"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder13"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder14"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder15"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder16"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder17"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder18"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder19"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("facultyName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("deptName"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("capacity")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division1"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum101"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum102"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum103"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum104"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum105"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum106"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum107"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum108"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum109"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum110"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum111"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum112"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum113"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum114"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum115"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum116"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum117"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum118"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum119"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("border")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("fullPoint")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division2"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgScore2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum201"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum202"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum203"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum204"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum205"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum206"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum207"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum208"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum209"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum210"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum211"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum212"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum213"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum214"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum215"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum216"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum217"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum218"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum219"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgScore2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgScore2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore21")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore22")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore23")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore24")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore25")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore26")).trim());
						
			//改行
			writer.newLine();
		}
	}
	
	/**
	 * KEYPOINT writeTypeB マーク（センター参加私大）
	 * @param writer
	 * @param list
	 * @throws IOException
	 */
	private void writeTypeB(BufferedWriter writer, List list, String examYear, String examName, String univExamDivName, String areaName) throws IOException{
		for(int i=0; i<list.size(); i++){
			Map m = (Map)list.get(i);
			
			writer.write(examYear);
			writer.write(OUT_DELIMITER);
			writer.write(examName);
			writer.write(OUT_DELIMITER);
			writer.write(univExamDivName+"（"+areaName+"）");
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange1")).trim());//半角に直す zen2Han(string); 空白は削除 trim();
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange6")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange7")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange8")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange9")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange10")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange17")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange18")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("scoreRange19")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("scheduleName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("univName"));			
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder1"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder2"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder3"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder4"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder5"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder6"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder7"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder8"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder9"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder10"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder11"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder12"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder13"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder14"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder15"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder16"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder17"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder18"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreBorder19"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("facultyName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("deptName"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("capacity")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write((String)m.get("division1"));//空で出力
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum101"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum102"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum103"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum104"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum105"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum106"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum107"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum108"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum109"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum110"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum111"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum112"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum113"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum114"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum115"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum116"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum117"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum118"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("scoreRangeNum119"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("border")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("fullPoint")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write((String)m.get("division2"));
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("wishedNum2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("avgScore2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum201"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum202"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum203"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum204"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum205"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum206"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum207"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum208"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum209"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum210"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum211"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum212"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum213"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum214"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum215"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum216"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum217"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum218"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(zen2Han(((String)m.get("scoreRangeNum219"))).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("activeNum2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("activeAvgScore2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("graduateNum2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("graduateAvgScore2")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore21")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore22")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore23")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore24")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore25")).trim());
			writer.write(OUT_DELIMITER);
			//writer.write(((String)m.get("courseScore26")).trim());
						
			//改行
			writer.newLine();
		}
	}
	
	/**
	 * KEYPOINT writeTypeC マーク（マーク私大）
	 * @param writer
	 * @param list
	 * @throws IOException
	 */
	private void writeTypeC(BufferedWriter writer, List list, String examYear, String examName, String univExamDivName, String areaName) throws IOException{
		for(int i=0; i<list.size(); i++){
			Map m = (Map)list.get(i);
			
			writer.write(examYear);
			writer.write(OUT_DELIMITER);
			writer.write(examName);
			writer.write(OUT_DELIMITER);
			writer.write(univExamDivName+"（"+areaName+"）");
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("univName"));
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank1")).trim());//半角に直す zen2Han(string); 空白は削除 trim();
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank6")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank7")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank8")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank9")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank10")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank17")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank18")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank19")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("facultyName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("deptName"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("borderRank")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("capacity")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution1"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution2"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution3"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution4"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution5"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution6"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution7"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution8"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution9"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution10"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution11"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution12"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution13"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution14"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution15"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution16"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution17"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution18"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution19"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore6")).trim());
						
			//改行
			writer.newLine();
		}
	}

	/**
	 * KEYPOINT writeTypeD 記述(国公立)
	 * @param writer
	 * @param list
	 * @throws IOException
	 */
	private void writeTypeD(BufferedWriter writer, List list, String examYear, String examName, String univExamDivName, String areaName) throws IOException{
		for(int i=0; i<list.size(); i++){
			Map m = (Map)list.get(i);
			
			writer.write(examYear);
			writer.write(OUT_DELIMITER);
			writer.write(examName);
			writer.write(OUT_DELIMITER);
			writer.write(univExamDivName+"（"+areaName+"）");
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("univName"));
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank1")).trim());//半角に直す zen2Han(string); 空白は削除 trim();
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank6")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank7")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank8")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank9")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank10")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank17")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank18")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank19")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("facultyName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("deptName"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("borderRank")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("capacity")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division1"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgDeviation1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution101"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution102"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution103"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution104"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution105"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution106"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution107"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution108"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution109"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution110"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution111"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution112"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution113"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution114"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution115"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution116"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution117"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution118"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution119"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgDeviation1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgDeviation1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division2"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgDeviation2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution201")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution202")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution203")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution204")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution205")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution206")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution207")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution208")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution209")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution210")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution211")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution212")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution213")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution214")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution215")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution216")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution217")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution218")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistribution219")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgDeviation2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgDeviation2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore21")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore22")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore23")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore24")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore25")).trim());
						
			//改行
			writer.newLine();
		}
	}

	/**
	 * KEYPOINT writeTypeE 記述(私立)
	 * @param writer
	 * @param list
	 * @throws IOException
	 */
	private void writeTypeE(BufferedWriter writer, List list, String examYear, String examName, String univExamDivName, String areaName) throws IOException{
		for(int i=0; i<list.size(); i++){
			Map m = (Map)list.get(i);
			
			writer.write(examYear);
			writer.write(OUT_DELIMITER);
			writer.write(examName);
			writer.write(OUT_DELIMITER);
			writer.write(univExamDivName+"（"+areaName+"）");
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("univName"));
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank1")).trim());//半角に直す zen2Han(string); 空白は削除 trim();
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank5")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank6")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank7")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank8")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank9")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank10")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank11")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank12")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank13")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank14")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank15")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank16")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank17")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank18")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han((String)m.get("freqDistRank19")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("facultyName"));
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("deptName"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("borderRank")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("capacity")).trim());
			writer.write(OUT_DELIMITER);
			writer.write((String)m.get("division"));
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("wishedNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("avgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution1"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution2"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution3"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution4"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution5"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution6"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution7"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution8"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution9"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution10"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution11"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution12"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution13"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution14"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution15"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution16"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution17"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution18"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(zen2Han(((String)m.get("freqDistribution19"))).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("activeAvgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateNum")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("graduateAvgDeviation")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore1")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore2")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore3")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore4")).trim());
			writer.write(OUT_DELIMITER);
			writer.write(((String)m.get("courseScore5")).trim());
						
			//改行
			writer.newLine();
		}
	}
		
	/**
	 * KEYPOINT readTypeA マーク（国公立）
	 * @param reader
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private List readTypeA(BufferedReader reader) throws UnsupportedEncodingException, IOException{
		
		final int END_PAGE = 81;
		
		//大学ごとにマップを追加していく
		List list = new ArrayList();
		//マップをつかうことで、後で好きな順に取り出せる
		Map map = null;

//[4] del start
//		//今回の大学名
//		String preUnivName = "";
//		//今回の学部名
//		String preFacultyName = "";
//		
//		//前回の大学名
//		String prePreUnivName = "";//[3] add
//[4] del end
		
		//行番号(pageNum × pageLineNumのはず)
		int lineNum = 1;
		//ページ内の行番号
		int pageLineNum = 1;
		String line = null;
		while ((line = reader.readLine()) != null) {
						
			//ページが変わった
			if((lineNum - 1) % END_PAGE == 0){
				pageLineNum = 1;
			}
						
			//ページ番号が書いてある最後の行は無視
			if(lineNum % END_PAGE == 0){
				// do nothing
			}
			//空白行は無視
			else if(line.trim().length() == 0){
				// do nothing
			}
			//無視の箇所
			else if(pageLineNum == 1 || pageLineNum == 2 || pageLineNum == 3 || pageLineNum == 4){
				// do nothing
			}
			//一行目
			else if((pageLineNum - 1 ) % 4 == 0){		
							
				map = new HashMap();
				list.add(map);
											
				//換算得点帯1〜19
				map.put("scoreRange1", getByteString(line, 59, 8));
				map.put("scoreRange2", getByteString(line, 67, 8));
				map.put("scoreRange3", getByteString(line, 75, 8));
				map.put("scoreRange4", getByteString(line, 83, 8));
				map.put("scoreRange5", getByteString(line, 91, 8));
				map.put("scoreRange6", getByteString(line, 99, 8));
				map.put("scoreRange7", getByteString(line, 107, 8));
				map.put("scoreRange8", getByteString(line, 115, 8));
				map.put("scoreRange9", getByteString(line, 123, 8));
				map.put("scoreRange10", getByteString(line, 131, 8));
				map.put("scoreRange11", getByteString(line, 139, 8));
				map.put("scoreRange12", getByteString(line, 147, 8));
				map.put("scoreRange13", getByteString(line, 155, 8));
				map.put("scoreRange14", getByteString(line, 163, 8));
				map.put("scoreRange15", getByteString(line, 171, 8));
				map.put("scoreRange16", getByteString(line, 179, 8));
				map.put("scoreRange17", getByteString(line, 187, 8));
				map.put("scoreRange18", getByteString(line, 195, 8));
				map.put("scoreRange19", getByteString(line, 203, 8));
	
			}
			//二行目
			else if((pageLineNum - 2) % 4 == 0){
				
				//日程名
				map.put("scheduleName", getByteString(line, 2, 2));
//[4] del start				
//				//前回の大学名を覚えておく
//				prePreUnivName = preUnivName;//[3] add
//				
//				//大学名
//				String thisUnivName = getByteString(line, 4, 14);
//				//大学名が存在しなければ、前回と同じ
//				if(zen2Han(thisUnivName).trim().equals("")){
//					//大学名
//					map.put("univName", preUnivName);
//				}
//				//大学名が存在すれば、今回のを使う
//				else{
//					map.put("univName", thisUnivName);
//				}
//				preUnivName = (String) map.get("univName");
//[4] del end
				map.put("univName", getByteString(line, 4, 14));//[4] add
				
				//換算得点ボーダー1〜19
				map.put("scoreBorder1", getByteString(line, 61, 2));
				map.put("scoreBorder2", getByteString(line, 69, 2));
				map.put("scoreBorder3", getByteString(line, 77, 2));
				map.put("scoreBorder4", getByteString(line, 85, 2));
				map.put("scoreBorder5", getByteString(line, 93, 2));
				map.put("scoreBorder6", getByteString(line, 101, 2));
				map.put("scoreBorder7", getByteString(line, 109, 2));
				map.put("scoreBorder8", getByteString(line, 117, 2));
				map.put("scoreBorder9", getByteString(line, 125, 2));
				map.put("scoreBorder10", getByteString(line, 133, 2));
				map.put("scoreBorder11", getByteString(line, 141, 2));
				map.put("scoreBorder12", getByteString(line, 149, 2));
				map.put("scoreBorder13", getByteString(line, 157, 2));
				map.put("scoreBorder14", getByteString(line, 165, 2));
				map.put("scoreBorder15", getByteString(line, 173, 2));
				map.put("scoreBorder16", getByteString(line, 181, 2));
				map.put("scoreBorder17", getByteString(line, 189, 2));
				map.put("scoreBorder18", getByteString(line, 197, 2));
				map.put("scoreBorder19", getByteString(line, 205, 2));
			}
			//三行目
			else if((pageLineNum - 3) % 4 == 0){
				
//[4] del start
//				//学部名
//				String thisFacultyName = getByteString(line, 6, 10);
//				//学部が存在しなくて、しかも大学名が前回と同じならば、前回の学部と同じ
//				//if(zen2Han(thisFacultyName).trim().equals("")){//[3] del
//				if(zen2Han(thisFacultyName).trim().equals("") && prePreUnivName.equals(preUnivName)){//[3] add
//					map.put("facultyName", preFacultyName);
//				}else{
//					//学部名
//					map.put("facultyName", thisFacultyName);
//				}
//				preFacultyName = (String) map.get("facultyName");
//[4] del end

				map.put("facultyName", getByteString(line, 6, 10));//[4] add

				//学科名
				map.put("deptName", getByteString(line, 18, 12));
				//定員
				map.put("capacity", getByteString(line, 33, 4));
				//区分１
				map.put("division1", getByteString(line, 37, 4));
				//志望人数１
				map.put("wishedNum1", getByteString(line, 43, 4));
				//平均換算得点１
				//map.put("avgScore1", getByteString(line, 48, 5));//[1] del
				map.put("avgScore1", getByteString(line, 47, 6));//[1] add
				//換算得点帯人数101〜119
				map.put("scoreRangeNum101", getByteString(line, 53, 8));
				map.put("scoreRangeNum102", getByteString(line, 61, 8));
				map.put("scoreRangeNum103", getByteString(line, 69, 8));
				map.put("scoreRangeNum104", getByteString(line, 77, 8));
				map.put("scoreRangeNum105", getByteString(line, 85, 8));
				map.put("scoreRangeNum106", getByteString(line, 93, 8));
				map.put("scoreRangeNum107", getByteString(line, 101, 8));
				map.put("scoreRangeNum108", getByteString(line, 109, 8));
				map.put("scoreRangeNum109", getByteString(line, 117, 8));
				map.put("scoreRangeNum110", getByteString(line, 125, 8));
				map.put("scoreRangeNum111", getByteString(line, 133, 8));
				map.put("scoreRangeNum112", getByteString(line, 141, 8));
				map.put("scoreRangeNum113", getByteString(line, 149, 8));
				map.put("scoreRangeNum114", getByteString(line, 157, 8));
				map.put("scoreRangeNum115", getByteString(line, 165, 8));
				map.put("scoreRangeNum116", getByteString(line, 173, 8));
				map.put("scoreRangeNum117", getByteString(line, 181, 8));
				map.put("scoreRangeNum118", getByteString(line, 189, 8));
				map.put("scoreRangeNum119", getByteString(line, 197, 8));
				//現役人数１
				map.put("activeNum1", getByteString(line, 206, 4));
				//現役平均換算得点１
				//map.put("activeAvgScore1", getByteString(line, 211, 5));//[1] del
				map.put("activeAvgScore1", getByteString(line, 210, 6));//[1] add
				//高卒人数１
				map.put("graduateNum1", getByteString(line, 217, 4));
				//高卒平均換算得点１
				//map.put("graduateAvgScore1", getByteString(line, 222, 5));//[1] del
				map.put("graduateAvgScore1", getByteString(line, 221, 6));//[1] add
				//教科別成績１１
				map.put("courseScore11", getByteString(line, 227, 4));
				//教科別成績１２
				map.put("courseScore12", getByteString(line, 231, 4));
				//教科別成績１３
				map.put("courseScore13", getByteString(line, 235, 4));
				//教科別成績１４
				map.put("courseScore14", getByteString(line, 239, 4));
				//教科別成績１５
				map.put("courseScore15", getByteString(line, 243, 4));
				//教科別成績１６
				map.put("courseScore16", getByteString(line, 247, 4));
	
			}
			//四行目
			else if((pageLineNum - 4) % 4 == 0){
				//ボーダー
				map.put("border", getByteString(line, 25, 4));
				//満点値
				map.put("fullPoint", getByteString(line, 31, 4));
				//区分２
				map.put("division2", getByteString(line, 41, 4));
				//志望人数２
				map.put("wishedNum2", getByteString(line, 47, 4));
				//平均換算得点２
				//map.put("avgScore2", getByteString(line, 52, 5));//[1] del
				map.put("avgScore2", getByteString(line, 51, 6));//[1] add
				//換算得点帯人数201〜219
				map.put("scoreRangeNum201", getByteString(line, 57, 8));
				map.put("scoreRangeNum202", getByteString(line, 65, 8));
				map.put("scoreRangeNum203", getByteString(line, 73, 8));
				map.put("scoreRangeNum204", getByteString(line, 81, 8));
				map.put("scoreRangeNum205", getByteString(line, 89, 8));
				map.put("scoreRangeNum206", getByteString(line, 97, 8));
				map.put("scoreRangeNum207", getByteString(line, 105, 8));
				map.put("scoreRangeNum208", getByteString(line, 113, 8));
				map.put("scoreRangeNum209", getByteString(line, 121, 8));
				map.put("scoreRangeNum210", getByteString(line, 129, 8));
				map.put("scoreRangeNum211", getByteString(line, 137, 8));
				map.put("scoreRangeNum212", getByteString(line, 145, 8));
				map.put("scoreRangeNum213", getByteString(line, 153, 8));
				map.put("scoreRangeNum214", getByteString(line, 161, 8));
				map.put("scoreRangeNum215", getByteString(line, 169, 8));
				map.put("scoreRangeNum216", getByteString(line, 177, 8));
				map.put("scoreRangeNum217", getByteString(line, 185, 8));
				map.put("scoreRangeNum218", getByteString(line, 193, 8));
				map.put("scoreRangeNum219", getByteString(line, 201, 8));
				//現役人数２
				map.put("activeNum2", getByteString(line, 210, 4));
				//現役平均換算得点２
				//map.put("activeAvgScore2", getByteString(line, 215, 5));//[1] del
				map.put("activeAvgScore2", getByteString(line, 214, 6));//[1] add
				//高卒人数２
				map.put("graduateNum2", getByteString(line, 221, 4));
				//高卒平均換算得点２
				//map.put("graduateAvgScore2", getByteString(line, 226, 5));//[1] del
				map.put("graduateAvgScore2", getByteString(line, 225, 6));//[1] add
				//教科別成績２１
				map.put("courseScore21", getByteString(line, 231, 4));
				//教科別成績２２
				map.put("courseScore22", getByteString(line, 235, 4));
				//教科別成績２３
				map.put("courseScore23", getByteString(line, 239, 4));
				//教科別成績２４
				map.put("courseScore24", getByteString(line, 243, 4));
				//教科別成績２５
				map.put("courseScore25", getByteString(line, 247, 4));
				//教科別成績２６
				map.put("courseScore26", getByteString(line, 251, 4));
							
			}
			lineNum ++;
			pageLineNum ++;			
		}
		
		return list;
	}
	/**
	 * KEYPOINT readTypeB マーク（センター参加私大）
	 * 
	 * @param reader
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private List readTypeB(BufferedReader reader) throws UnsupportedEncodingException, IOException{
		final int END_PAGE = 81;
		
		//大学ごとにマップを追加していく
		List list = new ArrayList();
		//マップをつかうことで、後で好きな順に取り出せる
		Map map = null;
		
//[4] del start
//		//今回の大学名
//		String preUnivName = "";
//		//今回の学部名
//		String preFacultyName = "";
//		
//		//前回の大学名
//		String prePreUnivName = "";//[3] add
//[4] del end		

		//行番号(pageNum × pageLineNumのはず)
		int lineNum = 1;
		//ページ内の行番号
		int pageLineNum = 1;
		String line = null;
		while ((line = reader.readLine()) != null) {
						
			//ページが変わった
			if((lineNum - 1) % END_PAGE == 0){
				pageLineNum = 1;
			}
						
			//ページ番号が書いてある最後の行は無視
			if(lineNum % END_PAGE == 0){
				// do nothing
			}
			//空白行は無視
			else if(line.trim().length() == 0){
				// do nothing
			}
			//無視の箇所
			else if(pageLineNum == 1 || pageLineNum == 2 || pageLineNum == 3 || pageLineNum == 4){
				// do nothing
			}
			//一行目
			else if((pageLineNum - 1 ) % 4 == 0){		
							
				map = new HashMap();
				list.add(map);
											
				//換算得点帯1〜19
				map.put("scoreRange1", getByteString(line, 59, 8));
				map.put("scoreRange2", getByteString(line, 67, 8));
				map.put("scoreRange3", getByteString(line, 75, 8));
				map.put("scoreRange4", getByteString(line, 83, 8));
				map.put("scoreRange5", getByteString(line, 91, 8));
				map.put("scoreRange6", getByteString(line, 99, 8));
				map.put("scoreRange7", getByteString(line, 107, 8));
				map.put("scoreRange8", getByteString(line, 115, 8));
				map.put("scoreRange9", getByteString(line, 123, 8));
				map.put("scoreRange10", getByteString(line, 131, 8));
				map.put("scoreRange11", getByteString(line, 139, 8));
				map.put("scoreRange12", getByteString(line, 147, 8));
				map.put("scoreRange13", getByteString(line, 155, 8));
				map.put("scoreRange14", getByteString(line, 163, 8));
				map.put("scoreRange15", getByteString(line, 171, 8));
				map.put("scoreRange16", getByteString(line, 179, 8));
				map.put("scoreRange17", getByteString(line, 187, 8));
				map.put("scoreRange18", getByteString(line, 195, 8));
				map.put("scoreRange19", getByteString(line, 203, 8));
	
			}
			//二行目
			else if((pageLineNum - 2) % 4 == 0){
				
				//日程名
				map.put("scheduleName", getByteString(line, 2, 2));

//[4] del start				
//				//前回の大学名を覚えておく
//				prePreUnivName = preUnivName;//[3] add
//				
//				//大学名
//				String thisUnivName = getByteString(line, 4, 14);
//				//大学名が存在しなければ、前回と同じ
//				if(zen2Han(thisUnivName).trim().equals("")){
//					//大学名
//					map.put("univName", preUnivName);
//				}
//				//大学名が存在すれば、今回のを使う
//				else{
//					map.put("univName", thisUnivName);
//				}
//				preUnivName = (String) map.get("univName");	
//[4] del end
				map.put("univName", getByteString(line, 4, 14));//[4] add
				
				//換算得点ボーダー1〜19
				map.put("scoreBorder1", getByteString(line, 61, 2));
				map.put("scoreBorder2", getByteString(line, 69, 2));
				map.put("scoreBorder3", getByteString(line, 77, 2));
				map.put("scoreBorder4", getByteString(line, 85, 2));
				map.put("scoreBorder5", getByteString(line, 93, 2));
				map.put("scoreBorder6", getByteString(line, 101, 2));
				map.put("scoreBorder7", getByteString(line, 109, 2));
				map.put("scoreBorder8", getByteString(line, 117, 2));
				map.put("scoreBorder9", getByteString(line, 125, 2));
				map.put("scoreBorder10", getByteString(line, 133, 2));
				map.put("scoreBorder11", getByteString(line, 141, 2));
				map.put("scoreBorder12", getByteString(line, 149, 2));
				map.put("scoreBorder13", getByteString(line, 157, 2));
				map.put("scoreBorder14", getByteString(line, 165, 2));
				map.put("scoreBorder15", getByteString(line, 173, 2));
				map.put("scoreBorder16", getByteString(line, 181, 2));
				map.put("scoreBorder17", getByteString(line, 189, 2));
				map.put("scoreBorder18", getByteString(line, 197, 2));
				map.put("scoreBorder19", getByteString(line, 205, 2));
			}
			//三行目
			else if((pageLineNum - 3) % 4 == 0){

//[4] del start				
//				//学部名
//				String thisFacultyName = getByteString(line, 6, 10);
//				//if(zen2Han(thisFacultyName).trim().equals("")){//[3] del
//				if(zen2Han(thisFacultyName).trim().equals("") && prePreUnivName.equals(preUnivName)){//[3] add
//					map.put("facultyName", preFacultyName);
//				}else{
//					//学部名
//					map.put("facultyName", thisFacultyName);		
//				}
//				preFacultyName = (String) map.get("facultyName");
//[4] del end
				map.put("facultyName", getByteString(line, 6, 10));//[4] add
				
				//学科名
				map.put("deptName", getByteString(line, 18, 12));
				//定員
				map.put("capacity", getByteString(line, 33, 4));
				//志望人数１
				map.put("wishedNum1", getByteString(line, 45, 4));
				//平均換算得点１
				//map.put("avgScore1", getByteString(line, 50, 5));//[2] del
				map.put("avgScore1", getByteString(line, 49, 6));//[2] add
				//換算得点帯人数101〜119
				map.put("scoreRangeNum101", getByteString(line, 55, 8));
				map.put("scoreRangeNum102", getByteString(line, 63, 8));
				map.put("scoreRangeNum103", getByteString(line, 71, 8));
				map.put("scoreRangeNum104", getByteString(line, 79, 8));
				map.put("scoreRangeNum105", getByteString(line, 87, 8));
				map.put("scoreRangeNum106", getByteString(line, 95, 8));
				map.put("scoreRangeNum107", getByteString(line, 103, 8));
				map.put("scoreRangeNum108", getByteString(line, 111, 8));
				map.put("scoreRangeNum109", getByteString(line, 119, 8));
				map.put("scoreRangeNum110", getByteString(line, 127, 8));
				map.put("scoreRangeNum111", getByteString(line, 135, 8));
				map.put("scoreRangeNum112", getByteString(line, 143, 8));
				map.put("scoreRangeNum113", getByteString(line, 151, 8));
				map.put("scoreRangeNum114", getByteString(line, 159, 8));
				map.put("scoreRangeNum115", getByteString(line, 167, 8));
				map.put("scoreRangeNum116", getByteString(line, 175, 8));
				map.put("scoreRangeNum117", getByteString(line, 183, 8));
				map.put("scoreRangeNum118", getByteString(line, 191, 8));
				map.put("scoreRangeNum119", getByteString(line, 199, 8));
				//現役人数１
				map.put("activeNum1", getByteString(line, 208, 4));
				//現役平均換算得点１
				//map.put("activeAvgScore1", getByteString(line, 213, 5));//[2] del
				map.put("activeAvgScore1", getByteString(line, 212, 6));//[2] add
				//高卒人数１
				map.put("graduateNum1", getByteString(line, 219, 4));
				//高卒平均換算得点１
				//map.put("graduateAvgScore1", getByteString(line, 224, 5));//[2] del
				map.put("graduateAvgScore1", getByteString(line, 223, 6));//[2] add
				//教科別成績１１
				map.put("courseScore11", getByteString(line, 229, 4));
				//教科別成績１２
				map.put("courseScore12", getByteString(line, 233, 4));
				//教科別成績１３
				map.put("courseScore13", getByteString(line, 237, 4));
				//教科別成績１４
				map.put("courseScore14", getByteString(line, 241, 4));
				//教科別成績１５
				map.put("courseScore15", getByteString(line, 245, 4));
				//教科別成績１６
				map.put("courseScore16", getByteString(line, 249, 4));
	
			}
			//四行目
			else if((pageLineNum - 4) % 4 == 0){
				//ボーダー
				map.put("border", getByteString(line, 25, 4));
				//満点値
				map.put("fullPoint", getByteString(line, 31, 4));
							
			}
			lineNum ++;
			pageLineNum ++;			
		}
		
		return list;
	}
	
	/**
	 * KEYPOINT readTypeC マーク（マーク私大）
	 *
	 * @param reader
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private List readTypeC(BufferedReader reader) throws UnsupportedEncodingException, IOException{
		
		final int END_PAGE = 72;
		
		//大学ごとにマップを追加していく
		List list = new ArrayList();
		//マップをつかうことで、後で好きな順に取り出せる
		Map map = null;

		//今回の大学名
		String preUnivName = "";
		//今回の学部名
		String preFacultyName = "";

		//前回の大学名
		String prePreUnivName = "";

		//行番号(pageNum × pageLineNumのはず)
		int lineNum = 1;
		//ページ内の行番号
		int pageLineNum = 1;
		String line = null;
		while ((line = reader.readLine()) != null) {
						
			//ページが変わった
			if((lineNum - 1) % END_PAGE == 0){
				pageLineNum = 1;
			}
						
			//ページ番号が書いてある最後の行は無視
			if(lineNum % END_PAGE == 0){
				// do nothing
			}
			//空白行は無視
			else if(line.trim().length() == 0){
				// do nothing
			}
			//無視の箇所
			else if(pageLineNum == 1 || pageLineNum == 2){
				// do nothing
			}
			//罫線
			else if(line.substring(0, 2).equals(" ─")){
				pageLineNum ++;
			}			
			//一行目
			else if((pageLineNum - 1 ) % 2 == 0){									
							
				map = new HashMap();
				list.add(map);
				
				//前回の大学名を覚えておく
				prePreUnivName = preUnivName;//[3] add
				
				String thisUnivName = getByteString(line, 3, 14);
				//大学名が存在しなければ、前回と同じ
				if(zen2Han(thisUnivName).trim().equals("")){
					//大学名
					map.put("univName", preUnivName);
				}
				//大学名が存在すれば、今回のを使う
				else{
					map.put("univName", thisUnivName);
				}
				preUnivName = (String) map.get("univName");	

				//度数分布ランク1〜19
				map.put("freqDistRank1", getByteString(line, 60, 2));
				map.put("freqDistRank2", getByteString(line, 68, 2));
				map.put("freqDistRank3", getByteString(line, 76, 2));
				map.put("freqDistRank4", getByteString(line, 84, 2));
				map.put("freqDistRank5", getByteString(line, 92, 2));
				map.put("freqDistRank6", getByteString(line, 100, 2));
				map.put("freqDistRank7", getByteString(line, 108, 2));
				map.put("freqDistRank8", getByteString(line, 116, 2));
				map.put("freqDistRank9", getByteString(line, 124, 2));
				map.put("freqDistRank10", getByteString(line, 132, 2));
				map.put("freqDistRank11", getByteString(line, 140, 2));
				map.put("freqDistRank12", getByteString(line, 148, 2));
				map.put("freqDistRank13", getByteString(line, 156, 2));
				map.put("freqDistRank14", getByteString(line, 164, 2));
				map.put("freqDistRank15", getByteString(line, 172, 2));
				map.put("freqDistRank16", getByteString(line, 180, 2));
				map.put("freqDistRank17", getByteString(line, 188, 2));
				map.put("freqDistRank18", getByteString(line, 196, 2));
				map.put("freqDistRank19", getByteString(line, 204, 2));

	
			}
			//二行目
			else if((pageLineNum - 2) % 2 == 0){
				
				String thisFacultyName = getByteString(line, 4, 10);
				//if(zen2Han(thisFacultyName).trim().equals("")){//[3] del
				if(zen2Han(thisFacultyName).trim().equals("") && prePreUnivName.equals(preUnivName)){//[3] add
					map.put("facultyName", preFacultyName);
				}else{
					//学部名
					map.put("facultyName", thisFacultyName);		
				}
				preFacultyName = (String) map.get("facultyName");
			
				//学科名
				map.put("deptName", getByteString(line, 16, 12));
				//ボーダーランク
				map.put("borderRank", getByteString(line, 29, 4));
				//定員
				map.put("capacity", getByteString(line, 34, 4));
				//区分
				map.put("division", getByteString(line, 38, 4));
				//志望人数
				map.put("wishedNum", getByteString(line, 43, 4));
				//平均偏差値
				map.put("avgDeviation", getByteString(line, 48, 4));
				//度数分布表
				map.put("freqDistribution1", getByteString(line, 52, 8));
				map.put("freqDistribution2", getByteString(line, 60, 8));
				map.put("freqDistribution3", getByteString(line, 68, 8));
				map.put("freqDistribution4", getByteString(line, 76, 8));
				map.put("freqDistribution5", getByteString(line, 84, 8));
				map.put("freqDistribution6", getByteString(line, 92, 8));
				map.put("freqDistribution7", getByteString(line, 100, 8));
				map.put("freqDistribution8", getByteString(line, 108, 8));
				map.put("freqDistribution9", getByteString(line, 116, 8));
				map.put("freqDistribution10", getByteString(line, 124, 8));
				map.put("freqDistribution11", getByteString(line, 132, 8));
				map.put("freqDistribution12", getByteString(line, 140, 8));
				map.put("freqDistribution13", getByteString(line, 148, 8));
				map.put("freqDistribution14", getByteString(line, 156, 8));
				map.put("freqDistribution15", getByteString(line, 164, 8));
				map.put("freqDistribution16", getByteString(line, 172, 8));
				map.put("freqDistribution17", getByteString(line, 180, 8));
				map.put("freqDistribution18", getByteString(line, 188, 8));
				map.put("freqDistribution19", getByteString(line, 196, 8));
				//現役人数
				map.put("activeNum", getByteString(line, 205, 4));
				//現役平均偏差値
				map.put("activeAvgDeviation", getByteString(line, 210, 4));
				//高卒人数
				map.put("graduateNum", getByteString(line, 215, 4));
				//高卒平均偏差値
				map.put("graduateAvgDeviation", getByteString(line, 220, 4));
				//教科別成績１
				map.put("courseScore1", getByteString(line, 225, 4));
				//教科別成績２
				map.put("courseScore2", getByteString(line, 230, 4));
				//教科別成績３
				map.put("courseScore3", getByteString(line, 235, 4));
				//教科別成績４
				map.put("courseScore4", getByteString(line, 240, 4));
				//教科別成績５
				map.put("courseScore5", getByteString(line, 245, 4));
				//教科別成績６
				map.put("courseScore6", getByteString(line, 250, 4));

			}

			lineNum ++;
			pageLineNum ++;			
		}
		
		return list;
	}
	
	/**
	 * KEYPOINT readTypeD 記述(国公立)
	 *
	 * @param reader
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private List readTypeD(BufferedReader reader) throws UnsupportedEncodingException, IOException{
		
		final int END_PAGE = 72;
		
		//大学ごとにマップを追加していく
		List list = new ArrayList();
		//マップをつかうことで、後で好きな順に取り出せる
		Map map = null;

		//今回の大学名
		String preUnivName = "";
		//今回の学部名
		String preFacultyName = "";

		//前回の大学名
		String prePreUnivName = "";

		//行番号(pageNum × pageLineNumのはず)
		int lineNum = 1;
		//ページ内の行番号
		int pageLineNum = 1;
		String line = null;
		while ((line = reader.readLine()) != null) {
						
			//ページが変わった
			if((lineNum - 1) % END_PAGE == 0){
				pageLineNum = 1;
			}
						
			//ページ番号が書いてある最後の行は無視
			if(lineNum % END_PAGE == 0){
				// do nothing	
			}
			//空白行は無視
			else if(line.trim().length() == 0){
				// do nothing	
			}
			//無視の箇所
			else if(pageLineNum == 1 || pageLineNum == 2){
				// do nothing	
			}
			//罫線
			else if(line.substring(0, 4).equals("   ─")){
				pageLineNum += 2;;	
			}			
			//一行目
			else if((pageLineNum - 0 ) % 3 == 0){									
					
				map = new HashMap();
				list.add(map);
				
				//前回の大学名を覚えておく
				prePreUnivName = preUnivName;//[3] add
				
				String thisUnivName = getByteString(line, 3, 14);
				//大学名が存在しなければ、前回と同じ
				if(zen2Han(thisUnivName).trim().equals("")){
					//大学名
					map.put("univName", preUnivName);
				}
				//大学名が存在すれば、今回のを使う
				else{
					map.put("univName", thisUnivName);
				}
				preUnivName = (String) map.get("univName");	

				//度数分布ランク1〜19
				map.put("freqDistRank1", getByteString(line, 60, 2));
				map.put("freqDistRank2", getByteString(line, 68, 2));
				map.put("freqDistRank3", getByteString(line, 76, 2));
				map.put("freqDistRank4", getByteString(line, 84, 2));
				map.put("freqDistRank5", getByteString(line, 92, 2));
				map.put("freqDistRank6", getByteString(line, 100, 2));
				map.put("freqDistRank7", getByteString(line, 108, 2));
				map.put("freqDistRank8", getByteString(line, 116, 2));
				map.put("freqDistRank9", getByteString(line, 124, 2));
				map.put("freqDistRank10", getByteString(line, 132, 2));
				map.put("freqDistRank11", getByteString(line, 140, 2));
				map.put("freqDistRank12", getByteString(line, 148, 2));
				map.put("freqDistRank13", getByteString(line, 156, 2));
				map.put("freqDistRank14", getByteString(line, 164, 2));
				map.put("freqDistRank15", getByteString(line, 172, 2));
				map.put("freqDistRank16", getByteString(line, 180, 2));
				map.put("freqDistRank17", getByteString(line, 188, 2));
				map.put("freqDistRank18", getByteString(line, 196, 2));
				map.put("freqDistRank19", getByteString(line, 204, 2));
	
			}
			//二行目
			else if((pageLineNum - 1) % 3 == 0){
				
				String thisFacultyName = getByteString(line, 4, 10);
				//if(zen2Han(thisFacultyName).trim().equals("")){//[3] del
				if(zen2Han(thisFacultyName).trim().equals("") && prePreUnivName.equals(preUnivName)){//[3] add
					map.put("facultyName", preFacultyName);
				}else{
					//学部名
					map.put("facultyName", thisFacultyName);		
				}
				preFacultyName = (String) map.get("facultyName");
			
				//学科名
				map.put("deptName", getByteString(line, 16, 12));
				//ボーダーランク
				map.put("borderRank", getByteString(line, 29, 4));
				//定員
				map.put("capacity", getByteString(line, 34, 4));
				//区分１
				map.put("division1", getByteString(line, 38, 4));
				//志望人数１
				map.put("wishedNum1", getByteString(line, 43, 4));
				//平均偏差値１
				map.put("avgDeviation1", getByteString(line, 48, 4));
				//度数分布表101〜119
				map.put("freqDistribution101", getByteString(line, 52, 8));
				map.put("freqDistribution102", getByteString(line, 60, 8));
				map.put("freqDistribution103", getByteString(line, 68, 8));
				map.put("freqDistribution104", getByteString(line, 76, 8));
				map.put("freqDistribution105", getByteString(line, 84, 8));
				map.put("freqDistribution106", getByteString(line, 92, 8));
				map.put("freqDistribution107", getByteString(line, 100, 8));
				map.put("freqDistribution108", getByteString(line, 108, 8));
				map.put("freqDistribution109", getByteString(line, 116, 8));
				map.put("freqDistribution110", getByteString(line, 124, 8));
				map.put("freqDistribution111", getByteString(line, 132, 8));
				map.put("freqDistribution112", getByteString(line, 140, 8));
				map.put("freqDistribution113", getByteString(line, 148, 8));
				map.put("freqDistribution114", getByteString(line, 156, 8));
				map.put("freqDistribution115", getByteString(line, 164, 8));
				map.put("freqDistribution116", getByteString(line, 172, 8));
				map.put("freqDistribution117", getByteString(line, 180, 8));
				map.put("freqDistribution118", getByteString(line, 188, 8));
				map.put("freqDistribution119", getByteString(line, 196, 8));
				//現役人数１	現役人数	4	205	4
				map.put("activeNum1", getByteString(line, 205, 4));
				//現役平均偏差値１	現役平均偏差値	4	210	4
				map.put("activeAvgDeviation1", getByteString(line, 210, 4));
				//高卒人数１	高卒人数	4	215	4
				map.put("graduateNum1", getByteString(line, 215, 4));
				//高卒平均偏差値１	高卒平均偏差値	4	220	4
				map.put("graduateAvgDeviation1", getByteString(line, 220, 4));
				//教科別成績１１	教科別成績	4	225	4
				map.put("courseScore11", getByteString(line, 225, 4));
				//教科別成績１２	教科別成績	4	230	4
				map.put("courseScore12", getByteString(line, 230, 4));
				//教科別成績１３	教科別成績	4	235	4
				map.put("courseScore13", getByteString(line, 235, 4));
				//教科別成績１４	教科別成績	4	240	4
				map.put("courseScore14", getByteString(line, 240, 4));
				//教科別成績１５	教科別成績	4	245	4
				map.put("courseScore15", getByteString(line, 245, 4));

			}
			//三行目
			else if((pageLineNum - 2) % 3 == 0){
				
				//区分２
				map.put("division2", getByteString(line, 40, 4));
				//志望人数２
				map.put("wishedNum2", getByteString(line, 45, 4));
				//平均偏差値２
				map.put("avgDeviation2", getByteString(line, 50, 4));
				//度数分布表201〜219
				map.put("freqDistribution201", getByteString(line, 54, 8));
				map.put("freqDistribution202", getByteString(line, 62, 8));
				map.put("freqDistribution203", getByteString(line, 70, 8));
				map.put("freqDistribution204", getByteString(line, 78, 8));
				map.put("freqDistribution205", getByteString(line, 86, 8));
				map.put("freqDistribution206", getByteString(line, 94, 8));
				map.put("freqDistribution207", getByteString(line, 102, 8));
				map.put("freqDistribution208", getByteString(line, 110, 8));
				map.put("freqDistribution209", getByteString(line, 118, 8));
				map.put("freqDistribution210", getByteString(line, 126, 8));
				map.put("freqDistribution211", getByteString(line, 134, 8));
				map.put("freqDistribution212", getByteString(line, 142, 8));
				map.put("freqDistribution213", getByteString(line, 150, 8));
				map.put("freqDistribution214", getByteString(line, 158, 8));
				map.put("freqDistribution215", getByteString(line, 166, 8));
				map.put("freqDistribution216", getByteString(line, 174, 8));
				map.put("freqDistribution217", getByteString(line, 182, 8));
				map.put("freqDistribution218", getByteString(line, 190, 8));
				map.put("freqDistribution219", getByteString(line, 198, 8));
				//現役人数２
				map.put("activeNum2", getByteString(line, 207, 4));
				//現役平均偏差値２
				map.put("activeAvgDeviation2", getByteString(line, 212, 4));
				//高卒人数２
				map.put("graduateNum2", getByteString(line, 217, 4));
				//高卒平均偏差値２
				map.put("graduateAvgDeviation2", getByteString(line, 222, 4));
				//教科別成績21〜25
				map.put("courseScore21", getByteString(line, 227, 4));
				map.put("courseScore22", getByteString(line, 232, 4));
				map.put("courseScore23", getByteString(line, 237, 4));
				map.put("courseScore24", getByteString(line, 242, 4));
				map.put("courseScore25", getByteString(line, 247, 4));

			}

			lineNum ++;
			pageLineNum ++;			
		}
		
		return list;
	}
	
	/**
	 * KEYPOINT readTypeE 記述(私立)
	 *
	 * @param reader
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	private List readTypeE(BufferedReader reader) throws UnsupportedEncodingException, IOException{
		
		final int END_PAGE = 72;
		
		//大学ごとにマップを追加していく
		List list = new ArrayList();
		//マップをつかうことで、後で好きな順に取り出せる
		Map map = null;

		//今回の大学名
		String preUnivName = "";
		//今回の学部名
		String preFacultyName = "";

		//前回の大学名
		String prePreUnivName = "";

		//行番号(pageNum × pageLineNumのはず)
		int lineNum = 1;
		//ページ内の行番号
		int pageLineNum = 1;
		String line = null;
		while ((line = reader.readLine()) != null) {
						
			//ページが変わった
			if((lineNum - 1) % END_PAGE == 0){
				pageLineNum = 1;
			}
						
			//ページ番号が書いてある最後の行は無視
			if(lineNum % END_PAGE == 0){
				// do nothing
			}
			//空白行は無視
			else if(line.trim().length() == 0){
				// do nothing
			}
			//無視の箇所
			else if(pageLineNum == 1 || pageLineNum == 2){
				// do nothing
			}
			//罫線
			else if(line.substring(0, 2).equals(" ─")){
				pageLineNum ++;
			}			
			//一行目
			else if((pageLineNum - 1 ) % 2 == 0){									
							
				map = new HashMap();
				list.add(map);
				
				//前回の大学名を覚えておく
				prePreUnivName = preUnivName;
				
				String thisUnivName = getByteString(line, 3, 14);
				//大学名が存在しなければ、前回と同じ
				if(zen2Han(thisUnivName).trim().equals("")){
					//大学名
					map.put("univName", preUnivName);
				}
				//大学名が存在すれば、今回のを使う
				else{
					map.put("univName", thisUnivName);
				}
				preUnivName = (String) map.get("univName");	

				//度数分布ランク1〜19
				map.put("freqDistRank1", getByteString(line, 60, 2));
				map.put("freqDistRank2", getByteString(line, 68, 2));
				map.put("freqDistRank3", getByteString(line, 76, 2));
				map.put("freqDistRank4", getByteString(line, 84, 2));
				map.put("freqDistRank5", getByteString(line, 92, 2));
				map.put("freqDistRank6", getByteString(line, 100, 2));
				map.put("freqDistRank7", getByteString(line, 108, 2));
				map.put("freqDistRank8", getByteString(line, 116, 2));
				map.put("freqDistRank9", getByteString(line, 124, 2));
				map.put("freqDistRank10", getByteString(line, 132, 2));
				map.put("freqDistRank11", getByteString(line, 140, 2));
				map.put("freqDistRank12", getByteString(line, 148, 2));
				map.put("freqDistRank13", getByteString(line, 156, 2));
				map.put("freqDistRank14", getByteString(line, 164, 2));
				map.put("freqDistRank15", getByteString(line, 172, 2));
				map.put("freqDistRank16", getByteString(line, 180, 2));
				map.put("freqDistRank17", getByteString(line, 188, 2));
				map.put("freqDistRank18", getByteString(line, 196, 2));
				map.put("freqDistRank19", getByteString(line, 204, 2));

	
			}
			//二行目
			else if((pageLineNum - 2) % 2 == 0){
				
				String thisFacultyName = getByteString(line, 4, 10);
				//if(zen2Han(thisFacultyName).trim().equals("")){//[3] del
				if(zen2Han(thisFacultyName).trim().equals("") && prePreUnivName.equals(preUnivName)){//[3] add
					map.put("facultyName", preFacultyName);
				}else{
					//学部名
					map.put("facultyName", thisFacultyName);		
				}
				preFacultyName = (String) map.get("facultyName");
			
				//学科名
				map.put("deptName", getByteString(line, 16, 12));
				//ボーダーランク
				map.put("borderRank", getByteString(line, 29, 4));
				//定員
				map.put("capacity", getByteString(line, 34, 4));
				//区分
				map.put("division", getByteString(line, 38, 4));
				//志望人数
				map.put("wishedNum", getByteString(line, 43, 4));
				//平均偏差値
				map.put("avgDeviation", getByteString(line, 48, 4));
				//度数分布表1〜19
				map.put("freqDistribution1", getByteString(line, 52, 8));
				map.put("freqDistribution2", getByteString(line, 60, 8));
				map.put("freqDistribution3", getByteString(line, 68, 8));
				map.put("freqDistribution4", getByteString(line, 76, 8));
				map.put("freqDistribution5", getByteString(line, 84, 8));
				map.put("freqDistribution6", getByteString(line, 92, 8));
				map.put("freqDistribution7", getByteString(line, 100, 8));
				map.put("freqDistribution8", getByteString(line, 108, 8));
				map.put("freqDistribution9", getByteString(line, 116, 8));
				map.put("freqDistribution10", getByteString(line, 124, 8));
				map.put("freqDistribution11", getByteString(line, 132, 8));
				map.put("freqDistribution12", getByteString(line, 140, 8));
				map.put("freqDistribution13", getByteString(line, 148, 8));
				map.put("freqDistribution14", getByteString(line, 156, 8));
				map.put("freqDistribution15", getByteString(line, 164, 8));
				map.put("freqDistribution16", getByteString(line, 172, 8));
				map.put("freqDistribution17", getByteString(line, 180, 8));
				map.put("freqDistribution18", getByteString(line, 188, 8));
				map.put("freqDistribution19", getByteString(line, 196, 8));
				//現役人数
				map.put("activeNum", getByteString(line, 205, 4));
				//現役平均偏差値
				map.put("activeAvgDeviation", getByteString(line, 210, 4));
				//高卒人数
				map.put("graduateNum", getByteString(line, 215, 4));
				//高卒平均偏差値
				map.put("graduateAvgDeviation", getByteString(line, 220, 4));
				//教科別成績１
				map.put("courseScore1", getByteString(line, 225, 4));
				//教科別成績２
				map.put("courseScore2", getByteString(line, 230, 4));
				//教科別成績３
				map.put("courseScore3", getByteString(line, 235, 4));
				//教科別成績４
				map.put("courseScore4", getByteString(line, 240, 4));
				//教科別成績５
				map.put("courseScore5", getByteString(line, 245, 4));

			}

			lineNum ++;
			pageLineNum ++;			
		}
		
		return list;
	}
	
}

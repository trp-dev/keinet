package jp.co.fj.freemenu.data;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10 	Totec) T.Yamada 	�쐬
 * 
 */
public class MakeStatsData {

	/**
	 * �͎��N�x
	 */
	private String examYear;
	
	/**
	 * �͎��R�[�h
	 */
	private String examCd;
	
	/**
	 * ���v���
	 */
	private int statisticsType;
	
	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @return
	 */
	public int getStatisticsType() {
		return statisticsType;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

	/**
	 * @param i
	 */
	public void setStatisticsType(int i) {
		statisticsType = i;
	}

}

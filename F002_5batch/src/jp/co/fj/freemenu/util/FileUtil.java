package jp.co.fj.freemenu.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10 	Totec) T.Yamada 	作成
 * 
 */
public class FileUtil {
	
	
	/**
	 * ファイル圧縮
	 * @param zippedFilePath 名前を含む圧縮先ファイルのフルパス
	 * @param 圧縮対象のファイル群(PDF)
	 */
	public static void zipFiles(String zippedFilePath, File[] files) {

		File zippedFile = new File(zippedFilePath);
		
		ZipOutputStream os = null;
		InputStream is =  null;
		try {
			
			os = new ZipOutputStream(new FileOutputStream(zippedFile.getPath()));
			byte[] buf = new byte[1024];
			
			// ファイルリスト分処理
			for (int i = 0; i < files.length; i++) {
				
				os.putNextEntry(new ZipEntry(files[i].getName()));
				is = new FileInputStream(files[i].getPath());
				
				int len = 0;
				// 書き込み開始
				while ((len = is.read(buf)) != -1) {
					os.write(buf, 0, len);
				}
				os.closeEntry();
			}
		} catch (FileNotFoundException e3) {
			e3.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(is != null) os.close();
				if (os != null) os.close();
			} catch (IOException e4) {
				e4.printStackTrace();
			}
		}

	}
	
	/**
	 * 再帰的に指定のフォルダ以下をすべて削除する
	 * @param dir
	 * @return
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i=0; i<children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		//全部空になったら、自分を消す
		return dir.delete();
	}
}

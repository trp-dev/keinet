package jp.co.fj.freemenu.util;

import java.io.File;

import com.fujitsu.systemwalker.outputassist.connector.ConnectorException;
import com.fujitsu.systemwalker.outputassist.connector.FormsFile;
import com.fujitsu.systemwalker.outputassist.connector.PrintForm;
import com.fujitsu.systemwalker.outputassist.connector.PrintProperties;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10 	Totec) T.Yamada 	作成
 * 
 */
public class PDFUtil {

	/**
	 * 
	 * @param pdfFilePath PDF作成先pdfファイルのフルパス
	 * @param datFilePath PDF作成元になるdatファイルのフルパス
	 * @param scriptDirPath 定義体のあるディレクトリパス
	 * @param scriptName 定義体名
	 * @param outCharSet 出力文字コード※上記から選択
	 * @throws ConnectorException
	 */
	public static void createPDF(String pdfFilePath, String datFilePath, String scriptDirPath, String scriptName, String delimiter, int outCharSet, String pdfAuther, String pdfTiltle) throws ConnectorException {

		File datFile = new File(datFilePath);

		// 定義体が存在するDirを指定して、FormsFile クラスを作成
		//FormsFile form = new FormsFile(new File(System.getProperty("user.dir")).getParent() + File.separator + "lcdefine" + File.separator);
		FormsFile form = new FormsFile(scriptDirPath);
		// 入力ファイルをUTF-8で指定します(データファイルパス必要)
		form.setDataFile(datFilePath, outCharSet);
		// 定義体ファイルのエンコードを指定します
		form.setFileType(outCharSet);
		// 帳票出力時のファイル名を指定します
		form.setScriptFile(scriptName);
		//区切り文字を指定します
		form.setGrpDelimit(delimiter);

		// PrintProperites オブジェクトを構築します
		PrintProperties prop = new PrintProperties();
		// プロパティ指定(出力方法)をします
		// 出力方法 PDFファイルを選択したとき
		prop.setDirectMethod(PrintProperties.OUTPUTMODE_PDF);
		// プロパティ指定(PDFファイル名)をします
		prop.setKeepPdf(pdfFilePath);
		// ファイル作者を指定します
		prop.setPdfAuthor(pdfAuther);
		// ファイルタイトルを指定します
		prop.setTitle(pdfTiltle);
		// ファイルのコメントを指定します
		//prop.setComment(pdfComment);

		// 帳票を出力
		new PrintForm().PrintOut(form, prop);

		try {
			// 資源を解放
			if (form != null) form.cleanup();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

package jp.co.fj.freemenu.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10 	T.Yamada 	作成
 * 
 */
public class DBUtil {
	
	/**
	 * DBコネクションを開く
	 * @throws Exception
	 */
	public static Connection openConnection(String DBDriver, String DBUrl, String DBUserName, String DBPassWord) throws Exception {
		Connection con = null;
		try{
			Class.forName(DBDriver);
			con = DriverManager.getConnection(
					DBUrl,
					DBUserName,
					DBPassWord
			);
			return con;
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * DBコネクショをン閉じる
	 * @throws Exception
	 */
	public static void closeConnection(Connection con) throws Exception{
		if(con != null){
			con.close();
		}
	}
}

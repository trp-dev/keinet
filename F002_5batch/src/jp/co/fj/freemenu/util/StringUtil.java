package jp.co.fj.freemenu.util;

import java.io.UnsupportedEncodingException;

import jp.co.fj.keinavi.util.JpnStringConv;

/**
 * @author Totec) T.Yamada
 *
 * ※jp.co.fj.freemenu.utilの中にはすでに
 * 別のStringUtil.javaが存在するので、こちらと
 * 結合するのが望ましい。
 *
 * 2005.8.10 	Totec) T.Yamada 	作成
 * 
 */
public class StringUtil {
	
	/**
	 * String.substringに似たもの。
	 * 文字数ではなくバイト数でインデックスを計算して、
	 * 文字列を返す。
	 * このメソッドを使用することで、
	 * 解析書通りのインデックス値でできる。
	 * @param string
	 * @param start
	 * @param num
	 * @return
	 */
	public static String getByteString(String string, int start, int num, String charSet) throws UnsupportedEncodingException{
		
		//定義書はインデックス値でないので、ここでマイナス１してインデックスの値にする
		start --;
		
		byte[] bytes = new byte[num];
		System.arraycopy(string.getBytes(charSet), start, bytes, 0, num);
		return new String(bytes, charSet);
	}
	
	/**
	 * 全角を半角に変換する
	 * @param string
	 * @return
	 */
	public static String zen2Han(String string){
		return JpnStringConv.kkanaZen2Han(JpnStringConv.latinZen2Han(string));
	}
	
}

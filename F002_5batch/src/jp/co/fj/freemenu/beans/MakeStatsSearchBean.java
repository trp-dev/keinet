package jp.co.fj.freemenu.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.fj.freemenu.data.MakeStatsData;

import com.fjh.beans.DefaultSearchBean;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10 	Totec) T.Yamada 	作成
 * 
 */
public class MakeStatsSearchBean extends DefaultSearchBean{
	
	private int statisticsType = -1;
	
	private String examYear;
	
	private String examCd;
	
	private String query = "SELECT EXAMYEAR, EXAMCD, STATISTICSTYPE FROM MAKESTATISTICS WHERE 1 = 1";

	/* (非 Javadoc)
	 * @see com.fjh.beans.DefaultSearchBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(statisticsType != -1){
			query += " AND STATISTICSTYPE = "+statisticsType;
		}
		if(examYear != null){
			query += " AND EXAMYEAR = '"+examYear+"'";
		}
		if(examCd != null){
			query += " AND EXAMCD = '"+examCd+"'";
		}
		
		//デフォルトは模試の古い順に取得する
		query += " ORDER BY EXAMYEAR ASC, EXAMCD ASC";
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()){
				MakeStatsData data = new MakeStatsData();
				data.setExamYear(rs.getString("EXAMYEAR"));
				data.setExamCd(rs.getString("EXAMCD"));
				data.setStatisticsType(rs.getInt("STATISTICSTYPE"));
				recordSet.add(data);
			}
		}catch(SQLException sqle){
			throw sqle;
		}catch(Exception e){
			throw e;
		} finally {
			if (rs != null) rs.close();
			if (ps != null) ps.close();
		}

	}

	/**
	 * @return
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @return
	 */
	public int getStatisticsType() {
		return statisticsType;
	}

	/**
	 * @param string
	 */
	public void setQuery(String string) {
		query = string;
	}

	/**
	 * @param i
	 */
	public void setStatisticsType(int i) {
		statisticsType = i;
	}

}

package jp.co.fj.freemenu.beans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.fjh.beans.DefaultBean;

/**
 * @author Totec) T.Yamada
 *
 * 2005.8.10  	Totec) T.Yamada 	�쐬
 * 
 */
public class MakeStatsDeleteBean extends DefaultBean{

	private int statisticsType = -1;
	
	private String examYear;
	
	private String examCd;
	
	private String query = "DELETE FROM MAKESTATISTICS WHERE 1 = 1";

	/* (�� Javadoc)
	 * @see com.fjh.beans.DefaultBean#execute()
	 */
	public void execute() throws SQLException, Exception {
		if(statisticsType != -1){
			query += " AND STATISTICSTYPE = "+statisticsType;
		}
		if(examYear != null){
			query += " AND EXAMYEAR = '"+examYear+"'";
		}
		if(examCd != null){
			query += " AND EXAMCD = '"+examCd+"'";
		}		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(query);
			
			// �폜����
			if (ps.executeUpdate() > 0) {
		
			}
		}catch(SQLException sqle){
			throw sqle;
		}catch(Exception e){
			throw e;
		} finally {
			if (rs != null) rs.close();
			if (ps != null) ps.close();
		}

	}
	
	/**
	 * @return
	 */
	public int getStatisticsType() {
		return statisticsType;
	}

	/**
	 * @param i
	 */
	public void setStatisticsType(int i) {
		statisticsType = i;
	}

	/**
	 * @return
	 */
	public String getExamCd() {
		return examCd;
	}

	/**
	 * @return
	 */
	public String getExamYear() {
		return examYear;
	}

	/**
	 * @param string
	 */
	public void setExamCd(String string) {
		examCd = string;
	}

	/**
	 * @param string
	 */
	public void setExamYear(String string) {
		examYear = string;
	}

}

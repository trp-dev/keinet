package jp.fujitsu.keinet.mst.db.factory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.beans.CefrBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrConvScoreBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuAppInfo;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.JpnKijutuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.Rank;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.data.UnivStemma;
import jp.co.fj.kawaijuku.judgement.factory.ParentFactory;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoBasicBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoCenterBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoCommonBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoContainer;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoSecondBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterBasicPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterChoiceSchoolPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterContainer;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGpCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGroup;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JpnStringUtil;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;
import jp.fujitsu.keinet.mst.db.model.ScheduleDetailData;
import jp.fujitsu.keinet.mst.db.service.ExamService;
import jp.fujitsu.keinet.mst.db.util.MyStaticLoader;
import jp.fujitsu.keinet.mst.db.util.Query;

import org.apache.commons.dbutils.DbUtils;

/**
 *
 * DB用大学マスタ展開処理
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivFactoryDB extends ParentFactory {

    /** コネクション */
    private final Connection con;

    /** 模試年度 */
    private final String examYear;

    /** 模試区分 */
    private final String examDiv;

    /**
     * コンストラクタです。
     *
     * @param con DBコネクション
     * @param restriction 制限する大学リスト
     * @param examYear 模試年度
     * @param examDiv 模試区分
     */
    private UnivFactoryDB(Connection con, List restriction, String examYear, String examDiv) throws Exception {
        this.con = con;
        this.examYear = examYear;
        this.examDiv = examDiv;
        for (Iterator ite = restriction.iterator(); ite.hasNext();) {
            restrict((String) ite.next());
        }
    }

    /**
     * コンストラクタです。
     *
     * @param con DBコネクション
     * @param examYear 模試年度
     * @param examDiv 模試区分
     */
    private UnivFactoryDB(Connection con, ExamService neds) throws Exception {
        this(con, loadRestriction(), neds.getExamYear(), neds.getExamDiv());
    }

    /**
     * コンストラクタです。
     *
     * @param con DBコネクション
     */
    public UnivFactoryDB(Connection con) throws Exception {
        this(con, createExamService(con));
    }

    /**
     * 制限設定を読み込みます。
     *
     * @return 制限する大学リスト
     * @throws IOException IO例外
     */
    private static List loadRestriction() throws IOException {

        /* 設定ファイルを読み込む */
        Properties prop = new Properties();
        InputStream in = null;
        try {
            in = UnivFactoryDB.class.getResourceAsStream("UnivFactoryDB.properties");
            if (in != null) {
                prop.load(in);
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }

        /* 制限する大学を追加する */
        List restriction = new ArrayList();
        String restrict = prop.getProperty("restrict");
        if (restrict != null && restrict.length() > 0) {
            String[] array = restrict.split(",");
            for (int i = 0; i < array.length; i++) {
                restriction.add(array[i]);
            }
        }

        return restriction;
    }

    /**
     * 模試サービスを生成します。
     *
     * @param con DBコネクション
     * @return {@link ExamService}
     */
    private static ExamService createExamService(Connection con) throws Exception {
        ExamService neds = new ExamService();
        neds.setConnection(null, con);
        neds.execute();
        return neds;
    }

    /**
     * クエリを返す。
     *
     * @param name クエリ名
     * @return {@link Query}
     */
    private Query getQuery(String name) throws Exception {
        return MyStaticLoader.load(name);
    }

    @Override
    protected UnivMasterContainer createUnivMasterContainer() {
        return new UnivMasterDBContainer();
    }

    @Override
    protected void prepareUnivExam(UnivMasterContainer container) throws Exception {
        deployExamscheduledetail((UnivMasterDBContainer) container);
    }

    @Override
    protected void deployRank(UnivMasterContainer container) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        try {
            query = getQuery("load_rank");
            ps = con.prepareStatement(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                String rankCode = rs.getString(1); //RANKCD
                container.getRankMaster().put(rankCode, new Rank( //
                        rs.getString(2), //RANKNAME
                        rs.getDouble(3), //RANKHLIMITS
                        rs.getDouble(4))); //RANKLLIMITS
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterBasic(UnivMasterContainer container) throws Exception {
        deployUnivMasterBasicCommon(container, getQuery("load_univmaster_basic_plus"));
    }

    /**
     * 大学マスタ基本情報（UNIVMASTER_BASIC）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterBasicCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("hn", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                UnivMasterBasicPlus data = new UnivMasterBasicPlus( //
                        rs.getString(1), //UNIVCD
                        rs.getString(2), //FACULTYCD
                        rs.getString(3), //DEPTCD
                        rs.getString(4), //UNIDIV
                        rs.getString(5), //UNINAME_ABBR
                        rs.getString(6), //FACULTYNAME_ABBR
                        rs.getString(7), //DEPTNAME_ABBR
                        rs.getString(8), //UNIVNAME_KANA
                        rs.getString(9), //DEPTNAME_KANA
                        rs.getString(10), //UNIGDIV
                        rs.getString(11), //PREFCD_EXAMHO
                        rs.getString(12), //PREFCD_EXAMSH
                        rs.getInt(13), //BORDERSCORE1
                        rs.getInt(14), //BORDERSCORE2
                        rs.getInt(15), //BORDERSCORE3
                        rs.getDouble(16), //BORDERSCORERATE2
                        rs.getString(17), //BUNRICD
                        rs.getInt(18), //PUBENTEXAMCAPA
                        rs.getInt(19), //UNICOEDDIV
                        rs.getInt(20), //UNINIGHTDIV
                        rs.getString(21), //ENTEXAMRANK
                        rs.getString(22), //DEPTSORTKEY
                        rs.getInt(23), //PUBREJECTEXSCORE
                        0, //足切予想得点はインターネットバンザイのみ必要
                        rs.getInt(24), //DOCKINGNEEDLDIV
                        rs.getString(25), //PREFNAME
                        rs.getString(26), //PREFNAME_SH
                        rs.getString(27), //DISTRICTCD
                        rs.getString(28), //REMARKS
                        rs.getString(29), //CHOICECD5
                        rs.getString(30), //UNIVKANJINAME
                        JpnStringUtil.hanKana2ZenKana(rs.getString(31)), //UNIVKANANAME
                        rs.getString(32), //UNIVOTHERNAME
                        rs.getString(33), //KANA_NUM
                        rs.getString(34), //FACULTYCONCD
                        rs.getString(35), //DEPTSERIAL_NO
                        rs.getString(36), //SCHEDULESYS
                        rs.getString(37), //SCHEDULESYSBRANCHCD
                        rs.getString(38), //EXAMCAPAREL
                        rs.getString(39), //BORDERINDIV
                        rs.getInt(40), //BORDERSCORENOEXAM1
                        rs.getInt(41), //BORDERSCORENOEXAM2
                        rs.getInt(42), //BORDERSCORENOEXAM3
                        rs.getDouble(43), //BORDERSCORERATENOEXAM1
                        rs.getDouble(44), //BORDERSCORERATENOEXAM2
                        rs.getDouble(45), //BORDERSCORERATENOEXAM3
                        rs.getDouble(46), //ENTEXAMFULLPNT_CENTER
                        rs.getDouble(47), //TOTALALLOTPNT_CENTER
                        rs.getDouble(48), //ENTEXAMFULLPNT_CENTER_NOEXAM
                        rs.getDouble(49), //TOTALALLOTPNT_CENTER_NOEXAM
                        rs.getString(50), //SETTINGPATTERN
                        rs.getString(51), //APP_REQ_PATTERN_CD
                        rs.getString(52), //ENGPT_BORDERUSEFLG
                        rs.getString(53));//ENGPT_REPBRANCH
                container.getUnivMasterBasic().add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterChoiceSchool(UnivMasterContainer container) throws Exception {
        deployUnivMasterChoiceSchoolCommon(container, getQuery("load_univmaster_choiceschool_plus"));
    }

    @Override
    protected void deployUnivMasterChoiceSchoolPub(UnivMasterContainer container) throws Exception {
        deployUnivMasterChoiceSchoolCommon(container, getQuery("load_pub_univmaster_choiceschool_plus"));
    }

    /**
     * 大学マスタ模試用志望校（UNIVMASTER_CHOICESCHOOL）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterChoiceSchoolCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("s", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD
                String branchCd = rs.getString(2); //UNICDBRANCHCD
                String entExamDiv = rs.getString(3); //ENTEXAMDIV

                UnivMasterChoiceSchoolPlus data = new UnivMasterChoiceSchoolPlus( //
                        rs.getInt(4), //TOTALALLOTPNT
                        rs.getInt(5), //TOTALALLOTPNT_NREP
                        rs.getInt(6), //ENTEXAMFULLPNT
                        rs.getString(7), //ENTEXAMFULLPNT_NREP
                        rs.getDouble(8), //IRRALALLOTPNT1
                        rs.getInt(9), //MAXSELECT_EN1
                        rs.getInt(10), //MAXSELECT_MA1
                        rs.getInt(11), //MAXSELECT_JA1
                        rs.getInt(12), //MAXSELECT_SC1
                        rs.getInt(13), //MAXSELECT_SO1
                        rs.getInt(14), //IRR_SUB1
                        rs.getDouble(15), //IRRALALLOTPNT2
                        rs.getInt(16), //MAXSELECT_EN2
                        rs.getInt(17), //MAXSELECT_MA2
                        rs.getInt(18), //MAXSELECT_JA2
                        rs.getInt(19), //MAXSELECT_SC2
                        rs.getInt(20), //MAXSELECT_SO2
                        rs.getInt(21), //IRR_SUB2
                        rs.getDouble(22), //IRRALALLOTPNT3
                        rs.getInt(23), //MAXSELECT_EN3
                        rs.getInt(24), //MAXSELECT_MA3
                        rs.getInt(25), //MAXSELECT_JA3
                        rs.getInt(26), //MAXSELECT_SC3
                        rs.getInt(27), //MAXSELECT_SO3
                        rs.getInt(28), //IRR_SUB3
                        rs.getDouble(29), //IRRALALLOTPNT4
                        rs.getInt(30), //MAXSELECT_EN4
                        rs.getInt(31), //MAXSELECT_MA4
                        rs.getInt(32), //MAXSELECT_JA4
                        rs.getInt(33), //MAXSELECT_SC4
                        rs.getInt(34), //MAXSELECT_SO4
                        rs.getInt(35), //IRR_SUB4
                        rs.getDouble(36), //IRRALALLOTPNT5
                        rs.getInt(37), //MAXSELECT_EN5
                        rs.getInt(38), //MAXSELECT_MA5
                        rs.getInt(39), //MAXSELECT_JA5
                        rs.getInt(40), //MAXSELECT_SC5
                        rs.getInt(41), //MAXSELECT_SO5
                        rs.getInt(42), //IRR_SUB5
                        rs.getDouble(43), //IRRALALLOTPNT6
                        rs.getInt(44), //MAXSELECT_EN6
                        rs.getInt(45), //MAXSELECT_MA6
                        rs.getInt(46), //MAXSELECT_JA6
                        rs.getInt(47), //MAXSELECT_SC6
                        rs.getInt(48), //MAXSELECT_SO6
                        rs.getInt(49), //IRR_SUB6
                        rs.getString(50), //SUBCOUNT
                        rs.getString(51), //STRING
                        rs.getInt(52), //FIRSTANS_SC_FLG
                        rs.getInt(53), //FIRSTANS_SO_FLG
                        rs.getInt(54), //PRIORITYBRANCHCD
                        rs.getString(55), //SAMECHOICE_NG_SC
                        rs.getString(56), //CENTERCVSSCORE_ENGPT
                        rs.getString(57), //ADDPTDIV_ENGPT
                        rs.getInt(58), //ADDPT_ENGPT
                        rs.getString(59), //CENTERCVSSCORE_KOKUGO_DESC
                        rs.getString(60), //ADDPTDIV_KOKUGO_DESC
                        rs.getInt(61), //ADDPT_KOKUGO_DESC
                        rs.getString(62));//BESTBRAN_SORT_CVSSCORE_USEDIV

                container.addUnivMasterChoiceSchool(data, key, branchCd, entExamDiv);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterCourse(UnivMasterContainer container) throws Exception {
        deployUnivMasterCourseCommon(container, getQuery("load_univmaster_course"));
    }

    @Override
    protected void deployUnivMasterCoursePub(UnivMasterContainer container) throws Exception {
        deployUnivMasterCourseCommon(container, getQuery("load_pub_univmaster_course"));
    }

    /**
     * 大学マスタ模試用教科（UNIVMASTER_COURSE）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterCourseCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD
                String branchCd = rs.getString(2); //UNICDBRANCHCD
                String entExamDiv = rs.getString(3); //ENTEXAMDIV
                String courseCd = rs.getString(4); // COURSECD

                UnivMasterCourse data = new UnivMasterCourse( //
                        rs.getDouble(5), //NECESALLOTPNT
                        rs.getInt(6), //NECESSUBCOUNT
                        rs.getInt(7)); //CEN_SECSUBCHOICEDIV

                container.addUnivMasterCource(data, key, branchCd, entExamDiv, courseCd);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterSelectGpCourse(UnivMasterContainer container) throws Exception {
        deployUnivMasterSelectGpCourseCommon(container, getQuery("load_univmaster_selectgpcourse"));
    }

    @Override
    protected void deployUnivMasterSelectGpCoursePub(UnivMasterContainer container) throws Exception {
        deployUnivMasterSelectGpCourseCommon(container, getQuery("load_pub_univmaster_selectgpcourse"));
    }

    /**
     * 大学マスタ模試用選択グループ教科（UNIVMASTER_SELECTGPCOURSE）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterSelectGpCourseCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD
                String branchCd = rs.getString(2); //UNICDBRANCHCD
                String entExamDiv = rs.getString(3); // ENTEXAMDIV
                String selectGNo = rs.getString(4); //SELECTG_NO
                String courseCd = rs.getString(5); //COURSECD

                UnivMasterSelectGpCourse data = new UnivMasterSelectGpCourse(rs.getInt(6)); //SGPERSUBSUBCOUNT

                container.addUnivMasterSelectGpCourse(data, key, branchCd, entExamDiv, selectGNo, courseCd);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterSelectGroup(UnivMasterContainer container) throws Exception {
        deployUnivMasterSelectGroupCommon(container, getQuery("load_univmaster_selectgroup"));
    }

    @Override
    protected void deployUnivMasterSelectGroupPub(UnivMasterContainer container) throws Exception {
        deployUnivMasterSelectGroupCommon(container, getQuery("load_pub_univmaster_selectgroup"));
    }

    /**
     * 大学マスタ模試用選択グループ（UNIVMASTER_SELECTGROUP）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterSelectGroupCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD
                String branchCd = rs.getString(2); //UNICDBRANCHCD
                String entExamDiv = rs.getString(3); //ENTEXAMDIV
                String selectGNo = rs.getString(4); //SELECTG_NO

                UnivMasterSelectGroup data = new UnivMasterSelectGroup( //
                        rs.getInt(5), //SELECTGSUBCOUNT
                        rs.getDouble(6)); //SELECTGALLOTPNT

                container.addUnivMasterSelectGroup(data, key, branchCd, entExamDiv, selectGNo);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void deployUnivMasterSubject(UnivMasterContainer container) throws Exception {
        deployUnivMasterSubjectCommon(container, getQuery("load_univmaster_subject"));
    }

    @Override
    protected void deployUnivMasterSubjectPub(UnivMasterContainer container) throws Exception {
        deployUnivMasterSubjectCommon(container, getQuery("load_pub_univmaster_subject"));
    }

    /**
     * 大学マスタ模試用科目（UNIVMASTER_SUBJECT）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     * @param query {@link Query}
     */
    private void deployUnivMasterSubjectCommon(UnivMasterContainer container, Query query) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            query.append(createOption("", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(2000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD
                String branchCd = rs.getString(2); //UNICDBRANCHCD
                String entExamDiv = rs.getString(3); //ENTEXAMDIV
                String examSubCd = rs.getString(4); //EXAMSUBCD

                UnivMasterSubject data = new UnivMasterSubject( //
                        rs.getString(5), //SUBBIT
                        rs.getDouble(6), //SUBPNT
                        rs.getString(7), //SUBAREA
                        rs.getString(8)); //CONCURRENT_NG

                container.addUnivMasterSubject(data, key, branchCd, entExamDiv, examSubCd);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected UnivData createUniv(UnivMasterContainer container, UnivMasterBasicPlus basic) {

        UnivMasterDBContainer dbContainer = (UnivMasterDBContainer) container;

        UnivData univ = new UnivData(basic.getUnivCd(), basic.getFacultyCd(), basic.getDeptCd(), basic.getUniDiv(), basic.getUniGDiv(), basic.getBorderScoreNoExam1(), basic.getBorderScoreNoExam2(), basic.getBorderScoreNoExam3(),
                basic.getBorderScoreRateNoExam2(), basic.getEntExamRank(), basic.getDockingNeedLDiv(), basic.getPubRejectExScore(), basic.getChoiceCD5(), basic.getRemarks(), basic.getDeptSortKey(), basic.getPrefNameExamHo(),
                basic.getPrefCdExamHo(), basic.getBunriCd(), basic.getUniNameAbbr(), basic.getFacultyNameAbbr(), basic.getDeptNameAbbr(), basic.getUnivNameKana(), basic.getDeptNameKana(), basic.getPubEntExamCapa(), basic.getUniCoedDiv(),
                basic.getUniNightDiv(), basic.getPrefCdExamSh(), basic.getPrefNameExamSh(), basic.getDistrictCd(), basic.getUnivKanjiName(), basic.getUnivKanaName(), basic.getUnivOtherName(), basic.getKanaNum(), basic.getFacultyConCd(),
                basic.getDeptSerialNo(), basic.getScheduleSys(), basic.getScheduleSysBranchCd(), basic.getExamCapaRel(), Integer.parseInt(StringUtil.nvl(basic.getBorderInDiv(), "1")), basic.getBorderScore2(), basic.getBorderScore1(),
                basic.getBorderScore3(), basic.getAppReqPatternCd(), Integer.parseInt(StringUtil.nvl(basic.getEngPtBorderUseFlg(), "0")), basic.getEngPtRepBranch());

        List scheduleList = (List) dbContainer.getScheduleMap().get(univ.getUniqueKey());
        if (scheduleList != null) {
            univ.getSchedules().addAll(scheduleList);
        }

        /* 参加試験判定フラグを設定 */
        univ.setSSHanteiFlg(0);

        return univ;
    }

    /**
     * ロード対象大学を絞り込むSQL条件文を生成します。
     *
     * @param prefix 列名の接頭辞
     * @return ロード対象大学を絞り込むSQL条件文
     */
    private static String createOption(String prefix, Set restriction) {

        if (prefix.length() > 0) {
            prefix = prefix + ".";
        }

        StringBuffer query = new StringBuffer();

        if (restriction.size() > 0) {

            query.append(" AND ( ");

            for (Iterator ite = restriction.iterator(); ite.hasNext();) {

                String key = (String) ite.next();

                if (key.length() == 4) {
                    query.append(" " + prefix + "univcd = " + key + " ");
                } else if (key.length() == 10) {
                    query.append(" ( " + prefix + "univcd = '" + key.substring(0, 4) + "' and " + prefix + "facultycd = '" + key.substring(4, 6) + "' and " + prefix + "deptcd = '" + key.substring(6) + "' )");
                } else {
                    throw new InternalError("指定の大学コードが不正です。");
                }

                if (ite.hasNext()) {
                    query.append(" OR ");
                }
            }

            query.append(" ) ");

        }

        return query.toString();
    }

    /**
     * 入試日程（EXAMSCHEDULEDETAIL）をメモリに展開します。
     *
     * @param container {@link UnivMasterDBContainer}
     */
    protected void deployExamscheduledetail(UnivMasterDBContainer container) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        try {
            query = getQuery("load_examscheduledetail");
            query.append(createOption("", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                String key = rs.getString(1); //UNIVCD+FACULTYCD+DEPTCD

                ScheduleDetailData data = new ScheduleDetailData( //
                        rs.getInt(2), //ENTEXAMDIV_1_2ORDER
                        rs.getInt(3), //SCHOOLPROVENTDIV
                        rs.getInt(4), //ENTEXAMDIV_1_2TERM
                        rs.getString(5), //SCHEDULEBRANCHNAME
                        toDate(rs.getString(6)), //ENTEXAMINPLEDATE1_1
                        toDate(rs.getString(7)), //ENTEXAMINPLEDATE1_2
                        toDate(rs.getString(8)), //ENTEXAMINPLEDATE1_3
                        toDate(rs.getString(9)), //ENTEXAMINPLEDATE1_4
                        rs.getString(10), //PLEDATEDIV1_1
                        rs.getString(11), //PLEDATEDIV1_2
                        rs.getString(12), //PLEDATEDIV1_3
                        toDate(rs.getString(13)), //ENTEXAMINPLEDATE2_1
                        toDate(rs.getString(14)), //ENTEXAMINPLEDATE2_2
                        toDate(rs.getString(15)), //ENTEXAMINPLEDATE2_3
                        toDate(rs.getString(16)), //ENTEXAMINPLEDATE2_4
                        rs.getString(17), //PLEDATEDIV2_1
                        rs.getString(18), //PLEDATEDIV2_2
                        rs.getString(19), //PLEDATEDIV2_3
                        rs.getString(20), //PLEDATEUNITEDIV
                        toDate(rs.getString(21)), //ENTEXAMAPPLIDEADLINE
                        rs.getString(22), //ENTEXAMAPPLIDEADLINEDIV
                        toDate(rs.getString(23)), //SUCANNDATE
                        toDate(rs.getString(24))); //PROCEDEADLINE

                List schedules = (List) container.getScheduleMap().get(key);
                if (schedules == null) {
                    schedules = new ArrayList();
                    container.getScheduleMap().put(key, schedules);
                }

                schedules.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * YYYYMMDD文字列をDateに変換します。
     *
     * @param yyyymmdd
     * @return {@link Date}
     */
    private Date toDate(String yyyymmdd) throws ParseException {
        return yyyymmdd == null ? null : ScheduleDetailData.formatter.parse(yyyymmdd);
    }

    @Override
    protected void deployStemma(UnivMasterContainer container) throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        try {
            query = getQuery("load_stemma");
            query.append(createOption("u", container.getRestriction()));

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);

            rs = ps.executeQuery();
            while (rs.next()) {

                final String univKey = rs.getString(1);

                List stemmas = (List) container.getStemmaMap().get(univKey);
                if (stemmas == null) {
                    stemmas = new ArrayList();
                    container.getStemmaMap().put(univKey, stemmas);
                }

                stemmas.add(new UnivStemma(rs.getString(2), rs.getString(3)));
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * 公表用データを返します。
     *
     * @param con DBコネクション
     * @param examYear 模試年度
     * @param examDiv 模試区分
     * @param univcd 大学コード
     * @param facultycd 学部コード
     * @param deptcd 学科コード
     * @return 公表用データ
     */
    public static Map searchOpenDatas(Connection con, String examYear, String examDiv, String univcd, String facultycd, String deptcd) throws Exception {
        List restriction = Collections.singletonList(univcd + facultycd + deptcd);
        UnivFactoryDB factory = new UnivFactoryDB(con, restriction, examYear, examDiv);
        return (Map) factory.getLoadedPubData().values().iterator().next();
    }

    @Override
    public String getExamDiv() {
        return examDiv;
    }

    @Override
    protected void loadUnivInfoBasic(UnivInfoContainer container) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(getQuery("load_univinfo_basic").toString());
            ps.setFetchSize(1000);
            ps.setInt(1, Integer.parseInt(examYear));
            ps.setString(2, examDiv);
            rs = ps.executeQuery();
            while (rs.next()) {
                container.getUnivInfoBasicList().add(new UnivInfoBasicBean(rs.getString("COMBIKEY"), rs.getString("UNIV10CD"), rs.getString("BRANCHNAME")));
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void loadUnivInfoCenter(UnivInfoContainer container) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(getQuery("load_univinfo_center").toString());
            ps.setFetchSize(1000);
            rs = ps.executeQuery();
            while (rs.next()) {
                UnivInfoCenterBean bean = new UnivInfoCenterBean();

                /* 共通情報を設定する */
                setCommonValue(rs, bean, "認");

                /* センタ固有情報を設定する */
                bean.setItem7(rs.getString("ITEM7"));
                bean.setRika1ansFlg(rs.getString("RIKA1ANSFLG"));
                bean.setSociety1ansFlg(rs.getString("SOCIETY1ANSFLG"));

                /* 国語記述配点を設定する */
                bean.addJpnDesc(rs.getString("JPNDESCRANGE"), rs.getString("JPNDESCTRUSTPT"), rs.getString("JPNDESCPT"), rs.getString("JPNDESCADDPT"), rs.getString("JPNDESCADDPTTYPE"), rs.getString("JPNDESCSEL"));

                container.getUnivInfoCenterMap().put(rs.getString("COMBIKEY"), bean);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    @Override
    protected void loadUnivInfoSecond(UnivInfoContainer container) throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(getQuery("load_univinfo_second").toString());
            ps.setFetchSize(1000);
            rs = ps.executeQuery();
            while (rs.next()) {
                UnivInfoSecondBean bean = new UnivInfoSecondBean();

                /* 共通情報を設定する */
                setCommonValue(rs, bean, "英資");

                /* 二次固有情報を設定する */
                bean.addEssay(rs.getString("ESSAY"), rs.getString("ESSAYTRUSTPT"), rs.getString("ESSAYPT"), rs.getString("ESSAYHISSU"));

                bean.addTotal(rs.getString("TOTAL"), rs.getString("TOTALTRUSTPT"), rs.getString("TOTALPT"), rs.getString("TOTALHISSU"));

                bean.addInterview(rs.getString("INTERVIEW"), rs.getString("INTERVIEWTRUSTPT"), rs.getString("INTERVIEWPT"), rs.getString("INTERVIEWHISSU"));

                bean.addPlactical(rs.getString("PLACTICAL"), rs.getString("PLACTICALTRUSTPT"), rs.getString("PLACTICALPT"), rs.getString("PLACTICALHISSU"));

                bean.addBoki(rs.getString("BOKI"), rs.getString("BOKITRUSTPT"), rs.getString("BOKIPT"), rs.getString("BOKIHISSU"));

                bean.addInfo(rs.getString("INFO"), rs.getString("INFOTRUSTPT"), rs.getString("INFOPT"), rs.getString("INFOHISSU"));

                bean.addResearch(rs.getString("RESEARCH"), rs.getString("RESEARCHTRUSTPT"), rs.getString("RESEARCHPT"), rs.getString("RESEARCHHISSU"));

                bean.addHoka(rs.getString("HOKA"), rs.getString("HOKATRUSTPT"), rs.getString("HOKAPT"), rs.getString("HOKAHISSU"));

                bean.setHoka2TrustPt(rs.getString("HOKA2TRUSTPT"));

                bean.setHoka2Pt(rs.getString("HOKA2PT"));

                container.getUnivInfoSecondMap().put(rs.getString("COMBIKEY"), bean);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * 情報誌用科目2と3の共通情報を設定します。
     *
     * @param rs 結果セット
     * @param bean {@link UnivInfoCommonBean}
     * @throws SQLException SQL例外
     */
    private void setCommonValue(ResultSet rs, UnivInfoCommonBean bean, String label) throws SQLException {

        bean.setScTrust(rs.getString("SCTRUST"));

        bean.setTotalSc(rs.getString("TOTALSC"));

        bean.addGai(rs.getString("GAITRUSTPT"), rs.getString("GAISHOPT"), rs.getString("GAISHOSEL"), rs.getString("GAIDAIPT"), rs.getString("GAIDAISEL"), COURSE_ALLOT.ENG);

        bean.addMath(rs.getString("MATHTRUSTPT"), rs.getString("MATHSHOPT"), rs.getString("MATHSHOSEL"), rs.getString("MATHDAIPT"), rs.getString("MATHDAISEL"), COURSE_ALLOT.MAT);

        bean.addKokugo(rs.getString("KOKUGOTRUSTPT"), rs.getString("KOKUGOSHOPT"), rs.getString("KOKUGOSHOSEL"), rs.getString("KOKUGODAIPT"), rs.getString("KOKUGODAISEL"), COURSE_ALLOT.JAP);

        bean.addRika(rs.getString("RIKATRUSTPT"), rs.getString("RIKASHOPT"), rs.getString("RIKASHOSEL"), rs.getString("RIKADAIPT"), rs.getString("RIKADAISEL"), COURSE_ALLOT.SCI);

        bean.addSociety(rs.getString("SOCIETYTRUSTPT"), rs.getString("SOCIETYSHOPT"), rs.getString("SOCIETYSHOSEL"), rs.getString("SOCIETYDAIPT"), rs.getString("SOCIETYDAISEL"), COURSE_ALLOT.SOC);

        /* 英語認定試験配点を設定する */
        bean.addEngCert(label, rs.getString("ENGCERTAPP"), rs.getString("ENGCERTTRUSTPT"), rs.getString("ENGCERTPT"), rs.getString("ENGCERTADDPT"), rs.getString("ENGCERTADDPTTYPE"), rs.getString("ENGCERTSEL"));

        bean.setSubName(rs.getString("SUBNAME"));
    }

    @Override
    protected void deployUnivComment(UnivMasterContainer container) throws Exception {
        /* Kei-Navi・インターネット模試判定で注釈文マスタは利用しないのでロードしない */
    }

    @Override
    protected List<CefrBean> deployCefrInfo() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<CefrBean> items = new ArrayList();
        try {
            query = getQuery("load_cefr_info");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                CefrBean data = new CefrBean(rs.getString("CEFRCD"), rs.getString("CEFRNAME"), rs.getString("CEFRABBR"), rs.getInt("DISPSEQUENCE"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<EngPtBean> deployEngCert() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<EngPtBean> items = new ArrayList();
        try {
            query = getQuery("load_engpt");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                EngPtBean data = new EngPtBean();
                data.setEngPtCd(rs.getString("ENGPTCD"));
                data.setEngPtName(rs.getString("ENGPTNAME"));
                data.setEngPtAbbr(rs.getString("ENGPTABBR"));
                data.setEngPtShort(rs.getString("ENGPTSHORT"));
                data.setScoreDecFlg(rs.getString("SCOREDECFLG"));
                data.setScoreTick(rs.getDouble("SCORETICK"));
                data.setRequirementStdUseFlg(rs.getString("REQUIREMENTSTDUSEFLG"));
                data.setRequirementDtlUseFlg(rs.getString("REQUIREMENTDTLUSEFLG"));
                data.setResultFlg(rs.getString("RESULTFLG"));
                data.setScoreConvStdUseFlg(rs.getString("SCORECONVSTDUSEFLG"));
                data.setScoreConvInvUseFlg(rs.getString("SCORECONVINVUSEFLG"));
                data.setScoreConvDtlUseFlg(rs.getString("SCORECONVDTLUSEFLG"));
                data.setLevelFlg(rs.getString("LEVELFLG"));
                data.setDetailDeployFlg(rs.getString("DETAILDEPLOYFLG"));
                data.setDispSequence(rs.getInt("DISPSEQUENCE"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<EngPtDetailBean> deployEngCertDetail() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<EngPtDetailBean> items = new ArrayList();
        try {
            query = getQuery("load_engpt_detail");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                EngPtDetailBean data = new EngPtDetailBean();
                data.setEngPtCd(rs.getString("ENGPTCD"));
                data.setEngPtLevelCd(rs.getString("ENGPTLEVELCD"));
                data.setEngPtLevelName(rs.getString("ENGPTLEVELNAME"));
                data.setEngPtLevelAbbr(rs.getString("ENGPTLEVELABBR"));
                data.setEngPtLevelShort(rs.getString("ENGPTLEVELSHORT"));
                data.setCefrJudgeScoreUpper(rs.getDouble("CEFRJUDGESCOREUPPER"));
                data.setCefrJudgeScoreUnder(rs.getDouble("CEFRJUDGESCOREUNDER"));
                data.setPassScore(rs.getDouble("PASSSCORE"));
                data.setEngPtLevelScoreUpper(rs.getDouble("ENGPTLEVELSCOREUPPER"));
                data.setEngPtLevelScoreUnder(rs.getDouble("ENGPTLEVELSCOREUNDER"));
                data.setCefrLevelUpper(rs.getString("CEFRLEVELUPPER"));
                data.setCefrLevelUnder(rs.getString("CEFRLEVELUNDER"));
                data.setDispSequence(rs.getInt("DISPSEQUENCE"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<CefrConvScoreBean> deployCefrConvScore() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<CefrConvScoreBean> items = new ArrayList();
        try {
            query = getQuery("load_cefr_conv_score");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                CefrConvScoreBean data = new CefrConvScoreBean();
                data.setEngPtCd(rs.getString("ENGPTCD"));
                data.setCefrCd(rs.getString("CEFRCD"));
                data.setCefrConvScore(rs.getDouble("CEFRCONVSCORE"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<JpnDescTotalEvaluationBean> deployJpnDescTotalEval() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<JpnDescTotalEvaluationBean> items = new ArrayList();
        try {
            query = getQuery("load_jpn_desc_total_eval");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                JpnDescTotalEvaluationBean data = new JpnDescTotalEvaluationBean();
                data.setSubCd(rs.getString("SUBCD"));
                data.setQ1Evaluation(rs.getString("Q1EVALUATION"));
                data.setQ2Evaluation(rs.getString("Q2EVALUATION"));
                data.setQ3Evaluation(rs.getString("Q3EVALUATION"));
                data.setTotalEvaluation(rs.getString("TOTALEVALUATION"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<EngSikakuAppInfo> deployEngPtAppReqList() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<EngSikakuAppInfo> items = new ArrayList();
        try {
            query = getQuery("load_engpt_appreq");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                EngSikakuAppInfo data = new EngSikakuAppInfo();
                data.setDaigakuTandokuCode(rs.getString("DAIGAKUTANDOKUCODE"));
                data.setSyPatternCode(rs.getString("SYPATTERNCODE"));
                data.setSyRiyoKubun(rs.getString("SYRIYOKUBUN"));
                data.setSankaShikakuCode(rs.getString("SANKASHIKAKUCODE"));
                data.setScore(rs.getDouble("SCORE"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<EngSikakuScoreInfo> deployEngPtScoreConvList() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Query query = null;
        List<EngSikakuScoreInfo> items = new ArrayList();
        try {
            query = getQuery("load_engpt_score_conv");

            ps = con.prepareStatement(query.toString());
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                EngSikakuScoreInfo data = new EngSikakuScoreInfo();
                data.setDaigakuTandokuCode(rs.getString("DAIGAKUTANDOKUCODE"));
                data.setTkPatternCodeEigoSs(rs.getString("TKPATTERNCODEEIGOSS"));
                data.setTkRiyoUmucode(rs.getString("TKRIYOUMUCODE"));
                data.setTkManten(rs.getString("TKMANTEN"));
                data.setSankaShikakuCode(rs.getString("SANKASHIKAKUCODE"));
                data.setSankaShikakunaiRenban(rs.getInt("SANKASHIKAKUNAIRENBAN"));
                data.setHanteiHoshikiKubun(rs.getString("HANTEIHOSHIKIKUBUN"));
                data.setScore1(rs.getDouble("SCORE1"));
                data.setScore2(rs.getDouble("SCORE2"));
                data.setTokuten1(rs.getDouble("TOKUTEN1"));
                data.setTokuten2(rs.getDouble("TOKUTEN2"));
                data.setKeisyaritu(rs.getDouble("KEISYARITU"));

                items.add(data);
            }
        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
        return items;
    }

    @Override
    protected List<JpnKijutuScoreInfo> deployKokugoDescScoreConvList() throws Exception {
        /*PreparedStatement ps = null;*/
        /*ResultSet rs = null;*/
        Query query = getQuery("load_kokugo_desc_score_conv");
        List<JpnKijutuScoreInfo> items = new ArrayList();
        try (PreparedStatement ps = con.prepareStatement(query.toString());) {
            query = getQuery("load_kokugo_desc_score_conv");

            /*ps = con.prepareStatement(query.toString());*/
            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);

            try (ResultSet rs = ps.executeQuery();) {
                while (rs.next()) {
                    JpnKijutuScoreInfo data = new JpnKijutuScoreInfo();

                    data.setDaigakuTandokuCode(rs.getString("DAIGAKUTANDOKUCODE"));
                    data.setTkPatternCodeKokugoKjs(rs.getString("TKPATTERNCODEKOKUGOKJS"));
                    data.setTkManten(rs.getDouble("TKMANTEN"));
                    data.setKokugoTkhyoKubun(rs.getString("KOKUGOTKHYOKUBUN"));
                    data.setSetsumonBango(rs.getString("SETSUMONBANGO"));
                    data.setHyoka(rs.getString("HYOKA"));
                    data.setTokuten(rs.getDouble("TOKUTEN"));

                    items.add(data);
                }
            }
            /*} finally {
            DbUtils.closeQuietly(null, ps, rs);*/
        }
        return items;
    }

    @Override
    protected Map<String, ExamSubjectBean> deployExamSubject() throws Exception {
        ResultSet rs = null;
        Query query = getQuery("load_exam_subject");
        Map<String, ExamSubjectBean> items = new HashMap<>();
        try (PreparedStatement ps = con.prepareStatement(query.toString())) {

            ps.setFetchSize(1000);
            ps.setString(1, examYear);
            ps.setString(2, examDiv);
            ps.setString(3, examYear);
            ps.setString(4, examDiv);

            rs = ps.executeQuery();
            while (rs.next()) {
                ExamSubjectBean data = new ExamSubjectBean();
                String examTypeCd = rs.getString("EXAMTYPECD");
                data.setSubCd(rs.getString("SUBCD"));
                data.setSubName(rs.getString("SUBNAME"));
                data.setSubAllotPnt(rs.getInt("SUBALLOTPNT"));
                data.setDispSequence(rs.getString("DISPSEQUENCE"));
                data.setSubAllotPnt(rs.getInt("SUBALLOTPNT"));

                if (!items.containsKey(examTypeCd + data.getSubCd())) {
                    items.put(examTypeCd + data.getSubCd(), data);
                }
            }
        }
        return items;
    }

}

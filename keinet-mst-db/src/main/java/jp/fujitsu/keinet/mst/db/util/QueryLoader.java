package jp.fujitsu.keinet.mst.db.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * SQLを外部ファイルからロードしてキャッシュしておくクラス
 * ※JakartaCommonsDbutils内のクラスをほぼそのままに拡張
 *
 * @author kawai
 */
public final class QueryLoader {

    // Singleton
    private static final QueryLoader INSTANCE = new QueryLoader();

    // SQLをキャッシュしておくマップ
    private final Map queryMap = new HashMap();

    // プロパティファイル
    private final String properties = "QueryLoader.properties";

    private String cache; // キャッシュするかどうか
    private String path; // SQLファイルがあるディレクトリのパス

    /**
     * コンストラクタ
     */
    private QueryLoader() {

        // 設定を読み込む
        InputStream in = QueryLoader.class.getResourceAsStream(properties);

        if (in == null) {
            throw new IllegalArgumentException("properties file not found.");
        }

        Properties props = new Properties();

        try {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        cache = (String) props.get("CACHE");
    }

    /**
     * このクラスのインスタンスを返す
     * @return Singletonなインスタンス
     */
    public static QueryLoader getInstance() {
        return INSTANCE;
    }

    /**
     * キャッシュされているSQLを取り出す
     *
     * @param key 取り出すSQLを識別するキー（外部ファイル名）
     * @return SQL SQLオブジェクト
     * @throws SQLException ファイルの読み込みに失敗したら発生する
     */
    public Query load(final String key) throws SQLException {

        // キャッシュが無効または未ロードなら初期化
        if ("0".equals(cache) || !queryMap.containsKey(key)) {
            init(key);
        }

        return new Query((String) queryMap.get(key));
    }

    /**
     * キャッシュされているSQLを取り出す
     *
     * @param key 取り出すSQLを識別するキー（外部ファイル名）
     * @return SQL SQLオブジェクト
     * @throws SQLException ファイルの読み込みに失敗したら発生する
     */
    public String getQuery(final String key) throws SQLException {

        // キャッシュが無効または未ロードなら初期化
        if ("0".equals(cache) || !queryMap.containsKey(key)) {
            init(key);
        }

        return (String) queryMap.get(key);
    }

    /**
     * キャッシュの初期化をする
     *
     * @param key SQLを識別するキー
     * @throws SQLException ファイルの読み込みに失敗したら発生する
     */
    private synchronized void init(final String key) throws SQLException {

        // キャッシュ有効かつロード済みなら中止
        if ("1".equals(cache) && queryMap.containsKey(key)) {
            return;
        }

        final StringBuffer buff = new StringBuffer();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path + File.separator + key + ".sql"),
                    "Windows-31J"));
            String line;
            while ((line = reader.readLine()) != null) {
                buff.append(line + " ");
            }

        } catch (final UnsupportedEncodingException e) {
            throw new InternalError(e.getMessage());

        } catch (final IOException e) {
            throw new SQLException(
                    e.getClass().getName() + ":" + e.getMessage());

        } finally {
            try {
                if (reader != null) { reader.close(); }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

        queryMap.put(key, buff.toString());
    }

    /**
     * @param string SQLファイルディレクトリのパス
     */
    public void setPath(final String string) {
        path = string;
    }

}

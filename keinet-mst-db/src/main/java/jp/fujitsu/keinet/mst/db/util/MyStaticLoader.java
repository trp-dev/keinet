package jp.fujitsu.keinet.mst.db.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

/**
 *
 * クエリローダ
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MyStaticLoader {

    /**
     * クエリをロードする。
     *
     * @param name クエリ名
     * @return {@link Query}
     */
    public static Query load(String name) throws SQLException, IOException {
        return new Query(readSQL(name));
    }

    /**
     * SQLファイルを読み込む
     *
     * @param name ファイル名
     * @return SQL
     */
    private static String readSQL(String name) throws IOException {
        /*BufferedReader br = null;*/
        try (InputStream in = MyStaticLoader.class.getResourceAsStream(name + ".sql"); BufferedReader br = new BufferedReader(new InputStreamReader(in, "MS932"));) {

            StringBuffer sql = new StringBuffer();
            while (br.ready()) {
                sql.append(br.readLine());
                sql.append(" ");
            }
            return sql.toString();
            /*} finally {
            if (br != null) {
                br.close();
            }*/
        }
    }

}

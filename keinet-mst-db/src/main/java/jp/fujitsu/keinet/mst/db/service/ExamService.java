package jp.fujitsu.keinet.mst.db.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.fujitsu.keinet.mst.db.util.MyStaticLoader;

import org.apache.commons.dbutils.DbUtils;

import com.fjh.beans.DefaultBean;

/**
 *
 * 模試検索サービス
 *
 * @author Tomohisa YAMADA - TOTEC
 * @version 1.0
 *
 */
public class ExamService extends DefaultBean {

    /** シリアルID */
    private static final long serialVersionUID = 4649626911215560270L;

    /**
     * 模試年度
     */
    private String examYear;

    /**
     * 模試区分
     */
    private String examDiv;

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws Exception {

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(MyStaticLoader.load("w10").toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                this.examYear = rs.getString(1);
                this.examDiv = rs.getString(2);
            } else {
                throw new Exception(
                        "模試が見つかりませんでした。");
            }

        } finally {
            DbUtils.closeQuietly(null, ps, rs);
        }
    }

    /**
     * @return examDiv 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }
    /**
     * @return examYear 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

}

package jp.fujitsu.keinet.mst.db.factory;

import java.sql.Connection;

/**
 *
 * 入試日程を読み込まないマスタ展開ロジックです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivFactoryNoSchedule extends UnivFactoryDB {

    /**
     * コンストラクタです。
     *
     * @param con DBコネクション
     */
    public UnivFactoryNoSchedule(Connection con) throws Exception {
        super(con);
    }

    /**
     * {@inheritDoc}
     */
    protected void deployExamscheduledetail(UnivMasterDBContainer container) throws Exception {
    }

}

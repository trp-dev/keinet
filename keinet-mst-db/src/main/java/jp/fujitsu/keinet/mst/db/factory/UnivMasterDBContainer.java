package jp.fujitsu.keinet.mst.db.factory;

import java.util.HashMap;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.factory.UnivMasterContainer;

/**
 *
 * DB用の {@link UnivMasterContainer} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterDBContainer extends UnivMasterContainer {

    /** スケジュールマップ */
    private Map scheduleMap;

    /**
     * {@inheritDoc}
     */
    protected void initialize() {

        super.initialize();

        if (getRestriction().isEmpty()) {
            scheduleMap = new HashMap(RECNUM_BASIC * 4 / 3);
        } else {
            scheduleMap = new HashMap();
        }
    }

    /**
     * スケジュールマップを返します。
     *
     * @return スケジュールマップ
     */
    public Map getScheduleMap() {
        return scheduleMap;
    }

}

SELECT
  UNIVCD || FACULTYCD || DEPTCD
  , UNICDBRANCHCD
  , ENTEXAMDIV
  , SELECTG_NO
  , SELECTGSUBCOUNT
  , SELECTGALLOTPNT
FROM
  UNIVMASTER_SELECTGROUP_PUB
WHERE
  ENTEXAMDIV IN ('1', '2')
  AND SCHOOLDIV = '5'
  AND EVENTYEAR = ?
  AND EXAMDIV = ?

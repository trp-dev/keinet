SELECT
  UNIVCD || FACULTYCD || DEPTCD
  , UNICDBRANCHCD
  , ENTEXAMDIV
  , EXAMSUBCD
  , NVL(SUBBIT, ' ') SUBBIT
  , NVL(SUBPNT, 9999.9) SUBPNT
  , NVL(TRIM(SUBAREA), 0) SUBAREA
  , TRIM(CONCURRENT_NG) CONCURRENT_NG
FROM
  UNIVMASTER_SUBJECT
WHERE
  ENTEXAMDIV IN ('1', '2', '3')
  AND SCHOOLDIV = '5'
  AND ADMISSIONDIV = '0'
  AND NEXTYEARDIV IN ('0', '9')
  AND EVENTYEAR = ?
  AND EXAMDIV = ?

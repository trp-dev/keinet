SELECT
    E.EXAMCD
  , E.EXAMNAME_ABBR
  , E.EXAMTYPECD
  , S.SUBCD
  , S.SUBNAME
  , S.SUBALLOTPNT
  , S.DISPSEQUENCE
FROM
  EXAMSUBJECT S
  INNER JOIN (
    SELECT
      E.*
    FROM
      EXAMINATION E
    WHERE
      E.EXAMYEAR = ?
      AND E.EXAMDIV = ?
      AND E.EXAMTYPECD IN ('01', '02')
    UNION
    SELECT
      E.*
    FROM
      EXAMINATION E
    WHERE
      EXISTS (
        SELECT
          1
        FROM
          EXAMINATION T
        WHERE
          T.EXAMYEAR = ?
          AND T.EXAMDIV = ?
          AND T.EXAMYEAR = E.EXAMYEAR
          AND (
            T.DOCKINGEXAMCD = E.EXAMCD
            OR E.DOCKINGEXAMCD = T.EXAMCD
          )
      )
      AND E.EXAMTYPECD IN ('01', '02')
  ) E
    ON E.EXAMYEAR = S.EXAMYEAR
    AND E.EXAMCD = S.EXAMCD
WHERE
 S.SUBCD < '7000'
ORDER BY
  E.EXAMTYPECD
  , E.INPLEDATE

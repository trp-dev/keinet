SELECT
  UNIVCD || FACULTYCD || DEPTCD
  , UNICDBRANCHCD
  , ENTEXAMDIV
  , COURSECD
  , NECESALLOTPNT
  , NECESSUBCOUNT
  , CEN_SECSUBCHOICEDIV
FROM
  UNIVMASTER_COURSE_PUB
WHERE
  ENTEXAMDIV IN ('1', '2')
  AND SCHOOLDIV = '5'
  AND EVENTYEAR = ?
  AND EXAMDIV = ?

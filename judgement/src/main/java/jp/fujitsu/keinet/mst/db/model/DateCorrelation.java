package jp.fujitsu.keinet.mst.db.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 *
 * 入試日程日と相関記号を使って日付を文字列表現する
 * 例）「20081107」 「=」 「20081108」 「/」 「20090109」 >>> 「11/7〜8,1/9」
 *
 *
 * <2010年度改修>
 * 2009.12.25	Tomohisa YAMADA - Totec
 * 				新規作成
 *
 * @author Tomohisa YAMADA - Totec
 * @version 1.0
 *
 */
public class DateCorrelation {

    /**
     * 文字列作成用バッファ
     */
    final StringBuffer buf = new StringBuffer();

    /**
     * 一意の日付リスト
     */
    final List uniques = new ArrayList();

    /**
     * カレンダー
     */
    final Calendar cal = Calendar.getInstance();

    /**
     * 日付リスト
     */
    private List dates = new ArrayList();

    /**
     * 相関文字列リスト
     */
    private List signs = new ArrayList();

    //ショートスラッシュトリム
    final static SimpleDateFormat FORMAT_SST = new SimpleDateFormat("M/d", Locale.JAPAN);
    //デート
    final static SimpleDateFormat FORMAT_DT = new SimpleDateFormat("d", Locale.JAPAN);

    //変換用マスタテーブル
    final private static String[][] conversion = {
        {"+", "・"},
        {"/", ","},
        {"-", "〜"},
        {"=", "〜"},
        {"*", "-"},
    };

    /**
     * 記号を変換
     * @param sign
     * @return
     */
    private static String convert(String sign) {
        for (int i = 0; i < conversion.length; i ++) {
            if (conversion[i][0].equals(sign)) {
                return conversion[i][1];
            }
        }

        throw new InternalError("該当の記号が見つかりません。");
    }

    /**
     * 一意の日付をリストにして返す。
     * @return
     */
    public List getUniqueDates() {

        List result = null;

        //そもそも日付がひとつの場合は
        //その日付だけをリストに詰めて返す
        if (dates.size() == 1) {
            result = new ArrayList();
            result.add(dates.get(0));
            return result;
        }

        result = _getUniqueDates(dates, signs);

        Collections.sort(result);

        return result;
    }

    /**
     * 一意の日付をリストにして返す。
     * @return
     */
    private List _getUniqueDates(List dates, List signs) {

        if (dates.size() == 1) {
            return uniques;
        }
        else {

            //すでに日程リストが存在している場合は
            //日程が重複しないように最後のエレメントを削除する
            if (uniques.size() > 0) {
                uniques.remove(uniques.size() - 1);
            }

            //「〜」の場合は期間を設定
            if ("-".equals(signs.get(0))
                    || "=".equals(signs.get(0))) {
                uniques.addAll(between((Date) dates.get(0), (Date) dates.get(1)));
            }
            //「」の場合は日付をそのまま設定
            else {
                uniques.add((Date) dates.get(0));
                uniques.add((Date) dates.get(1));
            }

            return _getUniqueDates(dates.subList(1, dates.size()), signs.subList(1, signs.size()));

        }
    }

    /**
     * 日付の開始日と終了日の間の日付をリストで返す
     * @return
     */
    private List between(Date start, Date end) {

        Calendar c = Calendar.getInstance();

        c.setTime(start);

        long diffs = end.getTime() - start.getTime();

        //同じ日の時は最初の日付だけ返す
        if (diffs == 0){
            uniques.add(start);
        }

        //日付が違うときは差分を取得
        final List datess = new ArrayList();

        int days = (int) (diffs/86400000 + 1);

        //から"〜"の時にその間の日付が必要
        int daysBetween = 0;

        while (days > 1) {

            if (daysBetween != 0) {
                c.add(Calendar.DAY_OF_YEAR, 1);
                days --;
            }

            datess.add(c.getTime());
            daysBetween ++;
        }

        return datess;
    }

    /**
     * 文字列の作成実行
     */
    public String createString() {

        if (dates.size() == 0) {
            return "";
        }

        //最初の日程を設定
        buf.append(FORMAT_SST.format(dates.get(0)));

        return _createString(dates, signs);
    }

    /**
     * 文字列算出メソッド（内部処理）
     */
    private String _createString(List dates, List signs) {

        if (dates.size() == 1) {
            return buf.toString();
        }
        else {

            cal.setTime((Date)dates.get(0));
            int m1 = cal.get(Calendar.MONTH);

            cal.setTime((Date)dates.get(1));
            int m2 = cal.get(Calendar.MONTH);

            buf.append(convert((String)signs.get(0)));

            if (m1 == m2) {
                buf.append(FORMAT_DT.format(dates.get(1)));
            }
            else {
                buf.append(FORMAT_SST.format(dates.get(1)));
            }

            return _createString(
                    dates.subList(1, dates.size()),
                    signs.subList(1, signs.size()));
        }
    }

    /**
     * 日付を追加
     */
    public void addDate(Date date) {
        dates.add(date);
    }

    /**
     * 相関記号を追加
     */
    public void addSign(String sign) {
        signs.add(sign);
    }
}

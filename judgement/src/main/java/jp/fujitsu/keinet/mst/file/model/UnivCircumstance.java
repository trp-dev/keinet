package jp.fujitsu.keinet.mst.file.model;

import java.io.Serializable;

/**
 * バンザイシステムの 大学状況クラス
 *
 *
 * 2000.10.09	Yumi MAKITA - 不明
 * 				新規作成
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 *              マスタデータの持ち方変更
 *
 * @author Yumi MAKITA - 不明
 * @version 1.0
 *
 *
 */
public class UnivCircumstance implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -8066558458717270086L;

    /**
       * 大学コード
       */
    public String univCode;

    /**
     * グラフの起点
     */
    public int startingPoint;

    /**
     * 合計人数
     */
    public int total;

    /**
     * 平均点
     */
    public double average;

    /**
     * 5点区切りデータ 27データ
     */
    public int[] details;

    /**
     * コンストラクタ
     * <UL>
     * 空の大学状況クラスオブジェクトを作る
     * </UL>
     */
    public UnivCircumstance() {
        univCode = "";
        startingPoint = 0;
        total = 0;
        average = 0;
        details = new int[27];
    }

    /**
     * コンストラクタ
     * <UL>
     * 大学状況クラスオブジェクトを作る
     * </UL>
     * @param <code>univcode</code> 大学コード
     * @param <code>startingPoint</code> グラフの起点
     * @param <code>total</code> 合計人数
     * @param <code>average</code> 平均点
     * @param <code>details</code> 5点区切りデータ
     */
    public UnivCircumstance(String aUnivCode, int aStartingPoint, int aTotal, double anAverage, int[] aDetails) {
        univCode = aUnivCode;
        startingPoint = aStartingPoint;
        total = aTotal;
        average = anAverage;
        details = aDetails;
    }

    /**
     * グラフ表示用のパラメタを取得する
     * @return グラフ表示用のデータ配列  int[] {x軸のサイズ,y軸の最大値,起点となる点数,起点から５点刻みの間に存在する人数×27}
     */
    public int[] getGraphParams() {
        int max = 0;
        for (int i = 0; i < details.length; i++) {
            if (details[i] > max) {
                max = details[i];
            }
        }
        int[] graphParam = new int[30];
        graphParam[0] = 27; //x軸
        graphParam[1] = max; //y軸の最大値
        graphParam[2] = startingPoint; //起点となる点数
        for (int i = 0; i < 27; i++) {
            graphParam[i + 3] = details[i];
        }
        return graphParam;
    }

}

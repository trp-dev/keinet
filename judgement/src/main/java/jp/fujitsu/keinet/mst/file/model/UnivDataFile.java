package jp.fujitsu.keinet.mst.file.model;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterBasicPlus;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;

/**
 *
 * バンザイ用判定大学基本情報
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivDataFile extends UnivData {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -3802775955872519378L;

    /**
     * 足切予想得点
     */
    private int pickedPoint;

    /**
     * 足切総合配点
     */
    private int pickedPointSub;

    /**
     * 入試状況データ
     */
    protected UnivCircumstance circumstance;

    /**
     * 前年度倍率
     */
    private String preMagnification = "";

    /**
     * コンストラクタです。
     */
    public UnivDataFile(UnivMasterBasicPlus basic) {
        super(basic.getUnivCd(), basic.getFacultyCd(), basic.getDeptCd(), basic.getUniDiv(), basic.getUniGDiv(), basic.getBorderScoreNoExam1(), basic.getBorderScoreNoExam2(), basic.getBorderScoreNoExam3(), basic.getBorderScoreRateNoExam2(),
                basic.getEntExamRank(), basic.getDockingNeedLDiv(), basic.getPubRejectExScore(), basic.getChoiceCD5(), basic.getRemarks(), basic.getDeptSortKey(), basic.getPrefNameExamHo(), basic.getPrefCdExamHo(), basic.getBunriCd(),
                basic.getUniNameAbbr(), basic.getFacultyNameAbbr(), basic.getDeptNameAbbr(), basic.getUnivNameKana(), basic.getDeptNameKana(), basic.getPubEntExamCapa(), basic.getUniCoedDiv(), basic.getUniNightDiv(),
                basic.getPrefCdExamSh(), basic.getPrefNameExamSh(), basic.getDistrictCd(), basic.getUnivKanjiName(), basic.getUnivKanaName(), basic.getUnivOtherName(), basic.getKanaNum(), basic.getFacultyConCd(), basic.getDeptSerialNo(),
                basic.getScheduleSys(), basic.getScheduleSysBranchCd(), basic.getExamCapaRel(), Integer.parseInt(StringUtil.nvl(basic.getBorderInDiv(), "1")), basic.getBorderScore2(), basic.getBorderScore1(), basic.getBorderScore3(),
                basic.getAppReqPatternCd(), Integer.parseInt(StringUtil.nvl(basic.getEngPtBorderUseFlg(), "0")), basic.getEngPtRepBranch());
        this.pickedPoint = basic.getRejectExScore();
    }

    /**
     * 足切情報を返します。
     *
     * @return 足切情報
     */
    public String getPickedLine() {
        if (pickedPoint == 0) {
            /* 予定はあったがなしの見こみ */
            return "なしの見込み";
        } else if (pickedPoint == 99999 || pickedPointSub == 0) {
            /* 当初から足きりの予定なし */
            return "--";
        } else {
            /* 足切情報文字列を返す */
            return pickedPoint + "/" + pickedPointSub + "点";
        }
    }

    /**
     * 前年度倍率を設定する。
     *
     * @param preMagnification 前年度倍率
     */
    public void setPreMagnification(String preMagnification) {
        this.preMagnification = preMagnification;
    }

    /**
     * 入試状況インスタンスを返す。
     *
     * @return 入試状況インスタンス
     */
    public UnivCircumstance getCircumstance() {
        return circumstance;
    }

    /**
     * 入試状況インスタンスを設定する。
     *
     * @param circumstance 入試状況インスタンス
     */
    public void setCircumstance(UnivCircumstance circumstance) {
        this.circumstance = circumstance;
    }

    /**
     * 前年度倍率を返す。
     *
     * @return 前年度倍率
     */
    public String getPreMagnification() {
        return preMagnification;
    }

    /**
     * 足切総合配点を設定する。
     *
     * @param i 足切総合配点
     */
    public void setPickedPointSub(int i) {
        pickedPointSub = i;
    }

}

package jp.co.fj.kawaijuku.judgement.factory.helper;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;

/**
 *
 * 二次範囲ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SecondRangeHelper extends SubjectHelper {

    public static void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject, SubjectKey subjectKey) {
        if (subject != null && subject.isImposed()) {
            if (subject.isRequired()) {
                setRequired(detailHolder, subject, subjectKey);
            } else {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectKey, SecondDetail.class);
                setElective(detailHolder, subject, newSubjectKey);
            }
        }
    }

    static void setRequired(CommonDetailHolder detailHolder, UnivMasterSubject ums_subject, SubjectKey subjectKey) {
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(subjectKey);
        if (!imposingSubject.getRequired().isImposed()) {
            /* 必須情報が未設定の場合のみ設定する */
            detailHolder.getImposingSubjects().put(subjectKey, imposingSubject.validateRequired(subjectKey.getCourseIndex(), ums_subject.getSubBit().trim(), ums_subject.getSubPnt(), Integer.parseInt(ums_subject.getSubArea())));
        }
    }

    static void setElective(CommonDetailHolder detailHolder, UnivMasterSubject ums_subject, SubjectKey subjectKey) {
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(subjectKey);
        if (!imposingSubject.getElective().isImposed()) {
            /* 選択情報が未設定の場合のみ設定する */
            detailHolder.getImposingSubjects().put(subjectKey,
                    imposingSubject.validateElective(subjectKey.getCourseIndex(), ums_subject.getSubBit().trim(), ums_subject.getSubPnt(), ums_subject.getConcurrentNg(), Integer.parseInt(ums_subject.getSubArea())));
        }
    }

}

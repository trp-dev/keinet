package jp.co.fj.kawaijuku.judgement.util;

/**
*
* 文字列操作クラス
*
* @author QQ)Kurimoto
* @version 1.0
*
*/
public class StringUtil {

    /**
     * 値が空文字かどうかチェックします。
     *
     * @param value 値
     * @return true:空文字、false:空文字でない
     */
    public static boolean isEmpty(String value) {
        return value == null || value.isEmpty();
    }

    /**
     * 値が空文字かどうかチェックします。
     * 空白が埋まっている場合も空文字と判定します。
     *
     * @param value 値
     * @return true:空文字、false:空文字でない
     */
    public static boolean isEmptyTrim(String value) {
        return value == null || value.isEmpty() || value.trim().length() == 0;
    }

    /**
     * 値が空文字の場合、ブランクを返却します。
     *
     * @param value 値
     * @return
     */
    public static String nvl(String value) {
        return nvl(value, "");
    }

    /**
     * 値が空文字の場合、代替文字を返却します。
     *
     * @param value 値
     * @param ifNullValue 代替文字
     * @return
     */
    public static String nvl(String value, String ifNullValue) {
        return isEmpty(value) ? ifNullValue : value;
    }
}

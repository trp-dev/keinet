package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 大学マスタ模試用教科データクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterCourse {

    //必須配点
    private final double necesAllotPnt;

    //必須科目数
    private final int necesSubCount;

    //センター二次同一科目選択区分
    private final int cenSecSubChoiceDiv;

    public UnivMasterCourse(double necesAllotPnt, int necesSubCount, int cenSecSubChoiceDiv) {
        this.necesAllotPnt = necesAllotPnt;
        this.necesSubCount = necesSubCount;
        this.cenSecSubChoiceDiv = cenSecSubChoiceDiv;
    }

    public double getNecesAllotPnt() {
        return necesAllotPnt;
    }

    public int getNecesSubCount() {
        return necesSubCount;
    }

    public int getCenSecSubChoiceDiv() {
        return cenSecSubChoiceDiv;
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

import java.util.List;
import java.util.Map;

/**
 *
 * 大学マスタ展開処理用インターフェース
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface UnivFactory {

    /**
     * 判定用大学マスタをロードして返します。
     *
     * @return 大学インスタンスリスト
     */
    List getLoadedData() throws Exception;

    /**
     * 公表用大学マスタをロードして返します。
     *
     * @return 公表用大学マスタマップ（key=大学コード＋学部コード＋学科コード）
     */
    Map getLoadedPubData() throws Exception;

    /**
     * ロードした大学マスタの模試区分を返します。
     *
     * @return ロードした大学マスタの模試区分
     */
    String getExamDiv();

    /**
     * 制限する大学を追加する。
     *
     * @param key 大学コード
     */
    void restrict(String key);

    /**
     * 情報誌用科目データをロードします。
     *
     * @return 情報誌用科目データマップ（key=大学コード＋学部コード＋学科コード）
     */
    Map loadUnivInfo() throws Exception;

    /**
     * 英語認定試験、国語記述情報データをロードします。
     *
     * @return 英語認定試験、国語記述情報データマップ
     * @throws Exception
     */
    Map loadSubjectInfo() throws Exception;
}

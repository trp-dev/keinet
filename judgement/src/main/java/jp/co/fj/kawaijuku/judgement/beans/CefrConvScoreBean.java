package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * CEFR換算用スコアマスタの情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
@ToString
public class CefrConvScoreBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7369792716576276233L;

    /** 認定試験コード */
    private String engPtCd;

    /** CEFRレベルコード */
    private String cefrCd;

    /** CEFR換算スコア */
    private double cefrConvScore;

}

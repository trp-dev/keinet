package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 英語認定試験の配点を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoEngCertBean implements UnivInfoAllotBean {

    /** ラベル */
    private final String label;

    /** 共通T英語認定試験 */
    private final String engCertApp;

    /** 共通テスト英語認定試験−配点信頼 */
    private final String engCertTrustPt;

    /** 共通テスト英語認定試験−配点 */
    private final String engCertPt;

    /** 共通テスト英語認定試験−加点 */
    private final String engCertAddPt;

    /** 共通テスト英語認定試験−加点種類 */
    private final String engCertAddPtType;

    /** 共通テスト英語認定試験−必選 */
    private final String engCertSel;

    /**
     * コンストラクタです。
     *
     * @param engCertApp 共通T英語認定試験
     * @param engCertTrustPt 配点信頼
     * @param engCertPt 配点
     * @param engCertAddPt 加点
     * @param engCertAddPtType 加点種類
     * @param engCertSel 必選
     */
    UnivInfoEngCertBean(String label, String engCertApp, String engCertTrustPt, String engCertPt, String engCertAddPt, String engCertAddPtType, String engCertSel) {
        this.label = label;
        this.engCertApp = engCertApp;
        this.engCertTrustPt = engCertTrustPt;
        this.engCertPt = engCertPt;
        this.engCertAddPt = engCertAddPt;
        this.engCertAddPtType = engCertAddPtType;
        this.engCertSel = engCertSel;
    }

    public String getMark() {
        return engCertApp;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public boolean isAllot() {
        return engCertApp != null && engCertApp.length() > 0;
    }

    @Override
    public String getTrustPt() {
        return engCertTrustPt;
    }

    @Override
    public String getPt() {
        return engCertPt;
    }

    @Override
    public String getAddPt() {
        return engCertAddPt;
    }

    @Override
    public String getAddPtType() {
        return engCertAddPtType;
    }

    @Override
    public String getSel() {
        return engCertSel;
    }

}

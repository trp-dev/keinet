package jp.co.fj.kawaijuku.judgement.beans;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 国語記述式総合評価マスタの情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
@ToString
public class JpnDescTotalEvaluationBean {

    /** 科目コード */
    private String subCd;

    /** 問１評価 */
    private String q1Evaluation;

    /** 問２評価 */
    private String q2Evaluation;

    /** 問３評価 */
    private String q3Evaluation;

    /** 総合評価 */
    private String totalEvaluation;

    /**
     * 問１〜問３評価が一致するかどうか返却します。
     *
     * @param q1 問１評価
     * @param q2 問２評価
     * @param q3 問３評価
     * @return true:一致する、false:一致しない
     */
    public boolean equals(String q1, String q2, String q3) {
        if (q1Evaluation == null || q2Evaluation == null || q3Evaluation == null) {
            return false;
        }
        if (q1Evaluation.equals(q1) && q2Evaluation.equals(q2) && q3Evaluation.equals(q3)) {
            return true;
        } else {
            return false;
        }
    }
}

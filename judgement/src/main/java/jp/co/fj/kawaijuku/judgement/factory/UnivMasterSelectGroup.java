package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 大学マスタ模試用選択グループデータクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterSelectGroup {

    //選択グループ科目数
    private final int selectGSubCount;

    //選択グループ配点
    private final double selectGAllotPnt;

    public UnivMasterSelectGroup(int selectGSubCount, double selectGAllotPnt) {
        this.selectGSubCount = selectGSubCount;
        this.selectGAllotPnt = selectGAllotPnt;
    }

    public int getSelectGSubCount() {
        return selectGSubCount;
    }

    public double getSelectGAllotPnt() {
        return selectGAllotPnt;
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 大学マスタ基本情報データクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterBasicPlus {

    //大学コード5桁
    private final String choiceCD5;

    //大学コード
    private final String univCd;

    //学部コード
    private final String facultyCd;

    //学科コード
    private final String deptCd;

    //大学区分
    private final String uniDiv;

    //大学短縮名：UNINAME_ABBR
    private final String uniNameAbbr;

    //学部短縮名：FACULTYNAME_ABBR
    private final String facultyNameAbbr;

    //学科短縮名：DEPTNAME_ABBR
    private final String deptNameAbbr;

    //大学カナ名：UNIVNAME_KANA
    private final String univNameKana;

    //学科カナ名：DEPTNAME_KANA
    private final String deptNameKana;

    //大学グループ区分（日程区分）
    private final String uniGDiv;

    //受験校本部県コード：PREFCD_EXAMHO
    private final String prefCdExamHo;

    //受験校校舎県コード：PREFCD_EXAMSH
    private final String prefCdExamSh;

    //ボーダ得点1_今年：BORDERSCORE1
    private final int borderScore1;

    //ボーダ得点2_今年：BORDERSCORE2
    private final int borderScore2;

    //ボーダ得点3_今年：BORDERSCORE3
    private final int borderScore3;

    //ボーダ得点率2_今年：BORDERSCORERATE2
    private final double borderScoreRate2;

    //文理コード：BUNRICD
    private final String bunriCd;

    //一般入試定員：PUBENTEXAMCAPA
    private final int pubEntExamCapa;

    //大学男女共学区分：UNICOEDDIV
    private final int uniCoedDiv;

    //大学夜間部区分：UNINIGHTDIV
    private final int uniNightDiv;

    //入試ランク_今年：ENTEXAMRANK
    private final String entExamRank;

    //学科ソートキー：DEPTSORTKEY
    private final String deptSortKey;

    //公表用足切予想得点
    private final int pubRejectExScore;

    //足切予想得点 _今年
    private final int rejectExScore;

    //ドッキング判定不要区分
    private final int dockingNeedLDiv;

    //---------------------------------------------

    private final String prefNameExamHo;
    //  String prefName = rs.getString("prefname");

    private final String prefNameExamSh;
    //  String prefNameSh = rs.getString("prefname_sh");

    //地区コード
    //※県コードを使って県マスタから取得する
    private final String districtCd;
    //  String districtCd = rs.getString("districtcd");

    //ガイド文字列
    //※ガイド備考から取得する
    private final String remarks;

    //大学コード表名
    private final String univKanjiName;

    //検索用大学カナ名
    private final String univKanaName;

    //大学その他名称
    private final String univOtherName;

    //カナ付50音順番号
    private final String kanaNum;

    //学部内容コード
    private final String facultyConCd;

    //学科通番
    private final String deptSerialNo;

    //日程方式
    private final String scheduleSys;

    //日程方式枝番
    private final String scheduleSysBranchCd;

    //入試定員信頼性
    private final String examCapaRel;

    //ボーダ入力区分:BORDERINDIV
    private final String borderInDiv;

    //ボーダ得点−濃厚(認定試験無し):BORDERSCORENOEXAM1
    private final int borderScoreNoExam1;

    //ボーダ得点−ボーダ(認定試験無し):BORDERSCORENOEXAM2
    private final int borderScoreNoExam2;

    //ボーダ得点−注意(認定試験無し):BORDERSCORENOEXAM3
    private final int borderScoreNoExam3;

    //ボーダ得点率−濃厚(認定試験無し):BORDERSCORERATENOEXAM1
    private final double borderScoreRateNoExam1;

    //ボーダ得点率−ボーダ(認定試験無し):BORDERSCORERATENOEXAM2
    private final double borderScoreRateNoExam2;

    //ボーダ得点率−注意(認定試験無し):BORDERSCORERATENOEXAM3
    private final double borderScoreRateNoExam3;

    //入試満点値−共通テスト:ENTEXAMFULLPNT_CENTER
    private final double entExamFullPntCenter;

    //総合配点−共通テスト:TOTALALLOTPNT_CENTER
    private final double totalAllotPntCenter;

    //入試満点値−共通テスト（認定試験なし）:ENTEXAMFULLPNT_CENTER_NOEXAM
    private final double entExamFullPntCenterNoExam;

    //総合配点−共通テスト（認定試験なし）:TOTALALLOTPNT_CENTER_NOEXAM
    private final double totalAllotPntCenterNoExam;

    //設定パターン:SETTINGPATTERN
    private final String settingPattern;

    //出願要件パターンコード:APP_REQ_PATTERN_CD
    private final String appReqPatternCd;

    //英語認定試験ボーダー利用フラグ:ENGPT_BORDERUSEFLG
    private final String engPtBorderUseFlg;

    //英語認定試験代表枝番:ENGPT_REPBRANCH
    private final String engPtRepBranch;

    /**
     * コンストラクタです。
     * @param choiceCD5
     * @param univCd
     * @param facultyCd
     * @param deptCd
     * @param uniDiv
     * @param uniNameAbbr
     * @param facultyNameAbbr
     * @param deptNameAbbr
     * @param univNameKana
     * @param deptNameKana
     * @param uniGDiv
     * @param prefCdExamHo
     * @param prefCdExamSh
     * @param borderScore1
     * @param borderScore2
     * @param borderScore3
     * @param borderScoreRate2
     * @param bunriCd
     * @param pubEntExamCapa
     * @param uniCoedDiv
     * @param uniNightDiv
     * @param entExamRank
     * @param deptSortKey
     * @param pubRejectExScore
     * @param rejectExScore
     * @param dockingNeedLDiv
     * @param prefNameExamHo
     * @param prefNameExamSh
     * @param districtCd
     * @param remarks
     * @param univKanjiName
     * @param univKanaName
     * @param univOtherName
     * @param kanaNum
     * @param facultyConCd
     * @param deptSerialNo
     * @param scheduleSys
     * @param scheduleSysBranchCd
     * @param examCapaRel
     * @param borderInDiv
     * @param borderScoreNoExam1
     * @param borderScoreNoExam2
     * @param borderScoreNoExam3
     * @param borderScoreRateNoExam1
     * @param borderScoreRateNoExam2
     * @param borderScoreRateNoExam3
     * @param entExamFullPntCenter
     * @param totalAllotPntCenter
     * @param entExamFullPntCenterNoExam
     * @param totalAllotPntCenterNoExam
     * @param settingPattern
     * @param appReqPatternCd
     * @param engPtBorderUseFlg
     * @param engPtRepBranch
     */
    public UnivMasterBasicPlus(String univCd, String facultyCd, String deptCd, String uniDiv, String uniNameAbbr, String facultyNameAbbr, String deptNameAbbr, String univNameKana, String deptNameKana, String uniGDiv, String prefCdExamHo,
            String prefCdExamSh, int borderScore1, int borderScore2, int borderScore3, double borderScoreRate2, String bunriCd, int pubEntExamCapa, int uniCoedDiv, int uniNightDiv, String entExamRank, String deptSortKey,
            int pubRejectExScore, int rejectExScore, int dockingNeedLDiv, String prefNameExamHo, String prefNameExamSh, String districtCd, String remarks, String choiceCD5, String univKanjiName, String univKanaName, String univOtherName,
            String kanaNum, String facultyConCd, String deptSerialNo, String scheduleSys, String scheduleSysBranchCd, String examCapaRel, String borderInDiv, int borderScoreNoExam1, int borderScoreNoExam2, int borderScoreNoExam3,
            double borderScoreRateNoExam1, double borderScoreRateNoExam2, double borderScoreRateNoExam3, double entExamFullPntCenter, double totalAllotPntCenter, double entExamFullPntCenterNoExam, double totalAllotPntCenterNoExam,
            String settingPattern, String appReqPatternCd, String engPtBorderUseFlg, String engPtRepBranch) {
        super();
        this.choiceCD5 = choiceCD5;
        this.univCd = univCd;
        this.facultyCd = facultyCd;
        this.deptCd = deptCd;
        this.uniDiv = uniDiv;
        this.uniNameAbbr = uniNameAbbr;
        this.facultyNameAbbr = facultyNameAbbr;
        this.deptNameAbbr = deptNameAbbr;
        this.univNameKana = univNameKana;
        this.deptNameKana = deptNameKana;
        this.uniGDiv = uniGDiv;
        this.prefCdExamHo = prefCdExamHo;
        this.prefCdExamSh = prefCdExamSh;
        this.borderScore1 = borderScore1;
        this.borderScore2 = borderScore2;
        this.borderScore3 = borderScore3;
        this.borderScoreRate2 = borderScoreRate2;
        this.bunriCd = bunriCd;
        this.pubEntExamCapa = pubEntExamCapa;
        this.uniCoedDiv = uniCoedDiv;
        this.uniNightDiv = uniNightDiv;
        this.entExamRank = entExamRank;
        this.deptSortKey = deptSortKey;
        this.pubRejectExScore = pubRejectExScore;
        this.rejectExScore = rejectExScore;
        this.dockingNeedLDiv = dockingNeedLDiv;
        this.prefNameExamHo = prefNameExamHo;
        this.prefNameExamSh = prefNameExamSh;
        this.districtCd = districtCd;
        this.remarks = remarks;
        this.univKanjiName = univKanjiName;
        this.univKanaName = univKanaName;
        this.univOtherName = univOtherName;
        this.kanaNum = kanaNum;
        this.facultyConCd = facultyConCd;
        this.deptSerialNo = deptSerialNo;
        this.scheduleSys = scheduleSys;
        this.scheduleSysBranchCd = scheduleSysBranchCd;
        this.examCapaRel = examCapaRel;
        this.borderInDiv = borderInDiv;
        this.borderScoreNoExam1 = borderScoreNoExam1;
        this.borderScoreNoExam2 = borderScoreNoExam2;
        this.borderScoreNoExam3 = borderScoreNoExam3;
        this.borderScoreRateNoExam1 = borderScoreRateNoExam1;
        this.borderScoreRateNoExam2 = borderScoreRateNoExam2;
        this.borderScoreRateNoExam3 = borderScoreRateNoExam3;
        this.entExamFullPntCenter = entExamFullPntCenter;
        this.totalAllotPntCenter = totalAllotPntCenter;
        this.entExamFullPntCenterNoExam = entExamFullPntCenterNoExam;
        this.totalAllotPntCenterNoExam = totalAllotPntCenterNoExam;
        this.settingPattern = settingPattern;
        this.appReqPatternCd = appReqPatternCd;
        this.engPtBorderUseFlg = engPtBorderUseFlg;
        this.engPtRepBranch = engPtRepBranch;
    }

    public String getChoiceCD5() {
        return this.choiceCD5;
    }

    public String getUnivCd() {
        return univCd;
    }

    public String getFacultyCd() {
        return facultyCd;
    }

    public String getDeptCd() {
        return deptCd;
    }

    public String getUniDiv() {
        return uniDiv;
    }

    public String getUniNameAbbr() {
        return uniNameAbbr;
    }

    public String getFacultyNameAbbr() {
        return facultyNameAbbr;
    }

    public String getDeptNameAbbr() {
        return deptNameAbbr;
    }

    public String getUnivNameKana() {
        return univNameKana;
    }

    public String getDeptNameKana() {
        return deptNameKana;
    }

    public String getUniGDiv() {
        return uniGDiv;
    }

    public String getPrefCdExamHo() {
        return prefCdExamHo;
    }

    public String getPrefCdExamSh() {
        return prefCdExamSh;
    }

    public int getBorderScore1() {
        return borderScore1;
    }

    public int getBorderScore2() {
        return borderScore2;
    }

    public int getBorderScore3() {
        return borderScore3;
    }

    public double getBorderScoreRate2() {
        return borderScoreRate2;
    }

    public String getBunriCd() {
        return bunriCd;
    }

    public int getPubEntExamCapa() {
        return pubEntExamCapa;
    }

    public int getUniCoedDiv() {
        return uniCoedDiv;
    }

    public int getUniNightDiv() {
        return uniNightDiv;
    }

    public String getEntExamRank() {
        return entExamRank;
    }

    public String getDeptSortKey() {
        return deptSortKey;
    }

    public int getRejectExScore() {
        return rejectExScore;
    }

    public int getDockingNeedLDiv() {
        return dockingNeedLDiv;
    }

    public String getPrefNameExamHo() {
        return prefNameExamHo;
    }

    public String getPrefNameExamSh() {
        return prefNameExamSh;
    }

    public String getDistrictCd() {
        return districtCd;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getUnivKanjiName() {
        return univKanjiName;
    }

    public String getUnivKanaName() {
        return univKanaName;
    }

    public String getUnivOtherName() {
        return univOtherName;
    }

    public int getPubRejectExScore() {
        return pubRejectExScore;
    }

    public String getKanaNum() {
        return kanaNum;
    }

    public String getFacultyConCd() {
        return facultyConCd;
    }

    public String getDeptSerialNo() {
        return deptSerialNo;
    }

    public String getScheduleSys() {
        return scheduleSys;
    }

    public String getScheduleSysBranchCd() {
        return scheduleSysBranchCd;
    }

    public String getExamCapaRel() {
        return examCapaRel;
    }

    public String getBorderInDiv() {
        return borderInDiv;
    }

    public int getBorderScoreNoExam1() {
        return borderScoreNoExam1;
    }

    public int getBorderScoreNoExam2() {
        return borderScoreNoExam2;
    }

    public int getBorderScoreNoExam3() {
        return borderScoreNoExam3;
    }

    public double getBorderScoreRateNoExam1() {
        return borderScoreRateNoExam1;
    }

    public double getBorderScoreRateNoExam2() {
        return borderScoreRateNoExam2;
    }

    public double getBorderScoreRateNoExam3() {
        return borderScoreRateNoExam3;
    }

    public double getEntExamFullPntCenter() {
        return entExamFullPntCenter;
    }

    public double getTotalAllotPntCenter() {
        return totalAllotPntCenter;
    }

    public double getEntExamFullPntCenterNoExam() {
        return entExamFullPntCenterNoExam;
    }

    public double getTotalAllotPntCenterNoExam() {
        return totalAllotPntCenterNoExam;
    }

    public String getSettingPattern() {
        return settingPattern;
    }

    public String getAppReqPatternCd() {
        return appReqPatternCd;
    }

    public String getEngPtBorderUseFlg() {
        return engPtBorderUseFlg;
    }

    public String getEngPtRepBranch() {
        return engPtRepBranch;
    }

}

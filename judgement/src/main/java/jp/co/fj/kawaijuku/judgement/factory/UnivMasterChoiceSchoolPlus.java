package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * ��w�}�X�^�͎��p�u�]�Z�f�[�^�N���X
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterChoiceSchoolPlus {

    //�����z�_
    private final int totalAllotPnt;

    //�����z�_�Q���_���Ȃ�
    private final int totalAllotPntNRep;

    //�������_�l
    private final int entExamFullPnt;

    //�������_�l�Q���_���Ȃ�
    private final String entExfamFullPntNRep;

    //�C���M�����z�_1
    private final double irrAllotPnt1;

    //�ő�I���Ȗڐ��|�p��1
    private final int maxSelectEn1;

    //�ő�I���Ȗڐ��|���w1
    private final int maxSelectMa1;

    //�ő�I���Ȗڐ��|����1
    private final int maxSelectJa1;

    //�ő�I���Ȗڐ��|����1
    private final int maxSelectSc1;

    //�ő�I���Ȗڐ��|�Љ�1
    private final int maxSelectSo1;

    //�C���M�����Ȗڐ�1
    private final int irrSub1;

    //�C���M�����z�_2
    private final double irrAllotPnt2;

    //�ő�I���Ȗڐ��|�p��2
    private final int maxSelectEn2;

    //�ő�I���Ȗڐ��|���w2
    private final int maxSelectMa2;

    //�ő�I���Ȗڐ��|����2
    private final int maxSelectJa2;

    //�ő�I���Ȗڐ��|����2
    private final int maxSelectSc2;

    //�ő�I���Ȗڐ��|�Љ�2
    private final int maxSelectSo2;

    //�C���M�����Ȗڐ�2
    private final int irrSub2;

    //�C���M�����z�_3
    private final double irrAllotPnt3;

    //�ő�I���Ȗڐ��|�p��3
    private final int maxSelectEn3;

    //�ő�I���Ȗڐ��|���w3
    private final int maxSelectMa3;

    //�ő�I���Ȗڐ��|����3
    private final int maxSelectJa3;

    //�ő�I���Ȗڐ��|����3
    private final int maxSelectSc3;

    //�ő�I���Ȗڐ��|�Љ�3
    private final int maxSelectSo3;

    //�C���M�����Ȗڐ�3
    private final int irrSub3;

    //�C���M�����z�_4
    private final double irrAllotPnt4;

    //�ő�I���Ȗڐ��|�p��4
    private final int maxSelectEn4;

    //�ő�I���Ȗڐ��|���w4
    private final int maxSelectMa4;

    //�ő�I���Ȗڐ��|����4
    private final int maxSelectJa4;

    //�ő�I���Ȗڐ��|����4
    private final int maxSelectSc4;

    //�ő�I���Ȗڐ��|�Љ�4
    private final int maxSelectSo4;

    //�C���M�����Ȗڐ�4
    private final int irrSub4;

    //�C���M�����z�_5
    private final double irrAllotPnt5;

    //�ő�I���Ȗڐ��|�p��5
    private final int maxSelectEn5;

    //�ő�I���Ȗڐ��|���w5
    private final int maxSelectMa5;

    //�ő�I���Ȗڐ��|����5
    private final int maxSelectJa5;

    //�ő�I���Ȗڐ��|����5
    private final int maxSelectSc5;

    //�ő�I���Ȗڐ��|�Љ�5
    private final int maxSelectSo5;

    //�C���M�����Ȗڐ�5
    private final int irrSub5;

    //�C���M�����z�_6
    private final double irrAllotPnt6;

    //�ő�I���Ȗڐ��|�p��6
    private final int maxSelectEn6;

    //�ő�I���Ȗڐ��|���w6
    private final int maxSelectMa6;

    //�ő�I���Ȗڐ��|����6
    private final int maxSelectJa6;

    //�ő�I���Ȗڐ��|����6
    private final int maxSelectSc6;

    //�ő�I���Ȗڐ��|�Љ�6
    private final int maxSelectSo6;

    //�C���M�����Ȗڐ�6
    private final int irrSub6;

    //����Ȗږ�
    private final String subCount;

    //�K�C�h���C���Ȗڕ�����
    private final String guideSubString;

    //���𓚉Ȗڃt���O�F����
    private final int firstAnsScFlg;

    //���𓚉Ȗڃt���O�F�Љ�
    private final int firstAnsSoFlg;

    //�D��}��
    private final int priorityBranchCd;

    //���ȓ���ȖڑI��s��
    private final String samechoiceNgSc;

    //���_���Z�p�^�[���R�[�h�|�p��F�莎��:CENTERCVSSCORE_ENGPT
    private final String centerCvsScoreEngPt;

    //���_�敪�|�p��F�莎��:ADDPTDIV_ENGPT
    private final String addPtDivEngPt;

    //���_�z�_�|�p��F�莎��:ADDPT_ENGPT
    private final int addPtEngPt;

    //���_���Z�p�^�[���R�[�h�|����L�q��:CENTERCVSSCORE_KOKUGO_DESC
    private final String centerCvsScoreKokugoDesc;

    //���_�敪�|����L�q��:ADDPTDIV_KOKUGO_DESC
    private final String addPtDivKokugoDesc;

    //���_�z�_�|����L�q��:ADDPT_KOKUGO_DESC
    private final int addPtKokugoDesc;

    //�őP�}�ԃ\�[�g�p���_���Z���p�敪:BESTBRAN_SORT_CVSSCORE_USEDIV
    private final String bestBranSortCvsScoreUseDiv;

    /**
     * �R���X�g���N�^�ł��B
     * @param totalAllotPnt
     * @param totalAllotPntNRep
     * @param entExamFullPnt
     * @param entExfamFullPntNRep
     * @param irrAllotPnt1
     * @param maxSelectEn1
     * @param maxSelectMa1
     * @param maxSelectJa1
     * @param maxSelectSc1
     * @param maxSelectSo1
     * @param irrSub1
     * @param irrAllotPnt2
     * @param maxSelectEn2
     * @param maxSelectMa2
     * @param maxSelectJa2
     * @param maxSelectSc2
     * @param maxSelectSo2
     * @param irrSub2
     * @param irrAllotPnt3
     * @param maxSelectEn3
     * @param maxSelectMa3
     * @param maxSelectJa3
     * @param maxSelectSc3
     * @param maxSelectSo3
     * @param irrSub3
     * @param irrAllotPnt4
     * @param maxSelectEn4
     * @param maxSelectMa4
     * @param maxSelectJa4
     * @param maxSelectSc4
     * @param maxSelectSo4
     * @param irrSub4
     * @param irrAllotPnt5
     * @param maxSelectEn5
     * @param maxSelectMa5
     * @param maxSelectJa5
     * @param maxSelectSc5
     * @param maxSelectSo5
     * @param irrSub5
     * @param irrAllotPnt6
     * @param maxSelectEn6
     * @param maxSelectMa6
     * @param maxSelectJa6
     * @param maxSelectSc6
     * @param maxSelectSo6
     * @param irrSub6
     * @param subCount
     * @param guideSubString
     * @param firstAnsScFlg
     * @param firstAnsSoFlg
     * @param priorityBranchCd
     * @param samechoiceNgSc
     * @param centerCvsScoreEngPt
     * @param addPtDivEngPt
     * @param addPtEngPt
     * @param centerCvsScoreKokugoDesc
     * @param addPtDivKokugoDesc
     * @param addPtKokugoDesc
     * @param bestBranSortCvsScoreUseDiv
     */
    public UnivMasterChoiceSchoolPlus(int totalAllotPnt, int totalAllotPntNRep, int entExamFullPnt, String entExfamFullPntNRep, double irrAllotPnt1, int maxSelectEn1, int maxSelectMa1, int maxSelectJa1, int maxSelectSc1, int maxSelectSo1,
            int irrSub1, double irrAllotPnt2, int maxSelectEn2, int maxSelectMa2, int maxSelectJa2, int maxSelectSc2, int maxSelectSo2, int irrSub2, double irrAllotPnt3, int maxSelectEn3, int maxSelectMa3, int maxSelectJa3,
            int maxSelectSc3, int maxSelectSo3, int irrSub3, double irrAllotPnt4, int maxSelectEn4, int maxSelectMa4, int maxSelectJa4, int maxSelectSc4, int maxSelectSo4, int irrSub4, double irrAllotPnt5, int maxSelectEn5,
            int maxSelectMa5, int maxSelectJa5, int maxSelectSc5, int maxSelectSo5, int irrSub5, double irrAllotPnt6, int maxSelectEn6, int maxSelectMa6, int maxSelectJa6, int maxSelectSc6, int maxSelectSo6, int irrSub6, String subCount,
            String guideSubString, int answerNoFlagSc, int answerNoFlagSo, int priorityBranchCd, String samechoiceNgSc, String centerCvsScoreEngPt, String addPtDivEngPt, int addPtEngPt, String centerCvsScoreKokugoDesc,
            String addPtDivKokugoDesc, int addPtKokugoDesc, String bestBranSortCvsScoreUseDiv) {
        super();
        this.totalAllotPnt = totalAllotPnt;
        this.totalAllotPntNRep = totalAllotPntNRep;
        this.entExamFullPnt = entExamFullPnt;
        this.entExfamFullPntNRep = entExfamFullPntNRep;
        this.irrAllotPnt1 = irrAllotPnt1;
        this.maxSelectEn1 = maxSelectEn1;
        this.maxSelectMa1 = maxSelectMa1;
        this.maxSelectJa1 = maxSelectJa1;
        this.maxSelectSc1 = maxSelectSc1;
        this.maxSelectSo1 = maxSelectSo1;
        this.irrSub1 = irrSub1;
        this.irrAllotPnt2 = irrAllotPnt2;
        this.maxSelectEn2 = maxSelectEn2;
        this.maxSelectMa2 = maxSelectMa2;
        this.maxSelectJa2 = maxSelectJa2;
        this.maxSelectSc2 = maxSelectSc2;
        this.maxSelectSo2 = maxSelectSo2;
        this.irrSub2 = irrSub2;
        this.irrAllotPnt3 = irrAllotPnt3;
        this.maxSelectEn3 = maxSelectEn3;
        this.maxSelectMa3 = maxSelectMa3;
        this.maxSelectJa3 = maxSelectJa3;
        this.maxSelectSc3 = maxSelectSc3;
        this.maxSelectSo3 = maxSelectSo3;
        this.irrSub3 = irrSub3;
        this.irrAllotPnt4 = irrAllotPnt4;
        this.maxSelectEn4 = maxSelectEn4;
        this.maxSelectMa4 = maxSelectMa4;
        this.maxSelectJa4 = maxSelectJa4;
        this.maxSelectSc4 = maxSelectSc4;
        this.maxSelectSo4 = maxSelectSo4;
        this.irrSub4 = irrSub4;
        this.irrAllotPnt5 = irrAllotPnt5;
        this.maxSelectEn5 = maxSelectEn5;
        this.maxSelectMa5 = maxSelectMa5;
        this.maxSelectJa5 = maxSelectJa5;
        this.maxSelectSc5 = maxSelectSc5;
        this.maxSelectSo5 = maxSelectSo5;
        this.irrSub5 = irrSub5;
        this.irrAllotPnt6 = irrAllotPnt6;
        this.maxSelectEn6 = maxSelectEn6;
        this.maxSelectMa6 = maxSelectMa6;
        this.maxSelectJa6 = maxSelectJa6;
        this.maxSelectSc6 = maxSelectSc6;
        this.maxSelectSo6 = maxSelectSo6;
        this.irrSub6 = irrSub6;
        this.subCount = subCount;
        this.guideSubString = guideSubString;
        this.firstAnsScFlg = answerNoFlagSc;
        this.firstAnsSoFlg = answerNoFlagSo;
        this.priorityBranchCd = priorityBranchCd;
        this.samechoiceNgSc = samechoiceNgSc;
        this.centerCvsScoreEngPt = centerCvsScoreEngPt;
        this.addPtDivEngPt = addPtDivEngPt;
        this.addPtEngPt = addPtEngPt;
        this.centerCvsScoreKokugoDesc = centerCvsScoreKokugoDesc;
        this.addPtDivKokugoDesc = addPtDivKokugoDesc;
        this.addPtKokugoDesc = addPtKokugoDesc;
        this.bestBranSortCvsScoreUseDiv = bestBranSortCvsScoreUseDiv;
    }

    public int getTotalAllotPnt() {
        return totalAllotPnt;
    }

    public int getTotalAllotPntNRep() {
        return totalAllotPntNRep;
    }

    public int getEntExamFullPnt() {
        return entExamFullPnt;
    }

    public String getEntExfamFullPntNRep() {
        return entExfamFullPntNRep;
    }

    public double getIrrAllotPnt1() {
        return irrAllotPnt1;
    }

    public int getMaxSelectEn1() {
        return maxSelectEn1;
    }

    public int getMaxSelectMa1() {
        return maxSelectMa1;
    }

    public int getMaxSelectJa1() {
        return maxSelectJa1;
    }

    public int getMaxSelectSc1() {
        return maxSelectSc1;
    }

    public int getMaxSelectSo1() {
        return maxSelectSo1;
    }

    public int getIrrSub1() {
        return irrSub1;
    }

    public double getIrrAllotPnt2() {
        return irrAllotPnt2;
    }

    public int getMaxSelectEn2() {
        return maxSelectEn2;
    }

    public int getMaxSelectMa2() {
        return maxSelectMa2;
    }

    public int getMaxSelectJa2() {
        return maxSelectJa2;
    }

    public int getMaxSelectSc2() {
        return maxSelectSc2;
    }

    public int getMaxSelectSo2() {
        return maxSelectSo2;
    }

    public int getIrrSub2() {
        return irrSub2;
    }

    public double getIrrAllotPnt3() {
        return irrAllotPnt3;
    }

    public int getMaxSelectEn3() {
        return maxSelectEn3;
    }

    public int getMaxSelectMa3() {
        return maxSelectMa3;
    }

    public int getMaxSelectJa3() {
        return maxSelectJa3;
    }

    public int getMaxSelectSc3() {
        return maxSelectSc3;
    }

    public int getMaxSelectSo3() {
        return maxSelectSo3;
    }

    public int getIrrSub3() {
        return irrSub3;
    }

    public double getIrrAllotPnt4() {
        return irrAllotPnt4;
    }

    public int getMaxSelectEn4() {
        return maxSelectEn4;
    }

    public int getMaxSelectMa4() {
        return maxSelectMa4;
    }

    public int getMaxSelectJa4() {
        return maxSelectJa4;
    }

    public int getMaxSelectSc4() {
        return maxSelectSc4;
    }

    public int getMaxSelectSo4() {
        return maxSelectSo4;
    }

    public int getIrrSub4() {
        return irrSub4;
    }

    public double getIrrAllotPnt5() {
        return irrAllotPnt5;
    }

    public int getMaxSelectEn5() {
        return maxSelectEn5;
    }

    public int getMaxSelectMa5() {
        return maxSelectMa5;
    }

    public int getMaxSelectJa5() {
        return maxSelectJa5;
    }

    public int getMaxSelectSc5() {
        return maxSelectSc5;
    }

    public int getMaxSelectSo5() {
        return maxSelectSo5;
    }

    public int getIrrSub5() {
        return irrSub5;
    }

    public double getIrrAllotPnt6() {
        return irrAllotPnt6;
    }

    public int getMaxSelectEn6() {
        return maxSelectEn6;
    }

    public int getMaxSelectMa6() {
        return maxSelectMa6;
    }

    public int getMaxSelectJa6() {
        return maxSelectJa6;
    }

    public int getMaxSelectSc6() {
        return maxSelectSc6;
    }

    public int getMaxSelectSo6() {
        return maxSelectSo6;
    }

    public int getIrrSub6() {
        return irrSub6;
    }

    public String getSubCount() {
        return subCount;
    }

    public String getGuideSubString() {
        return guideSubString;
    }

    public int getFirstAnsScFlg() {
        return firstAnsScFlg;
    }

    public int getFirstAnsSoFlg() {
        return firstAnsSoFlg;
    }

    public int getPriorityBranchCd() {
        return priorityBranchCd;
    }

    public String getSamechoiceNgSc() {
        return samechoiceNgSc;
    }

    public String getCenterCvsScoreEngPt() {
        return centerCvsScoreEngPt;
    }

    public String getAddPtDivEngPt() {
        return addPtDivEngPt;
    }

    public int getAddPtEngPt() {
        return addPtEngPt;
    }

    public String getCenterCvsScoreKokugoDesc() {
        return centerCvsScoreKokugoDesc;
    }

    public String getAddPtDivKokugoDesc() {
        return addPtDivKokugoDesc;
    }

    public int getAddPtKokugoDesc() {
        return addPtKokugoDesc;
    }

    public String getBestBranSortCvsScoreUseDiv() {
        return bestBranSortCvsScoreUseDiv;
    }

}

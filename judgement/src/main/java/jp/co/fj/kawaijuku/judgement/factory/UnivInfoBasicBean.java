package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 情報誌用科目1の情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoBasicBean {

    /** 結合キー */
    private final String combiKey;

    /** 戸籍10桁コード */
    private final String univ10Cd;

    /** 枝番名称 */
    private final String branchName;

    /**
     * コンストラクタです。
     *
     * @param combiKey 結合キー
     * @param univ10Cd 戸籍10桁コード
     * @param branchName 枝番名称
     */
    public UnivInfoBasicBean(String combiKey, String univ10Cd, String branchName) {
        this.combiKey = combiKey;
        this.univ10Cd = univ10Cd;
        this.branchName = branchName;
    }

    /**
     * 結合キーを返します。
     *
     * @return 結合キー
     */
    public String getCombiKey() {
        return combiKey;
    }

    /**
     * 戸籍10桁コードを返します。
     *
     * @return 戸籍10桁コード
     */
    public String getUniv10Cd() {
        return univ10Cd;
    }

    /**
     * 枝番名称を返します。
     *
     * @return 枝番名称
     */
    public String getBranchName() {
        return branchName;
    }

}

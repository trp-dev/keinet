package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 情報誌用科目のその他科目情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoOtherBean {

    /** 表記 */
    private final String signage;

    /** マーク */
    private final String mark;

    /** 配点信頼 */
    private final String trustPt;

    /** 配点 */
    private final String pt;

    /** 必選 */
    private final String hissu;

    /**
     * コンストラクタです。
     *
     * @param signage 表記
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    UnivInfoOtherBean(String signage, String mark, String trustPt, String pt, String hissu) {
        this.signage = signage;
        this.mark = mark;
        this.trustPt = trustPt;
        this.pt = pt;
        this.hissu = hissu;
    }

    /**
     * 表記を返します。
     *
     * @return 表記
     */
    public String getSignage() {
        return signage;
    }

    /**
     * マークを返します。
     *
     * @return マーク
     */
    public String getMark() {
        return mark;
    }

    /**
     * 配点信頼を返します。
     *
     * @return 配点信頼
     */
    public String getTrustPt() {
        return trustPt;
    }

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public String getPt() {
        return pt;
    }

    /**
     * 必選を返します。
     *
     * @return 必選
     */
    public String getHissu() {
        return hissu;
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 大学マスタ模試用選択グループ教科データクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterSelectGpCourse {

    //選択グループ教科別選択科目数
    private final int sGPerSubSubCount;

    public UnivMasterSelectGpCourse(int sGPerSubSubCount) {
        this.sGPerSubSubCount = sGPerSubSubCount;
    }

    public int getsGPerSubSubCount() {
        return sGPerSubSubCount;
    }

}

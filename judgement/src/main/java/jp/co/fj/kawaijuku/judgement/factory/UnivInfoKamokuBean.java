package jp.co.fj.kawaijuku.judgement.factory;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * 英語認定試験／国語記述の配点を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Getter
@Setter
public class UnivInfoKamokuBean {

    private String joinKey;

    private String univCd;

    private String cenSubName;

    private String secSubName;

}

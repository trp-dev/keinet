package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 情報誌用科目2の情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoCenterBean extends UnivInfoCommonBean {

    /** センタ７科目型名 */
    private String item7;

    /** 理科第１解答科目フラグ */
    private String rika1ansFlg;

    /** 地公第１解答科目フラグ */
    private String society1ansFlg;

    /** 国語記述配点 */
    private UnivInfoJpnDescBean jpnDesc;

    /**
     * センタ７科目型名を返します。
     *
     * @return センタ７科目型名
     */
    public String getItem7() {
        return item7;
    }

    /**
     * センタ７科目型名をセットします。
     *
     * @param item7 センタ７科目型名
     */
    public void setItem7(String item7) {
        this.item7 = item7;
    }

    /**
     * 理科第１解答科目フラグを返します。
     *
     * @return 理科第１解答科目フラグ
     */
    public String getRika1ansFlg() {
        return rika1ansFlg;
    }

    /**
     * 理科第１解答科目フラグをセットします。
     *
     * @param rika1ansFlg 理科第１解答科目フラグ
     */
    public void setRika1ansFlg(String rika1ansFlg) {
        this.rika1ansFlg = rika1ansFlg;
    }

    /**
     * 地公第１解答科目フラグを返します。
     *
     * @return 地公第１解答科目フラグ
     */
    public String getSociety1ansFlg() {
        return society1ansFlg;
    }

    /**
     * 地公第１解答科目フラグをセットします。
     *
     * @param society1ansFlg 地公第１解答科目フラグ
     */
    public void setSociety1ansFlg(String society1ansFlg) {
        this.society1ansFlg = society1ansFlg;
    }

    /**
     * 国語記述配点を追加します。
     *
     * @param jpnDescRange 記述範囲
     * @param jpnDescTrustPt 配点信頼
     * @param jpnDescPt 配点
     * @param jpnDescAddPt 加点
     * @param jpnDescAddPtType 加点種類
     * @param jpnDescSel 必選
     */
    public void addJpnDesc(String jpnDescRange, String jpnDescTrustPt, String jpnDescPt, String jpnDescAddPt, String jpnDescAddPtType, String jpnDescSel) {
        jpnDesc = new UnivInfoJpnDescBean("記", jpnDescRange, jpnDescTrustPt, jpnDescPt, jpnDescAddPt, jpnDescAddPtType, jpnDescSel);
    }

    /**
     * 国語記述配点を返します。
     *
     * @return 国語記述配点
     */
    public UnivInfoJpnDescBean getJpnDesc() {
        return jpnDesc;
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;

/**
 *
 * 情報誌用科目データマップのファクトリです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoFactory {

    /**
     * 情報誌用科目データマップを生成します。
     *
     * @param container {@link UnivInfoContainer}
     * @return 情報誌用科目データマップ
     */
    public Map create(UnivInfoContainer container) {

        Map dataMap = new HashMap(container.getUnivInfoBasicList().size() * 4 / 3);

        for (Iterator ite = container.getUnivInfoBasicList().iterator(); ite.hasNext();) {

            UnivInfoBasicBean basic = (UnivInfoBasicBean) ite.next();

            SortedMap map = (SortedMap) dataMap.get(basic.getUniv10Cd());
            if (map == null) {
                map = new TreeMap();
                dataMap.put(basic.getUniv10Cd(), map);
            }

            UnivInfoCenterBean center = (UnivInfoCenterBean) container.getUnivInfoCenterMap().get(basic.getCombiKey());
            if (center == null) {
                throw new RuntimeException("情報誌用科目2が見つかりません。結合キー=" + basic.getCombiKey());
            }

            UnivInfoSecondBean second = (UnivInfoSecondBean) container.getUnivInfoSecondMap().get(basic.getCombiKey());
            if (second == null) {
                throw new RuntimeException("情報誌用科目3が見つかりません。結合キー=" + basic.getCombiKey());
            }

            map.put(basic.getCombiKey(), createUnivInfoBean(basic, center, second));
        }

        return dataMap;
    }

    /**
     * {@link UnivInfoBean} を生成します。
     *
     * @param basic {@link UnivInfoBasicBean}
     * @param center {@link UnivInfoCenterBean}
     * @param second {@link UnivInfoSecondBean}
     * @return {@link UnivInfoBean}
     */
    private UnivInfoBean createUnivInfoBean(UnivInfoBasicBean basic, UnivInfoCenterBean center, UnivInfoSecondBean second) {

        UnivInfoBean bean = new UnivInfoBean();

        /* 枝番名称を設定する */
        bean.setBranchName(null2Empty(basic.getBranchName()));

        /* センタ課しフラグを設定する */
        bean.setCExist(!"00".equals(basic.getCombiKey().substring(5, 7)));

        /* センタ教科数を設定する */
        bean.setCenCourseNum("0−0".equals(center.getItem7()) ? "" : null2Empty(center.getItem7()));

        /* センタ試験科目を設定する */
        bean.setC_subString(null2Empty(center.getSubName()));

        /* センタ配点を設定する */
        bean.setAllot1(createCenterAllotString(center));

        /* 理科第1解答科目フラグを設定する*/
        bean.setFirstAnsFlgSci(!"0".equals(center.getRika1ansFlg()));

        /* 地公第1解答科目フラグを設定する*/
        bean.setFirstAnsFlgSoc(!"0".equals(center.getSociety1ansFlg()));

        /* 二次課しフラグを設定する */
        bean.setSExist(!"00".equals(basic.getCombiKey().substring(7, 9)));

        /* 二次試験科目を設定する */
        bean.setS_subString(null2Empty(second.getSubName()));

        /* 二次配点を設定する */
        bean.setAllot2(createSecondAllotString(second));

        /* 共通テスト配点を設定する */
        bean.setCenterAllot(createCenterAllot(center));

        /* 二次配点を設定する */
        bean.setSecondAllot(createSecondAllot(second));

        return bean;
    }

    /**
     * センタ配点を生成します。
     *
     * @param center {@link UnivInfoCenterBean}
     * @return センタ配点
     */
    private String createCenterAllotString(UnivInfoCenterBean center) {
        StringBuffer sb = new StringBuffer();
        /* 総点を追加 */
        appendFullPoint(sb, center);
        /* 教科配点を追加 */
        appendCenerAllot(sb, center);
        return sb.toString();
    }

    /**
     * 二次配点を生成します。
     *
     * @param second {@link UnivInfoSecondBean}
     * @return 二次配点
     */
    private String createSecondAllotString(UnivInfoSecondBean second) {
        StringBuffer sb = new StringBuffer();
        /* 総点を追加 */
        appendFullPoint(sb, second);
        /* 教科配点を追加 */
        appendSecondAllot(sb, second);
        /* その他科目配点を追加 */
        appendOtherAllot(sb, second);
        return sb.toString();
    }

    /**
     * 共通テスト用の教科配点を設定
     *
     * @param bean {@link UnivInfoCenterBean}
     * @return
     */
    private Map<COURSE_ALLOT, String> createCenterAllot(UnivInfoCenterBean bean) {
        Map<COURSE_ALLOT, String> allots = new HashMap();
        String allot;

        for (Iterator ite = bean.getCourseList().iterator(); ite.hasNext();) {
            UnivInfoCourseBean course = (UnivInfoCourseBean) ite.next();
            StringBuffer sb = new StringBuffer();
            /* 教科の配点を設定 */
            appendAllot(sb, bean, course);

            /* PCバンザイ用に文字を加工 */
            allots.put(course.getSubject(), replacePcBanzai(sb.toString()));
        }
        /* 英語認定試験の配点を設定 */
        allot = createAllotExt(bean.getEngCert());
        if (!isEmpty(allot)) {
            allots.put(COURSE_ALLOT.ENGPT, replacePcBanzai(allot));
        }

        /* 国語記述の配点の場合 */
        allot = createAllotExt(bean.getJpnDesc());
        if (!isEmpty(allot)) {
            allots.put(COURSE_ALLOT.JAP_DESC, replacePcBanzai(allot));
        }

        return allots;
    }

    /**
     * 二次用の教科配点を設定
     *
     * @param bean {@link UnivInfoCenterBean}
     * @return
     */
    private Map<COURSE_ALLOT, String> createSecondAllot(UnivInfoSecondBean bean) {
        Map<COURSE_ALLOT, String> allots = new HashMap();

        for (Iterator ite = bean.getCourseList().iterator(); ite.hasNext();) {
            UnivInfoCourseBean course = (UnivInfoCourseBean) ite.next();
            StringBuffer sb = new StringBuffer();
            /* 教科の配点を設定 */
            appendAllot(sb, bean, course);

            /* PCバンザイ用に文字を加工 */
            allots.put(course.getSubject(), replacePcBanzai(sb.toString()));
        }
        /* その他科目の設定 */
        StringBuffer sb = new StringBuffer();
        appendOtherAllot(sb, bean);
        String other = sb.toString();
        if (!isEmpty(other) && other.startsWith("／")) {
            other = other.substring(1);
        }
        /* PCバンザイ用に文字を加工 */
        allots.put(COURSE_ALLOT.ETC, replacePcBanzai(other));

        return allots;
    }

    /**
     * 総点を文字列バッファに追加します。
     *
     * @param sb 文字列バッファ
     * @param bean {@link UnivInfoCommonBean}
     */
    private void appendFullPoint(StringBuffer sb, UnivInfoCommonBean bean) {
        sb.append("総点");
        if (isEmpty(bean.getTotalSc()) || isZero(bean.getTotalSc()) || "8".equals(bean.getScTrust())) {
            sb.append("−");
        } else {
            if ("9".equals(bean.getScTrust())) {
                sb.append("推");
            }
            sb.append(toAllotStr(bean.getTotalSc()));
        }
    }

    /**
     * 共通テストの教科配点を文字列バッファに追加します。
     *
     * @param sb 文字列バッファ
     * @param bean {@link UnivInfoCenterBean}
     */
    private void appendCenerAllot(StringBuffer sb, UnivInfoCenterBean bean) {
        for (Iterator ite = bean.getCourseList().iterator(); ite.hasNext();) {
            UnivInfoCourseBean course = (UnivInfoCourseBean) ite.next();
            /* 教科の配点を設定 */
            sb.append(course.getLabel());
            appendAllot(sb, bean, course);
        }
    }

    /**
     * 二次試験の教科配点を文字列バッファに追加します。
     *
     * @param sb 文字列バッファ
     * @param bean {@link UnivInfoCommonBean}
     */
    private void appendSecondAllot(StringBuffer sb, UnivInfoCommonBean bean) {
        for (Iterator ite = bean.getCourseList().iterator(); ite.hasNext();) {
            UnivInfoCourseBean course = (UnivInfoCourseBean) ite.next();
            sb.append(course.getLabel());
            appendAllot(sb, bean, course);
        }
    }

    private void appendAllot(StringBuffer sb, UnivInfoCommonBean bean, UnivInfoCourseBean course) {
        /* 配点がゼロまたは配点信頼が「8:非表示」の場合 */
        if (isZero(course.getShoPt()) || "8".equals(course.getTrustPt()) || "8".equals(bean.getScTrust())) {
            sb.append("−");
            /* 配点信頼が「7:合算」の場合 */
        } else if ("7".equals(course.getTrustPt())) {
            sb.append("※");
        } else {
            /* 配点信頼が「9:推定」の場合 */
            if ("9".equals(course.getTrustPt())) {
                sb.append("推");
            }
            /* 必選小が「2:選択」の場合 */
            if ("2".endsWith(course.getShoSel())) {
                sb.append("※");
            }

            /* 配点小を設定 */
            sb.append(toAllotStr(course.getShoPt()));

            /* 配点大が設定されている場合 */
            if (!isEmpty(course.getDaiPt())) {
                sb.append("(");
                /* 必選大が「2:選択」の場合 */
                if ("2".endsWith(course.getDaiSel())) {
                    sb.append("※");
                }
                /* 配点大を設定 */
                sb.append(toAllotStr(course.getDaiPt()));
                sb.append(")");
            }
        }
    }

    /**
     * 英語認定試験／国語記述配点を返却します。
     *
     * @param allot {@link UnivInfoAllotBean}
     */
    private String createAllotExt(UnivInfoAllotBean allot) {
        StringBuffer sb = new StringBuffer();
        /* 配点表示対象外の場合 */
        if (!allot.isAllot()) {
            return "";
        }
        if (isEmpty(allot.getPt())) {
            return "";
        }
        /* 配点がゼロかつ配点信頼が「8:非表示」の場合 */
        if (isZero(allot.getPt()) && "8".equals(allot.getTrustPt())) {
            sb.append("−");
        } else if ("7".equals(allot.getTrustPt())) {
            /* 上記の状態以外かつ配点信頼が「7:合算」の場合 */
            sb.append("※");
        } else {
            /* 配点信頼が「9:推定」の場合 */
            if ("9".equals(allot.getTrustPt())) {
                sb.append("推");
            }
            /* 必選が「2:選択」の場合 */
            if ("2".equals(allot.getSel())) {
                sb.append("※");
            }
            if (!"※".equals(sb.toString())) {
                /* 加点区分が設定されている場合 */
                if (!isEmpty(allot.getAddPt())) {
                    sb.append(allot.getAddPt());
                }
            }
            /* 配点を設定 */
            sb.append(toAllotStr(allot.getPt()));
        }
        return sb.toString();
    }

    /**
     * その他科目の配点を文字列バッファに追加します。
     *
     * @param sb 文字列バッファ
     * @param second {@link UnivInfoSecondBean}
     */
    private void appendOtherAllot(StringBuffer sb, UnivInfoSecondBean second) {

        StringBuffer result = new StringBuffer();

        result.append("／");

        if (isEmpty(second.getHoka2Pt())) {
            /* その他配点が未設定の場合 */
            appendOtherAllot(result, second.getScTrust(), second.getOtherList(), second);

        } else {
            /* その他配点が設定されている場合 */

            /* 配点の有無で分ける */
            List blankList = new ArrayList(second.getOtherList().size());
            List notBlankList = new ArrayList(second.getOtherList().size());
            for (Iterator ite = second.getOtherList().iterator(); ite.hasNext();) {
                UnivInfoOtherBean other = (UnivInfoOtherBean) ite.next();
                /* 配点が設定されてない場合 */
                if (isEmpty(other.getPt())) {
                    /* マークが設定されている場合 */
                    if ("●".equals(other.getMark())) {
                        blankList.add(other);
                    }
                } else {
                    /* 配点が設定されている場合 */
                    notBlankList.add(other);
                }
            }

            /* 配点がある科目情報を追加する */
            appendOtherAllot(result, second.getScTrust(), notBlankList, second);

            /* 配点がない科目情報を追加する */
            if (!notBlankList.isEmpty() && !blankList.isEmpty()) {
                result.append("／");
            }
            UnivInfoEngCertBean eng = second.getEngCert();
            for (int i = 0; i < blankList.size(); i++) {
                UnivInfoOtherBean other = (UnivInfoOtherBean) blankList.get(i);
                if (i == 0) {
                    if (isEmpty(eng.getPt()) && "●".equals(eng.getMark())) {
                        result.append(eng.getLabel()).append("・");
                    }
                } else {
                    result.append("・");
                }
                result.append(other.getSignage());
            }
            if (isZero(second.getHoka2Pt()) || "8".equals(second.getHoka2TrustPt()) || "8".equals(second.getScTrust())) {
                result.append("−");
            } else if ("7".equals(second.getHoka2TrustPt())) {
                result.append("※");
            } else {
                if ("9".equals(second.getHoka2TrustPt())) {
                    result.append("推");
                }
                result.append(toAllotStr(second.getHoka2Pt()));
            }
        }

        if (!"／".equals(result.toString())) {
            sb.append(result.toString());
        }
    }

    /**
     * その他科目のリストの内容を文字列バッファに追加します。
     *
     * @param sb 文字列バッファ
     * @param scTrust 二次配点信頼
     * @param list リスト
     */
    private void appendOtherAllot(StringBuffer sb, String scTrust, List list, UnivInfoSecondBean second) {
        String engPtAllot = createAllotExt(second.getEngCert());
        if (!isEmpty(engPtAllot)) {
            sb.append(second.getEngCert().getLabel()).append(engPtAllot);
        }
        for (int i = 0; i < list.size(); i++) {
            UnivInfoOtherBean other = (UnivInfoOtherBean) list.get(i);
            if (i > 0 || !isEmpty(engPtAllot)) {
                sb.append("／");
            }
            sb.append(other.getSignage());
            if (isZero(other.getPt()) || "8".equals(other.getTrustPt()) || "8".equals(scTrust)) {
                sb.append("−");
            } else if ("7".equals(other.getTrustPt())) {
                sb.append("※");
            } else {
                if ("9".equals(other.getTrustPt())) {
                    sb.append("推");
                }
                if ("2".equals(other.getHissu())) {
                    sb.append("※");
                }
                sb.append(toAllotStr(other.getPt()));
            }
        }
    }

    /**
     * PCバンザイ用に文字を置き換えます。
     *
     * @param allot 教科配点
     * @return
     */
    private String replacePcBanzai(String allot) {
        return (allot == null) ? "" : allot.replace("−", "-").replace("※", "*").replace("／", "/");
    }

    /**
     * 文字列が空かNULLであるかを判断します。
     *
     * @param string 文字列
     * @return 文字列が空かNULLならtrue
     */
    private boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    /**
     * 文字列が数値のゼロであるかを判断します。
     *
     * @param string 文字列
     * @return 文字列が数値のゼロならtrue
     */
    private boolean isZero(String string) {
        return Double.parseDouble(string) == 0;
    }

    /**
     * NULL文字列を空文字列に変換します。
     *
     * @param string 変換する文字列
     * @return 変換した文字列
     */
    private String null2Empty(String string) {
        return string == null ? "" : string;
    }

    /**
     * 文字列を配点文字列に変換します。（例：100.0→100）
     *
     * @param string 変換する文字列
     * @return 変換した文字列
     */
    private String toAllotStr(String string) {
        return string.endsWith(".0") ? string.substring(0, string.length() - 2) : string;
    }

}

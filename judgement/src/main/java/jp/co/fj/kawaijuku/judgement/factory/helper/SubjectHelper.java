package jp.co.fj.kawaijuku.judgement.factory.helper;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

/**
 *
 * 課し科目ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SubjectHelper {

    public static <T extends Object> void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject, SubjectKey subjectKey, Class<T> clazz) {
        if (subject != null && subject.isImposed()) {
            if (subject.isRequired()) {
                setRequired(detailHolder, subject, subjectKey);
            } else if (subject.isAny()) {
                setAny(detailHolder, subject, subjectKey);
            } else {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectKey, clazz);
                setElective(detailHolder, subject, newSubjectKey);
            }
        }
    }

    public final static void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject, int rangeUpper, SubjectKey subjectKeyUpper, SubjectKey subjectKeyLower) {

        if (subject != null) {
            SubjectKey subjectKey = subjectKeyLower;
            if (Integer.parseInt(subject.getSubArea()) == rangeUpper) {
                subjectKey = subjectKeyUpper;
            }
            SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectKey, SecondDetail.class);
            prepare(detailHolder, subject, newSubjectKey, SecondDetail.class);
        }
    }

    public final static void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject, SecondSubjectRange subjectRange) {

        if (subject != null) {
            int range = Integer.parseInt(subject.getSubArea());
            if (range == subjectRange.rangeUpper()) {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectRange.subjectKeyUpper(), SecondDetail.class);
                prepare(detailHolder, subject, newSubjectKey, SecondDetail.class);
            } else if (range == subjectRange.rangeLower()) {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectRange.subjectKeyLower(), SecondDetail.class);
                prepare(detailHolder, subject, newSubjectKey, SecondDetail.class);
            } else {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectRange.subjectKeyUnknown(), SecondDetail.class);
                prepare(detailHolder, subject, newSubjectKey, SecondDetail.class);
            }
        }
    }

    static void setRequired(CommonDetailHolder detailHolder, UnivMasterSubject ums_subject, SubjectKey subjectKey) {
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(subjectKey);
        if (!imposingSubject.getRequired().isImposed()) {
            /* 必須情報が未設定の場合のみ設定する */
            detailHolder.getImposingSubjects().put(subjectKey, imposingSubject.validateRequired(subjectKey.getCourseIndex(), ums_subject.getSubBit().trim(), ums_subject.getSubPnt(), JudgementConstants.Score.DETAIL_SCOPE_NA));
        }
    }

    static void setElective(CommonDetailHolder detailHolder, UnivMasterSubject ums_subject, SubjectKey subjectKey) {
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(subjectKey);
        if (!imposingSubject.getElective().isImposed()) {
            /* 選択情報が未設定の場合のみ設定する */
            detailHolder.getImposingSubjects().put(subjectKey,
                    imposingSubject.validateElective(subjectKey.getCourseIndex(), ums_subject.getSubBit().trim(), ums_subject.getSubPnt(), ums_subject.getConcurrentNg(), JudgementConstants.Score.DETAIL_SCOPE_NA));
        }
    }

    static void setAny(CommonDetailHolder detailHolder, UnivMasterSubject ums_subject, SubjectKey subjectKey) {
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(subjectKey);
        if (!imposingSubject.getAny().isImposed()) {
            /* 任意情報が未設定の場合のみ設定する */
            detailHolder.getImposingSubjects().put(subjectKey, imposingSubject.validateAny(subjectKey.getCourseIndex(), ums_subject.getSubBit().trim(), ums_subject.getSubPnt(), JudgementConstants.Score.DETAIL_SCOPE_NA));
        }
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 英語認定試験／国語記述の配点を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface UnivInfoAllotBean {

    /**
     * ラベルを返します。
     *
     * @return ラベル
     */
    public String getLabel();

    /**
     * 判定フラグを返します。
     *
     * @return 共通T英語認定試験
     */
    public boolean isAllot();

    /**
     * 配点信頼を返します。
     *
     * @return 配点信頼
     */
    public String getTrustPt();

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public String getPt();

    /**
     * 加点を返します。
     *
     * @return 加点
     */
    public String getAddPt();

    /**
     * 加点種類を返します。
     *
     * @return 加点種類
     */
    public String getAddPtType();

    /**
     * 必選を返します。
     *
     * @return 必選
     */
    public String getSel();

}

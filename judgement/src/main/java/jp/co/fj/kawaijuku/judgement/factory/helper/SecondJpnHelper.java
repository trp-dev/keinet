package jp.co.fj.kawaijuku.judgement.factory.helper;

import java.util.Arrays;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

/**
 *
 * 二次国語用課しヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SecondJpnHelper extends SubjectHelper {

    public static void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject) {
        if (subject != null) {
            SubjectKey subjectKey;
            if (Arrays.binarySearch(JudgementConstants.Detail.Second.Japanese.RANGES_COMTEMPORARY, subject.getSubArea()) >= 0) {
                //現
                subjectKey = SecondDetail.KEY_COMTEMPORARY;
            } else if (Arrays.binarySearch(JudgementConstants.Detail.Second.Japanese.RANGES_ANCIENT, subject.getSubArea()) >= 0) {
                //現古
                subjectKey = SecondDetail.KEY_ANCIENT;
            } else if (Arrays.binarySearch(JudgementConstants.Detail.Second.Japanese.RANGES_CHINESE, subject.getSubArea()) >= 0) {
                //現古漢
                subjectKey = SecondDetail.KEY_CHINESE;
            } else {
                return;
            }
            if (subject.isRequired()) {
                setRequired(detailHolder, subject, subjectKey);
            } else {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectKey, SecondDetail.class);
                setElective(detailHolder, subject, newSubjectKey);
            }
        }
    }

}

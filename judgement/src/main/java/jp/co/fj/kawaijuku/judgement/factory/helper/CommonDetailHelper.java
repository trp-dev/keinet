package jp.co.fj.kawaijuku.judgement.factory.helper;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterChoiceSchoolPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGpCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGroup;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;

/**
 *
 * 共通詳細情報作成ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
abstract public class CommonDetailHelper {

    /**
     * {@link CommonDetail} を生成します。
     *
     * @return {@link CommonDetail}
     */
    abstract protected CommonDetail createCommonDetail();

    /**
     * 課し科目を設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     * @param map 大学マスタ科目マップ
     */
    abstract protected void applySubjects(CommonDetailHolder detailHolder, Map map);

    /**
     * イレギュラ�@を生成します。
     *
     * @param irregular 配点
     * @param maxSelect 教科別最大科目数
     * @param irrSub 科目数
     */
    abstract protected Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub);

    /**
     * 詳細データを生成します。
     *
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @param umcs 大学マスタ志望校
     * @param umc_branch_map 大学マスタ教科枝番別マップ
     * @param ums_branch_map 大学マスタ選択グループ枝番別マップ
     * @param umsg_branch_map 大学マスタ選択グループ教科枝番別マップ
     * @param umsgc_branch_map 大学マスタ科目枝番別マップ
     * @return {@link CommonDetail}
     */
    public CommonDetail create(String branchCd, String entExamDiv, UnivMasterChoiceSchoolPlus umcs, Map umc_branch_map, Map ums_branch_map, Map umsg_branch_map, Map umsgc_branch_map) {

        Map umc_course_map = getEntExamDivMap(umc_branch_map, branchCd, entExamDiv);
        Map umsg_group_map = getEntExamDivMap(umsg_branch_map, branchCd, entExamDiv);
        Map umsgc_gcourse_map = getEntExamDivMap(umsgc_branch_map, branchCd, entExamDiv);
        Map ums_subject_map = getEntExamDivMap(ums_branch_map, branchCd, entExamDiv);

        return create(branchCd, umcs, umc_course_map, umsg_group_map, umsgc_gcourse_map, ums_subject_map);
    }

    /**
     * 詳細データを生成します。
     *
     * @param branchCd 枝番
     * @param umcs 大学マスタ志望校
     * @param umc_course_map 大学マスタ教科マップ
     * @param umsg_group_map 大学マスタ選択グループマップ
     * @param umsgc_gcourse_map 大学マスタ選択グループ教科マップ
     * @param ums_subject_map 大学マスタ科目マップ
     * @return {@link CommonDetail}
     */
    private CommonDetail create(String branchCd, UnivMasterChoiceSchoolPlus umcs, Map umc_course_map, Map umsg_group_map, Map umsgc_gcourse_map, Map ums_subject_map) {

        CommonDetail detail = createCommonDetail();

        CommonDetailHolder detailHolder = detail.getForJudgement();

        if (umc_course_map != null) {
            /* 大学マスタ教科の情報を設定する */
            setCourse(detail, detailHolder, umc_course_map);
        }

        if (umsg_group_map != null) {
            /* 大学マスタ選択グループの情報を設定する */
            setSelectGroup(detailHolder, umsg_group_map);
        }

        if (umsgc_gcourse_map != null) {
            /* 大学マスタ選択グループ教科の情報を設定する */
            setSelectGroupCourse(detailHolder, umsgc_gcourse_map);
        }

        if (ums_subject_map != null) {
            /* 課し科目を設定する */
            applySubjects(detailHolder, ums_subject_map);
        }

        /* ガイド文字列を設定する */
        detail.setGuideSubString(umcs.getGuideSubString());

        /* 枝番を設定する */
        detail.setBranchCode(branchCd);

        /* 配点比（その他含まない）を設定する */
        detail.setAllotPntRate(JudgementUtil.check9(umcs.getTotalAllotPntNRep()));

        /* 満点値を設定する */
        detail.setFullPnt(JudgementUtil.check9(umcs.getEntExamFullPnt()));

        /* 科目文字列を設定する */
        detail.setSubString(umcs.getSubCount());

        /* 配点比（その他含む）を設定する */
        detail.setAllotPntRateAll(JudgementUtil.check9(umcs.getTotalAllotPnt()));

        /* イレギュラ�@を設定する */
        setIrregular1(detail, umcs);

        /* 第一解答科目フラグを設定する */
        setFirstAnsFlg(detailHolder, umcs);

        /* 優先枝番を設定する */
        detail.setPriorityBranchCd(umcs.getPriorityBranchCd());

        /* 理科同一科目選択不可を設定する */
        detailHolder.setSamechoiceNgSc(umcs.getSamechoiceNgSc());

        /* 得点換算パターンコード−英語認定試験 */
        detailHolder.setTkPatternCodeEigoSs(umcs.getCenterCvsScoreEngPt());

        /* 加点区分−英語参加試験 */
        detailHolder.setEngKatenKubun(umcs.getAddPtDivEngPt());

        /* 加点配点−英語参加試験 */
        detailHolder.setEngKatenHaiten(umcs.getAddPtEngPt());

        /* 得点換算パターンコード−国語記述式 */
        detailHolder.setTkPatternCodeKokugoKjs(umcs.getCenterCvsScoreKokugoDesc());

        /* 加点区分−国語記述式 */
        detailHolder.setJpnKatenKubun(umcs.getAddPtDivKokugoDesc());

        /* 加点配点−国語記述式 */
        detailHolder.setJpnKatenHaiten(umcs.getAddPtKokugoDesc());

        /* 最善枝番ソート用得点換算利用区分 */
        detailHolder.setBestBranchNoSorttkKansanKbn(Double.parseDouble(StringUtil.nvl(umcs.getBestBranSortCvsScoreUseDiv(), "0")));

        /* setupを実行する */
        detail.setup();

        return detail;
    }

    /**
     * マップから入試試験区分別マップを取得します。
     *
     * @param map マップ
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @return 入試試験区分別マップ
     */
    private Map getEntExamDivMap(Map map, String branchCd, String entExamDiv) {
        if (map == null) {
            return null;
        } else {
            Map branchMap = (Map) map.get(branchCd);
            return (Map) (branchMap == null ? null : branchMap.get(entExamDiv));
        }
    }

    /**
     * 大学マスタ教科の情報を設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     * @param umc_course_map 大学マスタ教科マップ
     */
    protected void setCourse(CommonDetail detail, CommonDetailHolder detailHolder, Map umc_course_map) {
        for (Iterator ite = umc_course_map.entrySet().iterator(); ite.hasNext();) {
            Entry entry = (Entry) ite.next();
            String courseCd = (String) entry.getKey();
            UnivMasterCourse course = (UnivMasterCourse) entry.getValue();
            /* その他教科以外を処理する */
            if (!"9".equals(courseCd)) {
                /* 必須科目の科目数と配点を設定する */
                detailHolder.setRequiredCourse(toIndex(courseCd), course.getNecesSubCount(), course.getNecesAllotPnt());
                /* 同一科目制限フラグを設定する */
                if (course.getCenSecSubChoiceDiv() == 1) {
                    detail.setUnadoptables(toIndex(courseCd));
                }
            }
        }
    }

    /**
     * 大学マスタ選択グループの情報を設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     * @param umsg_group_map 大学マスタ選択グループマップ
     */
    private void setSelectGroup(CommonDetailHolder detailHolder, Map umsg_group_map) {
        for (Iterator ite = umsg_group_map.entrySet().iterator(); ite.hasNext();) {
            Entry entry = (Entry) ite.next();
            String selectGNo = (String) entry.getKey();
            UnivMasterSelectGroup group = (UnivMasterSelectGroup) entry.getValue();
            detailHolder.setElectiveGroup(toIndex(selectGNo), group.getSelectGSubCount(), group.getSelectGAllotPnt());
        }
    }

    /**
     * 大学マスタ選択グループ教科の情報を設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     * @param umsgc_gcourse_map 大学マスタ選択グループ教科マップ
     */
    private void setSelectGroupCourse(CommonDetailHolder detailHolder, Map umsgc_gcourse_map) {
        for (Iterator groupIte = umsgc_gcourse_map.entrySet().iterator(); groupIte.hasNext();) {
            Entry groupEntry = (Entry) groupIte.next();
            String selectGNo = (String) groupEntry.getKey();
            for (Iterator courseIte = ((Map) groupEntry.getValue()).entrySet().iterator(); courseIte.hasNext();) {
                Entry courseEntry = (Entry) courseIte.next();
                String courseCd = (String) courseEntry.getKey();
                UnivMasterSelectGpCourse gc = (UnivMasterSelectGpCourse) courseEntry.getValue();
                detailHolder.setElectiveGroupCourse(toIndex(selectGNo), toIndex(courseCd), gc.getsGPerSubSubCount());
            }
        }
    }

    /**
     * イレギュラ�@を設定します。
     *
     * @param detail {@link CommonDetail}
     * @param umcs 大学マスタ志望校
     */
    private void setIrregular1(CommonDetail detail, UnivMasterChoiceSchoolPlus umcs) {

        //イレギュラ配点のいづれかが設定されている場合のみ
        //イレギュラ�@を実行する
        if (umcs.getIrrAllotPnt1() != JudgementConstants.Detail.ALLOT_NA || umcs.getIrrAllotPnt2() != JudgementConstants.Detail.ALLOT_NA || umcs.getIrrAllotPnt3() != JudgementConstants.Detail.ALLOT_NA
                || umcs.getIrrAllotPnt4() != JudgementConstants.Detail.ALLOT_NA || umcs.getIrrAllotPnt5() != JudgementConstants.Detail.ALLOT_NA || umcs.getIrrAllotPnt6() != JudgementConstants.Detail.ALLOT_NA) {

            final double[] irregular_6 = { umcs.getIrrAllotPnt1(), umcs.getIrrAllotPnt2(), umcs.getIrrAllotPnt3(), umcs.getIrrAllotPnt4(), umcs.getIrrAllotPnt5(), umcs.getIrrAllotPnt6() };

            final int[][] maxSelect = { { umcs.getMaxSelectEn1(), umcs.getMaxSelectMa1(), umcs.getMaxSelectJa1(), umcs.getMaxSelectSc1(), umcs.getMaxSelectSo1() },
                    { umcs.getMaxSelectEn2(), umcs.getMaxSelectMa2(), umcs.getMaxSelectJa2(), umcs.getMaxSelectSc2(), umcs.getMaxSelectSo2() },
                    { umcs.getMaxSelectEn3(), umcs.getMaxSelectMa3(), umcs.getMaxSelectJa3(), umcs.getMaxSelectSc3(), umcs.getMaxSelectSo3() },
                    { umcs.getMaxSelectEn4(), umcs.getMaxSelectMa4(), umcs.getMaxSelectJa4(), umcs.getMaxSelectSc4(), umcs.getMaxSelectSo4() },
                    { umcs.getMaxSelectEn5(), umcs.getMaxSelectMa5(), umcs.getMaxSelectJa5(), umcs.getMaxSelectSc5(), umcs.getMaxSelectSo5() },
                    { umcs.getMaxSelectEn6(), umcs.getMaxSelectMa6(), umcs.getMaxSelectJa6(), umcs.getMaxSelectSc6(), umcs.getMaxSelectSo6() } };

            final int[] irrSub = { umcs.getIrrSub1(), umcs.getIrrSub2(), umcs.getIrrSub3(), umcs.getIrrSub4(), umcs.getIrrSub5(), umcs.getIrrSub6() };

            detail.setUnivIrregular1(createIrregular1(irregular_6, maxSelect, irrSub));
        }
    }

    /**
     * 第一解答科目フラグを設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     * @param umcs 大学マスタ志望校
     */
    private void setFirstAnsFlg(CommonDetailHolder detailHolder, UnivMasterChoiceSchoolPlus umcs) {
        detailHolder.setFirstAnsSc(umcs.getFirstAnsScFlg());
        detailHolder.setFirstAnsSo(umcs.getFirstAnsSoFlg());
    }

    /**
     * 課し科目を返します。
     *
     * @param ums_subject_map 大学マスタ科目マップ
     * @param subCd 科目コード
     * @return {@link UnivMasterSubject}
     */
    protected final UnivMasterSubject getSubject(Map ums_subject_map, String subCd) {
        return (UnivMasterSubject) ums_subject_map.get(subCd);
    }

    /**
     * コード（教科コード、選択グループ番号）をインデックス値に変換します。
     *
     * @param code コード（教科コード、選択グループ番号）
     * @return インデックス値
     */
    private int toIndex(String code) {
        if ("1".equals(code)) {
            return 0;
        } else if ("2".equals(code)) {
            return 1;
        } else if ("3".equals(code)) {
            return 2;
        } else if ("4".equals(code)) {
            return 3;
        } else if ("5".equals(code)) {
            return 4;
        } else {
            throw new IllegalArgumentException("インデックス値に変換できませんでした。" + code);
        }
    }

}

package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * 認定試験マスタの情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
public class EngPtBean implements ComboBoxItems, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6137461000105526890L;

    /** 認定試験コード */
    private String engPtCd;

    /** 認定試験正式名 */
    private String engPtName;

    /** 認定試験短縮名 */
    private String engPtAbbr;

    /** 認定試験超短縮名 */
    private String engPtShort;

    /** スコア小数点ありフラグ */
    private String scoreDecFlg;

    /** スコア刻み */
    private double scoreTick;

    /** 出願要件基本利用フラグ */
    private String requirementStdUseFlg;

    /** 出願要件詳細利用フラグ */
    private String requirementDtlUseFlg;

    /** 合否有無フラグ */
    private String resultFlg;

    /** 得点換算基本利用フラグ */
    private String scoreConvStdUseFlg;

    /** 得点換算個別利用フラグ */
    private String scoreConvInvUseFlg;

    /** 得点換算詳細利用フラグ */
    private String scoreConvDtlUseFlg;

    /** レベルありフラグ */
    private String levelFlg;

    /** 詳細展開フラグ */
    private String detailDeployFlg;

    /** 並び順 */
    private int dispSequence;

    @Override
    public String getName() {
        return engPtName;
    }

    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}

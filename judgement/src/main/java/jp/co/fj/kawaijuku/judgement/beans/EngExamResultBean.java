package jp.co.fj.kawaijuku.judgement.beans;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 合否の情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
@ToString
public class EngExamResultBean implements ComboBoxItems {

    /**
     *
     * コンストラクタです。
     *
     * @param flg 合否フラグ
     * @param name 名称
     */
    public EngExamResultBean(String flg, String name) {
        super();
        this.flg = flg;
        this.name = name;
    }

    /** 合否フラグ */
    private String flg;

    /** 名称 */
    private String name;

    @Override
    public String getName() {
        return name;
    }

}

package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

/**
 *
 * 模試科目マスタ情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamSubjectBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 877069838967244395L;

    /** セッションキー */
    public static final String SESSION_KEY = ExamSubjectBean.class.getSimpleName();

    /** 模試コード */
    private String examCd;

    /** 科目コード */
    private String subCd;

    /** 科目名 */
    private String subName;

    /** 配点 */
    private int subAllotPnt;

    /** 表示順 */
    private String dispSequence;

    /** 基礎科目フラグ */
    private String basicFlg;

    /** 模試種別 */
    private String examTypeCd;

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

    /**
     * 模試コードを返します。
     *
     * @return examCd 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * subAllotPntを返します。
     *
     * @return subAllotPnt
     */
    public int getSubAllotPnt() {
        return subAllotPnt;
    }

    /**
     * subAllotPntを設定します。
     *
     * @param subAllotPnt subAllotPnt
     */
    public void setSubAllotPnt(int subAllotPnt) {
        this.subAllotPnt = subAllotPnt;
    }

    /**
     * 表示順を返します。
     *
     * @return 表示順
     */
    public String getDispSequence() {
        return dispSequence;
    }

    /**
     * 表示順をセットします。
     *
     * @param dispSequence 表示順
     */
    public void setDispSequence(String dispSequence) {
        this.dispSequence = dispSequence;
    }

    /**
     * 基礎科目フラグを返します。
     *
     * @return 基礎科目フラグ
     */
    public String getBasicFlg() {
        return basicFlg;
    }

    /**
     * 基礎科目フラグをセットします。
     *
     * @param basicFlg 基礎科目フラグ
     */
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

    /**
     * 模試種別を返します。
     *
     * @return examTypeCd
     */
    public String getExamTypeCd() {
        return examTypeCd;
    }

    /**
     * 模試種別を設定します。
     *
     * @param examTypeCd 模試種別
     */
    public void setExamTypeCd(String examTypeCd) {
        this.examTypeCd = examTypeCd;
    }
}

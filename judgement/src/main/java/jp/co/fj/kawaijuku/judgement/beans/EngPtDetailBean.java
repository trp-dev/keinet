package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * Fè±Ú×}X^ÌîñðÛ·éBeanÅ·B
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
public class EngPtDetailBean implements ComboBoxItems, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4382818543432144806L;

    /** Fè±R[h */
    private String engPtCd;

    /** Fè±xR[h */
    private String engPtLevelCd;

    /** Fè±x³®¼ */
    private String engPtLevelName;

    /** Fè±xZk¼ */
    private String engPtLevelAbbr;

    /** Fè±x´Zk¼ */
    private String engPtLevelShort;

    /** bdeq»èXRAãÀ */
    private double cefrJudgeScoreUpper;

    /** bdeq»èXRAºÀ */
    private double cefrJudgeScoreUnder;

    /** iXRA */
    private double passScore;

    /** Fè±xXRAãÀ */
    private double engPtLevelScoreUpper;

    /** Fè±xXRAºÀ */
    private double engPtLevelScoreUnder;

    /** bdeqxãÀ */
    private String cefrLevelUpper;

    /** bdeqxºÀ */
    private String cefrLevelUnder;

    /** ÀÑ */
    private int dispSequence;

    @Override
    public String getName() {
        return engPtLevelName;
    }

    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}

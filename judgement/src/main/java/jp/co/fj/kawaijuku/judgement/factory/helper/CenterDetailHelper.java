package jp.co.fj.kawaijuku.judgement.factory.helper;

import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.data.Irregular1Center;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail.Center;

/**
 *
 * センタ詳細情報作成ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CenterDetailHelper extends CommonDetailHelper {

    /** Singletonインスタンス */
    private static final CenterDetailHelper INSTANCE = new CenterDetailHelper();

    /**
     * コンストラクタです。
     */
    private CenterDetailHelper() {
    }

    /**
     * Singletonインスタンスを返します。
     *
     * @return {@link CenterDetailHelper}
     */
    public static CenterDetailHelper getInstance() {
        return INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    protected CommonDetail createCommonDetail() {
        return new CenterDetail();
    }

    /**
     * {@inheritDoc}
     */
    protected Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub) {
        return new Irregular1Center(irregular, maxSelect, irrSub);
    }

    /**
     * {@inheritDoc}
     */
    protected void applySubjects(CommonDetailHolder detailHolder, Map map) {

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.WRITING), CenterDetail.KEY_EWRITING, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.LISTENING), CenterDetail.KEY_ELISTENING, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.MATH1), CenterDetail.KEY_MATH1, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.MATH1A), CenterDetail.KEY_MATH1A, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.MATH2), CenterDetail.KEY_MATH2, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.MATH2B), CenterDetail.KEY_MATH2B, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.JPRESENT), CenterDetail.KEY_JPRESENT, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.JCLASSICS), CenterDetail.KEY_JCLASSICS, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.JCHINESE), CenterDetail.KEY_JCHINESE, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.PHYSICS), CenterDetail.KEY_PHYSICS, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.CHEMISTRY), CenterDetail.KEY_CHEMISTRY, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.BIOLOGY), CenterDetail.KEY_BIOLOGY, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.EARTHSCIENCE), CenterDetail.KEY_EARTHSCIENCE, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.PHYSICS_BASIC), CenterDetail.KEY_PHYSICS_BASIC, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.CHEMISTRY_BASIC), CenterDetail.KEY_CHEMISTRY_BASIC, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.BIOLOGY_BASIC), CenterDetail.KEY_BIOLOGY_BASIC, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.EARTHSCIENCE_BASIC), CenterDetail.KEY_EARTHSCIENCE_BASIC, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.JHISTORYA), CenterDetail.KEY_JHISTORYA, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.JHISTORYB), CenterDetail.KEY_JHISTORYB, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.WHISTORYA), CenterDetail.KEY_WHISTORYA, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.WHISTORYB), CenterDetail.KEY_WHISTORYB, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.GEOGRAPHYA), CenterDetail.KEY_GEOGRAPHYA, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.GEOGRAPHYB), CenterDetail.KEY_GEOGRAPHYB, CenterDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.ETHICS), CenterDetail.KEY_ETHIC, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.POLITICS), CenterDetail.KEY_POLITICS, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.SOCIALSTUDY), CenterDetail.KEY_SOCIALSTUDIES, CenterDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Center.Subcd.ETHICALPOLITICS), CenterDetail.KEY_ETHICALPOLITICS, CenterDetail.class);
    }

}

package jp.co.fj.kawaijuku.judgement.factory;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

/**
 *
 * 大学マスタ模試用科目データクラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterSubject {

    /** 科目ビット：任意 */
    private static final String BIT_ANY = "N";

    //科目ビット
    private final String subBit;

    //科目配点
    private final double subPnt;

    //科目範囲
    private final String subArea;

    //同時選択不可区分
    private final String concurrentNg;

    public UnivMasterSubject(String subBit, double subPnt, String subArea, String concurrentNg) {
        this.subBit = subBit;
        this.subPnt = subPnt;
        this.subArea = subArea;
        this.concurrentNg = concurrentNg;
    }

    public String getSubBit() {
        return subBit;
    }

    public double getSubPnt() {
        return subPnt;
    }

    public String getSubArea() {
        return subArea;
    }

    public final String getConcurrentNg() {
        return concurrentNg;
    }

    /**
     * 課されているかどうか
     * @return
     */
    public boolean isImposed() {

        if (getSubBit().trim().length() == 0) {
            return false;
        }

        return true;
    }

    /**
     * 必須かどうか
     * @return
     */
    public boolean isRequired() {

        if (!isImposed()) {
            return false;
        }

        if (JudgementConstants.Detail.BIT_REQUIRED.equals(getSubBit().trim())) {
            return true;
        }

        return false;
    }

    /**
     * 選択かどうか
     * @return
     */
    public boolean isElective() {

        if (!isImposed()) {
            return false;
        }

        return !isRequired() && !isAny();
    }

    /**
     * 任意かどうか
     *
     * @return
     */
    public boolean isAny() {

        if (!isImposed()) {
            return false;
        }

        if (BIT_ANY.equals(getSubBit().trim())) {
            return true;
        }

        return false;
    }
}

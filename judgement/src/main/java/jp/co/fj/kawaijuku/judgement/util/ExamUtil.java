package jp.co.fj.kawaijuku.judgement.util;

public class ExamUtil {

    /**
     * 英語認定試験の入力チェックを行います。
     *
     * @param data マスターデータ
     * @param input 入力値
     * @return 0：正常、1：未入力、2：スコアエラー、3：CEFRエラー
     */
    /*public static int validateEngCert(EngCertJpnDesc data, InputEngPtBean input) {
    
        int result = 1;
         試験名が未選択の場合
        if (StringUtil.isEmpty(input.getEngPtCd())) {
            return result;
        }
         英語認定試験情報を取得
        EngPtBean exam = data.getEngCertExamList().stream().filter(e -> e.getEngPtCd().equals(input.getEngPtCd())).findFirst().orElse(null);
         試験レベルが未選択の場合
        if ("1".equals(exam.getLevelFlg()) && StringUtil.isEmpty(input.getEngPtLevelCd())) {
            return result;
        }
         スコア・CEFRレベル・合否が未入力の場合
        if (StringUtil.isEmpty(input.getScore()) && StringUtil.isEmpty(input.getCefrLevelCd()) && StringUtil.isEmpty(input.getResult())) {
            return result;
        }
    
        result = 2;
        double score = 0;
        if (!StringUtil.isEmpty(input.getScore())) {
             スコアが数値以外の場合
            try {
                score = Double.parseDouble(input.getScore());
            } catch (Exception ex) {
                return result;
            }
             スコアが少数以下なしの場合
            if (Constants.FLG_OFF.equals(exam.getScoreDecFlg())) {
                try {
                    Integer.parseInt(input.getScore());
                } catch (Exception e) {
                    return result;
                }
            }
        }
    
         英語認定試験詳細情報を取得
        EngPtDetailBean examDetail;
        if (Constants.FLG_ON.equals(exam.getLevelFlg())) {
            examDetail = data.getEngCertExamDetailList().stream().filter(e -> e.getEngPtLevelCd().equals(input.getEngPtLevelCd())).findFirst().orElse(null);
        } else {
            examDetail = data.getEngCertExamDetailList().stream().filter(e -> e.getEngPtCd().equals(input.getEngPtCd())).findFirst().orElse(null);
        }
        if (examDetail != null && score > 0) {
             認定試験のスコア上限を超過する場合
            if (examDetail.getEngPtLevelScoreUpper() < score) {
                return result;
            }
             認定試験のスコア下限未満の場合
            if (examDetail.getEngPtLevelScoreUnder() > score) {
                return result;
            }
        }
    
        result = 3;
        if (examDetail != null && !StringUtil.isEmpty(input.getCefrLevelCd())) {
            if (!Constants.NON_LEVEL_CEFR_CD.equals(input.getCefrLevelCd())) {
                int cefrLevel = Integer.parseInt(input.getCefrLevelCd());
                 設定可能なCEFRレベルを超過する場合
                if (Integer.parseInt(examDetail.getCefrLevelUpper()) > cefrLevel) {
                    return result;
                }
                 設定可能なCEFRレベル未満の場合
                if (Integer.parseInt(examDetail.getCefrLevelUnder()) < cefrLevel) {
                    return result;
                }
            }
        }
        return 0;
    }*/

    /**
     * 英語認定試験情報を生成します。
     *
     * @param data マスターデータ
     * @param input 入力値
     */
    /*public static void createEngCert(EngCertJpnDesc data, InputEngPtBean input) {
         英語認定試験情報を取得
        EngPtBean exam = data.getEngCertExamList().stream().filter(e -> e.getEngPtCd().equals(input.getEngPtCd())).findFirst().orElse(null);
         英語認定試験詳細情報を取得
        EngPtDetailBean examDetail;
        if (exam == null) {
            return;
        }
        if ("1".equals(exam.getLevelFlg())) {
            examDetail = data.getEngCertExamDetailList().stream().filter(e -> e.getEngPtLevelCd().equals(input.getEngPtLevelCd())).findFirst().orElse(null);
        } else {
            examDetail = data.getEngCertExamDetailList().stream().filter(e -> e.getEngPtCd().equals(input.getEngPtCd())).findFirst().orElse(null);
        }
    
         試験名を設定
        String name = exam.getEngPtName();
        if (Constants.FLG_ON.equals(exam.getLevelFlg())) {
            name += " " + examDetail.getEngPtLevelName();
        }
        input.setDispEngPtName(name);
    
        input.setScoreCorrectFlg(Constants.FLG_OFF);
        input.setCefrLevelCdCorrectFlg(Constants.FLG_OFF);
        input.setResultCorrectFlg(Constants.FLG_OFF);
        input.setEntryFlg(Constants.FLG_ON);
    
         スコアが入力されている場合
        if (!StringUtil.isEmpty(input.getScore())) {
             スコアを設定
            input.setDispScore(input.getScore());
             スコアからCEFRを設定
            CefrBean cefr = ExamUtil.getCefrByScore(data, exam, examDetail, Double.parseDouble(input.getScore()));
            input.setDispCefrLevel(cefr.getCefrName());
            input.setCefrLevelCd(cefr.getCefrCd());
            input.setDispResult(Constants.DBL_HYPHEN);
            input.setResult(null);
        } else {
             CEFRが入力されている場合
            if (!StringUtil.isEmpty(input.getCefrLevelCd())) {
                 CEFRからスコアを取得
                String score = ExamUtil.getScoreByCefr(data, exam, input.getCefrLevelCd());
                 スコアを設定
                input.setScore(score);
                if (StringUtil.isEmpty(score) || BigDecimal.ZERO.toString().equals(score)) {
                    input.setDispScore(Constants.DBL_HYPHEN);
                } else {
                    input.setDispScore(score);
                    input.setScoreCorrectFlg(Constants.FLG_ON);
                }
                CefrBean cefr = data.getCefrList().stream().filter(e -> e.getCefrCd().equals(input.getCefrLevelCd())).findFirst().orElse(null);
                input.setCefrLevelCd(cefr.getCefrCd());
                input.setDispCefrLevel(cefr.getCefrName());
                input.setResult(null);
                input.setDispResult(Constants.DBL_HYPHEN);
            } else {
                 不合格の場合
                if ("2".equals(input.getResult())) {
                     スコアを設定
                    input.setScore(null);
                    input.setDispScore(Constants.DBL_HYPHEN);
                     CEFRを設定
                    CefrBean cefr = data.getCefrList().stream().filter(e -> e.getCefrCd().equals(Constants.NON_LEVEL_CEFR_CD)).findFirst().orElse(null);
                    input.setCefrLevelCd(cefr.getCefrCd());
                    input.setDispCefrLevel(cefr.getCefrName());
                    input.setCefrLevelCdCorrectFlg(Constants.FLG_ON);
                     合否を設定
                    input.setDispResult(Constants.ENG_EXAM_RESULT.get("2").getName());
                } else {
                    String score = null;
                     合格スコアを取得
                    if (Constants.FLG_ON.equals(exam.getScoreDecFlg())) {
                        score = String.valueOf(examDetail.getPassScore());
                    } else {
                        score = String.valueOf((int) examDetail.getPassScore());
                    }
                     スコアを設定
                    input.setScore(score);
                    input.setDispScore(score);
                    input.setScoreCorrectFlg(Constants.FLG_ON);
                     スコアからCEFRを設定
                    CefrBean cefr = ExamUtil.getCefrByScore(data, exam, examDetail, Double.parseDouble(score));
                    input.setCefrLevelCd(cefr.getCefrCd());
                    input.setDispCefrLevel(cefr.getCefrName());
                    input.setCefrLevelCdCorrectFlg(Constants.FLG_ON);
                     合否を設定
                    input.setDispResult(Constants.ENG_EXAM_RESULT.get("1").getName());
                }
            }
        }
    
    }*/

    /**
     * 選択した認定試験とスコアからCEFR換算用スコアを取得し、CEFRレベルを求めます。
     *
     * @param master マスターデータ
     * @param exam 認定試験情報
     * @param examDetail 認定試験詳細情報
     * @param score スコア
     * @return CEFR情報
     */
    /*public static CefrBean getCefrByScore(EngCertJpnDesc master, EngPtBean exam, EngPtDetailBean examDetail, double score) {
        CefrBean cefr;
    
         スコアからCEFR換算用スコアを取得
        CefrConvScoreBean conv =
                master.getCefrConvScoreList().stream().filter(e -> e.getCefrConvScore() <= score && e.getEngPtCd().equals(examDetail.getEngPtCd())).sorted(comparing(CefrConvScoreBean::getCefrCd, naturalOrder())).findFirst().orElse(null);
        if (conv == null) {
             CEFRレベルなしを設定
            cefr = master.getCefrList().stream().filter(e -> Constants.NON_LEVEL_CEFR_CD.equals(e.getCefrCd())).findFirst().orElse(null);
            return cefr;
        }
         CEFRレベルのスコア下限未満の場合
        if (score < examDetail.getCefrJudgeScoreUnder()) {
             CEFRレベルなしを設定
            cefr = master.getCefrList().stream().filter(e -> Constants.NON_LEVEL_CEFR_CD.equals(e.getCefrCd())).findFirst().orElse(null);
            return cefr;
        }
        String cefrCd = conv.getCefrCd();
    
         合格スコアを取得
        double passScore = 0;
        if (Constants.FLG_ON.equals(exam.getResultFlg())) {
            passScore = examDetail.getPassScore();
        }
    
         合格スコア以上
        if (passScore <= score) {
             英検の場合
            if (Constants.EIKEN_CD.equals(exam.getEngPtCd())) {
                 CEFR判定レベルの上限
                cefr = master.getCefrList().stream().filter(e -> e.getCefrCd().equals(examDetail.getCefrLevelUpper())).findFirst().orElse(null);
            } else {  英検以外の場合
                 換算用スコアからCEFRレベルを取得
                cefr = master.getCefrList().stream().filter(e -> e.getCefrCd().equals(cefrCd)).findFirst().orElse(null);
            }
        } else { 合格スコア未満
             換算用スコアからCEFRレベルを取得
            cefr = master.getCefrList().stream().filter(e -> e.getCefrCd().equals(cefrCd)).findFirst().orElse(null);
        }
    
        return cefr;
    }*/

    /**
     * 選択した認定試験とCEFRレベルからCEFR換算用スコアを取得し、スコアを求めます。
     *
     * @param master マスターデータ
     * @param exam 認定試験情報
     * @param cefrLevelCd CEFRレベルコード
     * @return スコア
     */
    /*public static String getScoreByCefr(EngCertJpnDesc master, EngPtBean exam, String cefrLevelCd) {
        String result = null;
        if (cefrLevelCd == null) {
            return result;
        }
    
         選択した認定試験とCEFRレベルからスコアを算出する
        CefrConvScoreBean conv = master.getCefrConvScoreList().stream().filter(e -> e.getEngPtCd().equals(exam.getEngPtCd()) && e.getCefrCd().equals(cefrLevelCd)).findFirst().orElse(null);
    
        if (conv == null) {
            result = null;
        } else if (Constants.FLG_ON.equals(exam.getScoreDecFlg())) {
            result = String.valueOf(conv.getCefrConvScore());
        } else {
            result = String.valueOf((int) conv.getCefrConvScore());
        }
        return result;
    
    }*/
}

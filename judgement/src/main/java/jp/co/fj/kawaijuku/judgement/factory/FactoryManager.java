package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.data.Univ;

/**
 *
 * 大学マスタ展開マネージャー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class FactoryManager {

    /** ロードファクトリインスタンス */
    private final UnivFactory factory;

    /** 判定用大学マスタのロード所要時間 */
    private long loadingTime;

    /** 情報誌用科目データのロード所要時間 */
    private long univInfoLoadingTime;

    /** 大学インスタンスリスト */
    private List datas;

    /** 大学インスタンスマップ */
    private Map datasMap;

    /** 情報誌用科目データマップ */
    private Map univInfoMap;

    /* CEFR情報マスタ */
    /*private List<CefrBean> cefrInfo = new ArrayList();*/

    /* 英語認定試験情報マスタ */
    /*private List<EngPtBean> engCert = new ArrayList();*/

    /* 英語認定試験詳細情報マスタ */
    /*private List<EngPtDetailBean> engCertDetail = new ArrayList();*/

    /* CEFR換算用スコア情報 */
    /*private List<CefrConvScoreBean> cefrConvScore = new ArrayList();*/

    /* 記述式総合評価マスタ */
    private List<JpnDescTotalEvaluationBean> jpnDescTotalEval = new ArrayList();

    /* 模試科目マスタ */
    private Map<String, ExamSubjectBean> examSubject;

    /**
     * コンストラクタです、
     *
     * @param factory ロードファクトリインスタンス
     */
    public FactoryManager(UnivFactory factory) {
        this.factory = factory;
    }

    /**
     * 判定用大学マスタをロードします。
     */
    public void load() throws Exception {

        /* 開始時間を記録する */
        long start = System.currentTimeMillis();

        /* 判定用大学マスタをロードする */
        datas = factory.getLoadedData();

        /* ロード時間を設定する */
        loadingTime = System.currentTimeMillis() - start;
    }

    /**
     * 情報誌用科目データをロードします。
     */
    public void loadUnivInfo() throws Exception {

        /* 開始時間を記録する */
        long start = System.currentTimeMillis();

        /* 情報誌用科目データをロードする */
        univInfoMap = factory.loadUnivInfo();

        /* 英語認定試験、国語記述に関連するマスタデータをロードする */
        loadSubjectInfo();

        /* ロード時間を設定する */
        univInfoLoadingTime = System.currentTimeMillis() - start;
    }

    /**
     * 共通テスト対応で追加された英語認定試験、国語記述に関連するマスタデータをロードします。
     *
     * @throws Exception
     */
    public void loadSubjectInfo() throws Exception {
        Map data = factory.loadSubjectInfo();
        /*cefrInfo = (List<CefrBean>) data.get(CefrBean.class.getName());
        cefrInfo.stream().forEach(item -> item.setCefrName(item.getCefrName().replace("未達", "なし")));
        engCert = (List<EngPtBean>) data.get(EngPtBean.class.getName());
        engCertDetail = (List<EngPtDetailBean>) data.get(EngPtDetailBean.class.getName());
        cefrConvScore = (List<CefrConvScoreBean>) data.get(CefrConvScoreBean.class.getName());*/
        jpnDescTotalEval = (List<JpnDescTotalEvaluationBean>) data.get(JpnDescTotalEvaluationBean.class.getName());
        examSubject = (Map<String, ExamSubjectBean>) data.get(ExamSubjectBean.class.getName());
    }

    /**
     * 展開されたマスタから指定の大学を検索します。
     *
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @param deptCd 学科コード
     * @return {@link Univ}
     */
    public Univ searchUniv(String univCd, String facultyCd, String deptCd) {

        if (datasMap == null) {
            datasMap = new HashMap(datas.size() * 4 / 3);
            for (Iterator ite = datas.iterator(); ite.hasNext();) {
                Univ univ = (Univ) ite.next();
                datasMap.put(univ.getUniqueKey(), univ);
            }
        }

        return (Univ) datasMap.get(univCd + facultyCd + deptCd);
    }

    /**
     * 大学インスタンスリストを返します。
     *
     * @return 大学インスタンスリスト
     */
    public List getDatas() {
        return datas;
    }

    /**
     * 判定用大学マスタのロード所要時間を返します。
     *
     * @return 判定用大学マスタのロード所要時間
     */
    public long getLoadingTime() {
        return loadingTime;
    }

    /**
     * 情報誌用科目データのロード所要時間を返します。
     *
     * @return 情報誌用科目データのロード所要時間
     */
    public long getUnivInfoLoadingTime() {
        return univInfoLoadingTime;
    }

    /**
     * ロードした大学マスタの模試区分を返します。
     *
     * @return ロードした大学マスタの模試区分
     */
    public String getExamDiv() {
        return factory.getExamDiv();
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    public Map getUnivInfoMap() {
        return univInfoMap;
    }

    /**
     * CEFR情報マスタリストを返します。
     *
     * @return CEFR情報マスタリスト
     */
    /*public List<CefrBean> getCefrInfo() {
        return cefrInfo;
    }*/

    /**
     * 英語認定試験情報マスタリストを返します。
     *
     * @return 英語認定試験情報マスタリスト
     */
    /*public List<EngPtBean> getEngCert() {
        return engCert;
    }*/

    /**
     * 英語認定試験詳細情報マスタリストを返します。
     *
     * @return 英語認定試験詳細情報マスタリスト
     */
    /*public List<EngPtDetailBean> getEngCertDetail() {
        return engCertDetail;
    }*/

    /**
     * CEFR換算用スコア情報を返します。
     *
     * @return CEFR換算用スコア情報
     */
    /*public List<CefrConvScoreBean> getCefrConvScore() {
        return cefrConvScore;
    }*/

    /**
     * 記述式総合評価マスタを返します。
     *
     * @return 記述式総合評価マスタ
     */
    public List<JpnDescTotalEvaluationBean> getJpnDescTotalEval() {
        return jpnDescTotalEval;
    }

    /**
     * 模試科目マスタを返します。
     *
     * @return 模試科目マスタ
     */
    public Map<String, ExamSubjectBean> getExamSubject() {
        return examSubject;
    }
}

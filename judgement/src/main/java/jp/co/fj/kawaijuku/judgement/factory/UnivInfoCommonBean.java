package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;

/**
 *
 * 情報誌用科目2と3の共通情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoCommonBean {

    /** 配点信頼 */
    private String scTrust;

    /** 総点 */
    private String totalSc;

    /** 科目名 */
    private String subName;

    /** 教科配点リスト */
    private final List courseList = new ArrayList(5);

    /** 英語認定試験配点 */
    private UnivInfoEngCertBean engCert;

    /**
     * 配点信頼を返します。
     *
     * @return 配点信頼
     */
    public String getScTrust() {
        return scTrust;
    }

    /**
     * 配点信頼をセットします。
     *
     * @param scTrust 配点信頼
     */
    public void setScTrust(String scTrust) {
        this.scTrust = scTrust;
    }

    /**
     * 総点を返します。
     *
     * @return 総点
     */
    public String getTotalSc() {
        return totalSc;
    }

    /**
     * 総点をセットします。
     *
     * @param totalSc 総点
     */
    public void setTotalSc(String totalSc) {
        this.totalSc = totalSc;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * 外国語の教科配点を追加します。
     *
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    public void addGai(String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        addCourse("／英", trustPt, shoPt, shoSel, daiPt, daiSel, subject);
    }

    /**
     * 数学の教科配点を追加します。
     *
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    public void addMath(String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        addCourse("／数", trustPt, shoPt, shoSel, daiPt, daiSel, subject);
    }

    /**
     * 国語の教科配点を追加します。
     *
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    public void addKokugo(String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        addCourse("／国", trustPt, shoPt, shoSel, daiPt, daiSel, subject);
    }

    /**
     * 理科の教科配点を追加します。
     *
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    public void addRika(String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        addCourse("／理", trustPt, shoPt, shoSel, daiPt, daiSel, subject);
    }

    /**
     * 地公の教科配点を追加します。
     *
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    public void addSociety(String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        addCourse("／地・公", trustPt, shoPt, shoSel, daiPt, daiSel, subject);
    }

    /**
     * 教科配点を追加します。
     *
     * @param label ラベル
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    private void addCourse(String label, String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        if (shoPt != null && shoPt.length() > 0) {
            courseList.add(new UnivInfoCourseBean(label, trustPt, shoPt, shoSel, daiPt, daiSel, subject));
        }
    }

    /**
     * 英語認定試験配点を追加します。
     *
     * @param label ラベル
     * @param engCertApp 共通T英語認定試験
     * @param engCertTrustPt 配点信頼
     * @param engCertPt 配点
     * @param engCertAddPt 加点
     * @param engCertAddPtType 加点種類
     * @param engCertSel 必選
     */
    public void addEngCert(String label, String engCertApp, String engCertTrustPt, String engCertPt, String engCertAddPt, String engCertAddPtType, String engCertSel) {
        engCert = new UnivInfoEngCertBean(label, engCertApp, engCertTrustPt, engCertPt, engCertAddPt, engCertAddPtType, engCertSel);
    }

    /**
     * 教科配点リストを返します。
     *
     * @return 教科配点リスト
     */
    public List getCourseList() {
        return courseList;
    }

    /**
     * 英語認定試験配点を返します。
     *
     * @return 英語認定試験配点
     */
    public UnivInfoEngCertBean getEngCert() {
        return engCert;
    }

}

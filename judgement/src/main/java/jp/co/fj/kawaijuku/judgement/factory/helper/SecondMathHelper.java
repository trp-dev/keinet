package jp.co.fj.kawaijuku.judgement.factory.helper;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.beans.judge.SecondMatHandler;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;

/**
 *
 * 二次数学用課しヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SecondMathHelper extends SubjectHelper {

    public static void prepare(CommonDetailHolder detailHolder, UnivMasterSubject subject) {
        if (subject != null) {
            SubjectKey subjectKey = SecondMatHandler.getSubjectKey(Integer.parseInt(subject.getSubArea()));
            if (subject.isRequired()) {
                setRequired(detailHolder, subject, subjectKey);
            } else {
                SubjectKey newSubjectKey = JudgedUtil.getSubjectKey(detailHolder, subject, subjectKey, SecondDetail.class);
                setElective(detailHolder, subject, newSubjectKey);
            }
        }
    }

}

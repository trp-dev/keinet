package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * 大学マスタ情報を保持するデータクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class UnivMasterContainer {

    /** 大学マスタ基本情報の想定レコード数 */
    protected static final int RECNUM_BASIC = 20000;

    /** 制限する大学リスト */
    private final Set restriction = new HashSet();

    /** 大学マスタ基本情報 */
    private List univMasterBasic;

    /** 大学マスタ模試用志望校 */
    private Map univMasterChoiceSchool;

    /** 大学マスタ模試用教科 */
    private Map univMasterCourse;

    /** 大学マスタ模試用科目 */
    private Map univMasterSubject;

    /** 大学マスタ模試用選択グループ */
    private Map univMasterSelectGroup;

    /** 大学マスタ模試用選択グループ教科 */
    private Map univMasterSelectGpCourse;

    /** 系統マップ */
    private Map stemmaMap;

    /** 二次ランク */
    private final Map rankMaster = new HashMap();

    /** 注釈文マスタマップ */
    private final Map<String, UnivComment> univCommentMap = new HashMap<String, UnivComment>();

    /**
     * 大学マスタ情報を初期化します。
     */
    protected void initialize() {
        if (restriction.isEmpty()) {
            univMasterBasic = new ArrayList(RECNUM_BASIC);
            univMasterChoiceSchool = new HashMap((int) (RECNUM_BASIC * 3.1 * 4 / 3));
            univMasterCourse = new HashMap((int) (RECNUM_BASIC * 10.4 * 4 / 3));
            univMasterSubject = new HashMap((int) (RECNUM_BASIC * 34.5 * 4 / 3));
            univMasterSelectGroup = new HashMap((int) (RECNUM_BASIC * 3.1 * 4 / 3));
            univMasterSelectGpCourse = new HashMap((int) (RECNUM_BASIC * 6.9 * 4 / 3));
            stemmaMap = new HashMap((int) (RECNUM_BASIC * 4.4 * 4 / 3));
        } else {
            univMasterBasic = new ArrayList();
            univMasterChoiceSchool = new HashMap();
            univMasterCourse = new HashMap();
            univMasterSubject = new HashMap();
            univMasterSelectGroup = new HashMap();
            univMasterSelectGpCourse = new HashMap();
            stemmaMap = new HashMap();
        }
    }

    /**
     * ロード対象の大学情報セットを返します。
     *
     * @return ロード対象の大学情報セット
     */
    public Set getRestriction() {
        return restriction;
    }

    /**
     * 大学マスタ基本情報を返します。
     *
     * @return 大学マスタ基本情報
     */
    public List getUnivMasterBasic() {
        return univMasterBasic;
    }

    /**
     * 大学マスタ模試用志望校を返します。
     *
     * @return 大学マスタ模試用志望校
     */
    public Map getUnivMasterChoiceSchool() {
        return univMasterChoiceSchool;
    }

    /**
     * 大学マスタ模試用科目を返します。
     *
     * @return 大学マスタ模試用科目
     */
    public Map getUnivMasterCourse() {
        return univMasterCourse;
    }

    /**
     * 大学マスタ模試用科目を返します。
     *
     * @return 大学マスタ模試用科目
     */
    public Map getUnivMasterSubject() {
        return univMasterSubject;
    }

    /**
     * 大学マスタ模試用選択グループを返します。
     *
     * @return 大学マスタ模試用選択グループ
     */
    public Map getUnivMasterSelectGroup() {
        return univMasterSelectGroup;
    }

    /**
     * 大学マスタ模試用選択グループ教科を返します。
     *
     * @return 大学マスタ模試用選択グループ教科
     */
    public Map getUnivMasterSelectGpCourse() {
        return univMasterSelectGpCourse;
    }

    /**
     * 二次ランクを返します。
     *
     * @return 二次ランク
     */
    public Map getRankMaster() {
        return rankMaster;
    }

    /**
     * 系統マップを返します。
     *
     * @return 系統マップ
     */
    public Map getStemmaMap() {
        return stemmaMap;
    }

    /**
     * 注釈文マスタマップを返します。
     *
     * @return 注釈文マスタマップ
     */
    public Map<String, UnivComment> getUnivCommentMap() {
        return univCommentMap;
    }

    /**
     * マップから指定したキーの値を取得します。
     *
     * @param map マップ
     * @param key キー
     * @param <V> 値の型
     * @return 値マップ
     */
    private Map getValue(Map map, String key) {
        Map child = (Map) map.get(key);
        if (child == null) {
            child = new HashMap();
            map.put(key, child);
        }
        return child;
    }

    /**
     * 大学マスタ模試用志望校を追加します。
     *
     * @param data {@link UnivMasterChoiceSchoolPlus}
     * @param key 大学キー
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     */
    public void addUnivMasterChoiceSchool(UnivMasterChoiceSchoolPlus data, String key, String branchCd, String entExamDiv) {
        getValue(getValue(univMasterChoiceSchool, key), branchCd).put(entExamDiv, data);
    }

    /**
     * 大学マスタ模試用教科を追加します。
     *
     * @param data {@link UnivMasterCourse}
     * @param key 大学キー
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @param courseCd 教科コード
     */
    public void addUnivMasterCource(UnivMasterCourse data, String key, String branchCd, String entExamDiv, String courseCd) {
        getValue(getValue(getValue(univMasterCourse, key), branchCd), entExamDiv).put(courseCd, data);
    }

    /**
     * 大学マスタ模試用選択グループ教科を追加します。
     *
     * @param data {@link UnivMasterSelectGpCourse}
     * @param key 大学キー
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @param selectGNo 選択グループ番号
     * @param courseCd 教科コード
     */
    public void addUnivMasterSelectGpCourse(UnivMasterSelectGpCourse data, String key, String branchCd, String entExamDiv, String selectGNo, String courseCd) {
        getValue(getValue(getValue(getValue(univMasterSelectGpCourse, key), branchCd), entExamDiv), selectGNo).put(courseCd, data);
    }

    /**
     * 大学マスタ模試用選択グループを追加します。
     *
     * @param data {@link UnivMasterSelectGroup}
     * @param key 大学キー
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @param selectGNo 選択グループ番号
     */
    public void addUnivMasterSelectGroup(UnivMasterSelectGroup data, String key, String branchCd, String entExamDiv, String selectGNo) {
        getValue(getValue(getValue(univMasterSelectGroup, key), branchCd), entExamDiv).put(selectGNo, data);
    }

    /**
     * 大学マスタ模試用科目を追加します。
     *
     * @param data {@link UnivMasterSubject}
     * @param key 大学キー
     * @param branchCd 枝番
     * @param entExamDiv 入試試験区分
     * @param examSubCd 科目コード
     */
    public void addUnivMasterSubject(UnivMasterSubject data, String key, String branchCd, String entExamDiv, String examSubCd) {
        getValue(getValue(getValue(univMasterSubject, key), branchCd), entExamDiv).put(examSubCd, data);
    }
}

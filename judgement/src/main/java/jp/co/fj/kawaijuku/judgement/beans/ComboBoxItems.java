package jp.co.fj.kawaijuku.judgement.beans;

/**
 * CEFR、国語記述入力ダイアログのコンボボックスインターフェイス
 *
 * @author QQ)Kurimoto
 *
 */
public interface ComboBoxItems {

    /**
     * コンボボックスのラベルを返却します。
     *
     * @return ラベル
     */
    public String getName();

}

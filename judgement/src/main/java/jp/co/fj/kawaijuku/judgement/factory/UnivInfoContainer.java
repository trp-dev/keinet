package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 情報誌用科目データを保持するデータクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoContainer {

    /** 情報誌用科目1の想定レコード数 */
    private static final int RECNUM_BASIC = 30000;

    /** 情報誌用科目1のリスト */
    private final List univInfoBasicList = new ArrayList(RECNUM_BASIC);

    /** 情報誌用科目2のマップ */
    private final Map univInfoCenterMap = new HashMap(RECNUM_BASIC * 4 / 3);

    /** 情報誌用科目3のマップ */
    private final Map univInfoSecondMap = new HashMap(RECNUM_BASIC * 4 / 3);

    /**
     * コンストラクタです。
     */
    public UnivInfoContainer() {
    }

    /**
     * 情報誌用科目1のリストを返します。
     *
     * @return 情報誌用科目1のリスト
     */
    public List getUnivInfoBasicList() {
        return univInfoBasicList;
    }

    /**
     * 情報誌用科目2のマップを返します。
     *
     * @return 情報誌用科目2のマップ
     */
    public Map getUnivInfoCenterMap() {
        return univInfoCenterMap;
    }

    /**
     * 情報誌用科目3のマップを返します。
     *
     * @return 情報誌用科目3のマップ
     */
    public Map getUnivInfoSecondMap() {
        return univInfoSecondMap;
    }

}

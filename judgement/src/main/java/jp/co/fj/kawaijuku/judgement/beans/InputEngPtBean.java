package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

import jp.co.fj.kawaijuku.judgement.Constants;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * 英語認定試験の入力情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
public class InputEngPtBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2896569642638560278L;

    /**
     * コンストラクタです。
     */
    public InputEngPtBean() {
        super();
        dispEngPtName = Constants.DBL_HYPHEN;
        dispScore = Constants.DBL_HYPHEN;
        dispCefrLevel = Constants.DBL_HYPHEN;
        dispResult = Constants.DBL_HYPHEN;
        entryFlg = Constants.FLG_OFF;
    }

    /** 認定試験コード */
    private String engPtCd;
    //    /** 認定試験正式名 */
    //    private String engPtName;
    //    /** 認定試験短縮名 */
    //    private String engPtAbbr;
    /** 認定試験レベルコード */
    private String engPtLevelCd;
    //    /** 認定試験レベル正式名 */
    //    private String engPtLevelName;
    //    /** 認定試験レベル短縮名 */
    //    private String engPtLevelAbbr;
    /** スコア */
    private String score;
    /** スコア補正フラグ */
    private String scoreCorrectFlg;
    /** スコア入力フラグ */
    private String scoreInputFlg;
    /** CEFRレベル */
    private String cefrLevelCd;
    //    /** CEFRレベル正式名 */
    //    private String cefrLevelName;
    //    /** CEFRレベル短縮名 */
    //    private String cefrLevelAbbr;
    /** CEFRレベル補正フラグ */
    private String cefrLevelCdCorrectFlg;
    /** CEFRレベル入力フラグ */
    private String cefrLevelCdInputFlg;
    /** 合否 */
    private String result;
    /** 合否フラグ */
    private String resultInputFlg;
    /** 合否補正フラグ */
    private String resultCorrectFlg;
    /** 入力フラグ */
    private String entryFlg;
    /** エラーコード */
    private int errorCode;

    /** 【表示用】試験名 */
    private String dispEngPtName;
    /** 【表示用】スコア */
    private String dispScore;
    /** 【表示用】CEFR */
    private String dispCefrLevel;
    /** 【表示用】合否 */
    private String dispResult;

    /**
     * 【表示用】試験名を返却します。
     *
     * @return 【表示用】試験名
     */
    public String getDispScore() {
        String result = this.dispScore;
        if (Constants.FLG_ON.equals(scoreCorrectFlg)) {
            result += Constants.ASTERISK;
        }
        return result;
    }

    /**
     * 【表示用】スコアを返却します。
     *
     * @return 【表示用】スコア
     */
    public String getDispCefrLevel() {
        String result = this.dispCefrLevel;
        if (Constants.FLG_ON.equals(cefrLevelCdCorrectFlg)) {
            result += Constants.ASTERISK;
        }
        return result;
    }

    /**
     * 【表示用】CEFRを返却します。
     *
     * @return 【表示用】CEFR
     */
    public String getDispResult() {
        String result = this.dispResult;
        if (Constants.FLG_ON.equals(resultCorrectFlg)) {
            result += Constants.ASTERISK;
        }
        return result;
    }

    /**
     * 英語認定試験が入力されているかどうか返却します。
     *
     * @return
     */
    public boolean isEmpty() {
        return StringUtil.isEmpty(engPtCd);
    }

    /**
     * 判定処理に設定する英語認定試験情報を返却します。
     *
     * @return 英語参加試験データ保持用クラス
     */
    public EngSSScoreData createEngSSScoreData() {
        EngSSScoreData result = new EngSSScoreData();
        result.setEngSSCode(engPtCd);
        result.setEngSSLevelCode(engPtLevelCd);
        result.setEngScore(StringUtil.isEmpty(score) ? 0d : Double.parseDouble(score));
        return result;
    }

    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}

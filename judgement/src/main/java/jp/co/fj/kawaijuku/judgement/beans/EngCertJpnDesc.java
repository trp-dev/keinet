package jp.co.fj.kawaijuku.judgement.beans;

import java.util.List;

/**
 * @author kuri
 *
 */
public interface EngCertJpnDesc {
    /*public List<CefrBean> getCefrList();*/

    /** 認定試験情報 */
    /*public List<EngPtBean> getEngCertExamList();*/

    /** 認定試験詳細情報 */
    /*public List<EngPtDetailBean> getEngCertExamDetailList();*/

    /** CEFR換算用スコア情報 */
    /*public List<CefrConvScoreBean> getCefrConvScoreList();*/

    /** 国語記述式総合評価情報 */
    public List<JpnDescTotalEvaluationBean> getJpnDescList();

}

package jp.co.fj.kawaijuku.judgement.factory.helper;

import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail.Second;

public interface SecondSubjectRange {
    int rangeUpper();

    SubjectKey subjectKeyUpper();

    int rangeLower();

    SubjectKey subjectKeyLower();

    SubjectKey subjectKeyUnknown();
}

final class Physics implements SecondSubjectRange {
    @Override
    public int rangeUpper() {
        return Second.Science.RANGE_PHYSICS;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_PHYSICS;
    }

    @Override
    public int rangeLower() {
        return Second.Science.RANGE_PHYSICS_BASIC;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return SecondDetail.KEY_PHYSICS_BASIC;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_PHYSICS_UNKNOWN;
    }
}

final class Chemistry implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Science.RANGE_CHEMISTRY;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_CHEMISTRY;
    }

    @Override
    public int rangeLower() {
        return Second.Science.RANGE_CHEMISTRY_BASIC;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return SecondDetail.KEY_CHEMISTRY_BASIC;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_CHEMISTRY_UNKNOWN;
    }
}

final class Biology implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Science.RANGE_BIOLOGY;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_BIOLOGY;
    }

    @Override
    public int rangeLower() {
        return Second.Science.RANGE_BIOLOGY_BASIC;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return SecondDetail.KEY_BIOLOGY_BASIC;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_BIOLOGY_UNKNOWN;
    }
}

final class EarthSience implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Science.RANGE_EARTHSCIENCE;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_EARTHSCIENCE;
    }

    @Override
    public int rangeLower() {
        return Second.Science.RANGE_EARTHSCIENCE_BASIC;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return SecondDetail.KEY_EARTHSCIENCE_BASIC;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_EARTHSCIENCE_UNKNOWN;
    }
}

final class JHistory implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Social.RANGE_JHISTORYB;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_JHISTORYB;
    }

    @Override
    public int rangeLower() {
        return -1;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return null;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_JHISTORYA;
    }
}

final class WHistory implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Social.RANGE_WHISTORYB;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_WHISTORYB;
    }

    @Override
    public int rangeLower() {
        return -1;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return null;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_WHISTORYA;
    }
}

final class Geography implements SecondSubjectRange {

    @Override
    public int rangeUpper() {
        return Second.Social.RANGE_GEOGRAPHYB;
    }

    @Override
    public SubjectKey subjectKeyUpper() {
        return SecondDetail.KEY_GEOGRAPHYB;
    }

    @Override
    public int rangeLower() {
        return -1;
    }

    @Override
    public SubjectKey subjectKeyLower() {
        return null;
    }

    @Override
    public SubjectKey subjectKeyUnknown() {
        return SecondDetail.KEY_GEOGRAPHYA;
    }
}

package jp.co.fj.kawaijuku.judgement.factory.helper;

import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetailHolder;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.data.Irregular1Second;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterCourse;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail.Second;

/**
 *
 * 二次詳細情報作成ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SecondDetailHelper extends CommonDetailHelper {

    /** Singletonインスタンス */
    private static final SecondDetailHelper INSTANCE = new SecondDetailHelper();
    private final Physics physics = new Physics();
    private final Chemistry chemistry = new Chemistry();
    private final Biology biology = new Biology();
    private final EarthSience earthSience = new EarthSience();
    private final JHistory jHistory = new JHistory();
    private final WHistory wHistory = new WHistory();
    private final Geography geography = new Geography();

    /**
     * コンストラクタです。
     */
    private SecondDetailHelper() {
    }

    /**
     * Singletonインスタンスを返します。
     *
     * @return {@link SecondDetailHelper}
     */
    public static SecondDetailHelper getInstance() {
        return INSTANCE;
    }

    /**
     * {@inheritDoc}
     */
    protected CommonDetail createCommonDetail() {
        return new SecondDetail();
    }

    /**
     * {@inheritDoc}
     */
    protected Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub) {
        return new Irregular1Second(irregular, maxSelect, irrSub);
    }

    /**
     * {@inheritDoc}
     */
    protected void setCourse(CommonDetail detail, CommonDetailHolder detailHolder, Map umc_course_map) {
        super.setCourse(detail, detailHolder, umc_course_map);
        /* その他教科の配点を設定する */
        UnivMasterCourse course = (UnivMasterCourse) umc_course_map.get("9");
        ((SecondDetailHolder) detailHolder).setOtherAllot(course == null ? Detail.ALLOT_NA : course.getNecesAllotPnt());
    }

    /**
     * {@inheritDoc}
     */
    protected void applySubjects(CommonDetailHolder detailHolder, Map map) {

        SecondRangeHelper.prepare(detailHolder, getSubject(map, Second.Subcd.ENGLISH1), SecondDetail.KEY_ENGLISH);
        SecondRangeHelper.prepare(detailHolder, getSubject(map, Second.Subcd.ENGLISH2), SecondDetail.KEY_ENGLISH);

        SecondMathHelper.prepare(detailHolder, getSubject(map, Second.Subcd.MATH1));
        SecondMathHelper.prepare(detailHolder, getSubject(map, Second.Subcd.MATH2));
        SecondMathHelper.prepare(detailHolder, getSubject(map, Second.Subcd.MATH3));

        SecondJpnHelper.prepare(detailHolder, getSubject(map, Second.Subcd.JAPANESE1));
        SecondJpnHelper.prepare(detailHolder, getSubject(map, Second.Subcd.JAPANESE2));
        SecondJpnHelper.prepare(detailHolder, getSubject(map, Second.Subcd.JAPANESE3));

        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.PHYSICS), physics);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.CHEMISTRY), chemistry);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.BIOLOGY), biology);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.EARTHSCIECNE), earthSience);

        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.JHISTORY), jHistory);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.WHISTORY), wHistory);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.GEOGRAPHY), geography);

        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.ETHIC), SecondDetail.KEY_ETHIC, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.POLITICS), SecondDetail.KEY_POLITICS, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.SOCIALSTUDY), SecondDetail.KEY_SOCIALSTUDIES, SecondDetail.class);

        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.ESSAY), SecondDetail.KEY_ESSAY, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.SYNTHESIS), SecondDetail.KEY_SYNTHESIS, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.INTERVIEW), SecondDetail.KEY_INTERVIEW, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.TECHNIQUE), SecondDetail.KEY_TECHNIQUE, SecondDetail.class);
        /*
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.ETCETERA), SecondDetail.KEY_ETCETERA, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.RESEARCH), SecondDetail.KEY_RESEARCH, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.BOOKKEEPING), SecondDetail.KEY_BOOKKEEPING, SecondDetail.class);
        SubjectHelper.prepare(detailHolder, getSubject(map, Second.Subcd.INFORMATION), SecondDetail.KEY_INFORMATION, SecondDetail.class);
        */
        // TODO:ここに英資を追加
    }

}

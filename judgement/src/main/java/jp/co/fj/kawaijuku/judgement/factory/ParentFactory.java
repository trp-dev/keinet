package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.CefrBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrConvScoreBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuAppInfo;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.JpnKijutuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.Rank;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.data.UnivStemma;

/**
 *
 * 大学マスタ展開処理
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)HASE.Shoji
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class ParentFactory implements UnivFactory {

    /** 大学マスタ情報を保持するデータクラス */
    private final UnivMasterContainer container = createUnivMasterContainer();

    /**
     * 大学マスタ情報を保持するデータクラスを生成します。
     *
     * @return {@link UnivMasterContainer}
     */
    abstract protected UnivMasterContainer createUnivMasterContainer();

    /**
     * ランクマスタをメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployRank(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ基本情報（UNIVMASTER_BASIC）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterBasic(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ模試用志望校（UNIVMASTER_CHOICESCHOOL）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterChoiceSchool(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ模試用教科（UNIVMASTER_COURSE）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterCourse(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ模試用科目（UNIVMASTER_SUBJECT）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSubject(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ模試用選択グループ（UNIVMASTER_SELECTGROUP）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSelectGroup(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ模試用選択グループ教科（UNIVMASTER_SELECTGPCOURSE）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSelectGpCourse(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ公表用志望校（UNIVMASTER_CHOICESCHOOL_PUB）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterChoiceSchoolPub(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ公表用教科（UNIVMASTER_COURSE_PUB）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterCoursePub(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ公表用科目（UNIVMASTER_SUBJECT_PUB）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSubjectPub(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ公表用選択グループ（UNIVMASTER_SELECTGROUP_PUB）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSelectGroupPub(UnivMasterContainer container) throws Exception;

    /**
     * 大学マスタ公表用選択グループ教科（UNIVMASTER_SELECTGPCOURSE_PUB）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivMasterSelectGpCoursePub(UnivMasterContainer container) throws Exception;

    /**
     * 大学系統（UNIVSTEMMA）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployStemma(UnivMasterContainer container) throws Exception;

    /**
     * 注釈文マスタ（UNIVCOMMENT）をメモリに展開します。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void deployUnivComment(UnivMasterContainer container) throws Exception;

    /**
     * CEFR情報マスタ(CEFR_INFO)をメモリに展開します。
     *
     * @throws Exception
     */
    abstract protected List<CefrBean> deployCefrInfo() throws Exception;

    /**
     * 英語認定試験情報マスタ(EXAMINFO)をメモリに展開します。
     *
     * @throws Exception
     */
    abstract protected List<EngPtBean> deployEngCert() throws Exception;

    /**
     * 英語認定試験詳細情報マスタ(EXAMDETAILINFO)をメモリに展開します。
     *
     * @throws Exception
     */
    abstract protected List<EngPtDetailBean> deployEngCertDetail() throws Exception;

    /**
     * CEFR換算用スコア情報(CEFRCVSSCOREINFO)をメモリに展開します。
     *
     * @throws Exception
     */
    abstract protected List<CefrConvScoreBean> deployCefrConvScore() throws Exception;

    /**
     * 記述式総合評価マスタ(DESCCOMPRATING)をメモリに展開します。
     *
     * @throws Exception
     */
    abstract protected List<JpnDescTotalEvaluationBean> deployJpnDescTotalEval() throws Exception;

    /**
     * 模試科目マスタ(EXAMSUBJECT)をメモリに展開します。
     *
     * @return
     * @throws Exception
     */
    abstract protected Map<String, ExamSubjectBean> deployExamSubject() throws Exception;

    /**
     * 英語資格・検定試験_出願要件情報を取得します。
     *
     * @return
     * @throws Exception
     */
    abstract protected List<EngSikakuAppInfo> deployEngPtAppReqList() throws Exception;

    /**
     * 英語資格・検定試験_得点換算情報を取得します。
     *
     * @return
     * @throws Exception
     */
    abstract protected List<EngSikakuScoreInfo> deployEngPtScoreConvList() throws Exception;

    /**
     * 国語記述式_得点換算マスタ情報を取得します。
     *
     * @return
     * @throws Exception
     */
    abstract protected List<JpnKijutuScoreInfo> deployKokugoDescScoreConvList() throws Exception;

    /**
     * 大学インスタンスを作成します。
     *
     * @param container {@link UnivMasterContainer}
     * @param basic {@link UnivMasterBasicPlus}
     */
    abstract protected UnivData createUniv(UnivMasterContainer container, UnivMasterBasicPlus basic);

    /**
     * 判定用大学マスタ生成処理の事前処理を行います。
     *
     * @param container {@link UnivMasterContainer}
     */
    abstract protected void prepareUnivExam(UnivMasterContainer container) throws Exception;

    /**
     * 情報誌用科目1をロードします。
     *
     * @param container {@link UnivInfoContainer}
     */
    abstract protected void loadUnivInfoBasic(UnivInfoContainer container) throws Exception;

    /**
     * 情報誌用科目2をロードします。
     *
     * @param container {@link UnivInfoContainer}
     */
    abstract protected void loadUnivInfoCenter(UnivInfoContainer container) throws Exception;

    /**
     * 情報誌用科目3をロードします。
     *
     * @param container {@link UnivInfoContainer}
     */
    abstract protected void loadUnivInfoSecond(UnivInfoContainer container) throws Exception;

    @Override
    public List getLoadedData() throws Exception {

        /* メモリを初期化する */
        container.initialize();

        /* 事前処理を行う */
        prepareUnivExam(container);

        /* データをすべてメモリ上に展開する */
        deployUnivExam();

        /* メモリ上のデータから大学インスタンスリストを作成する */
        return createUnivList();
    }

    /**
     * 判定用大学マスタをメモリ上に展開します。
     */
    private void deployUnivExam() throws Exception {
        deployRank(container);
        deployUnivMasterBasic(container);
        deployUnivMasterChoiceSchool(container);
        deployUnivMasterCourse(container);
        deployUnivMasterSubject(container);
        deployUnivMasterSelectGroup(container);
        deployUnivMasterSelectGpCourse(container);
        deployStemma(container);
        deployUnivComment(container);
    }

    /**
     * システム別に展開されたマスタデータから大学インスタンスリストを作成します。
     *
     * @return 大学インスタンスリスト
     */
    private List createUnivList() throws Exception {

        List list = new ArrayList(container.getUnivMasterBasic().size());

        for (Iterator ite = container.getUnivMasterBasic().iterator(); ite.hasNext();) {

            UnivMasterBasicPlus basic = (UnivMasterBasicPlus) ite.next();

            /* 個々のシステム別に大学情報を作成する */
            UnivData univ = createUniv(container, basic);

            /* ランク情報を設定する */
            setRank(univ, basic.getEntExamRank());

            /* 系統情報を設定する */
            setStemmaData(univ);

            /* 注釈文マスタを設定する */
            univ.setUnivComment(container.getUnivCommentMap().get(univ.getUniqueKey()));

            /* 作成した大学情報を一覧に追加する */
            list.add(univ);

            /* GC対象になるように、Mapから削除しておく */
            ite.remove();
            Map umcs_map = (Map) container.getUnivMasterChoiceSchool().remove(univ.getUniqueKey());
            Map umc_map = (Map) container.getUnivMasterCourse().remove(univ.getUniqueKey());
            Map ums_map = (Map) container.getUnivMasterSubject().remove(univ.getUniqueKey());
            Map umsg_map = (Map) container.getUnivMasterSelectGroup().remove(univ.getUniqueKey());
            Map umsgc_map = (Map) container.getUnivMasterSelectGpCourse().remove(univ.getUniqueKey());

            /* 判定詳細情報を設定する */
            univ.setDetailHash(FactoryHelper.createCrossPattern(univ.getUnivCd(), univ.getFacultyCd(), univ.getDeptCd(), umcs_map, umc_map, ums_map, umsg_map, umsgc_map));

            /* 足切判定詳細情報を設定する */
            if (univ.getPickedPubPoint() > 0) {
                univ.setRejectHash(FactoryHelper.createRejectCrossPattern(univ.getUnivCd(), univ.getFacultyCd(), univ.getDeptCd(), umcs_map, umc_map, ums_map, umsg_map, umsgc_map));
            }
        }

        return list;
    }

    /**
     * ランクを設定します。
     *
     * @param univ {@link UnivData}
     * @param rankCd ランクコード
     */
    private void setRank(UnivData univ, String rankCd) {
        Rank rank = (Rank) container.getRankMaster().get(rankCd);
        univ.setRankName(rank.getRankName());
        univ.setRankHLimits(rank.getRankHLimits());
        univ.setRankLLimits(rank.getRankLLimits());
    }

    /**
     * 系統情報を設定します。
     *
     * @param univAll 大学インスタンスリスト
     */
    private void setStemmaData(UnivData univ) {
        List stemmas = (List) container.getStemmaMap().get(univ.getUniqueKey());
        if (stemmas != null) {
            for (Iterator ite = stemmas.iterator(); ite.hasNext();) {
                UnivStemma stemma = (UnivStemma) ite.next();
                if ("1".equals(stemma.getStemmaDiv())) {
                    univ.getSStemmaList().add(new String[] { stemma.getStemmaCd(), null });
                } else if ("2".equals(stemma.getStemmaDiv())) {
                    univ.getMStemmaList().add(new String[] { stemma.getStemmaCd(), null });
                } else if ("3".equals(stemma.getStemmaDiv())) {
                    univ.getRegionList().add(new String[] { stemma.getStemmaCd(), null });
                }
            }
        }
    }

    @Override
    public Map getLoadedPubData() throws Exception {

        /* メモリを初期化する */
        container.initialize();

        /* データをすべてメモリ上に展開する */
        deployUnivPub();

        /* メモリ上のデータから公表用データマップを作成して返す */
        return createPubMap();
    }

    /**
     * 公表用大学マスタをメモリ上に展開します。
     */
    private void deployUnivPub() throws Exception {
        deployUnivMasterChoiceSchoolPub(container);
        deployUnivMasterCoursePub(container);
        deployUnivMasterSubjectPub(container);
        deployUnivMasterSelectGroupPub(container);
        deployUnivMasterSelectGpCoursePub(container);
    }

    /**
     * システム別に展開されたマスタデータから公表用データマップを作成します。
     *
     * @return 公表用データマップ
     */
    private Map createPubMap() throws Exception {

        Map map = new HashMap(container.getUnivMasterChoiceSchool().keySet().size() * 4 / 3);

        for (Iterator ite = container.getUnivMasterChoiceSchool().keySet().iterator(); ite.hasNext();) {
            String key = (String) ite.next();
            String univCd = key.substring(0, 4);
            String facultyCd = key.substring(4, 6);
            String deptCd = key.substring(6);

            /* GC対象になるように、Mapから削除しておく */
            Map umcs_map = (Map) container.getUnivMasterChoiceSchool().get(key);
            Map umc_map = (Map) container.getUnivMasterCourse().remove(key);
            Map ums_map = (Map) container.getUnivMasterSubject().remove(key);
            Map umsg_map = (Map) container.getUnivMasterSelectGroup().remove(key);
            Map umsgc_map = (Map) container.getUnivMasterSelectGpCourse().remove(key);
            ite.remove();

            /* 公表用データマップに追加する */
            map.put(key, FactoryHelper.createPublibCrossPattern(univCd, facultyCd, deptCd, umcs_map, umc_map, ums_map, umsg_map, umsgc_map));
        }

        return map;
    }

    @Override
    public void restrict(String key) {
        container.getRestriction().add(key);
    }

    @Override
    public Map loadUnivInfo() throws Exception {
        UnivInfoContainer container = new UnivInfoContainer();
        loadUnivInfoBasic(container);
        loadUnivInfoCenter(container);
        loadUnivInfoSecond(container);
        return new UnivInfoFactory().create(container);
    }

    @Override
    public Map loadSubjectInfo() throws Exception {
        Map datas = new HashMap();
        datas.put(CefrBean.class.getName(), deployCefrInfo());
        datas.put(EngPtBean.class.getName(), deployEngCert());
        datas.put(EngPtDetailBean.class.getName(), deployEngCertDetail());
        datas.put(CefrConvScoreBean.class.getName(), deployCefrConvScore());
        datas.put(JpnDescTotalEvaluationBean.class.getName(), deployJpnDescTotalEval());
        datas.put(ExamSubjectBean.class.getName(), deployExamSubject());

        return datas;
    }
}

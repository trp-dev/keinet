package jp.co.fj.kawaijuku.judgement.factory;

/**
 *
 * 国語記述式の配点を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoJpnDescBean implements UnivInfoAllotBean {

    /** ラベル */
    private final String label;

    /** 共通テスト国語-記述範囲 */
    private final String jpnDescRange;

    /** 共通テスト国語記述−配点信頼 */
    private final String jpnDescTrustPt;

    /** 共通テスト国語記述−配点 */
    private final String jpnDescPt;

    /** 共通テスト国語記述−加点 */
    private final String jpnDescAddPt;

    /** 共通テスト国語記述−加点種類 */
    private final String jpnDescAddPtType;

    /** 共通テスト国語記述−必選 */
    private final String jpnDescSel;

    /**
     * コンストラクタです。
     *
     * @param jpnDescRange 記述範囲
     * @param jpnDescTrustPt 配点信頼
     * @param jpnDescPt 配点
     * @param jpnDescAddPt 加点
     * @param jpnDescAddPtType 加点種類
     * @param jpnDescSel 必選
     */
    UnivInfoJpnDescBean(String label, String jpnDescRange, String jpnDescTrustPt, String jpnDescPt, String jpnDescAddPt, String jpnDescAddPtType, String jpnDescSel) {
        this.label = label;
        this.jpnDescRange = jpnDescRange;
        this.jpnDescTrustPt = jpnDescTrustPt;
        this.jpnDescPt = jpnDescPt;
        this.jpnDescAddPt = jpnDescAddPt;
        this.jpnDescAddPtType = jpnDescAddPtType;
        this.jpnDescSel = jpnDescSel;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public boolean isAllot() {
        return "1".equals(jpnDescRange);
    }

    @Override
    public String getTrustPt() {
        return jpnDescTrustPt;
    }

    @Override
    public String getPt() {
        return jpnDescPt;
    }

    @Override
    public String getAddPt() {
        return jpnDescAddPt;
    }

    @Override
    public String getAddPtType() {
        return jpnDescAddPtType;
    }

    @Override
    public String getSel() {
        return jpnDescSel;
    }

}

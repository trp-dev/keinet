package jp.co.fj.kawaijuku.judgement.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.HandledPoint;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;

public final class JudgedUtil {

    private static final JudgedUtil ins = new JudgedUtil();
    private static final ObjectMapper om = new ObjectMapper();

    /**
     * 判定結果の判定用得点操作データクラスより該当教科のグループインデックスを取得します。
     *
     * @param adoptedScore
     * @param courseIndex
     * @return
     */
    public static int findGroupIndex(List adoptedScore, int courseIndex) {
        int result = 0;
        for (Object obj : adoptedScore) {
            HandledPoint point = (HandledPoint) obj;
            if (point.getCourseIndex() == courseIndex) {
                result = point.getSubjectKey().getSg();
                break;
            }
        }
        return result;
    }

    /**
     *
     * @param <T>
     * @param target
     * @param clazz
     * @return
     */
    private static <T> SubjectKey getSelectGroupSubjectKey(SubjectKey target, String subBit, Class<T> clazz) {
        SubjectKey result = target;
        int groupIndex = Integer.valueOf(subBit);
        for (Field field : clazz.getDeclaredFields()) {
            try {
                SubjectKey key;
                field.setAccessible(true);
                key = (SubjectKey) field.get(clazz);
                if (target.getKeyString().equals(key.getKeyString()) && key.getSg() == groupIndex) {
                    result = key;
                    break;
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    /**
     * 二次・個別試験の課し科目キーを取得します。
     *
     * @param <T>
     * @param detailHolder
     * @param subject
     * @param target
     * @param clazz
     * @return
     */
    public static <T> SubjectKey getSubjectKey(CommonDetailHolder detailHolder, UnivMasterSubject subject, SubjectKey target, Class<T> clazz) {
        SubjectKey result = target;
        // 選択科目ではない場合、そのまま返却
        if (!subject.isElective()) {
            return result;
        }
        // 科目ビットの１桁目を取得（グループインデックス）
        String subBit = subject.getSubBit().substring(0, 1);
        // 該当グループインデックスの課し科目キーを取得
        SubjectKey key = getSelectGroupSubjectKey(target, subBit, clazz);
        // 該当科目が課されてない場合
        ImposingSubject imposingSubject = detailHolder.getImposingSubject(key);
        if (!imposingSubject.getElective().isImposed()) {
            result = key;
        }
        return result;
    }

    /**
     * 成績データのデバッグログを出力する。
     *
     * @param score 成績データ
     * @throws Exception 例外
     */
    public static void outputScoreDebugLog(Logger logger, Score score) throws Exception {

        logger.debug("----- 成績データ開始 -------------------------");
        logger.debug(String.format("{\"mark\":%s,\"wrtn\":%s}", om.writeValueAsString(score.getMark()), om.writeValueAsString(score.getWrtn())));
        logger.debug("----- 成績データ終了 -------------------------");
    }

    /**
     * 判定結果のデバッグログを出力する。
     *
     * @param judgements 判定結果
     * @throws Exception 例外
     */
    public static void outputResultDebugLog(Logger logger, List<Judgement> judgements) throws Exception {

        logger.debug("----- 判定結果データ開始 -------------------------");
        om.addMixIn(Judgement.class, ResultIgnore.class);
        List<ResultJson> results = new ArrayList<ResultJson>();
        for (Judgement judgement : judgements) {
            results.add(ins.new ResultJson(judgement));
        }
        logger.debug("{\"rslt\":" + om.writeValueAsString(results) + "}");
        logger.debug("----- 判定結果データ終了 -------------------------");
    }

    /**
     * 判定結果のデバッグログを出力する。
     *
     * @param judgement 判定結果
     * @throws Exception 例外
     */
    public static void outputResultDebugLog(Logger logger, Judgement judgement) throws Exception {

        logger.debug("----- 判定結果データ開始 -------------------------");
        om.addMixIn(Judgement.class, ResultIgnore.class);
        logger.debug("{\"rslt\":" + om.writeValueAsString(ins.new ResultJson(judgement)) + "}");
        logger.debug("----- 判定結果データ終了 -------------------------");
    }

    class ResultJson implements Serializable {
        /**
         * コンストラクタです。
         * @param judge
         */
        public ResultJson(Judgement judge) {
            super();
            this.univCode = judge.getUniv().getUniqueKey();
            Map<SubjectKey, ImposingSubject> cmap = (Map<SubjectKey, ImposingSubject>) judge.getSelectedDetail().getCenter().getForJudgement().getImposingSubjects();
            this.center = cmap.entrySet().stream().filter(e -> e.getValue().isImposed()).map(m -> m.getKey()).collect(Collectors.toList());
            Map<SubjectKey, ImposingSubject> smap = (Map<SubjectKey, ImposingSubject>) judge.getSelectedDetail().getSecond().getForJudgement().getImposingSubjects();
            this.second = smap.entrySet().stream().filter(e -> e.getValue().isImposed()).map(m -> m.getKey()).collect(Collectors.toList());
            this.judge = judge;
        }

        /** SVUID */
        private static final long serialVersionUID = 1L;
        @Getter
        final String univCode;
        @Getter
        final List<SubjectKey> center;
        @Getter
        final List<SubjectKey> second;
        @Getter
        final Judgement judge;
    }

    /**
     * 判定結果のデバッグログに出力しないメソッドを定義するインターフェイス
     *
     * @author SRS)Kurimoto
     *
     */
    interface ResultIgnore {
        @JsonIgnore
        Score getScore();

        @JsonIgnore
        Univ getUniv();

        @JsonIgnore
        UnivDetail getSelectedDetail();
    }
}

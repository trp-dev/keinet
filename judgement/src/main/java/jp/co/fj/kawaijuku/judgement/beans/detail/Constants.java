package jp.co.fj.kawaijuku.judgement.beans.detail;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

public class Constants {

    /** �p�ꎑ�i�E���莎�� */
    public static final SubjectKey KEY_ENG_QUALIFICATION = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 380, 0);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v1�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G1 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 381, 1);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v2�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G2 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 382, 2);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v3�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G3 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 383, 3);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v4�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G4 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 384, 4);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v5�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G5 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 385, 5);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v6�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G6 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 386, 6);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v7�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G7 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 387, 7);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v8�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G8 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 388, 8);
    /** �p�ꎑ�i�E���莎���i�I���O���[�v9�j */
    public static final SubjectKey KEY_ENG_QUALIFICATION_G9 = new SubjectKey("�p��", null, JudgementConstants.Detail.Etc.COURSE_INDEX, 389, 9);

}

package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.factory.helper.CenterDetailHelper;
import jp.co.fj.kawaijuku.judgement.factory.helper.CommonDetailHelper;
import jp.co.fj.kawaijuku.judgement.factory.helper.SecondDetailHelper;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;

/**
 *
 * 大学マスタ展開ヘルパー
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class FactoryHelper {

    /**
     * 【模試用】枝番をクロスパターンで作成します。
     */
    public static Map createCrossPattern(String univcd, String facultycd, String deptcd, Map umcs_map, Map umc_map, Map ums_map, Map umsg_map, Map umsgc_map) {

        Map details = setup_detail(univcd, facultycd, deptcd, umcs_map, umc_map, ums_map, umsg_map, umsgc_map, false);

        /* センタ枝リスト */
        List centers = (List) details.get("1");
        if (centers == null) {
            /* 存在しない場合は空のインスタンスを挿入しておく */
            centers = new ArrayList();
            centers.add(new CenterDetail());
        }

        /* 二次枝リスト */
        List seconds = (List) details.get("2");
        if (seconds == null) {
            /* 存在しない場合は空のインスタンスを挿入しておく */
            seconds = new ArrayList();
            seconds.add(new SecondDetail());
        }

        /* センタを基準にペアを作成する */
        Map map = new TreeMap();
        for (Iterator centerIte = centers.iterator(); centerIte.hasNext();) {

            /* センタ詳細情報 */
            CenterDetail center = (CenterDetail) centerIte.next();

            for (Iterator secondIte = seconds.iterator(); secondIte.hasNext();) {

                /* 二次詳細情報 */
                SecondDetail second = (SecondDetail) secondIte.next();

                /* ペア枝番を採番する */
                String branch = JudgementUtil.leftPad(Integer.toString(map.size() + 1), JudgementConstants.Detail.BRANCHCD_LENGTH, '0');

                /* センタと二次の詳細情報をペアにして完成させる */
                UnivDetail detail = new UnivDetail(univcd, facultycd, deptcd, branch);
                detail.setCenter(center);
                detail.setSecond(second);

                map.put(branch, detail);
            }
        }

        return map;
    }

    /**
     * 【第一段階選抜用】枝番をクロスパターンで作成します。
     */
    public static Map createRejectCrossPattern(String univcd, String facultycd, String deptcd, Map umcs_map, Map umc_map, Map ums_map, Map umsg_map, Map umsgc_map) {

        Map details = setup_detail(univcd, facultycd, deptcd, umcs_map, umc_map, ums_map, umsg_map, umsgc_map, true);

        /* 第一段階選抜枝リスト */
        List rejects = (List) details.get("3");
        if (rejects == null) {
            /* 存在しない場合はここまで */
            return null;
        }

        /* センタを基準にペアを作成する */
        Map map = new TreeMap();
        for (Iterator ite = rejects.iterator(); ite.hasNext();) {

            /*  第一段階選抜詳細情報 */
            CenterDetail reject = (CenterDetail) ite.next();

            /* ペア枝番を採番する */
            String branch = JudgementUtil.leftPad(Integer.toString(map.size() + 1), JudgementConstants.Detail.BRANCHCD_LENGTH, '0');

            /* センタと二次の詳細情報をペアにして完成させる */
            UnivDetail detail = new UnivDetail(univcd, facultycd, deptcd, branch);
            detail.setCenter(reject);
            detail.setSecond(new SecondDetail());

            map.put(branch, detail);
        }

        return map;
    }

    /**
     * 【公表用】枝番をクロスパターンで作成します。
     */
    public static Map createPublibCrossPattern(String univcd, String facultycd, String deptcd, Map umcs_map, Map umc_map, Map ums_map, Map umsg_map, Map umsgc_map) {

        if (umcs_map == null) {
            /* 志望校データが無ければここまで */
            return Collections.EMPTY_MAP;
        }

        /* センタと二次の枝番数をカウントする */
        int centerCount = 0;
        int secondCount = 0;
        for (Iterator ite = umcs_map.values().iterator(); ite.hasNext();) {
            Map umcs_branch = (Map) ite.next();
            for (Iterator ite2 = umcs_branch.keySet().iterator(); ite2.hasNext();) {
                String entExamDiv = (String) ite2.next();
                if ("1".equals(entExamDiv)) {
                    centerCount++;
                } else if ("2".equals(entExamDiv)) {
                    secondCount++;
                }
            }
        }

        if (centerCount < 2 || secondCount < 2 || centerCount != secondCount) {
            /* センタと二次の枝番が2以上の同数でなければ模試用と同じ扱いとする */
            return createCrossPattern(univcd, facultycd, deptcd, umcs_map, umc_map, ums_map, umsg_map, umsgc_map);
        }

        /* 判定用詳細データを作成する */
        Map details = setup_detail(univcd, facultycd, deptcd, umcs_map, umc_map, ums_map, umsg_map, umsgc_map, false);

        /* センタ枝リスト */
        List centers = (List) details.get("1");

        /* 二次枝リスト */
        List seconds = (List) details.get("2");

        /* 枝番が同じペアを作成する */
        Map map = new TreeMap();
        for (int i = 0; i < centers.size(); i++) {

            /* センタ詳細情報 */
            CenterDetail center = (CenterDetail) centers.get(i);

            /* 二次詳細情報 */
            SecondDetail second = (SecondDetail) seconds.get(i);

            /* ペア枝番を採番する */
            String branch = JudgementUtil.leftPad(Integer.toString(map.size() + 1), JudgementConstants.Detail.BRANCHCD_LENGTH, '0');

            /* センタと二次の詳細情報をペアにして完成させる */
            UnivDetail detail = new UnivDetail(univcd, facultycd, deptcd, branch);
            detail.setCenter(center);
            detail.setSecond(second);

            map.put(branch, detail);
        }

        return map;
    }

    /**
     * 判定用詳細データを作成します。
     */
    private static Map setup_detail(String univcd, String facultycd, String deptcd, Map umcs_branch_map, Map umc_branch_map, Map ums_branch_map, Map umsg_branch_map, Map umsgc_branch_map, boolean isReject) {

        if (umcs_branch_map == null) {
            /* 詳細データ自体が存在しない場合は空のマップを返す */
            return Collections.EMPTY_MAP;
        }

        /* 枝番の昇順で詳細データを作成する */
        Map details = new HashMap();
        List keys = new ArrayList(umcs_branch_map.keySet());
        Collections.sort(keys);
        for (Iterator ite = keys.iterator(); ite.hasNext();) {

            String branchCd = (String) ite.next();
            Map umcs_branch = (Map) umcs_branch_map.get(branchCd);

            /* 試験区分別に詳細データを作成する */
            for (Iterator ite2 = umcs_branch.entrySet().iterator(); ite2.hasNext();) {

                Entry entry = (Entry) ite2.next();
                String entExamDiv = (String) entry.getKey();
                UnivMasterChoiceSchoolPlus umcs = (UnivMasterChoiceSchoolPlus) entry.getValue();

                if ((isReject && !"3".equals(entExamDiv)) || (!isReject && "3".equals(entExamDiv))) {
                    /* 対象の試験区分でなければ次へ */
                    continue;
                }

                List list = (List) details.get(entExamDiv);
                if (list == null) {
                    list = new ArrayList();
                    details.put(entExamDiv, list);
                }

                CommonDetailHelper detailHelper;
                if ("1".equals(entExamDiv) || "3".equals(entExamDiv)) {
                    /* センタまたは第一段階選抜 */
                    detailHelper = CenterDetailHelper.getInstance();
                } else if ("2".equals(entExamDiv)) {
                    /* 二次 */
                    detailHelper = SecondDetailHelper.getInstance();
                } else {
                    /* 不明 */
                    continue;
                }

                /* データを作成 */
                list.add(detailHelper.create(branchCd, entExamDiv, umcs, umc_branch_map, ums_branch_map, umsg_branch_map, umsgc_branch_map));
            }
        }
        return details;
    }
}

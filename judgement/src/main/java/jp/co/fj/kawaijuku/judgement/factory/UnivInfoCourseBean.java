package jp.co.fj.kawaijuku.judgement.factory;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;

/**
 *
 * 情報誌用科目の教科配点を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoCourseBean {

    /** ラベル */
    private final String label;

    /** 配点信頼 */
    private final String trustPt;

    /** 配点小 */
    private final String shoPt;

    /** 必選小 */
    private final String shoSel;

    /** 配点大 */
    private final String daiPt;

    /** 必選大 */
    private final String daiSel;

    /** 教科 */
    private final COURSE_ALLOT subject;

    /**
     * コンストラクタです。
     *
     * @param label ラベル
     * @param trustPt 配点信頼
     * @param shoPt 配点小
     * @param shoSel 必選小
     * @param daiPt 配点大
     * @param daiSel 必選大
     * @param subject 教科
     */
    UnivInfoCourseBean(String label, String trustPt, String shoPt, String shoSel, String daiPt, String daiSel, COURSE_ALLOT subject) {
        this.label = label;
        this.trustPt = trustPt;
        this.shoPt = shoPt;
        this.shoSel = shoSel;
        this.daiPt = daiPt;
        this.daiSel = daiSel;
        this.subject = subject;
    }

    /**
     * ラベルを返します。
     *
     * @return ラベル
     */
    public String getLabel() {
        return label;
    }

    /**
     * 配点信頼を返します。
     *
     * @return 配点信頼
     */
    public String getTrustPt() {
        return trustPt;
    }

    /**
     * 配点小を返します。
     *
     * @return 配点小
     */
    public String getShoPt() {
        return shoPt;
    }

    /**
     * 必選小を返します。
     *
     * @return 必選小
     */
    public String getShoSel() {
        return shoSel;
    }

    /**
     * 配点大を返します。
     *
     * @return 配点大
     */
    public String getDaiPt() {
        return daiPt;
    }

    /**
     * 必選大を返します。
     *
     * @return 必選大
     */
    public String getDaiSel() {
        return daiSel;
    }

    /**
     * 教科を返します。
     *
     * @return 必選大
     */
    public COURSE_ALLOT getSubject() {
        return subject;
    }

}

package jp.co.fj.kawaijuku.judgement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.fj.kawaijuku.judgement.beans.EngExamResultBean;

/**
 * @author kuri
 *
 */
public final class Constants {

    /* 英検コード */
    public static final String EIKEN_CD = "20";

    /** CEFRレベルコード：レベル無し */
    public static final String NON_LEVEL_CEFR_CD = "99";

    /** 全角ハイフン */
    public static final String DBL_HYPHEN = "−";

    /** アスタリスク */
    public static final String ASTERISK = "*";

    /** フラグ：ON */
    public static final String FLG_ON = "1";

    /** フラグ：OFF */
    public static final String FLG_OFF = "0";

    /** 英語 */
    public final static class English {
        /** リーディング満点値 */
        public static final int PERFECT_SCORE_WRITING = 100;
        /** リスニング満点値 */
        public static final int PERFECT_SCORE_LISTENING = 100;
    }

    /**
     * 教科配点インデックス
     *
     */
    public enum COURSE_ALLOT {
        /** 英語 */
        ENG,
        /** 数学 */
        MAT,
        /** 国語 */
        JAP,
        /** 理科 */
        SCI,
        /** 地歴公民 */
        SOC,
        /** 英語認定 */
        ENGPT,
        /** 国語記述 */
        JAP_DESC,
        /** その他科目（二次試験） */
        ETC
    }

    /** 合否 */
    public static final Map<String, EngExamResultBean> ENG_EXAM_RESULT;
    static {
        Map<String, EngExamResultBean> set = new HashMap();
        set.put("1", new EngExamResultBean("1", "合格"));
        set.put("2", new EngExamResultBean("2", "不合格"));
        ENG_EXAM_RESULT = set;
    }
    public static final List<EngExamResultBean> ENG_EXAM_RESULT_LIST;
    static {
        List<EngExamResultBean> set = new ArrayList();
        set.add(ENG_EXAM_RESULT.get("1"));
        set.add(ENG_EXAM_RESULT.get("2"));
        ENG_EXAM_RESULT_LIST = set;
    }
}

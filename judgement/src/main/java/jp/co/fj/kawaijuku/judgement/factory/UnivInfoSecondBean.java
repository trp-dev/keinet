package jp.co.fj.kawaijuku.judgement.factory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 情報誌用科目3の情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoSecondBean extends UnivInfoCommonBean {

    /** その他科目リスト */
    private final List otherList = new ArrayList(8);

    /** その他−配点信頼 */
    private String hoka2TrustPt;

    /** その他−配点 */
    private String hoka2Pt;

    /**
     * その他−配点信頼を返します。
     *
     * @return その他−配点信頼
     */
    public String getHoka2TrustPt() {
        return hoka2TrustPt;
    }

    /**
     * その他−配点信頼をセットします。
     *
     * @param hoka2TrustPt その他−配点信頼
     */
    public void setHoka2TrustPt(String hoka2TrustPt) {
        this.hoka2TrustPt = hoka2TrustPt;
    }

    /**
     * その他−配点を返します。
     *
     * @return その他−配点
     */
    public String getHoka2Pt() {
        return hoka2Pt;
    }

    /**
     * その他−配点をセットします。
     *
     * @param hoka2Pt その他−配点
     */
    public void setHoka2Pt(String hoka2Pt) {
        this.hoka2Pt = hoka2Pt;
    }

    /**
     * 小論文を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addEssay(String mark, String trustPt, String pt, String hissu) {
        addOther("小", mark, trustPt, pt, hissu);
    }

    /**
     * 総合を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addTotal(String mark, String trustPt, String pt, String hissu) {
        addOther("総", mark, trustPt, pt, hissu);
    }

    /**
     * 面接を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addInterview(String mark, String trustPt, String pt, String hissu) {
        addOther("面", mark, trustPt, pt, hissu);
    }

    /**
     * 実技を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addPlactical(String mark, String trustPt, String pt, String hissu) {
        addOther("実", mark, trustPt, pt, hissu);
    }

    /**
     * 簿記を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addBoki(String mark, String trustPt, String pt, String hissu) {
        addOther("簿", mark, trustPt, pt, hissu);
    }

    /**
     * 情報を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addInfo(String mark, String trustPt, String pt, String hissu) {
        addOther("情", mark, trustPt, pt, hissu);
    }

    /**
     * 調査書を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addResearch(String mark, String trustPt, String pt, String hissu) {
        addOther("調", mark, trustPt, pt, hissu);
    }

    /**
     * 他を追加します。
     *
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    public void addHoka(String mark, String trustPt, String pt, String hissu) {
        addOther("他", mark, trustPt, pt, hissu);
    }

    /**
     * その他科目を追加します。
     *
     * @param signage 表記
     * @param mark マーク
     * @param trustPt 配点信頼
     * @param pt 配点
     * @param hissu 必選
     */
    private void addOther(String signage, String mark, String trustPt, String pt, String hissu) {
        if (mark != null && mark.length() > 0) {
            otherList.add(new UnivInfoOtherBean(signage, mark, trustPt, pt, hissu));
        }
    }

    /**
     * その他科目リストを返します。
     *
     * @return その他科目リスト
     */
    public List getOtherList() {
        return otherList;
    }

}

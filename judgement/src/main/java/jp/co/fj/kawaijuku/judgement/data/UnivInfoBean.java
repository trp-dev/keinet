package jp.co.fj.kawaijuku.judgement.data;

import java.util.Map;

import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;

/**
 *
 * 画面に表示する情報誌用科目の情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivInfoBean {

    /** 枝番名称 */
    private String branchName;

    /** センタ課しフラグ */
    private boolean isCExist;

    /** センタ教科数 */
    private String cenCourseNum;

    /** センタ試験科目 */
    private String c_subString;

    /** 理科第1解答科目フラグ */
    private boolean firstAnsFlgSci;

    /** 地公第1解答科目フラグ */
    private boolean firstAnsFlgSoc;

    /** センタ配点 */
    private String allot1;

    /** 二次課しフラグ */
    private boolean isSExist;

    /** 二次試験科目 */
    private String s_subString;

    /** 二次配点 */
    private String allot2;

    /** 共通テスト教科別配点 */
    private Map<COURSE_ALLOT, String> centerAllot;

    /** 二次教科別配点 */
    private Map<COURSE_ALLOT, String> secondAllot;

    /**
     * 枝番名称を返します。
     *
     * @return 枝番名称
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * 枝番名称をセットします。
     *
     * @param branchName 枝番名称
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * センタ課しフラグを返します。
     *
     * @return センタ課しフラグ
     */
    public boolean isCExist() {
        return isCExist;
    }

    /**
     * センタ課しフラグをセットします。
     *
     * @param isCExist センタ課しフラグ
     */
    public void setCExist(boolean isCExist) {
        this.isCExist = isCExist;
    }

    /**
     * センタ教科数を返します。
     *
     * @return センタ教科数
     */
    public String getCenCourseNum() {
        return cenCourseNum;
    }

    /**
     * センタ教科数をセットします。
     *
     * @param cenCourseNum センタ教科数
     */
    public void setCenCourseNum(String cenCourseNum) {
        this.cenCourseNum = cenCourseNum;
    }

    /**
     * センタ試験科目を返します。
     *
     * @return センタ試験科目
     */
    public String getC_subString() {
        return c_subString;
    }

    /**
     * センタ試験科目をセットします。
     *
     * @param c_subString センタ試験科目
     */
    public void setC_subString(String c_subString) {
        this.c_subString = c_subString;
    }

    /**
     * 理科第1解答科目フラグを返します。
     *
     * @return 理科第1解答科目フラグ
     */
    public boolean isFirstAnsFlgSci() {
        return firstAnsFlgSci;
    }

    /**
     * 理科第1解答科目フラグをセットします。
     *
     * @param firstAnsFlgSci 理科第1解答科目フラグ
     */
    public void setFirstAnsFlgSci(boolean firstAnsFlgSci) {
        this.firstAnsFlgSci = firstAnsFlgSci;
    }

    /**
     * 地公第1解答科目フラグを返します。
     *
     * @return 地公第1解答科目フラグ
     */
    public boolean isFirstAnsFlgSoc() {
        return firstAnsFlgSoc;
    }

    /**
     * 地公第1解答科目フラグをセットします。
     *
     * @param firstAnsFlgSoc 地公第1解答科目フラグ
     */
    public void setFirstAnsFlgSoc(boolean firstAnsFlgSoc) {
        this.firstAnsFlgSoc = firstAnsFlgSoc;
    }

    /**
     * センタ配点を返します。
     *
     * @return センタ配点
     */
    public String getAllot1() {
        return allot1;
    }

    /**
     * センタ配点をセットします。
     *
     * @param allot1 センタ配点
     */
    public void setAllot1(String allot1) {
        this.allot1 = allot1;
    }

    /**
     * 二次課しフラグを返します。
     *
     * @return 二次課しフラグ
     */
    public boolean isSExist() {
        return isSExist;
    }

    /**
     * 二次課しフラグをセットします。
     *
     * @param isSExist 二次課しフラグ
     */
    public void setSExist(boolean isSExist) {
        this.isSExist = isSExist;
    }

    /**
     * 二次試験科目を返します。
     *
     * @return 二次試験科目
     */
    public String getS_subString() {
        return s_subString;
    }

    /**
     * 二次試験科目をセットします。
     *
     * @param s_subString 二次試験科目
     */
    public void setS_subString(String s_subString) {
        this.s_subString = s_subString;
    }

    /**
     * 二次配点を返します。
     *
     * @return 二次配点
     */
    public String getAllot2() {
        return allot2;
    }

    /**
     * 二次配点をセットします。
     *
     * @param allot2 二次配点
     */
    public void setAllot2(String allot2) {
        this.allot2 = allot2;
    }

    /**
     * centerAllotを返します。
     *
     * @return centerAllot
     */
    public Map<COURSE_ALLOT, String> getCenterAllot() {
        return centerAllot;
    }

    /**
     * centerAllotを設定します。
     *
     * @param centerAllot centerAllot
     */
    public void setCenterAllot(Map<COURSE_ALLOT, String> centerAllot) {
        this.centerAllot = centerAllot;
    }

    /**
     * secondAllotを返します。
     *
     * @return secondAllot
     */
    public Map<COURSE_ALLOT, String> getSecondAllot() {
        return secondAllot;
    }

    /**
     * secondAllotを設定します。
     *
     * @param secondAllot secondAllot
     */
    public void setSecondAllot(Map<COURSE_ALLOT, String> secondAllot) {
        this.secondAllot = secondAllot;
    }

    /**
     * 共通テストの教科配点を返します。
     *
     * @param course 教科
     * @return 教科配点
     */
    public String getCenterAllot(final COURSE_ALLOT course) {
        String result = "-";
        if (centerAllot != null && centerAllot.containsKey(course)) {
            result = centerAllot.get(course);
        }
        return result;
    }

    /**
     * 二次の教科配点を返します。
     *
     * @param course 教科
     * @return 教科配点
     */
    public String getSecondAllot(final COURSE_ALLOT course) {
        String result = "-";
        if (secondAllot != null && secondAllot.containsKey(course)) {
            result = secondAllot.get(course);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("枝番名称=").append(branchName);
        sb.append("/センタ課しフラグ=").append(isCExist);
        sb.append("/センタ教科数=").append(cenCourseNum);
        sb.append("/センタ試験科目=").append(c_subString);
        sb.append("/理科第1解答科目フラグ=").append(firstAnsFlgSci);
        sb.append("/地公第1解答科目フラグ=").append(firstAnsFlgSoc);
        sb.append("/センタ配点=").append(allot1);
        sb.append("/センタ配点（総点）=").append(isSExist);
        sb.append("/二次試験科目=").append(s_subString);
        sb.append("/二次配点=").append(allot2);
        return sb.toString();
    }

}

package jp.co.fj.kawaijuku.judgement.beans;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * CEFRマスタの情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
public class CefrBean implements ComboBoxItems, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3295623154501258376L;

    /**
     * コンストラクタです。
     */
    public CefrBean() {
        super();
    }

    /**
     * コンストラクタです。
     * @param cefrCd
     * @param cefrName
     * @param cefrAbbr
     * @param dispSequence
     */
    public CefrBean(String cefrCd, String cefrName, String cefrAbbr, int dispSequence) {
        super();
        this.cefrCd = cefrCd;
        this.cefrName = cefrName;
        this.cefrAbbr = cefrAbbr;
        this.dispSequence = dispSequence;
    }

    /** CEFRレベルコード */
    private String cefrCd;

    /** CEFRレベル正式名 */
    private String cefrName;

    /** CEFRレベル短縮名 */
    private String cefrAbbr;

    /** 並び順 */
    private int dispSequence;

    @Override
    public String getName() {
        return cefrName;
    }

    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}

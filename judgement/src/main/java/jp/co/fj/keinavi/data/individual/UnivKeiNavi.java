package jp.co.fj.keinavi.data.individual;

import jp.co.fj.kawaijuku.judgement.data.Univ;

/**
 *
 * 大学基本情報インターフェース（KeiNavi用）
 *
 *
 * @deprecated use {@link jp.co.fj.kawaijuku.judgement.data.UnivData} instead
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface UnivKeiNavi extends Univ {

    /**
     * 追加インデックスを設定する。
     *
     * @param parseInt 追加インデックス
     * @deprecated 大学インスタンスはスレッド間で共用されるため、ここで設定するのはNG
     */
    void setAddedIndex(int parseInt);

    /**
     * 追加インデックスを返す。
     *
     * @return 追加インデックス
     * @deprecated 大学インスタンスはスレッド間で共用されるため、利用してはいけない
     */
    int getAddedIndex();

    /**
     * 大学名を返す。
     *
     * @return 大学名
     * @deprecated use getNameUniv instead
     */
    public String getUnivName();

    /**
     * 学部名を返す。
     *
     * @return 学部名
     * @deprecated use getNameDept instead
     */
    public String getDeptName();

    /**
     * 学科名を返す。
     *
     * @return 学科名
     * @deprecated use getNameDept instead
     */
    public String getSubName();

}

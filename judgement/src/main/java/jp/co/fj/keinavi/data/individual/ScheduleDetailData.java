package jp.co.fj.keinavi.data.individual;

import java.util.Date;

/**
 *
 * 入試日程詳細データ保持用データクラス
 *
 * @deprecated use jp.fujitsu.keinet.mst.db.model.ScheduleDetailData instead
 *
 */
public class ScheduleDetailData extends jp.fujitsu.keinet.mst.db.model.ScheduleDetailData {

    /** serialVersionUID */
    private static final long serialVersionUID = 5672818713333178751L;

    /**
     * コンストラクタ
     * @deprecated
     */
    public ScheduleDetailData(Date entExamInpleDate11, Date entExamInpleDate12, Date entExamInpleDate13,
            Date entExamInpleDate14, String inpleDate11, String inpleDate12, String inpleDate13,
            Date entExamInpleDate21, Date entExamInpleDate22, Date entExamInpleDate23, Date entExamInpleDate24,
            String inpleDate21, String inpleDate22, String inpleDate23, String union, String branchName, int div1,
            int div2, int div3) {
        super(entExamInpleDate11, entExamInpleDate12, entExamInpleDate13, entExamInpleDate14, inpleDate11, inpleDate12,
                inpleDate13, entExamInpleDate21, entExamInpleDate22, entExamInpleDate23, entExamInpleDate24,
                inpleDate21, inpleDate22, inpleDate23, union, branchName, div1, div2, div3);
    }

    /**
     * コンストラクタ
     * @deprecated
     */
    public ScheduleDetailData(int div1, int div2, int div3) {
        super(div1, div2, div3);
    }

}

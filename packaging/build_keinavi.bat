@echo off

set JAVA_HOME=D:\keinet_project\java\11
set REPOS=%~dp0..
set PACKAGING=%REPOS%\packaging
set KEINAVI=%REPOS%\keinavi\target\keinavi\

rem モジュールをパッケージングする
cd %PACKAGING%
cmd /c D:\keinet_project\maven\apache-maven-3.6.2\bin\mvn -f %PACKAGING%\pom_keinavi.xml -P release clean package

pause

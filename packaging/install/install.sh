#!/bin/sh
mvn install:install-file -DgroupId=hmzip14 -DartifactId=hmzip14 -Dversion=1.0 -Dpackaging=jar -Dfile=hmzip14.jar
mvn install:install-file -DgroupId=poi -DartifactId=poi -Dversion=2.0-RC1-20031102 -Dpackaging=jar -Dfile=poi.jar
mvn install:install-file -DgroupId=poi-contrib -DartifactId=poi-contrib -Dversion=2.0-RC1-20031102 -Dpackaging=jar -Dfile=poi-contrib.jar
mvn install:install-file -DgroupId=poi-scratchpad -DartifactId=poi-scratchpad -Dversion=2.0-RC1-20031102 -Dpackaging=jar -Dfile=poi-scratchpad.jar
mvn install:install-file -DgroupId=mail -DartifactId=mail -Dversion=1.3.3_01 -Dpackaging=jar -Dfile=mail.jar
mvn install:install-file -DgroupId=ojdbc14 -DartifactId=ojdbc14 -Dversion=9.0.2.0.0 -Dpackaging=jar -Dfile=ojdbc14.jar
mvn install:install-file -DgroupId=ojdbc6 -DartifactId=ojdbc6 -Dversion=11.2.0.2.0 -Dpackaging=jar -Dfile=ojdbc6.jar
mvn install:install-file -DgroupId=totec -DartifactId=totec -Dversion=1.0 -Dpackaging=jar -Dfile=totec.jar
mvn install:install-file -DgroupId=jaxrpc -DartifactId=jaxrpc -Dversion=1.1 -Dpackaging=jar -Dfile=jaxrpc.jar
mvn install:install-file -DgroupId=jp.ac.kawai-juku.keinet -DartifactId=auth -Dversion=1.00 -Dpackaging=jar -Dfile=auth.jar
mvn install:install-file -DgroupId=jp.ac.kawai-juku.keinet -DartifactId=JSPDev -Dversion=1.00 -Dpackaging=jar -Dfile=JSPDev.jar

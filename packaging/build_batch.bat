@echo off
setlocal enabledelayedexpansion

set JAVA_HOME=D:\keinet_project\java\11
set BASE_PATH=D:\keinet_project\workspace\

for /D %%d in (%BASE_PATH%F*) do (
	set TGT_PATH=%%d\
	set OUT_PATH=!TGT_PATH!bin\
	if exist !OUT_PATH! (
		rmdir /s /q !OUT_PATH!
	)
	mkdir !OUT_PATH!
	echo ******************** Compile !OUT_PATH! start ********************
	!JAVA_HOME!\bin\javac -nowarn -sourcepath !TGT_PATH!src !TGT_PATH!src\*.java -cp !TGT_PATH!lib\*;!TGT_PATH!*.jar -d !OUT_PATH!
	echo ******************** Compile !OUT_PATH! end   ********************
)

exit /b 0


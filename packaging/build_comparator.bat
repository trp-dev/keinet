@echo off
setlocal enabledelayedexpansion

set JAVA_HOME=D:\keinet_project\java\11
set REPOS=%~dp0..
set PACKAGING=%REPOS%\packaging

rem モジュールをパッケージングする
cd %PACKAGING%
cmd /c D:\keinet_project\maven\apache-maven-3.6.2\bin\mvn -f %PACKAGING%\pom_comparator.xml -P release clean package

exit /b 0


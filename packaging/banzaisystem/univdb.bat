@echo off

set target=%1
:input1
rem ファイル名を指定
if "%target%"=="" (
	set /p target="ファイル名を指定してください(年度＋模試区分＋ファイル連番):"
)
if "%target%"=="" goto :input

set JAVA_HOME=D:\keinet_project\java\11
set BASE=D:\keinet_project\data\setupdb
set ZIPFILE=%BASE%\%target%.zip
set DBFILE=%BASE%\univ%target:~0,6%.db


rem zipを解凍
echo %ZIPFILE%を解凍。
powershell Expand-Archive -Path %ZIPFILE% -DestinationPath %BASE% -Force
del %BASE%\univ.db
move %DBFILE% %BASE%\univ.db
echo.

pause
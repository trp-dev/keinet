@echo off

set JAVA_HOME=D:\keinet_project\java\11

set BASE=D:\keinet_project\workspace
set PACKAGING=%BASE%\packaging\banzaisystem
set TARGET=%BASE%\banzaisystem\target\lib

set OUTPUT=D:\keinet_project\setup
set APPJAR=%OUTPUT%\appDownload
set APPXML=%OUTPUT%\appVersion2.xml

set IS_CMD_BUILD="C:\Program Files (x86)\InstallShield\2020\System\IsCmdBld.exe"
set ISM=%PACKAGING%\InstallShield\banzaisystem.ism

rem 出力フォルダをクリアする
for /D %%f in (%OUTPUT%\*) do rd "%%f" /s /q
set /a counter=0
for /D %%f in (%OUTPUT%\*) do set /a counter=counter+1
if not %counter% == 0 (
    echo 出力フォルダのクリアに失敗しました。
    goto end
)

rem モジュールを更新する
rem cd /d %BASE%
rem svn up

rem モジュールをパッケージングする
cd %PACKAGING%
cmd /c D:\keinet_project\maven\apache-maven-3.6.2\bin\mvn -P release clean package
if not "%ERRORLEVEL%" == "0" goto end

rem バージョンを取得する
for /F %%v in ('findstr /C:"version=" %BASE%\banzaisystem\target\classes\updater.properties') do set VERSION=%%v
set VERSION=%VERSION:~8%

rem インストーラをビルドする
%IS_CMD_BUILD% -p %ISM% -l PRODUCT_VERSION="%VERSION%" -l PATH_TO_LIB_FILES="%TARGET%" -b %OUTPUT%

rem 更新サイトモジュールをコピーする
rem copy %BASE%\banzaisystem-updater\target\banzaisystem-updater.war %OUTPUT%\Package

rem ビルドによって「リリースの場所」が書き換わってしまうため、元に戻す
rem cd /d %BASE%\packaging\banzaisystem\InstallShield
rem svn revert banzaisystem.ism

if EXIST "%APPJAR%\" (
    rem 既存のファイルを削除
    del /Q %APPJAR%\*
) ELSE (
    rem ディレクトリ作成
    mkdir "%APPJAR%\"
)

rem ビルドしたライブラリをコピー
xcopy /Q %TARGET%\* %APPJAR%\
echo [%APPJAR%]にjarファイルをコピーしました。
echo.

rem appVersion2.xmlの生成
rem ヘッダーの出力
echo ^<?xml version="1.0"?^>>%APPXML%
echo ^<app version="%VERSION%"^>>>%APPXML%
echo ^<modules^> >>%APPXML%
rem appDownload配下のjarファイルをループ処理
for %%a in (%APPJAR%\*.jar) do (
    rem jarファイルのサイズ・ファイル名を出力
    echo ^<module size="%%~za"^>%%~nxa^</module^>>>%APPXML%
)
rem フッターの出力
echo ^</modules^>>>%APPXML%
echo ^</app^>>>%APPXML%
echo.
echo [%APPXML%]を作成しました。
echo.
powershell compress-archive -Force -Path %APPJAR%,%APPXML% -DestinationPath %APPJAR%.zip
echo [%APPJAR%.zip]を作成しました。
echo.

:end

pause

start %OUTPUT%

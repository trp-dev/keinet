@echo off

set JAVA_HOME=D:\keinet_project\java\11
set MAVEN_HOME=D:\keinet_project\maven\apache-maven-3.6.2\
set BASE_PATH=%~dp0..
set CURRENT=%~dp0

echo *********************************************
echo *
echo *  1:共通
echo *  2:Kei-Navi
echo *  3:模試判定
echo *  4:iバンザイ
echo *  5:PCバンザイ
echo *  上記以外：全て
echo *
echo *********************************************

set /p TARGET=ビルド対象を入力してください。: 

IF %TARGET%==1 (
	set POM_PATH=%CURRENT%pom_common.xml
) ELSE IF %TARGET%==2 (
	set POM_PATH=%CURRENT%pom_keinavi.xml
) ELSE IF %TARGET%==3 (
	set POM_PATH=%CURRENT%pom_inet.xml
) ELSE IF %TARGET%==4 (
	set POM_PATH=%CURRENT%pom_banzai.xml
) ELSE IF %TARGET%==5 (
	set POM_PATH=%CURRENT%pom_pcbanzai.xml
) ELSE (
	set POM_PATH=%CURRENT%pom.xml
)

rem モジュールをパッケージングする
cd %CURRENT%
cmd /c %MAVEN_HOME%bin\mvn -f %POM_PATH% -P release clean package

pause

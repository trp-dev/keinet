/**
 * クロスブラウザ用ライブラリ
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */

var IE4 = false;
var NN4 = false;
var IE6 = false; // IE5, Opera, NN6, NN7

if (document.layers) NN4 = true;
else if (document.getElementById) IE6 = true;
else if (document.all) IE4 = true;

var VISIBLE = NN4 ? "show" : "visible";
var HIDDEN = NN4 ? "hide" : "hidden";

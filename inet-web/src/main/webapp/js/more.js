$(document).ready(function(){
	//何行ずつ区切るか行数を設定するための変数
	var len = 100;
	//何行目まで表示が進んだか記録するための変数
	var pointer = len;
	//全部で何行あるか最初に記録しておくための変数
	var max = $('#dataMyTable tbody tr').length;
	//最初にlen行目以降を隠す
	$('#dataMyTable tbody tr:gt('+ (len-1) +')').hide();
	//表示する時用関数を用意
	var showMoreLines = function(event){
		//現在行からlen行だけ表示
		$('#dataMyTable tbody tr:lt('+ (pointer+len) +')').fadeIn('slow');
		//現在行の位置を進める
		pointer += len;
		//最終行に到達したら続きをみる削除
		if (pointer > max - 1) {readMore.remove();}
		//リンククリックイベントをキャンセル
		event.preventDefault();
		return false;
	};
	//一行あたりのセル数
	var tdCount = $('#dataMyTable tbody tr:eq(0) td').size();
	//続きをみるをtable最後行の直後に挿入
	var readMore = $('<tr><td id="more" colspan="' + tdCount +'"><a href="#">続きを見る</a></td></tr>');
		readMore.on('click', 'a', showMoreLines);
		$('#dataMyTable tbody  tr:last').after(readMore);
	//maxがlen以下ならreadMore非表示
	if (max < len) {readMore.css("display", "none");};
});
/**
 * DOMユーティリティクラス
 * 要素へのアクセスを楽にしたもの
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */


// コンストラクタ
function DOMUtil(obj) {
	this.obj = obj;
}

function _getParent(index) {
	var parent = this.obj;
	for (var i=0; i<index; i++) {
		parent = parent.parentNode;
	}
	return parent;
}

function _getChild() {
	var child = this.obj;
	for (var i=0; i<arguments.length; i++) {
		child = child.childNodes[arguments[i]];
	}
	return child;
}

// public
DOMUtil.prototype.getParent = _getParent;
DOMUtil.prototype.getChild = _getChild;


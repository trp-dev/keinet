
function openWindow(url, width, height) {
	var style = "resizable=yes,scrollbars=yes,status=yes,titlebar=yes";
	if (width) style += ",width=" + width;
	if (height) style += ",height=" + height;
	return window.open(url, "_blank", style);
}

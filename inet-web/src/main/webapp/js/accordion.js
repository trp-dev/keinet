$(function(){
     
$(".accordion p").click(function(){
    $(this).next("ul").slideToggle();
    $(this).children("span.plus01").toggleClass("minus01");
    $(this).children("span.plus02").toggleClass("minus02");
}); 

 
$(".accordion dt").click(function(){
    $(this).next("dd").slideToggle();
    $(this).next("dd").siblings("dd").slideUp();
    $(this).toggleClass("open");    
    $(this).siblings("dt").removeClass("open");
});
 
});
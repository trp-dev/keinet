$.fn.isVisible = function() {
    return $.expr.filters.visible(this[0]);
};
$(function(){
	/* 成績入力方法：クリック */
	$(".help_edit").click(function() {
		window.open("https://www.keinet.ne.jp/web/hantei/helpindex.html#help-step01")
	});

	/* スコア情報のクリア：クリック */
	$(".clear-cefr").click(function() {
		var fix = getFix(this.id);
		/* 確認メッセージ */
		if (!confirm(getMessage("j007a"))) {
			return;
		}
		clearEngCert(fix);
	});

	/* 入力する：クリック */
	$(".input-cefr").click(function() {
		var fix = getFix(this.id);
		clearEngCert(fix);
		/* 「入力する」ボタンを非表示 */
		$(this).hide();
		/* 「戻る」ボタンを表示 */
		$getById("cancel-cefr-btn", fix).show();
		/* 入力項目を表示 */
		$getById("input-cefr", fix).slideToggle();
	});

	/* 修正する：クリック */
	$(".update-cefr").click(function() {
		var fix = getFix(this.id);
		var data = JSON.parse($("#engCert" + fix.suffix).val());

		$getRadio("engpt", fix).val([data.engPtCd]);
		var $engpt = $getSelectedRadio("engpt", fix);
		if ($engpt.data("level") == "1") {
			/* 試験名に対応した試験レベルを表示 */
			showSelectableEngptLvl(fix, $engpt.val(), true);
			/* 「種類」枠を表示 */
			$getById("engpt-lvl-row", fix).show();
		} else {
			/* 「種類」枠を非表示 */
			$getById("engpt-lvl-row", fix).hide();
		}
		/* 「スコア」枠を表示 */
		$getById("score-row", fix).show();
		/* スコアを入力した場合 */
		if (data.scoreInputFlg === "1") {
			resetScore(fix);
			$getById("cefr-score", fix).val(data.score);
		} else {
			$getById("unknown-score", fix).data("sw", "1").text("※スコア入力に戻る");
			/* 「スコア」のクリア＆非活性 */
			$getById("cefr-score", fix).prop("disabled", true).val("");
			/* 「CEFR」枠を表示 */
			$getById("cefr-lvl-row", fix).show();

			/* CEFRレベルを入力した場合 */
			var $cefr = $getRadio("cefr-lvl", fix)
			if (data.cefrLevelCdInputFlg === "1") {
				/* OFF時 */
				resetCefr(fix);
				/* 「CEFR」の活性／非活性を切り替え */
				$cefr.each(function(i, elem) {
					$(elem).prop("disabled", false);
				});
				$cefr.val([data.cefrLevelCd]);
			} else {
				/* ON時 */
				$getById("unknown-cefr", fix).data("sw", "1").text("※CEFR入力に戻る");
				/* 「合否」枠を表示 */
				if ($engpt.data("result") == "1") {
					$getById("cefr-result-row", fix).show();
					/* 「CEFRが不明な方：合否を入力」リンクを表示 */
					$getById("unknown-cefr", fix).show();
				} else {
					/* 「CEFRが不明な方：合否を入力」リンクを非表示 */
					$getById("unknown-cefr", fix).hide();
				}
				/* 「CEFR」の活性／非活性を切り替え */
				$cefr.each(function(i, elem) {
					$(elem).prop("disabled", true).prop("checked", false);
				});
				var $result = $getRadio("cefr-result", fix)
				$result.val([data.result]);
			}
		}
		/* 「修正する」ボタンを非表示 */
		$(this).hide();
		/* 「戻る」ボタンを表示 */
		$getById("cancel-cefr-btn", fix).show();
		/* 入力項目を表示 */
		$getById("input-cefr", fix).slideToggle();
	});

	/* 戻る：クリック */
	$(".cancel-cefr").click(function() {
		var fix = getFix(this.id);
		/* 「戻る」ボタンを非表示 */
		$(this).hide();
		/* 「入力する」ボタンを表示 */
		var isInput = $getById("cefr-status", fix).val();
		if (isInput === "1") {
			$getById("update-cefr-btn", fix).show();
		} else {
			$getById("input-cefr-btn", fix).show();
		}
		/* 入力項目を非表示 */
		$getById("input-cefr", fix).slideToggle();

	});

	/* 試験名ラジオボタン：クリック */
	$(".cefr-engpt").change(function() {
		var fix = getFix(this.name);
		/* 選択された試験名を取得 */
		var $engpt = $(this);

		if ($engpt.data("level") == "1") {
			/* 試験名に対応した試験レベルを表示 */
			showSelectableEngptLvl(fix, $engpt.val(), false);
			/* 「種類」枠を表示 */
			$getById("engpt-lvl-row", fix).show();
			/* 「スコア」枠を非表示 */
			$getById("score-row", fix).hide();
		} else {
			/* 「種類」枠を非表示 */
			$getById("engpt-lvl-row", fix).hide();
			/* 「スコア」枠を表示 */
			$getById("score-row", fix).show();
		}
		/* 「スコア」、「CEFR」、「合否」をクリア */
		resetScore(fix);
		if ($engpt.data("result") == "1") {
			/* 「CEFRが不明な方：合否を入力」リンクを表示 */
			$getById("unknown-cefr", fix).show();
		} else {
			/* 「CEFRが不明な方：合否を入力」リンクを非表示 */
			$getById("unknown-cefr", fix).hide();
		}
	});

	/* 種類：クリック */
	$(".cefr-engpt-lvl").change(function() {
		var fix = getFix(this.name);
		/* 「スコア」枠を表示 */
		$getById("score-row", fix).show();
	});

	/* スコアが不明：クリック */
	$(".unknown-score").click(function() {
		var fix = getFix(this.id);
		if ($(this).data("sw") === "1") {
			/* OFF時 */
			resetScore(fix);
		} else {
			/* ON時 */
			$(this).data("sw", "1").text("※スコア入力に戻る");
			/* 「スコア」のクリア＆非活性 */
			$getById("cefr-score", fix).prop("disabled", true).val("");
			/* 「CEFR」枠を表示 */
			$getById("cefr-lvl-row", fix).show();
			resetCefr(fix);
		}
	});

	/* CEFRが不明：クリック */
	$(".unknown-cefr").click(function() {
		var fix = getFix(this.id);
		var disabled;
		if ($(this).data("sw") === "1") {
			/* OFF時 */
			resetCefr(fix);
			disabled = false;
		} else {
			/* ON時 */
			$(this).data("sw", "1").text("※CEFR入力に戻る");
			/* 「合否」枠を表示 */
			$getById("cefr-result-row", fix).show();
			disabled = true;
		}
		/* 「CEFR」の活性／非活性を切り替え */
		var $cefr = $getRadio("cefr-lvl", fix)
		$cefr.each(function(i, elem) {
			$(elem).prop("disabled", disabled).prop("checked", false);
		});
	});

	/* 設定する：クリック */
	$(".apply-cefr").click(function() {
		var fix = getFix(this.id);

		/* エラーをクリア */
		$getById("engcert-error", fix).text("");

		/* POSTデータを生成 */
		var data = createEngCertData(fix);
		$("#engCert" + fix.suffix).val(data);
		var postData = "forward=&suffix=" + fix.suffix + "&engCert=" + data;

		//ajaxでservletにリクエストを送信
		$.ajax({
			type    : "POST",          //GET / POST
			url     : "InputEngCert",  //送信先のServlet URL
			data    : postData,        //リクエストJSON
			async   : true,           //true:非同期(デフォルト), false:同期
			success : function(data, dataType) {
				showEngCert(data, fix);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("リクエスト時にエラーが発生しました。");
			}
		});
	});
});

/**
 * 英語認定試験情報をクリアします。
 *
 * @param {FIX情報} fix
 */
function clearEngCert(fix) {
	$getById("input-cefr-btn", fix).show();
	$getById("update-cefr-btn", fix).hide();
	$getById("cancel-cefr-btn", fix).hide();
	/* 入力項目を非表示 */
	$getById("input-cefr", fix).slideUp();
	/* 「種類」枠を非表示 */
	$getById("engpt-lvl-row", fix).hide();
	/* 「スコア」枠を非表示 */
	$getById("score-row", fix).hide();
	/* 「CEFR」枠を非表示 */
	$getById("cefr-lvl-row", fix).hide();
	/* 「合否」枠を非表示 */
	$getById("cefr-result-row", fix).hide();
	/* 内容をクリア */
	var $engpt = $getRadio("engpt", fix);
	$engpt.each(function(i, elem) {
		$(elem).prop("checked", false);
	});
	$getById("cefr-status", fix).val("");
	$getById("engcert-error", fix).text("");
	/* 表示項目をクリア */
	$getById("disp-cefr-exma", fix).text("−");
	$getById("disp-cefr-score", fix).text("−");
	$getById("disp-cefr-level", fix).text("−");
	$getById("disp-cefr-result", fix).text("−");
	$("#engCert" + fix.suffix).val(JSON.stringify({}));
	$("input[name=changed]").val("true");
}

/**
 * サーバーから返却された情報を画面に設定します。
 *
 * @param {responseデータ} data
 * @param {FIX情報} fix
 */
function showEngCert(data, fix) {

	switch (data.errorCode) {
		case 0:
			/* 入力値を画面に設定 */
			$getById("disp-cefr-exma", fix).text(data.dispEngPtName);
			$getById("disp-cefr-score", fix).text(data.dispScore);
			$getById("disp-cefr-level", fix).text(data.dispCefrLevel);
			$getById("disp-cefr-result", fix).text(data.dispResult);
			$getById("cefr-status", fix).val(data.entryFlg);
			/* 入力項目を非表示 */
			$getById("input-cefr", fix).slideUp();
			/* 「戻る」ボタンを非表示 */
			$getById("cancel-cefr-btn", fix).hide();
			/* 「修正する」ボタンを表示 */
			$getById("update-cefr-btn", fix).show();
			if (!$("#pc").is(":visible")) {
				var position = $getById("disp-cefr", fix).offset().top;
				$('body,html').animate({scrollTop:position}, 400, 'swing');
			}
			$("input[name=changed]").val("true");
			break;
		case 1:
			clearEngCert(fix);
			if (!$("#pc").is(":visible")) {
				var position = $getById("disp-cefr", fix).offset().top;
				$('body,html').animate({scrollTop:position}, 400, 'swing');
			}
			break;
		case 2:
			$getById("engcert-error", fix).text(getMessage("j032a"));
			break;
		case 3:
			$getById("engcert-error", fix).text(getMessage("j033a"));
			break;
	}
}

function createPostData() {
	var pre = $("#pc").is(":visible") ? "pc-" : "sp-";
	var q1, q2, q3, q4
	q1 = $("input[name=" + pre + "q1]:checked").val();
	q2 = $("input[name=" + pre + "q2]:checked").val();
	q3 = $("input[name=" + pre + "q3]:checked").val();
	q4 = $("input[name=" + pre + "q4]:checked").val();
	$("#t1JaDescQ1").val(q1);
	$("#t1JaDescQ2").val(q2);
	$("#t1JaDescQ3").val(q3);
	$("#t1JaDescQ4").val(q4);
	$("#engCert01").val(createEngCertData({ sign: "#", prefix: pre, suffix: "01"}));
	$("#engCert02").val(createEngCertData({ sign: "#", prefix: pre, suffix: "02"}));

}
/**
 * 英語認定試験のPOSTデータを生成します。
 *
 * @param {FIX情報} fix
 */
function createEngCertData(fix) {
	/* 入力値を取得 */
	var $engpt = $getSelectedRadio("engpt", fix);
	var $engptLvl = $getSelectedRadio("engpt-lvl", fix);
	var score = $getById("cefr-score", fix).val();
	var $cefr = $getSelectedRadio("cefr-lvl", fix);
	var $result = $getSelectedRadio("cefr-result", fix);

	/* POSTデータを生成 */
	var data = {};
	if ($engpt.length === 1) {
		data.engPtCd = $engpt.val();
		if ($engptLvl.length === 1) {
			data.engPtLevelCd = $engptLvl.val();
		}
		if (score !== "") {
			data.score = score;
			data.scoreInputFlg = "1";
		}
		if ($cefr.length === 1) {
			data.cefrLevelCd = $cefr.val();
			data.cefrLevelCdInputFlg = "1";
		}
		if ($result.length === 1) {
			data.result = $result.val();
			data.resultInputFlg = "1";
		}
	}

	return JSON.stringify(data);
}

/**
 * 選択された試験名に該当する種類を表示を行います。
 *
 * @param {FIX情報} fix
 */
function showSelectableEngptLvl(fix, engptCd, isCheckedKeep) {
	/* 種類を取得 */
	var $engptLvl = $getRadio("engpt-lvl", fix);
	var count = 0;
	$engptLvl.each(function(i, elem) {
		/* 選択された試験名に該当する種類を表示 */
		if ($(elem).data("engptcd") == engptCd) {
			count ++;
			$(elem).parent().show();
			/* IEだと非表示分が考慮されずCSSが適用されないため、コードで設定 */
			if (count === 8 && fix.prefix == "pc-") {
				$(elem).parent().css("clear","left");
			}
		} else {
			$(elem).parent().hide();
		}
		/* チェックはOFFにする */
		if (!isCheckedKeep) {
			$(elem).prop("checked", false);
		}
	});
}

/**
 * スコア、CEFR、合否の初期化を行います。
 *
 * @param {FIX情報} fix
 */
function resetScore(fix) {
	$getById("unknown-score", fix).data("sw", "0").text("※スコアが不明な方：CEFRを入力");
	/* 「スコア」のクリア＆活性 */
	$getById("cefr-score", fix).prop("disabled", false).val("");
	/* 「CEFR」枠を非表示 */
	$getById("cefr-lvl-row", fix).hide();
	resetCefr(fix);
}

/**
 * CEFR、合否の初期化を行います。
 *
 * @param {FIX情報} fix
 */
function resetCefr(fix) {
	/* 「CEFR」の選択状態をクリア */
	var $cefr = $getRadio("cefr-lvl", fix)
	$cefr.each(function(i, elem) {
		$(elem).prop("checked", false).prop("disabled", false);
	});
	$getById("unknown-cefr", fix).data("sw", "0").text("※CEFRが不明な方：合否を入力");
	/* 「合否」枠を非表示 */
	$getById("cefr-result-row", fix).hide();
	var $result = $getRadio("cefr-result", fix);
	$result.each(function(i, elem) {
		$(elem).prop("checked", false);
	});
}

/**
 * 国語記述式の入力チェックを行います。
 *
 */
function checkJpnDesc() {
	$(".Ja0ScoreError").text("");
	var q1, q2, q3, q4;
	var pre = $("#pc").is(":visible") ? "pc" : "sp";
	q1 = $("input[name=" + pre + "-q1]:checked").val();
	q2 = $("input[name=" + pre + "-q2]:checked").val();
	q3 = $("input[name=" + pre + "-q3]:checked").val();
	q4 = $("input[name=" + pre + "-q4]:checked").val();

	/* すべて未選択はOK */
	if (q1 === "" && q2 === "" && q3 === "" && q4 === "") {
		return true;
	}
	/* すべて選択はOK */
	if (q1 !== "" && q2 !== "" && q3 !== "" && q4 !== "") {
		return true;
	}
	/* 上記以外の場合、エラー */
	$(".Ja0ScoreError").text(getMessage("j034a"));
	valid = false;
}

/**
 * コントロールのFIX情報を取得します。
 *
 * @param {ID} id
 */
function getFix(id) {
	var data = {
			sign: "#",
			prefix: id.slice(0, 3),
			suffix: id.slice(-2)
		};
	return data;
}

/**
 * FIX情報を加味したコントロールIDを生成します。
 *
 * @param {ID} id
 * @param {FIX情報} fix
 */
function $getById(id, fix) {
	return $(fix.sign + fix.prefix + id + fix.suffix);
}

/**
 * FIX情報を加味したコントロール名を生成します。
 *
 * @param {ID} id
 * @param {FIX情報} fix
 */
function getByName(name, fix) {
	return fix.prefix + name + fix.suffix;
}

/**
 * 値が空かどうか評価します。
 *
 * @param {値} value
 */
function isEmpty(value) {
	return (value === undefined || value === null || value.length === 0);
}

/**
 * 選択されているラジオボタンを取得します。
 *
 * @param {ID} id
 * @param {FIX情報} fix
 */
function $getSelectedRadio(id, fix) {
	return $("input[name=" + getByName(id, fix) + "]:checked");
}

/**
 * ラジオボタンを取得します。
 *
 * @param {ID} id
 * @param {FIX情報} fix
 */
function $getRadio(id, fix) {
	return $("input[name=" + getByName(id, fix) + "]");
}

/**
 * メッセージを取得します。
 *
 * @param {メッセージID} id
 */
function getMessage(id) {
	var result = "";
	$("input[name='message']").val().split(";").forEach(function(value) {
		var msg = value.split(":");
		if (id == msg[0]) {
			result = msg[1];
			return;
		}
	});
	return result;
}

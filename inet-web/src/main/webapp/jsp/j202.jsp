<!DOCTYPE html>
<%@ page contentType="text/html;charset=MS932" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="jp.co.fj.keinavi.util.individual.iprops" var="bundle" />
<c:set var="DispMax"><fmt:message key="DISP_MAX_RESULT" bundle="${bundle}" /></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
	<title>大学選択（条件指定）	模試判定システム</title>
	<link rel="stylesheet" href="./shared_lib/style/normalize.css">
	<link rel="stylesheet" href="./shared_lib/style/common.css">
	<link rel="stylesheet" href="./shared_lib/style/button.css">
	<link rel="stylesheet" href="./shared_lib/style/buttonsp.css" media="(max-width:640px)">
	<link rel="stylesheet" href="./shared_lib/style/sp.css" media="(max-width:640px)">
	<link rel="stylesheet" href="./shared_lib/style/j202pc.css">
	<link rel="stylesheet" href="./shared_lib/style/j202sp.css" media="(max-width:640px)">
	<!--[if lt IE 9]>
	<script src="./js/html5shiv-printshiv.js" type="text/javascript"></script>
	<link rel="stylesheet" href="./shared_lib/style/ie8.css">
	<![endif]-->
	<script type="text/javascript" src="./js/jquery-1.7.min.js"></script>
	<script src="./js/jquery.smoothScroll.js" type="text/javascript"></script>
	<script src="./js/accordion.js" type="text/javascript"></script>
	<script type="text/javascript">
	<!--
		<%@ include file="/jsp/script/submit_exit.jsp" %>
		<%@ include file="/jsp/script/area_script.jsp" %>

        // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
        if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
            document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
        }

		<%-- 画面を遷移する --%>
		function submitMenu(forward) {
			document.forms[0].forward.value = forward;
			document.forms[0].backward.value = "j202";
			document.forms[0].target = "_self";
			document.forms[0].submit();
		}

		<%-- 条件リセット --%>
		function submitReset() {
			if (confirm("<kn:message id="j013a" />")) {
				document.forms[0].button.value = "reset";
				submitMenu("j202");
			}
		}

		/**
	 	* 判定
	 	*/
		function submitJudge(raingDivValue) {
			document.forms[0].button.value = "judge";
			document.forms[0].raingDiv.value = raingDivValue;
			submitMenu('j204');
		}

		/**
	 	* フォームデフォルト設定
	 	*/
	 	function restoreForm(){
	 		var form = document.forms[0];
			<%--schoolDivProviso--%>
			var schoolDivProviso = form.elements["schoolDivProviso"];
			var schoolDivProvisoSP = form.elements["schoolDivProvisoSP"];
			for(var i=0; i<schoolDivProviso.length; i++){
				<c:forEach var="sdp" items="${J202Form.schoolDivProviso}">if(schoolDivProviso[i].value == '<c:out value="${sdp}"/>')schoolDivProviso[i].checked = true;</c:forEach>
				<c:forEach var="sdp" items="${J202Form.schoolDivProviso}">if(schoolDivProvisoSP[i].value == '<c:out value="${sdp}"/>')schoolDivProvisoSP[i].checked = true;</c:forEach>
			}
			<%--raingProviso--%>
			var raingProviso = form.elements["raingProviso"];
			var raingProvisoSP = form.elements["raingProvisoSP"];
			for(var i=0; i<raingProviso.length; i++){
				<c:forEach var="sdp" items="${J202Form.raingProviso}">if(raingProviso[i].value == '<c:out value="${sdp}"/>')raingProviso[i].checked = true;</c:forEach>
				<c:forEach var="sdp" items="${J202Form.raingProviso}">if(raingProvisoSP[i].value == '<c:out value="${sdp}"/>')raingProvisoSP[i].checked = true;</c:forEach>
			}
			<%--outOfTergetProviso--%>
			var outOfTergetProviso = form.elements["outOfTergetProviso"];
			var outOfTergetProvisoSP = form.elements["outOfTergetProvisoSP"];
			for(var i=0; i<outOfTergetProviso.length; i++){
				<c:forEach var="sdp" items="${J202Form.outOfTergetProviso}">if(outOfTergetProviso[i].value == '<c:out value="${sdp}"/>')outOfTergetProviso[i].checked = true;</c:forEach>
				<c:forEach var="sdp" items="${J202Form.outOfTergetProviso}">if(outOfTergetProvisoSP[i].value == '<c:out value="${sdp}"/>')outOfTergetProvisoSP[i].checked = true;</c:forEach>
			}
            <%--allotPntRatio--%>
            var allotPntRatio = form.elements["allotPntRatio"];
            var allotPntRatioSP = form.elements["allotPntRatioSP"];
            for(var i=0; i<allotPntRatio.length; i++){
                <c:forEach var="sdp" items="${J202Form.allotPntRatio}">if(allotPntRatio[i].value == '<c:out value="${sdp}"/>')allotPntRatio[i].checked = true;</c:forEach>
                <c:forEach var="sdp" items="${J202Form.allotPntRatio}">if(allotPntRatioSP[i].value == '<c:out value="${sdp}"/>')allotPntRatioSP[i].checked = true;</c:forEach>
            }
	 	}

		// 学部系統用配列
	 	var data1 = new Array();
		data1["0011"] = new Array("0100", "0200", "0300", "0400", "0500", "0600", "0700", "0800", "0900");
		data1["0021"] = new Array("1000", "1100", "1200", "1300", "1400");
		data1["0022"] = new Array("1500", "1600");
		data1["0023"] = new Array("1700", "1800", "1900", "2000");
		data1["0031"] = new Array("2100", "2200", "2300", "2400", "2500", "2600");
		data1["0032"] = new Array("2700", "2800", "2900", "3000", "3100", "3200", "3300");
		data1["0041"] = new Array("3400", "3500", "3600", "3700", "3800");
		data1["0042"] = new Array("3900", "4000", "4100", "4200", "4300", "4400", "4500", "4600", "4700", "4800", "4900", "5000", "5100", "5200");
		data1["0043"] = new Array("5300", "5400", "5500", "5600", "5700", "5800");
		data1["0051"] = new Array("5900", "6000", "6100", "6200", "6300", "6400");
		data1["0061"] = new Array("6500", "6600", "6700", "6800", "6900");
		data1["0062"] = new Array("7400", "7500", "7600", "7700", "7800", "7900");
		data1["0071"] = new Array("7000", "7100", "7200", "7300");

		// 子チェックボックスに自動でチェックをつける
		function checkChild(obj){
			obj.className = "";
			var form = document.forms[0];
			var el;
			if (obj.name.indexOf('SP') != -1) {
				el = form.elements["stemma2Code"];
			} else {
				el = form.elements["stemma2CodeSP"];
			}

			for (var i=0; i<el.length; i++) {
				if (obj.value == el[i].value) el[i].checked = obj.checked;
			}

			var block = data1[obj.value];
			var elements = form.elements["stemmaCode"];
			var elementsSP = form.elements["stemmaCodeSP"];
			for (var i=0; i<elements.length; i++) {
				for (var j=0; j<block.length; j++) {
					if (block[j] == elements[i].value) {
						elements[i].checked = obj.checked;
						elementsSP[i].checked = obj.checked;
					}
				}
			}

			accordiontext();

		}

		// 自分のチェックをつけた際に、兄弟もすべてついていたら親もつける
		function checkParent(obj){
			var form = document.forms[0];
			var el;
			if (obj.name.indexOf('SP') != -1) {
				el = form.elements["stemmaCode"];
			} else {
				el = form.elements["stemmaCodeSP"];
			}

			for (var i=0; i<el.length; i++) {
				if (obj.value == el[i].value) el[i].checked = obj.checked;
			}

			var e1 = form.elements["stemma2Code"];
			var e2 = form.elements["stemmaCode"];
			var e3 = form.elements["stemma2CodeSP"];
			var block = "";
			for (var i=0; i<e1.length; i++) {
				var pref = data1[e1[i].value];
				for (var j=0; j<pref.length; j++) {
					if (pref[j] == obj.value) {
						block = e1[i].value;
						break;
					}
				}
			}

			var flag = true;
			var checkedflg = false;
			var noncheckedflg = false;
			for (var i=0; i<e2.length; i++) {
				var pref = data1[block];
				for (var j=0; j<pref.length; j++) {
					if (pref[j] == e2[i].value) {
						if (!e2[i].checked) {
							flag = false;
							noncheckedflg = true;
						} else {
							checkedflg = true;
						}
					}
				}
			}

			for (var i=0; i<e1.length; i++) {
				if (e1[i].value == block) {
					e1[i].checked = flag;
					e3[i].checked = flag;
					if (checkedflg && noncheckedflg) {
						e3[i].className = "innerCheck";
						e3[i].checked = true;
					} else {
						e3[i].className = "";
					}
				}
			}

			accordiontext();
		}

		// 値を変更したときのアクション
	    function changeForm(obj) {
	    	var form = document.forms[0];
	        // PCとSPの値の同期
	        var objname = obj.name;
	        var indexSP = objname.indexOf("SP");
	        var obj1 = "";
	        //　反映元がPCかSPかを判定し、反映先の情報を取得
	        if (indexSP >= 0) {
	        	// 判定元がSPの場合
	        	var obj1name = objname.split("SP").join("");
	        	obj1 = form.elements[obj1name];
	        } else {
	        	// 判定元がPCの場合
	        	obj1 = form.elements[obj.name + "SP"];
	        }

	        var objre = form.elements[objname];
			if (obj1) {
	        	for(var i=0;i<objre.length;i++) {
	        		obj1[i].checked = objre[i].checked;
	        	}
			}

			accordiontext();
		}

	 	// 二次・夜間主を選択した時の処理
	    function checkYakan(obj) {
	    	var form = document.forms[0];
	        // PCとSPの値の同期
	        var objname = obj.name;
	        var indexSP = objname.indexOf("SP");
	        var obj1 = "";
	        //　反映元がPCかSPかを判定し、反映先の情報を取得
	        if (indexSP >= 0) {
	        	// 判定元がSPの場合
	        	var obj1name = objname.split("SP").join("");
	        	obj1 = form.elements[obj1name];
	        } else {
	        	// 判定元がPCの場合
	        	obj1 = form.elements[obj.name + "SP"];
	        }

	        var objre = form.elements[objname];

	        // 夜間主(非表示項目)のチェック状態を変更
	        for(var i=0;i<objre.length;i++) {
	        	if (objre[i].value == "3") {
		        	objre[i].checked = obj.checked;
		        	obj1[i].checked = obj.checked;
	        	}
			}
		}

	 	/*
	 	 * アコーディオンの指定あり・指定なしの判定
	 	 */
	 	function accordiontext() {
			var checkedflg = false;
			var form = document.forms[0];
			var el = form.elements["stemmaCode"];
			for (var i=0; i<el.length; i++) {
				if (el[i].checked) {
					checkedflg = true;
				}
			}

			if (checkedflg) {
				document.all.keitouCheck.innerHTML = "指定あり";
			} else {
				document.all.keitouCheck.innerHTML = "指定なし";
			}
            // 地区
			checkedflg = false;
			el = form.elements["pref"];
			for (var i=0; i<el.length; i++) {
				if (el[i].checked) {
					checkedflg = true;
				}
			}
			if (checkedflg) {
				document.all.chikuCheck.innerHTML = "指定あり";
			} else {
				document.all.chikuCheck.innerHTML = "指定なし";
			}
            // 配点比率
            el = form.elements["allotPntRatio"];
            if (el[0].checked) {
                document.all.hiritsuCheck.innerHTML = "指定なし";
            } else {
                document.all.hiritsuCheck.innerHTML = "指定あり";
            }
            // 評価範囲
			checkedflg = false;
			el = form.elements["raingProviso"];
			for (var i=0; i<el.length; i++) {
				if (el[i].checked) {
					checkedflg = true;
				}
			}
			if (checkedflg) {
				document.all.hyokaCheck.innerHTML = "指定あり";
			} else {
				document.all.hyokaCheck.innerHTML = "指定なし";
			}
            // 対象外
			checkedflg = false;
			el = form.elements["outOfTergetProviso"];
			for (var i=0; i<el.length; i++) {
				if (el[i].checked) {
					checkedflg = true;
				}
			}
			if (checkedflg) {
				document.all.taisyogaiCheck.innerHTML = "指定あり";
			} else {
				document.all.taisyogaiCheck.innerHTML = "指定なし";
			}
 		}

	 	function accordionCheckBox() {

			var form = document.forms[0];
			// 学部系統
			var e1 = form.elements["stemma2CodeSP"];
			var e2 = form.elements["stemmaCodeSP"];
			for (var i=0; i<e1.length; i++) {
				var checkedflg = false;
				var noncheckedflg = false;
				var listEl = data1[e1[i].value];
				for (var j=0; j<listEl.length; j++) {
					for (var k=0; k<e2.length; k++) {
						if (listEl[j] == e2[k].value) {
							if (e2[k].checked) {
								checkedflg = true;
							} else {
								noncheckedflg = true;
							}
						}
					}
				}

				if (checkedflg && noncheckedflg) {
					e1[i].className = "innerCheck";
					e1[i].checked = true;
				}
			}

			// 地区
			var e1 = form.elements["blockSP"];
			var e2 = form.elements["prefSP"];
			for (var i=0; i<e1.length; i++) {
				var checkedflg = false;
				var noncheckedflg = false;
				var listEl = data[e1[i].value];
				for (var j=0; j<listEl.length; j++) {
					for (var k=0; k<e2.length; k++) {
						if (listEl[j] == e2[k].value) {
							if (e2[k].checked) {
								checkedflg = true;
							} else {
								noncheckedflg = true;
							}
						}
					}
				}

				if (checkedflg && noncheckedflg) {
					e1[i].className = "innerCheck";
					e1[i].checked = true;
				}
			}
	 	}

		/**
	 	* 初期化
		 */
		var ps, ps2;//コンボボックスグローバル化
		function init(){
			restoreForm();
			checkZenchiku();
			// アコーディオンの指定あり・指定なしの判定
			accordiontext();
			// 学部系統、地区で子が一部チェックされている場合の親のスタイル変更
			accordionCheckBox();

			<%@ include file="/jsp/script/loading.jsp" %>

		}

	// -->
	</script>
</head>
	<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
	<div id="wrapper">
	<form action="<c:url value="UnivCond" />" method="POST">
	<input type="hidden" name="forward" value="">
	<input type="hidden" name="backward" value="">
	<input type="hidden" name="button" value="">
	<input type="hidden" name="thisYear" value="<c:out value="${J202Form.thisYear}" />">
	<input type="hidden" name="searchStemmaDiv" value="3">
	<input type="hidden" name="raingDiv" value="3">

	<%@ include file="/jsp/include/header.jsp" %>

	<div id="LoadingLayer">
		<!-- コンテンツここから -->
			<div class="container">
				<h3 class="title01">ロード中</h3>
				<div class="contents">
					<div class="contents-inner">
						<div class="onjudge">
							<p>ロード中です ...</p>
						<!-- /.onjudge --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents --></div>

			<!-- /.container --></div>
	</div>
<%-- i202_BEGIN --%>
	<div id="MainLayer" style="display:none;">

	<!-- コンテンツここから -->
			<div class="container">


				<div class="contents">
					<h3 class="title01">判定大学を指定する</h3>

					<div class="contents-inner202a">

						<div class="text-inner202">
							<p>条件を選択し、この画面の下にある「判定」をクリックすると、条件に合う大学が表示されます。<br>（検索結果：<c:out value="${DispMax}" />件以内）</p>
						<!-- /.text-inner --></div>

					<!-- /.contents-inner --></div>
				</div>

	<!-- PC用コンテンツここから -->
				<div class="contents cl-pc">
					<div class="contents-inner">
						<h4 class="title02">学校区分</h4>
						<div class="list-pc01">
							<ul>
								<li><label><input type="checkbox" name="schoolDivProviso" value="1" onclick="changeForm(this)">国公立大（前期日程）</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="2" onclick="changeForm(this)">国公立大（後期日程）</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="3" onclick="changeForm(this)">国公立大（中期・別日程）</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="4" onclick="changeForm(this)">私立大（一般方式）</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="5" onclick="changeForm(this)">私立大（共通テスト利用方式）</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="6" onclick="changeForm(this)">国公立短大一般</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="8" onclick="changeForm(this)">私立短大一般</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="7" onclick="changeForm(this)">国公立短大共通テスト利用</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="9" onclick="changeForm(this)">私立短大共通テスト利用</label></li>
								<li><label><input type="checkbox" name="schoolDivProviso" value="10" onclick="changeForm(this)">文部科学省所管外の大学校・短期大学校</label></li>
							</ul>
						<!-- /.list-pc --></div>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<h4 class="title02">学部系統</h4>
						<table class="table-pc">
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0011" onclick="checkChild(this)"  <c:if test="${J202Form.mstemma0011 == '1'}">checked</c:if>>文・人文</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="0100" onclick="checkParent(this)" <c:if test="${J202Form.region0100 == '1'}">checked</c:if>>日本文</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0200" onclick="checkParent(this)" <c:if test="${J202Form.region0200 == '1'}">checked</c:if>>外国文</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0300" onclick="checkParent(this)" <c:if test="${J202Form.sstemma02 == '1'}">checked</c:if>>外国語</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0400" onclick="checkParent(this)" <c:if test="${J202Form.region0400 == '1'}">checked</c:if>>哲・倫理・宗教</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0500" onclick="checkParent(this)" <c:if test="${J202Form.region0500 == '1'}">checked</c:if>>史・地理</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0600" onclick="checkParent(this)" <c:if test="${J202Form.region0600 == '1'}">checked</c:if>>教育</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0700" onclick="checkParent(this)" <c:if test="${J202Form.region0700 == '1'}">checked</c:if>>心理</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0800" onclick="checkParent(this)" <c:if test="${J202Form.region0800 == '1'}">checked</c:if>>地域・国際</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="0900" onclick="checkParent(this)" <c:if test="${J202Form.region0900 == '1'}">checked</c:if>>文化・教養</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code"  value="0021" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0021 == '1'}">checked</c:if>>社会・国際</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="1000" onclick="checkParent(this)" <c:if test="${J202Form.region1000 == '1'}">checked</c:if>>社会</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1100" onclick="checkParent(this)" <c:if test="${J202Form.region1100 == '1'}">checked</c:if>>社会福祉</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1200" onclick="checkParent(this)" <c:if test="${J202Form.region1200 == '1'}">checked</c:if>>国際法・政治</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1300" onclick="checkParent(this)" <c:if test="${J202Form.region1300 == '1'}">checked</c:if>>国際経済・経営</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1400" onclick="checkParent(this)" <c:if test="${J202Form.region1400 == '1'}">checked</c:if>>国際関係</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0022" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0022 == '1' == true}">checked</c:if>>法・政治</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="1500" onclick="checkParent(this)" <c:if test="${J202Form.region1500 == '1'}">checked</c:if>>法</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1600" onclick="checkParent(this)" <c:if test="${J202Form.region1600 == '1'}">checked</c:if>>政治</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0023" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0023 == '1'}">checked</c:if>>経済・経営・商</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="1700" onclick="checkParent(this)" <c:if test="${J202Form.sstemma07 == '1'}">checked</c:if>>経済</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1800" onclick="checkParent(this)" <c:if test="${J202Form.region1800 == '1'}">checked</c:if>>経営</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="1900" onclick="checkParent(this)" <c:if test="${J202Form.region1900 == '1'}">checked</c:if>>経営情報</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2000" onclick="checkParent(this)" <c:if test="${J202Form.region2000 == '1'}">checked</c:if>>商・会計・他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox"  name="stemma2Code" value="0031" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0031 == '1'}">checked</c:if>>教育（教員養成課程）</label><br><small>*国公立大のみ</small>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="2100" onclick="checkParent(this)" <c:if test="${J202Form.sstemma09 == '1'}">checked</c:if>>学校-教科</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2200" onclick="checkParent(this)" <c:if test="${J202Form.sstemma10 == '1'}">checked</c:if>>学校-実技</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2300" onclick="checkParent(this)" <c:if test="${J202Form.region2300 == '1'}">checked</c:if>>幼稚園</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2400" onclick="checkParent(this)" <c:if test="${J202Form.region2400 == '1'}">checked</c:if>>養護</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2500" onclick="checkParent(this)" <c:if test="${J202Form.region2500 == '1'}">checked</c:if>>特別支援学校</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2600" onclick="checkParent(this)" <c:if test="${J202Form.region2600 == '1'}">checked</c:if>>その他教員養成</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0032" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0032 == '1'}">checked</c:if>>教育（総合科学課程）</label><br><small>*国公立大のみ</small>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="2700" onclick="checkParent(this)" <c:if test="${J202Form.region2700 == '1'}">checked</c:if>>総課-スポーツ</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2800" onclick="checkParent(this)" <c:if test="${J202Form.region2800 == '1'}">checked</c:if>>総課-芸術・デザイン</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="2900" onclick="checkParent(this)" <c:if test="${J202Form.region2900 == '1'}">checked</c:if>>総課-情報</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3000" onclick="checkParent(this)" <c:if test="${J202Form.region3000 == '1'}">checked</c:if>>総課-国際・言語・文化</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3100" onclick="checkParent(this)" <c:if test="${J202Form.region3100 == '1'}">checked</c:if>>総課-心理・臨床</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3200" onclick="checkParent(this)" <c:if test="${J202Form.region3200 == '1'}">checked</c:if>>総課-地域・社会・生活</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3300" onclick="checkParent(this)" <c:if test="${J202Form.region3300== '1'}">checked</c:if>>総課-自然・環境・他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0041" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0041 == '1'}">checked</c:if>>理</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="3400" onclick="checkParent(this)" <c:if test="${J202Form.region3400 == '1'}">checked</c:if>>数学・数理情報</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3500" onclick="checkParent(this)" <c:if test="${J202Form.region3500 == '1'}">checked</c:if>>物理</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3600" onclick="checkParent(this)" <c:if test="${J202Form.region3600 == '1'}">checked</c:if>>化学</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3700" onclick="checkParent(this)" <c:if test="${J202Form.region3700 == '1'}">checked</c:if>>生物</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="3800" onclick="checkParent(this)" <c:if test="${J202Form.region3800 == '1'}">checked</c:if>>地学・他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0042" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0042 == '1'}">checked</c:if>>工</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="3900" onclick="checkParent(this)" <c:if test="${J202Form.region3900 == '1'}">checked</c:if>>機械</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4000" onclick="checkParent(this)" <c:if test="${J202Form.region4000 == '1'}">checked</c:if>>航空・宇宙</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4100" onclick="checkParent(this)" <c:if test="${J202Form.region4100 == '1'}">checked</c:if>>電気・電子</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4200" onclick="checkParent(this)" <c:if test="${J202Form.region4200 == '1'}">checked</c:if>>通信・情報</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4300" onclick="checkParent(this)" <c:if test="${J202Form.region4300 == '1'}">checked</c:if>>建築</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4400" onclick="checkParent(this)" <c:if test="${J202Form.region4400 == '1'}">checked</c:if>>土木・環境</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4500" onclick="checkParent(this)" <c:if test="${J202Form.region4500 == '1'}">checked</c:if>>応用化学</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4600" onclick="checkParent(this)" <c:if test="${J202Form.region4600 == '1'}">checked</c:if>>材料・物質工</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4700" onclick="checkParent(this)" <c:if test="${J202Form.region4700 == '1'}">checked</c:if>>応用物理</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4800" onclick="checkParent(this)" <c:if test="${J202Form.region4800 == '1'}">checked</c:if>>資源・エネルギー</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="4900" onclick="checkParent(this)" <c:if test="${J202Form.region4900 == '1'}">checked</c:if>>生物工・生命工</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5000" onclick="checkParent(this)" <c:if test="${J202Form.region5000 == '1'}">checked</c:if>>経営工・管理工</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5100" onclick="checkParent(this)" <c:if test="${J202Form.region5100 == '1'}">checked</c:if>>船舶・海洋</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5200" onclick="checkParent(this)" <c:if test="${J202Form.region5200 == '1'}">checked</c:if>>デザイン工・他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0043" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0043 == '1'}">checked</c:if>>農</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="5300" onclick="checkParent(this)" <c:if test="${J202Form.region5300 == '1'}">checked</c:if>>生物生産・応用生命</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5400" onclick="checkParent(this)" <c:if test="${J202Form.region5400 == '1'}">checked</c:if>>環境科学</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5500" onclick="checkParent(this)" <c:if test="${J202Form.region5500 == '1'}">checked</c:if>>経済システム</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5600" onclick="checkParent(this)" <c:if test="${J202Form.region5600 == '1'}">checked</c:if>>獣医</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5700" onclick="checkParent(this)" <c:if test="${J202Form.region5700 == '1'}">checked</c:if>>酪農・畜産</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="5800" onclick="checkParent(this)" <c:if test="${J202Form.region5800 == '1'}">checked</c:if>>水産</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0051" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0051 == '1'}">checked</c:if>>医・歯・薬・保健</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="5900" onclick="checkParent(this)" <c:if test="${J202Form.region5900 == '1'}">checked</c:if>>医</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6000" onclick="checkParent(this)" <c:if test="${J202Form.region6000 == '1'}">checked</c:if>>歯</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6100" onclick="checkParent(this)" <c:if test="${J202Form.region6100 == '1'}">checked</c:if>>薬</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6200" onclick="checkParent(this)" <c:if test="${J202Form.region6200 == '1'}">checked</c:if>>看護</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6300" onclick="checkParent(this)" <c:if test="${J202Form.region6300 == '1'}">checked</c:if>>医療技術</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6400" onclick="checkParent(this)" <c:if test="${J202Form.region6400 == '1'}">checked</c:if>>保健・福祉・他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0061" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0061 == '1'}">checked</c:if>>生活科学</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="6500" onclick="checkParent(this)" <c:if test="${J202Form.region6500 == '1'}">checked</c:if>>食物・栄養</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6600" onclick="checkParent(this)" <c:if test="${J202Form.region6600 == '1'}">checked</c:if>>被服</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6700" onclick="checkParent(this)" <c:if test="${J202Form.region6700 == '1'}">checked</c:if>>児童</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6800" onclick="checkParent(this)" <c:if test="${J202Form.region6800 == '1'}">checked</c:if>>住居</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="6900" onclick="checkParent(this)" <c:if test="${J202Form.region6900 == '1'}">checked</c:if>>生活科学</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="stemma2Code" value="0062" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0062 == '1'}">checked</c:if>>芸術・スポーツ科学</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="7400" onclick="checkParent(this)" <c:if test="${J202Form.region7400 == '1'}">checked</c:if> >美術・デザイン</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7500" onclick="checkParent(this)" <c:if test="${J202Form.region7500 == '1'}">checked</c:if>>音楽</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7600" onclick="checkParent(this)" <c:if test="${J202Form.region7600 == '1'}">checked</c:if>>その他芸術</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7700" onclick="checkParent(this)" <c:if test="${J202Form.region7700 == '1'}">checked</c:if>>芸術理論</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7800" onclick="checkParent(this)" <c:if test="${J202Form.region7800 == '1'}">checked</c:if>>スポーツ・健康</label></li>
 										<li><label><input type="checkbox" name="stemmaCode" value="7900" onclick="checkParent(this)" <c:if test="${J202Form.region7900 == '1'}">checked</c:if>>その他</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox"  name="stemma2Code" value="0071" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0071 == '1'}">checked</c:if>>総合・環境・情報・人間</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="stemmaCode" value="7000" onclick="checkParent(this)" <c:if test="${J202Form.region7000 == '1'}">checked</c:if>>総合</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7100" onclick="checkParent(this)" <c:if test="${J202Form.region7100 == '1'}">checked</c:if>>環境</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7300" onclick="checkParent(this)" <c:if test="${J202Form.region7300 == '1'}">checked</c:if>>情報</label></li>
										<li><label><input type="checkbox" name="stemmaCode" value="7200" onclick="checkParent(this)" <c:if test="${J202Form.region7200 == '1'}">checked</c:if>>人間</label></li>
									</ul>
								</td>
							</tr>
						</table>
					<!-- /.contents-inner --></div>

					<p class="pagetop"><a href="#wrapper"></a></p>

					<div class="contents-inner">
						<h4 class="title02"><label>地区</label></h4>
						<%
						// 地区のチェックマップ
						java.util.HashMap c1 = new java.util.HashMap();
						pageContext.setAttribute("c1", c1);
						// 県のチェックマップ
						java.util.HashMap c2 = new java.util.HashMap();
						pageContext.setAttribute("c2", c2);
						%>
						<c:forEach var="block" items="${J202Form.block}">
							<% c1.put(pageContext.getAttribute("block"), " checked"); %>
						</c:forEach>
						<c:forEach var="pref" items="${J202Form.pref}">
							<% c2.put(pageContext.getAttribute("pref"), " checked"); %>
						</c:forEach>
						<table class="table-pc">
							<tr>
								<th>
									<label><input type="checkbox" name="blockAll" value="00" onClick="checkBlockALL(this)">全地区</label>
								</th>
								<td></td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="01"onClick="checkBlock(this)"<c:out value="${c1['01']}" />>北海道</label>
								</th>
								<td>
									<ul>
										<!-- 非表示項目(北海道) -->
										<li><label><input type="checkbox" name="pref" value="01" style="display:none" <c:out value="${c2['01']}" />></label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="02" onClick="checkBlock(this)"<c:out value="${c1['02']}" />>東北</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="02" onClick="checkPref(this)"<c:out value="${c2['02']}" />>青森</label></li>
										<li><label><input type="checkbox" name="pref" value="03" onClick="checkPref(this)"<c:out value="${c2['03']}" />>岩手</label></li>
										<li><label><input type="checkbox" name="pref" value="04" onClick="checkPref(this)"<c:out value="${c2['04']}" />>宮城</label></li>
										<li><label><input type="checkbox" name="pref" value="05" onClick="checkPref(this)"<c:out value="${c2['05']}" />>秋田</label></li>
										<li><label><input type="checkbox" name="pref" value="06" onClick="checkPref(this)"<c:out value="${c2['06']}" />>山形</label></li>
										<li><label><input type="checkbox" name="pref" value="07" onClick="checkPref(this)"<c:out value="${c2['07']}" />>福島</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="03" onClick="checkBlock(this)"<c:out value="${c1['03']}" />>関東</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="08" onClick="checkPref(this)"<c:out value="${c2['08']}" />>茨城</label></li>
										<li><label><input type="checkbox" name="pref" value="09" onClick="checkPref(this)"<c:out value="${c2['09']}" />>栃木</label></li>
										<li><label><input type="checkbox" name="pref" value="10" onClick="checkPref(this)"<c:out value="${c2['10']}" />>群馬</label></li>
										<li><label><input type="checkbox" name="pref" value="11" onClick="checkPref(this)"<c:out value="${c2['11']}" />>埼玉</label></li>
										<li><label><input type="checkbox" name="pref" value="12" onClick="checkPref(this)"<c:out value="${c2['12']}" />>千葉</label></li>
										<li><label><input type="checkbox" name="pref" value="13" onClick="checkPref(this)"<c:out value="${c2['13']}" />>東京</label></li>
										<li><label><input type="checkbox" name="pref" value="14" onClick="checkPref(this)"<c:out value="${c2['14']}" />>神奈川</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="04" onClick="checkBlock(this)"<c:out value="${c1['04']}" />>甲信越</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="15" onClick="checkPref(this)"<c:out value="${c2['15']}" />>新潟</label></li>
										<li><label><input type="checkbox" name="pref" value="19" onClick="checkPref(this)"<c:out value="${c2['19']}" />>山梨</label></li>
										<li><label><input type="checkbox" name="pref" value="20" onClick="checkPref(this)"<c:out value="${c2['20']}" />>長野</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="05" onClick="checkBlock(this)"<c:out value="${c1['05']}" />>北陸</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="16" onClick="checkPref(this)"<c:out value="${c2['16']}" />>富山</label></li>
										<li><label><input type="checkbox" name="pref" value="17" onClick="checkPref(this)"<c:out value="${c2['17']}" />>石川</label></li>
										<li><label><input type="checkbox" name="pref" value="18" onClick="checkPref(this)"<c:out value="${c2['18']}" />>福井</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="06" onClick="checkBlock(this)"<c:out value="${c1['06']}" />>東海</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="21" onClick="checkPref(this)"<c:out value="${c2['21']}" />>岐阜</label></li>
										<li><label><input type="checkbox" name="pref" value="22" onClick="checkPref(this)"<c:out value="${c2['22']}" />>静岡</label></li>
										<li><label><input type="checkbox" name="pref" value="23" onClick="checkPref(this)"<c:out value="${c2['23']}" />>愛知</label></li>
										<li><label><input type="checkbox" name="pref" value="24" onClick="checkPref(this)"<c:out value="${c2['24']}" />>三重</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="07" onClick="checkBlock(this)"<c:out value="${c1['07']}" />>近畿</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="25" onClick="checkPref(this)"<c:out value="${c2['25']}" />>滋賀</label></li>
										<li><label><input type="checkbox" name="pref" value="26" onClick="checkPref(this)"<c:out value="${c2['26']}" />>京都</label></li>
										<li><label><input type="checkbox" name="pref" value="27" onClick="checkPref(this)"<c:out value="${c2['27']}" />>大阪</label></li>
										<li><label><input type="checkbox" name="pref" value="28" onClick="checkPref(this)"<c:out value="${c2['28']}" />>兵庫</label></li>
										<li><label><input type="checkbox" name="pref" value="29" onClick="checkPref(this)"<c:out value="${c2['29']}" />>奈良</label></li>
										<li><label><input type="checkbox" name="pref" value="30" onClick="checkPref(this)"<c:out value="${c2['30']}" />>和歌山</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="08" onClick="checkBlock(this)"<c:out value="${c1['08']}" />>中国</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="31" onClick="checkPref(this)"<c:out value="${c2['31']}" />>鳥取</label></li>
										<li><label><input type="checkbox" name="pref" value="32" onClick="checkPref(this)"<c:out value="${c2['32']}" />>島根</label></li>
										<li><label><input type="checkbox" name="pref" value="33" onClick="checkPref(this)"<c:out value="${c2['33']}" />>岡山</label></li>
										<li><label><input type="checkbox" name="pref" value="34" onClick="checkPref(this)"<c:out value="${c2['34']}" />>広島</label></li>
										<li><label><input type="checkbox" name="pref" value="35" onClick="checkPref(this)"<c:out value="${c2['35']}" />>山口</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="09" onClick="checkBlock(this)"<c:out value="${c1['09']}" />>四国</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="36" onClick="checkPref(this)"<c:out value="${c2['36']}" />>徳島</label></li>
										<li><label><input type="checkbox" name="pref" value="37" onClick="checkPref(this)"<c:out value="${c2['37']}" />>香川</label></li>
										<li><label><input type="checkbox" name="pref" value="38" onClick="checkPref(this)"<c:out value="${c2['38']}" />>愛媛</label></li>
										<li><label><input type="checkbox" name="pref" value="39" onClick="checkPref(this)"<c:out value="${c2['39']}" />>高知</label></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th>
									<label><input type="checkbox" name="block" value="10" onClick="checkBlock(this)"<c:out value="${c1['10']}" />>九州</label>
								</th>
								<td>
									<ul>
										<li><label><input type="checkbox" name="pref" value="40" onClick="checkPref(this)"<c:out value="${c2['40']}" />>福岡</label></li>
										<li><label><input type="checkbox" name="pref" value="41" onClick="checkPref(this)"<c:out value="${c2['41']}" />>佐賀</label></li>
										<li><label><input type="checkbox" name="pref" value="42" onClick="checkPref(this)"<c:out value="${c2['42']}" />>長崎</label></li>
										<li><label><input type="checkbox" name="pref" value="43" onClick="checkPref(this)"<c:out value="${c2['43']}" />>熊本</label></li>
										<li><label><input type="checkbox" name="pref" value="44" onClick="checkPref(this)"<c:out value="${c2['44']}" />>大分</label></li>
										<li><label><input type="checkbox" name="pref" value="45" onClick="checkPref(this)"<c:out value="${c2['45']}" />>宮崎</label></li>
										<li><label><input type="checkbox" name="pref" value="46" onClick="checkPref(this)"<c:out value="${c2['46']}" />>鹿児島</label></li>
										<li><label><input type="checkbox" name="pref" value="47" onClick="checkPref(this)"<c:out value="${c2['47']}" />>沖縄</label></li>
									</ul>
								</td>
							</tr>
						</table>
					<!-- .contents-inner --></div>
					<c:forEach var="block" items="${J202Form.block}">
						<% c1.put(pageContext.getAttribute("block"), " checked"); %>
					</c:forEach>
					<c:forEach var="pref" items="${J202Form.pref}">
						<% c2.put(pageContext.getAttribute("pref"), " checked"); %>
					</c:forEach>
					<p class="pagetop"><a href="#wrapper"></a></p>

					<div class="contents-inner">
						<h4 class="title02">配点比率</h4>

						<div class="list-pc02">
							<ul>
							<li><label><input type="radio" name="allotPntRatio" value="0" checked onclick="changeForm(this)">指定しない</label></li>
							<li><label><input type="radio" name="allotPntRatio" value="1"<c:if test="${ J202Form.allotPntRatio == '1' }"> checked</c:if> onclick="changeForm(this)">共通テスト重視型（共通テスト65&#65285;以上）</label></li>
							<li><label><input type="radio" name="allotPntRatio" value="2"<c:if test="${ J202Form.allotPntRatio == '2' }"> checked</c:if> onclick="changeForm(this)">均等型</label></li>
							<li><label><input type="radio" name="allotPntRatio" value="3"<c:if test="${ J202Form.allotPntRatio == '3' }"> checked</c:if> onclick="changeForm(this)">２次・個別重視型（２次・個別65&#65285;以上）</label></li>
							</ul>
						<!-- /.list-pc02 --></div>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<h4 class="title02">評価範囲</h4>

						<div class="list-pc02">
							<ul>
							<li><label><input type="checkbox" name="raingProviso" value="1" onclick="changeForm(this)">A判定</label></li>
							<li><label><input type="checkbox" name="raingProviso" value="2" onclick="changeForm(this)">B判定</label></li>
							<li><label><input type="checkbox" name="raingProviso" value="3" onclick="changeForm(this)">C判定</label></li>
							<li><label><input type="checkbox" name="raingProviso" value="4" onclick="changeForm(this)">D判定</label></li>
							<li><label><input type="checkbox" name="raingProviso" value="5" onclick="changeForm(this)">E判定</label></li>
							</ul>
							<p style="margin-bottom: 15px;">
								「Ａ判定」にチェックをした場合、「共通テスト」または「２次・個別試験」または「総合」がＡ判定となる大学が検索されます。
							</p>
						<!-- /.list-pc02 --></div>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<h4 class="title02">対象外</h4>

						<div class="list-pc02">
							<ul>
								<li><label><input type="checkbox" name="outOfTergetProviso" value="1" onclick="changeForm(this)">女子大</label></li>
								<li><label><input type="checkbox" name="outOfTergetProviso" value="2" onclick="checkYakan(this);changeForm(this)">二部・夜間主</label></li>
								<li style="display:none;"><label><input type="checkbox" name="outOfTergetProviso" value="3">夜間主</label></li>
							</ul>
						<!-- /.list-pc02 --></div>

					<!-- /.contents-inner --></div>

				<!-- /.contents.cl-pc --></div>
	<!-- PC用コンテンツここまで -->

	<!-- SP用コンテンツここから -->
				<div class="contents cl-sp">
					<div class="contents-inner">
							<h4 class="title02">学校区分</h4>
							<div class="list-sp01">
									<ul>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="1" onclick="changeForm(this)">&nbsp;&nbsp;国公立大（前期日程）</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="2" onclick="changeForm(this)">&nbsp;&nbsp;国公立大（後期日程）</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="3" onclick="changeForm(this)">&nbsp;&nbsp;国公立大（中期・別日程）</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="4" onclick="changeForm(this)">&nbsp;&nbsp;私立大（一般方式）</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="5" onclick="changeForm(this)">&nbsp;&nbsp;私立大（共通テスト利用方式）</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="6" onclick="changeForm(this)">&nbsp;&nbsp;国公立短大一般</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="8" onclick="changeForm(this)">&nbsp;&nbsp;私立短大一般</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="7" onclick="changeForm(this)">&nbsp;&nbsp;国公立短大共通テスト利用</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="9" onclick="changeForm(this)">&nbsp;&nbsp;私立短大共通テスト利用</label></li>
										<li><label><input type="checkbox" name="schoolDivProvisoSP" value="10" onclick="changeForm(this)">&nbsp;&nbsp;文部科学省所管外の大学校・短期大学校</label></li>
									</ul>
							<!-- /.list-sp01 --></div>
					<!-- /.contents-inner --></div>
					<div class="contents-inner">
						<ul class="accordion">
							<li>
								<p><span class="plus01">学部系統<i class="checkText"><span id="keitouCheck" style="padding-bottom: 0px; padding-top: 2px;"></span></i></span></p>
								<ul>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0011" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0011 == '1'}">checked</c:if>><p><span class="plus02">文・人文</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0100" onclick="checkParent(this)" <c:if test="${J202Form.region0100 == '1'}">checked</c:if>><span>日本文</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0200" onclick="checkParent(this)" <c:if test="${J202Form.region0200 == '1'}">checked</c:if>><span>外国文</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0300" onclick="checkParent(this)" <c:if test="${J202Form.sstemma02 == '1'}">checked</c:if>><span>外国語</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0400" onclick="checkParent(this)" <c:if test="${J202Form.region0400 == '1'}">checked</c:if>><span>哲・倫理・宗教</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0500" onclick="checkParent(this)" <c:if test="${J202Form.region0500 == '1'}">checked</c:if>><span>史・地理</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0600" onclick="checkParent(this)" <c:if test="${J202Form.region0600 == '1'}">checked</c:if>><span>教育</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0700" onclick="checkParent(this)" <c:if test="${J202Form.region0700 == '1'}">checked</c:if>><span>心理</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0800" onclick="checkParent(this)" <c:if test="${J202Form.region0800 == '1'}">checked</c:if>><span>地域・国際</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="0900" onclick="checkParent(this)" <c:if test="${J202Form.region0900 == '1'}">checked</c:if>><span>文化・教養</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP"  value="0021" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0021 == '1'}">checked</c:if>><p><span class="plus02">社会・国際</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1000" onclick="checkParent(this)" <c:if test="${J202Form.region1000 == '1'}">checked</c:if>><span>社会</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1100" onclick="checkParent(this)" <c:if test="${J202Form.region1100 == '1'}">checked</c:if>><span>社会福祉</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1200" onclick="checkParent(this)" <c:if test="${J202Form.region1200 == '1'}">checked</c:if>><span>国際法・政治</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1300" onclick="checkParent(this)" <c:if test="${J202Form.region1300 == '1'}">checked</c:if>><span>国際経済・経営</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1400" onclick="checkParent(this)" <c:if test="${J202Form.region1400 == '1'}">checked</c:if>><span>国際関係</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0022" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0022 == '1' == true}">checked</c:if>><p><span class="plus02">法・政治</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1500" onclick="checkParent(this)" <c:if test="${J202Form.region1500 == '1'}">checked</c:if>><span>法</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1600" onclick="checkParent(this)" <c:if test="${J202Form.region1600 == '1'}">checked</c:if>><span>政治</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0023" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0023 == '1'}">checked</c:if>><p><span class="plus02">経済・経営・商</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1700" onclick="checkParent(this)" <c:if test="${J202Form.sstemma07 == '1'}">checked</c:if>><span>経済</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1800" onclick="checkParent(this)" <c:if test="${J202Form.region1800 == '1'}">checked</c:if>><span>経営</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="1900" onclick="checkParent(this)" <c:if test="${J202Form.region1900 == '1'}">checked</c:if>><span>経営情報</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2000" onclick="checkParent(this)" <c:if test="${J202Form.region2000 == '1'}">checked</c:if>><span>商・会計・他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox"  name="stemma2CodeSP" value="0031" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0031 == '1'}">checked</c:if>><p><span class="plus02">教育(教員養成課程)<br><small>*国公立大のみ</small></span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2100" onclick="checkParent(this)" <c:if test="${J202Form.sstemma09 == '1'}">checked</c:if>><span>学校-教科</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2200" onclick="checkParent(this)" <c:if test="${J202Form.sstemma10 == '1'}">checked</c:if>><span>学校-実技</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2300" onclick="checkParent(this)" <c:if test="${J202Form.region2300 == '1'}">checked</c:if>><span>幼稚園</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2400" onclick="checkParent(this)" <c:if test="${J202Form.region2400 == '1'}">checked</c:if>><span>養護</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2500" onclick="checkParent(this)" <c:if test="${J202Form.region2500 == '1'}">checked</c:if>><span>特別支援学校</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2600" onclick="checkParent(this)" <c:if test="${J202Form.region2600 == '1'}">checked</c:if>><span>その他教員養成</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0032" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0032 == '1'}">checked</c:if>><p><span class="plus02">教育(総合科学課程)<br><small>*国公立大のみ</small></span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2700" onclick="checkParent(this)" <c:if test="${J202Form.region2700 == '1'}">checked</c:if>><span>総課-スポーツ</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2800" onclick="checkParent(this)" <c:if test="${J202Form.region2800 == '1'}">checked</c:if>><span>総課-芸術・デザイン</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="2900" onclick="checkParent(this)" <c:if test="${J202Form.region2900 == '1'}">checked</c:if>><span>総課-情報</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3000" onclick="checkParent(this)" <c:if test="${J202Form.region3000 == '1'}">checked</c:if>><span>総課-国際・言語・文化</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3100" onclick="checkParent(this)" <c:if test="${J202Form.region3100 == '1'}">checked</c:if>><span>総課-心理・臨床</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3200" onclick="checkParent(this)" <c:if test="${J202Form.region3200 == '1'}">checked</c:if>><span>総課-地域・社会・生活</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3300" onclick="checkParent(this)" <c:if test="${J202Form.region3300== '1'}">checked</c:if>><span>総課-自然・環境・他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0041" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0041 == '1'}">checked</c:if>><p><span class="plus02">理</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3400" onclick="checkParent(this)" <c:if test="${J202Form.region3400 == '1'}">checked</c:if>><span>数学・数理情報</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3500" onclick="checkParent(this)" <c:if test="${J202Form.region3500 == '1'}">checked</c:if>><span>物理</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3600" onclick="checkParent(this)" <c:if test="${J202Form.region3600 == '1'}">checked</c:if>><span>化学</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3700" onclick="checkParent(this)" <c:if test="${J202Form.region3700 == '1'}">checked</c:if>><span>生物</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3800" onclick="checkParent(this)" <c:if test="${J202Form.region3800 == '1'}">checked</c:if>><span>地学・他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0042" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0042 == '1'}">checked</c:if>><p><span class="plus02">工</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="3900" onclick="checkParent(this)" <c:if test="${J202Form.region3900 == '1'}">checked</c:if>><span>機械</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4000" onclick="checkParent(this)" <c:if test="${J202Form.region4000 == '1'}">checked</c:if>><span>航空・宇宙</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4100" onclick="checkParent(this)" <c:if test="${J202Form.region4100 == '1'}">checked</c:if>><span>電気・電子</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4200" onclick="checkParent(this)" <c:if test="${J202Form.region4200 == '1'}">checked</c:if>><span>通信・情報</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4300" onclick="checkParent(this)" <c:if test="${J202Form.region4300 == '1'}">checked</c:if>><span>建築</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4400" onclick="checkParent(this)" <c:if test="${J202Form.region4400 == '1'}">checked</c:if>><span>土木・環境</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4500" onclick="checkParent(this)" <c:if test="${J202Form.region4500 == '1'}">checked</c:if>><span>応用化学</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4600" onclick="checkParent(this)" <c:if test="${J202Form.region4600 == '1'}">checked</c:if>><span>材料・物質工</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4700" onclick="checkParent(this)" <c:if test="${J202Form.region4700 == '1'}">checked</c:if>><span>応用物理</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4800" onclick="checkParent(this)" <c:if test="${J202Form.region4800 == '1'}">checked</c:if>><span>資源・エネルギー</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="4900" onclick="checkParent(this)" <c:if test="${J202Form.region4900 == '1'}">checked</c:if>><span>生物工・生命工</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5000" onclick="checkParent(this)" <c:if test="${J202Form.region5000 == '1'}">checked</c:if>><span>経営工・管理工</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5100" onclick="checkParent(this)" <c:if test="${J202Form.region5100 == '1'}">checked</c:if>><span>船舶・海洋</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5200" onclick="checkParent(this)" <c:if test="${J202Form.region5200 == '1'}">checked</c:if>><span>デザイン工・他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0043" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0043 == '1'}">checked</c:if>><p><span class="plus02">農</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5300" onclick="checkParent(this)" <c:if test="${J202Form.region5300 == '1'}">checked</c:if>><span>生物生産・応用生命</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5400" onclick="checkParent(this)" <c:if test="${J202Form.region5400 == '1'}">checked</c:if>><span>環境科学</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5500" onclick="checkParent(this)" <c:if test="${J202Form.region5500 == '1'}">checked</c:if>><span>経済システム</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5600" onclick="checkParent(this)" <c:if test="${J202Form.region5600 == '1'}">checked</c:if>><span>獣医</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5700" onclick="checkParent(this)" <c:if test="${J202Form.region5700 == '1'}">checked</c:if>><span>酪農・畜産</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5800" onclick="checkParent(this)" <c:if test="${J202Form.region5800 == '1'}">checked</c:if>><span>水産</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0051" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0051 == '1'}">checked</c:if>><p><span class="plus02">医・歯・薬・保健</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="5900" onclick="checkParent(this)" <c:if test="${J202Form.region5900 == '1'}">checked</c:if> ><span>医</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6000" onclick="checkParent(this)" <c:if test="${J202Form.region6000 == '1'}">checked</c:if>><span>歯</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6100" onclick="checkParent(this)" <c:if test="${J202Form.region6100 == '1'}">checked</c:if>><span>薬</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6200" onclick="checkParent(this)" <c:if test="${J202Form.region6200 == '1'}">checked</c:if>><span>看護</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6300" onclick="checkParent(this)" <c:if test="${J202Form.region6300 == '1'}">checked</c:if>><span>医療技術</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6400" onclick="checkParent(this)" <c:if test="${J202Form.region6400 == '1'}">checked</c:if>><span>保健・福祉・他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0061" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0061 == '1'}">checked</c:if>><p><span class="plus02">生活科学</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6500" onclick="checkParent(this)" <c:if test="${J202Form.region6500 == '1'}">checked</c:if>><span>食物・栄養</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6600" onclick="checkParent(this)" <c:if test="${J202Form.region6600 == '1'}">checked</c:if>><span>被服</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6700" onclick="checkParent(this)" <c:if test="${J202Form.region6700 == '1'}">checked</c:if>><span>児童</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6800" onclick="checkParent(this)" <c:if test="${J202Form.region6800 == '1'}">checked</c:if>><span>住居</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="6900" onclick="checkParent(this)" <c:if test="${J202Form.region6900 == '1'}">checked</c:if>><span>生活科学</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="stemma2CodeSP" value="0062" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0062 == '1'}">checked</c:if>><p><span class="plus02">芸術・スポーツ科学</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7400" onclick="checkParent(this)" <c:if test="${J202Form.region7400 == '1'}">checked</c:if>><span>美術・デザイン</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7500" onclick="checkParent(this)" <c:if test="${J202Form.region7500 == '1'}">checked</c:if>><span>音楽</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7600" onclick="checkParent(this)" <c:if test="${J202Form.region7600 == '1'}">checked</c:if>><span>その他芸術</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7700" onclick="checkParent(this)" <c:if test="${J202Form.region7700 == '1'}">checked</c:if>><span>芸術理論</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7800" onclick="checkParent(this)" <c:if test="${J202Form.region7800 == '1'}">checked</c:if>><span>スポーツ・健康</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7900" onclick="checkParent(this)" <c:if test="${J202Form.region7900 == '1'}">checked</c:if>><span>その他</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox"  name="stemma2CodeSP" value="0071" onclick="checkChild(this)" <c:if test="${J202Form.mstemma0071 == '1'}">checked</c:if>><p><span class="plus02">総合・環境・情報・<br>人間</span></p>
										<ul>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7000" onclick="checkParent(this)" <c:if test="${J202Form.region7000 == '1'}">checked</c:if>><span>総合</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7100" onclick="checkParent(this)" <c:if test="${J202Form.region7100 == '1'}">checked</c:if>><span>環境</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7300" onclick="checkParent(this)" <c:if test="${J202Form.region7300 == '1'}">checked</c:if>><span>情報</label></span></li>
											<li><label><input type="checkbox" name="stemmaCodeSP" value="7200" onclick="checkParent(this)" <c:if test="${J202Form.region7200 == '1'}">checked</c:if>><span>人間</label></span></li>
										</ul>
									</li>
									<li><span class="pagetop"><a href="#wrapper"></a></span></li>
								</ul>
							</li>
						</ul>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<ul class="accordion">
							<li>
								<p><span class="plus01">地区<i class="checkText"><span id="chikuCheck" style="padding-bottom: 0px; padding-top: 2px;"></span></i></span></p>
								<ul>
									<li>
										<label><input type="checkbox" name="blockAllSP" value="00" onClick="checkBlockALL(this)"><span class="space">全地区</span></label>
									</li>
									<li>
										<label><input type="checkbox" name="blockSP" value="01"onClick="checkBlock(this)"<c:out value="${c1['01']}" />><span class="space">北海道</span></label>
										<ul>
										<!-- 非表示項目(北海道) -->
											<li><label><input type="checkbox" name="prefSP" value="01" style="display:none" <c:out value="${c2['01']}" />></label></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="02" onClick="checkBlock(this)"<c:out value="${c1['02']}" />><p><span class="plus02">東北</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="02" onClick="checkPref(this)"<c:out value="${c2['02']}" />><span>青森</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="03" onClick="checkPref(this)"<c:out value="${c2['03']}" />><span>岩手</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="04" onClick="checkPref(this)"<c:out value="${c2['04']}" />><span>宮城</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="05" onClick="checkPref(this)"<c:out value="${c2['05']}" />><span>秋田</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="06" onClick="checkPref(this)"<c:out value="${c2['06']}" />><span>山形</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="07" onClick="checkPref(this)"<c:out value="${c2['07']}" />><span>福島</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="03" onClick="checkBlock(this)"<c:out value="${c1['03']}" />><p><span class="plus02">関東</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="08" onClick="checkPref(this)"<c:out value="${c2['08']}" />><span>茨城</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="09" onClick="checkPref(this)"<c:out value="${c2['09']}" />><span>栃木</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="10" onClick="checkPref(this)"<c:out value="${c2['10']}" />><span>群馬</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="11" onClick="checkPref(this)"<c:out value="${c2['11']}" />><span>埼玉</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="12" onClick="checkPref(this)"<c:out value="${c2['12']}" />><span>千葉</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="13" onClick="checkPref(this)"<c:out value="${c2['13']}" />><span>東京</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="14" onClick="checkPref(this)"<c:out value="${c2['14']}" />><span>神奈川</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="04" onClick="checkBlock(this)"<c:out value="${c1['04']}" />><p><span class="plus02">甲信越</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="15" onClick="checkPref(this)"<c:out value="${c2['15']}" />><span>新潟</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="19" onClick="checkPref(this)"<c:out value="${c2['19']}" />><span>山梨</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="20" onClick="checkPref(this)"<c:out value="${c2['20']}" />><span>長野</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="05" onClick="checkBlock(this)"<c:out value="${c1['05']}" />><p><span class="plus02">北陸</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="16" onClick="checkPref(this)"<c:out value="${c2['16']}" />><span>富山</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="17" onClick="checkPref(this)"<c:out value="${c2['17']}" />><span>石川</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="18" onClick="checkPref(this)"<c:out value="${c2['18']}" />><span>福井</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="06" onClick="checkBlock(this)"<c:out value="${c1['06']}" />><p><span class="plus02">東海</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="21" onClick="checkPref(this)"<c:out value="${c2['21']}" />><span>岐阜</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="22" onClick="checkPref(this)"<c:out value="${c2['22']}" />><span>静岡</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="23" onClick="checkPref(this)"<c:out value="${c2['23']}" />><span>愛知</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="24" onClick="checkPref(this)"<c:out value="${c2['24']}" />><span>三重</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="07" onClick="checkBlock(this)"<c:out value="${c1['07']}" />><p><span class="plus02">近畿</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="25" onClick="checkPref(this)"<c:out value="${c2['25']}" />><span>滋賀</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="26" onClick="checkPref(this)"<c:out value="${c2['26']}" />><span>京都</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="27" onClick="checkPref(this)"<c:out value="${c2['27']}" />><span>大阪</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="28" onClick="checkPref(this)"<c:out value="${c2['28']}" />><span>兵庫</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="29" onClick="checkPref(this)"<c:out value="${c2['29']}" />><span>奈良</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="30" onClick="checkPref(this)"<c:out value="${c2['30']}" />><span>和歌山</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="08" onClick="checkBlock(this)"<c:out value="${c1['08']}" />><p><span class="plus02">中国</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="31" onClick="checkPref(this)"<c:out value="${c2['31']}" />><span>鳥取</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="32" onClick="checkPref(this)"<c:out value="${c2['32']}" />><span>島根</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="33" onClick="checkPref(this)"<c:out value="${c2['33']}" />><span>岡山</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="34" onClick="checkPref(this)"<c:out value="${c2['34']}" />><span>広島</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="35" onClick="checkPref(this)"<c:out value="${c2['35']}" />><span>山口</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="09" onClick="checkBlock(this)"<c:out value="${c1['09']}" />><p><span class="plus02">四国</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="36" onClick="checkPref(this)"<c:out value="${c2['36']}" />><span>徳島</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="37" onClick="checkPref(this)"<c:out value="${c2['37']}" />><span>香川</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="38" onClick="checkPref(this)"<c:out value="${c2['38']}" />><span>愛媛</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="39" onClick="checkPref(this)"<c:out value="${c2['39']}" />><span>高知</label></span></li>
										</ul>
									</li>
									<li>
										<input type="checkbox" name="blockSP" value="10" onClick="checkBlock(this)"<c:out value="${c1['10']}" />><p><span class="plus02">九州</span></p>
										<ul>
											<li><label><input type="checkbox" name="prefSP" value="40" onClick="checkPref(this)"<c:out value="${c2['40']}" />><span>福岡</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="41" onClick="checkPref(this)"<c:out value="${c2['41']}" />><span>佐賀</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="42" onClick="checkPref(this)"<c:out value="${c2['42']}" />><span>長崎</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="43" onClick="checkPref(this)"<c:out value="${c2['43']}" />><span>熊本</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="44" onClick="checkPref(this)"<c:out value="${c2['44']}" />><span>大分</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="45" onClick="checkPref(this)"<c:out value="${c2['45']}" />><span>宮崎</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="46" onClick="checkPref(this)"<c:out value="${c2['46']}" />><span>鹿児島</label></span></li>
											<li><label><input type="checkbox" name="prefSP" value="47" onClick="checkPref(this)"<c:out value="${c2['47']}" />><span>沖縄</label></span></li>
										</ul>
									</li>
									<li><span class="pagetop"><a href="#wrapper"></a></span></li>
								</ul>
							</li>
						</ul>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<ul class="accordion">
							<li>
								<p><span class="plus01">配点比率<i class="checkText"><span id="hiritsuCheck" style="padding-bottom: 0px; padding-top: 2px;"></span></i></span></p>
								<ul class="control">
									<li>
										<span class="list-sp02">
											<label class="indent"><input type="radio" name="allotPntRatioSP" value="0" checked onclick="changeForm(this)">指定しない</label>
											<label class="indent"><input type="radio" name="allotPntRatioSP" value="1"<c:if test="${ J202Form.allotPntRatio == '1' }"> checked</c:if> onclick="changeForm(this)">共通テスト重視型（共通テスト65&#65285;以上）</label>
											<label class="indent"><input type="radio" name="allotPntRatioSP" value="2"<c:if test="${ J202Form.allotPntRatio == '2' }"> checked</c:if> onclick="changeForm(this)">均等型</label>
											<label class="indent"><input type="radio" name="allotPntRatioSP" value="3"<c:if test="${ J202Form.allotPntRatio == '3' }"> checked</c:if> onclick="changeForm(this)">２次・個別重視型（２次・個別65&#65285;以上）</label>
										</span>
									</li>
								</ul>
							</li>
						</ul>
					<!-- /.contents-inner --></div>

					<div class="contents-inner">
						<ul class="accordion">
							<li>
								<p><span class="plus01">評価範囲<i class="checkText"><span id="hyokaCheck" style="padding-bottom: 0px; padding-top: 2px;"></span></i></span></p>
								<ul class="control">
									<li style="padding-bottom: 15px;">
										<span class="list-sp02">
											<label><input type="checkbox" name="raingProvisoSP" value="1" onclick="changeForm(this)">A判定</label>
											<label><input type="checkbox" name="raingProvisoSP" value="2" onclick="changeForm(this)">B判定</label>
											<label><input type="checkbox" name="raingProvisoSP" value="3" onclick="changeForm(this)">C判定</label>
											<label><input type="checkbox" name="raingProvisoSP" value="4" onclick="changeForm(this)">D判定</label>
											<label><input type="checkbox" name="raingProvisoSP" value="5" onclick="changeForm(this)">E判定</label>
										</span>
										<p>
										「Ａ判定」にチェックをした場合、「共通テスト」または「２次・個別試験」がＡ判定となる大学が検索されます。
										</p>
									</li>
								</ul>
							</li>
						</ul>
					<!-- /.contents-inner --></div>
					<div class="contents-inner">
						<ul class="accordion">
							<li>
								<p><span class="plus01">対象外<i class="checkText"><span id="taisyogaiCheck" style="padding-bottom: 0px; padding-top: 2px;"></span></i></span></p>
								<ul class="control">
									<li>
										<span class="list-sp02">
											<label><input type="checkbox" name="outOfTergetProvisoSP" value="1" onclick="changeForm(this)">女子大</label>
											<label><input type="checkbox" name="outOfTergetProvisoSP" value="2" onclick="checkYakan(this);changeForm(this)">二部・夜間主</label>
											<label style="display:none;"><input type="checkbox" name="outOfTergetProvisoSP" value="3">夜間主</label>
										</span>
									</li>
								</ul>
							</li>
						</ul>
					<!-- /.contents-inner --></div>
				<!-- /.contents.cl-sp --></div>
	<!-- SP用コンテンツ ここまで -->

				<div class="contents">

					<div class="contents-inner">
						<div class="btn">
							<input type="submit" name="judge" value="" onclick="return submitJudge('3')" id="hantei-btn" class="cl-pc">
							<input type="submit" name="Judge" value="" onclick="return submitJudge('1')" class="hantei-btn cl-sp">
							<input type="button" name="" value="" onclick="submitReset()" id="j202-clear-btn">
						<!-- /.submit-btn.btn --></div>
					<!-- /.contents-inner --></div>
				<!-- /.contents--></div>

			<!-- /.container --></div>
		</div>
	<!-- コンテンツここまで -->

<%@ include file="/jsp/include/footer.jsp" %>
</form>
</div>
</body>
</html>

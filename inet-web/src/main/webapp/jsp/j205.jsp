<%@ page contentType="text/html;charset=MS932"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld" %>
<%@ page import="jp.co.fj.kawaijuku.judgement.util.JudgementUtil" %>
<jsp:useBean id="Judge" scope="request" type="jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean" />
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
    <title>結果詳細 模試判定システム</title>
    <link rel="stylesheet" href="./shared_lib/style/normalize.css">
    <link rel="stylesheet" href="./shared_lib/style/common.css">
    <link rel="stylesheet" href="./shared_lib/style/button.css">
    <link rel="stylesheet" href="./shared_lib/style/buttonsp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="./shared_lib/style/sp.css" media="(max-width:640px)">
    <link rel="stylesheet" href="./shared_lib/style/j205pc.css">
    <link rel="stylesheet" href="./shared_lib/style/j205sp.css" media="(max-width:640px)">
    <style type="text/css">
    <!--
        div.g-nav { display: none; }
    -->
    </style>
    <!--[if lt IE 9]>
    <script src="./js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="./shared_lib/style/ie8.css">
    <![endif]-->
    <script type="text/javascript" src="./js/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="./js/jquery.smoothScroll.js"></script>
    <script type="text/javascript">
    <!--

    <%@ include file="/jsp/script/submit_menu.jsp" %>

    // Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
    if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
        document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
    }

    /**
     * サブミッション
     */
    function submitForm(forward) {
        document.forms[0].target = "_self";
        document.forms[0].forward.value = forward;
        document.forms[0].backward.value = "<c:out value="${param.forward}" />";
        document.forms[0].save.value = "0";
        document.forms[0].submit();
    }
    /**
     * 公表科目サブミッション
     */
     function submitOpenSub() {
         var form = document.forms[0];
         // 出力先ウィンドウを開く
         if (window.navigator.userAgent.indexOf('iPhone') == -1 && window.navigator.userAgent.indexOf('Edge') == -1 && window.navigator.userAgent.indexOf('Android') == -1) {
         	op = window.open("", "j206", "resizable=yes,scrollbars=yes,status=yes,titlebar=yes");
        	op.focus();
         }
         form.forward.value = "j206";
         form.backward.value = "<c:out value="${param.forward}" />";
         if (window.navigator.userAgent.indexOf('iPhone') == -1 && window.navigator.userAgent.indexOf('Edge') == -1 && window.navigator.userAgent.indexOf('Android') == -1) {
         	form.target = "j206";
     	} else {
     		form.target = "_blank";
     	}
         form.submit();
     }
    /**
     * 枝番設定
     */
    function setBranchInfo(branch){
        document.forms[0].selectBranch.value = branch;
    }
    //-->
    </script>
</head>
<body oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
<div id="wrapper"></div>
<form action="<c:url value="UnivDetail" />" method="post">
<%-- hiddens --%>
<input type="hidden" name="forward" value="">
<input type="hidden" name="backward" value="">
<input type="hidden" name="save" value="">
<input type="hidden" name="judgeNo" value="<c:out value="${param.judgeNo}" />">
<!-- 隠れフィールド　大学・学部・学科情報 -->
<input type="hidden" name="uniqueKey" value="<%=encTableNBSP(Judge.getUniqueKey())%>">
<!-- 隠れフィールド　選択された枝番 -->
<input type="hidden"name="selectBranch" value="">
<%-- /hiddens --%>

<%@ include file="/jsp/include/header.jsp" %>

<!-- コンテンツここから -->
<div class="container">
    <div class="contents">

        <!-- 大見出し ここから -->
        <h3 class="title01">判定結果詳細</h3>
        <!-- 大見出し ここまで -->

        <div class="contents-inner">
            <h4 class="title02"><kn:univType><c:out value="${Judge.univCd}"/></kn:univType><c:out value="${Judge.univName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${Judge.deptName}"/>&nbsp;&nbsp;&nbsp;<c:out value="${Judge.subName}"/><%= changeNOS(Judge.getExamCapaRel(), Judge.getCapacity()) %></h4>
            <div class="text-inner">
                <p>本部所在地：<c:out value="${Judge.prefName}"/>&nbsp;&nbsp;&nbsp;キャンパス所在地：<c:out value="${Judge.prefName_sh}"/></p>
            </div><!-- /.text-inner -->
            <div class="btn link-btn">
            	<p>
            	<c:choose>
            	<c:when test="${ univExamDiv == '04' || univExamDiv == '05' || univExamDiv == '06' }">
                	<input type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01" class="cl-pc">
				</c:when>
				<c:otherwise>
					<input type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01" style="visibility:hidden" class="cl-pc">
				</c:otherwise>
				</c:choose>
            	<c:if test="${ not (Judge.univDivCd == '08' || Judge.univDivCd == '09')}">
					<input type="button" name="" value="" onclick="window.open('https://search.keinet.ne.jp/outline/department/<c:out value="${Judge.univCd}"/>')" id="j205-btn02" class="cl-pc">
				</c:if>
            	</p>
            </div><!-- /.btn.link-btn -->
            <div class="btn link-btn01 cl-sp" align="right" >
            	<c:choose>
            	<c:when test="${ univExamDiv == '04' || univExamDiv == '05' || univExamDiv == '06' }">
                	<input style="position: absolute; right: 160px;" type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01-sp" class="cl-sp1">
				</c:when>
				<c:otherwise>
					<input style="position: absolute; right: 160px; visibility:hidden" type="button" name="" value="" onclick="submitOpenSub()" id="j205-btn01-sp" class="cl-sp1">
				</c:otherwise>
				</c:choose>
            	<c:if test="${ not (Judge.univDivCd == '08' || Judge.univDivCd == '09')}">
					<input style="position: absolute; right: 10px;" type="button" name="" value="" onclick="window.open('https://search.keinet.ne.jp/outline/department_sp/<c:out value="${Judge.univCd}"/>')" id="j205-btn02-sp" class="cl-sp1">
				</c:if>
				<div class="contents-inner01"></div>
            </div><!-- /.btn.link-btn01 -->
            <div class="section-container">
                <div id="section01">
                    <h4 class="section-title">共通テスト&nbsp;判定&nbsp;<%=Judge.getCLetterJudgement()%></h4>

                    <div class="text-inner">
                        <div class="section-inner">
                            <div class="panel"><img src="./shared_lib/img/hantei_<c:out value="${ Judge.centerLetterJudgement }" />.png" /></div>
                            <div class="myscore">
                                <h5 class="point">あなたの得点</h5>
                                <p><span class="text-19"><%= encTableHAI(Judge.getC_scoreStr()) %>点</span><span class="text-16">（<%= encTableHAI(Judge.cFullPoint) %>点中）</span></p>
                            </div><!-- /.myscore -->
                        </div><!-- /.section-inner -->

                            <table>
                                <thead>
                                    <tr>
                                        <td width="82" height="21" style="background-color:#7b95d8;color:#fff;border-bottom: solid 1px #fff;">教科</td>
                                        <th width="70" height="21">英</th>
                                        <th width="70" height="21">数</th>
                                        <th width="70" height="21">国</th>
                                        <th width="70" height="21">理</th>
                                        <th width="70" height="21">地公</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td width="82" height="21" style="background-color:#7b95d8;color:#fff;border-bottom: solid 1px #fff;">得点</td>
                                        <td class="full2"><c:out value="${Judge.score1English}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Mat}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Jap}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Sci}"/></td>
                                        <td class="full2"><c:out value="${Judge.score1Soc}"/></td>
                                    </tr>
                                     <tr>
                                        <td width="82" height="21" class="full01"style="background-color:#7b95d8;color:#fff;">配点</td>
                                        <td class="full3"><c:out value="${Judge.allot1English}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Mat}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Jap}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Sci}"/></td>
                                        <td class="full3"><c:out value="${Judge.allot1Soc}"/></td>
                                    </tr>
                                </tbody>
                            </table>

                        <hr style="background-color:#fff"><hr>

                        <h5 class="clear point">ボーダー得点・評価基準点</h5>
                        <p><span class="text-16">ボーダー得点</span>&nbsp;<span class="text-19"><%= encTableHAI(Judge.getBorderPointStr()) %>点</span></p>
                        <ul class="rank-marks">
                            <li><%=encTableNBSP(Judge.getCDLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCCLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCBLine()) %><li>
                            <li><%=encTableNBSP(Judge.getCALine()) %><li>
                        </ul>
                        <ul class="rank-meter">
                            <li class="rank-meter-e">E</li>
                            <li class="rank-meter-d">D</li>
                            <li class="rank-meter-c">C</li>
                            <li class="rank-meter-b">B</li>
                            <li class="rank-meter-a">A</li>
                        </ul>
                        <ul class="myrank">
                            <c:forEach var="i" begin="1" end="5" step="1">
                            <li <c:if test="${Judge.c_judgement == i}">id="myRankCurrent"</c:if>>★</li>
                            </c:forEach>
                        </ul>

                    </div><!-- /.text-inner -->
                    <c:if test="${ Judge.scheduleCd == ' ' }">
                    <div id="notCovered">
                        <p>課さない</p>
                    </div><!-- /#notCovered -->
                    </c:if>
                </div><!-- /#section01 -->

                <div id="section02">
                    <h4 class="section-title">2次・個別試験&nbsp;判定&nbsp;<%=Judge.getSLetterJudgement()%></h4>
                    <div class="text-inner">
                            <div class="section-inner">
                                <div class="panel"><img src="./shared_lib/img/hantei_<c:out value="${ Judge.secondLetterJudgement }" />.png" /></div>
                                <div class="myscore">
                                    <h5 class="point">あなたの偏差値</h5>
                                    <p><span class="text-19"><%= encTableHAI(Judge.getS_scoreStr()) %></span></p>
                                </div><!-- /.myscore -->
                            </div><!-- /section-inner -->

                        <table>
                            <thead>
                                <tr>
                                    <td width="72" height="21"  style="background-color:#7b95d8;color:#fff;border-bottom: solid 1px #fff;">教科</td>
                                    <th width="60" height="21">英</th>
                                    <th width="60" height="21">数</th>
                                    <th width="60" height="21">国</th>
                                    <th width="60" height="21">理</th>
                                    <th width="60" height="21">地公</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="72" height="21" height="25" style="background-color:#7b95d8;color:#fff;border-bottom: solid 1px #fff;">偏差値</td>
                                    <td class="full2"><c:out value="${Judge.score2Eng}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Mat}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Jap}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Sci}"/></td>
                                    <td class="full2"><c:out value="${Judge.score2Soc}"/></td>
                                </tr>
                                <tr>
                                    <td width="72" height="21" class="full01"style="background-color:#7b95d8;color:#fff;">配点</td>
                                    <td class="full3"><c:out value="${Judge.allot2Eng}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Mat}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Jap}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Sci}"/></td>
                                    <td class="full3"><c:out value="${Judge.allot2Soc}"/></td>
                                </tr>
                            </tbody>
                        </table>

                        <hr style="background-color:#fff"><hr>

                        <h5 class="clear point">ボーダー偏差値・評価基準偏差値</h5>
                        <p>
                            <span class="text-16">ボーダー偏差値</span>&nbsp;
                            <span class="text-19">
                            <c:if test="${Judge.rankName == ' -- ' && Judge.rank != '-99'}">--</c:if>
                            <c:if test="${Judge.rankName != ' -- ' && Judge.rank != '-99'}">
                            <%= Judge.getRankLLimitsStr().trim().length() > 0 ? Judge.getRankLLimitsStr()+" &#65374; "+Judge.getRankHLimitsStr() : "--" %>
                            </c:if>
                            <c:if test="${Judge.rank == '-99'}">BF</c:if>
                            </span>
                        </p>
                        <ul class="deviation-marks">
                            <li><%=encTableNBSP(Judge.getSDLine())%><li>
                            <li><%=encTableNBSP(Judge.getSCLine())%><li>
                            <li><%=encTableNBSP(Judge.getSBLine())%><li>
                            <li><%=encTableNBSP(Judge.getSALine())%><li>
                        </ul>
                        <ul class="deviation-meter">
                            <li class="deviation-meter-e">E</li>
                            <li class="deviation-meter-d">D</li>
                            <li class="deviation-meter-c">C</li>
                            <li class="deviation-meter-b">B</li>
                            <li class="deviation-meter-a">A</li>
                        </ul>
                        <ul class="mydeviation clear">
                            <c:forEach var="i" begin="1" end="5" step="1">
                            <li <c:if test="${Judge.s_judgement == i}">id="myDeviationCurrent"</c:if>>★</li>
                            </c:forEach>
                        </ul>


                    </div><!-- /.text-inner -->
                </div><!-- /#section02 -->
            </div><!-- /.section-container -->

<c:if test="${ Judge.scheduleCd != ' ' }">
            <div id="section03">
                <h4 class="section-title">総合&nbsp;判定&nbsp;<%=Judge.getTLetterJudgement()%></h4>
                <div class="text-inner">
                    <div class="section-inner">
                        <div class="panel"><img src="./shared_lib/img/hantei_<%= Judge.getTLetterJudgement().trim().length() > 0 ? Judge.getTLetterJudgement().trim()=="*" ? "kome" : Judge.getTLetterJudgement().toLowerCase().replaceAll("#", "s") : "none" %>.png" /></div>
                        <div class="myscore">
                            <p><span class="point">総合ポイント</span><span class="text-19 total-indent01"><%= encTableHAI(Judge.getT_scoreStr()) %></span></p>
                            <p><span class="point">配点ウェイト（点）</span><br class="br" /><span class="text-16"><span class="total-indent03">共通テスト</span>：<c:out value="${Judge.CAllotPntRateAll}"/><br class="br"><span class="total-indent03">２次・個別</span>：<c:out value="${Judge.SAllotPntRateAll}"/></span></p>
                        </div><!-- /.myscore -->
                    </div><!-- /.section-inner -->
                </div><!-- /.text-inner -->
            </div><!-- /#section03 -->
</c:if>
        <div class="text-inner notes">
            <p class="small"><span class="kome">※</span><span class="indent">大学の指定科目・配点が複数パターンある場合、最も高い成績になるパターンで判定を行っています。</span></p>
            <p class="small"><span class="kome">※</span><span class="indent">小論文や総合問題等を課す場合、全統模試では他の教科で代替して判定を行っているケースがあります。</span></p>
        </div>

        </div><!-- /.contents-inner -->
        <div class="contents-inner">
            <div class="close-btn btn">
                <input type="button" name="" value="" onclick="javascript:window.close()" id="close-btn">
            </div><!-- /.submit-btn.btn -->
        </div><!-- /.contents-inner -->

    </div><!-- /.contents -->
</div><!-- /.container -->
<!-- コンテンツここまで -->

<%@ include file="/jsp/include/footer.jsp" %>

</form>
</body>
</html>
<%!
/**
 * 表の中に””（空文字）があるときに"--"を設定してくれるメソッド（ＮＮ対応の場合は必ずとおす）
 * encは実行済み
 * @param	value  表内に設定する値Value値
 * @return	"value" または "&nbsp;"
 */
public String encTableHAI(String value){
    if(value == null || value.equals("")){
        return ("--");
      }else{
        return (enc(value));
      }
}
private static String getAction(int Flg){
 if(Flg == 1){
   return("BranchfavoriteDetail");
  }else{
   return("BranchlistDetail");
  }
}
public String getBranchNumber(int index){
  return (JudgementUtil.fillZero(index+1));
}
/**
 *
 * @param	strSrc
 * @return
 */
public static final String enc(String strSrc){
  int				nLen;
  if(strSrc == null || (nLen = strSrc.length()) <= 0)
  return "";

  StringBuffer	sbEnc = new StringBuffer(nLen * 2);

  for(int i = 0; i < nLen; i++){
    char	c;
    switch(c = strSrc.charAt(i)){
      case '<':	sbEnc.append("&lt;");	break;
      case '>':	sbEnc.append("&gt;");	break;
      case '&':	sbEnc.append("&amp;");	break;
      case '"':	sbEnc.append("&quot;");	break;
      case '\'':sbEnc.append("&#39;");	break;
      case '\\':sbEnc.append("&yen;");	break;
      default: sbEnc.append(c); break;
    }
  }
  return sbEnc.toString();
}
/**
 *
 * @param	value
 * @return	"value"
 */
public static final String encTableNBSP(String value){
  if(value.equals("")){
    return ("&nbsp;");
  }else{
    return (enc(value));
  }
}
/**
 * 募集人員の表示内容を引数の値によって変更させるメソッド
 * encは実行済み
 * @param	examCapaRel  入試定員信頼性
 * @param	capacity  募集人員
 * @return	募集人員
 */
public String changeNOS(String examCapaRel, String capacity){
	if (examCapaRel.equals("8") || capacity.equals("0")) {
		return "";
	} else if (examCapaRel.equals("9")) {
		return "（募集人員 推定" + enc(capacity) + "名）";
	} else {
		return "（募集人員" + enc(capacity) + "名）";
	}
}
%>
	<script type="text/javascript">
	(function($){
		$("#engptIncludeTitle").click(function() {
			$("#engpt").slideToggle(500);
			if ($("#engptBtn").data("sw") == "1") {
				$("#engptBtn").data("sw", "0").attr("class", "expand-cefr");
			} else {
				$("#engptBtn").data("sw", "1").attr("class", "contract-cefr");
			}
		});
	})(jQuery);
	</script>

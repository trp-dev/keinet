<!DOCTYPE html>
<%@ page pageEncoding="MS932"%>
 
<div id="section04" style="float: left; width: 100%; margin-top: 15px;<c:if test="${!isEngPtDisplay}"> display: none;</c:if>">
    <div  id="engptIncludeTitle">
        <h4 class="section-title" >【参考】英語認定試験を含む共通テスト&nbsp;判定&nbsp;&nbsp;<c:out value="${JudgeI.centerLetterIncludeJudgement}" />
            <div style="float: right; margin-top: 3px;">
                <input type="button" name="" value="" id="engptBtn" class="expand-cefr">
            </div>
        </h4>
    </div>
    <div id="engpt" class="text-inner" style="display: none;">
        <div class="section-inner">
            <div style="display: table-cell; width:100px;">
                <div class="panel">
                    <img src="./shared_lib/img/hantei_<c:out value="${JudgeI.centerLetterJudgement}" />.png">
                </div>
                <c:if test="${JudgeI.isUseAppRequirement()}"><div class="panel eng">
                    <img src="./shared_lib/img/engpt_<c:out value="${JudgeI.engPtHantei}" /><c:out value="${JudgeI.engPtKokuchi}" />.png">
                </div></c:if>
            </div>
            <div class="myscore">
                <p>
                    <span class="point">あなたの得点</span><br />
                    <div class="cl-pc">
                        <span class="text-19">総点　<%= encTableHAI(JudgeI.getC_scoreStr()) %>点</span><span class="text-16">（<%= encTableHAI(JudgeI.cFullPoint) %>点中）</span><br />
                        <c:if test="${JudgeI.isEngPtAdopt() && JudgeI.isUseScoreConversion()}">
                        <span class="text-19">英語認定試験　<c:out value="${JudgeI.engPtScore}" />点（<c:out value="${JudgeI.engPtAllot}" />点中）</span>
                        </c:if>
                    </div>
                    <div class="cl-sp">
                        <span class="text-16">総点</span><br />
                        <span class="text-16"><%= encTableHAI(JudgeI.getC_scoreStr()) %>点（<%= encTableHAI(JudgeI.cFullPoint) %>点中）</span><br />
                        <c:if test="${JudgeI.isEngPtAdopt() && JudgeI.isUseScoreConversion()}">
                        <span class="text-16">英語認定試験</span><br />
                        <span class="text-16"><c:out value="${JudgeI.engPtScore}" />点（<c:out value="${JudgeI.engPtAllot}" />点中）</span>
                        </c:if>
                    </div>
                </p>
                <p>
                    <span class="point">ボーダー得点</span><br />
                    <div class="cl-pc">
                        <span class="text-19"><%= encTableHAI(JudgeE.getBorderPointStr()) %>点</span>
                    </div>
                    <div class="cl-sp">
                        <span class="text-16"><%= encTableHAI(JudgeE.getBorderPointStr()) %>点</span>
                    </div>
                </p>
            </div><!-- /.myscore -->
        </div><!-- /.section-inner -->
        <div class="text-inner notes engpt-exam-info">
            <c:if test="${JudgeI.engPtHantei == 2}"><p class="small"><span class="kome">△</span><span class="indent">…総スコアは要件を満たしている<br />（技能別等他の要件を確認してください）</span></p></c:if>
            <c:if test="${JudgeI.engPtHantei == 3}"><p class="small"><span class="kome">×</span><span class="indent">…必要な要件を満たしていない</span></p></c:if>
            <c:if test="${JudgeI.engPtKokuchi == 1}"><p class="small"><span class="kome">！</span><span class="indent">…取扱が複雑で評価に反映していない</span></p></c:if>
        </div>
    </div><!-- /.text-inner -->
</div><!-- /#section04 -->

<!DOCTYPE html>
<%@ page pageEncoding="MS932"%>

<div id="section00">
	<h3 class="title01">英語認定試験　スコア入力</h3>
	<div class="contents-inner">
		<div class="text-inner">
			<p>英語認定試験のスコアを入力されると、共通テストを利用する入試方式において英語認定試験を反映した共通テストの判定も行います。</p>
			<p>スコアが不明な場合、CEFRまたは合否からも判定は可能です。その場合、入力したCEFRまたは合否におけるスコアの下限値（該当箇所に＊を表示）を用いて判定を行います。</p>
		<!-- /.text-inner --></div>

		<div class="btn clear-btn">
			<input type="button" class="clear-cefr" id="pc-clear-cefr-btn01">
		</div>
		<table id="pc-disp-cefr01" class="table-pc table-cefr-pc" style="overflow: visible;">
			<thead>
				<tr>
					<th>資格・検定試験名�@</th>
					<th class="table-pc-title">スコア</th>
					<th class="table-pc-title">CEFR</th>
					<th class="table-pc-title">合否</th>
					<th class="table-pc-title"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<div id="pc-disp-cefr-exma01"><c:out value="${J101Form.engCert01.dispEngPtName}" /></div></td>
					<td>
						<div id="pc-disp-cefr-score01"><c:out value="${J101Form.engCert01.dispScore}" /></div></td>
					<td>
						<div id="pc-disp-cefr-level01"><c:out value="${J101Form.engCert01.dispCefrLevel}" /></div></td>
					<td>
						<div id="pc-disp-cefr-result01"><c:out value="${J101Form.engCert01.dispResult}" /></div></td>
					<td>
						<input type="hidden" id="pc-cefr-status01" value='<c:out value="${J101Form.engCert01.entryFlg}" />'>
						<input type="button" class="input-cefr"  id="pc-input-cefr-btn01"  <c:if test="${ '1' == J101Form.engCert01.entryFlg }"> style="display: none;"</c:if>>
						<input type="button" class="update-cefr" id="pc-update-cefr-btn01" <c:if test="${ '0' == J101Form.engCert01.entryFlg }"> style="display: none;"</c:if>>
						<input type="button" class="cancel-cefr" id="pc-cancel-cefr-btn01">
					</td>
				</tr>
			</tbody>
		<!-- /.table-pc --></table>
		<div id="pc-input-cefr01" style="display: none;">
			<table class="table-pc table-input-cefr-pc" style="overflow: visible;">
				<tbody>
					<tr>
						<th class="table-pc-title">試験名</th>
						<td>
							<c:forEach var="item" items="${applicationScope.engCert}" varStatus="status">
								<label><input type="radio" class="cefr-engpt" name="pc-engpt01" data-result='<c:out value="${item.resultFlg}" />' data-level='<c:out value="${item.levelFlg}" />' <c:if test="${ item.engPtCd == J101Form.engCert01.engPtCd }"> checked</c:if> value="<c:out value="${item.engPtCd}" />"><c:out value="${item.engPtName}" /></label>
							</c:forEach>
						</td>
					</tr>
					<tr id="pc-engpt-lvl-row01" style="display: none;">
						<th>種類</th>
						<td>
							<c:forEach var="item" items="${applicationScope.engCertDetail}" varStatus="status">
								<label><input type="radio" class="cefr-engpt-lvl" name="pc-engpt-lvl01" data-engptcd="<c:out value="${item.engPtCd}" />" <c:if test="${ item.engPtLevelCd == J101Form.engCert01.engPtLevelCd }"> checked</c:if> value="<c:out value="${item.engPtLevelCd}" />"><c:out value="${item.engPtLevelName}" /></label>
							</c:forEach>
						</td>
					</tr>
					<tr id="pc-score-row01" style="display: none;">
						<th>スコア</th>
						<td style="text-align: right;">
							<input type="number" maxlength="4" min="0" max="9999.9" name="cefr01Score" id="pc-cefr-score01" value="<c:if test="${ '1' == J101Form.engCert01.scoreInputFlg }"><c:out value="${J101Form.engCert01.score}" /></c:if>">
							<p id="pc-unknown-score01" data-sw="0" class="unknown unknown-score">※スコアが不明な方：CEFRを入力</P>
						</td>
					</tr>
					<tr id="pc-cefr-lvl-row01" style="display: none;">
						<th>CEFR</th>
						<td>
							<c:forEach var="item" items="${applicationScope.cefrInfo}" varStatus="status">
								<label><input type="radio" class="cefr-lvl" name="pc-cefr-lvl01" data-cefrcd="<c:out value="${item.cefrCd}" />" <c:if test="${ item.cefrCd == J101Form.engCert01.cefrLevelCd && '1' == J101Form.engCert01.cefrLevelCdInputFlg }"> checked</c:if> value="<c:out value="${item.cefrCd}" />"><c:out value="${item.cefrName}" /></label>
							</c:forEach>
							<p id="pc-unknown-cefr01" data-sw="0" class="unknown unknown-cefr" style="float: right;">※CEFRが不明な方：合否を入力</P>
						</td>
					</tr>
					<tr id="pc-cefr-result-row01" style="display: none;">
						<th>合否</th>
						<td>
							<label><input type="radio" name="pc-cefr-result01" <c:if test="${ '1' == J101Form.engCert01.result }"> checked</c:if> value="1" >合格</label>
							<label><input type="radio" name="pc-cefr-result01" <c:if test="${ '2' == J101Form.engCert01.result }"> checked</c:if> value="2" >不合格</label>
						</td>
					</tr>
				</tbody>
			<!-- /.table-pc --></table>
			<div class="btn" style="margin-top: 6px; display: flex">
				<div id="pc-engcert-error01" class="error engCertScoreError"></div>
				<div >
					<input type="button" class="apply-cefr" id="pc-apply-cefr-btn01">
				</div>
			</div>
		</div>
		<div class="btn clear-btn space">
			<input type="button" class="clear-cefr" id="pc-clear-cefr-btn02">
		</div>
		<table id="pc-disp-cefr02" class="table-pc table-cefr-pc" style="overflow: visible;">
			<thead>
				<tr>
					<th>資格・検定試験名�A</th>
					<th class="table-pc-title">スコア</th>
					<th class="table-pc-title">CEFR</th>
					<th class="table-pc-title">合否</th>
					<th class="table-pc-title"></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<div id="pc-disp-cefr-exma02"><c:out value="${J101Form.engCert02.dispEngPtName}" /></div></td>
					<td>
						<div id="pc-disp-cefr-score02"><c:out value="${J101Form.engCert02.dispScore}" /></div></td>
					<td>
						<div id="pc-disp-cefr-level02"><c:out value="${J101Form.engCert02.dispCefrLevel}" /></div></td>
					<td>
						<div id="pc-disp-cefr-result02"><c:out value="${J101Form.engCert02.dispResult}" /></div></td>
					<td>
						<input type="hidden" id="pc-cefr-status02" value='<c:out value="${J101Form.engCert02.entryFlg}" />'>
						<input type="button" class="input-cefr"  id="pc-input-cefr-btn02"  <c:if test="${ '1' == J101Form.engCert02.entryFlg }"> style="display: none;"</c:if>>
						<input type="button" class="update-cefr" id="pc-update-cefr-btn02" <c:if test="${ '0' == J101Form.engCert02.entryFlg }"> style="display: none;"</c:if>>
						<input type="button" class="cancel-cefr" id="pc-cancel-cefr-btn02">
					</td>
				</tr>
			</tbody>
		<!-- /.table-pc --></table>
		<div id="pc-input-cefr02" style="display: none;">
			<table class="table-pc table-input-cefr-pc" style="overflow: visible;">
				<tbody>
					<tr>
						<th class="table-pc-title">試験名</th>
						<td>
							<c:forEach var="item" items="${applicationScope.engCert}" varStatus="status">
								<label><input type="radio" class="cefr-engpt" name="pc-engpt02" data-result='<c:out value="${item.resultFlg}" />' data-level='<c:out value="${item.levelFlg}" />' <c:if test="${ item.engPtCd == J101Form.engCert02.engPtCd }"> checked</c:if> value="<c:out value="${item.engPtCd}" />"><c:out value="${item.engPtName}" /></label>
							</c:forEach>
						</td>
					</tr>
					<tr id="pc-engpt-lvl-row02" style="display: none;">
						<th>種類</th>
						<td>
							<c:forEach var="item" items="${applicationScope.engCertDetail}" varStatus="status">
								<label><input type="radio" class="cefr-engpt-lvl" name="pc-engpt-lvl02" data-engptcd="<c:out value="${item.engPtCd}" />" <c:if test="${ item.engPtLevelCd == J101Form.engCert02.engPtLevelCd }"> checked</c:if> value="<c:out value="${item.engPtLevelCd}" />"><c:out value="${item.engPtLevelName}" /></label>
							</c:forEach>
						</td>
					</tr>
					<tr id="pc-score-row02" style="display: none;">
						<th>スコア</th>
						<td style="text-align: right;">
							<input type="number" maxlength="4" min="0" max="9999.9" name="cefr01Score" id="pc-cefr-score02" value="<c:if test="${ '−' != J101Form.engCert02.score }"><c:out value="${J101Form.engCert02.score}" /></c:if>" onchange="changeForm(this)">
							<p id="pc-unknown-score02" data-sw="0" class="unknown unknown-score">※スコアが不明な方：CEFRを入力</P>
						</td>
					</tr>
					<tr id="pc-cefr-lvl-row02" style="display: none;">
						<th>CEFR</th>
						<td>
							<c:forEach var="item" items="${applicationScope.cefrInfo}" varStatus="status">
								<label><input type="radio" class="cefr-lvl" name="pc-cefr-lvl02" data-cefrcd="<c:out value="${item.cefrCd}" />" <c:if test="${ item.cefrCd == J101Form.engCert02.cefrLevelCd }"> checked</c:if> value="<c:out value="${item.cefrCd}" />"><c:out value="${item.cefrName}" /></label>
							</c:forEach>
							<p id="pc-unknown-cefr02" data-sw="0" class="unknown unknown-cefr" style="float: right;">※CEFRが不明な方：合否を入力</P>
						</td>
					</tr>
					<tr id="pc-cefr-result-row02" style="display: none;">
						<th>合否</th>
						<td>
							<label><input type="radio" name="pc-cefr-result02" <c:if test="${ '1' == J101Form.engCert02.result }"> checked</c:if> value="1" >合格</label>
							<label><input type="radio" name="pc-cefr-result02" <c:if test="${ '2' == J101Form.engCert02.result }"> checked</c:if> value="2" >不合格</label>
						</td>
					</tr>
				</tbody>
			<!-- /.table-pc --></table>
			<div class="btn" style="margin-top: 6px; display: flex">
				<div id="pc-engcert-error02" class="error engCertScoreError"></div>
				<div >
					<input type="button" class="apply-cefr" id="pc-apply-cefr-btn02">
				</div>
			</div>
		</div>
	<!-- /.contents-inner --></div>
<!-- /#section00 --></div>

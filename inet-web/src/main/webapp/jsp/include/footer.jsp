<!DOCTYPE html>
<%@ page pageEncoding="MS932"%>
<!-- フッターここから -->
<footer>
	<!-- new one -->
	<div class="footer" id="footer">
		<p class="pagetop" id="pagetop">
			<a href="#wrapper"></a>
		</p>
		<div class="footer-line cl-sp"></div>

		<div class="footer-nav">

			<div class="contents">
				<ul>
					<li><a href="https://www.keinet.ne.jp/">Kei-Netホームページ</a></li>
					<li><a href="https://www.keinet.ne.jp/contact/">お問い合わせ</a></li>
					<li><a href="https://www.keinet.ne.jp/suisho.html">ご利用環境</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/privacy/index.html" target="_blank">個人情報保護方針</a></li>
				</ul>
			</div>

			<p class="copyright">Copyright&nbsp;Kawaijuku Educational&nbsp;Information&nbsp;Network.</p>

		</div>
		<!-- footer-nav -->


		<div class="footer-container">
			<p class="footer-logo">
				<a href="https://www.kawai-juku.ac.jp/" target="_blank"><img src="./shared_lib/img/img_footer_logo01.png" alt="河合塾"></a><a href="https://www.kawaijuku.jp/jp/" target="_blank"><img src="./shared_lib/img/img_footer_logo02.png"
					alt="河合塾グループ"></a>
			</p>
			<div class="footer-info cl-pc">
				<p>
					<img src="./shared_lib/img/footer_info.gif" alt="河合塾グループサイトのご案内">
				</p>
				<ul>
					<li><a href="https://www.kawaijuku.jp/jp/kawaijuku/" target="_blank">河合塾グループについて</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/sr/" target="_blank">社会へ向けた取り組み(SR)</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/research/" target="_blank">教育の研究・開発</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/service/" target="_blank">サービス案内</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/recruit/" target="_blank">採用情報</a></li>
					<li><a href="https://www.kawaijuku.jp/jp/inquiry/" target="_blank">お問い合わせ</a></li>
				</ul>
			</div>
		</div>
		<!-- previous one
                    <p class="pagetop"><a href="#wrapper"></a></p>
                    <div class="footer-line"></div>
                    <div class="contents cl-pc">
						<ul>
							<li><a href="http://www.keinet.ne.jp/contact/">お問い合わせ</a></li>
							<li><a href="http://www.keinet.ne.jp/suisho.html">ご利用環境</a></li>
							<li><a href="https://www.kawaijuku.jp/jp/privacy/index.html" target="_blank">個人情報保護方針</a></li>
						</ul>
					</div>
                    <p class="copyright">Copyright&nbsp;&copy;&nbsp;Kawaijuku Educational&nbsp;Information&nbsp;Network.</p>
                    <p class="footer-logo"><a href="https://www.kawai-juku.ac.jp/" target="_blank"><img src="./shared_lib/img/img_footer_logo01.png" alt="河合塾"></a><a href="https://www.kawaijuku.jp/jp/" target="_blank"><img src="./shared_lib/img/img_footer_logo02.png" alt="河合塾グループ"></a></p>
                 previous one to here-->
		<!-- /.footer -->
	</div>
	<!-- /.footer -->
</footer>
<!-- フッターここまで -->

<!-- SP用グローバルナビゲーション ここから -->
<div class="cl-sp g-nav-sp" <c:if test="${ param.forward == null || param.forward == '' || param.forward == 'j001' || param.forward == 'j206' || ErrorCode == '1005' }">style="display:none;"</c:if>>
	<ul>
		<c:choose>
			<c:when test="${ param.forward == 'j101' || param.forward == 'j102' }">
				<li class="g-nav-current">成績入力</li>
			</c:when>
			<c:otherwise>
				<li><a href="javascript:submitMenu('j101')">成績入力</a></li>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${ param.forward == 'j203' }">
				<li class="g-nav-current">大学選択<br>(名称指定)
				</li>
			</c:when>
			<c:otherwise>
				<li><a href="javascript:submitMenu('j203')">大学選択<br>(名称指定)
				</a></li>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${ param.forward == 'j202' }">
				<li class="g-nav-current">大学選択<br>(条件指定)
				</li>
			</c:when>
			<c:otherwise>
				<li><a href="javascript:submitMenu('j202')">大学選択<br>(条件指定)
				</a></li>
			</c:otherwise>
		</c:choose>
	</ul>
</div>
<!-- SP用グローバルナビゲーション ここまで -->

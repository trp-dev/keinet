<%-- \¦ÅåEo^ÅåÌæ¾ --%>
<%@ page pageEncoding="MS932" %>
<fmt:setBundle basename="jp.co.fj.keinavi.util.individual.iprops" var="bundle" />
<c:set var="DispMax"><fmt:message key="DISP_MAX_RESULT" bundle="${bundle}" /></c:set>
<c:set var="RegistMax"><fmt:message key="REGIST_MAX_RESULT" bundle="${bundle}" /></c:set>
<%-- /\¦ÅåEo^ÅåÌæ¾ --%>
<script type="text/javascript">
<!--

	<%-- `FbNóÔðÛ·ézñ --%>
	var checked = new Array();

	<%-- \[gL[CfbNX --%>
<%-- 	var IDX_UNIV_NAME = 13; åw¼Ji --%>
<%-- 	var IDX_CODE = 14; åwR[h{wR[h{úöR[h{wÈR[h --%>
<%-- 	var IDX_RATE = 15; z_ä¦ --%>
<%-- 	var IDX_TOTAL_POINT = 16; ]¿|Cg --%>
<%-- 	var IDX_RATING_CENTER = 17; ¤ÊeXg]¿ --%>
<%-- 	var IDX_RATING_SECOND = 18; Q]¿ --%>
<%-- 	var IDX_BORDER = 19; ¤ÊeXg{[_[¾_¦ --%>
<%-- 	var IDX_RANK = 20; QNÎ·l --%>
<%-- 	var IDX_SORT_KEY = 21; wÈ\[gL[ --%>
    var IDX_UNIV_NAME = 11; <%-- åw¼Ji --%>
    var IDX_CODE = 12; <%-- åwR[h{wR[h{úöR[h{wÈR[h --%>
    var IDX_RATE = 13; <%-- z_ä¦ --%>
    var IDX_TOTAL_POINT = 8; <%-- ]¿ --%>
    var IDX_RATING_CENTER = 15; <%-- ¤ÊeXg]¿ --%>
    var IDX_RATING_SECOND = 16; <%-- Q]¿ --%>
    var IDX_BORDER = 17; <%-- ¤ÊeXg{[_[¾_¦ --%>
    var IDX_RANK = 18; <%-- QNÎ·l --%>
    var IDX_SORT_KEY = 19; <%-- wÈ\[gL[ --%>
    var IDX_DIV_CD = 20; <%-- åwæª --%>
    var IDX_PREF_CD = 21; <%-- ó±Z{§R[h --%>
    var IDX_KANA_NUM = 22; <%-- Jit«50¹Ô --%>
    var IDX_UNIVNIGHT_DIV = 23; <%-- åwéÔæª --%>
    var IDX_FACCON_CD = 24; <%-- wàeR[h --%>
    var IDX_SCEDULE = 25; <%-- åwO[væª --%>
    var IDX_DEPT_NO = 26; <%-- wÈÊÔ --%>
    var IDX_SCSYS = 27; <%-- úöû® --%>
    var IDX_SCSYS_BRNO = 28; <%-- úöû®}Ô --%>
    var IDX_DEPT_NAME = 29; <%-- wÈJi¼ --%>
    var IDX_DEPT_CD = 30; <%-- wÈR[h --%>

	<%-- §ÀÍÍàÈçÊf[^ðìé --%>
	<%--
		0  ... j[NL[
		1  ... åw¼{w¼{wÈ¼
		2  ... ¤ÊeXg{[_+¾_¦ /* 2015/09/07 CCCx¨ add u¾_¦vÇÁÎ */
		3  ... ¤ÊeXg¾_
        4  ... QN
		5  ... QÎ·l
		6  ... ¤ÊeXg]¿
		7  ... Q]¿
		8  ... ]¿
		9  ... z_i¤ÊeXgj
		10 ... z_iQj
		11 ... åw¼Jii\[gpj
		12 ... åwR[h{wR[h{úöR[h{wÈR[hi\[gpj
		13 ... z_ä¦i\[gpj
		14 ... ]¿|Cgi\[gpj
		15 ... ¤ÊeXg]¿i\[gpj
		16 ... Q]¿i\[gpj
		17 ... ¤ÊeXg{[_[¾_¦i\[gpj
		18 ... QNÎ·li\[gpj
		19 ... wÈ\[gL[i\[gpj
		20 ... åwæªi\[gpj
		21 ... ó±Z{§R[hi\[gpj
		22 ... Jit«50¹Ôi\[gpj
		23 ... åwéÔæªi\[gpj
		24 ... wàeR[hi\[gpj
		25 ... åwO[væªi\[gpj
		26 ... wÈÊÔi\[gpj
		27 ... úöû®i\[gpj
		28 ... úöû®}Ôi\[gpj
		29 ... wÈJi¼i\[gpj
		30 ... wÈR[hi\[gpj
	--%>
	var data = new Array();
	<c:if test="${ bean.recordCount <= DispMax }">
	/* 2015/09/07 CCCx¨ mod startu{[_[¾_¦vÇÁÎ*/
		<c:forEach var="univ" items="${bean.recordSetAll}" varStatus="status">
		data[<c:out value="${status.index}" />] = new Array(
		"<c:out value="${univ.uniqueKey}" />",
		"<c:out value="${univ.univFullName}" />",
		"<c:out value="${univ.centerScore}" />",
		"<c:out value="${univ.c_scoreStr}" />",
		"<c:out value="${univ.rank}" />",
		"<c:out value="${univ.s_scoreStr}" />",
		"<c:out value="${univ.CLetterJudgement}" />",
		"<c:out value="${univ.SLetterJudgement}" />",
		"<c:out value="${univ.TLetterJudgement}" />",
		<c:if test="${univ.CAllotPntRateAll != ' - '}">"<c:out value="${univ.CAllotPntRateAll}" />"</c:if><c:if test="${univ.CAllotPntRateAll == ' - '}">""</c:if>,
		<c:if test="${univ.SAllotPntRateAll != ' - '}">"<c:out value="${univ.SAllotPntRateAll}" />"</c:if><c:if test="${univ.SAllotPntRateAll == ' - '}">""</c:if>,
		"<c:out value="${univ.univNameKana}" />",
		"<c:out value="${univ.univCd}" /><c:out value="${univ.facultyCd}" /><kn:sortKey target="schedule" /><c:out value="${univ.deptSortKey}" />",
		<kn:sortKey target="rate" />,
		<c:out value="${univ.t_score}" />,
		"<kn:sortKey target="center" />",
		"<kn:sortKey target="second" />",
		<kn:sortKey target="border" />,
		<kn:sortKey target="rank" />,
		"<c:out value="${univ.deptSortKey}" />",
		"<c:out value="${univ.univDivCd}" />",
		"<c:out value="${univ.prefCd}" />",
		"<c:out value="${univ.kanaNum}" />",
		"<c:out value="${univ.uniNightDiv}" />",
		"<c:out value="${univ.facultyConCd}" />",
		"<kn:sortKey target="schedule" />",
		"<c:out value="${univ.deptSerialNo}" />",
		"<c:out value="${univ.scheduleSys}" />",
		"<c:out value="${univ.scheduleSysBranchCd}" />",
		"<c:out value="${univ.deptNameKana}" />",
		"<c:out value="${univ.deptCd}" />");
		/* 2015/09/07 CCCx¨ mod end u{[_[¾_¦vÇÁÎ*/
		</c:forEach>
	</c:if>

	<%-- e[uð`æ·é --%>
	function drawTable() {
		$(sw.getLayer("ListTBody")).children().each(function(i, tr){
		if (data.length > i) {
			var color = '#F4E5D6';
			<c:if test="${param.forward == 'j204'}">
			if (registed[data[i][0]]) {
				if (registed[data[i][0]]) {
	    	        $(tr).addClass("selected");
				} else {
					$(tr).removeClass("selected");
				}
			} else {
					$(tr).removeClass("selected");
			}
			</c:if>
			$(tr).children().each(function(j, td){

				var span = $(td).children(":first");


		<c:choose>
			<c:when test="${ param.forward == 'j204' }">
				if (j == 0) {
					<%-- ¡`FbN{bNX--%>
					if (registed[data[i][0]]) {
						if (registed[data[i][0]]) {
							$(span).html("<input type=\"checkbox\" name=\"dummy\" style=\"visibility:hidden\">");
						} else {
							$(span).html("<input type=\"checkbox\" name=\"univValue\" value=\"" + data[i][0] + "\" onclick=\"selectUniv(this)\""+ (checked[data[i][0]] ? " checked" : "") + ">");
						}
					} else {
						$(span).html("<input type=\"checkbox\" name=\"univValue\" value=\"" + data[i][0] + "\" onclick=\"selectUniv(this)\"" + (checked[data[i][0]] ? " checked" : "") + ">");
					}
				} else if (j == 1) {
					<%-- ¡Ú× --%>
                    $(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
				} else {
					/* 2015/09/07 CCCx¨ mod start u{[_[¾_¦vÇÁÎ*/
					var border = data[i][2];
					var arr = new Array();
					if(border != null){
							var border1 =border.replace(/\.0/g, '');
							data[i][2]=border1;
					}
					/* 2015/09/07 CCCx¨ mod endu{[_[¾_¦vÇÁÎ*/
					$(span).text(data[i][j]);
				}
			</c:when>
			<c:when test="${ param.forward == 'j301' }">
				if (j == 0) {
					<%-- ¡Ú× --%>
                    $(span).html("<a href=\"javascript:openDetail('" + data[i][0] + "')\">" + data[i][1] + "</a>");
				}
				else if (j == 10) {
					<%-- ¡í --%>
                    $(span).html("<input type=\"button\" value=\"í\" onclick=\"javascript:deleteUniv('" + data[i][0] + "');\">");
				}
				else {
					/* 2015/09/07 CCCx¨ mod start u{[_[¾_¦vÇÁÎ*/
					var border = data[i][2];
					var arr = new Array();
					if(border != null){
					var border1 =border.replace(/\.0/g, '');
					data[i][2]=border1;
					}
					$(span).text(data[i][j+1]);
					/* 2015/09/07 CCCx¨ mod end u{[_[¾_¦vÇÁÎ*/
				}
			</c:when>
		</c:choose>
			});
		}
		});
	}

	<%-- ¸\[g --%>
	function sortByAsc(a, b, index) {
		if (a[index] == b[index]) return 0;
		else if (a[index] > b[index]) return 1;
		else if (a[index] < b[index]) return -1;
	}

	<%-- ~\[g --%>
	function sortByDesc(a, b, index) {
		return -(sortByAsc(a, b, index));
	}

	<%-- ¸\[g(]¿p) --%>
	function sortByAscTotal(a, b, index) {
		var avalue = convertRating(a[index]);
		var bvalue = convertRating(b[index]);
		if (avalue == bvalue) return 0;
		else if (avalue > bvalue) return 1;
		else if (avalue < bvalue) return -1;
	}

	<%-- ~\[g(]¿p) --%>
	function sortByDescTotal(a, b, index) {
		return -(sortByAscTotal(a, b, index));
	}

	<%-- \[gÀs --%>
	<%-- \[gÀs --%>
	function execSort(mode) {
		form.sortKey.value = mode;
		switch (parseInt(mode)) {
			<%-- åwR[h --%>
			case 0:
				data.sort(
					function (a, b) {
						<%-- åwæªi¸j--%>
						var value = sortByAsc(a, b, IDX_DIV_CD);
						if (value == 0) {
							<%-- ó±Z{§R[hi¸j--%>
							var value = sortByAsc(a, b, IDX_PREF_CD);
							if (value == 0) {
								<%-- Jit«50¹Ôi¸j--%>
								var value = sortByAsc(a, b, IDX_KANA_NUM);
								if (value == 0) {
									<%-- åwéÔæªi¸j--%>
									var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
									if (value == 0) {
										<%-- wàeR[hi¸j--%>
										var value = sortByAsc(a, b, IDX_FACCON_CD);
										if (value == 0) {
											<%-- åwO[væªi¸j--%>
											var value = sortByAsc(a, b, IDX_SCEDULE);
											if (value == 0) {
												<%-- wÈÊÔi¸j--%>
												var value = sortByAsc(a, b, IDX_DEPT_NO);
												if (value == 0) {
													<%-- úöû®i¸j--%>
													var value = sortByAsc(a, b, IDX_SCSYS);
													if (value == 0) {
														<%-- úöû®}Ôi¸j--%>
														var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
														if (value == 0) {
															<%-- wÈJi¼i¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NAME);
															if (value == 0) {
																<%-- wÈR[hi¸j--%>
																return sortByAsc(a, b, IDX_DEPT_CD);
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);
				break;

			<%-- åw¼ --%>
			case 1:
				data.sort(
					function (a, b) {
						<%-- Jit«50¹Ôi¸j--%>
						var value = sortByAsc(a, b, IDX_KANA_NUM);
						if (value == 0) {
							<%-- åwæªi¸j--%>
							var value = sortByAsc(a, b, IDX_DIV_CD);
							if (value == 0) {
								<%-- ó±Z{§R[hi¸j--%>
								var value = sortByAsc(a, b, IDX_PREF_CD);
								if (value == 0) {
									<%-- åwéÔæªi¸j--%>
									var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
									if (value == 0) {
										<%-- wàeR[hi¸j--%>
										var value = sortByAsc(a, b, IDX_FACCON_CD);
										if (value == 0) {
											<%-- åwO[væªi¸j--%>
											var value = sortByAsc(a, b, IDX_SCEDULE);
											if (value == 0) {
												<%-- wÈÊÔi¸j--%>
												var value = sortByAsc(a, b, IDX_DEPT_NO);
												if (value == 0) {
													<%-- úöû®i¸j--%>
													var value = sortByAsc(a, b, IDX_SCSYS);
													if (value == 0) {
														<%-- úöû®}Ôi¸j--%>
														var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
														if (value == 0) {
															<%-- wÈJi¼i¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NAME);
															if (value == 0) {
																<%-- wÈR[hi¸j--%>
																return sortByAsc(a, b, IDX_DEPT_CD);
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- ¤ÊeXg]¿ --%>
			case 2:
				data.sort(
					function (a, b) {
						<%-- ¤ÊeXg]¿i¸j--%>
						var value = sortByAsc(a, b, IDX_RATING_CENTER);
						if (value == 0) {
							<%-- ¤ÊeXg{[_[¾_¦i~j--%>
							var value = sortByDesc(a, b, IDX_BORDER);
							if (value == 0) {
								<%-- z_ä¦i~j--%>
								var value = sortByDesc(a, b, IDX_RATE);
								if (value == 0) {
									<%-- åwæªi¸j--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- ó±Z{§R[hi¸j--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- Jit«50¹Ôi¸j--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- åwéÔæªi¸j--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- wàeR[hi¸j--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- åwO[væªi¸j--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- wÈÊÔi¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- úöû®i¸j--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- úöû®}Ôi¸j--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- wÈJi¼i¸j--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- wÈR[hi¸j--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 2]¿ --%>
			case 3:
				data.sort(
					function (a, b) {
						<%-- Q]¿i¸j--%>
						var value = sortByAsc(a, b, IDX_RATING_SECOND);
						if (value == 0) {
							<%-- QNÎ·li~j--%>
							var value = sortByDesc(a, b, IDX_RANK);
							if (value == 0) {
								<%-- z_ä¦i¸j--%>
								var value = sortByAsc(a, b, IDX_RATE);
								if (value == 0) {
									<%-- åwæªi¸j--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- ó±Z{§R[hi¸j--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- Jit«50¹Ôi¸j--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- åwéÔæªi¸j--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- wàeR[hi¸j--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- åwO[væªi¸j--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- wÈÊÔi¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- úöû®i¸j--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- úöû®}Ôi¸j--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- wÈJi¼i¸j--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- wÈR[hi¸j--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);


				break;
			<%-- ]¿ --%>
			case 4:
				data.sort(
					function (a, b) {
						<%-- ]¿(¸)--%>
						var value = sortByAscTotal(a, b, IDX_TOTAL_POINT);
						if (value == 0) {
							<%-- åwæªi¸j--%>
							var value = sortByAsc(a, b, IDX_DIV_CD);
							if (value == 0) {
								<%-- ó±Z{§R[hi¸j--%>
								var value = sortByAsc(a, b, IDX_PREF_CD);
								if (value == 0) {
									<%-- Jit«50¹Ôi¸j--%>
									var value = sortByAsc(a, b, IDX_KANA_NUM);
									if (value == 0) {
										<%-- åwéÔæªi¸j--%>
										var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
										if (value == 0) {
											<%-- wàeR[hi¸j--%>
											var value = sortByAsc(a, b, IDX_FACCON_CD);
											if (value == 0) {
												<%-- åwO[væªi¸j--%>
												var value = sortByAsc(a, b, IDX_SCEDULE);
												if (value == 0) {
													<%-- wÈÊÔi¸j--%>
													var value = sortByAsc(a, b, IDX_DEPT_NO);
													if (value == 0) {
														<%-- úöû®i¸j--%>
														var value = sortByAsc(a, b, IDX_SCSYS);
														if (value == 0) {
															<%-- úöû®}Ôi¸j--%>
															var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
															if (value == 0) {
																<%-- wÈJi¼i¸j--%>
																var value = sortByAsc(a, b, IDX_DEPT_NAME);
																if (value == 0) {
																	<%-- wÈR[hi¸j--%>
																	return sortByAsc(a, b, IDX_DEPT_CD);
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- ¤ÊeXgä --%>
			case 5:
				data.sort(
					function (a, b) {
						<%-- z_ä¦i~j--%>
						var value = sortByDesc(a, b, IDX_RATE);
						if (value == 0) {
							<%-- ¤ÊeXg]¿i¸j--%>
							var value = sortByAsc(a, b, IDX_RATING_CENTER);
							if (value == 0) {
								<%-- ¤ÊeXg{[_[¾_¦i~j--%>
								var value = sortByDesc(a, b, IDX_BORDER);
								if (value == 0) {
									<%-- åwæªi¸j--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- ó±Z{§R[hi¸j--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- Jit«50¹Ôi¸j--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- åwéÔæªi¸j--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- wàeR[hi¸j--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- åwO[væªi¸j--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- wÈÊÔi¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- úöû®i¸j--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- úöû®}Ôi¸j--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- wÈJi¼i¸j--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- wÈR[hi¸j--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- 2ä --%>
			case 6:
				data.sort(
					function (a, b) {
						<%-- z_ä¦i¸j--%>
						var value = sortByAsc(a, b, IDX_RATE);
						if (value == 0) {
							<%-- Q]¿i¸j--%>
							var value = sortByAsc(a, b, IDX_RATING_SECOND);
							if (value == 0) {
								<%-- QNÎ·li~j--%>
								var value = sortByDesc(a, b, IDX_RANK);
								if (value == 0) {
									<%-- åwæªi¸j--%>
									var value = sortByAsc(a, b, IDX_DIV_CD);
									if (value == 0) {
										<%-- ó±Z{§R[hi¸j--%>
										var value = sortByAsc(a, b, IDX_PREF_CD);
										if (value == 0) {
											<%-- Jit«50¹Ôi¸j--%>
											var value = sortByAsc(a, b, IDX_KANA_NUM);
											if (value == 0) {
												<%-- åwéÔæªi¸j--%>
												var value = sortByAsc(a, b, IDX_UNIVNIGHT_DIV);
												if (value == 0) {
													<%-- wàeR[hi¸j--%>
													var value = sortByAsc(a, b, IDX_FACCON_CD);
													if (value == 0) {
														<%-- åwO[væªi¸j--%>
														var value = sortByAsc(a, b, IDX_SCEDULE);
														if (value == 0) {
															<%-- wÈÊÔi¸j--%>
															var value = sortByAsc(a, b, IDX_DEPT_NO);
															if (value == 0) {
																<%-- úöû®i¸j--%>
																var value = sortByAsc(a, b, IDX_SCSYS);
																if (value == 0) {
																	<%-- úöû®}Ôi¸j--%>
																	var value = sortByAsc(a, b, IDX_SCSYS_BRNO);
																	if (value == 0) {
																		<%-- wÈJi¼i¸j--%>
																		var value = sortByAsc(a, b, IDX_DEPT_NAME);
																		if (value == 0) {
																			<%-- wÈR[hi¸j--%>
																			return sortByAsc(a, b, IDX_DEPT_CD);
																		} else {
																			return value;
																		}
																	} else {
																		return value;
																	}
																} else {
																	return value;
																}
															} else {
																return value;
															}
														} else {
															return value;
														}
													} else {
														return value;
													}
												} else {
													return value;
												}
											} else {
												return value;
											}
										} else {
											return value;
										}
									} else {
										return value;
									}
								} else {
									return value;
								}
							} else {
								return value;
							}
						} else {
							return value;
						}
					}
				);

				break;
			<%-- s¾ --%>
			default:
				alert("\[gG[F" + mode);
		}
		<%-- Ä`æ --%>
		drawTable();
	}

	<%-- ÊíÌ\[g --%>
	function sortNormal(index) {
		oc.sort(
			function (a, b) {
				if (a[index] == b[index]) return 0;
				else if (a[index] > b[index]) return 1;
				else if (a[index] < b[index]) return -1;
			}
		);
		// Ä`æ
		drawTable2();
	}

	function convertRating(c) {
		switch (c){
		  case "A":
		    return "10";
		  case "B":
			return "11";
		  case "C":
			return "12";
		  case "D":
			return "13";
		  case "E":
			return "14";
		  case "AH":
			return "30";
		  case "BH":
		    return "31";
	      case "CH":
			return "32";
	      case "DH":
			return "33";
		  case "EH":
			return "34";
		  case "A#":
			return "40";
		  case "B#":
		    return "41";
		  case "C#":
			return "42";
		  case "D#":
		    return "43";
	      case "E#":
		    return "44";
		  case "AG":
			return "50";
	      case "BG":
		    return "51";
		  case "CG":
		    return "52";
		  case "DG":
		    return "53";
		  case "EG":
		    return "54";
		  case "H":
		    return "70";
		  case "G":
			return "71";
	      case "*":
			return "90";
		  case "  ":
			return "91";
		}
	}
// -->
</script>

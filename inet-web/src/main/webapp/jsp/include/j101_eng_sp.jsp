<!DOCTYPE html>
<%@ page pageEncoding="MS932"%>

<div id="sectionSp00">
	<h3 class="title01">英語認定試験 スコア入力</h3>
	<div class="contents-inner">
		<div class="text-inner">
			<p>英語認定試験のスコアを入力されると、共通テストを利用する入試方式において英語認定試験を反映した共通テストの判定も行います。</p>
			<p>スコアが不明な場合、CEFRまたは合否からも判定は可能です。その場合、入力したCEFRまたは合否におけるスコアの下限値（該当箇所に＊を表示）を用いて判定を行います。</p>
			<!-- /.text-inner -->
		</div>
		<div class="btn clear-btn">
			<input type="button" class="clear-cefr" id="sp-clear-cefr-btn01">
		</div>
		<table id="sp-disp-cefr01" class="table-sp table-cefr-sp">
			<tbody>
			<tr>
				<th style="width: auto;">資格・検定試験名�@</th>
				<th style="width: 15%;">スコア</th>
				<th style="width: 15%;">CEFR</th>
				<th style="width: 15%;">合否</th>
				<th style="width: auto;">
				</th>
			</tr>
			<tr>
				<td>
					<div id="sp-disp-cefr-exma01"><c:out value="${J101Form.engCert01.dispEngPtName}" /></div></td>
				<td>
					<div id="sp-disp-cefr-score01"><c:out value="${J101Form.engCert01.dispScore}" /></div></td>
				<td>
					<div id="sp-disp-cefr-level01"><c:out value="${J101Form.engCert01.dispCefrLevel}" /></div></td>
				<td>
					<div id="sp-disp-cefr-result01"><c:out value="${J101Form.engCert01.dispResult}" /></div></td>
				<td>
					<input type="hidden" id="sp-cefr-status01" value='<c:out value="${J101Form.engCert01.entryFlg}" />'>
					<input type="button" class="input-cefr  sp-input-cefr"  id="sp-input-cefr-btn01"  <c:if test="${ '1' == J101Form.engCert01.entryFlg }"> style="display: none;"</c:if>>
					<input type="button" class="update-cefr sp-update-cefr" id="sp-update-cefr-btn01" <c:if test="${ '0' == J101Form.engCert01.entryFlg }"> style="display: none;"</c:if>>
					<input type="button" class="cancel-cefr sp-cancel-cefr" id="sp-cancel-cefr-btn01">
				</td>
			</tr>
		<!-- /.table-sp --></tbody></table>
		<div id="sp-input-cefr01" style="border-bottom: #547adb 1px solid; padding-bottom: 15px; display: none;">
			<table class="sp-cefr" style="margin-bottom: 5px;">
				<tbody>
					<tr>
						<td>
							<div class="title">試験名</div>
							<div class="item">
								<c:forEach var="item" items="${applicationScope.engCert}" varStatus="status">
								<label><input type="radio" class="cefr-engpt" name="sp-engpt01" data-result='<c:out value="${item.resultFlg}" />' data-level='<c:out value="${item.levelFlg}" />' <c:if test="${ item.engPtCd == J101Form.engCert01.engPtCd }"> checked</c:if> value="<c:out value="${item.engPtCd}" />"><c:out value="${item.engPtName}" /></label>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr id="sp-engpt-lvl-row01" style="display: none;">
						<td>
							<div class="title">種類</div>
							<div class="item">
								<c:forEach var="item" items="${applicationScope.engCertDetail}" varStatus="status">
								<label><input type="radio" class="cefr-engpt-lvl" name="sp-engpt-lvl01" data-engptcd="<c:out value="${item.engPtCd}" />" <c:if test="${ item.engPtLevelCd == J101Form.engCert01.engPtLevelCd }"> checked</c:if> value="<c:out value="${item.engPtLevelCd}" />"><c:out value="${item.engPtLevelName}" /></label>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr id="sp-score-row01" style="display: none;">
						<td>
							<div class="title">スコア</div>
							<div class="item" style="text-align: right;">
								<p>
									<input type="number" maxlength="4" min="0" max="9999.9" name="cefr01ScoreSP" id="sp-cefr-score01" value="<c:if test="${ '−' != J101Form.engCert01.score }"><c:out value="${J101Form.engCert01.score}" /></c:if>" onchange="changeForm(this)">
								</p>
								<p id="sp-unknown-score01" data-sw="0" class="unknown unknown-score">※スコアが不明な方：CEFRを入力</P>
							</div>
						</td>
					</tr>
					<tr id="sp-cefr-lvl-row01" style="display: none;">
						<td>
							<div class="title">CEFR</div>
							<div class="item" style="float: right;">
								<p>
									<c:forEach var="item" items="${applicationScope.cefrInfo}" varStatus="status">
									<label><input type="radio" class="cefr-lvl" name="sp-cefr-lvl01" data-cefrcd="<c:out value="${item.cefrCd}" />" <c:if test="${ item.cefrCd == J101Form.engCert01.cefrLevelCd }"> checked</c:if> value="<c:out value="${item.cefrCd}" />"><c:out value="${item.cefrName}" /></label>
									</c:forEach>
								</p>
								<p id="sp-unknown-cefr01" data-sw="0" class="unknown unknown-cefr" style="float: right;">※CEFRが不明な方：合否を入力</P>
							</div>
						</td>
					</tr>
					<tr id="sp-cefr-result-row01" style="display: none;">
						<td>
							<div class="title">合否</div>
							<div class="item">
								<label><input type="radio" name="sp-cefr-result01" <c:if test="${ '1' == J101Form.engCert01.result }"> checked</c:if> value="1" >合格</label>
								<label><input type="radio" name="sp-cefr-result01" <c:if test="${ '2' == J101Form.engCert01.result }"> checked</c:if> value="2" >不合格</label>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn" style="margin-top: 6px; height: auto;">
				<input type="button" class="apply-cefr" id="sp-apply-cefr-btn01">
				<div id="sp-engcert-error01" class="error engCertScoreError"></div>
			</div>
		</div>
		<div class="btn clear-btn space">
			<input type="button" class="clear-cefr" id="sp-clear-cefr-btn02">
		</div>
		<table id="sp-disp-cefr02" class="table-sp table-cefr-sp">
			<tbody>
			<tr>
				<th style="width: auto;">資格・検定試験名�A</th>
				<th style="width: 15%;">スコア</th>
				<th style="width: 15%;">CEFR</th>
				<th style="width: 15%;">合否</th>
				<th style="width: auto;">
				</th>
			</tr>
			<tr>
				<td>
					<div id="sp-disp-cefr-exma02"><c:out value="${J101Form.engCert02.dispEngPtName}" /></div></td>
				<td>
					<div id="sp-disp-cefr-score02"><c:out value="${J101Form.engCert02.dispScore}" /></div></td>
				<td>
					<div id="sp-disp-cefr-level02"><c:out value="${J101Form.engCert02.dispCefrLevel}" /></div></td>
				<td>
					<div id="sp-disp-cefr-result02"><c:out value="${J101Form.engCert02.dispResult}" /></div></td>
				<td>
					<input type="hidden" id="sp-cefr-status02" value='<c:out value="${J101Form.engCert02.entryFlg}" />'>
					<input type="button" class="input-cefr  sp-input-cefr"  id="sp-input-cefr-btn02"  <c:if test="${ '1' == J101Form.engCert02.entryFlg }"> style="display: none;"</c:if>>
					<input type="button" class="update-cefr sp-update-cefr" id="sp-update-cefr-btn02" <c:if test="${ '0' == J101Form.engCert02.entryFlg }"> style="display: none;"</c:if>>
					<input type="button" class="cancel-cefr sp-cancel-cefr" id="sp-cancel-cefr-btn02">
				</td>
			</tr>
		<!-- /.table-sp --></tbody></table>
		<div id="sp-input-cefr02" style="border-bottom: #547adb 1px solid; padding-bottom: 15px; display: none;">
			<table class="sp-cefr" style="margin-bottom: 5px;">
				<tbody>
					<tr>
						<td>
							<div class="title">試験名</div>
							<div class="item">
								<c:forEach var="item" items="${applicationScope.engCert}" varStatus="status">
								<label><input type="radio" class="cefr-engpt" name="sp-engpt02" data-result='<c:out value="${item.resultFlg}" />' data-level='<c:out value="${item.levelFlg}" />' <c:if test="${ item.engPtCd == J101Form.engCert02.engPtCd }"> checked</c:if> value="<c:out value="${item.engPtCd}" />"><c:out value="${item.engPtName}" /></label>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr id="sp-engpt-lvl-row02" style="display: none;">
						<td>
							<div class="title">種類</div>
							<div class="item">
								<c:forEach var="item" items="${applicationScope.engCertDetail}" varStatus="status">
								<label><input type="radio" class="cefr-engpt-lvl" name="sp-engpt-lvl02" data-engptcd="<c:out value="${item.engPtCd}" />" <c:if test="${ item.engPtLevelCd == J101Form.engCert02.engPtLevelCd }"> checked</c:if> value="<c:out value="${item.engPtLevelCd}" />"><c:out value="${item.engPtLevelName}" /></label>
								</c:forEach>
							</div>
						</td>
					</tr>
					<tr id="sp-score-row02" style="display: none;">
						<td>
							<div class="title">スコア</div>
							<div class="item" style="text-align: right;">
								<p>
									<input type="number" maxlength="4" min="0" max="9999.9" name="cefr02ScoreSP" id="sp-cefr-score02" value="<c:if test="${ '−' != J101Form.engCert02.score }"><c:out value="${J101Form.engCert02.score}" /></c:if>" onchange="changeForm(this)">
								</p>
								<p id="sp-unknown-score02" data-sw="0" class="unknown unknown-score">※スコアが不明な方：CEFRを入力</P>
							</div>
						</td>
					</tr>
					<tr id="sp-cefr-lvl-row02" style="display: none;">
						<td>
							<div class="title">CEFR</div>
							<div class="item" style="float: right;">
								<p>
									<c:forEach var="item" items="${applicationScope.cefrInfo}" varStatus="status">
									<label><input type="radio" class="cefr-lvl" name="sp-cefr-lvl02" data-cefrcd="<c:out value="${item.cefrCd}" />" <c:if test="${ item.cefrCd == J101Form.engCert02.cefrLevelCd }"> checked</c:if> value="<c:out value="${item.cefrCd}" />"><c:out value="${item.cefrName}" /></label>
									</c:forEach>
								</p>
								<p id="sp-unknown-cefr02" data-sw="0" class="unknown unknown-cefr" style="float: right;">※CEFRが不明な方：合否を入力</P>
							</div>
						</td>
					</tr>
					<tr id="sp-cefr-result-row02" style="display: none;">
						<td>
							<div class="title">合否</div>
							<div class="item">
								<label><input type="radio" name="sp-cefr-result02" <c:if test="${ '1' == J101Form.engCert02.result }"> checked</c:if> value="1" >合格</label>
								<label><input type="radio" name="sp-cefr-result02" <c:if test="${ '2' == J101Form.engCert02.result }"> checked</c:if> value="2" >不合格</label>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="btn" style="margin-top: 6px; height: auto;">
				<input type="button" class="apply-cefr" id="sp-apply-cefr-btn02">
				<div id="sp-engcert-error02" class="error engCertScoreError"></div>
			</div>
		</div>
	<!-- /.contents-inner --></div>
<!-- /#section-sp00 --></div>

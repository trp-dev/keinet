<!DOCTYPE html>
<%@ page pageEncoding="MS932"%>
<!-- ヘッダーここから -->
<header class="header">

	<div class="header-logo cl-sp">
		<div class="cl-inline-sp">
			<h1>
				<a href="https://www.keinet.ne.jp/"><img src="./shared_lib/img/img_header_logo.png" alt="河合塾が運営する大学情報サイト　Kei-net"></a>
			</h1>
		</div>
		<p>
			<a href="https://www.kawai-juku.ac.jp/" target="_blank"><img src="./shared_lib/img/kawai_logo2.gif" alt="河合塾"></a>
		</p>
	</div>
	<div id="gHeader" class="gStyle cl-pc">
		<div class="gStyleIn">
			<p class="groupLogo2">
				<a href="https://www.kawai-juku.ac.jp/" target="_blank"><img src="./shared_lib/img/kawai_logo.gif" class="normal" alt="河合塾" width="62" height="13" /></a>
			</p>
			<p class="groupLogo">
				<a href="https://www.kawaijuku.jp/jp/" target="_blank"><img src="./shared_lib/img/header_group.png" alt="河合塾グループ" width="65" height="13" class="normal" /></a>
			</p>
			<!-- / class gStyleIn -->
		</div>
		<!-- / id gHeader -->
	</div>
	<div class="header-logo2 cl-pc">
		<h1>
			<a href="https://www.keinet.ne.jp/"><img src="./shared_lib/img/img_header_logo.png" alt="河合塾が運営する大学情報サイト　Kei-net"></a>
		</h1>
		<!-- /.header-logo2 -->
	</div>

	<div class="header-line">
		<div class="header-line-inner">
			<h2 class="header-title">
				<c:choose>
					<c:when test="${ param.forward == '' || submitTop == 'false' || ex.close }">
						模試判定システム
					</c:when>
					<c:otherwise>
						<a href="javascript:submitMenu('j001')">模試判定システム</a>
					</c:otherwise>
				</c:choose>
			</h2>
			<p class="help">
				<a href="https://www.keinet.ne.jp/web/hantei/helpindex.html" target="new"></a>
			</p>
			<!-- /.header-line-inner -->
		</div>
		<!-- /.header-line -->
	</div>

	<%-- グローバルナビゲーション --%>
	<div class="g-nav cl-pc" <c:if test="${ param.forward == null || param.forward == '' || param.forward == 'j001' || param.forward == 'j206' || ErrorCode == '1005' }">style="display:none;"</c:if>>
		<ul>
			<c:choose>
				<c:when test="${ param.forward == 'j101' || param.forward == 'j102' }">
					<li class="g-nav-current">成績入力</li>
				</c:when>
				<c:otherwise>
					<li><a href="javascript:submitMenu('j101')">成績入力</a></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${ param.forward == 'j203' }">
					<li class="g-nav-current">大学選択（名称指定）</li>
				</c:when>
				<c:otherwise>
					<li><a href="javascript:submitMenu('j203')">大学選択（名称指定）</a></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${ param.forward == 'j202' }">
					<li class="g-nav-current">大学選択（条件指定）</li>
				</c:when>
				<c:otherwise>
					<li><a href="javascript:submitMenu('j202')">大学選択（条件指定）</a></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${ param.forward == 'j301' }">
					<li class="g-nav-current">登録大学</li>
				</c:when>
				<c:otherwise>
					<c:choose>
						<c:when test="${ empty UnivList }">
							<li>登録大学</li>
						</c:when>
						<c:otherwise>
							<li><a href="javascript:submitMenu('j301')">登録大学</a></li>
						</c:otherwise>
					</c:choose>
				</c:otherwise>
			</c:choose>
		</ul>
	</div>
</header>
<!-- ヘッダーここまで -->

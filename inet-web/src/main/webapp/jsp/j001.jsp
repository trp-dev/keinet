<!DOCTYPE html>
<%@ page contentType="text/html;charset=MS932"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld"%>
<c:set var="simpleLayout" value="true" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<title>模試判定システム</title>
<link rel="stylesheet" href="./shared_lib/style/normalize.css">
<link rel="stylesheet" href="./shared_lib/style/common.css">
<link rel="stylesheet" href="./shared_lib/style/button.css">
<link rel="stylesheet" href="./shared_lib/style/buttonsp.css" media="(max-width:640px)">
<link rel="stylesheet" href="./shared_lib/style/sp.css" media="(max-width:640px)">
<link rel="stylesheet" href="./shared_lib/style/j001pc.css">
<link rel="stylesheet" href="./shared_lib/style/j001sp.css" media="(max-width:640px)">

<!--[if lt IE 9]>
		<script src="./js/html5shiv-printshiv.js" type="text/javascript"></script>
		<link rel="stylesheet" href="./shared_lib/style/ie8.css">
		<![endif]-->
<script src="./js/footerFixed.js" type="text/javascript"></script>
<script src="./js/jquery-1.7.min.js" type="text/javascript"></script>
<script src="./js/jquery.smoothScroll.js" type="text/javascript"></script>
<script type="text/javascript" src="./js/OpenWindow.js"></script>
<script type="text/javascript">
	var form;
<%@ include file="/jsp/script/submit_exit.jsp" %>
	function init() {
		var msg = "<c:out value="${ErrorMessage}" />";
		if (msg != "")
			alert(msg);
	}

	function jump(rinkno) {
		document.forms[0].rinkno.value = rinkno;
		submitMenu('j101');
	}
<%@ include file="/jsp/script/submit_menu.jsp" %>
	// Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
	if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
		document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
	}
</script>
</head>

<body onload="init()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
	<div id="wrapper"></div>
	<form action="<c:url value="Top" />" method="POST">
		<c:set var="submitTop" value="false" />
		<input type="hidden" name="forward" value="j101"> <input type="hidden" name="backward" value="j001"> <input type="hidden" name="rinkno" value="Z">

		<%@ include file="/jsp/include/header.jsp"%>

		<!-- コンテンツここから -->
		<div class="container">
			<!-- 2015/09/07 horihata del start -->
			<!-- <h3 class="title01 cl-pc">模試判定システムへようこそ</h3> -->
			<!-- 2015/09/07 horihata del end -->
			<div class="contents line">


				<div class="contents-inner">
					<div class="text-inner pickup">
						<p>
							模試判定システムは、河合塾の全統模試の成績をもとに志望校の合格可能性評価を行うシステムです。<br>全統模試の個人成績表を手元にご用意いただき、成績入力・志望校の選択を行ってください。
						</p>
					</div>
					<div class="btn submit-btn cl-pc">
						<input type="submit" name="" value="" onClick="javascript:submitMenu('j101')" id="start-btn">
					</div>
					<!-- /.contents-inner -->
				</div>
				<div class="contents-inner next cl-sp">
					<ul>
						<li><a href="javascript:jump('A')" class="multiline">全統共通テスト模試・記述模試<br>いずれも受験
						</a></li>
						<li><a href="javascript:jump('B')">全統共通テスト模試のみ受験</a></li>
						<li><a href="javascript:jump('C')">全統記述模試のみ受験</a></li>
					</ul>
					<!-- /.contents-inner -->
				</div>
				<div class="contents-inner">
					<div class="text-inner user-guide">
						<h4 class="title03 cl-pc">＜ご利用の前に＞</h4>
						<p>
							※30分間何も操作しないと「タイムアウト」し、データが消去されます。その場合はお手数ですが、はじめからやり直してください。<br>※表示される入試科目等は変更される可能性があります。必ず大学発表の学生募集要項で確認してください。<br>※ボーダーラインは、前年度入試の結果と今年度の全統模試の志望動向を参考にして設定していますが、今後の模試の志望動向等により変更する可能性があります。
						</p>
					</div>
				</div>
				<!-- /.contents -->
			</div>

			<!-- /.clear .container -->
		</div>
		<!-- コンテンツここまで -->

		<%@ include file="/jsp/include/footer.jsp"%>

	</form>
</body>
</html>

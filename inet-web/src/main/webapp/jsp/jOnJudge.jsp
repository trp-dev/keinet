<%@ page contentType="text/html;charset=MS932"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="kn" uri="/WEB-INF/taglib.tld"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=yes">
<title>判定実行中 模試判定システム</title>
<link rel="stylesheet" href="./shared_lib/style/normalize.css">
<link rel="stylesheet" href="./shared_lib/style/common.css">
<link rel="stylesheet" href="./shared_lib/style/sp.css" media="(max-width:640px)">
<link rel="stylesheet" href="./shared_lib/style/jonjudge.css">
<link rel="stylesheet" href="./shared_lib/style/jonjudgesp.css" media="(max-width:640px)">
<!--[if lt IE 9]>
    <script src="./js/html5shiv-printshiv.js" type="text/javascript"></script>
    <link rel="stylesheet" href="./shared_lib/style/ie8.css">
    <![endif]-->
<script type="text/javascript" src="./js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="./js/footerFixed.js"></script>
<script type="text/javascript">
<!--

<%@ include file="/jsp/script/submit_menu.jsp" %>
	// Android2.3のみ拡大・縮小を固定する（フッタータブバー固定のため）
	if ((navigator.userAgent.indexOf("Android 2.3") != -1)) {
		document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,width=320">');
	}

	var TIME_IN_SEC = 3;

	function goNow() {
		setTimeout("submitForm()", 1000 * TIME_IN_SEC);
	}

	/**
	 * サブミッション
	 */
	function submitForm() {
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
//-->
</script>
</head>
<%-- <body onload="goNow()" bgcolor="#F4EDE2" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />"> --%>
<body onload="goNow()" oncontextmenu="return <c:out value="${initParam.contextMenuParam}" />">
	<form action="<c:url value="JOnJudgeServlet" />" method="POST" onSubmit="return false">
		<c:set var="submitTop" value="false" />
		<input type="hidden" name="forward" value="<c:out value="${param.forward}" />"> <input type="hidden" name="backward" value="<c:out value="${param.backward}" />">

		<%-- HEADER --%>
		<%@ include file="/jsp/include/header.jsp"%>
		<%-- /HEADER --%>

		<!-- コンテンツここから -->
		<div class="container">
			<div class="contents">
				<!-- 大見出し ここから -->
				<h3 class="title01">判定実行中</h3>
				<!-- 大見出し ここまで -->

				<div class="contents-inner cl-pc">
					<div class="onjudge">
						<h1>判定実行中です。</h1>
						<p>しばらくお待ちください。</p>
					</div>
					<!-- /.onjudge -->
				</div>
				<!-- /.contents-inner -->

				<div class="contents-inner">
					<div class="onjudge1 cl-sp">
						<h1>判定実行中です。</h1>
						<p>しばらくお待ちください。</p>
					</div>
					<!-- /.onjudge -->
				</div>
				<!-- /.contents-inner -->
			</div>
			<!-- /.contents -->

		</div>
		<!-- /.container -->
		<!-- コンテンツここまで -->

		<%-- FOOTER --%>
		<%@ include file="/jsp/include/footer.jsp"%>
		<%-- /FOOTER --%>

	</form>
</body>
</html>

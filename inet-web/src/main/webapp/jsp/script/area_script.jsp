
	// �s���{���f�[�^
	var data = new Array();
	<c:forEach var="block" items="${ PrefBean.blockCodeList }">
		data["<c:out value="${block}" />"] = new Array(
		<c:forEach var="pref" items="${ PrefBean.blockMap[block] }" varStatus="status">
			<c:if test="${ status.index > 0 }">,</c:if>
			"<c:out value="${pref}" />"
		</c:forEach>
		);
	</c:forEach>

	function checkAll() {
		checkElemtns("block", true);
		checkElemtns("blockSP", true);
		checkElemtns("pref", true);
		checkElemtns("prefSP", true);
		accordiontext();
	}

	function removeCheckAll() {
		checkElemtns("block", false);
		checkElemtns("blockSP", false);
		checkElemtns("pref", false);
		checkElemtns("prefSP", false);
		accordiontext();
	}

	function checkElemtns(name, value) {
		var form = document.forms[0];
		var e = form.elements[name];
		for (var i=0; i<e.length; i++) {
			e[i].checked = value;
			e[i].className = "";
		}
	}

	function checkBlockALL(obj) {
		if (obj.checked) {
			checkAll();
		} else {
			removeCheckAll();
		}
	}
	
	function checkBlock(obj) {
		obj.className = "";
		var form = document.forms[0];
		var el;
		if (obj.name.indexOf('SP') != -1) {
			el = form.elements["block"];
		} else {
			el = form.elements["blockSP"];
		}
		
		for (var i=0; i<el.length; i++) {
			if (obj.value == el[i].value) el[i].checked = obj.checked;
		}
		
		var block = data[obj.value];
		var block1 = form.elements["block"];
		var elements = form.elements["pref"];
		var elementsSP = form.elements["prefSP"];
		for (var i=0; i<elements.length; i++) {
			for (var j=0; j<block.length; j++) {
				if (block[j] == elements[i].value) {
					elements[i].checked = obj.checked;
					elementsSP[i].checked = obj.checked;
				}
			}
		}

		checkZenchiku();
		accordiontext();
	}
	
	function checkPref(obj) {
		var form = document.forms[0];
		var el;
		if (obj.name.indexOf('SP') != -1) {
			el = form.elements["pref"];
		} else {
			el = form.elements["prefSP"];
		}
		
		for (var i=0; i<el.length; i++) {
			if (obj.value == el[i].value) el[i].checked = obj.checked;
		}
		

		var e1 = form.elements["block"];
		var e2 = form.elements["pref"];
		var e3 = form.elements["blockSP"];
		var block = "";
		for (var i=0; i<e1.length; i++) {
			var pref = data[e1[i].value];
			for (var j=0; j<pref.length; j++) {
				if (pref[j] == obj.value) {
					block = e1[i].value;
					break;
				}
			}
		}
		var flag = true;
		var checkedflg = false;
		var noncheckedflg = false;
		
		for (var i=0; i<e2.length; i++) {
			var pref = data[block];
			for (var j=0; j<pref.length; j++) {
				if (pref[j] == e2[i].value) {
					if (!e2[i].checked) { 
						flag = false;
						noncheckedflg = true;
					} else {
						checkedflg = true;
					}
				}
			}
		}
		for (var i=0; i<e1.length; i++) {
			if (e1[i].value == block) {
				e1[i].checked = flag;
				e3[i].checked = flag;
				if (checkedflg && noncheckedflg) {
					e3[i].className = "innerCheck";
					e3[i].checked = true;
				} else {
					e3[i].className = "";
				}
			}
		}

		checkZenchiku();
		accordiontext();
	}

	function checkZenchiku() {
		var form = document.forms[0];
		var e1 = form.elements["pref"];
		var e2 = form.elements["block"];
		var e3 = form.elements["blockAll"];
		var e4 = form.elements["blockAllSP"];
		var e1flg = true;
		var e2flg = true;
		for (var i=0; i<e1.length; i++) {
			
			if (!e1[i].checked) {
				e1flg = false;
			}
		}

		for (var j=0; j<e2.length; j++) {
			if (!e2[j].checked) {
				e2flg = false;
			}
		}

		if (e1flg && e2flg) {
			e3.checked = true;
			e4.checked = true;
		} else {
			e3.checked = false;
			e4.checked = false;
		}

	}


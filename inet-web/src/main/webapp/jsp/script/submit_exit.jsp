
<%--「終了」ボタン --%>
function submitExit() {
	if (confirm("<kn:message id="j025a" />")) {
		document.forms[0].action = "<c:url value="Logout" />";
		document.forms[0].forward.value = "logout";
		document.forms[0].backward.value = "<c:out value="${param.forward}" default="j001" />";
		document.forms[0].target = "_self";
		document.forms[0].submit();
	}
}

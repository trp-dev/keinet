document.write('<style type="text/css"><!--');


if (navigator.platform.indexOf('Mac') < 0) {

//windows_IE --------------------------------
if (navigator.appName.indexOf('Microsoft') >= 0) {
//point換算=15pt
document.write('.text24{ font-family:"ＭＳ Ｐゴシック"; font-size:24px; line-height:120%;}');
document.write('.text24-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:24px; line-height:100%;}');
document.write('.text24-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:24px; line-height:140%;}');
//point換算=13.5pt
document.write('.text18{ font-family:"ＭＳ Ｐゴシック"; font-size:18px; line-height:120%;}');
document.write('.text18-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:18px; line-height:100%;}');
document.write('.text18-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:18px; line-height:140%;}');
//point換算=12pt
document.write('.text16{ font-family:"ＭＳ Ｐゴシック"; font-size:16px; line-height:120%;}');
document.write('.text16-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:16px; line-height:100%;}');
document.write('.text16-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:16px; line-height:140%;}');
//point換算=10.5pt
document.write('.text14{ font-family:"ＭＳ Ｐゴシック"; font-size:14px; line-height:120%;}');
document.write('.text14-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:14px; line-height:100%;}');
document.write('.text14-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:14px; line-height:140%;}');
//point換算=9pt
document.write('.text12{ font-family:"ＭＳ Ｐゴシック"; font-size:12px; line-height:120%;}');
document.write('.text12-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:12px; line-height:100%;}');
document.write('.text12-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:12px; line-height:140%;}');
//point換算=8pt
document.write('.text10{ font-family:"ＭＳ Ｐゴシック"; font-size:11px; line-height:120%;}');
document.write('.text10-lh{ font-family:"ＭＳ Ｐゴシック"; font-size:11px; line-height:100%;}');
document.write('.text10-hh{ font-family:"ＭＳ Ｐゴシック"; font-size:11px; line-height:140%;}');

} else {

//windows_Netscape --------------------------------
//point換算=15pt
document.write('.text24{ font-family:"MS PGothic"; font-size:24px; line-height:120%; }');
document.write('.text24-lh{ font-family:"MS PGothic"; font-size:24px; line-height:100%; }');
document.write('.text24-hh{ font-family:"MS PGothic"; font-size:24px; line-height:140%; }');
//point換算=14.5pt
document.write('.text18{ font-family:"MS PGothic"; font-size:19px; line-height:120%; }');
document.write('.text18-lh{ font-family:"MS PGothic"; font-size:19px; line-height:100%; }');
document.write('.text18-hh{ font-family:"MS PGothic"; font-size:19px; line-height:140%; }');
//point換算=13pt
document.write('.text16{ font-family:"MS PGothic"; font-size:17px; line-height:120%; }');
document.write('.text16-lh{ font-family:"MS PGothic"; font-size:17px; line-height:100%; }');
document.write('.text16-hh{ font-family:"MS PGothic"; font-size:17px; line-height:140%; }');
//point換算=11pt
document.write('.text14{ font-family:"MS PGothic"; font-size:15px; line-height:120%; }');
document.write('.text14-lh{ font-family:"MS PGothic"; font-size:15px; line-height:100%; }');
document.write('.text14-hh{ font-family:"MS PGothic"; font-size:15px; line-height:140%; }');
//point換算=9pt
document.write('.text12{ font-family:"MS PGothic"; font-size:12px; line-height:120%; }');
document.write('.text12-lh{ font-family:"MS PGothic"; font-size:12px; line-height:100%; }');
document.write('.text12-hh{ font-family:"MS PGothic"; font-size:12px; line-height:140%; }');
//point換算=8pt
document.write('.text10{ font-family:"MS PGothic"; font-size:11px; line-height:120%; }');
document.write('.text10-lh{ font-family:"MS PGothic"; font-size:11px; line-height:100%; }');
document.write('.text10-hh{ font-family:"MS PGothic"; font-size:11px; line-height:140%; }');
}
}

// Mac --------------------------------
if (navigator.platform.indexOf('Mac') >= 0) {
//このサイズはNetscapeで太くなります。
document.write('.text24{ font-family:"Osaka","sans-serif"; font-size:24px; line-height:28px;}');
document.write('.text24-lh{ font-family:"Osaka","sans-serif"; font-size:24px; line-height:24px;}');
document.write('.text24-hh{ font-family:"Osaka","sans-serif"; font-size:24px; line-height: 34px;}');

document.write('.text18{ font-family:"Osaka","sans-serif"; font-size:18px; line-height:22px;}');
document.write('.text18-lh{ font-family:"Osaka","sans-serif"; font-size:18px; line-height:18px;}');
document.write('.text18-hh{ font-family:"Osaka","sans-serif"; font-size: 18px; line-height:24px;}');

document.write('.text16{ font-family:"Osaka","sans-serif"; font-size:16px; line-height:19px;}');
document.write('.text16-lh{ font-family:"Osaka","sans-serif"; font-size:16px; line-height:16px;}');
document.write('.text16-hh{ font-family:"Osaka","sans-serif"; font-size: 16px; line-height:22px;}');

document.write('.text14{ font-family:"Osaka","sans-serif"; font-size:14px; line-height:17px;}');
document.write('.text14-lh{ font-family:"Osaka","sans-serif"; font-size:14px; line-height:14px;}');
document.write('.text14-hh{ font-family:"Osaka","sans-serif"; font-size: 14px; line-height:20px;}');

document.write('.text12{ font-family:"Osaka","sans-serif"; font-size:12px; line-height:14px;}');
document.write('.text12-lh{ font-family:"Osaka","sans-serif"; font-size:12px;  line-height:12px;}');
document.write('.text12-hh{ font-family:"Osaka","sans-serif"; font-size: 12px; line-height:16px;}');

document.write('.text10{ font-family:"Osaka","sans-serif"; font-size: 10px; line-height:12px;}');
document.write('.text10-lh{ font-family:"Osaka","sans-serif"; font-size: 10px; line-height:10px;}');
document.write('.text10-hh{ font-family:"Osaka","sans-serif"; font-size: 10px; line-height:14px;}');
}


// Link color --------------------------------
document.write("a:link{ text-decoration:underline; color:#2986B1;}");
document.write("a:active{ text-decoration:none}");


document.write('//--></style>');

SELECT DISTINCT
  UNIVCD
FROM
  UNIVMASTER_BASIC
WHERE
  EVENTYEAR = /*eventYear*/'2012'
  AND EXAMDIV = /*examDiv*/'07'
  /*IF univCd != null*/
  AND UNIVCD = /*univCd*/'1025'
  /*END*/

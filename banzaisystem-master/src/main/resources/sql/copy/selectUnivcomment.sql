SELECT
  *
FROM
  UNIVCOMMENT
WHERE
  EVENTYEAR =/*eventYear*/'2013'
  AND EXAMDIV = /*examDiv*/'01'
  /* 07区分以外はレコード0件にする */
  /*IF examDiv != "07"*/
  AND EVENTYEAR IS NULL
  /*END*/

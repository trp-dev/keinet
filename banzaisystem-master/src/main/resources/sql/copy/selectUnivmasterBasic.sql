SELECT
  T.UNIVCD
  , T.FACULTYCD
  , T.DEPTCD
  , T.UNIDIV
  , T.CHOICECD5
  , T.UNINAME_ABBR
  , T.FACULTYNAME_ABBR
  , T.DEPTNAME_ABBR
  , T.UNIVNAME_KANA
  , T.LDISTRICTCD
  , T.UNIGDIV
  , T.APPLILIMITDIV
  , T.PREFCD_EXAMSH
  , T.PREFCD_EXAMHO
  , T.ENTEXAMSTEMMACD_M
  , T.ENTEXAMSTEMMACD_S
  , T.BUNRICD
  , T.UNICOEDDIV
  , T.UNINIGHTDIV
  , T.UNIGNAME
  , T.ENTEXAMORNDIV
  , T.PUBREJECTCOMP
  , T.PUBREJECTEXSCORE
  , T.ENTEXAMRANK
  , T.ENTEXAMRANK_DEVI
  , T.BORDERINDIV
  , T.BORDERSCORE1
  , T.BORDERSCORE2
  , T.BORDERSCORE3
  , T.BORDERSCORERATE1
  , T.BORDERSCORERATE2
  , T.BORDERSCORERATE3
  , T.BORDERSCORENOEXAM1
  , T.BORDERSCORENOEXAM2
  , T.BORDERSCORENOEXAM3
  , T.BORDERSCORERATENOEXAM1
  , T.BORDERSCORERATENOEXAM2
  , T.BORDERSCORERATENOEXAM3
  , T.REJECTEXSCORE
  , T.REJECTSCORERATE
  , T.DOCKINGNEEDLDIV
  , T.REP_MSTEMMA
  , T.REP_SSTEMMA
  , T.REP_REGION
  , T.REP_BRANCHCD
  , T.REGISTREORDIV
  , T.SCHEDULESYS
  , T.SCHEDULESYSBRANCHCD
  , T.SCHEDULESYSNAME
  , T.LMAINDIV
  , T.ARTUNIDIV
  , NULL AS ENTEXAMAPPLIDEADLINE
  , NULL AS ENTEXAMINPLEDATE
  , TO_SEARCHSTR(T.UNIVNAME_KANA) AS UNIVNAME_KANA_NORMALIZED
  , '0' AS EXAMSCHEDULEDETAIL_FLAG
  /*IF examDiv == "07"*/
  , (
    SELECT
      CASE
        WHEN COUNT(*) > 0
        THEN '1'
        ELSE '0'
        END
    FROM
      UNIVCOMMENT C
    WHERE
      C.EVENTYEAR = T.EVENTYEAR
      AND C.EXAMDIV = T.EXAMDIV
      AND C.UNIVCD = T.UNIVCD
      AND C.FACULTYCD = T.FACULTYCD
      AND C.DEPTCD = T.DEPTCD
  ) AS UNIVCOMMENT_FLAG
  --ELSE , '0' AS UNIVCOMMENT_FLAG
  /*END*/
  , T.ENTEXAMFULLPNT_CENTER
  , T.TOTALALLOTPNT_CENTER
  , T.ENTEXAMFULLPNT_CENTER_NOEXAM
  , T.TOTALALLOTPNT_CENTER_NOEXAM
  , T.SETTINGPATTERN
  , T.APP_REQ_PATTERN_CD
  , T.ENGPT_BORDERUSEFLG
  , T.ENGPT_REPBRANCH
FROM
  UNIVMASTER_BASIC T
WHERE
  T.EVENTYEAR =/*eventYear*/'2013'
  AND T.EXAMDIV = /*examDiv*/'01'

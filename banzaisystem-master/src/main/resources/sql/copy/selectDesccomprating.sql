SELECT
  T.SUBCD
  , T.SHOQUESTION1
  , T.SHOQUESTION2
  , T.SHOQUESTION3
  , T.DESCCOMPRATING
FROM
  DESCCOMPRATING T
WHERE
  T.EVENTYEAR =/*eventYear*/'2013'
  AND T.EXAMCD = /*examDiv*/'01'

SELECT
  EXAMYEAR
  , EXAMCD
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , PATTERNCD
  , NUMBERS_A
  , NUMBERS_S
  , NUMBERS_G
  , AVGPNT_A
  , AVGPNT_S
  , AVGPNT_G
FROM
  UNIVDIST_CENTER
WHERE
  EXAMYEAR = /*eventYear*/'2013'
UNION ALL
SELECT
  EXAMYEAR
  , EXAMCD
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , PATTERNCD
  , NUMBERS_A
  , NUMBERS_S
  , NUMBERS_G
  , AVGPNT_A
  , AVGPNT_S
  , AVGPNT_G
FROM
  UNIVDIST_SECOND
WHERE
  EXAMYEAR = /*eventYear*/'2013'
UNION ALL
SELECT
  EXAMYEAR
  , EXAMCD
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , PATTERNCD
  , NUMBERS_A
  , NUMBERS_S
  , NUMBERS_G
  , AVGPNT_A
  , AVGPNT_S
  , AVGPNT_G
FROM
  UNIVDIST_NORMAL
WHERE
  EXAMYEAR = /*eventYear*/'2013'

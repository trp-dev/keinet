package jp.ac.kawai_juku.banzaisystem.batch.builder;

import java.util.List;
import java.util.Set;

import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 入試出願締切日ビルダです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AppliDeadLineBuilder {

    /** 入試出願締切日セット */
    private final Set<String> dateSet = CollectionsUtil.newHashSet();

    /**
     * コンストラクタです。
     *
     * @param list 入試日程詳細リスト
     */
    public AppliDeadLineBuilder(List<ScheduleDetailBean> list) {
        build(list);
    }

    /**
     * 入試出願締切日セットをビルドします。
     *
     * @param list 入試日程詳細リスト
     */
    private void build(List<ScheduleDetailBean> list) {
        for (ScheduleDetailBean bean : list) {
            String date = bean.getEntExamAppliDeadline();
            if (date != null && date.length() == 8) {
                dateSet.add(date);
            }
        }
    }

    /**
     * 入試出願締切日セットを返します。
     *
     * @return dateSet 入試出願締切日セット
     */
    public Set<String> getDateSet() {
        return dateSet;
    }

}

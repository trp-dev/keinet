package jp.ac.kawai_juku.banzaisystem.batch.service;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.builder.JStringBuilder;
import jp.ac.kawai_juku.banzaisystem.batch.dao.UnivJudgeMasterDao;

import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * PCバンザイシステム用大学判定用マスタ生成サービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivJudgeMasterService {

    /** Logger */
    private static final Logger logger = Logger.getLogger(UnivJudgeMasterService.class);

    /** DAO */
    @Resource(name = "univJudgeMasterDao")
    private UnivJudgeMasterDao dao;

    /**
     * 判定用大学マスタを生成します。
     */
    public void create() {
        dao.selectUniv(null, new IterationCallback<String, Object>() {
            @Override
            public Object iterate(String univCd, IterationContext context) {
                List<String[]> paramList = createJStringList(univCd);
                if (!paramList.isEmpty()) {
                    dao.insertUnivJudgement(paramList);
                }
                logger.info(String.format("判定用大学マスタをINSERTしました。[大学コード=%s]", univCd));
                return null;
            }
        });
    }

    /**
     * 指定した大学の判定用文字列を生成します。
     *
     * @param univCd 大学コード
     * @return 判定用文字列リスト
     */
    public List<String[]> createJStringList(String univCd) {

        List<BeanMap> basicList = dao.selectBasicList(univCd);
        List<BeanMap> choiceAllList = dao.selectChoiceSchoolList(univCd);
        List<BeanMap> courseAllList = dao.selectCourseList(univCd);
        List<BeanMap> subjectAllList = dao.selectSubjectList(univCd);
        List<BeanMap> groupAllList = dao.selectSelectGroupList(univCd);
        List<BeanMap> groupCourseAllList = dao.selectSelectGpCourseList(univCd);

        List<String[]> list = CollectionsUtil.newArrayList(basicList.size());
        for (BeanMap basic : basicList) {
            String univKey = basic.get("univkey").toString();
            List<BeanMap> choiceList = filterByUnivKey(choiceAllList, univKey);
            List<BeanMap> courseList = filterByUnivKey(courseAllList, univKey);
            List<BeanMap> subjectList = filterByUnivKey(subjectAllList, univKey);
            List<BeanMap> groupList = filterByUnivKey(groupAllList, univKey);
            List<BeanMap> groupCourseList = filterByUnivKey(groupCourseAllList, univKey);

            if (choiceList.isEmpty()) {
                logger.warn("大学マスタ模試用志望校が存在しません。" + univKey);
                continue;
            }

            String jString =
                    new JStringBuilder(basic, choiceList, courseList, subjectList, groupList, groupCourseList)
                            .getjString();
            if (logger.isDebugEnabled()) {
                logger.debug(univKey + "=" + jString);
            }

            list.add(new String[] { univKey, jString });
        }

        return list;
    }

    /**
     * データリストから指定した大学キーのデータのみを抽出します。
     *
     * @param list データリスト
     * @param univKey 大学キー
     * @return データリスト
     */
    private List<BeanMap> filterByUnivKey(List<BeanMap> list, String univKey) {
        List<BeanMap> container = CollectionsUtil.newArrayList();
        for (Iterator<BeanMap> ite = list.iterator(); ite.hasNext();) {
            BeanMap bean = ite.next();
            if (bean.get("univkey").toString().equals(univKey)) {
                container.add(bean);
                ite.remove();
            }
        }
        return container;
    }

}

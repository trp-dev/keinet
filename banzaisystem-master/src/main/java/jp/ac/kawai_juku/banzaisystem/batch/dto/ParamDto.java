package jp.ac.kawai_juku.banzaisystem.batch.dto;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

/**
 *
 * PCバンザイシステム用マスタ生成バッチの起動パラメータを保持するDTOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Component(instance = InstanceType.SINGLETON)
public class ParamDto {

    /** 行事年度 */
    private String eventYear;

    /** 模試区分 */
    private String examDiv;

    /**
     * 行事年度をセットします。
     *
     * @param eventYear 行事年度
     */
    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

    /**
     * 行事年度を返します。
     *
     * @return 行事年度
     */
    public String getEventYear() {
        return eventYear;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

}

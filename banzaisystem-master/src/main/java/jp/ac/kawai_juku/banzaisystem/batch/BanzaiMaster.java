package jp.ac.kawai_juku.banzaisystem.batch;

import static org.seasar.framework.container.SingletonS2Container.getComponent;
import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;
import jp.ac.kawai_juku.banzaisystem.batch.exception.BZBatchException;
import jp.ac.kawai_juku.banzaisystem.batch.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.batch.service.MasterCopyService;
import jp.ac.kawai_juku.banzaisystem.batch.service.UnivJudgeMasterService;
import jp.ac.kawai_juku.banzaisystem.batch.service.VacuumService;

import org.seasar.framework.container.factory.SingletonS2ContainerFactory;
import org.seasar.framework.log.Logger;

/**
 *
 * PCバンザイシステム用マスタ生成バッチです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BanzaiMaster {

    /** Logger */
    private static final Logger logger = Logger.getLogger(BanzaiMaster.class);

    /**
     * コンストラクタです。
     */
    private BanzaiMaster() {
    }

    /**
     * メインメソッドです。
     *
     * @param args 引数
     */
    public static void main(String[] args) {
        try {
            SingletonS2ContainerFactory.init();
            getComponent(InitializeService.class).initialize(args);
            getComponent(MasterCopyService.class).copy();
            getComponent(UnivJudgeMasterService.class).create();
            getComponent(VacuumService.class).vacuum();
            ParamDto dto = getComponent(ParamDto.class);
            logger.info(String.format("マスタを生成しました。[年度=%s][模試区分=%s]", dto.getEventYear(), dto.getExamDiv()));
        } catch (BZBatchException e) {
            logger.error(e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            logger.error("バッチ処理中にエラーが発生しました。", e);
            System.exit(1);
        }
    }

}

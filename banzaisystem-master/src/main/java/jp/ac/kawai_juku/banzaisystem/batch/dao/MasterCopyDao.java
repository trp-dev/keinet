package jp.ac.kawai_juku.banzaisystem.batch.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.BZMConstants;
import jp.ac.kawai_juku.banzaisystem.batch.builder.BasicDateBuilder;
import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;
import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.jdbc.SqlBatchUpdate;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * PCバンザイシステム用マスタ生成バッチのマスタコピーに関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MasterCopyDao extends BaseDao {

    /** JdbcManager（Kei-NaviDB側） */
    @Resource
    private JdbcManager jdbcManager;

    /** JdbcManager（マスタファイル側） */
    @Resource
    private JdbcManager sqliteJdbcManager;

    /** バッチパラメータDTO */
    @Resource
    private ParamDto paramDto;

    /**
     * 大学マスタ情報を登録します。
     *
     * @param eventYear 行事年度
     * @param examDiv 模試区分
     */
    public void insertUnivMaster(String eventYear, String examDiv) {
        Object[] params = { eventYear, examDiv, BZMConstants.VERSION };
        Class<?>[] paramClasses = new Class<?>[params.length];
        Arrays.fill(paramClasses, String.class);
        sqliteJdbcManager.updateBySql("INSERT INTO UNIVMASTER VALUES(?,?,?)", paramClasses).params(params).execute();
    }

    /**
     * マスタを取得します。
     *
     * @param sqlPath SQLファイルパス
     * @param callback 結果を処理するコールバッククラス
     * @return 未登録のデータリスト
     */
    public List<Object[]> selectMaster(String sqlPath, IterationCallback<BeanMap, List<Object[]>> callback) {
        return jdbcManager.selectBySqlFile(BeanMap.class, sqlPath, paramDto).iterate(callback);
    }

    /**
     * コピー先テーブルの列名リストを取得します。
     *
     * @param destTableName コピー先テーブル名
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectDestColumnNameList(String destTableName, IterationCallback<BeanMap, Object> callback) {
        sqliteJdbcManager.selectBySql(BeanMap.class, "PRAGMA TABLE_INFO('" + destTableName + "')").iterate(callback);
    }

    /**
     * マスタを登録します。
     *
     * @param insertSql INSERT文
     * @param list INSERTするデータリスト
     */
    public void insert(String insertSql, List<Object[]> list) {
        Class<?>[] paramClasses = new Class<?>[list.get(0).length];
        Arrays.fill(paramClasses, Object.class);
        SqlBatchUpdate batchUpdate = sqliteJdbcManager.updateBatchBySql(insertSql, paramClasses);
        for (Object[] row : list) {
            batchUpdate.params(row);
        }
        batchUpdate.execute();
    }

    /**
     * 大学コードリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectUnivCd(IterationCallback<String, Object> callback) {
        sqliteJdbcManager.selectBySql(String.class, "SELECT DISTINCT UNIVCD FROM UNIVMASTER_BASIC").iterate(callback);
    }

    /**
     * 入試日程詳細リストを取得します。
     *
     * @param year 年度
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @return 入試日程詳細リスト
     */
    public List<ScheduleDetailBean> selectExamScheduleDetail(String year, String examDiv, String univCd) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("examDiv", examDiv);
        param.put("univCd", univCd);
        return jdbcManager.selectBySqlFile(ScheduleDetailBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 入試出願締切日を登録します。
     *
     * @param key {@link UnivKeyBean}
     * @param dateSet 入試出願締切日セット
     */
    public void insertUnivAppliDeadLine(UnivKeyBean key, Set<String> dateSet) {

        Class<?>[] paramClasses = new Class<?>[4];
        Arrays.fill(paramClasses, String.class);

        SqlBatchUpdate batchUpdate =
                sqliteJdbcManager.updateBatchBySql("INSERT INTO UNIVAPPLIDEADLINE VALUES(?,?,?,?)", paramClasses);
        for (String date : dateSet) {
            Object[] params = new Object[4];
            params[0] = key.getUnivCd();
            params[1] = key.getFacultyCd();
            params[2] = key.getDeptCd();
            params[3] = date;
            batchUpdate.params(params);
        }
        batchUpdate.execute();
    }

    /**
     * 入試実施日を登録します。
     *
     * @param key {@link UnivKeyBean}
     * @param dateSet 入試実施日セット
     */
    public void insertUnivInpleDate(UnivKeyBean key, Set<?> dateSet) {

        Class<?>[] paramClasses = new Class<?>[4];
        Arrays.fill(paramClasses, String.class);

        SqlBatchUpdate batchUpdate =
                sqliteJdbcManager.updateBatchBySql("INSERT INTO UNIVINPLEDATE VALUES(?,?,?,?)", paramClasses);
        for (Object date : dateSet) {
            Object[] params = new Object[4];
            params[0] = key.getUnivCd();
            params[1] = key.getFacultyCd();
            params[2] = key.getDeptCd();
            params[3] = date;
            batchUpdate.params(params);
        }
        batchUpdate.execute();
    }

    /**
     * 大学マスタ基本情報の「入試出願締切日」「入試実施日」を更新します。
     *
     * @param key {@link UnivKeyBean}
     * @param builder {@link BasicDateBuilder}
     */
    public void updateBasicDate(UnivKeyBean key, BasicDateBuilder builder) {

        if (builder.getAppliDeadLine() == null && builder.getInpleDate() == null) {
            /* 両方共NULLなら更新しない */
            return;
        }

        Object[] params = { builder.getAppliDeadLine(), builder.getInpleDate(), key.getUnivCd(), key.getFacultyCd(),
                key.getDeptCd() };
        Class<?>[] paramClasses = new Class<?>[params.length];
        Arrays.fill(paramClasses, String.class);

        sqliteJdbcManager.updateBySql("UPDATE UNIVMASTER_BASIC " + "SET ENTEXAMAPPLIDEADLINE = ?, ENTEXAMINPLEDATE = ? "
                + "WHERE UNIVCD = ? AND FACULTYCD = ? AND DEPTCD = ?", paramClasses).params(params).execute();
    }

    /**
     * 大学マスタ基本情報の「入試日程詳細フラグ」を1に更新します。
     *
     * @param key {@link UnivKeyBean}
     */
    public void updateExamScheduleDetailFlag(UnivKeyBean key) {
        sqliteJdbcManager.updateBySqlFile(getSqlPath(), key).execute();
    }

}

package jp.ac.kawai_juku.banzaisystem.batch.builder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail.Center;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail.Second;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 判定用文字列ビルダです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JStringBuilder {

    /** ブロックの区切り文字 */
    private static final char BLOCK_SEP = '#';

    /** ブロックの区切り文字（全角） */
    private static final char BLOCK_SEP_ZEN = '＃';

    /** 値の区切り文字 */
    private static final char VALUE_SEP = ';';

    /** 値の区切り文字（全角） */
    private static final char VALUE_SEP_ZEN = '；';

    /** 選択グループ区切り文字 */
    protected static final String GROUP_SEP = ":";

    /** 教科コード配列 */
    private static final String[] COURSE_CD_ARRAY = { "1", "2", "3", "4", "5" };

    /** 選択グループ番号配列 */
    private static final String[] GROUP_NO_ARRAY = { "1", "2", "3", "4", "5" };

    /** センター科目コード配列 */
    private static final String[] CENTER_SUBCD_ARRAY = { Center.Subcd.WRITING, Center.Subcd.LISTENING, Center.Subcd.MATH1, Center.Subcd.MATH1A, Center.Subcd.MATH2, Center.Subcd.MATH2B, Center.Subcd.JPRESENT, Center.Subcd.JCLASSICS,
            Center.Subcd.JCHINESE, Center.Subcd.PHYSICS_BASIC, Center.Subcd.CHEMISTRY_BASIC, Center.Subcd.BIOLOGY_BASIC, Center.Subcd.EARTHSCIENCE_BASIC, Center.Subcd.PHYSICS, Center.Subcd.CHEMISTRY, Center.Subcd.BIOLOGY,
            Center.Subcd.EARTHSCIENCE, Center.Subcd.JHISTORYA, Center.Subcd.WHISTORYA, Center.Subcd.GEOGRAPHYA, Center.Subcd.JHISTORYB, Center.Subcd.WHISTORYB, Center.Subcd.GEOGRAPHYB, Center.Subcd.ETHICS, Center.Subcd.POLITICS,
            Center.Subcd.SOCIALSTUDY, Center.Subcd.ETHICALPOLITICS, Center.Subcd.ENNINTEI, Center.Subcd.JKIJUTSU };

    /** 二次数学科目コード配列 */
    private static final String[] SECOND_MATH_ARRAY = { Second.Subcd.MATH1, Second.Subcd.MATH2, Second.Subcd.MATH3 };

    /** 二次国語科目コード配列 */
    private static final String[] SECOND_JAPANESE_ARRAY = { Second.Subcd.JAPANESE1, Second.Subcd.JAPANESE2, Second.Subcd.JAPANESE3 };

    /** 二次上位科目範囲：理科 */
    private static final String SUBAREA_UPPER_SCI = "02";

    /** 二次下位科目範囲：理科 */
    private static final String SUBAREA_LOWER_SCI = "01";

    /** 二次上位科目範囲：社会 */
    private static final String SUBAREA_UPPER_SOC = "03";

    /** 二次科目定義 */
    private enum WS {

        /** 英語１ */
        ENGLISH1(Second.Subcd.ENGLISH1),

        /** 英語２ */
        ENGLISH2(Second.Subcd.ENGLISH2),

        /** 数学�T */
        MATH1(SECOND_MATH_ARRAY, "01"),

        /** 数学�TＡ */
        MATH1A(SECOND_MATH_ARRAY, "02"),

        /** 数学�UＡ */
        MATH2A(SECOND_MATH_ARRAY, "03"),

        /** 数学�UＢ */
        MATH2B(SECOND_MATH_ARRAY, "04"),

        /** 数学�V */
        MATH3(SECOND_MATH_ARRAY, "05"),

        /** 現代文 */
        JPS(SECOND_JAPANESE_ARRAY, Second.Japanese.RANGES_COMTEMPORARY),

        /** 現代文・古文 */
        JCL(SECOND_JAPANESE_ARRAY, Second.Japanese.RANGES_ANCIENT),

        /** 現代文・古文・漢文 */
        JCN(SECOND_JAPANESE_ARRAY, Second.Japanese.RANGES_CHINESE),

        /** 物理基礎 */
        PHYSICS_BASIC(Second.Subcd.PHYSICS, SUBAREA_LOWER_SCI),

        /** 化学基礎 */
        CHEMISTRY_BASIC(Second.Subcd.CHEMISTRY, SUBAREA_LOWER_SCI),

        /** 生物基礎 */
        BIOLOGY_BASIC(Second.Subcd.BIOLOGY, SUBAREA_LOWER_SCI),

        /** 地学基礎 */
        EARTHSCIECNE_BASIC(Second.Subcd.EARTHSCIECNE, SUBAREA_LOWER_SCI),

        /** 物理 */
        PHYSICS(Second.Subcd.PHYSICS, SUBAREA_UPPER_SCI),

        /** 化学 */
        CHEMISTRY(Second.Subcd.CHEMISTRY, SUBAREA_UPPER_SCI),

        /** 生物 */
        BIOLOGY(Second.Subcd.BIOLOGY, SUBAREA_UPPER_SCI),

        /** 地学 */
        EARTHSCIECNE(Second.Subcd.EARTHSCIECNE, SUBAREA_UPPER_SCI),

        /** 物理不明 */
        PHYSICS_UNKNOWN(Second.Subcd.PHYSICS),

        /** 化学不明 */
        CHEMISTRY_UNKNOWN(Second.Subcd.CHEMISTRY),

        /** 生物不明 */
        BIOLOGY_UNKNOWN(Second.Subcd.BIOLOGY),

        /** 地学不明 */
        EARTHSCIECNE_UNKNOWN(Second.Subcd.EARTHSCIECNE),

        /** 日本史Ａ */
        JHISTORY_A(Second.Subcd.JHISTORY),

        /** 世界史Ａ */
        WHISTORY_A(Second.Subcd.WHISTORY),

        /** 地理Ａ */
        GEOGRAPHY_A(Second.Subcd.GEOGRAPHY),

        /** 日本史Ｂ */
        JHISTORY_B(Second.Subcd.JHISTORY, SUBAREA_UPPER_SOC),

        /** 世界史Ｂ */
        WHISTORY_B(Second.Subcd.WHISTORY, SUBAREA_UPPER_SOC),

        /** 地理Ｂ */
        GEOGRAPHY_B(Second.Subcd.GEOGRAPHY, SUBAREA_UPPER_SOC),

        /** 倫理 */
        ETHIC(Second.Subcd.ETHIC),

        /** 政治経済 */
        POLITICS(Second.Subcd.POLITICS),

        /** 現代社会 */
        SOCIALSTUDY(Second.Subcd.SOCIALSTUDY),

        /** 小論文 */
        ESSAY(Second.Subcd.ESSAY),

        /** 総合問題 */
        SYNTHESIS(Second.Subcd.SYNTHESIS),

        /** 面接 */
        INTERVIEW(Second.Subcd.INTERVIEW),

        /** 実技 */
        TECHNIQUE(Second.Subcd.TECHNIQUE),

        /** 調査 */
        RESEARCH(Second.Subcd.RESEARCH),

        /** 簿記 */
        BOOKKEEPING(Second.Subcd.BOOKKEEPING),

        /** 情報 */
        INFORMATION(Second.Subcd.INFORMATION),

        /** その他 */
        ETCETERA(Second.Subcd.ETCETERA);

        /** 科目コード配列 */
        private final String[] subCdArray;

        /** 範囲区分配列 */
        private final String[] subAreaArray;

        /**
         * コンストラクタです。
         *
         * @param subCdArray 科目コード配列
         * @param subAreaArray 範囲区分配列
         */
        WS(String[] subCdArray, String... subAreaArray) {
            this.subCdArray = subCdArray;
            this.subAreaArray = subAreaArray;
        }

        /**
         * コンストラクタです。
         *
         * @param subCd 科目コード
         * @param subAreaArray 範囲区分配列
         */
        WS(String subCd, String... subAreaArray) {
            this(new String[] { subCd }, subAreaArray);
        }

    }

    /** 大学マスタ基本情報 */
    private final BeanMap basic;

    /** センター判定詳細情報 */
    private final List<Branch> centerBranchList;

    /** 二次判定詳細情報 */
    private final List<Branch> secondBranchList;

    /** 足切判定詳細情報 */
    private final List<Branch> rejectBranchList;

    /** 判定用文字列 */
    private final String jString;

    /**
     * コンストラクタです。
     *
     * @param basic 大学マスタ基本情報
     * @param choiceList 大学マスタ模試用志望校データリスト
     * @param courseList 大学マスタ模試用教科リスト
     * @param subjectList 大学マスタ模試用科目リスト
     * @param groupList 大学マスタ模試用選択グループリスト
     * @param groupCourseList 大学マスタ模試用選択グループ教科リスト
     */
    public JStringBuilder(BeanMap basic, List<BeanMap> choiceList, List<BeanMap> courseList, List<BeanMap> subjectList, List<BeanMap> groupList, List<BeanMap> groupCourseList) {
        this.basic = basic;
        this.centerBranchList = createExamList(choiceList, courseList, subjectList, groupList, groupCourseList, "1");
        this.secondBranchList = createExamList(choiceList, courseList, subjectList, groupList, groupCourseList, "2");
        this.rejectBranchList = createExamList(choiceList, courseList, subjectList, groupList, groupCourseList, "3");
        this.jString = build();
    }

    /**
     * 入試試験区分別データを生成します。
     *
     * @param choiceList 大学マスタ模試用志望校データリスト
     * @param courseList 大学マスタ模試用教科リスト
     * @param subjectList 大学マスタ模試用科目リスト
     * @param groupList 大学マスタ模試用選択グループリスト
     * @param groupCourseList 大学マスタ模試用選択グループ教科リスト
     * @param entExamDiv 入試試験区分
     * @return {@link Branch}
     */
    private List<Branch> createExamList(List<BeanMap> choiceList, List<BeanMap> courseList, List<BeanMap> subjectList, List<BeanMap> groupList, List<BeanMap> groupCourseList, String entExamDiv) {

        List<Branch> list = CollectionsUtil.newArrayList();
        for (BeanMap choice : filterByEntExamDiv(choiceList, entExamDiv)) {
            list.add(new Branch(choice, courseList, subjectList, groupList, groupCourseList));
        }
        return list;
    }

    /**
     * リストから指定した入試試験区分のデータを抽出します。
     *
     * @param list データリスト
     * @param entExamDiv 入試試験区分
     * @return 抽出したデータリスト
     */
    private List<BeanMap> filterByEntExamDiv(List<BeanMap> list, String entExamDiv) {

        List<BeanMap> container = CollectionsUtil.newArrayList();

        for (Iterator<BeanMap> ite = list.iterator(); ite.hasNext();) {
            BeanMap bean = ite.next();
            if (bean.get("entexamdiv").equals(entExamDiv)) {
                container.add(bean);
                ite.remove();
            }
        }

        return container;
    }

    /**
     * 判定用文字列をビルドします。
     *
     * @return 判定用文字列
     */
    private String build() {
        StringBuilder sb = new StringBuilder(1024);
        appendBasic(sb);
        appendCenterDetail(sb, centerBranchList);
        appendSecondDetail(sb);
        if (!ObjectUtils.toString(basic.get("rejectexscore")).equals("0") || !ObjectUtils.toString(basic.get("pubrejectexscore")).equals("0")) {
            appendCenterDetail(sb, rejectBranchList);
        }
        return sb.toString();
    }

    /**
     * 大学基本情報を文字列バッファに付加します。
     *
     * @param sb 文字列バッファ
     */
    private void appendBasic(StringBuilder sb) {
        sb.append(basic.get("univdivcd")).append(VALUE_SEP);
        sb.append(basic.get("schedulecd")).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("cenConcentrate"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("cenBorder"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("cenAttention"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("cenScorerate"))).append(VALUE_SEP);
        sb.append(basic.get("rankcd")).append(VALUE_SEP);
        sb.append(basic.get("dockingneedldiv")).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("pubrejectexscore"))).append(VALUE_SEP);
        sb.append(basic.get("borderindiv")).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("borderscorenoexam1"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("borderscorenoexam2"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("borderscorenoexam3"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("borderscoreratenoexam2"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("appReqPatternCd"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("engptBorderuseflg"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(basic.get("engptRepbranch")));
    }

    /**
     * センター判定詳細情報を文字列バッファに付加します。
     *
     * @param sb 文字列バッファ
     * @param list 枝番別データリスト
     */
    private void appendCenterDetail(StringBuilder sb, List<Branch> list) {
        for (Branch branch : list) {

            /* 共通 */
            appendCommonDetail(branch, sb);

            /* 科目ビット */
            for (String subCd : CENTER_SUBCD_ARRAY) {
                sb.append(findSubBit(branch, subCd)).append(VALUE_SEP);
            }

            /* 科目配点 */
            for (String subCd : CENTER_SUBCD_ARRAY) {
                sb.append(findSubPnt(branch, subCd)).append(VALUE_SEP);
            }

            /* 同時選択不可区分 */
            for (String subCd : CENTER_SUBCD_ARRAY) {
                sb.append(findConcurrentNg(branch, subCd)).append(VALUE_SEP);
            }

            /* イレギュラ情報 */
            appendIrregular(branch, sb);
        }
    }

    /**
     * 二次判定詳細情報を文字列バッファに付加します。
     *
     * @param sb 文字列バッファ
     */
    private void appendSecondDetail(StringBuilder sb) {
        for (Branch branch : secondBranchList) {

            /* 共通 */
            appendCommonDetail(branch, sb);

            /* 科目ビット */
            for (WS subject : WS.values()) {
                sb.append(findSubBit(findSbj(branch, subject))).append(VALUE_SEP);
            }

            /* 科目配点 */
            for (WS subject : WS.values()) {
                if (StringUtils.startsWithAny(subject.subCdArray[0], "4", "5", "6")) {
                    /* 理科・社会のみ設定する */
                    sb.append(findSubPnt(findSbj(branch, subject))).append(VALUE_SEP);
                } else {
                    sb.append(StringUtils.EMPTY).append(VALUE_SEP);
                }
            }

            /* 同時選択不可区分ビット */
            for (WS subject : WS.values()) {
                sb.append(findConcurrentNg(findSbj(branch, subject))).append(VALUE_SEP);
            }

            /* 範囲区分 */
            sb.append(findSubArea(findSbj(branch, WS.ENGLISH1))).append(VALUE_SEP);
            sb.append(findSubArea(findSbj(branch, WS.ENGLISH2))).append(VALUE_SEP);

            /* イレギュラ情報 */
            appendIrregular(branch, sb);
        }
    }

    /**
     * センター・二次共通の判定詳細情報を文字列バッファに付加します。
     *
     * @param branch 枝番別データ
     * @param sb 文字列バッファ
     */
    private void appendCommonDetail(Branch branch, StringBuilder sb) {

        sb.append(BLOCK_SEP);
        sb.append(branch.choice.get("entexamdiv")).append(VALUE_SEP);
        sb.append(branch.choice.get("branchcd")).append(VALUE_SEP);
        sb.append(branch.choice.get("prioritybranchcd")).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(branch.choice.get("samechoiceNgSc"))).append(VALUE_SEP);
        sb.append(findSubCount(branch)).append(VALUE_SEP);

        /* 総合配点 */
        sb.append(ObjectUtils.toString(branch.choice.get("totalallotpnt"))).append(VALUE_SEP);

        /* 総合配点＿小論文なし */
        sb.append(ObjectUtils.toString(branch.choice.get("totalallotpntNrep"))).append(VALUE_SEP);

        /* 入試満点値 */
        sb.append(ObjectUtils.toString(branch.choice.get("entexamfullpnt"))).append(VALUE_SEP);

        /* 入試満点値＿小論文なし */
        sb.append(ObjectUtils.toString(branch.choice.get("entexamfullpntNrep"))).append(VALUE_SEP);

        /* 入試必須科目数 */
        for (String courseCd : COURSE_CD_ARRAY) {
            sb.append(findNecesSubCount(branch, courseCd)).append(VALUE_SEP);
        }

        /* 入試必須配点 */
        for (String courseCd : COURSE_CD_ARRAY) {
            sb.append(findNecesAllotPnt(branch, courseCd)).append(VALUE_SEP);
        }

        /* 入試必須配点（その他） */
        sb.append(findNecesAllotPnt(branch, "9")).append(VALUE_SEP);

        /* 入試選択科目数 */
        for (String groupNo : GROUP_NO_ARRAY) {
            sb.append(findSelectGSubCount(branch, groupNo)).append(VALUE_SEP);
        }

        /* 入試選択配点 */
        for (String groupNo : GROUP_NO_ARRAY) {
            sb.append(findSelectGAllotPnt(branch, groupNo)).append(VALUE_SEP);
        }

        /* 選択グループ教科別選択科目数 */
        for (String courseCd : COURSE_CD_ARRAY) {
            for (String groupNo : GROUP_NO_ARRAY) {
                sb.append(findSgPerSubSubCount(branch, groupNo, courseCd)).append(VALUE_SEP);
            }
        }

        /* 同一科目選択区分 */
        for (String courseCd : COURSE_CD_ARRAY) {
            sb.append(findCenSecSubChoicdDiv(branch, courseCd)).append(VALUE_SEP);
        }

        /* 第一解答科目フラグ */
        sb.append("0").append(VALUE_SEP);
        sb.append("0").append(VALUE_SEP);
        sb.append("0").append(VALUE_SEP);
        sb.append(branch.choice.get("firstansScFlg")).append(VALUE_SEP);
        sb.append(branch.choice.get("firstansSoFlg")).append(VALUE_SEP);

        /* 英語認定試験 */
        sb.append(ObjectUtils.toString(branch.choice.get("centercvsscoreEngpt"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(branch.choice.get("addptdivEngpt"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(branch.choice.get("addptEngpt"))).append(VALUE_SEP);

        /* 国語記述式 */
        sb.append(ObjectUtils.toString(branch.choice.get("centercvsscoreKokugoDesc"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(branch.choice.get("addptdivKokugoDesc"))).append(VALUE_SEP);
        sb.append(ObjectUtils.toString(branch.choice.get("addptKokugoDesc"))).append(VALUE_SEP);

        /* 最善枝番ソート用得点換算利用区分 */
        sb.append(ObjectUtils.toString(branch.choice.get("bestbranSortCvsscoreUsediv"))).append(VALUE_SEP);
    }

    /**
     * 判定科目名を返します。
     *
     * @param branch 枝番別データ
     * @return 判定科目名
     */
    private String findSubCount(Branch branch) {

        String subCount = (String) branch.choice.get("subcount");

        if (subCount != null) {
            /* 区切り文字を全角に変換しておく */
            if (subCount.indexOf(BLOCK_SEP) > -1) {
                subCount = subCount.replace(BLOCK_SEP, BLOCK_SEP_ZEN);
            }
            if (subCount.indexOf(VALUE_SEP) > -1) {
                subCount = subCount.replace(VALUE_SEP, VALUE_SEP_ZEN);
            }
        }

        return subCount;
    }

    /**
     * 指定した条件に一致する列データを取得します。
     *
     * @param list データリスト
     * @param condName 条件列名
     * @param condValue 条件値
     * @param name 列名
     * @param defaultValue デフォルト値
     * @return データ
     */
    private String findByCondition(List<BeanMap> list, String condName, Object condValue, String name, String defaultValue) {
        for (BeanMap bean : list) {
            if (bean.get(condName).equals(condValue)) {
                Object value = bean.get(name);
                return value == null ? defaultValue : value.toString();
            }
        }
        return defaultValue;
    }

    /**
     * 指定した条件に一致する列データを取得します。
     *
     * @param list データリスト
     * @param condName 条件列名
     * @param condValue 条件値
     * @param name 列名
     * @param defaultValue デフォルト値
     * @return データ
     */
    private String findByConditionGroup(List<BeanMap> list, String condName, Object condValue, String name, String defaultValue) {
        StringBuffer result = new StringBuffer();
        for (BeanMap bean : list) {
            if (bean.get(condName).equals(condValue)) {
                Object value = bean.get(name);
                if (result.length() != 0) {
                    result.append(GROUP_SEP);
                }
                if (value != null) {
                    result.append(value.toString());
                }
            }
        }
        if (result.length() == 0) {
            result.append(defaultValue);
        }
        return result.toString();
    }

    /**
     * 指定した条件に一致する列データを取得します。
     *
     * @param list データリスト
     * @param condName1 条件列名1
     * @param condValue1 条件値1
     * @param condName2 条件列名2
     * @param condValue2 条件値2
     * @param name 列名
     * @param defaultValue デフォルト値
     * @return データ
     */
    private String findByCondition(List<BeanMap> list, String condName1, Object condValue1, String condName2, Object condValue2, String name, String defaultValue) {
        for (BeanMap bean : list) {
            if (bean.get(condName1).equals(condValue1) && bean.get(condName2).equals(condValue2)) {
                Object value = bean.get(name);
                return value == null ? defaultValue : value.toString();
            }
        }
        return defaultValue;
    }

    /**
     * 指定した教科コードの入試教科配点を取得します。
     *
     * @param branch 枝番別データ
     * @param courseCd 教科コード
     * @return 入試教科配点
     */
    private String findNecesAllotPnt(Branch branch, String courseCd) {
        return findByCondition(branch.courseList, "coursecd", courseCd, "necesallotpnt", StringUtils.EMPTY);
    }

    /**
     * 指定した教科コードの入試必須科目数を取得します。
     *
     * @param branch 枝番別データ
     * @param courseCd 教科コード
     * @return 入試必須科目数
     */
    private String findNecesSubCount(Branch branch, String courseCd) {
        return findByCondition(branch.courseList, "coursecd", courseCd, "necessubcount", "0");
    }

    /**
     * 指定した選択グループ番号の入試選択科目数を取得します。
     *
     * @param branch 枝番別データ
     * @param groupNo 選択グループ番号
     * @return 入試選択科目数
     */
    private String findSelectGSubCount(Branch branch, String groupNo) {
        return findByCondition(branch.groupList, "groupNo", groupNo, "selectgsubcount", "0");
    }

    /**
     * 指定した選択グループ番号の入試選択配点を取得します。
     *
     * @param branch 枝番別データ
     * @param groupNo 選択グループ番号
     * @return 入試選択配点
     */
    private String findSelectGAllotPnt(Branch branch, String groupNo) {
        return findByCondition(branch.groupList, "groupNo", groupNo, "selectgallotpnt", StringUtils.EMPTY);
    }

    /**
     * 指定した選択グループ番号＋教科コードの選択グループ教科別選択科目数を取得します。
     *
     * @param branch 枝番別データ
     * @param groupNo 選択グループ番号
     * @param courseCd 教科コード
     * @return 選択グループ教科別選択科目数
     */
    private String findSgPerSubSubCount(Branch branch, String groupNo, String courseCd) {
        return findByCondition(branch.groupCourseList, "groupNo", groupNo, "coursecd", courseCd, "sgpersubsubcount", "0");
    }

    /**
     * 指定した教科コードの同一科目選択区分を取得します。
     *
     * @param branch 枝番別データ
     * @param courseCd 教科コード
     * @return 同一科目選択区分
     */
    private String findCenSecSubChoicdDiv(Branch branch, String courseCd) {
        return findByCondition(branch.courseList, "coursecd", courseCd, "cenSecsubchoicediv", "0");
    }

    /**
     * 指定した科目コードの科目配点を取得します。
     *
     * @param branch 枝番別データ
     * @param subCd 科目コード
     * @return 科目配点
     */
    private String findSubPnt(Branch branch, String subCd) {
        return findByConditionGroup(branch.subjectList, "subcd", subCd, "subpnt", StringUtils.EMPTY);
    }

    /**
     * 指定した科目コードの同時選択不可区分を取得します。
     *
     * @param branch 枝番別データ
     * @param subCd 科目コード
     * @return 同時選択不可区分
     */
    private String findConcurrentNg(Branch branch, String subCd) {
        return findByConditionGroup(branch.subjectList, "subcd", subCd, "concurrentNg", StringUtils.EMPTY);
    }

    /**
     * 指定した科目コードの科目ビットを取得します。
     *
     * @param branch 枝番別データ
     * @param subCd 科目コード
     * @return 科目ビット（必須）
     */
    private String findSubBit(Branch branch, String subCd) {
        return findByConditionGroup(branch.subjectList, "subcd", subCd, "subbit", StringUtils.EMPTY);
    }

    /**
     * 指定した検索条件（科目コード＋範囲区分）に一致する科目データを取得します。
     *
     * @param branch 枝番別データ
     * @param subject 科目検索条件
     * @return 科目データ
     */
    private List<BeanMap> findSbj(Branch branch, WS subject) {
        List<BeanMap> result = new ArrayList<BeanMap>();
        for (BeanMap bean : branch.subjectList) {
            if (ArrayUtils.contains(subject.subCdArray, bean.get("subcd"))) {
                Object subArea = bean.get("subarea");
                if (ArrayUtils.isEmpty(subject.subAreaArray)) {
                    /*
                     * 理科は科目範囲が未設定（範囲不明）の場合、
                     * 上位科目と下位科目の科目範囲と一致しない場合に範囲不明と判断する。
                     * 地歴は科目範囲が未設定（下位科目）の場合、
                     * 上位科目の科目範囲と一致しない場合に下位科目と判断する。
                     */
                    if (subject.subCdArray[0].startsWith("4")) {
                        if (!SUBAREA_UPPER_SCI.equals(subArea) && !SUBAREA_LOWER_SCI.equals(subArea)) {
                            result.add(bean);
                        }
                    } else if (subject.subCdArray[0].startsWith("5")) {
                        if (!SUBAREA_UPPER_SOC.equals(subArea)) {
                            result.add(bean);
                        }
                    } else {
                        result.add(bean);
                    }
                } else if (ArrayUtils.contains(subject.subAreaArray, subArea)) {
                    result.add(bean);
                }
            }
        }
        return result;
    }

    /**
     * 科目データから範囲区分を取り出します。
     *
     * @param subject 科目データ
     * @return 範囲区分
     */
    private String findSubArea(List<BeanMap> subject) {
        Object value = subject == null || subject.size() == 0 ? null : subject.get(0).get("subarea");
        return value == null ? "00" : value.toString();
    }

    /**
     * 科目データから科目配点を取り出します。
     *
     * @param subjects 科目データ
     * @return 科目配点
     */
    private String findSubPnt(List<BeanMap> subjects) {
        return getSubjectValue(subjects, "subpnt");
    }

    /**
     * 科目データから科目ビットを取り出します。
     *
     * @param subjects 科目データ
     * @return 科目ビット
     */
    private String findSubBit(List<BeanMap> subjects) {
        return getSubjectValue(subjects, "subbit");
    }

    /**
     * 科目データから同時選択不可区分を取り出します。
     *
     * @param subjects 科目データ
     * @return 同時選択不可区分
     */
    private String findConcurrentNg(List<BeanMap> subjects) {
        return getSubjectValue(subjects, "concurrentNg");
    }

    /**
     * 指定した名称の列データを取得します。
     *
     * @param subjects データリスト
     * @param name 列名
     * @return データ
     */
    private String getSubjectValue(List<BeanMap> subjects, String name) {
        StringBuffer result = new StringBuffer();
        for (BeanMap bean : subjects) {
            Object value = bean.get(name);
            if (result.length() != 0) {
                result.append(GROUP_SEP);
            }
            if (value != null) {
                result.append(value.toString());
            }
        }
        return result.toString();
    }

    /**
     * イレギュラ情報を文字列バッファに付加します。
     *
     * @param branch 枝番別データ
     * @param sb 文字列バッファ
     */
    private void appendIrregular(Branch branch, StringBuilder sb) {
        if (!appendIrregular1(branch, sb)) {
            /* イレギュラ情報なし */
            sb.append("0");
        }
    }

    /**
     * イレギュラ情報�@を文字列バッファに付加します。
     *
     * @param branch 枝番別データ
     * @param sb 文字列バッファ
     * @return イレギュラ�@でなければfalse
     */
    private boolean appendIrregular1(Branch branch, StringBuilder sb) {

        /* イレギュラ配点の有無を判定する  */
        boolean found = false;
        for (int i = 0; i < 6; i++) {
            if (branch.choice.get("irralallotpnt" + (i + 1)) != null) {
                found = true;
                break;
            }
        }
        if (!found) {
            return false;
        }

        /* イレギュラ区分 */
        sb.append("1").append(VALUE_SEP);

        /* イレギュラ配点 */
        for (int i = 0; i < 6; i++) {
            String key = "irralallotpnt" + (i + 1);
            sb.append(ObjectUtils.toString(branch.choice.get(key))).append(VALUE_SEP);
        }

        /* 最大選択科目数 */
        for (int i = 0; i < 30; i++) {
            sb.append(branch.choice.get("maxselect" + (i + 1))).append(VALUE_SEP);
        }

        /* イレギュラ科目数 */
        for (int i = 0; i < 6; i++) {
            if (i > 0) {
                sb.append(VALUE_SEP);
            }
            sb.append(branch.choice.get("irrSub" + (i + 1)));
        }

        return true;
    }

    /** 枝番別データ */
    private final class Branch {

        /** 大学マスタ模試用志望校データ */
        private final BeanMap choice;

        /** 大学マスタ模試用教科リスト */
        private final List<BeanMap> courseList;

        /** 大学マスタ模試用科目リスト */
        private final List<BeanMap> subjectList;

        /** 大学マスタ模試用選択グループリスト */
        private final List<BeanMap> groupList;

        /** 大学マスタ模試用選択グループ教科リスト */
        private final List<BeanMap> groupCourseList;

        /**
         * コンストラクタです。
         *
         * @param choice 大学マスタ模試用志望校データ
         * @param courseList 大学マスタ模試用教科リスト
         * @param subjectList 大学マスタ模試用科目リスト
         * @param groupList 大学マスタ模試用選択グループリスト
         * @param groupCourseList 大学マスタ模試用選択グループ教科リスト
         */
        private Branch(BeanMap choice, List<BeanMap> courseList, List<BeanMap> subjectList, List<BeanMap> groupList, List<BeanMap> groupCourseList) {
            this.choice = choice;
            this.courseList = filterByBranchCd(courseList);
            this.subjectList = filterByBranchCd(subjectList);
            this.groupList = filterByBranchCd(groupList);
            this.groupCourseList = filterByBranchCd(groupCourseList);
        }

        /**
         * 模試用志望校データの枝番に一致するデータを抽出します。
         *
         * @param list データリスト
         * @return 抽出したデータリスト
         */
        private List<BeanMap> filterByBranchCd(List<BeanMap> list) {

            List<BeanMap> container = CollectionsUtil.newArrayList();

            Object entExamDiv = choice.get("entexamdiv");
            Object branchCd = choice.get("branchcd");

            for (Iterator<BeanMap> ite = list.iterator(); ite.hasNext();) {
                BeanMap bean = ite.next();
                if (bean.get("entexamdiv").equals(entExamDiv) && bean.get("branchcd").equals(branchCd)) {
                    container.add(bean);
                    ite.remove();
                }
            }

            return container;
        }
    }

    /**
     * 判定用文字列を返します。
     *
     * @return 判定用文字列
     */
    public String getjString() {
        return jString;
    }

}

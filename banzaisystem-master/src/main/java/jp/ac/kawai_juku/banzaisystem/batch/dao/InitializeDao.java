package jp.ac.kawai_juku.banzaisystem.batch.dao;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;

import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * PCバンザイシステム用マスタ生成バッチの初期処理に関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class InitializeDao extends BaseDao {

    /** JdbcManager */
    @Resource
    private JdbcManager jdbcManager;

    /** バッチパラメータDTO */
    @Resource
    private ParamDto paramDto;

    /**
     * 処理年度の大学マスタ基本情報のレコード数を返します。
     *
     * @return レコード数（0件の場合はNULL）
     */
    public Integer countBasicByEventYear() {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), paramDto.getEventYear()).getSingleResult();
    }

    /**
     * 処理模試区分の大学マスタ基本情報のレコード数を返します。
     *
     * @return レコード数（0件の場合はNULL）
     */
    public Integer countBasicByExamDiv() {
        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getSingleResult();
    }

}

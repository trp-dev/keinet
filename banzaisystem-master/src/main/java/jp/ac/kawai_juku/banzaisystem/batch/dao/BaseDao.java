package jp.ac.kawai_juku.banzaisystem.batch.dao;

/**
 *
 * PCバンザイシステム用マスタ生成バッチの基底DAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseDao {

    /**
     * 実行中のメソッド名からSQLファイルパスを生成して返します。<br>
     * 生成パス: sql/[メソッド名].sql
     *
     * @return SQLファイルパス
     */
    protected final String getSqlPath() {
        StackTraceElement element = new Throwable().getStackTrace()[1];
        String methodName = element.getMethodName();
        return "sql/" + methodName + ".sql";
    }

}

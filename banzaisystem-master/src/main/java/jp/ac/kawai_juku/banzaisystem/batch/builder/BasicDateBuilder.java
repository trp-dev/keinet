package jp.ac.kawai_juku.banzaisystem.batch.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;
import jp.co.fj.kawaijuku.judgement.builder.InpleDateBuilder;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学マスタ基本情報の「入試出願締切日」「入試実施日」ビルダです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BasicDateBuilder {

    /** 入試出願締切日 */
    private final String appliDeadLine;

    /** 入試実施日 */
    private final String inpleDate;

    /**
     * コンストラクタです。
     *
     * @param list 入試日程詳細リスト
     */
    public BasicDateBuilder(List<ScheduleDetailBean> list) {
        this.appliDeadLine = buildAppliDeadLine(list);
        this.inpleDate = buildInpleDate(list);
    }

    /**
     * 入試出願締切日をビルドします。
     *
     * @param list 入試日程詳細リスト
     * @return 入試出願締切日
     */
    private String buildAppliDeadLine(List<ScheduleDetailBean> list) {

        /* １期１次本学 */
        String date = findMinAppliDeadLine(list, "01", "1", "1");
        if (date != null) {
            return date;
        }

        /* １期１次地方 */
        return findMinAppliDeadLine(list, "01", "1", "2");
    }

    /**
     * 入試日程詳細リストから条件にマッチする最小の入試出願締切日を返します。
     *
     * @param list 入試日程詳細リスト
     * @param term 入試１期２期区分
     * @param order 入試１次２次区分
     * @param entDiv 入試本学地方区分
     * @return 入試出願締切日
     */
    private String findMinAppliDeadLine(List<ScheduleDetailBean> list, String term, String order, String entDiv) {
        List<String> dateList = CollectionsUtil.newArrayList(new AppliDeadLineBuilder(find(list, term, order, entDiv)).getDateSet());
        Collections.sort(dateList);
        return dateList.isEmpty() ? null : dateList.get(0);
    }

    /**
     * 条件にマッチする入試日程詳細のリストを返します。
     *
     * @param list 入試日程詳細リスト
     * @param term 入試１期２期区分
     * @param order 入試１次２次区分
     * @param entDiv 入試本学地方区分
     * @return 入試出願締切日
     */
    private List<ScheduleDetailBean> find(List<ScheduleDetailBean> list, String term, String order, String entDiv) {
        List<ScheduleDetailBean> container = CollectionsUtil.newArrayList();
        for (ScheduleDetailBean bean : list) {
            if (bean.getEntExamDiv12Term().equals(term) && bean.getEntExamDiv12Order().equals(order) && bean.getSchoolProventDiv().equals(entDiv)) {
                container.add(bean);
            }
        }
        return container;
    }

    /**
     * 入試実施日をビルドします。
     *
     * @param list 入試日程詳細リスト
     * @return 入試実施日
     */
    private String buildInpleDate(List<ScheduleDetailBean> list) {

        /* １期１次本学 */
        String date = findMinInpleDate(list, "01", "1", "1");
        if (date != null) {
            return date;
        }

        /* １期２次本学 */
        date = findMinInpleDate(list, "01", "2", "1");
        if (date != null) {
            return date;
        }

        /* １期２次地方 */
        date = findMinInpleDate(list, "01", "2", "2");
        if (date != null) {
            return date;
        }

        /* １期１次地方 */
        return findMinInpleDate(list, "01", "1", "2");
    }

    /**
     * 入試日程詳細リストから条件にマッチする最小の入試出願締切日を返します。
     *
     * @param list 入試日程詳細リスト
     * @param term 入試１期２期区分
     * @param order 入試１次２次区分
     * @param entDiv 入試本学地方区分
     * @return 入試出願締切日
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private String findMinInpleDate(List<ScheduleDetailBean> list, String term, String order, String entDiv) {
        List dateList = new ArrayList(new InpleDateBuilder(find(list, term, order, entDiv)).getDateSet());
        Collections.sort(dateList);
        return dateList.isEmpty() ? null : (String) dateList.get(0);
    }

    /**
     * 入試出願締切日を返します。
     *
     * @return 入試出願締切日
     */
    public String getAppliDeadLine() {
        return appliDeadLine;
    }

    /**
     * 入試実施日を返します。
     *
     * @return 入試実施日
     */
    public String getInpleDate() {
        return inpleDate;
    }

}

package jp.ac.kawai_juku.banzaisystem.batch.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.builder.AppliDeadLineBuilder;
import jp.ac.kawai_juku.banzaisystem.batch.builder.BasicDateBuilder;
import jp.ac.kawai_juku.banzaisystem.batch.dao.MasterCopyDao;
import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;
import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.builder.InpleDateBuilder;

import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.StringUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * PCバンザイシステム用マスタ生成バッチのマスタコピーサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MasterCopyService {

    /** Logger */
    private static final Logger logger = Logger.getLogger(MasterCopyService.class);

    /** 項目名変換定義 */
    private Map<String, String> changeName = new java.util.HashMap<String, String>() {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            put("RIKA1ANS_FLG", "RIKA_1ANS_FLG");
            put("RIKA1ANS_METHOD", "RIKA_1ANS_METHOD");
            put("SOCIETY1ANS_FLG", "SOCIETY_1ANS_FLG");
            put("SOCIETY1ANS_METHOD", "SOCIETY_1ANS_METHOD");
            put("D_CEN7ITEM_TYP_NAME", "D_CEN_7ITEM_TYP_NAME");
            put("D_SEC7ITEM_TYP_NAME", "D_SEC_7ITEM_TYP_NAME");
        }
    };

    /** コピー先テーブル定義 */
    public enum DestTable {

        /** 大学基本情報 */
        UNIVMASTER_BASIC,

        /** 情報誌用科目（基本） */
        JH01_KAMOKU1,

        /** 情報誌用科目（共通テスト） */
        JH01_KAMOKU2,

        /** 情報誌用科目（２次） */
        JH01_KAMOKU3,

        /** 情報誌用科目日程ファイル */
        UNIVINFO_NITTEI,

        /** 大学コード紐付きテーブル */
        UNIVCDHOOKUP,

        /** 難易ランクマスタ */
        RANK,

        /** 日程マスタ */
        SCHEDULE,

        /** 県マスタ */
        PREFECTURE,

        /** 地区マスタ */
        DISTRICT,

        /** 大学系統マスタ */
        UNIVSTEMMA,

        /** 系統マスタ */
        STEMMA,

        /** 大学区分マスタ */
        UNIVDIVISION,

        /** 模試マスタ */
        EXAMINATION,

        /** 模試科目マスタ */
        EXAMSUBJECT,

        /** 科目別成績（全国） */
        SUBRECORD_A,

        /** センター得点換算表 */
        CENTERCVSSCORE,

        /** 大学名称マスタ */
        UNIVNAME,

        /** 資格マスタ */
        CERTIFICATION,

        /** 大学資格情報 */
        UNIVMASTER_CERT,

        /** 志望大学学力分布 */
        UNIVDISTRECORD,

        /** 志願者内訳 */
        CANDIDATEDETAIL,

        /** 大学マスタ公表用志望校 */
        UNIVMASTER_CHOICESCHOOL_PUB,

        /** 大学マスタ公表用教科 */
        UNIVMASTER_COURSE_PUB,

        /** 大学マスタ公表用科目 */
        UNIVMASTER_SUBJECT_PUB,

        /** 大学マスタ公表用選択グループ */
        UNIVMASTER_SELECTGROUP_PUB,

        /** 大学マスタ公表用選択グループ教科 */
        UNIVMASTER_SELECTGPCOURSE_PUB,

        /** 入試日程詳細 */
        EXAMSCHEDULEDETAIL,

        /** 注釈文マスタ */
        UNIVCOMMENT,

        /** CEFR情報 */
        /*CEFR_INFO,*/

        /** 認定試験情報 */
        /*ENGPTINFO,*/

        /** 検定試験詳細情報 */
        /*ENGPTDETAILINFO,*/

        /** CEFR換算用スコア情報 */
        /*CEFRCVSSCOREINFO,*/

        /** 英語資格・検定試験_出願要件 */
        /*ENGPT_APPREQ,*/

        /** 英語資格・検定試験_得点換算 */
        /*ENGPT_SCORECONV,*/

        /** 記述名称マスタ */
        /* QUESTIONNAME, */

        /** 国語記述式設問別条件マスタ */
        /* DESCQUEPROVISO, */

        /** 国語記述式設問別評価マスタ */
        /* DESCQUERATING, */

        /** 国語記述式総合評価マスタ */
        /* DESCCOMPRATING, */

        /** 国語記述式総合評価配点マスタ */
        /* DESCCOMPRATINGPNT, */

        /** 国語記述式_得点換算 */
        /* KOKUGO_DESC_SCORECONV, */

        /** 情報誌用科目（科目名） */
        JH01_KAMOKU4;

        /**
         * コピー元データのSQLファイルパスを返します。
         *
         * @return コピー元データのSQLファイルパス
         */
        public String getSqlPath() {
            return "sql/copy/select" + StringUtil.camelize(toString()) + ".sql";
        }

    }

    /** DAO */
    @Resource
    private MasterCopyDao masterCopyDao;

    /** バッチパラメータDTO */
    @Resource
    private ParamDto paramDto;

    /**
     * マスタをコピーします。
     */
    public void copy() {

        /* 大学マスタ情報を登録する */
        insertUnivMaster();

        /* マスタをコピーする */
        for (DestTable destTable : DestTable.values()) {
            copy(destTable);
        }

        /* 入試日程詳細関連の情報を更新する */
        updateExamScheduleDetail();
    }

    /**
     * 大学マスタ情報を登録します。
     */
    public void insertUnivMaster() {
        masterCopyDao.insertUnivMaster(paramDto.getEventYear(), paramDto.getExamDiv());
        logger.info("大学マスタ情報を登録しました。");
    }

    /**
     * マスタをコピーします。
     *
     * @param destTable コピー先テーブル
     */
    private void copy(DestTable destTable) {

        Callback callback = new Callback(destTable);
        List<Object[]> list = masterCopyDao.selectMaster(destTable.getSqlPath(), callback);
        if (list != null && !list.isEmpty()) {
            callback.insert(list);
        }

        logger.info(String.format("マスタデータ[%s]を%d件INSERTしました。", destTable, callback.count));
    }

    /** マスタのSELECT結果を処理するコールバッククラス */
    private final class Callback implements IterationCallback<BeanMap, List<Object[]>> {

        /** コピー先テーブル */
        private final DestTable destTable;

        /** INSERT対象列リスト */
        private List<String> columnNameList;

        /** INSERT文 */
        private String insertSql;

        /** SELECT結果リスト */
        private List<Object[]> entityList = CollectionsUtil.newArrayList(1000);

        /** レコード数 */
        private long count;

        /**
         * コンストラクタです。
         *
         * @param destTable コピー先テーブル
         */
        private Callback(DestTable destTable) {
            this.destTable = destTable;
        }

        @Override
        public List<Object[]> iterate(BeanMap entity, IterationContext context) {

            /* INSERT対象列リストとINSERT文を初期化する */
            if (columnNameList == null) {
                columnNameList = createColumnNameList(entity);
                insertSql = createInsert(columnNameList);
            }

            Object[] row = new Object[columnNameList.size()];
            for (int index = 0; index < columnNameList.size(); index++) {
                String key = columnNameList.get(index);
                Object value = entity.get(key);
                if (value instanceof BigDecimal) {
                    /* SQLiteのドライバはBigDecimalをサポートしていないので型変換する */
                    BigDecimal bd = (BigDecimal) value;
                    int i = bd.intValue();
                    double d = bd.doubleValue();
                    if ((double) i == d) {
                        row[index] = Integer.valueOf(i);
                    } else {
                        row[index] = Double.valueOf(d);
                    }
                } else {
                    row[index] = value;
                }
            }

            /* 1000件単位でINSERTする */
            entityList.add(row);
            if (entityList.size() == 1000) {
                insert(entityList);
                entityList.clear();
            }

            return entityList;
        }

        /**
         * INSERTを実行します。
         *
         * @param list INSERTするデータリスト
         */
        private void insert(List<Object[]> list) {
            masterCopyDao.insert(insertSql, list);
            count += list.size();
        }

        /**
         * INSERT対象列リストを取得します。
         *
         * @param map SELECT結果マップ
         * @return 列名リスト
         */
        private List<String> createColumnNameList(final BeanMap map) {

            final List<String> list = CollectionsUtil.newArrayList();
            masterCopyDao.selectDestColumnNameList(destTable.toString(), new IterationCallback<BeanMap, Object>() {
                @Override
                public Object iterate(BeanMap entity, IterationContext context) {
                    String name = StringUtil.decapitalize(StringUtil.camelize((String) entity.get("name")));
                    if (map.containsKey(name)) {
                        /* コピー元に存在する列のみとする */
                        list.add(name);
                    }
                    return null;
                }
            });

            return list;
        }

        /**
         * INSERT文を生成します。
         *
         * @param columnNameList 列名リスト
         * @return INSERT文
         */
        private String createInsert(List<String> columnNameList) {

            String itemName = "";
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO ");
            sb.append(destTable);
            sb.append("(");

            StringBuilder columns = new StringBuilder();
            StringBuilder values = new StringBuilder();
            for (String name : columnNameList) {
                if (columns.length() > 0) {
                    columns.append(",");
                    values.append(",");
                }

                /* 変換対象項目の場合は項目名を変更する */
                itemName = StringUtil.decamelize(name);
                if (changeName.containsKey(itemName)) {
                    columns.append(changeName.get(itemName));
                } else {
                    columns.append(itemName);
                }
                values.append("?");
            }

            sb.append(columns);
            sb.append(") VALUES (");
            sb.append(values);
            sb.append(")");

            return sb.toString();
        }

    }

    /**
     * 入試日程詳細関連の情報を更新します。
     */
    private void updateExamScheduleDetail() {
        masterCopyDao.selectUnivCd(new ExamScheduleDetailCallback());
    }

    /** 大学コード単位で入試日程詳細関連の処理を行うコールバッククラス */
    private class ExamScheduleDetailCallback implements IterationCallback<String, Object> {

        @Override
        public Object iterate(String univCd, IterationContext context) {

            /* 学科単位で処理する */
            Map<UnivKeyBean, List<ScheduleDetailBean>> map = findExamScheduleDetailMap(univCd);
            for (Entry<UnivKeyBean, List<ScheduleDetailBean>> entry : map.entrySet()) {
                /* 入試出願締切日を登録する */
                masterCopyDao.insertUnivAppliDeadLine(entry.getKey(), new AppliDeadLineBuilder(entry.getValue()).getDateSet());
                /* 入試実施日を登録する */
                Set<?> dateSet = new InpleDateBuilder(entry.getValue()).getDateSet();
                masterCopyDao.insertUnivInpleDate(entry.getKey(), dateSet);
                /* 大学マスタ基本情報の「入試出願締切日」「入試実施日」を更新する */
                masterCopyDao.updateBasicDate(entry.getKey(), new BasicDateBuilder(entry.getValue()));
                /* 複数日程ある場合は、大学マスタ基本情報の「入試日程詳細フラグ」を1に更新する */
                /* 2019/08/26 処理削除
                if (entry.getValue().size() > 1 || dateSet.size() > 1) {
                    masterCopyDao.updateExamScheduleDetailFlag(entry.getKey());
                }
                */
            }

            logger.info(String.format("入試日程詳細を処理しました。[大学コード=%s]", univCd));

            return null;
        }

        /**
         * 入試日程詳細マップを取得します。
         *
         * @param univCd 大学コード
         * @return 入試日程詳細マップ
         */
        private Map<UnivKeyBean, List<ScheduleDetailBean>> findExamScheduleDetailMap(String univCd) {

            List<ScheduleDetailBean> list = masterCopyDao.selectExamScheduleDetail(paramDto.getEventYear(), paramDto.getExamDiv(), univCd);

            Map<UnivKeyBean, List<ScheduleDetailBean>> map = CollectionsUtil.newHashMap();
            for (ScheduleDetailBean bean : list) {
                List<ScheduleDetailBean> container = map.get(bean);
                if (container == null) {
                    container = CollectionsUtil.newArrayList();
                    map.put(bean, container);
                }
                container.add(bean);
            }

            return map;
        }

    }

}

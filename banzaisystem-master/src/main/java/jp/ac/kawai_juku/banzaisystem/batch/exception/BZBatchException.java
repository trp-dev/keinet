package jp.ac.kawai_juku.banzaisystem.batch.exception;

/**
 *
 * バッチ処理中に致命的なエラーが発生した場合に投げる例外です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public final class BZBatchException extends RuntimeException {

    /**
     * コンストラクタです。
     *
     * @param message エラーメッセージ
     */
    public BZBatchException(String message) {
        super(message);
    }

}

package jp.ac.kawai_juku.banzaisystem.batch;

import java.io.InputStream;
import java.util.Properties;

import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.PropertiesUtil;

/**
 *
 * バンザイシステムマスタ生成バッチの定数定義クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZMConstants {

    /** バージョン */
    public static final String VERSION;

    static {
        InputStream in = null;
        try {
            in = BZMConstants.class.getResourceAsStream("/version.properties");
            Properties props = new Properties();
            PropertiesUtil.load(props, in);
            VERSION = props.getProperty("version");
        } finally {
            InputStreamUtil.close(in);
        }
    }

    /**
     * コンストラクタです。
     */
    private BZMConstants() {
    }

}

package jp.ac.kawai_juku.banzaisystem.batch.service;

import javax.annotation.Resource;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import jp.ac.kawai_juku.banzaisystem.batch.dao.VacuumDao;

import org.seasar.framework.log.Logger;

/**
 *
 * Vacuum実行サービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class VacuumService {

    /** Logger */
    private static final Logger logger = Logger.getLogger(VacuumService.class);

    /** DAO */
    @Resource
    private VacuumDao vacuumDao;

    /**
     * Vacuumを実行します。
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void vacuum() {
        logger.info("Analyzeを開始します。");
        vacuumDao.analyze();
        logger.info("Analyzeが完了しました。");
        logger.info("Vacuumを開始します。");
        vacuumDao.vacuum();
        logger.info("Vacuumが完了しました。");
    }

}

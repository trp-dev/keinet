package jp.ac.kawai_juku.banzaisystem.batch.dao;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;

import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.jdbc.SqlBatchUpdate;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * PCバンザイシステム用の大学判定用マスタ生成に関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivJudgeMasterDao extends BaseDao {

    /** JdbcManager（Kei-NaviDB側） */
    @Resource
    private JdbcManager jdbcManager;

    /** JdbcManager（マスタファイル側） */
    @Resource
    private JdbcManager sqliteJdbcManager;

    /** バッチパラメータDTO */
    @Resource
    private ParamDto paramDto;

    /**
     * 大学コードを取得します。
     *
     * @param univCd 大学コード
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectUniv(String univCd, IterationCallback<String, Object> callback) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).iterate(callback);
    }

    /**
     * 大学マスタ基本情報リストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ基本情報リスト
     */
    public List<BeanMap> selectBasicList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学マスタ模試用志望校リストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ模試用志望校リスト
     */
    public List<BeanMap> selectChoiceSchoolList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学マスタ模試用教科リストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ模試用教科リスト
     */
    public List<BeanMap> selectCourseList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学マスタ模試用科目リストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ模試用科目リスト
     */
    public List<BeanMap> selectSubjectList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学マスタ模試用選択グループリストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ模試用選択グループリスト
     */
    public List<BeanMap> selectSelectGroupList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学マスタ模試用選択グループ教科リストを取得します。
     *
     * @param univCd 大学コード
     * @return 大学マスタ模試用選択グループ教科リスト
     */
    public List<BeanMap> selectSelectGpCourseList(String univCd) {

        BeanMap param = new BeanMap();
        param.put("eventYear", paramDto.getEventYear());
        param.put("examDiv", paramDto.getExamDiv());
        param.put("univCd", univCd);

        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 判定用大学マスタを登録します。
     *
     * @param paramList パラメータリスト
     */
    public void insertUnivJudgement(List<String[]> paramList) {
        SqlBatchUpdate batchUpdate =
                sqliteJdbcManager.updateBatchBySql("INSERT INTO UNIVJUDGEMENT VALUES(?,?)", String.class, String.class);
        for (Object[] params : paramList) {
            batchUpdate.params(params);
        }
        batchUpdate.execute();
    }

}

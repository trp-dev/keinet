package jp.ac.kawai_juku.banzaisystem.batch.service;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.batch.dao.InitializeDao;
import jp.ac.kawai_juku.banzaisystem.batch.dto.ParamDto;
import jp.ac.kawai_juku.banzaisystem.batch.exception.BZBatchException;

import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.OutputStreamUtil;

/**
 *
 * PCバンザイシステム用マスタ生成バッチの初期処理を行うサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class InitializeService {

    /** Logger */
    private static final Logger logger = Logger.getLogger(InitializeService.class);

    /** DAO */
    @Resource
    private InitializeDao initializeDao;

    /**
     * 初期処理を行います。
     *
     * @param args 引数
     */
    public void initialize(String[] args) {
        checkArgs(args);
        initMasterFile();
    }

    /**
     * 引数をチェックします。
     *
     * @param args 引数
     */
    private void checkArgs(String[] args) {

        if (args.length < 1) {
            throw new BZBatchException("年度を指定してください。");
        }

        if (args.length < 2) {
            throw new BZBatchException("模試区分を指定してください。");
        }

        ParamDto dto = getComponent(ParamDto.class);
        dto.setEventYear(args[0]);
        dto.setExamDiv(args[1]);

        if (initializeDao.countBasicByEventYear() == null) {
            throw new BZBatchException(dto.getEventYear() + "年度の大学基本情報が存在しません。");
        }

        if (initializeDao.countBasicByExamDiv() == null) {
            throw new BZBatchException("模試区分" + dto.getExamDiv() + "の大学基本情報が存在しません。");
        }
    }

    /**
     * マスタファイルを初期化します。
     */
    private void initMasterFile() {
        InputStream in = null;
        OutputStream out = null;
        try {
            File file = new File("univ.db");
            in = getClass().getResourceAsStream("/template.db");
            out = new FileOutputStream(new File("univ.db"));
            InputStreamUtil.copy(in, out);
            logger.info("マスタファイルを初期化しました。" + file.getAbsolutePath());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        } finally {
            InputStreamUtil.close(in);
            OutputStreamUtil.close(out);
        }
    }

}

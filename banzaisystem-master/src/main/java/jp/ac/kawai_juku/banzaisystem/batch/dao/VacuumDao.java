package jp.ac.kawai_juku.banzaisystem.batch.dao;

import javax.annotation.Resource;

import org.seasar.extension.jdbc.JdbcManager;

/**
 *
 * Vacuum実行用のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class VacuumDao extends BaseDao {

    /** JdbcManager（マスタファイル側） */
    @Resource
    private JdbcManager sqliteJdbcManager;

    /**
     * Analyzeを実行します。
     */
    public void analyze() {
        sqliteJdbcManager.updateBySql("analyze").execute();
    }

    /**
     * Vacuumを実行します。
     */
    public void vacuum() {
        sqliteJdbcManager.updateBySql("vacuum").execute();
    }

}

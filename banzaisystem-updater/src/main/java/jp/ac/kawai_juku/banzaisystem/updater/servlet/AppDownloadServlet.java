package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import java.io.File;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.kawai_juku.banzaisystem.updater.BZUConstants;

/**
 *
 * 更新モジュール送信機能のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AppDownloadServlet extends DownloadServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 4327911703055711275L;

    /**
     * {@inheritDoc}
     */
    protected File findFile(HttpServletRequest req, HttpServletResponse res) {

        /* モジュール名をパラメータから取得する */
        String name = getParameter(req, res, "module");
        if (name == null) {
            return null;
        }

        /* jarファイルマップをコンテキストから取得する */
        Map jarFileMap = (Map) getServletContext().getAttribute(BZUConstants.KEY_JAR_FILE_MAP);

        /* jarファイルオブジェクトをマップから取得する */
        File file = (File) jarFileMap.get(name);
        if (file == null) {
            return null;
        }

        /* jarファイルオブジェクトをチェックする */
        if (!checkFile(res, file)) {
            return null;
        }

        return file;
    }

}

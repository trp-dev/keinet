package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import java.io.File;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.kawai_juku.banzaisystem.updater.BZUConstants;

/**
 *
 * 大学マスタリストXML生成機能のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DbVersionServlet extends BaseServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -187853500925775901L;

    /** 大学マスタファイル名のパターン */
    private static final Pattern DBFILENAME_PATTERN = Pattern.compile("[0-9]{8}\\.zip", Pattern.CASE_INSENSITIVE);

    /**
     * {@inheritDoc}
     */
    protected String execure(HttpServletRequest req, HttpServletResponse res) {

        /* 大学マスタファイルディレクトリを取得する */
        File path = (File) getServletContext().getAttribute(BZUConstants.KEY_DBFILEPATH);

        /* filesタグを生成する */
        StringBuffer sb = new StringBuffer(1024);
        sb.append("<files>").append(BZUConstants.CRLF);
        File[] files = path.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isFile() && file.canRead() && DBFILENAME_PATTERN.matcher(file.getName()).matches()) {
                    sb.append("<file size=\"").append(file.length()).append("\">");
                    sb.append(file.getName());
                    sb.append("</file>");
                    sb.append(BZUConstants.CRLF);
                }
            }
        }
        sb.append("</files>");

        /* filesタグをrequestに設定する */
        req.setAttribute("files", sb.toString());

        return "dbVersion.jsp";
    }

}

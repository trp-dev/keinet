package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seasar.framework.log.Logger;

/**
 *
 * バンザイシステム更新データ配信アプリの基底サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseServlet extends HttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -5879391609034324089L;

    /** Logger */
    private static final Logger logger = Logger.getLogger(BaseServlet.class);

    /**
     * {@inheritDoc}
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        /* リクエストを処理する */
        String jspName = execure(req, res);
        if (jspName != null) {
            /* JSPへ転送する */
            getServletContext().getRequestDispatcher("/WEB-INF/jsp/" + jspName).forward(req, res);
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doPost(req, res);
    }

    /**
     * リクエストを処理します。
     *
     * @param req {@link HttpServletRequest}
     * @param res {@link HttpServletResponse}
     * @return JSPファイル名
     */
    protected abstract String execure(HttpServletRequest req, HttpServletResponse res);

    /**
     * パラメータを返します。<br>
     * パラメータが存在しない場合は、応答コードに400(Bad Request)を設定します。
     *
     * @param req {@link HttpServletRequest}
     * @param res {@link HttpServletResponse}
     * @param name パラメータ名
     * @return パラメータ
     */
    protected final String getParameter(HttpServletRequest req, HttpServletResponse res, String name) {
        String param = req.getParameter(name);
        if (param == null) {
            res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            logger.error("パラメータ[" + name + "]がありません。");
        }
        return param;
    }

}

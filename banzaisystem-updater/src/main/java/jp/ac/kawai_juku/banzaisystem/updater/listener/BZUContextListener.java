package jp.ac.kawai_juku.banzaisystem.updater.listener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import jp.ac.kawai_juku.banzaisystem.updater.BZUConstants;

import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.FileUtil;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.OutputStreamUtil;

/**
 *
 * バンザイシステム更新データ配信アプリの {@link ServletContextListener} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZUContextListener implements ServletContextListener {

    /** Logger */
    private static final Logger logger = Logger.getLogger(BZUContextListener.class);

    /**
     * {@inheritDoc}
     */
    public void contextInitialized(ServletContextEvent event) {

        ServletContext context = event.getServletContext();

        /* アプリケーションバージョンをコンテキストに設定する */
        context.setAttribute(BZUConstants.KEY_VERSION, findVersion());

        /* jarファイルマップを生成する */
        Map jarFileMap = createJarFileMap(context);

        /* jarファイルマップをコンテキストに設定する */
        context.setAttribute(BZUConstants.KEY_JAR_FILE_MAP, jarFileMap);

        /* <modules>タグをコンテキストに設定する */
        context.setAttribute(BZUConstants.KEY_MODULES, createModules(jarFileMap));

        /* 大学マスタファイルディレクトリをコンテキストに設定する */
        context.setAttribute(BZUConstants.KEY_DBFILEPATH, findDbPath(context));
    }

    /**
     * アプリケーションバージョンを返します。
     *
     * @return アプリケーションバージョン
     */
    private String findVersion() {
        String version = BZUConstants.VERSION;
        logger.info("バンザイシステムバージョン=" + version);
        return version;
    }

    /**
     * jarファイルマップを生成します。
     *
     * @param context {@link ServletContext}
     * @return jarファイルマップ
     */
    private Map createJarFileMap(ServletContext context) {

        Map map = new HashMap();

        Set pathes = context.getResourcePaths("/WEB-INF/lib/");

        if (pathes == null) {
            /* libフォルダが存在しない場合(ローカル開発環境) */
            File[] files = new File("D:\\keinet_project\\data\\banzaisystem\\lib").listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File file = files[i];
                    if (file.isFile() && file.canRead() && file.getName().endsWith(".jar")) {
                        map.put(file.getName(), file);
                    }
                }
            }
        } else {
            /* libフォルダが存在する場合 */
            for (Iterator ite = pathes.iterator(); ite.hasNext();) {
                String path = (String) ite.next();
                if (path.endsWith(".jar")) {
                    /* warファイルに入っているので、一時ファイルに書き出す */
                    File file = null;
                    InputStream in = null;
                    OutputStream out = null;
                    try {
                        file = File.createTempFile("BZU", null);
                        file.deleteOnExit();
                        in = context.getResourceAsStream(path);
                        out = new FileOutputStream(file);
                        InputStreamUtil.copy(in, out);
                    } catch (IOException e) {
                        throw new IORuntimeException(e);
                    } finally {
                        try {
                            InputStreamUtil.close(in);
                        } finally {
                            OutputStreamUtil.close(out);
                        }
                    }
                    map.put(new File(path).getName(), file);
                }
            }
        }

        return map;
    }

    /**
     * &lt;modules&gt;タグを生成します。
     *
     * @param jarFileMap jarファイルマップ
     * @return &lt;modules&gt;タグ
     */
    private String createModules(Map jarFileMap) {

        StringBuffer sb = new StringBuffer(1024);
        sb.append("<modules>").append(BZUConstants.CRLF);

        for (Iterator ite = jarFileMap.entrySet().iterator(); ite.hasNext();) {
            Entry entry = (Entry) ite.next();
            String name = (String) entry.getKey();
            File file = (File) entry.getValue();
            sb.append("<module size=\"").append(file.length()).append("\">");
            sb.append(name);
            sb.append("</module>");
            sb.append(BZUConstants.CRLF);
        }

        sb.append("</modules>");

        return sb.toString();
    }

    /**
     * 大学マスタファイルディレクトリのパスを返します。
     *
     * @param context {@link ServletContext}
     * @return 大学マスタファイルディレクトリのパス
     */
    private File findDbPath(ServletContext context) {

        String param = context.getInitParameter("dbFilePath");
        if (param == null) {
            throw new RuntimeException("コンテキストパラメータ[dbFilePath]が未設定です。");
        }

        File path = new File(param);
        if (!path.isDirectory()) {
            throw new RuntimeException("大学マスタファイルディレクトリが存在しません。" + path);
        } else {
            logger.info("大学マスタファイルディレクトリ=" + FileUtil.getCanonicalPath(path));
        }

        return path;
    }

    /**
     * {@inheritDoc}
     */
    public void contextDestroyed(ServletContextEvent event) {

        ServletContext context = event.getServletContext();

        /* jar一時ファイルを削除する */
        if (context.getResourcePaths("/WEB-INF/lib/") != null) {
            Map jarFileMap = (Map) context.getAttribute(BZUConstants.KEY_JAR_FILE_MAP);
            for (Iterator ite = jarFileMap.values().iterator(); ite.hasNext();) {
                File file = (File) ite.next();
                file.delete();
            }
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * アプリケーションバージョン情報XML生成機能のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AppVersionServlet extends BaseServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -2395144451286280284L;

    /**
     * {@inheritDoc}
     */
    protected String execure(HttpServletRequest req, HttpServletResponse res) {
        return "appVersion.jsp";
    }

}

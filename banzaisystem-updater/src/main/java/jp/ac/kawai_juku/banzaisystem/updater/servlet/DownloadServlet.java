package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.InputStreamUtil;

/**
 *
 * ファイル送信機能の基底サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class DownloadServlet extends BaseServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -8944451679602856251L;

    /** Logger */
    private static final Logger logger = Logger.getLogger(AppDownloadServlet.class);

    /**
     * {@inheritDoc}
     */
    protected String execure(HttpServletRequest req, HttpServletResponse res) {

        File file = findFile(req, res);
        if (file == null) {
            return null;
        }

        res.setContentType("application/octet-stream");
        res.setContentLength((int) file.length());

        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(file);
            out = res.getOutputStream();
            InputStreamUtil.copy(in, out);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        } finally {
            InputStreamUtil.close(in);
        }

        return null;
    }

    /**
     * クライアントに送信するファイルを返します。
     *
     * @param req {@link HttpServletRequest}
     * @param res {@link HttpServletResponse}
     * @return クライアントに送信するファイル
     */
    protected abstract File findFile(HttpServletRequest req, HttpServletResponse res);

    /**
     * ダウンロード可能なファイルであるかをチェックします。
     *
     * @param res {@link HttpServletResponse}
     * @param file ファイル
     * @return ダウンロード可能なファイルならtrue
     */
    protected boolean checkFile(HttpServletResponse res, File file) {
        if (!file.canRead()) {
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            logger.error("ファイルが読み込めません。" + file);
            return false;
        }
        return true;
    }

}

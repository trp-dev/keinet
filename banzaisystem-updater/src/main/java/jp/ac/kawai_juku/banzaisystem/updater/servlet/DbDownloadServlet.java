package jp.ac.kawai_juku.banzaisystem.updater.servlet;

import java.io.File;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.ac.kawai_juku.banzaisystem.updater.BZUConstants;

/**
 *
 * 大学マスタファイル送信機能のサーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DbDownloadServlet extends DownloadServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 7422161805719172149L;

    /** 模試年度のパターン */
    private static final Pattern EXAMYEAR_PATTERN = Pattern.compile("[0-9]{4}");

    /** 模試区分のパターン */
    private static final Pattern EXAMDIV_PATTERN = Pattern.compile("[0-9]{2}");

    /**
     * {@inheritDoc}
     */
    protected File findFile(HttpServletRequest req, HttpServletResponse res) {

        /* 模試年度をパラメータから取得する */
        String examYear = getParameter(req, res, "examYear");
        if (examYear == null || !EXAMYEAR_PATTERN.matcher(examYear).matches()) {
            return null;
        }

        /* 模試区分をパラメータから取得する */
        String examDiv = getParameter(req, res, "examDiv");
        if (examDiv == null || !EXAMDIV_PATTERN.matcher(examDiv).matches()) {
            return null;
        }

        /* 大学マスタファイルディレクトリのパスをコンテキストから取得する */
        File parent = (File) getServletContext().getAttribute(BZUConstants.KEY_DBFILEPATH);

        /* 大学マスタファイルオブジェクトを生成する */
        File file = new File(parent, examYear + examDiv + ".zip");

        /* 大学マスタファイルオブジェクトをチェックする */
        if (!checkFile(res, file)) {
            return null;
        }

        return file;
    }

}

package jp.ac.kawai_juku.banzaisystem.updater;

import java.io.InputStream;
import java.util.Properties;

import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.PropertiesUtil;

/**
 *
 * バンザイシステム更新データ配信アプリの定数定義クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZUConstants {

    /** 改行コード */
    public static final String CRLF = "\r\n";

    /** コンテキストキー：バージョン */
    public static final String KEY_VERSION = "version";

    /** コンテキストキー：jarファイルマップ */
    public static final String KEY_JAR_FILE_MAP = "jarFileMap";

    /** コンテキストキー：&lt;modules&gt;タグ */
    public static final String KEY_MODULES = "modules";

    /** コンテキストキー：大学マスタファイルディレクトリのパス */
    public static final String KEY_DBFILEPATH = "dbFilePath";

    /** バージョン */
    public static final String VERSION;

    static {
        InputStream in = null;
        try {
            in = BZUConstants.class.getResourceAsStream("/updater.properties");
            Properties props = new Properties();
            PropertiesUtil.load(props, in);
            VERSION = props.getProperty("version");
        } finally {
            InputStreamUtil.close(in);
        }
    }

    /**
     * コンストラクタです。
     */
    private BZUConstants() {
    }

}

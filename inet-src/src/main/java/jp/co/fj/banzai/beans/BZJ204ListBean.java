package jp.co.fj.banzai.beans;

import java.util.List;

import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.inet.beans.J204ListBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;

/**
 *
 * インターネットバンザイ用のJ204ListBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZJ204ListBean extends J204ListBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -4502200581690797587L;

    /**
     * コンストラクタです。
     *
     * @param list 判定結果リスト
     */
    public BZJ204ListBean(List judgedList) {
        super(judgedList);
    }

    /**
     * {@inheritDoc}
     */
    protected DetailPageBean createDetailPageBean(Judgement judgement) {
        return new BZDetailPageBean(judgement);
    }

    /**
     * {@inheritDoc}
     */
    protected J204DetailBean createJ204DetailBean(DetailPageBean detail) {
        return new BZJ204DetailBean((BZDetailPageBean) detail);
    }

}

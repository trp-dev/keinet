package jp.co.fj.banzai.beans;

import jp.co.fj.inet.beans.JDetailPageBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.factory.UnivComment;
import jp.fujitsu.keinet.mst.file.model.UnivCircumstance;
import jp.fujitsu.keinet.mst.file.model.UnivDataFile;

/**
 *
 * インターネットバンザイ用のDetailPageBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZDetailPageBean extends JDetailPageBean {

    /** serialVersionUID */
    private static final long serialVersionUID = 626547020860007047L;

    /** 第1段階選抜 */
    private String pickedLine;

    /** 出願予定者平均点 */
    private String average;

    /** 出願予定者数 */
    private String candidates;

    /**
     * コンストラクタです。
     *
     * @param bean {@link Judgement}
     */
    BZDetailPageBean(Judgement bean) {
        super(bean);
    }

    /**
     * {@inheritDoc}
     */
    public void execute() throws Exception {

        super.execute();

        /* 値の加工を伴うものは、ここで加工を済ませておく */

        UnivDataFile univ = (UnivDataFile) super.univ;

        pickedLine = univ.getPickedLine();
        average = "";
        candidates = "";

        if (univ.getCircumstance() != null) {
            UnivCircumstance uc = univ.getCircumstance();
            if (uc.average == Math.floor(uc.average)) {
                average = Integer.toString((int) uc.average);
            } else {
                average = Double.toString(uc.average);
            }
            candidates = Integer.toString(uc.total);
        }
    }

    /**
     * 第1段階選抜を返します。
     *
     * @return 第1段階選抜
     */
    public String getPickedLine() {
        return pickedLine;
    }

    /**
     * 前年度倍率を返します。
     *
     * @return 前年度倍率
     */
    public String getPreMagnification() {
        return ((UnivDataFile) univ).getPreMagnification();
    }

    /**
     * 出願予定者平均点を返します。
     *
     * @return 出願予定者平均点
     */
    public String getAverage() {
        return average;
    }

    /**
     * 出願予定者数を返します。
     *
     * @return 出願予定者数
     */
    public String getCandidates() {
        return candidates;
    }

    /**
     * 注釈文マスタの種別フラグを返します。
     *
     * @return 注釈文マスタの種別フラグ
     */
    public String getTypeFlg() {
        UnivComment comment = univ.getUnivComment();
        return comment == null ? null : comment.getTypeFlg();
    }

    /**
     * 注釈文マスタのコメントを返します。
     *
     * @return 注釈文マスタのコメント
     */
    public String getCommentStatement() {
        UnivComment comment = univ.getUnivComment();
        return comment == null ? null : comment.getCommentStatement();
    }

}

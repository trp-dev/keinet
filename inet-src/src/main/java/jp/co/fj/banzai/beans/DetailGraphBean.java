package jp.co.fj.banzai.beans;

import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.fujitsu.keinet.mst.file.model.UnivDataFile;

import com.fjh.beans.DefaultBean;

/**
 *
 * 詳細グラフ情報を作成するためのBeanです。<br>
 * ※旧インターネットバンザイシステムの {@link jp.co.fjh.banzai.beans.DetailGraphBean} から移植
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DetailGraphBean extends DefaultBean {

    /** serialVersionUID */
    private static final long serialVersionUID = 4743928491636485894L;

    /** 詳細情報 */
    private final DetailPageBean pageBean;

    /** 得点帯軸の数 */
    private int x_Axis;

    /** 起点となる点数 */
    private int startingPoint;

    /** ボーダー得点 */
    private int borderPoint;

    /** A判定得点 */
    private int aPoint;

    /** B判定得点 */
    private int bPoint;

    /** C判定得点 */
    private int cPoint;

    /** D判定得点 */
    private int dPoint;

    /** E判定得点*/
    private int ePoint;

    /** 得点帯別人数 */
    private int[] details = new int[27];

    /** グラフ長計算用得点帯別人数 */
    private int[] p_details = new int[27];

    /** あなたの位置フラグ */
    private int[] judge = new int[27];

    /** A判定得点帯フラグ */
    private int[] judge_a = new int[27];

    /** B判定得点帯フラグ */
    private int[] judge_b = new int[27];

    /** C判定得点帯フラグ */
    private int[] judge_c = new int[27];

    /** D判定得点帯フラグ */
    private int[] judge_d = new int[27];

    /** E判定得点帯フラグ */
    private int[] judge_e = new int[27];

    /** ボーダー得点帯フラグ */
    private int[] judge_border = new int[27];

    /** 得点帯の間隔 */
    private int interval;

    /** 最低得点表記調整用得点 */
    private int adjust;

    /** 人数軸ラベル計算用割合 */
    private double parcent;

    /**
     * コンストラクタです。
     *
     * @param pageBean {@link DetailPageBean}
     */
    public DetailGraphBean(DetailPageBean pageBean) {
        this.pageBean = pageBean;
    }

    /**
     * {@inheritDoc}
     */
    public void execute() throws Exception {

        Judgement bean = pageBean.getBean();

        UnivDataFile univ = (UnivDataFile) bean.getUniv();

        int[] graphParam;
        if (univ.getCircumstance() == null) {
            return;
        } else {
            graphParam = univ.getCircumstance().getGraphParams();
        }

        /* ボーダー得点 */
        borderPoint = univ.getBorderPoint();

        /* A〜E判定得点 */
        aPoint = univ.getCenterAPoint();
        bPoint = univ.getCenterBPoint();
        cPoint = univ.getCenterCPoint();
        dPoint = univ.getCenterDPoint();
        ePoint = univ.getCenterEPoint();

        /* 得点帯軸の数 */
        x_Axis = graphParam[0];

        /* 最大人数 */
        int max = graphParam[1];

        /* 起点となる点数 */
        startingPoint = graphParam[2];

        /* 得点帯別人数 */
        for (int i = 0; i < 27; i++) {
            details[i] = graphParam[i + 3];
            p_details[i] = graphParam[i + 3];
        }

        /* 得点帯の間隔を設定する */
        setIntervalG(pageBean.getDetail().getCFullPoint());

        /* 人数軸ラベル計算用割合 */
        parcent = selectParcent(max);

        /* あなたの位置フラグ */
        judge = search(bean.getCScore());

        /* ボーダー得点帯フラグ */
        judge_border = search(borderPoint);

        /* A〜E判定得点帯フラグ */
        judge_a = search(aPoint);
        judge_b = search(bPoint);
        judge_c = search(cPoint);
        judge_d = search(dPoint);
        judge_e = search(ePoint);
    }

    /**
     * 人数軸ラベル計算用割合を算出する。
     *
     * @param max 最大人数
     * @return 人数軸ラベル計算用割合
     */
    private double selectParcent(int max) {

        double a_parcent = 1;

        if (max <= 10)
            a_parcent = 0.5;
        else if (max > 10 && max <= 20)
            a_parcent = 1;
        else if (max > 20 && max <= 30)
            a_parcent = 1.5;
        else if (max > 30 && max <= 40)
            a_parcent = 2;
        else if (max > 40 && max <= 50)
            a_parcent = 2.5;
        else if (max > 50 && max <= 100)
            a_parcent = 5;
        else if (max > 100 && max <= 150)
            a_parcent = 7.5;
        else if (max > 150 && max <= 200)
            a_parcent = 10;
        else if (max > 200 && max <= 250)
            a_parcent = 12.5;
        else if (max > 250 && max <= 300)
            a_parcent = 15;
        else if (max > 300) {
            a_parcent = 17.5;
            for (int i = 0; i < 27; i++) {
                if (details[i] > 300)
                    p_details[i] = 350;
            }
        }

        return a_parcent;
    }

    /**
     * 得点帯の間隔を設定します。
     *
     * @param fullPoint センタ満点値
     */
    private void setIntervalG(int fullPoint) {
        if (fullPoint > 50 && fullPoint <= 100) {
            interval = 2;
            adjust = 1;
        } else if (fullPoint > 100 && fullPoint <= 500) {
            interval = 5;
            adjust = 4;
        } else if (fullPoint > 500) {
            interval = 10;
            adjust = 9;
        } else {
            interval = 1;
            adjust = 0;
        }
    }

    /**
     * 得点が含まれる区分を示すフラグを返します。
     *
     * @param point センタ得点
     * @return 得点が含まれる区分を示すフラグ
     */
    private int[] search(int point) {

        int[] flag = new int[27];

        if (point < 0) {
            return flag;
        }

        for (int i = 0; i < x_Axis; i++) {
            double left = startingPoint + (i - 1) * interval;
            double right = startingPoint + i * interval;
            if (i == 0) {
                if (point < left || (point >= left && point < right)) {
                    flag[i] = 1;
                }
            } else if (i == x_Axis - 1) {
                if (point >= right || (point >= left && point < right)) {
                    flag[i] = 1;
                }
            } else {
                if (point >= left && point < right) {
                    flag[i] = 1;
                }
            }
        }

        return flag;
    }

    /**
     * 得点帯軸の数を返します。
     *
     * @return 得点帯軸の数
     */
    public int getX_Axis() {
        return this.x_Axis;
    }

    /**
     * 起点となる点数を返します。
     *
     * @return 起点となる点数
     */
    public int getStartingPoint() {
        return this.startingPoint;
    }

    /**
     * ボーダー得点を返します。
     *
     * @return ボーダー得点
     */
    public int getBorderPoint() {
        return this.borderPoint;
    }

    /**
     * A判定得点を返します。
     *
     * @return A判定得点
     */
    public int getaPoint() {
        return aPoint;
    }

    /**
     * B判定得点を返します。
     *
     * @return B判定得点
     */
    public int getbPoint() {
        return bPoint;
    }

    /**
     * C判定得点を返します。
     *
     * @return C判定得点
     */
    public int getcPoint() {
        return cPoint;
    }

    /**
     * D判定得点を返します。
     *
     * @return D判定得点
     */
    public int getdPoint() {
        return dPoint;
    }

    /**
     * E判定得点を返します。
     *
     * @return E判定得点
     */
    public int getePoint() {
        return ePoint;
    }

    /**
     * 得点帯別人数を返します。
     *
     * @return 得点帯別人数
     */
    public int[] getDetails() {
        return this.details;
    }

    /**
     * グラフ長計算用得点帯別人数を返します。
     *
     * @return グラフ長計算用得点帯別人数
     */
    public int[] getP_details() {
        return this.p_details;
    }

    /**
     * あなたの位置フラグを返します。
     *
     * @return あなたの位置フラグ
     */
    public int[] getJudge() {
        return this.judge;
    }

    /**
     * A判定得点帯フラグを返します。
     *
     * @return A判定得点帯フラグ
     */
    public int[] getJudge_a() {
        return judge_a;
    }

    /**
     * B判定得点帯フラグを返します。
     *
     * @return B判定得点帯フラグ
     */
    public int[] getJudge_b() {
        return judge_b;
    }

    /**
     * C判定得点帯フラグを返します。
     *
     * @return C判定得点帯フラグ
     */
    public int[] getJudge_c() {
        return judge_c;
    }

    /**
     * D判定得点帯フラグを返します。
     *
     * @return D判定得点帯フラグ
     */
    public int[] getJudge_d() {
        return judge_d;
    }

    /**
     * E判定得点帯フラグを返します。
     *
     * @return E判定得点帯フラグ
     */
    public int[] getJudge_e() {
        return judge_e;
    }

    /**
     * ボーダー得点帯フラグを返します。
     *
     * @return ボーダー得点帯フラグ
     */
    public int[] getJudge_border() {
        return this.judge_border;
    }

    /**
     * 得点帯の間隔を返します。
     *
     * @return 得点帯の間隔
     */
    public int getInterval() {
        return this.interval;
    }

    /**
     * 最低得点表記調整用得点を返します。
     *
     * @return 最低得点表記調整用得点
     */
    public int getAdjust() {
        return this.adjust;
    }

    /**
     * 人数軸ラベル計算用割合を返します。
     *
     * @return 人数軸ラベル計算用割合
     */
    public double getParcent() {
        return this.parcent;
    }

}

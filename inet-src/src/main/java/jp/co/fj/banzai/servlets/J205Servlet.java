package jp.co.fj.banzai.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.banzai.beans.DetailGraphBean;
import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.inet.beans.J204ListBean;
import jp.co.fj.inet.beans.JScoreBean;
import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.kawaijuku.judgement.Constants;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.JudgementExecutor;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

/**
 *
 * インターネットバンザイ用の判定結果詳細画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J205Servlet extends jp.co.fj.inet.servlets.J205Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -492755186197720824L;
    /** デバッグログ出力フラグ */
    private static final String DEBUG_SCORE = IPropsLoader.getInstance().getMessage("DEBUG_SCORE");
    private static final String DEBUG_RESULT = IPropsLoader.getInstance().getMessage("DEBUG_RESULT");
    private static final Logger logger = Logger.getLogger("jlog");

    /**
     * {@inheritDoc}
     */
    public void forward(HttpServletRequest request, HttpServletResponse response, String forwardPage) throws ServletException, IOException {

        if (forwardPage.endsWith("j205.jsp")) {
            /* 得点分布情報を設定する */
            try {
                DetailGraphBean graphBean = new DetailGraphBean((DetailPageBean) request.getAttribute("Judge"));
                graphBean.execute();
                request.setAttribute("graphPage", graphBean);
            } catch (Exception e) {
                throw new KeiNetException(e, "00J205010101", "0J205にてエラーが発生しました。", false);
            }
        }

        super.forward(request, response, forwardPage);
    }

    /**
     * 判定を実行します。
     *
     * @param session セッション情報
     * @param jlb 判定結果リストBean
     * @param uniqueKey 大学ユニークキー
     * @param exam 模試情報
     * @param f101 成績入力画面のフォームBean
     * @return {@link DetailPageBean}
     * @throws Exception 判定に失敗した時
     */
    protected DetailPageBean judge(HttpSession session, J204ListBean jlb, String uniqueKey, ExamBean exam, J101Form f101, boolean engPtInclude) throws Exception {
        Map<String, ExamSubjectBean> examSubject = (Map<String, ExamSubjectBean>) getServletContext().getAttribute(ExamSubjectBean.SESSION_KEY);
        if (jlb != null) {
            for (Iterator ite = jlb.getRecordSetAll().iterator(); ite.hasNext();) {
                J204DetailBean bean = (J204DetailBean) ite.next();
                if (bean.getUniqueKey().equals(uniqueKey)) {
                    JScoreBean score = new JScoreBean(f101, exam, examSubject);
                    score.execute();
                    /* デバッグログを出力する */
                    if (Constants.FLG_ON.equals(DEBUG_SCORE)) {
                        MDC.put("code", "-");
                        JudgedUtil.outputScoreDebugLog(logger, score);
                    }
                    JudgementExecutor exec = new JudgementExecutor(bean.getUniv(), score);
                    /* 判定結果で採用された枝番を指定 */
                    exec.addBranch(bean.getSelectedBranch());
                    exec.execute();
                    DetailPageBean detail = jlb.judge2Detail(exec.getJudgement());
                    /* 本人成績の情報を作成する */
                    detail.executeScore();
                    /* デバッグログを出力する */
                    if (Constants.FLG_ON.equals(DEBUG_RESULT)) {
                        JudgedUtil.outputResultDebugLog(logger, exec.getJudgement());
                    }
                    return detail;
                }
            }
        }
        return null;
    }

}

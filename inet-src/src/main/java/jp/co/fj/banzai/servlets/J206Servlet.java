package jp.co.fj.banzai.servlets;

/**
 *
 * インターネットバンザイ用の公表科目表示画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J206Servlet extends jp.co.fj.inet.servlets.J206Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -3853204533960527644L;

}

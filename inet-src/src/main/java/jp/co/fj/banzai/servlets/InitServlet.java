package jp.co.fj.banzai.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import jp.co.fj.inet.data.UnivMasterData;
import jp.co.fj.inet.servlets.BaseInitServlet;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;
import jp.co.fj.kawaijuku.judgement.factory.UnivFactory;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.log4j.xml.DOMConfigurator;

import com.fjh.db.DBManager;

/**
 *
 * インターネットバンザイ用のInitServletです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class InitServlet extends BaseInitServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -4662213321250606721L;

    /**
     * {@inheritDoc}
     */
    public void init() throws ServletException {

        /* ServletContext */
        ServletContext context = getServletContext();

        /* パス情報を初期化する */
        initPath(context);

        try {
            /* common.propertiesを読み込む */
            Properties props = loadCommonProperties();

            /* ログファイル用のディレクトリを作成しておく */
            String logRootPath = props.getProperty("KNLogRootPath");
            if (logRootPath != null) {
                File logDir = new File(logRootPath, "Log");
                createDir(logDir);
                createDir(new File(logDir, "ALog"));
                createDir(new File(logDir, "DLog"));
                createDir(new File(logDir, "ELog"));
            }

            /* common.propertiesをアプリケーションスコープに設定する */
            context.setAttribute(KNCommonProperty.class.getName(), props);

            context.setAttribute("system", "banzai");
        } catch (IOException e) {
            throw new ServletException(e);
        }

        super.init();
    }

    /**
     * パス情報を初期化します。
     *
     * @param context {@link ServletContext}
     */
    private void initPath(ServletContext context) throws ServletException {

        /* common.propertiesのパスをセット */
        KNPropertyCtrl.FilePath = context.getRealPath("/WEB-INF/classes/jp/co/fj/keinavi/resources");

        /* メッセージファイルがあるパスをセット */
        MessageLoader.getInstance().setPath(context.getRealPath("/WEB-INF"));

        /* Log4j設定 */
        DOMConfigurator.configure(context.getRealPath("/WEB-INF/config/log4j.xml"));
    }

    /**
     * common.propertiesを読み込みます。
     *
     * @return {@link Properties}
     */
    private Properties loadCommonProperties() throws IOException {
        Properties props = new Properties();
        InputStream in = null;
        try {
            props.load(new FileInputStream(new File(KNPropertyCtrl.FilePath, "common.properties")));
            return props;
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * ディレクトリを作成します。
     *
     * @param dir ディレクトリ
     */
    private void createDir(File dir) throws IOException {
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                throw new IOException("ディレクトリの作成に失敗しました。" + dir);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {

        try {
            DBManager.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.destroy();
    }

    /**
     * {@inheritDoc}
     */
    protected UnivMasterData loadUnivMaster() throws Exception {

        Properties props = (Properties) getServletContext().getAttribute(KNCommonProperty.class.getName());

        String[] keys = new String[] { "FILE_CHARSET", "FILEDIR", "FILE_UNIVMASTER_BASIC", "FILE_UNIVMASTER_CHOICESCHOOL", "FILE_UNIVMASTER_COURSE", "FILE_UNIVMASTER_SUBJECT", "FILE_UNIVMASTER_SELECTGROUP", "FILE_UNIVMASTER_SELECTGPCOURSE",
                "FILE_RANK", "FILE_UNIVSTEMMA", "FILE_CIRCUMSTANCE", "FILE_UNIV_PASS_FAIL_COUNT", "FILE_UNIVMASTER_INFO", "FILE_UNIVMASTER_OTHERNAME", "FILE_PREFECTURE", "FILE_KAMOKU1", "FILE_KAMOKU2", "FILE_KAMOKU3", "FILE_KAMOKU4",
                "FILE_UNIVCOMMENT", "FILE_DESCCOMPRATING", "FILE_EXAMSUBJECT" };

        Object[] initargs = new String[keys.length];

        for (int i = 0; i < keys.length; i++) {
            initargs[i] = props.getProperty(keys[i]);
        }

        Class factoryClass = Class.forName("jp.fujitsu.keinet.mst.file.factory.UnivFactoryFile");

        Class[] parameterTypes = new Class[keys.length];

        Arrays.fill(parameterTypes, String.class);

        Constructor constructor = factoryClass.getConstructor(parameterTypes);

        UnivFactory factory = (UnivFactory) constructor.newInstance(initargs);

        FactoryManager manager = new FactoryManager(factory);

        /*LoggerUtil.initMDC();*/
        /* 判定用大学マスタをロードする */
        System.out.println(">> 判定用大学マスタの展開を開始");
        manager.load();
        System.out.println(">> 判定用大学マスタの展開を終了");
        System.out.println(">> 展開数 : " + manager.getDatas().size());
        System.out.println(">> ローディング時間 " + manager.getLoadingTime());

        /* 情報誌用科目データをロードする */
        System.out.println(">> 情報誌用科目データの展開を開始");
        manager.loadUnivInfo();
        System.out.println(">> 情報誌用科目データの展開を終了");
        System.out.println(">> 展開数 : " + manager.getUnivInfoMap().size());
        System.out.println(">> ローディング時間 " + manager.getUnivInfoLoadingTime());
        /*LoggerUtil.destoryMDC();*/

        return new UnivMasterData(manager);
    }

}

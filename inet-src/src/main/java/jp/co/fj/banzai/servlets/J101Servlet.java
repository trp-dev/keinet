package jp.co.fj.banzai.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * インターネットバンザイ用の成績入力画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J101Servlet extends jp.co.fj.inet.servlets.J101Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 9111389322581714332L;

    /**
     * {@inheritDoc}
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String sslFlg = request.getParameter("sslFlg");
        if (sslFlg != null) {
            /* SSL接続フラグをセッションに保持する */
            HttpSession session = request.getSession(false);
            session.setAttribute("sslFlg", Boolean.valueOf(sslFlg));
        }

        super.execute(request, response);
    }

}

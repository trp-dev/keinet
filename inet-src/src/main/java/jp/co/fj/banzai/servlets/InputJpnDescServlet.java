package jp.co.fj.banzai.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.inet.servlets.DefaultHttpServlet;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * 成績入力画面サーブレット
 *
 * 2005.04.25	Yoshimoto KAWAI - Totec
 *				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class InputJpnDescServlet extends DefaultHttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -7384281799250979257L;

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        // HTTPセッション
        HttpSession session = request.getSession(false);
        // formに設定
        J101Form form = (J101Form) getActionForm(request, "jp.co.fj.inet.forms.J101Form");
        JpnDescTotalEvaluationBean jpnDesc = setJpnDescTotalEval(form);
        session.setAttribute("J101Form", form);
        // json形式で返却
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            String resJson = mapper.writeValueAsString(jpnDesc);
            pw.print(resJson);
        }
    }

    private JpnDescTotalEvaluationBean setJpnDescTotalEval(J101Form form) {
        final ServletContext context = getServletContext();
        List<JpnDescTotalEvaluationBean> jpnDesc = (List<JpnDescTotalEvaluationBean>) context.getAttribute("jpnDesc");
        JpnDescTotalEvaluationBean item = jpnDesc.stream().filter(e -> e.equals(form.getT1JaDescQ1(), form.getT1JaDescQ2(), form.getT1JaDescQ3())).findFirst().orElse(null);
        String eval = (item == null) ? "" : item.getTotalEvaluation();
        form.setT1JaDescQ4(eval);
        return item;
    }
}

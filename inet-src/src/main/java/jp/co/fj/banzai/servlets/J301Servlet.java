package jp.co.fj.banzai.servlets;

import java.util.List;

import jp.co.fj.banzai.beans.BZJ204ListBean;
import jp.co.fj.inet.beans.J204ListBean;

/**
 *
 * インターネットバンザイ用の志望大学一覧画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J301Servlet extends jp.co.fj.inet.servlets.J301Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 3200604592247152804L;

    /**
     * {@inheritDoc}
     */
    public J204ListBean createJudgementListBean(List judgedList) {
        return new BZJ204ListBean(judgedList);
    }

}

package jp.co.fj.banzai.servlets;

import java.util.List;

import jp.co.fj.banzai.beans.BZJ204ListBean;
import jp.co.fj.inet.beans.J204ListBean;

/**
 *
 * インターネットバンザイ用の判定結果一覧画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J204Servlet extends jp.co.fj.inet.servlets.J204Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 2632444662155163383L;

    /**
     * {@inheritDoc}
     */
    public J204ListBean createJudgementListBean(List judgedList) {
        return new BZJ204ListBean(judgedList);
    }

}

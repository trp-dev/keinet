package jp.co.fj.banzai.servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.data.LoginSession;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.util.KNCommonProperty;

/**
 *
 * インターネットバンザイ用のトップ画面サーブレットです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J001Servlet extends jp.co.fj.inet.servlets.J001Servlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -5028419460899152355L;

    /**
     * {@inheritDoc}
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* 初回アクセス時にセッションを初期化しておく */
        if (request.getParameter("forward") == null) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
        }

        /* 成績入力画面への遷移時に、ログインセッションと模試データ以外のセッションデータを削除する */
        if ("j101".equals(getForward(request))) {
            HttpSession session = request.getSession(false);
            if (session != null) {
                for (Enumeration enu = session.getAttributeNames(); enu.hasMoreElements();) {
                    String name = (String) enu.nextElement();
                    if (!name.equals(LoginSession.SESSION_KEY) && !name.equals(ExamBean.SESSION_KEY)) {
                        session.removeAttribute(name);
                    }
                }
            }
        }

        super.execute(request, response);
    }

    /**
     * {@inheritDoc}
     */
    protected ExamBean createExamBean() {
        return new ExamBean(JudgementConstants.Exam.Code.CENTERRESEARCH);
    }

    /**
     * {@inheritDoc}
     */
    public void forward(HttpServletRequest request, HttpServletResponse response, String forwardPage)
            throws ServletException, IOException {

        if (forwardPage.endsWith(".jsp")) {
            Properties props = (Properties) getServletContext().getAttribute(KNCommonProperty.class.getName());
            /* サーバIPアドレスをリクエストスコープに設定する */
            request.setAttribute("serverAddress", props.getProperty("SENDIP"));
            request.setAttribute("sslServerAddress", props.getProperty("SSLSEND"));
        }

        super.forward(request, response, forwardPage);
    }

}

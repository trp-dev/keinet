package jp.co.fj.banzai.beans;

import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivComment;
import jp.fujitsu.keinet.mst.file.model.UnivDataFile;

/**
 *
 * インターネットバンザイ用のJ204DetailBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZJ204DetailBean extends J204DetailBean {

    /** serialVersionUID */
    private static final long serialVersionUID = -1480070780162467777L;

    /** 第1段階選抜 */
    private final String pickedLine;

    /** 注釈文マスタの種別フラグ */
    private final String typeFlg;

    /**
     * コンストラクタです。
     *
     * @param detail {@link BZDetailPageBean}
     */
    BZJ204DetailBean(BZDetailPageBean detail) {
        super(detail);
        UnivDataFile univ = (UnivDataFile) detail.getBean().getUniv();
        this.pickedLine = univ.getPickedLine();
        UnivComment comment = univ.getUnivComment();
        this.typeFlg = comment == null ? null : comment.getTypeFlg();
    }

    /**
     * 第1段階選抜を返します。
     *
     * @return 第1段階選抜
     */
    public String getPickedLine() {
        return pickedLine;
    }

    /**
     * 注釈文マスタの種別フラグを返します。
     *
     * @return 注釈文マスタの種別フラグ
     */
    public String getTypeFlg() {
        return typeFlg;
    }

}

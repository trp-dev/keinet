package jp.co.fj.inet.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import jp.co.fj.inet.util.LoggerUtil;

import org.apache.log4j.MDC;

public class LoggerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 特にやる事なし。
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // セッションがスタートしていることが前提。
        LoggerUtil.initMDC();
        MDC.put("addr", request.getRemoteAddr());
        MDC.put("host", request.getRemoteHost());
        MDC.put("form", createScreenId(request));
        try {
            chain.doFilter(request, response);
        } finally {
            LoggerUtil.destoryMDC();
        }
    }

    /**
     * 画面ID（転送元:転送先）を生成します。
     *
     * @param request {@link HttpServletRequest}
     * @return 画面ID
     */
    private String createScreenId(final ServletRequest request) {
        final String backward = (String) request.getParameter("backward");
        final String forward = (String) request.getParameter("forward");
        final StringBuffer buff = new StringBuffer();
        if (backward == null && forward == null) {
            buff.append("-");
        } else {
            if (backward != null && !"".equals(backward)) {
                buff.append(backward);
            }
            buff.append(":");
            if (forward != null && !"".equals(forward)) {
                buff.append(forward);
            }
        }
        return buff.toString();
    }

    @Override
    public void destroy() {
        // 特にやる事なし。
    }
}

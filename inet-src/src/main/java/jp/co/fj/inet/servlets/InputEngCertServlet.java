package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.kawaijuku.judgement.beans.InputEngPtBean;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * 成績入力画面サーブレット
 *
 * 2005.04.25	Yoshimoto KAWAI - Totec
 *				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class InputEngCertServlet extends DefaultHttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -8074184700165727653L;

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ObjectMapper mapper = new ObjectMapper();

        // HTTPセッション
        HttpSession session = request.getSession(false);
        // 入力値を取得
        String json = request.getParameter("engCert");
        String suffix = request.getParameter("suffix");
        // オブジェクトに変換
        InputEngPtBean input = mapper.readValue(json, InputEngPtBean.class);
        /*UnivMasterData data = (UnivMasterData) getServletContext().getAttribute(UnivMasterData.class.getName());
        // 入力チェック
        int validResult = ExamUtil.validateEngCert(data, input);
        input.setErrorCode(validResult);
        if (validResult == 0) {
            // 認定試験情報を設定
            ExamUtil.createEngCert(data, input);
        }*/
        input.setErrorCode(0);
        // formに設定
        J101Form form = (J101Form) session.getAttribute("J101Form");
        if ("01".equals(suffix)) {
            form.setEngCert01(input);
        } else {
            form.setEngCert02(input);
        }
        session.setAttribute("J101Form", form);
        // json形式で返却
        response.setContentType("application/json;charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            String resJson = mapper.writeValueAsString(input);
            pw.print(resJson);
        }
    }
}

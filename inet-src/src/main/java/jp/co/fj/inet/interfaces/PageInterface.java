package jp.co.fj.inet.interfaces;

/**
 * PageInterface
 * JSPファイル名を定義するインターフェース
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public interface PageInterface {

	// --------------------------------------------------
	// 転送サーブレット
	String SERVLET_DISPATCHER = "/DispatcherServlet";
	// エラーページ
	String JSP_ERROR = "/jsp/error.jsp";
	// ログアウト画面
	String JSP_LOGOUT = "/jsp/jLogout.jsp";
	// --------------------------------------------------
	// ログイン画面
	String JSP_J001 = "/jsp/j001.jsp";
	// 成績入力画面
	String JSP_J101 = "/jsp/j101.jsp";
	// 成績入力完了画面
	String JSP_J102 = "/jsp/j102.jsp";
	// 大学選択画面
	String JSP_J201 = "/jsp/j201.jsp";
	// 大学選択（条件選択）画面
	String JSP_J202 = "/jsp/j202.jsp";
	// 大学選択（大学指定）画面
	String JSP_J203 = "/jsp/j203.jsp";
	// 判定処理中画面
	String JSP_JOnJudge = "/jsp/jOnJudge.jsp";
	// 判定結果一覧画面
	String JSP_J204 = "/jsp/j204.jsp";
	// 判定結果詳細画面（判定科目）
	String JSP_J205 = "/jsp/j205.jsp";
	// 判定結果詳細画面（公表科目）
	String JSP_J206 = "/jsp/j206.jsp";
	// 志望大学一覧画面
	String JSP_J301 = "/jsp/j301.jsp";
	// --------------------------------------------------

}

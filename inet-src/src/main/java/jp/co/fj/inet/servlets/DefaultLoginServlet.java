/*
 * 作成日: 2004/06/30
 */
package jp.co.fj.inet.servlets;

import javax.servlet.http.HttpServletRequest;

/**
 * ログイン画面サーブレット<BR>
 * ログインチェックは常にTRUEを返す
 *
 * @author kawai
 */
abstract public class DefaultLoginServlet extends DefaultHttpServlet {

    /**
    *
    */
    private static final long serialVersionUID = -1800172598703919211L;

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#isLogin(javax.servlet.http.HttpServletRequest)
     */
    protected boolean isLogin(HttpServletRequest request) {
        return true;
    }
}

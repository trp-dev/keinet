package jp.co.fj.inet.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * リクエストのエンコーディングを設定するフィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class EncodingFilter implements Filter {

    /** エンコーディング設定 */
    private String encoding;

    /**
     * {@inheritDoc}
     */
    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter("encoding");
    }

    /**
     * {@inheritDoc}
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
            ServletException {
        if (req.getCharacterEncoding() == null) {
            req.setCharacterEncoding(encoding);
        }
        chain.doFilter(req, res);
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {
    }

}

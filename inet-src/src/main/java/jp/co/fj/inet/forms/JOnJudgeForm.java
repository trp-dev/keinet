package jp.co.fj.inet.forms;

import jp.co.fj.keinavi.forms.individual.IOnJudgeForm;

/**
 *
 * 判定処理中画面アクションフォーム
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JOnJudgeForm extends IOnJudgeForm {

    /** serialVersionUID */
    private static final long serialVersionUID = 6288131966177519687L;

    /** 配点比率 */
    private String allotPntRatio;

    /**
     * 配点比率を返します。
     *
     * @return 配点比率
     */
    public String getAllotPntRatio() {
        return allotPntRatio;
    }

    /**
     * 配点比率をセットします。
     *
     * @param allotPntRatio 配点比率
     */
    public void setAllotPntRatio(String allotPntRatio) {
        this.allotPntRatio = allotPntRatio;
    }

}

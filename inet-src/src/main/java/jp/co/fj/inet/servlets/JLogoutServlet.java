package jp.co.fj.inet.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * ログアウト画面サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JLogoutServlet extends DefaultLoginServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 213229410117898544L;

    /**
     * {@inheritDoc}
     */
    public void execute(final HttpServletRequest request, final HttpServletResponse response) throws ServletException,
            IOException {

        // アクセスログ
        actionLog(request, "110");

        forward(request, response, JSP_LOGOUT);
    }

}

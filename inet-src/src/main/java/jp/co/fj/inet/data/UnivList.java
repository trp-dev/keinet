/*
 * 作成日: 2005/05/09
 */
package jp.co.fj.inet.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * 志望大学一覧を保持するクラス
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 * 2009.10.16   Totec) S.Hase
 *					大学コード１０桁からDEPTCDの抽出を「7文字目から2文字」→「7文字目から4文字」に変更。
 *
 */
public class UnivList extends ArrayList {

    /**
    *
    */
    private static final long serialVersionUID = -6031600334405177379L;

    // セッションキー
    public static final String SESSION_KEY = "UnivList";

    // 判定済みフラグ
    private boolean judged = true;

    /**
     * 要素を判定に利用する形式に変換して返す
     *
     * UUUUFFDD → UUUU,FF,DD
     *
     * @return
     */
    public String[] getUnivValue() {
        String[] array = new String[size()];
        for (int i = 0; i < size(); i++) {
            String value = (String) get(i);
            array[i] = value.substring(0, 4) + "," + value.substring(4, 6) + "," + value.substring(6, 10);
        }
        return array;
    }

    /**
     * 要素をキーとするマップを返す
     *
     * @return
     */
    public Map getKeyMap() {
        Map map = new HashMap();
        Iterator ite = iterator();
        while (ite.hasNext()) {
            String value = (String) ite.next();
            map.put(value, Boolean.TRUE);
        }
        return map;
    }

    public boolean isJudged() {
        return this.judged;
    }

    public void setJudged(boolean judged) {
        this.judged = judged;
    }
}

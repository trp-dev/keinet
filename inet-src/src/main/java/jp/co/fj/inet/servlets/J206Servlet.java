package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.inet.beans.JSubjectPageBean;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.forms.individual.I206Form;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 判定結果詳細画面（公表科目）サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class J206Servlet extends DefaultHttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 8445289724125835646L;

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if ("j206".equals(getForward(request))) {

            /* アクションフォーム */
            I206Form form = (I206Form) getActionForm(request, I206Form.class.getName());

            try {
                /* 対象の大学インスタンスを取得する */
                UnivData univ = findUniv(form.getUniqueKey());
                /* 表示用データを作成する */
                JSubjectPageBean subjectPageBean = new JSubjectPageBean(univ);
                subjectPageBean.execute(getUnivInfoMap());
                request.setAttribute("pageBean", subjectPageBean);
            } catch (Exception e) {
                throw new KeiNetException(e, "00J206010101", "J206にてエラーが発生しました。", false);
            }

            forward(request, response, JSP_J206);
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * 大学インスタンスをメモリから取得します。
     *
     * @param uniqueKey 大学キー
     * @return {@link UnivData}
     */
    private UnivData findUniv(String uniqueKey) throws Exception {

        for (Iterator ite = getUnivAll().iterator(); ite.hasNext();) {
            UnivData univ = (UnivData) ite.next();
            if (univ.getUniqueKey().endsWith(uniqueKey)) {
                return univ;
            }
        }

        throw new KeiNetException("詳細データの取得に失敗しました。" + uniqueKey, "1007", MessageLoader.getInstance().getMessage("j028a"));
    }

}

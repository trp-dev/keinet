package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.inet.beans.J204ListBean;
import jp.co.fj.inet.beans.JScoreBean;
import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.inet.forms.J205Form;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.kawaijuku.judgement.Constants;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.JudgementExecutor;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;
import jp.co.fj.keinavi.util.individual.IPropsLoader;
import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.log4j.Logger;

/**
 *
 * 判定結果詳細サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class J205Servlet extends DefaultHttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 16342185294495830L;
    /** デバッグログ出力フラグ */
    private static final String DEBUG_SCORE = IPropsLoader.getInstance().getMessage("DEBUG_SCORE");
    private static final String DEBUG_RESULT = IPropsLoader.getInstance().getMessage("DEBUG_RESULT");
    private static final Logger logger = Logger.getLogger("jlog");

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if ("j205".equals(getForward(request))) {

            /* アクションフォーム */
            J205Form form = (J205Form) getActionForm(request, J205Form.class.getName());

            /* HTTPセッション */
            HttpSession session = request.getSession(false);

            /* セッションに格納してある結果一覧を取得 */
            J204ListBean jlb;
            if (form.getJudgeNo() != null && form.getJudgeNo().trim().equals("2")) {
                jlb = (J204ListBean) session.getAttribute("useListBean2");
            } else {
                jlb = (J204ListBean) session.getAttribute("useListBean");
            }

            /* 模試Bean */
            ExamBean exam = getExamBean(request);

            /* 成績入力画面のアクションフォームを取得する */
            J101Form f101 = (J101Form) session.getAttribute("J101Form");

            try {
                /* 英語認定試験【含まない】評価の判定処理を実行 */
                DetailPageBean exclude = judge(session, jlb, form.getUniqueKey(), exam, f101, false);

                /* 判定詳細データが見つからない場合はエラー */
                if (exclude == null) {
                    throw new KeiNetException("詳細データの取得に失敗しました。", "1006", MessageLoader.getInstance().getMessage("j028a"), false);
                }

                /* ドッキング判定できないなら総合は出さない */
                if (!exam.isCombi()) {
                    exclude.setTLetterJudgement("");
                    exclude.setT_scoreStr("");
                }

                request.setAttribute("Judge", exclude);

                /* 英語認定試験【含む】評価の判定処理を実行 */
                /*DetailPageBean include = judge(session, jlb, form.getUniqueKey(), exam, f101, true);
                request.setAttribute("JudgeI", include);*/

                /* 英語認定試験を含む共通テスト評価欄の表示有無を設定
                 * いずれかに該当する場合、表示しない
                 *   １．英語認定試験が未入力
                 *   ２．一般私大
                 *   ３．英語認定試験を利用しない大学
                 *  */
                /*if (f101.isEngPtEmpty() || " ".equals(exclude.getScheduleCd()) || (!include.isUseAppRequirement() && !include.isUseScoreConversion())) {
                    request.setAttribute("isEngPtDisplay", false);
                } else {
                    request.setAttribute("isEngPtDisplay", true);
                }*/

                /* 模試区分を設定する */
                request.setAttribute("univExamDiv", getExamDiv());

            } catch (Exception e) {
                throw new KeiNetException(e, "00J205010101", "0J205にてエラーが発生しました。", false);
            }

            forward(request, response, JSP_J205);
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * 判定を実行します、
     *
     * @param session セッション情報
     * @param jlb 判定結果リストBean
     * @param uniqueKey 大学ユニークキー
     * @param exam 模試情報
     * @param f101 成績入力画面のフォームBean
     * @return {@link DetailPageBean}
     * @throws Exception 判定に失敗した時
     */
    protected DetailPageBean judge(HttpSession session, J204ListBean jlb, String uniqueKey, ExamBean exam, J101Form f101, boolean engPtInclude) throws Exception {
        Map<String, ExamSubjectBean> examSubject = (Map<String, ExamSubjectBean>) getServletContext().getAttribute(ExamSubjectBean.SESSION_KEY);
        if (jlb != null) {
            for (Iterator ite = jlb.getRecordSetAll().iterator(); ite.hasNext();) {
                J204DetailBean bean = (J204DetailBean) ite.next();
                if (bean.getUniqueKey().equals(uniqueKey)) {
                    JScoreBean score = new JScoreBean(f101, exam, examSubject);
                    score.execute();
                    /* デバッグログを出力する */
                    if (Constants.FLG_ON.equals(DEBUG_SCORE)) {
                        /*MDC.put("code", "-");*/
                        JudgedUtil.outputScoreDebugLog(logger, score);
                    }
                    /*UnivData univ;
                    if (engPtInclude) {
                        univ = deepCopy(bean.getUniv());
                        univ.setSSHanteiFlg(1);
                    } else {
                        univ = bean.getUniv();
                    }*/
                    JudgementExecutor exec = new JudgementExecutor(bean.getUniv(), score);
                    exec.execute();
                    DetailPageBean detail = jlb.judge2Detail(exec.getJudgement());
                    /* 本人成績の情報を作成する */
                    detail.executeScore();
                    /* デバッグログを出力する */
                    if (Constants.FLG_ON.equals(DEBUG_RESULT)) {
                        JudgedUtil.outputResultDebugLog(logger, exec.getJudgement());
                    }

                    return detail;
                }
            }
        }
        return null;
    }

    /*private <T> T deepCopy(T obj) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new ObjectOutputStream(baos).writeObject(obj);
        return (T) new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray())).readObject();
    }*/
}

package jp.co.fj.inet.framework.exception;

import javax.servlet.ServletException;

import lombok.Getter;
import lombok.Setter;

/**
 * Kei-Net例外クラス
 *
 * @author kurimoto
 */
@Getter
@Setter
public class KeiNetException extends ServletException {

    /** SID */
    private static final long serialVersionUID = -9077686213437172493L;
    String errorCode = "0"; // エラーコード
    String errorMessage; // エラーメッセージ
    boolean close = true; // エラー画面に閉じるボタンを表示するかどうか
    boolean timeout = false; // タイムアウトかどうか

    /**
     * コンストラクタです。
     * @param errorMessage エラーメッセージ
     */
    public KeiNetException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    /**
     * コンストラクタです。
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     */
    public KeiNetException(String errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * コンストラクタです。
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     * @param close 閉じる有無
     */
    public KeiNetException(String errorCode, String errorMessage, boolean close) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.close = close;
    }

    /**
     * コンストラクタです。
     * @param e 例外
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     */
    public KeiNetException(Throwable e, String errorCode, String errorMessage) {
        super(e);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * コンストラクタです。
     * @param e 例外
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     * @param close 閉じる有無
     */
    public KeiNetException(Throwable e, String errorCode, String errorMessage, boolean close) {
        super(e);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.close = close;
    }

    /**
     * コンストラクタです。
     * @param superMessage ServletExceptionに渡すメッセージ
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     */
    public KeiNetException(String superMessage, String errorCode, String errorMessage) {
        super(superMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * コンストラクタです。
     * @param superMessage ServletExceptionに渡すメッセージ
     * @param errorCode エラーコード
     * @param errorMessage エラーメッセージ
     * @param close 閉じる有無
     */
    public KeiNetException(String superMessage, String errorCode, String errorMessage, boolean close) {
        super(superMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.close = close;
    }

    /**
     * エラーメッセージを表示するかどうか
     * @return
     */
    public boolean isShowMessage() {
        return this.getErrorCode().startsWith("1");
    }

    public KeiNetException Timeout() {
        this.timeout = true;
        return this;
    }
}

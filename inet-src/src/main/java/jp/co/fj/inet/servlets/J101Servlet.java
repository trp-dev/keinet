package jp.co.fj.inet.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.data.UnivList;
import jp.co.fj.inet.forms.J101Form;

/**
 *
 * 成績入力画面サーブレット
 *
 * 2005.04.25	Yoshimoto KAWAI - Totec
 *				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class J101Servlet extends DefaultHttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -9129687072334267426L;

    /* (非 Javadoc)
     * @see jp.co.fj.keinavi.servlets.DefaultHttpServlet#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // HTTPセッション
        HttpSession session = request.getSession(false);
        // アクションフォーム - request scope
        J101Form rform = (J101Form) getActionForm(request, "jp.co.fj.inet.forms.J101Form");

        // 転送元がj101の場合
        if ("j101".equals(getBackward(request))) {
            // フォームが変更されていればアクションフォームをセッションに保持する
            if ("true".equals(rform.getChanged())) {
                /*deserializeEngCert(request, rform);*/
                session.setAttribute("J101Form", rform);

                // 志望大学リストの判定済みフラグを落とす
                UnivList univ = (UnivList) session.getAttribute(UnivList.SESSION_KEY);
                if (univ != null) {
                    univ.setJudged(false);
                }
            }
        }

        // JSP
        if ("j101".equals(getForward(request))) {

            // アクションフォーム - session scope
            J101Form sform = (J101Form) session.getAttribute("J101Form");

            // セッションになければフォームの初期値をセットする
            if (sform == null) {
                sform = new J101Form();
                sform.setType("2");
                session.setAttribute("J101Form", sform);
            }

            super.forward(request, response, JSP_J101);

            // 転送
        } else {
            super.forward(request, response, SERVLET_DISPATCHER);
        }

    }

    /*private void deserializeEngCert(HttpServletRequest request, J101Form form) throws JsonParseException, JsonMappingException, IOException {
        UnivMasterData data = (UnivMasterData) getServletContext().getAttribute(UnivMasterData.class.getName());
        ObjectMapper mapper = new ObjectMapper();
    
        InputEngPtBean engCert01 = mapper.readValue(request.getParameter("engCert01"), InputEngPtBean.class);
        ExamUtil.createEngCert(data, engCert01);
        form.setEngCert01(engCert01);
    
        InputEngPtBean engCert02 = mapper.readValue(request.getParameter("engCert02"), InputEngPtBean.class);
        ExamUtil.createEngCert(data, engCert02);
        form.setEngCert02(engCert02);
    
    }*/

}

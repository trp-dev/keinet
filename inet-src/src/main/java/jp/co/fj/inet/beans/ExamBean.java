package jp.co.fj.inet.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.data.individual.ExaminationData;

/**
 *
 * 模試データを保持するBeanです。<br>
 * スマホ対応以前はDBの模試マスタを参照して動的にデータを生成していましたが、<br>
 * 通年でドッキング判定が行えるように、内部で模試データを固定生成するように変更しました。<br>
 * publicメソッドはJSPから呼び出されているものも多く、削除せずに固定値を返すようにしています。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamBean implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 4706630156949561220L;

    /** セッションキー */
    public static final String SESSION_KEY = "ExamBean";

    /** 模試リスト */
    private final List examList;

    /** マーク模試 */
    private final ExaminationData markExam;

    /** 記述模試 */
    private final ExaminationData wrtnExam;

    /**
     * コンストラクタです。
     *
     * @param markExamCd マーク模試コード
     */
    public ExamBean(String markExamCd) {

        /* マーク模試 */
        markExam = new ExaminationData();
        markExam.setExamCd(markExamCd);
        markExam.setExamTypeCd(JudgementConstants.Exam.Type.MARK);

        /* 記述模試は＃３記述で固定する */
        wrtnExam = new ExaminationData();
        wrtnExam.setExamCd(JudgementConstants.Exam.Code.WRTN3);
        wrtnExam.setExamTypeCd(JudgementConstants.Exam.Type.WRITTEN);

        /* 模試リストを初期化する */
        examList = new ArrayList(2);
        examList.add(markExam);
        examList.add(wrtnExam);
    }

    /**
     * マーク模試がセンターリサーチかどうかを返します。
     *
     * @return マーク模試がセンターリサーチならtrue
     */
    public boolean isCenterResearch() {
        return JudgementConstants.Exam.Code.CENTERRESEARCH.equals(markExam.getExamCd());
    }

    /**
     * マーク模試がセンタープレかどうかを返します。
     *
     * @return マーク模試がセンタープレならtrue
     */
    public boolean isCenterPre() {
        return JudgementConstants.Exam.Code.CENTERPRE.equals(markExam.getExamCd());
    }

    /**
     * 記述模試が高２記述かどうかを返します。
     *
     * @return 記述模試が高２記述ならtrue
     */
    public boolean isWrtn2nd() {
        return false;
    }

    /**
     * 記述模試が＃１記述かどうかを返します。
     *
     * @return 記述模試が＃１記述ならtrue
     */
    public boolean is1stWrtn() {
        return false;
    }

    /**
     * マーク模試が第1解答科目模試かどうかを返します。
     *
     * @return マーク模試が第1解答科目模試ならtrue
     */
    public boolean isAns1st() {
        return true;
    }

    /**
     * 模試リストのサイズを返します。
     *
     * @return 模試リストのサイズ
     */
    public int getSize() {
        return examList.size();
    }

    /**
     * ドッキング判定が可能であるかを返します。
     *
     * @return ドッキング判定が可能ならtrue
     */
    public boolean isCombi() {
        return true;
    }

    /**
     * 模試リストを返します。
     *
     * @return 模試リスト
     */
    public List getExamList() {
        return examList;
    }

    /**
     * マーク模試を返します。
     *
     * @return マーク模試
     */
    public ExaminationData getMarkExam() {
        return this.markExam;
    }

    /**
     * 記述模試を返します。
     *
     * @return 記述模試
     */
    public ExaminationData getWrtnExam() {
        return this.wrtnExam;
    }

}

package jp.co.fj.inet.forms;

/**
 *
 * 志望大学一覧画面アクションフォーム
 *
 * 2005.05.09	Yoshimoto KAWAI - Totec
 * 				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class J301Form extends BaseForm {

    /**
    *
    */
    private static final long serialVersionUID = -1500491901874449274L;
    private String mode; // 動作モード
    private String uniqueKey; // 大学キー
    private String sortKey; // ソートキー

    public String getMode() {
        return this.mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUniqueKey() {
        return this.uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public String getSortKey() {
        return this.sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }
}

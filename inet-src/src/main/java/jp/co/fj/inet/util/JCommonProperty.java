/*
 * 作成日: 2005/06/13
 */
package jp.co.fj.inet.util;

import jp.co.fj.keinavi.util.KNCommonProperty;


/**
 *
 * KNCommonPropertyの拡張クラス
 * 
 * 2005.06.13	[新規作成]
 * 
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 * 
 */
public class JCommonProperty extends KNCommonProperty {

	/**
	 * リファラチェックURL
	 * @return
	 * @throws Exception
	 */
	public static String getRefererUrl() {
		return getValue("RefererUrl");
	}
	
}

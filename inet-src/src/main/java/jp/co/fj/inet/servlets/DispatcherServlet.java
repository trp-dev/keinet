package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * DispatcherServlet
 * 指定された画面IDへの転送を行うServlet
 *
 * 2005.04.25	Yoshimoto KAWAI - Totec
 * 				[新規作成]
 * 				keinaviからのコピーに対して以下の変更を加えた
 * 				・転送マップ内容をクリア
 * 				・ログインセッションの取得をコメントアウト
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class DispatcherServlet extends DefaultHttpServlet {

    /**
    *
    */
    private static final long serialVersionUID = 5227049789872811565L;
    private static final Map forwardMap = new HashMap();

    static {
        // トップ画面
        forwardMap.put("j001", "/Top");
        // 成績入力画面
        forwardMap.put("j101", "/InputScore");
        // 成績入力画面
        forwardMap.put("j102", "/InputCompleted");
        // 大学選択画面
        forwardMap.put("j201", "/UnivSelection");
        // 大学選択（条件選択）画面
        forwardMap.put("j202", "/UnivCond");
        // 大学選択（大学指定）画面
        forwardMap.put("j203", "/UnivSpec");
        // 判定結果一覧画面
        forwardMap.put("j204", "/UnivJudge");
        // 判定結果詳細画面（判定科目）
        forwardMap.put("j205", "/UnivDetail");
        // 判定結果詳細画面（公表科目）
        forwardMap.put("j206", "/UnivDetailPublic");
        // 志望大学一覧画面
        forwardMap.put("j301", "/UnivList");
    }

    /**
     * @see jp.co.fj.inet.servlets.DefaultHttpServlet#execute(
     * 			javax.servlet.http.HttpServletRequest,
     * 			javax.servlet.http.HttpServletResponse)
     */
    public void execute(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        // 転送先の画面ID
        final String key = getForward(request);
        final String forward = key == null ? null : (String) forwardMap.get(key);

        if (forward == null) {
            throw new KeiNetException("不正な転送先が指定されました。画面ID: " + key, "1003", MessageLoader.getInstance().getMessage("j027a"));
        } else {
            forward(request, response, forward);
        }

    }

}

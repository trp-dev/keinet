package jp.co.fj.inet.servlets;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.beans.JScoreBean;
import jp.co.fj.inet.beans.JudgeByCondBean;
import jp.co.fj.inet.data.JProvisoData;
import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.inet.forms.J204Form;
import jp.co.fj.kawaijuku.judgement.Constants;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.keinavi.beans.individual.JudgeByUnivBean;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.date.ShortDatePlusUtil;
import jp.co.fj.keinavi.util.db.RecordProcessor;

import org.apache.log4j.Logger;

/**
 *
 * インターネット模試判定用の判定処理スレッドです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
class JudgeThread extends Thread {

    /** デバッグログ出力フラグ */
    private static final String DEBUG_SCORE = KNCommonProperty.getStringValue("DEBUG_SCORE");
    private static final String DEBUG_RESULT = KNCommonProperty.getStringValue("DEBUG_RESULT");
    private static final Logger logger = Logger.getLogger("jlog");

    /** HttpSession */
    private final HttpSession session;

    /** アクションフォーム */
    private final J204Form form;

    /** 大学インスタンスリスト */
    private final List univAll;

    /** 判定結果リストのセッションキー */
    private final String sessionKey;

    /** 画面用の判定結果一覧ファクトリ */
    private final JudgementListFactory factory;

    /** 模試科目マスタ */
    private final Map<String, ExamSubjectBean> examSubject;

    private final String useSystem;

    /**
     * コンストラクタです。
     *
     * @param session {@link HttpSession}
     * @param form アクションフォーム
     * @param univAll 大学インスタンスリスト
     * @param sessionKey 判定結果リストのセッションキー
     * @param factory 画面用の判定結果一覧ファクトリ
     * @param examSubject 模試科目マスタ
     */
    JudgeThread(HttpSession session, J204Form form, List univAll, String sessionKey, JudgementListFactory factory, Map<String, ExamSubjectBean> examSubject, String system) {
        this.session = session;
        this.form = form;
        this.univAll = univAll;
        this.sessionKey = sessionKey;
        this.factory = factory;
        this.examSubject = examSubject;
        this.useSystem = system;
        /* 結果を初期化する */
        session.setAttribute(sessionKey, null);
        session.setAttribute(JOnJudgeServlet.THREAD_KEY, JOnJudgeServlet.STATUS_PROG);
    }

    /**
     * {@inheritDoc}
     */
    public void run() {

        /* 成績入力画面のアクションフォームを取得する */
        J101Form f101 = (J101Form) session.getAttribute("J101Form");

        /* 模試データ */
        ExamBean exam = (ExamBean) session.getAttribute(ExamBean.SESSION_KEY);

        try {
            /* 成績データを初期化する */
            Score score = new JScoreBean(f101, exam, examSubject);
            score.execute();

            /* デバッグログを出力する */
            if (Constants.FLG_ON.equals(DEBUG_SCORE)) {
                /*MDC.put("code", "-");*/
                JudgedUtil.outputScoreDebugLog(logger, score);
            }

            List judgedList;
            if ("judge".equals(form.getButton())) {
                /* こだわり条件判定の場合 */
                judgedList = judgeByCond(score, createJProvisoData());
            } else {
                /* 大学指定判定の場合 */
                judgedList = judgeByUniv(score);
            }

            /* デバッグログを出力する */
            if (Constants.FLG_ON.equals(DEBUG_RESULT)) {
                JudgedUtil.outputResultDebugLog(logger, judgedList);
            }

            /* 結果一覧をセッションにセットする */
            session.setAttribute(sessionKey, factory.createJudgementListBean(judgedList));
        } catch (Exception e) {
            session.setAttribute(JOnJudgeServlet.THREAD_KEY, JOnJudgeServlet.STATUS_ERR_SQL);
            e.printStackTrace();
        }

        session.setAttribute(JOnJudgeServlet.THREAD_KEY, JOnJudgeServlet.STATUS_DONE);
    }

    /**
     * 条件判定の検索条件データを生成します。
     *
     * @return {@link JProvisoData}
     */
    private JProvisoData createJProvisoData() {

        JProvisoData data = new JProvisoData();

        /* 地区 */
        data.setDistrictProviso(concatComma(form.getPref()));

        /* 系統区分 */
        data.setSearchStemmaDiv(form.getSearchStemmaDiv());

        if ("1".equals(form.getSearchStemmaDiv())) {
            /* 中系統だったら、中系統コードを入れる */
            data.setStemmaProviso(concatComma(form.getStemma2Code()));
        } else {
            /*
             * 小系統(文系)または小系統(理系)だったら、分野コードを入れる
             * 分野コードは中系統・文系・理系で値が重なることがあるので一意にする
             */
            Set set = CollectionUtil.array2Set(form.getStemmaCode());
            String[] stemmaCodes = (String[]) set.toArray(new String[set.size()]);
            data.setStemmaProviso(concatComma(stemmaCodes));
        }

        /* 2次試験課し区分 */
        data.setDescImposeDiv(form.getDescImposeDiv());

        /* 試験科目課し区分 */
        data.setSubImposeDiv(form.getSubImposeDiv());

        /* 課し試験科目 */
        data.setImposeSub(concatComma(form.getImposeSub()));

        /* 学校区分 */
        data.setSchoolDivProviso(concatComma(form.getSchoolDivProviso()));

        /* 入試開始日 */
        data.setStartDateProviso(ShortDatePlusUtil.getUnformattedString(form.getStartMonth() + "/" + form.getStartDate()));

        /* 入試終了日 */
        data.setEndDateProviso(ShortDatePlusUtil.getUnformattedString(form.getEndMonth() + "/" + form.getEndDate()));

        /* 評価試験区分 */
        data.setRaingDiv(form.getRaingDiv());

        /* 評価範囲 */
        data.setRaingProviso(concatComma(form.getRaingProviso()));

        /* 対象外条件 */
        data.setOutOfTergetProviso(concatComma(form.getOutOfTergetProviso()));

        /* 入試日区分 */
        data.setDateSearchDiv(form.getDateSearchDiv());

        /* 配点比率 */
        data.setAllotPntRatio(form.getAllotPntRatio());

        return data;
    }

    /**
     * 文字列配列の全ての要素をカンマで結合して返す
     *
     * @param array 文字列配列
     * @return 結合した文字列
     */
    private String concatComma(String[] array) {
        if (array == null) {
            return " ";
        } else {
            return CollectionUtil.deSplitComma(array);
        }
    }

    /**
     * こだわり条件判定を実行します。
     *
     * @param score {@link Score}
     * @param data {@link JProvisoData}
     * @return 判定結果リスト
     * @throws Exception 例外
     */
    private List judgeByCond(Score score, JProvisoData data) throws Exception {
        JudgeByCondBean jbcb = new JudgeByCondBean();
        jbcb.setUnivAllList(univAll);
        jbcb.setDistrictProviso(CollectionUtil.deconcatComma(data.getDistrictProviso()));
        jbcb.setSearchStemmaDiv(data.getSearchStemmaDiv());
        jbcb.setSearchStemmaDiv(data.getSearchStemmaDiv());
        jbcb.setStemmaProviso(CollectionUtil.deconcatComma(data.getStemmaProviso()));
        jbcb.setDescImposeDiv(data.getDescImposeDiv());
        jbcb.setSubImposeDiv(data.getSubImposeDiv());
        jbcb.setImposeSub(CollectionUtil.deconcatComma(data.getImposeSub()));
        jbcb.setStartMonth(RecordProcessor.getMonthDigit(data.getStartDateProviso()));
        jbcb.setStartDate(RecordProcessor.getDateDigit(data.getStartDateProviso()));
        jbcb.setEndMonth(RecordProcessor.getMonthDigit(data.getEndDateProviso()));
        jbcb.setEndDate(RecordProcessor.getDateDigit(data.getEndDateProviso()));
        jbcb.setRaingDiv(data.getRaingDiv());
        jbcb.setRaingProviso(CollectionUtil.deconcatComma(data.getRaingProviso()));
        jbcb.setSchoolDivProviso(CollectionUtil.deconcatComma(data.getSchoolDivProviso()));
        jbcb.setOutOfTergetProviso(CollectionUtil.deconcatComma(data.getOutOfTergetProviso()));
        jbcb.setDateSearchDiv(data.getDateSearchDiv());
        jbcb.setAllotPntRatio(data.getAllotPntRatio());
        jbcb.setScoreBean(score);
        jbcb.setSystem(useSystem);
        jbcb.execute();
        return jbcb.getRecordSet();
    }

    /**
     * 大学指定判定を実行します。
     *
     * @param score {@link Score}
     * @return 判定結果リスト
     * @throws Exception 例外
     */
    private List judgeByUniv(Score score) throws Exception {
        JudgeByUnivBean jbub = new JudgeByUnivBean();
        jbub.setUnivAllList(univAll);
        jbub.setUnivArray(createUnivArray());
        jbub.setScoreBean(score);
        jbub.execute();
        return jbub.getRecordSet();
    }

    /**
     * 大学指定判定用の大学情報配列を生成します。
     *
     * @return 大学情報配列
     */
    private String[][] createUnivArray() {
        String[][] univArray = new String[form.getUnivValue().length][4];
        for (int i = 0; i < form.getUnivValue().length; i++) {
            StringTokenizer st = new StringTokenizer(form.getUnivValue()[i], ",");
            String[] univValue = new String[4];
            /* 大学コード */
            univValue[0] = st.nextToken();
            /* 学部コード */
            univValue[1] = st.nextToken();
            /* 学科コード */
            univValue[2] = st.nextToken();
            /* 追加順 */
            univValue[3] = Integer.toString(i);
            univArray[i] = univValue;
        }
        return univArray;
    }
}

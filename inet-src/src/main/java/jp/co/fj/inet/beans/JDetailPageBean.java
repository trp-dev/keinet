package jp.co.fj.inet.beans;

import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;

/**
 *
 * 模試判定システムの {@link DetailPageBean} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JDetailPageBean extends DetailPageBean {

    /** serialVersionUID */
    private static final long serialVersionUID = 4393516222349732959L;

    /**
     * コンストラクタです。
     *
     * @param bean {@link Judgement}
     */
    protected JDetailPageBean(Judgement bean) {
        super(bean);
    }

    /**
     * {@inheritDoc}
     */
    public void execute() throws Exception {

        super.execute();

        /* ドッキング判定が不要な国公立大学は、センタ配点を「--」とする */
        Univ univ = bean.getUniv();
        if (!univ.isDockingNeeded() && ("01".equals(univ.getUnivDivCd()) || "02".equals(univ.getUnivDivCd()))) {
            super.cAllotPntRateAll = "--";
        }
    }

}

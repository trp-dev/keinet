package jp.co.fj.inet.data;

import java.io.Serializable;

/**
 *
 * ログイン情報データクラス
 *
 * 2005.05.11	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class LoginSession implements Serializable {

    /**
    *
    */
    private static final long serialVersionUID = -1912467273882360113L;
    public static final String SESSION_KEY = "LoginSession"; // セッションキー

}

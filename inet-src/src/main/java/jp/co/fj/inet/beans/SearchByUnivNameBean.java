package jp.co.fj.inet.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.util.JpnStringConv;

/**
 *
 * 大学名検索Bean
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SearchByUnivNameBean {

    /** 全角ひらがな・全角カタカナのみ文字列パターン */
    private static final Pattern KANA_PATTERN = Pattern.compile("^[ぁ-んァ-ヶー・]+$");

    /**
     * 大学名検索を行います。
     *
     * @param univAll 大学インスタンスリスト
     * @param condition 検索条件（大学名）
     * @return 検索にヒットした大学インスタンスリスト
     */
    public List execute(List univAll, String condition) {

        List list = new ArrayList();

        if (univAll == null || univAll.isEmpty() || condition == null || condition.length() == 0) {
            /* 不正な入力値ならここまで */
            return list;
        }

        /* 検索条件が全角ひらがなと全角カタカナのみで構成される場合は全角カタカナに変換する */
        String name;
        if (KANA_PATTERN.matcher(condition).matches()) {
            name = JpnStringConv.hkana2Kkana(condition);
        } else {
            name = condition;
        }

        for (Iterator ite = univAll.iterator(); ite.hasNext();) {
            UnivData univ = (UnivData) ite.next();
            if ((univ.getUnivKanjiName() != null && univ.getUnivKanjiName().indexOf(name) > -1)
                    || (univ.getUnivKanaName() != null && univ.getUnivKanaName().indexOf(name) > -1)
                    || (univ.getUnivOtherName() != null && univ.getUnivOtherName().indexOf(name) > -1)) {
                /* 「大学コード表名」「大学カナ名」「大学その他名称」の何れかに部分一致する場合 */
                list.add(univ);
            }
        }

        return list;
    }

}

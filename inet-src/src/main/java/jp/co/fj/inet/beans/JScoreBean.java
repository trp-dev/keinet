package jp.co.fj.inet.beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.fj.inet.forms.J101Form;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.keinavi.data.individual.SubRecordData;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

/**
 *
 * Kei-NaviのScoreBeanを継承して初期化処理をするBean
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class JScoreBean extends Score {

    // serialVersionUID
    private static final long serialVersionUID = 4656022230185668656L;

    // 成績入力画面のアクションフォーム
    private final J101Form form;

    // 模試Bean
    private final ExamBean exam;

    // 模試科目マスタ
    private final Map<String, ExamSubjectBean> examSubject;

    /**
     * コンストラクタ
     *
     * @param form
     * @param exam
     */
    public JScoreBean(final J101Form form, final ExamBean exam, final Map<String, ExamSubjectBean> examSubject) {
        super(exam.getMarkExam(), null, exam.getWrtnExam(), null);
        this.form = form;
        this.exam = exam;
        this.examSubject = examSubject;
    }

    /**
     * @see com.fjh.beans.DefaultBean#execute()
     */
    public void execute() throws SQLException, Exception {

        // 対象模試はとりあえず記述系
        // 対象模試は適当でもいい気がするが・・・
        setTargetExam(exam.getWrtnExam());
        // マーク系模試
        setCExam(exam.getMarkExam());
        // 記述系模試
        setSExam(exam.getWrtnExam());

        // マーク系の成績をセットする
        setMark(new MarkBuilder().getRecord());
        // 偏差値にマーク系利用が選ばれていなければ
        // 記述系の成績をセットする
        if (!"1".equals(form.getType())) {
            setWrtn(new WrtnBuilder().getRecord());
        }

        // 記述系の成績がなければ対象模試はマーク系
        if (getWrtn() == null) {
            setTargetExam(exam.getMarkExam());
        }

        /* 英語認定試験情報を設定 */
        createEngPtScore();

        // execute
        super.execute();
    }

    /**
     * 判定書利用の英語認定試験情報を生成します。
     *
     */
    private void createEngPtScore() {
        List<EngSSScoreData> result = new ArrayList<>();
        if (!form.getEngCert01().isEmpty()) {
            result.add(form.getEngCert01().createEngSSScoreData());
        }
        if (!form.getEngCert02().isEmpty()) {
            result.add(form.getEngCert02().createEngSSScoreData());
        }
        this.setEngSSScoreDatas(result);
    }

    /**
     * 抽象的な成績ビルダー
     */
    private abstract class ScoreBuilder {

        // 科目レコードの入れ物
        protected final Map record = new HashMap();

        /**
         * 入れ物に科目レコードを追加する
         *
         * @param data
         */
        protected void addRecord(SubRecordData data) {
            if (data != null) {
                record.put(data.getSubCd(), data);
            }
        }

        /**
         * レコードがすでに存在するかどうか
         * @param subcd 科目コード
         * @return
         */
        protected boolean exist(String subcd) {
            return record.get(subcd) != null;
        }

        /**
         * 科目レコードを取得する
         * @param subcd
         * @return
         */
        protected SubRecordData getRecord(String subcd) {
            return (SubRecordData) record.get(subcd);
        }

        /**
         * 科目レコードの入れ物を返す
         *
         * @return
         */
        protected List getRecord() {
            if (record.size() > 0) {
                //データを格納していたマップをリストに
                //変換してかえす。
                final List container = new ArrayList();

                for (Iterator it = record.entrySet().iterator(); it.hasNext();) {
                    container.add((SubRecordData) ((Map.Entry) it.next()).getValue());
                }

                return container;
            } else {
                return null;
            }
        }

        /**
         * SubRecordDataのインスタンスに得点を詰めて返す
         *
         * @param subCd
         * @param score
         * @param deviation
         * @return
         */
        protected SubRecordData createSubScore(final String subCd, final String score) {
            // 成績がなければNULLを返す
            if (score == null || "".equals(score)) {
                return null;

                // あるなら作る
            } else {
                SubRecordData record = new SubRecordData();
                record.setSubCd(subCd);
                record.setScore(score);
                record.setCvsScore(score);
                record.setSubjectAllotPnt(getSubjectAllotPnt("01", subCd));
                return record;
            }
        }

        /**
         * SubRecordDataのインスタンスに国語記述のスコアを詰めて返す
         *
         * @return
         */
        /*protected SubRecordData createJapDescSubScore() {
            SubRecordData record = new SubRecordData();
        
            record.setSubCd(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU);
            record.setScope("99");
            record.setJpnHyoka1(form.getT1JaDescQ1());
            record.setJpnHyoka2(form.getT1JaDescQ2());
            record.setJpnHyoka3(form.getT1JaDescQ3());
            record.setJpnHyokaAll(form.getT1JaDescQ4());
            record.setSubjectAllotPnt(getSubjectAllotPnt(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU));
            return record;
        }*/

        /**
         * SubRecordDataのインスタンスに得点を詰めて返す
         *
         * @param subCd 科目コード
         * @param score 換算得点
         * @param scope 範囲区分
         * @return {@link SubRecordData}
         */
        protected SubRecordData createSubScore(final String subCd, final String score, final String scope) {
            // 成績がなければNULLを返す
            if (score == null || "".equals(score)) {
                return null;

                // あるなら作る
            } else {
                SubRecordData record = new SubRecordData();
                record.setSubCd(subCd);
                record.setScore(score);
                record.setCvsScore(score);
                record.setScope(scope);
                record.setSubjectAllotPnt(getSubjectAllotPnt("01", subCd));
                return record;
            }
        }

        /**
         * SubRecordDataのインスタンスに偏差値を詰めて返す
         *
         * @param subCd
         * @param score
         * @param deviation
         * @return
         */
        protected SubRecordData createSubDeviation(final String subCd, final String deviation, final String areaDiv) {
            // 成績がなければNULLを返す
            if (deviation == null || "".equals(deviation)) {
                return null;

                // あるなら作る
            } else {
                SubRecordData record = new SubRecordData();
                record.setScope(areaDiv);
                record.setSubCd(subCd);
                record.setCDeviation(deviation);
                record.setSubjectAllotPnt(getSubjectAllotPnt("02", subCd));
                return record;
            }
        }

        /**
         * 模試科目マスタから教科配点を取得します。
         *
         * @param examTypeCd 模試種別
         * @param subCd 科目コード
         * @return
         */
        private String getSubjectAllotPnt(String examTypeCd, String subCd) {
            String result = String.valueOf(Detail.ALLOT_NA);
            if (examSubject.containsKey(examTypeCd + subCd)) {
                result = String.valueOf(examSubject.get(examTypeCd + subCd).getSubAllotPnt());
            }
            return result;
        }

        /**
         * SubRecordDataのインスタンスに偏差値を詰めて返す
         * AreaDivはデフォルトの2を入れる
         *
         * @param subCd
         * @param score
         * @param deviation
         * @return
         */
        protected SubRecordData createSubDeviation(final String subCd, final String deviation) {
            return createSubDeviation(subCd, deviation, "2");
        }
    }

    /**
     *
     * マーク系の成績ビルダー
     *
     */
    private class MarkBuilder extends ScoreBuilder {

        // 第1解答科目
        private final String ANS1ST_NO1 = IPropsLoader.getInstance().getMessage("CODE_ANS1ST_NO1");
        // 第2解答科目
        private final String ANS1ST_NO2 = IPropsLoader.getInstance().getMessage("CODE_ANS1ST_NO2");
        // その他
        private final String ANS1ST_ETC = IPropsLoader.getInstance().getMessage("CODE_ANS1ST_ETC");

        /**
         * コンストラクタ
         */
        public MarkBuilder() {

            // 英語
            addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WRITING, form.getT1En1Score()));

            // リスニング
            addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LISTENING, form.getT1En2Score()));

            if ("1".equals(form.getT1Ma1Subject())) {
                // 数学I
                addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1, form.getT1Ma1Score()));
            } else if ("2".equals(form.getT1Ma1Subject())) {
                // 数学IA
                addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1A, form.getT1Ma1Score()));
            }

            if ("1".equals(form.getT1Ma2Subject())) {
                // 数学II
                addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2, form.getT1Ma2Score()));
            } else if ("2".equals(form.getT1Ma2Subject())) {
                // 数学IIB
                addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2B, form.getT1Ma2Score()));
            }

            // 現代文
            addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE, form.getT1Ja1Score()));

            // 古文
            addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD, form.getT1Ja2Score()));

            // 漢文
            addRecord(createSubScore(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE, form.getT1Ja3Score()));

            if (exam.isAns1st()) {
                // 理科1
                setupMarkScScore(form.getT1Sc1Subject(), form.getT1Sc1Score(), ANS1ST_NO1);
                // 理科2
                setupMarkScScore(form.getT1Sc2Subject(), form.getT1Sc2Score(), ANS1ST_NO2);
                // 理科3
                setupMarkScScore(form.getT1Sc3Subject(), form.getT1Sc3Score(), ANS1ST_NO2);
                // 理科4
                setupMarkScScore(form.getT1Sc4Subject(), form.getT1Sc4Score(), ANS1ST_NO2);
            } else {
                // 理科1
                setupMarkScScore(form.getT1Sc1Subject(), form.getT1Sc1Score(), ANS1ST_ETC);
                // 理科2
                setupMarkScScore(form.getT1Sc2Subject(), form.getT1Sc2Score(), ANS1ST_ETC);
                // 理科3
                setupMarkScScore(form.getT1Sc3Subject(), form.getT1Sc3Score(), ANS1ST_ETC);
                // 理科4
                setupMarkScScore(form.getT1Sc4Subject(), form.getT1Sc4Score(), ANS1ST_ETC);
            }

            if (exam.isAns1st()) {
                // 地歴・公民1
                setupMarkGhScore(form.getT1Gh1Subject(), form.getT1Gh1Score(), ANS1ST_NO1);
                // 地歴・公民2
                setupMarkGhScore(form.getT1Gh2Subject(), form.getT1Gh2Score(), ANS1ST_NO2);
            } else {
                // 地歴・公民1
                setupMarkGhScore(form.getT1Gh1Subject(), form.getT1Gh1Score(), ANS1ST_ETC);
                // 地歴・公民2
                setupMarkGhScore(form.getT1Gh2Subject(), form.getT1Gh2Score(), ANS1ST_ETC);
            }

            // 偏差値はマーク利用なら入れる
            if ("1".equals(form.getType())) {
                //マーク成績を二次判定に使用する
                mark4Second();
            }
        }

        /**
         * マーク模試を二次判定に使用する準備をする
         */
        private void mark4Second() {

            // 英語
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WRITING, form.getT1En1Dev());

            // リスニング
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LISTENING, form.getT1En2Dev());

            // 英＋Ｌ
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ENGLISH, form.getT1En3Dev());

            // 数学�@
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONE, form.getT1Ma3Dev());

            // 数学�A
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONETWO, form.getT1Ma4Dev());

            // 現代文
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE, form.getT1Ja6Dev());

            // 現・古
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD, form.getT1Ja5Dev());

            // 現・古・漢
            addRecord(JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE, form.getT1Ja4Dev());

            // 理科1
            String sci1 = toMarkScSubcd(form.getT1Sc1Subject());

            //入力がある場合のみ設定が必要
            if (sci1 != null) {
                addRecord(sci1, form.getT1Sc1Dev());
            }

            // 理科2
            String sci2 = toMarkScSubcd(form.getT1Sc2Subject());

            //入力がある場合のみ設定が必要
            if (sci2 != null) {
                addRecord(sci2, form.getT1Sc2Dev());
            }

            // 理科3
            String sci3 = toMarkScSubcd(form.getT1Sc3Subject());

            //入力がある場合のみ設定が必要
            if (sci3 != null) {
                addRecord(sci3, form.getT1Sc3Dev());
            }

            // 理科4
            String sci4 = toMarkScSubcd(form.getT1Sc4Subject());

            //入力がある場合のみ設定が必要
            if (sci4 != null) {
                addRecord(sci4, form.getT1Sc4Dev());
            }

            // 地歴・公民1
            String soc1 = toMarkGhSubcd(form.getT1Gh1Subject());

            //入力がある場合のみ設定が必要
            if (soc1 != null) {
                addRecord(soc1, form.getT1Gh1Dev());
            }

            // 地歴・公民2
            String soc2 = toMarkGhSubcd(form.getT1Gh2Subject());

            //入力がある場合のみ設定が必要
            if (soc2 != null) {
                addRecord(soc2, form.getT1Gh2Dev());
            }
        }

        /**
         * 偏差値レコードを追加
         * @param subcd 科目コード
         * @param deviation 偏差値
         */
        private void addRecord(String subcd, String deviation) {
            //既に同科目コードのレコードが存在する場合は
            //偏差値のみ付加する
            if (exist(subcd)) {
                appendCDeviation(subcd, deviation);
            }
            //新規レコードとして追加する
            else {
                addRecord(createSubDeviation(subcd, deviation));
            }
        }

        /**
         * 設定済みマーク科目成績に偏差値を付加する
         * @param subcd 科目コード
         * @param deviation 偏差値
         */
        private void appendCDeviation(String subcd, String deviation) {
            if (deviation != null && !"".equals(deviation)) {
                getRecord(subcd).setCDeviation(deviation);
            }
        }

        /**
         * マーク系の理科の得点をセットアップする
         *
         * @param subject 科目コンボボックスの選択値
         * @param score 換算得点
         * @param scope 範囲区分
         */
        private void setupMarkScScore(final String subject, final String score, final String scope) {
            String subCd = toMarkScSubcd(subject);
            if (subCd != null) {
                addRecord(createSubScore(subCd, score, scope));
            }
        }

        /**
         * マーク系の地歴・公民の得点をセットアップする
         *
         * @param subject 科目コンボボックスの選択値
         * @param score 換算得点
         * @param scope 範囲区分
         */
        private void setupMarkGhScore(final String subject, final String score, final String scope) {
            String subCd = toMarkGhSubcd(subject);
            if (subCd != null) {
                addRecord(createSubScore(subCd, score, scope));
            }
        }

        /**
         * マーク系の理科の科目コードを返します。
         *
         * @param subject 科目コンボボックスの選択値
         */
        private String toMarkScSubcd(final String subject) {
            if ("1".equals(subject)) {
                // 物理
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS;
            } else if ("2".equals(subject)) {
                // 化学
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY;
            } else if ("3".equals(subject)) {
                // 生物
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY;
            } else if ("4".equals(subject)) {
                // 地学
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH;
            } else if ("7".equals(subject)) {
                // 物理基礎
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC;
            } else if ("8".equals(subject)) {
                // 化学基礎
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC;
            } else if ("9".equals(subject)) {
                // 生物基礎
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC;
            } else if ("10".equals(subject)) {
                // 地学基礎
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH_BASIC;
            } else {
                // 未選択
                return null;
            }
        }

        /**
         * マーク系の地歴・公民の科目コードを返します。
         *
         * @param subject 科目コードコンボボックスの選択値
         */
        private String toMarkGhSubcd(final String subject) {
            if ("1".equals(subject)) {
                // 日本史A
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYA;
            } else if ("2".equals(subject)) {
                // 日本史B
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYB;
            } else if ("3".equals(subject)) {
                // 世界史A
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYA;
            } else if ("4".equals(subject)) {
                // 世界史B
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYB;
            } else if ("5".equals(subject)) {
                // 地理A
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYA;
            } else if ("6".equals(subject)) {
                // 地理B
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYB;
            } else if ("7".equals(subject)) {
                // 倫理
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICS;
            } else if ("8".equals(subject)) {
                // 政治・経済
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_POLITICS;
            } else if ("9".equals(subject)) {
                // 現代社会
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_SOCIAL;
            } else if ("10".equals(subject)) {
                // 倫政
                return JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS;
            } else {
                // 未選択
                return null;
            }
        }

    }

    /**
     *
     * 記述系の成績ビルダー
     *
     */
    private class WrtnBuilder extends ScoreBuilder {

        /**
         * コンストラクタ
         */
        public WrtnBuilder() {

            if ("1".equals(form.getListeningFlag())) {
                // 英語
                // リスニングあり
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ENGLISH, form.getT2EnDev()));
            } else {
                // リスニングなし
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ENGLISH, form.getT2EnDev(), "1"));
            }

            if ("1".equals(form.getT2MaSubject())) {
                // 数学I
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1, form.getT2MaDev()));
            } else if ("2".equals(form.getT2MaSubject())) {
                // 数学IA
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1A, form.getT2MaDev()));
            } else if ("3".equals(form.getT2MaSubject())) {
                // 数学IIA
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2A, form.getT2MaDev()));
            } else if ("4".equals(form.getT2MaSubject())) {
                // 数学IIB
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2B, form.getT2MaDev()));
            } else if ("5".equals(form.getT2MaSubject())) {
                // 数学III
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH3, form.getT2MaDev()));
            }

            if ("1".equals(form.getT2JaSubject())) {
                // 現・古・漢
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP3, form.getT2JaDev()));
            } else if ("2".equals(form.getT2JaSubject())) {
                // 現・古
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP2, form.getT2JaDev()));
            } else if ("3".equals(form.getT2JaSubject())) {
                // 現代文
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP1, form.getT2JaDev()));
            }

            // 理科1
            setupWrtnScDev(form.getT2Sc1Subject(), form.getT2Sc1Dev());
            // 理科2
            setupWrtnScDev(form.getT2Sc2Subject(), form.getT2Sc2Dev());

            // 地歴・公民1
            setupWrtnGhDev(form.getT2Gh1Subject(), form.getT2Gh1Dev());
            // 地歴・公民2
            setupWrtnGhDev(form.getT2Gh2Subject(), form.getT2Gh2Dev());
        }

        /**
         * 記述系の理科の成績をセットアップする
         *
         * @param subject 科目コンボボックスの選択値
         * @param deviation 偏差値
         */
        private void setupWrtnScDev(final String subject, final String deviation) {
            if ("1".equals(subject)) {
                // 物理(基礎)
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC, deviation, "1"));
            } else if ("2".equals(subject)) {
                // 物理
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS, deviation));
            } else if ("3".equals(subject)) {
                // 化学(基礎)
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC, deviation, "1"));
            } else if ("4".equals(subject)) {
                // 化学
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY, deviation));
            } else if ("5".equals(subject)) {
                // 生物(基礎)
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC, deviation, "1"));
            } else if ("6".equals(subject)) {
                // 生物
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY, deviation));
            } else if ("7".equals(subject)) {
                // 地学(基礎)
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH_BASIC, deviation, "1"));
            } else if ("8".equals(subject)) {
                // 地学
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH, deviation));
            }
        }

        /**
         * 記述系の地歴・公民の成績をセットアップする
         *
         * @param subject 科目コンボボックスの選択値
         * @param deviation 偏差値
         */
        private void setupWrtnGhDev(final String subject, final String deviation) {
            if ("1".equals(subject)) {
                // 日本史B
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JHISTORYB, deviation));
            } else if ("2".equals(subject)) {
                // 世界史B
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_WHISTORYB, deviation));
            } else if ("3".equals(subject)) {
                // 地理B
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_GEOGRAPHYB, deviation));
            } else if ("4".equals(subject)) {
                // 倫理
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ETHICS, deviation));
            } else if ("5".equals(subject)) {
                // 政治・経済
                addRecord(createSubDeviation(JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_POLITICS, deviation));
            }
        }

    }

}

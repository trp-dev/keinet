package jp.co.fj.inet.servlets;

import java.util.List;

import jp.co.fj.inet.beans.J204ListBean;

/**
 *
 * 画面用の判定結果一覧ファクトリです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
interface JudgementListFactory {

    /**
     * 画面用の判定結果一覧を生成します。
     *
     * @param judgedList 判定結果リスト
     * @return {@link J204ListBean}
     */
    J204ListBean createJudgementListBean(List judgedList);

}

package jp.co.fj.inet.data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * 名称指定検索画面の検索条件を保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J203Condition implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 3161933664132085936L;

    /** 大学名検索の条件 */
    private String searchStr;

    /** 頭文字検索の条件 */
    private String charValue;

    /** 判定対象の大学情報リスト */
    private List univValueList;

    /** 名称指定�A画面に戻るフラグ */
    private Boolean facultyStatusFlg;

    /**
     * 大学名検索の条件を返します。
     *
     * @return 大学名検索の条件
     */
    public String getSearchStr() {
        return searchStr;
    }

    /**
     * 大学名検索の条件をセットします。
     *
     * @param searchStr 大学名検索の条件
     */
    public void setSearchStr(String searchStr) {
        this.searchStr = searchStr;
    }

    /**
     * 頭文字検索の条件を返します。
     *
     * @return 頭文字検索の条件
     */
    public String getCharValue() {
        return charValue;
    }

    /**
     * 頭文字検索の条件をセットします。
     *
     * @param charValue 頭文字検索の条件
     */
    public void setCharValue(String charValue) {
        this.charValue = charValue;
    }

    /**
     * 判定対象の大学情報リストを返します。
     *
     * @return 判定対象の大学情報リスト
     */
    public List getUnivValueList() {
        return univValueList;
    }

    /**
     * 判定対象の大学情報リストをセットします。
     *
     * @param univValueList 判定対象の大学情報リスト
     */
    public void setUnivValueList(List univValueList) {
        this.univValueList = univValueList;
    }

    /**
     * 名称指定�A画面に戻るフラグを返します。
     *
     * @return 名称指定�A画面に戻るフラグ
     */
    public Boolean getFacultyStatusFlg() {
        return facultyStatusFlg;
    }

    /**
     * 名称指定�A画面に戻るフラグをセットします。
     *
     * @param facultyStatusFlg 名称指定�A画面に戻るフラグ
     */
    public void setFacultyStatusFlg(Boolean facultyStatusFlg) {
        this.facultyStatusFlg = facultyStatusFlg;
    }

}

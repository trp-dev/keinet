package jp.co.fj.inet.beans;

import java.util.Map;
import java.util.SortedMap;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.beans.individual.judgement.SubjectPageBean;

/**
 *
 * 公表用大学マスタの代わりに情報誌用科目データを利用する {@link SubjectPageBean} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JSubjectPageBean extends SubjectPageBean {

    /** serialVersionUID */
    private static final long serialVersionUID = 5103401371823348650L;

    /**
     * コンストラクタです。
     *
     * @param univ {@link UnivData}
     */
    public JSubjectPageBean(UnivData univ) {
        super(univ);
    }

    /**
     * 情報誌用科目データから表示用の枝番リストを作ります。
     *
     * @param univInfoMap 情報誌用科目データマップ
     */
    public void execute(Map univInfoMap) throws Exception {

        setupParams();

        if (univInfoMap != null) {
            SortedMap map = (SortedMap) univInfoMap.get(getUniv().getUniqueKey());
            if (map != null) {
                getDetailPageList().addAll(map.values());
            }
        }
    }

}

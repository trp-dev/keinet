package jp.co.fj.inet.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.SubjectStateBean;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.keinavi.beans.individual.JudgementManager;
import jp.co.fj.keinavi.util.individual.IPropsLoader;

import org.apache.log4j.Logger;

/**
 *
 * こだわり条件判定実行クラス
 *
 *
 * @author TOTEC)YAMADA.Tomohisa
 * @author TOTEC)HASE.Shoji
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JudgeByCondBean {

    /** 判定結果最大件数(判定結果画面に表示する最大件数＋1) */
    private static final int MAX_RECORD_NUM = Integer.parseInt(IPropsLoader.getInstance().getMessage("DISP_MAX_RESULT")) + 1;

    private static final Logger logger = Logger.getLogger("judgement");

    /** 大学インスタンスリスト */
    private List univAllList;

    /** 成績データ */
    private Score scoreBean;

    /** 県コードの配列 */
    private String[] districtProviso;

    /** 系統区分(1:中系統、2:小系統(文系)、3:小系統(理系)) */
    private String searchStemmaDiv;

    /** 中系統コードまたは分野コードの配列 */
    private String[] stemmaProviso;

    /** 二次・独自試験を課さないフラグ */
    private String descImposeDiv;

    /** 試験科目課し区分(0:課さない、1:課す) */
    private String subImposeDiv;

    /** 課し試験科目 */
    private String[] imposeSub;

    /** 学校区分の配列 */
    private String[] schoolDivProviso;

    /** 入試日条件有効フラグ */
    private String dateSearchDiv;

    /** 入試日条件開始月 */
    private String startMonth;

    /** 入試日条件開始日 */
    private String startDate;

    /** 入試日条件終了月 */
    private String endMonth;

    /** 入試日条件終了日 */
    private String endDate;

    /** 評価試験区分(1:センターまたは二次・私大、2:総合) */
    private String raingDiv;

    /** 評価範囲(1:A判定、2:B判定、3:C判定、4:D判定、5:E判定) */
    private String[] raingProviso;

    /** 対象外区分(1:女子大、2:二部、3:夜間主) */
    private String[] outOfTergetProviso;

    /** 配点比率 */
    private String allotPntRatio;

    /** 判定結果リスト */
    private List recordSet;

    private String system;

    /**
     * 対象模試がセンタープレの場合、一般私大・短大の二次判定を＃３記述ではなく
     * センタープレの成績を優先利用するかどうかのフラグ。
     * インターネット模試判定において「優先しない」とするため、デフォルト値はfalseとする。
     */
    private boolean precedeCenterPre = false;

    /**
     * こだわり条件判定を実行します。
     */
    public void execute() throws Exception {

        /* 判定対象大学リストを初期化する */
        List judgeTargetList = new ArrayList(univAllList);

        /* 地区(県コード)による絞り込みを実行する */
        if (districtProviso != null && districtProviso.length > 0) {
            restrictDistrict(judgeTargetList);
        }

        if ("1".equals(searchStemmaDiv)) {
            if (stemmaProviso != null && stemmaProviso.length > 0) {
                /* 中系統による絞り込みを実行する */
                restrictMStemma(judgeTargetList);
            }
        } else {
            if (stemmaProviso != null && stemmaProviso.length > 0) {
                /* 分野による絞り込みを実行する */
                restrictRegion(judgeTargetList);
            }
        }

        /* 学校区分による絞り込みを実行する */
        if (schoolDivProviso != null && schoolDivProviso.length > 0) {
            restrictSchoolDiv(judgeTargetList);
        }

        /* 入試日による絞り込みを実行する */
        if ("1".equals(dateSearchDiv)) {
            if (startMonth != null && startDate != null && endMonth != null && endDate != null) {
                restrictExamDate(judgeTargetList);
            }
        }

        /* 対象外条件による絞り込みを実行する */
        if (outOfTergetProviso != null && outOfTergetProviso.length > 0) {
            restrictOutOfTarget(judgeTargetList);
        }

        /* 配点比による絞り込みを実行する */
        if (allotPntRatio != null && allotPntRatio.length() > 0 && !allotPntRatio.equals("0")) {
            restrictAllotPointRate(judgeTargetList);
        }

        List judgedList;
        if ("1".equals(descImposeDiv)) {
            /* 二次・独自試験を課さない場合 */
            judgedList = restrictImpose(judgeTargetList, false, true);
        } else {
            /* 二次・独自試験を課す場合 */
            if ("1".equals(subImposeDiv)) {
                /* 試験科目を「課す」場合 */
                judgedList = restrictImpose(judgeTargetList, true, true);
            } else {
                /* 試験科目を「課さない」場合 */
                judgedList = restrictImpose(judgeTargetList, true, false);
            }
        }

        /*
         * インターネット模試判定＆インターネットバンザイは、初期表示時のソート処理を
         * JSP(JavaScript)で行うため、コメントアウトする。
         * ただし、このモジュールをKei-Naviにマージする場合はソート処理が必要となるため、注意が必要である。
         */
        //if (scoreBean.isTargetMarkExam()) {
        //    /* 対象模試がマークなら、センタ評価順にソートする */
        //    Collections.sort(judgedList, new JudgementSorterV2().new SortByCenterScoreRank());
        //} else {
        //    /* 対象模試が記述なら、二次評価順にソートする */
        //   Collections.sort(judgedList, new JudgementSorterV2().new SortBySecondScoreRank());
        //}

        /* 外部に公開する判定結果リストを設定する */
        recordSet = judgedList;
    }

    /**
     * 地区(県コード)による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictDistrict(List judgeTargetList) {
        Iterator iteS = judgeTargetList.iterator();
        while (iteS.hasNext()) {
            UnivData u = (UnivData) iteS.next();
            boolean rmFlg = true;
            for (int i = 0; i < districtProviso.length; i++) {
                if (u.getPrefCd().equals(districtProviso[i])) {
                    rmFlg = false;
                    break;
                }
            }
            if (rmFlg) {
                iteS.remove();
            }
        }
    }

    /**
     * 中系統による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictMStemma(List judgeTargetList) {
        Iterator iteS = judgeTargetList.iterator();
        while (iteS.hasNext()) {
            UnivData u = (UnivData) iteS.next();
            boolean rmFlg = true;
            if (u.getMStemmaList() != null && u.getMStemmaList().size() > 0) {
                Iterator iteMS = u.getMStemmaList().iterator();
                while (iteMS.hasNext() && rmFlg) {
                    String[] mStemma = (String[]) iteMS.next();
                    for (int i = 0; i < stemmaProviso.length; i++) {
                        if (mStemma != null && mStemma[0] != null && mStemma[0].equals(stemmaProviso[i])) {
                            rmFlg = false;
                            break;
                        }
                    }
                }
            }
            if (rmFlg) {
                iteS.remove();
            }
        }
    }

    /**
     * 分野による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictRegion(List judgeTargetList) {
        Iterator iteS = judgeTargetList.iterator();
        while (iteS.hasNext()) {
            UnivData u = (UnivData) iteS.next();
            boolean rmFlg = true;
            if (u.getRegionList() != null && u.getRegionList().size() > 0) {
                Iterator iteMR = u.getRegionList().iterator();
                while (iteMR.hasNext() && rmFlg) {
                    String[] regionCd = (String[]) iteMR.next();
                    for (int i = 0; i < stemmaProviso.length; i++) {
                        if (regionCd != null && regionCd[0] != null && regionCd[0].equals(stemmaProviso[i])) {
                            rmFlg = false;
                            break;
                        }
                    }
                }
            }
            if (rmFlg) {
                iteS.remove();
            }
        }
    }

    /**
     * 学校区分による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictSchoolDiv(List judgeTargetList) {
        Iterator iteB = judgeTargetList.iterator();
        while (iteB.hasNext()) {
            UnivData u = (UnivData) iteB.next();
            boolean rmFlg = true;
            for (int i = 0; i < schoolDivProviso.length; i++) {
                if (schoolDivProviso[i].equals("1")) {
                    /* 国公立大前期 */
                    if ((u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_BFR)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("2")) {
                    /* 国公立大後期 */
                    if ((u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_AFT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("3")) {
                    /* 国公立大中期・その他 */
                    if ((u.getUnivDivCd().equals("01") || u.getUnivDivCd().equals("02")) && (u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_MID) || u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_NEW))) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("4")) {
                    /* 私立大一般 */
                    if (u.getUnivDivCd().equals("03") && !u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("5")) {
                    /* 私立大センター利用 */
                    if (u.getUnivDivCd().equals("03") && u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("6")) {
                    /* 国公立短大一般 */
                    if (u.getUnivDivCd().equals("04") && !u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("7")) {
                    /* 国公立短大センター利用 */
                    if (u.getUnivDivCd().equals("04") && u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("8")) {
                    /* 私立短大一般 */
                    if (u.getUnivDivCd().equals("05") && !u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("9")) {
                    /* 私立短大センター利用 */
                    if (u.getUnivDivCd().equals("05") && u.getSchedule().equals(JudgementConstants.Univ.SCHEDULE_CNT)) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("10")) {
                    /* 文部科学省所管外 */
                    if (u.getUnivDivCd().equals("06") || u.getUnivDivCd().equals("07")) {
                        rmFlg = false;
                        break;
                    }
                } else if (schoolDivProviso[i].equals("11")) {
                    /* 専門学校 */
                    if (u.getUnivDivCd().equals("08") || u.getUnivDivCd().equals("09")) {
                        rmFlg = false;
                        break;
                    }
                }
            }
            if (rmFlg) {
                iteB.remove();
            }
        }
    }

    /**
     * 入試日による絞り込みを実行する。
     *
     * @param judgeTargetList 判定対象大学リスト
     * @throws Exception 同一日付チェック時例外
     */
    private void restrictExamDate(List list) throws Exception {

        final List dates = getDateList(Integer.parseInt(startMonth), Integer.parseInt(startDate), Integer.parseInt(endMonth), Integer.parseInt(endDate));

        for (Iterator ite = list.iterator(); ite.hasNext();) {

            UnivData univ = (UnivData) ite.next();

            /* 削除対象フラグを初期化する */
            boolean shouldRemove = true;

            for (Iterator ite2 = dates.iterator(); ite2.hasNext();) {
                Date date = (Date) ite2.next();
                /* 日付重複がひとつでも見つかれば削除しない */
                if (univ.isDateMatched(date)) {
                    shouldRemove = false;
                    break;
                }
            }

            /* 削除対象となった大学をリストから除去する */
            if (shouldRemove) {
                ite.remove();
            }
        }
    }

    /**
     * 指定した期間の日付リストを返します。
     *
     * @param month1 開始月
     * @param date1 開始日
     * @param month2 終了月
     * @param date2 終了日
     * @return 日付リスト
     */
    private List getDateList(int month1, int date1, int month2, int date2) {

        Calendar today = Calendar.getInstance();

        /* カレンダーに指定の日付を設定する */
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.clear();
        calendar2.clear();

        /* 年は今年を使う */
        int year1 = today.get(Calendar.YEAR);

        /* 1月〜3月は翌年として扱う */
        if (month1 < 4 && month1 > 0) {
            year1++;
        }

        /* 年は今年を使う */
        int year2 = today.get(Calendar.YEAR);

        /* 1月〜3月は翌年として扱う */
        if (month2 < 4 && month2 > 0) {
            year2++;
        }

        calendar1.set(year1, month1 - 1, date1);
        calendar2.set(year2, month2 - 1, date2);

        List dateList = new ArrayList();
        do {
            /* リストに日付を追加する */
            dateList.add(calendar1.getTime());
            /* 開始日に1日加算する */
            calendar1.add(Calendar.DAY_OF_YEAR, 1);
        } while (!calendar1.getTime().after(calendar2.getTime()));

        return dateList;
    }

    /**
     * 対象外条件による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictOutOfTarget(List judgeTargetList) {
        boolean[] newOutOfTP = new boolean[] { false, false, false };
        for (int i = 0; i < outOfTergetProviso.length; i++) {
            if (!outOfTergetProviso[i].trim().equals("")) {
                newOutOfTP[Integer.parseInt(outOfTergetProviso[i]) - 1] = true;
            }
        }
        Iterator iteS = judgeTargetList.iterator();
        while (iteS.hasNext()) {
            UnivData u = (UnivData) iteS.next();
            if (u.getFlag_women() == 2 && newOutOfTP[0]) {
                /* 女子大を除く */
                iteS.remove();
            } else if (u.getFlag_night() == 2 && newOutOfTP[1]) {
                /* 二部を除く */
                iteS.remove();
            } else if (u.getFlag_night() == 3 && newOutOfTP[2]) {
                /* 夜間を除く */
                iteS.remove();
            }
        }
    }

    /**
     * 配点比による絞り込みを実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     */
    private void restrictAllotPointRate(List judgeTargetList) {

        boolean centerFlg = allotPntRatio.equals("1");
        boolean halfFlg = allotPntRatio.equals("2");
        boolean secondFlg = allotPntRatio.equals("3");

        for (Iterator univIte = judgeTargetList.iterator(); univIte.hasNext();) {
            UnivData univ = (UnivData) univIte.next();

            /* 削除対象フラグを初期化する */
            boolean shouldRemove = true;

            /* 配点比の検索条件を満たす枝番を探す */
            for (Iterator detailIte = univ.getDetailHash().values().iterator(); detailIte.hasNext();) {
                UnivDetail detail = (UnivDetail) detailIte.next();
                double centerRate = calcCenterRate(detail);
                if (centerRate >= 65) {
                    if (centerFlg) {
                        shouldRemove = false;
                        break;
                    }
                } else if (centerRate > 35) {
                    if (halfFlg) {
                        shouldRemove = false;
                        break;
                    }
                } else if (centerRate >= 0) {
                    if (secondFlg) {
                        shouldRemove = false;
                        break;
                    }
                }
            }

            /* 削除対象となった大学をリストから除去する */
            if (shouldRemove) {
                univIte.remove();
            }
        }
    }

    /**
     * センタ配点比を計算します。
     *
     * @param detail 大学詳細データ
     * @return センタ配点比
     */
    private double calcCenterRate(UnivDetail detail) {
        int cAllot = detail.getCenter().getAllotPntRateAll();
        int sAllot = detail.getSecond().getAllotPntRateAll();
        if (cAllot == Detail.ALLOT_POINT_RATE_ALL_NA && sAllot == Detail.ALLOT_POINT_RATE_ALL_NA) {
            return -1;
        } else if (cAllot == Detail.ALLOT_POINT_RATE_ALL_NA || cAllot == 0) {
            return 0;
        } else if (sAllot == Detail.ALLOT_POINT_RATE_ALL_NA || sAllot == 0) {
            return 100;
        } else {
            return (double) cAllot / (cAllot + sAllot) * 100;
        }
    }

    /**
     * 二次科目による絞り込み＆判定を実行します。
     *
     * @param judgeTargetList 判定対象大学リスト
     * @param impose2ndFlg 二次試験課しフラグ
     * @param imposeSubFlg 科目課しフラグ
     * @return 判定結果リスト
     * @throws Exception 判定実行時例外
     */
    private List restrictImpose(List judgeTargetList, boolean impose2ndFlg, boolean imposeSubFlg) throws Exception {

        /* 判定結果リストを初期化する */
        List judgedList = new ArrayList(MAX_RECORD_NUM);

        /* 判定処理マネージャを初期化する */
        JudgementManager judge = new JudgementManager(scoreBean, precedeCenterPre);

        /* 二次科目検索と判定を1件ずつ行う */
        Iterator it = judgeTargetList.listIterator();
        long st = System.currentTimeMillis();
        while (it.hasNext()) {
            UnivData univ = (UnivData) it.next();
            ArrayList branch = querySecondSubject((Univ) univ, impose2ndFlg, imposeSubFlg);
            if (!branch.isEmpty()) {
                judge.setUniv(univ);
                judge.setBranch(branch);
                judge.execute();
                Judgement result = judge.getResult();
                /* 評価範囲による絞り込みを実行する */
                if (raingProviso == null || raingProviso.length == 0 || restrictJudgement(result)) {
                    judgedList.add(judge.getResult());
                    if (judgedList.size() == MAX_RECORD_NUM) {
                        /* 判定結果最大件数に達したらここまで */
                        break;
                    }
                }
            }
        }
        long ed = System.currentTimeMillis();
        if (logger.getLevel() != null) {
            logger.info(String.format("%d,%d", judgeTargetList.size(), ed - st));
        }

        return judgedList;
    }

    /**
     * 二次科目チェック処理(二次科目検索)を行います。
     *
     * @param univ 大学インスタンス
     * @param impose2ndFlg 二次試験課しフラグ
     * @param imposeSubFlg 科目課しフラグ
     * @return 二次科目検索条件にマッチする枝番リスト
     * @throws Exception 二次科目チェック時例外
     */
    private ArrayList querySecondSubject(Univ univ, boolean impose2ndFlg, boolean imposeSubFlg) throws Exception {

        SubjectStateBean state = new SubjectStateBean(univ);

        if (!impose2ndFlg) {
            /* 二次試験なし */
            state.setNotImposing2nd(true);
        } else {
            /* 二次試験あり*/
            if (imposeSub != null) {
                /* 課し科目条件を設定する */
                setSubjects(state);
            }
            /* 科目課しフラグを設定する */
            state.setImposingSubject(imposeSubFlg);
        }

        state.execute();

        //採用された枝番を設定
        return (ArrayList) state.getBranches();
    }

    /**
     * 課し科目条件を設定します。
     *
     * @param state {@link SubjectStateBean}
     */
    private void setSubjects(SubjectStateBean state) {
        for (int i = 0; i < imposeSub.length; i++) {
            if (imposeSub[i].equals("physics")) {
                state.allow_physics();
            } else if (imposeSub[i].equals("chemistry")) {
                state.allow_chemistry();
            } else if (imposeSub[i].equals("biology")) {
                state.allow_biology();
            } else if (imposeSub[i].equals("earthScience")) {
                state.allow_earth();
            } else if (imposeSub[i].equals("physicsBasic")) {
                state.allow_physicsBasic();
            } else if (imposeSub[i].equals("chemistryBasic")) {
                state.allow_chemistryBasic();
            } else if (imposeSub[i].equals("biologyBasic")) {
                state.allow_biologyBasic();
            } else if (imposeSub[i].equals("earthScienceBasic")) {
                state.allow_earthBasic();
            } else if (imposeSub[i].equals("math1")) {
                state.allow_mathI();
            } else if (imposeSub[i].equals("math1A")) {
                state.allow_mathIA();
            } else if (imposeSub[i].equals("math2A")) {
                state.allow_mathIIA();
            } else if (imposeSub[i].equals("math2B")) {
                state.allow_mathIIB();
            } else if (imposeSub[i].equals("math3B")) {
                state.allow_mathIII();
            } else if (imposeSub[i].equals("japaneseLit")) {
                state.allow_japanese1();
            } else if (imposeSub[i].equals("jClassics")) {
                state.allow_japanese2();
            } else if (imposeSub[i].equals("japanese")) {
                state.allow_japanese3();
            } else if (imposeSub[i].equals("english")) {
                state.allow_english();
            } else if (imposeSub[i].equals("jHistory")) {
                state.allow_jHistroy();
            } else if (imposeSub[i].equals("wHistory")) {
                state.allow_wHistroy();
            } else if (imposeSub[i].equals("geography")) {
                state.allow_geography();
            } else if (imposeSub[i].equals("politics")) {
                state.allow_politics();
            } else if (imposeSub[i].equals("comprehensive")) {
                state.allow_comprehensive();
            } else if (imposeSub[i].equals("interview")) {
                state.allow_interview();
            } else if (imposeSub[i].equals("performance")) {
                state.allow_performance();
            } else if (imposeSub[i].equals("essay")) {
                state.allow_essay();
            } else if (imposeSub[i].equals("ethic")) {
                state.allow_ethic();
                // TODO:ここに英資を追加
            } else {
                throw new RuntimeException("存在しない科目です。" + imposeSub[i]);
            }
        }
    }

    /**
     * 評価範囲による絞り込みを実行します。
     *
     * @param jb 判定結果
     * @return 判定結果が評価範囲による検索条件にマッチするならtrue
     */
    private boolean restrictJudgement(Judgement jb) {

        if ("banzai".equals(system)) {
            /* センタまたは二次・私大の場合 */
            if (raingDiv.equals("1") || raingDiv.equals("3")) {
                if (jb.getCGhJudgement() == 0 && matchRatingProviso(jb.getCJudgement())) {
                    return true;
                }
            }
        } else {
            /* 総合の場合 */
            if (raingDiv.equals("2") || raingDiv.equals("3")) {
                if (jb.getTGhJudgement() == 0 && matchRatingProviso(jb.getTJudgement())) {
                    return true;
                }
            }

            /* センタまたは二次・私大の場合 */
            if (raingDiv.equals("1") || raingDiv.equals("3")) {
                if (jb.getCGhJudgement() == 0 && matchRatingProviso(jb.getCJudgement())) {
                    return true;
                }
                if (jb.getSGhJudgement() == 0 && matchRatingProviso(jb.getSJudgement())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 画面で選択した評価とマッチするかを調べます。
     *
     * @param judgement 評価
     * @return 画面で選択した評価とマッチするならtrue
     */
    private boolean matchRatingProviso(int judgement) {
        if (judgement > 0) {
            for (int i = 0; i < raingProviso.length; i++) {
                if (raingProviso[i].equals("1")) {
                    /* A判定 */
                    if (judgement == JudgementConstants.JUDGE_A) {
                        return true;
                    }
                } else if (raingProviso[i].equals("2")) {
                    /* B判定 */
                    if (judgement == JudgementConstants.JUDGE_B) {
                        return true;
                    }
                } else if (raingProviso[i].equals("3")) {
                    /* C判定 */
                    if (judgement == JudgementConstants.JUDGE_C) {
                        return true;
                    }
                } else if (raingProviso[i].equals("4")) {
                    /* D判定 */
                    if (judgement == JudgementConstants.JUDGE_D) {
                        return true;
                    }
                } else if (raingProviso[i].equals("5")) {
                    /* E判定 */
                    if (judgement == JudgementConstants.JUDGE_E) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * センタープレでの成績を差し替えを有効化する。
     */
    public void setPrecedeCenterPre() {
        precedeCenterPre = true;
    }

    /**
     * 判定結果リストを返します。
     *
     * @return 判定結果リスト
     */
    public List getRecordSet() {
        return recordSet;
    }

    /**
     * 大学インスタンスリストを設定します。
     *
     * @param list 大学インスタンスリスト
     */
    public void setUnivAllList(List list) {
        univAllList = list;
    }

    /**
     * 成績データを設定します。
     *
     * @param bean 成績データ
     */
    public void setScoreBean(Score bean) {
        scoreBean = bean;
    }

    /**
     * 県コードの配列を設定します。
     *
     * @param strings 県コードの配列
     */
    public void setDistrictProviso(String[] strings) {
        districtProviso = strings;
    }

    /**
     * 系統区分を設定します。
     *
     * @param string 系統区分
     */
    public void setSearchStemmaDiv(String string) {
        searchStemmaDiv = string;
    }

    /**
     * 中系統コードまたは分野コードの配列を設定します。
     *
     * @param 中系統コードまたは分野コードの配列
     */
    public void setStemmaProviso(String[] strings) {
        stemmaProviso = strings;
    }

    /**
     * 二次・独自試験を課さないフラグを設定します。
     *
     * @param string 二次・独自試験を課さないフラグ
     */
    public void setDescImposeDiv(String string) {
        descImposeDiv = string;
    }

    /**
     * 試験科目課し区分を設定します。
     *
     * @param string 試験科目課し区分
     */
    public void setSubImposeDiv(String string) {
        subImposeDiv = string;
    }

    /**
     * 課し試験科目を設定します。
     *
     * @param strings 課し試験科目
     */
    public void setImposeSub(String[] strings) {
        imposeSub = strings;
    }

    /**
     * 学校区分の配列を設定します。
     *
     * @param strings 学校区分の配列
     */
    public void setSchoolDivProviso(String[] strings) {
        schoolDivProviso = strings;
    }

    /**
     * 入試日条件有効フラグを設定します。
     *
     * @param 入試日条件有効フラグ
     */
    public void setDateSearchDiv(String string) {
        dateSearchDiv = string;
    }

    /**
     * 入試日条件開始月を設定します。
     *
     * @param string 入試日条件開始月
     */
    public void setStartMonth(String string) {
        startMonth = string;
    }

    /**
     * 入試日条件開始日を設定します。
     *
     * @param string 入試日条件開始日
     */
    public void setStartDate(String string) {
        startDate = string;
    }

    /**
     * 入試日条件終了月を設定します。
     *
     * @param string 入試日条件終了月
     */
    public void setEndMonth(String string) {
        endMonth = string;
    }

    /**
     * 入試日条件終了日を設定します。
     *
     * @param string 入試日条件終了日
     */
    public void setEndDate(String string) {
        endDate = string;
    }

    /**
     * 評価試験区分を設定します。
     *
     * @param string 評価試験区分
     */
    public void setRaingDiv(String string) {
        raingDiv = string;
    }

    /**
     * 評価範囲を設定します。
     *
     * @param strings 評価範囲
     */
    public void setRaingProviso(String[] strings) {
        raingProviso = strings;
    }

    /**
     * 対象外区分を設定します。
     *
     * @param strings 対象外区分
     */
    public void setOutOfTergetProviso(String[] strings) {
        outOfTergetProviso = strings;
    }

    /**
     * 配点比率をセットします。
     *
     * @param allotPntRatio 配点比率
     */
    public void setAllotPntRatio(String allotPntRatio) {
        this.allotPntRatio = allotPntRatio;
    }

    /**
     * systemを返します。
     *
     * @return system
     */
    public String getSystem() {
        return system;
    }

    /**
     * systemを設定します。
     *
     * @param system system
     */
    public void setSystem(String system) {
        this.system = system;
    }

}

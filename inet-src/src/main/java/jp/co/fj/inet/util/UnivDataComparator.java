package jp.co.fj.inet.util;

import java.util.Comparator;

import jp.co.fj.kawaijuku.judgement.data.UnivData;

import org.apache.commons.lang.StringUtils;

/**
 *
 * {@link UnivData}のComparatorです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class UnivDataComparator implements Comparator {

    /**
     * {@inheritDoc}
     */
    public int compare(Object o1, Object o2) {
        return compare((UnivData) o1, (UnivData) o2);
    }

    /**
     * {@link UnivData}を比較します。
     *
     * @param d1 {@link UnivData}1
     * @param d2 {@link UnivData}2
     * @return 比較結果
     */
    protected abstract int compare(UnivData d1, UnivData d2);

    /**
     * 文字列を比較します。
     *
     * @param o1 文字列1
     * @param o2 文字列2
     * @return 比較結果
     */
    protected final int compare(String o1, String o2) {
        String s1 = StringUtils.defaultString(o1);
        String s2 = StringUtils.defaultString(o2);
        return s1.compareTo(s2);
    }

}

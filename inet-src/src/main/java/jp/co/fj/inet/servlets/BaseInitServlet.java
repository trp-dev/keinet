package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import jp.co.fj.inet.data.UnivMasterData;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.keinavi.util.log.KNAccessLog;
import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.log4j.Logger;

/**
 *
 * システム初期化サーブレットの基底クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
abstract public class BaseInitServlet extends HttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 7042909732830881639L;
    protected static final Logger logger = Logger.getLogger("accesslog");

    /**
     * {@inheritDoc}
     */
    public void init() throws ServletException {

        super.init();

        final ServletContext context = getServletContext();

        /* 大学マスタを別スレッドでロードする */
        new Thread(new Runnable() {
            public void run() {
                try {
                    UnivMasterData data = loadUnivMaster();
                    context.setAttribute(data.getClass().getName(), data);
                    context.setAttribute("q1", data.getJpnDescQ1());
                    context.setAttribute("q2", data.getJpnDescQ2());
                    context.setAttribute("q3", data.getJpnDescQ3());
                    context.setAttribute("q4", data.getJpnDescTotal());
                    context.setAttribute("jpnDesc", data.getJpnDescTotalEval());
                    /*context.setAttribute("engCert", data.getEngCert());
                    context.setAttribute("engCertDetail", data.getEngCertDetail());
                    context.setAttribute("cefrInfo", data.getCefrInfo());*/
                    context.setAttribute(ExamSubjectBean.SESSION_KEY, data.getExamSubject());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        context.setAttribute("message", getAllMessage());
        /* ログを出力する */
        KNAccessLog.lv1(null, null, null, null, null, "INITIALIZED");
        /*LoggerUtil.initMDC();
        logger.info("INITIALIZED");
        LoggerUtil.destoryMDC();*/
    }

    private String getAllMessage() {
        StringBuffer result = new StringBuffer();
        Map msgs;
        try {
            msgs = MessageLoader.getInstance().getAllMessage();
            for (Object key : msgs.keySet()) {
                if (result.length() > 0) {
                    result.append(";");
                }
                result.append(String.format("%s:%s", key, msgs.get(key)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    };

    /**
     * {@inheritDoc}
     */
    public void destroy() {

        super.destroy();

        /* ログを出力する */
        KNAccessLog.lv1(null, null, null, null, null, "DESTROYED");
        /*MDC.put("code", "-");
        logger.info("DESTROYED");
        MDC.remove("code");*/
    }

    /**
     * 大学マスタをロードします。
     *
     * @return {@link UnivMasterData}
     */
    abstract protected UnivMasterData loadUnivMaster() throws Exception;

}

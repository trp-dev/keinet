package jp.co.fj.inet.beans;

import java.io.Serializable;

import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;

/**
 *
 * 判定結果一覧画面の判定結果1件を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J204DetailBean implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 5354857496013008285L;

    /** センタボーダ */
    private final String borderPointStr;

    /** ランク名 */
    private final String rankName;

    /** ランク下限値 */
    private final String rankLLimitsStr;

    /** ランク上限値 */
    private final String rankHLimitsStr;

    /** センタ評価得点 */
    private final String c_scoreStr;

    /** 二次評価偏差値 */
    private final String s_scoreStr;

    /** センタ評価 */
    private final String CLetterJudgement;

    /** 二次評価 */
    private final String SLetterJudgement;

    /** 総合評価 */
    private final String TLetterJudgement;

    /** センタ配点比 */
    private final String CAllotPntRateAll;

    /** 二次配点比 */
    private final String SAllotPntRateAll;

    /** 総合ポイント */
    private final int t_score;

    /** ランク下限値 */
    private final double rankLLimits;

    /** 大学インスタンス */
    private final UnivData univ;

    /** 選択された枝番 */
    private final String selectedBranch;

    /**
     * コンストラクタです。
     *
     * @param listBean {@link DetailPageBean}
     */
    protected J204DetailBean(DetailPageBean detail) {
        this.borderPointStr = detail.getBorderPointStr();
        this.rankName = detail.getRankName();
        this.rankLLimitsStr = detail.getRankLLimitsStr();
        this.rankHLimitsStr = detail.getRankHLimitsStr();
        this.c_scoreStr = detail.getC_scoreStr();
        this.s_scoreStr = detail.getS_scoreStr();
        this.CLetterJudgement = detail.getCLetterJudgement();
        this.SLetterJudgement = detail.getSLetterJudgement();
        this.CAllotPntRateAll = detail.getCAllotPntRateAll();
        this.SAllotPntRateAll = detail.getSAllotPntRateAll();
        this.TLetterJudgement = detail.getEvalTotal().getLetter();
        this.t_score = detail.getT_score();
        this.rankLLimits = detail.getRankLLimits();
        this.univ = (UnivData) detail.getBean().getUniv();
        this.selectedBranch = detail.selectedBranch;
    }

    /**
     * 大学ユニークキーを返します。
     *
     * @return 大学ユニークキー
     */
    public String getUniqueKey() {
        return univ.getUniqueKey();
    }

    /**
     * 大学コードを返します。
     *
     * @return 大学コード
     */
    public String getUnivCd() {
        return univ.getUnivCd();
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univ.getUnivName();
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getDeptName() {
        return univ.getDeptName();
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getSubName() {
        return univ.getSubName();
    }

    /**
     * センタボーダを返します。
     *
     * @return センタボーダ
     */
    public String getBorderPointStr() {
        return borderPointStr;
    }

    /**
     * ランク名を返します。
     *
     * @return ランク名
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * ランク下限値を返します。
     *
     * @return ランク下限値
     */
    public String getRankLLimitsStr() {
        return rankLLimitsStr;
    }

    /**
     * ランク上限値を返します。
     *
     * @return ランク上限値
     */
    public String getRankHLimitsStr() {
        return rankHLimitsStr;
    }

    /**
     * センタ評価得点を返します。
     *
     * @return センタ評価得点
     */
    public String getC_scoreStr() {
        return c_scoreStr;
    }

    /**
     * 二次評価偏差値を返します。
     *
     * @return 二次評価偏差値
     */
    public String getS_scoreStr() {
        return s_scoreStr;
    }

    /**
     * センタ評価を返します。
     *
     * @return センタ評価
     */
    public String getCLetterJudgement() {
        return CLetterJudgement;
    }

    /**
     * 二次評価を返します。
     *
     * @return 二次評価
     */
    public String getSLetterJudgement() {
        return SLetterJudgement;
    }

    /**
     * 総合評価を返します。
     *
     * @return 総合評価
     */
    public String getTLetterJudgement() {
        return TLetterJudgement;
    }

    /**
     * センタ配点比を返します。
     *
     * @return センタ配点比
     */
    public String getCAllotPntRateAll() {
        return CAllotPntRateAll;
    }

    /**
     * 二次配点比を返します。
     *
     * @return 二次配点比
     */
    public String getSAllotPntRateAll() {
        return SAllotPntRateAll;
    }

    /**
     * 大学名カナを返します。
     *
     * @return 大学名カナ
     */
    public String getUnivNameKana() {
        return univ.getUnivNameKana();
    }

    /**
     * 学部コードを返します。
     *
     * @return 学部コード
     */
    public String getFacultyCd() {
        return univ.getFacultyCd();
    }

    /**
     * 学科ソートキーを返します。
     *
     * @return 学科ソートキー
     */
    public String getDeptSortKey() {
        return univ.getDeptSortKey();
    }

    /**
     * 日程コードを返します。
     *
     * @return 日程コード
     */
    public String getScheduleCd() {
        return univ.getSchedule();
    }

    /**
     * センタボーダ得点率を返します。
     *
     * @return センタボーダ得点率
     */
    public double getCenScoreRate() {
        return univ.getCenScoreRate();
    }

    /**
     * 総合ポイントを返します。
     *
     * @return 総合ポイント
     */
    public int getT_score() {
        return t_score;
    }

    /**
     * ランク下限値を返します。
     *
     * @return ランク下限値
     */
    public double getRankLLimits() {
        return rankLLimits;
    }

    /**
     * 大学インスタンスを返します。
     *
     * @return 大学インスタンス
     */
    public UnivData getUniv() {
        return univ;
    }

    /**
     * 大学コード表名を返します。
     *
     * @return 大学コード表名
     */
    public String getUnivKanjiName() {
        return univ.getUnivKanjiName();
    }

    /**
     * 大学区分を返します。
     *
     * @return 大学区分
     */
    public String getUnivDivCd() {
        return univ.getUnivDivCd();
    }

    /**
     * 受験校本部県コードを返します。
     *
     * @return 受験校本部県コード
     */
    public String getPrefCd() {
        return univ.getPrefCd();
    }

    /**
     * カナ付50音順番号を返します。
     *
     * @return カナ付50音順番号
     */
    public String getKanaNum() {
        return univ.getKanaNum();
    }

    /**
     * 大学夜間部区分を返します。
     *
     * @return 大学夜間部区分
     */
    public String getUniNightDiv() {
        return Integer.toString(univ.getFlag_night());
    }

    /**
     * 学部内容コードを返します。
     *
     * @return 学部内容コード
     */
    public String getFacultyConCd() {
        return univ.getFacultyConCd();
    }

    /**
     * 学科通番を返します。
     *
     * @return 学科通番
     */
    public String getDeptSerialNo() {
        return univ.getDeptSerialNo();
    }

    /**
     * 日程方式を返します。
     *
     * @return 日程方式
     */
    public String getScheduleSys() {
        return univ.getScheduleSys();
    }

    /**
     * 日程方式枝番を返します。
     *
     * @return 日程方式枝番
     */
    public String getScheduleSysBranchCd() {
        return univ.getScheduleSysBranchCd();
    }

    /**
     * 学科カナ名を返します。
     *
     * @return 学科カナ名
     */
    public String getDeptNameKana() {
        return univ.getSubNameKana();
    }

    /**
     * 学科コードを返します。
     *
     * @return 学科コード
     */
    public String getDeptCd() {
        return univ.getDeptCd();
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivFullName() {
        return univCd2TypeString(this.getUnivCd()) + this.getUnivName() + " " + this.getDeptName() + " " + this.getSubName();
    }

    /**
     * 共通テストのボーダー得点（率）を返します。
     *
     * @return ボーダー得点（率）
     */
    public String getCenterScore() {
        String result = "";
        if (!"".equals(this.borderPointStr) && !"0".equals(this.borderPointStr)) {
            result = this.borderPointStr;
            result += "(" + String.valueOf(this.getCenScoreRate()) + ")";
        }

        return result;
    }

    /**
     * ランクを返します。
     *
     * @return ランク
     */
    public String getRank() {
        if (" -- ".equals(this.rankName) && this.univ.getRank() != -99) {
            return " -- ";
        }
        if (this.univ.getRank() == -99) {
            return " BF ";
        }
        if (!" -- ".equals(this.rankName) && this.univ.getRank() != -99) {
            return this.rankLLimitsStr + "-" + StringUtil.nvl(this.rankHLimitsStr, "　");
        }
        return "";
    }

    /**
     * 大学コードを大学種別文字列に変換します。
     *
     * @param univCd 大学コード
     * @return 大学種別
     */
    private String univCd2TypeString(final String univCd) {
        int value = Integer.parseInt(univCd);
        if (value >= 1000 && value <= 1499) {
            return "（国）";
        } else if (value >= 1500 && value <= 1899) {
            return "（公）";
        } else if (value >= 2000 && value <= 2999) {
            return "（私）";
        } else if (value >= 3000 && value <= 3499) {
            return "（国）";
        } else if (value >= 3500 && value <= 3999) {
            return "（公）";
        } else if (value >= 4000 && value <= 4999) {
            return "（私）";
        } else {
            return "（他）";
        }
    }

    /**
     * selectedBranchを返します。
     *
     * @return selectedBranch
     */
    public String getSelectedBranch() {
        return selectedBranch;
    }

}

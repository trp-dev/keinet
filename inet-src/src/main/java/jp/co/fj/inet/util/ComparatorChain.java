package jp.co.fj.inet.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 *
 * Comparatorチェーンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ComparatorChain implements Comparator {

    /** Comparatorリスト */
    private final List list = new ArrayList();

    /**
     * Comparatorをチェーンに追加します。
     *
     * @param c Comparator
     * @return 自分自身
     */
    public ComparatorChain add(Comparator c) {
        list.add(c);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public int compare(Object o1, Object o2) {
        for (Iterator ite = list.iterator(); ite.hasNext();) {
            int result = ((Comparator) ite.next()).compare(o1, o2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }

}

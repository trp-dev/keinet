package jp.co.fj.inet.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.fj.kawaijuku.judgement.beans.judge.Judgement;
import jp.co.fj.keinavi.beans.individual.judgement.DetailPageBean;

/**
 *
 * 画面用の判定結果一覧を作成・保持するBeanです。<br>
 * ※{@link jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean} をインターネット模試判定用に焼き直した。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J204ListBean implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = -5411812839191736370L;

    /** 判定結果リスト */
    private final List list;

    /**
     * コンストラクタです。
     *
     * @param list 判定結果リスト
     */
    public J204ListBean(List judgedList) {

        list = new ArrayList(judgedList.size());

        for (Iterator ite = judgedList.iterator(); ite.hasNext();) {
            DetailPageBean detail = judge2Detail((Judgement) ite.next());
            list.add(createJ204DetailBean(detail));
        }
    }

    /**
     * {@link Judgement} を {@link DetailPageBean} に変換します。
     *
     * @param judgement {@link Judgement}
     * @return {@link DetailPageBean}
     */
    public DetailPageBean judge2Detail(Judgement judgement) {
        try {
            DetailPageBean detail = createDetailPageBean(judgement);
            detail.execute();
            return detail;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@link DetailPageBean} を生成します。
     *
     * @param judgement {@link Judgement}
     * @return {@link DetailPageBean}
     */
    protected DetailPageBean createDetailPageBean(Judgement judgement) {
        return new JDetailPageBean(judgement);
    }

    /**
     * {@link J204DetailBean} を生成します。
     *
     * @param detail {@link DetailPageBean}
     * @return {@link J204DetailBean}
     */
    protected J204DetailBean createJ204DetailBean(DetailPageBean detail) {
        return new J204DetailBean(detail);
    }

    /*
     * 以下のメソッドはJSPから呼び出される。
     * getRecordSet()とgetRecordSetAll()は同じ機能だが、
     * JSPではどちらも利用しているため両方残しておく。
     */

    /**
     * 判定結果リストを返します。
     *
     * @return recordSet 判定結果リスト
     */
    public List getRecordSet() {
        return list;
    }

    /**
     * 判定結果リストを返します。
     *
     * @return recordSet 判定結果リスト
     */
    public List getRecordSetAll() {
        return list;
    }

    /**
     * 判定結果リストのサイズを返します。
     *
     * @return 判定結果リストのサイズ
     */
    public int getRecordCount() {
        return list.size();
    }

}

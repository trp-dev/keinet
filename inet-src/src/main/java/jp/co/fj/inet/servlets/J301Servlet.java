package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.inet.beans.J204ListBean;
import jp.co.fj.inet.data.UnivList;
import jp.co.fj.inet.forms.J204Form;
import jp.co.fj.inet.forms.J301Form;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 *
 * 志望大学一覧画面サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class J301Servlet extends DefaultHttpServlet implements JudgementListFactory {

    /** serialVersionUID */
    private static final long serialVersionUID = -7503897860356612408L;

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* HTTPセッション */
        HttpSession session = request.getSession(false);

        /* 志望大学リスト */
        UnivList univ = (UnivList) session.getAttribute(UnivList.SESSION_KEY);

        /* アクションフォーム */
        J301Form form = (J301Form) getActionForm(request, J301Form.class.getName());

        if ("j301".equals(getBackward(request)) && !"j204".equals(getForward(request))) {
            /* 転送元がJSPの場合 */

            /* アクションログを出力する */
            KNLog.Lv2(request.getRemoteAddr(), null, null, null, "j301:j301", 3005, null);
            /*MDC.put("form", "j301:j301");
            MDC.put("code", "3005");
            logger.info("-");*/

            /* 志望大学判定結果をセッションら取得する */
            J204ListBean bean = (J204ListBean) session.getAttribute("useListBean2");

            if ("1".equals(form.getMode())) {
                /* 削除モード */
                univ.remove(univ.indexOf(form.getUniqueKey()));
                for (Iterator ite = bean.getRecordSetAll().iterator(); ite.hasNext();) {
                    J204DetailBean d = (J204DetailBean) ite.next();
                    if (d.getUniqueKey().equals(form.getUniqueKey())) {
                        ite.remove();
                        break;
                    }
                }
            } else if ("2".equals(form.getMode())) {
                /* すべて削除モード */
                bean.getRecordSetAll().clear();
                univ.clear();
            }
        } else {
            /* 転送元がJSP以外の場合は、ソートキーを初期値にする */
            form.setSortKey("0");
        }

        if ("j301".equals(getForward(request))) {
            if (univ == null || univ.size() == 0 || univ.isJudged()) {
                /* 志望大学がないか判定済みならJSPへ */
                request.setAttribute("form", form);
                forward(request, response, JSP_J301);
            } else {
                /* そうでなければ判定処理をコールする */
                J204Form jform = new J204Form();
                jform.setUnivValue(univ.getUnivValue());
                Map<String, ExamSubjectBean> examSubject = (Map<String, ExamSubjectBean>) getServletContext().getAttribute(ExamSubjectBean.SESSION_KEY);
                String system = (String) getServletContext().getAttribute("system");
                new JudgeThread(session, jform, getUnivAll(), "useListBean2", this, examSubject, system).start();
                univ.setJudged(true); // 判定済みとする
                forward(request, response, JSP_JOnJudge);
            }
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * {@inheritDoc}
     */
    public J204ListBean createJudgementListBean(List judgedList) {
        return new J204ListBean(judgedList);
    }
}

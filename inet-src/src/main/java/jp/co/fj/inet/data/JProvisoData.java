package jp.co.fj.inet.data;

import jp.co.fj.keinavi.data.individual.ProvisoData;

/**
 *
 * 模試判定システムの {@link ProvisoData} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JProvisoData extends ProvisoData {

    /** 配点比率 */
    private String allotPntRatio;

    /**
     * 配点比率を返します。
     *
     * @return 配点比率
     */
    public String getAllotPntRatio() {
        return allotPntRatio;
    }

    /**
     * 配点比率をセットします。
     *
     * @param allotPntRatio 配点比率
     */
    public void setAllotPntRatio(String allotPntRatio) {
        this.allotPntRatio = allotPntRatio;
    }

}

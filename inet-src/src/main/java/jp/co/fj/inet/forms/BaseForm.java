package jp.co.fj.inet.forms;

import com.fjh.forms.ActionForm;

/**
 *
 * 基本アクションフォーム
 *
 * 2005.06.20	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public abstract class BaseForm extends ActionForm {

    /**
    *
    */
    private static final long serialVersionUID = 2556069493040256712L;
    private String scrollX;
    private String scrollY;

    /* (非 Javadoc)
     * @see com.fjh.forms.ActionForm#validate()
     */
    public void validate() {
    }

    public String getScrollX() {
        return this.scrollX;
    }

    public void setScrollX(String scrollX) {
        this.scrollX = scrollX;
    }

    public String getScrollY() {
        return this.scrollY;
    }

    public void setScrollY(String scrollY) {
        this.scrollY = scrollY;
    }
}

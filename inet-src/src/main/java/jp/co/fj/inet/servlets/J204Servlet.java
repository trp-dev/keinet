package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.inet.beans.J204ListBean;
import jp.co.fj.inet.data.UnivList;
import jp.co.fj.inet.forms.J204Form;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 *
 * 判定結果一覧サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class J204Servlet extends DefaultHttpServlet implements JudgementListFactory {

    /** serialVersionUID */
    private static final long serialVersionUID = -1017741401406206308L;

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if ("j204".equals(getForward(request))) {
            /* アクションフォーム */
            J204Form form = (J204Form) getActionForm(request, J204Form.class.getName());

            /* HTTPセッション */
            HttpSession session = request.getSession(false);

            if ("j204".equals(getBackward(request))) {
                /* 転送元がJSP */
                if ("add".equals(form.getButton()) && form.getUnivValue() != null) {
                    /*「志望大学一覧に追加」を押している場合 */

                    /* アクションログを出力する */
                    KNLog.Lv2(request.getRemoteAddr(), null, null, null, "j204:j204", 3004, null);
                    /*MDC.put("form", "j204:j204");
                    MDC.put("code", "3004");
                    logger.info("-");*/

                    /* 志望大学リストをセッションから取得する */
                    UnivList univ = (UnivList) session.getAttribute(UnivList.SESSION_KEY);

                    /* 志望大学リストがセッションになければ保存しておく */
                    if (univ == null) {
                        univ = new UnivList();
                        session.setAttribute(UnivList.SESSION_KEY, univ);
                    }

                    /* 判定結果をセッションから取得する */
                    J204ListBean bean1 = (J204ListBean) session.getAttribute("useListBean");

                    /* 志望大学判定結果をセッションら取得する */
                    J204ListBean bean2 = (J204ListBean) session.getAttribute("useListBean2");

                    /* 志望大学判定結果がセッションになければ保存しておく */
                    if (bean2 == null) {
                        bean2 = createJudgementListBean(Collections.EMPTY_LIST);
                        session.setAttribute("useListBean2", bean2);
                    }

                    /* 追加する */
                    Set set = new HashSet();
                    String[] array = form.getUnivValue();
                    for (int i = 0; i < array.length; i++) {
                        if (!univ.contains(array[i])) {
                            univ.add(array[i]);
                            set.add(array[i]);
                        }
                    }

                    /* 選択された志望大学をbean2へコピーする */
                    for (Iterator ite = bean1.getRecordSetAll().iterator(); ite.hasNext();) {
                        J204DetailBean d = (J204DetailBean) ite.next();
                        if (set.contains(d.getUniqueKey())) {
                            bean2.getRecordSetAll().add(d);
                        }
                    }

                    /* 登録フラグを設定する */
                    request.setAttribute("Registed", Boolean.TRUE);
                }

                request.setAttribute("form", form);

                forward(request, response, JSP_J204);
            } else if ("j301".equals(getBackward(request))) {
                request.setAttribute("form", form);

                forward(request, response, JSP_J204);
            } else {
                /* 転送元がJSP以外なら判定処理実行する */
                Map<String, ExamSubjectBean> examSubject = (Map<String, ExamSubjectBean>) getServletContext().getAttribute(ExamSubjectBean.SESSION_KEY);
                String system = (String) getServletContext().getAttribute("system");
                new JudgeThread(session, form, getUnivAll(), "useListBean", this, examSubject, system).start();
                forward(request, response, JSP_JOnJudge);
            }
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * {@inheritDoc}
     */
    public J204ListBean createJudgementListBean(List judgedList) {
        return new J204ListBean(judgedList);
    }

}

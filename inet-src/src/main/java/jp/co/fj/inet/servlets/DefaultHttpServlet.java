package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.data.LoginSession;
import jp.co.fj.inet.data.UnivMasterData;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.inet.interfaces.PageInterface;
import jp.co.fj.keinavi.interfaces.MessageInterface;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.log.KNAccessLog;
import jp.co.fj.keinavi.util.log.KNLog;
import jp.co.fj.keinavi.util.message.MessageLoader;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;

import com.fjh.db.DBManager;
import com.fjh.forms.ActionForm;
import com.fjh.servlets.DefaultServlet;

/**
 *
 * DefaultHttpServlet
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class DefaultHttpServlet extends DefaultServlet implements PageInterface, MessageInterface {

    /** serialVersionUID */
    private static final long serialVersionUID = -3137533050819273630L;
    protected static final Logger logger = Logger.getLogger("accesslog");

    /**
     * 大学インスタンスリストを返します。
     *
     * @return 大学インスタンスリスト
     */
    protected final ArrayList getUnivAll() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getUnivAll();
    }

    /**
     * 模試区分を返します。
     *
     * @return  模試区分
     */
    protected final String getExamDiv() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getExamDiv();
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    protected final Map getUnivInfoMap() {
        UnivMasterData data = getUnivMasterData();
        return data == null ? null : data.getUnivInfoMap();
    }

    /**
     * メモリからロードした大学マスタ情報を返します。
     *
     * @return {@link UnivMasterData}
     */
    private UnivMasterData getUnivMasterData() {
        return (UnivMasterData) getServletContext().getAttribute(UnivMasterData.class.getName());
    }

    /**
     * {@inheritDoc}
     */
    protected String[] getParameterValues(HttpServletRequest req, String strName) throws Exception {
        return req.getParameterValues(strName);
    }

    /**
     * {@inheritDoc}
     */
    public final void destroy() {
        super.destroy();
        try {
            DBManager.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 画面表示処理を実行します。
     *
     * @param request {@link HttpServletRequest}
     * @param response {@link HttpServletResponse}
     */
    protected abstract void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    /**
     * {@inheritDoc}
     */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        try {
            /* 脆弱性対応：X-Frame-Optionsレスポンスヘッダを出力する */
            response.setHeader("X-Frame-Options", "SAMEORIGIN");
            /* アクセスログを出力する */
            accessLog(request);
            if (isLogin(request)) {
                /* ログインチェックを通過すれば実行 */
                execute(request, response);
            }
        } catch (KeiNetException e) {
            /* Kei-Navi例外の場合 */
            KNLog.Err(request.getRemoteAddr(), "", "", "", e.getErrorCode(), e.getMessage(), "");
            /*MDC.put("ecode", e.getErrorCode());
            MDC.put("emsg", e.getMessage());
            logger.error("-");*/
            request.setAttribute("ex", e);
            /* エラー画面へ遷移する */
            forward(request, response, JSP_ERROR);
        } catch (final Throwable e) {
            /* それ以外のエラー */
            KNLog.Err(request.getRemoteAddr(), "", "", "", "999999999999", e.getMessage(), e);
            /*MDC.put("ecode", "999999999999");
            MDC.put("emsg", e.getMessage());
            logger.error(e);*/
            /* エラー画面へ遷移する */
            forward(request, response, JSP_ERROR);
        }
    }

    /**
     * アクセスログを出力します。
     *
     * @param request {@link HttpServletRequest}
     */
    protected final void accessLog(final HttpServletRequest request) {

        if (request.getAttribute("AccessLogFlag") != null) {
            /* ログ出力済みならここまで */
            return;
        }
        KNAccessLog.lv1(request.getRemoteAddr(), request.getRemoteHost(), null, createScreenId(request), "1");
        /*MDC.put("code", "1");
        logger.info("-");
        MDC.remove("code");*/

        /* ログ出力済みフラグを立てる */
        request.setAttribute("AccessLogFlag", Boolean.TRUE);
    }

    /**
     * アクションログ（Lv2ログ）を出力します。
     *
     * @param request {@link HttpServletRequest}
     * @param code 実行条件コード
     * @param info 補足情報
     */
    protected final void actionLog(final HttpServletRequest request, final String code, final String info) {
        KNAccessLog.lv2(request.getRemoteAddr(), request.getRemoteHost(), null, createScreenId(request), code, info);
        /*MDC.put("code", code);
        if (info == null) {
            logger.info("-");
        } else {
            logger.info(info);
        }*/
    }

    /**
     * アクションログ（Lv2ログ）を出力します。
     *
     * @param request {@link HttpServletRequest}
     * @param code 実行条件コード
     */
    protected final void actionLog(final HttpServletRequest request, final String code) {
        actionLog(request, code, null);
    }

    /**
     * 画面ID（転送元:転送先）を生成します。
     *
     * @param request {@link HttpServletRequest}
     * @return 画面ID
     */
    private String createScreenId(final HttpServletRequest request) {
        final String backward = getBackward(request);
        final String forward = getForward(request);
        final StringBuffer buff = new StringBuffer();
        if (backward != null) {
            buff.append(backward);
        }
        buff.append(":");
        if (forward != null) {
            buff.append(forward);
        }
        return buff.toString();
    }

    /**
     * {@inheritDoc}
     */
    protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * ログイン状態をチェック処理します。<br>
     * 転送先画面IDの有無もチェックします。
     *
     * @param request {@link HttpServletRequest}
     * @return ログイン状態ならtrue
     */
    protected boolean isLogin(HttpServletRequest request) throws ServletException, IOException {

        if (getForward(request) == null) {
            throw new KeiNetException("お気に入りからのアクセスがありました。", "1003", MessageLoader.getInstance().getMessage("j026a"));
        }

        HttpSession session = request.getSession(false);

        if (session == null) {
            throw createTimeoutException();
        } else {
            return session.getAttribute(LoginSession.SESSION_KEY) != null;
        }
    }

    /**
     * タイムアウト発生時の例外を生成します。
     *
     * @return {@link KeiNetException}
     */
    private KeiNetException createTimeoutException() throws IOException {
        return new KeiNetException("1002", MessageLoader.getInstance().getMessage("j027a")).Timeout();
    }

    /**
     * コネクションプールからコネクションを取得します。
     *
     * @param request {@link HttpServletRequest}
     * @return {@link Connection}
     */
    protected final Connection getConnectionPool(HttpServletRequest request) throws Exception {
        return DBManager.getConnectionPool(getDbKey());
    }

    /**
     * コネクションをコネクションプールに返却します。
     *
     * @param request {@link HttpServletRequest}
     * @param con {@link Connection}
     */
    protected final void releaseConnectionPool(HttpServletRequest request, Connection con) throws ServletException {
        if (con != null) {
            try {

                DBManager.releaseConnectionPool(getDbKey(), con);
            } catch (Exception e) {
                throw new ServletException(e);
            }
        }
    }

    /**
     * DBキーを返します。
     *
     * @return DBキー
     */
    private String getDbKey() throws Exception {
        return KNCommonProperty.getNDBSID();
    }

    /**
     * {@inheritDoc}
     */
    public final ActionForm getActionForm(HttpServletRequest request, String className) throws ServletException {
        try {
            return super.getActionForm(request, className);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    /**
     * エラーメッセージをセットします。
     *
     * @param request {@link HttpServletRequest}
     * @param e {@link KeiNetException}
     */
    protected final void setErrorMessage(HttpServletRequest request, KeiNetException e) {
        KNLog.Err(request.getRemoteAddr(), "", "", "", e.getErrorCode(), e.getMessage(), "");
        /*MDC.put("ecode", e.getErrorCode());
        MDC.put("emsg", e.getMessage());
        logger.error("-");*/
        request.setAttribute("ErrorCode", e.getErrorCode());
        request.setAttribute("ErrorMessage", e.getErrorMessage());
    }

    /**
     * 転送先の画面IDを返します。
     *
     * @return 転送先の画面ID
     */
    protected String getForward(HttpServletRequest request) {
        return (String) request.getParameter("forward");
    }

    /**
     * 転送元の画面IDを返します。
     *
     * @return 転送元の画面ID
     */
    protected final String getBackward(HttpServletRequest request) {
        return (String) request.getParameter("backward");
    }

    /**
     * 模試Beanを返します。
     *
     * @param request {@link HttpServletRequest}
     * @return {@link ExamBean}
     */
    protected final ExamBean getExamBean(final HttpServletRequest request) throws ServletException {
        HttpSession session = request.getSession(false);
        return (ExamBean) session.getAttribute(ExamBean.SESSION_KEY);
    }

    /**
     * {@inheritDoc}
     */
    public void forward(HttpServletRequest request, HttpServletResponse response, String forwardPage) throws ServletException, IOException {

        Properties props = (Properties) getServletContext().getAttribute(KNCommonProperty.class.getName());

        if (props != null && forwardPage.endsWith(".jsp")) {

            /* キャッシュサーバURLをリクエストスコープに設定する */
            String cacheUrl = props.getProperty("CACHE_SERVER_URL");
            String sslCacheUrl = props.getProperty("SSL_CACHE_SERVER_URL");
            if (cacheUrl != null && sslCacheUrl != null) {
                Boolean sslFlg = (Boolean) request.getSession().getAttribute("sslFlg");
                request.setAttribute("cacheServerUrl", BooleanUtils.toBoolean(sslFlg) ? sslCacheUrl : cacheUrl);
            }
            /* サーバIPアドレスをリクエストスコープに設定する */
            String sslSend = props.getProperty("SSLSEND");
            if (sslSend != null) {
                request.setAttribute("sslServerAddress", props.getProperty("SSLSEND"));
            }

            /* ロードバランサ用サーバIDをリクエストスコープに設定する */
            request.setAttribute("serverId", sslSend);

            /* トップ画面URLをリクエストスコープに設定する */
            request.setAttribute("topUrl", props.getProperty("TOP_URL"));
        }

        super.forward(request, response, forwardPage);
    }

}

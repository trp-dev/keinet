package jp.co.fj.inet.data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jp.co.fj.kawaijuku.judgement.beans.EngCertJpnDesc;
import jp.co.fj.kawaijuku.judgement.beans.ExamSubjectBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;

/**
 *
 * メモリからロードした大学マスタ情報を保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterData implements EngCertJpnDesc {

    /** 大学インスタンスリスト */
    private final ArrayList univAll;

    /** 模試区分 */
    private final String examDiv;

    /** 情報誌用科目データマップ */
    private final Map univInfoMap;

    /* CEFR情報マスタ */
    /*private final List<CefrBean> cefrInfo;*/

    /* 英語認定試験情報マスタ */
    /*private final List<EngPtBean> engCert;*/

    /* 英語認定試験詳細情報マスタ */
    /*private final List<EngPtDetailBean> engCertDetail;*/

    /* CEFR換算用スコア情報 */
    /*private final List<CefrConvScoreBean> cefrConvScore;*/

    /* 記述式総合評価マスタ */
    private final List<JpnDescTotalEvaluationBean> jpnDescTotalEval;

    /* 国語記述問１評価 */
    private final LinkedHashMap<String, String> jpnDescQ1 = new LinkedHashMap();
    /* 国語記述問２評価 */
    private final LinkedHashMap<String, String> jpnDescQ2 = new LinkedHashMap();
    /* 国語記述問３評価 */
    private final LinkedHashMap<String, String> jpnDescQ3 = new LinkedHashMap();
    /* 国語記述総合評価 */
    private final LinkedHashMap<String, String> jpnDescTotal = new LinkedHashMap();

    /* 模試科目マスタ */
    private final Map<String, ExamSubjectBean> examSubject;

    /**
     * コンストラクタです。
     *
     * @param univAll 大学インスタンスリスト
     * @param examDiv 模試区分
     * @param pubMap 公表用大学マスタマップ
     */
    public UnivMasterData(FactoryManager manager) {
        this.univAll = (ArrayList) manager.getDatas();
        this.examDiv = manager.getExamDiv();
        this.univInfoMap = manager.getUnivInfoMap();
        /*this.cefrInfo = manager.getCefrInfo();
        this.engCert = manager.getEngCert();
        this.engCertDetail = manager.getEngCertDetail();
        this.cefrConvScore = manager.getCefrConvScore();*/
        this.jpnDescTotalEval = manager.getJpnDescTotalEval();
        this.examSubject = manager.getExamSubject();

        jpnDescQ1.put("", "未選択");
        jpnDescQ2.put("", "未選択");
        jpnDescQ3.put("", "未選択");
        jpnDescTotal.put("", "未選択");
        /* 問１評価のユニーク値を取得 */
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ1Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ1.put(item, item);
        }
        /* 問２評価のユニーク値を取得 */
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ2Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ2.put(item, item);
        }
        /* 問３評価のユニーク値を取得 */
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ3Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ3.put(item, item);
        }
        /* 総合評価のユニーク値を取得 */
        for (String item : jpnDescTotalEval.stream().map(i -> i.getTotalEvaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescTotal.put(item, item);
        }
    }

    /**
     * 大学インスタンスリストを返します。
     *
     * @return 大学インスタンスリスト
     */
    public ArrayList getUnivAll() {
        return univAll;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    public Map getUnivInfoMap() {
        return univInfoMap;
    }

    /**
     * CEFR情報マスタリストを返します。
     *
     * @return CEFR情報マスタリスト
     */
    /*public List<CefrBean> getCefrInfo() {
        return cefrInfo;
    }*/

    /**
     * 英語認定試験情報マスタリストを返します。
     *
     * @return 英語認定試験情報マスタリスト
     */
    /*public List<EngPtBean> getEngCert() {
        return engCert;
    }*/

    /**
     * 英語認定試験詳細情報マスタリストを返します。
     *
     * @return 英語認定試験詳細情報マスタリスト
     */
    /*public List<EngPtDetailBean> getEngCertDetail() {
        return engCertDetail;
    }*/

    /**
     * CEFR換算用スコア情報を返します。
     *
     * @return CEFR換算用スコア情報
     */
    /*public List<CefrConvScoreBean> getCefrConvScore() {
        return cefrConvScore;
    }*/

    /**
     * 記述式総合評価マスタを返します。
     *
     * @return 記述式総合評価マスタ
     */
    public List<JpnDescTotalEvaluationBean> getJpnDescTotalEval() {
        return jpnDescTotalEval;
    }

    /**
     * jpnDescQ1を返します。
     *
     * @return jpnDescQ1
     */
    public LinkedHashMap<String, String> getJpnDescQ1() {
        return jpnDescQ1;
    }

    /**
     * jpnDescQ2を返します。
     *
     * @return jpnDescQ2
     */
    public LinkedHashMap<String, String> getJpnDescQ2() {
        return jpnDescQ2;
    }

    /**
     * jpnDescQ3を返します。
     *
     * @return jpnDescQ3
     */
    public LinkedHashMap<String, String> getJpnDescQ3() {
        return jpnDescQ3;
    }

    /**
     * jpnDescTotalを返します。
     *
     * @return jpnDescTotal
     */
    public LinkedHashMap<String, String> getJpnDescTotal() {
        return jpnDescTotal;
    }

    /*@Override
    public List<CefrBean> getCefrList() {
        return cefrInfo;
    }*/

    /*@Override
    public List<EngPtBean> getEngCertExamList() {
        return engCert;
    }*/

    /*@Override
    public List<EngPtDetailBean> getEngCertExamDetailList() {
        return engCertDetail;
    }*/

    /*@Override
    public List<CefrConvScoreBean> getCefrConvScoreList() {
        return cefrConvScore;
    }*/

    @Override
    public List<JpnDescTotalEvaluationBean> getJpnDescList() {
        return jpnDescTotalEval;
    }

    /**
     * examSubjectを返します。
     *
     * @return examSubject
     */
    public Map<String, ExamSubjectBean> getExamSubject() {
        return examSubject;
    }
}

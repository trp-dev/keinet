package jp.co.fj.inet.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.ExamBean;
import jp.co.fj.inet.data.LoginSession;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.inet.util.JCommonProperty;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * トップ画面サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J001Servlet extends DefaultLoginServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -7948269128560985537L;

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);

        boolean firstFlg = session == null || session.getAttribute(LoginSession.SESSION_KEY) == null;

        if (firstFlg) {
            /* セッションが無い場合は初回アクセス用の処理を行う */

            /* リファラチェックを行う */
            final String referer = JCommonProperty.getRefererUrl();
            if (!"".equals(referer)) {
                if (request.getHeader("REFERER") == null || !request.getHeader("REFERER").startsWith(referer)) {
                    throw new KeiNetException("REFERERチェックエラー: " + request.getHeader("REFERER"), "1004", MessageLoader.getInstance().getMessage("j026a"));
                }
            }

            /* セッションを初期化する */
            session = request.getSession(true);

            /* ログインセッションを保持する */
            session.setAttribute(LoginSession.SESSION_KEY, new LoginSession());

            /* トップページなのでキャッシュを無効にしておく */
            response.setHeader("Pragma", "No-cache");
            response.setHeader("Cache-Control", "no-cache");

            /* 模試データを作る */
            session.setAttribute(ExamBean.SESSION_KEY, createExamBean());
        }

        if (!"j101".equals(getForward(request)) && (firstFlg || "j001".equals(super.getForward(request)))) {
            /* 画面を表示する */
            forward(request, response, JSP_J001);
        } else {
            /* 処理を転送する */
            if (getUnivAll() == null || getUnivAll().size() == 0) {
                /* 大学マスタロード中なら、再表示する */
                setErrorMessage(request, new KeiNetException("大学データロード中エラー", "1005", MessageLoader.getInstance().getMessage("j022a")));
                forward(request, response, JSP_J001);
            } else {
                actionLog(request, "101");
                forward(request, response, SERVLET_DISPATCHER);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    protected String getForward(final HttpServletRequest request) {
        final String forward = super.getForward(request);
        return forward == null ? "j001" : forward;
    }

    /**
     * 模試情報Beanを生成します。
     *
     * @return {@link ExamBean}
     */
    protected ExamBean createExamBean() {
        return new ExamBean(JudgementConstants.Exam.Code.CENTERPRE);
    }

}

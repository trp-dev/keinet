package jp.co.fj.inet.forms;

/**
 *
 * 大学選択（条件選択）画面アクションフォーム
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J202Form extends JOnJudgeForm {

    /** serialVersionUID */
    private static final long serialVersionUID = -9046595715545063126L;

    /** 分野コード：その他 */
    private String region7900;

    /**
     * 分野コード：その他を返します。
     *
     * @return 分野コード：その他
     */
    public String getRegion7900() {
        return region7900;
    }

    /**
     * 分野コード：その他をセットします。
     *
     * @param region7900 分野コード：その他
     */
    public void setRegion7900(String region7900) {
        this.region7900 = region7900;
    }

}

package jp.co.fj.inet.util.taglib;

import java.io.BufferedWriter;
import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 *
 * 大学コードから大学の種別を出力するTaglib
 *
 * 2005.06.16	[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 */
public class UnivTypeTag extends BodyTagSupport {

    /**
    *
    */
    private static final long serialVersionUID = -3245280959415472288L;

    /* (非 Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
        return (EVAL_BODY_BUFFERED);
    }

    /* (非 Javadoc)
     * @see javax.servlet.jsp.tagext.IterationTag#doAfterBody()
     */
    public int doAfterBody() throws JspException {
        return SKIP_BODY;
    }

    /* (非 Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doEndTag()
     */
    public int doEndTag() throws JspException {
        if (bodyContent != null) {
            try {
                BufferedWriter writer = new BufferedWriter(pageContext.getOut());
                writer.write(univCd2TypeString(Integer.parseInt(bodyContent.getString())));
                writer.flush();
            } catch (NumberFormatException e) {
                throw new JspException("大学コードの値が不正です。" + bodyContent.getString());
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }
        }
        return EVAL_PAGE;
    }

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.BodyTag#doInitBody()
     */
    public void doInitBody() throws JspException {
        super.doInitBody();
    }

    /**
     * 大学コードを大学種別文字列に変換する
     *
     * @param value
     * @return
     */
    protected String univCd2TypeString(final int value) {
        if (value >= 1000 && value <= 1499) {
            return "（国）";
        } else if (value >= 1500 && value <= 1899) {
            return "（公）";
        } else if (value >= 2000 && value <= 2999) {
            return "（私）";
        } else if (value >= 3000 && value <= 3499) {
            return "（国）";
        } else if (value >= 3500 && value <= 3999) {
            return "（公）";
        } else if (value >= 4000 && value <= 4999) {
            return "（私）";
        } else {
            return "（他）";
        }
    }
}

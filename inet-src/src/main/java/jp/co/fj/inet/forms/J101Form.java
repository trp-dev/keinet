package jp.co.fj.inet.forms;

import jp.co.fj.kawaijuku.judgement.beans.InputEngPtBean;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 成績入力画面アクションフォーム
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
@Getter
@Setter
@ToString
public class J101Form extends BaseForm {

    /** serialVersionUID */
    private static final long serialVersionUID = -2700535591737149350L;

    private String type; // 私大判定種別
    private String mode; // 動作モード
    private String changed; // フォーム変更フラグ

    // テーブル1 ----------------------------------------
    // 科目
    private String t1Ma1Subject; // 数学1

    /**
     * コンストラクタです。
     */
    public J101Form() {
        super();
        engCert01 = new InputEngPtBean();
        engCert02 = new InputEngPtBean();
    }

    private String t1Ma2Subject; // 数学2
    private String t1Sc1Subject; // 理科1
    private String t1Sc2Subject; // 理科2
    private String t1Sc3Subject; // 理科3
    private String t1Sc4Subject; // 理科4
    private String t1Gh1Subject; // 地歴・公民1
    private String t1Gh2Subject; // 地歴・公民2
    // 換算得点
    private String t1En1Score; // 英語
    private String t1En2Score; // リスニング
    private String t1Ma1Score; // 数学1
    private String t1Ma2Score; // 数学2
    private String t1JaDescQ1; // 国語記述問１評価
    private String t1JaDescQ2; // 国語記述問２評価
    private String t1JaDescQ3; // 国語記述問３評価
    private String t1JaDescQ4; // 国語記述総合評価
    private String t1Ja1Score; // 現代文
    private String t1Ja2Score; // 古文
    private String t1Ja3Score; // 漢文
    private String t1Sc1Score; // 理科1
    private String t1Sc2Score; // 理科2
    private String t1Sc3Score; // 理科3
    private String t1Sc4Score; // 理科4
    private String t1Gh1Score; // 地歴・公民1
    private String t1Gh2Score; // 地歴・公民2
    // 偏差値
    private String t1En1Dev; // 英語
    private String t1En2Dev; // リスニング
    private String t1En3Dev; // 英語＋L
    private String t1Ma3Dev; // 数学�@
    private String t1Ma4Dev; // 数学�A
    private String t1Ja4Dev; // 現・古・漢
    private String t1Ja5Dev; // 現・古
    private String t1Ja6Dev; // 現代文
    private String t1Sc1Dev; // 理科1
    private String t1Sc2Dev; // 理科2
    private String t1Sc3Dev; // 理科3
    private String t1Sc4Dev; // 理科4
    private String t1Gh1Dev; // 地歴・公民1
    private String t1Gh2Dev; // 地歴・公民2
    // テーブル2 ----------------------------------------
    // 科目
    //private String t2EnSubject;	// 英語
    private String listeningFlag; // リスニング受験かどうか
    private String t2MaSubject; // 数学
    private String t2JaSubject; // 国語
    private String t2Sc1Subject; // 理科1
    private String t2Sc2Subject; // 理科2
    private String t2Gh1Subject; // 地歴・公民1
    private String t2Gh2Subject; // 地歴・公民2
    // 偏差値
    private String t2EnDev; // 英語
    private String t2MaDev; // 数学
    private String t2JaDev; // 国語
    private String t2Sc1Dev; // 理科1
    private String t2Sc2Dev; // 理科2
    private String t2Gh1Dev; // 地歴・公民1
    private String t2Gh2Dev; // 地歴・公民2

    /* 英語認定試験1 */
    private InputEngPtBean engCert01;
    /* 英語認定試験2 */
    private InputEngPtBean engCert02;

    /**
     * 英語認定試験が入力されているかどうかを返却します。
     *
     * @return true:入力あり、false:入力なし
     */
    public boolean isEngPtEmpty() {
        return engCert01.isEmpty() && engCert02.isEmpty();
    }

    /**
     * 国語が入力されているかどうかを返却します。
     *
     * @return true:入力あり、false:入力なし
     */
    public boolean isJapEmpty() {
        return StringUtil.isEmpty(t1Ja1Score) && StringUtil.isEmpty(t1Ja2Score) && StringUtil.isEmpty(t1Ja3Score) && StringUtil.isEmpty(t1JaDescQ1) && StringUtil.isEmpty(t1JaDescQ2) && StringUtil.isEmpty(t1JaDescQ3)
                && StringUtil.isEmpty(t1JaDescQ4);
    }

}

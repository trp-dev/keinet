package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.beans.SearchByUnivNameBean;
import jp.co.fj.inet.data.J203Condition;
import jp.co.fj.inet.data.JNestedUnivData;
import jp.co.fj.inet.forms.J203Form;
import jp.co.fj.inet.util.ComparatorChain;
import jp.co.fj.inet.util.UnivDataComparator;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.keinavi.beans.individual.judgement.SearchUnivInitCharBean;
import jp.co.fj.keinavi.data.individual.NestedUnivData;
import jp.co.fj.keinavi.util.KNJpnStringConv;
import jp.co.fj.keinavi.util.log.KNLog;

/**
 *
 * 大学選択（大学指定）画面サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J203Servlet extends DefaultHttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = -328973613178684917L;

    /** 検索結果のComparator */
    private final ComparatorChain resultComparator = new ComparatorChain();

    /**
     * コンストラクタです。
     */
    public J203Servlet() {
        /* 検索結果の第1ソートキー：カナ付50音順番号 */
        resultComparator.add(new UnivDataComparator() {
            protected int compare(UnivData d1, UnivData d2) {
                return compare(d1.getKanaNum(), d2.getKanaNum());
            }
        });
        /* 検索結果の第2ソートキー：大学コード */
        resultComparator.add(new UnivDataComparator() {
            protected int compare(UnivData d1, UnivData d2) {
                return compare(d1.getUnivCd(), d2.getUnivCd());
            }
        });
        /* 検索結果の第3ソートキー：大学夜間部区分 */
        resultComparator.add(new UnivDataComparator() {
            protected int compare(UnivData d1, UnivData d2) {
                return Double.compare(d1.getFlag_night(), d2.getFlag_night());
            }
        });
        /* 検索結果の第4ソートキー：学部内容コード */
        resultComparator.add(new UnivDataComparator() {
            protected int compare(UnivData d1, UnivData d2) {
                return compare(d1.getFacultyConCd(), d2.getFacultyConCd());
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /* HTTPセッション */
        HttpSession session = request.getSession(false);

        /* アクションフォーム */
        J203Form form = (J203Form) getActionForm(request, J203Form.class.getName());

        /* 検索条件をセッションから取得する */
        J203Condition jc = getJudgementCondition(session);

        if ("j203".equals(getForward(request))) {
            /* JSPへの遷移の場合 */
            if ("j203".equals(getBackward(request))) {
                /* 遷移元がJSPの場合 */
                List searchList;
                if (form.getButton().trim().equals("search")) {
                    /* 大学名検索の場合 */
                    KNLog.Lv2(request.getRemoteAddr(), null, null, null, "j203:j203", 3001, null);
                    /*MDC.put("form", "j203:j203");
                    MDC.put("code", "3001");
                    logger.info("-");*/
                    /* 大学名検索を実行する */
                    searchList = searchByUnivName(form.getSearchStr());
                    /* セッションに検索条件を保存する */
                    jc.setSearchStr(form.getSearchStr());
                    jc.setCharValue(null);
                } else {
                    /* 頭文字が押された場合 */
                    KNLog.Lv2(request.getRemoteAddr(), null, null, null, "j203:j203", 3003, null);
                    /*MDC.put("form", "j203:j203");
                    MDC.put("code", "3003");
                    logger.info("-");*/
                    /* 頭文字検索を実行する */
                    searchList = searchByInitial(form.getCharValue());
                    /* 大学名検索の検索条件をクリアする */
                    form.setSearchStr("");
                    /* セッションに検索条件を保存する */
                    jc.setCharValue(form.getCharValue());
                    jc.setSearchStr(null);
                }
                request.setAttribute("nestedUnivList", searchList);
                request.setAttribute("nestedUnivListSize", Integer.toString(searchList.size()));
            } else if ("j204".equals(getBackward(request)) && jc.getUnivValueList() != null) {
                /* 遷移元が判定結果一覧画面の場合は、JSPに画面復元用の情報を渡す */
                List searchList = null;
                if (jc.getSearchStr() != null) {
                    searchList = searchByUnivName(jc.getSearchStr());
                    form.setSearchStr(jc.getSearchStr());
                } else {
                    searchList = searchByInitial(jc.getCharValue());
                    form.setCharValue(jc.getCharValue());
                }
                if (searchList != null) {
                    request.setAttribute("nestedUnivList", searchList);
                    request.setAttribute("nestedUnivListSize", Integer.toString(searchList.size()));
                    request.setAttribute("univValueList", jc.getUnivValueList());
                    request.setAttribute("facultyStatusFlg", jc.getFacultyStatusFlg());
                }
            } else {
                /* 初回表示の場合 */
                form.setSearchStr("");
            }

            request.setAttribute("form", form);

            /* タブの制御で利用する判定モードフラグを設定する */
            session.setAttribute("JudgeMode", "2");

            forward(request, response, JSP_J203);
        } else {
            /* 他画面への遷移の場合 */
            if ("j204".equals(getForward(request))) {
                /* 判定対象の大学情報と名称指定�A画面に戻るフラグをセッションに保存する */
                jc.setUnivValueList(form.getUnivValue() == null ? null : Arrays.asList(form.getUnivValue()));
                jc.setFacultyStatusFlg(Boolean.valueOf(form.getFacultyStatusFlg()));
            }
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * 判定実行条件をセッションから取得します。
     *
     * @param session {@link HttpSession}
     * @return {@link J203Condition}
     */
    private J203Condition getJudgementCondition(HttpSession session) {
        J203Condition jc = (J203Condition) session.getAttribute(J203Condition.class.getName());
        if (jc == null) {
            /* セッションに存在しなければ保存しておく */
            jc = new J203Condition();
            session.setAttribute(J203Condition.class.getName(), jc);
        }
        return jc;
    }

    /**
     * 大学名検索を実行します。
     *
     * @param condition 検索条件（大学名）
     * @return 検索にヒットした大学インスタンスリスト
     */
    private List searchByUnivName(String condition) {
        return toNestedList(new SearchByUnivNameBean().execute(getUnivAll(), condition));
    }

    /**
     * 頭文字検索を実行します。
     *
     * @param condition 検索条件（頭文字）
     * @return 検索にヒットした大学インスタンスリスト
     */
    private List searchByInitial(String condition) {
        SearchUnivInitCharBean suic = new SearchUnivInitCharBean();
        suic.setInitCharHalf(KNJpnStringConv.hkana2Han(condition));
        suic.setInitCharFull(KNJpnStringConv.hkana2Kkana(condition));
        suic.setCompairData(getUnivAll());
        suic.execute();
        return toNestedList(suic.getRecordSet());
    }

    /**
     * 大学インスタンスリストを入れ子構造にします。
     *
     * @param searchList 大学インスタンスリスト
     * @return 入れ子大学リスト
     */
    private List toNestedList(List searchList) {

        /* 事前にソートしておく */
        Collections.sort(searchList, resultComparator);

        List list = new ArrayList();
        String[] tempData = new String[3];
        for (Iterator ite = searchList.iterator(); ite.hasNext();) {
            UnivData data = (UnivData) ite.next();

            JNestedUnivData univData;
            if (!data.getUnivCd().equals(tempData[0])) {
                /* 初めてのUNIV */
                univData = new JNestedUnivData();
                univData.setUnivCd(data.getUnivCd());
                univData.setUnivNameAbbr(data.getUnivName());
                univData.setUnivKanjiName(data.getUnivKanjiName());
                univData.setFacultyList(new ArrayList());
                list.add(univData);
            }
            univData = (JNestedUnivData) list.get(list.size() - 1);

            NestedUnivData.FacultyData facultyData;
            if (!data.getFacultyCd().equals(tempData[1]) || univData.getFacultyList().size() == 0) {
                /* 初めてのFACULTY */
                facultyData = univData.new FacultyData();
                facultyData.setFacultyCd(data.getFacultyCd());
                facultyData.setFacultyNameAbbr(data.getDeptName());
                facultyData.setDeptList(new ArrayList());
                univData.getFacultyList().add(facultyData);
            }
            facultyData = (NestedUnivData.FacultyData) univData.getFacultyList().get(univData.getFacultyList().size() - 1);

            NestedUnivData.FacultyData.DeptData deptData;
            if (!data.getDeptCd().equals(tempData[2]) || facultyData.getDeptList().size() == 0) {
                /* 初めてのDEPT */
                deptData = facultyData.new DeptData();
                deptData.setDeptCd(data.getDeptCd());
                deptData.setDeptNameAbbr(data.getSubName());
                deptData.setSuperAbbrName(data.getSubName());
                facultyData.getDeptList().add(deptData);
            }

            tempData[0] = data.getUnivCd();
            tempData[1] = data.getFacultyCd();
            tempData[2] = data.getDeptCd();
        }

        return list;
    }

}

package jp.co.fj.inet.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.data.UnivList;
import jp.co.fj.inet.framework.exception.KeiNetException;
import jp.co.fj.keinavi.beans.individual.judgement.JudgementListBean;
import jp.co.fj.keinavi.servlets.individual.BaseJudgeThread;
import jp.co.fj.keinavi.util.message.MessageLoader;

/**
 *
 * 判定処理中サーブレット
 *
 * 2005.05.06	Yoshimoto KAWAI - Totec
 * 				IOnJudgeServletをベースに新規作成
 *
 * 2005.05.19	Yoshimoto KAWAI - Totec
 * 				アクションフォームは不要なのでコメントアウト
 *
 * <2010年度改修>
 * 2009.09.24   Tomohisa YAMADA - Totec
 * 				keinavi.jar最新化対応
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 *
 *
 */
public class JOnJudgeServlet extends DefaultHttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 4272822991276671189L;
    //[003] del-start
    //	public static final String THREAD_KEY = IOnJudgeServlet.THREAD_KEY;
    //	public static final String STATUS_PROG = IOnJudgeServlet.STATUS_PROG;
    //	public static final String STATUS_DONE = IOnJudgeServlet.STATUS_DONE;
    //	public static final String STATUS_ERR_SQL = IOnJudgeServlet.STATUS_ERR_SQL;
    //[003] del-end
    //[003] add-start
    public static final String THREAD_KEY = "threadStatus";
    public static final String STATUS_PROG = BaseJudgeThread.STATUS_PROG;
    public static final String STATUS_DONE = BaseJudgeThread.STATUS_DONE;
    public static final String STATUS_ERR_SQL = BaseJudgeThread.STATUS_ERR;
    //[003] add-end

    private final String ERROR_MSG_ABNORM = "Thread異常処理発生(status == null)";
    private final String ERROR_MSG_SQL = "Thread異常処理発生(SQL ERROR)";
    private final String ERROR_MSG_MAP = "結果が存在しません";
    private final String ERROR_CODE_ABNORM = "YYYYY";
    private final String ERROR_CODE_SQL = "ZZZZZ";
    private final String ERROR_CODE_MAP = "AAAAA";

    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // HTTPセッション
        HttpSession session = request.getSession(false);
        // ステータス
        String status = (String) session.getAttribute(THREAD_KEY);

        // アクションフォーム
        //JOnJudgeForm form = (JOnJudgeForm)
        //	super.getActionForm(request, "jp.co.fj.inet.forms.JOnJudgeForm");

        //再表示時にフォームの値を引き継ぐ必要がある
        //request.setAttribute("form", form);

        //ありえない異常終了
        if (status == null) {
            throw new KeiNetException(ERROR_CODE_ABNORM, ERROR_MSG_ABNORM);
        }
        //SQLエラー
        else if (status.equals(STATUS_ERR_SQL)) {
            throw new KeiNetException(ERROR_CODE_SQL, ERROR_MSG_SQL);
        }
        //処理中
        else if (status.equals(STATUS_PROG)) {
            //処理中表示
            super.forward(request, response, JSP_JOnJudge);
        }
        //処理が終わった
        else if (status.equals(STATUS_DONE)) {
            // 結果がないのでエラー
            if (request.getSession(false).getAttribute(JudgementListBean.SESSION_KEY) == null) {
                throw new KeiNetException(ERROR_CODE_MAP, ERROR_MSG_MAP);
            }

            // JSP転送
            // j204
            if ("j204".equals(getForward(request))) {
                // 登録済みの志望大学マップをrequestにセットしておく
                UnivList univ = (UnivList) session.getAttribute(UnivList.SESSION_KEY);
                if (univ != null) {
                    request.setAttribute("UnivMap", univ.getKeyMap());
                }

                super.forward(request, response, JSP_J204);

                // j301
            } else if ("j301".equals(getForward(request))) {
                super.forward(request, response, JSP_J301);

                // 転送先不明
            } else {
                throw new KeiNetException("転送先が不明です。" + getForward(request), "1003", MessageLoader.getInstance().getMessage("j027a"));
            }
        }
    }
}

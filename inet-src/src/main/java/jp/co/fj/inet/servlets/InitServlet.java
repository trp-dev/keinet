package jp.co.fj.inet.servlets;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import jp.co.fj.inet.data.UnivMasterData;
import jp.co.fj.kawaijuku.judgement.data.Univ;
import jp.co.fj.kawaijuku.judgement.factory.FactoryManager;
import jp.co.fj.kawaijuku.judgement.factory.UnivFactory;
import jp.co.fj.keinavi.util.KNCommonProperty;
import jp.co.fj.keinavi.util.KNPropertyCtrl;
import jp.co.fj.keinavi.util.message.MessageLoader;
import jp.co.fj.keinavi.util.sql.QueryLoader;

import org.apache.log4j.xml.DOMConfigurator;

import com.fjh.db.DBManager;

/**
 *
 * InitServlet
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class InitServlet extends BaseInitServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 4589816524536195734L;

    /**
     * {@inheritDoc}
     */
    public void init() throws ServletException {

        /* ServletContext */
        ServletContext context = getServletContext();

        /* パス情報を初期化する */
        initPath(context);

        context.setAttribute("system", "hantei");

        super.init();
    }

    /**
     * パス情報を初期化します。
     *
     * @param context {@link ServletContext}
     */
    private void initPath(ServletContext context) throws ServletException {

        /* common.propertiesのパスをセット */
        KNPropertyCtrl.FilePath = context.getRealPath("/WEB-INF/classes/jp/co/fj/keinavi/resources");

        /* SQLファイルがあるパスをセット */
        QueryLoader.getInstance().setPath(context.getRealPath("/WEB-INF/sql"));

        /* メッセージファイルがあるパスをセット */
        MessageLoader.getInstance().setPath(context.getRealPath("/WEB-INF"));

        /* Log4j設定 */
        DOMConfigurator.configure(context.getRealPath("/WEB-INF/config/log4j.xml"));
    }

    /**
     * {@inheritDoc}
     */
    public void destroy() {

        try {
            DBManager.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.destroy();
    }

    /**
     * {@inheritDoc}
     */
    protected UnivMasterData loadUnivMaster() throws Exception {

        Connection con = null;
        try {
            /* DBコネクションはプールからではなく直接取得する */
            Class.forName(KNCommonProperty.getNDBDriver());
            con = DriverManager.getConnection(KNCommonProperty.getNDBURL(), KNCommonProperty.getNDBConnUserID(), KNCommonProperty.getNDBConnUserPass());

            Class factoryClass = Class.forName("jp.fujitsu.keinet.mst.db.factory.UnivFactoryNoSchedule");

            Constructor constructor = factoryClass.getConstructor(new Class[] { Connection.class });

            UnivFactory factory = (UnivFactory) constructor.newInstance(new Object[] { con });

            FactoryManager manager = new FactoryManager(factory);
            /*LoggerUtil.initMDC();*/
            /* 判定用大学マスタをロードする */
            System.out.println(">> 模試判定 大学マスタの展開を開始 模試区分:[" + factory.getExamDiv() + "]");
            manager.load();

            /* 専門学校の大学インスタンスを削除する */
            deleteVocationalSchoolData(manager.getDatas());

            System.out.println(">> 模試判定 大学マスタの展開を終了");
            System.out.println(">> 模試判定 展開数 : " + manager.getDatas().size());
            System.out.println(">> 模試判定 ローディング時間 " + manager.getLoadingTime());

            /* 情報誌用科目データをロードする */
            System.out.println(">> 模試判定 情報誌用科目データの展開を開始");
            manager.loadUnivInfo();
            System.out.println(">> 模試判定 情報誌用科目データの展開を終了");
            System.out.println(">> 模試判定 展開数 : " + manager.getUnivInfoMap().size());
            System.out.println(">> 模試判定 ローディング時間 " + manager.getUnivInfoLoadingTime());

            return new UnivMasterData(manager);
        } finally {
            if (con != null) {
                con.close();
            }
            /*LoggerUtil.destoryMDC();*/
        }
    }

    /**
     * 専門学校の大学インスタンスを削除します。
     *
     * @param datas 大学インスタンスのリスト
     */
    private void deleteVocationalSchoolData(List datas) {
        for (Iterator ite = datas.iterator(); ite.hasNext();) {
            Univ univ = (Univ) ite.next();
            if ("08".equals(univ.getUnivDivCd()) || "09".equals(univ.getUnivDivCd())) {
                ite.remove();
            }
        }
    }

}

package jp.co.fj.inet.forms;

/**
 *
 * 判定結果一覧画面アクションフォーム
 *
 * 2005.05.06	Yoshimoto KAWAI - Totec
 *				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class J204Form extends JOnJudgeForm {

    /**
    *
    */
    private static final long serialVersionUID = 2503334361975600339L;
    private String sortKey; // ソートキー

    public void validate() {
    }

    public String getSortKey() {
        return this.sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }
}

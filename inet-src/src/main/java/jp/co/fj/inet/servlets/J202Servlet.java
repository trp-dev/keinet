package jp.co.fj.inet.servlets;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.fj.inet.data.J203Condition;
import jp.co.fj.inet.forms.J202Form;
import jp.co.fj.keinavi.beans.PrefBean;
import jp.co.fj.keinavi.util.CollectionUtil;
import jp.co.fj.keinavi.util.date.SpecialDateUtil;

/**
 *
 * 大学選択（条件選択）画面サーブレット
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J202Servlet extends DefaultHttpServlet {

    /** serialVersionUID */
    private static final long serialVersionUID = 7848110575800828879L;

    /**
     * {@inheritDoc}
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // HTTPセッション
        HttpSession session = request.getSession(false);

        // 名称指定検索の検索条件をクリアしておく
        session.removeAttribute(J203Condition.class.getName());

        // アクションフォーム - request scopre
        J202Form rform = (J202Form) getActionForm(request, J202Form.class.getName());

        // 転送元がj202の場合
        if ("j202".equals(getBackward(request))) {
            //「判定」ボタンが押されている場合
            // アクションフォームをセッションに保持する
            // 同時に I202Servlet における restoreForm と同内容の処理を行っておく
            restoreForm(rform);
            session.setAttribute("J202Form", rform);

            //「条件リセット」ボタンが押されている場合
            if ("reset".equals(rform.getButton())) {
                session.setAttribute("J202Form", null);
            }
        }

        // JSP
        if ("j202".equals(getForward(request))) {

            // アクションフォーム - session scope
            J202Form form = (J202Form) session.getAttribute("J202Form");
            // セッションになければフォームの初期値をセットする
            if (form == null) {
                form = new J202Form();

                // 入試日
                form.setStartMonth("04");
                form.setStartDate("01");
                form.setEndMonth("03");
                form.setEndDate("31");
                // 学校区分
                form.setSchoolDivProviso(CollectionUtil.deconcatComma("1"));
                // 評価範囲
                form.setRaingProviso(CollectionUtil.deconcatComma("1,2,3,4,5"));
                // 課し区分
                form.setSubImposeDiv("2");
                // 入試日区分
                form.setDateSearchDiv("0");
                // 今年度
                form.setThisYear(SpecialDateUtil.getThisJpnYear());
                // 配点比率
                form.setAllotPntRatio("0");

                session.setAttribute("J202Form", form);
            }

            try {
                // 都道府県Bean
                // 本来なら初期化処理はInitServletへ持っていきたい
                request.setAttribute("PrefBean", PrefBean.getInstance(null));
            } catch (Exception e) {
                throw new ServletException(e);
            }

            // タブの制御で利用する判定モードフラグ
            session.setAttribute("JudgeMode", "1");

            forward(request, response, JSP_J202);
        } else {
            forward(request, response, SERVLET_DISPATCHER);
        }
    }

    /**
     * 中系統・小系統のフラグの持ち方を変換する
     *
     * @param form {@link J202Form}
     */
    private void restoreForm(J202Form form) {

        if ("1".equals(form.getSearchStemmaDiv())) {
            // 中系統
            Set s = CollectionUtil.array2Set(form.getStemma2Code());

            // 中系統フラグ
            form.setMstemmaflg(true);
            form.setStemmaAflg(false);
            form.setStemmaSflg(false);

            if (s.contains("0011"))
                form.setMstemma0011("1");
            if (s.contains("0021"))
                form.setMstemma0021("1");
            if (s.contains("0022"))
                form.setMstemma0022("1");
            if (s.contains("0023"))
                form.setMstemma0023("1");
            if (s.contains("0031"))
                form.setMstemma0031("1");
            if (s.contains("0032"))
                form.setMstemma0032("1");
            if (s.contains("0041"))
                form.setMstemma0041("1");
            if (s.contains("0042"))
                form.setMstemma0042("1");
            if (s.contains("0043"))
                form.setMstemma0043("1");
            if (s.contains("0051"))
                form.setMstemma0051("1");
            if (s.contains("0061"))
                form.setMstemma0061("1");
            if (s.contains("0071"))
                form.setMstemma0071("1");
            if (s.contains("0062"))
                form.setMstemma0062("1");

        } else {
            // 文型・理系
            Set s = CollectionUtil.array2Set(form.getStemmaCode());

            if (form.getSearchStemmaDiv().equals("2")) {
                // 文系フラグ
                form.setMstemmaflg(false);
                form.setStemmaAflg(true);
                form.setStemmaSflg(false);
            } else {
                // 理系フラグ
                form.setMstemmaflg(false);
                form.setStemmaAflg(false);
                form.setStemmaSflg(true);
            }

            /** 文系・理系の中系統 */
            // 文・人文
            if (s.contains("0100") && s.contains("0200") && s.contains("0300") && s.contains("0400")
                    && s.contains("0500") && s.contains("0600") && s.contains("0700") && s.contains("0800")
                    && s.contains("0900")) {
                form.setMstemma0011("1");
            }
            // 社会・国際
            if (s.contains("1000") && s.contains("1100") && s.contains("1200") && s.contains("1300")
                    && s.contains("1400")) {
                form.setMstemma0021("1");
            }
            // 法・政治
            if (s.contains("1500") && s.contains("1600")) {
                form.setMstemma0022("1");
            }
            // 経済・経営・商
            if (s.contains("1700") && s.contains("1800") && s.contains("1900") && s.contains("2000")) {
                form.setMstemma0023("1");
            }
            // 教員養成
            if (s.contains("2100") && s.contains("2200") && s.contains("2300") && s.contains("2400")
                    && s.contains("2500") && s.contains("2600")) {
                form.setMstemma0031("1");
            }
            // 総合科学課程
            if (s.contains("2700") && s.contains("2800") && s.contains("2900") && s.contains("3000")
                    && s.contains("3100") && s.contains("3200") && s.contains("3300")) {
                form.setMstemma0032("1");
            }
            // 理
            if (s.contains("3400") && s.contains("3500") && s.contains("3600") && s.contains("3700")
                    && s.contains("3800")) {
                form.setMstemma0041("1");
            }
            // 工
            if (s.contains("3900") && s.contains("4000") && s.contains("4100") && s.contains("4200")
                    && s.contains("4300") && s.contains("4400") && s.contains("4500") && s.contains("4600")
                    && s.contains("4700") && s.contains("4800") && s.contains("4900") && s.contains("5000")
                    && s.contains("5100") && s.contains("5200")) {
                form.setMstemma0042("1");
            }
            // 農
            if (s.contains("5300") && s.contains("5400") && s.contains("5500") && s.contains("5600")
                    && s.contains("5700") && s.contains("5800")) {
                form.setMstemma0043("1");
            }
            // 医・歯・薬・保険
            if (s.contains("5900") && s.contains("6000") && s.contains("6100") && s.contains("6200")
                    && s.contains("6300") && s.contains("6400")) {
                form.setMstemma0051("1");
            }
            // 家政・生活科学
            if (s.contains("6500") && s.contains("6600") && s.contains("6700") && s.contains("6800")
                    && s.contains("6900")) {
                form.setMstemma0061("1");
            }
            // 総合・環境・人間・情報
            if (s.contains("7000") && s.contains("7100") && s.contains("7200") && s.contains("7300")) {
                form.setMstemma0071("1");
            }
            // 芸術・体育・他
            if (s.contains("7400") && s.contains("7500") && s.contains("7600") && s.contains("7700")
                    && s.contains("7800") && s.contains("7900")) {
                form.setMstemma0062("1");
            }

            /** 文系・理系の小系統 */
            // 文学
            if (s.contains("0100") && s.contains("0200")) {
                form.setSstemma01("1");
            }
            // 外国文
            if (s.contains("0300")) {
                form.setSstemma02("1");
            }
            // 哲・史・教・心
            if (s.contains("0400") && s.contains("0500") && s.contains("0600") && s.contains("0700")
                    && s.contains("0800") && s.contains("0900")) {
                form.setSstemma03("1");
            }
            // 社会・福祉
            if (s.contains("1000") && s.contains("1100")) {
                form.setSstemma04("1");
            }
            // 国際
            if (s.contains("1200") && s.contains("1300") && s.contains("1400")) {
                form.setSstemma05("1");
            }
            // 法政治政
            if (s.contains("1500") && s.contains("1600")) {
                form.setSstemma06("1");
            }
            // 経済
            if (s.contains("1700")) {
                form.setSstemma07("1");
            }
            // 経営・商・会計
            if (s.contains("1800") && s.contains("1900") && s.contains("2000")) {
                form.setSstemma08("1");
            }
            // 教員養成-教科
            if (s.contains("2100")) {
                form.setSstemma09("1");
            }
            // 教員養成-実技
            if (s.contains("2200")) {
                form.setSstemma10("1");
            }
            // 教員養成-その他
            if (s.contains("2300") && s.contains("2400") && s.contains("2500") && s.contains("2600")) {
                form.setSstemma11("1");
            }
            // 総合科学課程
            if (s.contains("2700") && s.contains("2800") && s.contains("2900") && s.contains("3000")
                    && s.contains("3100") && s.contains("3200") && s.contains("3300")) {
                form.setSstemma12("1");
            }
            // 理
            if (s.contains("3400") && s.contains("3500") && s.contains("3600") && s.contains("3700")
                    && s.contains("3800")) {
                form.setSstemma13("1");
            }
            // 機械・航空
            if (s.contains("3900") && s.contains("4000")) {
                form.setSstemma14("1");
            }
            // 電気電子・情報
            if (s.contains("4100") && s.contains("4200")) {
                form.setSstemma15("1");
            }
            // 土木・建築
            if (s.contains("4300") && s.contains("4400")) {
                form.setSstemma16("1");
            }
            // 応化・応物・資
            if (s.contains("4500") && s.contains("4600") && s.contains("4700") && s.contains("4800")) {
                form.setSstemma17("1");
            }
            // 機械・航空
            if (s.contains("4900") && s.contains("5000") && s.contains("5100") && s.contains("5200")) {
                form.setSstemma18("1");
            }
            // 農
            if (s.contains("5300") && s.contains("5400") && s.contains("5500") && s.contains("5600")
                    && s.contains("5700") && s.contains("5800")) {
                form.setSstemma19("1");
            }
            // 医・歯
            if (s.contains("5900") && s.contains("6000")) {
                form.setSstemma20("1");
            }
            // 薬・看護・保険
            if (s.contains("6100") && s.contains("6200") && s.contains("6300") && s.contains("6400")) {
                form.setSstemma21("1");
            }
            // 家政・生活科学
            if (s.contains("6500") && s.contains("6600") && s.contains("6700") && s.contains("6800")) {
                form.setSstemma22("1");
            }
            // 総・環・人・情
            if (s.contains("7000") && s.contains("7100") && s.contains("7200") && s.contains("7300")) {
                form.setSstemma23("1");
            }
            // 総・環・人・情
            if (s.contains("7400") && s.contains("7500") && s.contains("7600") && s.contains("7700")
                    && s.contains("7800") && s.contains("7900")) {
                form.setSstemma24("1");
            }

            /** 文系・理系の分野コード */
            if (s.contains("0100")) {
                form.setRegion0100("1");
                form.setRegionflg01(true);
            }
            if (s.contains("0200")) {
                form.setRegion0200("1");
                form.setRegionflg01(true);
            }
            if (s.contains("0300")) {
                form.setRegion0300("1");
                form.setRegionflg02(true);
            }
            if (s.contains("0400")) {
                form.setRegion0400("1");
                form.setRegionflg03(true);
            }
            if (s.contains("0500")) {
                form.setRegion0500("1");
                form.setRegionflg03(true);
            }
            if (s.contains("0600")) {
                form.setRegion0600("1");
                form.setRegionflg03(true);
            }
            if (s.contains("0700")) {
                form.setRegion0700("1");
                form.setRegionflg03(true);
            }
            if (s.contains("0800")) {
                form.setRegion0800("1");
                form.setRegionflg03(true);
            }
            if (s.contains("0900")) {
                form.setRegion0900("1");
                form.setRegionflg03(true);
            }
            if (s.contains("1000")) {
                form.setRegion1000("1");
                form.setRegionflg04(true);
            }
            if (s.contains("1100")) {
                form.setRegion1100("1");
                form.setRegionflg04(true);
            }
            if (s.contains("1200")) {
                form.setRegion1200("1");
                form.setRegionflg05(true);
            }
            if (s.contains("1300")) {
                form.setRegion1300("1");
                form.setRegionflg05(true);
            }
            if (s.contains("1400")) {
                form.setRegion1400("1");
                form.setRegionflg05(true);
            }
            if (s.contains("1500")) {
                form.setRegion1500("1");
                form.setRegionflg06(true);
            }
            if (s.contains("1600")) {
                form.setRegion1600("1");
                form.setRegionflg06(true);
            }
            if (s.contains("1700")) {
                form.setRegion1700("1");
                form.setRegionflg07(true);
            }
            if (s.contains("1800")) {
                form.setRegion1800("1");
                form.setRegionflg08(true);
            }
            if (s.contains("1900")) {
                form.setRegion1900("1");
                form.setRegionflg08(true);
            }
            if (s.contains("2000")) {
                form.setRegion2000("1");
                form.setRegionflg08(true);
            }
            if (s.contains("2100")) {
                form.setRegion2100("1");
                form.setRegionflg09(true);
            }
            if (s.contains("2200")) {
                form.setRegion2200("1");
                form.setRegionflg10(true);
            }
            if (s.contains("2300")) {
                form.setRegion2300("1");
                form.setRegionflg11(true);
            }
            if (s.contains("2400")) {
                form.setRegion2400("1");
                form.setRegionflg11(true);
            }
            if (s.contains("2500")) {
                form.setRegion2500("1");
                form.setRegionflg11(true);
            }
            if (s.contains("2600")) {
                form.setRegion2600("1");
                form.setRegionflg11(true);
            }
            if (s.contains("2700")) {
                form.setRegion2700("1");
                form.setRegionflg12(true);
            }
            if (s.contains("2800")) {
                form.setRegion2800("1");
                form.setRegionflg12(true);
            }
            if (s.contains("2900")) {
                form.setRegion2900("1");
                form.setRegionflg12(true);
            }
            if (s.contains("3000")) {
                form.setRegion3000("1");
                form.setRegionflg12(true);
            }
            if (s.contains("3100")) {
                form.setRegion3100("1");
                form.setRegionflg12(true);
            }
            if (s.contains("3200")) {
                form.setRegion3200("1");
                form.setRegionflg12(true);
            }
            if (s.contains("3300")) {
                form.setRegion3300("1");
                form.setRegionflg12(true);
            }
            if (s.contains("3400")) {
                form.setRegion3400("1");
                form.setRegionflg13(true);
            }
            if (s.contains("3500")) {
                form.setRegion3500("1");
                form.setRegionflg13(true);
            }
            if (s.contains("3600")) {
                form.setRegion3600("1");
                form.setRegionflg13(true);
            }
            if (s.contains("3700")) {
                form.setRegion3700("1");
                form.setRegionflg13(true);
            }
            if (s.contains("3800")) {
                form.setRegion3800("1");
                form.setRegionflg13(true);
            }
            if (s.contains("3900")) {
                form.setRegion3900("1");
                form.setRegionflg14(true);
            }
            if (s.contains("4000")) {
                form.setRegion4000("1");
                form.setRegionflg14(true);
            }
            if (s.contains("4100")) {
                form.setRegion4100("1");
                form.setRegionflg15(true);
            }
            if (s.contains("4200")) {
                form.setRegion4200("1");
                form.setRegionflg15(true);
            }
            if (s.contains("4300")) {
                form.setRegion4300("1");
                form.setRegionflg16(true);
            }
            if (s.contains("4400")) {
                form.setRegion4400("1");
                form.setRegionflg16(true);
            }
            if (s.contains("4500")) {
                form.setRegion4500("1");
                form.setRegionflg17(true);
            }
            if (s.contains("4600")) {
                form.setRegion4600("1");
                form.setRegionflg17(true);
            }
            if (s.contains("4700")) {
                form.setRegion4700("1");
                form.setRegionflg17(true);
            }
            if (s.contains("4800")) {
                form.setRegion4800("1");
                form.setRegionflg17(true);
            }
            if (s.contains("4900")) {
                form.setRegion4900("1");
                form.setRegionflg18(true);
            }
            if (s.contains("5000")) {
                form.setRegion5000("1");
                form.setRegionflg18(true);
            }
            if (s.contains("5100")) {
                form.setRegion5100("1");
                form.setRegionflg18(true);
            }
            if (s.contains("5200")) {
                form.setRegion5200("1");
                form.setRegionflg18(true);
            }
            if (s.contains("5300")) {
                form.setRegion5300("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5400")) {
                form.setRegion5400("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5500")) {
                form.setRegion5500("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5600")) {
                form.setRegion5600("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5700")) {
                form.setRegion5700("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5800")) {
                form.setRegion5800("1");
                form.setRegionflg19(true);
            }
            if (s.contains("5900")) {
                form.setRegion5900("1");
                form.setRegionflg20(true);
            }
            if (s.contains("6000")) {
                form.setRegion6000("1");
                form.setRegionflg20(true);
            }
            if (s.contains("6100")) {
                form.setRegion6100("1");
                form.setRegionflg21(true);
            }
            if (s.contains("6200")) {
                form.setRegion6200("1");
                form.setRegionflg21(true);
            }
            if (s.contains("6300")) {
                form.setRegion6300("1");
                form.setRegionflg21(true);
            }
            if (s.contains("6400")) {
                form.setRegion6400("1");
                form.setRegionflg21(true);
            }
            if (s.contains("6500")) {
                form.setRegion6500("1");
                form.setRegionflg22(true);
            }
            if (s.contains("6600")) {
                form.setRegion6600("1");
                form.setRegionflg22(true);
            }
            if (s.contains("6700")) {
                form.setRegion6700("1");
                form.setRegionflg22(true);
            }
            if (s.contains("6800")) {
                form.setRegion6800("1");
                form.setRegionflg22(true);
            }
            if (s.contains("6900")) {
                form.setRegion6900("1");
                form.setRegionflg22(true);
            }
            if (s.contains("7000")) {
                form.setRegion7000("1");
                form.setRegionflg23(true);
            }
            if (s.contains("7100")) {
                form.setRegion7100("1");
                form.setRegionflg23(true);
            }
            if (s.contains("7200")) {
                form.setRegion7200("1");
                form.setRegionflg23(true);
            }
            if (s.contains("7300")) {
                form.setRegion7300("1");
                form.setRegionflg23(true);
            }
            if (s.contains("7400")) {
                form.setRegion7400("1");
                form.setRegionflg24(true);
            }
            if (s.contains("7500")) {
                form.setRegion7500("1");
                form.setRegionflg24(true);
            }
            if (s.contains("7600")) {
                form.setRegion7600("1");
                form.setRegionflg24(true);
            }
            if (s.contains("7700")) {
                form.setRegion7700("1");
                form.setRegionflg24(true);
            }
            if (s.contains("7800")) {
                form.setRegion7800("1");
                form.setRegionflg24(true);
            }
            if (s.contains("7900")) {
                form.setRegion7900("1");
                form.setRegionflg24(true);
            }
        }
    }

}

package jp.co.fj.inet.util;

import org.apache.log4j.MDC;

public class LoggerUtil {

    /**
     * log4jのMDCを初期化します。
     *
     */
    public static void initMDC() {
        MDC.put("addr", "-");
        MDC.put("host", "-");
        MDC.put("form", "-");
        MDC.put("code", "-");
        MDC.put("ecode", "-");
        MDC.put("emsg", "-");
    }

    /**
     * log4jのMDCを破棄します。
     */
    public static void destoryMDC() {
        MDC.remove("addr");
        MDC.remove("host");
        MDC.remove("form");
        MDC.remove("code");
        MDC.remove("ecode");
        MDC.remove("emsg");
    }
}

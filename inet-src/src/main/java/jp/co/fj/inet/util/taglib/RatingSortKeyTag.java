package jp.co.fj.inet.util.taglib;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import jp.co.fj.inet.beans.J204DetailBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

/**
 *
 * 評価文字列をソート用のキーに変換するTaglib
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)YAMADA.Tomohisa
 *
 */
public class RatingSortKeyTag extends TagSupport {

    /* serialVersionUID */
    private static final long serialVersionUID = -4813419031361630313L;

    /* 評価文字列変換マップ */
    private static final Map CONV = new HashMap();

    static {
        CONV.put("A", "10");
        CONV.put("B", "11");
        CONV.put("C", "12");
        CONV.put("D", "13");
        CONV.put("E", "14");
        CONV.put("AH", "30");
        CONV.put("BH", "31");
        CONV.put("CH", "32");
        CONV.put("DH", "33");
        CONV.put("EH", "34");
        CONV.put("A#", "40");
        CONV.put("B#", "41");
        CONV.put("C#", "42");
        CONV.put("D#", "43");
        CONV.put("E#", "44");
        CONV.put("AG", "50");
        CONV.put("BG", "51");
        CONV.put("CG", "52");
        CONV.put("DG", "53");
        CONV.put("EG", "54");
        CONV.put("H", "70");
        CONV.put("G", "71");
        CONV.put("X", "72");
        CONV.put("*", "90");
        CONV.put("", "91");
    }

    /* 出力ターゲット */
    private String target;

    /**
     * {@inheritDoc}
     */
    public int doStartTag() throws JspException {
        return SKIP_BODY;
    }

    /**
     * {@inheritDoc}
     */
    public int doEndTag() throws JspException {
        JspWriter writer = pageContext.getOut();
        try {
            J204DetailBean bean = (J204DetailBean) pageContext.getAttribute("univ");
            if ("center".equals(target)) {
                /* センター評価 */
                writer.write(convertRating(bean.getCLetterJudgement().trim()));
            } else if ("second".equals(target)) {
                /* ２次評価 */
                writer.write(convertRating(bean.getSLetterJudgement().trim()));
            } else if ("schedule".equals(target)) {
                /* 日程コード */
                writer.write(convertScheduleCd(bean.getScheduleCd()));
            } else if ("border".equals(target)) {
                /* センターボーダ得点率 */
                writer.write(bean.getCenScoreRate() == 99 ? "-1" : String.valueOf(bean.getCenScoreRate()));
            } else if ("rank".equals(target)) {
                /* ２次ランク偏差値（下限値） */
                writer.write(bean.getRankLLimits() == 99 ? "-1" : String.valueOf(bean.getRankLLimits()));
            } else if ("rate".equals(target)) {
                /* 配点率 */
                writer.write(calcRate(bean.getCAllotPntRateAll(), bean.getSAllotPntRateAll()));
            } else {
                /* 不明 */
                throw new IllegalArgumentException("不明なターゲットが指定されました。" + target);
            }
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return EVAL_PAGE;
    }

    /**
     * 評価文字列を変換して返す
     *
     * @param value
     */
    private String convertRating(final String value) {
        return (String) CONV.get(value);
    }

    /**
     * 日程コードを変換して返す
     *
     * @param value
     * @return
     */
    private String convertScheduleCd(final String value) {
        if (JudgementConstants.Univ.SCHEDULE_CNT.equals(value)) {
            return "Y";
        } else if (JudgementConstants.Univ.SCHEDULE_NML.equals(value)) {
            return "Z";
        } else if (JudgementConstants.Univ.SCHEDULE_BFR.equals(value)) {
            return "A";
        } else {
            return value;
        }
    }

    /**
     * 配点比率を計算する
     *
     * @param c
     * @param s
     * @return
     */
    private String calcRate(final String c, final String s) {

        /* センター配点 */
        final int center;
        try {
            center = Integer.parseInt(c);
        } catch (NumberFormatException e) {
            /* センター配点がなければ最小値 */
            return "-1";
        }

        /* ２次配点 */
        final int second;
        try {
            second = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            /* ２次配点がなければ最大値 */
            return "9999";
        }

        return String.valueOf((double) center / second);
    }

    public void setTarget(String target) {
        this.target = target;
    }

}

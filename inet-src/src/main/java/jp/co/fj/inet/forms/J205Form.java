package jp.co.fj.inet.forms;

import jp.co.fj.keinavi.forms.individual.I205Form;

/**
 *
 * 判定結果詳細画面アクションフォーム
 *
 * 2005.05.09	Yoshimoto KAWAI - Totec
 *				[新規作成]
 *
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class J205Form extends I205Form {

    /**
     *
     */
    private static final long serialVersionUID = 8866314957602883624L;

}

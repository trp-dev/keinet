package jp.co.fj.inet.forms;

/**
 *
 * 大学選択（大学指定）画面アクションフォーム
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class J203Form extends JOnJudgeForm {

    /** serialVersionUID */
    private static final long serialVersionUID = -1032559529500127754L;

    /** 名称指定�A画面に戻るフラグ */
    private String facultyStatusFlg;

    /**
     * 名称指定�A画面に戻るフラグを返します。
     *
     * @return 名称指定�A画面に戻るフラグ
     */
    public String getFacultyStatusFlg() {
        return facultyStatusFlg;
    }

    /**
     * 名称指定�A画面に戻るフラグをセットします。
     *
     * @param facultyStatusFlg 名称指定�A画面に戻るフラグ
     */
    public void setFacultyStatusFlg(String facultyStatusFlg) {
        this.facultyStatusFlg = facultyStatusFlg;
    }

}

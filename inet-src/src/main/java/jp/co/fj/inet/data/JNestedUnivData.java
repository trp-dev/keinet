package jp.co.fj.inet.data;

import jp.co.fj.keinavi.data.individual.NestedUnivData;

/**
 *
 * 模試判定システム用の入れ子大学クラスです。<br>
 * 大学コード表名を保持するためにKei-Naviとは別に定義しています。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JNestedUnivData extends NestedUnivData {

    /** 大学コード表名 */
    private String univKanjiName;

    /**
     * 大学コード表名を返します。
     *
     * @return 大学コード表名
     */
    public String getUnivKanjiName() {
        return univKanjiName;
    }

    /**
     * 大学コード表名をセットします。
     *
     * @param univKanjiName 大学コード表名
     */
    public void setUnivKanjiName(String univKanjiName) {
        this.univKanjiName = univKanjiName;
    }

}

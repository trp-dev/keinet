SELECT
  P.INDIVIDUALID AS ID
  , P.UNIVCD
  , P.FACULTYCD
  , P.DEPTCD
  , P.CANDIDATERANK
  , P.PROVINCES_FLG AS PROVINCESFLG
  , B.UNINAME_ABBR AS UNIVNAME
  , B.FACULTYNAME_ABBR AS FACULTYNAME
  , B.DEPTNAME_ABBR AS DEPTNAME
  , B.EXAMSCHEDULEDETAIL_FLAG AS EXAMSCHEDULEDETAILFLAG
  , B.UNIVCOMMENT_FLAG AS UNIVCOMMENTFLAG
FROM
  EXAMPLANUNIV P
  INNER JOIN UNIVMASTER_BASIC B
    ON B.UNIVCD = P.UNIVCD
    AND B.FACULTYCD = P.FACULTYCD
    AND B.DEPTCD = P.DEPTCD
WHERE
  P.INDIVIDUALID = /*id*/0
ORDER BY
  P.CANDIDATERANK

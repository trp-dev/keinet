SELECT
  PATTERNCD
  , ENTEXAMFULLPNT_NREP AS FULLPNT
  , STEP_SIZE AS STEPSIZE
  , NUMBERS1
  , NUMBERS2
  , NUMBERS3
  , NUMBERS4
  , NUMBERS5
  , NUMBERS6
  , NUMBERS7
  , NUMBERS8
  , NUMBERS9
  , NUMBERS10
  , NUMBERS11
  , NUMBERS12
  , NUMBERS13
  , NUMBERS14
  , NUMBERS15
  , NUMBERS16
  , NUMBERS17
  , NUMBERS18
  , NUMBERS19
FROM
  UNIVDISTRECORD
WHERE
  EXAMYEAR = /*examYear*/'x'
  AND EXAMCD = /*examCd*/'x'
  AND UNIVCD = /*univCd*/'x'
  AND FACULTYCD = /*facultyCd*/'x'
  AND DEPTCD = /*deptCd*/'x'

/*
 * 処理概要：指定の大学-学部-学科で取得可能な資格情報を取得する。
 * 表 定 義：テーブルレイアウト.xls -- 資格マスタ, 大学資格情報
 * 設計情報：詳細設計書（個人成績･志望大学）.xls -- SQL_C1_004
 * 特記事項：条件は大学資格情報の次の要素の一致「大学コード・学部コード・学科コード」
 * 作 成 日：2014/03/12
 * 作 成 者：TOTEC)OOMURA.Masafumi
 * 更 新 日：
 * 更新内容：
 */
SELECT
  T0.CERTCD AS CERTCD,
  T1.CERTNAME AS CERTNAME
FROM
  -- 指定の大学・学部・学科で取得可能な資格コードを得る
  (
  SELECT
    CERTCD
  FROM
    UNIVMASTER_CERT
  WHERE
    UNIVCD = /*univCd*/'1234'
    AND FACULTYCD = /*facultyCd*/'56'
    AND DEPTCD = /*deptCd*/'7890'
  ) T0
  -- 資格コードに紐付く資格名を得る
  INNER JOIN CERTIFICATION T1
  ON T0.CERTCD = T1.CERTCD
-- 並びは資格コードの昇順
ORDER BY
  T0.CERTCD

INSERT
INTO CANDIDATEUNIV(
  INDIVIDUALID
  , EXAMYEAR
  , EXAMCD
  , SECTION
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , DISP_FLG
  , DISP_NUMBER
  , YEAR
  , EXAMDIV
)
SELECT
  /*id*/1
  , /*exam.examYear*/'2012'
  , /*exam.examCd*/'01'
  , '2'
  , /*univKey.univCd*/'9999'
  , /*univKey.facultyCd*/'99'
  , /*univKey.deptCd*/'9999'
  , A.DISP_FLG
  , CASE
    WHEN A.DISP_FLG = '1'
    THEN B.DISP_NUMBER
    ELSE C.DISP_NUMBER
    END
  , /*systemDto.univYear*/'2012'
  , /*systemDto.univExamDiv*/'07'
FROM
  (
    SELECT
      CASE
        WHEN CNT > 0
        THEN '0'
        ELSE '1'
        END AS DISP_FLG
    FROM
      (
        SELECT
          COUNT(1) AS CNT
        FROM
          CANDIDATEUNIV
        WHERE
          INDIVIDUALID = /*id*/1
          AND EXAMYEAR = /*exam.examYear*/'2012'
          AND EXAMCD = /*exam.examCd*/'01'
          AND SECTION = '1'
          AND UNIVCD = /*univKey.univCd*/'9999'
          AND FACULTYCD = /*univKey.facultyCd*/'99'
          AND DEPTCD = /*univKey.deptCd*/'9999'
      )
  ) A
  CROSS JOIN (
    SELECT
      COALESCE(MAX(DISP_NUMBER), 0) + 1 AS DISP_NUMBER
    FROM
      CANDIDATEUNIV
    WHERE
      INDIVIDUALID = /*id*/1
      AND EXAMYEAR = /*exam.examYear*/'2012'
      AND EXAMCD = /*exam.examCd*/'01'
      AND DISP_FLG = '1'
  ) B
  CROSS JOIN (
    SELECT
      COALESCE(MAX(DISP_NUMBER), 0) + 1 AS DISP_NUMBER
    FROM
      CANDIDATEUNIV
    WHERE
      INDIVIDUALID = /*id*/1
      AND EXAMYEAR = /*exam.examYear*/'2012'
      AND EXAMCD = /*exam.examCd*/'01'
      AND DISP_FLG = '0'
  ) C
WHERE
  NOT EXISTS (
    SELECT
      1
    FROM
      CANDIDATEUNIV
    WHERE
      INDIVIDUALID = /*id*/1
      AND EXAMYEAR = /*exam.examYear*/'2012'
      AND EXAMCD = /*exam.examCd*/'01'
      AND SECTION = '2'
      AND UNIVCD = /*univKey.univCd*/'9999'
      AND FACULTYCD = /*univKey.facultyCd*/'99'
      AND DEPTCD = /*univKey.deptCd*/'9999'
  )

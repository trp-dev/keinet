UPDATE CANDIDATEUNIV
SET
  DISP_FLG = '1'
  , DISP_NUMBER = DISP_NUMBER + /*max*/0
WHERE
  INDIVIDUALID = /*id*/0
  AND UNIVCD = /*univKey.univCd*/'x'
  AND FACULTYCD = /*univKey.facultyCd*/'x'
  AND DEPTCD = /*univKey.deptCd*/'x'
  AND EXAMYEAR = /*exam.examYear*/'2012'
  AND EXAMCD = /*exam.examCd*/'01'
  AND SECTION = '2'
  AND DISP_FLG = '0'

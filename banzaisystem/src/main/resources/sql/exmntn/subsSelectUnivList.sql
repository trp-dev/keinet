SELECT
  B.UNIVCD
  , B.FACULTYCD
  , B.DEPTCD
  , B.UNIVNAME_KANA AS UNIVNAMEKANA
  , B.UNINAME_ABBR AS UNIVNAME
  , B.FACULTYNAME_ABBR AS FACULTYNAME
  , B.DEPTNAME_ABBR AS DEPTNAME
  , P.PREFCD
  , P.PREFNAME
  , B.ENTEXAMINPLEDATE
  , B.ENTEXAMAPPLIDEADLINE
  , B.EXAMSCHEDULEDETAIL_FLAG AS EXAMSCHEDULEDETAILFLAG
  , B.UNIVCOMMENT_FLAG AS UNIVCOMMENTFLAG
FROM
  UNIVMASTER_BASIC B
  LEFT JOIN PREFECTURE P
    ON P.PREFCD = B.PREFCD_EXAMHO
WHERE
  B.ROWID > 0
  /*IF divList != null */
  AND (
    /*BEGIN*/
      /*IF "1" in divList*/
      B.UNIDIV = '01'
      /*END*/
      /*IF "2" in divList*/
      OR B.UNIDIV = '02'
      /*END*/
      /*IF "3" in divList*/
      OR (B.UNIDIV = '03' AND B.UNIGDIV = '1')
      /*END*/
      /*IF "4" in divList*/
      OR (B.UNIDIV = '03' AND B.UNIGDIV != '1')
      /*END*/
      /*IF "5" in divList*/
      OR (B.UNIDIV IN ('04', '05') AND B.UNIGDIV = '1')
      /*END*/
      /*IF "6" in divList*/
      OR (B.UNIDIV IN ('04', '05') AND B.UNIGDIV != '1')
      /*END*/
      /*IF "7" in divList*/
      OR B.UNIDIV IN ('06', '07')
      /*END*/
    /*END*/
  )
  /*END*/

  /*IF femail.value == "2"*/
  AND B.UNICOEDDIV != '2'
  /*END*/
  /*IF femail.value == "3"*/
  AND B.UNICOEDDIV = '2'
  /*END*/

  /*IF night.value == "2"*/
  AND B.UNINIGHTDIV = '1'
  /*END*/
  /*IF night.value == "3"*/
  AND B.UNINIGHTDIV != '1'
  /*END*/

  /*IF univName != null*/
  AND B.UNIVNAME_KANA_NORMALIZED LIKE /*univName*/'' || '%'
  /*END*/

  /*IF prefCdList != null*/
  AND B.PREFCD_EXAMHO IN /*prefCdList*/('')
  /*END*/

  /*IF mStemmaList != null || sStemmaList != null || regionList != null*/
    AND (
    /*BEGIN*/
      /*IF mStemmaList != null*/
      EXISTS (
        SELECT
          1
        FROM
          UNIVSTEMMA S
        WHERE
          S.UNIVCD = B.UNIVCD
          AND S.FACULTYCD = B.FACULTYCD
          AND S.DEPTCD = B.DEPTCD
          AND S.STEMMADIV = '2'
          AND S.STEMMACD IN /*mStemmaList*/('')
      )
      /*END*/
      /*IF sStemmaList != null*/
      OR EXISTS (
        SELECT
          1
        FROM
          UNIVSTEMMA S
        WHERE
          S.UNIVCD = B.UNIVCD
          AND S.FACULTYCD = B.FACULTYCD
          AND S.DEPTCD = B.DEPTCD
          AND S.STEMMADIV = '1'
          AND S.STEMMACD IN /*sStemmaList*/('')
      )
      /*END*/
      /*IF regionList != null*/
      OR EXISTS (
        SELECT
          1
        FROM
          UNIVSTEMMA S
        WHERE
          S.UNIVCD = B.UNIVCD
          AND S.FACULTYCD = B.FACULTYCD
          AND S.DEPTCD = B.DEPTCD
          AND S.STEMMADIV = '3'
          AND S.STEMMACD IN /*regionList*/('')
      )
      /*END*/
    /*END*/
    )
  /*END*/

  /*IF certCdList != null*/
  AND EXISTS (
    SELECT
      1
    FROM
      UNIVMASTER_CERT C
    WHERE
      C.UNIVCD = B.UNIVCD
      AND C.FACULTYCD = B.FACULTYCD
      AND C.DEPTCD = B.DEPTCD
      AND C.CERTCD IN /*certCdList*/('')
  )
  /*END*/

  /*IF scheduleList != null */
  AND (
    B.UNIDIV NOT IN ('01', '02')
    OR (
      /*BEGIN*/
        /*IF "1" in scheduleList*/
        B.UNIGDIV = 'D'
        /*END*/
        /*IF "2" in scheduleList*/
        OR B.UNIGDIV = 'E'
        /*END*/
        /*IF "3" in scheduleList*/
        OR B.UNIGDIV = 'C'
        /*END*/
        /*IF "4" in scheduleList*/
        OR (B.UNIDIV IN ('01', '02') AND B.UNIGDIV NOT IN ('D', 'E', 'C'))
        /*END*/
      /*END*/
    )
  )
  /*END*/

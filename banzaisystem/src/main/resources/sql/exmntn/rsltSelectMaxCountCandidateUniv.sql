SELECT
  COALESCE(MAX(CNT), 0)
FROM
  (
    SELECT
      COUNT(1) AS CNT
      , EXAMCD
    FROM
      CANDIDATEUNIV
    WHERE
      INDIVIDUALID = /*id*/1
      AND EXAMYEAR = /*exam.examYear*/'2015'
      AND EXAMCD <> /*exam.examCd*/'01'
      AND SECTION = '2'
      AND DISP_FLG = '1'
      AND UNIVCD || FACULTYCD || DEPTCD NOT IN /*univKeyList*/('1025010101')
    GROUP BY
      EXAMCD
  )

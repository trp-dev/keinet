SELECT
   C.INDIVIDUALID            AS individualId
  ,C.EXAMYEAR                AS examYear
  ,C.EXAMDIV                 AS examDiv
  ,ifnull(C.EXAMCD1, '')                 AS cefrExamCd1
  ,ifnull(C.EXAMCD2, '')                 AS cefrExamCd2
  ,ifnull(RTRIM(E1.ENGPTNAME,'　 '), '')   AS cefrExamName1
  ,ifnull(RTRIM(E2.ENGPTNAME,'　 '), '')   AS cefrExamName2
  ,ifnull(RTRIM(E1.ENGPTNAME_ABBR,'　 '), '')   AS cefrExamNameAbbr1
  ,ifnull(RTRIM(E2.ENGPTNAME_ABBR,'　 '), '')   AS cefrExamNameAbbr2
  ,ifnull(C.EXAMLEVELCD1, '')            AS cefrExamLevel1
  ,ifnull(C.EXAMLEVELCD2, '')            AS cefrExamLevel2
  ,ifnull(RTRIM(ED1.ENGPTLEVELNAME,'　 '), '')  AS cefrExamLevelName1
  ,ifnull(RTRIM(ED2.ENGPTLEVELNAME,'　 '), '')  AS cefrExamLevelName2
  ,ifnull(RTRIM(ED1.ENGPTLEVELNAME_ABBR,'　 '), '')  AS cefrExamLevelNameAbbr1
  ,ifnull(RTRIM(ED2.ENGPTLEVELNAME_ABBR,'　 '), '')  AS cefrExamLevelNameAbbr2
  ,ifnull(C.CEFRLEVELCD1, '')            AS cefrLevelCd1
  ,ifnull(C.CEFRLEVELCD2, '')            AS cefrLevelCd2
  ,ifnull(CI1.CERFLEVELNAME, '')         AS cefrLevel1
  ,ifnull(CI2.CERFLEVELNAME, '')         AS cefrLevel2
  ,ifnull(CI1.CERFLEVELNAME_ABBR, '')    AS cefrLevelAbbr1
  ,ifnull(CI2.CERFLEVELNAME_ABBR, '')    AS cefrLevelAbbr2
  ,ifnull(C.SCORE1, 0)                   AS cefrScore1
  ,ifnull(C.SCORE2, 0)                   AS cefrScore2
  ,ifnull(C.SCORE_CORRECTFLG1, '')       AS scoreCorrectFlg1
  ,ifnull(C.SCORE_CORRECTFLG2, '')       AS scoreCorrectFlg2
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG1, '') AS cefrLevelCdCorrectFlg1
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG2, '') AS cefrLevelCdCorrectFlg2
  ,ifnull(C.EXAM_RESULT_CORRECTFLG1, '') AS examResultCorrectFlg1
  ,ifnull(C.EXAM_RESULT_CORRECTFLG2, '') AS examResultCorrectFlg2
  ,ifnull(C.EXAM_RESULT1, '')            AS examResult1
  ,ifnull(C.EXAM_RESULT2, '')            AS examResult2
  ,ifnull(C.UNKNOWN_SCORE1, '')          AS unknownScore1
  ,ifnull(C.UNKNOWN_SCORE2, '')          AS unknownScore2
  ,ifnull(C.UNKNOWN_CEFR1, '')           AS unknownCefr1
  ,ifnull(C.UNKNOWN_CEFR2, '')           AS unknownCefr2
FROM
  CEFR_SCORE C
LEFT JOIN ENGPTINFO E1
ON E1.ENGPTCD = C.EXAMCD1
LEFT JOIN ENGPTINFO E2
ON E2.ENGPTCD = C.EXAMCD2
LEFT JOIN CEFR_INFO CI1
ON CI1.CEFRLEVELCD = C.CEFRLEVELCD1
LEFT JOIN CEFR_INFO CI2
ON CI2.CEFRLEVELCD = C.CEFRLEVELCD2
LEFT JOIN ENGPTDETAILINFO ED1
ON ED1.ENGPTCD = C.EXAMCD1
AND ED1.ENGPTLEVELCD = C.EXAMLEVELCD1
LEFT JOIN ENGPTDETAILINFO ED2
ON ED2.ENGPTCD = C.EXAMCD2
AND ED2.ENGPTLEVELCD = C.EXAMLEVELCD2
WHERE
  C.INDIVIDUALID = /*id*/1
  AND C.EXAMYEAR = /*exam.examYear*/'2013'
  AND C.EXAMDIV = /*exam.examCd*/'66'

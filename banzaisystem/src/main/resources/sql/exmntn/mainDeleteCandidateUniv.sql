DELETE
FROM
  CANDIDATEUNIV
WHERE
  INDIVIDUALID = /*id*/1
  AND UNIVCD = /*univKey.univCd*/'x'
  AND FACULTYCD = /*univKey.facultyCd*/'x'
  AND DEPTCD = /*univKey.deptCd*/'x'
  AND (
    (
      SECTION = '1'
      AND EXAMYEAR = /*exam.examYear*/'x'
      AND EXAMCD = /*exam.examCd*/'x'
    )
    OR (
      SECTION = '2'
      AND EXISTS (
        SELECT
          1
        FROM
          CANDIDATEUNIV A
        WHERE
          A.INDIVIDUALID = CANDIDATEUNIV.INDIVIDUALID
          AND A.UNIVCD = CANDIDATEUNIV.UNIVCD
          AND A.FACULTYCD = CANDIDATEUNIV.FACULTYCD
          AND A.DEPTCD = CANDIDATEUNIV.DEPTCD
          AND A.EXAMYEAR = /*exam.examYear*/'x'
          AND A.EXAMCD = /*exam.examCd*/'x'
          AND A.SECTION = '2'
          AND A.DISP_FLG = '1'
      )
    )
  )

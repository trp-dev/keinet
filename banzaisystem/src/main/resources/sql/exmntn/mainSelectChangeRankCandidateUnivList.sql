SELECT
  UNIVCD
  , FACULTYCD
  , DEPTCD
  , DISP_NUMBER AS DISPNUMBER
FROM
  CANDIDATEUNIV
WHERE
  INDIVIDUALID = /*id*/''
  AND EXAMYEAR = /*exam.examYear*/''
  AND EXAMCD = /*exam.examCd*/''
  AND DISP_FLG = '1'
  AND DISP_NUMBER
  /*IF isUp*/
  <=
  --ELSE >=
  /*END*/
  (
    SELECT
      DISP_NUMBER
    FROM
      CANDIDATEUNIV
    WHERE
      INDIVIDUALID = /*id*/''
      AND EXAMYEAR = /*exam.examYear*/''
      AND EXAMCD = /*exam.examCd*/''
      AND UNIVCD = /*univKey.univCd*/'x'
      AND FACULTYCD = /*univKey.facultyCd*/'x'
      AND DEPTCD = /*univKey.deptCd*/'x'
      AND DISP_FLG = '1'
  )
ORDER BY
  DISPNUMBER/*IF isUp*/ DESC/*END*/
LIMIT
  2

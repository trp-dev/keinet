INSERT
INTO CEFR_SCORE(
  INDIVIDUALID
  , EXAMYEAR
  , EXAMDIV
  , EXAMCD1
  , EXAMLEVELCD1
  , SCORE1
  , SCORE_CORRECTFLG1
  , CEFRLEVELCD1
  , CEFRLEVELCD_CORRECTFLG1
  , EXAM_RESULT1
  , EXAM_RESULT_CORRECTFLG1
  , UNKNOWN_SCORE1
  , UNKNOWN_CEFR1
  , EXAMCD2
  , EXAMLEVELCD2
  , SCORE2
  , SCORE_CORRECTFLG2
  , CEFRLEVELCD2
  , CEFRLEVELCD_CORRECTFLG2
  , EXAM_RESULT2
  , EXAM_RESULT_CORRECTFLG2
  , UNKNOWN_SCORE2
  , UNKNOWN_CEFR2
)
VALUES (
  /*individualId*/0
  , /*examYear*/'x'
  , /*examDiv*/'x'
  , /*examCd1*/'x'
  , /*cefrExamLevel1*/'x'
  , /*cefrScore1*/0.0
  , /*scoreCorrectFlg1*/'x'
  , /*cefrLevelCD1*/'x'
  , /*cefrLevelCdCorrectFlg1*/'x'
  , /*examResult1*/'x'
  , /*examResultCorrectFlg1*/'x'
  , /*unknownScore1*/'x'
  , /*unknownCefr1*/'x'
  , /*examCd2*/'x'
  , /*cefrExamLevel2*/'x'
  , /*cefrScore2*/0.0
  , /*scoreCorrectFlg2*/'x'
  , /*cefrLevelCD2*/'x'
  , /*cefrLevelCdCorrectFlg2*/'x'
  , /*examResult2*/'x'
  , /*examResultCorrectFlg2*/'x'
  , /*unknownScore2*/'x'
  , /*unknownCefr2*/'x'
)

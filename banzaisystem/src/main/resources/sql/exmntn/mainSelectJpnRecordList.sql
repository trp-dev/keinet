SELECT
    J.INDIVIDUALID   AS individualid
  , J.EXAMYEAR       AS examyear
  , J.EXAMDIV        AS examdiv
  , J.SUBCD          AS subcd
  , J.DESC_Q1_RATING AS question1
  , J.DESC_Q2_RATING AS question2
  , J.DESC_Q3_RATING AS question3
  , J.DESC_TOTAL_RATING AS jpnTotal
FROM
  JPN_DESC_EVALUATION J
WHERE
  INDIVIDUALID = /*id*/1
  AND EXAMYEAR = /*exam.examYear*/'2013'
  AND EXAMDIV = /*exam.examCd*/'66'

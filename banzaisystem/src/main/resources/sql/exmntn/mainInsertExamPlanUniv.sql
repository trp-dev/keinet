INSERT 
INTO EXAMPLANUNIV( 
  INDIVIDUALID
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , PROVINCES_FLG
  , CANDIDATERANK
  , YEAR
  , EXAMDIV
) 
SELECT
  /*id*/0
  , /*univCd*/'x'
  , /*facultyCd*/'x'
  , /*deptCd*/'x'
  , '0'
  , COALESCE(MAX(CANDIDATERANK) + 1, 1)
  , /*year*/'x'
  , /*examDiv*/'x' 
FROM
  EXAMPLANUNIV 
WHERE
  INDIVIDUALID = /*id*/0

SELECT
    R.INDIVIDUALID
  , R.SUBCD
  , E1.EXAMCD
  , R.SCORE
  , R.CVSSCORE
  , R.DEVIATION AS CDEVIATION
  , R.SCOPE
  , COALESCE(S.DISPSEQUENCE, ABS(R.SUBCD)) AS DISPSEQUENCE
  , COALESCE(CAST(S.SUBALLOTPNT AS TEXT), '9999.9') AS SUBJECTALLOTPNT
FROM
  SUBRECORD_I R
  INNER JOIN HISTORYINFO H
     ON H.INDIVIDUALID = R.INDIVIDUALID
  INNER JOIN EXAMINATION E
     ON E.EXAMYEAR = R.EXAMYEAR
    AND E.EXAMCD = R.EXAMCD
  LEFT JOIN EXAMSUBJECT S
     ON S.EXAMYEAR = R.EXAMYEAR
    AND S.EXAMCD = R.EXAMCD
    AND S.SUBCD = R.SUBCD
  INNER JOIN (
    SELECT
        R2.INDIVIDUALID
      , R2.EXAMYEAR
      , R2.EXAMCD
      , MIN(E.DISPSEQUENCE)
    FROM
      SUBRECORD_I R2
      LEFT JOIN (
        SELECT
          *
        FROM
          EXAMINATION A
        WHERE
          EXISTS (
            SELECT
              1
            FROM
              EXAMINATION B
            WHERE
              B.EXAMYEAR = /*examYear*/'2013'
              AND B.EXAMCD = /*examCd*/'66'
              AND B.EXAMYEAR = A.EXAMYEAR
              AND (
                A.EXAMCD = B.DOCKINGEXAMCD
                OR B.EXAMCD = A.DOCKINGEXAMCD
              )
          )
      ) E
        ON R2.EXAMYEAR = E.EXAMYEAR
        AND R2.EXAMCD = E.EXAMCD
    WHERE
      R2.EXAMYEAR = E.EXAMYEAR
      AND R2.EXAMCD = E.EXAMCD
    GROUP BY
      R2.INDIVIDUALID
      , R2.EXAMYEAR
  ) E1
     ON R.INDIVIDUALID = E1.INDIVIDUALID
    AND R.EXAMYEAR = E1.EXAMYEAR
    AND R.EXAMCD = E1.EXAMCD
/*BEGIN*/
WHERE
  /*IF grade == null*/
  H.GRADE IS NULL
  /*END*/
  /*IF grade == -2*/
  H.GRADE >= 4
  /*END*/
  /*IF grade != null && grade >= 0*/
  H.GRADE = /*grade*/1
  /*END*/
  /*IF class != ""*/
    /*IF class == null*/
  AND H.CLASS IS NULL
  --ELSE AND H.CLASS = /*class*/'x'
    /*END*/
  /*END*/
  AND R.SUBCD < '7000'
/*END*/
ORDER BY
  DISPSEQUENCE

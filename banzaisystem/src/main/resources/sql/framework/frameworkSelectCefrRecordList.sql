SELECT
   C.INDIVIDUALID            AS individualId
  ,C.EXAMYEAR                AS examYear
  ,C.EXAMDIV                 AS examDiv
  ,ifnull(C.EXAMCD1,'')      AS cefrExamCd1
  ,ifnull(C.EXAMCD2,'')      AS cefrExamCd2
  ,ifnull(E1.ENGPTNAME,'')   AS cefrExamName1
  ,ifnull(E2.ENGPTNAME,'')   AS cefrExamName2
  ,ifnull(C.CEFRLEVELCD1,'') AS cefrLevelCd1
  ,ifnull(C.CEFRLEVELCD2,'') AS cefrLevelCd2
  ,ifnull(RTRIM(ED1.ENGPTLEVELNAME_ABBR),'')  AS cefrExamLevelName1
  ,ifnull(RTRIM(ED2.ENGPTLEVELNAME_ABBR),'')  AS cefrExamLevelName2
  ,ifnull(CI1.CERFLEVELNAME_ABBR,'') AS cefrLevel1
  ,ifnull(CI2.CERFLEVELNAME_ABBR,'') AS cefrLevel2
  ,ifnull(C.SCORE1,0)        AS cefrScore1
  ,ifnull(C.SCORE2,0)        AS cefrScore2
  ,ifnull(C.SCORE_CORRECTFLG1,'')       AS score_CorrectFlg1
  ,ifnull(C.SCORE_CORRECTFLG2,'')       AS score_CorrectFlg2
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG1,'') AS cefrLevelCd_CorrectFlg1
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG2,'') AS cefrLevelCd_CorrectFlg2
  ,ifnull(C.EXAM_RESULT1,'') AS exam_Result1
  ,ifnull(C.EXAM_RESULT2,'') AS exam_Result2
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG1,'') AS cefrLevelCd_CorrectFlg1
  ,ifnull(C.CEFRLEVELCD_CORRECTFLG2,'') AS cefrLevelCd_CorrectFlg2
FROM
  CEFR_SCORE C
LEFT JOIN ENGPTINFO E1
ON E1.ENGPTCD = C.EXAMCD1
LEFT JOIN ENGPTINFO E2
ON E2.ENGPTCD = C.EXAMCD2
LEFT JOIN CEFR_INFO CI1
ON CI1.CEFRLEVELCD = C.CEFRLEVELCD1
LEFT JOIN CEFR_INFO CI2
ON CI2.CEFRLEVELCD = C.CEFRLEVELCD2
LEFT JOIN ENGPTDETAILINFO ED1
ON ED1.ENGPTCD = C.EXAMCD1
AND ED1.ENGPTLEVELCD = C.EXAMLEVELCD1
LEFT JOIN ENGPTDETAILINFO ED2
ON ED2.ENGPTCD = C.EXAMCD2
AND ED2.ENGPTLEVELCD = C.EXAMLEVELCD2
INNER JOIN HISTORYINFO H
ON H.INDIVIDUALID = C.INDIVIDUALID
/*BEGIN*/
WHERE
  /*IF grade == null*/
  H.GRADE IS NULL
  /*END*/
  /*IF grade == -2*/
  H.GRADE >= 4
  /*END*/
  /*IF grade != null && grade >= 0*/
  H.GRADE = /*grade*/1
  /*END*/
  /*IF class != ""*/
    /*IF class == null*/
  AND H.CLASS IS NULL
  --ELSE AND H.CLASS = /*class*/'x'
    /*END*/
  /*END*/
  AND C.EXAMYEAR = /*examYear*/'2013'
  AND C.EXAMDIV = /*examCd*/'66'
/*END*/

SELECT
    J.INDIVIDUALID   AS individualid
  , J.EXAMYEAR       AS examyear
  , J.EXAMDIV        AS examdiv
  , J.SUBCD          AS subcd
  , J.DESC_Q1_RATING AS question1
  , J.DESC_Q2_RATING AS question2
  , J.DESC_Q3_RATING AS question3
  , J.DESC_TOTAL_RATING AS jpnTotal
  , COALESCE(CAST(S.SUBALLOTPNT AS TEXT), '9999.9') AS SUBALLOTPNT
FROM
  JPN_DESC_EVALUATION J
INNER JOIN HISTORYINFO H
ON H.INDIVIDUALID = J.INDIVIDUALID
  LEFT JOIN EXAMSUBJECT S
     ON S.EXAMYEAR = J.EXAMYEAR
    AND S.EXAMCD = J.EXAMDIV
    AND S.SUBCD = J.SUBCD
/*BEGIN*/
WHERE
  /*IF grade == null*/
  H.GRADE IS NULL
  /*END*/
  /*IF grade == -2*/
  H.GRADE >= 4
  /*END*/
  /*IF grade != null && grade >= 0*/
  H.GRADE = /*grade*/1
  /*END*/
  /*IF class != ""*/
    /*IF class == null*/
  AND H.CLASS IS NULL
  --ELSE AND H.CLASS = /*class*/'x'
    /*END*/
  /*END*/
  AND J.EXAMYEAR = /*examYear*/'2013'
  AND J.EXAMDIV = /*examCd*/'66'
  AND J.SUBCD = '3915'
/*END*/

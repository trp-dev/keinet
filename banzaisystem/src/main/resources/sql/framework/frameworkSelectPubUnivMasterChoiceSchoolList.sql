SELECT
  ENTEXAMDIV
  , UNICDBRANCHCD
  , TOTALALLOTPNT
  , TOTALALLOTPNT_NREP AS TOTALALLOTPNTNREP
  , ENTEXAMFULLPNT
  , ENTEXAMFULLPNT_NREP AS ENTEXAMFULLPNTNREP
  , SUBCOUNT
  , SCORE_CONV_PATTERN_ENGPT AS CENTERCVSSCOREENGPT
  , ADDPT_DIV_ENGPT AS ADDPTDIVENGPT
  , ADDPT_PT_ENGPT  AS ADDPTENGPT
  , SCORE_CONV_PATTERN_KOKUGO_DESC AS CENTERCVSSCOREENGPT
  , ADDPT_DIV_KOKUGO_DESC AS ADDPTDIVENGPT
  , ADDPT_PT_KOKUGO_DESC  AS ADDPTENGPT
FROM
  UNIVMASTER_CHOICESCHOOL_PUB
WHERE
  UNIVCD = /*univCd*/'x'
  AND FACULTYCD = /*facultyCd*/'x'
  AND DEPTCD = /*deptCd*/'x'

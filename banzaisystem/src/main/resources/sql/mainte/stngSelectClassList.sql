SELECT DISTINCT
  H.CLASS AS CLS
FROM
  HISTORYINFO H
WHERE
  H.YEAR = /*year*/'2013'
  /*IF grade == null*/
  AND GRADE IS NULL
  /*END*/
  /*IF grade != null && grade >= 0*/
  AND GRADE = /*grade*/1
  /*END*/
  AND EXISTS (
    SELECT
      1
    FROM
      INDIVIDUALRECORD R
    WHERE
      R.INDIVIDUALID = H.INDIVIDUALID
      AND R.EXAMYEAR = H.YEAR
      /*IF examCd != null*/
      AND R.EXAMCD = /*examCd*/'01'
      /*END*/
  )
ORDER BY
  H.CLASS IS NULL
  , H.CLASS

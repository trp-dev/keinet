UPDATE CANDIDATEUNIV
SET
  DISP_FLG = '1'
  , DISP_NUMBER = DISP_NUMBER + (
    SELECT
      COALESCE(MAX(B.DISP_NUMBER), 0)
    FROM
      CANDIDATEUNIV B
    WHERE
      B.INDIVIDUALID = CANDIDATEUNIV.INDIVIDUALID
      AND B.EXAMYEAR = CANDIDATEUNIV.EXAMYEAR
      AND B.EXAMCD = CANDIDATEUNIV.EXAMCD
      AND B.SECTION = '2'
      AND B.DISP_FLG = '1'
  )
WHERE
  INDIVIDUALID = /*id*/0
  AND EXAMYEAR = /*exam.examYear*/'2012'
  AND EXAMCD = /*exam.examCd*/'01'
  AND SECTION = '2'
  AND DISP_FLG = '0'

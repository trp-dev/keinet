INSERT
INTO CANDIDATERATING(
  INDIVIDUALID
  , EXAMYEAR
  , EXAMCD
  , CANDIDATERANK
  , UNIVCD
  , FACULTYCD
  , DEPTCD
  , RATINGCVSSCORE
  , RATINGDEVIATION
  , RATINGPOINT
  , CENTERRATING
  , SECONDRATING
  , TOTALRATING
  , FULLPOINT
  , INCLUDEFULLPOINT
  , INCLUDERATINGCVSSCORE
  , INCLUDERATINGDEVIATION
  , INCLUDERATING
  , APP_QUAL_JUDGE
  , APP_QUAL_NOTICE
)
VALUES (
  /*id*/1
  , /*examYear*/'x'
  , /*examCd*/'x'
  , /*rank*/1
  , /*univCd*/'x'
  , /*facultyCd*/'x'
  , /*deptCd*/'x'
  , /*score*/'x'
  , /*deviation*/'x'
  , /*tPoint*/'x'
  , /*cRating*/'x'
  , /*sRating*/'x'
  , /*tRating*/'x'
  , /*fullpoint*/'x'
  , /*fullpointinc*/'x'
  , /*scoreinc*/'x'
  , /*deviationinc*/'x'
  , /*ratinginc*/'x'
  , /*judge*/'x'
  , /*notice*/'x'
)

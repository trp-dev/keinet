DELETE
FROM
  INDIVIDUALRECORD
WHERE
  EXAMYEAR = /*year*/'2012'
  /*IF examCd != null*/
  AND EXAMCD = /*examCd*/'01'
  /*END*/
  AND EXISTS (
    SELECT
      1
    FROM
      HISTORYINFO
    WHERE
      HISTORYINFO.INDIVIDUALID = INDIVIDUALRECORD.INDIVIDUALID
      AND HISTORYINFO.YEAR = INDIVIDUALRECORD.EXAMYEAR
      /*IF grade == null*/
      AND HISTORYINFO.GRADE IS NULL
      /*END*/
      /*IF grade != null && grade >= 0*/
      AND HISTORYINFO.GRADE = /*grade*/3
      /*END*/
      /*IF class != ""*/
        /*IF class == null*/
        AND HISTORYINFO.CLASS IS NULL
        --ELSE AND HISTORYINFO.CLASS = /*class*/'0A'
        /*END*/
      /*END*/
  )

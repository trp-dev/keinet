DELETE
FROM
  HISTORYINFO
WHERE
  NOT EXISTS (
    SELECT
      1
    FROM
      INDIVIDUALRECORD
    WHERE
      INDIVIDUALRECORD.INDIVIDUALID = HISTORYINFO.INDIVIDUALID
  )

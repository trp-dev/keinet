SELECT
  B.INDIVIDUALID AS ID
  , B.NAME_KANA AS KANANAME
FROM
  BASICINFO B
WHERE
  EXISTS (
    SELECT
      1
    FROM
      HISTORYINFO H
    WHERE
      H.INDIVIDUALID = B.INDIVIDUALID
      AND H.YEAR = /*year*/'2012'
      /*IF grade == null*/
        AND H.GRADE IS NULL
        --ELSE AND H.GRADE = /*grade*/3
      /*END*/
      /*IF class == null*/
        AND H.CLASS IS NULL
        --ELSE AND SUBSTR('00' || TRIM(H.CLASS), -2, 2) = /*class*/'01'
      /*END*/
      /*IF classNo == null*/
        AND H.CLASS_NO IS NULL
        --ELSE AND ROUND(H.CLASS_NO) = ROUND(/*classNo*/'00001')
      /*END*/
  )

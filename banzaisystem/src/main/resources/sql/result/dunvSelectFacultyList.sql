SELECT DISTINCT
  COALESCE(FACULTYNAME_ABBR, '　') AS LABEL
  , FACULTYCD AS VALUE
FROM
  UNIVMASTER_BASIC
WHERE
  UNIVCD = /*univCd*/'x'
ORDER BY
  VALUE

SELECT DISTINCT
  A.UNIVCD
  , A.SUCANNDATE
  , B.UNINAME_ABBR AS UNIVNAME
FROM
  (
    SELECT DISTINCT
      P.UNIVCD
      , D.SUCANNDATE
    FROM
      EXAMPLANUNIV P
      INNER JOIN EXAMSCHEDULEDETAIL D
        ON D.UNIVCD = P.UNIVCD
        AND D.FACULTYCD = P.FACULTYCD
        AND D.DEPTCD = P.DEPTCD
        AND D.SUCANNDATE IS NOT NULL
    WHERE
      EXISTS (
        SELECT
          1
        FROM
          HISTORYINFO H
        WHERE
          H.INDIVIDUALID = P.INDIVIDUALID
          AND H.YEAR = P.YEAR
          /*IF grade == null*/
          AND H.GRADE IS NULL
          --ELSE AND H.GRADE = /*grade*/3
          /*END*/
          /*IF class == null*/
          AND H.CLASS IS NULL
          --ELSE AND H.CLASS = /*class*/'0A'
          /*END*/
      )
  ) A
  INNER JOIN UNIVMASTER_BASIC B
    ON B.UNIVCD = A.UNIVCD
ORDER BY
  A.UNIVCD

SELECT
  COUNT(1)
FROM
  HISTORYINFO
WHERE
  YEAR = /*nendo*/'2014'
  AND GRADE = /*grade*/2
  AND CLASS = /*class*/'01'
  AND ROUND(CLASS_NO) = ROUND(/*classNo*/'00001')
  /*IF id !=null*/
  AND INDIVIDUALID <> /*id*/0
  /*END*/

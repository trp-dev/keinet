SELECT
  EXAMYEAR
  , EXAMCD
FROM
  (
    SELECT
      EXAMYEAR
      , EXAMCD
      , (
        SELECT
          COUNT(1)
        FROM
          EXAMINATION B
        WHERE
          B.EXAMYEAR = A.EXAMYEAR
          AND B.EXAMDIV <= /*examDiv*/'03'
          AND B.OUT_DATAOPENDATE || B.EXAMTYPECD >= A.OUT_DATAOPENDATE || A.EXAMTYPECD
          AND EXISTS (
            SELECT
              1
            FROM
              INDIVIDUALRECORD R
            WHERE
              R.EXAMYEAR = B.EXAMYEAR
              AND R.EXAMCD = B.EXAMCD
              AND R.INDIVIDUALID = /*id*/1
          )
      ) DISPNUM
    FROM
      EXAMINATION A
    WHERE
      A.EXAMYEAR = /*year*/'2015'
      AND A.EXAMDIV <= /*examDiv*/'03'
      AND EXISTS (
        SELECT
          1
        FROM
          INDIVIDUALRECORD R
        WHERE
          R.EXAMYEAR = A.EXAMYEAR
          AND R.EXAMCD = A.EXAMCD
          AND R.INDIVIDUALID = /*id*/1
      )
    UNION ALL
    SELECT
      EXAMYEAR
      , EXAMCD
      , (
        SELECT
          COUNT(1)
        FROM
          EXAMINATION B
        WHERE
          B.EXAMYEAR = A.EXAMYEAR
          AND B.EXAMDIV <= /*examDiv*/'03'
          AND B.OUT_DATAOPENDATE || B.EXAMTYPECD >= A.OUT_DATAOPENDATE || A.EXAMTYPECD
      ) DISPNUM
    FROM
      EXAMINATION A
    WHERE
      A.EXAMYEAR = /*year*/'2015'
      AND A.EXAMDIV <= /*examDiv*/'03'
      AND NOT EXISTS (
        SELECT
          1
        FROM
          INDIVIDUALRECORD R
        WHERE
          R.EXAMYEAR = A.EXAMYEAR
          AND R.INDIVIDUALID = /*id*/1
      )
  )
WHERE
  DISPNUM = 1

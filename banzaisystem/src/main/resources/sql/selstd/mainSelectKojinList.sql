SELECT
  B.INDIVIDUALID
  , H.GRADE
  , H.CLASS AS CLS
  , H.CLASS_NO AS CLASSNO
  , B.NAME_KANA AS NAMEKANA
FROM
  BASICINFO B
  INNER JOIN HISTORYINFO H
    ON H.INDIVIDUALID = B.INDIVIDUALID
    AND H.YEAR = /*year*/'2013'
/*BEGIN*/
WHERE
  /*IF grade == null*/
  H.GRADE IS NULL
  /*END*/
  /*IF grade == -2*/
  H.GRADE >= 4
  /*END*/
  /*IF grade != null && grade >= 0*/
  H.GRADE = /*grade*/1
  /*END*/
  /*IF class != ""*/
    /*IF class == null*/
    AND H.CLASS IS NULL
    --ELSE AND H.CLASS = /*class*/'x'
    /*END*/
  /*END*/
/*END*/

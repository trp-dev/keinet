SELECT
  COUNT(1)
FROM
  INDIVIDUALRECORD R1
WHERE
  R1.INDIVIDUALID = /*mainId*/0
  AND R1.EXAMYEAR = /*year*/'2012'
  AND EXISTS (
    SELECT
      1
    FROM
      INDIVIDUALRECORD R2
    WHERE
      R2.INDIVIDUALID = /*subId*/0
      AND R2.EXAMYEAR = R1.EXAMYEAR
      AND R2.EXAMCD = R1.EXAMCD
  )

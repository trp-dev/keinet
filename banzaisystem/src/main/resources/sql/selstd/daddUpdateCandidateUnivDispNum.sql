UPDATE CANDIDATEUNIV
SET
  DISP_NUMBER = DISP_NUMBER + /*maxDispNum*/0
WHERE
  (
    INDIVIDUALID = /*subId*/0
    OR (
      INDIVIDUALID = /*mainId*/0
      AND EXISTS (
        SELECT
          1
        FROM
          INDIVIDUALRECORD
        WHERE
          INDIVIDUALRECORD.INDIVIDUALID = /*subId*/0
          AND INDIVIDUALRECORD.EXAMYEAR = CANDIDATEUNIV.EXAMYEAR
          AND INDIVIDUALRECORD.EXAMCD = CANDIDATEUNIV.EXAMCD
      )
    )
  )
  AND NOT EXISTS (
    SELECT
      1
    FROM
      INDIVIDUALRECORD
    WHERE
      INDIVIDUALRECORD.INDIVIDUALID = CANDIDATEUNIV.INDIVIDUALID
      AND INDIVIDUALRECORD.EXAMYEAR = CANDIDATEUNIV.EXAMYEAR
      AND INDIVIDUALRECORD.EXAMCD = CANDIDATEUNIV.EXAMCD
  )

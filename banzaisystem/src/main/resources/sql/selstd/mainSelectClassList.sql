SELECT DISTINCT
  CLASS
FROM
  HISTORYINFO
/*BEGIN*/
WHERE
  /*IF grade == null*/
  GRADE IS NULL
  /*END*/
  /*IF grade == -2*/
  GRADE >= 4
  /*END*/
  /*IF grade != null && grade >= 0*/
  GRADE = /*grade*/1
  /*END*/
  AND YEAR = /*year*/'2013'
/*END*/
ORDER BY
  CLASS IS NULL
  , CLASS

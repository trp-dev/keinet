SELECT
    J.INDIVIDUALID      AS INDIVIDUALID
  , J.DESC_Q1_RATING    AS DESCRATING1
  , J.DESC_Q2_RATING    AS DESCRATING2
  , J.DESC_Q3_RATING    AS DESCRATING3
  , J.DESC_TOTAL_RATING AS DESCRATINGTOTAL
  , HI.GRADE            AS GRADE
  , HI.CLASS            AS CLS
  , HI.CLASS_NO         AS CLASSNO
  , BI.NAME_KANA        AS NAMEKANA
  , J.EXAMDIV           AS EXAMCD
  , EX.OUT_DATAOPENDATE AS EXAMOPENDATE
FROM
  JPN_DESC_EVALUATION J
  INNER JOIN HISTORYINFO HI
    ON HI.INDIVIDUALID = J.INDIVIDUALID
    AND HI.YEAR = /*year*/'2012'
  LEFT JOIN BASICINFO BI
    ON BI.INDIVIDUALID = J.INDIVIDUALID
  LEFT JOIN EXAMINATION EX
    ON EX.EXAMYEAR = J.EXAMYEAR
    AND EX.EXAMCD = J.EXAMDIV
WHERE
  /*$individualid*/
  AND J.EXAMYEAR = /*year*/'2019'

ORDER BY
  J.INDIVIDUALID
 ,J.EXAMYEAR
 ,J.EXAMDIV

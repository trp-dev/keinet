SELECT
    C1.INDIVIDUALID       AS INDIVIDUALID
  , ifnull(C1.EXAMCD1,'') AS ENGPTEXAMCD1
  , ifnull(E1.ENGPTNAME,'')      AS ENGPTNAME1
  , ifnull(C1.EXAMLEVELCD1,'')   AS ENGPTLEVELCD1
  , ifnull(E2.ENGPTLEVELNAME,'') AS ENGPTLEVELCDNAME1
  , ifnull(C1.SCORE1,0)   AS SCORE1
  , ifnull(C1.SCORE_CORRECTFLG1,'')  AS SCORECORRECTFLG1
  , ifnull(C1.CEFRLEVELCD1,'')       AS CEFRLEVELCD1
  , ifnull(C2.CERFLEVELNAME_ABBR,'') AS CEFRLEVELNAME1
  , ifnull(C1.CEFRLEVELCD_CORRECTFLG1,'') AS CEFRLEVELCORRECTFLG1
  , ifnull(C1.EXAM_RESULT1,'')       AS RESULT1
  , ifnull(C1.EXAM_RESULT_CORRECTFLG1,'') AS RESULTCORRECTFLG1
  , ifnull(C1.EXAMCD2,'')   AS ENGPTEXAMCD2
  , ifnull(E3.ENGPTNAME,'') AS ENGPTNAME2
  , ifnull(C1.EXAMLEVELCD2,'')   AS ENGPTLEVELCD2
  , ifnull(E4.ENGPTLEVELNAME,'') AS ENGPTLEVELCDNAME2
  , ifnull(C1.SCORE2,0)   AS SCORE2
  , ifnull(C1.SCORE_CORRECTFLG2,'')  AS SCORECORRECTFLG2
  , ifnull(C1.CEFRLEVELCD2,'')       AS CEFRLEVELCD2
  , ifnull(C3.CERFLEVELNAME_ABBR,'') AS CEFRLEVELNAME2
  , ifnull(C1.CEFRLEVELCD_CORRECTFLG2,'') AS CEFRLEVELCORRECTFLG2
  , ifnull(C1.EXAM_RESULT2,'') AS RESULT2
  , ifnull(C1.EXAM_RESULT_CORRECTFLG2,'') AS RESULTCORRECTFLG2
  , HI.GRADE              AS GRADE
  , HI.CLASS              AS CLS
  , HI.CLASS_NO           AS CLASSNO
  , BI.NAME_KANA          AS NAMEKANA
  , C1.EXAMDIV            AS EXAMCD
  , EX.OUT_DATAOPENDATE   AS EXAMOPENDATE
FROM
  CEFR_SCORE C1
  INNER JOIN HISTORYINFO HI
    ON HI.INDIVIDUALID = C1.INDIVIDUALID
    AND HI.YEAR = /*year*/'2012'
  LEFT JOIN ENGPTINFO E1
    ON E1.ENGPTCD = C1.EXAMLEVELCD1
  LEFT JOIN ENGPTDETAILINFO E2
    ON E2.ENGPTCD = C1.EXAMLEVELCD1
  LEFT JOIN CEFR_INFO C2
    ON C2.CEFRLEVELCD = C1.CEFRLEVELCD1
  LEFT JOIN ENGPTINFO E3
    ON E3.ENGPTCD = C1.EXAMLEVELCD2
  LEFT JOIN ENGPTDETAILINFO E4
    ON E4.ENGPTCD = C1.EXAMLEVELCD2
  LEFT JOIN CEFR_INFO C3
    ON C3.CEFRLEVELCD = C1.CEFRLEVELCD2
  LEFT JOIN BASICINFO BI
    ON BI.INDIVIDUALID = C1.INDIVIDUALID
  LEFT JOIN EXAMINATION EX
    ON EX.EXAMYEAR = C1.EXAMYEAR
    AND EX.EXAMCD = C1.EXAMDIV
WHERE
  /*$individualid*/
  AND C1.EXAMYEAR = /*year*/'2019'

ORDER BY
  C1.INDIVIDUALID
 ,C1.EXAMYEAR
 ,C1.EXAMDIV

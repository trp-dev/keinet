SELECT
  H.GRADE AS GRADE                      -- 学年
  , H.CLASS AS CLS                      -- クラス
  , H.CLASS_NO AS CLASSNO               -- クラス番号
  , B.NAME_KANA AS NAMEKANA             -- カナ氏名
  , C.DISP_NUMBER AS CANDIDATERANK      -- 表示順（志望順位）
  , C.UNIVCD || C.FACULTYCD || C.DEPTCD AS UNIVKEY     -- 10桁コード（大学コード+学部コード+学科コード）
  , U.CHOICECD5 AS CHOICECD5            -- 5桁コード
  , CASE
    WHEN S.SCHEDULECD IN (' ', 'N')
    THEN '　'
    ELSE S.SCHEDULENAME
    END AS SCHEDULENAME                 -- 日程名
  , U.UNINAME_ABBR AS UNIVNAME          -- 大学短縮名
  , U.FACULTYNAME_ABBR AS FACULTYNAME   -- 学部短縮名
  , U.DEPTNAME_ABBR AS DEPTNAME         -- 学科短縮名
FROM
  -- 学籍基本情報
  BASICINFO B
  -- 学籍履歴情報（結合：個人ID）
  INNER JOIN HISTORYINFO H
    ON  H.INDIVIDUALID  = B.INDIVIDUALID
    AND H.YEAR = /*examYear*/'2014'
  -- 志望大学（結合：個人ID）
  INNER JOIN CANDIDATEUNIV C
    ON  C.INDIVIDUALID  = H.INDIVIDUALID
    AND EXAMYEAR = /*examYear*/'2014'
    AND EXAMCD = /*examCd*/'02'
    AND DISP_FLG = '1'
  -- 大学基本情報（結合：大学コード・学部コード・学科コード）
  INNER JOIN UNIVMASTER_BASIC U
    ON  U.UNIVCD        = C.UNIVCD
    AND U.FACULTYCD     = C.FACULTYCD
    AND U.DEPTCD        = C.DEPTCD
  -- 日程マスタ（結合：日程コード）
  INNER JOIN SCHEDULE S
    ON  S.SCHEDULECD    = U.UNIGDIV
WHERE
  B.INDIVIDUALID = /*id*/0
  -- ▼大学指定：大学区分：ここから
  /*IF univDivFlag && (nationalFlag || centerFlag || privateFlag || collegeFlag) */
  AND (
    /*BEGIN*/
      /* ▽大学区分指定：国公立 */
      /*IF nationalFlag*/
        U.UNIDIV IN ('01', '02')
      /*END*/
      /* ▽大学区分指定：センター利用私大・短大 */
      /*IF centerFlag*/
        OR (U.UNIDIV IN ('03', '04', '05') AND S.SCHEDULECD = '1')
      /*END*/
      /* ▽大学区分指定：私立大学 */
      /*IF privateFlag*/
        OR (U.UNIDIV IN ('03') AND S.SCHEDULECD <> '1')
      /*END*/
      /* ▽大学区分指定：短期大学・その他 */
      /*IF collegeFlag*/
        OR (U.UNIDIV IN ('04', '05') AND S.SCHEDULECD <> '1')
        OR (U.UNIDIV IN ('06', '07'))
      /*END*/
    /*END*/
  )
  /*END*/
  -- ▲大学指定：大学区分：ここまで
  -- ▼大学指定：大学名：ここから
  /*IF univNameFlag && univCdCondition != null */
    AND /*$univCdCondition*/
  /*END*/
  -- ▲大学指定：大学名：ここまで
ORDER BY
  CANDIDATERANK

package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の出力データ（個人成績一覧＋志望大学）Beanです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 *
 */
public class SelstdDprtExcelMakerOutputDataBean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** 氏名（カナ） */
    private String nameKana;

    /** 文理コード */
    private String bunriCode;

    /** 個人成績一覧リスト */
    private final List<SelstdDprtExcelMakerRecordBean> recordBeanList;

    /** 個人成績一覧リスト（ドッキング模試用） */
    private final List<SelstdDprtExcelMakerRecordBean> recordDockingBeanList;

    /** 志望大学リスト */
    private final List<SelstdDprtExcelMakerCandidateUnivBean> candidateUnivBeanList;

    /**
     * コンストラクタです。
     */
    public SelstdDprtExcelMakerOutputDataBean() {
        recordBeanList = new ArrayList<SelstdDprtExcelMakerRecordBean>();
        recordDockingBeanList = new ArrayList<SelstdDprtExcelMakerRecordBean>();
        candidateUnivBeanList = new ArrayList<SelstdDprtExcelMakerCandidateUnivBean>();
    }

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * 氏名（カナ）を返します。
     *
     * @return 氏名（カナ）
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 氏名（カナ）をセットします。
     *
     * @param nameKana 氏名（カナ）
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 文理コードを返します。
     *
     * @return 文理コード
     */
    public String getBunriCode() {
        return bunriCode;
    }

    /**
     * 文理コードをセットします。
     *
     * @param bunriCode 文理コード
     */
    public void setBunriCode(String bunriCode) {
        this.bunriCode = bunriCode;
    }

    /**
     * 個人成績一覧リストを返します。
     *
     * @return 個人成績一覧リスト
     */
    public List<SelstdDprtExcelMakerRecordBean> getRecordBeanList() {
        return recordBeanList;
    }

    /**
     * 個人成績一覧リスト（ドッキング模試用）を返します。
     *
     * @return 個人成績一覧リスト（ドッキング模試用）
     */
    public List<SelstdDprtExcelMakerRecordBean> getRecordDockingBeanList() {
        return recordDockingBeanList;
    }

    /**
     * 志望大学リストを返します。
     *
     * @return 志望大学リスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivBean> getCandidateUnivBeanList() {
        return candidateUnivBeanList;
    }

}

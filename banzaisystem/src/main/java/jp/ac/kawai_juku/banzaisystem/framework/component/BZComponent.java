package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * コンポーネントのインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface BZComponent {

    /**
     * コンポーネントを初期化します。
     *
     * @param field コンポーネントのフィールド
     * @param view コンポーネントが宣言されているView
     */
    void initialize(Field field, AbstractView view);

}

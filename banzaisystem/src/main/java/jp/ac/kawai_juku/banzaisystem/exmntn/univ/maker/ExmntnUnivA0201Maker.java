package jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE_DISP;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_NONE;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXCLUDE_EXAM_SECOND;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.HYPHEN;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCandidateDetailBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.service.ExmntnDistService;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.service.ExmntnDlcsService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0201InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailCenterSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSecondSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.service.ExmntnUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivExcelUtil;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivUtil;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.hssf.usermodel.HSSFCell;

/**
 *
 * Excel帳票メーカー(志望大学詳細)です。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A02_01)
public class ExmntnUnivA0201Maker extends BaseExcelMaker<ExmntnUnivA0201InBean> {

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル：クラス番号 */
    private static final String TITLE_CLSNO = "クラス番号：";

    /** タイトル：氏名 */
    private static final String TITLE_NAME = "　氏名：";

    /** タイトル：志望順位 */
    private static final String TITLE_SHIBOUJYUNI = "志望順位：";

    /** 評価コメント(センタ) */
    /* private static final String RATING_COMMENT_CENTER = "%s判定まであと"; */
    private static final String RATING_COMMENT_CENTER = "%s判定まであと%s点";

    /** 評価コメント(二次) */
    private static final String RATING_COMMENT_SECOND = "%s判定まであと%s";

    /** 公表科目：理1 */
    private static final String FIRSTANSSCI = "理1";

    /** 公表科目：地公1 */
    private static final String FIRSTANSSOC = "地公1";

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックス：クラス番号・氏名 */
    private static final String IDX_CLASSNO_NAME = "A5";

    /** インデックス：模試名（上段） */
    private static final String IDX_EXAMINATION1 = "A6";

    /** インデックス：私大評価用偏差値(数学(1科目)) */
    /* private static final String IDX_PRIVATE_UNIV_DEV_MATH_ONE = "AS11"; */
    private static final String IDX_PRIVATE_UNIV_DEV_MATH_ONE = "AW11";

    /** インデックス：志望大学 */
    private static final String IDX_UNIV = "A17";

    /** インデックス：募集人員 */
    private static final String IDX_BOSYU = "D19";

    /** インデックス：本部所在地 */
    private static final String IDX_HONBU = "L19";

    /** インデックス：キャンパス所在地 */
    private static final String IDX_CAMPUS = "L20";

    /** インデックス：志望順位 */
    private static final String IDX_SHIBOUJYUNI = "A20";

    /** インデックス：共通テスト公表科目 */
    /* private static final String IDX_C_SUBCOUNT = "D25"; */
    private static final String IDX_C_SUBCOUNT = "D24";

    /** インデックス：共通テスト英語配点 */
    /* private static final String IDX_C_EIGO_HAITEN = "D29"; */
    private static final String IDX_C_EIGO_HAITEN = "D28";

    /** インデックス：共通テスト英語得点 */
    /* private static final String IDX_C_EIGO_TOKUTEN = "D30"; */
    private static final String IDX_C_EIGO_TOKUTEN = "D29";

    /** インデックス：共通テスト認定配点 */
    /* private static final String IDX_C_KYOTSU_HAITEN = "F29"; */

    /** インデックス：共通テスト認定得点 */
    /* private static final String IDX_C_KYOTSU_TOKUTEN = "F30"; */

    /** インデックス：共通テスト数学配点 */
    /* private static final String IDX_C_MATH_HAITEN = "H29"; */
    private static final String IDX_C_MATH_HAITEN = "F28";

    /** インデックス：共通テスト数学1得点 */
    /* private static final String IDX_C_MATH1_TOKUTEN = "H30"; */
    private static final String IDX_C_MATH1_TOKUTEN = "F29";

    /** インデックス：共通テスト数学2得点 */
    /* private static final String IDX_C_MATH2_TOKUTEN = "J30"; */
    private static final String IDX_C_MATH2_TOKUTEN = "H29";

    /** インデックス：共通テスト国語1配点 */
    /* private static final String IDX_C_JAP1_HAITEN = "L29"; */
    private static final String IDX_C_JAP1_HAITEN = "J28";

    /** インデックス：共通テスト国語1得点 */
    /* private static final String IDX_C_JAP1_TOKUTEN = "L30"; */
    private static final String IDX_C_JAP1_TOKUTEN = "J29";

    /** インデックス：共通テスト理科配点 */
    /* private static final String IDX_C_SCI_HAITEN = "P29"; */
    private static final String IDX_C_SCI_HAITEN = "L28";

    /** インデックス：共通テスト理科1得点 */
    /* private static final String IDX_C_SCI1_TOKUTEN = "P30"; */
    private static final String IDX_C_SCI1_TOKUTEN = "L29";

    /** インデックス：共通テスト理科2得点 */
    /* private static final String IDX_C_SCI2_TOKUTEN = "R30"; */
    private static final String IDX_C_SCI2_TOKUTEN = "N29";

    /** インデックス：共通テスト理科3得点 */
    /* private static final String IDX_C_SCI3_TOKUTEN = "T30"; */
    private static final String IDX_C_SCI3_TOKUTEN = "P29";

    /** インデックス：共通テスト社会配点 */
    /* private static final String IDX_C_SOC_HAITEN = "V29"; */
    private static final String IDX_C_SOC_HAITEN = "R28";

    /** インデックス：共通テスト社会1得点 */
    /* private static final String IDX_C_SOC1_TOKUTEN = "V30"; */
    private static final String IDX_C_SOC1_TOKUTEN = "R29";

    /** インデックス：共通テスト社会2得点 */
    /* private static final String IDX_C_SOC2_TOKUTEN = "X30"; */
    private static final String IDX_C_SOC2_TOKUTEN = "T29";

    /** インデックス：第１段階選抜予想 */
    /* private static final String IDX_ICHIDANKAISENBATSU = "F31"; */
    private static final String IDX_ICHIDANKAISENBATSU = "F30";

    /** インデックス：センター試験注意事項 */
    /* private static final String IDX_C_ATTENTION = "A32"; */
    private static final String IDX_C_ATTENTION = "A31";

    /** インデックス：センター試験注釈文 */
    /* private static final String IDX_C_COMMENT_STATEMENT = "D32"; */
    private static final String IDX_C_COMMENT_STATEMENT = "D31";

    /** インデックス：本人得点 */
    /* private static final String IDX_HONNIN_TOKUTEN = "T31"; */
    private static final String IDX_HONNIN_TOKUTEN = "R30";

    /** インデックス：二次公表科目 */
    /* private static final String IDX_S_SUBCOUNT = "D36"; */
    private static final String IDX_S_SUBCOUNT = "D35";

    /** インデックス：二次英語配点 */
    /* private static final String IDX_S_ENGLISH_HAITEN = "D40"; */
    private static final String IDX_S_ENGLISH_HAITEN = "D39";

    /** インデックス：二次英語偏差値 */
    /* private static final String IDX_S_ENGLISH_DEV = "D41"; */
    private static final String IDX_S_ENGLISH_DEV = "D40";

    /** インデックス：二次数学配点 */
    /* private static final String IDX_S_MATH_HAITEN = "F40"; */
    private static final String IDX_S_MATH_HAITEN = "F39";

    /** インデックス：二次数学偏差値 */
    /* private static final String IDX_S_MATH_DEV = "F41"; */
    private static final String IDX_S_MATH_DEV = "F40";

    /** インデックス：二次国語配点 */
    /* private static final String IDX_S_JAPANASE_HAITEN = "H40"; */
    private static final String IDX_S_JAPANASE_HAITEN = "H39";

    /** インデックス：二次国語偏差値 */
    /* private static final String IDX_S_JAPANASE_DEV = "H41"; */
    private static final String IDX_S_JAPANASE_DEV = "H40";

    /** インデックス：二次理科配点 */
    /* private static final String IDX_S_SCI_HAITEN = "J40"; */
    private static final String IDX_S_SCI_HAITEN = "J39";

    /** インデックス：二次理科1偏差値 */
    /* private static final String IDX_S_SCI1_DEV = "J41"; */
    private static final String IDX_S_SCI1_DEV = "J40";

    /** インデックス：二次理科2偏差値 */
    /* private static final String IDX_S_SCI2_DEV = "L41"; */
    private static final String IDX_S_SCI2_DEV = "L40";

    /** インデックス：二次社会配点 */
    /* private static final String IDX_S_SOC_HAITEN = "N40"; */
    private static final String IDX_S_SOC_HAITEN = "N39";

    /** インデックス：二次社会1偏差値 */
    /* private static final String IDX_S_SOC1_DEV = "N41"; */
    private static final String IDX_S_SOC1_DEV = "N40";

    /** インデックス：二次社会2偏差値 */
    /* private static final String IDX_S_SOC2_DEV = "P41"; */
    private static final String IDX_S_SOC2_DEV = "P40";

    /** インデックス：二次その他配点 */
    /* private static final String IDX_S_OTHER_HAITEN = "R40"; */
    private static final String IDX_S_OTHER_HAITEN_HEAD = "R38";
    /** インデックス：二次その他配点 */
    private static final String IDX_S_OTHER_HAITEN = "R39";

    /** インデックス：二次試験備考 */
    /* private static final String IDX_S_BIKOU = "D42"; */
    private static final String IDX_S_BIKOU = "D41";

    /** インデックス：総合評価：評価 */
    /* private static final String IDX_SOUGOU_HYOUKA = "AF20"; */
    private static final String IDX_SOUGOU_HYOUKA = "Z20";

    /** インデックス：総合評価：評価ポイント */
    /* private static final String IDX_SOUGOU_HYOUKAPOINT = "AF21"; */
    private static final String IDX_SOUGOU_HYOUKAPOINT = "Z21";

    /** インデックス：共通テスト評価：評価_含まない */
    /** インデックス：共通テスト評価：評価 */
    /* private static final String IDX_C_HYOUKA = "AD25"; */
    private static final String IDX_C_HYOUKA = "Z24";

    /** インデックス：共通テスト評価：評価得点_含まない */
    /** インデックス：共通テスト評価：評価得点 */
    /* private static final String IDX_C_HYOUKATOKUTEN = "AD26"; */
    private static final String IDX_C_HYOUKATOKUTEN = "Z25";

    /** インデックス：共通テスト評価：満点_含まない */
    /** インデックス：共通テスト評価：満点 */
    /* private static final String IDX_C_MANTEN = "AD27"; */
    private static final String IDX_C_MANTEN = "Z26";

    /** インデックス：共通テスト評価：ボーダーライン_含まない */
    /** インデックス：共通テスト評価：ボーダーライン */
    /* private static final String IDX_C_BORDERLINE = "AD28"; */
    private static final String IDX_C_BORDERLINE = "Z27";

    /** インデックス：共通テスト評価：判定まであと_含まない */
    /** インデックス：共通テスト評価：判定まであと */
    private static final String IDX_C_HANTEI = "V28";

    /** インデックス：共通テスト評価：評価_含む */
    /* private static final String IDX_C_INCLUDE_HYOUKA = "AF25"; */

    /** インデックス：共通テスト評価：評価得点_含む */
    /* private static final String IDX_C_INCLUDE_HYOUKATOKUTEN = "AF26"; */

    /** インデックス：共通テスト評価：満点_含む */
    /* private static final String IDX_C_INCLUDE_MANTEN = "AF27"; */

    /** インデックス：共通テスト評価：ボーダーライン_含む */
    /* private static final String IDX_C_INCLUDE_BORDERLINE = "AF28"; */

    /** インデックス：共通テスト評価：判定まであと_含む */
    /* private static final String IDX_C_INCLUDE_HANTEI = "AF29"; */

    /** インデックス：共通テスト評価：コメント */
    /* private static final String IDX_C_COMMENT = "Z29"; */
    /* private static final String IDX_C_COMMENT = "X28"; */

    /** インデックス：共通テスト評価：A評価基準_含まない */
    /** インデックス：共通テスト評価：A評価基準 */
    /* private static final String IDX_C_A_HYOUKAKIJYUN = "AD30"; */
    private static final String IDX_C_A_HYOUKAKIJYUN = "Z29";

    /** インデックス：共通テスト評価：B評価基準_含まない */
    /** インデックス：共通テスト評価：B評価基準 */
    /* private static final String IDX_C_B_HYOUKAKIJYUN = "AD31"; */
    private static final String IDX_C_B_HYOUKAKIJYUN = "Z30";

    /** インデックス：共通テスト評価：C評価基準_含まない */
    /** インデックス：共通テスト評価：C評価基準 */
    /* private static final String IDX_C_C_HYOUKAKIJYUN = "AD32"; */
    private static final String IDX_C_C_HYOUKAKIJYUN = "Z31";

    /** インデックス：共通テスト評価：D評価基準_含まない */
    /** インデックス：共通テスト評価：D評価基準 */
    /* private static final String IDX_C_D_HYOUKAKIJYUN = "AD33"; */
    private static final String IDX_C_D_HYOUKAKIJYUN = "Z32";

    /** インデックス：共通テスト評価：A評価基準_含む */
    /* private static final String IDX_C_A_INCLUDE_HYOUKAKIJYUN = "AF30"; */

    /** インデックス：共通テスト評価：B評価基準_含む */
    /* private static final String IDX_C_B_INCLUDE_HYOUKAKIJYUN = "AF31"; */

    /** インデックス：共通テスト評価：C評価基準_含む */
    /* private static final String IDX_C_C_INCLUDE_HYOUKAKIJYUN = "AF32"; */

    /** インデックス：共通テスト評価：D評価基準_含む */
    /* private static final String IDX_C_D_INCLUDE_HYOUKAKIJYUN = "AF33"; */

    /** インデックス：二次評価：評価 */
    /* private static final String IDX_S_HYOUKA = "AF36"; */
    private static final String IDX_S_HYOUKA = "Z35";

    /** インデックス：二次評価：評価偏差値 */
    /* private static final String IDX_S_HYOUKAHENSATI = "AF37"; */
    private static final String IDX_S_HYOUKAHENSATI = "Z36";

    /** インデックス：二次評価：満点 */
    /* private static final String IDX_S_MANTEN = "AF38"; */
    private static final String IDX_S_MANTEN = "Z37";

    /** インデックス：二次評価：ボーダーランク */
    /* private static final String IDX_S_BORDERRUNK = "AF39"; */
    private static final String IDX_S_BORDERRUNK = "Z38";

    /** インデックス：二次評価：コメント */
    /* private static final String IDX_S_COMMENT = "Z40"; */
    private static final String IDX_S_COMMENT = "V39";

    /** インデックス：二次評価：A評価基準 */
    /* private static final String IDX_S_A_HYOUKAKIJYUN = "AF41"; */
    private static final String IDX_S_A_HYOUKAKIJYUN = "Z40";

    /** インデックス：二次評価：B評価基準 */
    /* private static final String IDX_S_B_HYOUKAKIJYUN = "AF42"; */
    private static final String IDX_S_B_HYOUKAKIJYUN = "Z41";

    /** インデックス：二次評価：C評価基準 */
    /* private static final String IDX_S_C_HYOUKAKIJYUN = "AF43"; */
    private static final String IDX_S_C_HYOUKAKIJYUN = "Z42";

    /** インデックス：二次評価：D評価基準 */
    /* private static final String IDX_S_D_HYOUKAKIJYUN = "AF44"; */
    private static final String IDX_S_D_HYOUKAKIJYUN = "Z43";

    /** インデックス：学力分布：偏差値／換算得点 */
    /* private static final String IDX_DIST_SCORE = "AI21"; */
    private static final String IDX_DIST_SCORE = "AC21";

    /** インデックス：学力分布：志願者数 */
    /* private static final String IDX_DIST_CANDIDATE_NUM = "AZ21"; */
    private static final String IDX_DIST_CANDIDATE_NUM = "AT21";

    /** インデックス：英語認定試験名１ */
    /* private static final String IDX_CEFR_NAME1 = "AV14"; */

    /** インデックス：英語認定試験スコア１ */
    /* private static final String IDX_CEFR_SCORE1 = "BE14"; */

    /** インデックス：英語認定試験CEFR１ */
    /* private static final String IDX_CEFR_CEFR1 = "BG14"; */

    /** インデックス：英語認定試験合否１ */
    /* private static final String IDX_CEFR_RESULT1 = "BJ14"; */

    /** インデックス：英語認定試験名２ */
    /* private static final String IDX_CEFR_NAME2 = "AV15"; */

    /** インデックス：英語認定試験スコア２ */
    /* private static final String IDX_CEFR_SCORE2 = "BE15"; */

    /** インデックス：英語認定試験CEFR２ */
    /* private static final String IDX_CEFR_CEFR2 = "BG15"; */

    /** インデックス：英語認定試験合否２ */
    /* private static final String IDX_CEFR_RESULT2 = "BJ15"; */

    /** 帳票ユーティリティ */
    private final ExmntnUnivExcelUtil excelUtil = new ExmntnUnivExcelUtil(this);

    /** 大学詳細画面のサービス */
    @Resource
    private ExmntnUnivService exmntnUnivService;

    /** 志望大学学力分布画面のサービス */
    @Resource
    private ExmntnDistService exmntnDistService;

    /** 取得可能資格ダイアログのサービス */
    @Resource
    private ExmntnDlcsService exmntnDlcsService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 生徒情報Bean */
    private ExmntnUnivStudentBean studentBean;

    /** 成績データ */
    private ScoreBean scoreBean;

    /** 志望順位と志望大学情報Beanのペアリスト */
    private List<Pair<String, ExmntnMainCandidateUnivBean>> pairList;

    /** 志望順位 */
    private String candidateRank;

    /** 大学詳細情報Bean */
    private ExmntnUnivDetailSvcOutBean univBean;

    /** マーク模試 */
    private ExamBean markExam;

    /** 記述模試 */
    private ExamBean writtenExam;

    /** 対象模試 */
    private ExamBean targetExam;

    /** Service */
    @Resource(name = "exmntnMainService")
    private ExmntnMainService service;

    @Override
    protected boolean prepareData(ExmntnUnivA0201InBean inBean) {

        /* 入力情報を保存する */
        studentBean = inBean.getStudentBean();
        scoreBean = inBean.getScoreBean();
        pairList = inBean.getPairList();

        if (studentBean == null || pairList.isEmpty() || scoreBean == null) {
            /* 「生徒情報無し」「志望大学情報無し」「判定未実行」の何れかなら出力しない */
            return false;
        }

        /* 対象模試（マーク模試）を保存する */
        markExam = scoreBean.getMarkExam();

        /* 対象模試（記述模試）を保存する */
        writtenExam = scoreBean.getWrittenExam();

        if (markExam == null && writtenExam == null) {
            /* 入力情報がない */
            return false;
        } else {
            /* 対象模試を判定する */
            targetExam = ExamUtil.judgeTargetExam(markExam, writtenExam);
        }

        /* 帳票ユーティリティに成績データを設定する */
        excelUtil.setScoreBean(scoreBean);

        return true;
    }

    @Override
    protected void initSheet() {

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* クラス番号・氏名を出力する */
        outputTargetName();
    }

    @Override
    protected void outputData() {
        for (Pair<String, ExmntnMainCandidateUnivBean> pair : pairList) {
            /* 志望順位を初期化する */
            candidateRank = pair.getLeft();
            /* 大学詳細情報を初期化する */
            univBean = exmntnUnivService.createUnivDetail(pair.getRight(), scoreBean);
            /* 個人成績一覧を出力する */
            excelUtil.outputSubRecord(targetExam, markExam, writtenExam, IDX_EXAMINATION1);
            /* 私大評価用偏差値を出力する */
            if (markExam != null && !ExamUtil.isCenter(markExam)) {
                excelUtil.outputPrivateUnivDeviation(IDX_PRIVATE_UNIV_DEV_MATH_ONE);
            }
            /* 志望大学情報を出力する */
            outputRecord();
            /* 学力分布情報を出力する */
            outputDistribution();
            /* 1大学毎に改シートする */
            breakSheet();
        }
    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年文字列 */
        String gradeStr = TITLE_GRADE + studentBean.getGrade();

        /* クラス文字列 */
        String classStr = TITLE_CLS + studentBean.getCls();

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeStr + classStr);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, TITLE_EXAM + targetExam);
    }

    /**
     * クラス番号・氏名を出力するメソッドです。
     */
    private void outputTargetName() {

        /* クラス番号文字列 */
        String clsnoStr = TITLE_CLSNO + studentBean.getClassNo();

        /* 氏名文字列 */
        String nameStr = TITLE_NAME + studentBean.getNameKana();

        /* クラス番号・氏名を出力する */
        setCellValue(IDX_CLASSNO_NAME, clsnoStr + nameStr);
    }

    /**
     * 志望大学情報を出力するメソッドです。
     */
    private void outputRecord() {

        /* 大学情報を出力する */
        outputTargetUniv();

        /*共通テスト情報を出力する */
        outputTargetCenter();

        /* 二次試験試験情報を出力する */
        outputTargetSecond();

        /* 評価情報を出力する */
        outputTargetHyoka();

        /* 英語認定試験情報を出力する */
        /* outputCefrData(); */
    }

    /**
     * 大学情報を出力するメソッドです。
     */
    private void outputTargetUniv() {

        /* 志望大学を出力する */
        setCellValue(IDX_UNIV, univBean.getUnivJoinName());

        /* 募集人員を出力する */
        if (univBean.getOfferCapacityKbn() != null && !univBean.getOfferCapacityKbn().equals(CAPACITY_NONE)) {
            if (CAPACITY_ESTIMATE.equals(univBean.getOfferCapacityKbn())) {
                setCellValue(IDX_BOSYU, univBean.getOfferCapacity() + CAPACITY_ESTIMATE_DISP);
            } else {
                setCellValue(IDX_BOSYU, univBean.getOfferCapacity());
            }
        }

        /* 本部所在地を出力する */
        setCellValue(IDX_HONBU, univBean.getPrefBase());

        /* キャンパス所在地を出力する */
        setCellValue(IDX_CAMPUS, univBean.getPrefCampus());

        /* 志望順位を出力する */
        outputTargetJyuni();
    }

    /**
     * 志望順位を出力するメソッドです。
     */
    private void outputTargetJyuni() {
        setCellValue(IDX_SHIBOUJYUNI, TITLE_SHIBOUJYUNI + candidateRank);
    }

    /**
     * 共通テスト情報を出力するメソッドです。
     */
    private void outputTargetCenter() {
        /* 情報誌用科目を取得 */
        UnivInfoBean info = findUnivInfoBean(univBean);
        ExmntnUnivDetailCenterSvcOutBean center = univBean.getCenter();
        outputTargetCenterEnglish(center, info);
        /* outputTargetCenterEnglishJoining(center, info); */
        outputTargetCenterMath(center, info);
        outputTargetCenterJapanese(center, info);
        outputTargetCenterScience(center, info);
        outputTargetCenterSociety(center, info);
        outputTargetCenterOther(center);
    }

    /**
     * 共通テスト:英語の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetCenterEnglish(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 共通テスト英語配点を出力する */
        setCellValue(IDX_C_EIGO_HAITEN, center.getAllotEnglish());

        /* 共通テスト英語得点を出力する */
        setCellValue(IDX_C_EIGO_TOKUTEN, center.getScoreEnglish());
    }

    /**
     * 共通テスト:認定の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    /* private void outputTargetCenterEnglishJoining(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) { */

    /* 共通テスト認定配点を出力する */
    /* if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) { */
    /* 情報誌科目から表示の場合 */
    /*
        setCellValue(IDX_C_KYOTSU_HAITEN, info.getCenterAllot(COURSE_ALLOT.ENGPT));
    } else {
    */
    /* 判定結果から表示の場合 */
    /*
        setCellValue(IDX_C_KYOTSU_HAITEN, center.getAllotEnglishJoining());
    }
    
    ExamBean distTargetExam = excelUtil.judgeDistTargetExam(markExam, writtenExam, univBean.getScheduleCd());
    boolean isCenter = ExamUtil.isCenter(distTargetExam);
    */
    /* 共通テスト認定得点を出力する */
    /*
    if (isCenter) {
        setCellValue(IDX_C_KYOTSU_TOKUTEN, univBean.getCenterInclude().getScoreEnglishJoining());
    } else {
        setCellValue(IDX_C_KYOTSU_TOKUTEN, "");
    }
    */
    /* } */

    /**
     * 共通テスト:数学の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetCenterMath(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 共通テスト数学配点を出力する */
        setCellValue(IDX_C_MATH_HAITEN, center.getAllotMath());

        /* 共通テスト数学1得点を出力する */
        setCellValue(IDX_C_MATH1_TOKUTEN, center.getScoreMath1());

        /* 共通テスト数学2得点を出力する */
        setCellValue(IDX_C_MATH2_TOKUTEN, center.getScoreMath2());
    }

    /**
     * 共通テスト:国語の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetCenterJapanese(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 共通テスト国語配点マーク配点を出力する */
        setCellValue(IDX_C_JAP1_HAITEN, center.getAllotJap());

        /* 共通テスト国語得点マーク得点を出力する */
        setCellValue(IDX_C_JAP1_TOKUTEN, center.getScoreJap());

    }

    /**
     * 共通テスト:理科の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetCenterScience(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 共通テスト理科配点を出力する */
        setCellValue(IDX_C_SCI_HAITEN, center.getAllotSci());

        /* 共通テスト理科1得点を出力する */
        setCellValue(IDX_C_SCI1_TOKUTEN, center.getScoreSci1());

        /* 共通テスト理科2得点を出力する */
        setCellValue(IDX_C_SCI2_TOKUTEN, center.getScoreSci2());

        /* 共通テスト理科3得点を出力する */
        setCellValue(IDX_C_SCI3_TOKUTEN, center.getScoreSci3());
    }

    /**
     * 共通テスト:社会の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetCenterSociety(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 共通テスト社会配点を出力する */
        setCellValue(IDX_C_SOC_HAITEN, center.getAllotSoc());

        /* 共通テスト社会1得点を出力する */
        setCellValue(IDX_C_SOC1_TOKUTEN, center.getScoreSoc1());

        /* 共通テスト社会2得点を出力する */
        setCellValue(IDX_C_SOC2_TOKUTEN, center.getScoreSoc2());
    }

    /**
     * 共通テスト:その他の情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     */
    private void outputTargetCenterOther(ExmntnUnivDetailCenterSvcOutBean center) {

        String fmt = "%s";
        /* 情報誌用科目を取得 */
        UnivInfoBean info = findUnivInfoBean(univBean);

        /* 公表科目を出力する */
        int univCd = Integer.parseInt(univBean.getUnivCd());
        /* 大学コードが6000以降の場合、公表科目を設定しない */
        if (info == null || !info.isCExist() || univCd >= 6000) {
            /* 共通テスト・二次の公表科目を設定 */
            if (!EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv()) && info == null && univCd < 6000) {
                setCellValueSeparator(IDX_C_SUBCOUNT, getMessage("label.exmntn.univ.noPubSubject"));
            } else {
                /* 共通テスト公表科目を設定 */
                setCellValueSeparator(IDX_C_SUBCOUNT, "");
            }
        } else {
            /* 共通テスト公表科目を生成 */
            StringBuffer args = new StringBuffer(info.getC_subString());

            /* 公表科目を設定 */
            if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
                if (info.isFirstAnsFlgSci()) {
                    args.append(FIRSTANSSCI);
                }
                if (info.isFirstAnsFlgSoc()) {
                    args.append(FIRSTANSSOC);
                }
                /* 共通テスト公表科目を設定 */
                setCellValueSeparator(IDX_C_SUBCOUNT, String.format(fmt, args.toString()));
            } else {
                setCellValueSeparator(IDX_C_SUBCOUNT, getMessage("label.exmntn.univ.noPubSubject"));
            }
        }

        /* 第1段階選抜予想を出力する */
        setCellValue(IDX_ICHIDANKAISENBATSU, center.getPickedLine());

        /* 本人得点を出力する */
        setCellValue(IDX_HONNIN_TOKUTEN, center.getRejectScore());

        if (ExmntnUnivUtil.isAttentionVisible(markExam, systemDto)) {
            /* 注意事項を出力する */
            setCellValue(IDX_C_ATTENTION, getMessage("label.exmntn.univ.centerAttention"));
            /* 注釈文を出力する */
            setCellValue(IDX_C_COMMENT_STATEMENT, center.getCommentStatement());
        }
    }

    /**
     * 二次試験試験情報を出力するメソッドです。
     */
    private void outputTargetSecond() {
        /* 情報誌用科目を取得 */
        UnivInfoBean info = findUnivInfoBean(univBean);
        ExmntnUnivDetailSecondSvcOutBean second = univBean.getSecond();
        outputTargetSecondEnglish(second, info);
        outputTargetSecondMath(second, info);
        outputTargetSecondJapanese(second, info);
        outputTargetSecondScience(second, info);
        outputTargetSecondSociety(second, info);
        outputTargetSecondOther(second);
    }

    /**
     * 情報誌用科目を取得します。
     *
     * @param univDetail 個人成績・志望大学 画面表示用の出力Bean
     * @return 情報誌用科目
     */
    private UnivInfoBean findUnivInfoBean(ExmntnUnivDetailSvcOutBean univDetail) {

        UnivInfoBean info = null;

        if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
            /* キーを取得 */
            String combiKey = right(univDetail.getCenter().getBranchCd(), 2) + right(univDetail.getSecond().getBranchCd(), 2);
            /* 情報誌用科目データマップを取得 */
            @SuppressWarnings("rawtypes")
            Map univ = (Map) systemDto.getUnivInfo().get(univDetail.getUnivKey());
            /* センター枝番、二次枝番から該当の情報誌用科目を取得 */
            for (Object key : univ.keySet()) {
                if (key.toString().endsWith(combiKey)) {
                    info = (UnivInfoBean) univ.get(key.toString());
                    break;
                }
            }
            /* 見つからない場合、先頭を取得 */
            if (info == null) {
                info = (UnivInfoBean) univ.get(univ.keySet().toArray()[0]);
            }
        }
        return info;
    }

    /**
     * 指定された文字列の最後から指定された文字列長の文字列を返します。
     *
     * @param value 基となる文字列
     * @param length 返す文字列の文字列長
     * @return 基となる文字列の最後から指定された文字列長の文字列
     */
    private static String right(String value, int length) {
        try {
            if (value.length() >= length)
                return value.substring(value.length() - length);
            else
                return value.substring(1);
        } catch (Exception e) {
            return value;
        }
    }

    /**
     * 二次試験:英語の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetSecondEnglish(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {

        /* 二次英語配点を出力する */
        setCellValue(IDX_S_ENGLISH_HAITEN, second.getAllotEnglish());

        /* 二次英語偏差値を出力する */
        setCellValue(IDX_S_ENGLISH_DEV, second.getDevEnglish());
    }

    /**
     * 二次試験:数学の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetSecondMath(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {

        /* 二次数学配点を出力する */
        setCellValue(IDX_S_MATH_HAITEN, second.getAllotMath());

        /* 二次数学偏差値を出力する */
        setCellValue(IDX_S_MATH_DEV, second.getDevMath());
    }

    /**
     * 二次試験:国語の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetSecondJapanese(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {

        /* 二次国語配点を出力する */
        setCellValue(IDX_S_JAPANASE_HAITEN, second.getAllotJap());

        /* 二次国語偏差値を出力する */
        setCellValue(IDX_S_JAPANASE_DEV, second.getDevJap());
    }

    /**
     * 二次試験:理科の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetSecondScience(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {

        /* 二次理科配点を出力する */
        setCellValue(IDX_S_SCI_HAITEN, second.getAllotSci());

        /* 二次理科1偏差値を出力する */
        setCellValue(IDX_S_SCI1_DEV, second.getDevSci1());

        /* 二次理科2偏差値を出力する */
        setCellValue(IDX_S_SCI2_DEV, second.getDevSci2());
    }

    /**
     * 二次試験:社会の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void outputTargetSecondSociety(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {

        /* 二次社会配点を出力する */
        setCellValue(IDX_S_SOC_HAITEN, second.getAllotSoc());

        /* 二次社会1偏差値を出力する */
        setCellValue(IDX_S_SOC1_DEV, second.getDevSoc1());

        /* 二次社会2偏差値を出力する */
        setCellValue(IDX_S_SOC2_DEV, second.getDevSoc2());

        /* 二次その他配点を出力する */
        if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
            /* 情報誌科目から表示の場合 */
            setCellValue(IDX_S_OTHER_HAITEN, info.getSecondAllot(COURSE_ALLOT.ETC));
        } else {
            /* 判定結果から表示の場合 */
            setCellValue(IDX_S_OTHER_HAITEN_HEAD, "");
            setCellValue(IDX_S_OTHER_HAITEN, "");

        }
    }

    /**
     * 二次試験:その他の情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     */
    private void outputTargetSecondOther(ExmntnUnivDetailSecondSvcOutBean second) {

        /* 情報誌用科目を取得 */
        UnivInfoBean info = findUnivInfoBean(univBean);

        /* 公表科目を出力する */
        int univCd = Integer.parseInt(univBean.getUnivCd());
        /* 大学コードが6000以降の場合、公表科目を設定しない */
        if (info == null || !info.isSExist() || univCd >= 6000) {
            /* 共通テスト・二次の公表科目を設定 */
            if (!EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv()) && info == null && univCd < 6000) {
                setCellValueSeparator(IDX_S_SUBCOUNT, getMessage("label.exmntn.univ.noPubSubject"));
            } else {
                /* 共通テスト公表科目を設定 */
                setCellValueSeparator(IDX_S_SUBCOUNT, "");
            }
        } else {
            /* 公表科目を設定 */
            if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
                /* 二次公表科目を設定 */
                setCellValueSeparator(IDX_S_SUBCOUNT, info.getS_subString());
            } else {
                setCellValueSeparator(IDX_S_SUBCOUNT, getMessage("label.exmntn.univ.noPubSubject"));
            }
        }

        /* 備考を出力する */
        setCellValueSeparator(IDX_S_BIKOU, second.getNote());
    }

    /**
     * 評価情報を出力するメソッドです。
     */
    private void outputTargetHyoka() {

        /* 総合評価情報を出力する */
        outputTargetSougouHyoka();

        /* センタ評価情報を出力する */
        /* outputTargetCenterHyouka(univBean.getCenter(), univBean.getCenterInclude()); */
        outputTargetCenterHyouka(univBean.getCenter());

        /* 二次評価情報を出力する */
        outputTargetSecondHyouka(univBean.getSecond());
    }

    /**
     * 総合評価情報を出力するメソッドです。
     */
    private void outputTargetSougouHyoka() {

        /* 総合評価：評価を出力する */
        setCellValue(IDX_SOUGOU_HYOUKA, univBean.getTotalRating());

        /* 総合評価：評価ポイントを出力する */
        setCellValue(IDX_SOUGOU_HYOUKAPOINT, univBean.getTotalRatingPoint());
    }

    /**
     * 共通テスト評価情報を出力するメソッドです。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     */
    private void outputTargetCenterHyouka(ExmntnUnivDetailCenterSvcOutBean center) {

        /* ExamBean distTargetExam = excelUtil.judgeDistTargetExam(markExam, writtenExam, univBean.getScheduleCd()); */
        /* boolean isCenter = ExamUtil.isCenter(distTargetExam); */

        /* 共通テスト評価：コメントを出力する */
        /* setCellValue(IDX_C_COMMENT, String.format(RATING_COMMENT_CENTER, "上の")); */
        setCellValue(IDX_C_HANTEI, String.format(RATING_COMMENT_CENTER, center.getRemainRaiting(), center.getRemainScore()));
        /* 共通テスト評価：評価を出力する */
        setCellValue(IDX_C_HYOUKA, center.getRating());
        /* 共通テスト評価：評価得点を出力する */
        setCellValue(IDX_C_HYOUKATOKUTEN, center.getScore());
        /* 共通テスト評価：満点を出力する */
        setCellValue(IDX_C_MANTEN, center.getFullPoint());
        /* 共通テスト評価：ボーダーラインを出力する */
        setCellValue(IDX_C_BORDERLINE, center.getBorder());
        /* 共通テスト評価：判定まで＊点を出力する */
        /* setCellValue(IDX_C_HANTEI, center.getRemainScore()); */
        /* 共通テスト評価：A評価基準を出力する */
        setCellValue(IDX_C_A_HYOUKAKIJYUN, center.getLineA());
        /* 共通テスト評価：B評価基準を出力する */
        setCellValue(IDX_C_B_HYOUKAKIJYUN, center.getLineB());
        /* 共通テスト評価：C評価基準を出力する */
        setCellValue(IDX_C_C_HYOUKAKIJYUN, center.getLineC());
        /* 共通テスト評価：D評価基準を出力する */
        setCellValue(IDX_C_D_HYOUKAKIJYUN, center.getLineD());
        /* if (!isCenter) { */
        /* 共通テスト評価：評価(含まない)を出力する */
        /* setCellValue(IDX_C_HYOUKA, center.getRating()); */
        /* 共通テスト評価：評価得点(含まない)を出力する */
        /* setCellValue(IDX_C_HYOUKATOKUTEN, center.getScore()); */
        /* 共通テスト評価：満点(含まない)を出力する */
        /* setCellValue(IDX_C_MANTEN, center.getFullPoint()); */
        /* 共通テスト評価：ボーダーライン(含まない)を出力する */
        /* setCellValue(IDX_C_BORDERLINE, center.getBorder()); */
        /* 共通テスト評価：ボーダーライン(含まない)を出力する */
        /* setCellValue(IDX_C_HANTEI, center.getRemainScore()); */
        /* 共通テスト評価：A評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_A_HYOUKAKIJYUN, center.getLineA()); */
        /* 共通テスト評価：B評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_B_HYOUKAKIJYUN, center.getLineB()); */
        /* 共通テスト評価：C評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_C_HYOUKAKIJYUN, center.getLineC()); */
        /* 共通テスト評価：D評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_D_HYOUKAKIJYUN, center.getLineD()); */
        /* } else { */
        /* if (!StringUtil.isEmptyTrim(include.getAppReqPaternCd())) { */
        /* 空を設定する */
        /* 共通テスト評価：評価(含まない) */
        /* setCellValue(IDX_C_HYOUKA, ""); */
        /* 共通テスト評価：評価得点(含まない) */
        /* setCellValue(IDX_C_HYOUKATOKUTEN, ""); */
        /* 共通テスト評価：満点(含まない) */
        /* setCellValue(IDX_C_MANTEN, ""); */
        /* 共通テスト評価：ボーダーライン(含まない) */
        /* setCellValue(IDX_C_BORDERLINE, ""); */
        /* 共通テスト評価：ボーダーライン(含まない) */
        /* setCellValue(IDX_C_HANTEI, ""); */
        /* 共通テスト評価：A評価基準(含まない) */
        /* setCellValue(IDX_C_A_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：B評価基準(含まない) */
        /* setCellValue(IDX_C_B_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：C評価基準(含まない) */
        /* setCellValue(IDX_C_C_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：D評価基準(含まない) */
        /* setCellValue(IDX_C_D_HYOUKAKIJYUN, ""); */
        /* } else { */
        /* 共通テスト評価：評価(含まない)を出力する */
        /* setCellValue(IDX_C_HYOUKA, center.getRating()); */
        /* 共通テスト評価：評価得点(含まない)を出力する */
        /* setCellValue(IDX_C_HYOUKATOKUTEN, center.getScore()); */
        /* 共通テスト評価：満点(含まない)を出力する */
        /* setCellValue(IDX_C_MANTEN, center.getFullPoint()); */
        /* 共通テスト評価：ボーダーライン(含まない)を出力する */
        /* setCellValue(IDX_C_BORDERLINE, center.getBorder()); */
        /* 共通テスト評価：ボーダーライン(含まない)を出力する */
        /* setCellValue(IDX_C_HANTEI, center.getRemainScore()); */
        /* 共通テスト評価：A評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_A_HYOUKAKIJYUN, center.getLineA()); */
        /* 共通テスト評価：B評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_B_HYOUKAKIJYUN, center.getLineB()); */
        /* 共通テスト評価：C評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_C_HYOUKAKIJYUN, center.getLineC()); */
        /* 共通テスト評価：D評価基準(含まない)を出力する */
        /* setCellValue(IDX_C_D_HYOUKAKIJYUN, center.getLineD()); */
        /* } */
        /* } */

        /* if (!StringUtil.isEmptyTrim(include.getAppReqPaternCd())) { */
        /* 共通テスト評価：評価(含む)を出力する */
        /* setCellValue(IDX_C_INCLUDE_HYOUKA, include.getRating() + " " + include.getEnglishRequirements()); */
        /* 共通テスト評価：評価得点(含む)を出力する */
        /* setCellValue(IDX_C_INCLUDE_HYOUKATOKUTEN, include.getScore()); */
        /* 共通テスト評価：満点(含む)を出力する */
        /* setCellValue(IDX_C_INCLUDE_MANTEN, include.getFullPoint()); */
        /* 共通テスト評価：ボーダーライン(含む)を出力する */
        /* setCellValue(IDX_C_INCLUDE_BORDERLINE, include.getBorder()); */
        /* 共通テスト評価：ボーダーライン(含まない)を出力する */
        /* setCellValue(IDX_C_INCLUDE_HANTEI, include.getRemainScore()); */
        /* 共通テスト評価：A評価基準(含む)を出力する */
        /* setCellValue(IDX_C_A_INCLUDE_HYOUKAKIJYUN, include.getLineA()); */
        /* 共通テスト評価：B評価基準(含む)を出力する */
        /* setCellValue(IDX_C_B_INCLUDE_HYOUKAKIJYUN, include.getLineB()); */
        /* 共通テスト評価：C評価基準(含む)を出力する */
        /* setCellValue(IDX_C_C_INCLUDE_HYOUKAKIJYUN, include.getLineC()); */
        /* 共通テスト評価：D評価基準(含む)を出力する */
        /* setCellValue(IDX_C_D_INCLUDE_HYOUKAKIJYUN, include.getLineD()); */
        /* } else { */
        /* 共通テスト評価：評価(含む) */
        /* setCellValue(IDX_C_INCLUDE_HYOUKA, ""); */
        /* 共通テスト評価：評価得点(含む) */
        /* setCellValue(IDX_C_INCLUDE_HYOUKATOKUTEN, ""); */
        /* 共通テスト評価：満点(含む) */
        /* setCellValue(IDX_C_INCLUDE_MANTEN, ""); */
        /* 共通テスト評価：ボーダーライン(含む) */
        /* setCellValue(IDX_C_INCLUDE_BORDERLINE, ""); */
        /* 共通テスト評価：ボーダーライン(含まない) */
        /* setCellValue(IDX_C_INCLUDE_HANTEI, ""); */
        /* 共通テスト評価：A評価基準(含む) */
        /* setCellValue(IDX_C_A_INCLUDE_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：B評価基準(含む) */
        /* setCellValue(IDX_C_B_INCLUDE_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：C評価基準(含む) */
        /* setCellValue(IDX_C_C_INCLUDE_HYOUKAKIJYUN, ""); */
        /* 共通テスト評価：D評価基準(含む) */
        /* setCellValue(IDX_C_D_INCLUDE_HYOUKAKIJYUN, ""); */
        /* } */
    }

    /**
     * 二次評価情報を出力するメソッドです。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     */
    private void outputTargetSecondHyouka(ExmntnUnivDetailSecondSvcOutBean second) {

        /* 二次評価：評価を出力する */
        setCellValue(IDX_S_HYOUKA, second.getRating());

        /* 二次評価：評価偏差値を出力する */
        setCellValue(IDX_S_HYOUKAHENSATI, second.getDev());

        /* 二次評価：満点を出力する */
        setCellValue(IDX_S_MANTEN, second.getFullPoint());

        /* 二次評価：ボーダーランクを出力する */
        setCellValue(IDX_S_BORDERRUNK, second.getBorderRank());

        /* 二次評価：コメントを出力する */
        setCellValue(IDX_S_COMMENT, String.format(RATING_COMMENT_SECOND, second.getRemainRaiting(), second.getRemainDeviation()));

        /* 二次評価：A評価基準を出力する */
        setCellValue(IDX_S_A_HYOUKAKIJYUN, second.getLineA());

        /* 二次評価：B評価基準を出力する */
        setCellValue(IDX_S_B_HYOUKAKIJYUN, second.getLineB());

        /* 二次評価：C評価基準を出力する */
        setCellValue(IDX_S_C_HYOUKAKIJYUN, second.getLineC());

        /* 二次評価：D評価基準を出力する */
        setCellValue(IDX_S_D_HYOUKAKIJYUN, second.getLineD());
    }

    /**
     * 英語認定試験情報を出力するメソッドです。
     */
    /* private void outputCefrData() { */

    /* CEFR成績リストを取得する */
    /*
    List<ExmntnMainCefrRecordBean> cefrList;
    cefrList = systemDto.getCefrChangeInfo();
    
    if (cefrList != null) {
    */
    /* 試験名１ */
    /* if(cefrList.get(0).getCefrExamLevelName1()==null)
    
    {
        setCellValue(IDX_CEFR_NAME1, cefrList.get(0).getCefrExamName1());
    }else
    {
        setCellValue(IDX_CEFR_NAME1, cefrList.get(0).getCefrExamName1() + cefrList.get(0).getCefrExamLevelName1());
    }
    */
    /* スコア１ */
    /*
    if (Constants.FLG_ON.equals(cefrList.get(0).getScoreCorrectFlg1())) {
        /* 認定試験スコア補正フラグに"1"が設定されている場合、"*"を表示
        setCellValue(IDX_CEFR_SCORE1, Constants.ASTERISK + " " + getStringScore(cefrList.get(0).getCefrScore1(), cefrList.get(0).getCefrExamCd1()));
    } else {
        setCellValue(IDX_CEFR_SCORE1, getStringScore(cefrList.get(0).getCefrScore1(), cefrList.get(0).getCefrExamCd1()));
    }
    */
    /* CEFR１ */
    /*
    if(Constants.FLG_ON.equals(cefrList.get(0).getCefrLevelCdCorrectFlg1()))
    {
        /* CEFRレベル補正フラグに"1"が設定されている場合、"*"を表示
        setCellValue(IDX_CEFR_CEFR1, Constants.ASTERISK + " " + cefrList.get(0).getCefrLevel1());
    }else
    {
        setCellValue(IDX_CEFR_CEFR1, cefrList.get(0).getCefrLevel1());
    }
    */
    /* 合否１ */
    /*
    if(cefrList.get(0).getExamResult1()!=null&&!"".equals(cefrList.get(0).getExamResult1()))
    {
        if (Constants.FLG_ON.equals(cefrList.get(0).getExamResultCorrectFlg1())) {
            /* 英検合否補正フラグに"1"が設定されている場合、"*"を表示
            setCellValue(IDX_CEFR_RESULT1, Constants.ASTERISK + " " + Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult1()).getName());
        } else {
            setCellValue(IDX_CEFR_RESULT1, Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult1()).getName());
        }
    }else
    {
        setCellValue(IDX_CEFR_RESULT1, "");
    }
    */
    /* 試験名２ */
    /*
    if(cefrList.get(0).getCefrExamLevelName2()==null)
    {
        setCellValue(IDX_CEFR_NAME2, cefrList.get(0).getCefrExamName2());
    }else
    {
        setCellValue(IDX_CEFR_NAME2, cefrList.get(0).getCefrExamName2() + cefrList.get(0).getCefrExamLevelName2());
    }
    */
    /* スコア２ */
    /*
    if (Constants.FLG_ON.equals(cefrList.get(0).getScoreCorrectFlg2())) {
        /* 認定試験スコア補正フラグに"1"が設定されている場合、"*"を表示
        setCellValue(IDX_CEFR_SCORE2, Constants.ASTERISK + " " + getStringScore(cefrList.get(0).getCefrScore2(), cefrList.get(0).getCefrExamCd2()));
    } else {
        setCellValue(IDX_CEFR_SCORE2, getStringScore(cefrList.get(0).getCefrScore2(), cefrList.get(0).getCefrExamCd2()));
    }
    */
    /* CEFR２ */
    /*
    if(Constants.FLG_ON.equals(cefrList.get(0).getCefrLevelCdCorrectFlg2()))
    {
        /* CEFRレベル補正フラグに"1"が設定されている場合、"*"を表示
        setCellValue(IDX_CEFR_CEFR2, Constants.ASTERISK + " " + cefrList.get(0).getCefrLevel2());
    }else
    {
        setCellValue(IDX_CEFR_CEFR2, cefrList.get(0).getCefrLevel2());
    }
    */
    /* 合否２ */
    /*
    if(cefrList.get(0).getExamResult2()!=null&&!"".equals(cefrList.get(0).getExamResult2()))
    {
        if (Constants.FLG_ON.equals(cefrList.get(0).getExamResultCorrectFlg2())) {
            /* 英検合否補正フラグに"1"が設定されている場合、"*"を表示
            setCellValue(IDX_CEFR_RESULT2, Constants.ASTERISK + " " + Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult2()).getName());
        } else {
            setCellValue(IDX_CEFR_RESULT2, Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult2()).getName());
        }
    }else
    {
        setCellValue(IDX_CEFR_RESULT2, "");
    }
    }
    }
    */

    /**
     * スコアを文字列に変換する。
     *
     * @param value スコア（Double型）
     * @param engPtCd 認定試験コード
     * @return スコア（String型）
     */
    /*private String getStringScore(Double value, String engPtCd) {
        EngPtBean exam = systemDto.getEngCertExamList().stream().filter(e -> e.getEngPtCd().equals(engPtCd)).findFirst().orElse(null);
        if (exam == null) {
            return "";
        } else if (value == null) {
            return "";
        } else if (Constants.FLG_ON.equals(exam.getScoreDecFlg())) {
            return String.valueOf(value);
        } else {
            return String.valueOf(value.longValue());
        }
    }*/

    /**
     * 学力分布情報を出力します。
     */
    private void outputDistribution() {

        /* 学力分布の対象模試を判定する */
        ExamBean distTargetExam = excelUtil.judgeDistTargetExam(markExam, writtenExam, univBean.getScheduleCd());

        /* 学力分布情報を取得する */
        ExmntnDistBean distBean = exmntnDistService.findDistBean(distTargetExam, scoreBean, univBean);

        /* 学力分布グラフを出力する */
        excelUtil.outputDistributionGraph(distBean, distTargetExam, getCell(IDX_DIST_SCORE));

        /* 志願者内訳を出力する */
        outputCandidateDetail(distBean, distTargetExam);
    }

    /**
     * 志願者内訳を出力します。
     *
     * @param distBean {@link ExmntnDistBean}
     * @param distTargetExam 学力分布の対象模試
     */
    private void outputCandidateDetail(ExmntnDistBean distBean, ExamBean distTargetExam) {

        HSSFCell candidateNumCell = getCell(IDX_DIST_CANDIDATE_NUM);
        HSSFCell numbersFirstCell = getCell(candidateNumCell.getRowIndex(), candidateNumCell.getColumnIndex() + 3);
        HSSFCell avgScoreCell = getCell(candidateNumCell.getRowIndex() + 2, candidateNumCell.getColumnIndex());
        HSSFCell avgFirstCell = getCell(avgScoreCell.getRowIndex(), avgScoreCell.getColumnIndex() + 3);

        /* 平均偏差値／平均換算得点ラベルを出力する */
        if (distBean.isDeviationDistFlg() || ExamUtil.isWritten(distTargetExam)) {
            setCellValue(avgScoreCell, getMessage("label.exmntn.a0201.avgDeviation"));
        } else {
            if (ExamUtil.isCenter(targetExam)) {
                setCellValue(avgScoreCell, getMessage("label.exmntn.a0201.avgPoint"));
            } else {
                setCellValue(avgScoreCell, getMessage("label.exmntn.a0201.avgCvsPoint"));
            }
        }

        /* 第1志望者／出願予定者ラベルを出力する */
        if (ExamUtil.isCenter(targetExam)) {
            setCellValue(numbersFirstCell, getMessage("label.exmntn.a0201.applicationPlan"));
            setCellValue(avgFirstCell, getMessage("label.exmntn.a0201.applicationPlan"));
        } else {
            setCellValue(numbersFirstCell, getMessage("label.exmntn.a0201.firstChoice"));
            setCellValue(avgFirstCell, getMessage("label.exmntn.a0201.firstChoice"));
        }

        /* 「志願者数」「平均換算得点／平均偏差値」を出力する */
        int rowIndex = candidateNumCell.getRowIndex();
        int cellnum = candidateNumCell.getColumnIndex() + 6;
        outputCandidateDetail(rowIndex, cellnum, distBean.getDetailPlan());
        outputCandidateDetail(rowIndex + 1, cellnum, distBean.getDetailTotal());
    }

    /**
     * 「志願者数」「平均換算得点／平均偏差値」を出力します。
     *
     * @param rowIndex 合計志願者数の行位置
     * @param cellnum 合計志願者数の列位置
     * @param bean {@link ExmntnDistCandidateDetailBean}
     */
    private void outputCandidateDetail(int rowIndex, int cellnum, ExmntnDistCandidateDetailBean bean) {
        outputCandidateDetail(rowIndex, cellnum, bean.getNumbersA());
        outputCandidateDetail(rowIndex, cellnum + 2, bean.getNumbersS());
        outputCandidateDetail(rowIndex, cellnum + 4, bean.getNumbersG());
        outputCandidateDetail(rowIndex + 2, cellnum, bean.getAvgPntA());
        outputCandidateDetail(rowIndex + 2, cellnum + 2, bean.getAvgPntS());
        outputCandidateDetail(rowIndex + 2, cellnum + 4, bean.getAvgPntG());
    }

    /**
     * 「志願者数」「平均換算得点／平均偏差値」を出力します。
     *
     * @param rowIndex 行位置
     * @param cellnum 列位置
     * @param value 値
     */
    private void outputCandidateDetail(int rowIndex, int cellnum, Number value) {
        if (value == null) {
            setCellValue(rowIndex, cellnum, HYPHEN);
        } else {
            setCellValue(rowIndex, cellnum, value);
        }
    }

}

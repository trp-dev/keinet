package jp.ac.kawai_juku.banzaisystem.systop.strt.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.REQUIRED_RECORD_VERSION;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import javax.annotation.Resource;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExitController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ViewResult;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.UnivCdHookUpService;
import jp.ac.kawai_juku.banzaisystem.framework.service.UpgradeRecordService;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.service.MainteStngService;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.controller.SystopLginController;

/**
 *
 * 起動画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopStrtController extends ExitController {

    /** 初期化サービス */
    @Resource
    private InitializeService initializeService;

    /** 成績データバージョンアップサービス */
    @Resource
    private UpgradeRecordService upgradeRecordService;

    /** 大学コード紐付けサービス */
    @Resource
    private UnivCdHookUpService univCdHookUpService;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 初期表示アクションです。
     *
     * @return 起動画面
     */
    @Execute()
    public ExecuteResult index() {

        /* パスワードとバージョン情報を初期化する */
        initializeService.initPasswordAndVersion();

        /* マスタ情報を初期化する */
        initializeService.initMaster();

        /* 画面表示ログを出力する */
        outputLog();

        /* 過年度の成績データを削除する */
        deletePastRecord();

        return new ViewResult() {
            @Override
            public void process(AbstractController controller) {
                super.process(controller);
                if (systemDto.isMismatchingMasterVersionFlg()) {
                    /* 大学マスタバージョンに不整合がある場合は、ログイン画面へ強制移動する */
                    JOptionPane.showMessageDialog(getActiveWindow(), getMessage("label.systop.strt.CM_E012"));
                    teacherButtonAction();
                }
            }
        };
    }

    /**
     * 過年度の成績データを削除します。
     */
    private void deletePastRecord() {
        try {
            /* 成績データファイルに接続できるよう、一時的に先生用フラグを立てる */
            systemDto.setTeacher(true);

            /* 成績データ情報を初期化する */
            initializeService.initRecordInfo();

            /* 大学マスタ年度＞成績データ年度なら、成績データを削除する */
            if (systemDto.getRecordInfo().getYear() != null
                    && systemDto.getUnivYear().compareTo(systemDto.getRecordInfo().getYear()) > 0) {
                String oldExamYear = systemDto.getRecordInfo().getYear();
                getComponent(MainteStngService.class).deletePastRecord(systemDto.getUnivYear());
                logService.output(GamenId.A1, oldExamYear, systemDto.getRecordInfo().getYear());
            }
        } finally {
            /* 先生用フラグを落とす */
            systemDto.setTeacher(false);
        }
    }

    /**
     * 再表示アクションです。
     *
     * @return 起動画面
     */
    @Execute()
    public ExecuteResult redisplay() {
        outputLog();
        return VIEW_RESULT;
    }

    /**
     * 画面表示ログを出力します。
     */
    private void outputLog() {
        logService.output(GamenId.A1, BZConstants.VERSION, systemDto.getUnivYear(), systemDto.getUnivExamDiv());
    }

    /**
     * 先生用ボタン押下アクションです。
     *
     * @return マスタ・プログラムチェック画面
     */
    @Execute
    public ExecuteResult teacherButtonAction() {

        /* 先生用フラグを立てる */
        systemDto.setTeacher(true);

        /* 成績データ情報を初期化する */
        initializeService.initRecordInfo();

        /* 成績データのバージョンをチェックする */
        if (systemDto.getRecordInfo().getVersion() < REQUIRED_RECORD_VERSION) {
            /* 成績データバージョン ＜ 成績データ必要バージョンの場合 */
            upgradeRecordService.upgrade();
        } else if (systemDto.getRecordInfo().getVersion() > REQUIRED_RECORD_VERSION) {
            /* 成績データバージョン ＞ 成績データ必要バージョンの場合 */
            systemDto.setMismatchingRecordVersionFlg(true);
            return forward(SystopLginController.class);
        }

        if (univCdHookUpService.needsHookUp()) {
            /* 紐付け対象あり */
            return new TaskResult() {
                @Override
                protected ExecuteResult doTask() {
                    univCdHookUpService.hookUp();
                    return forward(SystopLginController.class);
                }
            };
        } else {
            /* 紐付け対象なし */
            return forward(SystopLginController.class);
        }
    }

    /**
     * 生徒用ボタン押下アクションです。
     *
     * @return 個人成績画面
     */
    @Execute
    public ExecuteResult studentButtonAction() {
        systemDto.setTeacher(false);
        return forward(ExmntnMainController.class);
    }

}

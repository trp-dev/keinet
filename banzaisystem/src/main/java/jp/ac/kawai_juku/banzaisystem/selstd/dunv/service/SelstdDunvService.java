package jp.ac.kawai_juku.banzaisystem.selstd.dunv.service;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.dao.SelstdDunvDao;

/**
 *
 * 大学選択ダイアログのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDunvService {

    /** DAO */
    @Resource
    private SelstdDunvDao selstdDunvDao;

    /**
     * 選択可能大学リストを取得します。
     *
     * @param exam 対象模試
     * @param idList 対象生徒の個人IDリスト
     * @return 選択可能大学リスト
     */
    public List<SelstdDunvBean> findSelectableUnivList(ExamBean exam, List<Integer> idList) {
        if (exam != null && idList != null && !idList.isEmpty()) {
            return selstdDunvDao.selectSelectableUnivList(exam, idList);
        } else {
            return Collections.emptyList();
        }
    }

}

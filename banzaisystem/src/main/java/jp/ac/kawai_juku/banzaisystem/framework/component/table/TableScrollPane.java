package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.SortableScrollPane.Header;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * テーブル表示用のScrollPaneです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class TableScrollPane extends BZScrollPane implements BZComponent {

    /**
     * コンストラクタです。
     *
     * @param vsbPolicy 縦スクロールバーのポリシー
     * @param hsbPolicy 横スクロールバーのポリシー
     */
    public TableScrollPane(int vsbPolicy, int hsbPolicy) {
        super(vsbPolicy, hsbPolicy);
    }

    /**
     * ヘッダラベルを生成します。
     *
     * @param name ヘッダ名
     * @param alignment アライメント設定
     * @param isDrawSeparator 区切り線描画フラグ
     * @return ラベル
     */
    protected final LabelHeader createHeaderLabel(String name, CellAlignment alignment, boolean isDrawSeparator) {
        return new LabelHeader(name, alignment, isDrawSeparator);
    }

    /**
     * ヘッダラベルを生成します。
     *
     * @param name ヘッダ名
     * @param alignment アライメント設定
     * @return ラベル
     */
    protected final LabelHeader createHeaderLabel(String name, CellAlignment alignment) {
        return createHeaderLabel(name, alignment, true);
    }

    /**
     * スクロール領域のヘッダを返します。
     *
     * @return ヘッダ
     */
    protected final JPanel getHeader() {
        return (JPanel) getColumnHeader().getView();
    }

    /**
     * 固定領域のヘッダを返します。
     *
     * @return ヘッダ
     */
    protected final JPanel getFixedHeader() {
        return (JPanel) getCorner(JScrollPane.UPPER_LEFT_CORNER);
    }

    /**
     * スクロール領域のテーブルを返します。
     *
     * @return テーブル
     */
    protected final JTable getTable() {
        return (JTable) ((JPanel) getViewport().getView()).getComponent(0);
    }

    /**
     * 固定領域のテーブルを返します。
     *
     * @return テーブル
     */
    protected final JTable getFixedTable() {
        return (JTable) ((JPanel) getRowHeader().getView()).getComponent(0);
    }

    /** ヘッダラベル定義 */
    protected final class LabelHeader extends JLabel implements Header {

        /** アライメント設定 */
        private final CellAlignment alignment;

        /** 区切り線描画フラグ */
        private final boolean isDrawSeparator;

        /**
         * コンストラクタです。
         *
         * @param name ヘッダ名
         * @param alignment アライメント設定
         * @param isDrawSeparator 区切り線描画フラグ
         */
        private LabelHeader(String name, CellAlignment alignment, boolean isDrawSeparator) {

            this.alignment = alignment;
            this.isDrawSeparator = isDrawSeparator;

            Class<?> clazz = TableScrollPane.this.getClass();
            BufferedImage img = ImageUtil.readImage(clazz, "table/" + name + ".png");
            setSize(img.getWidth(), img.getHeight());
            setIcon(new ImageIcon(img));
            setName(name);
        }

        @Override
        public CellAlignment getAlignment() {
            return alignment;
        }

        @Override
        public boolean isDrawSeparator() {
            return isDrawSeparator;
        }

    }

}

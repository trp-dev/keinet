package jp.ac.kawai_juku.banzaisystem.selstd.dadd.service;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Window;
import java.util.Collections;

import javax.annotation.Resource;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.dao.ExmntnScdlDao;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.dao.MainteImptImportDao;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.bean.SelstdDaddKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.dao.SelstdDaddDao;
import jp.ac.kawai_juku.banzaisystem.selstd.main.dao.SelstdMainDao;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 個人追加ダイアログのサービスです。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDaddService {

    /** 名寄せ処理結果を表すenum */
    public enum AggregationResult {

        /** 成功 */
        SUCCESS,

        /** 中断 */
        ABORT,

        /** 失敗（処理対象外） */
        FAILURE;
    }

    /** DAO */
    @Resource
    private SelstdDaddDao selstdDaddDao;

    /** 個人成績インポート処理のDAO */
    @Resource
    private MainteImptImportDao mainteImptImportDao;

    /** 対象生徒選択画面のDAO */
    @Resource
    private SelstdMainDao selstdMainDao;

    /** 受験スケジュール画面のDAO */
    @Resource
    private ExmntnScdlDao exmntnScdlDao;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 学籍履歴情報内で指定の条件に該当する既存の登録情報をカウントします。
     *
     * @param nendo 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @param id 更新対象生徒の個人ID（生徒追加時はNULL）
     * @return 既存の登録情報の有無（true:既存あり, false:既存無し）
     */
    public boolean hasRegisteredHistoryInfo(String nendo, Integer grade, String cls, String classNo, Integer id) {
        return selstdDaddDao.countRegisteredHistoryInfo(nendo, grade, cls, classNo, id) > 0;
    }

    /**
     * 生徒情報を登録します。
     *
     * @param nendo 年度
     * @param id 更新対象生徒の個人ID（生徒追加時はNULL）
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @param nameKana カナ氏名
     */
    @Lock
    public void registKojin(String nendo, Integer id, String grade, String cls, String classNo, String nameKana) {

        Integer individualId;
        if (id == null) {
            /* 生徒追加の場合 */

            /* 学籍基本情報を登録する → 個人ID（連番）が生成される */
            individualId = mainteImptImportDao.insertBasicInfo(nameKana);

            /* 学籍履歴情報を登録する */
            mainteImptImportDao.insertHistoryInfo(individualId, nendo, grade, cls, classNo);

            /* ログを出力する */
            logService.output(GamenId.B_d3, "追加", individualId.toString(), grade, cls, classNo);
        } else {
            /* 生徒更新の場合 */
            individualId = id;

            /* 学籍基本情報を更新する */
            selstdDaddDao.updateBasicInfo(individualId, nameKana);

            /* 学籍履歴情報を更新する */
            selstdDaddDao.updateHistoryInfo(individualId, nendo, grade, cls, classNo);

            /* ログを出力する */
            logService.output(GamenId.B_d3, "修正", individualId.toString(), grade, cls, classNo);
        }
    }

    /**
     * 個人情報を取得します。
     *
     * @param year 年度
     * @param id 個人ID
     * @return {@link SelstdDaddKojinBean}
     */
    public SelstdDaddKojinBean findKojinBean(String year, Integer id) {
        SelstdDaddKojinBean bean = selstdDaddDao.selectKojinBean(year, id);
        if (bean == null) {
            throw new MessageDialogException(getMessage("error.selstd.dadd.B-d3_E006"));
        }
        return bean;
    }

    /**
     * 名寄せを実行します。
     *
     * @param nendo 年度
     * @param subId 従情報の個人ID
     * @param grade 従情報の学年
     * @param cls 従情報のクラス
     * @param classNo 従情報のクラス番号
     * @param nameKana 従情報のカナ氏名
     * @param window {@link Window} （確認ダイアログ表示用。nullの場合は確認ダイアログを表示しない。）
     * @return {@link AggregationResult}
     */
    @Lock
    public AggregationResult doNameBasedAggregation(String nendo, Integer subId, Integer grade, String cls,
            String classNo, String nameKana, Window window) {

        /* 生徒追加の場合は名寄せしない */
        if (subId == null) {
            return AggregationResult.FAILURE;
        }

        /* クラス・クラス番号・カナ氏名の何れかが未入力なら名寄せしない */
        if (StringUtils.isEmpty(cls) || StringUtils.isEmpty(classNo) || StringUtils.isEmpty(nameKana)) {
            return AggregationResult.FAILURE;
        }

        /* 名寄せ処理の主情報の個人IDを検索する */
        SelstdDaddKojinBean mainBean = findMainBean(nendo, subId, grade, cls, classNo, nameKana);
        if (mainBean == null) {
            return AggregationResult.FAILURE;
        }

        /* 主従で模試情報が重複する場合はエラーとする */
        if (selstdDaddDao.countIndividualRecord(nendo, mainBean.getId(), subId) > 0) {
            throw new MessageDialogException(getMessage("error.selstd.dadd.B-d3_E007"));
        }

        /* ログ用に名寄せ前の従情報を取得しておく */
        SelstdDaddKojinBean subBean = findKojinBean(nendo, subId);

        /*
         * 名寄せ処理継続確認ダイアログを表示する。
         * ※単体テスト対応として、windowがnullの場合はダイアログを表示しない。
         */
        if (window != null && !showConfirmDialog(window)) {
            return AggregationResult.ABORT;
        }

        /* 従情報を主情報に更新する */
        updateSub(mainBean.getId(), subId);

        /* 従情報を削除する */
        deleteSub(subId);

        /* ログを出力する */
        logService.output(GamenId.B_d3, "統合", mainBean.getId().toString(), mainBean.getGrade().toString(),
                mainBean.getCls(), mainBean.getClassNo(), subBean.getId().toString(), subBean.getGrade().toString(),
                subBean.getCls(), subBean.getClassNo());

        return AggregationResult.SUCCESS;
    }

    /**
     * 従情報に紐付く主情報を検索します。
     *
     * @param nendo 年度
     * @param subId 従情報の個人ID
     * @param grade 従情報の学年
     * @param cls 従情報のクラス
     * @param classNo 従情報のクラス番号
     * @param nameKana 従情報のカナ氏名
     * @return 主情報
     */
    private SelstdDaddKojinBean findMainBean(String nendo, Integer subId, Integer grade, String cls, String classNo,
            String nameKana) {
        String subNameKana = JpnStringUtil.normalize(nameKana);
        for (SelstdDaddKojinBean bean : selstdDaddDao.selectMainList(nendo, subId, grade, cls, classNo)) {
            if (subNameKana.equals(JpnStringUtil.normalize(bean.getNameKana()))) {
                return bean;
            }
        }
        return null;
    }

    /**
     * 名寄せ処理継続確認ダイアログを表示します。
     *
     * @param window {@link Window}
     * @return 名寄せ処理継続ならtrue
     */
    private boolean showConfirmDialog(Window window) {
        String[] button = { getMessage("label.selstd.dadd.dialogYes"), getMessage("label.selstd.dadd.dialogNo") };
        int result =
                JOptionPane.showOptionDialog(window, getMessage("label.selstd.dadd.B_W002"), null,
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

    /**
     * 従情報を主情報に更新します。
     *
     * @param mainId 主情報の個人ID
     * @param subId 従情報の個人ID
     */
    private void updateSub(Integer mainId, Integer subId) {

        /* 従情報更新用のパラメータを準備する */
        BeanMap param = new BeanMap();
        param.put("mainId", mainId);
        param.put("subId", subId);

        /* 主情報の最新受験模試のシステム登録志望大学を未受験模試にコピーする */
        copyLatestCandidateUniv(mainId);

        /* 従情報の最新受験模試のシステム登録志望大学を未受験模試にコピーする */
        copyLatestCandidateUniv(subId);

        /*
         * 従情報が未受験である模試のシステム登録志望大学のうち、
         * 主情報のシステム登録志望大学と重複するものを削除する。
         * ※未公開模試（主従共に未受験の模試）の主情報が残るように従情報を先に削除する。
         */
        selstdDaddDao.deleteDuplicatedCandidateUniv(subId, mainId);

        /*
         * 主情報が未受験である模試のシステム登録志望大学のうち、
         * 従情報のシステム登録志望大学と重複するものを削除する。
         */
        selstdDaddDao.deleteDuplicatedCandidateUniv(mainId, subId);

        /* 主従の志望大学で最大の表示順を取得する */
        Integer maxDispNum = selstdDaddDao.selectCandidateUnivMaxDispNum(param);

        /* 主従生徒が未受験である模試のシステム登録志望大学の志望順位を最大の表示順だけ加算する */
        selstdDaddDao.updateCandidateUnivDispNum(mainId, subId, maxDispNum);

        /* 従情報の志望大学データを主情報に更新する */
        selstdDaddDao.updateCandidateUniv(param);

        /* 主情報のシステム登録志望大学のうち、模試記入志望大学と重複するものを非表示にする */
        selstdDaddDao.updateCandidateUnivDispOff(mainId);

        /* 主情報の志望大学の表示順を振り直す */
        mainteImptImportDao.updateCandidateUnivDispNumber(selstdDaddDao.selectCandidateUnivList(mainId));

        /* 主情報のシステム登録志望大学のうち、登録数の上限を超える分を削除する */
        selstdDaddDao.deleteExcessCandidateUniv(mainId);

        /* 従情報の模試記入志望大学データを主情報に更新する */
        selstdDaddDao.updateCandidateRating(param);

        /* 従情報の個表データを主情報に更新する */
        selstdDaddDao.updateIndividualRecord(param);

        /* 従情報の成績データを主情報に更新する */
        selstdDaddDao.updateSubRecord(param);

        /* 従情報の受験予定大学のうち、主生徒と重複するものを削除する */
        selstdDaddDao.deleteDuplicatedExamPlanUniv(param);

        /* 従情報の受験予定大学の志望順位を登録上限数だけ加算する */
        selstdDaddDao.updateExamPlanUnivCandidateRank(subId);

        /* 従情報の受験予定大学を主情報に更新する */
        selstdDaddDao.updateExamPlanUniv(param);

        /* 主情報の受験予定大学の志望順位を振り直す */
        exmntnScdlDao.updateExamPlanUnivRank(selstdDaddDao.selectExamPlanUnivArray(mainId));

        /* 主情報の志望大学のうち、登録上限数を超える分を削除する */
        selstdDaddDao.deleteExcessExamPlanUniv(mainId);
    }

    /**
     * 指定生徒の最新受験模試のシステム登録志望大学を未受験模試にコピーします。
     *
     * @param id 個人ID
     */
    private void copyLatestCandidateUniv(Integer id) {

        /* 指定生徒の最新受験模試（全ての模試を未受験なら最新公開模試）を取得する */
        ExamBean examBean = selstdDaddDao.selectLatestExamBean(id, systemDto.getUnivYear(), systemDto.getUnivExamDiv());

        /* 指定生徒が未受験である模試のシステム登録志望大学を削除する */
        selstdDaddDao.deletePreviousCandidateUniv(id, examBean);

        /* 指定生徒の最新受験模試のシステム登録志望大学を未受験模試にコピーする */
        selstdDaddDao.insertLatestCandidateUniv(id, examBean);
    }

    /**
     * 従情報を削除します。
     *
     * @param subId 従情報の個人ID
     */
    private void deleteSub(Integer subId) {

        /* 従情報削除用のパラメータを準備する */
        BeanMap param = new BeanMap();
        param.put("idCondition", DaoUtil.createIn("INDIVIDUALID", Collections.singletonList(subId)));

        /* 従情報の学籍基本情報を削除する */
        selstdMainDao.deleteBasicInfo(param);

        /* 従情報の学籍履歴情報を削除する */
        selstdMainDao.deleteHistoryInfo(param);
    }

}

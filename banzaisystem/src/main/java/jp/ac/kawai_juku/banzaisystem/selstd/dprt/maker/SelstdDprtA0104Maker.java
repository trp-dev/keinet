package jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerOutputStudentDataBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerStudentRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtExcelMakerService;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;

import org.apache.commons.lang3.ObjectUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * Excel帳票メーカー(連続個人成績表)です。
 *
 *
 * @author QQ)Nakao.Takaaki
 * @author QQ)Nakao.Takaaki
 *
 */
@Template(id = TemplateId.A01_04)
public class SelstdDprtA0104Maker extends BaseExcelMaker<SelstdDprtExcelMakerInBean> {

    /** 1ページに出力する最大模試数 */
    private static final int LISTMAX = 10;

    /** 出力できる最大人数 */
    private static final int MAX_STUDENT_NUM = 400;

    /** 1教科に出力する最大科目数（総合） */
    private static final int SUBTOTALMAX = 1;

    /** 1教科に出力する最大科目数（英語） */
    private static final int SUBENGMAX = 3;

    /** 1教科に出力する最大科目数（数学） */
    private static final int SUBMATMAX = 3;

    /** 1教科に出力する最大科目数（国語） */
    private static final int SUBJAPMAX = 1;

    /** 1教科に出力する最大科目数（理科） */
    private static final int SUBSCIMAX = 3;

    /** 1教科に出力する最大科目数（地歴公民） */
    private static final int SUBSOCMAX = 2;

    /** 一人に出力する最大志望大学数（1列分） */
    private static final int CANDIDATEUNIVCOLUMNMAX = 3;

    /** 一人に出力する志望大学列数 */
    private static final int CANDIDATEUNIVCOLUMN = 2;

    /** 一人に出力する最大志望大学数 */
    private static final int CANDIDATEUNIVMAX = CANDIDATEUNIVCOLUMNMAX * CANDIDATEUNIVCOLUMN;

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象生徒 */
    private static final String IDX_TARGET_STUDENT = "A3";

    /** インデックスrow：個人成績一覧（開始位置） */
    private static final int IDX_ROW_LIST_START = 6;

    /** インデックスrow：個人成績一覧（開始位置→型・科目） */
    private static final int IDX_ROW_LIST_SUBJECT = 0;

    /** インデックスrow：個人成績一覧（開始位置→得点） */
    private static final int IDX_ROW_LIST_POINT = IDX_ROW_LIST_SUBJECT + 1;

    /** インデックスrow：個人成績一覧（開始位置→偏差値） */
    private static final int IDX_ROW_LIST_DEVIATION = IDX_ROW_LIST_POINT + 1;

    /** インデックスrow：個人成績一覧（１生徒当たりの行数） */
    private static final int IDX_ROW_LIST_LINE = 3;

    /** インデックスcell：個人成績一覧（対象模試） */
    private static final int IDX_CELL_EXAM = 0;

    /** インデックスcell：個人成績一覧（総合） */
    private static final int IDX_CELL_TOTAL = 2;

    /** インデックスcell：個人成績一覧（英語） */
    private static final int IDX_CELL_ENG = IDX_CELL_TOTAL + SUBTOTALMAX;

    /** インデックスcell：個人成績一覧（数学） */
    private static final int IDX_CELL_MAT = IDX_CELL_ENG + SUBENGMAX;

    /** インデックスcell：個人成績一覧（国語） */
    private static final int IDX_CELL_JAP = IDX_CELL_MAT + SUBMATMAX;

    /** インデックスcell：個人成績一覧（理科） */
    private static final int IDX_CELL_SCI = IDX_CELL_JAP + SUBJAPMAX;

    /** インデックスcell：個人成績一覧（地歴公民） */
    private static final int IDX_CELL_SOC = IDX_CELL_SCI + SUBSCIMAX;

    /** インデックスrow：志望大学（開始位置） */
    private static final int IDX_ROW_LIST_CANDIDATEUNIV = 6;

    /** インデックスcell：志望大学（大学名） */
    private static final int IDX_CELL_UNIVNAME = 16;

    /** インデックスcell：志望大学（大学名→学部名） */
    private static final int IDX_CELL_FACULTYNAME = 1;

    /** インデックスcell：志望大学（大学名→学科名） */
    private static final int IDX_CELL_DEPTNAME = 2;

    /** インデックスcell：志望大学（大学名→評価） */
    private static final int IDX_CELL_CENTEREVAL = 3;

    /** インデックスrow：志望大学（志望大学列の列数） */
    private static final int IDX_CELL_UNIV_COLUMN = 5;

    /** 出力データリスト（個人成績一覧＋志望大学） */
    private List<SelstdDprtExcelMakerOutputStudentDataBean> outputDataBeanList = new ArrayList<SelstdDprtExcelMakerOutputStudentDataBean>();

    /** ページ毎に出力する学年 */
    private Integer pageGrade;

    /** ページ毎に出力するクラス */
    private String pageCls;

    /** ページ毎に出力するクラス番号 */
    private String pageClsNo;

    /** ページ毎に出力するクラス番号 */
    private String pageName;

    /** 対象模試 */
    private ExamBean exam;

    /** 個人IDリスト */
    private List<Integer> idList;

    /** サービス */
    @Resource(name = "selstdDprtExcelMakerService")
    private SelstdDprtExcelMakerService service;

    @Override
    protected boolean prepareData(SelstdDprtExcelMakerInBean inBean) {

        idList = inBean.getIdList();
        if (idList.size() > MAX_STUDENT_NUM) {
            /* 出力できる最大人数を超えている場合はエラーとする */
            throw new MessageDialogException(getMessage("error.output.main.D_E002", MAX_STUDENT_NUM));
        }

        /* 出力データリストを取得する*/
        if (service.prepareDataStudent(inBean, outputDataBeanList, getTemplate())) {

            /* 帳票出力用に対象模試を保存する */
            exam = inBean.getExam();
            /* 学年とクラスをセットする（１シート目用） */
            pageGrade = outputDataBeanList.get(0).getGrade();
            pageCls = outputDataBeanList.get(0).getCls();
            pageClsNo = outputDataBeanList.get(0).getClassNo();
            pageName = outputDataBeanList.get(0).getNameKana();
            return true;
        }

        return false;
    }

    @Override
    protected void initSheet() {

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象生徒を出力する */
        outputTargetStudent();
    }

    @Override
    protected void outputData() {

        /* 帳票データを出力する */
        outputExcelData();

    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年・クラス文字列 */
        String gradeCls = null;

        /* 学年・クラス文字列を作成する */
        gradeCls = service.createTargetClass(pageGrade, pageCls, pageClsNo);

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeCls);
    }

    /**
     * 対象生徒を出力するメソッドです。
     */
    private void outputTargetStudent() {
        /* 模試分ループする */
        for (int index = 0; index < outputDataBeanList.size(); index++) {
            String nameStr = null;
            /* 氏名文字列を作成する */
            nameStr = service.createName(pageName);

            setCellValue(IDX_TARGET_STUDENT, nameStr);
        }
    }

    /**
     * 帳票データを出力するメソッドです。
     */
    private void outputExcelData() {

        /* 1ページ内の模試カウント */
        int cntExam = 0;

        int examSize = 0;

        int recCnt = 0;

        int univRecCnt = 0;

        Integer studentId = 0;

        Integer univStudentId = 0;

        SelstdDprtExcelMakerStudentRecordBean wkbean = null;

        SelstdDprtExcelMakerCandidateUnivStudentBean wkUnivbean = null;

        List<SelstdDprtExcelMakerStudentRecordBean> scoreList = null;

        List<SelstdDprtExcelMakerCandidateUnivStudentBean> univList = null;

        examSize = outputDataBeanList.size();

        if (examSize > 1) {
            /* 模試分ループする */
            for (int index = 0; index < outputDataBeanList.size(); index++) {

                SelstdDprtExcelMakerOutputStudentDataBean bean = outputDataBeanList.get(index);

                /* 1ページ内の最大出力模試数を超えたかチェックする */
                if (cntExam >= LISTMAX || (index != 0
                        && (!ObjectUtils.equals(pageGrade, bean.getGrade()) || !ObjectUtils.equals(pageCls, bean.getCls()) || !ObjectUtils.equals(pageClsNo, bean.getClassNo()) || !ObjectUtils.equals(studentId, bean.getIndividualId())))) {
                    /* 学年とクラスをセットする */
                    pageGrade = bean.getGrade();
                    pageCls = bean.getCls();
                    pageClsNo = bean.getClassNo();
                    pageName = bean.getNameKana();

                    /* 改シートする */
                    breakSheet();
                    cntExam = 0;
                }
                studentId = bean.getIndividualId();

                /* 模試情報出力文字を作成する */
                String strExam = "";
                if (bean.getExamName() != null) {
                    strExam = bean.getExamName() + "（" + Integer.parseInt(bean.getInpleDate().substring(4, 6)) + "月）";

                    /* 個人成績一覧データを出力する */
                    outputRecordList(cntExam, strExam, bean.getRecordBeanList());

                    /* 志望大学データを出力する */
                    outputCandidateUnivList(cntExam, bean.getCandidateUnivBeanList());
                }
                cntExam++;
            }
        } else {
            SelstdDprtExcelMakerOutputStudentDataBean bean = null;
            SelstdDprtExcelMakerStudentRecordBean record = null;
            bean = outputDataBeanList.get(0);
            /* マーク模試のみ国語(科目コード:3910)の成績をセットする */
            if (bean.getRecordBeanList().size() > 0) {
                record = bean.getRecordBeanList().get(0);
            } else {
                return;
            }
            /* 生徒数分ループする */
            for (int index = 0; index < idList.size(); index++) {
                scoreList = CollectionsUtil.newArrayList();
                univList = CollectionsUtil.newArrayList();
                /* 一人分のレコード数を取得(成績) */
                for (int index2 = recCnt; index2 < bean.getRecordBeanList().size(); index2++) {
                    wkbean = bean.getRecordBeanList().get(index2);
                    if (index2 != 0 && !ObjectUtils.equals(studentId, wkbean.getIndividualId())) {
                        studentId = wkbean.getIndividualId();
                        break;
                    }
                    recCnt++;
                    scoreList.add(bean.getRecordBeanList().get(index2));
                    studentId = wkbean.getIndividualId();
                }

                if (Exam.Type.MARK.equals(record.getExamType())) {
                    service.setResultsMarkStudentJapanese(exam, scoreList);
                }

                /* 一人分のレコード数を取得(大学) */
                for (int index2 = univRecCnt; index2 < bean.getCandidateUnivBeanList().size(); index2++) {
                    wkUnivbean = bean.getCandidateUnivBeanList().get(index2);
                    if (index2 != 0 && !ObjectUtils.equals(univStudentId, wkUnivbean.getIndividualId())) {
                        univStudentId = wkUnivbean.getIndividualId();
                        break;
                    }
                    univRecCnt++;
                    univStudentId = wkUnivbean.getIndividualId();
                    univList.add(bean.getCandidateUnivBeanList().get(index2));
                }

                /* 1ページ内の最大出力模試数を超えたかチェックする */
                if (index != 0) {
                    wkbean = bean.getRecordBeanList().get(recCnt - 1);
                    /* 学年とクラスをセットする */
                    pageGrade = wkbean.getGrade();
                    pageCls = wkbean.getCls();
                    pageClsNo = wkbean.getClassNo();
                    pageName = wkbean.getNameKana();

                    /* 改シートする */
                    breakSheet();
                    cntExam = 0;
                }

                /* 模試情報出力文字を作成する */
                String strExam = "";
                if (bean.getExamName() != null) {
                    strExam = bean.getExamName() + "（" + Integer.parseInt(bean.getInpleDate().substring(4, 6)) + "月）";

                    /* 個人成績一覧データを出力する */
                    outputRecordListTan(cntExam, strExam, scoreList, scoreList.size());

                    /* 志望大学データを出力する */
                    outputCandidateUnivListTan(cntExam, univList);
                }
            }
            //            }
        }
    }

    /**
     * 連続個人成績表のデータを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param strExam 対象模試名
     * @param outputData 成績データデータリスト（１模試分）
     */
    private void outputRecordList(int cntStudent, String strExam, List<SelstdDprtExcelMakerStudentRecordBean> outputData) {

        /* 1教科内の科目数カウント（総合１） */
        int cntSubTotal = 0;

        /* 1教科内の科目数カウント（英語） */
        int cntSubEng = 0;

        /* 1教科内の科目数カウント（数学） */
        int cntSubMat = 0;

        /* 1教科内の科目数カウント（国語） */
        int cntSubJap = 0;

        /* 1教科内の科目数カウント（理科） */
        int cntSubSci = 0;

        /* 1教科内の科目数カウント（地歴公民） */
        int cntSubSoc = 0;

        /* 成績情報出力位置 */
        int rowRecord = IDX_ROW_LIST_START + (IDX_ROW_LIST_LINE * cntStudent);

        /* 対象模試 */
        setCellValue(rowRecord, IDX_CELL_EXAM, strExam);

        /* マーク模試のみ国語(科目コード:3910)の成績をセットする */
        /* 科目成績 */
        SelstdDprtExcelMakerStudentRecordBean record = outputData.get(0);
        if (Exam.Type.MARK.equals(record.getExamType())) {
            service.setResultsMarkStudentJapanese(exam, outputData);
        }

        /* 成績を出力する */
        for (int subcnt = 0; subcnt < outputData.size(); subcnt++) {

            /* 科目成績 */
            SelstdDprtExcelMakerStudentRecordBean subrecord = outputData.get(subcnt);

            /* 教科 */
            String course = subrecord.getCourseCd();

            /* 成績データがある場合のみ出力する */
            if (course != null) {

                /* 総合１を出力する */
                if (course.equals(BZConstants.COURSE_TOTAL1)) {
                    /* 最大科目数まで出力する */
                    if (cntSubTotal < SUBTOTALMAX && outputRecordData(rowRecord, IDX_CELL_TOTAL + cntSubTotal, false, subrecord)) {
                        cntSubTotal++;
                    }
                }

                /* 英語を出力する */
                if (course.equals(BZConstants.COURSE_ENG)) {
                    if (cntSubEng < SUBENGMAX && outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord)) {
                        cntSubEng++;
                    }
                    /*
                    if (cntSubEng < SUBENGMAX) {
                        if (subrecord.getSubCd().equals(SUBCODE_CENTER_DEFAULT_ENGLISH)) {
                            continue;
                        }
                        boolean result;
                        if (Exam.Type.MARK.equals(subrecord.getExamType())) {
                            if (subrecord.getSubCd().contentEquals(SUBCODE_CENTER_DEFAULT_ENNINTEI)) {
                                result = outputStrRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, subrecord);
                            } else {
                                result = outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord);
                            }
                        } else {
                            result = outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord);
                        }
                        if (result) {
                            cntSubEng++;
                        }
                    }
                    */
                }

                /* 数学を出力する */
                if (course.equals(BZConstants.COURSE_MAT)) {
                    /* 最大科目数まで出力する */
                    if (cntSubMat < SUBMATMAX && outputRecordData(rowRecord, IDX_CELL_MAT + cntSubMat, false, subrecord)) {
                        cntSubMat++;
                    }
                }

                /* 国語を出力する */
                if (course.equals(BZConstants.COURSE_JAP)) {
                    /* 最大科目数まで出力する */
                    if (cntSubJap < SUBJAPMAX && outputRecordData(rowRecord, IDX_CELL_JAP + cntSubJap, false, subrecord)) {
                        cntSubJap++;
                    }
                }

                /* 理科を出力する */
                if (course.equals(BZConstants.COURSE_SCI)) {
                    if (cntSubSci == 0 && FlagUtil.toBoolean(subrecord.getBasicFlg())) {
                        /* 1科目めが基礎科目なら、1列目は空欄にする */
                        cntSubSci++;
                    }
                    /* 最大科目数まで出力する */
                    if (cntSubSci < SUBSCIMAX && outputRecordData(rowRecord, IDX_CELL_SCI + cntSubSci, true, subrecord)) {
                        cntSubSci++;
                    }
                }

                /* 地歴公民を出力する */
                if (course.equals(BZConstants.COURSE_SOC)) {
                    /* 最大科目数まで出力する */
                    if (cntSubSoc < SUBSOCMAX && outputRecordData(rowRecord, IDX_CELL_SOC + cntSubSoc, true, subrecord)) {
                        cntSubSoc++;
                    }
                }
            }
        }
    }

    /**
     * 連続個人成績表のデータを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param strExam 対象模試名
     * @param outputData 成績データデータリスト（１模試分）
     * @param lpCnt 対象レコード数
     */
    private void outputRecordListTan(int cntStudent, String strExam, List<SelstdDprtExcelMakerStudentRecordBean> outputData, int lpCnt) {

        /* 1教科内の科目数カウント（総合１） */
        int cntSubTotal = 0;

        /* 1教科内の科目数カウント（英語） */
        int cntSubEng = 0;

        /* 1教科内の科目数カウント（数学） */
        int cntSubMat = 0;

        /* 1教科内の科目数カウント（国語） */
        int cntSubJap = 0;

        /* 1教科内の科目数カウント（理科） */
        int cntSubSci = 0;

        /* 1教科内の科目数カウント（地歴公民） */
        int cntSubSoc = 0;

        /* 成績情報出力位置 */
        int rowRecord = IDX_ROW_LIST_START + (IDX_ROW_LIST_LINE * cntStudent);

        /* 対象模試 */
        setCellValue(rowRecord, IDX_CELL_EXAM, strExam);

        /* 成績を出力する */
        for (int subcnt = 0; subcnt < lpCnt; subcnt++) {

            /* 科目成績 */
            SelstdDprtExcelMakerStudentRecordBean subrecord = outputData.get(subcnt);

            /* 教科 */
            String course = subrecord.getCourseCd();

            /* 成績データがある場合のみ出力する */
            if (course != null) {

                /* 総合１を出力する */
                if (course.equals(BZConstants.COURSE_TOTAL1)) {
                    /* 最大科目数まで出力する */
                    if (cntSubTotal < SUBTOTALMAX && outputRecordData(rowRecord, IDX_CELL_TOTAL + cntSubTotal, false, subrecord)) {
                        cntSubTotal++;
                    }
                }

                /* 英語を出力する */
                if (course.equals(BZConstants.COURSE_ENG)) {
                    if (cntSubEng < SUBENGMAX && outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord)) {
                        cntSubEng++;
                    }
                }

                /* 数学を出力する */
                if (course.equals(BZConstants.COURSE_MAT)) {
                    /* 最大科目数まで出力する */
                    if (cntSubMat < SUBMATMAX && outputRecordData(rowRecord, IDX_CELL_MAT + cntSubMat, false, subrecord)) {
                        cntSubMat++;
                    }
                }

                /* 国語を出力する */
                if (course.equals(BZConstants.COURSE_JAP)) {
                    /* 最大科目数まで出力する */
                    if (cntSubJap < SUBJAPMAX && outputRecordData(rowRecord, IDX_CELL_JAP + cntSubJap, false, subrecord)) {
                        cntSubJap++;
                    }
                }

                /* 理科を出力する */
                if (course.equals(BZConstants.COURSE_SCI)) {
                    if (cntSubSci == 0 && FlagUtil.toBoolean(subrecord.getBasicFlg())) {
                        /* 1科目めが基礎科目なら、1列目は空欄にする */
                        cntSubSci++;
                    }
                    /* 最大科目数まで出力する */
                    if (cntSubSci < SUBSCIMAX && outputRecordData(rowRecord, IDX_CELL_SCI + cntSubSci, true, subrecord)) {
                        cntSubSci++;
                    }
                }

                /* 地歴公民を出力する */
                if (course.equals(BZConstants.COURSE_SOC)) {
                    /* 最大科目数まで出力する */
                    if (cntSubSoc < SUBSOCMAX && outputRecordData(rowRecord, IDX_CELL_SOC + cntSubSoc, true, subrecord)) {
                        cntSubSoc++;
                    }
                }
            }
        }
    }

    /**
     * 成績を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param flgSciSoc 理科or地歴公民
     * @param subrecord 科目成績
     * @return 出力したらtrue
     */
    private boolean outputRecordData(int row, int cell, boolean flgSciSoc, SelstdDprtExcelMakerStudentRecordBean subrecord) {

        /* 模試科目マスタにない科目は出力しない */
        if (subrecord.getSubAddrName() == null) {
            return false;
        }

        /* 科目短縮名を出力する */
        setCellValue(row + IDX_ROW_LIST_SUBJECT, cell, subrecord.getSubAddrName());

        /* 科目得点を出力する */
        setCellValue(row + IDX_ROW_LIST_POINT, cell, subrecord.getSubScore());

        /* 科目偏差値を出力する */
        setCellValue(row + IDX_ROW_LIST_DEVIATION, cell, subrecord.getSubDeviation());

        return true;
    }

    /**
     * 志望大学データを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param outputData 志望大学データリスト（１人分）
     */
    private void outputCandidateUnivList(int cntStudent, List<SelstdDprtExcelMakerCandidateUnivStudentBean> outputData) {

        /* 志望大学カウント（１列当たり）*/
        int cntUniv = 0;

        /* 志望大学列カウント */
        int cntClumn = 0;

        /* 志望大学出力位置（row） */
        int rowRecord = IDX_ROW_LIST_CANDIDATEUNIV + (IDX_ROW_LIST_LINE * cntStudent);

        /* 志望大学を出力する */
        for (int index = 0; index < outputData.size(); index++) {

            /* １列の志望大学数をオーバーしたので次の列に移動する */
            if (cntUniv >= CANDIDATEUNIVCOLUMNMAX) {
                cntUniv = 0;
                cntClumn++;
            }

            /* 志望大学の出力位置（cell）*/
            int cell = IDX_CELL_UNIVNAME + (IDX_CELL_UNIV_COLUMN * cntClumn);

            /* 志望大学 */
            SelstdDprtExcelMakerCandidateUnivStudentBean candidateUniv = outputData.get(index);

            /* 成績データがある場合のみ６件出力する */
            if (candidateUniv.getDispNumber() != null && index < CANDIDATEUNIVMAX) {

                /* 大学名 */
                setCellValue(rowRecord + cntUniv, cell, candidateUniv.getUnivName());

                /* 学科名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_FACULTYNAME, candidateUniv.getFacultyName());

                /* 学部名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_DEPTNAME, candidateUniv.getDeptName());

                /* 共通テスト評価 */
                String resltValue = "";
                resltValue = candidateUniv.getValuateExclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                /*
                if (candidateUniv.getExamCd().equals(BZConstants.EXAM_RESARCH)) {
                    if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                        resltValue = "  " + ":" + candidateUniv.getValuateInclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    } else {
                        resltValue = candidateUniv.getValuateExclude() + ":" + "      " + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    }
                } else {
                    if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                        resltValue = candidateUniv.getValuateExclude() + ":" + candidateUniv.getValuateInclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    } else {
                        resltValue = candidateUniv.getValuateExclude() + ":" + "      " + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    }
                }
                */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, resltValue);

            }

            cntUniv++;
        }
    }

    /**
     * 志望大学データを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param outputData 志望大学データリスト（１人分）
     */
    private void outputCandidateUnivListTan(int cntStudent, List<SelstdDprtExcelMakerCandidateUnivStudentBean> outputData) {

        /* 志望大学カウント（１列当たり）*/
        int cntUniv = 0;

        /* 志望大学列カウント */
        int cntClumn = 0;

        /* 志望大学列出力数カウント */
        int dispcnt = 0;

        /* 志望大学出力位置（row） */
        int rowRecord = IDX_ROW_LIST_CANDIDATEUNIV + (IDX_ROW_LIST_LINE * cntStudent);

        /* 志望大学を出力する */
        for (int index = 0; index < outputData.size(); index++) {

            /* １列の志望大学数をオーバーしたので次の列に移動する */
            if (cntUniv >= CANDIDATEUNIVCOLUMNMAX) {
                cntUniv = 0;
                cntClumn++;
            }

            /* 志望大学の出力位置（cell）*/
            int cell = IDX_CELL_UNIVNAME + (IDX_CELL_UNIV_COLUMN * cntClumn);

            /* 志望大学 */
            SelstdDprtExcelMakerCandidateUnivStudentBean candidateUniv = outputData.get(index);

            /* 成績データがある場合のみ６件出力する */
            if (candidateUniv.getDispNumber() != null && dispcnt < CANDIDATEUNIVMAX) {

                /* 大学名 */
                setCellValue(rowRecord + cntUniv, cell, candidateUniv.getUnivName());

                /* 学科名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_FACULTYNAME, candidateUniv.getFacultyName());

                /* 学部名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_DEPTNAME, candidateUniv.getDeptName());

                /* 共通テスト評価 */
                String resltValue = "";
                resltValue = candidateUniv.getValuateExclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                /*
                if (candidateUniv.getExamCd().equals(BZConstants.EXAM_RESARCH)) {
                    if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                        resltValue = "  " + ":" + candidateUniv.getValuateInclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    } else {
                        resltValue = candidateUniv.getValuateExclude() + ":" + "      " + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    }
                } else {
                    if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                        resltValue = candidateUniv.getValuateExclude() + ":" + candidateUniv.getValuateInclude() + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    } else {
                        resltValue = candidateUniv.getValuateExclude() + ":" + "      " + ":" + candidateUniv.getSecondRating() + ":" + candidateUniv.getTotalRating();
                    }
                }
                */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, resltValue);

            } else {
                return;
            }
            dispcnt++;
            cntUniv++;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.result.clnd.controller;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.result.clnd.service.ResultClndService;
import jp.ac.kawai_juku.banzaisystem.result.clnd.view.ResultClndView;
import jp.ac.kawai_juku.banzaisystem.result.dfcl.controller.ResultDfclController;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.service.SelstdMainService;

import org.apache.commons.lang3.time.FastDateFormat;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 入試結果入力-大学指定(カレンダー)画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultClndController extends SubController {

    /** 表示中の月を保持するカレンダー */
    private Calendar cal;

    /** カレンダー月日フォーマット */
    private final FastDateFormat format = FastDateFormat.getInstance("M月");

    /** View */
    @Resource(name = "resultClndView")
    private ResultClndView view;

    /** サービス */
    @Resource(name = "resultClndService")
    private ResultClndService service;

    /** 対象生徒選択画面のサービス */
    @Resource
    private SelstdMainService selstdMainService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    @Override
    public void activatedAction() {

        /* 3月を初期値表示する */
        if (cal == null) {
            cal = Calendar.getInstance();
            cal.set(systemDto.getNendo().intValue() + 1, 2, 1);
        }

        /* 学年コンボボックスを初期化する */
        initGradeComboBox();
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
        for (Integer grade : selstdMainService.findGradeList()) {
            itemList.add(new ComboBoxItem<Integer>(grade));
        }
        view.gradeComboBox.setItemList(itemList);
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        initClassComboBox();
        return null;
    }

    /**
     * クラスコンボボックスを初期化します。
     */
    private void initClassComboBox() {
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年が存在しない場合 */
            view.classComboBox.setItemList(Collections.<ComboBoxItem<String>> emptyList());
        } else {
            /* 学年が存在する場合 */
            List<ComboBoxItem<String>> itemList = CollectionsUtil.newArrayList();
            for (String cls : selstdMainService.findClassList(grade.getValue())) {
                itemList.add(new ComboBoxItem<String>(cls));
            }
            view.classComboBox.setItemList(itemList);
        }
    }

    /**
     * クラスコンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult classComboBoxAction() {

        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年が存在しない場合 */
            view.calendarTable.setSucAnnDateList(Collections.<ResultUnivBean> emptyList());
        } else {
            /* 学年が存在する場合 */
            view.calendarTable.setSucAnnDateList(service.findSucAnnDateList(grade.getValue(),
                    view.classComboBox.getSelectedValue()));
        }

        /* カレンダーを描画する */
        drawCalendar();

        return null;
    }

    /**
     * カレンダーを描画します。
     */
    private void drawCalendar() {
        view.monthLabel.setText(format.format(cal));
        view.calendarTable.setDispMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));
    }

    /**
     * 前月ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult beforeMonthButtonAction() {
        cal.add(Calendar.MONTH, -1);
        drawCalendar();
        return null;
    }

    /**
     * 今月ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult currentMonthButtonAction() {
        int month = Calendar.getInstance().get(Calendar.MONTH);
        cal.set(Calendar.YEAR, month < 3 ? systemDto.getNendo().intValue() + 1 : systemDto.getNendo().intValue());
        cal.set(Calendar.MONTH, month);
        drawCalendar();
        return null;
    }

    /**
     * 次月ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult nextMonthButtonAction() {
        cal.add(Calendar.MONTH, 1);
        drawCalendar();
        return null;
    }

    /**
     * 学部学科変更ボタン押下アクションです。
     *
     * @return 学部学科選択ダイアログ
     */
    @Execute
    public ExecuteResult changeButtonAction() {
        return forward(ResultDfclController.class, null, view.examineeTable.getExamineeBean());
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.component.ExmntnUnivImageLabel;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.component.ExmntnUnivTextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageToggleButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.VerticalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 志望大学詳細画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivView extends SubView {

    /** グループ番号：判定科目コンポーネント */
    public static final int GROUP_EXAM_SUBJECT = 1;

    /** グループ番号：公表科目コンポーネント */
    public static final int GROUP_PUB_SUBJECT = 2;

    /** コンボボックス_志望順位 */
    @Location(x = 62, y = 7)
    @Size(width = 43, height = 20)
    @FireActionEvent
    public ComboBox<ExmntnMainCandidateUnivBean> candidateRankComboBox;

    /** 募集人数ラベル */
    @Location(x = 71, y = 28)
    @Size(width = 46, height = 18)
    @Font(BZFont.BOLD)
    public ExmntnUnivTextLabel capacityLabel;

    /** 大学名ラベル */
    @Location(x = 228, y = 5)
    @Size(width = 267, height = 19)
    @Font(BZFont.UNIV_NAME)
    public ExmntnUnivTextLabel univNameLabel;

    /** 本部所在地ラベル */
    @Location(x = 71, y = 46)
    @Size(width = 46, height = 15)
    @Font(BZFont.BOLD)
    public ExmntnUnivTextLabel properLocationLabel;

    /** キャンパス所在地ラベル */
    @Location(x = 226, y = 46)
    @Size(width = 46, height = 15)
    @Font(BZFont.BOLD)
    public ExmntnUnivTextLabel campusLocationLabel;

    /** 総合判定ラベル */
    @Location(x = 560, y = 11)
    @Size(width = 60, height = 50)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.RATING)
    public ExmntnUnivTextLabel totalRatingLabel;

    /** 共通テスト判定ラベル */
    @Location(x = 585, y = 78)
    @Size(width = 60, height = 50)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.RATING)
    public ExmntnUnivTextLabel centerRatingLabel;

    /** 共通テスト（含む）判定ラベル */
    /*@Location(x = 639, y = 75)
    @Size(width = 35, height = 50)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.RATING)
    public ExmntnUnivTextLabel centerIncludeRatingLabel;*/

    /** 二次判定ラベル */
    @Location(x = 585, y = 258)
    @Size(width = 60, height = 50)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.RATING)
    public ExmntnUnivTextLabel secondRatingLabel;

    /** 総合判定アイコン */
    @Location(x = 511, y = 10)
    public ExmntnUnivImageLabel resultTotalLabel;

    /** 共通テスト判定アイコン */
    @Location(x = 531, y = 79)
    public ExmntnUnivImageLabel resultCenterLabel;

    /** 共通テスト（含む）判定アイコン */
    /*@Location(x = 589, y = 86)
    public ExmntnUnivImageLabel resultCenterIncludeLabel;*/

    /** 共通テスト（含む）英語資格要件判定１アイコン */
    /*@Location(x = 620, y = 86)
    public ExmntnUnivImageLabel resultCenterIncludeEngLabel;*/

    /** 二次判定アイコン */
    @Location(x = 531, y = 257)
    public ExmntnUnivImageLabel resultSecondLabel;

    /** 総合判定：評価ポイントラベル */
    @Location(x = 621, y = 35)
    @Size(width = 52, height = 28)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.RATING_POINT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel totalRatingPointLabel;

    /** 共通テスト：公表科目ラベル */
    @Location(x = 7, y = 83)
    @Size(width = 54, height = 74)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 51, b = 152)
    @Group(GROUP_PUB_SUBJECT)
    public TextLabel centerPubSubjectLabel;

    /** 共通テスト：評価得点 */
    @Location(x = 572, y = 144)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerScoreLabel;

    /** 共通テスト：満点 */
    @Location(x = 572, y = 157)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerFullScoreLabel;

    /** 共通テスト：ボーダーライン */
    @Location(x = 572, y = 170)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerBorderLabel;

    /** 共通テスト：評価基準A */
    @Location(x = 572, y = 186)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerALineLabel;

    /** 共通テスト：評価基準B */
    @Location(x = 572, y = 199)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerBLineLabel;

    /** 共通テスト：評価基準C */
    @Location(x = 572, y = 212)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerCLineLabel;

    /** 共通テスト：評価基準D */
    @Location(x = 572, y = 225)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerDLineLabel;

    /** 共通テスト：あと何点-判定 */
    @Location(x = 619, y = 149)
    @Size(width = 47, height = 23)
    public ExmntnUnivTextLabel centerRemainRaitingLabel;

    /** 共通テスト：あと何点-得点 */
    @Location(x = 614, y = 172)
    @Size(width = 52, height = 27)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerRemainScoreLabel;

    /** 共通テスト（含む）：英語資格要件 */
    /*@Location(x = 639, y = 110)
    @Size(width = 35, height = 27)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeEnglishRequirementsLabel;*/

    /** 共通テスト（含む）：あと何点-判定 */
    /*@Location(x = 599, y = 137)
    @Size(width = 47, height = 23)
    public ExmntnUnivTextLabel centerIncludeRemainRaitingLabel;*/

    /** 共通テスト（含む）：あと何点-得点 */
    /*@Location(x = 582, y = 146)
    @Size(width = 52, height = 27)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeRemainScoreLabel;*/

    /** 共通テスト（含む）：評価得点 */
    /*@Location(x = 648, y = 170)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerIncludeScoreLabel;*/

    /** 共通テスト（含む）：満点 */
    /*@Location(x = 648, y = 181)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerIncludeFullScoreLabel;*/

    /** 共通テスト（含む）：ボーダーライン */
    /*@Location(x = 648, y = 192)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel centerIncludeBorderLabel;*/

    /** 共通テスト（含む）：評価基準A */
    /*@Location(x = 648, y = 205)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeALineLabel;*/

    /** 共通テスト（含む）：評価基準B */
    /*@Location(x = 648, y = 218)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeBLineLabel;*/

    /** 共通テスト（含む）：評価基準C */
    /*@Location(x = 648, y = 231)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeCLineLabel;*/

    /** 共通テスト（含む）：評価基準D */
    /*@Location(x = 648, y = 244)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public ExmntnUnivTextLabel centerIncludeDLineLabel;*/

    /** 共通テスト：公表科目 */
    @Location(x = 65, y = 70)
    @Size(width = 424, height = 67)
    @VerticalAlignment(SwingConstants.NORTH)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerPubSubCountLabel;

    /** 共通テスト：配点 英語 */
    @Location(x = 58, y = 159)
    @Size(width = 55, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotEnglishPubLabel;

    /** 共通テスト：配点 英語認定 */
    /*@Location(x = 93, y = 159)
    @Size(width = 55, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotEnglishJoiningPubLabel;*/

    /** 共通テスト：配点 数学 */
    @Location(x = 108, y = 159)
    @Size(width = 97, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotMathPubLabel;

    /** 共通テスト：配点 国語(マ) */
    @Location(x = 200, y = 159)
    @Size(width = 54, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotLanguagePubLabel;

    /** 共通テスト：配点 国語(記述) */
    /*@Location(x = 228, y = 159)
    @Size(width = 54, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotLanguageDescPubLabel;*/

    /** 共通テスト：配点 理科 */
    @Location(x = 249, y = 159)
    @Size(width = 146, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotSciencePubLabel;

    /** 共通テスト：配点 地歴公民 */
    @Location(x = 395, y = 159)
    @Size(width = 96, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel centerAllotSocietyPubLabel;

    /** 共通テスト：本人得点 英語 */
    @Location(x = 58, y = 178)
    @Size(width = 55, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreEnglishLabel;

    /** 共通テスト：本人得点 英語認定試験 */
    /*@Location(x = 93, y = 178)
    @Size(width = 55, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreEnglishJoiningLabel;*/

    /** 共通テスト：本人得点 数学１ */
    @Location(x = 109, y = 178)
    @Size(width = 47, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreMathILabel;

    /** 共通テスト：本人得点 数学２ */
    @Location(x = 154, y = 178)
    @Size(width = 50, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreMathIILabel;

    /** 共通テスト：本人得点 国語(マ) */
    @Location(x = 200, y = 178)
    @Size(width = 54, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreLanguageLabel;

    /** 共通テスト：本人得点 国語(記) */
    /*@Location(x = 228, y = 178)
    @Size(width = 54, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreLanguageDescLabel;*/

    /** 共通テスト：本人得点 理科1 */
    @Location(x = 251, y = 178)
    @Size(width = 47, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreScienceILabel;

    /** 共通テスト：本人得点 理科2 */
    @Location(x = 298, y = 178)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreScienceIILabel;

    /** 共通テスト：本人得点 理科3 */
    @Location(x = 346, y = 178)
    @Size(width = 49, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreScienceIIILabel;

    /** 共通テスト：本人得点 地歴公民1 */
    @Location(x = 395, y = 178)
    @Size(width = 47, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreSocietyILabel;

    /** 共通テスト：本人得点 地歴公民2 */
    @Location(x = 442, y = 178)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel centerScoreSocietyIILabel;

    /** 【未使用】共通テスト：備考 */
    @Location(x = 65, y = 195)
    @Size(width = 449, height = 29)
    @VerticalAlignment(SwingConstants.NORTH)
    @Invisible
    public ExmntnUnivTextLabel centerDescriptionLabel;

    /** 【未使用】共通テスト：2段階選抜 */
    @Location(x = 76, y = 230)
    @Size(width = 76, height = 17)
    @Invisible
    public ExmntnUnivTextLabel centerSecondSelectedLabel;

    /** 【未使用】共通テスト：1段階選抜 */
    @Location(x = 259, y = 228)
    @Size(width = 255, height = 17)
    @Invisible
    public ExmntnUnivTextLabel centerFirstSelectedLabel;

    /** 共通テスト：1段階選抜予想 本人得点*/
    @Location(x = 125, y = 197)
    @Size(width = 489, height = 17)
    public ExmntnUnivTextLabel centerFirstSelectedScoreLabel;

    /** 共通テスト：注意事項ラベル */
    @Location(x = 11, y = 222)
    @Foreground(r = 0, g = 51, b = 152)
    @Invisible
    public TextLabel centerAttentionLabel;

    /** 共通テスト：注釈文 */
    @Location(x = 65, y = 215)
    @Size(width = 424, height = 27)
    @VerticalAlignment(SwingConstants.NORTH)
    public ExmntnUnivTextLabel centerCommentStatementLabel;

    /** 二次試験：公表科目ラベル */
    @Location(x = 7, y = 266)
    @Size(width = 54, height = 57)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 51, b = 152)
    @Group(GROUP_PUB_SUBJECT)
    public TextLabel secondPubSubjectLabel;

    /** 二次試験：評価得点 */
    @Location(x = 572, y = 321)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel secondScoreLabel;

    /** 二次試験：満点 */
    @Location(x = 572, y = 334)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel secondFullScoreLabel;

    /** 二次試験：ボーダーライン */
    @Location(x = 572, y = 347)
    @Size(width = 24, height = 13)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Foreground(r = 51, g = 0, b = 255)
    public ExmntnUnivTextLabel secondBorderLabel;

    /** 二次試験：評価基準A */
    @Location(x = 572, y = 364)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Size(width = 24, height = 13)
    public ExmntnUnivTextLabel secondALineLabel;

    /** 二次試験：評価基準B */
    @Location(x = 572, y = 377)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Size(width = 24, height = 13)
    public ExmntnUnivTextLabel secondBLineLabel;

    /** 二次試験：評価基準C */
    @Location(x = 572, y = 390)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Size(width = 24, height = 13)
    public ExmntnUnivTextLabel secondCLineLabel;

    /** 二次試験：評価基準D */
    @Location(x = 572, y = 403)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Size(width = 24, height = 13)
    public ExmntnUnivTextLabel secondDLineLabel;

    /** 二次試験：公表科目 */
    @Location(x = 65, y = 247)
    @Size(width = 424, height = 52)
    @VerticalAlignment(SwingConstants.NORTH)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondPubSubCountLabel;

    /** 二次試験：タイトル その他 */
    @Location(x = 320, y = 317)
    @Size(width = 171, height = 16)
    @HorizontalAlignment(SwingConstants.CENTER)
    @VerticalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 51, b = 152)
    public ExmntnUnivTextLabel secondOtherPubLabel;

    /** 二次試験：配点 英語 */
    @Location(x = 56, y = 336)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotEnglishPubLabel;

    /** 二次試験：配点 数学 */
    @Location(x = 92, y = 336)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotMathPubLabel;

    /** 二次試験：配点 国語 */
    @Location(x = 130, y = 336)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotLanguagePubLabel;

    /** 二次試験：配点 理科 */
    @Location(x = 186, y = 336)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotSciencePubLabel;

    /** 二次試験：配点 社会 */
    @Location(x = 260, y = 336)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotSocietyPubLabel;

    /** 二次試験：配点 その他 */
    @Location(x = 320, y = 336)
    @Size(width = 171, height = 37)
    @HorizontalAlignment(SwingConstants.LEFT)
    @VerticalAlignment(SwingConstants.NORTH)
    @Foreground(r = 0, g = 0, b = 255)
    @Group(GROUP_PUB_SUBJECT)
    public ExmntnUnivTextLabel secondAllotOtherPubLabel;

    /** 二次試験：偏差値 英語 */
    @Location(x = 56, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationEnglishLabel;

    /** 二次試験：偏差値 数学 */
    @Location(x = 92, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationMathLabel;

    /** 二次試験：偏差値 国語 */
    @Location(x = 130, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationLanguageLabel;

    /** 二次試験：偏差値 理科1 */
    @Location(x = 166, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationScienceILabel;

    /** 二次試験：偏差値 理科2 */
    @Location(x = 204, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationScienceIILabel;

    /** 二次試験：偏差値 社会1 */
    @Location(x = 240, y = 355)
    @Size(width = 48, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationSocietyILabel;

    /** 二次試験：偏差値 社会2 */
    @Location(x = 274, y = 355)
    @Size(width = 52, height = 18)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondDeviationSocietyIILabel;

    /** 二次試験：備考 */
    @Location(x = 65, y = 390)
    @Size(width = 427, height = 28)
    @VerticalAlignment(SwingConstants.NORTH)
    @Invisible
    public ExmntnUnivTextLabel secondDescriptionLabel;

    /** 二次試験：あと何点-判定 */
    @Location(x = 619, y = 327)
    @Size(width = 47, height = 23)
    public ExmntnUnivTextLabel secondRemainRaitingLabel;

    /** 二次試験：あと何点-得点 */
    @Location(x = 614, y = 348)
    @Size(width = 52, height = 27)
    @HorizontalAlignment(SwingConstants.CENTER)
    public ExmntnUnivTextLabel secondRemainScoreLabel;

    /** 備考欄ラベル */
    @Location(x = 6, y = 421)
    @Size(width = 481, height = 34)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel remarksLabel;

    /** 前の大学ボタン */
    @Location(x = 118, y = 8)
    @Disabled
    public ImageButton beforeUnivButton;

    /** 次の大学ボタン */
    @Location(x = 169, y = 8)
    @Disabled
    public ImageButton nextUnivButton;

    /** この大学の入試情報を見るボタン */
    @Location(x = 312, y = 25)
    public ImageButton showUnivButton;

    /** 公表科目表示ボタン */
    @Location(x = 408, y = 52)
    @Invisible
    public ImageToggleButton pubSubjectButton;

    /** 学力分布ボタン */
    @Location(x = 612, y = 207)
    public ImageButton academicDistribution1Button;

    /** 学力分布ボタン */
    @Location(x = 612, y = 384)
    public ImageButton academicDistribution2Button;

    /** 個人成績表ボタン */
    @Location(x = 495, y = 426)
    public ImageButton printPersonalScoreButton;

    /** 大学詳細印刷ボタン */
    @Location(x = 589, y = 426)
    public ImageButton printUnivDetailButton;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
        candidateRankComboBox.setMaximumRowCount(9);
    }

}

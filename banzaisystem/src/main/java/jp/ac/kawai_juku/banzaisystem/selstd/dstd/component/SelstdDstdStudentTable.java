package jp.ac.kawai_juku.banzaisystem.selstd.dstd.component;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.view.SelstdDstdView;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainTable;

import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 生徒一覧ダイアログの生徒テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdDstdStudentTable extends SelstdMainTable {

    /**
     * コンストラクタです。
     */
    public SelstdDstdStudentTable() {
        super(null, null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        BufferedImage img = ImageUtil.readImage(getClass(), "table/upper_right_corner.png");
        JLabel label = new JLabel(new ImageIcon(img));
        label.setSize(img.getWidth(), img.getHeight());
        setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, label);
        getTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                SelstdDstdView view = SingletonS2Container.getComponent(SelstdDstdView.class);
                view.okButton.setEnabled(getTable().getSelectedRowCount() > 0);
            }
        });
    }

    @Override
    protected JPanel createColumnHeaderView() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.add(createHeaderLabel("class", CellAlignment.CENTER, true));
        panel.add(createHeaderLabel("number", CellAlignment.CENTER, true));
        panel.add(createHeaderLabel("name", CellAlignment.LEFT, false));
        return panel;
    }

    /**
     * テーブルの選択モードを設定します。
     *
     * @param selectionMode 選択モード
     */
    public void setSelectionMode(int selectionMode) {
        getTable().setSelectionMode(selectionMode);
    }

    @Override
    protected JPanel createFixedColumnHeaderView() {
        return null;
    }

    @Override
    public List<Integer> getSelectedIndividualIdList() {
        List<Integer> list = CollectionsUtil.newArrayList();
        JTable table = getTable();
        TableModel model = table.getModel();
        for (int rowIndex : table.getSelectedRows()) {
            Object id = ((CellData) model.getValueAt(rowIndex, 0)).getDispValue();
            list.add((Integer) id);
        }
        return list;
    }

}

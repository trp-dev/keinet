package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * ルートノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class RootNode extends DefaultMutableTreeNode implements Node {

    /** フォルダアイコン */
    private static final ImageIcon ICON;
    static {
        ICON = new ImageIcon(ImageUtil.readImage(RootNode.class, "icon/folder.png"));
    }

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    RootNode(Object userObject) {
        super(userObject);
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation.CenterSubjectConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * センター科目チェックボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CenterCheckBox extends SubjectCheckBox<CenterSubject> {

    /** センター科目 */
    private CenterSubject subject;

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        subject = field.getAnnotation(CenterSubjectConfig.class).value();
    }

    @Override
    protected int getBoxHeight() {
        return 70;
    }

    @Override
    public CenterSubject getSubject() {
        return subject;
    }

}

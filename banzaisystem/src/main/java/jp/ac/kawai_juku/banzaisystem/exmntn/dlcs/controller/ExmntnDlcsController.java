package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.controller;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.service.ExmntnDlcsService;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.view.ExmntnDlcsView;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;

import org.seasar.framework.log.Logger;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 取得可能資格ダイアログのControllerです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnDlcsController extends AbstractController {

    /** Logger */
    private static final Logger logger = Logger.getLogger(ExmntnDlcsController.class);

    /** Service */
    @Resource
    private ExmntnDlcsService exmntnDlcsService;

    /** View */
    @Resource
    private ExmntnDlcsView exmntnDlcsView;

    /** 志望大学詳細View（取得可能資格ダイアログの呼び出し元） */
    @Resource
    private ExmntnUnivView exmntnUnivView;

    /**
     * 初期表示アクションです。
     * <br/>関数の実行タイミング：「ダイアログが呼び出された」
     *
     * @return Viewを表示する結果オブジェクト（→取得可能資格ダイアログが表示される）
     */
    @Logging(GamenId.C1_d1)
    @Execute
    public ExecuteResult index() {
        /* ダイアログ上の表示要素を初期化します。*/
        initControls();
        /* 返却物：Viewを表示する結果オブジェクト */
        return VIEW_RESULT;
    }

    /**
     * OKボタンアクションです。
     * <br/>関数の実行タイミング：「OKボタンがクリックされた」
     *
     * @return Viewを閉じる結果オブジェクト（→ダイアログを閉じるアクションが実行される）
     */
    @Execute
    public ExecuteResult closeButtonAction() {
        /* 処理概要：ダイアログを閉じる */
        /* ＃キャンセル時は特に何もしない */
        /* 返却物：Viewを閉じる結果オブジェクト */
        return CLOSE_RESULT;
    }

    /* ▼ 画面要素-初期化関連：ここから */

    /**
     * ダイアログ上の表示要素を初期化します。
     */
    private void initControls() {
        /* 資格名リストの表示テーブルを初期化する */
        initNameTable();
    }

    /**
     * 資格名リストの表示テーブルを初期化します。
     */
    private void initNameTable() {
        /* 親画面上で現在選択されている志望大学の情報を取得 */
        ExmntnMainCandidateUnivBean selectedUnivBean = exmntnUnivView.candidateRankComboBox.getSelectedValue();
        if (selectedUnivBean == null) {
            /* 取得可能資格が有る場合のみ取得可能資格ダイアログが呼び出される仕様のため
             * この分岐に処理が流れて来ることは理屈上あり得ない。（この分岐はfoolproofとしてのみ存在。）
             */
            if (logger.isDebugEnabled()) {
                logger.debug("志望大学の情報を得られませんでした");
            }
            /* 既存の表示情報をクリア */
            List<String> blankList = CollectionsUtil.newArrayList();
            exmntnDlcsView.nameTable.setNameList(blankList);
        } else {
            /* 資格情報の取得条件となる 大学コード・学部コード・学科コード を取得 */
            String univCd = selectedUnivBean.getUnivCd();
            String fauctyCd = selectedUnivBean.getFacultyCd();
            String deptCd = selectedUnivBean.getDeptCd();
            if (logger.isDebugEnabled()) {
                logger.debug("志望大学の情報 : ");
                logger.debug("- (" + univCd + ") " + selectedUnivBean.getUnivName());
                logger.debug("- (" + fauctyCd + ") " + selectedUnivBean.getFacultyName());
                logger.debug("- (" + deptCd + ") " + selectedUnivBean.getDeptName());
            }

            /* 大学-学部-学科に紐付く資格の一覧を取得 */
            List<String> nameList = exmntnDlcsService.findQualifiableCertificationList(univCd, fauctyCd, deptCd);
            if (logger.isDebugEnabled()) {
                logger.debug("取得可能資格の数 : " + nameList.size());
            }

            /* 大学-学部-学科に紐付く資格の一覧を資格名リストとして格納 */
            exmntnDlcsView.nameTable.setNameList(nameList);
        }
    }

    /* ▲ 画面要素-初期化関連：ここまで */
}

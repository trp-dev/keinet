package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

/**
 *
 * CEFR成績保存用のDAO入力Beanです。
 *
 *
 * @author QQ)NAKAO.Takaaki
 * @author QQ)NAKAO.Takaaki
 */
public class ExmntnMainSaveCefrDaoInBean {

    /** 個人ID */
    private Integer individualId;

    /** 模試年度 */
    private String examYear;

    /** 模試区分 */
    private String examDiv;

    /** 認定試験コード1 */
    private String examCd1;

    /** 認定試験レベルコード1 */
    private String cefrExamLevel1;

    /** スコア1 */
    private Double cefrScore1;

    /** スコア補正フラグ1 */
    private String scoreCorrectFlg1;

    /** CEFRレベルコード1 */
    private String cefrLevelCD1;

    /** CEFRレベルコード補正フラグ1 */
    private String cefrLevelCdCorrectFlg1;

    /** 合否1 */
    private String examResult1;

    /** 合否補正フラグ1 */
    private String examResultCorrectFlg1;

    /** 不明スコア1 */
    private String unknownScore1;

    /** 不明CEFR1 */
    private String unknownCefr1;

    /** 認定試験コード2 */
    private String examCd2;

    /** 認定試験レベルコード2 */
    private String cefrExamLevel2;

    /** スコア2 */
    private Double cefrScore2;

    /** スコア補正フラグ2 */
    private String scoreCorrectFlg2;

    /** CEFRレベルコード2 */
    private String cefrLevelCD2;

    /** CEFRレベルコード補正フラグ2 */
    private String cefrLevelCdCorrectFlg2;

    /** 合否2 */
    private String examResult2;

    /** 合否補正フラグ2 */
    private String examResultCorrectFlg2;

    /** 不明スコア2 */
    private String unknownScore2;

    /** 不明CEFR2 */
    private String unknownCefr2;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

    /**
     * 認定試験コード1を返します。
     *
     * @return 認定試験コード1
     */
    public String getExamCd1() {
        return examCd1;
    }

    /**
     * 認定試験コード1をセットします。
     *
     * @param examCd1 認定試験コード1
     */
    public void setExamCd1(String examCd1) {
        this.examCd1 = examCd1;
    }

    /**
     * 認定試験レベルコード1を返します。
     *
     * @return 認定試験レベルコード1
     */
    public String getCefrExamLevel1() {
        return cefrExamLevel1;
    }

    /**
     * 認定試験レベルコード1をセットします。
     *
     * @param cefrExamLevel1 認定試験レベルコード1
     */
    public void setCefrExamLevel1(String cefrExamLevel1) {
        this.cefrExamLevel1 = cefrExamLevel1;
    }

    /**
     * スコア1を返します。
     *
     * @return スコア1
     */
    public Double getCefrScore1() {
        return cefrScore1;
    }

    /**
     * スコア1をセットします。
     *
     * @param cefrScore1 スコア1
     */
    public void setCefrScore1(Double cefrScore1) {
        this.cefrScore1 = cefrScore1;
    }

    /**
     * スコア補正フラグ1を返します。
     *
     * @return スコア補正フラグ1
     */
    public String getScoreCorrectFlg1() {
        return scoreCorrectFlg1;
    }

    /**
     * スコア補正フラグ1をセットします。
     *
     * @param scoreCorrectFlg1 スコア補正フラグ1
     */
    public void setScoreCorrectFlg1(String scoreCorrectFlg1) {
        this.scoreCorrectFlg1 = scoreCorrectFlg1;
    }

    /**
     * CEFRレベルコード1を返します。
     *
     * @return CEFRレベルコード1
     */
    public String getCefrLevelCD1() {
        return cefrLevelCD1;
    }

    /**
     * CEFRレベルコード1をセットします。
     *
     * @param cefrLevelCD1 CEFRレベルコード1
     */
    public void setCefrLevelCD1(String cefrLevelCD1) {
        this.cefrLevelCD1 = cefrLevelCD1;
    }

    /**
     * CEFRレベルコード補正フラグ1を返します。
     *
     * @return CEFRレベルコード補正フラグ1
     */
    public String getCefrLevelCdCorrectFlg1() {
        return cefrLevelCdCorrectFlg1;
    }

    /**
     * CEFRレベルコード補正フラグ1をセットします。
     *
     * @param cefrLevelCdCorrectFlg1 CEFRレベルコード補正フラグ1
     */
    public void setCefrLevelCdCorrectFlg1(String cefrLevelCdCorrectFlg1) {
        this.cefrLevelCdCorrectFlg1 = cefrLevelCdCorrectFlg1;
    }

    /**
     * 合否1を返します。
     *
     * @return 合否1
     */
    public String getExamResult1() {
        return examResult1;
    }

    /**
     * 合否1をセットします。
     *
     * @param examResult1 合否1
     */
    public void setExamResult1(String examResult1) {
        this.examResult1 = examResult1;
    }

    /**
     * 合否補正フラグ1を返します。
     *
     * @return 合否補正フラグ1
     */
    public String getExamResultCorrectFlg1() {
        return examResultCorrectFlg1;
    }

    /**
     * 合否補正フラグ1をセットします。
     *
     * @param examResultCorrectFlg1 合否補正フラグ1
     */
    public void setExamResultCorrectFlg1(String examResultCorrectFlg1) {
        this.examResultCorrectFlg1 = examResultCorrectFlg1;
    }

    /**
     * 不明スコア1を返します。
     *
     * @return 不明スコア1
     */
    public String getUnknownScore1() {
        return unknownScore1;
    }

    /**
     * 不明スコア1をセットします。
     *
     * @param unknownScore1 不明スコア1
     */
    public void setUnknownScore1(String unknownScore1) {
        this.unknownScore1 = unknownScore1;
    }

    /**
     * 不明CEFR1を返します。
     *
     * @return 不明CEFR1
     */
    public String getUnknownCefr1() {
        return unknownCefr1;
    }

    /**
     * 不明CEFR1をセットします。
     *
     * @param unknownCefr1 不明CEFR1
     */
    public void setUnknownCefr1(String unknownCefr1) {
        this.unknownCefr1 = unknownCefr1;
    }

    /**
     * 認定試験コード2を返します。
     *
     * @return 認定試験コード2
     */
    public String getExamCd2() {
        return examCd2;
    }

    /**
     * 認定試験コード2をセットします。
     *
     * @param examCd2 認定試験コード2
     */
    public void setExamCd2(String examCd2) {
        this.examCd2 = examCd2;
    }

    /**
     * 認定試験レベルコード2を返します。
     *
     * @return 認定試験レベルコード2
     */
    public String getCefrExamLevel2() {
        return cefrExamLevel2;
    }

    /**
     * 認定試験レベルコード2をセットします。
     *
     * @param cefrExamLevel2 認定試験レベルコード2
     */
    public void setCefrExamLevel2(String cefrExamLevel2) {
        this.cefrExamLevel2 = cefrExamLevel2;
    }

    /**
     * スコア2を返します。
     *
     * @return スコア2
     */
    public Double getCefrScore2() {
        return cefrScore2;
    }

    /**
     * スコア2をセットします。
     *
     * @param cefrScore2 スコア2
     */
    public void setCefrScore2(Double cefrScore2) {
        this.cefrScore2 = cefrScore2;
    }

    /**
     * スコア補正フラグ2を返します。
     *
     * @return スコア補正フラグ2
     */
    public String getScoreCorrectFlg2() {
        return scoreCorrectFlg2;
    }

    /**
     * スコア補正フラグ2をセットします。
     *
     * @param scoreCorrectFlg2 スコア補正フラグ2
     */
    public void setScoreCorrectFlg2(String scoreCorrectFlg2) {
        this.scoreCorrectFlg2 = scoreCorrectFlg2;
    }

    /**
     * CEFRレベルコード2を返します。
     *
     * @return CEFRレベルコード2
     */
    public String getCefrLevelCD2() {
        return cefrLevelCD2;
    }

    /**
     * CEFRレベルコード2をセットします。
     *
     * @param cefrLevelCD2 CEFRレベルコード2
     */
    public void setCefrLevelCD2(String cefrLevelCD2) {
        this.cefrLevelCD2 = cefrLevelCD2;
    }

    /**
     * CEFRレベルコード補正フラグ2を返します。
     *
     * @return CEFRレベルコード補正フラグ2
     */
    public String getCefrLevelCdCorrectFlg2() {
        return cefrLevelCdCorrectFlg2;
    }

    /**
     * CEFRレベルコード補正フラグ2をセットします。
     *
     * @param cefrLevelCdCorrectFlg2 CEFRレベルコード補正フラグ2
     */
    public void setCefrLevelCdCorrectFlg2(String cefrLevelCdCorrectFlg2) {
        this.cefrLevelCdCorrectFlg2 = cefrLevelCdCorrectFlg2;
    }

    /**
     * 合否2を返します。
     *
     * @return 合否2
     */
    public String getExamResult2() {
        return examResult2;
    }

    /**
     * 合否2をセットします。
     *
     * @param examResult2 合否2
     */
    public void setExamResult2(String examResult2) {
        this.examResult2 = examResult2;
    }

    /**
     * 合否補正フラグ2を返します。
     *
     * @return 合否補正フラグ2
     */
    public String getExamResultCorrectFlg2() {
        return examResultCorrectFlg2;
    }

    /**
     * 合否補正フラグ2をセットします。
     *
     * @param examResultCorrectFlg2 合否補正フラグ2
     */
    public void setExamResultCorrectFlg2(String examResultCorrectFlg2) {
        this.examResultCorrectFlg2 = examResultCorrectFlg2;
    }

    /**
     * 不明スコア2を返します。
     *
     * @return 不明スコア2
     */
    public String getUnknownScore2() {
        return unknownScore2;
    }

    /**
     * 不明スコア2をセットします。
     *
     * @param unknownScore2 不明スコア2
     */
    public void setUnknownScore2(String unknownScore2) {
        this.unknownScore2 = unknownScore2;
    }

    /**
     * 不明CEFR2を返します。
     *
     * @return 不明CEFR2
     */
    public String getUnknownCefr2() {
        return unknownCefr2;
    }

    /**
     * 不明CEFR2をセットします。
     *
     * @param unknownCefr2 不明CEFR2
     */
    public void setUnknownCefr2(String unknownCefr2) {
        this.unknownCefr2 = unknownCefr2;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.stng.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.annotation.DataFolderConfig;

/**
 *
 * メンテナンス-環境設定画面のフォルダテキストフィールドです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MainteStngFolderField extends TextField {

    /** データフォルダ */
    private DataFolder dataFolder;

    /**
     * コンストラクタです。
     */
    public MainteStngFolderField() {
        setEditable(false);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        dataFolder = field.getAnnotation(DataFolderConfig.class).value();
    }

    /**
     * データフォルダを返します。
     *
     * @return データフォルダ
     */
    public DataFolder getDataFolder() {
        return dataFolder;
    }

}

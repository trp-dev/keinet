package jp.ac.kawai_juku.banzaisystem.exmntn.main.dto;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.StudentExamPlanUnivBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 志望大学個人成績画面のDTOです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainDto {

    /** 生徒用の志望大学リスト */
    private List<UnivKeyBean> studentCandidateUnivList;

    /** 生徒用の受験予定大学リスト */
    private List<StudentExamPlanUnivBean> studentExamPlanUnivList;

    /** 判定実行時の成績データ */
    private ScoreBean judgeScore;

    /** 成績変更有無：マーク模試 */
    private boolean isChangeMarkScore;

    /** 成績変更有無：記述模試 */
    private boolean isChangeWrittenScore;

    /** 成績変更有無：マーク(CEFR) */
    /* private boolean isChangeMarkCefrScore; */

    /** 成績変更有無：マーク(国語記述) */
    private boolean isChangeMarkJpnScore;

    /**
     * 生徒用の志望大学リストを返します。
     *
     * @return 生徒用の志望大学リスト
     */
    public List<UnivKeyBean> getStudentCandidateUnivList() {
        return studentCandidateUnivList;
    }

    /**
     * 生徒用の志望大学リストをセットします。
     *
     * @param studentCandidateUnivList 生徒用の志望大学リスト
     */
    public void setStudentCandidateUnivList(List<UnivKeyBean> studentCandidateUnivList) {
        this.studentCandidateUnivList = studentCandidateUnivList;
    }

    /**
     * 生徒用の受験予定大学リストを返します。
     *
     * @return 生徒用の受験予定大学リスト
     */
    public List<StudentExamPlanUnivBean> getStudentExamPlanUnivList() {
        return studentExamPlanUnivList;
    }

    /**
     * 生徒用の受験予定大学リストをセットします。
     *
     * @param studentExamPlanUnivList 生徒用の受験予定大学リスト
     */
    public void setStudentExamPlanUnivList(List<StudentExamPlanUnivBean> studentExamPlanUnivList) {
        this.studentExamPlanUnivList = studentExamPlanUnivList;
    }

    /**
     * 成績変更有無を初期化します。
     */
    public void initChangeScoreState() {
        this.isChangeMarkScore = false;
        this.isChangeWrittenScore = false;
        /* this.isChangeMarkCefrScore = false; */
        this.isChangeMarkJpnScore = false;
    }

    /**
     * 成績変更有無：マーク模試を返します。
     *
     * @return 成績変更有無：マーク模試
     */
    /*
    public boolean isChangeMarkCefrScore() {
        return isChangeMarkCefrScore;
    }
    */
    /**
     * 成績変更有無：マーク模試(CEFR)をセットします。
     *
     * @param isChangeMarkCefrScore 成績変更有無：マーク模試(CEFR)
     */

    /*
    public void setChangeMarkCefrScore(boolean isChangeMarkCefrScore) {
        this.isChangeMarkCefrScore = isChangeMarkCefrScore;
    }
    */
    /**
     * 成績変更有無：マーク模試を返します。
     *
     * @return 成績変更有無：マーク模試
     */
    public boolean isChangeMarkJpnScore() {
        return isChangeMarkJpnScore;
    }

    /**
     * 成績変更有無：マーク模試(国語記述)をセットします。
     *
     * @param isChangeMarkJpnScore 成績変更有無：マーク模試(国語記述)
     */
    public void setChangeMarkJpnScore(boolean isChangeMarkJpnScore) {
        this.isChangeMarkJpnScore = isChangeMarkJpnScore;
    }

    /**
     * 成績変更有無：マーク模試を返します。
     *
     * @return 成績変更有無：マーク模試
     */
    public boolean isChangeMarkScore() {
        return isChangeMarkScore;
    }

    /**
     * 成績変更有無：マーク模試をセットします。
     *
     * @param isChangeMarkScore 成績変更有無：マーク模試
     */
    public void setChangeMarkScore(boolean isChangeMarkScore) {
        this.isChangeMarkScore = isChangeMarkScore;
    }

    /**
     * 成績変更有無：記述模試を返します。
     *
     * @return 成績変更有無：記述模試
     */
    public boolean isChangeWrittenScore() {
        return isChangeWrittenScore;
    }

    /**
     * 成績変更有無：記述模試をセットします。
     *
     * @param isChangeWrittenScore 成績変更有無：記述模試
     */
    public void setChangeWrittenScore(boolean isChangeWrittenScore) {
        this.isChangeWrittenScore = isChangeWrittenScore;
    }

    /**
     * 判定実行時の成績データを返します。
     *
     * @return 判定実行時の成績データ
     */
    public ScoreBean getJudgeScore() {
        return judgeScore;
    }

    /**
     * 判定実行時の成績データをセットします。
     *
     * @param judgeScore 判定実行時の成績データ
     */
    public void setJudgeScore(ScoreBean judgeScore) {
        this.judgeScore = judgeScore;
    }

}

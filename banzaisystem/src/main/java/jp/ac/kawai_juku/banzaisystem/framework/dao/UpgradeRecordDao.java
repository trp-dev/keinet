package jp.ac.kawai_juku.banzaisystem.framework.dao;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

/**
 *
 * 成績データバージョンアップDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UpgradeRecordDao extends BaseDao {

    /**
     * SQLファイルを実行します。
     *
     * @param path 実行するSQLファイル
     */
    public void execute(String path) {
        jdbcManager.updateBySqlFile(path).execute();
    }

    /**
     * 成績データ情報のバージョンを最新化します。
     *
     * @param version バージョン
     */
    public void updateRecordInfoVersion(Integer version) {
        if (jdbcManager.updateBySqlFile(getSqlPath(), version).execute() == 0) {
            throw new RuntimeException("成績データ情報の更新に失敗しました。" + version);
        }
    }

}

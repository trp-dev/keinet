package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.view;

import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.component.ExmntnDlcsTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * 取得資格ダイアログのViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class ExmntnDlcsView extends AbstractView {

    /** 資格名テーブル */
    @Location(x = 21, y = 48)
    @Size(width = 288, height = 107)
    public ExmntnDlcsTable nameTable;

    /** 説明ラベル */
    @Location(x = 20, y = 163)
    @Foreground(r = 161, g = 66, b = 18)
    public TextLabel noteLabel;

    /** 閉じるボタン */
    @Location(x = 124, y = 208)
    public ImageButton closeButton;

}

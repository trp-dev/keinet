package jp.ac.kawai_juku.banzaisystem.selstd.dunv.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.component.SelstdDunvTable;

/**
 *
 * 大学選択ダイアログViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
@Dialog
public class SelstdDunvView extends AbstractView {

    /** 大学名称フィールド */
    @Location(x = 112, y = 29)
    @Size(width = 135, height = 19)
    @MaxLength(12)
    public TextField univNameField;

    /** 検索開始(小)ボタン */
    @Location(x = 249, y = 25)
    public ImageButton startSearchSmallButton;

    /** 選択可能大学テーブル */
    @Location(x = 27, y = 91)
    @Size(width = 116, height = 223)
    public SelstdDunvTable selectableTable;

    /** 追加矢印ボタン */
    @Location(x = 156, y = 163)
    @Disabled
    public ImageButton addAllowButton;

    /** 削除矢印ボタン */
    @Location(x = 156, y = 200)
    @Disabled
    public ImageButton removeAllowButton;

    /** 選択済大学テーブル */
    @Location(x = 194, y = 91)
    @Size(width = 116, height = 223)
    public SelstdDunvTable selectedTable;

    /** OKボタン */
    @Location(x = 90, y = 341)
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 177, y = 341)
    public ImageButton cancelButton;

    @Override
    public void initialize() {
        super.initialize();
        selectableTable.setSelectableTable(true);
    }

}

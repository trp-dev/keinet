package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.data.Irregular1Second;
import jp.co.fj.kawaijuku.judgement.factory.helper.SecondRangeHelper;
import jp.co.fj.kawaijuku.judgement.factory.helper.SubjectHelper;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * 二次判定詳細情報生成ファクトリ<br>
 * ※既存PC版模試判定プロジェクトより移植
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class SecondDetailFactory extends DetailFactory<SecondDetail> {

    /** インデックス：科目配点 */
    private static final int IDX_SUBPNT = getSubBitIndex() + Subject.values().length;

    /** インデックス：同時選択不可区分 */
    private static final int IDX_CONCURRENT_NG = IDX_SUBPNT + Subject.values().length;

    /** インデックス：科目範囲 */
    private static final int IDX_SUBAREA = IDX_CONCURRENT_NG + Subject.values().length;

    /** インデックス：イレギュラ区分 */
    private static final int IDX_IRRDIV = IDX_SUBAREA + 2;

    /** 科目情報 */
    private enum Subject {

        /** 英語１ */
        ENGLISH1(SecondDetail.KEY_ENGLISH),

        /** 英語２ */
        ENGLISH2(SecondDetail.KEY_ENGLISH),

        /** 数学�T */
        MATH1(SecondDetail.KEY_MATH1),

        /** 数学�TＡ */
        MATH1A(SecondDetail.KEY_MATH1A),

        /** 数学�UＡ */
        MATH2A(SecondDetail.KEY_MATH2A),

        /** 数学�UＢ */
        MATH2B(SecondDetail.KEY_MATH2B),

        /** 数学�V */
        MATH3(SecondDetail.KEY_MATH3),

        /** 現代文 */
        COMTEMPORARY(SecondDetail.KEY_COMTEMPORARY),

        /** 現代文・古文 */
        ANCIENT(SecondDetail.KEY_ANCIENT),

        /** 現代文・古文・漢文 */
        CHINESE(SecondDetail.KEY_CHINESE),

        /** 物理基礎 */
        PHYSICS_BASIC(SecondDetail.KEY_PHYSICS_BASIC),

        /** 化学基礎 */
        CHEMISTRY_BASIC(SecondDetail.KEY_CHEMISTRY_BASIC),

        /** 生物基礎 */
        BIOLOGY_BASIC(SecondDetail.KEY_BIOLOGY_BASIC),

        /** 地学基礎 */
        EARTHSCIENCE_BASIC(SecondDetail.KEY_EARTHSCIENCE_BASIC),

        /** 物理 */
        PHYSICS(SecondDetail.KEY_PHYSICS),

        /** 化学 */
        CHEMISTRY(SecondDetail.KEY_CHEMISTRY),

        /** 生物 */
        BIOLOGY(SecondDetail.KEY_BIOLOGY),

        /** 地学 */
        EARTHSCIENCE(SecondDetail.KEY_EARTHSCIENCE),

        /** 物理不明 */
        PHYSICS_UNKNOWN(SecondDetail.KEY_PHYSICS_UNKNOWN),

        /** 化学不明 */
        CHEMISTRY_UNKNOWN(SecondDetail.KEY_CHEMISTRY_UNKNOWN),

        /** 生物不明 */
        BIOLOGY_UNKNOWN(SecondDetail.KEY_BIOLOGY_UNKNOWN),

        /** 地学不明 */
        EARTHSCIENCE_UNKNOWN(SecondDetail.KEY_EARTHSCIENCE_UNKNOWN),

        /** 日本史Ａ */
        JHISTORYA(SecondDetail.KEY_JHISTORYA),

        /** 世界史Ａ */
        WHISTORYA(SecondDetail.KEY_WHISTORYA),

        /** 地理Ａ */
        GEOGRAPHYA(SecondDetail.KEY_GEOGRAPHYA),

        /** 日本史Ｂ */
        JHISTORYB(SecondDetail.KEY_JHISTORYB),

        /** 世界史Ｂ */
        WHISTORYB(SecondDetail.KEY_WHISTORYB),

        /** 地理Ｂ */
        GEOGRAPHYB(SecondDetail.KEY_GEOGRAPHYB),

        /** 倫理 */
        ETHIC(SecondDetail.KEY_ETHIC),

        /** 政経 */
        POLITICS(SecondDetail.KEY_POLITICS),

        /** 現代社会 */
        SOCIALSTUDIES(SecondDetail.KEY_SOCIALSTUDIES),

        /** 小論文 */
        ESSAY(SecondDetail.KEY_ESSAY),

        /** 総合問題 */
        SYNTHESIS(SecondDetail.KEY_SYNTHESIS),

        /** 面接 */
        INTERVIEW(SecondDetail.KEY_INTERVIEW),

        /** 実技 */
        TECHNIQUE(SecondDetail.KEY_TECHNIQUE),

        /** 調査 */
        RESEARCH(SecondDetail.KEY_RESEARCH),

        /** 簿記 */
        BOOKKEEPING(SecondDetail.KEY_BOOKKEEPING),

        /** 情報 */
        INFORMATION(SecondDetail.KEY_INFORMATION),

        /** その他 */
        ETCETERA(SecondDetail.KEY_ETCETERA);

        /** 科目キー */
        private final SubjectKey key;

        /**
         * コンストラクタです。
         *
         * @param key 科目キー
         */
        Subject(SubjectKey key) {
            this.key = key;
        }

    }

    /**
     * コンストラクタです。
     *
     * @param values マスタ配列
     */
    SecondDetailFactory(String[] values) {
        super(values);
    }

    @Override
    protected SecondDetail createDetail() {
        return new SecondDetail();
    }

    @Override
    protected void prepareSubject(CommonDetailHolder detailHolder) {
        for (Subject sbj : Subject.values()) {

            String[] subBits = getString(getSubBitIndex() + sbj.ordinal()).split(GROUP_SEP);
            String[] concurrentNgs = getString(IDX_CONCURRENT_NG + sbj.ordinal()).split(GROUP_SEP);
            String[] subPnts = getString(IDX_SUBPNT + sbj.ordinal()).split(GROUP_SEP);
            for (int i = 0; i < subBits.length; i++) {
                String subBit = subBits[i];
                String concurrentNg = getArrayValue(concurrentNgs, i);
                int courseIndex = sbj.key.getCourseIndex();
                if (courseIndex == Detail.English.COURSE_INDEX) {
                    /* 範囲区分は英語のみ設定する */
                    String subArea = getString(IDX_SUBAREA + sbj.ordinal());
                    SecondRangeHelper.prepare(detailHolder, createSubject(subBit, Detail.ALLOT_NA, subArea, concurrentNg), sbj.key);
                } else if (courseIndex == Detail.Science.COURSE_INDEX || courseIndex == Detail.Social.COURSE_INDEX) {
                    /* 科目配点は理科・社会のみ設定する（イレギュラ�C） */
                    double subPnt = NumberUtils.toDouble(getArrayValue(subPnts, i), Detail.ALLOT_NA);
                    SubjectHelper.prepare(detailHolder, createSubject(subBit, subPnt, null, concurrentNg), sbj.key, SecondDetail.class);
                } else {
                    /* 範囲区分・科目配点設定なし */
                    SubjectHelper.prepare(detailHolder, createSubject(subBit, Detail.ALLOT_NA, null, concurrentNg), sbj.key, SecondDetail.class);
                }
            }

        }
    }

    @Override
    protected int getIrregularDivIndex() {
        return IDX_IRRDIV;
    }

    @Override
    protected Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub) {
        return new Irregular1Second(irregular, maxSelect, irrSub);
    }

}

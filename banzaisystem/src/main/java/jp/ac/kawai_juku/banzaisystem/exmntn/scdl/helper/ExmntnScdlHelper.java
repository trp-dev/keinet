package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.helper;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlA0601InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.service.ExmntnScdlService;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.view.ExmntnScdlView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ComparatorChain;
import jp.ac.kawai_juku.banzaisystem.framework.util.RatingComparator;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * 受験スケジュール画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnScdlHelper {

    /** Comparatorマップ */
    private final Map<Code, Comparator<ExmntnScdlBean>> comparatorMap = createComparatorMap();

    /** View */
    @Resource(name = "exmntnScdlView")
    private ExmntnScdlView view;

    /** サービス */
    @Resource(name = "exmntnScdlService")
    private ExmntnScdlService service;

    /** 志望大学ヘルパー */
    @Resource
    private ExmntnMainUnivHelper exmntnMainUnivHelper;

    /** 志望大学詳細画面のヘルパー */
    @Resource
    private ExmntnUnivHelper exmntnUnivHelper;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * Comparatorマップを生成します。
     *
     * @return Comparatorマップ
     */
    private Map<Code, Comparator<ExmntnScdlBean>> createComparatorMap() {

        Map<Code, Comparator<ExmntnScdlBean>> map = new HashMap<>();

        /* 志望順位 */
        Comparator<ExmntnScdlBean> rankComparator = new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return o1.getCandidateRank().compareTo(o2.getCandidateRank());
            }
        };
        map.put(Code.SCHEDULE_ORDER_RANK, rankComparator);

        /* センタ評価 */
        map.put(Code.SCHEDULE_ORDER_CENTER, new ComparatorChain<ExmntnScdlBean>().add(new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return RatingComparator.getInstance().compare(o1.getCenterRating(), o2.getCenterRating());
            }
        }).add(rankComparator));

        /* 二次評価 */
        map.put(Code.SCHEDULE_ORDER_SECOND, new ComparatorChain<ExmntnScdlBean>().add(new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return RatingComparator.getInstance().compare(o1.getSecondRating(), o2.getSecondRating());
            }
        }).add(rankComparator));

        /* 総合評価 */
        map.put(Code.SCHEDULE_ORDER_TOTAL, new ComparatorChain<ExmntnScdlBean>().add(new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return RatingComparator.getInstance().compare(o1.getTotalRating(), o2.getTotalRating());
            }
        }).add(rankComparator));

        /* 入試日 */
        map.put(Code.SCHEDULE_ORDER_DATE, new ComparatorChain<ExmntnScdlBean>().add(new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return ObjectUtils.compare(o1.getMinInpleDate(), o2.getMinInpleDate(), true);
            }
        }).add(rankComparator));

        return Collections.unmodifiableMap(map);
    }

    /**
     * 受験スケジュールテーブルを表示します。
     */
    public void displayTable() {

        List<ExmntnScdlBean> list;
        if (systemDto.isTeacher()) {
            /* 先生用 */
            list = service.findTeachderExamPlanUnivList(getKojinId());
        } else {
            /* 生徒用 */
            list = service.findStudentExamPlanUnivList();
        }

        view.scheduleTable.setExamPlanUnivList(list);
    }

    /**
     * 画面選択中の個人IDを返します。
     *
     * @return 個人ID
     */
    private Integer getKojinId() {
        return systemDto.getTargetIndividualId();
    }

    /**
     * 志望順位を変更します。
     *
     * @param isUp 上に移動するならtrue
     */
    public void changeCandidateRank(boolean isUp) {
        if (systemDto.isTeacher()) {
            /* 先生用 */
            service.changeTeacherRank(getKojinId(), view.scheduleTable.changeRank(isUp));
        } else {
            /* 生徒用 */
            service.changeStudentRank(view.scheduleTable.changeRank(isUp));
        }
    }

    /**
     * 削除ボタン押下アクションです。
     */
    public void deleteButtonAction() {

        List<ExmntnScdlBean> list = view.scheduleTable.getSelectedList();

        if (systemDto.isTeacher()) {
            /* 先生用 */
            service.deleteTeacherExamPlanUniv(getKojinId(), list);
        } else {
            /* 生徒用 */
            service.deleteStudentExamPlanUniv(list);
        }

        /* テーブルから消す */
        view.scheduleTable.delete(list);

        /* 志望大学リストの「予」アイコンを削除する */
        exmntnMainUnivHelper.deleteSchedule(list);
    }

    /**
     * 詳細判定ボタンアクションです。
     */
    public void detailJudgeButtonAction() {
        exmntnUnivHelper.dispSearchDetail(view.scheduleTable.getDetailJudgeBean());
    }

    /**
     * 並べ替えコンボボックス変更アクションです。
     */
    public void sortComboBoxAction() {
        view.scheduleTable.sortAndDisplay();
    }

    /**
     * 地方受験チェックボックス変更アクションです。
     *
      * @param bean {@link ExmntnScdlBean}
     */
    public void provincesCheckBoxAction(ExmntnScdlBean bean) {
        if (systemDto.isTeacher()) {
            /* 先生用 */
            service.changeTeacherProvincesFlg(bean);
        } else {
            /* 生徒用 */
            service.changeStudentProvincesFlg(bean);
        }
    }

    /**
     * Excel帳票メーカー(受験スケジュール)の入力Beanを生成します。
     *
     * @return {@link ExmntnScdlA0601InBean}
     */
    public ExmntnScdlA0601InBean createA0601InBean() {

        /* 受験スケジュールリストを生成する */
        List<ExmntnScdlBean> sceduleList = createSceduleList();

        /* 上位26位までとする */
        if (sceduleList.size() > 26) {
            sceduleList = sceduleList.subList(0, 26);
        }

        /* ソートする */
        if (view.isInitialized()) {
            sortExamPlanUnivList(sceduleList, view.sortComboBox.getSelectedValue());
        } else {
            sortExamPlanUnivList(sceduleList, Code.SCHEDULE_ORDER_RANK);
        }

        return new ExmntnScdlA0601InBean(exmntnUnivHelper.createStudentBean(), dto.getJudgeScore(), sceduleList);
    }

    /**
      * 受験スケジュールリストを生成します。
      *
      * @return 受験スケジュールリスト
      */
    public List<ExmntnScdlBean> createSceduleList() {

        List<ExmntnScdlBean> sceduleList;

        if (systemDto.isTeacher()) {
            /* 先生用 */
            sceduleList = service.findTeachderExamPlanUnivList(getKojinId());
        } else {
            /* 生徒用 */
            sceduleList = service.findStudentExamPlanUnivList();
        }

        return sceduleList;
    }

    /**
     * 受験予定大学リストをソートします。
     *
     * @param examPlanUnivList 受験予定大学リスト
     * @param sortKey ソートキー
     */
    public void sortExamPlanUnivList(List<ExmntnScdlBean> examPlanUnivList, Code sortKey) {
        Collections.sort(examPlanUnivList, comparatorMap.get(sortKey));
    }

}

package jp.ac.kawai_juku.banzaisystem.result.dunv.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 大学選択ダイアログのDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultDunvDao extends BaseDao {

    /**
     * 大学リストを取得します。
     *
     * @param univName 大学名
     * @return 大学リスト
     */
    public List<LabelValueBean> selectUnivList(String univName) {
        return jdbcManager.selectBySqlFile(LabelValueBean.class, getSqlPath(), univName).getResultList();
    }

    /**
     * 学部リストを取得します。
     *
     * @param univCd 大学コード
     * @return 学部リスト
     */
    public List<LabelValueBean> selectFacultyList(String univCd) {
        return jdbcManager.selectBySqlFile(LabelValueBean.class, getSqlPath(), univCd).getResultList();
    }

    /**
     * 学科リストを取得します。
     *
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @return 学科リスト
     */
    public List<LabelValueBean> selectDeptList(String univCd, String facultyCd) {
        BeanMap param = new BeanMap();
        param.put("univCd", univCd);
        param.put("facultyCd", facultyCd);
        return jdbcManager.selectBySqlFile(LabelValueBean.class, getSqlPath(), param).getResultList();
    }

}

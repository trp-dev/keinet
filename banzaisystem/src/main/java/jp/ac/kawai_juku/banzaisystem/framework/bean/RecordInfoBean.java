package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 成績データ情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class RecordInfoBean {

    /** バージョン */
    private int version;

    /** 年度 */
    private String year;

    /** 模試区分 */
    private String examDiv;

    /**
     * バージョンを返します。
     *
     * @return バージョン
     */
    public int getVersion() {
        return version;
    }

    /**
     * バージョンをセットします。
     *
     * @param version バージョン
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * 年度を返します。
     *
     * @return 年度
     */
    public String getYear() {
        return year;
    }

    /**
     * 年度をセットします。
     *
     * @param year 年度
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

}

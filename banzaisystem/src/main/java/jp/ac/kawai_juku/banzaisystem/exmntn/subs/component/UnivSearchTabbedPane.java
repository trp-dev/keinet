package jp.ac.kawai_juku.banzaisystem.exmntn.subs.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.SubViewManager;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索タブのタブペインコンポーネントです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class UnivSearchTabbedPane extends JPanel implements BZComponent, SubViewManager {

    /** タブチェックボックスの背景色 */
    private static final Color CB_COLOR = new Color(153, 214, 214);

    /** タブリスト */
    private final List<Tab> tabList = CollectionsUtil.newArrayList();

    /** タブ表示領域 */
    private final JPanel tabArea = new JPanel(null);

    /** タブパネル表示領域 */
    private final JScrollPane panelArea =
            new BZScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    /**
     * コンストラクタです。
     */
    public UnivSearchTabbedPane() {
        super(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        TabConfig config = BZFieldUtil.getAnnotation(field, TabConfig.class);
        for (Class<? extends SubController> clazz : config.value()) {
            Tab tab = new Tab(view, clazz);
            tabList.add(tab);
        }

        tabArea.setOpaque(false);
        panelArea.setBorder(null);
        panelArea.setBackground(Color.WHITE);
        setOpaque(false);
        add(tabArea);
        add(panelArea);

        /* 先頭のタブを基準に大きさを調整する */
        Tab tab = tabList.get(0);
        Dimension size = tab.view.getSize();
        tabArea.setBounds(0, 0, size.width, tab.label.getHeight());
        panelArea.setBounds(0, tabArea.getHeight(), 671, 361);
        setSize(panelArea.getWidth(), tabArea.getHeight() + panelArea.getHeight());
    }

    /**
     * 全てのタブが非アクティブの場合は、先頭のタブをアクティブにします。<br>
     * アクティブなタブが存在する場合は、アクセスログ出力のためにそのタブを再度アクティブにします。
     */
    public void activate() {
        if (tabArea.getComponentCount() == 0) {
            activate(tabList.get(0).controller.getClass(), null);
        } else {
            for (Tab tab : tabList) {
                if (isActive(tab)) {
                    activate(tab.controller.getClass(), null);
                    break;
                }
            }
        }
    }

    @Override
    public void activate(Class<? extends SubController> clazz, String methodName) {

        tabArea.removeAll();

        int left = 6;
        for (Tab tab : tabList) {

            tab.checkbox.setLocation(left, 0);
            tabArea.add(tab.checkbox);
            left += tab.checkbox.getWidth();

            if (clazz.isAssignableFrom(tab.controller.getClass())) {
                /* アクティブにするタブ */
                tab.label.setLocation(left, 0);
                tabArea.add(tab.label);
                panelArea.setViewportView(tab.view.getView());
                tab.controller.activatedAction();
            } else {
                /* 非アクティブにするタブ */
                tab.button.setLocation(left, 0);
                tabArea.add(tab.button);
            }

            left += tab.label.getWidth() + 6;
        }

        revalidate();
        repaint();
    }

    /**
     * 指定したタブがアクティブであるかを返します。
     *
     * @param tab タブ
     * @return アクティブならtrue
     */
    private boolean isActive(Tab tab) {
        for (Component c : tabArea.getComponents()) {
            if (c == tab.label) {
                return true;
            }
        }
        return false;
    }

    /**
     * 指定したタブのチェックボックスを返します。
     *
     * @param clazz {@link SubController}
     * @return チェックボックス
     */
    public JCheckBox findCheckBox(Class<? extends SubController> clazz) {

        for (Tab tab : tabList) {
            if (clazz.isAssignableFrom(tab.controller.getClass())) {
                return tab.checkbox;
            }
        }

        throw new RuntimeException(String.format("指定したサブコントローラ[%s]は登録されていません。", clazz));
    }

    /**
     * タブをクリアします。
     */
    public void clear() {
        tabArea.removeAll();
        for (Tab tab : tabList) {
            tab.checkbox.setSelected(true);
        }
    }

    /**
     * タブ情報を保持するクラスです。
     */
    private final class Tab {

        /** コントローラ */
        private final SubController controller;

        /** ビュー */
        private final SubView view;

        /** タブラベル */
        private final JLabel label = new JLabel();

        /** タブボタン */
        private final JButton button = new JButton();

        /** チェックボックス */
        private final JCheckBox checkbox = new JCheckBox();

        /**
         * コンストラクタです。
         *
         * @param superView 上位View
         * @param clazz 画面のコントローラクラス
         */
        private Tab(AbstractView superView, Class<? extends SubController> clazz) {

            controller = SingletonS2Container.getComponent(clazz);
            view = controller.getSubView();

            BufferedImage imgOn = ImageUtil.readImage(superView.getClass(), "tab/" + controller.getId() + "_on.png");
            BufferedImage imgOff = ImageUtil.readImage(superView.getClass(), "tab/" + controller.getId() + "_off.png");

            label.setIcon(new ImageIcon(imgOn));
            label.setSize(imgOn.getWidth(), imgOn.getHeight());

            button.setBorder(null);
            button.setBounds(0, 0, imgOff.getWidth(), imgOff.getHeight());
            button.setIcon(new ImageIcon(imgOff));
            button.setFocusable(false);
            button.setFocusPainted(false);
            button.addMouseListener(ImageButton.CURSOR_MOUSE_LISTENER);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    activate(controller.getClass(), null);
                }
            });

            checkbox.setSize(22, 30);
            checkbox.setBackground(CB_COLOR);
            checkbox.setMargin(new Insets(0, 7, 0, 0));
        }
    }

}

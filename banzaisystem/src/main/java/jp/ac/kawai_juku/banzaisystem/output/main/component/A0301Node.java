package jp.ac.kawai_juku.banzaisystem.output.main.component;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0301Maker;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.output.main.service.OutputMainService;

/**
 *
 * 簡易個人表(複数生徒一括)のノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class A0301Node extends OutputExcelNode {

    /**
     * コンストラクタです。
     */
    A0301Node() {
        super("簡易個人表(複数生徒一括)");
    }

    @Override
    void output(OutputMainHelper helper) {
        OutputMainService service = getComponent(OutputMainService.class);
        new ExcelLauncher(GamenId.D).addCreator(ExmntnUnivA0301Maker.class,
                service.createA0301InBean(helper.findTargetExam(), helper.findPrintTargetList())).launch();
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.dao;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CANDIDATE_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXAM_UNIV_MAX;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivDaoBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.CandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 志望大学リストに関するDAOです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainCandidateUnivDao extends BaseDao {

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 志望大学リストを取得します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @return 志望大学リスト
     */
    public List<ExmntnMainCandidateUnivBean> selectCandidateUnivList(ExamBean exam, Integer id) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        param.put("system", systemDto);
        return jdbcManager.selectBySqlFile(ExmntnMainCandidateUnivBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 大学名称リストを取得します。
     *
     * @param univList 大学キーリスト
     * @return 大学名称リスト
     */
    public List<ExmntnMainCandidateUnivBean> selectUnivNameList(List<UnivKeyBean> univList) {
        BeanMap param = new BeanMap();
        param.put("where", DaoUtil.createUnivWhere(univList));
        List<ExmntnMainCandidateUnivBean> list =
                jdbcManager.selectBySqlFile(ExmntnMainCandidateUnivBean.class, getSqlPath(), param).getResultList();
        /* 引数の順番通りに戻す */
        ExmntnMainCandidateUnivBean[] array = new ExmntnMainCandidateUnivBean[list.size()];
        for (ExmntnMainCandidateUnivBean bean : list) {
            array[univList.indexOf(bean)] = bean;
        }
        return Arrays.asList(array);
    }

    /**
     * 志望大学を削除します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @param list 削除する大学リスト
     */
    public void deleteCandidateUniv(Integer id, ExamBean exam, List<? extends UnivKeyBean> list) {
        for (UnivKeyBean bean : list) {
            BeanMap param = new BeanMap();
            param.put("id", id);
            param.put("exam", exam);
            param.put("univKey", bean);
            jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
        }
    }

    /**
     * 模試記入志望大学削除された、非表示になっているシステム登録志望大学を表示します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @param list 削除する大学リスト
     */
    public void updateCandidateUnivDispOn(Integer id, ExamBean exam, List<? extends UnivKeyBean> list) {
        for (UnivKeyBean bean : list) {
            BeanMap param = new BeanMap();
            param.put("id", id);
            param.put("exam", exam);
            param.put("univKey", bean);
            param.put("max", Integer.valueOf(EXAM_UNIV_MAX + CANDIDATE_UNIV_MAX));
            jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
        }
    }

    /**
     * 志望順位を変更する志望大学リスト（順位を入れ替える2レコード）を取得します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @param keyBean 大学キー
     * @param isUp 上に移動するならtrue
     * @return 志望大学リスト
     */
    public Pair<ExmntnMainCandidateUnivDaoBean, ExmntnMainCandidateUnivDaoBean> selectChangeRankCandidateUnivList(
            ExamBean exam, Integer id, UnivKeyBean keyBean, boolean isUp) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        param.put("univKey", keyBean);
        param.put("isUp", isUp);
        List<ExmntnMainCandidateUnivDaoBean> list =
                jdbcManager.selectBySqlFile(ExmntnMainCandidateUnivDaoBean.class, getSqlPath(), param).getResultList();
        if (list.size() == 2) {
            return Pair.of(list.get(0), list.get(1));
        } else {
            return null;
        }
    }

    /**
     * 志望大学を更新します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @param bean {@link ExmntnMainCandidateUnivDaoBean}
     */
    public void updateCandidateUniv(ExamBean exam, Integer id, ExmntnMainCandidateUnivDaoBean bean) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        param.put("bean", bean);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 年度内の全ての模試の志望大学リストを取得します。
     *
     * @param id 個人ID
     * @return 志望大学リスト
     */
    public List<CandidateUnivBean> selectAllCandidateUnivList(Integer id) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", systemDto.getUnivYear());
        return jdbcManager.selectBySqlFile(CandidateUnivBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 年度内の全ての模試の志望大学を削除します。
     *
     * @param id 個人ID
     */
    public void deleteAllCandidateUniv(Integer id) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", systemDto.getUnivYear());
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

}

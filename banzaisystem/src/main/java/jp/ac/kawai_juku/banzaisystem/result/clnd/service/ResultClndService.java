package jp.ac.kawai_juku.banzaisystem.result.clnd.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.result.clnd.dao.ResultClndDao;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;

/**
 *
 * 入試結果入力-大学指定(カレンダー)画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultClndService {

    /** DAO */
    @Resource
    private ResultClndDao resultClndDao;

    /**
     * 合格発表日リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @return 合格発表日リスト
     */
    public List<ResultUnivBean> findSucAnnDateList(Integer grade, String cls) {
        return resultClndDao.selectSucAnnDateList(grade, cls);
    }

}

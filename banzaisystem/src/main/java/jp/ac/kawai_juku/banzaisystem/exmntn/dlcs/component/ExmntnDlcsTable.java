package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 取得資格ダイアログのテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)OOMURA.Masafumi
 *
 */
@SuppressWarnings("serial")
public class ExmntnDlcsTable extends TableScrollPane {

    /** ポイントアイコン */
    private final ImageIcon pointIcon;

    /**
     * コンストラクタです。
     */
    public ExmntnDlcsTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(null);
        pointIcon = new ImageIcon(ImageUtil.readImage(getClass(), "table/point.png"));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        /* 空のテーブルパネルを格納 */
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        JTable table = new BZTable(new ReadOnlyTableModel(1)) {
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                ((JLabel) c).setIcon(pointIcon);
                return c;
            }
        };

        table.setFocusable(false);
        table.setRowHeight(18);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setRowSelectionAllowed(false);

        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(270);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 資格名リストをセットします。
     *
     * @param nameList 資格名リスト
     */
    public void setNameList(List<String> nameList) {
        Vector<Vector<String>> dataVector = CollectionsUtil.newVector();
        for (String name : nameList) {
            Vector<String> row = CollectionsUtil.newVector();
            row.add(name);
            dataVector.add(row);
        }
        ((ReadOnlyTableModel) getTable().getModel()).setDataVector(dataVector);
    }

}

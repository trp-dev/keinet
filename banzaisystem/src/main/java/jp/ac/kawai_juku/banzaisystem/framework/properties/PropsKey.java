package jp.ac.kawai_juku.banzaisystem.framework.properties;

/**
 *
 * プロパティキーのインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
interface PropsKey {

    /**
     * 初期値を返します。
     *
     * @return 初期値
     */
    String getDefaultValue();

}

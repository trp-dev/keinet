package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の個人成績一覧データBeanです。
 * (CEFR情報)
 *
 * @author QQ)Nakao.Takaaki
 *
 */
public class SelstdDprtExcelCefrMakerRecordBean {

    /** 個人ID */
    private Integer individualId;

    /** 認定試験コード1 */
    private String engptExamCd1;

    /** 認定試験正式名1 */
    private String engptName1;

    /** 認定試験レベルコード1 */
    private String engptLevelCd1;

    /** 認定試験レベル正式名1 */
    private String engptLevelCdName1;

    /** スコア1 */
    private double score1;

    /** スコア補正フラグ1 */
    private String scoreCorrectFlg1;

    /** CEFRレベルコード1 */
    private String cefrLevelCd1;

    /** CEFRレベル短縮名1 */
    private String cefrLevelName1;

    /** CEFRレベルコード補正フラグ1 */
    private String cefrLevelCorrectFlg1;

    /** 合否1 */
    private String result1;

    /** 合否補正フラグ1 */
    private String resultCorrectFlg1;

    /** 認定試験コード2 */
    private String engptExamCd2;

    /** 認定試験正式名2 */
    private String engptName2;

    /** 認定試験レベルコード2 */
    private String engptLevelCd2;

    /** 認定試験レベル正式名2 */
    private String engptLevelCdName2;

    /** スコア2 */
    private double score2;

    /** スコア補正フラグ2 */
    private String scoreCorrectFlg2;

    /** CEFRレベルコード2 */
    private String cefrLevelCd2;

    /** CEFRレベル短縮名2 */
    private String cefrLevelName2;

    /** CEFRレベルコード補正フラグ2 */
    private String cefrLevelCorrectFlg2;

    /** 合否2 */
    private String result2;

    /** 合否補正フラグ2 */
    private String resultCorrectFlg2;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** 氏名（カナ） */
    private String nameKana;

    /** 文理コード */
    private String bunriCode;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 認定試験コード1を返します。
     *
     * @return 認定試験コード1
     */
    public String getEngptExamCd1() {
        return engptExamCd1;
    }

    /**
     * 認定試験コード1をセットします。
     *
     * @param engptExamCd1 認定試験コード1
     */
    public void setEngptExamCd1(String engptExamCd1) {
        this.engptExamCd1 = engptExamCd1;
    }

    /**
     * 認定試験正式名1を返します。
     *
     * @return 認定試験正式名1
     */
    public String getEngptName1() {
        return engptName1;
    }

    /**
     * 認定試験正式名1をセットします。
     *
     * @param engptName1 認定試験正式名1
     */
    public void setEngptName1(String engptName1) {
        this.engptName1 = engptName1;
    }

    /**
     * 認定試験レベルコード1を返します。
     *
     * @return 認定試験レベルコード1
     */
    public String getEngptLevelCd1() {
        return engptLevelCd1;
    }

    /**
     * 認定試験レベルコード1をセットします。
     *
     * @param engptLevelCd1 認定試験レベルコード1
     */
    public void setEngptLevelCd1(String engptLevelCd1) {
        this.engptLevelCd1 = engptLevelCd1;
    }

    /**
     * 認定試験レベル正式名1を返します。
     *
     * @return 認定試験レベル正式名1
     */
    public String getEngptLevelCdName1() {
        return engptLevelCdName1;
    }

    /**
     * 認定試験レベル正式名1をセットします。
     *
     * @param engptLevelCdName1 認定試験レベル正式名1
     */
    public void setEngptLevelCdName1(String engptLevelCdName1) {
        this.engptLevelCdName1 = engptLevelCdName1;
    }

    /**
     * スコア1を返します。
     *
     * @return スコア1
     */
    public double getScore1() {
        return score1;
    }

    /**
     * スコア1をセットします。
     *
     * @param score1 スコア1
     */
    public void setScore1(double score1) {
        this.score1 = score1;
    }

    /**
     * スコア補正フラグ1を返します。
     *
     * @return スコア補正フラグ1
     */
    public String getScoreCorrectFlg1() {
        return scoreCorrectFlg1;
    }

    /**
     * スコア補正フラグ1をセットします。
     *
     * @param scoreCorrectFlg1 スコア補正フラグ1
     */
    public void setScoreCorrectFlg1(String scoreCorrectFlg1) {
        this.scoreCorrectFlg1 = scoreCorrectFlg1;
    }

    /**
     * CEFRレベルコード1を返します。
     *
     * @return CEFRレベルコード1
     */
    public String getCefrLevelCd1() {
        return cefrLevelCd1;
    }

    /**
     * CEFRレベルコード1をセットします。
     *
     * @param cefrLevelCd1 CEFRレベルコード1
     */
    public void setCefrLevelCd1(String cefrLevelCd1) {
        this.cefrLevelCd1 = cefrLevelCd1;
    }

    /**
     * CEFRレベル短縮名1を返します。
     *
     * @return CEFRレベル短縮名1
     */
    public String getCefrLevelName1() {
        return cefrLevelName1;
    }

    /**
     * CEFRレベル短縮名1をセットします。
     *
     * @param cefrLevelName1 CEFRレベル短縮名1
     */
    public void setCefrLevelName1(String cefrLevelName1) {
        this.cefrLevelName1 = cefrLevelName1;
    }

    /**
     * CEFRレベルコード補正フラグ1を返します。
     *
     * @return CEFRレベルコード補正フラグ1
     */
    public String getCefrLevelCorrectFlg1() {
        return cefrLevelCorrectFlg1;
    }

    /**
     * CEFRレベルコード補正フラグ1をセットします。
     *
     * @param cefrLevelCorrectFlg1 CEFRレベルコード補正フラグ1
     */
    public void setCefrLevelCorrectFlg1(String cefrLevelCorrectFlg1) {
        this.cefrLevelCorrectFlg1 = cefrLevelCorrectFlg1;
    }

    /**
     * 合否1を返します。
     *
     * @return 合否1
     */
    public String getResult1() {
        return result1;
    }

    /**
     * 合否1をセットします。
     *
     * @param result1 合否1
     */
    public void setResult1(String result1) {
        this.result1 = result1;
    }

    /**
     * 合否補正フラグ1を返します。
     *
     * @return 合否補正フラグ1
     */
    public String getResultCorrectFlg1() {
        return resultCorrectFlg1;
    }

    /**
     * 合否補正フラグ1をセットします。
     *
     * @param resultCorrectFlg1 合否補正フラグ1
     */
    public void setResultCorrectFlg1(String resultCorrectFlg1) {
        this.resultCorrectFlg1 = resultCorrectFlg1;
    }

    /**
     * 認定試験コード2を返します。
     *
     * @return 認定試験コード2
     */
    public String getEngptExamCd2() {
        return engptExamCd2;
    }

    /**
     * 認定試験コード2をセットします。
     *
     * @param engptExamCd2 認定試験コード2
     */
    public void setEngptExamCd2(String engptExamCd2) {
        this.engptExamCd2 = engptExamCd2;
    }

    /**
     * 認定試験正式名2を返します。
     *
     * @return 認定試験正式名2
     */
    public String getEngptName2() {
        return engptName2;
    }

    /**
     * 認定試験正式名2をセットします。
     *
     * @param engptName2 認定試験正式名2
     */
    public void setEngptName2(String engptName2) {
        this.engptName2 = engptName2;
    }

    /**
     * 認定試験レベルコード2を返します。
     *
     * @return 認定試験レベルコード2
     */
    public String getEngptLevelCd2() {
        return engptLevelCd2;
    }

    /**
     * 認定試験レベルコード2をセットします。
     *
     * @param engptLevelCd2 認定試験レベルコード2
     */
    public void setEngptLevelCd2(String engptLevelCd2) {
        this.engptLevelCd2 = engptLevelCd2;
    }

    /**
     * 認定試験レベル正式名2を返します。
     *
     * @return 認定試験レベル正式名2
     */
    public String getEngptLevelCdName2() {
        return engptLevelCdName2;
    }

    /**
     * 認定試験レベル正式名2をセットします。
     *
     * @param engptLevelCdName2 認定試験レベル正式名2
     */
    public void setEngptLevelCdName2(String engptLevelCdName2) {
        this.engptLevelCdName2 = engptLevelCdName2;
    }

    /**
     * スコア2を返します。
     *
     * @return スコア2
     */
    public double getScore2() {
        return score2;
    }

    /**
     * スコア2をセットします。
     *
     * @param score2 スコア2
     */
    public void setScore2(double score2) {
        this.score2 = score2;
    }

    /**
     * スコア補正フラグ2を返します。
     *
     * @return スコア補正フラグ2
     */
    public String getScoreCorrectFlg2() {
        return scoreCorrectFlg2;
    }

    /**
     * スコア補正フラグ2をセットします。
     *
     * @param scoreCorrectFlg2 スコア補正フラグ2
     */
    public void setScoreCorrectFlg2(String scoreCorrectFlg2) {
        this.scoreCorrectFlg2 = scoreCorrectFlg2;
    }

    /**
     * CEFRレベルコード2を返します。
     *
     * @return CEFRレベルコード2
     */
    public String getCefrLevelCd2() {
        return cefrLevelCd2;
    }

    /**
     * CEFRレベルコード2をセットします。
     *
     * @param cefrLevelCd2 CEFRレベルコード2
     */
    public void setCefrLevelCd2(String cefrLevelCd2) {
        this.cefrLevelCd2 = cefrLevelCd2;
    }

    /**
     * CEFRレベル短縮名2を返します。
     *
     * @return CEFRレベル短縮名2
     */
    public String getCefrLevelName2() {
        return cefrLevelName2;
    }

    /**
     * CEFRレベル短縮名2をセットします。
     *
     * @param cefrLevelName2 CEFRレベル短縮名2
     */
    public void setCefrLevelName2(String cefrLevelName2) {
        this.cefrLevelName2 = cefrLevelName2;
    }

    /**
     * CEFRレベルコード補正フラグ2を返します。
     *
     * @return CEFRレベルコード補正フラグ2
     */
    public String getCefrLevelCorrectFlg2() {
        return cefrLevelCorrectFlg2;
    }

    /**
     * CEFRレベルコード補正フラグ2をセットします。
     *
     * @param cefrLevelCorrectFlg2 CEFRレベルコード補正フラグ2
     */
    public void setCefrLevelCorrectFlg2(String cefrLevelCorrectFlg2) {
        this.cefrLevelCorrectFlg2 = cefrLevelCorrectFlg2;
    }

    /**
     * 合否2を返します。
     *
     * @return 合否2
     */
    public String getResult2() {
        return result2;
    }

    /**
     * 合否2をセットします。
     *
     * @param result2 合否2
     */
    public void setResult2(String result2) {
        this.result2 = result2;
    }

    /**
     * 合否補正フラグ2を返します。
     *
     * @return 合否補正フラグ2
     */
    public String getResultCorrectFlg2() {
        return resultCorrectFlg2;
    }

    /**
     * 合否補正フラグ2をセットします。
     *
     * @param resultCorrectFlg2 合否補正フラグ2
     */
    public void setResultCorrectFlg2(String resultCorrectFlg2) {
        this.resultCorrectFlg2 = resultCorrectFlg2;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * 氏名（カナ）を返します。
     *
     * @return 氏名（カナ）
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 氏名（カナ）をセットします。
     *
     * @param nameKana 氏名（カナ）
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 文理コードを返します。
     *
     * @return 文理コード
     */
    public String getBunriCode() {
        return bunriCode;
    }

    /**
     * 文理コードをセットします。
     *
     * @param bunriCode 文理コード
     */
    public void setBunriCode(String bunriCode) {
        this.bunriCode = bunriCode;
    }

}

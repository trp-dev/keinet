package jp.ac.kawai_juku.banzaisystem.framework.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;

import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;

/**
 *
 * ファイルをExcel起動するアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class InvokeExcelResult extends TaskResult {

    /**
     * ファイルをExcel起動します。
     *
     * @param file ファイル
     */
    protected final void invokeExcel(File file) {

        String[] cmdarray = new String[7];
        cmdarray[0] = "cmd";
        cmdarray[1] = "/c";
        cmdarray[2] = "start";
        cmdarray[3] = "";
        cmdarray[4] = "excel.exe";
        cmdarray[5] = file.getAbsolutePath();
        cmdarray[6] = "/e";

        try {
            Runtime.getRuntime().exec(cmdarray);
        } catch (Exception e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E008"), e);
        }
    }

}

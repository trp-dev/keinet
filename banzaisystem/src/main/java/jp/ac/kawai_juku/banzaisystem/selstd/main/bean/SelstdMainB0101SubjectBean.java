package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;

/**
 *
 * 個人成績CSVの科目成績を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdMainB0101SubjectBean extends SubjectRecord {

    /** 科目名 */
    private String subName;

    /** 配点 */
    private String subAllotPnt;

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public String getSubAllotPntStr() {
        return subAllotPnt;
    }

    /**
     * 配点をセットします。
     *
     * @param subAllotPnt 配点
     */
    public void setSubAllotPnt(String subAllotPnt) {
        this.subAllotPnt = subAllotPnt;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

}

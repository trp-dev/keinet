package jp.ac.kawai_juku.banzaisystem.framework.dao;

import java.util.List;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.framework.bean.CvsScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.Jh01Kamoku1Bean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.Jh01Kamoku2Bean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.Jh01Kamoku3Bean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.Jh01KamokuCommonBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.RankBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.RecordInfoBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScheduleBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.beans.CefrBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrConvScoreBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuAppInfo;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.JpnKijutuScoreInfo;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoBasicBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoCenterBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoCommonBean;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoContainer;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoFactory;
import jp.co.fj.kawaijuku.judgement.factory.UnivInfoSecondBean;

import org.seasar.extension.jdbc.IterationCallback;

/**
 *
 * バンザイシステムの初期化処理に関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class InitializeDao extends BaseDao {

    /**
     * 大学マスタインポート済の模試リストを取得します。
     *
     * @param systemDto システム情報DTO
     * @return 大学マスタインポート済の模試リスト
     */
    public List<ExamBean> selectExamList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 判定用文字列リストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectJStringList(IterationCallback<String, Object> callback) {
        jdbcManager.selectBySqlFile(String.class, getSqlPath()).iterate(callback);
    }

    /**
     * ランクリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectRankBeanList(IterationCallback<RankBean, Object> callback) {
        jdbcManager.selectBySqlFile(RankBean.class, getSqlPath()).iterate(callback);
    }

    /**
     * センター得点換算表のリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectCvsScoreList(IterationCallback<CvsScoreBean, Object> callback) {
        jdbcManager.selectBySqlFile(CvsScoreBean.class, getSqlPath()).iterate(callback);
    }

    /**
     * 科目別成績（全国）のリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectSubrecordAllList(IterationCallback<SubRecordAllBean, Object> callback) {
        jdbcManager.selectBySqlFile(SubRecordAllBean.class, getSqlPath()).iterate(callback);
    }

    /**
     * 模試科目マスタのリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectExamSubjectList(IterationCallback<ExamSubjectBean, Object> callback) {
        jdbcManager.selectBySqlFile(ExamSubjectBean.class, getSqlPath()).iterate(callback);
    }

    /**
     * 日程マスタのリストを取得します。
     *
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectScheduleList(IterationCallback<ScheduleBean, Object> callback) {
        jdbcManager.selectBySqlFile(ScheduleBean.class, getSqlPath()).iterate(callback);
    }

    /**
     * 成績データ情報を取得します。
     *
     * @return {@link RecordInfoBean}
     */
    public RecordInfoBean selectRecordInfoBean() {
        return jdbcManager.selectBySqlFile(RecordInfoBean.class, getSqlPath()).getSingleResult();
    }

    /**
     * CEFR情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean CEFR情報
     */
    public List<CefrBean> selectCefrList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(CefrBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 認定試験情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 認定試験情報
     */
    public List<EngPtBean> selectEngPtList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(EngPtBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 認定試験詳細情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 認定試験詳細情報
     */
    public List<EngPtDetailBean> selectEngPtDetailList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(EngPtDetailBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * CEFR換算用スコア情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean CEFR換算用スコア情報
     */
    public List<CefrConvScoreBean> selectCefrConvScoreList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(CefrConvScoreBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 国語記述式総合評価情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 国語記述式総合評価情報
     */
    public List<JpnDescTotalEvaluationBean> selectJpnWritingTotalEvaluationList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(JpnDescTotalEvaluationBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 英語資格・検定試験_出願要件情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 認定試験情報
     */
    public List<EngSikakuAppInfo> selectEngPtappreqList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(EngSikakuAppInfo.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 英語資格・検定試験_得点換算情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 認定試験情報
     */
    public List<EngSikakuScoreInfo> selectEngPtscoreconvList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(EngSikakuScoreInfo.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 認定試験情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return CefrBean 認定試験情報
     */
    public List<JpnKijutuScoreInfo> selectKokugoDescscoreconvList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(JpnKijutuScoreInfo.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 情報誌用科目情報を取得します。
     *
     * @param systemDto システム情報DTO
     * @return {@link UnivInfoContainer}
     */
    public Map loadUnivInfo(SystemDto systemDto) {
        UnivInfoContainer container = new UnivInfoContainer();
        selectUnivInfoBasic(container, systemDto);
        selectUnivInfoCenter(container);
        selectUnivInfoSecond(container);
        return new UnivInfoFactory().create(container);
    }

    /**
     * 情報誌用科目1をロードします。
     *
     * @param container 情報誌用科目データを保持するデータクラス
     * @param systemDto システム情報DTO
     */
    private void selectUnivInfoBasic(UnivInfoContainer container, SystemDto systemDto) {
        List<Jh01Kamoku1Bean> items = jdbcManager.selectBySqlFile(Jh01Kamoku1Bean.class, getSqlPath(), systemDto).getResultList();

        for (Jh01Kamoku1Bean item : items) {
            container.getUnivInfoBasicList().add(new UnivInfoBasicBean(item.getCombiKey(), item.getUniv10Cd(), item.getBranchName()));
        }
    }

    /**
     * 情報誌用科目2をロードします。
     *
     * @param container 情報誌用科目データを保持するデータクラス
     */
    private void selectUnivInfoCenter(UnivInfoContainer container) {
        List<Jh01Kamoku2Bean> items = jdbcManager.selectBySqlFile(Jh01Kamoku2Bean.class, getSqlPath()).getResultList();

        for (Jh01Kamoku2Bean item : items) {
            UnivInfoCenterBean bean = new UnivInfoCenterBean();

            /* 共通情報を設定する */
            setCommonValue(item, bean, "認");

            /* センタ固有情報を設定する */
            bean.setItem7(item.getItem7());
            bean.setRika1ansFlg(item.getRika1AnsFlg());
            bean.setSociety1ansFlg(item.getSociety1AnsFlg());

            /* 国語記述配点を設定する */
            bean.addJpnDesc(item.getJpnDescRange(), item.getJpnDescTrustPt(), item.getJpnDescPt(), item.getJpnDescAddPt(), item.getJpnDescAddPtType(), item.getJpnDescSel());

            container.getUnivInfoCenterMap().put(item.getCombiKey(), bean);
        }
    }

    /**
     * 情報誌用科目3をロードします。
     *
     * @param container 情報誌用科目データを保持するデータクラス
     */
    private void selectUnivInfoSecond(UnivInfoContainer container) {
        List<Jh01Kamoku3Bean> items = jdbcManager.selectBySqlFile(Jh01Kamoku3Bean.class, getSqlPath()).getResultList();
        for (Jh01Kamoku3Bean item : items) {
            UnivInfoSecondBean bean = new UnivInfoSecondBean();

            /* 共通情報を設定する */
            setCommonValue(item, bean, "英資");

            /* 二次固有情報を設定する */
            bean.addEssay(item.getEssay(), item.getEssayTrustPt(), item.getEssayPt(), item.getEssayHissu());

            /* 総合 */
            bean.addTotal(item.getTotal(), item.getTotalTrustPt(), item.getTotalPt(), item.getTotalHissu());

            /* 面接 */
            bean.addInterview(item.getInterview(), item.getInterviewTrustPt(), item.getInterviewPt(), item.getInterviewHissu());

            /* 実技 */
            bean.addPlactical(item.getPlactical(), item.getPlacticalTrustPt(), item.getPlacticalPt(), item.getPlacticalHissu());

            /* 簿記 */
            bean.addBoki(item.getBoki(), item.getBokiTrustPt(), item.getBokiPt(), item.getBokiHissu());

            /* 情報 */
            bean.addInfo(item.getInfo(), item.getInfoTrustPt(), item.getInfoPt(), item.getInfoHissu());

            /* 調査書 */
            bean.addResearch(item.getResearch(), item.getResearchTrustPt(), item.getResearchPt(), item.getResearchHissu());

            /* 他 */
            bean.addHoka(item.getHoka(), item.getHokaTrustPt(), item.getHokaPt(), item.getHokaHissu());

            /* その他配点信頼 */
            bean.setHoka2TrustPt(item.getHoka2TrustPt());

            /* その他配点 */
            bean.setHoka2Pt(item.getHoka2Pt());

            container.getUnivInfoSecondMap().put(item.getCombiKey(), bean);
        }
    }

    /**
     * 情報誌用科目2と3の共通情報を設定します。
     *
     * @param item 結果セット
     * @param bean {@link UnivInfoCommonBean}
     * @param label ラベル
     */
    private void setCommonValue(Jh01KamokuCommonBean item, UnivInfoCommonBean bean, String label) {

        /* 配点信頼 */
        bean.setScTrust(item.getScTrust());

        /* 総点 */
        bean.setTotalSc(item.getTotalSc());

        /* 外国語 */
        bean.addGai(item.getGaiTrustPt(), item.getGaiShoPt(), item.getGaiShoSel(), item.getGaiDaiPt(), item.getGaiDaiSel(), COURSE_ALLOT.ENG);

        /* 数学 */
        bean.addMath(item.getMathTrustPt(), item.getMathShoPt(), item.getMathShoSel(), item.getMathDaiPt(), item.getMathDaiSel(), COURSE_ALLOT.MAT);

        /* 国語 */
        bean.addKokugo(item.getKokugoTrustPt(), item.getKokugoShoPt(), item.getKokugoShoSel(), item.getKokugoDaiPt(), item.getKokugoDaiSel(), COURSE_ALLOT.JAP);

        /* 理科 */
        bean.addRika(item.getRikaTrustPt(), item.getRikaShoPt(), item.getRikaShoSel(), item.getRikaDaiPt(), item.getRikaDaiSel(), COURSE_ALLOT.SCI);

        /* 地歴公民 */
        bean.addSociety(item.getSocietyTrustPt(), item.getSocietyShoPt(), item.getSocietyShoSel(), item.getSocietyDaiPt(), item.getSocietyDaiSel(), COURSE_ALLOT.SOC);

        /* 科目名 */
        bean.setSubName(item.getSubName());

        /* 英語認定試験 */
        bean.addEngCert(label, item.getEngCertApp(), item.getEngCertTrustPt(), item.getEngCertPt(), item.getEngCertAddPt(), item.getEngCertAddPtType(), item.getEngCertSel());
    }
}

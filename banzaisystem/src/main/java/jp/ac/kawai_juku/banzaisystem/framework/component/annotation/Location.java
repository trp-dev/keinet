package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * コンポーネントの座標を設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Location {

    /**
     * コンポーネントのX座標を設定します。
     *
     * @return X座標
     */
    int x();

    /**
     * コンポーネントのY座標を設定します。
     *
     * @return Y座標
     */
    int y();

}

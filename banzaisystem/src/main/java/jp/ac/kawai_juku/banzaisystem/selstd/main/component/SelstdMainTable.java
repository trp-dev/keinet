package jp.ac.kawai_juku.banzaisystem.selstd.main.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.SortableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 対象生徒選択画面の抽象テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class SelstdMainTable extends SortableScrollPane {

    /**
     * テーブル種別
     *
     */
    public enum TableType {
        /** 個人名 */
        NAME,
        /** 個人名＋成績 */
        SCORE,
        /** 個人名＋志望大学 */
        CANDIDATE
    }

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** 評価列 */
    private static final List<Integer> RATE_COLUMNS = Arrays.asList(11, 12, 13, 14);

    private final TableType type;

    /** データリスト */
    private List<Map<String, CellData>> dataList;

    /**
     * コンストラクタです。
     *
     * @param sortKey ソートキーの初期値
     * @param type テーブルの種別
     */
    public SelstdMainTable(String sortKey, TableType type) {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER, sortKey);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
        this.type = type;
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setColumnHeaderView(createColumnHeaderView());
        JPanel fixedHeader = createFixedColumnHeaderView();
        if (fixedHeader != null) {
            setCorner(JScrollPane.UPPER_LEFT_CORNER, fixedHeader);
        }
        setViewportView(createTableView(getHeader()));
        if (fixedHeader != null) {
            /* 固定領域にテーブルがある場合（個人名＋成績の場合） */
            setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
            setRowHeaderView(createTableView(fixedHeader));
            /*
             * 固定領域をマウスドラッグでスクロールさせると、
             * 可変領域がスクロールせずに取り残されて行位置がずれるため、
             * 固定領域の位置変更を可変領域側に通知するようにする。
             * ※原因はSwingAPIのバグだと思われる。
             */
            final JViewport rightView = getViewport();
            getRowHeader().addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    JViewport leftView = (JViewport) e.getSource();
                    rightView.setViewPosition(new Point(rightView.getViewPosition().x, leftView.getViewPosition().y));
                }
            });
            /* 2つのテーブルのリスト選択モデルを同一にする */
            getTable().setSelectionModel(getFixedTable().getSelectionModel());
        }
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    protected abstract JPanel createColumnHeaderView();

    /**
     * 固定領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    protected abstract JPanel createFixedColumnHeaderView();

    /**
     * テーブルを生成します。
     *
     * @param header ヘッダ
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final JPanel header) {

        final JTable table = new BZTable(new ReadOnlyTableModel(header.getComponentCount() + 1)) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (TableType.CANDIDATE.equals(type)) {
                    if (RATE_COLUMNS.contains(column)) {
                        Font font = c.getFont();
                        c.setFont(new Font(BZConstants.MONOSPACED_FONT, font.getStyle(), font.getSize()));
                    }
                }
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (Component c : header.getComponents()) {
                    x += c.getWidth();
                    if (((Header) c).isDrawSeparator()) {
                        g.drawLine(x - 1, 0, x - 1, getHeight());
                    }
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);

        /* テーブルの選択状態によって「生徒更新」「生徒削除」ボタンの状態を変えるリスナを追加する */
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                SelstdMainView view = SingletonS2Container.getComponent(SelstdMainView.class);
                if (table.getSelectedRowCount() > 0) {
                    /* 「個人名＋志望大学」のみ同一生徒が複数行に渡る場合があるので考慮する */
                    List<Integer> list = new ArrayList<>(2);
                    TableModel model = table.getModel();
                    for (int rowIndex : table.getSelectedRows()) {
                        Object id = ((CellData) model.getValueAt(rowIndex, 0)).getDispValue();
                        if (list.indexOf(id) < 0) {
                            list.add((Integer) id);
                            if (list.size() > 1) {
                                break;
                            }
                        }
                    }
                    view.editStudentButton.setEnabled(list.size() == 1);
                    view.deleteStudentButton.setEnabled(list.size() > 0);
                } else {
                    view.editStudentButton.setEnabled(false);
                    view.deleteStudentButton.setEnabled(false);
                }
            }
        });

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        Component[] components = header.getComponents();
        for (int i = 0; i < components.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(components[i].getWidth());
            columnModel.getColumn(i).setCellRenderer(((Header) components[i]).getAlignment().getRenderer());
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 選択中の個人IDリストを返します。
     *
     * @return 選択中の個人IDリスト
     */
    public List<Integer> getSelectedIndividualIdList() {

        List<Integer> list = CollectionsUtil.newArrayList();

        JTable table = getTable();
        TableModel model = table.getModel();
        int[] rowIndexArray;
        if (table.getSelectedRowCount() > 0) {
            /* 選択されている場合 */
            rowIndexArray = table.getSelectedRows();
        } else {
            /* 未選択なら全生徒 */
            rowIndexArray = new int[model.getRowCount()];
            for (int rowIndex = 0; rowIndex < rowIndexArray.length; rowIndex++) {
                rowIndexArray[rowIndex] = rowIndex;
            }
        }

        for (int rowIndex : rowIndexArray) {
            Object id = ((CellData) model.getValueAt(rowIndex, 0)).getDispValue();
            if (list.indexOf(id) < 0) {
                /* 重複しないようにする */
                list.add((Integer) id);
            }
        }

        return list;
    }

    /**
     * データリストをクリアします。
     */
    public final void clearDataList() {
        setDataList(Collections.<Map<String, CellData>> emptyList());
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public final void setDataList(List<Map<String, CellData>> dataList) {
        /* ソート用にデータリストを保持しておく */
        this.dataList = dataList;
        /* ソートする */
        sort(getSortKey(), isAscending());
    }

    @Override
    protected void sort(final String sortKey, final boolean isAscending) {

        /* ソートする */
        if (!StringUtils.isEmpty(sortKey)) {
            final CellData nullCellData = new CellData(null);
            Collections.sort(dataList, new Comparator<Map<String, CellData>>() {
                @Override
                public int compare(Map<String, CellData> o1, Map<String, CellData> o2) {

                    /* 第1ソートキーでソートする */
                    int result = ObjectUtils.defaultIfNull(o1.get(sortKey), nullCellData).compareTo(ObjectUtils.defaultIfNull(o2.get(sortKey), nullCellData));
                    if (result != 0) {
                        return isAscending ? result : -result;
                    }

                    /* 学年でソートする */
                    if (!sortKey.equals("grade")) {
                        result = ObjectUtils.compare(o1.get("grade"), o2.get("grade"));
                        if (result != 0) {
                            /* 学年は降順とする */
                            return -result;
                        }
                    }

                    /* クラスでソートする */
                    if (!sortKey.equals("class")) {
                        result = ObjectUtils.compare(o1.get("class"), o2.get("class"));
                        if (result != 0) {
                            return result;
                        }
                    }

                    /* クラス番号でソートする */
                    if (!sortKey.equals("number")) {
                        result = ObjectUtils.compare(o1.get("number"), o2.get("number"));
                        if (result != 0) {
                            return result;
                        }
                    }

                    /* 個人IDでソートする */
                    result = ObjectUtils.compare(o1.get("id"), o2.get("id"));
                    if (result != 0) {
                        return result;
                    }

                    /* 志望順位でソートする */
                    return ObjectUtils.compare(o1.get("wishRank"), o2.get("wishRank"));
                }
            });
        }

        /* ソートした結果をテーブルにセットする */
        setDataVector(getTable(), getHeader());
        JPanel fixedHeader = getFixedHeader();
        if (fixedHeader != null) {
            setDataVector(getFixedTable(), fixedHeader);
        }
    }

    /**
     * テーブルにデータをセットします。
     *
     * @param table テーブル
     * @param header ヘッダ
     */
    protected void setDataVector(JTable table, JPanel header) {
        ((BZTableModel) table.getModel()).setDataVector(toDataVector(header, dataList));
    }

    /**
     * データリストをテーブルデータに変換します。
     *
     * @param header ヘッダ
     * @param dataList データリスト
     * @return テーブルデータ
     */
    protected Vector<Vector<CellData>> toDataVector(JPanel header, List<Map<String, CellData>> dataList) {

        Vector<Vector<CellData>> data = CollectionsUtil.newVector(dataList.size());

        for (Map<String, CellData> map : dataList) {
            Vector<CellData> rowData = CollectionsUtil.newVector();
            /* 先頭列は個人IDをセットしておく */
            rowData.add(map.get("id"));
            /* 2列目以降をセットする */
            for (Component c : header.getComponents()) {
                rowData.add(ObjectUtils.defaultIfNull(map.get(c.getName()), new CellData(null)));
            }
            data.add(rowData);
        }

        return data;
    }

}

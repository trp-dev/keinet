package jp.ac.kawai_juku.banzaisystem.output.main.dao;

import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 帳票出力・CSVファイル画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainDao extends BaseDao {

    /**
     * 生徒情報を取得します。
     *
     * @param id 個人ID
     * @param year 年度
     * @return {@link ExmntnUnivStudentBean}
     */
    public ExmntnUnivStudentBean selectStudentBean(Integer id, String year) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("year", year);
        return jdbcManager.selectBySqlFile(ExmntnUnivStudentBean.class, getSqlPath(), param).getSingleResult();
    }

}

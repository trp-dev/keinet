package jp.ac.kawai_juku.banzaisystem.framework.util;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * バイト値に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ByteUtil {

    /**
     * コンストラクタです。
     */
    private ByteUtil() {
    }

    /**
     * バイト配列から16進数文字列に変換します。
     *
     * @param byteArray バイト配列
     * @return 16進数文字列
     */
    public static String toHexString(byte[] byteArray) {

        StringBuilder sb = new StringBuilder(byteArray.length * 2);

        for (byte b : byteArray) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }

        return sb.toString();
    }

    /**
     * 16進数文字列からバイト配列に変換します。
     *
     * @param str 16進数文字列
     * @return バイト配列
     */
    public static byte[] toByteArray(String str) {

        if (StringUtils.isEmpty(str) || str.length() % 2 != 0) {
            return null;
        }

        byte[] array = new byte[str.length() / 2];

        for (int i = 0; i < array.length; i++) {
            int begin = i * 2;
            try {
                array[i] = (byte) Integer.parseInt(str.substring(begin, begin + 2), 16);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        return array;
    }

}

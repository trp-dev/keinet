package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 模試科目マスタ情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamSubjectBean {

    /** 模試コード */
    private String examCd;

    /** 科目コード */
    private String subCd;

    /** 科目名 */
    private String subName;

    /** 表示順 */
    private String dispSequence;

    /** 基礎科目フラグ */
    private String basicFlg;

    /** 配点 */
    private Integer suballotPnt;

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

    /**
     * 模試コードを返します。
     *
     * @return examCd 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * 表示順を返します。
     *
     * @return 表示順
     */
    public String getDispSequence() {
        return dispSequence;
    }

    /**
     * 表示順をセットします。
     *
     * @param dispSequence 表示順
     */
    public void setDispSequence(String dispSequence) {
        this.dispSequence = dispSequence;
    }

    /**
     * 基礎科目フラグを返します。
     *
     * @return 基礎科目フラグ
     */
    public String getBasicFlg() {
        return basicFlg;
    }

    /**
     * 基礎科目フラグをセットします。
     *
     * @param basicFlg 基礎科目フラグ
     */
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public Integer getSuballotPnt() {
        return suballotPnt;
    }

    /**
     * 配点をセットします。
     *
     * @param suballotPnt 配点
     */
    public void setSuballotPnt(Integer suballotPnt) {
        this.suballotPnt = suballotPnt;
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * 選択大学リストです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class OutputMainUnivList extends BZScrollPane implements BZComponent {

    /** リストモデル */
    private final DefaultListModel<String> model = new DefaultListModel<>();

    /** 大学リスト */
    private final List<SelstdDunvBean> univList = new ArrayList<>();

    /** 大学リスト設定時の対象模試 */
    private ComboBoxItem<ExamBean> exam;

    /** 大学リスト設定時の学年 */
    private ComboBoxItem<Integer> grade;

    /**
     * コンストラクタです。
     */
    public OutputMainUnivList() {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        final JList<String> list = new JList<>(model);
        list.setFocusable(false);
        list.setSelectionModel(new DefaultListSelectionModel() {
            @Override
            public boolean isSelectedIndex(int index) {
                return false;
            }
        });
        addPropertyChangeListener("enabled", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                list.setEnabled(isEnabled());
            }
        });
        setViewportView(list);
    }

    /**
     * 大学リストを返します。
     *
     * @return 大学リスト
     */
    public List<SelstdDunvBean> getUnivList() {
        return univList;
    }

    /**
     * 大学リストをセットします。
     *
     * @param list 大学リスト
     * @param exam 対象模試
     * @param grade 学年
     */
    public void setUnivList(List<SelstdDunvBean> list, ComboBoxItem<ExamBean> exam, ComboBoxItem<Integer> grade) {
        univList.clear();
        univList.addAll(list);
        model.clear();
        for (SelstdDunvBean bean : list) {
            model.addElement(bean.getUnivName());
        }
        /* 大学リスト設定時の情報を保持しておく */
        this.exam = exam;
        this.grade = grade;
    }

    /**
     * 大学リストをクリアします。
     */
    public void clearUnivList() {
        setUnivList(Collections.<SelstdDunvBean> emptyList(), null, null);
    }

    /**
     * 大学リスト設定時の情報から変化がある場合に限り、大学リストをクリアします。<br>
     * ※対象模試・学年以外については、それぞれ変更直後に大学リストをクリアするため、チェック対象としない。
     *
     * @param exam 対象模試
     * @param grade 学年
     */
    public void clearUnivList(ComboBoxItem<ExamBean> exam, ComboBoxItem<Integer> grade) {
        if (!ObjectUtils.equals(exam, this.exam) || !ObjectUtils.equals(grade, this.grade)) {
            clearUnivList();
        }
    }

}

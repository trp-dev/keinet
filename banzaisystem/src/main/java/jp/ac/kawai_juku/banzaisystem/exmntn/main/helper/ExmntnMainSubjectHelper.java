package jp.ac.kawai_juku.banzaisystem.exmntn.main.helper;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_1;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_NA;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_OTHER;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.RANGE_SECOND_LOWER;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.RANGE_SECOND_UPPER;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LISTENING;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_SOCIAL;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WRITING;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_NA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ENGLISH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP2;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP3;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH3;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_WHISTORYB;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績画面の科目ヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainSubjectHelper {

    /** 空の科目コンボボックスアイテム */
    private static final ComboBoxItem<BZSubjectRecord> BLANK_ITEM = new ComboBoxItem<BZSubjectRecord>(" ", null);

    /**
     * マーク模試：英語科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkEng() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_WRITING, ANSWER_NO_NA);
    }

    /**
     * マーク模試：リスニング科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkLis() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_LISTENING, ANSWER_NO_NA);
    }

    /**
     * マーク模試：CEFR1科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkCefr1() {
        return new BZSubjectRecord(SUBCODE_NA, ANSWER_NO_NA);
    }

    /**
     * マーク模試：CEFR2科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkCefr2() {
        return new BZSubjectRecord(SUBCODE_NA, ANSWER_NO_NA);
    }

    /**
     * マーク：数学（上段）項目を返します。
     *
     * @param exam 対象模試
     * @return マーク：数学（上段）項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkMath1List(ExamBean exam) {
        return createList(exam, ANSWER_NO_NA, SUBCODE_CENTER_DEFAULT_MATH1, SUBCODE_CENTER_DEFAULT_MATH1A);
    }

    /**
     * マーク模試：数学（下段）項目を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：数学（下段）
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkMath2List(ExamBean exam) {
        return createList(exam, ANSWER_NO_NA, SUBCODE_CENTER_DEFAULT_MATH2, SUBCODE_CENTER_DEFAULT_MATH2B);
    }

    /**
     * マーク模試：現代文科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkJapCo() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_LITERATURE, ANSWER_NO_NA);
    }

    /**
     * マーク模試：古文科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkJapCl() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_OLD, ANSWER_NO_NA);
    }

    /**
     * マーク模試：漢文科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkJapCh() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_CHINESE, ANSWER_NO_NA);
    }

    /**
     * マーク模試：国語記述科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getMarkJapWr() {
        return new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_JKIJUTSU, ANSWER_NO_NA);
    }

    /**
     * マーク模試：理科専門項目（上段）を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：理科専門項目（上段）
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkScience1List(ExamBean exam) {
        return getMarkScienceList(exam, true);
    }

    /**
     * マーク模試：理科専門項目（下段）を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：理科専門項目（下段）
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkScience2List(ExamBean exam) {
        return getMarkScienceList(exam, false);
    }

    /**
     * マーク模試：理科専門項目を返します。
     *
     * @param exam 対象模試
     * @param isFirst 第1解答フラグ
     * @return マーク模試：理科専門項目
     */
    private List<ComboBoxItem<BZSubjectRecord>> getMarkScienceList(ExamBean exam, boolean isFirst) {
        return createList(exam, createMarkScope(exam, isFirst), SUBCODE_CENTER_DEFAULT_PHYSICS, SUBCODE_CENTER_DEFAULT_CHEMISTRY, SUBCODE_CENTER_DEFAULT_BIOLOGY, SUBCODE_CENTER_DEFAULT_EARTH);
    }

    /**
     * マーク模試：理科基礎項目を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：理科基礎項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkScienceBasicList(ExamBean exam) {
        return createList(exam, createMarkScope(exam, false), SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC, SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC, SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC, SUBCODE_CENTER_DEFAULT_EARTH_BASIC);
    }

    /**
     * マーク模試：地歴公民項目（上段）を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：地歴公民項目（上段）
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkSociety1List(ExamBean exam) {
        return getMarkSocietyList(exam, true);
    }

    /**
     * マーク模試：地歴公民項目（下段）を返します。
     *
     * @param exam 対象模試
     * @return マーク模試：地歴公民項目（下段）
     */
    public List<ComboBoxItem<BZSubjectRecord>> getMarkSociety2List(ExamBean exam) {
        return getMarkSocietyList(exam, false);
    }

    /**
     * マーク模試：地歴公民項目を返します。
     *
     * @param exam 対象模試
     * @param isFirst 第1解答フラグ
     * @return マーク模試：地歴公民項目
     */
    private List<ComboBoxItem<BZSubjectRecord>> getMarkSocietyList(ExamBean exam, boolean isFirst) {
        if (ExamUtil.isPre(exam) || ExamUtil.isCenter(exam)) {
            return createList(exam, createMarkScope(exam, isFirst), SUBCODE_CENTER_DEFAULT_JHISTORYB, SUBCODE_CENTER_DEFAULT_WHISTORYB, SUBCODE_CENTER_DEFAULT_GEOGRAPHYB, SUBCODE_CENTER_DEFAULT_JHISTORYA, SUBCODE_CENTER_DEFAULT_WHISTORYA,
                    SUBCODE_CENTER_DEFAULT_GEOGRAPHYA, SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS, SUBCODE_CENTER_DEFAULT_SOCIAL, SUBCODE_CENTER_DEFAULT_ETHICS, SUBCODE_CENTER_DEFAULT_POLITICS);
        } else {
            return createList(exam, createMarkScope(exam, isFirst), SUBCODE_CENTER_DEFAULT_JHISTORYB, SUBCODE_CENTER_DEFAULT_WHISTORYB, SUBCODE_CENTER_DEFAULT_GEOGRAPHYB, SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS,
                    SUBCODE_CENTER_DEFAULT_SOCIAL, SUBCODE_CENTER_DEFAULT_ETHICS, SUBCODE_CENTER_DEFAULT_POLITICS);
        }
    }

    /**
     * 記述模試：英語科目情報を返します。
     *
     * @param exam 対象模試
     * @param listeningFlg リスニング選択フラグ
     * @return 科目情報
     */
    public BZSubjectRecord getWrtEng(ExamBean exam, boolean listeningFlg) {
        String scope;
        if (ExamUtil.is2ndWrtn(exam)) {
            scope = ANSWER_NO_NA;
        } else if (listeningFlg) {
            scope = RANGE_SECOND_UPPER;
        } else {
            scope = RANGE_SECOND_LOWER;
        }
        return new BZSubjectRecord(SUBCODE_SECOND_DEFAULT_ENGLISH, scope);
    }

    /**
     * 記述模試：数学項目を返します。
     *
     * @param exam 対象模試
     * @return 記述模試：数学項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getWrittenMathList(ExamBean exam) {
        if (ExamUtil.is2ndWrtn(exam)) {
            /* 高２記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_MATH1, SUBCODE_SECOND_DEFAULT_MATH1A, SUBCODE_SECOND_DEFAULT_MATH2A, SUBCODE_SECOND_DEFAULT_MATH2B);
        } else {
            /* ＃１〜３記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_MATH1, SUBCODE_SECOND_DEFAULT_MATH1A, SUBCODE_SECOND_DEFAULT_MATH2A, SUBCODE_SECOND_DEFAULT_MATH2B, SUBCODE_SECOND_DEFAULT_MATH3);
        }
    }

    /**
     * 記述模試：国語項目を返します。
     *
     * @param exam 対象模試
     * @return 記述模試：国語項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getWrittenJapaneseList(ExamBean exam) {
        if (ExamUtil.is2ndWrtn(exam)) {
            /* 高２記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_JAP3, SUBCODE_SECOND_DEFAULT_JAP2, SUBCODE_SECOND_DEFAULT_JAP1);
        } else {
            /* ＃１〜３記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_JAP3, SUBCODE_SECOND_DEFAULT_JAP2, SUBCODE_SECOND_DEFAULT_JAP1);
        }
    }

    /**
     * 記述模試：理科専門項目を返します。
     *
     * @param exam 対象模試
     * @return 記述模試：理科項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getWrittenScienceList(ExamBean exam) {
        if (ExamUtil.is2ndWrtn(exam)) {
            /* 高２記述 */
            return createList(exam, RANGE_SECOND_UPPER, SUBCODE_SECOND_DEFAULT_PHYSICS, SUBCODE_SECOND_DEFAULT_CHEMISTRY, SUBCODE_SECOND_DEFAULT_BIOLOGY);
        } else {
            /* ＃１〜３記述 */
            return createList(exam, RANGE_SECOND_UPPER, SUBCODE_SECOND_DEFAULT_PHYSICS, SUBCODE_SECOND_DEFAULT_CHEMISTRY, SUBCODE_SECOND_DEFAULT_BIOLOGY, SUBCODE_SECOND_DEFAULT_EARTH);
        }
    }

    /**
     * 記述模試：理科基礎項目を返します。
     *
     * @param exam 対象模試
     * @return 記述模試：理科項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getWrittenScienceBasicList(ExamBean exam) {
        if (ExamUtil.is2ndWrtn(exam)) {
            /* 高２記述 */
            return createList(exam, RANGE_SECOND_LOWER, SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC, SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC, SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC, SUBCODE_SECOND_DEFAULT_EARTH_BASIC);
        } else {
            /* ＃１〜３記述 */
            return createList(exam, RANGE_SECOND_LOWER, SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC, SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC, SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC, SUBCODE_SECOND_DEFAULT_EARTH_BASIC);
        }
    }

    /**
     * 記述模試：地歴公民項目を返します。
     *
     * @param exam 対象模試
     * @return 記述模試：地歴公民項目
     */
    public List<ComboBoxItem<BZSubjectRecord>> getWrittenSocietyList(ExamBean exam) {
        if (ExamUtil.is2ndWrtn(exam)) {
            /* 高２記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_JHISTORYB, SUBCODE_SECOND_DEFAULT_WHISTORYB, SUBCODE_SECOND_DEFAULT_GEOGRAPHYB, SUBCODE_SECOND_DEFAULT_ETHICS, SUBCODE_SECOND_DEFAULT_POLITICS);

        } else {
            /* ＃１〜３記述 */
            return createList(exam, ANSWER_NO_NA, SUBCODE_SECOND_DEFAULT_JHISTORYB, SUBCODE_SECOND_DEFAULT_WHISTORYB, SUBCODE_SECOND_DEFAULT_GEOGRAPHYB, SUBCODE_SECOND_DEFAULT_ETHICS, SUBCODE_SECOND_DEFAULT_POLITICS);
        }
    }

    /**
     * コンボボックス項目リストを生成します。
     *
     * @param exam 対象模試
     * @param scope 範囲区分
     * @param subCdArray 科目コードリスト
     * @return コンボボックス項目リスト
     */
    private List<ComboBoxItem<BZSubjectRecord>> createList(ExamBean exam, String scope, String... subCdArray) {
        List<ComboBoxItem<BZSubjectRecord>> list = CollectionsUtil.newArrayList(subCdArray.length + 1);
        list.add(BLANK_ITEM);
        for (String subCd : subCdArray) {
            list.add(createItem(exam, subCd, scope));
        }
        return list;
    }

    /**
     * コンボボックス項目を生成します。
     *
     * @param exam 対象模試
     * @param subCd 科目コード
     * @param scope 範囲区分
     * @return コンボボックス項目
     */
    private ComboBoxItem<BZSubjectRecord> createItem(ExamBean exam, String subCd, String scope) {
        return new ComboBoxItem<BZSubjectRecord>(findSubName(exam, subCd), new BZSubjectRecord(subCd, scope));
    }

    /**
     * 科目名を返します。
     *
     * @param exam 対象模試
     * @param subCd 科目コード
     * @return 科目名
     */
    private String findSubName(ExamBean exam, String subCd) {
        Map<String, ExamSubjectBean> map = getComponent(SystemDto.class).getExamSubjectMap().get(exam.getExamCd());
        if (map == null) {
            throw new RuntimeException(String.format("模試科目マスタが存在しません。[模試コード=%s]".concat(exam.getExamCd())));
        } else {
            ExamSubjectBean bean = map.get(subCd);
            return bean == null ? null : bean.getSubName();
        }
    }

    /**
     * マーク模試の理科・社会の範囲区分を生成します。
     *
     * @param exam 対象模試
     * @param isFirst 第1解答フラグ
     * @return 範囲区分
     */
    private String createMarkScope(ExamBean exam, boolean isFirst) {
        if (ExamUtil.isAns1st(exam)) {
            return isFirst ? ANSWER_NO_1 : ANSWER_NO_OTHER;
        } else {
            return ANSWER_NO_NA;
        }
    }

}

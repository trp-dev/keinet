package jp.ac.kawai_juku.banzaisystem.result.indv.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellEditor;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CenterAlignedCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CodeComboCellEditor;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CodeComboCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * 入試結果入力-個人指定画面のテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ResultIndvTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(250, 180, 121);

    /** カラム幅定義 */
    private static final int[] COL_WIDTH = { 39, 67, 136, 91, 91, 110, 121, 45 };

    /** カラム×コード種別対応テーブル */
    private static final CodeType[] CODETYPE_TABLE = { null, null, null, null, null, CodeType.RESULT_ENTEXAMMODE,
            CodeType.RESULT_RESULTDIV, CodeType.RESULT_ADMISSIONDIV };

    /**
     * コンストラクタです。
     */
    public ResultIndvTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        JTable table = new BZTable(new ReadOnlyTableModel(COL_WIDTH.length + 1)) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                if (c instanceof JLabel) {
                    JLabel label = (JLabel) c;
                    label.setBorder(column >= 2 ? BorderFactory.createEmptyBorder(0, 5, 0, 0) : null);
                }
                return c;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                /* コンボボックス列のみ編集可能とする */
                return CODETYPE_TABLE[column] != null;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int width : COL_WIDTH) {
                    x += width;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(27);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            TableColumn column = columnModel.getColumn(i);
            column.setPreferredWidth(COL_WIDTH[i]);
            CodeType codeType = CODETYPE_TABLE[i];
            if (i <= 1) {
                column.setCellRenderer(new CenterAlignedCellRenderer());
            } else if (i == 7) {
                column.setCellRenderer(new RadioButtonCellRenderer());
                column.setCellEditor(new RadioButtonCellEditor(SEPARATOR_COLOR));
            } else if (codeType != null) {
                column.setCellRenderer(new CodeComboCellRenderer(codeType, COL_WIDTH[i], table.getRowHeight()));
                column.setCellEditor(new CodeComboCellEditor(codeType, COL_WIDTH[i], table.getRowHeight(),
                        SEPARATOR_COLOR));
            }
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public void setDataList(Vector<Vector<Object>> dataList) {
        ((BZTableModel) getTable().getModel()).setDataVector(dataList);
    }

}

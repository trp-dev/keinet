package jp.ac.kawai_juku.banzaisystem.systop.lgin.view;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.PasswordField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * マスタ・プログラムチェック画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopLginView extends AbstractView {

    /** 終了ボタン */
    @Location(x = 464, y = 2)
    public ImageButton exitButton;

    /** 戻るボタン */
    @Location(x = 461, y = 110)
    public ImageButton backButton;

    /** 「パスワードを入力してください。」ラベル */
    @Location(x = 127, y = 207)
    public TextLabel passwordLabel;

    /** パスワード */
    @Location(x = 193, y = 225)
    @Size(width = 120, height = 20)
    @MaxLength(8)
    public PasswordField passwordField;

    /** OKボタン */
    @Location(x = 318, y = 225)
    @Disabled
    public ImageButton okButton;

    /** クリアボタン */
    @Location(x = 360, y = 225)
    @Disabled
    public ImageButton clearButton;

    /** 説明テキスト */
    @Location(x = 140, y = 280)
    @Foreground(r = 255, g = 0, b = 0)
    public TextLabel noteLabel;

    @Override
    @InitMethod
    public void initialize() {

        super.initialize();

        ((AbstractDocument) passwordField.getDocument()).setDocumentFilter(new DocumentFilter() {
            @Override
            public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
                super.remove(fb, offset, length);
                changeButtonStatus();
            }

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                    throws BadLocationException {
                super.replace(fb, offset, length, text, attrs);
                changeButtonStatus();
            }

            /**
             * ボタンの状態を変更します。
             */
            private void changeButtonStatus() {
                boolean b = passwordField.getPassword().length > 0;
                okButton.setEnabled(b);
                clearButton.setEnabled(b);
            }
        });
    }

}

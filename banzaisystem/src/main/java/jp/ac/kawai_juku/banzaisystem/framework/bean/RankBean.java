package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * ランク情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class RankBean {

    /** ランクコード */
    private String rankCd;

    /** ランク名 */
    private String rankName;

    /** ランク上限値 */
    private double rankHLimits;

    /** ランク下限値 */
    private double rankLLimits;

    /**
     * ランクコードを返します。
     *
     * @return ランクコード
     */
    public String getRankCd() {
        return rankCd;
    }

    /**
     * ランクコードをセットします。
     *
     * @param rankCd ランクコード
     */
    public void setRankCd(String rankCd) {
        this.rankCd = rankCd;
    }

    /**
     * ランク上限値を返します。
     *
     * @return ランク上限値
     */
    public double getRankHLimits() {
        return rankHLimits;
    }

    /**
     * ランク上限値をセットします。
     *
     * @param rankHLimits ランク上限値
     */
    public void setRankHLimits(double rankHLimits) {
        this.rankHLimits = rankHLimits;
    }

    /**
     * ランク下限値を返します。
     *
     * @return ランク下限値
     */
    public double getRankLLimits() {
        return rankLLimits;
    }

    /**
     * ランク下限値をセットします。
     *
     * @param rankLLimits ランク下限値
     */
    public void setRankLLimits(double rankLLimits) {
        this.rankLLimits = rankLLimits;
    }

    /**
     * ランク名を返します。
     *
     * @return ランク名
     */
    public String getRankName() {
        return rankName;
    }

    /**
     * ランク名をセットします。
     *
     * @param rankName ランク名
     */
    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.dmod.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * 基準データ修正ダイアログViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
@Dialog
public class MainteDmodView extends AbstractView {

    /** 学年ラベル */
    @Location(x = 65, y = 51)
    @Size(width = 150, height = 12)
    @Foreground(r = 51, g = 51, b = 51)
    @Font(BZFont.BOLD)
    public TextLabel gradeLabel;

    /** クラスラベル */
    @Location(x = 65, y = 77)
    @Size(width = 150, height = 12)
    @Foreground(r = 51, g = 51, b = 51)
    @Font(BZFont.BOLD)
    public TextLabel classLabel;

    /** 番号ラベル */
    @Location(x = 65, y = 102)
    @Size(width = 150, height = 12)
    @Foreground(r = 51, g = 51, b = 51)
    @Font(BZFont.BOLD)
    public TextLabel numberLabel;

    /** 氏名ラベル */
    @Location(x = 65, y = 128)
    @Size(width = 150, height = 12)
    @Foreground(r = 51, g = 51, b = 51)
    @Font(BZFont.BOLD)
    public TextLabel nameLabel;

    /** 学年コンボボックス */
    @Location(x = 288, y = 47)
    @Size(width = 42, height = 20)
    public ComboBox<Integer> gradeComboBox;

    /** クラスフィールド */
    @Location(x = 288, y = 73)
    @Size(width = 42, height = 19)
    public TextField classField;

    /** 番号フィールド */
    @Location(x = 288, y = 99)
    @Size(width = 51, height = 19)
    public TextField numberField;

    /** 氏名フィールド */
    @Location(x = 288, y = 125)
    @Size(width = 147, height = 19)
    public TextField nameField;

    /** 説明ラベル */
    @Location(x = 26, y = 231)
    @Foreground(r = 51, g = 51, b = 51)
    public TextLabel noteLabel;

    /** OKボタン */
    @Location(x = 148, y = 168)
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 235, y = 168)
    public ImageButton cancelButton;

}

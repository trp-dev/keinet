package jp.ac.kawai_juku.banzaisystem.framework.service.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.TemplateId;

/**
 *
 * 帳票テンプレート設定のアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface Template {

    /**
     * 帳票テンプレートIDを設定します。
     *
     * @return 帳票テンプレートID
     */
    TemplateId id();

    /**
     * グラフ帳票フラグを設定します。
     *
     * @return グラフ帳票フラグ
     */
    boolean hasGraph() default false;

    /**
     * 最大シート数を設定します。
     *
     * @return 最大シート数
     */
    int maxSheetCount() default 0;

}

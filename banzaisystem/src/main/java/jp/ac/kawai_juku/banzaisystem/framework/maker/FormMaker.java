package jp.ac.kawai_juku.banzaisystem.framework.maker;

import java.io.File;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.FormMakerInBean;

/**
 *
 * 帳票メーカーのインターフェースです。
 *
 *
 * @param <T> 入力Beanの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface FormMaker<T extends FormMakerInBean> {

    /**
     * Excelファイルを生成します。
     *
     * @param inBean {@link FormMakerInBean}
     * @return 生成したExcelファイルリスト
     */
    List<File> create(T inBean);

    /**
     * 帳票IDを返します。
     *
     * @return 帳票ID
     */
    PrintId getPrintId();

}

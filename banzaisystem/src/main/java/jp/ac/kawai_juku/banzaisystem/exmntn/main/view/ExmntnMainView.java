package jp.ac.kawai_juku.banzaisystem.exmntn.main.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.exmntn.dist.controller.ExmntnDistController;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.annotation.MarkTextField;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectComboBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectTextField;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainTable;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.controller.ExmntnPrntController;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.controller.ExmntnRsltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.controller.ExmntnSubsController;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.controller.ExmntnUnivController;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZLayoutFocusTraversalPolicy;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZTabbedPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NumberField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 個人成績画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
public class ExmntnMainView extends AbstractView {

    /** 対象生徒選択ボタン */
    @Location(x = 281, y = 2)
    public ImageButton selectStudentButton;

    /** 個人成績・志望大学ラベル */
    @Location(x = 385, y = 2)
    public ImageLabel exmntnLabel;

    /** 帳票出力ボタン */
    @Location(x = 518, y = 2)
    public ImageButton reportButton;

    /** メンテナンスボタン */
    @Location(x = 632, y = 2)
    public ImageButton maintenanceButton;

    /** インフォメーションボタン */
    @Location(x = 736, y = 2)
    public ImageButton infoButton;

    /** インフォメーションラベル */
    @Location(x = 866, y = 2)
    public TextLabel informationLabel;

    /** 入試結果入力ボタン */
    @Location(x = 736, y = 2)
    @Invisible
    public ImageButton resultButton;

    /** ヘルプボタン */
    @Location(x = 854, y = 2)
    @Invisible
    public ImageButton helpButton;

    /** 終了ボタン */
    @Location(x = 928, y = 2)
    public ImageButton exitButton;

    /** 国語記述入力ボタン */
    /*
    @Location(x = 16, y = 136)
    public ImageButton inputButton;
    */

    /** 学年ラベル */
    @Location(x = 200, y = 45)
    public TextLabel gradeLabel;

    /** クラスラベル */
    @Location(x = 218, y = 45)
    public TextLabel classLabel;

    /** 番号ラベル */
    @Location(x = 303, y = 45)
    public TextLabel numberLabel;

    /** 現代文ラベル */
    @Location(x = 364, y = 88)
    @Size(width = 34, height = 20)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel contemporaryLabel;

    /** 古文ラベル  */
    @Location(x = 364, y = 109)
    @Size(width = 34, height = 20)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel classicsLabel;

    /** 漢文ラベル  */
    @Location(x = 421, y = 109)
    @Size(width = 34, height = 20)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel chineseLabel;

    /** 型ラベル(マーク模試) */
    @Location(x = 933, y = 84)
    @Size(width = 74, height = 49)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel markTypeLabel;

    /** 型ラベル(記述模試) */
    @Location(x = 933, y = 165)
    @Size(width = 74, height = 46)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel writtenTypeLabel;

    /** 生徒一覧ボタン */
    @Location(x = 516, y = 39)
    public ImageButton studentListButton;

    /** 前の生徒ボタン */
    @Location(x = 601, y = 43)
    public ImageButton beforeStudentButton;

    /** 次の生徒ボタン */
    @Location(x = 661, y = 43)
    public ImageButton nextStudentButton;

    /** 初期値に戻すボタン */
    @Location(x = 739, y = 41)
    public ImageButton returnInitDataPinkButton;

    /** 再判定ボタン */
    @Location(x = 846, y = 41)
    public ImageButton reJudgeButton;

    /** 成績保存ボタン */
    @Location(x = 917, y = 41)
    public ImageButton saveRecordButton;

    /** 第1,第2入れ替えボタン(理科) */
    @Location(x = 493, y = 70)
    public ImageButton changeFirstSecondOfSienceButton;

    /** 第1,第2入れ替えボタン(社会) */
    @Location(x = 872, y = 70)
    public ImageButton changeFirstSecondOfSocialButton;

    /** 志望大学リスト テーブル */
    @Location(x = 691, y = 313)
    @Size(width = 322, height = 385)
    public ExmntnMainTable candidateUnivTable;

    /** タブ */
    @Location(x = 4, y = 223)
    @TabConfig({ ExmntnUnivController.class, ExmntnDistController.class, ExmntnSubsController.class, ExmntnRsltController.class, ExmntnPrntController.class })
    public BZTabbedPane tabbedPane;

    /** ↑ボタン */
    @Location(x = 697, y = 254)
    @Disabled
    public ImageButton upButton;

    /** ↓ボタン */
    @Location(x = 723, y = 254)
    @Disabled
    public ImageButton downButton;

    /** 削除ボタン */
    @Location(x = 761, y = 254)
    @Disabled
    public ImageButton deleteButton;

    /** 詳細判定ボタン */
    @Location(x = 930, y = 254)
    @Disabled
    public ImageButton detailJudgeButton;

    /** スケジュール追加ボタン */
    @Location(x = 887, y = 252)
    @Invisible
    public ImageButton addScheduleButton;

    /** たくさんあるので、テキストフィールドはここにまとめる+++++++++++++++++++++ */
    /** 学年テキスト */
    @Location(x = 163, y = 41)
    @Size(width = 33, height = 20)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @MaxLength(1)
    public TextField gradeField;

    /** クラステキスト */
    @Location(x = 253, y = 41)
    @Size(width = 37, height = 20)
    @MaxLength(2)
    public TextField classField;

    /** 番号テキスト */
    @Location(x = 330, y = 41)
    @Size(width = 65, height = 20)
    @MaxLength(5)
    public TextField numberField;

    /** 氏名テキスト  */
    @Location(x = 373, y = 41)
    @Size(width = 139, height = 20)
    public TextField nameField;

    /** 英語1テキスト  */
    @Location(x = 200, y = 89)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField english1Field;

    /** リスニングテキスト  */
    @Location(x = 200, y = 110)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField listeningField;

    /** 英語2テキスト  */
    @Location(x = 200, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField english2Field;

    /** CEFR1テキスト  */
    /*    @Location(x = 170, y = 133)
    @Size(width = 154, height = 15)
    @ReadOnly
    public TextField cefr1Field;*/

    /** CEFR2テキスト  */
    /*    @Location(x = 170, y = 149)
    @Size(width = 154, height = 15)
    @ReadOnly
    public TextField cefr2Field;*/

    /** CEFRレベル1テキスト  */
    /*    @Location(x = 327, y = 133)
    @Size(width = 34, height = 15)
    @ReadOnly
    public TextField cefrLvl1Field;*/
    /** CEFRレベル2テキスト  */
    /*    @Location(x = 327, y = 149)
    @Size(width = 34, height = 15)
    @ReadOnly
    public TextField cefrLvl2Field;*/

    /** 数学1テキスト  */
    @Location(x = 327, y = 89)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField math1Field;

    /** 数学2テキスト  */
    @Location(x = 327, y = 110)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField math2Field;

    /** 数学3テキスト  */
    @Location(x = 327, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField math3Field;

    /** 現代文1テキスト  */
    @Location(x = 393, y = 89)
    @Size(width = 30, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField contemporaryField;

    /** 古文テキスト  */
    @Location(x = 393, y = 110)
    @Size(width = 30, height = 20)
    @NumberField(maxLength = 2)
    @MarkTextField
    public ExmntnMainSubjectTextField classicsField;

    /** 漢文テキスト  */
    @Location(x = 450, y = 110)
    @Size(width = 30, height = 20)
    @NumberField(maxLength = 2)
    @MarkTextField
    public ExmntnMainSubjectTextField chineseField;

    /** 現古漢テキスト  */
    @Location(x = 447, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField contemporaryClassicsChineseField;

    /** 国語記述評価テキスト  */
    /*
    @Location(x = 425, y = 133)
    @Size(width = 55, height = 15)
    @ReadOnly
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextField jpnWritingField;
    */
    /** 理科1テキスト  */
    @Location(x = 594, y = 89)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField science1Field;

    /** 理科2テキスト  */
    @Location(x = 594, y = 110)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField science2Field;

    /** 理科3テキスト  */
    @Location(x = 594, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField science3Field;

    /** 理科4テキスト  */
    @Location(x = 594, y = 189)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField science4Field;

    /** 理科5テキスト  */
    @Location(x = 735, y = 89)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 2)
    @MarkTextField
    public ExmntnMainSubjectTextField science5Field;

    /** 理科6テキスト  */
    @Location(x = 735, y = 110)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 2)
    @MarkTextField
    public ExmntnMainSubjectTextField science6Field;

    /** 理科7テキスト  */
    @Location(x = 735, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField science7Field;

    /** 理科8テキスト  */
    @Location(x = 735, y = 189)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField science8Field;

    /** 地歴公民1テキスト  */
    @Location(x = 892, y = 89)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField society1Field;

    /** 地歴公民2テキスト  */
    @Location(x = 892, y = 110)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 3)
    @MarkTextField
    public ExmntnMainSubjectTextField society2Field;

    /** 地歴公民3テキスト  */
    @Location(x = 892, y = 168)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField society3Field;

    /** 地歴公民4テキスト  */
    @Location(x = 892, y = 189)
    @Size(width = 34, height = 20)
    @NumberField(maxLength = 5, scale = 1)
    public ExmntnMainSubjectTextField society4Field;

    /** エラーメッセージ1 文字*/
    @Location(x = 16, y = 112)
    @Foreground(r = 255, g = 0, b = 0)
    @Invisible
    @Font(BZFont.INPUT_SCORE_ERROR)
    public ImageLabel errorMessageChar1Label;

    /** エラーメッセージ2 文字*/
    @Location(x = 16, y = 190)
    @Foreground(r = 255, g = 0, b = 0)
    @Invisible
    @Font(BZFont.INPUT_SCORE_ERROR)
    public ImageLabel errorMessageChar2Label;

    /** テキストフィールドはここまで++++++++++++++++++++++++++++++++++++ */

    /** コンボボックス++++++++++++++++++++++++++++++++++++++++++++++++++ */
    /** コンボボックス_学年 */
    @Location(x = 155, y = 41)
    @Size(width = 42, height = 20)
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** コンボボックス_クラス */
    @Location(x = 250, y = 41)
    @Size(width = 46, height = 20)
    @FireActionEvent
    public ComboBox<String> classComboBox;

    /** コンボボックス_番号 */
    @Location(x = 303, y = 41)
    @Size(width = 65, height = 20)
    @FireActionEvent
    public ComboBox<ExmntnMainKojinBean> numberComboBox;

    /** コンボボックス_マークシート試験 */
    @Location(x = 16, y = 90)
    @Size(width = 144, height = 20)
    public ComboBox<ExamBean> exmntnMarkComboBox;

    /** コンボボックス_記述試験 */
    @Location(x = 16, y = 168)
    @Size(width = 144, height = 20)
    public ComboBox<ExamBean> exmntnWritingComboBox;

    /** コンボボックス_数学1 */
    @Location(x = 248, y = 89)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox math1ComboBox;

    /** コンボボックス_数学2 */
    @Location(x = 248, y = 110)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox math2ComboBox;

    /** コンボボックス_数学3 */
    @Location(x = 248, y = 168)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox math3ComboBox;

    /** コンボボックス_国語 */
    @Location(x = 373, y = 168)
    @Size(width = 71, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox japaneseComboBox;

    /** コンボボックス_理科1 */
    @Location(x = 514, y = 89)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science1ComboBox;

    /** コンボボックス_理科2 */
    @Location(x = 514, y = 110)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science2ComboBox;

    /** コンボボックス_理科3 */
    @Location(x = 494, y = 168)
    @Size(width = 96, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science3ComboBox;

    /** コンボボックス_理科4 */
    @Location(x = 494, y = 189)
    @Size(width = 96, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science4ComboBox;

    /** コンボボックス_理科5 */
    @Location(x = 655, y = 89)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science5ComboBox;

    /** コンボボックス_理科6 */
    @Location(x = 655, y = 110)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science6ComboBox;

    /** コンボボックス_理科7 */
    @Location(x = 655, y = 168)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science7ComboBox;

    /** コンボボックス_理科8 */
    @Location(x = 655, y = 189)
    @Size(width = 76, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox science8ComboBox;

    /** コンボボックス_社会1 */
    @Location(x = 801, y = 89)
    @Size(width = 87, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox society1ComboBox;

    /** コンボボックス_社会2 */
    @Location(x = 801, y = 110)
    @Size(width = 87, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox society2ComboBox;

    /** コンボボックス_社会3 */
    @Location(x = 783, y = 168)
    @Size(width = 105, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox society3ComboBox;

    /** コンボボックス_社会4 */
    @Location(x = 783, y = 189)
    @Size(width = 105, height = 20)
    @NoAction
    public ExmntnMainSubjectComboBox society4ComboBox;

    /** コンボボックスはここまで++++++++++++++++++++++++++++++++++++ */

    /** リスニングチェックボックス */
    @Location(x = 173, y = 193)
    public ExmntnMainSubjectCheckBox listeningCheckBox;

    @Override
    @InitMethod
    public void initialize() {

        super.initialize();

        math1Field.setPairComboBox(math1ComboBox);
        math2Field.setPairComboBox(math2ComboBox);
        math3Field.setPairComboBox(math3ComboBox);
        contemporaryClassicsChineseField.setPairComboBox(japaneseComboBox);
        science1Field.setPairComboBox(science1ComboBox);
        science2Field.setPairComboBox(science2ComboBox);
        science3Field.setPairComboBox(science3ComboBox);
        science4Field.setPairComboBox(science4ComboBox);
        science5Field.setPairComboBox(science5ComboBox);
        science6Field.setPairComboBox(science6ComboBox);
        science7Field.setPairComboBox(science7ComboBox);
        science8Field.setPairComboBox(science8ComboBox);
        society1Field.setPairComboBox(society1ComboBox);
        society2Field.setPairComboBox(society2ComboBox);
        society3Field.setPairComboBox(society3ComboBox);
        society4Field.setPairComboBox(society4ComboBox);

        errorMessageChar1Label.setIconTextGap(-errorMessageChar1Label.getWidth() + 4);
        errorMessageChar2Label.setIconTextGap(-errorMessageChar2Label.getWidth() + 4);

        science1ComboBox.setMaximumRowCount(11);
        science2ComboBox.setMaximumRowCount(11);
        society1ComboBox.setMaximumRowCount(11);
        society2ComboBox.setMaximumRowCount(11);

        setDefaultFocusTraversalPolicy();
    }

    /**
     * デフォルト（記述模試が高２記述以外の場合）のLayoutFocusTraversalPolicyを設定します。
     */
    public void setDefaultFocusTraversalPolicy() {
        BZLayoutFocusTraversalPolicy policy = new BZLayoutFocusTraversalPolicy();
        policy.addPair(english1Field, listeningField);
        policy.addPair(listeningField, math1ComboBox);
        policy.addPair(math1Field, math2ComboBox);
        policy.addPair(math2Field, contemporaryField);
        policy.addPair(contemporaryField, classicsField);
        policy.addPair(chineseField, science1ComboBox);
        policy.addPair(science1Field, science2ComboBox);
        policy.addPair(science2Field, science5ComboBox);
        policy.addPair(science5Field, science6ComboBox);
        policy.addPair(science6Field, society1ComboBox);
        policy.addPair(society1Field, society2ComboBox);
        policy.addPair(english2Field, listeningCheckBox);
        policy.addPair(listeningCheckBox, math3ComboBox);
        policy.addPair(science3Field, science4ComboBox);
        policy.addPair(science4Field, science7ComboBox);
        policy.addPair(science7Field, science8ComboBox);
        policy.addPair(science8Field, society3ComboBox);
        policy.addPair(society3Field, society4ComboBox);
        setFocusTraversalPolicy(policy);
    }

    /**
     * 記述模試が高２記述の場合のLayoutFocusTraversalPolicyを設定します。
     */
    public void set2NDWrtntFocusTraversalPolicy() {
        BZLayoutFocusTraversalPolicy policy = new BZLayoutFocusTraversalPolicy();
        policy.addPair(english1Field, listeningField);
        policy.addPair(listeningField, math1ComboBox);
        policy.addPair(math1Field, math2ComboBox);
        policy.addPair(math2Field, contemporaryField);
        policy.addPair(contemporaryField, classicsField);
        policy.addPair(chineseField, science1ComboBox);
        policy.addPair(science1Field, science2ComboBox);
        policy.addPair(science2Field, science5ComboBox);
        policy.addPair(science5Field, science6ComboBox);
        policy.addPair(science6Field, society1ComboBox);
        policy.addPair(society1Field, society2ComboBox);
        policy.addPair(english2Field, listeningCheckBox);
        policy.addPair(listeningCheckBox, math3ComboBox);
        policy.addPair(science3Field, science4ComboBox);
        policy.addPair(science4Field, society3ComboBox);
        policy.addPair(society3Field, society4ComboBox);
        setFocusTraversalPolicy(policy);
    }

}

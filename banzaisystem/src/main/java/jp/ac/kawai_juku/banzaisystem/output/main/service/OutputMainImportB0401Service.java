package jp.ac.kawai_juku.banzaisystem.output.main.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_ON;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;
import javax.swing.SwingUtilities;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CsvId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainUnivHelper;
import jp.ac.kawai_juku.banzaisystem.framework.bean.CandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivCdHookUpBean;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CIndex;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvReader;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvRow;
import jp.ac.kawai_juku.banzaisystem.framework.dao.UnivCdHookUpDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.maker.B03CsvMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.ac.kawai_juku.banzaisystem.output.main.dao.OutputMainImportB0401Dao;
import jp.ac.kawai_juku.banzaisystem.output.main.view.OutputMainView;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.exception.IORuntimeException;

/**
 *
 * 志望大学・評価一覧CSV取り込みのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainImportB0401Service {

    /** 列位置定義：個人ID */
    private static final CIndex IDX_ID = new CIndex(0);

    /** 列位置定義：模試コード */
    private static final CIndex IDX_EXAM_CD = new CIndex(5);

    /** 列位置定義：大学コード10桁 */
    private static final CIndex IDX_UNIV_KEY = new CIndex(9);

    /** 列位置定義：大学コード5桁 */
    private static final CIndex IDX_UNIV5_KEY = new CIndex(10);

    /** ログサービス */
    @Resource
    private LogService logService;

    /** DAO */
    @Resource(name = "outputMainImportB0401Dao")
    private OutputMainImportB0401Dao dao;

    /** 大学コード紐付け処理のDAO */
    @Resource
    private UnivCdHookUpDao univCdHookUpDao;

    /** 志望大学ヘルパー */
    @Resource(name = "exmntnMainUnivHelper")
    private ExmntnMainUnivHelper univHelper;

    /** 志望大学個人成績画面のDTO */
    @Resource
    private ExmntnMainDto exmntnMainDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** View */
    @Resource(name = "outputMainView")
    private OutputMainView view;

    /**
     * 志望大学・評価一覧CSVを取り込みます。
     *
     * @param file CSVファイル
     */
    @Lock
    public void importCsv(File file) {

        /* 取り込み開始ログを出力する */
        outputStartLog(file);

        /* インポート可能模試リストを初期化する */
        List<ExamBean> examList = dao.selectExamList();

        /* 登録処理データマップ */
        Map<ExamBean, Map<Integer, List<UnivKeyBean>>> dataMap = new HashMap<>();

        try (CsvReader reader = new CsvReader(new FileInputStream(file)); B03CsvMaker maker = new B03CsvMaker();) {

            /* 1行目はヘッダ行のためスキップする */
            reader.readLine();

            CsvRow row = null;
            while ((row = reader.readLine()) != null) {
                /* 10桁の大学コードセット */
                univCodeSet(row);
                if (!addData(row, examList, dataMap, maker)) {
                    /* 入力情報が不正な場合の処理スキップメッセージを出力する */
                    maker.addSkipMessage(row.getRowNum());
                }
            }

            if (!dataMap.isEmpty()) {
                /* 志望大学を登録する */
                if (view.importTypeReplaceRadio.isSelected()) {
                    /* 置換モード */
                    registReplace(dataMap);
                } else {
                    /* 追加モード */
                    registAdd(dataMap);
                }
                /*
                 * 志望大学個人成績画面の志望大学リストを更新する。
                 * テーブルの書き換えが発生するので、イベントディスパッチスレッドで処理する。
                 */
                if (systemDto.getTargetIndividualId() != null && exmntnMainDto.getJudgeScore() != null) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            univHelper.initCandidateUniv(exmntnMainDto.getJudgeScore(), true);
                        }
                    });
                }
                /* 取り込み終了ログを出力する */
                outputEndLog(dataMap.keySet());
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 取り込み開始ログを出力します。
     *
     * @param file CSVファイル
     */
    private void outputStartLog(File file) {
        logService.output(CsvId.B04_01, "CSV取り込み開始", file.getAbsolutePath());
    }

    /**
     * 取り込み終了ログを出力します。
     *
     * @param importedExamSet インポート実施模試セット
     */
    private void outputEndLog(Set<ExamBean> importedExamSet) {
        for (ExamBean exam : importedExamSet) {
            logService.output(CsvId.B04_01, "CSV取り込み終了", exam.getExamYear(), exam.getExamCd());
        }
    }

    /**
     * 大学コード(5桁)に対応する大学コード(10桁)を設定
     *
     * @param row {@link CsvRow}
     */
    private void univCodeSet(CsvRow row) {

        String univCd = "";
        String hokanUnivCd = "";

        if (view.univCode10Radio.isSelected()) {
            /* 10桁のコードが選択されている場合は処理しない */
            return;
        } else {
            /* 10桁のコードを補完する */
            /* 5桁コードがない場合補完不可 */
            if (row.get(IDX_UNIV5_KEY) == null || row.get(IDX_UNIV5_KEY).equals("")) {
                return;
            }
            univCd = row.get(IDX_UNIV5_KEY);
        }

        /* 10桁のコードを補完する */
        if (dao.countUniv5(univCd) > 0) {
            hokanUnivCd = dao.selectUniv(univCd);
            row.set(IDX_UNIV_KEY, hokanUnivCd);
        } else {
            return;
        }

    }

    /**
     * 登録処理データマップにデータを追加します。
     *
     * @param row {@link CsvRow}
     * @param examList インポート可能模試リスト
     * @param dataMap 登録処理データマップ
     * @param maker {@link B03CsvMaker}
     * @return 追加に成功した場合はtrue
     */
    private boolean addData(CsvRow row, List<ExamBean> examList, Map<ExamBean, Map<Integer, List<UnivKeyBean>>> dataMap, B03CsvMaker maker) {

        /* 対象模試を取得する */
        ExamBean exam = findExamBean(row, examList);
        if (exam == null) {
            return false;
        }

        /* 個人IDを取得する */
        Integer id = findId(row);
        if (id == null) {
            return false;
        }

        /* 大学キーを取得する */
        UnivKeyBean bean = findUnivKeyBean(row);
        if (bean == null) {
            return false;
        }

        /* 模試別のデータマップを初期化する */
        Map<Integer, List<UnivKeyBean>> map = dataMap.get(exam);
        if (map == null) {
            map = new HashMap<>();
            dataMap.put(exam, map);
        }

        /* 個人別の大学キーリストを初期化する */
        List<UnivKeyBean> list = map.get(id);
        if (list == null) {
            list = new ArrayList<>();
            map.put(id, list);
        }

        if (list.indexOf(bean) < 0) {
            /* 大学キーが重複していなければリストに追加する */
            list.add(bean);
            return true;
        } else {
            return false;
        }
    }

    /**
     * CSVで入力されている模試コードに一致する模試データを返します。
     *
     * @param row {@link CsvRow}
     * @param examList インポート可能模試リスト
     * @return {@link ExamBean}
     */
    private ExamBean findExamBean(CsvRow row, List<ExamBean> examList) {

        String examYear = systemDto.getUnivYear();
        String examCd = row.get(IDX_EXAM_CD);

        /* 模試コードを0詰めする */
        if (!StringUtils.isEmpty(examCd) && examCd.length() == 1) {
            examCd = "0" + examCd;
        }

        for (ExamBean bean : examList) {
            if (bean.getExamYear().equals(examYear) && bean.getExamCd().equals(examCd)) {
                return bean;
            }
        }

        return null;
    }

    /**
     * 個人IDを返します。不正な個人IDの場合はnullを返します。
     *
     * @param row {@link CsvRow}
     * @return 個人ID
     */
    private Integer findId(CsvRow row) {

        String strId = row.get(IDX_ID);

        if (!StringUtils.isEmpty(strId) && ValidateUtil.isPositiveInteger(strId)) {
            Integer id = Integer.valueOf(strId);
            if (dao.countId(id) > 0) {
                return id;
            }
        }

        return null;
    }

    /**
     * 大学キーを返します。不正な大学キーの場合はnullを返します。
     *
     * @param row {@link CsvRow}
     * @return {@link UnivKeyBean}
     */
    private UnivKeyBean findUnivKeyBean(CsvRow row) {

        String checkUnivCd = "";

        if (view.univCode10Radio.isSelected()) {
            String univKey = row.get(IDX_UNIV_KEY);
            if (univKey != null && univKey.length() == 10) {
                UnivKeyBean bean = new UnivKeyBean(univKey);
                if (dao.countUniv(bean) > 0) {
                    return bean;
                }
            }
        } else {
            String univKey = row.get(IDX_UNIV5_KEY);
            if (univKey != null && univKey.length() == 5) {
                if (dao.countUniv5(univKey) > 0) {
                    checkUnivCd = dao.selectUniv(univKey);
                    UnivKeyBean bean = new UnivKeyBean(checkUnivCd);
                    if (dao.countUniv(bean) > 0) {
                        return bean;
                    }
                }
            }
        }

        return null;
    }

    /**
     * 置換モードで志望大学を登録します。
     *
     * @param dataMap 登録処理データマップ
     */
    private void registReplace(Map<ExamBean, Map<Integer, List<UnivKeyBean>>> dataMap) {
        for (ExamBean exam : dataMap.keySet()) {
            for (Entry<Integer, List<UnivKeyBean>> entry : dataMap.get(exam).entrySet()) {
                /* 対象模試の志望大学を削除する */
                UnivCdHookUpBean deleteBean = new UnivCdHookUpBean();
                deleteBean.setId(entry.getKey());
                deleteBean.setExamYear(exam.getExamYear());
                deleteBean.setExamCd(exam.getExamCd());
                univCdHookUpDao.deleteCandidateUniv(deleteBean);
                /* システムで登録した大学（区分=2）で登録する */
                int dispNum = 1;
                List<CandidateUnivBean> list = new ArrayList<>(entry.getValue().size());
                for (UnivKeyBean univKeyBean : entry.getValue()) {
                    CandidateUnivBean bean = new CandidateUnivBean();
                    bean.setId(entry.getKey());
                    bean.setExamYear(exam.getExamYear());
                    bean.setExamCd(exam.getExamCd());
                    bean.setSection(Code.CANDIDATE_UNIV_SCREEN.getValue());
                    bean.setUnivCd(univKeyBean.getUnivCd());
                    bean.setFacultyCd(univKeyBean.getFacultyCd());
                    bean.setDeptCd(univKeyBean.getDeptCd());
                    bean.setDispFlg(FLG_ON);
                    bean.setDispNum(Integer.valueOf(dispNum++));
                    bean.setYear(systemDto.getUnivYear());
                    bean.setExamDiv(systemDto.getUnivExamDiv());
                    list.add(bean);
                }
                univCdHookUpDao.insertCandidateUniv(list);
            }
        }
    }

    /**
     * 追加モードで志望大学を登録します。
     *
     * @param dataMap 登録処理データマップ
     */
    private void registAdd(Map<ExamBean, Map<Integer, List<UnivKeyBean>>> dataMap) {
        for (ExamBean exam : dataMap.keySet()) {
            for (Entry<Integer, List<UnivKeyBean>> entry : dataMap.get(exam).entrySet()) {
                /* システムで登録した大学（区分=2）で登録する */
                Integer dispNum = dao.countCandidateUnivRow(entry.getKey(), exam.getExamYear(), exam.getExamCd());
                List<CandidateUnivBean> list = new ArrayList<>(entry.getValue().size());
                for (UnivKeyBean univKeyBean : entry.getValue()) {
                    /* 既に登録済みのデータかチェック */
                    if (dao.countCandidateUniv(entry.getKey(), exam.getExamYear(), exam.getExamCd(), univKeyBean.getUnivCd(), univKeyBean.getFacultyCd(), univKeyBean.getDeptCd()) > 0) {
                        /* 登録済みの場合は、登録処理を行わない */
                        continue;
                    }

                    dispNum++;
                    CandidateUnivBean bean = new CandidateUnivBean();
                    bean.setId(entry.getKey());
                    bean.setExamYear(exam.getExamYear());
                    bean.setExamCd(exam.getExamCd());
                    bean.setSection(Code.CANDIDATE_UNIV_SCREEN.getValue());
                    bean.setUnivCd(univKeyBean.getUnivCd());
                    bean.setFacultyCd(univKeyBean.getFacultyCd());
                    bean.setDeptCd(univKeyBean.getDeptCd());
                    bean.setDispFlg(FLG_ON);
                    bean.setDispNum(Integer.valueOf(dispNum));
                    bean.setYear(systemDto.getUnivYear());
                    bean.setExamDiv(systemDto.getUnivExamDiv());
                    list.add(bean);
                }
                if (list.size() > 0) {
                    univCdHookUpDao.insertCandidateUniv(list);
                }
            }
        }
    }
}

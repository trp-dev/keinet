package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dto;

import java.util.List;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;

/**
 *
 * 検索結果画面のDTOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnRsltDto {

    /** 検索結果データリスト */
    private List<Map<String, CellData>> dataList;

    /**
     * 検索結果データリストを返します。
     *
     * @return 検索結果データリスト
     */
    public List<Map<String, CellData>> getDataList() {
        return dataList;
    }

    /**
     * 検索結果データリストをセットします。
     *
     * @param dataList 検索結果データリスト
     */
    public void setDataList(List<Map<String, CellData>> dataList) {
        this.dataList = dataList;
    }

}

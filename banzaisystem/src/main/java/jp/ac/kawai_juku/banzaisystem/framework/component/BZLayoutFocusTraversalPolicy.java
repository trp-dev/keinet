package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Component;
import java.awt.Container;
import java.util.List;

import javax.swing.LayoutFocusTraversalPolicy;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * バンザイシステムのLayoutFocusTraversalPolicyです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZLayoutFocusTraversalPolicy extends LayoutFocusTraversalPolicy {

    /** コンポーネントペアリスト */
    private final List<Pair<Component, Component>> pairList = CollectionsUtil.newArrayList();

    /**
     * コンポーネントのペアを追加します。<br>
     * Tabの移動順はleft→rightとなります。
     *
     * @param left 左側コンポーネント
     * @param right 右側コンポーネント
     */
    public void addPair(Component left, Component right) {
        pairList.add(Pair.of(left, right));
    }

    @Override
    public Component getComponentAfter(Container aContainer, Component aComponent) {
        for (Pair<Component, Component> pair : pairList) {
            if (pair.getLeft() == aComponent && pair.getRight().isEnabled()) {
                return pair.getRight();
            }
        }
        return super.getComponentAfter(aContainer, aComponent);
    }

    @Override
    public Component getComponentBefore(Container aContainer, Component aComponent) {
        for (Pair<Component, Component> pair : pairList) {
            if (pair.getRight() == aComponent && pair.getLeft().isEnabled()) {
                return pair.getLeft();
            }
        }
        return super.getComponentBefore(aContainer, aComponent);
    }

}

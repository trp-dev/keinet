package jp.ac.kawai_juku.banzaisystem.result.main.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.MenuController;
import jp.ac.kawai_juku.banzaisystem.result.main.view.ResultMainView;

/**
 *
 * 入試結果入力画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class ResultMainController extends MenuController {

    /** View */
    @Resource
    private ResultMainView resultMainView;

    /**
     * 初期表示アクションです。
     *
     * @return メンテナンス画面
     */
    @Execute
    public ExecuteResult index() {
        resultMainView.tabbedPane.activate();
        return VIEW_RESULT;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivDaoBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dao.ExmntnMainCandidateUnivDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.CandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.UnivCdHookUpDao;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.Counter;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績・志望大学画面用の志望大学サービスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainCandidateUnivService {

    /** DAO */
    @Resource(name = "exmntnMainCandidateUnivDao")
    private ExmntnMainCandidateUnivDao dao;

    /** 大学コード紐付け処理のDAO */
    @Resource
    private UnivCdHookUpDao univCdHookUpDao;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** View */
    @Resource(name = "exmntnSubsView")
    private ExmntnSubsView view;

    /**
     * （先生用）志望大学リストを取得します。
     *
     * @param id 個人ID
     * @param score 成績データ
     * @return 志望大学リスト
     */
    public List<ExmntnMainCandidateUnivBean> findTeacherCandidateUnivList(Integer id, ScoreBean score) {

        /* 判定実行時の成績を保存する */
        dto.setJudgeScore(score);

        /* 対象模試を判断する */
        ExamBean exam = ExamUtil.judgeTargetExam(score.getMarkExam(), score.getWrittenExam());
        if (exam == null) {
            return CollectionsUtil.newVector(0);
        }

        /* DBから志望大学リストを取得する */
        List<ExmntnMainCandidateUnivBean> list = dao.selectCandidateUnivList(exam, id);
        /* List<ExmntnMainCandidateUnivBean> list2 = dao.selectCandidateUnivList(exam, id); */

        /* 判定を実行する */
        if (!list.isEmpty()) {
            /*
            BZJudgementExecutor.getInstance().judge(score, list, false);
            BZJudgementExecutor.getInstance().judge(score, list2, true);
            */
            BZJudgementExecutor.getInstance().judge(score, list);
            /* BZJudgementExecutor.getInstance().judge(score, list2); */
            /*
            for (int index = 0; index < list2.size(); index++) {
                if (!ExamUtil.isCenter(exam)) {
            
                } else {
                    if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                        list.get(index).setValuateExclude("");
                    } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                    }
                }if(ExamUtil.isCenter(exam)){
                    if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                        list.get(index).setValuateExclude("");
                        list.get(index).setValuateInclude(list2.get(index).getValuateInclude());
                    } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                        list.get(index).setValuateInclude("");
                    }
                }else {
                    if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                        list.get(index).setValuateInclude(list2.get(index).getValuateInclude());
                    } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                        list.get(index).setValuateInclude("");
                    }
                }
            }
            */
        }
        return list;
    }

    /**
     * （生徒用）志望大学リストを取得します。
     *
     * @param keyList 大学キーリスト
     * @param score 成績データ
     * @return 志望大学リスト
     */
    public List<ExmntnMainCandidateUnivBean> findStudentCandidateUnivList(List<UnivKeyBean> keyList, ScoreBean score) {

        /* 判定実行時の成績を保存する */
        dto.setJudgeScore(score);

        /* 対象模試を判断する */
        /*ExamBean exam = ExamUtil.judgeTargetExam(score.getMarkExam(), score.getWrittenExam());*/

        if (keyList == null || keyList.isEmpty()) {
            /* 大学キーが空なら処理はここまで */
            return CollectionsUtil.newVector(0);
        }

        /* DBから大学名称リストを取得する */
        List<ExmntnMainCandidateUnivBean> list = dao.selectUnivNameList(keyList);
        /* List<ExmntnMainCandidateUnivBean> list2 = dao.selectUnivNameList(keyList); */

        /* 判定を実行する */
        if (!list.isEmpty()) {
            /*
            BZJudgementExecutor.getInstance().judge(score, list, false);
            BZJudgementExecutor.getInstance().judge(score, list2, true);
              */
            BZJudgementExecutor.getInstance().judge(score, list);
            /* BZJudgementExecutor.getInstance().judge(score, list2); */
            /*
            for (int index = 0; index < list2.size(); index++) {
                if (exam == null) {
            */
            /* テスト未選択の場合は処理を実施しない */
            /*
                } else {
                    if (!ExamUtil.isCenter(exam)) {
            
                    } else {
                        if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                            list.get(index).setValuateExclude("");
                        } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                        }
                    }
                    if (ExamUtil.isCenter(exam)) {
                        if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                            list.get(index).setValuateExclude("");
                            list.get(index).setValuateInclude(list2.get(index).getValuateInclude());
                        } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                            list.get(index).setValuateInclude("");
                        }
                    } else {
                        if (!StringUtil.isEmptyTrim(list2.get(index).getAppReqPaternCd())) {
                            list.get(index).setValuateInclude(list2.get(index).getValuateInclude());
                        } else {
            */
            /* 英語認定試験を課さない場合、含まない評価をそのまま表示する */
            /* 英語認定試験を課さない場合、含む評価は空表示 */
            /*
                            list.get(index).setValuateInclude("");
                        }
                    }
                }
            }
            */
        }
        return list;
    }

    /**
     * （先生用）志望大学を削除します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @param list 削除する大学リスト
     */
    public void deleteTeacherCandidateUniv(Integer id, ExamBean exam, List<? extends UnivKeyBean> list) {

        /* 志望大学を削除する */
        dao.deleteCandidateUniv(id, exam, list);

        /* 模試記入志望大学削除された、非表示になっているシステム登録志望大学を表示する */
        dao.updateCandidateUnivDispOn(id, exam, list);

        /* 全ての模試の志望大学を取得する */
        List<CandidateUnivBean> allList = dao.selectAllCandidateUnivList(id);

        /* 模試・表示フラグ別に表示順を振り直す */
        Counter<Pair<String, String>> counter = Counter.newInstance();
        for (CandidateUnivBean bean : allList) {
            bean.setDispNum(counter.add(Pair.of(bean.getExamCd(), bean.getDispFlg())));
        }

        /* 登録しなおす */
        dao.deleteAllCandidateUniv(id);
        univCdHookUpDao.insertCandidateUniv(allList);
    }

    /**
     * （生徒用）志望大学を削除します。
     *
     * @param list 削除する大学リスト
     */
    public void deleteStudentCandidateUniv(List<? extends UnivKeyBean> list) {

        /* 志望大学を削除する */
        dto.getStudentCandidateUnivList().removeAll(list);
    }

    /**
     * （先生用）志望順位を変更します。
     *
     * @param id 個人ID
     * @param score 成績データ
     * @param keyBean 移動対象
     * @param isUp 上に移動するならtrue
     */
    @Lock
    public void changeTeacherRank(Integer id, ScoreBean score, UnivKeyBean keyBean, boolean isUp) {

        /* 対象模試を判断する */
        ExamBean exam = ExamUtil.judgeTargetExam(score.getMarkExam(), score.getWrittenExam());
        if (exam == null) {
            return;
        }

        /* 志望順位を入れ替えるデータを取得する */
        Pair<ExmntnMainCandidateUnivDaoBean, ExmntnMainCandidateUnivDaoBean> pair = dao.selectChangeRankCandidateUnivList(exam, id, keyBean, isUp);
        if (pair == null) {
            return;
        }

        /* 志望順位を入れ替える */
        Integer tmp = pair.getLeft().getDispNumber();
        pair.getLeft().setDispNumber(pair.getRight().getDispNumber());
        pair.getRight().setDispNumber(tmp);

        /* DBを更新する */
        dao.updateCandidateUniv(exam, id, pair.getLeft());
        dao.updateCandidateUniv(exam, id, pair.getRight());
    }

    /**
     * （生徒用）志望順位を変更します。
     *
     * @param univList 大学キーリスト
     * @param keyBean 移動対象
     * @param isUp 上に移動するならtrue
     */
    public void changeStudentRank(List<UnivKeyBean> univList, UnivKeyBean keyBean, boolean isUp) {
        int index = univList.indexOf(keyBean);
        int newIndex = index + (isUp ? -1 : 1);
        univList.add(newIndex, univList.remove(index));
    }

}

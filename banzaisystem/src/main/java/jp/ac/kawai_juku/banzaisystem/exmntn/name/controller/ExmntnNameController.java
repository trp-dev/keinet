package jp.ac.kawai_juku.banzaisystem.exmntn.name.controller;

import javax.annotation.Resource;
import javax.swing.JToggleButton;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.view.ExmntnNameView;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;

/**
 *
 * 大学検索（大学名称・所在地）画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnNameController extends SubController {

    /** View */
    @Resource
    private ExmntnNameView exmntnNameView;

    @Override
    @Logging(GamenId.C3_1)
    public void activatedAction() {

    }

    /**
     * クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearUnivNameButtonAction() {
        exmntnNameView.inputField.setText("");
        return null;
    }

    /**
     * 名称所在地クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearNameSeatButtonAction() {
        clearUnivNameButtonAction();
        for (JToggleButton b : exmntnNameView.findComponentsByClass(JToggleButton.class)) {
            b.setSelected(false);
        }
        return null;
    }

}

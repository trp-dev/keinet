package jp.ac.kawai_juku.banzaisystem.framework.properties;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_OFF;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 設定ファイルのキーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum SettingKey implements PropsKey {

    /** パスワード初期化フラグ */
    INIT_PASSWORD {
        @Override
        public String getDefaultValue() {
            return FLG_OFF;
        }
    },

    /** 現バージョン */
    CURRENT_VERSION {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 最新バージョン */
    LATEST_VERSION {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 先生用パスワード */
    TEACHER_PASSWORD {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 管理者用パスワード */
    ADMIN_PASSWORD {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 大学マスタ（ローカル） */
    UNIV_LOCAL_DIR {
        @Override
        public String getDefaultValue() {
            return new File("db").getAbsolutePath();
        }
    },

    /** 成績データ（ローカル） */
    RECORD_LOCAL_DIR {
        @Override
        public String getDefaultValue() {
            return new File("db").getAbsolutePath();
        }
    },

    /** 出力ファイル（ローカル） */
    OUTPUT_LOCAL_DIR {
        @Override
        public String getDefaultValue() {
            return new File("output").getAbsolutePath();
        }
    },

    /** ログファイル（ローカル） */
    LOG_LOCAL_DIR {
        @Override
        public String getDefaultValue() {
            return new File("log").getAbsolutePath();
        }
    },

    /** 環境設定ファイル（ローカル） */
    CONFIG_LOCAL_DIR {
        @Override
        public String getDefaultValue() {
            return new File("config").getAbsolutePath();
        }
    },

    /** ネットワーク設定 */
    NETWORK_SETTING {
        @Override
        public String getDefaultValue() {
            return FLG_OFF;
        }
    },

    /** 大学マスタ（ネットワーク） */
    UNIV_NETWORK_DIR {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 成績データ（ネットワーク） */
    RECORD_NETWORK_DIR {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 出力ファイル（ネットワーク） */
    OUTPUT_NETWORK_DIR {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** ログファイル（ネットワーク） */
    LOG_NETWORK_DIR {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 環境設定ファイル（ネットワーク） */
    CONFIG_NETWORK_DIR {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 更新データ配信サイトURL */
    UPDATER_SITE_URL {
        @Override
        public String getDefaultValue() {
            return "http://banzai-download.kawai-juku.ac.jp/pcbanzai/";
        }
    },

    /** プロキシホスト名 */
    PROXY_HOST {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** プロキシポート */
    PROXY_PORT {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** プロキシユーザ名 */
    PROXY_USER {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** プロキシパスワード */
    PROXY_PASSWORD {
        @Override
        public String getDefaultValue() {
            return StringUtils.EMPTY;
        }
    },

    /** 成績デバッグ出力 */
    DEBUG_SCORE {
        @Override
        public String getDefaultValue() {
            return "0";
        }
    },

    /** 判定結果デバッグ出力 */
    DEBUG_RESULT {
        @Override
        public String getDefaultValue() {
            return "0";
        }
    };

}

package jp.ac.kawai_juku.banzaisystem.mainte.mstr.helper;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClient;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClientException;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet.UnivDb;

import org.seasar.framework.log.Logger;

/**
 *
 * メンテナンス-大学マスタ・プログラム最新化画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMstrHelper {

    /** Logger */
    private static final Logger logger = Logger.getLogger(UpdaterHttpClient.class);

    /** Service */
    @Resource
    private MainteMstrService mainteMstrService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 更新データ配信サイトからダウンロード可能大学マスタセットを取得します。
     *
     * @param client {@link UpdaterHttpClient}
     * @param ignoreError 通信エラー無視フラグ
     * @return ダウンロード可能大学マスタセット
     */
    public UnivDbSet findDbSet(UpdaterHttpClient client, boolean ignoreError) {
        try {
            return mainteMstrService.findDbSet(client);
        } catch (UpdaterHttpClientException e) {
            if (ignoreError) {
                logger.error(e);
                return null;
            } else {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"), e);
            }
        }
    }

    /**
     * 更新処理対象の大学マスタを返します。
     *
     * @param client {@link UpdaterHttpClient}
     * @param selectedMaster 画面で選択されている大学マスタ
     * @return 更新処理対象の大学マスタ
     */
    public UnivDb getUpdateTargetUnivMaster(UpdaterHttpClient client, String[] selectedMaster) {

        UnivDbSet dbSet = findDbSet(client, false);
        if (dbSet.isEmpty()) {
            /*
             * サーバ上に大学マスタが存在しない場合はエラーとする。
             * ※画面表示後にサーバ上から大学マスタが消された場合に発生するケース。
             */
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"));
        }

        UnivDb latest = dbSet.getLatest();
        if (selectedMaster == null || latest.getEventYear().compareTo(systemDto.getUnivYear()) > 0) {
            /* 画面で大学マスタを未選択か、サーバ上の最新大学マスタが次年度の場合 */
            return latest;
        } else {
            UnivDb univDb = dbSet.get(selectedMaster[0], selectedMaster[1]);
            if (univDb == null) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"));
            } else {
                return univDb;
            }
        }
    }

    /**
     * ダウンロード処理対象の大学マスタを返します。
     *
     * @param client {@link UpdaterHttpClient}
     * @param selectedMaster 画面で選択されている大学マスタ
     * @return ダウンロード処理対象の大学マスタ
     */
    public UnivDb getDownloadTargetUnivMaster(UpdaterHttpClient client, String[] selectedMaster) {

        UnivDbSet dbSet = findDbSet(client, false);
        if (dbSet.isEmpty()) {
            /*
             * サーバ上に大学マスタが存在しない場合はエラーとする。
             * ※画面表示後にサーバ上から大学マスタが消された場合に発生するケース。
             */
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"));
        }

        /* 画面で選択されている大学マスタを優先する */
        if (selectedMaster != null) {
            UnivDb univDb = dbSet.get(selectedMaster[0], selectedMaster[1]);
            if (univDb == null) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"));
            } else {
                return univDb;
            }
        } else {
            /* サーバ上の最新大学マスタを返す */
            return dbSet.getLatest();
        }
    }

}

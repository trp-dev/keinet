package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

/**
 *
 * 模試科目マスタに関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamSubjectUtil {

    /** 模試科目マスタマップ */
    private final Map<String, ExamSubjectBean> examSubjectMap;

    /**
     * コンストラクタです。
     *
     * @param systemDto システム情報DTO
     * @param exam 対象模試
     */
    public ExamSubjectUtil(SystemDto systemDto, ExamBean exam) {
        examSubjectMap = systemDto.getExamSubjectMap().get(exam.getExamCd());
        if (examSubjectMap == null) {
            throw new RuntimeException(String.format("模試科目マスタが未定義です。[模試コード=%s]", exam.getExamCd()));
        }
    }

    /**
     * 科目名を返します。
     *
     * @param subCd 科目コード
     * @return 科目名
     */
    public String getSubName(String subCd) {
        ExamSubjectBean bean = examSubjectMap.get(subCd);
        return bean == null ? null : bean.getSubName();
    }

    /**
     * 科目の表示順を返します。
     *
     * @param subCd 科目コード
     * @return 科目の表示順
     */
    public String getDispSequence(String subCd) {
        ExamSubjectBean bean = examSubjectMap.get(subCd);
        return bean == null ? subCd : bean.getDispSequence();
    }

    /**
     * 基礎科目であるかどうかを判定します。
     *
     * @param subCd 科目コード
     * @return 基礎科目ならtrue
     */
    public boolean isBasic(String subCd) {
        ExamSubjectBean bean = examSubjectMap.get(subCd);
        return bean == null ? false : FlagUtil.toBoolean(bean.getBasicFlg());
    }

}

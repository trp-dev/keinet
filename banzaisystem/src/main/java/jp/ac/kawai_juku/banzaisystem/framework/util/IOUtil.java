package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.util.FileUtil;

/**
 *
 * ファイルなどのIO操作に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class IOUtil {

    /**
     * コンストラクタです。
     */
    private IOUtil() {
    }

    /**
     * 入出力を閉じます。
     *
     * @param in 入力Closeable
     * @param out 出力Closeable
     */
    public static void close(Closeable in, Closeable out) {
        try {
            close(in);
        } finally {
            close(out);
        }
    }

    /**
     * {@link Closeable}を閉じます。
     *
     * @param c {@link Closeable}
     */
    public static void close(Closeable c) {
        try {
            if (c != null) {
                c.close();
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * ファイルを削除します。
     *
     * @param file ファイル
     */
    public static void deleteFile(File file) {
        if (file != null && file.exists() && !file.delete()) {
            throw new IORuntimeException(new IOException("ファイルの削除に失敗しました。" + file));
        }
    }

    /**
     * ファイルを移動します。
     *
     * @param srcFile 移動するファイル
     * @param destDir 移動先ディレクトリ
     */
    public static void moveFileToDirectory(File srcFile, File destDir) {
        try {
            new File(destDir, srcFile.getName()).delete();
            FileUtils.moveFileToDirectory(srcFile, destDir, false);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * ファイルをコピーします。
     *
     * @param srcFile 移動するファイル
     * @param destDir 移動先ディレクトリ
     */
    public static void copyFileToDirectory(File srcFile, File destDir) {
        try {
            if (!isIdentical(srcFile, new File(destDir, srcFile.getName()))) {
                FileUtils.copyFileToDirectory(srcFile, destDir, false);
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * ファイルオブジェクトをリネームします。
     *
     * @param file リネームするファイルオブジェクト
     * @param newName 新しい名前
     * @return リネーム後のファイルオブジェクト
     */
    public static File renameFile(File file, String newName) {
        File dest = new File(file.getParentFile(), newName);
        if (!file.renameTo(dest)) {
            throw new IORuntimeException(new IOException(String.format("リネームに失敗しました。[%s]->[%s]", file, newName)));
        }
        return dest;
    }

    /**
     * ディレクトリが書き込み可能であるかをチェックします。
     *
     * @param dir ディレクトリ
     * @return 書き込み可能ならtrue
     */
    public static boolean isWritableDirectory(File dir) {
        if (!dir.canWrite()) {
            return false;
        }
        try {
            writeTest(dir);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    /**
     * ディレクトリに対する書き込みテストを行います。
     *
     * @param dir ディレクトリ
     * @throws IOException 書き込みに失敗したとき
     */
    private static void writeTest(File dir) throws IOException {
        OutputStream out = null;
        File file = null;
        try {
            file = File.createTempFile("banzai", null, dir);
            out = new FileOutputStream(file);
            out.write(0);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } finally {
                if (file != null) {
                    file.delete();
                }
            }
        }
    }

    /**
     * ファイルが同一であるかを返します。
     *
     * @param file1 ファイル1
     * @param file2 ファイル2
     * @return ファイルが同一ならtrue
     */
    public static boolean isIdentical(File file1, File file2) {
        return FileUtil.getCanonicalPath(file1).equals(FileUtil.getCanonicalPath(file2));
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ公表用選択グループを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterSelectGroupBean {

    /** 入試試験区分 */
    private String entExamDiv;

    /** 大学コード枝番 */
    private String uniCdBranchCd;

    /** 選択グループ番号 */
    private String selectGNo;

    /** 選択グループ科目数 */
    private int selectGSubCount;

    /** 選択グループ配点 */
    private double selectGAllotPnt;

    /**
     * 入試試験区分を返します。
     *
     * @return 入試試験区分
     */
    public String getEntExamDiv() {
        return entExamDiv;
    }

    /**
     * 入試試験区分をセットします。
     *
     * @param entExamDiv 入試試験区分
     */
    public void setEntExamDiv(String entExamDiv) {
        this.entExamDiv = entExamDiv;
    }

    /**
     * 大学コード枝番を返します。
     *
     * @return 大学コード枝番
     */
    public String getUniCdBranchCd() {
        return uniCdBranchCd;
    }

    /**
     * 大学コード枝番をセットします。
     *
     * @param uniCdBranchCd 大学コード枝番
     */
    public void setUniCdBranchCd(String uniCdBranchCd) {
        this.uniCdBranchCd = uniCdBranchCd;
    }

    /**
     * 選択グループ番号を返します。
     *
     * @return 選択グループ番号
     */
    public String getSelectGNo() {
        return selectGNo;
    }

    /**
     * 選択グループ番号をセットします。
     *
     * @param selectGNo 選択グループ番号
     */
    public void setSelectGNo(String selectGNo) {
        this.selectGNo = selectGNo;
    }

    /**
     * 選択グループ科目数を返します。
     *
     * @return 選択グループ科目数
     */
    public int getSelectGSubCount() {
        return selectGSubCount;
    }

    /**
     * 選択グループ科目数をセットします。
     *
     * @param selectGSubCount 選択グループ科目数
     */
    public void setSelectGSubCount(int selectGSubCount) {
        this.selectGSubCount = selectGSubCount;
    }

    /**
     * 選択グループ配点を返します。
     *
     * @return 選択グループ配点
     */
    public double getSelectGAllotPnt() {
        return selectGAllotPnt;
    }

    /**
     * 選択グループ配点をセットします。
     *
     * @param selectGAllotPnt 選択グループ配点
     */
    public void setSelectGAllotPnt(double selectGAllotPnt) {
        this.selectGAllotPnt = selectGAllotPnt;
    }

}

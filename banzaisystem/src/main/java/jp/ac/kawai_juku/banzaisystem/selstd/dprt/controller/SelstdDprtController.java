package jp.ac.kawai_juku.banzaisystem.selstd.dprt.controller;

import java.awt.Component;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0101Maker;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0102Maker;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0103Maker;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtService;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.view.SelstdDprtView;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.controller.SelstdDstdController;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.dto.SelstdDstdDto;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.service.SelstdDstdService;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 一覧出力ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class SelstdDprtController extends AbstractController {

    /** View */
    @Resource(name = "selstdDprtView")
    private SelstdDprtView view;

    /** Service */
    @Resource(name = "selstdDprtService")
    private SelstdDprtService service;

    /** 対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    /** 生徒一覧ダイアログのサービス */
    @Resource
    private SelstdDstdService selstdDstdService;

    /** 生徒一覧ダイアログのDTO */
    @Resource
    private SelstdDstdDto selstdDstdDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 初期表示アクションです。
     *
     * @return 一覧出力ダイアログ
     */
    @Logging(GamenId.B_d1)
    @Execute
    public ExecuteResult index() {

        /* 初期値を設定する */
        view.examComboBox.copyFrom(selstdMainView.examComboBox);
        view.scoreListRadio.setSelected(true);
        view.selectedStudentRadio.setSelected(true);
        view.numberStartField.setText("0");
        view.numberEndField.setText("99999");

        return VIEW_RESULT;
    }

    /**
     * 対象模試コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult examComboBoxAction() {
        initGradeComboBox();
        return null;
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        ExamBean exam = view.examComboBox.getSelectedValue();
        if (exam == null) {
            view.gradeComboBox.setItemList(Collections.<ComboBoxItem<Integer>> emptyList());
        } else {
            List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
            for (Integer grade : service.findGradeList(exam)) {
                itemList.add(new ComboBoxItem<Integer>(grade));
            }
            if (view.gradeComboBox.getItemCount() == 0) {
                /* 初回のみ模試の対象学年を初期値とする */
                view.gradeComboBox.setItemList(itemList, new ComboBoxItem<Integer>(exam.getTargetGrade()));
            } else {
                view.gradeComboBox.setItemList(itemList);
            }
        }
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        initClassTable();
        changePrintButtonStatus();
        return null;
    }

    /**
     * クラステーブルを初期化します。
     */
    private void initClassTable() {
        ComboBoxItem<ExamBean> exam = view.examComboBox.getSelectedItem();
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (exam == null || grade == null) {
            view.classTable.setClassList(exam, grade, Collections.<String> emptyList());
        } else {
            view.classTable.setClassList(exam, grade, service.findClassList(exam.getValue(), grade.getValue()));
        }
    }

    /**
     * 「画面で選択されている生徒」ラジオ選択アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult selectedStudentRadioAction() {
        for (Component c : view.findComponentsByGroup(0)) {
            c.setEnabled(false);
        }
        changePrintButtonStatus();
        return null;
    }

    /**
     * 「改めて選択する」ラジオ選択アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult selectAgain20RadioAction() {
        for (Component c : view.findComponentsByGroup(0)) {
            c.setEnabled(true);
        }
        changePrintButtonStatus();
        return null;
    }

    /**
     * 「全選択」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult allChoiceButtonAction() {
        view.classTable.selectAll(true);
        return null;
    }

    /**
     * 「全解除」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult allLiftButtonAction() {
        view.classTable.selectAll(false);
        return null;
    }

    /**
     * 印刷ボタンの状態を変更します。
     */
    public void changePrintButtonStatus() {
        /* 「改めて選択する」場合、クラス未選択か席次番号が空欄なら印刷ボタンを無効にする */
        view.printButton
                .setEnabled(view.selectedStudentRadio.isSelected()
                        || (!view.classTable.getSelectedClassList().isEmpty() && (!view.numberStartField.getText()
                                .isEmpty() || !view.numberEndField.getText().isEmpty())));
        /* 「生徒一覧から選択」で選択した生徒をクリアする */
        selstdDstdDto.getSelectedIndividualIdList().clear();
    }

    /**
     * 生徒一覧選択ボタンアクションです。
     *
     * @return 生徒一覧ダイアログ
     */
    @Execute
    public ExecuteResult choiceStudentListButtonAction() {
        /* 複数選択モードで起動する */
        return forward(SelstdDstdController.class, "indexMulti");
    }

    /**
     * 印刷ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult printButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.B_d1).addCreator(findExcelCreatorService(), createInBean()).launch();
                return null;
            }
        };
    }

    /**
     * Excel生成サービスの入力Beanを生成します。
     *
     * @return 入力Bean
     */
    private SelstdDprtExcelMakerInBean createInBean() {
        return new SelstdDprtExcelMakerInBean(view.examComboBox.getSelectedValue(),
                ExamUtil.findDockingExam(view.examComboBox), findPrintTargetList());
    }

    /**
     * ラジオの選択値に従ったExcel生成サービスのクラスを返します。
     *
     * @return Excel生成サービスのクラス
     */
    private Class<? extends BaseExcelMaker<SelstdDprtExcelMakerInBean>> findExcelCreatorService() {
        Code code = view.getSelectedCode(CodeType.OUTPUT_LIST_TARGET);
        if (code == Code.SCORE_LIST) {
            /* 個人成績・志望一覧(個人成績一覧)帳票 */
            return SelstdDprtA0101Maker.class;
        } else if (code == Code.SCORE_PLUS_UNIV9) {
            /* 個人成績・志望一覧(志望大学上位9大学) */
            return SelstdDprtA0102Maker.class;
        } else {
            /* 個人成績・志望一覧(志望大学上位20大学) */
            return SelstdDprtA0103Maker.class;
        }
    }

    /**
     * 印刷対象の個人IDリストを返します。
     *
     * @return 個人IDリスト
     */
    private List<Integer> findPrintTargetList() {
        if (view.selectedStudentRadio.isSelected()) {
            /* 「画面で選択されている生徒」の場合 */
            return systemDto.getSelectedIndividualIdList();
        } else if (!selstdDstdDto.getSelectedIndividualIdList().isEmpty()) {
            /* 生徒一覧ダイアログで選択されている場合 */
            return selstdDstdDto.getSelectedIndividualIdList();
        } else {
            /* 画面の条件で検索する */
            List<Integer> list = CollectionsUtil.newArrayList();
            for (Map<String, CellData> map : selstdDstdService.findKojinList(view.gradeComboBox.getSelectedValue(),
                    view.classTable.getSelectedClassList(), view.numberStartField.getText(),
                    view.numberEndField.getText())) {
                list.add((Integer) map.get("id").getSortValue());
            }
            return list;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dstd.dto;

import java.util.List;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 生徒一覧ダイアログのDTOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDstdDto {

    /** 選択中生徒の個人IDリスト */
    private final List<Integer> selectedIndividualIdList = CollectionsUtil.newArrayList();

    /**
     * 選択中生徒の個人IDリストを返します。
     *
     * @return 選択中生徒の個人IDリスト
     */
    public List<Integer> getSelectedIndividualIdList() {
        return selectedIndividualIdList;
    }

}

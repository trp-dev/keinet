package jp.ac.kawai_juku.banzaisystem.exmntn.subs.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.helper.ExmntnEnteHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.controller.ExmntnNameController;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.view.ExmntnNameView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.controller.ExmntnRsltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.helper.ExmntnSubsHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;

/**
 *
 * 大学検索画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class ExmntnSubsController extends SubController {

    /** View */
    @Resource(name = "exmntnSubsView")
    private ExmntnSubsView view;

    /** 大学検索（大学名称・所在地）画面のView */
    @Resource
    private ExmntnNameView exmntnNameView;

    /** 個人成績画面の成績ヘルパー */
    @Resource
    private ExmntnMainScoreHelper exmntnMainScoreHelper;

    /** 個人成績画面の志望大学ヘルパー */
    @Resource
    private ExmntnMainUnivHelper exmntnMainUnivHelper;

    /** 大学検索（入試情報（評価・日程・科目））画面のヘルパー */
    @Resource
    private ExmntnEnteHelper exmntnEnteHelper;

    /** Helper */
    @Resource
    private ExmntnSubsHelper exmntnSubsHelper;

    @Override
    @Logging(GamenId.C3)
    public void activatedAction() {
        view.tabbedPane.activate();
        /* 含まないのみにチェックOFF */
        /*
        view.englishJoinningCheckBox.setSelected(false);
        if (exmntnMainScoreHelper.getTargetExam() == null) {
            view.englishJoinningCheckBox.setVisible(true);
        } else {
            view.englishJoinningCheckBox.setVisible(!ExamUtil.isCenter(exmntnMainScoreHelper.getTargetExam()));
        }
        */
    }

    /**
     * 検索ボタン押下時のアクションです。
     *
     * @return 検索結果画面
     */
    @Execute
    public ExecuteResult startSearchLargeButtonAction() {

        /* 大学名をチェックする */
        if (view.tabbedPane.findCheckBox(ExmntnNameController.class).isSelected() && !ValidateUtil.isHiragana(exmntnNameView.inputField.getText())) {
            throw new MessageDialogException(getMessage("error.exmntn.subs.C_E007"));
        }

        /* 検索結果をクリアする */
        exmntnSubsHelper.clearSearchResult();

        /* 判定に利用する成績データを生成する */
        final ScoreBean scoreBean = exmntnMainScoreHelper.createDispScore();

        /* 別スレッドで検索する */
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                exmntnSubsHelper.search(scoreBean);
                return activate(ExmntnRsltController.class);
            }

            @Override
            protected void done() {
                /* 検索時の成績で志望大学リストを初期化する */
                exmntnMainUnivHelper.initCandidateUniv(scoreBean, true);
            }
        };
    }

    /**
     * 区分チェックボックスアクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult univDivCheckBoxAction() {
        exmntnEnteHelper.renewScheduleStatus();
        return null;
    }

}

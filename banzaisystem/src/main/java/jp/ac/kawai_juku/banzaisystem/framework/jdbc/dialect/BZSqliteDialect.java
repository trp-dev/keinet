package jp.ac.kawai_juku.banzaisystem.framework.jdbc.dialect;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.persistence.TemporalType;

import org.seasar.extension.jdbc.ValueType;
import org.seasar.extension.jdbc.dialect.SqliteDialect;
import org.seasar.extension.jdbc.types.StringType;

/**
 *
 * SQLite用の方言クラスです。<br>
 * 空文字列のパラメータをNULLに変換します。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZSqliteDialect extends SqliteDialect {

    /** StringTypeのインスタンス */
    private final StringType stringType = new StringType() {
        @Override
        public void bindValue(PreparedStatement ps, int index, Object value) throws SQLException {
            if (value == null || (value instanceof String && ((String) value).isEmpty())) {
                super.bindValue(ps, index, null);
            } else {
                super.bindValue(ps, index, value);
            }
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public ValueType getValueType(Class<?> clazz, boolean lob, TemporalType temporalType) {
        if (clazz == String.class) {
            return stringType;
        } else {
            return super.getValueType(clazz, lob, temporalType);
        }
    }

}

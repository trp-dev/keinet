package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;


/**
 *
 * 志望大学CSVの出力情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainB0201Bean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private String grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** カナ氏名 */
    private String nameKana;

    /** 模試コード */
    private String examCd;

    /** 模試名 */
    private String examName;

    /** 大学コード10桁 */
    private String univKey;

    /** 大学コード5桁 */
    private String choiceCd5;

    /** 大学名 */
    private String univName;

    /** 学部名 */
    private String facultyName;

    /** 学科名 */
    private String deptName;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public String getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * カナ氏名を返します。
     *
     * @return カナ氏名
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * カナ氏名をセットします。
     *
     * @param nameKana カナ氏名
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 模試名を返します。
     *
     * @return 模試名
     */
    public String getExamName() {
        return examName;
    }

    /**
     * 模試名をセットします。
     *
     * @param examName 模試名
     */
    public void setExamName(String examName) {
        this.examName = examName;
    }

    /**
     * 大学コード5桁を返します。
     *
     * @return 大学コード5桁
     */
    public String getChoiceCd5() {
        return choiceCd5;
    }

    /**
     * 大学コード5桁をセットします。
     *
     * @param choiceCd5 大学コード5桁
     */
    public void setChoiceCd5(String choiceCd5) {
        this.choiceCd5 = choiceCd5;
    }

    /**
     * 大学コード10桁を返します。
     *
     * @return 大学コード10桁
     */
    public String getUnivKey() {
        return univKey;
    }

    /**
     * 大学コード10桁をセットします。
     *
     * @param univKey 大学コード10桁
     */
    public void setUnivKey(String univKey) {
        this.univKey = univKey;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.CandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamPlanUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivCdHookUpBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

/**
 *
 * 大学コード紐付け処理のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivCdHookUpDao extends BaseDao {

    /**
     * [志望大学] 大学コード紐付け処理対象リストを取得します。
     *
     * @param systemDto システム情報
     * @return {@link UnivCdHookUpBean}のリスト
     */
    public List<UnivCdHookUpBean> selectUnivCdHookUpList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(UnivCdHookUpBean.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * [受験予定大学] 大学コード紐付け処理対象リストを取得します。
     *
     * @param systemDto システム情報
     * @return 個人IDリスト
     */
    public List<Integer> selectUnivCdHookUpExamPlanUnivList(SystemDto systemDto) {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), systemDto).getResultList();
    }

    /**
     * 志望大学リストを取得します。
     *
     * @param bean {@link UnivCdHookUpBean}
     * @return 志望大学リスト
     */
    public List<CandidateUnivBean> selectCandidateUnivList(UnivCdHookUpBean bean) {
        return jdbcManager.selectBySqlFile(CandidateUnivBean.class, getSqlPath(), bean).getResultList();
    }

    /**
     * 志望大学リストを削除します。
     *
     * @param bean {@link UnivCdHookUpBean}
     */
    public void deleteCandidateUniv(UnivCdHookUpBean bean) {
        jdbcManager.updateBySqlFile(getSqlPath(), bean).execute();
    }

    /**
     * 志望大学リストを登録します。
     *
     * @param list 志望大学リスト
     */
    public void insertCandidateUniv(List<CandidateUnivBean> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * 受験予定大学リストを取得します。
     *
     * @param id 個人ID
     * @return 受験予定大学リスト
     */
    public List<ExamPlanUnivBean> selectExamPlanUnivList(Integer id) {
        return jdbcManager.selectBySqlFile(ExamPlanUnivBean.class, getSqlPath(), id).getResultList();
    }

    /**
     * 受験予定大学を削除します。
     *
     * @param id 個人ID
     */
    public void deleteExamPlanUniv(Integer id) {
        jdbcManager.updateBySqlFile(getSqlPath(), id).execute();
    }

    /**
     * 受験予定大学を登録します。
     *
     * @param list 受験予定大学リスト
     */
    public void insertExamPlanUniv(List<ExamPlanUnivBean> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * 成績データ情報の年度・模試区分を最新化します。
     *
     * @param systemDto システム情報
     */
    public void updateRecordInfo(SystemDto systemDto) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), systemDto).execute();
    }

}

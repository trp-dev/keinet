package jp.ac.kawai_juku.banzaisystem.exmntn.univ.util;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_1;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_ENG;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_JAP;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_MAT;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_SCI;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_SOC;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONETWO;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHTWO;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistNumbersBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamSubjectUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 帳票に関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnUnivExcelUtil {

    /** インデックスrow：模試成績（科目名→得点） */
    private static final int IDX_ROW_EXAM_SCORE = 1;

    /** インデックスrow：模試成績（科目名→換算得点） */
    private static final int IDX_ROW_EXAM_CVSSCORE = 2;

    /** インデックスrow：模試成績（科目名→偏差値） */
    private static final int IDX_ROW_EXAM_DEVIATION = 3;

    /** インデックスcell：模試成績（１科目あたり列数） */
    private static final int IDX_CELL_EXAM_NUM = 3;

    /** インデックスcell：模試成績（英語） */
    private static final int IDX_CELL_EXAM_ENG = 3;

    /** インデックスcell：模試成績（数学） */
    /* private static final int IDX_CELL_EXAM_MAT = IDX_CELL_EXAM_ENG + IDX_CELL_EXAM_NUM * 2; */
    private static final int IDX_CELL_EXAM_MAT = IDX_CELL_EXAM_ENG + IDX_CELL_EXAM_NUM * 3;

    /** インデックスcell：模試成績（国語） */
    private static final int IDX_CELL_EXAM_JAP = IDX_CELL_EXAM_MAT + IDX_CELL_EXAM_NUM * 3;

    /** インデックスcell：模試成績（理科） */
    private static final int IDX_CELL_EXAM_SCI = IDX_CELL_EXAM_JAP + IDX_CELL_EXAM_NUM;

    /** インデックスcell：模試成績（地歴公民） */
    private static final int IDX_CELL_EXAM_SOC = IDX_CELL_EXAM_SCI + IDX_CELL_EXAM_NUM * 3;

    /** １目盛りあたりの人数の表示フォーマット */
    private static final String FMT_SCALE = "□=%s人";

    /** 満点ラベルの表示フォーマット */
    private static final String FMT_FULLPNT = "満点:%s";

    /** Excel生成サービス */
    private final BaseExcelMaker<?> excel;

    /** システム情報DTO */
    private final SystemDto systemDto;

    /** マーク成績リスト */
    private List<SubjectRecord> markScoreList;

    /** 記述成績リスト */
    private List<SubjectRecord> writtenScoreList;

    /**
     * コンストラクタです。
     *
     * @param excel Excel生成サービス
     */
    public ExmntnUnivExcelUtil(BaseExcelMaker<?> excel) {
        this.excel = excel;
        this.systemDto = getComponent(SystemDto.class);
    }

    /**
     * 個人成績一覧を出力します。
     *
     * @param targetExam 対象模試
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @param exam1Position 対象模試（上段）のセル位置
     */
    /*
    public void outputSubRecord(ExamBean targetExam, ExamBean markExam, ExamBean writtenExam, String exam1Position) {
        outputEngSubRecord(targetExam, markExam, writtenExam, exam1Position, false);
    }
    */

    /**
     * 個人成績一覧を出力します。
     *
     * @param targetExam 対象模試
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @param exam1Position 対象模試（上段）のセル位置
     */
    public void outputSubRecord(ExamBean targetExam, ExamBean markExam, ExamBean writtenExam, String exam1Position) {

        /* 対象模試（上段） */
        ExamBean exam1 = targetExam;

        /* 対象模試（下段） */
        ExamBean exam2 = ExamUtil.isMark(targetExam) ? writtenExam : markExam;

        /* 対象模試（上段）のセル */
        HSSFCell exam1Cell = excel.getCell(exam1Position);

        /* 対象模試（下段）のセル */
        HSSFCell exam2Cell = excel.getCell(exam1Cell.getRowIndex() + 5, exam1Cell.getColumnIndex());

        /* 上段に成績を出力する */
        /* 模試名を出力する */
        excel.setCellValue(exam1Cell.getRowIndex(), exam1Cell.getColumnIndex(), exam1.toString());
        /* 成績を出力する */
        int row = exam1Cell.getRowIndex() + 1;
        if (ExamUtil.isMark(exam1)) {
            /* outputSubRecordMark(row, new ExamSubjectUtil(systemDto, exam1), exam1, engFlg); */
            outputSubRecordMark(row, new ExamSubjectUtil(systemDto, exam1), exam1);
        } else {
            outputSubRecordWrite(row, new ExamSubjectUtil(systemDto, exam1));
        }

        /* 下段に成績を出力する */
        if (exam2 != null) {
            /* 模試名を出力する */
            excel.setCellValue(exam2Cell.getRowIndex(), exam2Cell.getColumnIndex(), exam2.toString());
            /* 成績を出力する */
            row = exam2Cell.getRowIndex() + 1;
            if (ExamUtil.isMark(exam2)) {
                /* outputSubRecordMark(row, new ExamSubjectUtil(systemDto, exam2), exam2, engFlg); */
                outputSubRecordMark(row, new ExamSubjectUtil(systemDto, exam2), exam2);
            } else {
                outputSubRecordWrite(row, new ExamSubjectUtil(systemDto, exam2));
            }
        }
    }

    /**
     * マーク模試成績データを出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param util {@link ExamSubjectUtil}
     * @param exam 対象模試
     */
    private void outputSubRecordMark(int row, ExamSubjectUtil util, ExamBean exam) {

        /* String strWk = "";
        
        if (engFlg) {
            engCellplus = IDX_CELL_EXAM_NUM;
        }
        */

        /* 英語 */
        int cntSubEng = 0;
        for (BZSubjectRecord rec : findMarkRecordList(COURSE_ENG)) {
            outputSubRecordData(row, IDX_CELL_EXAM_ENG + (IDX_CELL_EXAM_NUM * cntSubEng++), rec, util);
        }

        /* if (engFlg) { */
        /* 英語(CEFR) */
        /* if (systemDto.getCefrInfo().get(0).getCefrLevelCd1().equals("") && systemDto.getCefrInfo().get(0).getCefrLevelCd2().equals("")) { */
        /* 空の場合はなにもしない */
        /* } else { */
        /* CEFRレベルの低い方(評価が高い方を値に設定) */
        /*
                    if (((systemDto.getCefrChangeInfo().get(0).getCefrLevelCd1().compareTo(systemDto.getCefrChangeInfo().get(0).getCefrLevelCd2()) <= 0)
                        && !((systemDto.getCefrChangeInfo().get(0).getCefrLevelCd1() == null) || (systemDto.getCefrChangeInfo().get(0).getCefrLevelCd1().equals(""))))
                        || ((systemDto.getCefrChangeInfo().get(0).getCefrLevelCd2() == null) || (systemDto.getCefrChangeInfo().get(0).getCefrLevelCd2().equals("")))) {
                    if (systemDto.getCefrChangeInfo().get(0).getCefrLevelCdCorrectFlg1().equals("1")) {
                        strWk = Constants.ASTERISK + systemDto.getCefrChangeInfo().get(0).getCefrLevelAbbr1();
                    } else {
                        strWk = systemDto.getCefrChangeInfo().get(0).getCefrLevelAbbr1();
                    }
                } else {
                    if (systemDto.getCefrChangeInfo().get(0).getCefrLevelCdCorrectFlg2().equals("1")) {
                        strWk = Constants.ASTERISK + systemDto.getCefrChangeInfo().get(0).getCefrLevelAbbr2();
                    } else {
                        strWk = systemDto.getCefrChangeInfo().get(0).getCefrLevelAbbr2();
                    }
                }
                setMarkCefrScore(row, IDX_CELL_EXAM_ENG + (IDX_CELL_EXAM_NUM * (cntSubEng - 1)), strWk);
                */
        /* 中央寄せにする */
        /*
                excel.getCell(row + IDX_ROW_EXAM_SCORE, IDX_CELL_EXAM_ENG + (IDX_CELL_EXAM_NUM * (cntSubEng - 1))).setCellStyle(cellCenter);
            }
        }
        */
        /* 数学 */
        int cntSubMat = 0;
        for (BZSubjectRecord rec : findMarkRecordList(COURSE_MAT)) {
            outputSubRecordData(row, IDX_CELL_EXAM_MAT + (IDX_CELL_EXAM_NUM * cntSubMat++), rec, util);
        }

        /* 国語の集計科目のマークの成績を出力する */
        BZSubjectRecord japanese = (BZSubjectRecord) findMarkRecord(SUBCODE_CENTER_DEFAULT_JAPANESE);
        if (japanese != null) {
            outputSubRecordData(row, IDX_CELL_EXAM_JAP, japanese, util);
        }

        /* 国語の記述の成績を出力する */
        /*
        BZSubjectRecord japanese2 = (BZSubjectRecord) findMarkRecord(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
        if (japanese2 != null) {
            setMarkJpnScore(row, IDX_CELL_EXAM_JAP2 + engCellplus, japanese2, util);
            /* 中央寄せにする
        excel.getCell(row + IDX_ROW_EXAM_SCORE, IDX_CELL_EXAM_JAP2 + engCellplus).setCellStyle(cellCenter);
        }*/

        /* 理科 */
        int cntSubSci = 0;
        for (BZSubjectRecord rec : findMarkRecordList(COURSE_SCI)) {
            if (cntSubSci == 0 && util.isBasic(rec.getSubCd())) {
                /* 1科目めが基礎科目なら、1列目は空欄にする */
                cntSubSci++;
            }
            outputSubRecordData(row, IDX_CELL_EXAM_SCI + (IDX_CELL_EXAM_NUM * cntSubSci++), rec, util);
        }

        /* 地歴公民 */
        int cntSubSoc = 0;
        for (BZSubjectRecord rec : findMarkRecordList(COURSE_SOC)) {
            outputSubRecordData(row, IDX_CELL_EXAM_SOC + (IDX_CELL_EXAM_NUM * cntSubSoc++), rec, util);
        }
    }

    /**
     * 国語記述成績を設定します。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param rec 成績データ
     * @param util {@link ExamSubjectUtil}
     */
    /*
    public void setMarkJpnScore(int row, int cell, BZSubjectRecord rec, ExamSubjectUtil util) {
        String subName = util.getSubName(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
    
        /* 値がない場合は表示しない
        if ("".equals(rec.getJpnHyoka1()) || "".equals(rec.getJpnHyoka2()) || "".equals(rec.getJpnHyoka3()) || "".equals(rec.getJpnHyokaAll()) || rec.getJpnHyoka1() == null || rec.getJpnHyoka2() == null || rec.getJpnHyoka3() == null) {
            return;
        }
        /* 科目名を出力する
        excel.setCellValue(row, cell, subName);
        /* 得点を出力する
        excel.setCellValue(row + IDX_ROW_EXAM_SCORE, cell, rec.getJpnHyoka1() + rec.getJpnHyoka2() + rec.getJpnHyoka3() + rec.getJpnHyokaAll());
    }
    */

    /**
     * CEFR成績を設定します。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param rec 成績データ
     */
    /* public void setMarkCefrScore(int row, int cell, String rec) { */
    /* 科目名を出力する */
    /* excel.setCellValue(row, cell, "ＣＥＦＲ"); */
    /* 得点を出力する */
    /* excel.setCellValue(row + IDX_ROW_EXAM_SCORE, cell, rec); */
    /* } */

    /**
     * マーク成績リストから指定した教科コードの成績を探します。
     *
     * @param courseCd 教科コード
     * @return {@link BZSubjectRecord}
     */
    private List<BZSubjectRecord> findMarkRecordList(String courseCd) {
        return findRecordList(markScoreList, courseCd);
    }

    /**
     * 記述成績リストから指定した教科コードの成績を探します。
     *
     * @param courseCd 教科コード
     * @return {@link BZSubjectRecord}
     */
    private List<BZSubjectRecord> findWrittenRecordList(String courseCd) {
        return findRecordList(writtenScoreList, courseCd);
    }

    /**
     * 成績リストから指定した教科コードの成績を探します。
     *
     * @param list 成績リスト
     * @param courseCd 教科コード
     * @return {@link BZSubjectRecord}
     */
    private List<BZSubjectRecord> findRecordList(List<?> list, String courseCd) {
        List<BZSubjectRecord> l = CollectionsUtil.newArrayList();
        if (list != null) {
            for (Object obj : list) {
                if (obj instanceof BZSubjectRecord) {
                    BZSubjectRecord rec = (BZSubjectRecord) obj;
                    if (rec.getCourseCd().equals(courseCd)) {
                        l.add(rec);
                    }
                }
            }
        }
        return l;
    }

    /**
     * マーク成績リストから指定した科目コードの成績を探します。
     *
     * @param subCd 科目コード
     * @return {@link BZSubjectRecord}
     */
    private SubjectRecord findMarkRecord(String subCd) {
        if (markScoreList != null) {
            for (Object obj : markScoreList) {
                SubjectRecord rec = (SubjectRecord) obj;
                if (rec.getSubCd().equals(subCd)) {
                    return rec;
                }
            }
        }
        return null;
    }

    /**
     * 記述模試成績データを出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param util {@link ExamSubjectUtil}
     */
    private void outputSubRecordWrite(int row, ExamSubjectUtil util) {

        /* 英語 */
        int cntSubEng = 0;
        for (BZSubjectRecord rec : findWrittenRecordList(COURSE_ENG)) {
            outputSubRecordData(row, IDX_CELL_EXAM_ENG + (IDX_CELL_EXAM_NUM * cntSubEng++), rec, util);
        }

        /* 数学 */
        int cntSubMat = 0;
        for (BZSubjectRecord rec : findWrittenRecordList(COURSE_MAT)) {
            outputSubRecordData(row, IDX_CELL_EXAM_MAT + (IDX_CELL_EXAM_NUM * cntSubMat++), rec, util);
        }

        /* 国語 */
        int cntSubJap = 0;
        for (BZSubjectRecord rec : findWrittenRecordList(COURSE_JAP)) {
            outputSubRecordData(row, IDX_CELL_EXAM_JAP + (IDX_CELL_EXAM_NUM * cntSubJap++), rec, util);
        }

        /* 理科 */
        int cntSubSci = 0;
        for (BZSubjectRecord rec : findWrittenRecordList(COURSE_SCI)) {
            if (cntSubSci == 0 && util.isBasic(rec.getSubCd())) {
                /* 1科目めが基礎科目なら、1列目は空欄にする */
                cntSubSci++;
            }
            outputSubRecordData(row, IDX_CELL_EXAM_SCI + (IDX_CELL_EXAM_NUM * cntSubSci++), rec, util);
        }

        /* 地歴公民 */
        int cntSubSoc = 0;
        for (BZSubjectRecord rec : findWrittenRecordList(COURSE_SOC)) {
            outputSubRecordData(row, IDX_CELL_EXAM_SOC + (IDX_CELL_EXAM_NUM * cntSubSoc++), rec, util);
        }
    }

    /**
     * 成績データ（得点・偏差値）を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param rec 成績データ
     * @param util {@link ExamSubjectUtil}
     */
    private void outputSubRecordData(int row, int cell, BZSubjectRecord rec, ExamSubjectUtil util) {
        String subName = util.getSubName(rec.getSubCd());
        /* 英語＋Ｌは出力しない */
        /* if (subName != null && !"1190".equals(rec.getSubCd())) { */
        if (subName != null) {
            /* 科目名を出力する */
            excel.setCellValue(row, cell, subName);
            /* 得点を出力する */
            excel.setCellValue(row + IDX_ROW_EXAM_SCORE, cell, rec.getScore());
            /* 換算得点を出力する */
            excel.setCellValue(row + IDX_ROW_EXAM_CVSSCORE, cell, rec.getCvsScore());
            /* 偏差値を出力する */
            excel.setCellValue(row + IDX_ROW_EXAM_DEVIATION, cell, rec.getCDeviation());
        }
    }

    /**
     * 私大評価用偏差値を出力します。
     *
     * @param mathOnePosition 数学(1科目目)のセル位置
     */
    public void outputPrivateUnivDeviation(String mathOnePosition) {

        /* 数学(1科目) */
        HSSFCell mathOneCell = excel.getCell(mathOnePosition);
        SubjectRecord recMathOne = findMarkRecord(SUBCODE_CENTER_DEFAULT_MATHONE);
        if (recMathOne != null) {
            excel.setCellValue(mathOneCell.getRowIndex(), mathOneCell.getColumnIndex(), recMathOne.getCDeviation());
        }

        /* 数学(2科目) */
        SubjectRecord recMathTwo = ObjectUtils.defaultIfNull(findMarkRecord(SUBCODE_CENTER_DEFAULT_MATHONETWO), findMarkRecord(SUBCODE_CENTER_DEFAULT_MATHTWO));
        if (recMathTwo != null) {
            excel.setCellValue(mathOneCell.getRowIndex() + 1, mathOneCell.getColumnIndex(), recMathTwo.getCDeviation());
        }

        /* 国語(現代文) */
        SubjectRecord recLiterature = findMarkRecord(SUBCODE_CENTER_DEFAULT_LITERATURE);
        if (recLiterature != null) {
            excel.setCellValue(mathOneCell.getRowIndex() + 2, mathOneCell.getColumnIndex(), recLiterature.getCDeviation());
        }

        /* 国語(現古) */
        SubjectRecord recLiteratureOld = findMarkRecord(SUBCODE_CENTER_DEFAULT_LITERATURE_OLD);
        if (recLiteratureOld != null) {
            excel.setCellValue(mathOneCell.getRowIndex() + 3, mathOneCell.getColumnIndex(), recLiteratureOld.getCDeviation());
        }

        /* 国語(現古漢) */
        SubjectRecord recJapanese = findMarkRecord(SUBCODE_CENTER_DEFAULT_JAPANESE);
        if (recJapanese != null) {
            excel.setCellValue(mathOneCell.getRowIndex() + 4, mathOneCell.getColumnIndex(), recJapanese.getCDeviation());
        }
    }

    /**
     * 成績データをセットします。
     *
     * @param scoreBean 成績データ
     */
    public void setScoreBean(ScoreBean scoreBean) {
        this.markScoreList = createMarkScoreList(scoreBean);
        this.writtenScoreList = createWrittenScoreList(scoreBean);
    }

    /**
     * マーク成績リストを生成します。
     *
     * @param scoreBean {@link ScoreBean}
     * @return マーク成績リスト
     */
    private List<SubjectRecord> createMarkScoreList(final ScoreBean scoreBean) {
        List<SubjectRecord> list = toSubjectRecordList(scoreBean.getScore().getMark());
        if (list != null) {
            /* 第一解答科目→第二解答科目→理科基礎科目となるように並べ替える */
            final ExamSubjectUtil util = new ExamSubjectUtil(systemDto, scoreBean.getMarkExam());
            Collections.sort(list, new Comparator<SubjectRecord>() {
                @Override
                public int compare(SubjectRecord o1, SubjectRecord o2) {

                    /* 第一解答科目は先頭にする */
                    if (ANSWER_NO_1.equals(o1.getScope()) && !ANSWER_NO_1.equals(o2.getScope())) {
                        return -1;
                    } else if (!ANSWER_NO_1.equals(o1.getScope()) && ANSWER_NO_1.equals(o2.getScope())) {
                        return 1;
                    }

                    /* 理科基礎科目は最後にする */
                    boolean isBasic1 = util.isBasic(o1.getSubCd());
                    boolean isBasic2 = util.isBasic(o2.getSubCd());
                    if (isBasic1 && !isBasic2) {
                        return 1;
                    } else if (!isBasic1 && isBasic2) {
                        return -1;
                    }

                    /* 科目の表示順で評価する */
                    String seq1 = util.getDispSequence(o1.getSubCd());
                    String seq2 = util.getDispSequence(o2.getSubCd());

                    return seq1.compareTo(seq2);
                }
            });
        }
        return list;
    }

    /**
     * 記述成績リストを生成します。
     *
     * @param scoreBean {@link ScoreBean}
     * @return 記述成績リスト
     */
    private List<SubjectRecord> createWrittenScoreList(final ScoreBean scoreBean) {
        List<SubjectRecord> list = toSubjectRecordList(scoreBean.getScore().getWrtn());
        if (list != null) {
            /* その他科目→理科基礎科目となるように並べ替える */
            final ExamSubjectUtil util = new ExamSubjectUtil(systemDto, scoreBean.getWrittenExam());
            Collections.sort(list, new Comparator<SubjectRecord>() {
                @Override
                public int compare(SubjectRecord o1, SubjectRecord o2) {

                    /* 理科基礎科目は最後にする */
                    boolean isBasic1 = util.isBasic(o1.getSubCd());
                    boolean isBasic2 = util.isBasic(o2.getSubCd());
                    if (isBasic1 && !isBasic2) {
                        return 1;
                    } else if (!isBasic1 && isBasic2) {
                        return -1;
                    }

                    /* 科目の表示順で評価する */
                    String seq1 = util.getDispSequence(o1.getSubCd());
                    String seq2 = util.getDispSequence(o2.getSubCd());

                    return seq1.compareTo(seq2);
                }
            });
        }
        return list;
    }

    /**
     * 型が不明な成績リストを {@link SubjectRecord} のリストに変換します。
     *
     * @param list 成績リスト
     * @return {@link SubjectRecord} のリスト
     */
    private List<SubjectRecord> toSubjectRecordList(List<?> list) {
        if (list == null) {
            return null;
        } else {
            List<SubjectRecord> l = new ArrayList<>(list.size());
            for (Object obj : list) {
                l.add((SubjectRecord) obj);
            }
            return l;
        }
    }

    /**
     * 学力分布グラフを出力します。
     *
     * @param distBean {@link ExmntnDistBean}
     * @param distTargetExam 学力分布の対象模試
     * @param scoreCell 偏差値／換算得点ラベルのセル
     */
    public void outputDistributionGraph(ExmntnDistBean distBean, ExamBean distTargetExam, HSSFCell scoreCell) {

        HSSFCell graphCell = excel.getCell(scoreCell.getRowIndex(), scoreCell.getColumnIndex() + 2);

        /* ヘッダラベルを出力する */
        if (distBean.isDeviationDistFlg() || ExamUtil.isWritten(distTargetExam)) {
            /* 偏差値帯の場合 */
            excel.setCellValue(scoreCell, getMessage("label.exmntn.univ.excel.deviation"));
            excel.setCellValue(graphCell, getMessage("label.exmntn.dist.deviationGraph"));
        } else {
            /* 得点帯の場合 */
            if (ExamUtil.isCenter(distTargetExam)) {
                excel.setCellValue(scoreCell, getMessage("label.exmntn.univ.excel.point"));
                excel.setCellValue(graphCell, getMessage("label.exmntn.dist.scoreGraphC"));
            } else {
                excel.setCellValue(scoreCell, getMessage("label.exmntn.univ.excel.cvsPoint"));
                excel.setCellValue(graphCell, getMessage("label.exmntn.dist.scoreGraph"));
            }
        }

        if (distBean.getPatternCd() == null) {
            /* 志望パターンが無ければここまで */
            return;
        }

        /* 満点を出力する */
        if (distBean.getFullPnt() != null) {
            excel.setCellValue(scoreCell.getRowIndex() + 2, scoreCell.getColumnIndex(), String.format(FMT_FULLPNT, distBean.getFullPnt()));
        }

        /* １目盛りあたりの人数を出力する */
        excel.setCellValue(scoreCell.getRowIndex() + 2, scoreCell.getColumnIndex() + 2, String.format(FMT_SCALE, distBean.getScale()));

        /* ラベル：人数を出力する */
        excel.setCellValue(scoreCell.getRowIndex() + 2, scoreCell.getColumnIndex() + 10, getMessage("label.exmntn.univ.excel.number"));

        /* ラベル：累計を出力する */
        excel.setCellValue(scoreCell.getRowIndex() + 2, scoreCell.getColumnIndex() + 12, getMessage("label.exmntn.univ.excel.total"));

        /* ボーダを出力する */
        excel.setCellValue(scoreCell.getRowIndex() + 2, scoreCell.getColumnIndex() + 14, distBean.getBorder());

        for (int i = 0; i < distBean.getNumbersList().size(); i++) {
            ExmntnDistNumbersBean bean = distBean.getNumbersList().get(i);
            HSSFCell headerCell = excel.getCell(scoreCell.getRowIndex() + 4 + i, scoreCell.getColumnIndex());
            /* 帯ヘッダを出力する */
            excel.setCellValue(headerCell, bean.getDistHeader());
            /* 目盛りを出力する */
            excel.setCellValue(headerCell.getRowIndex(), headerCell.getColumnIndex() + 2, StringUtils.repeat(bean.isEmphasisFlg() ? '■' : '□', bean.getScaleNum()));
            /* 人数を出力する */
            excel.setCellValue(headerCell.getRowIndex(), headerCell.getColumnIndex() + 10, bean.getNumbers());
            /* 累計を出力する */
            excel.setCellValue(headerCell.getRowIndex(), headerCell.getColumnIndex() + 12, bean.getTotal());
            /* 評価基準を出力する */
            if (!StringUtils.isEmpty(bean.getStandard())) {
                excel.setCellValue(headerCell.getRowIndex(), headerCell.getColumnIndex() + 14, bean.getStandard());
            }
        }
    }

    /**
     * 学力分布の対象模試を判定します。
     *
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @param scheduleCd 日程コード
     * @return 学力分布の対象模試
     */
    public ExamBean judgeDistTargetExam(ExamBean markExam, ExamBean writtenExam, String scheduleCd) {
        if (Univ.SCHEDULE_NML.equals(scheduleCd)) {
            /* 一般私大は記述模試を優先する */
            return ObjectUtils.firstNonNull(writtenExam, markExam);
        } else {
            /* 一般私大以外は対象模試を返す */
            return ExamUtil.judgeTargetExam(markExam, writtenExam);
        }
    }

}

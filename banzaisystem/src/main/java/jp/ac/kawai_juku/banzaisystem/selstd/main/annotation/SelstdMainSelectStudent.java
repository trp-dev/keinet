package jp.ac.kawai_juku.banzaisystem.selstd.main.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.seasar.framework.aop.annotation.Interceptor;

/**
 *
 * 対象生徒選択画面で、対象生徒選択処理を行う<br>
 * アクションメソッドに指定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Interceptor
public @interface SelstdMainSelectStudent {
}

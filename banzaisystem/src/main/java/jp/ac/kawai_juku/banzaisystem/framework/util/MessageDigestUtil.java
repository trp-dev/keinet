package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;

import org.seasar.framework.util.FileInputStreamUtil;

/**
 *
 * {@link MessageDigest}に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class MessageDigestUtil {

    /** MD5 */
    private static final String ALGORITHM_SHA_256 = "SHA-256";

    /**
     * コンストラクタです。
     */
    private MessageDigestUtil() {
    }

    /**
     * ファイルのSHA-256ハッシュ値を計算します。
     *
     * @param file ファイル
     * @return ハッシュ値
     */
    public static byte[] sha256(File file) {
        return digest(file, ALGORITHM_SHA_256);
    }

    /**
     * 文字列のSHA-256ハッシュ値を計算します。
     *
     * @param str 文字列
     * @return ハッシュ値
     */
    public static byte[] sha256(String str) {
        return digest(new ByteArrayInputStream(str.getBytes()), ALGORITHM_SHA_256);
    }

    /**
     * ファイルのハッシュ値を計算します。
     *
     * @param file ファイル
     * @param algorithm アルゴリズム
     * @return ハッシュ値
     */
    private static byte[] digest(File file, String algorithm) {
        return digest(new BufferedInputStream(FileInputStreamUtil.create(file)), algorithm);
    }

    /**
     * 入力ストリームのハッシュ値を計算します。
     *
     * @param in 入力ストリーム
     * @param algorithm アルゴリズム
     * @return ハッシュ値
     */
    private static byte[] digest(InputStream in, String algorithm) {

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(algorithm);
            byte[] buff = new byte[1024 * 8];
            int len = -1;
            while ((len = in.read(buff)) > 0) {
                digest.update(buff, 0, len);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            IOUtil.close(in);
        }

        return digest.digest();
    }

}

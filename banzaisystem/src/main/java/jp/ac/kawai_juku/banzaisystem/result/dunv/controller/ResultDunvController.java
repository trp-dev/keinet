package jp.ac.kawai_juku.banzaisystem.result.dunv.controller;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.result.dunv.service.ResultDunvService;
import jp.ac.kawai_juku.banzaisystem.result.dunv.view.ResultDunvView;

/**
 *
 * 大学選択ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultDunvController extends AbstractController {

    /** View */
    @Resource(name = "resultDunvView")
    private ResultDunvView view;

    /** Service */
    @Resource(name = "resultDunvService")
    private ResultDunvService service;

    /**
     * 初期表示アクションです。
     *
     * @return 大学検索ダイアログ画面
     */
    @Execute
    public ExecuteResult index() {

        return VIEW_RESULT;
    }

    /**
     * 検索開始ボタンアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult startButtonAction() {
        view.univList.setList(service.findUnivList(view.univNameField.getText()));
        return null;
    }

    /**
     * 大学リストアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult univListAction() {
        List<LabelValueBean> list = view.univList.getSelectedList();
        if (list.isEmpty()) {
            view.facultyList.setList(Collections.<LabelValueBean> emptyList());
        } else {
            view.facultyList.setList(service.findFacultyList(list.get(0).getValue()));
        }
        return null;
    }

    /**
     * 学部リストアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult facultyListAction() {
        List<LabelValueBean> univList = view.univList.getSelectedList();
        List<LabelValueBean> facultyList = view.facultyList.getSelectedList();
        if (univList.isEmpty() || facultyList.isEmpty()) {
            view.deptList.setList(Collections.<LabelValueBean> emptyList());
        } else {
            view.deptList.setList(service.findDeptList(univList.get(0).getValue(), facultyList.get(0).getValue()));
        }
        return null;
    }

    /**
     * 学科リストアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult deptListAction() {
        view.okButton.setEnabled(!view.deptList.getSelectedList().isEmpty());
        return null;
    }

    /**
     * OKボタンアクションです。
     *
     * @return ダイアログを閉じる
     */
    @Execute
    public ExecuteResult okButtonAction() {
        /*
         * List<LabelValueBean> univList = view.univList.getSelectedList();
         * List<LabelValueBean> facultyList = view.facultyList.getSelectedList();
         * List<LabelValueBean> deptList = view.deptList.getSelectedList();
         */
        return CLOSE_RESULT;
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return ダイアログを閉じる
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        return CLOSE_RESULT;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * CSV帳票メーカーの入力Beanインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface CsvMakerInBean extends FormMakerInBean {
}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

/**
 *
 * 志望大学詳細画面 二次試験情報Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
public class ExmntnUnivDetailSecondSvcOutBean {

    /** 採用枝番 */
    private String branchCd;

    /** 評価 */
    private String rating;

    /** 評価偏差値 */
    private String dev;

    /** 満点 */
    private String fullPoint;

    /** ボーダランク */
    private String borderRank;

    /** 評価基準-A */
    private String lineA;

    /** 評価基準-B */
    private String lineB;

    /** 評価基準-C */
    private String lineC;

    /** 評価基準-D */
    private String lineD;

    /** 判定科目 */
    private String subCount;

    /** 配点-教科配点(英語) */
    private String allotEnglish;

    /** 配点-教科配点(数学) */
    private String allotMath;

    /** 配点-教科配点(国語) */
    private String allotJap;

    /** 配点-教科配点(理科) */
    private String allotSci;

    /** 配点-教科配点(社会) */
    private String allotSoc;

    /** その他-配点 */
    private String otherAllot;

    /** 偏差値-英語 */
    private String devEnglish;

    /** 偏差値-数学 */
    private String devMath;

    /** 偏差値-国語 */
    private String devJap;

    /** 偏差値-理科1 */
    private String devSci1;

    /** 偏差値-理科2 */
    private String devSci2;

    /** 偏差値-社会 */
    private String devSoc1;

    /** 偏差値-社会 */
    private String devSoc2;

    /** 備考 */
    private String note;

    /** あと何点-判定 */
    private String remainRaiting;

    /** あと何点-偏差値 */
    private String remainDeviation;

    /** 判定結果有無 */
    private boolean isJudged;

    /**
     * 評価を返します。
     *
     * @return 評価
     */
    public String getRating() {
        return rating;
    }

    /**
     * 評価をセットします。
     *
     * @param rating 評価
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 評価偏差値を返します。
     *
     * @return 評価偏差値
     */
    public String getDev() {
        return dev;
    }

    /**
     * 評価偏差値をセットします。
     *
     * @param dev 評価偏差値
     */
    public void setDev(String dev) {
        this.dev = dev;
    }

    /**
     * 満点を返します。
     *
     * @return 満点
     */
    public String getFullPoint() {
        return fullPoint;
    }

    /**
     * 満点をセットします。
     *
     * @param fullPoint 満点
     */
    public void setFullPoint(String fullPoint) {
        this.fullPoint = fullPoint;
    }

    /**
     * ボーダランクを返します。
     *
     * @return ボーダランク
     */
    public String getBorderRank() {
        return borderRank;
    }

    /**
     * ボーダランクをセットします。
     *
     * @param borderRank ボーダランク
     */
    public void setBorderRank(String borderRank) {
        this.borderRank = borderRank;
    }

    /**
     * 評価基準-Aを返します。
     *
     * @return 評価基準-A
     */
    public String getLineA() {
        return lineA;
    }

    /**
     * 評価基準-Aをセットします。
     *
     * @param lineA 評価基準-A
     */
    public void setLineA(String lineA) {
        this.lineA = lineA;
    }

    /**
     * 評価基準-Bを返します。
     *
     * @return 評価基準-B
     */
    public String getLineB() {
        return lineB;
    }

    /**
     * 評価基準-Bをセットします。
     *
     * @param lineB 評価基準-B
     */
    public void setLineB(String lineB) {
        this.lineB = lineB;
    }

    /**
     * 評価基準-Cを返します。
     *
     * @return 評価基準-C
     */
    public String getLineC() {
        return lineC;
    }

    /**
     * 評価基準-Cをセットします。
     *
     * @param lineC 評価基準-C
     */
    public void setLineC(String lineC) {
        this.lineC = lineC;
    }

    /**
     * 評価基準-Dを返します。
     *
     * @return 評価基準-D
     */
    public String getLineD() {
        return lineD;
    }

    /**
     * 評価基準-Dをセットします。
     *
     * @param lineD 評価基準-D
     */
    public void setLineD(String lineD) {
        this.lineD = lineD;
    }

    /**
     * 配点-教科配点(英語)を返します。
     *
     * @return 配点-教科配点(英語)
     */
    public String getAllotEnglish() {
        return allotEnglish;
    }

    /**
     * 配点-教科配点(英語)をセットします。
     *
     * @param allotEnglish 配点-教科配点(英語)
     */
    public void setAllotEnglish(String allotEnglish) {
        this.allotEnglish = allotEnglish;
    }

    /**
     * 配点-教科配点(数学)を返します。
     *
     * @return 配点-教科配点(数学)
     */
    public String getAllotMath() {
        return allotMath;
    }

    /**
     * 配点-教科配点(数学)をセットします。
     *
     * @param allotMath 配点-教科配点(数学)
     */
    public void setAllotMath(String allotMath) {
        this.allotMath = allotMath;
    }

    /**
     * 配点-教科配点(国語)を返します。
     *
     * @return 配点-教科配点(国語)
     */
    public String getAllotJap() {
        return allotJap;
    }

    /**
     * 配点-教科配点(国語)をセットします。
     *
     * @param allotJap 配点-教科配点(国語)
     */
    public void setAllotJap(String allotJap) {
        this.allotJap = allotJap;
    }

    /**
     * 配点-教科配点(理科)を返します。
     *
     * @return 配点-教科配点(理科)
     */
    public String getAllotSci() {
        return allotSci;
    }

    /**
     * 配点-教科配点(理科)をセットします。
     *
     * @param allotSci 配点-教科配点(理科)
     */
    public void setAllotSci(String allotSci) {
        this.allotSci = allotSci;
    }

    /**
     * 配点-教科配点(社会)を返します。
     *
     * @return 配点-教科配点(社会)
     */
    public String getAllotSoc() {
        return allotSoc;
    }

    /**
     * 配点-教科配点(社会)をセットします。
     *
     * @param allotSoc 配点-教科配点(社会)
     */
    public void setAllotSoc(String allotSoc) {
        this.allotSoc = allotSoc;
    }

    /**
     * その他-配点を返します。
     *
     * @return その他-配点
     */
    public String getOtherAllot() {
        return otherAllot;
    }

    /**
     * その他-配点をセットします。
     *
     * @param otherAllot その他-配点
     */
    public void setOtherAllot(String otherAllot) {
        this.otherAllot = otherAllot;
    }

    /**
     * 偏差値-英語を返します。
     *
     * @return 偏差値-英語
     */
    public String getDevEnglish() {
        return devEnglish;
    }

    /**
     * 偏差値-英語をセットします。
     *
     * @param devEnglish 偏差値-英語
     */
    public void setDevEnglish(String devEnglish) {
        this.devEnglish = devEnglish;
    }

    /**
     * 偏差値-数学を返します。
     *
     * @return 偏差値-数学
     */
    public String getDevMath() {
        return devMath;
    }

    /**
     * 偏差値-数学をセットします。
     *
     * @param devMath 偏差値-数学
     */
    public void setDevMath(String devMath) {
        this.devMath = devMath;
    }

    /**
     * 偏差値-国語を返します。
     *
     * @return 偏差値-国語
     */
    public String getDevJap() {
        return devJap;
    }

    /**
     * 偏差値-国語をセットします。
     *
     * @param devJap 偏差値-国語
     */
    public void setDevJap(String devJap) {
        this.devJap = devJap;
    }

    /**
     * 偏差値-理科1を返します。
     *
     * @return 偏差値-理科1
     */
    public String getDevSci1() {
        return devSci1;
    }

    /**
     * 偏差値-理科1をセットします。
     *
     * @param devSci1 偏差値-理科1
     */
    public void setDevSci1(String devSci1) {
        this.devSci1 = devSci1;
    }

    /**
     * 偏差値-理科2を返します。
     *
     * @return 偏差値-理科2
     */
    public String getDevSci2() {
        return devSci2;
    }

    /**
     * 偏差値-理科2をセットします。
     *
     * @param devSci2 偏差値-理科2
     */
    public void setDevSci2(String devSci2) {
        this.devSci2 = devSci2;
    }

    /**
     * 偏差値-社会を返します。
     *
     * @return 偏差値-社会
     */
    public String getDevSoc1() {
        return devSoc1;
    }

    /**
     * 偏差値-社会をセットします。
     *
     * @param devSoc1 偏差値-社会
     */
    public void setDevSoc1(String devSoc1) {
        this.devSoc1 = devSoc1;
    }

    /**
     * 偏差値-社会を返します。
     *
     * @return 偏差値-社会
     */
    public String getDevSoc2() {
        return devSoc2;
    }

    /**
     * 偏差値-社会をセットします。
     *
     * @param devSoc2 偏差値-社会
     */
    public void setDevSoc2(String devSoc2) {
        this.devSoc2 = devSoc2;
    }

    /**
     * 備考を返します。
     *
     * @return 備考
     */
    public String getNote() {
        return note;
    }

    /**
     * 備考をセットします。
     *
     * @param note 備考
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * あと何点-判定を返します。
     *
     * @return あと何点-判定
     */
    public String getRemainRaiting() {
        return remainRaiting;
    }

    /**
     * あと何点-判定をセットします。
     *
     * @param remainRaiting あと何点-判定
     */
    public void setRemainRaiting(String remainRaiting) {
        this.remainRaiting = remainRaiting;
    }

    /**
     * あと何点-偏差値を返します。
     *
     * @return あと何点-偏差値
     */
    public String getRemainDeviation() {
        return remainDeviation;
    }

    /**
     * あと何点-偏差値をセットします。
     *
     * @param remainDeviation あと何点-偏差値
     */
    public void setRemainDeviation(String remainDeviation) {
        this.remainDeviation = remainDeviation;
    }

    /**
     * 判定科目を返します。
     *
     * @return 判定科目
     */
    public String getSubCount() {
        return subCount;
    }

    /**
     * 判定科目をセットします。
     *
     * @param subCount 判定科目
     */
    public void setSubCount(String subCount) {
        this.subCount = subCount;
    }

    /**
     * 採用枝番を返します。
     *
     * @return 採用枝番
     */
    public String getBranchCd() {
        return branchCd;
    }

    /**
     * 採用枝番をセットします。
     *
     * @param branchCd 採用枝番
     */
    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    /**
     * isJudgedを返します。
     *
     * @return isJudged
     */
    public boolean isJudged() {
        return isJudged;
    }

    /**
     * isJudgedを設定します。
     *
     * @param isJudged isJudged
     */
    public void setJudged(boolean isJudged) {
        this.isJudged = isJudged;
    }

}

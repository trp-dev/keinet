package jp.ac.kawai_juku.banzaisystem;

import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;

/**
 *
 * データフォルダを識別するenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum DataFolder {

    /** 大学マスタ */
    UNIV_FOLDER("大学マスタ", SettingKey.UNIV_LOCAL_DIR, SettingKey.UNIV_NETWORK_DIR, BZConstants.UNIV_FILE_NAME),

    /** 成績データ */
    RECORD_FOLDER("成績データ", SettingKey.RECORD_LOCAL_DIR, SettingKey.RECORD_NETWORK_DIR, BZConstants.RECORD_FILE_NAME),

    /** 出力ファイル */
    OUTPUT_FOLDER("出力ファイル", SettingKey.OUTPUT_LOCAL_DIR, SettingKey.OUTPUT_NETWORK_DIR, null),

    /** ログファイル */
    LOG_FOLDER("ログファイル", SettingKey.LOG_LOCAL_DIR, SettingKey.LOG_NETWORK_DIR, BZConstants.LOG_FILE_NAME),

    /** 環境設定ファイル */
    CONFIG_FOLDER("環境設定ファイル", SettingKey.CONFIG_LOCAL_DIR, SettingKey.CONFIG_NETWORK_DIR, BZConstants.CONFIG_FILE_NAME);

    /** データフォルダ識別名 */
    private final String name;

    /** ローカル設定キー */
    private final SettingKey localKey;

    /** ネットワーク設定キー */
    private final SettingKey networkKey;

    /** データファイル名 */
    private final String dataFileName;

    /**
     * コンストラクタです。
     *
     * @param name データフォルダ識別名
     * @param localKey ローカル設定キー
     * @param networkKey ネットワーク設定キー
     * @param dataFileName データファイル名
     */
    DataFolder(String name, SettingKey localKey, SettingKey networkKey, String dataFileName) {
        this.name = name;
        this.localKey = localKey;
        this.networkKey = networkKey;
        this.dataFileName = dataFileName;
    }

    /**
     * ローカル設定キーを返します。
     *
     * @return 設定キー
     */
    public SettingKey getLocalKey() {
        return localKey;
    }

    /**
     * ネットワーク設定キーを返します。
     *
     * @return ネットワーク設定キー
     */
    public SettingKey getNetworkKey() {
        return networkKey;
    }

    /**
     * データファイル名を返します。
     *
     * @return データファイル名
     */
    public String getDataFileName() {
        return dataFileName;
    }

    /**
     * データフォルダ識別名を返します。
     *
     * @return データフォルダ識別名
     */
    public String getName() {
        return name;
    }

}

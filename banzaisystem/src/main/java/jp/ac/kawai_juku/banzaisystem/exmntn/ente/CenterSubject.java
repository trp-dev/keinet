package jp.ac.kawai_juku.banzaisystem.exmntn.ente;

import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;

/**
 *
 * センター試験使用科目を識別するenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum CenterSubject implements SearchSubject {

    /** 英語 */
    ENGLISH("英語", CenterDetail.KEY_EWRITING),

    /** リスニング */
    LISTENING("英リス", CenterDetail.KEY_ELISTENING),

    /** CEFR */
    /* ENGCEFR("CEFR", CenterDetail.KEY_ENGLISH_SANKA), */

    /** 数学�T */
    MATH1("数学�T", CenterDetail.KEY_MATH1),

    /** 数学ＩＡ */
    MATH1A("数学ＩＡ", CenterDetail.KEY_MATH1A),

    /** 数学�U */
    MATH2("数学�U", CenterDetail.KEY_MATH2),

    /** 数学�UＢ */
    MATH2B("数学�UＢ", CenterDetail.KEY_MATH2B),

    /** 現代文 */
    JPRESENT("現代文", CenterDetail.KEY_JPRESENT),

    /** 現代文 */
    /* JPRESENTWRITE("現代文(記)", CenterDetail.KEY_JAPANESE_KIJUTSU), */

    /** 古文 */
    JCLASSICS("古文", CenterDetail.KEY_JCLASSICS),

    /** 漢文 */
    JCHINESE("漢文", CenterDetail.KEY_JCHINESE),

    /** 物理基礎 */
    PHYSICS_BASIC("物理基礎", CenterDetail.KEY_PHYSICS_BASIC),

    /** 化学基礎 */
    CHEMISTRY_BASIC("化学基礎", CenterDetail.KEY_CHEMISTRY_BASIC),

    /** 生物基礎 */
    BIOLOGY_BASIC("生物基礎", CenterDetail.KEY_BIOLOGY_BASIC),

    /** 地学基礎 */
    EARTHSCIENCE_BASIC("地学基礎", CenterDetail.KEY_EARTHSCIENCE_BASIC),

    /** 物理 */
    PHYSICS("物理", CenterDetail.KEY_PHYSICS),

    /** 化学 */
    CHEMISTRY("化学", CenterDetail.KEY_CHEMISTRY),

    /** 生物 */
    BIOLOGY("生物", CenterDetail.KEY_BIOLOGY),

    /** 地学 */
    EARTHSCIENCE("地学", CenterDetail.KEY_EARTHSCIENCE),

    /** 日本史Ａ */
    JHISTORYA("日本史Ａ", CenterDetail.KEY_JHISTORYA),

    /** 世界史Ａ */
    WHISTORYA("世界史Ａ", CenterDetail.KEY_WHISTORYA),

    /** 地理Ａ */
    GEOGRAPHYA("地理Ａ", CenterDetail.KEY_GEOGRAPHYA),

    /** 日本史Ｂ */
    JHISTORYB("日本史Ｂ", CenterDetail.KEY_JHISTORYB),

    /** 世界史Ｂ */
    WHISTORYB("世界史Ｂ", CenterDetail.KEY_WHISTORYB),

    /** 地理Ｂ */
    GEOGRAPHYB("地理Ｂ", CenterDetail.KEY_GEOGRAPHYB),

    /** 現代社会 */
    SOCIALSTUDIES("現代社会", CenterDetail.KEY_SOCIALSTUDIES),

    /** 倫理 */
    ETHIC("倫理", CenterDetail.KEY_ETHIC),

    /** 政経 */
    POLITICS("政経", CenterDetail.KEY_POLITICS),

    /** 倫政 */
    ETHICALPOLITICS("倫政", CenterDetail.KEY_ETHICALPOLITICS);

    /** 科目名 */
    private final String name;

    /** 科目キー */
    private final SubjectKey key;

    /**
     * コンストラクタです。
     *
     * @param name 科目名
     * @param key 科目キー
     */
    CenterSubject(String name, SubjectKey key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * 科目キーを返します。
     *
     * @return 科目キー
     */
    public SubjectKey getKey() {
        return key;
    }

}

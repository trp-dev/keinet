package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 検索結果画面のユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ExmntnRsltUtil {

    /** 区分セルデータマップ */
    private static final Map<Code, CellData> UNIV_DIV_CELL_MAP;
    static {
        Map<Code, CellData> map = new HashMap<>();
        for (Code code : CodeType.UNIV_RESULT_UNIV_DIV.findCodeList()) {
            map.put(code, new CellData(code.getAliasName(), code));
        }
        UNIV_DIV_CELL_MAP = Collections.unmodifiableMap(map);
    }

    /**
     * コンストラクタです。
     */
    private ExmntnRsltUtil() {
    }

    /**
     * YYYYMMDDをM/Dに変換します。
     *
     * @param yyyymmdd YYYYMMDD
     * @return M/D
     */
    public static String toMD(String yyyymmdd) {
        if (StringUtils.isEmpty(yyyymmdd)) {
            return null;
        }
        String mm = yyyymmdd.substring(4, 6);
        String dd = yyyymmdd.substring(6, 8);
        return StringUtils.stripStart(mm, "0") + "/" + StringUtils.stripStart(dd, "0");
    }

    /**
     * 大学コードを区分セルデータに変換します。
     *
     * @param univCd 大学コード
     * @return {@link CellData}
     */
    public static CellData toDivision(String univCd) {
        return UNIV_DIV_CELL_MAP.get(toUnivDiv(univCd));
    }

    /**
     * 大学コードを大学区分に変換します。
     *
     * @param univCd 大学コード
     * @return 大学区分
     */
    public static Code toUnivDiv(String univCd) {
        int code = Integer.parseInt(univCd);
        if ((code >= 1000 && code <= 1499) || (code >= 3000 && code <= 3499)) {
            return Code.UNIV_RESULT_UNIV_DIV_NATIONAL;
        } else if ((code >= 1500 && code <= 1899) || (code >= 3500 && code <= 3999)) {
            return Code.UNIV_RESULT_UNIV_DIV_PUBLIC;
        } else if ((code >= 2000 && code <= 2999) || (code >= 4000 && code <= 4999)) {
            return Code.UNIV_RESULT_UNIV_DIV_PRIVATE;
        } else {
            return Code.UNIV_RESULT_UNIV_DIV_OTHER;
        }
    }

}

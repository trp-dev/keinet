package jp.ac.kawai_juku.banzaisystem.output.main.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dao.ExmntnMainCandidateUnivDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301StudentInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.ScoreBeanDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.ScoreBeanService;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.output.main.dao.OutputMainDao;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 帳票出力・CSVファイル画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainService {

    /** 判定用成績データサービス */
    @Resource
    private ScoreBeanService scoreBeanService;

    /** DAO */
    @Resource
    private OutputMainDao outputMainDao;

    /** DAO */
    @Resource
    private ScoreBeanDao scoreBeanDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 志望大学リストに関するDAO */
    @Resource
    private ExmntnMainCandidateUnivDao exmntnMainCandidateUnivDao;

    /**
     * 簡易個人表のExcel生成サービスの入力Beanを生成します。
     *
     * @param exam 対象模試
     * @param idList 個人IDリスト
     * @return {@link ExmntnUnivA0301InBean}
     */
    public ExmntnUnivA0301InBean createA0301InBean(ExamBean exam, List<Integer> idList) {
        /* ドッキングチェック用 */
        ExamBean dockingExam = null;
        List<ExmntnUnivA0301StudentInBean> studentList = CollectionsUtil.newArrayList(idList.size());
        for (Integer id : idList) {
            dockingExam = ExamUtil.findDockingExamStudentB(exam, id, scoreBeanDao, systemDto);
            ExmntnUnivA0301StudentInBean bean = createStudentInBean(exam, dockingExam, id);
            if (bean != null) {
                studentList.add(bean);
            }
        }
        return new ExmntnUnivA0301InBean(studentList);
    }

    /**
     * 生徒一人分の簡易個人表のExcel生成サービスの入力Beanを生成します。
     *
     * @param exam 対象模試
     * @param dockingExam 対象模試のドッキング先模試
     * @param id 個人ID
     * @return {@link ExmntnUnivA0301StudentInBean}
     */
    private ExmntnUnivA0301StudentInBean createStudentInBean(ExamBean exam, ExamBean dockingExam, Integer id) {

        /* 生徒情報を取得する */
        ExmntnUnivStudentBean studentBean = outputMainDao.selectStudentBean(id, exam.getExamYear());

        /* 判定用の成績データを生成する */
        ScoreBean scoreBean = scoreBeanService.createScoreBean(exam, id);

        /* 志望大学情報リストを生成する */
        List<ExmntnMainCandidateUnivBean> candidateUnivList = createCandidateUnivList(exam, id, scoreBean);

        if (!scoreBean.isEntered() && candidateUnivList.isEmpty()) {
            /* 成績未入力かつ志望大学無しなら生成しない */
            return null;
        } else {
            /* 入力Beanを生成して返す */
            return new ExmntnUnivA0301StudentInBean(studentBean, scoreBean, candidateUnivList, true);
        }
    }

    /**
     * 志望大学情報リストを生成します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @param scoreBean {@link ScoreBean}
     * @return 志望大学情報リスト
     */
    private List<ExmntnMainCandidateUnivBean> createCandidateUnivList(ExamBean exam, Integer id, ScoreBean scoreBean) {
        List<ExmntnMainCandidateUnivBean> list = exmntnMainCandidateUnivDao.selectCandidateUnivList(exam, id);
        if (list.size() > 9) {
            /* 上位9位までとする */
            return list.subList(0, 9);
        } else {
            return list;
        }
    }

}

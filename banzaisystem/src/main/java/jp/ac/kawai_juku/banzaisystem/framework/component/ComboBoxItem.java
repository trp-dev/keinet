package jp.ac.kawai_juku.banzaisystem.framework.component;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * コンボボックスのアイテムを保持するクラスです。
 *
 *
 * @param <T> 値の型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ComboBoxItem<T> {

    /** ラベル */
    private final String label;

    /** 値 */
    private final T value;

    /**
     * コンストラクタです。
     *
     * @param label ラベル
     * @param value 値
     */
    public ComboBoxItem(String label, T value) {
        this.label = StringUtils.defaultString(label);
        this.value = value;
    }

    /**
     * コンストラクタです。
     *
     * @param value 値
     */
    public ComboBoxItem(T value) {
        this(ObjectUtils.toString(value), value);
    }

    /**
     * ラベルを返します。
     *
     * @return ラベル
     */
    public String getLabel() {
        return label;
    }

    /**
     * 値を返します。
     *
     * @return 値
     */
    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else {
            return ObjectUtils.equals(value, ((ComboBoxItem<?>) obj).value);
        }
    }

    @Override
    public int hashCode() {
        return value == null ? 0 : value.hashCode();
    }

}

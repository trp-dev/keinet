package jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXCLUDE_EXAM_SECOND;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Color;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailCenterSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSecondSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.PubUnivMasterService;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;
import jp.co.fj.kawaijuku.judgement.Constants.COURSE_ALLOT;
import jp.co.fj.kawaijuku.judgement.data.UnivInfoBean;

/**
 *
 * 志望大学詳細画面の公表科目に関するヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivPubSubjectHelper {

    /** View */
    @Resource(name = "exmntnUnivView")
    private ExmntnUnivView view;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** 公表用大学マスタロードサービス */
    @Resource
    private PubUnivMasterService pubUnivMasterService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    /**
     * 公表科目を表示します。
     *
     * @param univDetail 画面に表示している大学詳細情報
     */
    public void showPubSubject(ExmntnUnivDetailSvcOutBean univDetail) {

        if (univDetail != null) {
            /* 情報誌用科目を取得 */
            UnivInfoBean info = findUnivInfoBean(univDetail);

            /* 共通テスト・二次の公表科目を設定する */

            int univCd = Integer.parseInt(univDetail.getUnivCd());
            /* 大学コードが6000以降の場合、公表科目を設定しない */
            if (info == null || univCd >= 6000) {

                /* 共通テスト・二次の公表科目を設定 */
                if (!EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv()) && info == null && univCd < 6000) {
                    setPubSubject(info, false);
                } else {
                    view.centerPubSubCountLabel.setText("");
                    view.secondPubSubCountLabel.setText("");
                }
            } else {
                /* 共通テスト・二次の公表科目を設定 */
                if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
                    setPubSubject(info, true);
                } else {
                    setPubSubject(info, false);
                }
            }

            /* センタ公表科目を設定する */
            setCenterDetail(univDetail.getCenter(), info);

            /* 二次公表科目を設定する */
            setSecondDetail(univDetail.getSecond(), info);
        }
    }

    /**
     * 情報誌用科目を取得します。
     *
     * @param univDetail 個人成績・志望大学 画面表示用の出力Bean
     * @return 情報誌用科目
     */
    private UnivInfoBean findUnivInfoBean(ExmntnUnivDetailSvcOutBean univDetail) {

        UnivInfoBean info = null;
        /* 情報誌科目から表示の場合 */
        if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
            /* キーを取得 */
            String combiKey = right(univDetail.getCenter().getBranchCd(), 2) + right(univDetail.getSecond().getBranchCd(), 2);
            /* 情報誌用科目データマップを取得 */
            @SuppressWarnings("rawtypes")
            Map univ = (Map) systemDto.getUnivInfo().get(univDetail.getUnivKey());
            /* センター枝番、二次枝番から該当の情報誌用科目を取得 */
            for (Object key : univ.keySet()) {
                if (key.toString().endsWith(combiKey)) {
                    info = (UnivInfoBean) univ.get(key.toString());
                    break;
                }
            }
            /* 見つからない場合、先頭を取得 */
            if (info == null) {
                info = (UnivInfoBean) univ.get(univ.keySet().toArray()[0]);
            }
        }
        return info;
    }

    /**
     * 指定された文字列の最後から指定された文字列長の文字列を返します。
     *
     * @param value 基となる文字列
     * @param length 返す文字列の文字列長
     * @return 基となる文字列の最後から指定された文字列長の文字列
     */
    private static String right(String value, int length) {
        try {
            if (value.length() >= length)
                return value.substring(value.length() - length);
            else
                return value.substring(1);
        } catch (Exception e) {
            return value;
        }
    }

    /**
     * センタ公表科目を返します。
     *
     * @param list 公表科目リスト
     * @param branchCd 判定採用枝番
     * @return {@link CenterDetail}
     */
    /*    private CenterDetail findCenterDetail(Collection<?> list, String branchCd) {
        for (Object obj : list) {
            UnivDetail detail = (UnivDetail) obj;
            if (StringUtils.equals(branchCd, detail.getCenter().getBranchCode())) {
                return detail.getCenter();
            }
        }
        return ((UnivDetail) list.iterator().next()).getCenter();
    }
    */

    /**
     * 共通テスト・二次の公表科目を設定します。
     *
     * @param info {@link UnivInfoBean}
     * @param pubSubjectFlg 公表科目設定フラグ
     */
    private void setPubSubject(UnivInfoBean info, Boolean pubSubjectFlg) {
        String fmt = "<html>%s</html>";

        /* 公表科目を設定 */
        if (pubSubjectFlg) {
            /* 共通テスト公表科目を生成 */
            if (info.isCExist()) {
                StringBuffer args = new StringBuffer(info.getC_subString());
                if (info.isFirstAnsFlgSci()) {
                    args.append(getMessage("label.exmntn.univ.firstAnsFlgSci"));
                }
                if (info.isFirstAnsFlgSoc()) {
                    args.append(getMessage("label.exmntn.univ.firstAnsFlgSoc"));
                }
                /* 共通テスト公表科目を設定 */
                view.centerPubSubCountLabel.setText(String.format(fmt, args.toString()));
            } else {
                view.centerPubSubCountLabel.setText("");
            }
            if (info.isSExist()) {
                view.secondPubSubCountLabel.setText(String.format(fmt, info.getS_subString()));
            } else {
                view.secondPubSubCountLabel.setText("");
            }
        } else {
            /* 公表科目表示対象外の模試の場合、固定メッセージを設定 */
            String msg = getMessage("label.exmntn.univ.noPubSubject");
            view.centerPubSubCountLabel.setText(msg);
            view.secondPubSubCountLabel.setText(msg);
        }
    }

    /**
     * Viewにセンタ配点を設定します。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @param info {@link info}
     */
    private void setCenterDetail(ExmntnUnivDetailCenterSvcOutBean center, UnivInfoBean info) {

        /* 判定結果から表示の場合 */
        /* 英語 */
        view.centerAllotEnglishPubLabel.setText(center.getAllotEnglish());
        /* 数学 */
        view.centerAllotMathPubLabel.setText(center.getAllotMath());
        /* 国語 */
        view.centerAllotLanguagePubLabel.setText(center.getAllotJap());
        /* 理科 */
        view.centerAllotSciencePubLabel.setText(center.getAllotSci());
        /* 地公 */
        view.centerAllotSocietyPubLabel.setText(center.getAllotSoc());
    }

    /*    *//**
            * 指定した教科が課されているかを返します。
            *
            * @param detail 詳細データ
            * @param courseIndex 教科インデックス
            * @return 課されているならtrue
            */
    /*
    private boolean isImposed(CommonDetailHolder detail, int courseIndex) {
     for (Object obj : detail.getImposingSubjects().entrySet()) {
         Entry<?, ?> entry = (Entry<?, ?>) obj;
         SubjectKey key = (SubjectKey) entry.getKey();
         ImposingSubject subject = (ImposingSubject) entry.getValue();
         if (subject.isImposed() && key.getCourseIndex() == courseIndex) {
             return true;
         }
     }
     return false;
    }
    
    *//**
      * 二次公表科目を返します。
      *
      * @param list 公表科目リスト
      * @param branchCd 判定採用枝番
      * @return {@link CenterDetail}
      *//*
        private SecondDetail findSecondDetail(Collection<?> list, String branchCd) {
         for (Object obj : list) {
             UnivDetail detail = (UnivDetail) obj;
             if (StringUtils.equals(branchCd, detail.getSecond().getBranchCode())) {
                 return detail.getSecond();
             }
         }
         return ((UnivDetail) list.iterator().next()).getSecond();
        }
        */

    /**
     * Viewに二次配点を設定します。
     *
     * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
     * @param info {@link UnivInfoBean}
     */
    private void setSecondDetail(ExmntnUnivDetailSecondSvcOutBean second, UnivInfoBean info) {
        String fmt = "<html>%s</html>";
        String wrk = "";
        /* 英語 */
        view.secondAllotEnglishPubLabel.setText(second.getAllotEnglish());
        /* 数学 */
        view.secondAllotMathPubLabel.setText(second.getAllotMath());
        /* 国語 */
        view.secondAllotLanguagePubLabel.setText(second.getAllotJap());
        /* 理科 */
        view.secondAllotSciencePubLabel.setText(second.getAllotSci());
        /* 地公 */
        view.secondAllotSocietyPubLabel.setText(second.getAllotSoc());
        /* 情報誌科目から表示の場合 */
        if (EXCLUDE_EXAM_SECOND.contains(systemDto.getUnivExamDiv())) {
            /* その他 */
            view.secondOtherPubLabel.setText("その他");
            view.secondAllotOtherPubLabel.setText(String.format(fmt, info.getSecondAllot(COURSE_ALLOT.ETC)));
        } else { /* 判定結果から表示の場合 */
            /* その他 */
            view.secondAllotOtherPubLabel.setOpaque(true);
            view.secondAllotOtherPubLabel.setBackground(Color.LIGHT_GRAY);
            view.secondAllotOtherPubLabel.setText(String.format(fmt, wrk));
        }
    }

    /**
     * 判定科目を表示します。
     */
    public void showExamSubject() {
        changeSubjectComponentVisible(false);
    }

    /**
     * 公表科目／判定科目コンポーネントの表示状態を切り替えます。
     *
     * @param pubSubjectFlg 公表科目表示フラグ
     */
    private void changeSubjectComponentVisible(boolean pubSubjectFlg) {
        view.setVisibleByGroup(ExmntnUnivView.GROUP_PUB_SUBJECT, pubSubjectFlg);
        view.setVisibleByGroup(ExmntnUnivView.GROUP_EXAM_SUBJECT, !pubSubjectFlg);
    }

}

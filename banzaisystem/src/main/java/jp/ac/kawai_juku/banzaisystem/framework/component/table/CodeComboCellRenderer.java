package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellRenderer;

import jp.ac.kawai_juku.banzaisystem.CodeType;

/**
 *
 * コードを利用するコンボボックスセルのレンダラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CodeComboCellRenderer extends JPanel implements TableCellRenderer {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** コンボボックス */
    private final JComboBox<String> combo = new JComboBox<String>();

    /** コード種別 */
    private final CodeType codeType;

    /**
     * コンストラクタです。
     *
     * @param codeType コード種別
     * @param width セル横
     * @param height セル高さ
     */
    public CodeComboCellRenderer(CodeType codeType, int width, int height) {
        super(new FlowLayout(FlowLayout.LEFT, 9, (int) Math.floor((double) (height - 20) / 2.0)));
        this.codeType = codeType;
        combo.setBorder(new MatteBorder(1, 1, 1, 1, new Color(165, 172, 178)));
        combo.setPreferredSize(new Dimension(width - 18, 20));
        combo.setRequestFocusEnabled(false);
        add(combo);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if (isSelected) {
            setBackground(table.getSelectionBackground());
        } else {
            setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
        }
        combo.removeAllItems();
        if (value != null) {
            if (!(value instanceof String)) {
                throw new RuntimeException("コードコンボボックスセルの値はString型で設定してください。" + value.getClass());
            }
            combo.addItem(codeType.value2Code((String) value).getName());
        }
        return this;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainValidateHelper;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.log.Logger;

/**
 *
 * バンザイシステムのフレームです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public final class BZFrame extends JFrame {

    /** Logger */
    private static final Logger logger = Logger.getLogger(BZFrame.class);

    /**
     * コンストラクタです。
     */
    public BZFrame() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        /* 多重起動防止用のロックを取得する */
        lock();

        /* コンボボックスUIを入れ替える */
        UIManager.getDefaults().put("ComboBoxUI", BZComboBoxUI.class.getName());

        /* FileChooserでWindows標準コンポーネットっぽく見せるためにフォントを変える */
        UIManager.put("FileChooser.listFont", new FontUIResource("MS UI Gothic", Font.PLAIN, 12));

        setTitle(getMessage("label.framework.title"));
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (WindowManager.getInstance().isTaskRunning()) {
                    JOptionPane.showMessageDialog(e.getWindow(), getMessage("error.framework.running"));
                } else {
                    try {
                        if (showConfirmDialog(e.getWindow())) {
                            ExmntnMainValidateHelper helper =
                                    SingletonS2Container.getComponent(ExmntnMainValidateHelper.class);
                            if (helper.checkMovable()) {
                                System.exit(0);
                            }
                        }
                    } catch (Throwable ex) {
                        System.exit(0);
                    }
                }
            }
        });
        setResizable(false);
        setUndecorated(true);
        setIconImage(ImageUtil.readImage("/icon.png"));
    }

    /**
     * 多重起動防止用のロックを取得します。
     */
    private void lock() {
        try {
            final File file = new File(".lock");
            file.deleteOnExit();
            final FileOutputStream out = new FileOutputStream(file);
            final FileChannel ch = out.getChannel();
            final FileLock lock = ch.tryLock();
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        unlock();
                    } catch (IOException e) {
                        logger.error("ロックの解除に失敗しました。", e);
                    }
                }

                /**
                 * ロックを解除します。
                 *
                 * @throws IOException IO例外
                 */
                private void unlock() throws IOException {
                    try {
                        if (lock != null) {
                            lock.release();
                        }
                    } finally {
                        if (out != null) {
                            out.close();
                        }
                    }
                }
            }));
            if (lock == null) {
                showMessageDialog(getMessage("error.framework.CM_E010"));
                System.exit(1);
            }
        } catch (IOException e) {
            logger.error("ロックの取得に失敗しました。", e);
            showMessageDialog(getMessage("error.framework.CM_E009"));
            System.exit(1);
        }
    }

    /**
     * メッセージダイアログを表示します。
     *
     * @param message メッセージ
     */
    private void showMessageDialog(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    /**
     * 終了確認ダイアログを表示します。
     *
     * @param parentComponent ダイアログの親コンポーネント
     * @return 終了OKならtrue
     */
    private boolean showConfirmDialog(Component parentComponent) {
        String message = getMessage("error.framework.CM_W001");
        String title = getMessage("label.framework.exitDialogTitle");
        String[] button = { getMessage("label.framework.exitDialogYes"), getMessage("label.framework.exitDialogNo") };
        int result =
                JOptionPane.showOptionDialog(parentComponent, message, title, JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * ラジオボタンのセルエディタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class RadioButtonCellEditor extends AbstractCellEditor implements TableCellEditor {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** パネル */
    private final JPanel panel;

    /** 編集中のセルの値 */
    private Object value;

    /**
     * コンストラクタです。
     *
     * @param color セル右側のボーダカラー
     */
    public RadioButtonCellEditor(Color color) {
        panel = new JPanel(new GridBagLayout());
        panel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, color));
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

        this.value = value;

        if (table.isRowSelected(row)) {
            panel.setBackground(table.getSelectionBackground());
        } else {
            panel.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
        }

        panel.removeAll();

        if (value != null) {
            final JRadioButton radio = new JRadioButton();
            radio.setOpaque(false);
            radio.setRequestFocusEnabled(false);
            radio.setSelected(((Boolean) value).booleanValue());
            radio.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    RadioButtonCellEditor.this.value = Boolean.valueOf(radio.isSelected());
                    /* 編集を終了して、データモデル内のデータ書き換えを発生させる */
                    fireEditingStopped();
                }
            });
            panel.add(radio, new GridBagConstraints());
        }

        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        return value;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return false;
    }

}

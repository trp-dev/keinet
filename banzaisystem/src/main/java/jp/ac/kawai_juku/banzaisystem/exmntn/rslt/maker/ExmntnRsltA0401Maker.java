package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.maker;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;
import java.util.Vector;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SearchSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.bean.ExmntnRsltA0401InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dao.ExmntnRsltSearhDao;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.StringUtil;

/**
 *
 * Excel帳票メーカー(検索結果)です。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Template(id = TemplateId.A04_01)
public class ExmntnRsltA0401Maker extends BaseExcelMaker<ExmntnRsltA0401InBean> {

    /** 最大出力数 */
    private static final int LISTMAX_ALL = 500;

    /** 1ページ目に出力する最大出力数 */
    private static final int LISTMAX_1PAGE = 20;

    /** 2ページ目以降に出力する最大出力数 */
    private static final int LISTMAX_2PAGE = 30;

    /** インデックスrow：大学一覧（開始位置）:1ページ目 */
    /* private static final int IDX_ROW_LIST_1PAGE_START = 18; */
    private static final int IDX_ROW_LIST_1PAGE_START = 17;

    /** インデックスrow：大学一覧（開始位置）:2ページ目 */
    /* private static final int IDX_ROW_LIST_2PAGE_START = 11; */
    private static final int IDX_ROW_LIST_2PAGE_START = 10;

    /** インデックスrow：得点・偏差値項目切替行（1ページ目） */
    /* private static final int IDX_ROW_PNT_PAGE1 = 17; */
    private static final int IDX_ROW_PNT_PAGE1 = 16;

    /** インデックスrow：得点・偏差値項目切替行（2ページ目以降） */
    /* private static final int IDX_ROW_PNT_PAGE2_SUB_ONES = 10; */
    private static final int IDX_ROW_PNT_PAGE2_SUB_ONES = 9;

    /** インデックス：得点(含まない) */
    /** インデックス：得点 */
    private static final int IDX_PNT = 8;

    /** インデックス：得点(含む) */
    /* private static final int IDX_PNT_INC = 12; */

    /** インデックス：偏差値  */
    /* private static final int IDX_DEV = 14; */
    private static final int IDX_DEV = 10;

    /** インデックスrow：大学一覧（１大学当たりの行数） */
    private static final int IDX_ROW_LIST_LINE = 1;

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル：クラス番号 */
    private static final String TITLE_CLSNO = "クラス番号：";

    /** タイトル：氏名 */
    private static final String TITLE_NAME = "　氏名：";

    /** タイトル：検索結果（ソートなし） */
    private static final String TITLE_SEARCH = "<検索結果>";

    /** タイトル：検索結果（ソートあり） */
    private static final String TITLE_SEARCH_SORTED = "<検索結果：%s順>";

    /** タイトル：得点 */
    private static final String TITLE_PNT = "得点";

    /** タイトル：本人得点 */
    private static final String TITLE_PNT_M = "本人得点";

    /** タイトル：偏差値 */
    private static final String TITLE_DEV = "偏差値";

    /** タイトル：本人偏差値 */
    private static final String TITLE_DEV_M = "本人偏差値";

    /** 文字列区切り:カンマ */
    private static final String STR_COMMA = ",";

    /** 文字列:〜 */
    /*    private static final String STR_KARA = "〜";
    */
    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A3";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A4";

    /** インデックス：クラス番号・氏名 */
    private static final String IDX_CLASSNO_NAME = "A7";

    /** インデックス：大学区分 */
    private static final String IDX_UNIV_KUBUN = "B9";

    /** インデックス：女子大 */
    /* private static final String IDX_WIMAN_UNIV = "I9"; */
    private static final String IDX_WIMAN_UNIV = "G9";

    /** インデックス：夜間 */
    /* private static final String IDX_NIGHT = "Q9"; */
    private static final String IDX_NIGHT = "L9";

    /** インデックス：都道府県 */
    private static final String IDX_PREFECTURE = "C10";

    /** インデックス：地方 */
    private static final String IDX_RURAL_ARE = "C11";

    /** インデックス：学部系統 */
    private static final String IDX_GAKUBU_SYSTEM = "B12";

    /** インデックス：共通テスト */
    /* private static final String IDX_CENTER_EXAM = "M10"; */
    private static final String IDX_CENTER_EXAM = "J10";

    /** インデックス：個別試験 */
    /* private static final String IDX_IND_EXAM = "M11"; */
    private static final String IDX_IND_EXAM = "J11";

    /** インデックス：共通テスト個別配点比 */
    /* private static final String IDX_CENTER_IND_SCORE = "M12"; */
    private static final String IDX_CENTER_IND_SCORE = "J12";

    /** インデックス：入試日程:国公立大 */
    /* private static final String IDX_EXAM_PUBLIC = "M13"; */
    private static final String IDX_EXAM_PUBLIC = "J13";

    /** インデックス：入試日程:私立・短大出願締切日 */
    /* private static final String IDX_EXAM_APPL = "M14"; */
    private static final String IDX_EXAM_APPL = "J14";

    /** インデックス：評価範囲(模試) */
    private static final String IDX_EVA_RANG_MOSHI = "B13";

    /** インデックス：評価範囲(評価) */
    private static final String IDX_EVA_RANG_HYOKA = "B14";

    /** インデックス：検索結果1ページ目 */
    private static final String IDX_SEARCH_SORT_1PAGE = "A15";

    /** インデックス：検索結果2ページ目 */
    private static final String IDX_SEARCH_SORT_2PAGE = "A8";

    /** ソート条件の文字列を取得するための配列 */
    /*
    private static final String[][] SORT_CONDITION =
            { { "division", "区分" }, { "seatOfUniv", "所在地" }, { "university", "大学" }, { "borderOfExclude", "ボーダ得点率(含まない)" }, { "pointOfExclude", "本人共テ得点(含まない)" }, { "borderOfInclude", "ボーダ得点率(含む)" }, { "pointOfInclude", "本人共テ得点(含む)" },
                    { "rankOfSecond", "二次ランク" }, { "devSecond", "二次偏差値" }, { "fullSecond", "二次満点" }, { "valuateExclude", "評価：共テ(含まない)" }, { "valuateInclude", "評価：共テ(含む)" }, { "valuateSecond", "評価：二" }, { "valuateSynthetic", "評価：総" } };
    */
    private static final String[][] SORT_CONDITION = { { "division", "区分" }, { "seatOfUniv", "所在地" }, { "university", "大学" }, { "borderOfExclude", "ボーダ得点率" }, { "pointOfExclude", "本人共テ得点" }, { "rankOfSecond", "二次ランク" },
            { "devSecond", "二次偏差値" }, { "valuateExclude", "評価：共テ" }, { "valuateSecond", "評価：二" }, { "valuateSynthetic", "評価：総" }, { "compStartPointCenter", "配点比：共テ" }, { "compStartPointSecond", "配点比：二次" } };

    /** ExmntnRsltExcelCreatorInBean */
    private ExmntnRsltA0401InBean excelBean;

    /** DAO */
    @Resource
    private ExmntnRsltSearhDao exmntnRsltSearhDao;

    @Override
    protected boolean prepareData(ExmntnRsltA0401InBean inBean) {

        /* 入力Beanを保持する */
        excelBean = inBean;

        /* 該当検索件数が0件の場合帳票出力しない */
        if (StringUtil.isNotEmpty(excelBean.getView().countLabel.getText()) && excelBean.getView().countLabel.getText().equals("0")) {
            return false;
        }

        return true;

    }

    @Override
    protected void initSheet() {

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* クラス番号・氏名を出力する */
        outputTargetName();

        /* 検索条件情報を出力する */
        outputSearchCondition();

        /* 検索結果ソート項目を出力する */
        outputTargetSerchSort();

        /* 得点・偏差値タイトルを出力する */
        outputPntTitle();

    }

    @Override
    protected void outputData() {

        /* 検索結果情報を出力する */
        outputRecord();

    }

    /**
     * 学年・クラスを出力するメソッドです。
     *
     */
    private void outputTargetClass() {

        String gradeStr = TITLE_GRADE;
        String classStr = TITLE_CLS;

        if (getComponent(SystemDto.class).isTeacher()) {
            /* 先生の場合 */
            if (excelBean.getMainView().gradeComboBox.getSelectedLabel() != null) {
                gradeStr += excelBean.getMainView().gradeComboBox.getSelectedLabel();
            }
            if (excelBean.getMainView().classComboBox.getSelectedLabel() != null) {
                classStr += excelBean.getMainView().classComboBox.getSelectedLabel();
            }
        } else {
            /* 学生の場合 */
            gradeStr += excelBean.getMainView().gradeField.getText();
            classStr += excelBean.getMainView().classField.getText();
        }

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeStr + classStr);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {

        String targetExam = TITLE_EXAM;

        /* 模試名をセットする */
        if (excelBean.getTargetExam() != null) {
            targetExam += excelBean.getTargetExam();
        }

        /* 対象模試を出力する */
        setCellValue(IDX_TARGET_EXAM, targetExam);
    }

    /**
     * クラス番号・氏名を出力するメソッドです。
     */
    private void outputTargetName() {

        String classNoStr = TITLE_CLSNO;
        String nameStr = TITLE_NAME;

        /* クラス番号をセットする */
        if (getComponent(SystemDto.class).isTeacher()) {
            if (excelBean.getMainView().numberComboBox.getSelectedLabel() != null) {
                classNoStr += excelBean.getMainView().numberComboBox.getSelectedLabel();
            }
        } else {
            classNoStr += excelBean.getMainView().numberField.getText();
        }

        /* 氏名をセットする */
        nameStr += excelBean.getMainView().nameField.getText();

        /* クラス番号・氏名を出力する */
        setCellValue(IDX_CLASSNO_NAME, classNoStr + nameStr);
    }

    /**
     * 検索結果ソート項目を出力するメソッドです。
     */
    private void outputTargetSerchSort() {

        String sortStr = TITLE_SEARCH;

        /* ソート項目をセットする */
        for (String[] str : SORT_CONDITION) {
            if (str[0].equals(excelBean.getView().resultTable.getSortKey())) {
                sortStr = String.format(TITLE_SEARCH_SORTED, str[1]);
                break;
            }
        }

        /* 検索結果ソート項目を出力する */
        if (getSheetCount() == 1) {
            setCellValue(IDX_SEARCH_SORT_1PAGE, sortStr);
        } else {
            setCellValue(IDX_SEARCH_SORT_2PAGE, sortStr);
        }
    }

    /**
     * 得点・偏差値タイトルを出力するメソッドです。
     */
    private void outputPntTitle() {
        int row = 0;
        /* 1ページ目と2ページ目で出力位置が違う */
        if (getSheetCount() == 1) {
            row = IDX_ROW_PNT_PAGE1;
        } else {
            row = IDX_ROW_PNT_PAGE2_SUB_ONES;
        }

        if (excelBean.getTargetExam() == null) {
            /* 得点・偏差値名称設定(リサーチ以外) */
            setCellValue(row, IDX_PNT, TITLE_PNT);
            /* setCellValue(row, IDX_PNT_INC, TITLE_PNT); */
            setCellValue(row, IDX_DEV, TITLE_DEV);
        } else if (ExamUtil.isCenter(excelBean.getTargetExam())) {
            /* 得点・偏差値名称設定(リサーチ) */
            setCellValue(row, IDX_PNT, TITLE_PNT_M);
            /* setCellValue(row, IDX_PNT_INC, TITLE_PNT_M); */
            setCellValue(row, IDX_DEV, TITLE_DEV_M);
        } else {
            /* 得点・偏差値名称設定(リサーチ以外) */
            setCellValue(row, IDX_PNT, TITLE_PNT);
            /* setCellValue(row, IDX_PNT_INC, TITLE_PNT); */
            setCellValue(row, IDX_DEV, TITLE_DEV);
        }
    }

    /**
     * 志望大学情報を出力するメソッドです。
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void outputRecord() {

        /* 1ページ内の大学出力カウント */
        int cntUniv = 0;

        Vector<Vector> resultList = excelBean.getView().resultTable.getFixDataList();
        Vector<Vector> resultList2 = excelBean.getView().resultTable.getDataList();

        int max = Math.min(LISTMAX_ALL, resultList.size());
        for (int i = 0; i < max; i++) {

            Vector<CellData> data = resultList.get(i);
            Vector<CellData> data2 = resultList2.get(i);

            /* 1ページ内の最大出力数を超えたかチェックする */
            if (getSheetCount() == 1) {
                /* 1ページ目の出力 */
                if (cntUniv >= LISTMAX_1PAGE) {
                    /* 改シートする */
                    breakSheet();
                    cntUniv = 0;
                }
            } else {
                /* 2ページ目以降の出力 */
                if (cntUniv >= LISTMAX_2PAGE) {
                    /* 改シートする */
                    breakSheet();
                    cntUniv = 0;
                }
            }

            /* 大学検索結果データを出力する */
            outputRecordList(cntUniv, data, data2);

            cntUniv++;
        }
    }

    /**
     * 検索条件情報を出力するメソッドです。
     */
    private void outputSearchCondition() {

        if (getSheetCount() == 1) {

            /* 大学区分を出力する */
            setCellValue(IDX_UNIV_KUBUN, createUnivKubun());

            /* 女子大を出力する */
            setCellValue(IDX_WIMAN_UNIV, excelBean.getSearchBean().getFemail().getName());

            /* 夜間を出力する */
            setCellValue(IDX_NIGHT, excelBean.getSearchBean().getNight().getName());

            /*「大学名称・所在地」を検索条件を出力 */
            outputExmntnName();

            /*「学部系統・取得資格」を検索条件を出力 */
            outputExmntnFclt();

            /*「入試（科目・日程・評価）」を検索条件を出力 */
            outputExmntnEnte();
        }
    }

    /**
     * 大学区分の出力文字列を生成します。
     *
     * @return 大学区分の出力文字列
     */
    private String createUnivKubun() {
        StringBuilder sb = new StringBuilder();
        if (excelBean.getSearchBean().getDivList() != null) {
            for (String value : excelBean.getSearchBean().getDivList()) {
                if (sb.length() > 0) {
                    sb.append(STR_COMMA);
                }
                sb.append(CodeType.UNIV_SEARCH_DIV.value2Code(value).getName());
            }
        }
        return sb.toString();
    }

    /**
     * 「大学名称・所在地」を検索条件を出力します。
     */
    private void outputExmntnName() {
        /* 都道府県を出力する */
        setCellValue(IDX_PREFECTURE, getPrefectures());
        /* 地方を出力する */
        setCellValue(IDX_RURAL_ARE, getRuralAreas());
    }

    /**
     * 「学部系統・取得資格」を検索条件を出力します。
     */
    private void outputExmntnFclt() {
        /* 学部系統一覧を出力する */
        setCellValue(IDX_GAKUBU_SYSTEM, getFacultySystemInfo());
    }

    /**
     * 「入試（科目・日程・評価）」を検索条件を出力します。
     */
    private void outputExmntnEnte() {

        /* 評価範囲(模試) を出力する */
        if (excelBean.getSearchBean().getRaingDivList() != null) {
            setCellValue(IDX_EVA_RANG_MOSHI, createCodeNameString(CodeType.UNIV_SEARCH_RANGE, excelBean.getSearchBean().getRaingDivList()));
        }

        /* 評価範囲(評価) を出力する */
        if (excelBean.getSearchBean().getRaingDivList() != null && excelBean.getSearchBean().getRatingList() != null) {
            setCellValue(IDX_EVA_RANG_HYOKA, createCodeNameString(CodeType.UNIV_SEARCH_RANGE_VALUE, excelBean.getSearchBean().getRatingList()));
        }

        /* 入試日程:国公立大を出力する */
        if (excelBean.getSearchBean().getScheduleList() != null) {
            setCellValue(IDX_EXAM_PUBLIC, createCodeNameString(CodeType.UNIV_SEARCH_SCHEDULE, excelBean.getSearchBean().getScheduleList()));
        }

        /* 入試日程:私立・短大を出力する */
        if (excelBean.getSearchBean().getScheduleList2() != null) {
            setCellValue(IDX_EXAM_APPL, createCodeNameString(CodeType.UNIV_SEARCH_SCHEDULE2, excelBean.getSearchBean().getScheduleList2()));
        }

        /* センター試験を出力する */
        if (excelBean.getSearchBean().getCenterList() != null) {
            setCellValue(IDX_CENTER_EXAM, createSearchSubjectString(excelBean.getSearchBean().getCenterList()));
        }

        /* 個別試験を出力する */
        setKobetuExam();

        /* センター個別配点比を出力する */
        if (excelBean.getSearchBean().getRateList() != null) {
            setCellValue(IDX_CENTER_IND_SCORE, createCodeNameString(CodeType.UNIV_SEARCH_RATE, excelBean.getSearchBean().getRateList()));
        }

    }

    /**
     * 検索科目文字列を生成します。
     *
     * @param list 検索科目リスト
     * @return 検索科目文字列
     */
    private String createSearchSubjectString(List<? extends SearchSubject> list) {
        StringBuilder sb = new StringBuilder();
        for (SearchSubject s : list) {
            if (sb.length() > 0) {
                sb.append(STR_COMMA);
            }
            if (!StringUtils.isEmpty(s.getName())) {
                sb.append(s.getName());
            }
        }
        return sb.toString();
    }

    /**
     * コード値リストからコード名称文字列を生成します。
     *
     * @param type コード種別
     * @param list コード値リスト
     * @return コード名称文字列
     */
    private String createCodeNameString(CodeType type, List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String rating : list) {
            if (sb.length() > 0) {
                sb.append(STR_COMMA);
            }
            sb.append(type.value2Code(rating).getName());
        }
        return sb.toString();
    }

    /**
     * 個別試験情報を出力します。
     */
    private void setKobetuExam() {
        if (excelBean.getSearchBean().getNotImpose() != null) {
            if (excelBean.getSearchBean().getNotImpose()) {
                setCellValue(IDX_IND_EXAM, Code.UNIV_SEARCH_NOT_IMPOSED.getName());
            } else if (excelBean.getSearchBean().getSecondList() != null) {
                setCellValue(IDX_IND_EXAM, createSearchSubjectString(excelBean.getSearchBean().getSecondList()));
            }
        }
    }

    /**
     * 都道府県文字列を取得します。
     *
     * @return 都道府県文字列
     */
    private String getPrefectures() {

        StringBuilder sb = new StringBuilder();
        if (excelBean.getSearchBean().getPrefCdList() != null) {
            for (String val : exmntnRsltSearhDao.selectPreNameList(excelBean.getSearchBean().getPrefCdList())) {
                if (sb.length() > 0) {
                    sb.append(STR_COMMA);
                }
                sb.append(val);
            }
        }

        return sb.toString();
    }

    /**
     * 地方文字列を取得します。
     *
     * @return 地方文字列
     */
    private String getRuralAreas() {

        StringBuilder sb = new StringBuilder();
        if (excelBean.getSearchBean().getDistCdList() != null) {
            for (String val : exmntnRsltSearhDao.selectDistNameList(excelBean.getSearchBean().getDistCdList())) {
                if (sb.length() > 0) {
                    sb.append(STR_COMMA);
                }
                sb.append(val);
            }
        }

        return sb.toString();
    }

    /**
     * 学部系統一覧文字列を取得します。
     *
     * @return 学部系統一覧
     */
    private String getFacultySystemInfo() {
        StringBuilder sb = new StringBuilder();
        if (excelBean.getSearchBean().getPrintRegionList() != null) {
            for (Code code : excelBean.getSearchBean().getPrintRegionList()) {
                if (sb.length() > 0) {
                    sb.append(STR_COMMA);
                }
                sb.append(code.getAliasName());
            }
        }
        return sb.toString();
    }
    /*
    *//**
      * 出願締切日選択文字列を取得します。
      *
      * @return 出願締切日文字列
      *//*
        private String getApplyDay() {
         StringBuilder sb = new StringBuilder();
         if (excelBean.getSearchBean().getApplyStart() != null) {
             sb.append(ExmntnRsltUtil.toMD(excelBean.getSearchBean().getApplyStart()));
         }
         if (excelBean.getSearchBean().getApplyEnd() != null) {
             sb.append(STR_KARA);
             sb.append(ExmntnRsltUtil.toMD(excelBean.getSearchBean().getApplyEnd()));
         }
         return sb.toString();
        }
        */

    /**
     * 大学検索結果一覧データを出力するメソッドです。
     *
     * @param cntUniv 1ページ内の人数カウント
     * @param data 大学データ(スクロールなし部分)
     * @param data2 大学データ(スクロール部分)
     */
    private void outputRecordList(int cntUniv, Vector<CellData> data, Vector<CellData> data2) {

        /* 大学情報出力位置 */
        int rowUniv = getPageStartNo(cntUniv);

        int cellnum = 0;
        /* ヘッダ項目 */
        for (CellData cell : data.subList(1, data.size())) {
            Object dispValue = cell.getDispValue();
            Object sortValue = cell.getSortValue();
            if (sortValue instanceof Pair) {
                /* 学部学科のみ分解して出力する */
                Pair<?, ?> pair = (Pair<?, ?>) sortValue;
                setCellValue(rowUniv, cellnum++, (String) pair.getLeft());
                setCellValue(rowUniv, cellnum++, (String) pair.getRight());
            } else if (dispValue != null) {
                setCellValue(rowUniv, cellnum++, dispValue.toString());
            } else {
                cellnum++;
            }
        }
        /* データ項目 */
        for (CellData cell : data2.subList(1, data2.size())) {
            Object dispValue = cell.getDispValue();
            String[] splitWk;
            String dispWk = "";
            /* ボーダ、満点、得点率の場合はデータ分割する */
            /* if (cellnum == 5 || cellnum == 9) { */
            if (cellnum == 5) {
                if (dispValue != null && !dispValue.toString().equals("")) {
                    splitWk = dispValue.toString().split("/", 0);
                    /* ボーダ */
                    dispWk = splitWk[0];
                    setCellValue(rowUniv, cellnum++, dispWk);
                    /* 満点 */
                    dispWk = splitWk[1];
                    splitWk = dispWk.split(" ", -1);
                    dispWk = splitWk[0];
                    setCellValue(rowUniv, cellnum++, dispWk);
                    /* 得点率 */
                    dispWk = splitWk[1].substring(1, 3);
                    setCellValue(rowUniv, cellnum++, Integer.parseInt(dispWk));
                } else {
                    /* ボーダ */
                    setCellValue(rowUniv, cellnum++, "");
                    /* 満点 */
                    setCellValue(rowUniv, cellnum++, "");
                    /* 得点率 */
                    setCellValue(rowUniv, cellnum++, "");
                }
            } else {
                if (dispValue != null) {
                    setCellValue(rowUniv, cellnum++, dispValue.toString());
                } else {
                    cellnum++;
                }
            }
        }

    }

    /**
     * 出力開始位置を取得します。
     * @param cntUniv 1ページ内の人数カウント
     * @return 出力開始位置
     */
    private int getPageStartNo(int cntUniv) {
        /* 大学情報出力位置 */
        if (getSheetCount() == 1) {
            return IDX_ROW_LIST_1PAGE_START + (IDX_ROW_LIST_LINE * cntUniv);
        } else {
            return IDX_ROW_LIST_2PAGE_START + (IDX_ROW_LIST_LINE * cntUniv);
        }
    }

}

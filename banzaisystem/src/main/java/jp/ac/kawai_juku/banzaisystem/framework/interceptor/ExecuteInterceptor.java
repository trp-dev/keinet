package jp.ac.kawai_juku.banzaisystem.framework.interceptor;

import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;

import org.aopalliance.intercept.MethodInvocation;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;

/**
 *
 * コントローラのアクションメソッドのインターセプタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExecuteInterceptor extends AbstractInterceptor {

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {

        Object result = mi.proceed();

        if (result != null) {
            if (result instanceof ExecuteResult) {
                ((ExecuteResult) result).process((AbstractController) mi.getThis());
                return null;
            } else {
                throw new RuntimeException(String.format("コントローラ[%s]のアクションメソッド[%s]の戻り値はExecuteResultである必要があります。",
                        BZClassUtil.getSimpleName(mi.getThis().getClass()), mi.getMethod().getName()));
            }
        }

        return result;
    }

}

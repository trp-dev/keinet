package jp.ac.kawai_juku.banzaisystem.exmntn.main.helper;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_1;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.RANGE_SECOND_LOWER;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCefrRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSubjectRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectComboBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectTextField;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績画面の成績に関するヘルパークラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainScoreHelper {

    /** 型の表示フォーマット（偏差値） */
    private static final String TYPE_DEVIATION_FORMAT = "<html><center><font size=\"+1\">%s</font><br>(偏差値 %s)</center></html>";

    /** 型の表示フォーマット（得点） */
    private static final String TYPE_SCORE_FORMAT = "<html><center><font size=\"+1\">%s</font><br>(%s/%s)</center></html>";

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView view;

    /** Service */
    @Resource(name = "exmntnMainService")
    private ExmntnMainService service;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** 入力チェックヘルパー */
    @Resource(name = "exmntnMainCheckHelper")
    private ExmntnMainCheckHelper checkHelper;

    /** 科目ヘルパー */
    @Resource(name = "exmntnMainSubjectHelper")
    private ExmntnMainSubjectHelper subjectHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public ExamBean getTargetExam() {
        return ExamUtil.judgeTargetExam(getMarkExam(), getWrittenExam());
    }

    /**
     * 選択されているマーク模試を返します。
     *
     * @return マーク模試
     */
    public ExamBean getMarkExam() {
        return getSelectedExam(view.exmntnMarkComboBox);
    }

    /**
     * 選択されているマーク模試を返します。
     *
     * @return マーク模試
     */
    public ExamBean getWrittenExam() {
        return getSelectedExam(view.exmntnWritingComboBox);
    }

    /**
     * 模試コンボボックスで選択されている模試を返します。
     *
     * @param combo 模試コンボボックス
     * @return 模試
     */
    private ExamBean getSelectedExam(ComboBox<ExamBean> combo) {
        ComboBoxItem<ExamBean> data = combo.getSelectedItem();
        if (data != null && !data.getLabel().isEmpty()) {
            return data.getValue();
        } else {
            return null;
        }
    }

    /**
     * マークの科目情報を初期化します。
     */
    public void initMarkSubject() {

        ExamBean exam = getMarkExam();
        if (exam != null) {
            /* 英語 */
            view.english1Field.setSubjectBean(subjectHelper.getMarkEng());

            /* リスニング */
            view.listeningField.setSubjectBean(subjectHelper.getMarkLis());

            /* 数学（上段） */
            view.math1ComboBox.setItemList(subjectHelper.getMarkMath1List(exam));

            /* 数学（下段） */
            view.math2ComboBox.setItemList(subjectHelper.getMarkMath2List(exam));

            /* 現代文 */
            view.contemporaryField.setSubjectBean(subjectHelper.getMarkJapCo());

            /* 古文 */
            view.classicsField.setSubjectBean(subjectHelper.getMarkJapCl());

            /* 現代文 */
            view.chineseField.setSubjectBean(subjectHelper.getMarkJapCh());

            /* 理科 */
            view.science1ComboBox.setItemList(subjectHelper.getMarkScience1List(exam));
            view.science2ComboBox.setItemList(subjectHelper.getMarkScience2List(exam));
            view.science5ComboBox.setItemList(subjectHelper.getMarkScienceBasicList(exam));
            view.science6ComboBox.setItemList(subjectHelper.getMarkScienceBasicList(exam));

            /* 地歴公民 */
            view.society1ComboBox.setItemList(subjectHelper.getMarkSociety1List(exam));
            view.society2ComboBox.setItemList(subjectHelper.getMarkSociety2List(exam));
            /* view.inputButton.setEnabled(true); */
        } else {
            /* コンボボックスをクリアする */
            clearSubjectCombo(true);
            /* view.inputButton.setEnabled(false); */
        }
    }

    /**
     * 記述の科目コンボボックスを初期化します。
     */
    public void initWrittenSubject() {

        ExamBean exam = getWrittenExam();
        if (exam != null) {
            /* 英語 */
            view.english2Field.setSubjectBean(subjectHelper.getWrtEng(exam, false));

            /* 数学 */
            view.math3ComboBox.setItemList(subjectHelper.getWrittenMathList(exam));

            /* 国語 */
            view.japaneseComboBox.setItemList(subjectHelper.getWrittenJapaneseList(exam));

            /* 理科専門 */
            List<ComboBoxItem<BZSubjectRecord>> scienceList = subjectHelper.getWrittenScienceList(exam);
            view.science3ComboBox.setItemList(scienceList);
            view.science4ComboBox.setItemList(scienceList);

            /* 理科基礎 */
            List<ComboBoxItem<BZSubjectRecord>> scienceBasicList = subjectHelper.getWrittenScienceBasicList(exam);
            view.science7ComboBox.setItemList(scienceBasicList);
            view.science8ComboBox.setItemList(scienceBasicList);

            /* 地歴公民 */
            List<ComboBoxItem<BZSubjectRecord>> societyList = subjectHelper.getWrittenSocietyList(exam);
            view.society3ComboBox.setItemList(societyList);
            view.society4ComboBox.setItemList(societyList);
        } else {
            /* コンボボックスをクリアする */
            clearSubjectCombo(false);
        }

    }

    /**
     * 科目コンボボックスをクリアします。
     *
     * @param isMark マーク模試フラグ
     */
    private void clearSubjectCombo(boolean isMark) {
        for (ExmntnMainSubjectTextField field : view.findComponentsByClass(ExmntnMainSubjectTextField.class)) {
            if (field.isMark() == isMark) {
                ExmntnMainSubjectComboBox combo = field.getPairComboBox();
                if (combo != null) {
                    combo.removeAllItems();
                }
            }
        }
    }

    /**
     * マーク模試成績を初期化します。
     *
     * @param id 個人ID
     */
    public void initMarkScore(Integer id) {

        /* 入力エリアを初期化する */
        checkHelper.initInputAreaMark(true);

        /* 型の成績をクリアする */
        view.markTypeLabel.setText(StringUtils.EMPTY);

        ExamBean exam = getMarkExam();
        if (exam == null || id == null) {
            return;
        }

        /* 科目成績マップを取得する */
        Map<String, ExmntnMainSubjectRecordBean> recordMap = service.findSubRecordMap(id, exam);

        /* CEFR成績リストを取得する */
        /*
        List<ExmntnMainCefrRecordBean> cefrList = service.findCefrRecordList(id, exam);
        systemDto.setCefrInfo(cefrList);
        */
        /* 国語記述リストを取得する */
        /*
        List<ExmntnMainJpnRecordBean> jpnList = service.findJpnRecordList(id, exam);
        systemDto.setJpnInfo(jpnList);
        */

        /* 英語 */
        setMarkScore(recordMap, view.english1Field);

        /* リスニング */
        setMarkScore(recordMap, view.listeningField);

        /* CEFR */
        /*setMarkCefrScore(cefrList, view.cefr1Field);
        setMarkCefrScore(cefrList, view.cefr2Field);
        setMarkCefrScore(cefrList, view.cefrLvl1Field);
        setMarkCefrScore(cefrList, view.cefrLvl2Field);*/

        /* 現代文 */
        setMarkScore(recordMap, view.contemporaryField);

        /* 古文 */
        setMarkScore(recordMap, view.classicsField);

        /* 漢文 */
        setMarkScore(recordMap, view.chineseField);

        /* 現代文(記) */
        /*
        setMarkJpnScore(jpnList, view.jpnWritingField);
        view.jpnWritingField.setEnabled(false);
        view.jpnWritingField.setDisabledTextColor(Color.BLACK);
        */

        /* 数学（上段）*/
        setMarkScore(recordMap, view.math1Field, null);

        /* 数学（下段）*/
        setMarkScore(recordMap, view.math2Field, null);

        /* 理科専門第1 */
        setMarkScore(recordMap, view.science1Field, null);

        /* 理科専門第2  */
        setMarkScore(recordMap, view.science2Field, view.science1ComboBox.getSelectedValue());

        /* 理科基礎（上段） */
        setMarkScore(recordMap, view.science5Field, null);

        /* 理科基礎（下段） */
        setMarkScore(recordMap, view.science6Field, view.science5ComboBox.getSelectedValue());

        /* 地歴公民第1 */
        setMarkScore(recordMap, view.society1Field, null);

        /* 地歴公民第2  */
        setMarkScore(recordMap, view.society2Field, view.society1ComboBox.getSelectedValue());

        /* 型 */
        setTypeScore(recordMap, view.markTypeLabel);
    }

    /**
     * マーク：テキストフィールドに成績を設定します。
     *
     * @param recordMap 科目成績マップ
     * @param field テキストフィールド
     */
    public void setMarkScore(Map<String, ExmntnMainSubjectRecordBean> recordMap, ExmntnMainSubjectTextField field) {
        ExmntnMainSubjectRecordBean rec = recordMap.get(field.getSubjectBean().getSubCd());
        if (rec != null) {
            field.setText(rec.getScore());
        }
    }

    /**
     * マーク：テキストフィールドにCEFR成績を設定します。
     *
     * @param cefrList CEFR成績リスト
     * @param field テキストフィールド
     */
    /*
     public void setMarkCefrScore(List<ExmntnMainCefrRecordBean> cefrList, TextField field) {
        if (cefrList == null) {
            return;
        }
        if (field.equals(view.cefr1Field)) {
            if (cefrList.get(0).getCefrExamLevelName1() == null) {
                field.setText(cefrList.get(0).getCefrExamNameAbbr1());
            } else {
                field.setText(cefrList.get(0).getCefrExamNameAbbr1() + " " + cefrList.get(0).getCefrExamLevelNameAbbr1());
            }
        } else if (field.equals(view.cefr2Field)) {
            if (cefrList.get(0).getCefrExamLevelName2() == null) {
                field.setText(cefrList.get(0).getCefrExamNameAbbr2());
            } else {
                field.setText(cefrList.get(0).getCefrExamNameAbbr2() + " " + cefrList.get(0).getCefrExamLevelNameAbbr2());
            }
        } else if (field.equals(view.cefrLvl1Field)) {
            field.setText(cefrList.get(0).getCefrLevelAbbr1());
        } else if (field.equals(view.cefrLvl2Field)) {
            field.setText(cefrList.get(0).getCefrLevelAbbr2());
        }
    }
    */

    /**
     * マーク：テキストフィールドに国語記述成績を設定します。
     *
     * @param jpnList 国語記述成績リスト
     * @param field テキストフィールド
     */
    /*
    public void setMarkJpnScore(List<ExmntnMainJpnRecordBean> jpnList, TextField field) {
        if (jpnList == null) {
            return;
        }
        field.setText(jpnList.get(0).getQuestion1() + jpnList.get(0).getQuestion2() + jpnList.get(0).getQuestion3() + jpnList.get(0).getJpnTotal());
    }
    */

    /**
     * マーク：テキストフィールドとコンボボックスに成績を設定します。
     *
     * @param recordMap 科目成績マップ
     * @param text {@link ExmntnMainSubjectTextField}
     * @param alreadyRec 設定済み科目情報
     */
    private void setMarkScore(Map<String, ExmntnMainSubjectRecordBean> recordMap, ExmntnMainSubjectTextField text, BZSubjectRecord alreadyRec) {
        ExmntnMainSubjectComboBox combo = text.getPairComboBox();
        for (int i = 0; i < combo.getItemCount(); i++) {
            ComboBoxItem<BZSubjectRecord> item = combo.getItemAt(i);
            BZSubjectRecord bean = item.getValue();
            if (bean == null || (alreadyRec != null && bean.getSubCd().equals(alreadyRec.getSubCd()))) {
                continue;
            }
            ExmntnMainSubjectRecordBean record = recordMap.get(bean.getSubCd());
            if (record == null) {
                continue;
            }
            if (ANSWER_NO_1.equals(bean.getScope()) && !bean.getScope().equals(record.getScope())) {
                /* 科目の範囲区分設定が1の場合、成績の範囲区分と一致している必要がある */
                continue;
            }
            text.setText(record.getScore());
            combo.setSelectedIndex(i);
            break;
        }
    }

    /**
     * 記述模試成績を初期化します。
     *
     * @param id 個人ID
     */
    public void initWrittenScore(Integer id) {

        /* 入力エリアを初期化する */
        checkHelper.initInputAreaWritten(true);

        /* 型の成績をクリアする */
        view.writtenTypeLabel.setText(StringUtils.EMPTY);

        ExamBean exam = getWrittenExam();
        if (exam == null || id == null) {
            return;
        }

        /* 科目成績マップを取得する */
        Map<String, ExmntnMainSubjectRecordBean> recordMap = service.findSubRecordMap(id, exam);

        /* 英語 */
        ExmntnMainSubjectRecordBean english = recordMap.get(view.english2Field.getSubjectBean().getSubCd());
        if (english != null) {
            view.english2Field.setText(english.getCDeviation());
            view.english2Field.setScore(english.getScore());
            view.listeningCheckBox.setSelected(!RANGE_SECOND_LOWER.equals(english.getScope()));
        }

        /* 数学 */
        setWrittenScore(recordMap, view.math3Field);

        /* 現古漢 */
        setWrittenScore(recordMap, view.contemporaryClassicsChineseField);

        /* 理科専門上段 */
        setWrittenScore(recordMap, view.science3Field, null);

        /* 理科専門下段 */
        setWrittenScore(recordMap, view.science4Field, view.science3ComboBox.getSelectedValue());

        /* 理科基礎上段 */
        setWrittenScore(recordMap, view.science7Field, null);

        /* 理科基礎下段 */
        setWrittenScore(recordMap, view.science8Field, view.science7ComboBox.getSelectedValue());

        /* 地歴公民上段 */
        setWrittenScore(recordMap, view.society3Field, null);

        /* 地歴公民下段 */
        setWrittenScore(recordMap, view.society4Field, view.society3ComboBox.getSelectedValue());

        /* 型 */
        setTypeScore(recordMap, view.writtenTypeLabel);
    }

    /**
     * 記述：テキストフィールドとコンボボックスに成績を設定します。
     *
     * @param recordMap 科目成績マップ
     * @param text {@link ExmntnMainSubjectTextField}
     */
    private void setWrittenScore(Map<String, ExmntnMainSubjectRecordBean> recordMap, ExmntnMainSubjectTextField text) {
        ExmntnMainSubjectComboBox combo = text.getPairComboBox();
        for (int i = 0; i < combo.getItemCount(); i++) {
            ComboBoxItem<BZSubjectRecord> item = combo.getItemAt(i);
            BZSubjectRecord bean = item.getValue();
            if (bean == null) {
                continue;
            }
            ExmntnMainSubjectRecordBean record = recordMap.get(bean.getSubCd());
            if (record == null) {
                continue;
            }
            text.setText(record.getCDeviation());
            text.setScore(record.getScore());
            combo.setSelectedIndex(i);
            break;
        }
    }

    /**
     * 記述：テキストフィールドとコンボボックスに成績を設定します。
     *
     * @param recordMap 科目成績マップ
     * @param text {@link ExmntnMainSubjectTextField}
     * @param alreadyRec 設定済み科目情報
     */
    private void setWrittenScore(Map<String, ExmntnMainSubjectRecordBean> recordMap, ExmntnMainSubjectTextField text, BZSubjectRecord alreadyRec) {
        ExmntnMainSubjectComboBox combo = text.getPairComboBox();
        for (int i = 0; i < combo.getItemCount(); i++) {
            ComboBoxItem<BZSubjectRecord> item = combo.getItemAt(i);
            BZSubjectRecord bean = item.getValue();
            if (bean == null || (alreadyRec != null && bean.getSubCd().equals(alreadyRec.getSubCd()) && StringUtils.equals(bean.getScope(), alreadyRec.getScope()))) {
                continue;
            }
            ExmntnMainSubjectRecordBean record = recordMap.get(bean.getSubCd());
            if (record == null) {
                continue;
            }
            if (alreadyRec != null && record.getSubCd().equals(alreadyRec.getSubCd()) && StringUtils.equals(record.getScope(), alreadyRec.getScope())) {
                continue;
            }
            if (RANGE_SECOND_LOWER.equals(bean.getScope()) && !bean.getScope().equals(record.getScope())) {
                /* 科目の範囲区分設定が1の場合、成績の範囲区分と一致している必要がある */
                continue;
            }
            text.setText(record.getCDeviation());
            text.setScore(record.getScore());
            combo.setSelectedIndex(i);
            break;
        }
    }

    /**
     * 型の成績を設定します。
     *
     * @param recordMap 科目成績マップ
     * @param label 型の成績のテキストラベル
     */
    private void setTypeScore(Map<String, ExmntnMainSubjectRecordBean> recordMap, TextLabel label) {
        ExmntnMainSubjectRecordBean bean = findTypeRecord(recordMap);
        if (bean != null) {
            if (!StringUtils.isEmpty(bean.getScore()) && !StringUtils.isEmpty(bean.getSubAllotPntStr())) {
                label.setText(String.format(TYPE_SCORE_FORMAT, bean.getSubName(), bean.getScore(), bean.getSubAllotPntStr()));
            } else {
                label.setText(String.format(TYPE_DEVIATION_FORMAT, bean.getSubName(), StringUtils.defaultString(bean.getCDeviation())));
            }
        } else {
            label.setText(StringUtils.EMPTY);
        }
    }

    /**
     * 科目成績マップから型の成績を取り出します。
     *
     * @param recordMap 科目成績マップ
     * @return 型の成績
     */
    private ExmntnMainSubjectRecordBean findTypeRecord(Map<String, ExmntnMainSubjectRecordBean> recordMap) {
        for (ExmntnMainSubjectRecordBean bean : recordMap.values()) {
            if (bean.getSubCd().compareTo("7000") >= 0) {
                return bean;
            }
        }
        return null;
    }

    /**
     * 画面に表示している情報から成績データを生成します。
     *
     * @return 成績データ
     */
    public ScoreBean createDispScore() {
        return createDispScore(getMarkExam(), getWrittenExam());
    }

    /**
     * 画面に表示している情報から成績データを生成します。
     *
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @return 成績データ
     */
    public ScoreBean createDispScore(ExamBean markExam, ExamBean writtenExam) {

        /* マーク模試成績 */
        List<BZSubjectRecord> marks = null;
        /* CEFRスコア */
        List<EngSSScoreData> cefr = null;
        if (markExam != null && !view.errorMessageChar1Label.isVisible()) {
            marks = CollectionsUtil.newArrayList();
            /* 英語 */
            addSubjectRecord(marks, view.english1Field, markExam);
            /* リスニング */
            addSubjectRecord(marks, view.listeningField, markExam);
            /* 数学（上段）  */
            addSubjectRecord(marks, view.math1Field, markExam);
            /* 数学（下段）  */
            addSubjectRecord(marks, view.math2Field, markExam);
            /* 現代文 */
            addSubjectRecord(marks, view.contemporaryField, markExam);
            /* 古文 */
            addSubjectRecord(marks, view.classicsField, markExam);
            /* 漢文 */
            addSubjectRecord(marks, view.chineseField, markExam);
            /* 現代文(記) */
            /*addSubjectRecordJpn(marks, view.jpnWritingField); */
            /* 理科専門第1  */
            addSubjectRecord(marks, view.science1Field, markExam);
            /* 理科専門第2  */
            addSubjectRecord(marks, view.science2Field, markExam);
            /* 理科基礎（上段）  */
            addSubjectRecord(marks, view.science5Field, markExam);
            /* 理科基礎（下段）  */
            addSubjectRecord(marks, view.science6Field, markExam);
            /* 地歴公民第1  */
            addSubjectRecord(marks, view.society1Field, markExam);
            /* 地歴公民第2  */
            addSubjectRecord(marks, view.society2Field, markExam);
            /* 成績を変更していたら総合を空欄にする */
            if (dto.isChangeMarkScore()) {
                view.markTypeLabel.setText(StringUtils.EMPTY);
            }
            cefr = CollectionsUtil.newArrayList();
            addCefrRecord(cefr);
        }

        /* 記述模試成績 */
        List<BZSubjectRecord> writtens = null;
        if (writtenExam != null && !view.errorMessageChar2Label.isVisible()) {
            writtens = CollectionsUtil.newArrayList();
            /* 英語 */
            addSubjectRecord(writtens, view.english2Field, writtenExam);
            /* 数学 */
            addSubjectRecord(writtens, view.math3Field, writtenExam);
            /* 国語  */
            addSubjectRecord(writtens, view.contemporaryClassicsChineseField, writtenExam);
            /* 理科専門（上段） */
            addSubjectRecord(writtens, view.science3Field, writtenExam);
            /* 理科専門（下段） */
            addSubjectRecord(writtens, view.science4Field, writtenExam);
            /* 理科基礎（上段） */
            addSubjectRecord(writtens, view.science7Field, writtenExam);
            /* 理科基礎（下段） */
            addSubjectRecord(writtens, view.science8Field, writtenExam);
            /* 地歴公民（上段） */
            addSubjectRecord(writtens, view.society3Field, writtenExam);
            /* 地歴公民（下段）  */
            addSubjectRecord(writtens, view.society4Field, writtenExam);
            /* 成績を変更していたら総合を空欄にする */
            if (dto.isChangeWrittenScore()) {
                view.writtenTypeLabel.setText(StringUtils.EMPTY);
            }
        }

        return new ScoreBean(markExam, service.preprocess(markExam, marks), writtenExam, writtens, cefr);
    }

    /**
     * テキストフィールドの成績を成績リストに追加します。
     *
     * @param list 成績リスト
     * @param field テキストフィールド
     * @param exam 模試データ
     */
    private void addSubjectRecord(List<BZSubjectRecord> list, ExmntnMainSubjectTextField field, ExamBean exam) {

        String text = field.getText();
        if (text.isEmpty()) {
            return;
        }

        BZSubjectRecord subject = field.getSubjectBean();
        BZSubjectRecord subject2 = null;
        if (subject == null) {
            return;
        }
        Integer scoring = service.findScoringList(subject.getSubCd(), exam);
        if (field.isMark()) {
            subject2 = new BZSubjectRecord(subject.getSubCd(), text, null, null, subject.getScope(), String.valueOf(scoring));
        } else {
            if (!dto.isChangeWrittenScore()) {
                subject2 = new BZSubjectRecord(subject.getSubCd(), field.getScore(), null, text, subject.getScope(), String.valueOf(scoring));
            } else {
                subject2 = new BZSubjectRecord(subject.getSubCd(), null, null, text, subject.getScope(), String.valueOf(scoring));
            }
        }
        if (scoring == 0) {
            subject2.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
        } else {
            subject2.setSubjectAllotPnt(String.valueOf(scoring));
        }
        list.add(subject2);
    }

    /**
     * CEFR成績を成績リストに追加します。
     *
     * @param list 成績リスト
     */
    private void addCefrRecord(List<EngSSScoreData> list) {
        List<ExmntnMainCefrRecordBean> cefrList = null;
        cefrList = CollectionsUtil.newArrayList();
        /*
        cefrList = systemDto.getCefrInfo();
        */
        /*
        if (!systemDto.isTeacher() && (cefrList == null || cefrList.size() == 0 || cefrList.get(0).getCefrExamCd1() == null || cefrList.get(0).getCefrExamCd2() == null)) {
        */
        ExmntnMainCefrRecordBean cefrdata = new ExmntnMainCefrRecordBean();
        cefrList = CollectionsUtil.newArrayList();
        cefrList.add(cefrdata);
        cefrList.get(0).setIndividualId(0);
        cefrList.get(0).setExamYear(StringUtils.EMPTY);
        cefrList.get(0).setExamDiv(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamCd1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamCd2(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamName1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamName2(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamNameAbbr1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamNameAbbr2(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevel1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevel2(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevelName1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevelName2(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevelNameAbbr1(StringUtils.EMPTY);
        cefrList.get(0).setCefrExamLevelNameAbbr2(StringUtils.EMPTY);
        cefrList.get(0).setCefrScore1(0.0);
        cefrList.get(0).setCefrScore2(0.0);
        cefrList.get(0).setCefrLevelCd1(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevelCd2(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevel1(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevel2(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevelAbbr1(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevelAbbr2(StringUtils.EMPTY);
        cefrList.get(0).setScoreCorrectFlg1(StringUtils.EMPTY);
        cefrList.get(0).setScoreCorrectFlg2(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevelCdCorrectFlg1(StringUtils.EMPTY);
        cefrList.get(0).setCefrLevelCdCorrectFlg2(StringUtils.EMPTY);
        cefrList.get(0).setExamResult1(StringUtils.EMPTY);
        cefrList.get(0).setExamResult2(StringUtils.EMPTY);
        cefrList.get(0).setExamResultCorrectFlg1(StringUtils.EMPTY);
        cefrList.get(0).setExamResultCorrectFlg2(StringUtils.EMPTY);
        cefrList.get(0).setUnknownScore1(StringUtils.EMPTY);
        cefrList.get(0).setUnknownScore2(StringUtils.EMPTY);
        cefrList.get(0).setUnknownCefr1(StringUtils.EMPTY);
        cefrList.get(0).setUnknownCefr2(StringUtils.EMPTY);
        systemDto.setCefrInfo(cefrList);
        /*
         * }
         */
        EngSSScoreData cefrInfo = new EngSSScoreData();
        cefrInfo.setEngSSCode(cefrList.get(0).getCefrExamCd1());
        cefrInfo.setEngSSLevelCode(cefrList.get(0).getCefrExamLevel1());
        if (cefrList.get(0).getCefrScore1() != null) {
            cefrInfo.setEngScore(cefrList.get(0).getCefrScore1());
        } else {
            cefrInfo.setEngScore(0);
        }
        list.add(cefrInfo);

        EngSSScoreData cefrInfo2 = new EngSSScoreData();
        cefrInfo2 = new EngSSScoreData();
        cefrInfo2.setEngSSCode(cefrList.get(0).getCefrExamCd2());
        cefrInfo2.setEngSSLevelCode(cefrList.get(0).getCefrExamLevel2());
        if (cefrList.get(0).getCefrScore2() != null) {
            cefrInfo2.setEngScore(cefrList.get(0).getCefrScore2());
        } else {
            cefrInfo2.setEngScore(0);
        }
        list.add(cefrInfo2);

        /*
        List<ExmntnMainCefrRecordBean> cefrChnageList = CollectionsUtil.newArrayList();
        
        ExmntnMainCefrRecordBean cefrChangedata = new ExmntnMainCefrRecordBean();
        cefrChnageList.add(cefrChangedata);
        cefrChnageList.get(0).setIndividualId(cefrList.get(0).getIndividualId());
        cefrChnageList.get(0).setExamYear(cefrList.get(0).getExamYear());
        cefrChnageList.get(0).setExamDiv(cefrList.get(0).getExamDiv());
        cefrChnageList.get(0).setCefrExamCd1(cefrList.get(0).getCefrExamCd1());
        cefrChnageList.get(0).setCefrExamCd2(cefrList.get(0).getCefrExamCd2());
        cefrChnageList.get(0).setCefrExamName1(cefrList.get(0).getCefrExamName1());
        cefrChnageList.get(0).setCefrExamName2(cefrList.get(0).getCefrExamName2());
        cefrChnageList.get(0).setCefrExamNameAbbr1(cefrList.get(0).getCefrExamNameAbbr1());
        cefrChnageList.get(0).setCefrExamNameAbbr2(cefrList.get(0).getCefrExamNameAbbr2());
        cefrChnageList.get(0).setCefrExamLevel1(cefrList.get(0).getCefrExamLevel1());
        cefrChnageList.get(0).setCefrExamLevel2(cefrList.get(0).getCefrExamLevel2());
        cefrChnageList.get(0).setCefrExamLevelName1(cefrList.get(0).getCefrExamLevelName1());
        cefrChnageList.get(0).setCefrExamLevelName2(cefrList.get(0).getCefrExamLevelName2());
        cefrChnageList.get(0).setCefrExamLevelNameAbbr1(cefrList.get(0).getCefrExamLevelNameAbbr1());
        cefrChnageList.get(0).setCefrExamLevelNameAbbr2(cefrList.get(0).getCefrExamLevelNameAbbr2());
        cefrChnageList.get(0).setCefrScore1(cefrList.get(0).getCefrScore1());
        cefrChnageList.get(0).setCefrScore2(cefrList.get(0).getCefrScore2());
        cefrChnageList.get(0).setCefrLevelCd1(cefrList.get(0).getCefrLevelCd1());
        cefrChnageList.get(0).setCefrLevelCd2(cefrList.get(0).getCefrLevelCd2());
        cefrChnageList.get(0).setCefrLevel1(cefrList.get(0).getCefrLevel1());
        cefrChnageList.get(0).setCefrLevel2(cefrList.get(0).getCefrLevel2());
        cefrChnageList.get(0).setCefrLevelAbbr1(cefrList.get(0).getCefrLevelAbbr1());
        cefrChnageList.get(0).setCefrLevelAbbr2(cefrList.get(0).getCefrLevelAbbr2());
        cefrChnageList.get(0).setScoreCorrectFlg1(cefrList.get(0).getScoreCorrectFlg1());
        cefrChnageList.get(0).setScoreCorrectFlg2(cefrList.get(0).getScoreCorrectFlg2());
        cefrChnageList.get(0).setCefrLevelCdCorrectFlg1(cefrList.get(0).getCefrLevelCdCorrectFlg1());
        cefrChnageList.get(0).setCefrLevelCdCorrectFlg2(cefrList.get(0).getCefrLevelCdCorrectFlg2());
        cefrChnageList.get(0).setExamResult1(cefrList.get(0).getExamResult1());
        cefrChnageList.get(0).setExamResult2(cefrList.get(0).getExamResult2());
        cefrChnageList.get(0).setExamResultCorrectFlg1(cefrList.get(0).getExamResultCorrectFlg1());
        cefrChnageList.get(0).setExamResultCorrectFlg2(cefrList.get(0).getExamResultCorrectFlg2());
        cefrChnageList.get(0).setUnknownScore1(cefrList.get(0).getUnknownScore1());
        cefrChnageList.get(0).setUnknownScore2(cefrList.get(0).getUnknownScore2());
        cefrChnageList.get(0).setUnknownCefr1(cefrList.get(0).getUnknownCefr1());
        cefrChnageList.get(0).setUnknownCefr2(cefrList.get(0).getUnknownCefr2());
        systemDto.setCefrChangeInfo(cefrChnageList);
        */
    }

    /**
     * 国語記述の成績を成績リストに追加します。
     *
     * @param list 成績リスト
     * @param field テキストフィールド
     */
    /*
    private void addSubjectRecordJpn(List<BZSubjectRecord> list, TextField field) {
        List<ExmntnMainJpnRecordBean> jpnList = null;
        jpnList = systemDto.getJpnInfo();
        if (!systemDto.isTeacher() && (jpnList == null || jpnList.size() == 0 || jpnList.get(0).getJpnTotal() == null)) {
            jpnList = CollectionsUtil.newArrayList();
            ExmntnMainJpnRecordBean jpndata = new ExmntnMainJpnRecordBean();
            jpnList.add(jpndata);
            jpnList.get(0).setIndividualId(0);
            jpnList.get(0).setExamYear(StringUtils.EMPTY);
            jpnList.get(0).setExamDiv(StringUtils.EMPTY);
            jpnList.get(0).setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
            jpnList.get(0).setQuestion1(StringUtils.EMPTY);
            jpnList.get(0).setQuestion2(StringUtils.EMPTY);
            jpnList.get(0).setQuestion3(StringUtils.EMPTY);
            jpnList.get(0).setJpnTotal(StringUtils.EMPTY);
            systemDto.setJpnInfo(jpnList);
        }
        BZSubjectRecord jpnrec = new BZSubjectRecord();
        jpnrec.setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
        jpnrec.setScore(null);
        jpnrec.setCvsScore(null);
        jpnrec.setCDeviation(null);
        jpnrec.setScope("99");
        jpnrec.setJpnHyoka1(jpnList.get(0).getQuestion1());
        jpnrec.setJpnHyoka2(jpnList.get(0).getQuestion2());
        jpnrec.setJpnHyoka3(jpnList.get(0).getQuestion3());
        jpnrec.setJpnHyokaAll(jpnList.get(0).getJpnTotal());
        jpnrec.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
        list.add(jpnrec);
    }
    */
    /**
     * 第1,第2入れ替えボタン押下アクションです。
     *
     * @param field1 第1成績のテキストフィールド
     * @param field2 第2成績のテキストフィールド
     */
    public void changeFirstSecondButtonAction(ExmntnMainSubjectTextField field1, ExmntnMainSubjectTextField field2) {

        ComboBox<?> combo1 = field1.getPairComboBox();
        ComboBox<?> combo2 = field2.getPairComboBox();

        int index1 = combo1.getSelectedIndex();
        String score1 = field1.getText();

        int index2 = combo2.getSelectedIndex();
        String score2 = field2.getText();

        combo1.setSelectedIndex(index2);
        field1.setText(score2);

        combo2.setSelectedIndex(index1);
        field2.setText(score1);

        checkHelper.checkMark();
    }
}

package jp.ac.kawai_juku.banzaisystem.mainte.stng.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CONFIG_FILE_NAME;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.InputFileChooser;
import jp.ac.kawai_juku.banzaisystem.framework.component.PasswordField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.ConfigProps;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.dnet.controller.MainteDnetController;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.service.MainteStngService;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.view.MainteStngView;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtService;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * メンテナンス-環境設定画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichiro
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteStngController extends SubController {

    /** 環境設定ファイルのファイルフィルタ */
    private final FileFilter configFileFilter = new FileFilter() {

        @Override
        public String getDescription() {
            return "環境設定ファイル（" + CONFIG_FILE_NAME + "）";
        }

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().equals(CONFIG_FILE_NAME);
        }
    };

    /** View */
    @Resource(name = "mainteStngView")
    private MainteStngView view;

    /** Service */
    @Resource(name = "mainteStngService")
    private MainteStngService service;

    /** 一覧出力ダイアログのサービス */
    @Resource
    private SelstdDprtService selstdDprtService;

    /** 初期化処理サービス */
    @Resource
    private InitializeService initializeService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 変更前の模試 */
    private ExamBean beforeExam;

    /** 変更前の学年 */
    private Integer beforeGrade;

    @Override
    @Logging(GamenId.E4)
    public void activatedAction() {

        /* 『別の環境設定ファイル』のパスをクリアする */
        view.anotherConfigFileField.setText(StringUtils.EMPTY);

        /* 模試コンボボックスを初期化する */
        initExamComboBox();

        /* パスワードを初期化する */
        for (PasswordField c : view.findComponentsByClass(PasswordField.class)) {
            c.setText(StringUtils.EMPTY);
        }
    }

    /**
     * 模試コンボボックスを初期化します。
     */
    private void initExamComboBox() {
        List<ComboBoxItem<ExamBean>> itemList = CollectionsUtil.newArrayList();
        for (ExamBean exam : service.findExamList()) {
            itemList.add(new ComboBoxItem<ExamBean>(exam));
        }
        if (!itemList.isEmpty()) {
            itemList.add(0, new ComboBoxItem<ExamBean>(getMessage("label.mainte.stng.allExam"), null));
        }
        view.examComboBox.setItemList(itemList);
    }

    /**
     * 対象模試コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult examComboBoxAction() {
        /* 模試変更時に全学年・全クラスになるように学年とクラスコンボボックスをクリアしておく */
        ExamBean exam = view.examComboBox.getSelectedValue();
        if (!ObjectUtils.equals(beforeExam, exam)) {
            view.gradeComboBox.removeAllItems();
            view.classComboBox.removeAllItems();
        }
        beforeExam = exam;
        initGradeComboBox();
        return null;
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        ExamBean exam = view.examComboBox.getSelectedValue();
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
        for (Integer grade : service.findGradeList(systemDto.getUnivYear(), exam)) {
            itemList.add(new ComboBoxItem<Integer>(grade));
        }
        if (!itemList.isEmpty()) {
            itemList.add(0, new ComboBoxItem<Integer>(getMessage("label.mainte.stng.allGrade"), -1));
        }
        view.gradeComboBox.setItemList(itemList);
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        /* 学年を全学年に変更した際に、全クラスになるように学年コンボボックスをクリアしておく */
        Integer grade = view.gradeComboBox.getSelectedValue();
        if (!ObjectUtils.equals(beforeGrade, grade) && grade != null && grade.intValue() < 0) {
            view.classComboBox.removeAllItems();
        }
        beforeGrade = grade;
        initClassComboBox();
        return null;
    }

    /**
     * クラスコンボボックスを初期化します。
     */
    private void initClassComboBox() {
        ExamBean exam = view.examComboBox.getSelectedValue();
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade != null) {
            List<ComboBoxItem<String>> itemList = CollectionsUtil.newArrayList();
            for (String cls : service.findClassList(systemDto.getUnivYear(), exam, grade.getValue())) {
                itemList.add(new ComboBoxItem<String>(cls));
            }
            if (!itemList.isEmpty()) {
                itemList.add(0, new ComboBoxItem<String>(getMessage("label.mainte.stng.allClass"), StringUtils.EMPTY));
            }
            view.classComboBox.setItemList(itemList);
        } else {
            view.classComboBox.removeAllItems();
        }
    }

    /**
     * 「削除(データ削除)」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult deleteButtonAction() {
        if (showConfirmDialog()) {
            ComboBoxItem<ExamBean> examItem = view.examComboBox.getSelectedItem();
            if (examItem != null) {
                ExamBean exam = examItem.getValue();
                service.deleteRecord(systemDto.getUnivYear(), exam == null ? null : exam.getExamCd(), view.gradeComboBox.getSelectedValue(), view.classComboBox.getSelectedValue());
                initExamComboBox();
                /* 選択中の生徒をクリアする */
                systemDto.clearSelectedIndividualIdList();
            }
        }
        return null;
    }

    /**
     * 処理継続確認ダイアログを表示します。
     *
     * @return 処理継続ならtrue
     */
    private boolean showConfirmDialog() {
        String[] button = { getMessage("label.mainte.stng.dialogYes"), getMessage("label.mainte.stng.dialogNo") };
        int result = JOptionPane.showOptionDialog(getActiveWindow(), getMessage("label.mainte.stng.E_W004"), null, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

    /**
     * 「現在の環境設定を書き出す」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult putCurrentSettingButtonAction() {

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int selected = fileChooser.showOpenDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            try {
                IOUtil.copyFileToDirectory(getConfigFile(), fileChooser.getSelectedFile());
            } catch (IORuntimeException e) {
                throw new MessageDialogException("", e);
            }
        }

        return null;
    }

    /**
     * 環境設定ファイルを返します。
     *
     * @return 環境設定ファイル
     */
    private File getConfigFile() {
        return new File(SettingPropsUtil.getConfigDir(), CONFIG_FILE_NAME);
    }

    /**
     * 「参照(別の『環境設定ファイル』を読み込む)」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult referenceButtonAction() {

        JFileChooser fileChooser = new InputFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(configFileFilter);

        int selected = fileChooser.showOpenDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            view.anotherConfigFileField.setText(fileChooser.getSelectedFile().getAbsolutePath());
        }

        return null;
    }

    /**
     * 「読み込み」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult loadingButtonAction() {

        File file = new File(view.anotherConfigFileField.getText());

        /* 環境設定ファイルの妥当性をチェックする */
        if (!checkConfigFile(file)) {
            throw new MessageDialogException("");
        }

        /* 環境設定ファイルを上書きする */
        File currentFile = getConfigFile();
        if (!IOUtil.isIdentical(file, currentFile)) {
            try {
                FileUtils.copyFile(file, currentFile, false);
            } catch (IOException e) {
                throw new MessageDialogException("", e);
            }
            /* 画面を再表示する */
            activatedAction();
        }

        return null;
    }

    /**
     * 環境設定ファイルの妥当性をチェックします。
     *
     * @param file 環境設定ファイル
     * @return チェック結果
     */
    private boolean checkConfigFile(File file) {

        if (!configFileFilter.accept(file)) {
            return false;
        }

        List<String> lines;
        try {
            lines = FileUtils.readLines(file);
        } catch (IOException e) {
            return false;
        }

        if (lines.isEmpty()) {
            return false;
        }

        return lines.get(0).equals("#" + CONFIG_FILE_NAME);
    }

    /**
     * 教員用パスワード変更アクションです・
     *
     * @return なし
     */
    @Execute
    public ExecuteResult changeTeacherPasswordButtonAction() {
        service.changeTeacherPassword(view.teachNewPassField.getStringPassword(), view.teachRePassField.getStringPassword());
        clearPasswordField(MainteStngView.GROUP_TPASSWORD);
        return null;
    }

    /**
     * 管理者用パスワード変更アクションです・
     *
     * @return なし
     */
    @Execute
    public ExecuteResult changeAdminPasswordButtonAction() {
        service.changeAdminPassword(view.adminCurrentPassField.getStringPassword(), view.adminNewPassField.getStringPassword(), view.adminRePassField.getStringPassword());
        clearPasswordField(MainteStngView.GROUP_APASSWORD);
        return null;
    }

    /**
     * 指定したグループのパスワードフィールドをクリアします。
     *
     * @param group グループ番号
     */
    private void clearPasswordField(int group) {
        for (JComponent c : view.findComponentsByGroup(group)) {
            if (c instanceof PasswordField) {
                ((PasswordField) c).setText(StringUtils.EMPTY);
            }
        }
    }

    /**
     * 「設定」ボタン押下アクションです。
     *
     * @return ネットワーク利用時ダイアログ
     */
    @Execute
    public ExecuteResult settingButtonAction() {
        service.checkAdminPassword(view.adminPassField.getStringPassword());
        view.adminPassField.setText(StringUtils.EMPTY);
        return forward(MainteDnetController.class);
    }

    /**
     * 「この環境設定情報を保存する」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult saveSettingInfoButtonAction() {
        ConfigProps config = loadConfigProps();
        /*
        config.setValue(ConfigKey.ROLLOVER_MONTH, ObjectUtils.toString(view.monthComboBox.getSelectedValue()));
        config.setValue(ConfigKey.ROLLOVER_DATE, ObjectUtils.toString(view.dayComboBox.getSelectedValue()));
        */
        config.store();
        return null;
    }

    /**
     * 環境設定ファイルを読み込みます。
     *
     * @return {@link ConfigProps}
     */
    private ConfigProps loadConfigProps() {
        try {
            return ConfigProps.load();
        } catch (MessageDialogException e) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E013"), e);
        }
    }

}

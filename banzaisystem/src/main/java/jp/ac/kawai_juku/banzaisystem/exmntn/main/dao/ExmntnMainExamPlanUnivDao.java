package jp.ac.kawai_juku.banzaisystem.exmntn.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainExamPlanUnivDaoInBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.extension.jdbc.IterationCallback;

/**
 *
 * 受験予定大学に関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainExamPlanUnivDao extends BaseDao {

    /**
     * 受験予定大学リストを取得します。
     *
     * @param id 個人ID
     * @return 受験予定大学リスト
     */
    public List<UnivKeyBean> selectExamPlanUnivList(Integer id) {
        return jdbcManager.selectBySqlFile(UnivKeyBean.class, getSqlPath(), id).getResultList();
    }

    /**
     * 受験予定大学リストを登録します。
     *
     * @param list 受験予定大学リスト
     */
    public void insertExamPlanUniv(List<ExmntnMainExamPlanUnivDaoInBean> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * 入試日リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @param callback 結果を処理するコールバッククラス
     * @return 取得処理が中断されたらtrue
     */
    public Boolean selectInpleDateList(UnivKeyBean key, IterationCallback<String, Boolean> callback) {
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), key).iterate(callback);
    }

}

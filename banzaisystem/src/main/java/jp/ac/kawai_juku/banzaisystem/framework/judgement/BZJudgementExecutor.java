package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCandidateUnivBean;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.beans.judge.JudgementExecutor;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Judge;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.StringUtils;
/*import org.seasar.framework.log.Logger;*/
import org.apache.log4j.Logger;

/**
 *
 * 判定実行クラスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZJudgementExecutor {

    /** Singletonインスタンス */
    private static final BZJudgementExecutor INSTANCE = new BZJudgementExecutor();

    private static final Logger logger = Logger.getLogger("jlog");
    private static final SettingProps props = SettingProps.load();

    /** 大学インスタンスファクトリ */
    private final BZUnivFactory factory = new BZUnivFactory();

    /** システム情報DTO */
    private final SystemDto systemDto;

    /**
     * コンストラクタです。
     */
    private BZJudgementExecutor() {
        systemDto = getComponent(SystemDto.class);
    }

    /**
     * インスタンスを返します。
     *
     * @return {@link BZJudgementExecutor}
     */
    public static BZJudgementExecutor getInstance() {
        return INSTANCE;
    }

    /**
     * 大学インスタンスを生成します。
     *
     * @param univKeyBean {@link UnivKeyBean}
     * @return 大学インスタンス
     */
    public UnivData createUniv(UnivKeyBean univKeyBean) {
        return factory.createUniv(systemDto, univKeyBean);
    }

    /**
     * （帳票用・志望大学用）判定を実行します。
     *
     * @param score 成績データ
     * @param resultList {@link JudgementResultBean} のリスト
     */
    public void judge(ScoreBean score, List<? extends JudgementResultBean> resultList) {
        for (JudgementResultBean resultBean : resultList) {
            judge(score, createUniv(resultBean), resultBean);
        }
    }

    /**
     * （帳票用・志望大学用）判定を実行します。
     *
     * @param scoreList 成績データリスト
     * @param resultList {@link JudgementResultBean} のリスト
     */
    public void judge(Map<Integer, ScoreBean> scoreList, List<SelstdMainCandidateUnivBean> resultList) {
        for (SelstdMainCandidateUnivBean resultBean : resultList) {
            if (!StringUtils.isEmpty(resultBean.getUnivCd())) {
                ScoreBean score = scoreList.get(resultBean.getIndividualId());
                judge(score, createUniv(resultBean), resultBean);
            }
        }
    }

    /**
     * （大学検索用）判定を実行します。
     *
     * @param score 成績データ
     * @param univ 大学キインスタンス
     * @param resultBean {@link JudgementResultBean}
     */
    public void judge(ScoreBean score, UnivData univ, JudgementResultBean resultBean) {
        setJudgedResult(resultBean, judge(score, univ));
    }

    /**
     * （詳細画面用）判定を実行します。
     *
     * @param score 成績データ
     * @param univKeyBean {@link UnivKeyBean}
     * @return JudgedResult 判定結果Bean
     */
    public JudgedResult judge(ScoreBean score, UnivKeyBean univKeyBean) {
        return judge(score, createUniv(univKeyBean));
    }

    /**
     * 判定を実行します。
     *
     * @param score 成績データ
     * @param univ 大学キインスタンス
     * @return JudgedResult 判定結果
     */
    public JudgedResult judge(ScoreBean score, UnivData univ) {
        try {
            Score judgeScore = score.getScore();
            // 共通テストリサーチでの一般私大判定の場合、成績データをクリアする
            boolean isCenPrivate = isCenterPrivate(score, univ);
            if (isCenPrivate) {
                judgeScore = new Score(null, null, null, null);
                judgeScore.setEngSSScoreDatas(new ArrayList<EngSSScoreData>());
            }
            // 成績情報をデバッグ出力
            if (BZConstants.FLG_ON.equals(props.getValue(SettingKey.DEBUG_SCORE))) {
                JudgedUtil.outputScoreDebugLog(logger, judgeScore);
            }

            // 判定処理を実行
            JudgementExecutor ex = new JudgementExecutor(univ, judgeScore);
            ex.execute();

            // 判定結果をPCバンザイ表示用に整形する
            JudgedResult result = new JudgedResult(ex.getJudgement()) {
                /** SerialVersion */
                private static final long serialVersionUID = -3308013216062738350L;

                @Override
                public void execute() throws Exception {
                    super.execute();
                    // 共通テストリサーチでの一般私大判定は*判定とする
                    if (isCenPrivate) {
                        sLetterJudgement = Judge.JUDGE_STR_X;
                    }
                }
            };
            result.execute();

            // 判定結果をデバッグ出力
            if (BZConstants.FLG_ON.equals(props.getValue(SettingKey.DEBUG_RESULT))) {
                JudgedUtil.outputResultDebugLog(logger, result.getBean());
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 共通テストリサーチでの一般私大判定かどうか判定します。
     *
     * @param score 成績データ
     * @param univ 大学キインスタンス
     * @return
     */
    private boolean isCenterPrivate(ScoreBean score, UnivData univ) {
        return score.getMarkExam() != null && ExamUtil.isCenter(score.getMarkExam()) && score.getScore().getWrtn() == null && Univ.SCHEDULE_NML.equals(univ.getSchedule());
    }

    /**
     * 判定結果を判定結果Beanにセットします。
     *
     * @param bean 判定結果Bean
     * @param result 判定結果
     */
    private void setJudgedResult(JudgementResultBean bean, JudgedResult result) {

        /*
         * ここでは大学検索結果と志望大学一覧の表示に必要な情報のみを設定する。
         * 科目配点等は詳細画面を表示する際に再度判定することで取得する。
         */

        /* 日程コード */
        bean.setScheduleCd(result.getScheduleCd());

        /* センター評価 */
        bean.setCenterRating(result.getCLetterJudgement());
        bean.setCLetterJudgement(result.getCLetterJudgement());

        /* 二次評価*/
        if (result.getSLetterJudgement().length() == 0) {
            bean.setSecondRating("  ");
        } else if (result.getSLetterJudgement().length() == 1) {
            bean.setSecondRating(result.getSLetterJudgement() + " ");
        } else {
            bean.setSecondRating(result.getSLetterJudgement());
        }

        /* 総合評価 */
        if (result.getEvalTotal().getLetter().length() == 0) {
            bean.setTotalRating("  ");
        } else if (result.getEvalTotal().getLetter().length() == 1) {
            bean.setTotalRating(result.getEvalTotal().getLetter() + " ");
        } else {
            bean.setTotalRating(result.getEvalTotal().getLetter());
        }

        /* センター得点 */
        bean.setcScore(result.getC_scoreStr().isEmpty() ? null : Integer.valueOf(result.getC_scoreStr()));

        /* 二次偏差値 */
        bean.setsScore(result.getS_scoreStr().isEmpty() ? null : Double.valueOf(result.getS_scoreStr()));

        /* 総合判定ポイント */
        bean.setTotalRatingPoint(result.getEvalTotal().getPointStr());

        /* センタ配点比 */
        bean.setcAllotPntRate(toIntegerAllotPntRate(result.getCAllotPntRateAll()));

        /* 二次配点比 */
        bean.setsAllotPntRate(toIntegerAllotPntRate(result.getSAllotPntRateAll()));
        /* センタボーダ */
        if (!result.getBorderPointStr().isEmpty()) {
            bean.setBorderPoint(Integer.valueOf(result.getBorderPointStr()));
        }

        /* センタボーダ得点率 */
        int cenScoreRate = (int) result.getCenScoreRate();
        if (cenScoreRate != JudgementConstants.Univ.CEN_SCORE_RATE_NA) {
            bean.setCenScoreRate(Integer.valueOf(cenScoreRate));
        }

        /* 二次ランク */
        if (result.getRank() == JudgementConstants.Univ.RANK_FORCE) {
            bean.setSecondRank(Univ.RANK_STR_NONE);
        } else if (result.getRank() == JudgementConstants.Univ.RANK_NUM_NONE) {
            bean.setSecondRank("--");
        } else {
            bean.setSecondRank(result.getRankLLimitsStr());
        }

        /* センタ満点値 */
        bean.setcFullPoint(result.getCFullPoint());

        /* 二次満点値 */
        bean.setsFullPoint(result.getSFullPoint());

        /* 【含まない】得点 */
        /*
        if (result.getBean().getSSHanteiFlg() == 0) {
            bean.setPointOfExclude(bean.getcScore());
            if (bean.getcScore() != null) {
                double dwk = 0.0;
        */
        /* 得点率は四捨五入とする */
        /*
                dwk = ((double) bean.getcScore() / (double) Integer.valueOf(bean.getcFullPoint()) + 0.005) * 100.0;
                bean.setPointExScoreRate((int) dwk);
            }
        }
        */
        /* 得点 */
        bean.setPointOfExclude(bean.getcScore());
        if (bean.getcScore() != null) {
            double dwk = 0.0;
            /* 得点率は四捨五入とする */
            dwk = ((double) bean.getcScore() / (double) Integer.valueOf(bean.getcFullPoint()) + 0.005) * 100.0;
            bean.setPointExScoreRate((int) dwk);
        }

        /* 【含む】得点 */
        /*
        if (result.getBean().getSSHanteiFlg() == 1) {
            bean.setPointOfInclude(bean.getcScore());
            if (bean.getcScore() != null) {
                double dwk = 0.0;
        */
        /* 得点率は四捨五入とする */
        /*
                dwk = ((double) bean.getcScore() / (double) Integer.valueOf(bean.getcFullPoint()) + 0.005) * 100.0;
                bean.setPointIncScoreRate((int) dwk);
            }
        }
        */

        /* 含まない評価 */
        /*
        if (result.getBean().getSSHanteiFlg() == 0) {
            if (result.getCLetterJudgement().length() == 1) {
                bean.setValuateExclude(result.getCLetterJudgement() + " ");
            } else {
                bean.setValuateExclude(result.getCLetterJudgement());
            }
        }
        */
        /* 評価 */
        if (result.getCLetterJudgement().length() == 0) {
            bean.setValuateExclude("  ");
        } else if (result.getCLetterJudgement().length() == 1) {
            bean.setValuateExclude(result.getCLetterJudgement() + " ");
        } else {
            bean.setValuateExclude(result.getCLetterJudgement());
        }

        /* 含む評価 */
        /*
        bean.setAppReqPaternCd(result.getBean().getUniv().getAppReqPaternCd());
        String wk = "";
        if (result.getBean().getSSHanteiFlg() == 1) {
            if (result.getCLetterJudgement().length() > 0) {
                bean.setCLetterJudgement(result.getCLetterJudgement());
                if ((result.getBean().getEngSSSutuGanHanTeiKbn() == 1) && (result.getBean().getEngSSHKSutuGanHTKokuchi() != 1)) {
                    bean.setEngSSSutuGanHanTeiKbn("〇");
                    bean.setEngSSHKSutuGanHTKokuchi("");
                    wk = "〇  ";
                } else if ((result.getBean().getEngSSSutuGanHanTeiKbn() == 2) && (result.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
                    bean.setEngSSSutuGanHanTeiKbn("△");
                    bean.setEngSSHKSutuGanHTKokuchi("！");
                    wk = "△! ";
                } else if ((result.getBean().getEngSSSutuGanHanTeiKbn() == 3) && (result.getBean().getEngSSHKSutuGanHTKokuchi() != 1)) {
                    bean.setEngSSSutuGanHanTeiKbn("×");
                    bean.setEngSSHKSutuGanHTKokuchi("");
                    wk = "×  ";
                } else if ((result.getBean().getEngSSSutuGanHanTeiKbn() == 3) && (result.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
                    bean.setEngSSSutuGanHanTeiKbn("×");
                    bean.setEngSSHKSutuGanHTKokuchi("！");
                    wk = "×! ";
                } else if ((result.getBean().getEngSSSutuGanHanTeiKbn() == 4) && (result.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
                    bean.setEngSSSutuGanHanTeiKbn("");
                    bean.setEngSSHKSutuGanHTKokuchi("！");
                    wk = "  ! ";
                } else {
                    bean.setEngSSSutuGanHanTeiKbn("");
                    bean.setEngSSHKSutuGanHTKokuchi("");
                    wk = "    ";
                }
                if (result.getCLetterJudgement().length() == 2) {
                    bean.setValuateInclude(result.getCLetterJudgement() + wk);
                } else {
                    bean.setValuateInclude(result.getCLetterJudgement() + " " + wk);
                }
            }
        }
        */
    }

    /**
     * 文字列型の配点比からInteger型の配点比に変換します。
     *
     * @param allotPntRateAll 文字列型の配点比
     * @return Integer型の配点比
     */
    private Integer toIntegerAllotPntRate(String allotPntRateAll) {
        if (" - ".equals(allotPntRateAll)) {
            return null;
        } else {
            return Integer.valueOf(allotPntRateAll);
        }
    }
}

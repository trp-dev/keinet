package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

/**
 *
 * 対象生徒選択画面の成績データを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainScoreBean extends SelstdMainKojinBean {

    /** 科目コード */
    private String subCd;

    /** 得点 */
    private Integer score;

    /** 偏差値 */
    private Double deviation;

    /** 科目名 */
    private String subName;

    /** 配点 */
    private Integer subAllotPnt;

    /** CEFR */
    private String cefr;

    /** 国語記述評価 */
    private String jpnDesc;

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

    /**
     * 得点を返します。
     *
     * @return 得点
     */
    public Integer getScore() {
        return score;
    }

    /**
     * 得点をセットします。
     *
     * @param score 得点
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * 偏差値を返します。
     *
     * @return 偏差値
     */
    public Double getDeviation() {
        return deviation;
    }

    /**
     * 偏差値をセットします。
     *
     * @param deviation 偏差値
     */
    public void setDeviation(Double deviation) {
        this.deviation = deviation;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public Integer getSubAllotPnt() {
        return subAllotPnt;
    }

    /**
     * 配点をセットします。
     *
     * @param subAllotPnt 配点
     */
    public void setSubAllotPnt(Integer subAllotPnt) {
        this.subAllotPnt = subAllotPnt;
    }

    /**
     * CEFRを返します。
     *
     * @return cefr
     */
    public String getCefr() {
        return cefr;
    }

    /**
     * CEFRを設定します。
     *
     * @param cefr CEFR
     */
    public void setCefr(String cefr) {
        this.cefr = cefr;
    }

    /**
     * 国語記述評価を返します。
     *
     * @return jpnDesc
     */
    public String getJpnDesc() {
        return jpnDesc;
    }

    /**
     * 国語記述評価を設定します。
     *
     * @param jpnDesc 国語記述評価
     */
    public void setJpnDesc(String jpnDesc) {
        this.jpnDesc = jpnDesc;
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.MARK_JAP_SUBNAME_MAP;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtExcelMakerService;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * Excel帳票メーカー(個人成績・志望一覧(個人成績一覧))です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A01_01)
public class SelstdDprtA0101Maker extends BaseExcelMaker<SelstdDprtExcelMakerInBean> {

    /** 1ページに出力する最大生徒数 */
    private static final int LISTMAX = 20;

    /** 1教科に出力する最大科目数 */
    private static final int SUBMAX = 3;

    /** 1教科に出力する最大科目数（総合） */
    private static final int SUBTOTALMAX = 1;

    /** 個人ID検索用IN句のカラム名 */
    private static final String COLUMN_INDIVIDUALID = "BI.INDIVIDUALID";

    /** 個人ID検索用IN句のカラム名 */
    /* private static final String COLUMN_INDIVIDUALID_CEFR = "C1.INDIVIDUALID"; */

    /** 個人ID検索用IN句のカラム名 */
    /* private static final String COLUMN_INDIVIDUALID_JPN = "J.INDIVIDUALID"; */

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックス：第一解答備考文言 */
    private static final String IDX_1STANS_REMARKS = "W3";

    /** インデックスrow：個人成績一覧（開始位置） */
    private static final int IDX_ROW_LIST_START = 6;

    /** インデックスrow：個人成績一覧（開始位置→生徒情報） */
    private static final int IDX_ROW_LIST_STUDENT = 1;

    /** インデックスrow：個人成績一覧（１生徒当たりの行数） */
    private static final int IDX_ROW_LIST_LINE = 3;

    /** インデックスcell：個人成績一覧（学年） */
    private static final int IDX_CELL_GRADE = 0;

    /** インデックスcell：個人成績一覧（クラス） */
    private static final int IDX_CELL_CLASS = 1;

    /** インデックスcell：個人成績一覧（クラス番号） */
    private static final int IDX_CELL_CLASSNO = 2;

    /** インデックスcell：個人成績一覧（氏名） */
    private static final int IDX_CELL_NAME = 3;

    /** インデックスcell：個人成績一覧（総合） */
    private static final int IDX_CELL_TOTAL = 5;

    /** インデックスcell：個人成績一覧（英語） */
    private static final int IDX_CELL_ENG = 8;

    /** インデックスcell：個人成績一覧（数学） */
    private static final int IDX_CELL_MAT = 11;

    /** インデックスcell：個人成績一覧（国語） */
    private static final int IDX_CELL_JAP = 14;

    /** インデックスcell：個人成績一覧（理科） */
    private static final int IDX_CELL_SCI = 17;

    /** インデックスcell：個人成績一覧（地歴公民） */
    private static final int IDX_CELL_SOC = 20;

    /** インデックスcell：個人成績一覧（科目短縮名→得点） */
    private static final int IDX_CELL_PNT = 1;

    /** インデックスcell：個人成績一覧（科目短縮名→偏差値） */
    private static final int IDX_CELL_DEV = 2;

    /** 個人成績一覧リスト */
    private List<SelstdDprtExcelMakerRecordBean> recordBeanList;

    /** ページ毎に出力する学年 */
    private Integer pageGrade;

    /** ページ毎に出力するクラス */
    private String pageCls;

    /** 対象模試 */
    private ExamBean exam;

    /** Cell(中央寄用) */
    private HSSFCellStyle cellCenter;

    /** Cell(左詰用) */
    private HSSFCellStyle cellLeft;

    /** サービス */
    @Resource(name = "selstdDprtExcelMakerService")
    private SelstdDprtExcelMakerService service;

    @Override
    protected boolean prepareData(SelstdDprtExcelMakerInBean inBean) {

        /* 画面から対象模試または個人IDが渡されなかった場合は帳票を作成しない */
        if (inBean.getExam() == null || inBean.getIdList().isEmpty()) {
            return false;
        }

        /* 個人成績一覧リストを取得する */
        getScoreList(inBean);

        /* リストが1件も取得できなかった場合は帳票は作成しない */
        if (recordBeanList.size() == 0) {
            return false;
        }

        /* 帳票出力用に対象模試を保存する */
        exam = inBean.getExam();

        /* 学年とクラスをセットする（１シート目用） */
        pageGrade = recordBeanList.get(0).getGrade();
        pageCls = recordBeanList.get(0).getCls();

        return true;
    }

    @Override
    protected void initSheet() {

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* 第一解答科目対応模試でなければ、備考文言を消す */
        if (!ExamUtil.isAns1st(exam)) {
            setCellValue(IDX_1STANS_REMARKS, "");
        }
    }

    @Override
    protected void outputData() {

        /* 個人成績一覧を出力する */
        outputRecord();

    }

    /**
     * 個人成績一覧リストを取得するメソッドです。
     *
     * @param inBean 画面からの設定値
     */
    private void getScoreList(SelstdDprtExcelMakerInBean inBean) {

        /* 個人IDを検索するIN句 */
        String individualid = DaoUtil.createIn(COLUMN_INDIVIDUALID, inBean.getIdList());
        /* String cefrindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_CEFR, inBean.getIdList()); */
        /* String jpnindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_JPN, inBean.getIdList()); */

        /* 個人成績一覧リストを取得する */
        /* recordBeanList = service.getScoreList(inBean.getExam(), individualid, getTemplate(), cefrindividualid, jpnindividualid); */
        recordBeanList = service.getScoreList(inBean.getExam(), individualid, getTemplate(), false);
    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年・クラス文字列 */
        String gradeCls = null;

        /* 学年・クラス文字列を作成する */
        gradeCls = service.createTargetClass(pageGrade, pageCls);

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeCls);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, service.createTargetExam(exam.toString()));
    }

    /**
     * 個人成績一覧を出力するメソッドです。
     */
    private void outputRecord() {

        /* 1ページ内の人数カウント*/
        int cntStudent = 0;

        cellCenter = this.createCellStyle();
        cellLeft = this.createCellStyle();

        /* 個人成績一覧データ（１人分） */
        List<SelstdDprtExcelMakerRecordBean> outputData = new ArrayList<SelstdDprtExcelMakerRecordBean>();

        /* 学年とクラスをセットする（１シート目用） */
        pageGrade = recordBeanList.get(0).getGrade();
        pageCls = recordBeanList.get(0).getCls();

        /* 対象学年・クラスを出力する（１シート目用） */
        outputTargetClass();

        /* 対象模試を出力する（１シート目用） */
        outputTargetExam();

        /* Cellのスタイルを取得 */
        cellLeft.cloneStyleFrom((HSSFCellStyle) getCell("J8").getCellStyle());
        cellCenter.cloneStyleFrom((HSSFCellStyle) getCell("P8").getCellStyle());

        /* Cellのスタイルを変更 */
        cellLeft.setAlignment(CellStyle.ALIGN_LEFT);
        cellCenter.setAlignment(CellStyle.ALIGN_CENTER);

        for (int index = 0; index < recordBeanList.size(); index++) {

            /* 個人成績一覧データ１人分のデータを取得する */
            outputData.add(recordBeanList.get(index));
            if (index != recordBeanList.size() - 1) {
                if (recordBeanList.get(index).getIndividualId().intValue() == recordBeanList.get(index + 1).getIndividualId().intValue()) {
                    continue;
                }
            }

            /* 1ページ内の最大出力人数を超えたかチェックする */
            if (cntStudent >= LISTMAX || (index != 0 && (!ObjectUtils.equals(pageGrade, outputData.get(0).getGrade()) || !ObjectUtils.equals(pageCls, outputData.get(0).getCls())))) {

                /* 学年とクラスをセットする */
                pageGrade = outputData.get(0).getGrade();
                pageCls = outputData.get(0).getCls();

                /* 改シートする */
                breakSheet();
                cntStudent = 0;
            }

            /* 個人成績一覧データを出力する */
            outputRecordList(cntStudent, outputData);

            /* 個人成績一覧データをクリア */
            outputData.clear();

            cntStudent++;
        }
    }

    /**
     * 個人成績一覧データを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param outputData 個人成績一覧データ（１人分）
     */
    private void outputRecordList(int cntStudent, List<SelstdDprtExcelMakerRecordBean> outputData) {

        /* 1教科内の科目数カウント（総合１） */
        int cntSubTotal1 = 0;

        /* 1教科内の科目数カウント（総合２） */
        int cntSubTotal2 = 0;

        /* 1教科内の科目数カウント（英語） */
        int cntSubEng = 0;

        /* 1教科内の科目数カウント（数学） */
        int cntSubMat = 0;

        /* 1教科内の科目数カウント（国語） */
        int cntSubJap = 0;

        /* 1教科内の科目数カウント（理科） */
        int cntSubSci = 0;

        /* 1教科内の科目数カウント（地歴公民） */
        int cntSubSoc = 0;

        /* 生徒情報出力位置 */
        int rowStudent = IDX_ROW_LIST_START + (IDX_ROW_LIST_LINE * cntStudent) + IDX_ROW_LIST_STUDENT;

        /* 成績情報出力位置 */
        int rowRecord = IDX_ROW_LIST_START + (IDX_ROW_LIST_LINE * cntStudent);

        /* 学年を出力する */
        setCellValue(rowStudent, IDX_CELL_GRADE, outputData.get(0).getGrade());
        /* クラスを出力する */
        setCellValue(rowStudent, IDX_CELL_CLASS, outputData.get(0).getCls());
        /* クラス番号を出力する */
        setCellValue(rowStudent, IDX_CELL_CLASSNO, outputData.get(0).getClassNo());
        /* 氏名を出力する */
        setCellValue(rowStudent, IDX_CELL_NAME, outputData.get(0).getNameKana());

        /* 成績を出力する */
        for (int subcnt = 0; subcnt < outputData.size(); subcnt++) {

            /* 科目成績 */
            SelstdDprtExcelMakerRecordBean subrecord = outputData.get(subcnt);

            /* 教科 */
            String course = subrecord.getCourseCd();

            /* 成績データがある場合のみ出力する */
            if (course != null) {

                /* 総合１を出力する */
                if (course.equals(BZConstants.COURSE_TOTAL1)) {
                    if (cntSubTotal1 < SUBTOTALMAX && outputRecordData(rowRecord + cntSubTotal1, IDX_CELL_TOTAL, subrecord)) {
                        cntSubTotal1++;
                    }
                }

                /* 総合２を出力する */
                if (course.equals(BZConstants.COURSE_TOTAL2)) {
                    if (cntSubTotal2 < SUBTOTALMAX && outputRecordData(rowRecord + cntSubTotal1 + cntSubTotal2, IDX_CELL_TOTAL, subrecord)) {
                        cntSubTotal2++;
                    }
                }

                /* 英語を出力する */
                if (course.equals(BZConstants.COURSE_ENG)) {
                    if (cntSubEng < SUBMAX && outputRecordData(rowRecord + cntSubEng, IDX_CELL_ENG, subrecord)) {
                        cntSubEng++;
                    }
                    /*
                    if (cntSubEng < SUBMAX) {
                        if (subrecord.getSubCd().equals(SUBCODE_CENTER_DEFAULT_ENGLISH)) {
                            continue;
                        }
                        boolean result;
                        if (subrecord.getSubCd().contentEquals(SUBCODE_CENTER_DEFAULT_ENNINTEI)) {
                            result = outputStrRecordData(rowRecord + cntSubEng, IDX_CELL_ENG, subrecord, true);
                        } else {
                            result = outputRecordData(rowRecord + cntSubEng, IDX_CELL_ENG, subrecord);
                        }
                        if (result) {
                            cntSubEng++;
                        }
                    }
                    */
                }

                /* 数学を出力する */
                if (course.equals(BZConstants.COURSE_MAT)) {
                    if (cntSubMat < SUBMAX && outputRecordData(rowRecord + cntSubMat, IDX_CELL_MAT, subrecord)) {
                        cntSubMat++;
                    }
                }

                /* 国語を出力する */
                if (course.equals(BZConstants.COURSE_JAP)) {
                    if (cntSubJap < SUBMAX) {
                        boolean result;
                        if (ExamUtil.isMark(exam)) {
                            result = outputMarkJapRecordData(rowRecord + cntSubJap, IDX_CELL_JAP, subrecord);
                        } else {
                            result = outputRecordData(rowRecord + cntSubJap, IDX_CELL_JAP, subrecord);
                        }
                        if (result) {
                            cntSubJap++;
                        }
                    }
                }

                /* 理科を出力する */
                if (course.equals(BZConstants.COURSE_SCI)) {
                    if (cntSubSci == 0 && FlagUtil.toBoolean(subrecord.getBasicFlg())) {
                        /* 1科目めが基礎科目なら、1行目は空欄にする */
                        cntSubSci++;
                    }
                    if (cntSubSci < SUBMAX && outputRecordData(rowRecord + cntSubSci, IDX_CELL_SCI, subrecord)) {
                        cntSubSci++;
                    }
                }

                /* 地歴公民を出力する */
                if (course.equals(BZConstants.COURSE_SOC)) {
                    if (cntSubSoc < SUBMAX && outputRecordData(rowRecord + cntSubSoc, IDX_CELL_SOC, subrecord)) {
                        cntSubSoc++;
                    }
                }
            }
        }
    }

    /**
     * 成績を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param subrecord 科目成績
     * @return 出力したらtrue
     */
    private boolean outputRecordData(int row, int cell, SelstdDprtExcelMakerRecordBean subrecord) {

        /* 模試科目マスタに存在しない科目は出力しない */
        if (StringUtils.isEmpty(subrecord.getSubAddrName())) {
            return false;
        }

        /* 科目短縮名を出力する */
        setCellValue(row, cell, subrecord.getSubAddrName());

        /* 科目得点(素点)を出力する */
        setCellValue(row, cell + IDX_CELL_PNT, subrecord.getSubScore());

        /* 科目偏差値を出力する */
        setCellValue(row, cell + IDX_CELL_DEV, subrecord.getSubDeviation());

        return true;
    }

    /**
     * マーク模試の国語成績を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param subrecord 科目成績
     * @return 出力したらtrue
     */
    private boolean outputMarkJapRecordData(int row, int cell, SelstdDprtExcelMakerRecordBean subrecord) {

        /* 科目名マップに存在しない科目は出力しない */
        String subName = MARK_JAP_SUBNAME_MAP.get(subrecord.getSubCd());
        if (StringUtils.isEmpty(subName)) {
            return false;
        }

        /* 科目名を出力する */
        setCellValue(row, cell, subName);

        /* 科目得点(素点)を出力する */
        setCellValue(row, cell + IDX_CELL_PNT, subrecord.getSubScore());

        /* 科目偏差値を出力する */
        setCellValue(row, cell + IDX_CELL_DEV, subrecord.getSubDeviation());

        return true;
    }

}

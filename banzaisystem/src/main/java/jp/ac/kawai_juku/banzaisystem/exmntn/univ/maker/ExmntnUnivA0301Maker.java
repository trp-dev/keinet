package jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.service.ExmntnDistService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301StudentInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.service.ExmntnUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivExcelUtil;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivUtil;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Excel帳票メーカー(簡易個人表)です。
 *
 *
 * @author TOTEC)MASAMI.SHIMIZU
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A03_01)
public class ExmntnUnivA0301Maker extends BaseExcelMaker<ExmntnUnivA0301InBean> {

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル（生徒情報）：クラス番号 */
    private static final String TITLE_STUDENT_CLSNO = "クラス番号：";

    /** タイトル（生徒情報）：氏名 */
    private static final String TITLE_STUDENT_NAME = "　　氏名：";

    /** タイトル（志望大学）：志望 */
    private static final String TITLE_CANDIDATE = "志望";

    /** タイトル（志望大学）：募集人員（文字列前） */
    private static final String TITEL_OFFER_CAPACITY1 = "(";

    /** タイトル（志望大学）：募集人員（文字列後） */
    private static final String TITEL_OFFER_CAPACITY2 = ")";

    /** タイトル（志望大学）：募集人員（推定） */
    private static final String TITEL_OFFER_CAPACITY3 = "推";

    /** セパレータ */
    private static final String SEPARATOR = "/";

    /** 最大ページ数 */
    private static final int PAGEMAX = 2;

    /** 最大大学数（1ページ目） */
    private static final int PAGE1_UNIVMAX = 4;

    /** 最大大学数（2ページ目） */
    private static final int PAGE2_UNIVMAX = 5;

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックス：生徒名 */
    private static final String IDX_STUDENT = "A5";

    /** インデックス：模試名（上段） */
    private static final String IDX_EXAMINATION1 = "A6";

    /** インデックス：私大評価用偏差値(数学(1科目)) */
    /* private static final String IDX_PRIVATE_UNIV_DEV_MATH_ONE = "AS11"; */
    private static final String IDX_PRIVATE_UNIV_DEV_MATH_ONE = "AW11";

    /** インデックス：英語認定試験名１ */
    /* private static final String IDX_CEFR_NAME1 = "AW14"; */

    /** インデックス：英語認定試験スコア１ */
    /* private static final String IDX_CEFR_SCORE1 = "BH14"; */

    /** インデックス：英語認定試験CEFR１ */
    /* private static final String IDX_CEFR_CEFR1 = "BJ14"; */

    /** インデックス：英語認定試験合否１ */
    /* private static final String IDX_CEFR_RESULT1 = "BL14"; */

    /** インデックス：英語認定試験名２ */
    /* private static final String IDX_CEFR_NAME2 = "AW15"; */

    /** インデックス：英語認定試験スコア２ */
    /* private static final String IDX_CEFR_SCORE2 = "BH15"; */

    /** インデックス：英語認定試験CEFR２ */
    /* private static final String IDX_CEFR_CEFR2 = "BJ15"; */

    /** インデックス：英語認定試験合否２ */
    /* private static final String IDX_CEFR_RESULT2 = "BL15"; */

    /** インデックスrow：志望大学（1ページ目） */
    private static final int IDX_ROW_UNIV_PAGE1 = 18;

    /** インデックスrow：志望大学（2ページ目） */
    private static final int IDX_ROW_UNIV_PAGE2 = 10;

    /** インデックスrow：志望大学（日程） */
    private static final int IDX_ROW_UNIV_SCHEDULE = 0;

    /** インデックスrow：志望大学（出願大学） */
    private static final int IDX_ROW_UNIV_APPLICATION = 1;

    /** インデックスrow：志望大学（大学名） */
    private static final int IDX_ROW_UNIV_UNIVNAME = 0;

    /** インデックスrow：志望大学（学部名） */
    private static final int IDX_ROW_UNIV_FACULTYNAME = 1;

    /** インデックスrow：志望大学（学科名） */
    private static final int IDX_ROW_UNIV_DEPTNAME = 2;

    /** インデックスrow：志望大学（募集人員） */
    private static final int IDX_ROW_UNIV_CAPACITY = 2;

    /** インデックスrow：志望大学（センター評価：評価得点／満点） */
    /* private static final int IDX_ROW_UNIV_CENTER_SCORE = 5; */
    private static final int IDX_ROW_UNIV_CENTER_SCORE = 4;

    /** インデックスrow：志望大学（センター評価：ボーダーライン） */
    /* private static final int IDX_ROW_UNIV_CENTER_BORDER = 6; */
    private static final int IDX_ROW_UNIV_CENTER_BORDER = 5;

    /** インデックスrow：志望大学（センター評価：評価） */
    /* private static final int IDX_ROW_UNIV_CENTER_RATING = 5; */
    private static final int IDX_ROW_UNIV_CENTER_RATING = 4;

    /** インデックスrow：志望大学（２次・一般評価：評価偏差値） */
    /* private static final int IDX_ROW_UNIV_SECOND_DEVIATION = 7; */
    private static final int IDX_ROW_UNIV_SECOND_DEVIATION = 6;

    /** インデックスrow：志望大学（２次・一般評価：ボーダーランク） */
    /* private static final int IDX_ROW_UNIV_SECOND_RANK = 8; */
    private static final int IDX_ROW_UNIV_SECOND_RANK = 7;

    /** インデックスrow：志望大学（２次・一般評価：評価） */
    /* private static final int IDX_ROW_UNIV_SECOND_RATING = 7; */
    private static final int IDX_ROW_UNIV_SECOND_RATING = 6;

    /** インデックスrow：志望大学（総合：ポイント） */
    /* private static final int IDX_ROW_UNIV_TOTAL_POINT = 9; */
    private static final int IDX_ROW_UNIV_TOTAL_POINT = 8;

    /** インデックスrow：志望大学（総合：評価） */
    /* private static final int IDX_ROW_UNIV_TOTAL_RATING = 9; */
    private static final int IDX_ROW_UNIV_TOTAL_RATING = 8;

    /** インデックスrow：志望大学（学力分布） */
    /* private static final int IDX_ROW_UNIV_DIST = 11; */
    private static final int IDX_ROW_UNIV_DIST = 10;

    /** インデックスrow：志望大学（注意事項） */
    /* private static final int IDX_ROW_UNIV_ATTENTION = 34; */
    private static final int IDX_ROW_UNIV_ATTENTION = 33;

    /** インデックスrow：志望大学（注釈文） */
    /* private static final int IDX_ROW_UNIV_COMMENT_STATEMENT = 34; */
    private static final int IDX_ROW_UNIV_COMMENT_STATEMENT = 33;

    /** インデックスcell：志望大学（日程） */
    /* private static final int IDX_CELL_UNIV_SCHEDULE = 3; */
    private static final int IDX_CELL_UNIV_SCHEDULE = 4;

    /** インデックスcell：志望大学（出願大学） */
    /* private static final int IDX_CELL_UNIV_APPLICATION = 3; */
    private static final int IDX_CELL_UNIV_APPLICATION = 4;

    /** インデックスcell：志望大学（大学名） */
    private static final int IDX_CELL_UNIV_UNIVNAME = 7;

    /** インデックスcell：志望大学（学部名） */
    private static final int IDX_CELL_UNIV_FACULTYNAME = 7;

    /** インデックスcell：志望大学（学科名） */
    private static final int IDX_CELL_UNIV_DEPTNAME = 7;

    /** インデックスcell：志望大学（募集人員） */
    private static final int IDX_CELL_UNIV_CAPACITY = 11;

    /** インデックスcell：志望大学（共通テスト含まない：評価得点／満点） */
    /** インデックスcell：志望大学（共通テスト：評価得点／満点） */
    /* private static final int IDX_CELL_UNIV_CENTER_SCORE = 7; */
    private static final int IDX_CELL_UNIV_CENTER_SCORE = 9;

    /** インデックスcell：志望大学（共通テスト含まない：ボーダーライン） */
    /** インデックスcell：志望大学（共通テスト：ボーダーライン） */
    /* private static final int IDX_CELL_UNIV_CENTER_BORDER = 7; */
    private static final int IDX_CELL_UNIV_CENTER_BORDER = 9;

    /** インデックスcell：志望大学（共通テスト含まない：評価） */
    /** インデックスcell：志望大学（共通テスト：評価） */
    /* private static final int IDX_CELL_UNIV_CENTER_RATING = 10; */
    private static final int IDX_CELL_UNIV_CENTER_RATING = 13;

    /** インデックスcell：志望大学（共通テスト含む：評価得点／満点） */
    /* private static final int IDX_CELL_UNIV_CENTER_INCLUDE_SCORE = 12; */

    /** インデックスcell：志望大学（共通テスト含む：ボーダーライン） */
    /* private static final int IDX_CELL_UNIV_CENTER_INCLUDE_BORDER = 12; */

    /** インデックスcell：志望大学（共通テスト含む：評価） */
    /* private static final int IDX_CELL_UNIV_CENTER_INCLUDE_RATING = 15; */

    /** インデックスcell：志望大学（２次・一般評価：評価偏差値） */
    /* private static final int IDX_CELL_UNIV_SECOND_DEVIATION = 7; */
    private static final int IDX_CELL_UNIV_SECOND_DEVIATION = 9;

    /** インデックスcell：志望大学（２次・一般評価：ボーダーランク） */
    /* private static final int IDX_CELL_UNIV_SECOND_RANK = 7; */
    private static final int IDX_CELL_UNIV_SECOND_RANK = 9;

    /** インデックスcell：志望大学（２次・一般評価：評価） */
    /* private static final int IDX_CELL_UNIV_SECOND_RATING = 12; */
    private static final int IDX_CELL_UNIV_SECOND_RATING = 13;

    /** インデックスcell：志望大学（総合：ポイント） */
    /* private static final int IDX_CELL_UNIV_TOTAL_POINT = 7; */
    private static final int IDX_CELL_UNIV_TOTAL_POINT = 9;

    /** インデックスcell：志望大学（総合：評価） */
    /* private static final int IDX_CELL_UNIV_TOTAL_RATING = 12; */
    private static final int IDX_CELL_UNIV_TOTAL_RATING = 13;

    /** インデックスcell：志望大学（注意事項） */
    private static final int IDX_CELL_UNIV_ATTENTION = 0;

    /** インデックスcell：志望大学（注釈文） */
    private static final int IDX_CELL_UNIV_COMMENT_STATEMENT = 2;

    /** インデックスcell：志望大学（１大学あたりの列数＋大学間の空欄列） */
    /* private static final int IDX_CELL_UNIV_NUM = 18; */
    private static final int IDX_CELL_UNIV_NUM = 17;

    /** 出力できる最大人数 */
    private static final int MAX_STUDENT_NUM = 400;

    /** 帳票ユーティリティ */
    private ExmntnUnivExcelUtil excelUtil = new ExmntnUnivExcelUtil(this);

    /** 大学詳細画面のサービス */
    @Resource
    private ExmntnUnivService exmntnUnivService;

    /** 志望大学学力分布画面のサービス */
    @Resource
    private ExmntnDistService exmntnDistService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 生徒情報リスト */
    private List<ExmntnUnivA0301StudentInBean> studentList;

    /** 生徒情報Bean */
    private ExmntnUnivA0301StudentInBean studentBean;

    /** 第1面出力フラグ */
    private boolean output1stFlg;

    /** 第2面出力フラグ */
    private boolean output2ndFlg;

    /** マーク模試 */
    private ExamBean markExam;

    /** 記述模試 */
    private ExamBean writtenExam;

    /** 対象模試 */
    private ExamBean targetExam;

    /** Service */
    @Resource(name = "exmntnMainService")
    private ExmntnMainService service;

    @Override
    protected boolean prepareData(ExmntnUnivA0301InBean inBean) {

        /* 出力のために入力beanを保存 */
        studentList = inBean.getStudentList();
        output1stFlg = inBean.isOutput1stFlg();
        output2ndFlg = inBean.isOutput2ndFlg();

        if (studentList.isEmpty()) {
            /* 複数生徒一括の場合：生徒が0人なら出力しない */
            return false;
        } else if (studentList.size() > MAX_STUDENT_NUM) {
            /* 出力できる最大人数を超えている場合はエラーとする */
            throw new MessageDialogException(getMessage("error.output.main.D_E002", MAX_STUDENT_NUM));
        } else {
            /* initSheetメソッド用に最初の生徒で初期化しておく */
            studentBean = studentList.get(0);
        }

        if (studentBean.getStudentBean() == null || studentBean.getScoreBean() == null || (!studentBean.getScoreBean().isEntered() && studentBean.getCandidateUnivList().isEmpty())) {
            /*
             * 「生徒情報無し」「判定未実行」「成績未入力かつ志望大学情報無し」の何れかなら出力しない。
             * 複数生徒一括の場合は、これらの条件を満たすデータはstudentListに設定されないので、
             * このチェック処理は単数出力の場合のみ機能する。
             */
            return false;
        }

        /* 対象模試（マーク模試）を保存する */
        markExam = studentBean.getScoreBean().getMarkExam();

        /* 対象模試（記述模試）を保存する */
        writtenExam = studentBean.getScoreBean().getWrittenExam();

        if (markExam == null && writtenExam == null) {
            /* 入力情報がない */
            return false;
        } else {
            /* 対象模試を判定する */
            targetExam = ExamUtil.judgeTargetExam(markExam, writtenExam);
        }

        return true;
    }

    @Override
    protected void initSheet() {

        /* 対象模試（マーク模試）を保存する */
        markExam = studentBean.getScoreBean().getMarkExam();

        /* 対象模試（記述模試）を保存する */
        writtenExam = studentBean.getScoreBean().getWrittenExam();

        /* 対象模試を判定する */
        targetExam = ExamUtil.judgeTargetExam(markExam, writtenExam);

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* クラス番号、氏名を出力する */
        outputStudent();
    }

    @Override
    protected void outputData() {
        for (int i = 0; i < studentList.size(); i++) {
            /* 帳票ユーティリティに成績データを設定する */
            excelUtil.setScoreBean(studentBean.getScoreBean());

            /* 個人成績一覧データを出力する */
            excelUtil.outputSubRecord(targetExam, markExam, writtenExam, IDX_EXAMINATION1);

            /* 私大評価用偏差値を出力する */
            if (markExam != null && !ExamUtil.isCenter(markExam)) {
                excelUtil.outputPrivateUnivDeviation(IDX_PRIVATE_UNIV_DEV_MATH_ONE);
            }

            /* 英語認定試験情報を出力する */
            /* outputCefrData(); */

            /* 志望大学データを出力する */
            outputCandidateUniv();

            /* 第1面を出力しないなら削除する */
            if (!output1stFlg) {
                /* 複数生徒一括の場合は、必ず第1面を出力するので2固定とする */
                removeSheetAt(2);
            }

            /* 次の生徒が存在する場合は改シートする */
            if (i + 1 < studentList.size()) {
                /* 生徒情報を次の生徒で上書きする */
                studentBean = studentList.get(i + 1);
                /* テンプレートの位置をリセット＆改シートする */
                resetTemplateIndexAndBreakSheet();
            }
        }
    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年文字列 */
        String gradeStr = TITLE_GRADE + studentBean.getStudentBean().getGrade();

        /* クラス文字列 */
        String classStr = TITLE_CLS + studentBean.getStudentBean().getCls();

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeStr + classStr);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, TITLE_EXAM + targetExam);
    }

    /**
     * 生徒情報を出力するメソッドです。
     */
    private void outputStudent() {

        /* クラス番号文字列 */
        String clsnoStr = TITLE_STUDENT_CLSNO + studentBean.getStudentBean().getClassNo();

        /* 氏名文字列 */
        String nameStr = TITLE_STUDENT_NAME + studentBean.getStudentBean().getNameKana();

        /* 生徒情報を出力する */
        setCellValue(IDX_STUDENT, clsnoStr + nameStr);
    }

    /**
     * 志望大学データを出力するメソッドです。
     */
    private void outputCandidateUniv() {

        /* ページ数カウント */
        int cntPage = 0;

        /* ページ内大学数カウント */
        int cntUniv = 0;

        /* 最大大学数 */
        int univMax = PAGE1_UNIVMAX;

        /* 大学数カウント（前期） */
        int cntUnivScheduleD = 0;

        /* 大学数カウント（中期） */
        int cntUnivScheduleC = 0;

        /* 大学数カウント（後期） */
        int cntUnivScheduleE = 0;

        for (ExmntnMainCandidateUnivBean bean : studentBean.getCandidateUnivList()) {

            /* 大学詳細情報を取得する */
            ExmntnUnivDetailSvcOutBean detail = exmntnUnivService.createUnivDetail(bean, studentBean.getScoreBean());

            /* 1ページ内の最大大学数を超えたかチェックする */
            if (cntUniv >= univMax) {

                univMax = PAGE2_UNIVMAX;
                cntUniv = 0;
                cntPage++;

                if (cntPage < PAGEMAX) {
                    /* 2ページまで出力する */
                    if (output2ndFlg) {
                        /* 改シートする */
                        breakSheet();
                    } else {
                        /* 第2面を出力しないなら改ページしない */
                        break;
                    }
                } else {
                    /* これ以降のデータ出力しない */
                    break;
                }
            }

            /* 志望フラグ */
            boolean flgCandidate = false;

            /* 日程区分が前期の最初の大学 */
            if (detail.getScheduleCd().equals(JudgementConstants.Univ.SCHEDULE_BFR) && cntUnivScheduleD == 0) {
                flgCandidate = true;
                cntUnivScheduleD++;
            }
            /* 日程区分が中期の最初の大学 */
            if (detail.getScheduleCd().equals(JudgementConstants.Univ.SCHEDULE_MID) && cntUnivScheduleC == 0) {
                flgCandidate = true;
                cntUnivScheduleC++;
            }
            /* 日程区分が後期の最初の大学 */
            if (detail.getScheduleCd().equals(JudgementConstants.Univ.SCHEDULE_AFT) && cntUnivScheduleE == 0) {
                flgCandidate = true;
                cntUnivScheduleE++;
            }

            /* 大学詳細を出力する */
            outputCandidateUnivDetail(cntPage, cntUniv, flgCandidate, detail);

            cntUniv++;
        }
    }

    /**
     * 志望大学データを出力するメソッドです。
     *
     * @param cntPage ページ数カウント
     * @param cntUniv ページ内大学数カウント
     * @param flgCandidate 志望フラグ
     * @param detail 大学詳細
     */
    private void outputCandidateUnivDetail(int cntPage, int cntUniv, boolean flgCandidate, ExmntnUnivDetailSvcOutBean detail) {

        /* 学力分布の対象模試を判定する */
        ExamBean distTargetExam = excelUtil.judgeDistTargetExam(markExam, writtenExam, detail.getScheduleCd());
        /* 模試がセンター・リサーチか判定 */
        /* boolean isCenter = ExamUtil.isCenter(distTargetExam); */

        /* 出力行 */
        int row = 0;

        /* 1ページ目と2ページ目で出力位置が違う */
        if (cntPage == 0) {
            row = IDX_ROW_UNIV_PAGE1;
        } else {
            row = IDX_ROW_UNIV_PAGE2;
        }

        /* 日程を出力する */
        setCellValue(row + IDX_ROW_UNIV_SCHEDULE, IDX_CELL_UNIV_SCHEDULE + (IDX_CELL_UNIV_NUM * cntUniv), systemDto.getScheduleNameMap().get(detail.getScheduleCd()));

        /* 「志望」表示 */
        String strCandidate = null;
        if (flgCandidate) {
            strCandidate = TITLE_CANDIDATE;
        }

        /* 出願大学を出力する */
        setCellValue(row + IDX_ROW_UNIV_APPLICATION, IDX_CELL_UNIV_APPLICATION + (IDX_CELL_UNIV_NUM * cntUniv), strCandidate);

        /* 大学名を出力する */
        setCellValue(row + IDX_ROW_UNIV_UNIVNAME, IDX_CELL_UNIV_UNIVNAME + (IDX_CELL_UNIV_NUM * cntUniv), detail.getUnivName());

        /* 学部名を出力する */
        setCellValue(row + IDX_ROW_UNIV_FACULTYNAME, IDX_CELL_UNIV_FACULTYNAME + (IDX_CELL_UNIV_NUM * cntUniv), detail.getFacultyName());

        /* 学科名を出力する */
        setCellValue(row + IDX_ROW_UNIV_DEPTNAME, IDX_CELL_UNIV_DEPTNAME + (IDX_CELL_UNIV_NUM * cntUniv), detail.getDeptName());

        /* 募集人員を出力する */
        setCellValue(row + IDX_ROW_UNIV_CAPACITY, IDX_CELL_UNIV_CAPACITY + (IDX_CELL_UNIV_NUM * cntUniv), createOfferCapacity(detail.getOfferCapacity(), detail.getOfferCapacityKbn()));

        /* 共通テスト評価(含まない)：評価得点／満点を出力する */
        /* 共通テスト評価：評価得点／満点を出力する */
        setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), concatString(detail.getCenter().getScore(), detail.getCenter().getFullPoint()));
        /* 共通テスト評価(含まない)：ボーダーラインを出力する */
        /* 共通テスト評価：ボーダーラインを出力する */
        setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getBorder());
        /* 共通テスト評価(含まない)：評価を出力する */
        /* 共通テスト評価：評価を出力する */
        setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getRating());

        /*
        if (!isCenter) {
            /* 共通テスト評価(含まない)：評価得点／満点を出力する
            setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), concatString(detail.getCenter().getScore(), detail.getCenter().getFullPoint()));
            /* 共通テスト評価(含まない)：ボーダーラインを出力する
            setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getBorder());
            /* 共通テスト評価(含まない)：評価を出力する
            setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getRating());
        } else {
            if (!StringUtil.isEmptyTrim(detail.getCenterInclude().getAppReqPaternCd())) {
                /* 共通テスト評価(含まない)：評価得点／満点
                setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), "");
                /* 共通テスト評価(含まない)：ボーダーライン
                setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), "");
                /* 共通テスト評価(含まない)：評価
                setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_RATING + (IDX_CELL_UNIV_NUM * cntUniv), "");
            } else {
                /* 共通テスト評価(含まない)：評価得点／満点を出力する
                setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), concatString(detail.getCenter().getScore(), detail.getCenter().getFullPoint()));
                /* 共通テスト評価(含まない)：ボーダーラインを出力する
                setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getBorder());
                /* 共通テスト評価(含まない)：評価を出力する
                setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getRating());
            }
        }
        */

        /*
        if (!StringUtil.isEmptyTrim(detail.getCenterInclude().getAppReqPaternCd())) {
            /* 共通テスト評価(含む)：評価得点／満点を出力する
            setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_INCLUDE_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), concatString(detail.getCenterInclude().getScore(), detail.getCenterInclude().getFullPoint()));
            /* 共通テスト評価(含む)：ボーダーラインを出力する
            setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_INCLUDE_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenterInclude().getBorder());
            /* 共通テスト評価(含む)：評価を出力する
            if ("".equals(detail.getCenterInclude().getEnglishRequirements()) || detail.getCenterInclude().getEnglishRequirements() == null) {
                setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_INCLUDE_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenterInclude().getRating());
            } else {
                setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_INCLUDE_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenterInclude().getRating() + "\n" + detail.getCenterInclude().getEnglishRequirements());
            }
        } else {
            /* 共通テスト評価(含む)：評価得点／満点
            setCellValue(row + IDX_ROW_UNIV_CENTER_SCORE, IDX_CELL_UNIV_CENTER_INCLUDE_SCORE + (IDX_CELL_UNIV_NUM * cntUniv), "");
            /* 共通テスト評価(含む)：ボーダーライン
            setCellValue(row + IDX_ROW_UNIV_CENTER_BORDER, IDX_CELL_UNIV_CENTER_INCLUDE_BORDER + (IDX_CELL_UNIV_NUM * cntUniv), "");
            /* 共通テスト評価(含む)：評価
            setCellValue(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_INCLUDE_RATING + (IDX_CELL_UNIV_NUM * cntUniv), "");
        }
        */

        /* 折り返して全体を表示するように設定する */
        /*
        HSSFCellStyle style = getCell(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_INCLUDE_RATING + (IDX_CELL_UNIV_NUM * cntUniv)).getCellStyle();
        style.setWrapText(true);
        getCell(row + IDX_ROW_UNIV_CENTER_RATING, IDX_CELL_UNIV_CENTER_INCLUDE_RATING + (IDX_CELL_UNIV_NUM * cntUniv)).setCellStyle(style);
        */

        /* ２次・一般評価：評価偏差値を出力する */
        setCellValue(row + IDX_ROW_UNIV_SECOND_DEVIATION, IDX_CELL_UNIV_SECOND_DEVIATION + (IDX_CELL_UNIV_NUM * cntUniv), detail.getSecond().getDev());

        /* ２次・一般評価：ボーダーランクを出力する */
        setCellValue(row + IDX_ROW_UNIV_SECOND_RANK, IDX_CELL_UNIV_SECOND_RANK + (IDX_CELL_UNIV_NUM * cntUniv), detail.getSecond().getBorderRank());

        /* ２次・一般評価：評価を出力する */
        setCellValue(row + IDX_ROW_UNIV_SECOND_RATING, IDX_CELL_UNIV_SECOND_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getSecond().getRating());

        /* 総合：ポイントを出力する */
        setCellValue(row + IDX_ROW_UNIV_TOTAL_POINT, IDX_CELL_UNIV_TOTAL_POINT + (IDX_CELL_UNIV_NUM * cntUniv), detail.getTotalRatingPoint());

        /* 総合：評価を出力する */
        setCellValue(row + IDX_ROW_UNIV_TOTAL_RATING, IDX_CELL_UNIV_TOTAL_RATING + (IDX_CELL_UNIV_NUM * cntUniv), detail.getTotalRating());

        /* 学力分布を出力する */
        excelUtil.outputDistributionGraph(exmntnDistService.findDistBean(distTargetExam, studentBean.getScoreBean(), detail), distTargetExam, getCell(row + IDX_ROW_UNIV_DIST, IDX_CELL_UNIV_NUM * cntUniv));

        if (ExmntnUnivUtil.isAttentionVisible(markExam, systemDto)) {
            /* 注意事項を出力する */
            setCellValue(row + IDX_ROW_UNIV_ATTENTION, IDX_CELL_UNIV_ATTENTION + (IDX_CELL_UNIV_NUM * cntUniv), getMessage("label.exmntn.univ.excel.centerAttention"));
            /* 注釈文を出力する */
            setCellValue(row + IDX_ROW_UNIV_COMMENT_STATEMENT, IDX_CELL_UNIV_COMMENT_STATEMENT + (IDX_CELL_UNIV_NUM * cntUniv), detail.getCenter().getCommentStatement());
        }
    }

    /**
     * 募集人数の文字列を作成するメソッドです。
     *
     * @param offerCapacity 募集人員
     * @param getOfferCapacityKbn 募集定員区分
     * @return 募集人員の文字列
     */
    private String createOfferCapacity(String offerCapacity, String getOfferCapacityKbn) {

        String strOfferCapacity = null;

        /* 定員信頼が［非表示］の場合は募集人員を表示しない */
        if (getOfferCapacityKbn == null || getOfferCapacityKbn.equals(BZConstants.CAPACITY_NONE)) {
            return StringUtils.EMPTY;
        }

        if (offerCapacity != null) {
            strOfferCapacity = TITEL_OFFER_CAPACITY1 + offerCapacity + TITEL_OFFER_CAPACITY2;

            /* 定員信頼が［推定］の場合"推"を表示する */
            if (getOfferCapacityKbn.equals(BZConstants.CAPACITY_ESTIMATE)) {
                strOfferCapacity += TITEL_OFFER_CAPACITY3;
            }

        } else {
            strOfferCapacity = TITEL_OFFER_CAPACITY1 + TITEL_OFFER_CAPACITY2;
        }

        return strOfferCapacity;
    }

    /**
     * 文字列をセパレータを挟んで結合するメソッドです。
     *
     * @param str1 文字列１
     * @param ste2 文字列２
     * @return 結合文字列
     */
    private String concatString(String str1, String ste2) {
        if (StringUtils.isEmpty(str1) && StringUtils.isEmpty(ste2)) {
            return null;
        } else {
            return StringUtils.join(new String[] { str1, ste2 }, SEPARATOR);
        }
    }

    /**
     * 英語認定試験情報を出力するメソッドです。
     */
    /*
    private void outputCefrData() {
    */
    /* CEFR成績リストを取得する */
    /*
    List<ExmntnMainCefrRecordBean> cefrList;
    
    if (!studentBean.getCsvdispFlg()) {
        cefrList = systemDto.getCefrChangeInfo();
    } else {
        cefrList = service.findCefrRecordList(studentBean.getStudentBean().getIndividualId(), markExam);
    }
    
    if (cefrList != null) {
    */
    /* 試験名１ */
    /*
    if (cefrList.get(0).getCefrExamLevelName1() == null) {
        setCellValue(IDX_CEFR_NAME1, cefrList.get(0).getCefrExamName1());
    } else {
        setCellValue(IDX_CEFR_NAME1, cefrList.get(0).getCefrExamName1() + cefrList.get(0).getCefrExamLevelName1());
    }
    */
    /* スコア１ */
    /*
    if (Constants.FLG_ON.equals(cefrList.get(0).getScoreCorrectFlg1())) {
    */
    /* 認定試験スコア補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
        setCellValue(IDX_CEFR_SCORE1, Constants.ASTERISK + " " + getStringScore(cefrList.get(0).getCefrScore1(), cefrList.get(0).getCefrExamCd1()));
    } else {
        setCellValue(IDX_CEFR_SCORE1, getStringScore(cefrList.get(0).getCefrScore1(), cefrList.get(0).getCefrExamCd1()));
    }
    */
    /* CEFR１ */
    /*
    if (Constants.FLG_ON.equals(cefrList.get(0).getCefrLevelCdCorrectFlg1())) {
    */
    /* CEFRレベル補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
        setCellValue(IDX_CEFR_CEFR1, Constants.ASTERISK + " " + cefrList.get(0).getCefrLevel1());
    } else {
        setCellValue(IDX_CEFR_CEFR1, cefrList.get(0).getCefrLevel1());
    }
    */
    /* 合否１ */
    /*
    if (cefrList.get(0).getExamResult1() != null && !"".equals(cefrList.get(0).getExamResult1())) {
        if (Constants.FLG_ON.equals(cefrList.get(0).getExamResultCorrectFlg1())) {
    */
    /* 英検合否補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
            setCellValue(IDX_CEFR_RESULT1, Constants.ASTERISK + " " + Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult1()).getName());
        } else {
            setCellValue(IDX_CEFR_RESULT1, Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult1()).getName());
        }
    } else {
        setCellValue(IDX_CEFR_RESULT1, "");
    }
    */
    /* 試験名２ */
    /*
    if (cefrList.get(0).getCefrExamLevelName2() == null) {
        setCellValue(IDX_CEFR_NAME2, cefrList.get(0).getCefrExamName2());
    } else {
        setCellValue(IDX_CEFR_NAME2, cefrList.get(0).getCefrExamName2() + cefrList.get(0).getCefrExamLevelName2());
    }
    */
    /* スコア２ */
    /*
    if (Constants.FLG_ON.equals(cefrList.get(0).getScoreCorrectFlg2())) {
    */
    /* 認定試験スコア補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
        setCellValue(IDX_CEFR_SCORE2, Constants.ASTERISK + " " + getStringScore(cefrList.get(0).getCefrScore2(), cefrList.get(0).getCefrExamCd2()));
    } else {
        setCellValue(IDX_CEFR_SCORE2, getStringScore(cefrList.get(0).getCefrScore2(), cefrList.get(0).getCefrExamCd2()));
    }
    */
    /* CEFR２ */
    /*
     if (Constants.FLG_ON.equals(cefrList.get(0).getCefrLevelCdCorrectFlg2())) {
     */
    /* CEFRレベル補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
        setCellValue(IDX_CEFR_CEFR2, Constants.ASTERISK + " " + cefrList.get(0).getCefrLevel2());
    } else {
        setCellValue(IDX_CEFR_CEFR2, cefrList.get(0).getCefrLevel2());
    }
    */
    /* 合否２ */
    /*
    if (cefrList.get(0).getExamResult2() != null && !"".equals(cefrList.get(0).getExamResult2())) {
        if (Constants.FLG_ON.equals(cefrList.get(0).getExamResultCorrectFlg2())) {
    */
    /* 英検合否補正フラグに"1"が設定されている場合、"*"を表示 */
    /*
            setCellValue(IDX_CEFR_RESULT2, Constants.ASTERISK + " " + Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult2()).getName());
        } else {
            setCellValue(IDX_CEFR_RESULT2, Constants.ENG_EXAM_RESULT.get(cefrList.get(0).getExamResult2()).getName());
        }
    } else {
        setCellValue(IDX_CEFR_RESULT2, "");
    }
    }
    }
    */

    /**
     * スコアを文字列に変換する。
     *
     * @param value スコア（Double型）
     * @param engPtCd 認定試験コード
     * @return スコア（String型）
     */
    /*private String getStringScore(Double value, String engPtCd) {
        EngPtBean exam = systemDto.getEngCertExamList().stream().filter(e -> e.getEngPtCd().equals(engPtCd)).findFirst().orElse(null);
        if (exam == null) {
            return "";
        } else if (value == null) {
            return "";
        } else if (Constants.FLG_ON.equals(exam.getScoreDecFlg())) {
            return String.valueOf(value);
        } else {
            return String.valueOf(value.longValue());
        }
    }*/

}

package jp.ac.kawai_juku.banzaisystem.framework.window;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Color;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLayer;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFrame;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.util.ControllerUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

import org.seasar.framework.container.factory.SingletonS2ContainerFactory;
import org.seasar.framework.util.ClassUtil;

/**
 *
 * ウィンドウ管理クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class WindowManager {

    /** Singletonインスタンス */
    private static WindowManager instance;

    /** フレーム */
    private final JFrame frame;

    /** アクティブダイアログスタック */
    private final Queue<JDialog> dialogStack = Collections.asLifoQueue(new LinkedList<JDialog>());

    /**
     * コンストラクタです。
     *
     * @param frameClass フレームのクラス
     * @param controllerClass 初期表示画面のコントローラクラス
     */
    private WindowManager(final Class<? extends JFrame> frameClass,
            final Class<? extends AbstractController> controllerClass) {

        /* フレームクラスのインスタンスを生成する */
        frame = (JFrame) ClassUtil.newInstance(frameClass);

        /* S2コンテナの初期化中はロゴ画像を表示する */
        displayFrame(new ImagePanel(ImageUtil.readImage("/logo.png")));

        SwingWorker<Object, Object> worker = new SwingWorker<Object, Object>() {

            @Override
            protected Object doInBackground() throws Exception {

                if (frameClass == BZFrame.class) {
                    /* 画像ファイルをキャッシュする(判定実行ツールはキャッシュしない) */
                    ImageUtil.load("jp.ac.kawai_juku.banzaisystem");
                }

                /* S2コンテナを初期化する */
                SingletonS2ContainerFactory.init();

                return null;
            }

            @Override
            protected void done() {
                try {
                    get();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                /* 初期表示画面を表示する */
                ControllerUtil.invoke(controllerClass, null);
            }

        };
        worker.execute();
    }

    /**
     * ウィンドウ管理クラスを初期化します。
     *
     * @param frameClass フレームのクラス
     * @param controllerClass 初期表示画面のコントローラクラス
     */
    public static void initialize(final Class<? extends JFrame> frameClass,
            final Class<? extends AbstractController> controllerClass) {
        if (instance == null) {
            instance = new WindowManager(frameClass, controllerClass);
        }
    }

    /**
     * インスタンスを返します。
     *
     * @return {@link WindowManager}
     */
    public static WindowManager getInstance() {
        return instance;
    }

    /**
     * 指定したコンポーネントをフレーム表示します。
     *
     * @param comp コンポーネント
     */
    @SuppressWarnings("unchecked")
    public void displayFrame(JComponent comp) {

        Container contentPane = frame.getContentPane();

        boolean resized = !contentPane.getSize().equals(comp.getSize());

        JLayer<JComponent> layer;
        if (contentPane.getComponentCount() == 0) {
            layer = new JLayer<JComponent>(comp, new WaitLayerUI());
            contentPane.add(layer);
        } else {
            layer = (JLayer<JComponent>) contentPane.getComponent(0);
            layer.setView(comp);
        }

        frame.pack();

        if (resized) {
            /* フレームサイズが変化したら、ウィンドウの位置を画面中央に寄せる */
            frame.setLocationRelativeTo(null);
        }

        if (!frame.isVisible()) {
            frame.setVisible(true);
        } else if (frame.isUndecorated()) {
            frame.dispose();
            frame.setBackground(Color.WHITE);
            frame.setUndecorated(false);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
            frame.pack();
        }

        frame.repaint();
    }

    /**
     * 指定したコンポーネントをダイアログ表示します。
     *
     * @param comp コンポーネント
     */
    public void displayDialog(JComponent comp) {

        JDialog dialog = new JDialog(getActiveWindow());
        dialog.setModal(true);
        dialog.setResizable(false);
        dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (isTaskRunning()) {
                    JOptionPane.showMessageDialog(getActiveWindow(), getMessage("error.framework.running"));
                } else {
                    dialogStack.poll().dispose();
                }
            }
        });
        dialog.getContentPane().add(new JLayer<JComponent>(comp, new WaitLayerUI()));
        dialog.pack();
        dialog.setLocationRelativeTo(getActiveWindow());

        dialogStack.offer(dialog);
        dialog.setVisible(true);
    }

    /**
     * アクティブなウィンドウを返します。
     *
     * @return {@link Window}
     */
    public Window getActiveWindow() {
        return dialogStack.isEmpty() ? frame : dialogStack.peek();
    }

    /**
     * 実行中アニメーションを開始します。
     */
    public void startWait() {
        getWaitLayerUI().start();
    }

    /**
     * 実行中アニメーションを停止します。
     */
    public void stopWait() {
        getWaitLayerUI().stop();
    }

    /**
     * タスク実行中フラグを返します。
     *
     * @return タスク実行中フラグ
     */
    public boolean isTaskRunning() {
        return getWaitLayerUI().isRunning();
    }

    /**
     * アクティブなウィンドウのWaitLayerUIを返します。
     *
     * @return {@link WaitLayerUI}
     */
    private WaitLayerUI getWaitLayerUI() {
        Window window = getActiveWindow();
        Container contentPane;
        if (window == frame) {
            contentPane = frame.getContentPane();
        } else {
            contentPane = ((JDialog) window).getContentPane();
        }
        return (WaitLayerUI) ((JLayer<?>) contentPane.getComponent(0)).getUI();
    }

    /**
     * 全てのダイアログを閉じます。
     */
    public void closeAllDialog() {
        while (!dialogStack.isEmpty()) {
            dialogStack.poll().dispose();
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.CsvMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;

/**
 *
 * 対象生徒選択画面のCSV帳票メーカーの入力Beanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainCsvInBean implements CsvMakerInBean {

    /** 対象模試 */
    private final ExamBean exam;

    /** 個人IDリスト */
    private final List<Integer> idList;

    /**
     * コンストラクタです。
     *
     * @param exam 対象模試
     * @param idList 個人IDリスト
     */
    public SelstdMainCsvInBean(ExamBean exam, List<Integer> idList) {
        this.exam = exam;
        this.idList = idList;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試ド
     */
    public ExamBean getExam() {
        return exam;
    }

    /**
     * 個人IDリストを返します。
     *
     * @return 個人IDリスト
     */
    public List<Integer> getIdList() {
        return idList;
    }

}

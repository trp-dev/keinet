package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

/**
 *
 * 大学基本情報/情報誌用科目日程ファイルDAO出力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
public class ExmntnUnivDaoOutBean {

    /** 募集定員 */
    private String offerCapacity;

    /** 募集信頼 */
    private String capacityFlag;

    /** 出願締切 */
    private String entExamApplideadlineDiv;

    /** 入試 */
    private String examDay;

    /** 合格発表 */
    private String sucannDate;

    /** 手続締切 */
    private String proceDeadline;

    /** 本部所在地 */
    private String prefBase;

    /** キャンパス所在地 */
    private String prefCampus;

    /** センタ備考 */
    private String centerNote;

    /** 二次備考 */
    private String secondNote;

    /** 足切予想得点 */
    private int rejectExScore;

    /** 注釈文 */
    private String commentStatement;

    /**
     * 募集定員を返します。
     *
     * @return 募集定員
     */
    public String getOfferCapacity() {
        return offerCapacity;
    }

    /**
     * 募集定員をセットします。
     *
     * @param offerCapacity 募集定員
     */
    public void setOfferCapacity(String offerCapacity) {
        this.offerCapacity = offerCapacity;
    }

    /**
     * 募集信頼を返します。
     *
     * @return 募集信頼
     */
    public String getCapacityFlag() {
        return capacityFlag;
    }

    /**
     * 募集信頼をセットします。
     *
     * @param capacityFlag 募集信頼
     */
    public void setCapacityFlag(String capacityFlag) {
        this.capacityFlag = capacityFlag;
    }

    /**
     * 出願締切を返します。
     *
     * @return 出願締切
     */
    public String getEntExamApplideadlineDiv() {
        return entExamApplideadlineDiv;
    }

    /**
     * 出願締切をセットします。
     *
     * @param entExamApplideadlineDiv 出願締切
     */
    public void setEntExamApplideadlineDiv(String entExamApplideadlineDiv) {
        this.entExamApplideadlineDiv = entExamApplideadlineDiv;
    }

    /**
     * 入試を返します。
     *
     * @return 入試
     */
    public String getExamDay() {
        return examDay;
    }

    /**
     * 入試をセットします。
     *
     * @param examDay 入試
     */
    public void setExamDay(String examDay) {
        this.examDay = examDay;
    }

    /**
     * 合格発表を返します。
     *
     * @return 合格発表
     */
    public String getSucannDate() {
        return sucannDate;
    }

    /**
     * 合格発表をセットします。
     *
     * @param sucannDate 合格発表
     */
    public void setSucannDate(String sucannDate) {
        this.sucannDate = sucannDate;
    }

    /**
     * 手続締切を返します。
     *
     * @return 手続締切
     */
    public String getProceDeadline() {
        return proceDeadline;
    }

    /**
     * 手続締切をセットします。
     *
     * @param proceDeadline 手続締切
     */
    public void setProceDeadline(String proceDeadline) {
        this.proceDeadline = proceDeadline;
    }

    /**
     * 本部所在地を返します。
     *
     * @return 本部所在地
     */
    public String getPrefBase() {
        return prefBase;
    }

    /**
     * 本部所在地をセットします。
     *
     * @param prefBase 本部所在地
     */
    public void setPrefBase(String prefBase) {
        this.prefBase = prefBase;
    }

    /**
     * キャンパス所在地を返します。
     *
     * @return キャンパス所在地
     */
    public String getPrefCampus() {
        return prefCampus;
    }

    /**
     * キャンパス所在地をセットします。
     *
     * @param prefCampus キャンパス所在地
     */
    public void setPrefCampus(String prefCampus) {
        this.prefCampus = prefCampus;
    }

    /**
     * センタ備考を返します。
     *
     * @return センタ備考
     */
    public String getCenterNote() {
        return centerNote;
    }

    /**
     * センタ備考をセットします。
     *
     * @param centerNote センタ備考
     */
    public void setCenterNote(String centerNote) {
        this.centerNote = centerNote;
    }

    /**
     * 二次備考を返します。
     *
     * @return 二次備考
     */
    public String getSecondNote() {
        return secondNote;
    }

    /**
     * 二次備考をセットします。
     *
     * @param secondNote 二次備考
     */
    public void setSecondNote(String secondNote) {
        this.secondNote = secondNote;
    }

    /**
     * 足切予想得点を返します。
     *
     * @return 足切予想得点
     */
    public int getRejectExScore() {
        return rejectExScore;
    }

    /**
     * 足切予想得点をセットします。
     *
     * @param rejectExScore 足切予想得点
     */
    public void setRejectExScore(int rejectExScore) {
        this.rejectExScore = rejectExScore;
    }

    /**
     * 注釈文を返します。
     *
     * @return 注釈文
     */
    public String getCommentStatement() {
        return commentStatement;
    }

    /**
     * 注釈文をセットします。
     *
     * @param commentStatement 注釈文
     */
    public void setCommentStatement(String commentStatement) {
        this.commentStatement = commentStatement;
    }

}

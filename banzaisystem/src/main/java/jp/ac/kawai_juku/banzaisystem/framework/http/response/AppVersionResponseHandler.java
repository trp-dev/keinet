package jp.ac.kawai_juku.banzaisystem.framework.http.response;

import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * アプリケーションバージョン情報の応答XMLハンドラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AppVersionResponseHandler extends BaseResponseHandler<AppVersionResponseBean> {

    /** appタグ終了フラグ */
    private boolean endAppFlg;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (qName.equals("app")) {
            getResponseBean().setVersion(attributes.getValue("version"));
        }
    }

    @Override
    protected void endElement(String qName, String value) {
        if (qName.equals("app")) {
            endAppFlg = true;
        } else if (qName.equals("module")) {
            getResponseBean().getModuleList().add(value);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        if (!endAppFlg) {
            fatalError(new SAXParseException("appタグが終了していません。", null));
        }
        if (getResponseBean().getVersion() == null) {
            fatalError(new SAXParseException("version属性が設定されていません。", null));
        }
        if (!ValidateUtil.isPositiveReal(getResponseBean().getVersion())) {
            fatalError(new SAXParseException("versionが数値ではありません。", null));
        }
        if (getResponseBean().getModuleList().isEmpty()) {
            fatalError(new SAXParseException("moduleタグが設定されていません。", null));
        }
    }

    @Override
    protected AppVersionResponseBean createResponseBean() {
        return new AppVersionResponseBean();
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.http.response;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * 更新データ配信サーバからの応答XMLを処理する基底SAXハンドラです。
 *
 *
 * @param <T> {@link BaseResponseBean}
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseResponseHandler<T extends BaseResponseBean> extends DefaultHandler {

    /** タグ値のバッファ */
    protected final StringBuffer buff = new StringBuffer(128);

    /** 応答データBean */
    private final T outBean = createResponseBean();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        buff.setLength(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        buff.append(ch, start, length);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        endElement(qName, buff.toString());
    }

    /**
     * タグ終了時の処理を行います。
     *
     * @param qName タグ名
     * @param value タグ値
     */
    protected abstract void endElement(String qName, String value);

    /**
     * 応答データBeanのインスタンスを生成します。
     *
     * @return {@link T}
     */
    protected abstract T createResponseBean();

    /**
     * 応答データBeanの値を返します。
     *
     * @return 応答データBeanの値
     */
    public final T getResponseBean() {
        return outBean;
    }

}

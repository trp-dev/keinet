package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.border.MatteBorder;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * コンボボックスです。
 *
 * @param <T> 値の型
 * @author TOTEC)morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings({ "serial" })
public class ComboBox<T> extends JComboBox<ComboBoxItem<T>> implements BZComponent {

    /** アクションイベント起動フラグ */
    private boolean fireActionEventFlg;

    /** 変更前の値 */
    private ComboBoxItem<?> previousItem;

    /**
     * コンストラクタです。
     */
    public ComboBox() {
        setBorder(new MatteBorder(1, 1, 1, 1, new Color(165, 172, 178)));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        if (!field.isAnnotationPresent(NoAction.class)) {
            Action action = field.getAnnotation(Action.class);
            final ActionListener listener;
            if (action == null) {
                listener = new ControllerActionListener(view, field.getName());
            } else {
                listener = new ControllerActionListener(view, action.value());
            }
            /* ActionListenerだと同じ値の再選択でイベントが発生するため、ItemListenerを利用する */
            addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        listener.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, null));
                    } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                        previousItem = (ComboBoxItem<?>) e.getItem();
                    }
                }
            });
        }
        fireActionEventFlg = field.isAnnotationPresent(FireActionEvent.class);
    }

    /**
     * 変更前の値に戻します。
     */
    public void turnBack() {
        if (previousItem != null) {
            ItemListener[] listeners = getItemListeners();
            for (ItemListener listener : listeners) {
                removeItemListener(listener);
            }
            setSelectedItem(previousItem);
            for (ItemListener listener : listeners) {
                addItemListener(listener);
            }
        }
    }

    /**
     * 変更前の値を返します。
     *
     * @return 変更前の値
     */
    public T getPreviousValue() {
        if (previousItem == null) {
            return null;
        } else {
            for (int i = 0; i < getItemCount(); i++) {
                if (previousItem.equals(getItemAt(i))) {
                    return getItemAt(i).getValue();
                }
            }
        }
        return null;
    }

    @Override
    public ComboBoxItem<T> getSelectedItem() {
        return getItemAt(getSelectedIndex());
    }

    /**
     * 選択中アイテムの値を返します。
     *
     * @return 値（選択中のアイテムが存在しない場合はnull）
     */
    public T getSelectedValue() {
        ComboBoxItem<T> item = getItemAt(getSelectedIndex());
        return item == null ? null : item.getValue();
    }

    /**
     * 選択中アイテムのラベルを返します。
     *
     * @return ラベル（選択中のアイテムが存在しない場合はnull）
     */
    public String getSelectedLabel() {
        ComboBoxItem<T> item = getItemAt(getSelectedIndex());
        return item == null ? null : item.getLabel();
    }

    /**
     * 選択値を保持したまま、コンボボックスにアイテムリストをセットします。
     *
     * @param itemList アイテムリスト
     */
    public void setItemList(List<ComboBoxItem<T>> itemList) {
        setItemList(itemList, true);
    }

    /**
     * コンボボックスにアイテムリストをセットします。
     *
     * @param itemList アイテムリスト
     * @param holdFlg 選択値保持フラグ
     */
    public void setItemList(List<ComboBoxItem<T>> itemList, boolean holdFlg) {
        setItemList(itemList, holdFlg ? getSelectedItem() : null);
    }

    /**
     * コンボボックスにアイテムリストをセットします。
     *
     * @param itemList アイテムリスト
     * @param selectedItem 選択値
     */
    public void setItemList(List<ComboBoxItem<T>> itemList, Object selectedItem) {

        /* アイテムリスナを削除する */
        ItemListener[] listeners = super.getItemListeners();
        for (ItemListener listener : listeners) {
            removeItemListener(listener);
        }

        /* アイテムを全削除→追加する */
        if (selectedItem == null || !selectedItem.equals(getSelectedItem()) || !equalItems(itemList)) {
            /*
             * 塾生データにおけるクラス番号コンボボックスなど、大量のデータを入れ替える場合に
             * 表示が遅くなる場合があるため、コンボボックスの内容に変更がある場合のみ処理する。
             */
            removeAllItems();
            for (ComboBoxItem<T> item : itemList) {
                addItem(item);
            }
        }

        /* 選択値を復元する */
        if (selectedItem != null) {
            setSelectedItem(selectedItem);
        }

        /* アイテムリスナを再度登録する */
        for (ItemListener listener : listeners) {
            addItemListener(listener);
        }

        /* 値変更イベントを起こす */
        if (fireActionEventFlg) {
            fireActionEventForce();
        }
    }

    /**
     * アイテムリストが現在の表示内容と同じであるかを判定します。
     *
     * @param itemList アイテムリスト
     * @return アイテムリストが現在の表示内容と同じならtrue
     */
    private boolean equalItems(List<ComboBoxItem<T>> itemList) {
        if (itemList.size() != getItemCount()) {
            return false;
        }
        for (int i = 0; i < getItemCount(); i++) {
            ComboBoxItem<T> item = getItemAt(i);
            ComboBoxItem<T> tagetItem = itemList.get(i);
            if (!item.equals(tagetItem) || !item.getLabel().equals(tagetItem.getLabel())) {
                return false;
            }
        }
        return true;
    }

    /**
     * コンボボックスの内容を選択位置含めてコピーします。
     *
     * @param src コピー元
     */
    public void copyFrom(ComboBox<T> src) {
        copyFrom(src, true);
    }

    /**
     * コンボボックスの内容をコピーします。
     *
     * @param src コピー元
     * @param copyIndexFlg 選択位置コピーフラグ
     */
    public void copyFrom(ComboBox<T> src, boolean copyIndexFlg) {

        /* アイテムリスナを削除する */
        ItemListener[] listeners = super.getItemListeners();
        for (ItemListener listener : listeners) {
            removeItemListener(listener);
        }

        /* アイテムを全削除→追加する */
        removeAllItems();
        for (int index = 0; index < src.getItemCount(); index++) {
            addItem(src.getItemAt(index));
        }

        /* 選択位置をコピーする */
        if (copyIndexFlg) {
            setSelectedIndex(src.getSelectedIndex());
        }

        /* アイテムリスナを再度登録する */
        for (ItemListener listener : listeners) {
            addItemListener(listener);
        }

        /* 値変更イベントを起こす */
        if (fireActionEventFlg) {
            fireActionEventForce();
        }
    }

    /**
     * 値変更イベントを起こさずに選択インデックスを変更します。
     *
     * @param anIndex インデックス
     */
    public void setSelectedIndexSilent(int anIndex) {
        ItemListener[] listeners = super.getItemListeners();
        for (ItemListener listener : listeners) {
            removeItemListener(listener);
        }
        setSelectedIndex(anIndex);
        for (ItemListener listener : listeners) {
            addItemListener(listener);
        }
    }

    /**
     * 値変更イベントを起こして選択インデックスを変更します。
     *
     * @param anIndex インデックス
     */
    public void setSelectedIndexNoisy(int anIndex) {
        setSelectedIndexSilent(anIndex);
        fireActionEventForce();
    }

    /**
     * 値変更イベントを強制的に起こします。
     */
    private void fireActionEventForce() {
        fireItemStateChanged(new ItemEvent(this, ItemEvent.ITEM_STATE_CHANGED, getSelectedItem(), ItemEvent.SELECTED));
    }

}

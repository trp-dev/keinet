package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.view;

import java.util.ArrayList;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.component.ExmntnScdlTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

/**
 *
 * 受験スケジュール画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnScdlView extends SubView {

    /** 並べ替えコンボボックス */
    @Location(x = 61, y = 10)
    @Size(width = 99, height = 20)
    public ComboBox<Code> sortComboBox;

    /** 受験スケジュールテーブル */
    @Location(x = 7, y = 36)
    @Size(width = 668, height = 411)
    public ExmntnScdlTable scheduleTable;

    /** ↑ボタン */
    @Location(x = 450, y = 8)
    @Disabled
    public ImageButton upButton;

    /** ↓ボタン */
    @Location(x = 476, y = 8)
    @Disabled
    public ImageButton downButton;

    /** 削除ボタン */
    @Location(x = 514, y = 8)
    @Disabled
    public ImageButton deleteButton;

    /** 詳細判定ボタン */
    @Location(x = 581, y = 8)
    @Disabled
    public ImageButton detailJudgeButton;

    /** 印刷するボタン */
    @Location(x = 547, y = 451)
    public ImageButton printButton;

    /** 備考欄ラベル */
    @Location(x = 17, y = 451)
    @Size(width = 531, height = 33)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel remarksLabel;

    @Override
    public void initialize() {

        super.initialize();

        /* 並べ替えコンボボックスを初期化する */
        List<ComboBoxItem<Code>> itemList = new ArrayList<>();
        for (Code code : CodeType.SCHEDULE_ORDER.findCodeList()) {
            itemList.add(new ComboBoxItem<Code>(code.getName(), code));
        }
        sortComboBox.setItemList(itemList);

        /* 並べ替えコンボボックスを受験スケジュールテーブルにセットする */
        scheduleTable.setSortComboBox(sortComboBox);
    }

}

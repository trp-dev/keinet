package jp.ac.kawai_juku.banzaisystem.framework.component;

import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;

/**
 *
 * サブ画面の管理インターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface SubViewManager {

    /**
     * 指定したサブ画面をアクティブにします。
     *
     * @param clazz サブ画面のコントローラクラス
     * @param methodName アクティブにするタイミングで起動するメソッド名
     */
    void activate(Class<? extends SubController> clazz, String methodName);

}

package jp.ac.kawai_juku.banzaisystem.mainte.impt.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.helper.MainteImptHelper;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.service.MainteImptService;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.view.MainteImptView;

/**
 *
 * メンテナンス-個人成績インポート画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteImptController extends SubController {

    /** View */
    @Resource(name = "mainteImptView")
    private MainteImptView view;

    /** Service */
    @Resource
    private MainteImptService mainteImptService;

    /** Helper */
    @Resource
    private MainteImptHelper mainteImptHelper;

    @Override
    @Logging(GamenId.E1)
    public void activatedAction() {
        initTable();
    }

    /**
     * インポート状況一覧テーブルを初期化します。
     */
    private void initTable() {
        view.mainteImportStatusTable.setDataList(mainteImptService.createDataList());
    }

    /**
     * 個人志望手動インポートボタン押下アクションです。
     *
     * @return インポートタスク
     */
    @Execute
    public ExecuteResult personalWishHandOpeButtonAction() {
        return mainteImptHelper.importCsvAction(getActiveWindow(), new Runnable() {
            @Override
            public void run() {
                initTable();
            }
        });
    }

}

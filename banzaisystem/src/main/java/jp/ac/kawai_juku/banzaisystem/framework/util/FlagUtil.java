package jp.ac.kawai_juku.banzaisystem.framework.util;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_OFF;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_ON;

/**
 *
 * フラグに関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class FlagUtil {

    /**
     * コンストラクタです。
     */
    protected FlagUtil() {
    }

    /**
     * booleanからフラグ文字列に変換します。
     *
     * @param bool boolean値
     * @return フラグ文字列
     */
    public static String toString(boolean bool) {
        return bool ? FLG_ON : FLG_OFF;
    }

    /**
     * フラグ文字列からbooleanに変換します。
     *
     * @param flag フラグ文字列
     * @return boolean値
     */
    public static boolean toBoolean(String flag) {
        return FLG_ON.equals(flag);
    }

}

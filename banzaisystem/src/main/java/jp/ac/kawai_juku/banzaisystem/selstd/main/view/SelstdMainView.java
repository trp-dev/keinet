package jp.ac.kawai_juku.banzaisystem.selstd.main.view;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeRadio;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainCandidateTable;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainNameTable;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainScoreTable;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 対象生徒選択画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainView extends AbstractView {

    /** 対象生徒選択ラベル */
    @Location(x = 281, y = 2)
    public ImageLabel selectStudentLabel;

    /** 個人成績・志望大学ボタン */
    @Location(x = 385, y = 2)
    public ImageButton exmntnButton;

    /** 帳票出力ボタン */
    @Location(x = 518, y = 2)
    public ImageButton reportButton;

    /** メンテナンスボタン */
    @Location(x = 632, y = 2)
    public ImageButton maintenanceButton;

    /** インフォメーションボタン */
    @Location(x = 736, y = 2)
    public ImageButton infoButton;

    /** インフォメーションラベル */
    @Location(x = 866, y = 2)
    public TextLabel informationLabel;

    /** 入試結果入力ボタン */
    @Location(x = 736, y = 2)
    @Invisible
    public ImageButton resultButton;

    /** ヘルプボタン */
    @Location(x = 854, y = 2)
    @Invisible
    public ImageButton helpButton;

    /** 終了ボタン */
    @Location(x = 928, y = 2)
    public ImageButton exitButton;

    /** インポートボタン */
    @Location(x = 316, y = 49)
    public ImageButton checkImportSituationButton;

    /** コンボボックス_模試 */
    @Location(x = 102, y = 50)
    @Size(width = 207, height = 20)
    @FireActionEvent
    public ComboBox<ExamBean> examComboBox;

    /** コンボボックス_学年 */
    @Location(x = 29, y = 80)
    @Size(width = 68, height = 20)
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** コンボボックス_クラス */
    @Location(x = 151, y = 80)
    @Size(width = 68, height = 20)
    @FireActionEvent
    public ComboBox<String> classComboBox;

    /** 「個人名のみ」ラジオ */
    @Location(x = 230, y = 85)
    @CodeConfig(Code.DISP_ITEM_NAME)
    public CodeRadio nameRadio;

    /** 「個人名＋成績」ラジオ */
    @Location(x = 326, y = 85)
    @CodeConfig(Code.DISP_ITEM_SCORE)
    @Selected
    public CodeRadio scoreRadio;

    /** 「個人名＋志望大学」ラジオ */
    @Location(x = 431, y = 85)
    @CodeConfig(Code.DISP_ITEM_UNIV)
    public CodeRadio univRadio;

    /** 英語認定試験チェックボックス */
    /*        @Location(x = 556, y = 85)
    @CodeConfig(Code.SEARCH_ENG_JOINING_EXCLUDE_ONLY)
    public CodeCheckBox englishJoinningCheckBox;*/

    /** 説明文 */
    @Location(x = 656, y = 50)
    public TextLabel descLabel;

    /** 個人のみテーブル */
    @Location(x = 13, y = 120)
    @Size(width = 994, height = 540)
    public SelstdMainNameTable nameTable;

    /** 個人名＋成績テーブル */
    @Location(x = 13, y = 120)
    @Size(width = 994, height = 540)
    public SelstdMainScoreTable scoreTable;

    /** 個人名＋志望大学テーブル */
    @Location(x = 13, y = 120)
    @Size(width = 994, height = 540)
    public SelstdMainCandidateTable candidateTable;

    /** 生徒追加ボタン */
    @Location(x = 23, y = 667)
    public ImageButton addStudentButton;

    /** 生徒更新タン */
    @Location(x = 144, y = 667)
    @Disabled
    public ImageButton editStudentButton;

    /** 生徒削除ボタン */
    @Location(x = 265, y = 667)
    @Disabled
    public ImageButton deleteStudentButton;

    /** 個人成績・志望大学確認ボタン */
    @Location(x = 424, y = 667)
    public ImageButton largeExmntnButton;

    /** 印刷ボタン */
    @Location(x = 889, y = 664)
    public ImageButton printButton;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
        examComboBox.setMaximumRowCount(10);
    }

}

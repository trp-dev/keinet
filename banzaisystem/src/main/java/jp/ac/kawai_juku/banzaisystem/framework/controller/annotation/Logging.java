package jp.ac.kawai_juku.banzaisystem.framework.controller.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.GamenId;

import org.seasar.framework.aop.annotation.Interceptor;

/**
 *
 * ログ出力するコントローラのメソッドに設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
@Interceptor
public @interface Logging {

    /**
     * 画面IDを設定します。
     *
     * @return 画面ID
     */
    GamenId value();

}

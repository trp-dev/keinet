package jp.ac.kawai_juku.banzaisystem.framework.controller;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;

/**
 *
 * 「終了する」ボタンがある画面の抽象コントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class ExitController extends AbstractController {

    /**
     * 「終了する」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exitButtonAction() {
        /* ×ボタン押下イベントを発生させる */
        Toolkit.getDefaultToolkit().getSystemEventQueue()
                .postEvent(new WindowEvent(getActiveWindow(), WindowEvent.WINDOW_CLOSING));
        return null;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.interceptor;

import static org.seasar.framework.container.SingletonS2Container.getComponent;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;

import org.aopalliance.intercept.MethodInvocation;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;

/**
 *
 * ログ出力インターセプタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class LoggingInterceptor extends AbstractInterceptor {

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        getComponent(LogService.class).output(mi.getMethod().getAnnotation(Logging.class).value());
        return mi.proceed();
    }

}

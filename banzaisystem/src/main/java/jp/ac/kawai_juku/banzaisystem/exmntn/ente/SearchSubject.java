package jp.ac.kawai_juku.banzaisystem.exmntn.ente;

/**
 *
 * 検索用科目インターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface SearchSubject {

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    String getName();

}

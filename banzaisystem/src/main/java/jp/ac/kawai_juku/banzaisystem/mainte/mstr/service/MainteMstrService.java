package jp.ac.kawai_juku.banzaisystem.mainte.mstr.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.UNIV_FILE_NAME;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.VERSION_NUMBER;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClient;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClientException;
import jp.ac.kawai_juku.banzaisystem.framework.http.response.AppVersionResponseBean;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.UnivCdHookUpService;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.TempFileUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet.UnivDb;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.service.MainteStngService;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.bean.SystopLginDataStatusBean;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.service.SystopLginService;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * メンテナンス-大学マスタ・プログラム最新化画面のサービスです。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMstrService {

    /** マスタ・プログラムチェック画面のサービス */
    @Resource
    private SystopLginService systopLginService;

    /** メンテナンス-環境設定画面のサービス */
    @Resource
    private MainteStngService mainteStngService;

    /** 初期化処理サービス */
    @Resource
    private InitializeService initializeService;

    /** 大学コード紐付けサービス */
    @Resource
    private UnivCdHookUpService univCdHookUpService;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * ダウンロード可能大学マスタセットを取得します。
     *
     * @param client {@link UpdaterHttpClient}
     * @return ダウンロード可能大学マスタセット
     * @throws UpdaterHttpClientException 通信エラー発生時
     */
    public UnivDbSet findDbSet(UpdaterHttpClient client) throws UpdaterHttpClientException {
        Set<String> set = CollectionsUtil.newHashSet();
        for (String file : client.dbVersion().getFileList()) {
            String baseName = FilenameUtils.getBaseName(file);
            if (baseName.length() == 8) {
                set.add(baseName);
            }
        }
        return new UnivDbSet(set);
    }

    /**
     * アプリケーションバージョン情報を取得します。
     *
     * @param client {@link UpdaterHttpClient}
     * @return {@link AppVersionResponseBean}
     * @throws UpdaterHttpClientException 通信エラー発生時
     */
    public AppVersionResponseBean findAppVersion(UpdaterHttpClient client) throws UpdaterHttpClientException {
        return client.appVersion();
    }

    /**
     * テーブルのデータリストを生成します。
     *
     * @param dbSet ダウンロード可能大学マスタセット
     * @return データリスト
     */
    public Vector<Vector<Object>> createDataList(UnivDbSet dbSet) {

        List<SystopLginDataStatusBean> list = systopLginService.findDataStatusList(dbSet);

        String currentMaster = systemDto.getUnivYear() + systemDto.getUnivExamDiv();

        int downloadableCount = 0;

        Vector<Vector<Object>> result = CollectionsUtil.newVector(list.size());
        for (SystopLginDataStatusBean bean : list) {
            Vector<Object> row = CollectionsUtil.newVector(3);
            row.add(new CellData(bean));
            row.add(isDownloadable(dbSet, currentMaster, bean) ? Boolean.valueOf(downloadableCount++ == 0) : null);
            row.add(new CellData(CodeType.UNIV_DL_STATUS.value2Code(bean.getUnivStatus())));
            result.add(row);
        }

        return result;
    }

    /**
     * ダウンロード可能な大学マスタであるか判定します。
     *
     * @param dbSet ダウンロード可能大学マスタセット
     * @param currentMaster 現在の大学マスタ
     * @param bean {@link SystopLginDataStatusBean}
     * @return ダウンロード可能な大学マスタならtrue
     */
    private boolean isDownloadable(UnivDbSet dbSet, String currentMaster, SystopLginDataStatusBean bean) {

        /* 状況が「提供予定」はダウンロード不可 */
        if (CodeType.UNIV_DL_STATUS.value2Code(bean.getUnivStatus()) == Code.UNIV_DL_UNAVAILABLE) {
            return false;
        }

        /* 現在の大学マスタよりも古いものはダウンロード不可 */
        if (currentMaster.compareTo(bean.getExamYear() + bean.getExamDiv()) > 0) {
            return false;
        }

        /* サーバ上に存在していればダウンロード可 */
        return dbSet != null && dbSet.get(bean.getExamYear(), bean.getExamDiv()) != null;
    }

    /**
     * プログラムを更新します。
     */
    public void updateProgram() {

        /* 更新機能のHTTPクライアントを初期化する */
        UpdaterHttpClient client = new UpdaterHttpClient();

        /* 更新データ配信サイトからアプリケーションバージョン情報を取得する */
        AppVersionResponseBean bean;
        try {
            bean = client.appVersion();
        } catch (UpdaterHttpClientException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"), e);
        }

        if (Double.parseDouble(bean.getVersion()) <= VERSION_NUMBER) {
            /* 現在のバージョンよりも新しくなければ処理はここまで */
            return;
        }

        /* 作業用フォルダを作る */
        File workFolder;
        try {
            workFolder = makeWorkFolder();
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E012"), e);
        }

        /* プログラムを更新する */
        try {
            updateProgram(client, bean, workFolder);
        } catch (UpdaterHttpClientException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"), e);
        } finally {
            if (workFolder.exists()) {
                FileUtils.deleteQuietly(workFolder);
            }
        }
    }

    /**
     * ダウンロード作業用フォルダを生成します。
     *
     * @return ダウンロード作業用フォルダ
     * @throws IOException IO例外
     */
    private File makeWorkFolder() throws IOException {
        File workFolder = new File("lib/download");
        if (workFolder.isDirectory()) {
            FileUtils.deleteDirectory(workFolder);
        }
        if (!workFolder.mkdir()) {
            throw new IOException("ダウンロード作業用フォルダの作成に失敗しました。" + workFolder);
        }
        return workFolder;
    }

    /**
     * プログラムを更新します。
     *
     * @param client {@link UpdaterHttpClient}
     * @param bean {@link AppVersionResponseBean}
     * @param workFolder 作業用フォルダ
     * @throws UpdaterHttpClientException 通信エラー
     */
    private void updateProgram(UpdaterHttpClient client, AppVersionResponseBean bean, File workFolder) throws UpdaterHttpClientException {

        File lib = new File("lib");

        /* libフォルダ配下に存在しないモジュールリストを作る */
        List<String> moduleList = CollectionsUtil.newArrayList();
        for (String module : bean.getModuleList()) {
            if (FileUtils.listFiles(lib, FileFilterUtils.nameFileFilter(module, IOCase.INSENSITIVE), FileFilterUtils.trueFileFilter()).isEmpty()) {
                moduleList.add(module);
            }
        }

        /* ダウンロード開始ログを出力する */
        outputLog("ダウンロード開始", BZConstants.VERSION, bean.getVersion());

        /* 作業用フォルダにダウンロードする */
        for (String module : moduleList) {
            client.appDownload(module, new File(workFolder, module));
        }

        /* ダウンロード終了ログを出力する */
        outputLog("ダウンロード終了", BZConstants.VERSION, bean.getVersion());

        /* 更新開始ログを出力する */
        outputLog("更新開始", BZConstants.VERSION, bean.getVersion());

        /* 作業フォルダをバージョンと同じ名前にリネームする */
        try {
            IOUtil.renameFile(workFolder, bean.getVersion());
        } catch (IORuntimeException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E012"), e);
        }

        /* 更新終了ログを出力する */
        outputLog("更新終了", BZConstants.VERSION, bean.getVersion());
    }

    /**
     * 大学マスタをダウンロードします。
     *
     * @param client {@link UpdaterHttpClient}
     * @param file ダウンロード先ファイル
     * @param univDb {@link UnivDb}
     */
    public void downloadUnivMaster(UpdaterHttpClient client, File file, UnivDb univDb) {

        /* 年度 */
        String examYear = univDb.getEventYear();

        /* 模試区分 */
        String examDiv = univDb.getExamDiv();

        /* ダウンロード開始ログを出力する */
        outputLog("大学マスタ（DL）", "ダウンロード開始", examYear, examDiv, file.getAbsolutePath());

        /* ダウンロードする */
        try {
            client.dbDownload(univDb, file);
        } catch (UpdaterHttpClientException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"), e);
        }

        /* ダウンロード終了ログを出力する */
        outputLog("大学マスタ（DL）", "ダウンロード終了", examYear, examDiv);
    }

    /**
     * 大学マスタ情報を取得します。
     *
     * @param file 大学マスタファイル
     * @return {@link UnivMasterBean}
     * @throws SQLException 大学マスタ情報の取得に失敗した場合
     */
    public UnivMasterBean getUnivMasterBean(File file) throws SQLException {
        try (Connection con = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath())) {
            return DaoUtil.findUnivMasterBean(con);
        }
    }

    /**
     * 大学マスタバージョンを取得します。
     *
     * @param file 大学マスタファイル
     * @return 大学マスタバージョン（行事年度4桁＋模試区分2桁）
     * @throws SQLException 大学マスタ情報の取得に失敗した場合
     */
    private String getMasterVersion(File file) throws SQLException {
        UnivMasterBean univMater = getUnivMasterBean(file);
        return univMater.getEventYear() + univMater.getExamDiv();
    }

    /**
     * 成績データバージョンを取得します。
     *
     * @param file 成績データファイル
     * @return 成績データバージョン
     * @throws SQLException 成績データバージョンの取得に失敗した場合
     */
    public int getRecordVersion(File file) throws SQLException {
        try (Connection con = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath())) {
            return DaoUtil.getRecordVersion(con);
        }
    }

    /**
     * 大学マスタを更新します。
     *
     * @param client {@link UpdaterHttpClient}
     * @param univDb {@link UnivDb}
     * @param isReboot 再起動有無
     */
    public void updateMaster(UpdaterHttpClient client, UnivDb univDb, boolean isReboot) {

        /* 年度 */
        String examYear = univDb.getEventYear();

        /* 模試区分 */
        String examDiv = univDb.getExamDiv();

        /* 現在の大学マスタファイル */
        File currentFile = findCurrentMasterFile();

        /* 一時ファイルを生成する */
        File file = TempFileUtil.createTempFile();
        try {
            /* ダウンロード開始ログを出力する */
            outputLog("大学マスタ（更新）", "ダウンロード開始", examYear, examDiv);

            /* ダウンロードする */
            try {
                client.dbDownload(univDb, file);
            } catch (UpdaterHttpClientException e) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E010"), e);
            }

            /* ダウンロード終了ログを出力する */
            outputLog("大学マスタ（更新）", "ダウンロード終了", examYear, examDiv);

            /* 更新開始ログを出力する */
            outputLog("大学マスタ（更新）", "更新開始", systemDto.getUnivYear(), systemDto.getUnivExamDiv(), examYear, examDiv, currentFile.getParentFile().getAbsolutePath());

            /* 大学マスタバージョンを取得する */
            String masterVersion;
            try {
                masterVersion = getMasterVersion(file);
            } catch (SQLException e) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E011"), e);
            }

            /* ダウンロード条件と大学マスタバージョンが不一致ならエラーとする */
            if (!masterVersion.equals(examYear + examDiv)) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E011"), new RuntimeException(String.format("ダウンロード条件と大学マスタバージョンが不一致です。[ダウンロード条件=%s][大学マスタバージョン=%s]", examYear + examDiv, masterVersion)));
            }

            /* 現在の大学マスタよりも古ければエラーとする */
            if (masterVersion.compareTo(systemDto.getUnivYear() + systemDto.getUnivExamDiv()) < 0) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E013"));
            }

            /* 現在の大学マスタファイルをリネームする */
            File backupFile;
            try {
                backupFile = renameCurrentMasterFile(currentFile);
            } catch (IORuntimeException e) {
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E011"), e);
            }

            /* 大学マスタファイルを移動する */
            try {
                FileUtils.moveFile(file, currentFile);
            } catch (IOException e) {
                /* 移動に失敗した場合は、バックアップに戻す */
                backupFile.renameTo(currentFile);
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E011"), e);
            }

            /* 更新終了ログを出力する */
            outputLog("大学マスタ（更新）", "更新終了", systemDto.getUnivYear(), systemDto.getUnivExamDiv(), examYear, examDiv);

            /* 大学マスタファイル切り替え後の後処理を行う */
            afterSwitchMasterProcess(isReboot);
        } finally {
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 大学マスタファイルを取り込みます。
     *
     * @param file 取り込む大学マスタファイル
     * @param univMasterBean 取り込む大学マスタファイルの大学マスタ情報
     * @param isReboot 再起動有無
     */
    public void importFile(File file, UnivMasterBean univMasterBean, boolean isReboot) {

        /* 現在の大学マスタファイル */
        File currentFile = findCurrentMasterFile();

        /* 更新開始ログを出力する */
        outputLog("大学マスタ（手動）", "更新開始", systemDto.getUnivYear(), systemDto.getUnivExamDiv(), StringUtils.EMPTY, StringUtils.EMPTY, file.getAbsolutePath(), currentFile.getParentFile().getAbsolutePath());

        /* 現在の大学マスタよりも古ければエラーとする */
        if ((univMasterBean.getEventYear() + univMasterBean.getExamDiv()).compareTo(systemDto.getUnivYear() + systemDto.getUnivExamDiv()) < 0) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E013"));
        }

        /* 現在の大学マスタファイルをリネームする */
        File backupFile;
        try {
            if (IOUtil.isIdentical(file, currentFile)) {
                /* 取り込む大学マスタファイルと現在の大学マスタファイルが同一なら処理はここまで */
                return;
            }
            backupFile = renameCurrentMasterFile(currentFile);
        } catch (IORuntimeException e) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E002"), e);
        }

        /* 取り込む大学マスタファイルをコピーする */
        try {
            FileUtils.copyFile(file, currentFile, false);
        } catch (IOException e) {
            /* コピーに失敗した場合は、バックアップに戻す */
            backupFile.renameTo(currentFile);
            throw new MessageDialogException(getMessage("error.mainte.mstr.E_E002"), e);
        }

        /* 更新終了ログを出力する */
        outputLog("大学マスタ（手動）", "更新終了", systemDto.getUnivYear(), systemDto.getUnivExamDiv(), univMasterBean.getEventYear(), univMasterBean.getExamDiv());

        /* 大学マスタファイル切り替え後の後処理を行う */
        afterSwitchMasterProcess(isReboot);
    }

    /**
     * 現在の大学マスタファイルを返します。
     *
     * @return 現在の大学マスタファイル
     */
    private File findCurrentMasterFile() {
        return new File(SettingPropsUtil.getUnivDir(), UNIV_FILE_NAME);
    }

    /**
     * 現在の大学マスタファイルをリネームします。
     *
     * @param currentFile 現在の大学マスタファイル
     * @return リネーム後の大学マスタファイル
     */
    private File renameCurrentMasterFile(File currentFile) {
        String ts = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS");
        String newName = StringUtils.replace(currentFile.getName(), ".", "_" + ts + ".");
        return IOUtil.renameFile(currentFile, newName);
    }

    /**
     * ログを出力します。
     *
     * @param contents 出力内容
     */
    private void outputLog(String... contents) {
        logService.output(GamenId.E3, contents);
    }

    /**
     * 大学マスタファイル切り替え後の後処理を行います。
     * @param isReboot リブート有無
     */
    private void afterSwitchMasterProcess(boolean isReboot) {

        /* 切り替え前の年度 */
        String oldYear = systemDto.getUnivYear();

        /* メモリ上の大学マスタ情報を初期化する */
        if (isReboot) {
            try {
                // 大学マスタ情報を取得
                systemDto.setUnivMaster(getUnivMasterBean(findCurrentMasterFile()));
            } catch (SQLException e) {
                // 何もしない
            }
        } else {
            initializeService.initMaster();
        }

        /* 切り替え後の年度 */
        String newYear = systemDto.getUnivYear();

        /* 選択中生徒の個人IDリストをクリアする */
        systemDto.clearSelectedIndividualIdList();

        if (!oldYear.equals(newYear)) {
            /* 年度繰り越しが発生した場合 */
            /* 前年度以前の大学マスタを削除する */
            deleteUnivBackup();
            /* 成績データを削除する */
            mainteStngService.deletePastRecord(newYear);
            /* 年度繰り越しログを出力する */
            outputLog(oldYear, newYear);
        }

        /* 大学コード紐付け処理を実行する */
        if (univCdHookUpService.needsHookUp()) {
            univCdHookUpService.hookUp();
        }
    }

    /**
     * 大学マスタのバックアップファイルを削除します。
     */
    private void deleteUnivBackup() {
        for (Iterator<File> ite = FileUtils.iterateFiles(SettingPropsUtil.getUnivDir(), new String[] { "db" }, false); ite.hasNext();) {
            File file = ite.next();
            if (file.isFile() && file.getName().startsWith("univ_")) {
                file.delete();
            }
        }
    }

    /** ダウンロード可能大学マスタセットを保持するクラス */
    public final class UnivDbSet {

        /** ダウンロード可能大学マスタセット */
        private final Set<String> set;

        /**
         * コンストラクタです。
         *
         * @param set ダウンロード可能大学マスタセット
         */
        private UnivDbSet(Set<String> set) {
            /* getメソッドで最新バージョンが返るよう、降順で保持する */
            List<String> list = CollectionsUtil.newArrayList(set);
            Collections.sort(list);
            Collections.reverse(list);
            this.set = CollectionsUtil.newLinkedHashSet(list);
        }

        /**
         * ダウンロード可能大学マスタが存在するかどうかを返します。
         *
         * @return ダウンロード可能大学マスタが存在するならtrue
         */
        public boolean isEmpty() {
            return set.isEmpty();
        }

        /**
         * 最新の大学マスタを返します。
         *
         * @return {@link UnivDb}
         */
        public UnivDb getLatest() {
            return new UnivDb(Collections.max(set));
        }

        /**
         * 指定された行事年度・模試区分の大学マスタを返します。
         *
         * @param eventYear 行事年度
         * @param examDiv 模試区分
         * @return {@link UnivDb}
         */
        public UnivDb get(String eventYear, String examDiv) {
            String target = eventYear + examDiv;
            for (String db : set) {
                if (db.startsWith(target)) {
                    return new UnivDb(db);
                }
            }
            return null;
        }

        /** 大学マスタファイル情報を保持するクラス */
        public final class UnivDb {

            /** 行事年度 */
            private final String eventYear;

            /** 模試区分 */
            private final String examDiv;

            /** 版数 */
            private final String version;

            /**
             * コンストラクタです。
             *
             * @param db 行事年度4桁＋模試区分2桁＋版数2桁
             */
            private UnivDb(String db) {
                this.eventYear = db.substring(0, 4);
                this.examDiv = db.substring(4, 6);
                this.version = db.substring(6, 8);
            }

            /**
             * 行事年度を返します。
             *
             * @return 行事年度
             */
            public String getEventYear() {
                return eventYear;
            }

            /**
             * 模試区分を返します。
             *
             * @return 模試区分
             */
            public String getExamDiv() {
                return examDiv;
            }

            /**
             * 版数を返します。
             *
             * @return 版数
             */
            public String getVersion() {
                return version;
            }

        }

    }

}

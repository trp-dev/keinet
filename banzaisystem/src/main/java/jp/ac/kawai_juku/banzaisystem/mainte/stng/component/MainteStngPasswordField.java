package jp.ac.kawai_juku.banzaisystem.mainte.stng.component;

import java.lang.reflect.Field;

import javax.swing.JComponent;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.MaxLengthDocumentFilter;
import jp.ac.kawai_juku.banzaisystem.framework.component.PasswordField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * メンテナンス-環境設定画面のパスワードフィールドです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MainteStngPasswordField extends PasswordField {

    /** グループ番号 */
    private int group;

    @Override
    public void initialize(Field field, final AbstractView view) {

        super.initialize(field, view);

        group = field.getAnnotation(Group.class).value()[0];

        ((AbstractDocument) getDocument()).setDocumentFilter(new MaxLengthDocumentFilter(field.getAnnotation(
                MaxLength.class).value()) {

            @Override
            public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr)
                    throws BadLocationException {
                super.insertString(fb, offset, str, attr);
                changeButtonStatus(view);
            }

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                    throws BadLocationException {
                super.replace(fb, offset, length, text, attrs);
                changeButtonStatus(view);
            }

            @Override
            public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
                super.remove(fb, offset, length);
                changeButtonStatus(view);
            }
        });
    }

    /**
     * 変更ボタンの状態を変更します。
     *
     * @param view {@link AbstractView}
     */
    private void changeButtonStatus(AbstractView view) {

        boolean isAllInputed = true;
        ImageButton button = null;
        for (JComponent jc : view.findComponentsByGroup(group)) {
            if (jc instanceof ImageButton) {
                button = (ImageButton) jc;
            } else {
                if (((PasswordField) jc).getStringPassword().isEmpty()) {
                    isAllInputed = false;
                }
            }
        }

        button.setEnabled(isAllInputed);
    }

}

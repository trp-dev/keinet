package jp.ac.kawai_juku.banzaisystem.framework.window;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.Timer;
import javax.swing.plaf.LayerUI;

/**
 *
 * 実行中アニメーションを表示するLayerUIです。<br>
 * ＜OTN「JLayerを使用したコンポーネントのデコレート方法」を参考＞
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class WaitLayerUI extends LayerUI<JComponent> implements ActionListener {

    /** プロパティ名：isRunning */
    private static final String PROPERTY_IS_RUNNING = "isRunning";

    /** プロパティ名：repaint */
    private static final String PROPERTY_REPAINT = "repaint";

    /** 実行中フラグ */
    private boolean isRunning;

    /** 書き換え実行用タイマ */
    private Timer mTimer;

    /** バー表示角度 */
    private double angle;

    @Override
    public void actionPerformed(ActionEvent e) {
        if (isRunning) {
            firePropertyChange(PROPERTY_REPAINT, null, null);
            angle += 2;
            if (angle >= 360) {
                angle = 0;
            }
        }
    }

    @Override
    public void paint(Graphics g, JComponent c) {

        super.paint(g, c);

        if (!isRunning) {
            return;
        }

        int w = c.getWidth();
        int h = c.getHeight();
        Graphics2D g2 = (Graphics2D) g.create();

        /* Gray it out. */
        Composite urComposite = g2.getComposite();
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f));
        g2.fillRect(0, 0, w, h);
        g2.setComposite(urComposite);

        /* Paint the wait indicator. */
        int s = Math.min(w, h) / 10;
        int cx = w / 2;
        int cy = h / 2;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(s / 4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2.setPaint(Color.white);
        g2.rotate(Math.PI * angle / 180d, cx, cy);
        for (int i = 0; i < 12; i++) {
            float scale = (11.0f - (float) i) / 11.0f;
            g2.drawLine(cx + s, cy, cx + s * 2, cy);
            g2.rotate(-Math.PI / 6, cx, cy);
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, scale));
        }

        g2.dispose();
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        JLayer<?> jlayer = (JLayer<?>) c;
        jlayer.getGlassPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jlayer.setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK
                | AWTEvent.MOUSE_WHEEL_EVENT_MASK | AWTEvent.KEY_EVENT_MASK);
    }

    @Override
    public void uninstallUI(JComponent c) {
        JLayer<?> jlayer = (JLayer<?>) c;
        jlayer.setLayerEventMask(0);
        super.uninstallUI(c);
    }

    @Override
    public void eventDispatched(AWTEvent e, JLayer<? extends JComponent> l) {
        if (isRunning && e instanceof InputEvent) {
            ((InputEvent) e).consume();
        }
    }

    @Override
    public void applyPropertyChange(PropertyChangeEvent pce, JLayer<? extends JComponent> l) {
        String cmd = pce.getPropertyName();
        if (PROPERTY_IS_RUNNING.equals(cmd)) {
            l.getGlassPane().setVisible((Boolean) pce.getNewValue());
        } else if (PROPERTY_REPAINT.equals(cmd)) {
            l.repaint();
        }
    }

    /**
     * 実行中アニメーションを開始します。
     */
    void start() {
        if (isRunning) {
            return;
        }
        isRunning = true;
        mTimer = new Timer(1000 / 30, this);
        mTimer.start();
        firePropertyChange(PROPERTY_IS_RUNNING, !isRunning, isRunning);
    }

    /**
     * 実行中アニメーションを停止します。
     */
    void stop() {
        isRunning = false;
        mTimer.stop();
        firePropertyChange(PROPERTY_IS_RUNNING, !isRunning, isRunning);
    }

    /**
     * 実行中フラグを返します。
     *
     * @return 実行中フラグ
     */
    boolean isRunning() {
        return isRunning;
    }

}

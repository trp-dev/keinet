package jp.ac.kawai_juku.banzaisystem.selstd.dunv.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.view.SelstdDunvView;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学選択ダイアログのテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdDunvTable extends TableScrollPane {

    /** 選択可能大学テーブルフラグ */
    private boolean isSelectableTable;

    /** 大学名称検索条件 */
    private String univNameCondition;

    /** 大学リスト */
    private final List<SelstdDunvBean> univList = CollectionsUtil.newArrayList();

    /**
     * コンストラクタです。
     */
    public SelstdDunvTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        final JTable table = new BZTable(new ReadOnlyTableModel(2));
        table.setFocusable(false);
        table.setRowHeight(18);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                SelstdDunvView view = SingletonS2Container.getComponent(SelstdDunvView.class);
                boolean b = table.getSelectedRowCount() > 0;
                if (isSelectableTable) {
                    view.addAllowButton.setEnabled(b);
                } else {
                    view.removeAllowButton.setEnabled(b);
                }
            }
        });

        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        columnModel.getColumn(0).setPreferredWidth(95);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 大学リストが空であるかを返します。
     *
     * @return 大学リストが空ならtrue
     */
    public boolean isEmpty() {
        return univList.isEmpty();
    }

    /**
     * 大学リストを返します。
     *
     * @return 大学リスト
     */
    public List<SelstdDunvBean> getUnivList() {
        return univList;
    }

    /**
     * 大学リストをセットします。
     *
     * @param list 大学リスト
     */
    public void setUnivList(List<SelstdDunvBean> list) {
        univList.clear();
        univList.addAll(list);
        univNameCondition = null;
        setDataVector();
    }

    /**
     * データをセットします。
     */
    private void setDataVector() {
        Vector<Vector<Object>> dataVector = CollectionsUtil.newVector(univList.size());
        for (SelstdDunvBean bean : univList) {
            if (!StringUtils.isEmpty(univNameCondition) && !StringUtils.isEmpty(bean.getUnivNameKana()) && !bean.getUnivNameKana().startsWith(univNameCondition)) {
                continue;
            }
            Vector<Object> row = CollectionsUtil.newVector(2);
            row.add(bean);
            row.add(bean.getUnivName());
            dataVector.add(row);
        }
        ((BZTableModel) getTable().getModel()).setDataVector(dataVector);
    }

    /**
     * 選択中の大学リストを返します。
     *
     * @return 選択中の大学リスト
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<SelstdDunvBean> getSelectedUnivList() {
        List<SelstdDunvBean> list = CollectionsUtil.newArrayList();
        Vector<Vector> dataVector = ((BZTableModel) getTable().getModel()).getDataVector();
        for (int index : getTable().getSelectedRows()) {
            Vector<Object> row = dataVector.get(index);
            list.add((SelstdDunvBean) row.get(0));
        }
        return list;
    }

    /**
     * 大学リストを削除します。
     *
     * @param list 大学リスト
     */
    public void removeUnivList(List<SelstdDunvBean> list) {
        for (SelstdDunvBean bean : list) {
            univList.remove(bean);
        }
        setDataVector();
    }

    /**
     * 大学リストを追加します。
     *
     * @param list 大学リスト
     */
    public void addUnivList(List<SelstdDunvBean> list) {
        univList.addAll(list);
        Collections.sort(univList);
        setDataVector();
    }

    /**
     * 選択可能大学テーブルフラグをセットします。
     *
     * @param isSelectableTable 選択可能大学テーブルフラグ
     */
    public void setSelectableTable(boolean isSelectableTable) {
        this.isSelectableTable = isSelectableTable;
    }

    /**
     * 大学名称検索条件をセットします。
     *
     * @param univNameCondition 大学名称検索条件
     */
    public void setUnivNameCondition(String univNameCondition) {
        this.univNameCondition = univNameCondition;
        setDataVector();
    }

}

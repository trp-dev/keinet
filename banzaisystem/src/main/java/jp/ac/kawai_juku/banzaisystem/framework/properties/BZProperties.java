package jp.ac.kawai_juku.banzaisystem.framework.properties;

import java.util.Properties;

import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * プロパティファイルの操作クラスです。
 *
 *
 * @param <T> キーの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
abstract class BZProperties<T extends PropsKey> {

    /** プロパティオブジェクト */
    private final Properties props;

    /**
     * コンストラクタです。
     *
     * @param props {@link Properties}
     */
    BZProperties(Properties props) {
        this.props = props;
    }

    /**
     * プロパティ値を文字列で返します。
     *
     * @param key プロパティキー
     * @return プロパティ値
     */
    public String getValue(T key) {
        return ObjectUtils.defaultIfNull(props.getProperty(key.toString()), key.getDefaultValue());
    }

    /**
     * プロパティ値を数値で返します。
     *
     * @param key プロパティキー
     * @return プロパティ値
     */
    public int getIntValue(T key) {
        return Integer.parseInt(getValue(key));
    }

    /**
     * プロパティ値をbooleanで返します。
     *
     * @param key プロパティキー
     * @return プロパティ値
     */
    public boolean getFlagValue(T key) {
        return FlagUtil.toBoolean(getValue(key));
    }

    /**
     * プロパティ値をセットします。
     *
     * @param key プロパティキー
     * @param value プロパティ値
     */
    public void setValue(T key, String value) {
        props.setProperty(key.toString(), value);
    }

    /**
     * プロパティからキーを削除します。
     *
     * @param key プロパティキー
     */
    public void remove(T key) {
        props.remove(key.toString());
    }

    /**
     * Propertiesを返します。
     *
     * @return props {@link Properties}
     */
    Properties getProperties() {
        return props;
    }

}

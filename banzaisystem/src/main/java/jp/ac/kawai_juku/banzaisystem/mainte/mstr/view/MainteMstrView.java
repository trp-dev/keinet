package jp.ac.kawai_juku.banzaisystem.mainte.mstr.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.component.MainteMstrTable;

/**
 *
 * メンテナンス-大学マスタ・プログラム最新化画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMstrView extends SubView {

    /** 画面情報更新ボタン */
    @Location(x = 210, y = 31)
    public ImageButton renewalScreenInfoButton;

    /** 更新文言ラベル */
    @Location(x = 430, y = 38)
    @Foreground(r = 255, g = 0, b = 0)
    public TextLabel renewalLabel;

    /** 大学マスタテーブル */
    @Location(x = 30, y = 129)
    @Size(width = 473, height = 401)
    public MainteMstrTable univMasterTable;

    /** 大学マスタを更新ボタン */
    @Location(x = 164, y = 536)
    public ImageButton updateMasterButton;

    /** 大学マスタダウンロードボタン */
    @Location(x = 43, y = 588)
    public ImageButton downloadUnivMstButton;

    /** 大学マスタ手動インポートボタン */
    @Location(x = 283, y = 583)
    public ImageButton importButton;

    /** 現バージョン */
    @Location(x = 510, y = 129)
    @Size(width = 111, height = 75)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.BOLD)
    public TextLabel currentVersionLabel;

    /** 最新バージョン */
    @Location(x = 622, y = 129)
    @Size(width = 112, height = 75)
    @HorizontalAlignment(SwingConstants.CENTER)
    @Font(BZFont.BOLD)
    public TextLabel latestVersionLabel;

    /** 状況 */
    @Location(x = 747, y = 129)
    @Size(width = 236, height = 75)
    public TextLabel statusLabel;

    /** プログラムを更新ボタン */
    @Location(x = 627, y = 212)
    public ImageButton downloadUnivMstUpdatePrgButton;

    @Override
    public void initialize() {
        super.initialize();
        currentVersionLabel.setText(BZConstants.VERSION);
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.dist.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Color;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistNumbersBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 帯別人数グラフパネルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnDistGraphPanel extends ImagePanel implements BZComponent {

    /** １目盛りあたりの人数の表示フォーマット */
    private static final String FMT_SCALE = "□＝%s人";

    /** 満点ラベルの表示フォーマット */
    private static final String FMT_FULLPNT = "満点：%s";

    /** 目盛りアイコン(ブルー) */
    private final Icon blueIcon;

    /** 目盛りアイコン(オレンジ) */
    private final Icon orangeIcon;

    /** 換算得点／偏差値ラベル */
    private final JLabel scoreLabel = new JLabel();

    /** 得点帯別／偏差値帯別人数ラベル */
    private final JLabel graphLabel = new JLabel();

    /** １目盛りあたりの人数ラベル */
    private final JLabel scaleLabel = new JLabel();

    /** 満点ラベル */
    private final JLabel fullPntLabel = new JLabel();

    /** ボーダーラベル */
    private final JLabel borderLabel = new JLabel();

    /**
     * コンストラクタです。
     */
    public ExmntnDistGraphPanel() {
        super(ExmntnDistGraphPanel.class, "graph.png");
        Color fg = new Color(0, 52, 153);
        scoreLabel.setBounds(1, 1, 58, 23);
        scoreLabel.setForeground(fg);
        scoreLabel.setHorizontalAlignment(JLabel.CENTER);
        graphLabel.setBounds(60, 1, 259, 23);
        graphLabel.setForeground(fg);
        graphLabel.setHorizontalAlignment(JLabel.CENTER);
        scaleLabel.setBounds(60, 25, 185, 20);
        scaleLabel.setHorizontalAlignment(JLabel.CENTER);
        fullPntLabel.setLocation(5, 33);
        borderLabel.setLocation(326, 31);
        blueIcon = new ImageIcon(ImageUtil.readImage(getClass(), "graphBlue.png"));
        orangeIcon = new ImageIcon(ImageUtil.readImage(getClass(), "graphOrange.png"));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
    }

    /**
     * 帯別人数グラフを初期化します。
     *
     * @param exam 対象模試
     * @param bean {@link ExmntnDistBean}
     */
    public void init(ExamBean exam, ExmntnDistBean bean) {

        /* 全てのラベルを削除する */
        removeAll();

        /* ヘッダラベルを追加する */
        addHeaderLabel(exam, bean);

        if (bean != null && bean.getPatternCd() != null) {
            /* １目盛りあたりの人数ラベルを追加する */
            addScaleLabel(bean);
            /* 満点ラベルを追加する */
            addFullPntLabel(bean);
            /* ボーダーラベルを追加する */
            addBorderLabel(bean);
            /* 帯別のラベルを追加する */
            addDistLabel(bean.getNumbersList());
        }

        /* 再描画する */
        repaint();
    }

    /**
     * ヘッダラベルを追加します。
     *
     * @param exam 対象模試
     * @param bean {@link ExmntnDistBean}
     */
    private void addHeaderLabel(ExamBean exam, ExmntnDistBean bean) {
        if ((bean != null && bean.isDeviationDistFlg()) || (exam != null && ExamUtil.isWritten(exam))) {
            scoreLabel.setText(getMessage("label.exmntn.dist.deviation"));
            graphLabel.setText(getMessage("label.exmntn.dist.deviationGraph"));
        } else {
            if (exam != null && ExamUtil.isCenter(exam)) {
                scoreLabel.setText(getMessage("label.exmntn.dist.point"));
                graphLabel.setText(getMessage("label.exmntn.dist.scoreGraphC"));
            } else {
                scoreLabel.setText(getMessage("label.exmntn.dist.cvsPoint"));
                graphLabel.setText(getMessage("label.exmntn.dist.scoreGraph"));
            }
        }
        add(scoreLabel);
        add(graphLabel);
    }

    /**
     * １目盛りあたりの人数ラベルを追加します。
     *
     * @param bean {@link ExmntnDistBean}
     */
    private void addScaleLabel(ExmntnDistBean bean) {
        scaleLabel.setText(String.format(FMT_SCALE, bean.getScale()));
        add(scaleLabel);
    }

    /**
     * 満点ラベルを追加します。
     *
     * @param bean {@link ExmntnDistBean}
     */
    private void addFullPntLabel(ExmntnDistBean bean) {
        if (bean.getFullPnt() != null) {
            fullPntLabel.setText(String.format(FMT_FULLPNT, bean.getFullPnt()));
            fullPntLabel.setSize(fullPntLabel.getPreferredSize());
            add(fullPntLabel);
        }
    }

    /**
     * ボーダーラベルを追加します。
     *
     * @param bean {@link ExmntnDistBean}
     */
    private void addBorderLabel(ExmntnDistBean bean) {
        borderLabel.setText(bean.getBorder());
        borderLabel.setSize(borderLabel.getPreferredSize());
        add(borderLabel);
    }

    /**
     * 帯別のラベルを追加します。
     *
     * @param numbersList 帯別人数リスト
     */
    private void addDistLabel(List<ExmntnDistNumbersBean> numbersList) {
        int y = 54;
        for (ExmntnDistNumbersBean bean : numbersList) {
            /* 帯ヘッダラベルを追加する */
            JLabel headerLabel = new JLabel(bean.getDistHeader());
            headerLabel.setBounds(1, y, 53, 11);
            headerLabel.setHorizontalAlignment(JLabel.RIGHT);
            add(headerLabel);
            /* 目盛りアイコンラベルを追加する */
            int x = 60;
            for (int i = 0; i < bean.getScaleNum(); i++) {
                JLabel iconLabel = new JLabel(bean.isEmphasisFlg() ? orangeIcon : blueIcon);
                iconLabel.setLocation(x, y);
                iconLabel.setSize(iconLabel.getPreferredSize());
                add(iconLabel);
                x += 8;
            }
            /* 人数ラベルを追加する */
            JLabel numLabel = new JLabel(Integer.toString(bean.getNumbers()));
            numLabel.setBounds(246, y, 28, 11);
            numLabel.setHorizontalAlignment(JLabel.RIGHT);
            add(numLabel);
            /* 累計ラベルを追加する */
            JLabel totalLabel = new JLabel(Integer.toString(bean.getTotal()));
            totalLabel.setBounds(283, y, 28, 11);
            totalLabel.setHorizontalAlignment(JLabel.RIGHT);
            add(totalLabel);
            /* 評価基準ラベルを追加する */
            if (!StringUtils.isEmpty(bean.getStandard())) {
                JLabel stdLabel = new JLabel(bean.getStandard());
                stdLabel.setBounds(329, y, 36, 11);
                add(stdLabel);
            }
            y += 17;
        }
    }

}

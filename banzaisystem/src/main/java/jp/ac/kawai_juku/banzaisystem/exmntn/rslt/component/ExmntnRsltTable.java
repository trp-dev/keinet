package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.SortableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 検索結果画面のテーブルです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnRsltTable extends SortableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** 評価列 */
    private static final List<Integer> RATE_COLUMNS = Arrays.asList(4, 5, 6);

    /** データリスト */
    private List<Map<String, CellData>> dataList;

    /**
     * コンストラクタです。
     */
    public ExmntnRsltTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS, "univKey");
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setColumnHeaderView(createColumnHeaderView());
        JPanel fixedHeader = createFixedColumnHeaderView();
        setCorner(JScrollPane.UPPER_LEFT_CORNER, fixedHeader);
        setViewportView(createTableView(getHeader(), true));
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        setRowHeaderView(createTableView(fixedHeader, false));
        final JViewport rightView = getViewport();
        getRowHeader().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JViewport leftView = (JViewport) e.getSource();
                rightView.setViewPosition(new Point(rightView.getViewPosition().x, leftView.getViewPosition().y));
            }
        });
        /* 2つのテーブルのリスト選択モデルを同一にする */
        getTable().setSelectionModel(getFixedTable().getSelectionModel());
    }

    /**
     * 固定領域のヘッダを生成します。
     * @return panel
     */
    private JPanel createFixedColumnHeaderView() {

        ImagePanel panel = new ImagePanel(ExmntnRsltTable.class, "bgFixed.png");

        List<JComponent> list = CollectionsUtil.newArrayList();
        list.add(createHeaderButton("division", CellAlignment.CENTER));
        list.add(createHeaderButton("seatOfUniv", CellAlignment.LEFT));
        list.add(createHeaderButton("university", CellAlignment.LEFT));
        list.add(createHeaderLabel("subjectFacultyDays", CellAlignment.LEFT));
        int x = 0;
        for (JComponent jc : list) {
            if (panel.getHeight() == jc.getHeight()) {
                jc.setLocation(x, 0);
            } else {
                jc.setLocation(x, panel.getHeight() - jc.getHeight());
            }
            panel.add(jc);
            x += jc.getWidth();
        }

        return panel;
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView() {
        ImagePanel panel = new ImagePanel(ExmntnRsltTable.class);
        int x = 0;
        for (JComponent jc : createColumnHeaderList()) {
            if (panel.getHeight() == jc.getHeight()) {
                jc.setLocation(x, 0);
            } else {
                jc.setLocation(x, panel.getHeight() - jc.getHeight());
            }
            panel.add(jc);
            x += jc.getWidth();
        }
        return panel;
    }

    /**
     * ヘッダコンポーネントリストを生成します。
     *
     * @return ヘッダコンポーネントリスト
     */
    private List<JComponent> createColumnHeaderList() {
        List<JComponent> list = CollectionsUtil.newArrayList();
        ExmntnMainView view = SingletonS2Container.getComponent(ExmntnMainView.class);
        ExamBean exam = view.exmntnMarkComboBox.getSelectedValue();
        boolean isCenter = false;
        list.add(createHeaderButton("borderOfExclude", CellAlignment.CENTER));
        if (exam != null) {
            isCenter = ExamUtil.isCenter(view.exmntnMarkComboBox.getSelectedValue());
        }

        if (exam != null && isCenter) {
            list.add(createHeaderButton("pointOfExclude", CellAlignment.CENTER));
        } else {
            list.add(createHeaderButton("pointOfExclude2", CellAlignment.CENTER));
        }

        /*
        list.add(createHeaderButton("borderOfInclude", CellAlignment.CENTER));
        list.add(createHeaderButton("pointOfInclude", CellAlignment.CENTER));
        */
        list.add(createHeaderButton("rankOfSecond", CellAlignment.CENTER));
        if (exam != null && isCenter) {
            list.add(createHeaderButton("devSecond", CellAlignment.CENTER));
        } else {
            list.add(createHeaderButton("devSecond2", CellAlignment.CENTER));
        }
        list.add(createHeaderButton("valuateExclude", CellAlignment.CENTER));
        /* list.add(createHeaderButton("valuateInclude", CellAlignment.CENTER)); */
        list.add(createHeaderButton("valuateSecond", CellAlignment.CENTER));
        list.add(createHeaderButton("valuateSynthetic", CellAlignment.CENTER));
        list.add(createHeaderButton("compStartPointCenter", CellAlignment.CENTER));
        list.add(createHeaderButton("compStartPointSecond", CellAlignment.CENTER));
        return list;
    }

    /**
     * テーブルを生成します。
     *
     * @param header ヘッダ
     * @param monospacedFlg ゴシック使用フラグ
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final JPanel header, boolean monospacedFlg) {

        ReadOnlyTableModel tableModel = new ReadOnlyTableModel(header.getComponentCount() + 1);

        final JTable table = new BZTable(tableModel) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                if (monospacedFlg && RATE_COLUMNS.contains(column)) {
                    Font font = c.getFont();
                    c.setFont(new Font(BZConstants.MONOSPACED_FONT, font.getStyle(), font.getSize()));
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (Component c : header.getComponents()) {
                    x += c.getWidth();
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ExmntnRsltView view = SingletonS2Container.getComponent(ExmntnRsltView.class);
                SystemDto systemDto = SingletonS2Container.getComponent(SystemDto.class);
                final ExmntnMainScoreHelper scoreHelper = SingletonS2Container.getComponent(ExmntnMainScoreHelper.class);
                view.addCandidateUnivButton.setEnabled(getTable().getSelectedRowCount() > 0 && (!systemDto.isTeacher() || systemDto.getTargetIndividualId() != null) && scoreHelper.getTargetExam() != null);
            }
        });

        /* ダブルクリック時の動作設定 */
        table.addMouseListener(new MouseAdapter() {

            /** 志望大学詳細画面のヘルパー */
            private final ExmntnUnivHelper helper = SingletonS2Container.getComponent(ExmntnUnivHelper.class);

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.rowAtPoint(e.getPoint());
                    if (row > -1) {
                        Map<String, CellData> map = dataList.get(row);
                        ExmntnMainCandidateUnivBean bean = new ExmntnMainCandidateUnivBean((UnivKeyBean) map.get("univKey").getDispValue());
                        bean.setUnivName(map.get("university").toString());
                        bean.setFacultyName(map.get("facultyName").toString());
                        bean.setDeptName(map.get("deptName").toString());
                        helper.dispSearchDetail(bean);
                    }
                }
            }
        });

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        Component[] components = header.getComponents();
        for (int i = 0; i < components.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(components[i].getWidth());
            columnModel.getColumn(i).setCellRenderer(((Header) components[i]).getAlignment().getRenderer());
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 選択中の大学キーリストを返します。
     *
     * @return 選択中の大学キーリスト
     */
    public List<UnivKeyBean> getSelectedUnivKeyList() {

        List<UnivKeyBean> list = CollectionsUtil.newArrayList();

        JTable table = getTable();
        TableModel model = table.getModel();
        int[] rowIndexArray;
        if (table.getSelectedRowCount() > 0) {
            /* 選択されている場合 */
            rowIndexArray = table.getSelectedRows();
        } else {
            /* 未選択の場合 */
            return list;
        }

        for (int rowIndex : rowIndexArray) {
            Object obj = ((CellData) model.getValueAt(rowIndex, 0)).getSortValue();
            list.add((UnivKeyBean) obj);
        }

        return list;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public void setDataList(List<Map<String, CellData>> dataList) {
        /* ソート用にデータリストを保持しておく */
        this.dataList = dataList;
        /* ソートする */
        sort(getSortKey(), isAscending());
    }

    @Override
    protected void sort(final String sortKey, final boolean isAscending) {
        Collections.sort(dataList, new Comparator<Map<String, CellData>>() {

            @Override
            public int compare(Map<String, CellData> o1, Map<String, CellData> o2) {

                String sKey = sortKey;
                /* 共通テスト（含まない）ボーダ/満点(率)の場合、得点率でソートする */
                /* 共通テスト ボーダ/満点(率)の場合、得点率でソートする */
                if (sortKey.equals("borderOfExclude")) {
                    sKey = "scoreRateExclude";
                }
                /* 共通テスト（含む）ボーダ/満点(率)の場合、得点率でソートする */
                /*
                if (sortKey.equals("borderOfInclude")) {
                    sKey = "scoreRateInclude";
                }
                */

                /* 共通テスト（含まない）本人得点の場合、得点率でソートする */
                /* 共通テスト 本人得点の場合、得点率でソートする */
                if (sortKey.equals("pointOfExclude") || sortKey.equals("pointOfExclude2")) {
                    sKey = "pointScoreRateExclude";
                }
                /* 共通テスト（含む）本人得点の場合、得点率でソートする */
                /*
                 if (sortKey.equals("pointOfInclude")) {
                    sKey = "pointScoreRateInclude";
                }
                */

                /* 第1ソートキーでソートする */
                int result = ObjectUtils.compare(o1.get(sKey), o2.get(sKey));
                if (result != 0) {
                    return isAscending ? result : -result;
                }

                /* 大学キーでソートする */
                return ObjectUtils.compare(o1.get("univKey"), o2.get("univKey"));
            }

        });
        /* ソートした結果をテーブルにセットする */
        setDataVector(getTable(), getHeader());
        JPanel fixedHeader = getFixedHeader();
        if (fixedHeader != null) {
            setDataVector(getFixedTable(), fixedHeader);
        }
    }

    /**
     * テーブルにデータをセットします。
     *
     * @param table テーブル
     * @param header ヘッダ
     */
    protected void setDataVector(JTable table, JPanel header) {
        ((BZTableModel) table.getModel()).setDataVector(toDataVector(header, dataList));
    }

    /**
     * データリストをテーブルデータに変換します。
     *
     * @param header ヘッダ
     * @param dataList データリスト
     * @return テーブルデータ
     */
    protected Vector<Vector<CellData>> toDataVector(JPanel header, List<Map<String, CellData>> dataList) {

        Vector<Vector<CellData>> data = CollectionsUtil.newVector(dataList.size());

        for (Map<String, CellData> map : dataList) {
            Vector<CellData> rowData = CollectionsUtil.newVector();
            /* 先頭列は大学キーをセットしておく */
            rowData.add(map.get("univKey"));
            /* 2列目以降をセットする */
            for (Component c : header.getComponents()) {
                rowData.add(ObjectUtils.defaultIfNull(map.get(c.getName()), new CellData(null)));
            }
            data.add(rowData);
        }

        return data;
    }

    /**
     * データリストを返します。
     *
     * @return データリスト
     */
    @SuppressWarnings({ "rawtypes" })
    public Vector<Vector> getDataList() {
        return ((ReadOnlyTableModel) getTable().getModel()).getDataVector();
    }

    /**
     * データリストを返します。
     *
     * @return データリスト
     */
    @SuppressWarnings({ "rawtypes" })
    public Vector<Vector> getFixDataList() {
        return ((ReadOnlyTableModel) getFixedTable().getModel()).getDataVector();
    }

    /**
     * 行の選択を解除します。
     */
    public void clearSelection() {
        getTable().clearSelection();
    }

}

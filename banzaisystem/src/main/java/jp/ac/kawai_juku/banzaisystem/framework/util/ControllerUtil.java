package jp.ac.kawai_juku.banzaisystem.framework.util;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.lang.reflect.Method;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.util.ClassUtil;
import org.seasar.framework.util.MethodUtil;

/**
 *
 * コントローラに関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ControllerUtil {

    /**
     * コンストラクタです。
     */
    private ControllerUtil() {
    }

    /**
     * コントローラのアクションメソッドを起動します。
     *
     * @param controllerClass 起動するコントローラのクラス
     * @param methodName 起動するメソッド名
     * @param params 起動するメソッドのパラメータ
     */
    public static void invoke(Class<? extends AbstractController> controllerClass, String methodName, Object... params) {

        AbstractController controller = getComponent(controllerClass);

        if (controller instanceof SubController) {
            throw new RuntimeException(String.format("サブコントローラ[%s]は直接起動できません。", controllerClass.getSimpleName()));
        }

        Class<?>[] argTypes = null;
        if (params != null) {
            argTypes = new Class<?>[params.length];
            for (int i = 0; i < params.length; i++) {
                argTypes[i] = params[i].getClass();
            }
        }

        Method method =
                ClassUtil.getDeclaredMethod(BZClassUtil.getOriginalClass(controller.getClass()), methodName == null
                        ? "index" : methodName, argTypes);
        if (!method.isAnnotationPresent(Execute.class)) {
            throw new RuntimeException("アクションメソッドにExecuteアノテーションが設定されていません。");
        }

        AbstractView view = controller.getView();
        if (!view.isInitialized()) {
            view.initialize();
        }

        MethodUtil.invoke(method, controller, params);
    }

}

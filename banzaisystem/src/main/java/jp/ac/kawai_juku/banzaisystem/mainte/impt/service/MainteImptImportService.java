package jp.ac.kawai_juku.banzaisystem.mainte.impt.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_1;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_NA;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.ANSWER_NO_OTHER;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_SOCIAL;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY_2ND;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY_2ND;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ENGLISH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS_2ND;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainCheckHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainSubjectHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CIndex;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvReader;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvRow;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.maker.B03CsvMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamSubjectUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.dao.MainteImptImportDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績インポート処理のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteImptImportService {

    /** 模試別のインポート対象科目コードセット */
    private static final Map<ExamBean, Set<String>> EXAM_SUBCD_MAP = CollectionsUtil.newHashMap();

    /** 科目コードと第１解答フラグの対応マップ(理科) */
    private static final Map<String, Integer> SC_FIRST_ANS_FLG_MAP;
    static {
        Map<String, Integer> map = CollectionsUtil.newHashMap();
        map.put(SUBCODE_CENTER_DEFAULT_PHYSICS, 0);
        map.put(SUBCODE_CENTER_DEFAULT_CHEMISTRY, 1);
        map.put(SUBCODE_CENTER_DEFAULT_BIOLOGY, 2);
        map.put(SUBCODE_CENTER_DEFAULT_EARTH, 3);
        SC_FIRST_ANS_FLG_MAP = Collections.unmodifiableMap(map);
    }

    /** 科目コードと第１解答フラグの対応マップ(地歴公民) */
    private static final Map<String, Integer> SO_FIRST_ANS_FLG_MAP;
    static {
        Map<String, Integer> map = CollectionsUtil.newHashMap();
        map.put(SUBCODE_CENTER_DEFAULT_WHISTORYB, 0);
        map.put(SUBCODE_CENTER_DEFAULT_JHISTORYB, 1);
        map.put(SUBCODE_CENTER_DEFAULT_GEOGRAPHYB, 2);
        map.put(SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS, 3);
        map.put(SUBCODE_CENTER_DEFAULT_POLITICS, 4);
        map.put(SUBCODE_CENTER_DEFAULT_SOCIAL, 5);
        map.put(SUBCODE_CENTER_DEFAULT_ETHICS, 6);
        map.put(SUBCODE_CENTER_DEFAULT_WHISTORYA, 7);
        map.put(SUBCODE_CENTER_DEFAULT_JHISTORYA, 8);
        map.put(SUBCODE_CENTER_DEFAULT_GEOGRAPHYA, 9);
        SO_FIRST_ANS_FLG_MAP = Collections.unmodifiableMap(map);
    }

    /** 科目成績レコード数 */
    private static final int SUB_COUNT = 26;

    /** 科目成績項目数 */
    private static final int SUB_ITEM_COUNT = 6;

    /** 志望校レコード数 */
    private static final int CANDIDATE_COUNT = 9;

    /** 志望校項目数 */
    private static final int CANDIDATE_ITEM_COUNT = 8;

    /** 列位置定義：年度 */
    private static final CIndex IDX_EXAM_YEAR = new CIndex(1);

    /** 列位置定義：模試コード */
    private static final CIndex IDX_EXAM_CD = IDX_EXAM_YEAR.right();

    /** 列位置定義：学年 */
    private static final CIndex IDX_GRADE = IDX_EXAM_CD.right().right();

    /** 列位置定義：クラス */
    private static final CIndex IDX_CLASS = IDX_GRADE.right();

    /** 列位置定義：クラス番号 */
    private static final CIndex IDX_CLASS_NO = IDX_CLASS.right();

    /** 列位置定義：カナ氏名 */
    private static final CIndex IDX_KANA_NAME = IDX_CLASS_NO.right();

    /** 列位置定義：受験型 */
    private static final CIndex IDX_EXAMTYPE = IDX_KANA_NAME.right();

    /** 列位置定義：文理コード */
    private static final CIndex IDX_BUNRICODE = IDX_EXAMTYPE.right();

    /** 列位置定義：科目コード１ */
    private static final CIndex IDX_SUBCD = IDX_BUNRICODE.right();

    /** 列位置定義：現文換算得点 */
    private static final CIndex IDX_CVS_GENBUN = IDX_SUBCD.add(SUB_COUNT * SUB_ITEM_COUNT + 1);

    /** 列位置定義：古文換算得点 */
    private static final CIndex IDX_CVS_KOBUN = IDX_CVS_GENBUN.right();

    /** 列位置定義：漢文換算得点 */
    private static final CIndex IDX_CVS_KANBUN = IDX_CVS_KOBUN.right();

    /** 列位置定義：英語リスニング選択 */
    private static final CIndex IDX_ENG_AREA = IDX_CVS_KANBUN.right();

    /** 列位置定義：物理選択 */
    private static final CIndex IDX_PHY_AREA = IDX_ENG_AREA.right();

    /** 列位置定義：化学選択 */
    private static final CIndex IDX_CHM_AREA = IDX_PHY_AREA.right();

    /** 列位置定義：生物選択 */
    private static final CIndex IDX_BIO_AREA = IDX_CHM_AREA.right();

    /** 列位置定義：地学選択 */
    private static final CIndex IDX_ESI_AREA = IDX_BIO_AREA.right();

    /** 列位置定義：志望校コード１(10桁) */
    private static final CIndex IDX_UNIVCD10 = IDX_ESI_AREA.right().right();

    /** 列位置定義：志望校コード１ */
    private static final CIndex IDX_CENTERCHECK = IDX_UNIVCD10.right().right().right();

    /** ログサービス */
    @Resource
    private LogService logService;

    /** 個人成績・志望大学画面のサービス */
    @Resource
    private ExmntnMainService exmntnMainService;

    /** DAO */
    @Resource(name = "mainteImptImportDao")
    private MainteImptImportDao dao;

    /** 個人成績画面の科目ヘルパー */
    @Resource(name = "exmntnMainSubjectHelper")
    private ExmntnMainSubjectHelper subjectHelper;

    /** 個人成績画面の科目ヘルパー */
    @Resource(name = "exmntnMainCheckHelper")
    private ExmntnMainCheckHelper checkHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 個人成績CSVをインポートします。
     *
     * @param file CSVファイル
     * @param maker {@link B03CsvMaker}
     */
    @Lock
    public void importCsv(File file, B03CsvMaker maker) {

        /* 取り込み開始ログを出力する */
        outputStartLog(file);

        /* インポート可能模試リストを初期化する */
        List<ExamBean> examList = dao.selectExamList(systemDto.getUnivExamDiv());

        /* インポート実施模試セットを初期化する */
        Set<ExamBean> importedExamSet = CollectionsUtil.newHashSet();

        try (CsvReader reader = new CsvReader(new FileInputStream(file))) {

            CsvRow row = null;
            /* 1行目はヘッダ行のためスキップする */
            row = reader.readLine();

            /* ヘッダのチェック(2020年度共通テストリサーチのみ影響あり。)*/
            /* (以降の年度では死にロジックとなる。)  */
            if (checkHeader(row)) {
                /* 取り込み終了ログを出力する */
                outputErrLog(maker);
                return;
            }

            while ((row = reader.readLine()) != null) {
                importRow(row, maker, examList, importedExamSet);
            }

            if (!importedExamSet.isEmpty()) {
                /* インポート状況を登録する */
                registImportStatus(importedExamSet);
                /* 選択中生徒をクリアする */
                systemDto.clearSelectedIndividualIdList();
                /* 取り込み終了ログを出力する */
                outputEndLog(importedExamSet);
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 個人成績CSVの1行をインポートします。
     *
     * @param row {@link CsvRow}
     * @param maker {@link B03CsvMaker}
     * @param examList インポート可能模試リスト
     * @param importedExamSet インポート実施模試セット
     */
    private void importRow(CsvRow row, B03CsvMaker maker, List<ExamBean> examList, Set<ExamBean> importedExamSet) {
        ExamBean exam = findExamBean(row, examList);
        if (exam != null && checkStudent(row)) {
            Integer id = registStudent(row, exam.getExamYear());
            delete(id, exam);
            registIndividualRecord(id, exam, row);
            registCandidateRating(id, exam, row);
            registCandidateUniv(id, exam);
            if (ExamUtil.isMark(exam)) {
                registMarkSubRecord(id, exam, row);
            } else {
                registWrtnSubRecord(id, exam, row);
            }
            importedExamSet.add(exam);
        } else if (maker != null) {
            /* 対象模試もしくは生徒情報が不正な場合の処理スキップメッセージを出力する */
            maker.addSkipMessage(row.getRowNum());
        }
    }

    /**
     * 生徒情報を登録します。
     *
     * @param row {@link CsvRow}
     * @param year 年度
     * @return 個人ID
     */
    private Integer registStudent(CsvRow row, String year) {

        /* クラスの0詰めを行っておく */
        String cls = row.get(IDX_CLASS);
        if (cls.isEmpty()) {
            cls = null;
        } else {
            cls = StringUtils.leftPad(cls.trim(), 2, '0');
        }

        Integer id = findIndividualId(row, year, cls);
        if (id != null) {
            /* 名寄せ成功 */
            return id;
        } else {
            /* 学籍基本情報と学籍履歴情報を登録する */
            id = registBasicInfo(row);
            registHistoryInfo(id, row, year, cls);
        }

        return id;
    }

    /**
     * 名寄せして個人IDを特定します。
     *
     * @param row {@link CsvRow}
     * @param year 年度
     * @param cls クラス
     * @return 個人ID
     */
    private Integer findIndividualId(CsvRow row, String year, String cls) {

        List<BeanMap> list = dao.selectStudentList(year, row.get(IDX_GRADE), cls, row.get(IDX_CLASS_NO));

        for (BeanMap bean : list) {
            String dbKana = ObjectUtils.toString(bean.get("kananame"));
            String csvKana = row.get(IDX_KANA_NAME);
            if (JpnStringUtil.normalize(dbKana).equals(JpnStringUtil.normalize(csvKana))) {
                return Integer.valueOf(bean.get("id").toString());
            }
        }

        return null;
    }

    /**
     * 学籍基本情報を登録します。
     *
     * @param row {@link CsvRow}
     * @return 個人ID
     */
    private Integer registBasicInfo(CsvRow row) {
        return dao.insertBasicInfo(row.get(IDX_KANA_NAME));
    }

    /**
     * 学籍履歴情報を登録します。
     *
     * @param row {@link CsvRow}
     * @param id 個人ID
     * @param year 年度
     * @param cls クラス
     */
    private void registHistoryInfo(Integer id, CsvRow row, String year, String cls) {
        dao.insertHistoryInfo(id, year, row.get(IDX_GRADE), cls, row.get(IDX_CLASS_NO));
    }

    /**
     * 個人の既存データを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    private void delete(Integer id, ExamBean exam) {
        dao.deleteSubRecord(id, exam);
        dao.deleteCandidateRating(id, exam);
        dao.deleteIndividualRecord(id, exam);
    }

    /**
     * 個表データを登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param row {@link CsvRow}
     */
    private void registIndividualRecord(Integer id, ExamBean exam, CsvRow row) {

        String grade = row.get(IDX_GRADE);

        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("grade", grade.isEmpty() ? null : Integer.valueOf(grade));
        param.put("class", row.get(IDX_CLASS));
        param.put("classNo", row.get(IDX_CLASS_NO));
        param.put("kanaName", row.get(IDX_KANA_NAME));
        param.put("examType", row.get(IDX_EXAMTYPE));
        param.put("bunriCode", row.get(IDX_BUNRICODE));

        dao.insertIndividualRecord(param);
    }

    /**
     * マーク模試の科目成績を登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param row {@link CsvRow}
     */
    private void registMarkSubRecord(Integer id, ExamBean exam, CsvRow row) {

        /* 模試科目マスタユーティリティ */
        ExamSubjectUtil util = new ExamSubjectUtil(systemDto, exam);

        /* 科目成績リスト */
        List<BZSubjectRecord> list = CollectionsUtil.newArrayList(30);

        /* 重複チェック用の科目コードセット */
        Set<String> importedSubCdSet = CollectionsUtil.newHashSet();

        /* インポート可能科目コードセット */
        Set<String> importableSubCdSet = getImportableSubCdSet(exam);

        /* マーク模試「現代文」得点 */
        Integer genbunScore = null;

        /* マーク模試「現古」得点 */
        Integer genkoScore = null;

        /* マーク模試「国語」得点 */
        Integer japScore = null;

        /* マーク模試「現代文」配点 */
        Integer genbunAllotPnt = null;

        /* マーク模試「現古」配点 */
        Integer genkoAllotPnt = null;

        /* マーク模試「国語」配点 */
        Integer japAllotPnt = null;

        for (int i = 0; i < SUB_COUNT; i++) {

            CIndex subCdIndex = IDX_SUBCD.add(i * SUB_ITEM_COUNT);
            String subCd = row.get(subCdIndex);
            String subjectAllotPnt = row.get(subCdIndex.add(2));
            String score = row.get(subCdIndex.add(3));
            String deviation = row.get(subCdIndex.add(4));

            if (!checkSubCd(subCd) || !importedSubCdSet.add(subCd)) {
                /* 不正科目コードまたはインポート済みなら処理しない */
                continue;
            }

            if (subCd.compareTo("7000") >= 0) {
                /* 型：得点と偏差値を含むレコードを生成する */
                if (checkSubScore(score) && checkDeviation(deviation)) {
                    list.add(new BZSubjectRecord(subCd, score, null, truncate(deviation), subjectAllotPnt));
                }
            } else if (checkSubScore(score) && !score.isEmpty()) {
                /* 科目：得点のみのレコードを生成する（換算得点と偏差値は後ほど算出する） */
                if (importableSubCdSet.contains(subCd)) {
                    if (Integer.parseInt(score) > checkHelper.getMarkFullScore(subCd)) {
                        /* 科目満点値を超過していたら登録しない */
                        continue;
                    }
                    /* インポート可能科目のみ追加する */
                    list.add(new BZSubjectRecord(subCd, score, null, null, calcMScope(row, exam, subCd, util), subjectAllotPnt));
                    if (subCd.equals(SUBCODE_CENTER_DEFAULT_LITERATURE)) {
                        /* マーク模試「現代文」得点を保持する */
                        genbunScore = Integer.valueOf(score);
                        if (!"".equals(subjectAllotPnt)) {
                            genbunAllotPnt = Integer.valueOf(subjectAllotPnt);
                        }
                    }
                } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_LITERATURE_OLD)) {
                    /* マーク模試「現古」得点を保持する */
                    genkoScore = Integer.valueOf(score);
                    if (!"".equals(subjectAllotPnt)) {
                        genkoAllotPnt = Integer.valueOf(subjectAllotPnt);
                    }
                } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_JAPANESE)) {
                    /* マーク模試「国語」得点を保持する */
                    japScore = Integer.valueOf(score);
                    if (!"".equals(subjectAllotPnt)) {
                        japAllotPnt = Integer.valueOf(subjectAllotPnt);
                    }
                }
            }
        }

        if (ExamUtil.isCenter(exam)) {
            /* センター・リサーチの「現代文」「古文」「漢文」の成績を追加する */
            addCenterJapanese(list, row, IDX_CVS_GENBUN, SUBCODE_CENTER_DEFAULT_LITERATURE);
            addCenterJapanese(list, row, IDX_CVS_KOBUN, SUBCODE_CENTER_DEFAULT_OLD);
            addCenterJapanese(list, row, IDX_CVS_KANBUN, SUBCODE_CENTER_DEFAULT_CHINESE);
        } else {
            /* マーク模試の「古文」「漢文」の成績を追加する */
            addMarkJapaneseOldAndChinese(list, genbunScore, genkoScore, japScore, genbunAllotPnt, genkoAllotPnt, japAllotPnt);
        }

        /* 科目成績を登録する */
        exmntnMainService.saveRecord(id, exam, exmntnMainService.preprocess(exam, list));
    }

    /**
     * 記述模試の科目成績を登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param row {@link CsvRow}
     */
    private void registWrtnSubRecord(Integer id, ExamBean exam, CsvRow row) {

        /* 科目成績リスト */
        List<BZSubjectRecord> list = CollectionsUtil.newArrayList(30);

        /* 重複チェック用の科目コードセット */
        Set<String> importedSubCdSet = CollectionsUtil.newHashSet();

        /* インポート可能科目コードセット */
        Set<String> importableSubCdSet = getImportableSubCdSet(exam);

        for (int i = 0; i < SUB_COUNT; i++) {

            CIndex subCdIndex = IDX_SUBCD.add(i * SUB_ITEM_COUNT);
            String subCd = row.get(subCdIndex);
            String subjectAllotPnt = row.get(subCdIndex.add(2));
            String score = row.get(subCdIndex.add(3));
            String deviation = row.get(subCdIndex.add(4));

            if (!checkSubCd(subCd) || !importedSubCdSet.add(subCd)) {
                /* 不正科目コードまたはインポート済みなら処理しない */
                continue;
            }

            if (subCd.compareTo("7000") >= 0) {
                /* 型：偏差値のみのレコードを生成する */
                if (checkDeviation(deviation)) {
                    list.add(new BZSubjectRecord(subCd, score, null, truncate(deviation), subjectAllotPnt));
                }
            } else if ((checkDeviation(deviation) && !deviation.isEmpty()) || (checkSubScore(score) && !score.isEmpty())) {
                /* 科目：偏差値のみのレコードを生成する */
                if (importableSubCdSet.contains(subCd)) {
                    /* インポート可能科目のみ追加する */
                    list.add(new BZSubjectRecord(subCd, score, null, truncate(deviation), calcWScope(row, subCd), subjectAllotPnt));
                }
            }
        }

        /* 科目成績を登録する */
        exmntnMainService.saveRecord(id, exam, list);
    }

    /**
     * 対象模試のインポート可能科目コードセットを返します。
     *
     * @param exam 対象模試
     * @return インポート可能科目コードセット
     */
    private Set<String> getImportableSubCdSet(ExamBean exam) {

        if (exam == null) {
            return null;
        }

        Set<String> set = EXAM_SUBCD_MAP.get(exam);
        if (set == null) {
            set = CollectionsUtil.newHashSet();
            if (ExamUtil.isMark(exam)) {
                addSubCd(set, subjectHelper.getMarkEng());
                addSubCd(set, subjectHelper.getMarkLis());
                addSubCd(set, subjectHelper.getMarkMath1List(exam));
                addSubCd(set, subjectHelper.getMarkMath2List(exam));
                if (!ExamUtil.isCenter(exam)) {
                    /* 国語はマーク模試の現代文のみ含める */
                    addSubCd(set, subjectHelper.getMarkJapCo());
                }
                addSubCd(set, subjectHelper.getMarkScience1List(exam));
                addSubCd(set, subjectHelper.getMarkScienceBasicList(exam));
                addSubCd(set, subjectHelper.getMarkSociety1List(exam));
            } else {
                addSubCd(set, subjectHelper.getWrtEng(exam, false));
                addSubCd(set, subjectHelper.getWrittenMathList(exam));
                addSubCd(set, subjectHelper.getWrittenJapaneseList(exam));
                addSubCd(set, subjectHelper.getWrittenScienceList(exam));
                addSubCd(set, subjectHelper.getWrittenScienceBasicList(exam));
                addSubCd(set, subjectHelper.getWrittenSocietyList(exam));
            }
            EXAM_SUBCD_MAP.put(exam, set);
        }

        return set;
    }

    /**
     * インポート対象科目コードセットに科目コードを追加します。
     *
     * @param subCdSet インポート対象科目コードセット
     * @param bean {@link ExmntnMainSubjectBean}
     */
    private void addSubCd(Set<String> subCdSet, BZSubjectRecord bean) {
        if (bean != null) {
            subCdSet.add(bean.getSubCd());
        }
    }

    /**
     * インポート対象科目コードセットに科目リストの科目コードを追加します。
     *
     * @param subCdSet インポート対象科目コードセット
     * @param list 科目リスト
     */
    private void addSubCd(Set<String> subCdSet, List<ComboBoxItem<BZSubjectRecord>> list) {
        for (ComboBoxItem<BZSubjectRecord> item : list) {
            addSubCd(subCdSet, item.getValue());
        }
    }

    /**
     * マーク模試の範囲区分を算出します。
     *
     * @param row {@link CsvRow}
     * @param exam 模試データ
     * @param subCd 科目コード
     * @param util {@link ExamSubjectUtil}
     * @return 範囲区分
     */
    private String calcMScope(CsvRow row, ExamBean exam, String subCd, ExamSubjectUtil util) {

        if (ExamUtil.isAns1st(exam)) {
            Integer flgSc = SC_FIRST_ANS_FLG_MAP.get(subCd);
            if (flgSc != null) {
                /* 第1解答科目フラグマップに定義あり(理科) */
                return firstAnsFlg2Scope(row.get(IDX_ENG_AREA), flgSc.intValue());
            }
            Integer flgSo = SO_FIRST_ANS_FLG_MAP.get(subCd);
            if (flgSo != null) {
                /* 第1解答科目フラグマップに定義あり(地歴公民) */
                return firstAnsFlg2Scope(row.get(IDX_PHY_AREA), flgSo.intValue());
            }
            if (util.isBasic(subCd)) {
                /* 理科基礎科目 */
                return ANSWER_NO_OTHER;
            }
        }

        return ANSWER_NO_NA;
    }

    /**
     * 第1解答科目フラグから範囲区分に変換します。
     *
     * @param flg 第1解答科目フラグ
     * @param value 判定値
     * @return 範囲区分（第1解答科目フラグが判定値の何れかと一致する場合は1、それ以外はnull）
     */
    private String firstAnsFlg2Scope(String flg, int value) {
        if (ValidateUtil.isPositiveInteger(flg) && !flg.isEmpty() && Integer.parseInt(flg) == value) {
            return ANSWER_NO_1;
        } else {
            return ANSWER_NO_OTHER;
        }
    }

    /**
     * 記述模試の範囲区分を算出します。
     *
     * @param row {@link CsvRow}
     * @param subCd 科目コード
     * @return 範囲区分
     */
    private String calcWScope(CsvRow row, String subCd) {
        switch (subCd) {
            case SUBCODE_SECOND_DEFAULT_ENGLISH:
                /* 英語 */
                return toScope(row.get(IDX_ENG_AREA));
            case SUBCODE_SECOND_DEFAULT_PHYSICS:
            case SUBCODE_SECOND_DEFAULT_CHEMISTRY:
            case SUBCODE_SECOND_DEFAULT_BIOLOGY:
            case SUBCODE_SECOND_DEFAULT_EARTH:
                /* ＃１〜＃３記述：理科専門 */
                return "2";
            case SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC:
            case SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC:
            case SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC:
            case SUBCODE_SECOND_DEFAULT_EARTH_BASIC:
                /* ＃１〜＃３記述：理科基礎*/
                return "1";
            case SUBCODE_SECOND_DEFAULT_PHYSICS_2ND:
                /* 高２記述：物理 */
                return toScope(row.get(IDX_PHY_AREA));
            case SUBCODE_SECOND_DEFAULT_CHEMISTRY_2ND:
                /* 高２記述：化学*/
                return toScope(row.get(IDX_CHM_AREA));
            case SUBCODE_SECOND_DEFAULT_BIOLOGY_2ND:
                /* 高２記述：生物 */
                return toScope(row.get(IDX_BIO_AREA));
            default:
                return "9";
        }
    }

    /**
     * 範囲区分に変換します。
     *
     * @param scope 範囲区分列データ
     * @return 数値範囲
     */
    private String toScope(String scope) {
        if (ValidateUtil.isInRange(scope, 0, 9)) {
            return scope;
        } else {
            return ANSWER_NO_NA;
        }
    }

    /**
     * 科目成績リストにマーク模試の「古文」「漢文」の成績を追加します。
     *
     * @param list 科目成績リスト
     * @param genbunScore 現代文得点
     * @param genkoScore 現古得点
     * @param japScore 国語得点
     * @param genbunAllotPnt 現代文配点
     * @param genkoAllotPnt 現古配点
     * @param japAllotPnt 国語配点
     */
    private void addMarkJapaneseOldAndChinese(List<BZSubjectRecord> list, Integer genbunScore, Integer genkoScore, Integer japScore, Integer genbunAllotPnt, Integer genkoAllotPnt, Integer japAllotPnt) {

        if (genbunScore == null) {
            /* 現代文の成績がなければ処理はここまで */
            return;
        }

        if (genkoScore == null) {
            /* 現古の成績がなければ処理はここまで */
            return;
        }

        /* 古文の成績を追加する */
        int kobunScore = genkoScore.intValue() - genbunScore.intValue();
        String kobunAllotPnt = null;
        if (genkoAllotPnt != null && genbunAllotPnt != null) {
            kobunAllotPnt = Integer.toString(genkoAllotPnt.intValue() - genbunAllotPnt.intValue());
        }
        if (kobunScore >= 0 && kobunScore <= checkHelper.getMarkFullScore(SUBCODE_CENTER_DEFAULT_OLD)) {
            list.add(new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_OLD, Integer.toString(kobunScore), null, null, kobunAllotPnt));
        }

        if (japScore == null) {
            /* 国語の成績がなければ処理はここまで */
            return;
        }

        /* 漢文の成績を追加する */
        int kanbunScore = japScore.intValue() - genkoScore.intValue();
        String kanbunAllotPnt = null;
        if (japAllotPnt != null && genkoAllotPnt != null) {
            kanbunAllotPnt = Integer.toString(japAllotPnt.intValue() - genkoAllotPnt.intValue());
        }
        if (kanbunScore >= 0 && kanbunScore <= checkHelper.getMarkFullScore(SUBCODE_CENTER_DEFAULT_CHINESE)) {
            list.add(new BZSubjectRecord(SUBCODE_CENTER_DEFAULT_CHINESE, Integer.toString(kanbunScore), null, null, kanbunAllotPnt));
        }
    }

    /**
     * 科目成績リストにセンター・リサーチの「現代文」「古文」「漢文」の成績を追加します。
     *
     * @param list 科目成績リスト
     * @param row {@link CsvRow}
     * @param index 列位置
     * @param subCd 科目コード
     */
    private void addCenterJapanese(List<BZSubjectRecord> list, CsvRow row, CIndex index, String subCd) {
        String score = row.get(index);
        if (checkSubScore(score) && !score.isEmpty()) {
            if (Integer.parseInt(score) <= checkHelper.getMarkFullScore(subCd)) {
                /* 共通テストリサーチの場合、配点を持っていないためnullを設定する */
                list.add(new BZSubjectRecord(subCd, score, null, null, null));
            }
        }
    }

    /**
     * 模試記入志望大学データを登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param row {@link CsvRow}
     */
    private void registCandidateRating(Integer id, ExamBean exam, CsvRow row) {

        /* マーク模試フラグ */
        boolean isMark = ExamUtil.isMark(exam);

        for (int rank = 0; rank < CANDIDATE_COUNT; rank++) {
            CIndex univCd10Index = IDX_UNIVCD10.add(rank * CANDIDATE_ITEM_COUNT);
            String univCd10 = row.get(univCd10Index);
            String score = row.get(univCd10Index.add(2));
            String cRating = row.get(univCd10Index.add(3));
            String sRating = row.get(univCd10Index.add(4));
            String tPoint = row.get(univCd10Index.add(5));
            String tRating = row.get(univCd10Index.add(6));
            String scheduleCd = findScheduleCd(exam, univCd10);

            String fullpoint = "";
            String fullpointinc = "";
            String scoreinc = "";
            String ratinginc = "";
            String judge = "";
            String notice = "";

            if (scheduleCd == null) {
                /* 日程コードが取得できなければ大学マスタに存在しないので飛ばす */
                continue;
            }

            /* 得点登録フラグを判定する */
            boolean isScore = isMark && !scheduleCd.equals(Univ.SCHEDULE_NML);

            if (checkCandidateRating(isScore, score, cRating, sRating, tPoint, tRating)) {
                registCandidateRating(id, exam, rank, isScore, univCd10, score, tPoint, cRating, sRating, tRating, fullpoint, fullpointinc, scoreinc, ratinginc, judge, notice);
            }
        }
    }

    /**
     * 模試記入志望大学データを登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param rank 志望順位（ゼロベース）
     * @param isScore 得点登録フラグ
     * @param univCd10 大学コード10桁
     * @param score 得点/偏差値
     * @param tPoint 総合評価ポイント
     * @param cRating センター評価
     * @param sRating 二次評価
     * @param tRating 総合評価
     * @param fullpoint 満点値
     * @param fullpointinc 共通テスト(含)満点値
     * @param scoreinc 共通テスト(含)評価得点/偏差値
     * @param ratinginc 共通テスト(含)評価
     * @param judge 出願資格判定区分
     * @param notice 出願資格非対応告知区分
     */
    private void registCandidateRating(Integer id, ExamBean exam, int rank, boolean isScore, String univCd10, String score, String tPoint, String cRating, String sRating, String tRating, String fullpoint, String fullpointinc,
            String scoreinc, String ratinginc, String judge, String notice) {

        UnivKeyBean univKey = new UnivKeyBean(univCd10);

        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("rank", Integer.valueOf(rank + 1));
        param.put("univCd", univKey.getUnivCd());
        param.put("facultyCd", univKey.getFacultyCd());
        param.put("deptCd", univKey.getDeptCd());
        if (isScore) {
            param.put("score", score);
            param.put("deviation", null);
            param.put("scoreinc", scoreinc);
            param.put("deviationinc", null);
        } else {
            param.put("score", null);
            param.put("deviation", truncate(score));
            param.put("scoreinc", null);
            param.put("deviationinc", truncate(scoreinc));
        }
        param.put("tPoint", tPoint);
        param.put("cRating", cRating);
        param.put("sRating", sRating);
        param.put("tRating", tRating);
        param.put("fullpoint", fullpoint);
        param.put("fullpointinc", fullpointinc);
        param.put("ratinginc", ratinginc);
        param.put("judge", judge);
        param.put("notice", notice);

        dao.insertCandidateRating(param);
    }

    /**
     * 指定した大学の日程コードを取得します。
     *
     * @param exam 模試データ
     * @param univCd10 大学コード10桁
     * @return 日程コード
     */
    private String findScheduleCd(ExamBean exam, String univCd10) {

        if (univCd10.length() < 10) {
            return null;
        }

        return dao.selectScheduleCd(exam, new UnivKeyBean(univCd10));
    }

    /**
     * 志望大学データを登録します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    private void registCandidateUniv(Integer id, ExamBean exam) {

        /* 志望大学データ（模試記入志望大学データのみ）を削除する */
        if (dao.deleteCandidateUniv(id, exam) > 0) {
            /* 再インポート時は、非表示の志望大学を表示しておく */
            dao.updateCandidateUnivDispOn(id, exam);
        }

        /* 模試記入志望大学データを志望大学にコピーする */
        dao.insertCandidateUniv(id, exam, systemDto.getUnivExamDiv());

        /* 模試記入志望大学と重複している志望大学を非表示にする */
        dao.updateCandidateUnivDispOff(id, exam);

        /* 志望大学の表示順を振り直す */
        dao.updateCandidateUnivDispNumber(dao.selectCandidateUnivList(id, exam));
    }

    /**
     * インポート状況を登録する。
     *
     * @param importedExamSet インポート実施模試セット
     */
    private void registImportStatus(Set<ExamBean> importedExamSet) {
        for (ExamBean exam : importedExamSet) {
            dao.deleteStatSubRecord(exam);
            dao.insertStatSubRecord(exam);
        }
    }

    /**
     * 取り込み開始ログを出力します。
     *
     * @param file CSVファイル
     */
    private void outputStartLog(File file) {
        logService.output(GamenId.E1, "個人成績取り込み開始", file.getAbsolutePath());
    }

    /**
     * 取り込み終了ログを出力します。
     *
     * @param importedExamSet インポート実施模試セット
     */
    private void outputEndLog(Set<ExamBean> importedExamSet) {
        for (ExamBean exam : importedExamSet) {
            logService.output(GamenId.E1, "個人成績取り込み終了", exam.getExamYear(), exam.getExamCd());
        }
    }

    /**
     * 取り込みエラーログを出力します。
     *
     * @param maker {@link B03CsvMaker}
     */
    private void outputErrLog(B03CsvMaker maker) {
        String message = "共通テストリサーチ取込み年度エラーのため取込失敗しました。";
        logService.output(GamenId.E1, message);
        maker.addErrMessage();
    }

    /**
     * CSVで入力されている模試コードに一致する模試データを返します。
     *
     * @param row {@link CsvRow}
     * @param examList インポート可能模試リスト
     * @return {@link ExamBean}
     */
    private ExamBean findExamBean(CsvRow row, List<ExamBean> examList) {

        String examYear = row.get(IDX_EXAM_YEAR);
        String examCd = row.get(IDX_EXAM_CD);

        if (StringUtils.isEmpty(examYear) || !ValidateUtil.isPositiveInteger(examYear)) {
            return null;
        }
        /* 模試コードを0詰めする */
        if (!StringUtils.isEmpty(examCd) && examCd.length() == 1) {
            examCd = "0" + examCd;
        }

        for (ExamBean bean : examList) {
            if (bean.getExamYear().equals(examYear) && bean.getExamCd().equals(examCd)) {
                return bean;
            }
        }

        return null;
    }

    /**
     * 生徒情報のチェックを行います。
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkStudent(CsvRow row) {
        return checkGrade(row) && checkClass(row) && checkClassNo(row) && checkKanaName(row) && checkByteLength(row, IDX_EXAMTYPE, 1) && checkByteLength(row, IDX_BUNRICODE, 1);
    }

    /**
     * 学年をチェックします。
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkGrade(CsvRow row) {
        String grade = row.get(IDX_GRADE);
        return StringUtils.isEmpty(grade) || ValidateUtil.isInRange(grade, 0, 9);
    }

    /**
     * クラスをチェックします。
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkClass(CsvRow row) {
        /* スペースが入っていてもエラーにならないようにTRIMしてからチェックする */
        String cls = row.get(IDX_CLASS).trim();
        return cls.length() <= 2 && ValidateUtil.isAlphaNum(cls);
    }

    /**
     * クラス番号をチェックします。
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkClassNo(CsvRow row) {
        String classNo = row.get(IDX_CLASS_NO);
        return classNo.length() <= 5 && ValidateUtil.isPositiveInteger(classNo);
    }

    /**
     * カナ氏名をチェックします。
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkKanaName(CsvRow row) {
        return ValidateUtil.isHankaku(row.get(IDX_KANA_NAME));
    }

    /**
     * 文字列のバイト数をチェックします。
     *
     * @param row {@link CsvRow}
     * @param index 列インデックス
     * @param limit バイト数上限
     * @return エラー無しならtrue
     */
    private boolean checkByteLength(CsvRow row, CIndex index, int limit) {
        try {
            return row.get(index).getBytes("Windows-31J").length <= limit;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 科目コードをチェックします。
     *
     * @param subCd 科目コード
     * @return エラー無しならtrue
     */
    private boolean checkSubCd(String subCd) {
        return ValidateUtil.isInRange(subCd, 1000, 9999);
    }

    /**
     * 科目得点をチェックします。
     *
     * @param score 得点
     * @return エラー無しならtrue
     */
    private boolean checkSubScore(String score) {
        return StringUtils.isEmpty(score) || ValidateUtil.isInRange(score, 0, 999);
    }

    /**
     * 偏差値をチェックします。
     *
     * @param deviation 偏差値
     * @return エラー無しならtrue
     */
    private boolean checkDeviation(String deviation) {
        return StringUtils.isEmpty(deviation) || ValidateUtil.isInRange(deviation, 0, 999.9);
    }

    /**
     * 志望校データをチェックします。
     *
     * @param isScore 得点登録フラグ
     * @param score 得点／偏差値
     * @param cRating センター評価
     * @param sRating 二次評価
     * @param tPoint 総合評価ポイント
     * @param tRating 総合評価
     * @return エラー無しならtrue
     */
    private boolean checkCandidateRating(boolean isScore, String score, String cRating, String sRating, String tPoint, String tRating) {

        /* 得点を登録するなら得点チェックを行い、そうでなければ偏差値チェックを行う */
        if (isScore) {
            if (!checkRatingScore(score)) {
                return false;
            }
        } else if (!checkDeviation(score)) {
            return false;
        }

        return checkRating(cRating) && checkRating(sRating) && checkRating(tRating) && checkRatingPoint(tPoint);
    }

    /**
     * 評価得点をチェックします。
     *
     * @param score 得点
     * @return エラー無しならtrue
     */
    private boolean checkRatingScore(String score) {
        return StringUtils.isEmpty(score) || ValidateUtil.isInRange(score, 0, 9999);
    }

    /**
     * 評価をチェックします。
     *
     * @param rating 評価
     * @return エラー無しならtrue
     */
    private boolean checkRating(String rating) {
        return rating.getBytes().length <= 3;
    }

    /**
     * 評価ポイントをチェックします。
     *
     * @param point 評価ポイント
     * @return エラー無しならtrue
     */
    private boolean checkRatingPoint(String point) {
        return StringUtils.isEmpty(point) || ValidateUtil.isInRange(point, 0, 999);
    }

    /**
     * ヘッダをチェックします。(共通テストリサーチの年度チェック)
     *
     * @param row {@link CsvRow}
     * @return エラー無しならtrue
     */
    private boolean checkHeader(CsvRow row) {
        boolean ret = false;
        String header = row.get(IDX_CENTERCHECK);

        if (StringUtils.isEmpty(header) || header.equals("志望大1セ評価") || header.equals("評セ1")) {
            ret = true;
        }
        return ret;
    }

    /**
     * 偏差値を小数第一位までで切り捨てます。
     *
     * @param dev 偏差値
     * @return 切り捨てた偏差値
     */
    private String truncate(String dev) {
        int index = dev.indexOf('.');
        if (index > -1 && index + 1 < dev.length()) {
            return dev.substring(0, index + 2);
        } else {
            return dev;
        }
    }

}

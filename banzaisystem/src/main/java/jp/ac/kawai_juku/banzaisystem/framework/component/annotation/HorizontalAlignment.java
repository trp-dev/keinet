package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.swing.SwingConstants;

/**
 *
 * HorizontalAlignmentを指定するためのアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface HorizontalAlignment {

    /**
     * HorizontalAlignmentを設定します。
     *
     * @return 横位置
     */
    int value() default SwingConstants.LEFT;

}

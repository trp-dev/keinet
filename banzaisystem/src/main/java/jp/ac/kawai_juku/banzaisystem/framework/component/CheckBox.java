package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * チェックボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CheckBox extends JCheckBox implements BZComponent {

    /** 前景色 */
    private Color foreGround;

    /**
     * コンストラクタです。
     */
    public CheckBox() {
        setOpaque(false);
        setBorder(null);
        setMargin(new Insets(0, 0, 0, 0));
        setVerticalAlignment(SwingConstants.TOP);
        setIconTextGap(6);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        if (field.isAnnotationPresent(Selected.class)) {
            setSelected(true);
        }

        if (!field.isAnnotationPresent(NoAction.class)) {
            Action action = field.getAnnotation(Action.class);
            final ActionListener listener;
            if (action == null) {
                listener = new ControllerActionListener(view, field.getName());
            } else {
                listener = new ControllerActionListener(view, action.value());
            }
            /* ActionListenerだとsetSelectedメソッドでの値変更時にイベントが発生しないため、ItemListenerを利用する */
            addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    listener.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, null));
                }
            });
        }

        if (!field.isAnnotationPresent(Size.class)) {
            setSize(13, 13);
        }
    }

    /**
     * 値変更イベントを起こさずに選択状態を変更します。
     *
     * @param b 選択状態
     */
    public final void setSelectedSilent(boolean b) {
        List<ItemListener> list = CollectionsUtil.newArrayList();
        for (ItemListener l : getItemListeners()) {
            removeItemListener(l);
            list.add(l);
        }
        setSelected(b);
        for (ItemListener l : list) {
            addItemListener(l);
        }
    }

    @Override
    public Color getForeground() {
        return foreGround != null && isEnabled() ? foreGround : super.getForeground();
    }

    @Override
    public void setForeground(Color fg) {
        foreGround = fg;
        if (fg != null && !fg.equals(getForeground())) {
            repaint();
        }
    }

}

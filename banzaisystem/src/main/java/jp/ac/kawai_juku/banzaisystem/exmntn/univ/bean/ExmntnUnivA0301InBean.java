package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

import java.util.Collections;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

/**
 *
 * Excel帳票メーカー(簡易個人表)の入力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivA0301InBean implements ExcelMakerInBean {

    /** 生徒情報リスト */
    private final List<ExmntnUnivA0301StudentInBean> studentList;

    /** 第1面出力フラグ */
    private final boolean output1stFlg;

    /** 第2面出力フラグ */
    private final boolean output2ndFlg;

    /**
     * コンストラクタです。
     *
     * @param studentBean 生徒情報Bean
     * @param scoreBean 成績データ＋選択科目情報
     * @param candidateUnivList 志望大学情報リスト
     * @param output1stFlg 第1面出力フラグ
     * @param output2ndFlg 第2面出力フラグ
     */
    public ExmntnUnivA0301InBean(ExmntnUnivStudentBean studentBean, ScoreBean scoreBean, List<ExmntnMainCandidateUnivBean> candidateUnivList, boolean output1stFlg, boolean output2ndFlg) {
        this.studentList = Collections.singletonList(new ExmntnUnivA0301StudentInBean(studentBean, scoreBean, candidateUnivList, false));
        this.output1stFlg = output1stFlg;
        this.output2ndFlg = output2ndFlg;
    }

    /**
     * コンストラクタです。
     *
     * @param studentList 生徒情報リスト
     */
    public ExmntnUnivA0301InBean(List<ExmntnUnivA0301StudentInBean> studentList) {
        this.studentList = studentList;
        this.output1stFlg = true;
        this.output2ndFlg = true;
    }

    /**
     * 生徒情報リストを返します。
     *
     * @return 生徒情報リスト
     */
    public List<ExmntnUnivA0301StudentInBean> getStudentList() {
        return studentList;
    }

    /**
     * 第1面出力フラグを返します。
     *
     * @return 第1面出力フラグ
     */
    public boolean isOutput1stFlg() {
        return output1stFlg;
    }

    /**
     * 第2面出力フラグを返します。
     *
     * @return 第2面出力フラグ
     */
    public boolean isOutput2ndFlg() {
        return output2ndFlg;
    }

}

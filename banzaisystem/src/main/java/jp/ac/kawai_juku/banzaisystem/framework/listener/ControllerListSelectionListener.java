package jp.ac.kawai_juku.banzaisystem.framework.listener;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.awt.event.ActionEvent;
import java.lang.reflect.Method;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.creator.ControllerCreator;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.util.MethodUtil;

/**
 *
 * コントローラのアクションメソッドを呼び出す{@link ListSelectionListener}です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ControllerListSelectionListener implements ListSelectionListener {

    /** コントローラ */
    private final Object controller;

    /** アクション名 */
    private final String actionName;

    /**
     * コンストラクタです。
     *
     * @param view {@link AbstractView}
     * @param name コンポーネント名
     */
    public ControllerListSelectionListener(AbstractView view, String name) {
        this.actionName = name + "Action";
        this.controller = getComponent(view.getId() + ControllerCreator.NAME_SUFFIX);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

        for (Method method : BZClassUtil.getOriginalClass(controller.getClass()).getMethods()) {
            if (method.isAnnotationPresent(Execute.class) && method.getName().equals(actionName)) {
                Class<?>[] params = method.getParameterTypes();
                Object[] args = createArgs(e, params);
                MethodUtil.invoke(method, controller, args);
                return;
            }
        }

        throw new RuntimeException(String.format("コントローラ[%s]にアクションメソッド[%s]が宣言されていません。", getCtrlName(), actionName));
    }

    /**
     * アクションメソッドの引数を生成します。
     *
     * @param e {@link ActionEvent}
     * @param params パラメータ配列
     * @return 引数
     */
    private Object[] createArgs(ListSelectionEvent e, Class<?>[] params) {

        if (params.length == 0) {
            return new Object[0];
        } else if (params.length > 1) {
            throw new RuntimeException(String.format("コントローラ[%s]のアクションメソッド[%s]に複数の引数が定義されています。", getCtrlName(),
                    actionName));
        }

        Class<?> cls = params[0];
        if (ActionEvent.class.isAssignableFrom(cls)) {
            return new Object[] { e };
        } else {
            throw new RuntimeException(String.format("コントローラ[%s]のアクションメソッド[%s]に非対応の引数[%s]が宣言されています。", getCtrlName(),
                    actionName, cls.getName()));
        }
    }

    /**
     * コントローラのクラス名を返します。
     *
     * @return コントローラのクラス名
     */
    private String getCtrlName() {
        return BZClassUtil.getSimpleName(controller.getClass());
    }

}

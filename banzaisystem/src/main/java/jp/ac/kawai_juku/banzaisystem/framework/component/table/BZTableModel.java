package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.seasar.framework.log.Logger;

/**
 *
 * バンザイシステムのテーブルモデルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZTableModel extends DefaultTableModel {

    /** Logger */
    private static final Logger logger = Logger.getLogger(BZTableModel.class);

    /**
     * コンストラクタです。
     *
     * @param columnCount カラム数
     */
    public BZTableModel(int columnCount) {
        super(0, columnCount);
    }

    /**
     * データをセットします。
     *
     * @param dataVector データ
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public final void setDataVector(Vector dataVector) {
        /*
         * タスク実行中にデータを書き換えると、テーブルの書き換えが複数スレッドで同時に起こり、
         * システムエラーとなる場合があるため、タスク実行中のデータ書き換えを行わないよう、
         * 開発中のみ例外を出して開発者に知らせるようにする。
         */
        if (logger.isDebugEnabled() && !Thread.currentThread().getName().startsWith("AWT-EventQueue-")) {
            throw new RuntimeException("データの書き換えはイベントディスパッチスレッドでのみ行えます。");
        }
        getDataVector().removeAllElements();
        getDataVector().addAll(dataVector);
        fireTableDataChanged();
    }

}

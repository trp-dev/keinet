package jp.ac.kawai_juku.banzaisystem.selstd.dadd.view;

import java.util.ArrayList;
import java.util.List;

import javax.swing.text.AbstractDocument;

import jp.ac.kawai_juku.banzaisystem.framework.component.AlphaNumDocumentFilter;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ShimeiHiraganaDocumentFilter;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NumberField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * 個人追加ダイアログのViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class SelstdDaddView extends AbstractView {

    /** コンボボックス_学年 */
    @Location(x = 55, y = 29)
    @Size(width = 43, height = 20)
    @NoAction
    public ComboBox<Integer> gradeComboBox;

    /** テキスト_クラス  */
    @Location(x = 146, y = 29)
    @Size(width = 42, height = 20)
    public TextField classField;

    /** テキスト_番号 */
    @Location(x = 230, y = 29)
    @Size(width = 60, height = 20)
    @NumberField(maxLength = 5)
    public TextField numberField;

    /** テキスト_かな氏名 */
    @Location(x = 98, y = 66)
    @Size(width = 200, height = 20)
    public TextField nameField;

    /** 説明ラベル */
    @Location(x = 26, y = 134)
    @Foreground(r = 55, g = 51, b = 50)
    public TextLabel noteLabel;

    /** OKボタン */
    @Location(x = 90, y = 191)
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 177, y = 191)
    public ImageButton cancelButton;

    @Override
    public void initialize() {

        super.initialize();

        /* このダイアログ上の特定の TextFieldのみ DocumentFilterを差し換える */

        /* クラス -- 最大文字数は 半角 x 2 */
        ((AbstractDocument) classField.getDocument()).setDocumentFilter(new AlphaNumDocumentFilter(2));
        /* IMEをOFFにする */
        classField.enableInputMethods(false);

        /* かな氏名 -- 最大文字数は 半角カタカナ x 13 */
        ((AbstractDocument) nameField.getDocument()).setDocumentFilter(new ShimeiHiraganaDocumentFilter());

        /* 学年コンボボックスを初期化する */
        List<ComboBoxItem<Integer>> itemList = new ArrayList<>(5);
        for (int i = 1; i <= 5; i++) {
            itemList.add(new ComboBoxItem<Integer>(Integer.valueOf(i)));
        }
        gradeComboBox.setItemList(itemList);
    }

}

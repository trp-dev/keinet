package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.JFileChooser;

/**
 *
 * 存在しないファイル、ディレクトリを選択できない {@link JFileChooser} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class InputFileChooser extends JFileChooser {

    /**
     * コンストラクタです。
     */
    public InputFileChooser() {
    }

    /**
     * コンストラクタです。
     *
     * @param currentDirectoryPath カレントディレクトリパス
     */
    public InputFileChooser(String currentDirectoryPath) {
        super(currentDirectoryPath);
    }

    @Override
    public void approveSelection() {
        if (getSelectedFile().exists()) {
            super.approveSelection();
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterChoiceSchoolBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterCourseBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSelectGpCourseBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSelectGroupBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 公表用大学マスタのロードDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterDao extends BaseDao {

    /**
     * 大学マスタ公表用志望校リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return {@link PubUnivMasterChoiceSchoolBean}のリスト
     */
    public List<PubUnivMasterChoiceSchoolBean> selectPubUnivMasterChoiceSchoolList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(PubUnivMasterChoiceSchoolBean.class, getSqlPath(), key).getResultList();
    }

    /**
     * 大学マスタ公表用教科リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return {@link PubUnivMasterCourseBean}のリスト
     */
    public List<PubUnivMasterCourseBean> selectPubUnivMasterCourseList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(PubUnivMasterCourseBean.class, getSqlPath(), key).getResultList();
    }

    /**
     * 大学マスタ公表用科目リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return {@link PubUnivMasterSubjectBean}のリスト
     */
    public List<PubUnivMasterSubjectBean> selectPubUnivMasterSubjectList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(PubUnivMasterSubjectBean.class, getSqlPath(), key).getResultList();
    }

    /**
     * 大学マスタ公表用選択グループリストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return {@link PubUnivMasterSelectGroupBean}のリスト
     */
    public List<PubUnivMasterSelectGroupBean> selectPubUnivMasterSelectGroupList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(PubUnivMasterSelectGroupBean.class, getSqlPath(), key).getResultList();
    }

    /**
     * 大学マスタ公表用選択グループ教科リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return {@link PubUnivMasterSelectGroupBean}のリスト
     */
    public List<PubUnivMasterSelectGpCourseBean> selectPubUnivMasterSelectGpCourseList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(PubUnivMasterSelectGpCourseBean.class, getSqlPath(), key).getResultList();
    }

}

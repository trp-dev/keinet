package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 背景画像を差し替え可能なイメージパネルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SwitchableImagePanel extends JPanel implements BZComponent {

    /** 背景画像 */
    private BufferedImage img;

    /**
     * コンストラクタです。
     */
    public SwitchableImagePanel() {
        /* 絶対位置レイアウトにする */
        super(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
    }

    /**
     * 背景画像を差し替えます。
     *
     * @param clazz 背景画像読み込み位置の基準クラス
     * @param name 背景画像ファイル名
     */
    public void replace(Class<?> clazz, String name) {
        img = ImageUtil.readImage(clazz, name);
        Dimension size = new Dimension(img.getWidth(), img.getHeight());
        setSize(size);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (img != null) {
            g.drawImage(img, 0, 0, null);
        }
    }

}

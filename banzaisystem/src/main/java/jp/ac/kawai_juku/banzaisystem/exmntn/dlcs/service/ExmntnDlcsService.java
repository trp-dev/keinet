package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean.ExmntnDlcsQualifiableCertificationInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean.ExmntnDlcsQualifiableCertificationOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.dao.ExmntnDlcsDao;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 取得可能資格ダイアログのServiceです。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnDlcsService {

    /** DAO */
    @Resource
    private ExmntnDlcsDao exmntnDlcsDao;

    /**
     * 取得可能資格名のリストを取得します。
     *
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @param deptCd 学科コード
     * @return 取得可能資格名リスト
     */
    public List<String> findQualifiableCertificationList(String univCd, String facultyCd, String deptCd) {
        /* 検索条件を準備：大学・学部・学科 */
        ExmntnDlcsQualifiableCertificationInBean serchCriteriaBean = new ExmntnDlcsQualifiableCertificationInBean();
        serchCriteriaBean.setUnivCd(univCd);
        serchCriteriaBean.setFacultyCd(facultyCd);
        serchCriteriaBean.setDeptCd(deptCd);
        /* 取得可能資格リストを取得 */
        List<ExmntnDlcsQualifiableCertificationOutBean> searchResultList =
                exmntnDlcsDao.selectQualifiableCertificationList(serchCriteriaBean);
        /* 資格名のみをリスト化 */
        List<String> nameList = CollectionsUtil.newArrayList();
        for (ExmntnDlcsQualifiableCertificationOutBean certRecord : searchResultList) {
            nameList.add(certRecord.getCertName());
        }
        /* 取得可能資格名のリストを返す */
        return nameList;
    }

}

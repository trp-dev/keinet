package jp.ac.kawai_juku.banzaisystem.exmntn.fclt.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * コンポーネントに文理区分を設定するためのアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Academic {
}

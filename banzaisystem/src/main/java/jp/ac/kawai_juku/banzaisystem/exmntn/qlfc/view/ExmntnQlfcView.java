package jp.ac.kawai_juku.banzaisystem.exmntn.qlfc.view;

import javax.swing.JCheckBox;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 大学検索（取得可能資格）画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnQlfcView extends SubView {

    /** 取得可能資格クリアボタン */
    @Location(x = 511, y = 12)
    public ImageButton clearObtainedLicenseButton;

    /** 資格カテゴリ：教員 */
    @Location(x = 11, y = 51)
    @CodeConfig(Code.CREDENTIAL_CATEGORY1)
    @Action("changeCategory")
    @Font(BZFont.CREDENTIAL_CATEGORY)
    public CodeCheckBox cc1CheckBox;

    /** 資格カテゴリ：社会(教育)・福祉・心理 */
    @Location(x = 11, y = 401)
    @CodeConfig(Code.CREDENTIAL_CATEGORY2)
    @Action("changeCategory")
    @Font(BZFont.CREDENTIAL_CATEGORY)
    public CodeCheckBox cc2CheckBox;

    /** 資格カテゴリ：医療・健康 */
    @Location(x = 11, y = 507)
    @CodeConfig(Code.CREDENTIAL_CATEGORY3)
    @Action("changeCategory")
    @Font(BZFont.CREDENTIAL_CATEGORY)
    public CodeCheckBox cc3CheckBox;

    /** 資格カテゴリ：土木・建築・技術 */
    @Location(x = 11, y = 699)
    @CodeConfig(Code.CREDENTIAL_CATEGORY4)
    @Action("changeCategory")
    @Font(BZFont.CREDENTIAL_CATEGORY)
    public CodeCheckBox cc4CheckBox;

    /** 資格カテゴリ：バイオ・食品・生活科学 */
    @Location(x = 11, y = 782)
    @CodeConfig(Code.CREDENTIAL_CATEGORY5)
    @Action("changeCategory")
    @Font(BZFont.CREDENTIAL_CATEGORY)
    public CodeCheckBox cc5CheckBox;

    /** 資格：保育士 */
    @Location(x = 137, y = 49)
    @CodeConfig(Code.CREDENTIAL_43)
    @Action("changeCredential")
    public CodeCheckBox c43CheckBox;

    /** 資格：幼稚園教諭 */
    @Location(x = 310, y = 49)
    @CodeConfig(Code.CREDENTIAL_01)
    @Action("changeCredential")
    public CodeCheckBox c01CheckBox;

    /** 資格：小学校教諭 */
    @Location(x = 475, y = 49)
    @CodeConfig(Code.CREDENTIAL_02)
    @Action("changeCredential")
    public CodeCheckBox c02CheckBox;

    /** 資格：中学校教諭(国語) */
    @Location(x = 137, y = 81)
    @CodeConfig(Code.CREDENTIAL_03)
    @Action("changeCredential")
    public CodeCheckBox c03CheckBox;

    /** 資格：中学校教諭(社会) */
    @Location(x = 310, y = 81)
    @CodeConfig(Code.CREDENTIAL_04)
    @Action("changeCredential")
    public CodeCheckBox c04CheckBox;

    /** 資格：中学校教諭(数学)  */
    @Location(x = 475, y = 81)
    @CodeConfig(Code.CREDENTIAL_05)
    @Action("changeCredential")
    public CodeCheckBox c05CheckBox;

    /** 資格：中学校教諭(理科) */
    @Location(x = 137, y = 103)
    @CodeConfig(Code.CREDENTIAL_06)
    @Action("changeCredential")
    public CodeCheckBox c06CheckBox;

    /** 資格：中学校教諭(音楽) */
    @Location(x = 310, y = 103)
    @CodeConfig(Code.CREDENTIAL_07)
    @Action("changeCredential")
    public CodeCheckBox c07CheckBox;

    /** 資格：中学校教諭(美術)  */
    @Location(x = 475, y = 103)
    @CodeConfig(Code.CREDENTIAL_08)
    @Action("changeCredential")
    public CodeCheckBox c08CheckBox;

    /** 資格：中学校教諭(保健体育) */
    @Location(x = 137, y = 125)
    @CodeConfig(Code.CREDENTIAL_09)
    @Action("changeCredential")
    public CodeCheckBox c09CheckBox;

    /** 資格：中学校教諭(保健のみ) */
    @Location(x = 310, y = 125)
    @CodeConfig(Code.CREDENTIAL_10)
    @Action("changeCredential")
    public CodeCheckBox c10CheckBox;

    /** 資格：中学校教諭(家庭)  */
    @Location(x = 475, y = 125)
    @CodeConfig(Code.CREDENTIAL_11)
    @Action("changeCredential")
    public CodeCheckBox c11CheckBox;

    /** 資格：中学校教諭(英語) */
    @Location(x = 137, y = 147)
    @CodeConfig(Code.CREDENTIAL_12)
    @Action("changeCredential")
    public CodeCheckBox c12CheckBox;

    /** 資格：中学校教諭(技術) */
    @Location(x = 310, y = 147)
    @CodeConfig(Code.CREDENTIAL_13)
    @Action("changeCredential")
    public CodeCheckBox c13CheckBox;

    /** 資格：高等学校教諭(国語) */
    @Location(x = 137, y = 179)
    @CodeConfig(Code.CREDENTIAL_14)
    @Action("changeCredential")
    public CodeCheckBox c14CheckBox;

    /** 資格：高等学校教諭(地理歴史) */
    @Location(x = 310, y = 179)
    @CodeConfig(Code.CREDENTIAL_15)
    @Action("changeCredential")
    public CodeCheckBox c15CheckBox;

    /** 資格：高等学校教諭(公民) */
    @Location(x = 475, y = 179)
    @CodeConfig(Code.CREDENTIAL_16)
    @Action("changeCredential")
    public CodeCheckBox c16CheckBox;

    /** 資格：高等学校教諭(数学) */
    @Location(x = 137, y = 201)
    @CodeConfig(Code.CREDENTIAL_17)
    @Action("changeCredential")
    public CodeCheckBox c17CheckBox;

    /** 資格：高等学校教諭(理科) */
    @Location(x = 310, y = 201)
    @CodeConfig(Code.CREDENTIAL_18)
    @Action("changeCredential")
    public CodeCheckBox c18CheckBox;

    /** 資格：高等学校教諭(音楽) */
    @Location(x = 475, y = 201)
    @CodeConfig(Code.CREDENTIAL_19)
    @Action("changeCredential")
    public CodeCheckBox c19CheckBox;

    /** 資格：高等学校教諭(美術) */
    @Location(x = 137, y = 223)
    @CodeConfig(Code.CREDENTIAL_20)
    @Action("changeCredential")
    public CodeCheckBox c20CheckBox;

    /** 資格：高等学校教諭(保健体育) */
    @Location(x = 310, y = 223)
    @CodeConfig(Code.CREDENTIAL_21)
    @Action("changeCredential")
    public CodeCheckBox c21CheckBox;

    /** 資格：高等学校教諭(保健のみ) */
    @Location(x = 475, y = 223)
    @CodeConfig(Code.CREDENTIAL_22)
    @Action("changeCredential")
    public CodeCheckBox c22CheckBox;

    /** 資格：高等学校教諭(家庭) */
    @Location(x = 137, y = 245)
    @CodeConfig(Code.CREDENTIAL_23)
    @Action("changeCredential")
    public CodeCheckBox c23CheckBox;

    /** 資格：高等学校教諭(英語) */
    @Location(x = 310, y = 245)
    @CodeConfig(Code.CREDENTIAL_24)
    @Action("changeCredential")
    public CodeCheckBox c24CheckBox;

    /** 資格：高等学校教諭(書道) */
    @Location(x = 475, y = 245)
    @CodeConfig(Code.CREDENTIAL_25)
    @Action("changeCredential")
    public CodeCheckBox c25CheckBox;

    /** 資格：高等学校教諭(工業) */
    @Location(x = 137, y = 267)
    @CodeConfig(Code.CREDENTIAL_26)
    @Action("changeCredential")
    public CodeCheckBox c26CheckBox;

    /** 資格：高等学校教諭(工芸) */
    @Location(x = 310, y = 267)
    @CodeConfig(Code.CREDENTIAL_27)
    @Action("changeCredential")
    public CodeCheckBox c27CheckBox;

    /** 資格：高等学校教諭(看護) */
    @Location(x = 475, y = 267)
    @CodeConfig(Code.CREDENTIAL_28)
    @Action("changeCredential")
    public CodeCheckBox c28CheckBox;

    /** 資格：高等学校教諭(情報) */
    @Location(x = 137, y = 289)
    @CodeConfig(Code.CREDENTIAL_29)
    @Action("changeCredential")
    public CodeCheckBox c29CheckBox;

    /** 資格：高等学校教諭(情報) */
    @Location(x = 310, y = 289)
    @CodeConfig(Code.CREDENTIAL_30)
    @Action("changeCredential")
    public CodeCheckBox c30CheckBox;

    /** 資格：高等学校教諭(水産) */
    @Location(x = 475, y = 289)
    @CodeConfig(Code.CREDENTIAL_31)
    @Action("changeCredential")
    public CodeCheckBox c31CheckBox;

    /** 資格：高等学校教諭(福祉) */
    @Location(x = 137, y = 311)
    @CodeConfig(Code.CREDENTIAL_32)
    @Action("changeCredential")
    public CodeCheckBox c32CheckBox;

    /** 資格：高等学校教諭(商船) */
    @Location(x = 310, y = 311)
    @CodeConfig(Code.CREDENTIAL_33)
    @Action("changeCredential")
    public CodeCheckBox c33CheckBox;

    /** 資格：高等学校教諭(商業) */
    @Location(x = 475, y = 311)
    @CodeConfig(Code.CREDENTIAL_34)
    @Action("changeCredential")
    public CodeCheckBox c34CheckBox;

    /** 資格：特別支援学校教諭 */
    @Location(x = 137, y = 343)
    @CodeConfig(Code.CREDENTIAL_35)
    @Action("changeCredential")
    public CodeCheckBox c35CheckBox;

    /** 資格：養護教諭 */
    @Location(x = 310, y = 343)
    @CodeConfig(Code.CREDENTIAL_36)
    @Action("changeCredential")
    public CodeCheckBox c36CheckBox;

    /** 資格：学校図書館司書教諭 */
    @Location(x = 475, y = 343)
    @CodeConfig(Code.CREDENTIAL_37)
    @Action("changeCredential")
    public CodeCheckBox c37CheckBox;

    /** 資格：栄養教諭 */
    @Location(x = 137, y = 365)
    @CodeConfig(Code.CREDENTIAL_38)
    @Action("changeCredential")
    public CodeCheckBox c38CheckBox;

    /** 資格：社会調査士 */
    @Location(x = 137, y = 401)
    @CodeConfig(Code.CREDENTIAL_39)
    @Action("changeCredential")
    public CodeCheckBox c39CheckBox;

    /** 資格：司書 */
    @Location(x = 310, y = 401)
    @CodeConfig(Code.CREDENTIAL_40)
    @Action("changeCredential")
    public CodeCheckBox c40CheckBox;

    /** 資格：社会教育主事 */
    @Location(x = 475, y = 401)
    @CodeConfig(Code.CREDENTIAL_41)
    @Action("changeCredential")
    public CodeCheckBox c41CheckBox;

    /** 資格：学芸員 */
    @Location(x = 137, y = 423)
    @CodeConfig(Code.CREDENTIAL_42)
    @Action("changeCredential")
    public CodeCheckBox c42CheckBox;

    /** 資格：社会福祉主事 */
    @Location(x = 310, y = 423)
    @CodeConfig(Code.CREDENTIAL_46)
    @Action("changeCredential")
    public CodeCheckBox c46CheckBox;

    /** 資格：社会福祉士 */
    @Location(x = 475, y = 423)
    @CodeConfig(Code.CREDENTIAL_44)
    @Action("changeCredential")
    public CodeCheckBox c44CheckBox;

    /** 資格：介護福祉士 */
    @Location(x = 137, y = 445)
    @CodeConfig(Code.CREDENTIAL_45)
    @Action("changeCredential")
    public CodeCheckBox c45CheckBox;

    /** 資格：認定心理士 */
    @Location(x = 310, y = 445)
    @CodeConfig(Code.CREDENTIAL_49)
    @Action("changeCredential")
    public CodeCheckBox c49CheckBox;

    /** 資格：児童福祉司 */
    @Location(x = 475, y = 445)
    @CodeConfig(Code.CREDENTIAL_47)
    @Action("changeCredential")
    public CodeCheckBox c47CheckBox;

    /** 資格：精神保健福祉士 */
    @CodeConfig(Code.CREDENTIAL_48)
    @Location(x = 137, y = 467)
    @Action("changeCredential")
    public CodeCheckBox c48CheckBox;

    /** 資格：医師 */
    @Location(x = 137, y = 507)
    @CodeConfig(Code.CREDENTIAL_50)
    @Action("changeCredential")
    public CodeCheckBox c50CheckBox;

    /** 資格：歯科医師 */
    @Location(x = 310, y = 507)
    @CodeConfig(Code.CREDENTIAL_51)
    @Action("changeCredential")
    public CodeCheckBox c51CheckBox;

    /** 資格：獣医師 */
    @Location(x = 475, y = 507)
    @CodeConfig(Code.CREDENTIAL_52)
    @Action("changeCredential")
    public CodeCheckBox c52CheckBox;

    /** 資格：薬剤師 */
    @Location(x = 137, y = 529)
    @CodeConfig(Code.CREDENTIAL_53)
    @Action("changeCredential")
    public CodeCheckBox c53CheckBox;

    /** 資格：看護師 */
    @Location(x = 310, y = 529)
    @CodeConfig(Code.CREDENTIAL_54)
    @Action("changeCredential")
    public CodeCheckBox c54CheckBox;

    /** 資格：保健師 */
    @Location(x = 475, y = 529)
    @CodeConfig(Code.CREDENTIAL_55)
    @Action("changeCredential")
    public CodeCheckBox c55CheckBox;

    /** 資格：助産師 */
    @Location(x = 137, y = 551)
    @CodeConfig(Code.CREDENTIAL_56)
    @Action("changeCredential")
    public CodeCheckBox c56CheckBox;

    /** 資格：理学療法士 */
    @Location(x = 310, y = 551)
    @CodeConfig(Code.CREDENTIAL_57)
    @Action("changeCredential")
    public CodeCheckBox c57CheckBox;

    /** 資格：作業療法士 */
    @Location(x = 475, y = 551)
    @CodeConfig(Code.CREDENTIAL_58)
    @Action("changeCredential")
    public CodeCheckBox c58CheckBox;

    /** 資格：言語聴覚士 */
    @Location(x = 137, y = 573)
    @CodeConfig(Code.CREDENTIAL_59)
    @Action("changeCredential")
    public CodeCheckBox c59CheckBox;

    /** 資格：言語聴覚士 */
    @Location(x = 310, y = 573)
    @CodeConfig(Code.CREDENTIAL_60)
    @Action("changeCredential")
    public CodeCheckBox c60CheckBox;

    /** 資格：臨床検査技師 */
    @Location(x = 475, y = 573)
    @CodeConfig(Code.CREDENTIAL_61)
    @Action("changeCredential")
    public CodeCheckBox c61CheckBox;

    /** 資格：診療放射線技師 */
    @Location(x = 137, y = 595)
    @CodeConfig(Code.CREDENTIAL_62)
    @Action("changeCredential")
    public CodeCheckBox c62CheckBox;

    /** 資格：細胞検査士 */
    @Location(x = 310, y = 595)
    @CodeConfig(Code.CREDENTIAL_63)
    @Action("changeCredential")
    public CodeCheckBox c63CheckBox;

    /** 資格：臨床工学技士 */
    @Location(x = 475, y = 595)
    @CodeConfig(Code.CREDENTIAL_64)
    @Action("changeCredential")
    public CodeCheckBox c64CheckBox;

    /** 資格：歯科衛生士 */
    @Location(x = 137, y = 617)
    @CodeConfig(Code.CREDENTIAL_65)
    @Action("changeCredential")
    public CodeCheckBox c65CheckBox;

    /** 資格：歯科技工士 */
    @Location(x = 310, y = 617)
    @CodeConfig(Code.CREDENTIAL_66)
    @Action("changeCredential")
    public CodeCheckBox c66CheckBox;

    /** 資格：はり師・きゅう師 */
    @Location(x = 475, y = 617)
    @CodeConfig(Code.CREDENTIAL_67)
    @Action("changeCredential")
    public CodeCheckBox c67CheckBox;

    /** 資格：救急救命士 */
    @Location(x = 137, y = 639)
    @CodeConfig(Code.CREDENTIAL_68)
    @Action("changeCredential")
    public CodeCheckBox c68CheckBox;

    /** 資格：健康運動実践指導者 */
    @Location(x = 310, y = 639)
    @CodeConfig(Code.CREDENTIAL_69)
    @Action("changeCredential")
    public CodeCheckBox c69CheckBox;

    /** 資格：柔道整復師 */
    @Location(x = 137, y = 661)
    @CodeConfig(Code.CREDENTIAL_82)
    @Action("changeCredential")
    public CodeCheckBox c82CheckBox;

    /** 資格：測量士 */
    @Location(x = 137, y = 701)
    @CodeConfig(Code.CREDENTIAL_70)
    @Action("changeCredential")
    public CodeCheckBox c70CheckBox;

    /** 資格：測量士補 */
    @Location(x = 310, y = 701)
    @CodeConfig(Code.CREDENTIAL_71)
    @Action("changeCredential")
    public CodeCheckBox c71CheckBox;

    /** 資格：一級建築士 */
    @Location(x = 475, y = 701)
    @CodeConfig(Code.CREDENTIAL_72)
    @Action("changeCredential")
    public CodeCheckBox c72CheckBox;

    /** 資格：二級建築士 */
    @Location(x = 137, y = 723)
    @CodeConfig(Code.CREDENTIAL_73)
    @Action("changeCredential")
    public CodeCheckBox c73CheckBox;

    /** 資格：木造建築士 */
    @Location(x = 310, y = 723)
    @CodeConfig(Code.CREDENTIAL_74)
    @Action("changeCredential")
    public CodeCheckBox c74CheckBox;

    /** 資格：技術士補 */
    @Location(x = 475, y = 723)
    @CodeConfig(Code.CREDENTIAL_75)
    @Action("changeCredential")
    public CodeCheckBox c75CheckBox;

    /** 資格：操縦士 */
    @Location(x = 137, y = 744)
    @CodeConfig(Code.CREDENTIAL_83)
    @Action("changeCredential")
    public CodeCheckBox c83CheckBox;

    /** 資格：バイオ技術者認定試験 */
    @Location(x = 137, y = 784)
    @CodeConfig(Code.CREDENTIAL_76)
    @Action("changeCredential")
    public CodeCheckBox c76CheckBox;

    /** 資格：食品衛生監視員 */
    @Location(x = 310, y = 784)
    @CodeConfig(Code.CREDENTIAL_77)
    @Action("changeCredential")
    public CodeCheckBox c77CheckBox;

    /** 資格：管理栄養士 */
    @Location(x = 475, y = 784)
    @CodeConfig(Code.CREDENTIAL_78)
    @Action("changeCredential")
    public CodeCheckBox c78CheckBox;

    /** 資格：栄養士 */
    @Location(x = 137, y = 805)
    @CodeConfig(Code.CREDENTIAL_79)
    @Action("changeCredential")
    public CodeCheckBox c79CheckBox;

    /** 資格：フードスペシャリスト */
    @Location(x = 310, y = 805)
    @CodeConfig(Code.CREDENTIAL_80)
    @Action("changeCredential")
    public CodeCheckBox c80CheckBox;

    /** 資格：衣料管理士 */
    @Location(x = 475, y = 805)
    @CodeConfig(Code.CREDENTIAL_81)
    @Action("changeCredential")
    public CodeCheckBox c81CheckBox;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
        cc2CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        cc4CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        cc5CheckBox.setVerticalTextPosition(JCheckBox.TOP);
    }

}

package jp.ac.kawai_juku.banzaisystem.systop.strt.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 起動画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopStrtView extends AbstractView {

    /** 終了ボタン */
    @Location(x = 464, y = 2)
    public ImageButton exitButton;

    /** 先生用ボタン */
    @Location(x = 56, y = 150)
    public ImageButton teacherButton;

    /** 生徒用ボタン */
    @Location(x = 290, y = 150)
    public ImageButton studentButton;

    /** 「モードを選択してください。」メッセージ */
    @Location(x = 66, y = 285)
    public TextLabel selectModeLabel;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
    }

}

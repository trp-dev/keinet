package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 * õÊõDao
 *
 * @author TOTEC)OZEKI.Hiroshige
 */
public class ExmntnRsltSearhDao extends BaseDao {

    /**
     * s¹{§}X^õ
     *
     * @param preCdList s¹{§õð
     * @return s¹{§}X^¼ÌXg
     */
    public List<String> selectPreNameList(List<String> preCdList) {

        BeanMap param = new BeanMap();
        param.put("preCd", preCdList);

        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }

    /**
     * nû}X^õ
     *
     * @param distCdList nûõð
     * @return nû}X^¼ÌXg
     */
    public List<String> selectDistNameList(List<String> distCdList) {

        BeanMap param = new BeanMap();
        param.put("distCd", distCdList);

        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.result.clnd.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 入試結果入力-大学指定(カレンダー)画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultClndDao extends BaseDao {

    /**
     * 合格発表日リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @return 合格発表日リスト
     */
    public List<ResultUnivBean> selectSucAnnDateList(Integer grade, String cls) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        return jdbcManager.selectBySqlFile(ResultUnivBean.class, getSqlPath(), param).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.helper.ExmntnScdlHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.maker.ExmntnScdlA0601Maker;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;

/**
 *
 * 受験スケジュール画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnScdlController extends SubController {

    /** ヘルパー */
    @Resource(name = "exmntnScdlHelper")
    private ExmntnScdlHelper helper;

    @Override
    @Logging(GamenId.C5)
    public void activatedAction() {
        helper.displayTable();
    }

    /**
     * ↑ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult upButtonAction() {
        helper.changeCandidateRank(true);
        return null;
    }

    /**
     * ↓ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult downButtonAction() {
        helper.changeCandidateRank(false);
        return null;
    }

    /**
     * 削除ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult deleteButtonAction() {
        helper.deleteButtonAction();
        return null;
    }

    /**
     * 詳細判定ボタンアクションです。
     *
     * @return なし（コントローラからは画面遷移しない。画面遷移はヘルパー内で行う。）
     */
    @Execute
    public ExecuteResult detailJudgeButtonAction() {
        helper.detailJudgeButtonAction();
        return null;
    }

    /**
     * 並べ替えコンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult sortComboBoxAction() {
        helper.sortComboBoxAction();
        return null;
    }

    /**
     * 印刷ボタン押下アクションです。
     *
     * @return 受験スケジュール画面
     */
    @Execute
    public ExecuteResult printButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C5).addCreator(ExmntnScdlA0601Maker.class, helper.createA0601InBean())
                        .launch();
                return null;
            }
        };
    }

}

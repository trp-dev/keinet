package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * 画像を背景に持つパネルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel {

    /** 背景画像 */
    private BufferedImage img;

    /**
     * コンストラクタです。
     *
     * @param clazz 背景画像読み込み位置の基準クラス
     */
    public ImagePanel(Class<?> clazz) {
        this(clazz, "bg.png");
    }

    /**
     * コンストラクタです。
     *
     * @param clazz 背景画像読み込み位置の基準クラス
     * @param name 背景画像ファイル名
     */
    public ImagePanel(Class<?> clazz, String name) {
        this(ImageUtil.readImage(clazz, name));
    }

    /**
     * コンストラクタです。
     *
     * @param img 背景画像
     */
    public ImagePanel(BufferedImage img) {

        /* 絶対位置レイアウトにする */
        super(null);

        this.img = img;

        Dimension size = new Dimension(img.getWidth(), img.getHeight());
        setSize(size);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);
    }

    /**
     * イメージをセットします。
     *
     * @param clazz クラス
     * @param name イメージファイル名
     */
    public void setImage(Class<?> clazz, String name) {
        BufferedImage image = ImageUtil.readImage(clazz, name);

        this.img = image;

        Dimension size = new Dimension(image.getWidth(), image.getHeight());
        setSize(size);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);

    }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

}

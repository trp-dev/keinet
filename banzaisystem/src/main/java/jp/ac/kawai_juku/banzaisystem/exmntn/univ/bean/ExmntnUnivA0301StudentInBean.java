package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

/**
 *
 * 帳票生成サービス（簡易個人表）の生徒一人分の情報を保持する入力Beanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivA0301StudentInBean implements ExcelMakerInBean {

    /** 生徒情報Bean */
    private final ExmntnUnivStudentBean studentBean;

    /** 成績データ */
    private final ScoreBean scoreBean;

    /** 志望大学情報リスト */
    private final List<ExmntnMainCandidateUnivBean> candidateUnivList;

    /** 帳票・CSV出力画面呼出しフラグ */
    private final Boolean csvDispFlg;

    /**
     * コンストラクタです。
     *
     * @param studentBean 生徒情報Bean
     * @param scoreBean 成績データ
     * @param candidateUnivList 志望大学詳細リスト
     * @param csvFlg 帳票・CSV出力画面呼出しフラグ
     */
    public ExmntnUnivA0301StudentInBean(ExmntnUnivStudentBean studentBean, ScoreBean scoreBean, List<ExmntnMainCandidateUnivBean> candidateUnivList, Boolean csvFlg) {
        this.studentBean = studentBean;
        this.scoreBean = scoreBean;
        this.candidateUnivList = candidateUnivList;
        this.csvDispFlg = csvFlg;
    }

    /**
     * 生徒情報Beanを返します。
     *
     * @return 生徒情報Bean
     */
    public ExmntnUnivStudentBean getStudentBean() {
        return studentBean;
    }

    /**
     * 成績データを返します。
     *
     * @return 成績データ
     */
    public ScoreBean getScoreBean() {
        return scoreBean;
    }

    /**
     * 志望大学情報リストを返します。
     *
     * @return 志望大学情報リスト
     */
    public List<ExmntnMainCandidateUnivBean> getCandidateUnivList() {
        return candidateUnivList;
    }

    /**
     * 帳票・CSV出力画面呼出しフラグを返します。
     *
     * @return 帳票・CSV出力画面呼出しフラグ
     */
    public Boolean getCsvdispFlg() {
        return csvDispFlg;
    }

}

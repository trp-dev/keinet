package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 画像ラベルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ImageLabel extends JLabel implements BZComponent {

    /** フィールド名の接尾辞 */
    private static final String SUFFIX = "Label";

    @Override
    public void initialize(Field field, AbstractView view) {
        if (!field.getName().endsWith(SUFFIX)) {
            throw new RuntimeException(String.format("%sのフィールド名は[***%s]とする必要があります。[%s]", getClass().getSimpleName(),
                    SUFFIX, field.getName()));
        }
        initialize(view, field.getName());
    }

    /**
     * ラベルを初期化します。
     *
     * @param view ラベルを配置する画面のView
     * @param name ラベル名
     */
    public void initialize(AbstractView view, String name) {
        String imageName = name.substring(0, name.length() - SUFFIX.length());
        BufferedImage img = ImageUtil.readImage(view.getClass(), "label/" + imageName + ".png");
        setSize(img.getWidth(), img.getHeight());
        setIcon(new ImageIcon(img));
    }

}

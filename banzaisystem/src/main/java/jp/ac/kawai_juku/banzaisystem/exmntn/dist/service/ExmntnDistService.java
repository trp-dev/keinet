package jp.ac.kawai_juku.banzaisystem.exmntn.dist.service;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_C;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_D;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCapacityBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistNumbersBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.dao.ExmntnDistDao;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 志望大学学力分布画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistService {

    /** [***以上]フォーマット */
    private static final String FMT_ABOVE = "%s以上";

    /** [***〜]フォーマット */
    private static final String FMT_FROM = "%s〜";

    /** [***以下]フォーマット */
    private static final String FMT_BELOW = "%s以下";

    /** ボーダーの表示フォーマット */
    private static final String FMT_BORDER = "ﾎﾞ %s";

    /** 人数の無効値 */
    private static final int INVALID_NUMBERS = 9999;

    /** 帯の数 */
    private static final int DIST_NUM = 19;

    /** 目盛りの最大数 */
    private static final int SCALE_MAX = 20;

    /** DAO */
    @Resource
    private ExmntnDistDao exmntnDistDao;

    /**
     * 募集定員情報を取得します。
     *
     * @param univKeyBean {@link UnivKeyBean}
     * @return {@link ExmntnDistCapacityBean}
     */
    public ExmntnDistCapacityBean findCapacityBean(UnivKeyBean univKeyBean) {
        return exmntnDistDao.selectCapacityBean(univKeyBean);
    }

    /**
     * 学力分布情報を取得します。
     *
     * @param exam 対象模試
     * @param scoreBean 成績
     * @param univKeyBean {@link UnivKeyBean}
     * @return {@link ExmntnDistBean}
     */
    public ExmntnDistBean findDistBean(ExamBean exam, ScoreBean scoreBean, UnivKeyBean univKeyBean) {

        if (exam == null) {
            /* 対象模試が無ければここまで */
            return new ExmntnDistBean();
        }

        /* 判定を実行する */
        JudgedResult result = judge(exam, scoreBean, univKeyBean);

        /* 志望大学学力分布を取得する */
        BeanMap dist = exmntnDistDao.selectUnivDistRecord(exam, univKeyBean);

        ExmntnDistBean bean = new ExmntnDistBean();

        if (dist != null) {
            /* 入試満点値 */
            Integer fullPnt = (Integer) dist.get("fullpnt");
            /* 刻み幅 */
            Integer stepSize = (Integer) dist.get("stepsize");
            /* 志望集計パターンを設定する */
            bean.setPatternCd((String) dist.get("patterncd"));
            /* 入試満点値を設定する */
            bean.setFullPnt(fullPnt);
            /* ボーダーを設定する */
            String border;
            if (fullPnt == null) {
                if (result.getSCLine().isEmpty()) {
                    /* ランクコード15と99は、ボーダーランク表示を詳細画面に合わせる */
                    if (result.getRank() == Univ.RANK_FORCE) {
                        border = Univ.RANK_STR_NONE;
                    } else {
                        border = "--";
                    }
                } else {
                    border = result.getSCLine();
                }
            } else {
                border = result.getBorderPointStr();
            }
            bean.setBorder(String.format(FMT_BORDER, border));
            /* 人数リストを生成する */
            List<Integer> numbersList = createNumbersList(dist);
            /* １目盛りあたりの人数を算出する */
            int scale = calcScale(numbersList);
            /* １目盛りあたりの人数を設定する */
            bean.setScale(scale);
            /* 帯別人数リストを設定する */
            bean.setNumbersList(createNumbersList(numbersList, result, fullPnt, stepSize, scale));
            /* 志願者内訳(総志望)を設定する */
            bean.setDetailTotal(exmntnDistDao.selectCandidateDetail(exam, univKeyBean, Code.CANDIDATE_COUNT_PTN_T));
            /* 志願者内訳(出願予定)を設定する */
            bean.setDetailPlan(exmntnDistDao.selectCandidateDetail(exam, univKeyBean, Code.CANDIDATE_COUNT_PTN_P));
            /* 偏差値帯フラグを設定する */
            bean.setDeviationDistFlg(fullPnt == null);
        } else {
            /* 私大は志願者内訳(出願予定)をnull(ハイフン表示)とする */
            if (result.getScheduleCd().equals(Univ.SCHEDULE_CNT) || result.getScheduleCd().equals(Univ.SCHEDULE_NML)) {
                bean.setDetailPlan(null);
            }
            /* 一般私大は偏差値帯を表示する */
            if (result.getScheduleCd().equals(Univ.SCHEDULE_NML)) {
                bean.setDeviationDistFlg(true);
            }
        }

        return bean;
    }

    /**
     * 対象模試の成績で判定を実行します。
     *
     * @param exam 対象模試
     * @param scoreBean 成績
     * @param univKeyBean {@link UnivKeyBean}
     * @return 判定結果
     */
    @SuppressWarnings("unchecked")
    private JudgedResult judge(ExamBean exam, ScoreBean scoreBean, UnivKeyBean univKeyBean) {
        ScoreBean newScoreBean;
        List<EngSSScoreData> cefrList = scoreBean.getScore().getEngSSScoreDatas();
        if (ExamUtil.isMark(exam)) {
            newScoreBean = new ScoreBean(exam, scoreBean.getScore().getMark(), null, null, cefrList);
        } else {
            newScoreBean = new ScoreBean(null, null, exam, scoreBean.getScore().getWrtn(), cefrList);
        }
        /*
        if (ExamUtil.isCenter(exam)) {
            return BZJudgementExecutor.getInstance().judge(newScoreBean, univKeyBean, true);
        } else {
            return BZJudgementExecutor.getInstance().judge(newScoreBean, univKeyBean, false);
        }
        */
        return BZJudgementExecutor.getInstance().judge(newScoreBean, univKeyBean);
    }

    /**
     * 人数リストを生成します。
     *
     * @param dist 志望大学学力分布情報
     * @return 人数リスト
     */
    private List<Integer> createNumbersList(BeanMap dist) {
        List<Integer> numbersList = CollectionsUtil.newArrayList(DIST_NUM);
        for (int i = DIST_NUM; i > 0; i--) {
            Integer numbers = (Integer) dist.get("numbers" + i);
            if (numbers.intValue() != INVALID_NUMBERS) {
                numbersList.add(numbers);
            }
        }
        return numbersList;
    }

    /**
     * １目盛りあたりの人数を算出します。
     *
     * @param numbersList 人数リスト
     * @return １目盛りあたりの人数
     */
    private int calcScale(List<Integer> numbersList) {
        int max = 0;
        int limit = numbersList.size() - 1;
        for (int i = 0; i < limit; i++) {
            /* 最下層を除く最大の人数を取得する */
            max = Math.max(max, numbersList.get(i).intValue());
        }
        return (int) Math.ceil((double) max / SCALE_MAX);
    }

    /**
     * 帯別人数リストを生成します。
     *
     * @param numbersList 人数リスト
     * @param result 判定結果
     * @param fullPnt 入試満点値
     * @param stepSize 刻み幅
     * @param scale １目盛りあたりの人数
     * @return 帯別人数リスト
     */
    private List<ExmntnDistNumbersBean> createNumbersList(List<Integer> numbersList, JudgedResult result, Integer fullPnt, Integer stepSize, int scale) {

        int total = 0;
        List<ExmntnDistNumbersBean> list = CollectionsUtil.newArrayList(numbersList.size());
        for (Integer numbers : numbersList) {
            total += numbers.intValue();
            ExmntnDistNumbersBean bean = new ExmntnDistNumbersBean();
            bean.setNumbers(numbers.intValue());
            bean.setTotal(total);
            bean.setScaleNum(Math.min((int) Math.ceil((double) numbers.intValue() / scale), SCALE_MAX));
            list.add(bean);
        }

        List<Pair<BigDecimal, BigDecimal>> rangeList = createRangeList(list, fullPnt, stepSize);

        /* 帯ヘッダを設定する */
        setDistHeader(list, rangeList, fullPnt);

        /* 評価基準を設定する */
        if (fullPnt == null) {
            setStandard(list, rangeList, JUDGE_STR_A, result.getSALine());
            setStandard(list, rangeList, JUDGE_STR_B, result.getSBLine());
            setStandard(list, rangeList, JUDGE_STR_C, result.getSCLine());
            setStandard(list, rangeList, JUDGE_STR_D, result.getSDLine());
        } else {
            setStandard(list, rangeList, JUDGE_STR_A, result.getCALine());
            setStandard(list, rangeList, JUDGE_STR_B, result.getCBLine());
            setStandard(list, rangeList, JUDGE_STR_C, result.getCCLine());
            setStandard(list, rangeList, JUDGE_STR_D, result.getCDLine());
        }

        /* 強調表示フラグを設定する */
        setEmphasisFlg(list, rangeList, result, fullPnt);

        return list;
    }

    /**
     * 得点／偏差値範囲リストを返します。
     *
     * @param list 帯別人数リスト
     * @param fullPnt 入試満点値
     * @param stepSize 刻み幅
     * @return 得点／偏差値範囲リスト
     */
    private List<Pair<BigDecimal, BigDecimal>> createRangeList(List<ExmntnDistNumbersBean> list, Integer fullPnt, Integer stepSize) {

        List<Pair<BigDecimal, BigDecimal>> rangeList = CollectionsUtil.newArrayList(list.size());

        /* 開始値を初期化する */
        BigDecimal start;
        if (fullPnt == null) {
            start = BigDecimal.valueOf(75.0d);
        } else {
            start = BigDecimal.valueOf(fullPnt.intValue() - stepSize.intValue());
        }

        /* 刻み幅を初期化する */
        BigDecimal step;
        if (stepSize == null) {
            step = BigDecimal.valueOf(2.5d);
        } else {
            step = BigDecimal.valueOf(stepSize);
        }

        /* 最上位を詰める(上限値をNULLとする) */
        rangeList.add(Pair.of(start, (BigDecimal) null));

        /* 最下位直前まで詰める */
        for (int i = 2; i < list.size(); i++) {
            BigDecimal end = start;
            start = start.subtract(step);
            rangeList.add(Pair.of(start, end));
        }

        /* 最下位を詰める(下限値をNULLとする) */
        rangeList.add(Pair.of((BigDecimal) null, start));

        return rangeList;
    }

    /**
     * 帯ヘッダを設定します。
     *
     * @param list 帯別人数リスト
     * @param rangeList 得点／偏差値範囲リスト
     * @param fullPnt 入試満点値
     */
    private void setDistHeader(List<ExmntnDistNumbersBean> list, List<Pair<BigDecimal, BigDecimal>> rangeList, Integer fullPnt) {
        for (int i = 0; i < list.size(); i++) {
            ExmntnDistNumbersBean bean = list.get(i);
            Pair<BigDecimal, BigDecimal> range = rangeList.get(i);
            String score;
            if (fullPnt == null) {
                /* 偏差値帯の場合 */
                if (range.getLeft() == null) {
                    score = Double.toString(range.getRight().subtract(BigDecimal.valueOf(0.1d)).doubleValue());
                } else {
                    score = Double.toString(range.getLeft().doubleValue());
                }
            } else {
                /* 得点帯の場合 */
                if (range.getLeft() == null) {
                    score = Integer.toString(range.getRight().subtract(BigDecimal.valueOf(1)).intValue());
                } else {
                    score = Integer.toString(range.getLeft().intValue());
                }
            }
            if (i == 0) {
                bean.setDistHeader(String.format(FMT_ABOVE, score));
            } else if (i + 1 == list.size()) {
                bean.setDistHeader(String.format(FMT_BELOW, score));
            } else {
                bean.setDistHeader(String.format(FMT_FROM, score));
            }
        }
    }

    /**
     * 評価基準を設定します。
     *
     * @param list 帯別人数リスト
     * @param rangeList 得点／偏差値範囲リスト
     * @param label 評価基準ラベル
     * @param value 評価基準値
     */
    private void setStandard(List<ExmntnDistNumbersBean> list, List<Pair<BigDecimal, BigDecimal>> rangeList, String label, String value) {
        if (!StringUtils.isEmpty(value)) {
            int index = findInRangeIndex(rangeList, new BigDecimal(value));
            if (index > -1) {
                list.get(index).setStandard(label + " " + value);
            }
        }
    }

    /**
     * 強調表示フラグを設定します。
     *
     * @param list 帯別人数リスト
     * @param rangeList 得点／偏差値範囲リスト
     * @param result 判定結果
     * @param fullPnt 入試満点値
     */
    private void setEmphasisFlg(List<ExmntnDistNumbersBean> list, List<Pair<BigDecimal, BigDecimal>> rangeList, JudgedResult result, Integer fullPnt) {

        String score = fullPnt == null ? result.getS_scoreStr() : result.getC_scoreStr();
        if (score.isEmpty()) {
            /* 未受験なら設定しない */
            return;
        }

        int index = findInRangeIndex(rangeList, new BigDecimal(score));

        ExmntnDistNumbersBean bean = list.get(index);
        bean.setEmphasisFlg(true);
        if (bean.getScaleNum() == 0) {
            bean.setScaleNum(1);
        }
    }

    /**
     * 指定した基準値が得点／偏差値範囲に収まっている位置を返します。
     *
     * @param rangeList 得点／偏差値範囲リスト
     * @param value 基準値
     * @return リスト内の位置
     */
    private int findInRangeIndex(List<Pair<BigDecimal, BigDecimal>> rangeList, BigDecimal value) {
        for (int i = 0; i < rangeList.size(); i++) {
            Pair<BigDecimal, BigDecimal> range = rangeList.get(i);
            if (range.getLeft() != null && range.getLeft().compareTo(value) > 0) {
                continue;
            }
            if (range.getRight() != null && range.getRight().compareTo(value) <= 0) {
                continue;
            }
            return i;
        }
        return -1;
    }

}

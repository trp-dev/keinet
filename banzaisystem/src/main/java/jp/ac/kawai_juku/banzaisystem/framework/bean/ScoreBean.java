package jp.ac.kawai_juku.banzaisystem.framework.bean;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONE;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.beans.score.ExtScore;
import jp.co.fj.kawaijuku.judgement.beans.score.Score;
import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 成績データ＋選択科目情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ScoreBean {

    /** 成績データ */
    private final Score score;

    /** マーク模試 */
    private final ExamBean markExam;

    /** 記述模試 */
    private final ExamBean writtenExam;

    /**
     * コンストラクタです。
     *
     * @param markExam マーク模試
     * @param markList マーク模試科目成績リスト
     * @param writtenExam 記述模試
     * @param writtenList 記述模試科目成績リスト
     * @param cefrList 英語認定試験成績リスト
     */
    public ScoreBean(ExamBean markExam, List<? extends SubjectRecord> markList, ExamBean writtenExam, List<? extends SubjectRecord> writtenList, List<EngSSScoreData> cefrList) {
        this.markExam = markExam;
        this.writtenExam = writtenExam;
        this.score = createScore(markExam, markList, writtenExam, writtenList);
        this.score.setEngSSScoreDatas(cefrList);
    }

    /**
     * 成績データを生成します。
     *
     * @param markExam マーク模試
     * @param markList マーク模試科目成績リスト
     * @param writtenExam 記述模試
     * @param writtenList 記述模試科目成績リスト
     * @return 成績データ
     */
    private Score createScore(ExamBean markExam, List<? extends SubjectRecord> markList, ExamBean writtenExam, List<? extends SubjectRecord> writtenList) {
        try {
            ExtScore s = new ExtScore(ExamUtil.toExamination(markExam), filter(markList), ExamUtil.toExamination(writtenExam), filter(writtenList), findAllMathOneBean(markExam));
            s.execute();
            return s;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 数学�@の全国成績を返します。
     *
     * @param markExam マーク模試
     * @return {@link SubRecordAllBean}
     */
    private SubRecordAllBean findAllMathOneBean(ExamBean markExam) {
        if (markExam == null) {
            return null;
        } else {
            SystemDto systemDto = getComponent(SystemDto.class);
            return systemDto.getSubrecordAllMap().get(markExam.getExamCd()).get(SUBCODE_CENTER_DEFAULT_MATHONE);
        }
    }

    /**
     * 科目成績リストに対して以下のフィルタ処理を行います。
     * <ul>
     * <li>型の成績を削除する。</li>
     * <li>空のリストはnullに変換する。</li>
     * </ul>
     *
     * @param list 科目成績リスト
     * @return 変換した科目成績リスト
     */
    private List<? extends SubjectRecord> filter(List<? extends SubjectRecord> list) {
        if (list == null) {
            return null;
        } else {
            List<SubjectRecord> newList = CollectionsUtil.newArrayList(list.size());
            for (SubjectRecord rec : list) {
                if (rec.getSubCd().compareTo("7000") < 0) {
                    newList.add(rec);
                }
            }
            return newList.isEmpty() ? null : newList;
        }
    }

    /**
     * 成績データを返します。
     *
     * @return 成績データ
     */
    public Score getScore() {
        return score;
    }

    /**
     * マーク模試を返します。
     *
     * @return マーク模試
     */
    public ExamBean getMarkExam() {
        return markExam;
    }

    /**
     * 記述模試を返します。
     *
     * @return 記述模試
     */
    public ExamBean getWrittenExam() {
        return writtenExam;
    }

    /**
     * 成績が設定されているかを返します。
     *
     * @return 成績が設定されているならtrue
     */
    public boolean isEntered() {
        return score.getCenter().isEntered() || score.getSecond().isEntered();
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.mstr.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellEditor;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.bean.SystopLginDataStatusBean;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.component.SystopLginUnivCellRenderer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;

/**
 *
 * 大学マスタテーブルです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
@SuppressWarnings("serial")
public class MainteMstrTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** カラム幅定義 */
    private static final int[] COLUMN_WIDTH = { 265, 69, 122 };

    /**
     * コンストラクタです。
     */
    public MainteMstrTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        JTable table = new BZTable(new ReadOnlyTableModel(3)) {

            @Override
            public boolean isCellEditable(int row, int column) {
                /* 対象選択列のみ編集可能とする */
                return column == 1;
            }

            @Override
            public void setValueAt(Object value, int row, int column) {
                if (column == 1 && value != null && ((Boolean) value).booleanValue()) {
                    for (int i = 0; i < getRowCount(); i++) {
                        if (getValueAt(i, column) != null) {
                            /* ラジオで選択されている行のみをtrueにする */
                            super.setValueAt(Boolean.valueOf(i == row), i, column);
                        }
                    }
                }
            }

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                if (c instanceof JLabel) {
                    JLabel label = (JLabel) c;
                    label.setBorder(column == 0 ? new EmptyBorder(0, 10, 0, 0) : null);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int width : COLUMN_WIDTH) {
                    x += width;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(31);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setRowSelectionAllowed(false);

        /* 列の幅をヘッダの列と合わせる */
        TableColumnModel columnModel = table.getColumnModel();
        for (int i = 0; i < COLUMN_WIDTH.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(COLUMN_WIDTH[i]);
        }

        /* 対象選択列のセルレンダラをセットする */
        columnModel.getColumn(1).setCellRenderer(new RadioButtonCellRenderer());

        /* 対象選択列のセルエディタをセットする */
        columnModel.getColumn(1).setCellEditor(new RadioButtonCellEditor(SEPARATOR_COLOR));

        /* 状況列のセルレンダラをセットする */
        columnModel.getColumn(2).setCellRenderer(new SystopLginUnivCellRenderer());

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public final void setDataList(Vector<Vector<Object>> dataList) {
        ((BZTableModel) getTable().getModel()).setDataVector(dataList);
    }

    /**
     * 選択中の大学マスタを返します。
     *
     * @return 選択中の大学マスタ（年度4桁＋模試区分2桁）
     */
    public final String[] getSelectedMaster() {
        BZTableModel model = (BZTableModel) getTable().getModel();
        for (Object obj : model.getDataVector()) {
            Vector<?> row = (Vector<?>) obj;
            SystopLginDataStatusBean bean = (SystopLginDataStatusBean) ((CellData) row.get(0)).getSortValue();
            if (BooleanUtils.isTrue((Boolean) row.get(1))) {
                return new String[] { bean.getExamYear(), bean.getExamDiv() };
            }
        }
        return null;
    }

    /**
     * 更新可の模試があるかどうか返却します。
     *
     * @return true:あり、false:なし
     */
    public final boolean isUpdatableMaster() {
        boolean result = false;
        BZTableModel model = (BZTableModel) getTable().getModel();
        for (Object obj : model.getDataVector()) {
            Vector<?> row = (Vector<?>) obj;
            SystopLginDataStatusBean bean = (SystopLginDataStatusBean) ((CellData) row.get(0)).getSortValue();
            if (Code.UNIV_DL_AVAILABLE.getValue().equals(bean.getUnivStatus())) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * すべて更新済かどうか返却します。
     *
     * @return true:更新済、false:未更新あり
     */
    public final boolean isAllUpdatedMaster() {
        boolean result = true;
        BZTableModel model = (BZTableModel) getTable().getModel();
        for (Object obj : model.getDataVector()) {
            Vector<?> row = (Vector<?>) obj;
            SystopLginDataStatusBean bean = (SystopLginDataStatusBean) ((CellData) row.get(0)).getSortValue();
            if (!Code.UNIV_DL_DONE.getValue().equals(bean.getUnivStatus())) {
                result = false;
                break;
            }
        }
        return result;
    }
}

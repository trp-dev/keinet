package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 志望大学情報を保持するBeanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainCandidateUnivBean extends JudgementResultBean {

    /** 大学名 */
    private String univName;

    /** 学部名  */
    private String facultyName;

    /** 学科名  */
    private String deptName;

    /** 区分 */
    private String section;

    /** 受験予定大学フラグ */
    private String planUnivFlg;

    /**
     * コンストラクタです。
     */
    public ExmntnMainCandidateUnivBean() {
    }

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public ExmntnMainCandidateUnivBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 志望大学リスト：区分を返します。
     *
     * @return 志望大学リスト：区分
     */
    public String getSection() {
        return section;
    }

    /**
     * 志望大学リスト：区分をセットします。
     *
     * @param section 志望大学リスト：区分
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 受験予定大学フラグを返します。
     *
     * @return 受験予定大学フラグ
     */
    public String getPlanUnivFlg() {
        return planUnivFlg;
    }

    /**
     * 受験予定大学フラグをセットします。
     *
     * @param planUnivFlg 受験予定大学フラグ
     */
    public void setPlanUnivFlg(String planUnivFlg) {
        this.planUnivFlg = planUnivFlg;
    }

}

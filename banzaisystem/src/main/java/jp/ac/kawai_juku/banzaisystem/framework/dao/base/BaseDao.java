package jp.ac.kawai_juku.banzaisystem.framework.dao.base;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.framework.log.Logger;

/**
 *
 * バンザイシステムの基底DAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseDao {

    /** Logger */
    private static final Logger logger = Logger.getLogger(BaseDao.class);

    /** 画面ID: フレームワーク */
    private static final String SCRID_FRAMEWORK = "framework";

    /** JdbcManager（大学マスタ） */
    @Resource
    protected JdbcManager jdbcManager;

    /**
     * 実行中のクラス名とメソッド名からSQLファイルパスを生成して返します。<br>
     * <br>
     * <b>フレームワークパッケージの生成ルール</b><br>
     * クラス: jp.ac.kawai_juku.banzaisystem.framework.dao.ExampleDao<br>
     * メソッド名: selectExampleList<br>
     * 生成パス: sql/framework/frameworkSelectExampleList.sql<br>
     * <br>
     * <b>機能別パッケージの生成ルール</b><br>
     * クラス: jp.ac.kawai_juku.banzaisystem.[機能ID].[画面ID].dao.ExampleDao<br>
     * メソッド名: selectExampleList<br>
     * 生成パス: sql/[機能ID]/[画面ID]SelectExampleList.sql
     *
     * @return SQLファイルパス
     */
    protected final String getSqlPath() {
        StackTraceElement element = new Throwable().getStackTrace()[1];
        String[] splitedClassName = getSplitedClassName(element);
        String methodName = element.getMethodName();
        return createSqlPath(splitedClassName, methodName);
    }

    /**
     * StackTraceElementから分割された完全修飾クラス名を取得します。
     *
     * @param element 評価するStackTraceElement
     * @return 分割された完全修飾クラス名
     */
    private String[] getSplitedClassName(StackTraceElement element) {

        String[] splitedClassName = StringUtils.split(element.getClassName(), '.');

        if (splitedClassName.length < 4) {
            throw new RuntimeException("パッケージ名が不正です。");
        }

        if (!"dao".equals(splitedClassName[splitedClassName.length - 2])) {
            throw new RuntimeException("DAOがdaoパッケージに置かれていないのでファイル名を決定できません。");
        }

        return splitedClassName;
    }

    /**
     * SQLファイルパスを生成します。
     *
     * @param splitedClassName 分割された完全修飾クラス名
     * @param methodName メソッド名
     * @return SQLファイルパス
     */
    private String createSqlPath(String[] splitedClassName, String methodName) {

        /* 画面IDを判定する */
        String screenId = splitedClassName[splitedClassName.length - 3];

        String path;
        if (SCRID_FRAMEWORK.equals(screenId)) {
            /* frameworkパッケージのパスを生成する */
            path = createFrameworkSqlPath(methodName);
        } else {
            /* 機能別パッケージのパスを生成する */
            path = createFunctionSqlPath(splitedClassName, methodName);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("SQLファイルパス=" + path);
        }

        return path;
    }

    /**
     * フレームワークパッケージのSQLファイルパスを生成します。
     *
     * @param methodName メソッド名
     * @return SQLファイルパス
     */
    private String createFrameworkSqlPath(String methodName) {
        return "sql/" + SCRID_FRAMEWORK + "/" + SCRID_FRAMEWORK + StringUtils.capitalize(methodName) + ".sql";
    }

    /**
     * 機能別パッケージのSQLファイルパスを生成します。
     *
     * @param splitedClassName 分割された完全修飾クラス名
     * @param methodName メソッド名
     * @return SQLファイルパス
     */
    private String createFunctionSqlPath(String[] splitedClassName, String methodName) {
        String functionId = splitedClassName[splitedClassName.length - 4];
        String screenId = splitedClassName[splitedClassName.length - 3];
        return "sql/" + functionId + "/" + screenId + StringUtils.capitalize(methodName) + ".sql";
    }

}

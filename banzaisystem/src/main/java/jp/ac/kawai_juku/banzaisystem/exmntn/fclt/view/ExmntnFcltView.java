package jp.ac.kawai_juku.banzaisystem.exmntn.fclt.view;

import java.util.Collections;
import java.util.Set;

import javax.swing.JCheckBox;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.annotation.Academic;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.component.ExmntnFcltCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索（学部系統）画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnFcltView extends SubView {

    /** 中系統(文系)：文・人文 */
    @Location(x = 13, y = 73)
    @Academic
    @CodeConfig(Code.M_STEMMA_11)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms11CheckBox;

    /** 小系統(文系)：文学 */
    @Location(x = 176, y = 74)
    @Academic
    @CodeConfig(Code.S_STEMMA_01)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss01CheckBox;

    /** 分野(文系)：日本文 */
    @Location(x = 313, y = 74)
    @Academic
    @CodeConfig(Code.REGION_0100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0100CheckBox;

    /** 分野(文系)：外国文 */
    @Location(x = 388, y = 74)
    @Academic
    @CodeConfig(Code.REGION_0200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0200CheckBox;

    /** 小系統(文系)：外国語 */
    @Location(x = 176, y = 96)
    @Academic
    @CodeConfig(Code.S_STEMMA_02)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss02CheckBox;

    /** 小系統(文系)：哲・史・教・心 */
    @Location(x = 176, y = 118)
    @Academic
    @CodeConfig(Code.S_STEMMA_03)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss03CheckBox;

    /** 分野(文系)：哲・倫理・宗教 */
    @Location(x = 313, y = 118)
    @Academic
    @CodeConfig(Code.REGION_0400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0400CheckBox;

    /** 分野(文系)：史・地理 */
    @Location(x = 425, y = 118)
    @Academic
    @CodeConfig(Code.REGION_0500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0500CheckBox;

    /** 分野(文系)：教育 */
    @Location(x = 520, y = 118)
    @Academic
    @CodeConfig(Code.REGION_0600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0600CheckBox;

    /** 分野(文系)：心理 */
    @Location(x = 313, y = 136)
    @Academic
    @CodeConfig(Code.REGION_0700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0700CheckBox;

    /** 分野(文系)：地域・国際 */
    @Location(x = 425, y = 136)
    @Academic
    @CodeConfig(Code.REGION_0800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0800CheckBox;

    /** 分野(文系)：文化・教養 */
    @Location(x = 520, y = 136)
    @Academic
    @CodeConfig(Code.REGION_0900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r0900CheckBox;

    /** 中系統(文系)：社会・国際 */
    @Location(x = 13, y = 166)
    @Academic
    @CodeConfig(Code.M_STEMMA_21)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms21CheckBox;

    /** 小系統(文系)：社会・社会福祉 */
    @Location(x = 176, y = 167)
    @Academic
    @CodeConfig(Code.S_STEMMA_04)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss04CheckBox;

    /** 分野(文系)：社会 */
    @Location(x = 313, y = 167)
    @Academic
    @CodeConfig(Code.REGION_1000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1000CheckBox;

    /** 分野(文系)：社会福祉 */
    @Location(x = 390, y = 167)
    @Academic
    @CodeConfig(Code.REGION_1100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1100CheckBox;

    /** 小系統(文系)：国際 */
    @Location(x = 176, y = 189)
    @Academic
    @CodeConfig(Code.S_STEMMA_05)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss05CheckBox;

    /** 分野(文系)：国際法 */
    @Location(x = 313, y = 189)
    @Academic
    @CodeConfig(Code.REGION_1200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1200CheckBox;

    /** 分野(文系)：国際経済 */
    @Location(x = 390, y = 189)
    @Academic
    @CodeConfig(Code.REGION_1300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1300CheckBox;

    /** 分野(文系)：国際関係 */
    @Location(x = 479, y = 189)
    @Academic
    @CodeConfig(Code.REGION_1400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1400CheckBox;

    /** 中系統(文系)：法・政治 */
    @Location(x = 13, y = 219)
    @Academic
    @CodeConfig(Code.M_STEMMA_22)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms22CheckBox;

    /** 小系統(文系)：法・政治 */
    @Location(x = 176, y = 220)
    @Academic
    @CodeConfig(Code.S_STEMMA_06)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss06CheckBox;

    /** 分野(文系)：法 */
    @Location(x = 313, y = 220)
    @Academic
    @CodeConfig(Code.REGION_1500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1500CheckBox;

    /** 分野(文系)：政治 */
    @Location(x = 365, y = 220)
    @Academic
    @CodeConfig(Code.REGION_1600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1600CheckBox;

    /** 中系統(文系)：経済・経営・商 */
    @Location(x = 13, y = 250)
    @Academic
    @CodeConfig(Code.M_STEMMA_23)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms23CheckBox;

    /** 小系統(文系)：経済 */
    @Location(x = 176, y = 251)
    @Academic
    @CodeConfig(Code.S_STEMMA_07)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss07CheckBox;

    /** 小系統(文系)：経営・商・会計 */
    @Location(x = 176, y = 273)
    @Academic
    @CodeConfig(Code.S_STEMMA_08)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss08CheckBox;

    /** 分野(文系)：経営 */
    @Location(x = 313, y = 273)
    @Academic
    @CodeConfig(Code.REGION_1800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1800CheckBox;

    /** 分野(文系)：経営情報 */
    @Location(x = 376, y = 273)
    @Academic
    @CodeConfig(Code.REGION_1900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r1900CheckBox;

    /** 分野(文系)：商・会計・他 */
    @Location(x = 464, y = 273)
    @Academic
    @CodeConfig(Code.REGION_2000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox r2000CheckBox;

    /** 中系統(文系)：教育(教員養成) */
    @Location(x = 13, y = 303)
    @Academic
    @CodeConfig(Code.M_STEMMA_31)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mas31CheckBox;

    /** 小系統(文系)：教員−教科 */
    @Location(x = 176, y = 304)
    @Academic
    @CodeConfig(Code.S_STEMMA_09)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas09CheckBox;

    /** 小系統(文系)：教員−実技 */
    @Location(x = 176, y = 326)
    @Academic
    @CodeConfig(Code.S_STEMMA_10)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas10CheckBox;

    /** 小系統(文系)：教員−他 */
    @Location(x = 176, y = 348)
    @Academic
    @CodeConfig(Code.S_STEMMA_11)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas11CheckBox;

    /** 分野(文系)：幼稚園 */
    @Location(x = 313, y = 348)
    @Academic
    @CodeConfig(Code.REGION_2300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2300CheckBox;

    /** 分野(文系)：養護 */
    @Location(x = 437, y = 348)
    @Academic
    @CodeConfig(Code.REGION_2400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2400CheckBox;

    /** 分野(文系)：特別支援学校 */
    @Location(x = 313, y = 366)
    @Academic
    @CodeConfig(Code.REGION_2500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2500CheckBox;

    /** 分野(文系)：その他教員養成 */
    @Location(x = 437, y = 366)
    @Academic
    @CodeConfig(Code.REGION_2600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2600CheckBox;

    /** 中系統(文系)：教育(総合科学) */
    @Location(x = 13, y = 395)
    @Academic
    @CodeConfig(Code.M_STEMMA_32)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mas32CheckBox;

    /** 小系統(文系)：教育(総合科学) */
    @Location(x = 176, y = 396)
    @Academic
    @CodeConfig(Code.S_STEMMA_12)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas12CheckBox;

    /** 分野(文系)：総課-スポーツ */
    @Location(x = 313, y = 396)
    @Academic
    @CodeConfig(Code.REGION_2700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2700CheckBox;

    /** 分野(文系)：総課-芸術・デザイン */
    @Location(x = 437, y = 396)
    @Academic
    @CodeConfig(Code.REGION_2800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2800CheckBox;

    /** 分野(文系)：総課-情報 */
    @Location(x = 313, y = 414)
    @Academic
    @CodeConfig(Code.REGION_2900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra2900CheckBox;

    /** 分野(文系)：総課-国際・言語・文化 */
    @Location(x = 437, y = 414)
    @Academic
    @CodeConfig(Code.REGION_3000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra3000CheckBox;

    /** 分野(文系)：総課-心理・臨床 */
    @Location(x = 313, y = 432)
    @Academic
    @CodeConfig(Code.REGION_3100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra3100CheckBox;

    /** 分野(文系)：総課-地域・社会・生活 */
    @Location(x = 437, y = 432)
    @Academic
    @CodeConfig(Code.REGION_3200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra3200CheckBox;

    /** 分野(文系)：総課-自然・環境・他 */
    @Location(x = 313, y = 450)
    @Academic
    @CodeConfig(Code.REGION_3300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra3300CheckBox;

    /** 中系統(文系)：生活科学 */
    @Location(x = 13, y = 480)
    @Academic
    @CodeConfig(Code.M_STEMMA_61)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms61BCheckBox;

    /** 小系統(文系)：生活科学 */
    @Location(x = 176, y = 481)
    @Academic
    @CodeConfig(Code.S_STEMMA_22)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas22CheckBox;

    /** 分野(文系)：食物・栄養 */
    @Location(x = 313, y = 481)
    @Academic
    @CodeConfig(Code.REGION_6500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra6500CheckBox;

    /** 分野(文系)：被服 */
    @Location(x = 407, y = 481)
    @Academic
    @CodeConfig(Code.REGION_6600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra6600CheckBox;

    /** 分野(文系)：児童 */
    @Location(x = 495, y = 481)
    @Academic
    @CodeConfig(Code.REGION_6700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra6700CheckBox;

    /** 分野(文系)：住居 */
    @Location(x = 313, y = 499)
    @Academic
    @CodeConfig(Code.REGION_6800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra6800CheckBox;

    /** 分野(文系)：生活科学 */
    @Location(x = 407, y = 499)
    @Academic
    @CodeConfig(Code.REGION_6900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra6900CheckBox;

    /** 中系統(文系)：芸術・スポーツ科学 */
    @Location(x = 13, y = 528)
    @Academic
    @CodeConfig(Code.M_STEMMA_62)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms62BCheckBox;

    /** 小系統(文系)：芸術・スポーツ科学 */
    @Location(x = 176, y = 529)
    @Academic
    @CodeConfig(Code.S_STEMMA_24)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas24CheckBox;

    /** 分野(文系)：美術・デザイン */
    @Location(x = 313, y = 529)
    @Academic
    @CodeConfig(Code.REGION_7400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7400CheckBox;

    /** 分野(文系)：音楽 */
    @Location(x = 462, y = 529)
    @Academic
    @CodeConfig(Code.REGION_7500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7500CheckBox;

    /** 分野(文系)：その他芸術 */
    @Location(x = 313, y = 547)
    @Academic
    @CodeConfig(Code.REGION_7600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7600CheckBox;

    /** 分野(文系)：芸術理論 */
    @Location(x = 462, y = 547)
    @Academic
    @CodeConfig(Code.REGION_7700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7700CheckBox;

    /** 分野(文系)：スポーツ・健康 */
    @Location(x = 313, y = 565)
    @Academic
    @CodeConfig(Code.REGION_7800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7800CheckBox;

    /** 中系統(文系)：総合・環境・人間・情報 */
    @Location(x = 13, y = 596)
    @Academic
    @CodeConfig(Code.M_STEMMA_71)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mas71CheckBox;

    /** 小系統(文系)：総・環・人・情 */
    @Location(x = 176, y = 596)
    @Academic
    @CodeConfig(Code.S_STEMMA_23)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sas23CheckBox;

    /** 分野(文系)：総合 */
    @Location(x = 313, y = 596)
    @Academic
    @CodeConfig(Code.REGION_7000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7000CheckBox;

    /** 分野(文系)：環境 */
    @Location(x = 377, y = 596)
    @Academic
    @CodeConfig(Code.REGION_7100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7100CheckBox;

    /** 分野(文系)：情報 */
    @Location(x = 441, y = 596)
    @Academic
    @CodeConfig(Code.REGION_7200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7300CheckBox;

    /** 分野(文系)：人間 */
    @Location(x = 504, y = 596)
    @Academic
    @CodeConfig(Code.REGION_7300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox ra7200CheckBox;

    /** 中系統(理系)：理 */
    @Location(x = 13, y = 674)
    @CodeConfig(Code.M_STEMMA_41)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms41CheckBox;

    /** 小系統(理系)：理 */
    @Location(x = 176, y = 675)
    @CodeConfig(Code.S_STEMMA_13)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss13CheckBox;

    /** 分野(理系)：数学・数理情報 */
    @Location(x = 313, y = 675)
    @CodeConfig(Code.REGION_3400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3400CheckBox;

    /** 分野(理系)：物理 */
    @Location(x = 431, y = 675)
    @CodeConfig(Code.REGION_3500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3500CheckBox;

    /** 分野(理系)：化学 */
    @Location(x = 513, y = 675)
    @CodeConfig(Code.REGION_3600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3600CheckBox;

    /** 分野(理系)：生物 */
    @Location(x = 313, y = 693)
    @CodeConfig(Code.REGION_3700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3700CheckBox;

    /** 分野(理系)：地学・他 */
    @Location(x = 431, y = 693)
    @CodeConfig(Code.REGION_3800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3800CheckBox;

    /** 中系統(理系)：工 */
    @Location(x = 13, y = 722)
    @CodeConfig(Code.M_STEMMA_42)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms42CheckBox;

    /** 小系統(理系)：機械・航空 */
    @Location(x = 176, y = 723)
    @CodeConfig(Code.S_STEMMA_14)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss14CheckBox;

    /** 分野(理系)：機械 */
    @Location(x = 313, y = 723)
    @CodeConfig(Code.REGION_3900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3900CheckBox;

    /** 分野(理系)：航空・宇宙 */
    @Location(x = 377, y = 723)
    @CodeConfig(Code.REGION_4000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4000CheckBox;

    /** 小系統(理系)：電気電子・情報 */
    @Location(x = 176, y = 745)
    @CodeConfig(Code.S_STEMMA_15)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss15CheckBox;

    /** 分野(理系)：電気・電子 */
    @Location(x = 313, y = 745)
    @CodeConfig(Code.REGION_4100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4100CheckBox;

    /** 分野(理系)：通信・情報 */
    @Location(x = 407, y = 745)
    @CodeConfig(Code.REGION_4200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4200CheckBox;

    /** 小系統(理系)：土木・建築 */
    @Location(x = 176, y = 767)
    @CodeConfig(Code.S_STEMMA_16)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss16CheckBox;

    /** 分野(理系)：建築 */
    @Location(x = 313, y = 767)
    @CodeConfig(Code.REGION_4300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4300CheckBox;

    /** 分野(理系)：土木・環境 */
    @Location(x = 377, y = 767)
    @CodeConfig(Code.REGION_4400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4400CheckBox;

    /** 小系統(理系)：応化・応物・資 */
    @Location(x = 176, y = 789)
    @CodeConfig(Code.S_STEMMA_17)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss17CheckBox;

    /** 分野(理系)：応用化学 */
    @Location(x = 313, y = 789)
    @CodeConfig(Code.REGION_4500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4500CheckBox;

    /** 分野(理系)：材料・物質工 */
    @Location(x = 402, y = 789)
    @CodeConfig(Code.REGION_4600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4600CheckBox;

    /** 分野(理系)：応用物理 */
    @Location(x = 313, y = 807)
    @CodeConfig(Code.REGION_4700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4700CheckBox;

    /** 分野(理系)：資源・エネルギー */
    @Location(x = 402, y = 807)
    @CodeConfig(Code.REGION_4800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4800CheckBox;

    /** 小系統(理系)：生物・経営工他 */
    @Location(x = 176, y = 829)
    @CodeConfig(Code.S_STEMMA_18)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss18CheckBox;

    /** 分野(理系)：生物工 */
    @Location(x = 313, y = 829)
    @CodeConfig(Code.REGION_4900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs4900CheckBox;

    /** 分野(理系)：経営工・管理工 */
    @Location(x = 407, y = 829)
    @CodeConfig(Code.REGION_5000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5000CheckBox;

    /** 分野(理系)：船舶・海洋 */
    @Location(x = 313, y = 847)
    @CodeConfig(Code.REGION_5100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5100CheckBox;

    /** 分野(理系)：デザイン工・他 */
    @Location(x = 407, y = 847)
    @CodeConfig(Code.REGION_5200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5200CheckBox;

    /** 中系統(理系)：農 */
    @Location(x = 13, y = 877)
    @CodeConfig(Code.M_STEMMA_43)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms43CheckBox;

    /** 小系統(理系)：農 */
    @Location(x = 176, y = 878)
    @CodeConfig(Code.S_STEMMA_19)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss19CheckBox;

    /** 分野(理系)：生物生産・応用生命 */
    @Location(x = 313, y = 878)
    @CodeConfig(Code.REGION_5300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5300CheckBox;

    /** 分野(理系)：環境科学 */
    @Location(x = 456, y = 878)
    @CodeConfig(Code.REGION_5400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5400CheckBox;

    /** 分野(理系)：経済システム */
    @Location(x = 313, y = 896)
    @CodeConfig(Code.REGION_5500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5500CheckBox;

    /** 分野(理系)：獣医 */
    @Location(x = 456, y = 896)
    @CodeConfig(Code.REGION_5600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5600CheckBox;

    /** 分野(理系)：酪農・畜産 */
    @Location(x = 313, y = 914)
    @CodeConfig(Code.REGION_5700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5700CheckBox;

    /** 分野(理系)：水産 */
    @Location(x = 456, y = 914)
    @CodeConfig(Code.REGION_5800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5800CheckBox;

    /** 中系統(理系)：医・歯・薬・保健 */
    @Location(x = 13, y = 944)
    @CodeConfig(Code.M_STEMMA_51)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms51CheckBox;

    /** 小系統(理系)：医・歯 */
    @Location(x = 176, y = 945)
    @CodeConfig(Code.S_STEMMA_20)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss20CheckBox;

    /** 分野(理系)：医 */
    @Location(x = 313, y = 945)
    @CodeConfig(Code.REGION_5900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs5900CheckBox;

    /** 分野(理系)：歯 */
    @Location(x = 365, y = 945)
    @CodeConfig(Code.REGION_6000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6000CheckBox;

    /** 小系統(理系)：薬・看護・保健 */
    @Location(x = 176, y = 967)
    @CodeConfig(Code.S_STEMMA_21)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox ss21CheckBox;

    /** 分野(理系)：薬 */
    @Location(x = 313, y = 967)
    @CodeConfig(Code.REGION_6100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6100CheckBox;

    /** 分野(理系)：看護 */
    @Location(x = 365, y = 967)
    @CodeConfig(Code.REGION_6200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6200CheckBox;

    /** 分野(理系)：医療技術 */
    @Location(x = 428, y = 967)
    @CodeConfig(Code.REGION_6300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6300CheckBox;

    /** 分野(理系)：保健・福祉・他 */
    @Location(x = 517, y = 967)
    @CodeConfig(Code.REGION_6400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6400CheckBox;

    /** 中系統(理系)：教育(教員養成) */
    @Location(x = 13, y = 998)
    @CodeConfig(Code.M_STEMMA_31)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mss31CheckBox;

    /** 小系統(理系)：教員−教科 */
    @Location(x = 176, y = 998)
    @CodeConfig(Code.S_STEMMA_09)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss09CheckBox;

    /** 小系統(理系)：教員−実技 */
    @Location(x = 176, y = 1020)
    @CodeConfig(Code.S_STEMMA_10)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss10CheckBox;

    /** 小系統(理系)：教員−他 */
    @Location(x = 176, y = 1042)
    @CodeConfig(Code.S_STEMMA_11)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss11CheckBox;

    /** 分野(理系)：幼稚園 */
    @Location(x = 313, y = 1042)
    @CodeConfig(Code.REGION_2300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2300CheckBox;

    /** 分野(理系)：養護 */
    @Location(x = 437, y = 1042)
    @CodeConfig(Code.REGION_2400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2400CheckBox;

    /** 分野(理系)：特別支援学校 */
    @Location(x = 313, y = 1060)
    @CodeConfig(Code.REGION_2500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2500CheckBox;

    /** 分野(理系)：その他教員養成 */
    @Location(x = 437, y = 1060)
    @CodeConfig(Code.REGION_2600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2600CheckBox;

    /** 中系統(理系)：教育(総合科学) */
    @Location(x = 13, y = 1090)
    @CodeConfig(Code.M_STEMMA_32)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mss32CheckBox;

    /** 小系統(理系)：教育(総合科学) */
    @Location(x = 176, y = 1090)
    @CodeConfig(Code.S_STEMMA_12)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss12CheckBox;

    /** 分野(理系)：総課-スポーツ */
    @Location(x = 313, y = 1090)
    @CodeConfig(Code.REGION_2700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2700CheckBox;

    /** 分野(理系)：総課-芸術・デザイン */
    @Location(x = 437, y = 1090)
    @CodeConfig(Code.REGION_2800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2800CheckBox;

    /** 分野(理系)：総課-情報 */
    @Location(x = 313, y = 1108)
    @CodeConfig(Code.REGION_2900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs2900CheckBox;

    /** 分野(理系)：総課-国際・言語・文化 */
    @Location(x = 437, y = 1108)
    @CodeConfig(Code.REGION_3000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3000CheckBox;

    /** 分野(理系)：総課-心理・臨床 */
    @Location(x = 313, y = 1126)
    @CodeConfig(Code.REGION_3100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3100CheckBox;

    /** 分野(理系)：総課-地域・社会・生活 */
    @Location(x = 437, y = 1126)
    @CodeConfig(Code.REGION_3200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3200CheckBox;

    /** 分野(理系)：総課-自然・環境・他 */
    @Location(x = 313, y = 1144)
    @CodeConfig(Code.REGION_3300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs3300CheckBox;

    /** 中系統(理系)：生活科学 */
    @Location(x = 13, y = 1174)
    @CodeConfig(Code.M_STEMMA_61)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms61RCheckBox;

    /** 小系統(理系)：生活科学 */
    @Location(x = 176, y = 1175)
    @CodeConfig(Code.S_STEMMA_22)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss22CheckBox;

    /** 分野(理系)：食物・栄養 */
    @Location(x = 313, y = 1175)
    @CodeConfig(Code.REGION_6500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6500CheckBox;

    /** 分野(理系)：被服 */
    @Location(x = 407, y = 1175)
    @CodeConfig(Code.REGION_6600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6600CheckBox;

    /** 分野(理系)：児童 */
    @Location(x = 495, y = 1175)
    @CodeConfig(Code.REGION_6700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6700CheckBox;

    /** 分野(理系)：住居 */
    @Location(x = 313, y = 1193)
    @CodeConfig(Code.REGION_6800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6800CheckBox;

    /** 分野(理系)：生活科学 */
    @Location(x = 407, y = 1193)
    @CodeConfig(Code.REGION_6900)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs6900CheckBox;

    /** 中系統(理系)：芸術・スポーツ科学 */
    @Location(x = 13, y = 1222)
    @CodeConfig(Code.M_STEMMA_62)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox ms62RCheckBox;

    /** 小系統(理系)：芸術・スポーツ科学 */
    @Location(x = 176, y = 1223)
    @CodeConfig(Code.S_STEMMA_24)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss24CheckBox;

    /** 分野(理系)：美術・デザイン */
    @Location(x = 313, y = 1223)
    @CodeConfig(Code.REGION_7400)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7400CheckBox;

    /** 分野(理系)：音楽 */
    @Location(x = 462, y = 1223)
    @CodeConfig(Code.REGION_7500)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7500CheckBox;

    /** 分野(理系)：その他芸術 */
    @Location(x = 313, y = 1241)
    @CodeConfig(Code.REGION_7600)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7600CheckBox;

    /** 分野(理系)：芸術理論 */
    @Location(x = 462, y = 1241)
    @CodeConfig(Code.REGION_7700)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7700CheckBox;

    /** 分野(理系)：スポーツ・健康 */
    @Location(x = 313, y = 1259)
    @CodeConfig(Code.REGION_7800)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7800CheckBox;

    /** 中系統(理系)：総合・環境・人間・情報 */
    @Location(x = 13, y = 1290)
    @CodeConfig(Code.M_STEMMA_71)
    @Action("changeMStemma")
    @Font(BZFont.MSTEMMA)
    public ExmntnFcltCheckBox mss71CheckBox;

    /** 小系統(理系)：総・環・人・情 */
    @Location(x = 176, y = 1290)
    @CodeConfig(Code.S_STEMMA_23)
    @Action("changeSStemma")
    public ExmntnFcltCheckBox sss23CheckBox;

    /** 分野(理系)：総合 */
    @Location(x = 313, y = 1290)
    @CodeConfig(Code.REGION_7000)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7000CheckBox;

    /** 分野(理系)：環境 */
    @Location(x = 377, y = 1290)
    @CodeConfig(Code.REGION_7100)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7100CheckBox;

    /** 分野(理系)：情報 */
    @Location(x = 441, y = 1290)
    @CodeConfig(Code.REGION_7200)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7300CheckBox;

    /** 分野(理系)：人間 */
    @Location(x = 504, y = 1290)
    @CodeConfig(Code.REGION_7300)
    @Action("changeRegion")
    public ExmntnFcltCheckBox rs7200CheckBox;

    /** 学部系統クリアボタン */
    @Location(x = 536, y = 44)
    public ImageButton clearFacultyButton;

    /** ＊説明文１ */
    @Location(x = 13, y = 351)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel asteriskDesc1Label;

    /** ＊説明文２ */
    @Location(x = 13, y = 435)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel asteriskDesc2Label;

    /** ＊説明文３ */
    @Location(x = 13, y = 1045)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel asteriskDesc3Label;

    /** ＊説明文４ */
    @Location(x = 13, y = 1129)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel asteriskDesc4Label;

    /** 分野チェックボックスに設定されている小系統コードセット */
    private Set<Code> regionSStemmaSet;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
        mas31CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        mas32CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        mas71CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        mss31CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        mss32CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        mss71CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        sas12CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        sss12CheckBox.setVerticalTextPosition(JCheckBox.TOP);
        /* 分野チェックボックスに設定されている小系統コードセットを初期化する */
        Set<Code> set = CollectionsUtil.newHashSet();
        for (CodeComponent c : findComponentsByCodeType(CodeType.REGION)) {
            set.add(c.getCode().getParent());
        }
        regionSStemmaSet = Collections.unmodifiableSet(set);
    }

    /**
     * 分野チェックボックスに設定されている小系統コードセットを返します。
     *
     * @return 分野チェックボックスに設定されている小系統コードセット
     */
    public Set<Code> getRegionSStemmaSet() {
        return regionSStemmaSet;
    }

}

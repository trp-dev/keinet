package jp.ac.kawai_juku.banzaisystem.framework.http.response;

import java.util.List;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学マスタリストの応答データBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DbVersionResponseBean implements BaseResponseBean {

    /** 大学マスタファイルリスト */
    private final List<String> fileList = CollectionsUtil.newArrayList(10);

    /**
     * コンストラクタです。
     */
    DbVersionResponseBean() {
    }

    /**
     * 大学マスタファイルリストを返します。
     *
     * @return 大学マスタファイルリスト
     */
    public List<String> getFileList() {
        return fileList;
    }

}

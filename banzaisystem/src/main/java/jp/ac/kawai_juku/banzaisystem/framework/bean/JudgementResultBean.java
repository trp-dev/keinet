package jp.ac.kawai_juku.banzaisystem.framework.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 判定結果を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class JudgementResultBean extends UnivKeyBean {

    /** 日程コード */
    private String scheduleCd;

    /** 評価-センタ  */
    private String centerRating;

    /** 評価-二次  */
    private String secondRating;

    /** 評価-総合  */
    private String totalRating;

    /** 評価-総合ポイント  */
    private String totalRatingPoint;

    /** センタ得点 */
    private Integer cScore;

    /** 二次偏差値 */
    private Double sScore;

    /** センタ配点比 */
    private Integer cAllotPntRate;

    /** 二次配点比 */
    private Integer sAllotPntRate;

    /** センタボーダ */
    private Integer borderPoint;

    /** センタボーダ得点率 */
    private Integer cenScoreRate;

    /** 二次ランク */
    private String secondRank;

    /**
     * 共通テスト(含まない)得点
     */
    private Integer pointOfExclude;

    /**
     * 共通テスト(含まない)得点率
     */
    private Integer pointExScoreRate;

    /**
     * 共通テスト(含む)得点
     */
    private Integer pointOfInclude;

    /**
     * 共通テスト(含む)得点率
     */
    private Integer pointIncScoreRate;

    /**
     * 共通テスト(含まない)評価
     */
    private String valuateExclude;

    /**
     * 共通テスト(含む)評価
     */
    private String valuateInclude;
    /**
     * 共通テスト(含む)評価
     */
    private String cLetterJudgement;

    /**
     * 共通テスト（含む）出願要件パターンコード
     */
    private String appReqPaternCd;

    /**
     * 共通テスト（含む）英語出願要件
     */
    private String engSSSutuGanHanTeiKbn;

    /**
     * 共通テスト（含む）英語認定試験出願基準非対応告知
     */
    private String engSSHKSutuGanHTKokuchi;

    /**
     * センター満点値
     */
    private String cFullPoint;

    /**
     * 二次満点値
     */
    private String sFullPoint;

    /**
     * コンストラクタです。
     */
    public JudgementResultBean() {
    }

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public JudgementResultBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * コンストラクタです。
     *
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @param deptCd 学科コード
     */
    public JudgementResultBean(String univCd, String facultyCd, String deptCd) {
        super(univCd, facultyCd, deptCd);
    }

    /**
     * 評価-センタを返します。
     *
     * @return 評価-センタ
     */
    public final String getCenterRating() {
        return centerRating;
    }

    /**
     * 評価-センタをセットします。
     *
     * @param centerRating 評価-センタ
     */
    public final void setCenterRating(String centerRating) {
        this.centerRating = centerRating;
    }

    /**
     * 評価-二次を返します。
     *
     * @return 評価-二次
     */
    public final String getSecondRating() {
        return secondRating;
    }

    /**
     * 評価-二次をセットします。
     *
     * @param secondRating 評価-二次
     */
    public final void setSecondRating(String secondRating) {
        this.secondRating = secondRating;
    }

    /**
     * 評価-総合を返します。
     *
     * @return 評価-総合
     */
    public final String getTotalRating() {
        return totalRating;
    }

    /**
     * 評価-総合をセットします。
     *
     * @param totalRating 評価-総合
     */
    public final void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    /**
     * センタ得点を返します。
     *
     * @return センタ得点
     */
    public final Integer getcScore() {
        return cScore;
    }

    /**
     * センタ得点をセットします。
     *
     * @param cScore センタ得点
     */
    public final void setcScore(Integer cScore) {
        this.cScore = cScore;
    }

    /**
     * 二次偏差値を返します。
     *
     * @return 二次偏差値
     */
    public final Double getsScore() {
        return sScore;
    }

    /**
     * 二次偏差値をセットします。
     *
     * @param sScore 二次偏差値
     */
    public final void setsScore(Double sScore) {
        this.sScore = sScore;
    }

    /**
     * センタ配点比を返します。
     *
     * @return センタ配点比
     */
    public final Integer getcAllotPntRate() {
        return cAllotPntRate;
    }

    /**
     * センタ配点比をセットします。
     *
     * @param cAllotPntRate センタ配点比
     */
    public final void setcAllotPntRate(Integer cAllotPntRate) {
        this.cAllotPntRate = cAllotPntRate;
    }

    /**
     * 二次配点比を返します。
     *
     * @return 二次配点比
     */
    public final Integer getsAllotPntRate() {
        return sAllotPntRate;
    }

    /**
     * 二次配点比をセットします。
     *
     * @param sAllotPntRate 二次配点比
     */
    public final void setsAllotPntRate(Integer sAllotPntRate) {
        this.sAllotPntRate = sAllotPntRate;
    }

    /**
     * 評価-総合ポイントを返します。
     *
     * @return 評価-総合ポイント
     */
    public final String getTotalRatingPoint() {
        return totalRatingPoint;
    }

    /**
     * 評価-総合ポイントをセットします。
     *
     * @param totalRatingPoint 評価-総合ポイント
     */
    public final void setTotalRatingPoint(String totalRatingPoint) {
        this.totalRatingPoint = totalRatingPoint;
    }

    /**
     * 日程コードを返します。
     *
     * @return 日程コード
     */
    public final String getScheduleCd() {
        return scheduleCd;
    }

    /**
     * 日程コードをセットします。
     *
     * @param scheduleCd 日程コード
     */
    public final void setScheduleCd(String scheduleCd) {
        this.scheduleCd = scheduleCd;
    }

    /**
     * センタボーダを返します。
     *
     * @return センタボーダ
     */
    public Integer getBorderPoint() {
        return borderPoint;
    }

    /**
     * センタボーダをセットします。
     *
     * @param borderPoint センタボーダ
     */
    public void setBorderPoint(Integer borderPoint) {
        this.borderPoint = borderPoint;
    }

    /**
     * センタボーダ得点率を返します。
     *
     * @return センタボーダ得点率
     */
    public Integer getCenScoreRate() {
        return cenScoreRate;
    }

    /**
     * センタボーダ得点率をセットします。
     *
     * @param cenScoreRate センタボーダ得点率
     */
    public void setCenScoreRate(Integer cenScoreRate) {
        this.cenScoreRate = cenScoreRate;
    }

    /**
     * 二次ランクを返します。
     *
     * @return 二次ランク
     */
    public String getSecondRank() {
        return secondRank;
    }

    /**
     * 二次ランクをセットします。
     *
     * @param secondRank 二次ランク
     */
    public void setSecondRank(String secondRank) {
        this.secondRank = secondRank;
    }

    /**
     * 【含まない】ボーダ/満点 (率)を返します。
     *
     * @return 【含まない】ボーダ/満点 (率)
     */
    public String getBorderOfExclude() {
        String result = null;
        if (this.borderPoint != null && this.cFullPoint != null) {
            result = String.format("%d/%s (%d)", this.borderPoint, this.cFullPoint, this.cenScoreRate);
        }
        return result;
    }

    /**
     * 【含まない】得点を返します。
     *
     * @return 【含まない】得点
     */
    public Integer getPointOfExclude() {
        return pointOfExclude;
    }

    /**
     * 【含まない】得点を設定します。
     *
     * @param pointOfExclude 【含まない】得点
     */
    public void setPointOfExclude(Integer pointOfExclude) {
        this.pointOfExclude = pointOfExclude;
    }

    /**
     * 【含まない】得点を返します。
     *
     * @return 【含まない】得点
     */
    public Integer getPointExScoreRate() {
        return pointExScoreRate;
    }

    /**
     * 【含まない】得点率を設定します。
     *
     * @param pointExScoreRate 【含まない】得点
     */
    public void setPointExScoreRate(Integer pointExScoreRate) {
        this.pointExScoreRate = pointExScoreRate;
    }

    /**
     * 【含む】ボーダ/満点 (率)を返します。
     *
     * @return 【含む】ボーダ/満点 (率)
     */
    public String getBorderOfInclude() {
        String result = null;
        if (this.borderPoint != null && this.cFullPoint != null) {
            result = String.format("%d/%s (%d)", this.borderPoint, this.cFullPoint, this.cenScoreRate);
        }
        return result;
    }

    /**
     * 【含む】得点を返します。
     *
     * @return 【含む】得点
     */
    public Integer getPointOfInclude() {
        return pointOfInclude;
    }

    /**
     * 【含む】得点を設定します。
     *
     * @param pointOfInclude 【含む】得点
     */
    public void setPointOfInclude(Integer pointOfInclude) {
        this.pointOfInclude = pointOfInclude;
    }

    /**
     * 【含む】得点を返します。
     *
     * @return 【含む】得点
     */
    public Integer getPointIncScoreRate() {
        return pointIncScoreRate;
    }

    /**
     * 【含む】得点率を設定します。
     *
     * @param pointIncScoreRate 【含む】得点率
     */
    public void setPointIncScoreRate(Integer pointIncScoreRate) {
        this.pointIncScoreRate = pointIncScoreRate;
    }

    /**
     * 【含まない】評価を返します。
     *
     * @return 【含まない】評価
     */
    public String getValuateExclude() {
        return valuateExclude;
    }

    /**
     * 【含まない】評価を設定します。
     *
     * @param valuateExclude 【含まない】評価
     */
    public void setValuateExclude(String valuateExclude) {
        this.valuateExclude = valuateExclude;
    }

    /**
     * 【含む】評価を返します。
     *
     * @return 【含む】評価
     */
    public String getValuateInclude() {
        return valuateInclude;
    }

    /**
     * 【含む】評価を設定します。
     *
     * @param valuateInclude 【含む】評価
     */
    public void setValuateInclude(String valuateInclude) {
        this.valuateInclude = valuateInclude;
    }

    /**
     * 【含む】評価を返します。
     *
     * @return 【含む】評価
     */
    public String getCLetterJudgement() {
        return cLetterJudgement;
    }

    /**
     * 【含む】評価を設定します。
     *
     * @param cLetterJudgement 【含む】評価
     */
    public void setCLetterJudgement(String cLetterJudgement) {
        this.cLetterJudgement = cLetterJudgement;
    }

    /**
     * 共通テスト 出願要件パターンコード
     *
     * @return 共通テスト 出願要件パターンコード
     */
    public String getAppReqPaternCd() {
        return appReqPaternCd;
    }

    /**
     * 共通テスト 出願要件パターンコードを設定します。
     *
     * @param appReqPaternCd 共通テスト 出願要件パターンコード
     */
    public void setAppReqPaternCd(String appReqPaternCd) {
        this.appReqPaternCd = appReqPaternCd;
    }

    /**
     * 共通テスト（含む）英語出願要件を返します。
     *
     * @return 共通テスト（含む）英語出願要件
     */
    public String getEngSSSutuGanHanTeiKbn() {
        return engSSSutuGanHanTeiKbn;
    }

    /**
     * 共通テスト（含む）英語出願要件を設定します。
     *
     * @param engSSSutuGanHanTeiKbn 共通テスト（含む）英語出願要件
     */
    public void setEngSSSutuGanHanTeiKbn(String engSSSutuGanHanTeiKbn) {
        this.engSSSutuGanHanTeiKbn = engSSSutuGanHanTeiKbn;
    }

    /**
     * 共通テスト（含む）英語認定試験出願基準非対応告知を返します。
     *
     * @return 共通テスト（含む）英語認定試験出願基準非対応告知
     */
    public String getEngSSHKSutuGanHTKokuchi() {
        return engSSHKSutuGanHTKokuchi;
    }

    /**
     * 共通テスト（含む）英語認定試験出願基準非対応告知を設定します。
     *
     * @param engSSHKSutuGanHTKokuchi 共通テスト（含む）英語認定試験出願基準非対応告知
     */
    public void setEngSSHKSutuGanHTKokuchi(String engSSHKSutuGanHTKokuchi) {
        this.engSSHKSutuGanHTKokuchi = engSSHKSutuGanHTKokuchi;
    }

    /**
     * センタ満点値を返します。
     *
     * @return センタ満点値
     */
    public String getcFullPoint() {
        return cFullPoint;
    }

    /**
     * センタ満点値を設定します。
     *
     * @param cFullPoint センタ満点値
     */
    public void setcFullPoint(String cFullPoint) {
        this.cFullPoint = cFullPoint;
    }

    /**
     * 二次満点値を返します。
     *
     * @return 二次満点値
     */
    public String getsFullPoint() {
        return sFullPoint;
    }

    /**
     * 二次満点値を設定します。
     *
     * @param sFullPoint 二次満点値
     */
    public void setsFullPoint(String sFullPoint) {
        this.sFullPoint = sFullPoint;
    }

}

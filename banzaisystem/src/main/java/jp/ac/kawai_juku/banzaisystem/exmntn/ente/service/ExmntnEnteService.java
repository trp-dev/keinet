package jp.ac.kawai_juku.banzaisystem.exmntn.ente.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.bean.ExmntnEnteJpnIniBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.dao.ExmntnEnteDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.view.ExmntnEnteView;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.seasar.framework.util.tiger.CollectionsUtil;;

/**
 *
 * 大学検索のサービスです。
 *
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
public class ExmntnEnteService {

    /** DAO */
    @Resource
    private ExmntnEnteDao exmntnEnteDao;

    /**
     * CEFR初期判定
     * @param individualid 生徒ID
     * @param year 年度
     * @param examdiv 模試コード
     * @return CEFRスコアデータ
     */
    /*
    public ExmntnEnteCEFRIniBean iniCefr(Integer individualid, String year, String examdiv) {
    
        return exmntnEnteDao.selectIniCefr(individualid, year, examdiv);
    }
    */

    /**
     * 国語(記)初期判定
     * @param individualid 生徒ID
     * @param year 年度
     * @param examdiv 模試コード
     * @return 国語スコアデータ
     */
    public ExmntnEnteJpnIniBean iniJpnWrite(Integer individualid, String year, String examdiv) {

        return exmntnEnteDao.selectIniJpn(individualid, year, examdiv);
    }

    /**
     * データリストをフィルタします。
     *
     * @param view ビュー
     * @param dataList データリスト
     * @return フィルタ結果
     */
    public List<Map<String, CellData>> filterDataList(ExmntnEnteView view, List<Map<String, CellData>> dataList) {

        if (!needFilter(view, CodeType.UNIV_SEARCH_SCHEDULE2)) {
            /* フィルタ条件なし */
            return dataList;
        }

        List<Map<String, CellData>> list = CollectionsUtil.newArrayList(dataList.size());
        for (Map<String, CellData> map : dataList) {
            Code div = (Code) map.get("division").getSortValue();
            if (div == Code.UNIV_RESULT_UNIV_DIV_NATIONAL || div == Code.UNIV_RESULT_UNIV_DIV_PUBLIC) {
                /* 国公立 */
            } else {
                /* 私立短大 */
                if (!checkPrivateJunior(view, map)) {
                    continue;
                }
            }
            list.add(map);
        }
        return list;
    }

    /**
     * 指定したコード種別でのフィルタが必要であるかを判断します。
     *
     * @param view ビュー
     * @param type コード種別
     * @return フィルタが必要ならtrue
     */
    private boolean needFilter(ExmntnEnteView view, CodeType type) {
        for (CodeComponent c : view.findComponentsByCodeType(type)) {
            if (c.isSelected()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 私立短大のフィルタ条件を満たしているかチェックします。
     *
     * @param view ビュー
     * @param map 検索結果
     * @return チェック結果
     */
    private boolean checkPrivateJunior(ExmntnEnteView view, Map<String, CellData> map) {
        String scheduleCd = map.get("scheduleCd").getSortValue().toString();
        if (scheduleCd.equals(Univ.SCHEDULE_CNT)) {
            return view.useCenterCheckBox.isSelected();
        } else {
            return view.publicCheckBox.isSelected();
        }
    }
}

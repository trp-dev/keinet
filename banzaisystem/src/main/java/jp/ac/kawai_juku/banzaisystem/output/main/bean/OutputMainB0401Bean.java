package jp.ac.kawai_juku.banzaisystem.output.main.bean;

/**
 *
 * 志望大学・評価一覧CSVの出力情報を保持するBeanです。
 * 1つのbeanがCSVの1行分の情報に相当します。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class OutputMainB0401Bean {

    /* 学年 */
    private String grade;

    /* クラス */
    private String cls;

    /* クラス番号 */
    private String classNo;

    /* カナ氏名 */
    private String nameKana;

    /* 志望順位 */
    private String candidateRank;

    /* 大学コード10桁 */
    private String univKey;

    /* 大学コード5桁 */
    private String choiceCd5;

    /* 日程 */
    private String scheduleName;

    /* 大学名 */
    private String univName;

    /* 学部名 */
    private String facultyName;

    /* 学科名 */
    private String deptName;

    /* 共通テスト（含まない）評価得点 */
    private String centerScore;

    /* 共通テスト（含まない）評価 */
    private String centerRating;

    /* 共通テスト（含む）評価得点 */
    private String centerIncScore;

    /* 共通テスト（含む）評価 */
    private String centerIncRating;

    /* 共通テスト（含む）英語出願要件 */
    private String centerIncJudge;

    /* 共通テスト（含む）英語認定試験出願基準非対応告知 */
    private String centerIncNotice;

    /* 二次偏差値 */
    private String secondDeviation;

    /* 二次評価 */
    private String secondRating;

    /* 総合ポイント */
    private String totalRatingPoint;

    /* 総合評価 */
    private String totalRating;

    /* 共通テスト（含まない）A評価基準 */
    private String ratingALine;

    /* 共通テスト（含まない）ボーダーライン */
    private String borderLine;

    /* 共通テスト（含まない）D評価基準 */
    private String ratingDLine;

    /* 共通テスト（含まない）満点 */
    private String centerFullScore;

    /* 共通テスト（含む）A評価基準 */
    private String ratingIncALine;

    /* 共通テスト（含む）ボーダーライン */
    private String incBorderLine;

    /* 共通テスト（含む）D評価基準 */
    private String ratingIncDLine;

    /* 共通テスト（含む）満点 */
    private String centerIncFullScore;

    /* 二次ランク */
    private String secondRank;

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public String getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * カナ氏名を返します。
     *
     * @return カナ氏名
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * カナ氏名をセットします。
     *
     * @param nameKana カナ氏名
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 志望順位を返します。
     *
     * @return 志望順位
     */
    public String getCandidateRank() {
        return candidateRank;
    }

    /**
     * 志望順位をセットします。
     *
     * @param candidateRank 志望順位
     */
    public void setCandidateRank(String candidateRank) {
        this.candidateRank = candidateRank;
    }

    /**
     * 大学コード10桁を返します。
     *
     * @return 大学コード10桁
     */
    public String getUnivKey() {
        return univKey;
    }

    /**
     * 大学コード10桁をセットします。
     *
     * @param univKey 大学コード10桁
     */
    public void setUnivKey(String univKey) {
        this.univKey = univKey;
    }

    /**
     * 大学コード5桁を返します。
     *
     * @return 大学コード5桁
     */
    public String getChoiceCd5() {
        return choiceCd5;
    }

    /**
     * 大学コード5桁をセットします。
     *
     * @param choiceCd5 大学コード5桁
     */
    public void setChoiceCd5(String choiceCd5) {
        this.choiceCd5 = choiceCd5;
    }

    /**
     * 日程を返します。
     *
     * @return 日程
     */
    public String getScheduleName() {
        return scheduleName;
    }

    /**
     * 日程をセットします。
     *
     * @param scheduleName 日程
     */
    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 共通テスト（含まない）評価得点を返します。
     *
     * @return 共通テスト（含まない）評価得点
     */
    public String getCenterScore() {
        return centerScore;
    }

    /**
     * 共通テスト（含まない）評価得点をセットします。
     *
     * @param centerScore 共通テスト（含まない）評価得点
     */
    public void setCenterScore(String centerScore) {
        this.centerScore = centerScore;
    }

    /**
     * 共通テスト（含まない）評価を返します。
     *
     * @return 共通テスト（含まない）評価
     */
    public String getCenterRating() {
        return centerRating;
    }

    /**
     * 共通テスト（含まない）評価をセットします。
     *
     * @param centerRating 共通テスト（含まない）評価
     */
    public void setCenterRating(String centerRating) {
        this.centerRating = centerRating;
    }

    /**
     * 共通テスト（含む）評価得点を返します。
     *
     * @return 共通テスト（含む）評価得点
     */
    public String getCenterIncScore() {
        return centerIncScore;
    }

    /**
     * 共通テスト（含む）評価得点をセットします。
     *
     * @param centerScore 共通テスト（含む）評価得点
     */
    public void setCenterIncScore(String centerScore) {
        this.centerIncScore = centerScore;
    }

    /**
     * 共通テスト（含む）評価を返します。
     *
     * @return 共通テスト（含む）評価
     */
    public String getCenterIncRating() {
        return centerIncRating;
    }

    /**
     * 共通テスト（含む）評価をセットします。
     *
     * @param centerRating 共通テスト（含む）評価
     */
    public void setCenterIncRating(String centerRating) {
        this.centerIncRating = centerRating;
    }

    /**
     * 共通テスト（含む）英語出願要件を返します。
     *
     * @return 共通テスト（含む）英語出願要件
     */
    public String getCenterIncJudge() {
        return centerIncJudge;
    }

    /**
     * 共通テスト（含む）英語出願要件をセットします。
     *
     * @param centerIncJudge 共通テスト（含む）英語出願要件
     */
    public void setCenterIncJudge(String centerIncJudge) {
        this.centerIncJudge = centerIncJudge;
    }

    /**
     * 共通テスト（含む）英語認定試験出願基準非対応告知を返します。
     *
     * @return 共通テスト（含む）英語認定試験出願基準非対応告知
     */
    public String getCenterIncNotice() {
        return centerIncNotice;
    }

    /**
     * 共通テスト（含む）英語認定試験出願基準非対応告知をセットします。
     *
     * @param centerIncNotice 共通テスト（含む）英語認定試験出願基準非対応告知
     */
    public void setCenterIncNotice(String centerIncNotice) {
        this.centerIncNotice = centerIncNotice;
    }

    /**
     * 二次偏差値を返します。
     *
     * @return 二次偏差値
     */
    public String getSecondDeviation() {
        return secondDeviation;
    }

    /**
     * 二次偏差値をセットします。
     *
     * @param secondDeviation 二次偏差値
     */
    public void setSecondDeviation(String secondDeviation) {
        this.secondDeviation = secondDeviation;
    }

    /**
     * 二次評価を返します。
     *
     * @return 二次評価
     */
    public String getSecondRating() {
        return secondRating;
    }

    /**
     * 二次評価をセットします。
     *
     * @param secondRating 二次評価
     */
    public void setSecondRating(String secondRating) {
        this.secondRating = secondRating;
    }

    /**
     * 総合ポイントを返します。
     *
     * @return 総合ポイント
     */
    public String getTotalRatingPoint() {
        return totalRatingPoint;
    }

    /**
     * 総合ポイントをセットします。
     *
     * @param totalRatingPoint 総合ポイント
     */
    public void setTotalRatingPoint(String totalRatingPoint) {
        this.totalRatingPoint = totalRatingPoint;
    }

    /**
     * 総合評価を返します。
     *
     * @return 総合評価
     */
    public String getTotalRating() {
        return totalRating;
    }

    /**
     * 総合評価をセットします。
     *
     * @param totalRating 総合評価
     */
    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    /**
     * 共通テスト（含まない）A評価基準を返します。
     *
     * @return 共通テスト（含まない）A評価基準
     */
    public String getRatingALine() {
        return ratingALine;
    }

    /**
     * 共通テスト（含まない）A評価基準をセットします。
     *
     * @param ratingALine 共通テスト（含まない）A評価基準
     */
    public void setRatingALine(String ratingALine) {
        this.ratingALine = ratingALine;
    }

    /**
     * 共通テスト（含まない）ボーダーラインを返します。
     *
     * @return 共通テスト（含まない）ボーダーライン
     */
    public String getBorderLine() {
        return borderLine;
    }

    /**
     * 共通テスト（含まない）ボーダーラインをセットします。
     *
     * @param borderLine 共通テスト（含まない）ボーダーライン
     */
    public void setBorderLine(String borderLine) {
        this.borderLine = borderLine;
    }

    /**
     * 共通テスト（含まない）D評価基準を返します。
     *
     * @return 共通テスト（含まない）D評価基準
     */
    public String getRatingDLine() {
        return ratingDLine;
    }

    /**
     * 共通テスト（含まない）D評価基準をセットします。
     *
     * @param ratingDLine 共通テスト（含まない）D評価基準
     */
    public void setRatingDLine(String ratingDLine) {
        this.ratingDLine = ratingDLine;
    }

    /**
     * 共通テスト（含まない）満点を返します。
     *
     * @return 共通テスト（含まない）満点
     */
    public String getCenterFullScore() {
        return centerFullScore;
    }

    /**
     * 共通テスト（含まない）満点をセットします。
     *
     * @param centerFullScore 共通テスト（含まない）満点
     */
    public void setCenterFullScore(String centerFullScore) {
        this.centerFullScore = centerFullScore;
    }

    /**
     * 共通テスト（含む）A評価基準を返します。
     *
     * @return 共通テスト（含む）A評価基準
     */
    public String getRatingIncALine() {
        return ratingIncALine;
    }

    /**
     * 共通テスト（含む）A評価基準をセットします。
     *
     * @param ratingALine 共通テスト（含む）A評価基準
     */
    public void setRatingIncALine(String ratingALine) {
        this.ratingIncALine = ratingALine;
    }

    /**
     * 共通テスト（含む）ボーダーラインを返します。
     *
     * @return 共通テスト（含む）ボーダーライン
     */
    public String getIncBorderLine() {
        return incBorderLine;
    }

    /**
     * 共通テスト（含む）ボーダーラインをセットします。
     *
     * @param borderLine 共通テスト（含む）ボーダーライン
     */
    public void setIncBorderLine(String borderLine) {
        this.incBorderLine = borderLine;
    }

    /**
     * 共通テスト（含む）D評価基準を返します。
     *
     * @return 共通テスト（含む）D評価基準
     */
    public String getRatingIncDLine() {
        return ratingIncDLine;
    }

    /**
     * 共通テスト（含む）D評価基準をセットします。
     *
     * @param ratingDLine 共通テスト（含む）D評価基準
     */
    public void setRatingIncDLine(String ratingDLine) {
        this.ratingIncDLine = ratingDLine;
    }

    /**
     * 共通テスト（含む）満点を返します。
     *
     * @return 共通テスト（含む）満点
     */
    public String getCenterIncFullScore() {
        return centerIncFullScore;
    }

    /**
     * 共通テスト（含む）満点をセットします。
     *
     * @param centerFullScore 共通テスト（含む）満点
     */
    public void setCenterIncFullScore(String centerFullScore) {
        this.centerIncFullScore = centerFullScore;
    }

    /**
     * 二次ランクを返します。
     *
     * @return 二次ランク
     */
    public String getSecondRank() {
        return secondRank;
    }

    /**
     * 二次ランクをセットします。
     *
     * @param secondRank 二次ランク
     */
    public void setSecondRank(String secondRank) {
        this.secondRank = secondRank;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.ente.dao;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.bean.ExmntnEnteCEFRIniBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.bean.ExmntnEnteJpnIniBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 大学検索のサービスです。
 *
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
public class ExmntnEnteDao extends BaseDao {

    /**
     * CEFRスコアを取得します。
     * @param individualid 生徒ID
     * @param year 年度
     * @param examdiv 模試コード
     * @return 件数
     */
    public ExmntnEnteCEFRIniBean selectIniCefr(Integer individualid, String year, String examdiv) {
        BeanMap param = new BeanMap();
        param.put("id", individualid);
        param.put("year", year);
        param.put("examdiv", examdiv);
        return jdbcManager.selectBySqlFile(ExmntnEnteCEFRIniBean.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 国語スコアを取得します。
     * @param individualid 生徒ID
     * @param year 年度
     * @param examdiv 模試コード
     * @return 件数
     */
    public ExmntnEnteJpnIniBean selectIniJpn(Integer individualid, String year, String examdiv) {
        BeanMap param = new BeanMap();
        param.put("id", individualid);
        param.put("year", year);
        param.put("examdiv", examdiv);
        return jdbcManager.selectBySqlFile(ExmntnEnteJpnIniBean.class, getSqlPath(), param).getSingleResult();
    }
}

package jp.ac.kawai_juku.banzaisystem.output.main.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.InputFileChooser;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.MenuController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.output.main.view.OutputMainView;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtService;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.controller.SelstdDstdController;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.dto.SelstdDstdDto;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.service.SelstdDstdService;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller.SelstdDunvController;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller.SelstdDunvController.SelstdDunvProcessor;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 帳票出力・CSVファイル画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainController extends MenuController {

    /** csvファイルのファイルフィルタ */
    private final FileFilter filter = new FileNameExtensionFilter("csvファイル（*.csv;*.txt）", "csv", "txt");

    /** View */
    @Resource(name = "outputMainView")
    private OutputMainView view;

    /** 対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    /** 一覧出力ダイアログのサービス */
    @Resource(name = "selstdDprtService")
    private SelstdDprtService service;

    /** 生徒一覧ダイアログのサービス */
    @Resource
    private SelstdDstdService selstdDstdService;

    /** Helper */
    @Resource
    private OutputMainHelper outputMainHelper;

    /** 生徒一覧ダイアログのDTO */
    @Resource
    private SelstdDstdDto selstdDstdDto;

    /**
     * 初期表示アクションです。
     *
     * @return 帳票出力・CSVファイル画面
     */
    @Execute
    @Logging(GamenId.D)
    public ExecuteResult index() {
        List<ComboBoxItem<ExamBean>> itemList = CollectionsUtil.newArrayList(selstdMainView.examComboBox.getItemCount());
        for (int i = 0; i < selstdMainView.examComboBox.getItemCount(); i++) {
            itemList.add(selstdMainView.examComboBox.getItemAt(i));
        }
        view.examComboBox.setItemList(itemList);
        return VIEW_RESULT;
    }

    /**
     * 対象模試コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult examComboBoxAction() {
        outputMainHelper.initGradeComboBox();
        changeUnivRadioAction(view.univDivRadio.isSelected());
        return null;
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        outputMainHelper.initClassTable();
        return null;
    }

    /**
     * 「全選択」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult allChoiceButtonAction() {
        view.classTable.selectAll(true);
        return null;
    }

    /**
     * 「全解除」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult allLiftButtonAction() {
        view.classTable.selectAll(false);
        return null;
    }

    /**
     * 印刷ボタンの状態を変更します。
     */
    public void changePrintButtonStatus() {
        /* クラス未選択か席次番号が空欄なら印刷ボタンを無効にする */
        view.printButton.setEnabled(!view.classTable.getSelectedClassList().isEmpty() && (!view.numberStartField.getText().isEmpty() || !view.numberEndField.getText().isEmpty()));
        /* 「生徒一覧から選択」で選択した生徒をクリアする */
        selstdDstdDto.getSelectedIndividualIdList().clear();
    }

    /**
     * 生徒一覧選択ボタンアクションです。
     *
     * @return 生徒一覧ダイアログ
     */
    @Execute
    public ExecuteResult choiceStudentListButtonAction() {
        /* 選択大学をクリアする */
        view.univList.clearUnivList();
        /* 複数選択モードで起動する */
        return forward(SelstdDstdController.class, "indexOutput");
    }

    /**
     * 印刷ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult printButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                view.outputTree.output(outputMainHelper);
                return null;
            }
        };
    }

    /**
     * 個人成績・志望大学ボタン押下アクションです。
     *
     * @return 個人成績・志望大学画面
     */
    @Execute
    @Override
    public ExecuteResult exmntnButtonAction() {
        return forward(ExmntnMainController.class, "redisplay");
    }

    /**
     * 検索ボタン押下アクションです。
     *
     * @return 大学選択ダイアログ
     */
    @Execute
    public ExecuteResult searchButtonAction() {
        return forward(SelstdDunvController.class, null, new SelstdDunvInBean(view.examComboBox.getSelectedValue(), outputMainHelper.findPrintTargetList(), view.univList.getUnivList(), new SelstdDunvProcessor() {
            @Override
            public void process(List<SelstdDunvBean> list) {
                view.univList.setUnivList(list, view.examComboBox.getSelectedItem(), view.gradeComboBox.getSelectedItem());
            }
        }));
    }

    /**
     * 大学区分指定ラジオボタン変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult univDivRadioAction() {
        changeUnivRadioAction(true);
        return null;
    }

    /**
     * 大学名指定ラジオボタン変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult univNameRadioAction() {
        changeUnivRadioAction(false);
        return null;
    }

    /**
     * 「大学区分指定」「大学名指定」ラジオボタン変更アクションです。
     *
     * @param univDivFlg 大学区分指定ラジオ選択フラグ
     */
    private void changeUnivRadioAction(boolean univDivFlg) {
        view.setEnabledByGroup(OutputMainView.GROUP_UNIV_DIV, univDivFlg);
        view.setEnabledByGroup(OutputMainView.GROUP_UNIV_NAME, !univDivFlg);
    }

    /**
     * 参照ボタンアクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult referenceButtonAction() {

        JFileChooser fileChooser = new InputFileChooser(view.csvFileField.getText());
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(filter);

        int selected = fileChooser.showOpenDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            view.csvFileField.setText(fileChooser.getSelectedFile().getAbsolutePath());
            view.retrieveButton.setEnabled(true);
        }

        return null;
    }

    /**
     * 取り込むボタンアクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult retrieveButtonAction() {

        File file = new File(view.csvFileField.getText());
        if (file.length() == 0 || !filter.accept(file)) {
            /* ファイルサイズが0または不正な拡張子の場合はエラーとする */
            throw new MessageDialogException(getMessage("error.output.main.D_E001"));
        }

        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                view.outputTree.input(outputMainHelper);
                return null;
            }

            @Override
            protected void done() {
                view.csvFileField.setText(null);
                view.retrieveButton.setEnabled(false);
            }
        };
    }

}

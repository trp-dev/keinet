package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の個人成績一覧データBeanです。
 * (国語記述)
 *
 * @author QQ)Nakao.Takaaki
 *
 */
public class SelstdDprtExcelJpnMakerStudentRecordBean {

    /** 個人ID */
    private Integer individualId;

    /** 国語(記)評価問1 */
    private String descRating1;

    /** 国語(記)評価問2 */
    private String descRating2;

    /** 国語(記)評価問3 */
    private String descRating3;

    /** 国語(記)総合評価 */
    private String descRatingTotal;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** 氏名（カナ） */
    private String nameKana;

    /** 対象模試 */
    private String examCd;

    /** 模試外部データ開放日 */
    private String examOpenDate;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 国語(記)評価問1を返します。
     *
     * @return 国語(記)評価問1
     */
    public String getDescRating1() {
        return descRating1;
    }

    /**
     * 国語(記)評価問1をセットします。
     *
     * @param descRating1 国語(記)評価問1
     */
    public void setDescRating1(String descRating1) {
        this.descRating1 = descRating1;
    }

    /**
     * 国語(記)評価問2を返します。
     *
     * @return 国語(記)評価問2
     */
    public String getDescRating2() {
        return descRating2;
    }

    /**
     * 国語(記)評価問2をセットします。
     *
     * @param descRating2 国語(記)評価問2
     */
    public void setDescRating2(String descRating2) {
        this.descRating2 = descRating2;
    }

    /**
     * 国語(記)評価問3を返します。
     *
     * @return 国語(記)評価問3
     */
    public String getDescRating3() {
        return descRating3;
    }

    /**
     * 国語(記)評価問3をセットします。
     *
     * @param descRating3 国語(記)評価問3
     */
    public void setDescRating3(String descRating3) {
        this.descRating3 = descRating3;
    }

    /**
     * 国語(記)総合評価を返します。
     *
     * @return 国語(記)総合評価
     */
    public String getDescRatingTotal() {
        return descRatingTotal;
    }

    /**
     * 国語(記)総合評価をセットします。
     *
     * @param descRatingTotal 国語(記)総合評価
     */
    public void setDescRatingTotal(String descRatingTotal) {
        this.descRatingTotal = descRatingTotal;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * 氏名（カナ）を返します。
     *
     * @return 氏名（カナ）
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 氏名（カナ）をセットします。
     *
     * @param nameKana 氏名（カナ）
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 対象模試をセットします。
     *
     * @param examCd 対象模試
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 模試外部データ開放日を返します。
     *
     * @return 模試外部データ開放日
     */
    public String getExamOpenDate() {
        return examOpenDate;
    }

    /**
     * 模試外部データ開放日をセットします。
     *
     * @param examOpenDate 模試外部データ開放日
     */
    public void setExamOpenDate(String examOpenDate) {
        this.examOpenDate = examOpenDate;
    }

}

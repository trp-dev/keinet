package jp.ac.kawai_juku.banzaisystem.framework.interceptor;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.sql.SQLException;

import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.seasar.framework.aop.interceptors.ThrowsInterceptor;

/**
 *
 * DAOのThrowsInterceptorです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class DaoThrowsInterceptor extends ThrowsInterceptor {

    /** ロック競合発生時のエラーコード */
    private static final String ERROR_SQLITE_BUSY = "[SQLITE_BUSY]";

    /**
     * 例外を処理します。
     *
     * @param e 例外
     * @param invocation {@link MethodInvocation}
     * @throws Throwable 例外
     */
    public void handleThrowable(Throwable e, MethodInvocation invocation) throws Throwable {
        int index = ExceptionUtils.indexOfThrowable(e, SQLException.class);
        if (index > -1) {
            SQLException se = (SQLException) ExceptionUtils.getThrowableList(e).get(index);
            if (se.getMessage().startsWith(ERROR_SQLITE_BUSY)) {
                throw new MessageDialogException(getMessage("error.framework.CM_E002"), e);
            }
        }
        throw e;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.util;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

/**
 *
 * 入試日程に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ScheduleUtil {

    /**
     * コンストラクタです。
     */
    private ScheduleUtil() {
    }

    /**
     * 出願締切日・入試日での検索を有効にするかどうかを判断します。
     *
     * @return 出願締切日・入試日での検索を有効にするならtrue
     */
    public static boolean isScheduleSearchEnabled() {
        String examDiv = getComponent(SystemDto.class).getUnivExamDiv();
        return "05".equals(examDiv) || "06".equals(examDiv) || "07".equals(examDiv);
    }

    /**
     * 出願締切日を表示するかどうかを判断します。
     *
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @return 出願締切日を表示するならtrue
     */
    public static boolean isAppliDeadLineEnabled(String examDiv, String univCd) {
        /* 入試日と表示条件は同じ */
        return isInpleDateEnabled(examDiv, univCd);
    }

    /**
     * 入試日を表示するかどうかを判断します。
     *
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @return 入試日を表示するならtrue
     */
    public static boolean isInpleDateEnabled(String examDiv, String univCd) {
        if (univCd.compareTo("5000") < 0) {
            if (univCd.startsWith("1")) {
                return "07".equals(examDiv);
            } else {
                return "05".equals(examDiv) || "06".equals(examDiv) || "07".equals(examDiv);
            }
        } else {
            return false;
        }
    }

    /**
     * 合格発表日を表示するかどうかを判断します。
     *
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @return 合格発表日を表示するならtrue
     */
    public static boolean isSucAnnDateEnabled(String examDiv, String univCd) {
        /* 入試日と表示条件は同じ */
        return isInpleDateEnabled(examDiv, univCd);
    }

    /**
     * 手続締切日を表示するかどうかを判断します。
     *
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @return 手続締切日を表示するならtrue
     */
    public static boolean isProceDeadLineEnabled(String examDiv, String univCd) {
        /* 入試日と表示条件は同じ */
        return isInpleDateEnabled(examDiv, univCd);
    }

    /**
     * 注意マーク表示が必要であるかどうかを判断します。
     *
     * @param examDiv 模試区分
     * @param univCd 大学コード
     * @param examScheduleDetailFlag 大学基本.入試日程詳細フラグ
     * @param univCommentFlag 大学基本.注釈文フラグ
     * @return 注意マーク表示が必要ならtrue
     */
    public static boolean isCautionNeeded(String examDiv, String univCd, String examScheduleDetailFlag, String univCommentFlag) {

        if ("07".equals(examDiv) && FlagUtil.toBoolean(univCommentFlag)) {
            /* 注釈文マスタが存在する場合 */
            return true;
        }

        return false;
    }

}

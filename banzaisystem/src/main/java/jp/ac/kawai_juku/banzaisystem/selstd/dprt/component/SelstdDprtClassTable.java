package jp.ac.kawai_juku.banzaisystem.selstd.dprt.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 一覧出力ダイアログのクラステーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdDprtClassTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** チェックボックス列のレンダラ */
    private final BooleanRenderer booleanRenderer = new BooleanRenderer();

    /** ラベル列のレンダラ */
    private final DefaultTableCellRenderer labelRenderer = new DefaultTableCellRenderer();

    /** チェックボックス列のエディタ */
    private final DefaultCellEditor cellEditor = new DefaultCellEditor(new JCheckBox());

    /** セル更新時の処理 */
    private TableCellUpdatedAction updatedAction;

    /** クラスリスト設定時の対象模試コンボボックスアイテム */
    private ComboBoxItem<ExamBean> examItem;

    /** クラスリスト設定時の学年コンボボックスアイテム */
    private ComboBoxItem<Integer> gradeItem;

    /**
     * コンストラクタです。
     */
    public SelstdDprtClassTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        Vector<String> columnNames = CollectionsUtil.newVector();
        for (int i = 0; i <= 2; i++) {
            columnNames.add(StringUtils.EMPTY);
        }

        BZTableModel tableModel = new BZTableModel(2) {

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                /* 先頭列がチェックボックスになるようにする */
                if (columnIndex == 0) {
                    return Boolean.class;
                } else {
                    return super.getColumnClass(columnIndex);
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                /* 先頭列のみ編集可能とする */
                return column == 0;
            }

            @Override
            public void fireTableCellUpdated(int row, int column) {
                super.fireTableCellUpdated(row, column);
                updatedAction.updated();
            }

        };

        JTable table = new BZTable(tableModel) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                return c;
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setRowSelectionAllowed(false);

        TableColumnModel columnModel = table.getColumnModel();

        columnModel.getColumn(0).setPreferredWidth(20);
        columnModel.getColumn(0).setCellEditor(cellEditor);
        columnModel.getColumn(0).setCellRenderer(booleanRenderer);
        columnModel.getColumn(1).setPreferredWidth(36);
        columnModel.getColumn(1).setCellRenderer(labelRenderer);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        cellEditor.getComponent().setEnabled(enabled);
        booleanRenderer.setEnabled(enabled);
        labelRenderer.setEnabled(enabled);
    }

    /** チェックボックス列のレンダラ */
    private static final class BooleanRenderer extends JCheckBox implements TableCellRenderer {

        /**
         * コンストラクタです。
         */
        private BooleanRenderer() {
            setHorizontalAlignment(JLabel.CENTER);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            setSelected(((Boolean) value).booleanValue());
            return this;
        }

    }

    /**
     * クラスリストをセットします。
     *
     * @param examItem 対象模試コンボボックスアイテム
     * @param gradeItem 学年コンボボックスアイテム
     * @param classList クラスリスト
     */
    public final void setClassList(ComboBoxItem<ExamBean> examItem, ComboBoxItem<Integer> gradeItem, List<String> classList) {

        Set<String> selectedSet = new HashSet<>();
        if (ObjectUtils.equals(examItem, this.examItem) && ObjectUtils.equals(gradeItem, this.gradeItem)) {
            /* 対象模試と学年が前回と同じ場合に限り、選択を復元する */
            selectedSet.addAll(getSelectedClassList());
        }

        Vector<Vector<Object>> dataVector = CollectionsUtil.newVector(classList.size());
        for (String cls : classList) {
            Vector<Object> row = CollectionsUtil.newVector(2);
            row.add(Boolean.valueOf(selectedSet.contains(cls)));
            row.add(cls);
            dataVector.add(row);
        }

        ((BZTableModel) getTable().getModel()).setDataVector(dataVector);

        /* クラスリスト設定時の情報を保持しておく */
        this.examItem = examItem;
        this.gradeItem = gradeItem;
    }

    /**
     * クラスリストをセットします。
     *
     * @param examItem 対象模試コンボボックスアイテム
     * @param gradeItem 学年コンボボックスアイテム
     * @param classList クラスリスト
     */
    public final void setClassListAll(ComboBoxItem<Integer> gradeItem, List<String> classList) {

        Set<String> selectedSet = new HashSet<>();
        if (ObjectUtils.equals(examItem, this.examItem) && ObjectUtils.equals(gradeItem, this.gradeItem)) {
            /* 対象模試と学年が前回と同じ場合に限り、選択を復元する */
            selectedSet.addAll(getSelectedClassList());
        }

        Vector<Vector<Object>> dataVector = CollectionsUtil.newVector(classList.size());
        for (String cls : classList) {
            Vector<Object> row = CollectionsUtil.newVector(2);
            row.add(Boolean.valueOf(selectedSet.contains(cls)));
            row.add(cls);
            dataVector.add(row);
        }

        ((BZTableModel) getTable().getModel()).setDataVector(dataVector);

        /* クラスリスト設定時の情報を保持しておく */
        this.gradeItem = gradeItem;
    }

    /**
     * 全選択／全解除操作を行います。
     *
     * @param flg 選択フラグ
     */
    public void selectAll(boolean flg) {
        TableModel model = getTable().getModel();
        for (int rowIndex = 0; rowIndex < model.getRowCount(); rowIndex++) {
            if (!model.getValueAt(rowIndex, 0).equals(flg)) {
                model.setValueAt(Boolean.valueOf(flg), rowIndex, 0);
            }
        }
    }

    /**
     * 選択クラスリストを返します。
     *
     * @return 選択クラスリスト
     */
    public List<String> getSelectedClassList() {
        List<String> list = CollectionsUtil.newArrayList();
        TableModel model = getTable().getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if (((Boolean) model.getValueAt(i, 0)).booleanValue()) {
                list.add((String) model.getValueAt(i, 1));
            }
        }
        return list;
    }

    /** セル更新時の処理を定義するインターフェース */
    public interface TableCellUpdatedAction {

        /**
         * セル更新時の処理を行います。
         */
        void updated();

    }

    /**
     * セル更新時の処理をセットします。
     *
     * @param updatedAction セル更新時の処理
     */
    public void setUpdatedAction(TableCellUpdatedAction updatedAction) {
        this.updatedAction = updatedAction;
    }

}

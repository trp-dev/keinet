package jp.ac.kawai_juku.banzaisystem.framework.bean;

import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

/**
 *
 * 模試情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamBean implements Comparable<ExamBean> {

    /** 模試年度 */
    private String examYear;

    /** 模試コード */
    private String examCd;

    /** 模試名 */
    private String examName;

    /** 模試短縮名 */
    private String examNameAbbr;

    /** 模試種類コード */
    private String examTypeCd;

    /** 模試区分 */
    private String examDiv;

    /** 外部データ開放日 */
    private String outDataOpenDate;

    /** ドッキング先模試コード */
    private String dockingExamCd;

    /** 模試実施基準日 */
    private String inpleDate;

    /** 対象学年 */
    private int targetGrade;

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 模試名を返します。
     *
     * @return 模試名
     */
    public String getExamName() {
        return examName;
    }

    /**
     * 模試名をセットします。
     *
     * @param examName 模試名
     */
    public void setExamName(String examName) {
        this.examName = examName;
    }

    /**
     * 模試短縮名を返します。
     *
     * @return 模試短縮名
     */
    public String getExamNameAbbr() {
        return examNameAbbr;
    }

    /**
     * 模試短縮名をセットします。
     *
     * @param examNameAbbr 模試短縮名
     */
    public void setExamNameAbbr(String examNameAbbr) {
        this.examNameAbbr = examNameAbbr;
    }

    /**
     * 模試種類コードを返します。
     *
     * @return 模試種類コード
     */
    public String getExamTypeCd() {
        return examTypeCd;
    }

    /**
     * 模試種類コードをセットします。
     *
     * @param examTypeCd 模試種類コード
     */
    public void setExamTypeCd(String examTypeCd) {
        this.examTypeCd = examTypeCd;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

    /**
     * 外部データ開放日を返します。
     *
     * @return 外部データ開放日
     */
    public String getOutDataOpenDate() {
        return outDataOpenDate;
    }

    /**
     * 外部データ開放日をセットします。
     *
     * @param outDataOpenDate 外部データ開放日
     */
    public void setOutDataOpenDate(String outDataOpenDate) {
        this.outDataOpenDate = outDataOpenDate;
    }

    /**
     * ドッキング先模試コードを返します。
     *
     * @return ドッキング先模試コード
     */
    public String getDockingExamCd() {
        return dockingExamCd;
    }

    /**
     * ドッキング先模試コードをセットします。
     *
     * @param dockingExamCd ドッキング先模試コード
     */
    public void setDockingExamCd(String dockingExamCd) {
        this.dockingExamCd = dockingExamCd;
    }

    /**
     * 模試を一意に識別するキーを返します。
     *
     * @return 模試を一意に識別するキー
     */
    private String getExamKey() {
        return examYear + examCd;
    }

    /**
     * 模試実施基準日を返します。
     *
     * @return 模試実施基準日
     */
    public String getInpleDate() {
        return inpleDate;
    }

    /**
     * 模試実施基準日をセットします。
     *
     * @param inpleDate 模試実施基準日
     */
    public void setInpleDate(String inpleDate) {
        this.inpleDate = inpleDate;
    }

    /**
     * 対象学年を返します。
     *
     * @return 対象学年
     */
    public Integer getTargetGrade() {
        return targetGrade;
    }

    /**
     * 対象学年をセットします。
     *
     * @param targetGrade 対象学年
     */
    public void setTargetGrade(Integer targetGrade) {
        this.targetGrade = targetGrade;
    }

    @Override
    public int hashCode() {
        return getExamKey().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        } else {
            return getExamKey().equals(((ExamBean) obj).getExamKey());
        }
    }

    @Override
    public String toString() {
        return ExamUtil.toLabel(this);
    }

    @Override
    public int compareTo(ExamBean o) {
        return getExamKey().compareTo(o.getExamKey());
    }

}

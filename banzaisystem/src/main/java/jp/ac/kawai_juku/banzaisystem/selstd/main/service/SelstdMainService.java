package jp.ac.kawai_juku.banzaisystem.selstd.main.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.NullGreaterCellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.RatingCellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.ScoreBeanService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.dao.MainteStngDao;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainScoreBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.dao.SelstdMainDao;

import org.apache.commons.lang3.ObjectUtils;
import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.tiger.CollectionsUtil;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 *
 * 対象生徒選択画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainService {
    private static final Logger logger = Logger.getLogger(SelstdMainService.class);

    /** ExecutorService */
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /** 科目名リストマップ */
    private static final Map<String, List<String>> SUBNAME_MAP = CollectionsUtil.newHashMap();

    /** 科目コード→カラムID対応マップ */
    private static final Map<String, Map<String, String>> SUBCD_MAP = CollectionsUtil.newHashMap();

    static {
        /* XMLを読み込んで、マップを初期化する */
        try {
            ObjectMapper om = new ObjectMapper();
            HeaderJson json = om.readValue(SelstdMainService.class.getResourceAsStream("head.json"), HeaderJson.class);
            for (Head head : json.heads.head) {
                List<String> list = CollectionsUtil.newArrayList();
                Map<String, String> map = CollectionsUtil.newHashMap();
                for (HeadColumn c : head.column) {
                    list.add(c.id);
                    for (String subCd : c.subCd) {
                        map.put(subCd, c.id);
                    }
                }
                for (String examCd : head.examCd) {
                    SUBNAME_MAP.put(examCd, list);
                    SUBCD_MAP.put(examCd, map);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** 判定用成績データサービス */
    @Resource
    private ScoreBeanService scoreBeanService;

    /** 個人成績・志望大学画面のサービス */
    @Resource
    private ExmntnMainService exmntnMainService;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** DAO */
    @Resource
    private SelstdMainDao selstdMainDao;

    /** メンテナンス-環境設定画面のDAO */
    @Resource
    private MainteStngDao mainteStngDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 学年リストを取得します。
     *
     * @return 学年リスト
     */
    public List<Integer> findGradeList() {
        return selstdMainDao.selectGradeList(systemDto.getUnivYear());
    }

    /**
     * クラスリストを取得します。
     *
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> findClassList(Integer grade) {
        return selstdMainDao.selectClassList(systemDto.getUnivYear(), grade);
    }

    /**
     * 個人リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @return 個人リスト
     */
    public List<Map<String, CellData>> findKojinList(Integer grade, String cls) {

        List<SelstdMainKojinBean> list = selstdMainDao.selectKojinList(systemDto.getUnivYear(), grade, cls);

        List<Map<String, CellData>> result = CollectionsUtil.newArrayList(list.size());
        for (SelstdMainKojinBean bean : list) {
            Map<String, CellData> map = CollectionsUtil.newHashMap();
            map.put("id", new CellData(bean.getIndividualId()));
            map.put("grade", new CellData(bean.getGrade()));
            map.put("class", new NullGreaterCellData(bean.getCls()));
            map.put("number", new NullGreaterCellData(bean.getClassNo()));
            map.put("name", new CellData(bean.getNameKana()));
            result.add(map);
        }

        return result;
    }

    /**
     * 個人IDリストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @return 個人IDリスト
     */
    public List<Integer> findKojinIdList(Integer grade, String cls) {

        List<SelstdMainKojinBean> list = selstdMainDao.selectKojinList(systemDto.getUnivYear(), grade, cls);

        List<Integer> result = CollectionsUtil.newArrayList(list.size());
        for (SelstdMainKojinBean bean : list) {
            result.add(bean.getIndividualId());
        }

        return result;
    }

    /**
     * 指定した模試の科目名リストを取得します。
     *
     * @param examCd 模試コード
     * @return 科目名リスト
     */
    public List<String> findSubNameList(String examCd) {
        return SUBNAME_MAP.get(examCd);
    }

    /**
     * 成績リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 個人リスト
     */
    public List<Map<String, CellData>> findScoreList(Integer grade, String cls, ExamBean exam) {

        List<SelstdMainScoreBean> list = selstdMainDao.selectScoreList(grade, cls, exam);

        Map<String, String> subCdMap = SUBCD_MAP.get(exam.getExamCd());
        Map<Integer, Map<String, CellData>> kojinMap = CollectionsUtil.newHashMap();
        for (SelstdMainScoreBean bean : list) {

            Map<String, CellData> map = kojinMap.get(bean.getIndividualId());
            if (map == null) {
                /* 個人が切り替わった場合 */
                map = CollectionsUtil.newHashMap();
                map.put("id", new CellData(bean.getIndividualId()));
                map.put("grade", new CellData(bean.getGrade()));
                map.put("class", new NullGreaterCellData(bean.getCls()));
                map.put("number", new NullGreaterCellData(bean.getClassNo()));
                map.put("name", new CellData(bean.getNameKana()));
                /* map.put("cefr", new CellData(bean.getCefr())); */
                /* map.put("jpnDesc", new CellData(bean.getJpnDesc())); */
                kojinMap.put(bean.getIndividualId(), map);
            }

            if (bean.getSubCd() == null) {
                /* 科目成績なし */
                continue;
            } else if (bean.getSubCd().startsWith("7")) {
                /* 総合1 */
                if (!map.containsKey("synthetic1")) {
                    map.put("synthetic1", new CellData(bean.getSubName()));
                    map.put("synthetic1Score", createScore(bean.getScore(), bean.getSubAllotPnt()));
                    map.put("synthetic1Dev", new CellData(bean.getDeviation()));
                }
            } else if (bean.getSubCd().startsWith("8")) {
                /* 総合2 */
                if (!map.containsKey("synthetic2")) {
                    map.put("synthetic2", new CellData(bean.getSubName()));
                    map.put("synthetic2Dev", new CellData(bean.getDeviation()));
                }
            } else {
                /* 科目 */
                String id = subCdMap.get(bean.getSubCd());
                if (id != null) {
                    map.put(id, createScore(bean.getScore(), bean.getSubAllotPnt()));
                    map.put(id + "Dev", new CellData(bean.getDeviation()));
                }
            }
        }

        return CollectionsUtil.newArrayList(kojinMap.values());
    }

    /**
     * 得点のセルデータを生成します。
     *
     * @param score 得点
     * @param subAllotPnt 配点
     * @return セルデータ
     */
    private CellData createScore(Integer score, Integer subAllotPnt) {

        StringBuilder dispValue = new StringBuilder();

        if (score != null) {
            dispValue.append(score);
            if (subAllotPnt != null) {
                dispValue.append('/');
                dispValue.append(subAllotPnt);
            }
        }

        return new CellData(dispValue.toString(), score);
    }

    /**
     * 志望大学リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @param dockingExam 対象模試のドッキング先模試
     * @return 志望大学リスト
     */
    public List<Map<String, CellData>> findCandidateUnivList(Integer grade, String cls, ExamBean exam, ExamBean dockingExam) {

        /* 志望大学リストを取得 */
        List<SelstdMainCandidateUnivBean> candiUnivList = selstdMainDao.selectCandidateUnivList(grade, cls, exam);
        /* 成績データを取得 */
        Map<Integer, ScoreBean> scoreList = scoreBeanService.createScoreBeanList(candiUnivList, grade, cls, exam);
        /* 判定処理を実行 */
        logger.debug("◆◆判定処理取得-開始◆◆");
        /* List<SelstdMainCandidateUnivBean> resultList = judge(candiUnivList, scoreList, noExclude); */
        List<SelstdMainCandidateUnivBean> resultList = judge(candiUnivList, scoreList);
        logger.debug("◆◆判定処理取得-終了◆◆");

        List<Map<String, CellData>> result = CollectionsUtil.newArrayList(candiUnivList.size());
        for (SelstdMainCandidateUnivBean bean : resultList) {
            Map<String, CellData> map = CollectionsUtil.newHashMap();
            map.put("id", new CellData(bean.getIndividualId()));
            map.put("grade", new CellData(bean.getGrade()));
            map.put("class", new NullGreaterCellData(bean.getCls()));
            map.put("number", new NullGreaterCellData(bean.getClassNo()));
            map.put("name", new CellData(bean.getNameKana()));
            map.put("synthetic1", new CellData(bean.getSubName()));
            map.put("synthetic1Score", createScore(bean.getScore(), bean.getSubAllotPnt()));
            map.put("synthetic1Dev", new CellData(bean.getDeviation()));
            map.put("wishRank", new CellData(bean.getCandidateRank().intValue() == 0 ? null : bean.getCandidateRank()));
            map.put("university", new CellData(bean.getUnivName(), bean.getUnivNameKana()));
            map.put("faculty", new CellData(bean.getFacultyName()));
            map.put("subjectSchedule", new CellData(bean.getDeptName()));
            map.put("center", new RatingCellData(bean.getCenterRating()));
            map.put("exclude", new RatingCellData(bean.getValuateExclude()));
            /*
            if (!ExamUtil.isCenter((ExamUtil.isMark(exam) ? exam : dockingExam))) {
                map.put("exclude", new RatingCellData(bean.getValuateExclude()));
                //含まない評価
                if (!noExclude && !StringUtil.isEmptyTrim(bean.getAppReqPaternCd())) {
                    map.put("include", new RatingCellData(bean.getValuateInclude())); //含む評価
                } else {
                    map.put("include", new RatingCellData("")); //含む評価
                }
            } else {
                if (!StringUtil.isEmptyTrim(bean.getAppReqPaternCd())) {
                    map.put("exclude", new RatingCellData(""));
                    //含まない評価
                    map.put("include", new RatingCellData(bean.getValuateInclude())); //含む評価
                } else {
                    map.put("exclude", new RatingCellData(bean.getValuateExclude()));
                    //含まない評価
                    map.put("include", new RatingCellData("")); //含む評価
                }
            }
            */
            map.put("secondStage", new RatingCellData(bean.getSecondRating()));
            map.put("synthetic", new RatingCellData(bean.getTotalRating()));
            map.put("univCd", new CellData(bean.getUnivCd()));
            result.add(map);
        }

        return result;
    }

    /**
     * 志望大学リストに対して判定を実行します。
     *
     * @param candiUnivList 志望大学リスト
     * @param scoreList 成績データリスト
     * @return 判定結果
    */
    private List<SelstdMainCandidateUnivBean> judge(List<SelstdMainCandidateUnivBean> candiUnivList, Map<Integer, ScoreBean> scoreList) {
        List<SelstdMainCandidateUnivBean> resultList = new ArrayList<SelstdMainCandidateUnivBean>();
        List<JudgementTask> tasks = new ArrayList<JudgementTask>();

        for (SelstdMainCandidateUnivBean univBean : candiUnivList) {
            /* 個人のスコア情報を取得 */
            ScoreBean score = scoreList.get(univBean.getIndividualId());
            /* 判定処理のタスクを登録 */
            /* tasks.add(new JudgementTask(score, univBean, noExclude)); */
            tasks.add(new JudgementTask(score, univBean));
        }

        try {
            /* 一括処理を実行 */
            for (Future<SelstdMainCandidateUnivBean> future : EXECUTOR.invokeAll(tasks)) {
                resultList.add(future.get());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return resultList;
    }

    @Data
    private static class HeaderJson {
        private Heads heads;
    }

    /** ヘッダリストを保持するクラス */
    @Data
    private static class Heads {

        /** ヘッダリスト */
        private List<Head> head;

    }

    /** ヘッダ */
    @Data
    private static class Head {

        /** 模試コードセット */
        private List<String> examCd;

        /** ヘッダカラムリスト */
        private List<HeadColumn> column;

    }

    /** ヘッダカラム */
    @Data
    private static class HeadColumn {

        /** ID */
        private String id;

        /** 科目コードセット */
        private List<String> subCd;

    }

    /**
     * 生徒を削除します。
     *
     * @param idList 個人IDリスト
     */
    @Lock
    public void deleteStudent(List<Integer> idList) {

        /* 1000件を超える場合を考慮して変換しておく */
        BeanMap param = new BeanMap();
        param.put("idCondition", DaoUtil.createIn("INDIVIDUALID", idList));

        /* ログ出力用に削除対象の生徒情報を取得しておく */
        Map<Integer, ExmntnMainKojinBean> kojinMap = new HashMap<>();
        for (ExmntnMainKojinBean bean : exmntnMainService.findKojinList(idList)) {
            kojinMap.put(bean.getIndividualId(), bean);
        }

        /* 受験予定大学を削除する */
        selstdMainDao.deleteExamPlanUniv(param);

        /* 志望大学を削除する */
        selstdMainDao.deleteCandidateUniv(param);

        /* 模試記入志望大学データを削除する */
        selstdMainDao.deleteCandidateRating(param);

        /* 個表データを削除する */
        selstdMainDao.deleteIndividualRecord(param);

        /* 成績データを削除する */
        selstdMainDao.deleteSubRecord(param);

        /* 学籍履歴情報を削除する */
        selstdMainDao.deleteHistoryInfo(param);

        /* 学籍基本情報を削除する */
        selstdMainDao.deleteBasicInfo(param);

        /* 個人成績インポート状況を削除する */
        mainteStngDao.deleteStatSubRecord();

        /* ログを出力する */
        List<String> contents = new ArrayList<>(kojinMap.size() * 4);
        for (Integer id : idList) {
            ExmntnMainKojinBean bean = kojinMap.get(id);
            if (bean != null) {
                contents.add(id.toString());
                contents.add(ObjectUtils.toString(bean.getGrade()));
                contents.add(ObjectUtils.toString(bean.getCls()));
                contents.add(ObjectUtils.toString(bean.getClassNo()));
            }
        }
        logService.output(GamenId.B1, contents.toArray(new String[contents.size()]));
    }

}

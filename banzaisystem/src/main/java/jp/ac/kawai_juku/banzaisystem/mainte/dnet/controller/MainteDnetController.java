package jp.ac.kawai_juku.banzaisystem.mainte.dnet.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.REQUIRED_MASTER_VERSION;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.REQUIRED_RECORD_VERSION;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.VERSION_NUMBER;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.event.ActionEvent;
import java.io.File;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.InputFileChooser;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.UnivCdHookUpService;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.dnet.service.MainteDnetService;
import jp.ac.kawai_juku.banzaisystem.mainte.dnet.view.MainteDnetView;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngFolderField;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngMoveButton;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngRefButton;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.helper.MainteStngHelper;

/**
 *
 * データフォルダ設定ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class MainteDnetController extends AbstractController {

    /** View */
    @Resource(name = "mainteDnetView")
    private MainteDnetView view;

    /** Service */
    @Resource(name = "mainteDnetService")
    private MainteDnetService service;

    /** メンテナンス-大学マスタ・プログラム最新化画面のサービス */
    @Resource
    private MainteMstrService mainteMstrService;

    /** 初期化処理サービス */
    @Resource
    private InitializeService initializeService;

    /** 大学コード紐付けサービス */
    @Resource
    private UnivCdHookUpService univCdHookUpService;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 初期表示アクションです。
     *
     * @return ネットワーク利用時ダイアログ
     */
    @Logging(GamenId.E4_d1)
    @Execute
    public ExecuteResult index() {
        initDataFolder();
        return VIEW_RESULT;
    }

    /**
     * データフォルダを初期化します。
     */
    private void initDataFolder() {
        view.initFolderButton.setEnabled(false);
        for (MainteStngMoveButton b : view.findComponentsByClass(MainteStngMoveButton.class)) {
            b.setEnabled(false);
        }
        for (MainteStngFolderField f : view.findComponentsByClass(MainteStngFolderField.class)) {
            f.setText(SettingPropsUtil.getDirectory(f.getDataFolder()).getAbsolutePath());
        }
    }

    /**
     * 「初期値に戻す」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult initFolderButtonAction() {
        initDataFolder();
        return null;
    }

    /**
     * 「参照」ボタン押下アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult refButtonAction(ActionEvent e) {

        DataFolder folder = findDataFolder(e);
        MainteStngHelper helper = new MainteStngHelper(view, folder);
        File currentFolder = SettingPropsUtil.getDirectory(folder);
        TextField field = helper.findFolderField();
        ImageButton moveButton = helper.findMoveButton();

        JFileChooser fileChooser = new InputFileChooser(field.getText());
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        int selected = fileChooser.showOpenDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            File newFolder = fileChooser.getSelectedFile();
            if (newFolder.isDirectory()) {
                field.setText(newFolder.getAbsolutePath());
                moveButton.setEnabled(!IOUtil.isIdentical(currentFolder, newFolder));
                changeInitButtonStatus();
            }
        }

        return null;
    }

    /**
     * 発生したボタンイベントに紐付くデータフォルダを返します。
     *
     * @param e {@link ActionEvent}
     * @return {@link DataFolder}
     */
    private DataFolder findDataFolder(ActionEvent e) {
        return ((MainteStngRefButton) e.getSource()).getDataFolder();
    }

    /**
     * 「初期値に戻す」ボタンの状態を変更します。
     */
    private void changeInitButtonStatus() {
        /* 移動ボタンが1つでも有効なら、初期値に戻すボタンも有効にする */
        for (MainteStngMoveButton b : view.findComponentsByClass(MainteStngMoveButton.class)) {
            if (b.isEnabled()) {
                view.initFolderButton.setEnabled(true);
                return;
            }
        }
        view.initFolderButton.setEnabled(false);
    }

    /**
     * 「移動」ボタン押下アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult moveButtonAction(ActionEvent e) {

        DataFolder folder = findDataFolder(e);
        MainteStngHelper helper = new MainteStngHelper(view, folder);
        File newFolder = new File(helper.findFolderField().getText());

        /* 移動先データフォルダをチェックする */
        checkNewFolder(folder, newFolder);

        /* 上書き確認ダイアログを表示する */
        int selected = showDialog(folder, newFolder);
        if (selected != JOptionPane.YES_OPTION && selected != JOptionPane.NO_OPTION) {
            return null;
        }

        /* 大学マスタの参照先が変わる場合は、大学マスタファイルの妥当性をチェックする */
        if (folder == DataFolder.UNIV_FOLDER && selected == JOptionPane.NO_OPTION) {
            checkUnivMaster(folder, newFolder);
        }

        /* 成績データの参照先が変わる場合は、成績データファイルの妥当性をチェックする */
        if (folder == DataFolder.RECORD_FOLDER && selected == JOptionPane.NO_OPTION) {
            checkRecord(folder, newFolder);
        }

        /* フォルダを移動する */
        service.moveFolder(folder, newFolder, selected == JOptionPane.YES_OPTION);

        /* コンポーネントの状態を変更する */
        helper.findMoveButton().setEnabled(false);
        changeInitButtonStatus();

        /* 大学マスタまたは成績データの参照先が変わる場合 */
        if ((folder == DataFolder.UNIV_FOLDER || folder == DataFolder.RECORD_FOLDER)
                && selected == JOptionPane.NO_OPTION) {
            /* 選択中の生徒をクリアする */
            systemDto.clearSelectedIndividualIdList();
            /* 大学マスタの参照先が変わる場合は、マスタ情報を初期化する */
            if (folder == DataFolder.UNIV_FOLDER) {
                initializeService.initMaster();
                logService.output(GamenId.E4_d1, systemDto.getUnivYear(), systemDto.getUnivExamDiv());
            }
            /* 成績データの参照先が変わる場合は、成績データ情報を初期化する */
            if (folder == DataFolder.RECORD_FOLDER) {
                initializeService.initRecordInfo();
            }
            /* 紐付けを行う */
            if (univCdHookUpService.needsHookUp()) {
                return new TaskResult() {
                    @Override
                    protected ExecuteResult doTask() {
                        univCdHookUpService.hookUp();
                        return null;
                    }
                };
            }
        }

        return null;
    }

    /**
     * 移動先データフォルダのチェックを行います。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     */
    private void checkNewFolder(DataFolder folder, File newFolder) {
        if (!newFolder.isDirectory()) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E006"));
        }
        if (!IOUtil.isWritableDirectory(newFolder)) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E009"));
        }
    }

    /**
     * 上書き確認ダイアログを表示します。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     * @return ダイアログ選択値
     */
    private int showDialog(DataFolder folder, File newFolder) {
        if (folder.getDataFileName() != null && new File(newFolder, folder.getDataFileName()).exists()) {
            String[] button =
                    { getMessage("label.mainte.dnet.dialogYes"), getMessage("label.mainte.dnet.dialogNo"),
                            getMessage("label.mainte.dnet.dialogCancel") };
            return JOptionPane.showOptionDialog(getActiveWindow(), getMessage("label.mainte.dnet.E_W001"), null,
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        } else {
            return JOptionPane.YES_OPTION;
        }
    }

    /**
     * 大学マスタファイルの妥当性をチェックします。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     */
    private void checkUnivMaster(DataFolder folder, File newFolder) {

        /* 大学マスタ情報を取得する */
        UnivMasterBean univMaster;
        try {
            univMaster = mainteMstrService.getUnivMasterBean(new File(newFolder, folder.getDataFileName()));
        } catch (SQLException e) {
            throw new MessageDialogException(getMessage("error.mainte.dnet.E_E014", e));
        }

        /* 現在の大学マスタよりも古ければエラーとする */
        String masterVersion = univMaster.getEventYear() + univMaster.getExamDiv();
        if (masterVersion.compareTo(systemDto.getUnivYear() + systemDto.getUnivExamDiv()) < 0) {
            throw new MessageDialogException(getMessage("error.mainte.dnet.E_E015"));
        }

        /* バージョンに不整合があるならエラーとする */
        if (masterVersion.compareTo(REQUIRED_MASTER_VERSION) < 0
                || VERSION_NUMBER < Double.parseDouble(univMaster.getRequiredVersion())) {
            throw new MessageDialogException(getMessage("error.mainte.dnet.CM_E015"));
        }
    }

    /**
     * 成績データファイルの妥当性をチェックします。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     */
    private void checkRecord(DataFolder folder, File newFolder) {

        /* 成績データバージョンを取得する */
        int version;
        try {
            version = mainteMstrService.getRecordVersion(new File(newFolder, folder.getDataFileName()));
        } catch (SQLException e) {
            throw new MessageDialogException(getMessage("error.mainte.dnet.E_E016", e));
        }

        /* 必要データファイルバージョンと不一致ならエラーとする */
        if (REQUIRED_RECORD_VERSION != version) {
            throw new MessageDialogException(getMessage("error.mainte.dnet.CM_E018"));
        }
    }

    /**
     * 閉じるボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult closeButtonAction() {
        return CLOSE_RESULT;
    }

}

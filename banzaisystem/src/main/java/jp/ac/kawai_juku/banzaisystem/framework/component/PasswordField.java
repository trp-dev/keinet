package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.lang.reflect.Field;

import javax.swing.JPasswordField;
import javax.swing.border.MatteBorder;
import javax.swing.text.AbstractDocument;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * パスワードフィールドです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class PasswordField extends JPasswordField implements BZComponent {

    /**
     * コンストラクタです。
     */
    public PasswordField() {
        setBorder(new MatteBorder(1, 1, 1, 1, new Color(204, 204, 204)));
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        if (!field.isAnnotationPresent(NoAction.class)) {
            /* Enterイベントのリスナを登録する */
            addActionListener(new ControllerActionListener(view, field.getName()));
        }

        MaxLength maxLength = field.getAnnotation(MaxLength.class);
        if (maxLength != null) {
            ((AbstractDocument) getDocument()).setDocumentFilter(new MaxLengthDocumentFilter(maxLength.value()));
        }
    }

    /**
     * パスワード文字列を返します。
     *
     * @return パスワード文字列
     */
    public final String getStringPassword() {
        return new String(getPassword());
    }

}

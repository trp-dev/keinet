package jp.ac.kawai_juku.banzaisystem.mainte.mtch.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.mainte.mtch.controller.MainteMtchController;

import org.apache.commons.lang3.ArrayUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 全模試結合状況参照テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MainteMtchStatusTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** ヘッダのボーダー色 */
    private static final Color HEADER_BORDER_COLOR = new Color(153, 153, 153);

    /** 模試名の色 */
    private static final Color EXAM_NAME_COLOR = new Color(49, 79, 129);

    /** 固定領域テーブルのカラム幅定義 */
    private static final int[] COL_WIDTH = { 68, 65, 68, 154 };

    /** 模試名のカラム幅定義 */
    private static final int EXAM_COL_WIDTH = 140;

    /**
     * コンストラクタです。
     */
    public MainteMtchStatusTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setCorner(JScrollPane.UPPER_LEFT_CORNER, createFixedColumnHeaderView());
        setRowHeaderView(createFixedTableView());
        Vector<Vector<Object>> dataVector = CollectionsUtil.newVector();
        for (int i = 0; i < 50; i++) {
            Vector<Object> data = CollectionsUtil.newVector();
            for (int j = 0; j < getFixedTable().getColumnCount() + 1; j++) {
                data.add(j);
            }
            dataVector.add(data);
        }
        ((ReadOnlyTableModel) getFixedTable().getModel()).setDataVector(dataVector);
    }

    /**
     * 固定領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createFixedColumnHeaderView() {
        ImagePanel panel = new ImagePanel(getClass(), "table/status.png");
        return panel;
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @param examList 模試リスト
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView(List<ExamBean> examList) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        for (ExamBean exam : examList) {
            JLabel label = new JLabel(exam.getExamName());
            label.setOpaque(true);
            label.setForeground(EXAM_NAME_COLOR);
            label.setBackground(SEPARATOR_COLOR);
            label.setPreferredSize(new Dimension(EXAM_COL_WIDTH, 26));
            label.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, HEADER_BORDER_COLOR));
            label.setHorizontalAlignment(SwingConstants.CENTER);
            panel.add(label);
        }
        return panel;
    }

    /**
     * 固定領域のテーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createFixedTableView() {

        JTable table = new BZTable(new ReadOnlyTableModel(COL_WIDTH.length + 1)) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int width : COL_WIDTH) {
                    x += width;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.addMouseListener(new MouseListener());

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        for (int i = 0; i < COL_WIDTH.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(COL_WIDTH[i]);
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * スクロール領域のテーブルを生成します。
     *
     * @param examList 模試リスト
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final List<ExamBean> examList) {

        JTable table = new BZTable(new ReadOnlyTableModel(examList.size())) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int i = 0; i < examList.size(); i++) {
                    x += EXAM_COL_WIDTH;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.addMouseListener(new MouseListener());

        TableColumnModel columnModel = table.getColumnModel();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            columnModel.getColumn(i).setPreferredWidth(EXAM_COL_WIDTH);
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /** ダブルクリック検知用のマウスリスナ */
    private static final class MouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 2) {
                /*
                JTable table = (JTable) e.getSource();
                int row = table.rowAtPoint(e.getPoint());
                */
                SingletonS2Container.getComponent(MainteMtchController.class).showDialogAction();
            }
        }
    }

    /**
     * 模試リストをセットします。
     *
     * @param examList 模試リスト
     */
    public void setExamList(List<ExamBean> examList) {
        /* スクロール領域を初期化する */
        setColumnHeaderView(createColumnHeaderView(examList));
        setViewportView(createTableView(examList));
        getTable().setSelectionModel(getFixedTable().getSelectionModel());
        Vector<Vector<Object>> dataVector = CollectionsUtil.newVector();
        for (int i = 0; i < 50; i++) {
            Vector<Object> data = CollectionsUtil.newVector();
            for (int j = 0; j < getTable().getColumnCount(); j++) {
                data.add(j);
            }
            dataVector.add(data);
        }
        ((ReadOnlyTableModel) getTable().getModel()).setDataVector(dataVector);
    }

}

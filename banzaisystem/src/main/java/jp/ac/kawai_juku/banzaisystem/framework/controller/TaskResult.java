package jp.ac.kawai_juku.banzaisystem.framework.controller;

import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

/**
 *
 * タスクを実行するアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class TaskResult implements ExecuteResult {

    /**
     * バックグラウンド実行するタスクを実装します。
     *
     * @return 実行後のアクション
     */
    protected abstract ExecuteResult doTask();

    /**
     * タスク実行後にイベントディスパッチスレッドで実行する処理を実装します。
     */
    protected void done() {
    }

    @Override
    public final void process(final AbstractController controller) {
        new SwingWorker<ExecuteResult, Object>() {

            @Override
            protected ExecuteResult doInBackground() throws Exception {
                WindowManager.getInstance().startWait();
                return doTask();
            }

            @Override
            protected void done() {
                try {
                    ExecuteResult result = get();
                    TaskResult.this.done();
                    if (result != null) {
                        result.process(controller);
                    }
                } catch (ExecutionException | InterruptedException e) {
                    throw new RuntimeException(e);
                } finally {
                    WindowManager.getInstance().stopWait();
                }
            }
        }.execute();
    }

    /**
     * 指定したサブ画面をアクティブにします。
     *
     * @param clazz アクティブにするサブ画面のコントローラ
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult activate(Class<? extends SubController> clazz) {
        return new ActivateResult(clazz, null);
    }

}

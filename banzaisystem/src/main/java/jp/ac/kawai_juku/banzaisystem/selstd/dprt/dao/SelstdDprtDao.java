package jp.ac.kawai_juku.banzaisystem.selstd.dprt.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 一覧出力ダイアログのDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDprtDao extends BaseDao {

    /**
     * 学年リストを取得します。
     *
     * @param exam 対象模試
     * @return 学年リスト
     */
    public List<Integer> selectGradeList(ExamBean exam) {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), exam).getResultList();
    }

    /**
     * 学年リスト(全模試)を取得します。
     *
     * @param year 対象年
     * @return 学年リスト
     */
    public List<Integer> selectGradeListAll(String year) {
        BeanMap param = new BeanMap();
        param.put("examYear", year);
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getResultList();
    }

    /**
     * クラスリストを生成します。
     *
     * @param exam 対象模試
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> selectClassList(ExamBean exam, Integer grade) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("grade", grade);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }

    /**
     * クラスリスト(全模試)を生成します。
     *
     * @param exam 対象模試
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> selectClassListAll(String year, Integer grade) {
        BeanMap param = new BeanMap();
        param.put("examYear", year);
        param.put("grade", grade);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }
}

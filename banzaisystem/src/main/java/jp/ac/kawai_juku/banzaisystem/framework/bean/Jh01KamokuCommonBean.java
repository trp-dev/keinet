package jp.ac.kawai_juku.banzaisystem.framework.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
*
 * 情報誌用科目2と3の共通情報を保持するBeanです。
*
*
 * @author QQ)Kurimoto
*
*/
@Getter
@Setter
@ToString
public class Jh01KamokuCommonBean {
    private String scTrust;
    private String totalSc;
    private String gaiTrustPt;
    private String gaiShoPt;
    private String gaiShoSel;
    private String gaiDaiPt;
    private String gaiDaiSel;
    private String engCertApp;
    private String engCertTrustPt;
    private String engCertPt;
    private String engCertAddPt;
    private String engCertAddPtType;
    private String engCertSel;
    private String mathTrustPt;
    private String mathShoPt;
    private String mathShoSel;
    private String mathDaiPt;
    private String mathDaiSel;
    private String kokugoTrustPt;
    private String kokugoShoPt;
    private String kokugoShoSel;
    private String kokugoDaiPt;
    private String kokugoDaiSel;
    private String rikaTrustPt;
    private String rikaShoPt;
    private String rikaShoSel;
    private String rikaDaiPt;
    private String rikaDaiSel;
    private String societyTrustPt;
    private String societyShoPt;
    private String societyShoSel;
    private String societyDaiPt;
    private String societyDaiSel;
    private String subName;
}

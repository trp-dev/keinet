package jp.ac.kawai_juku.banzaisystem.framework.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
*
* 情報誌用科目3の情報を保持するBeanです。
*
*
 * @author QQ)Kurimoto
*
*/
@Getter
@Setter
@ToString
public class Jh01Kamoku3Bean extends Jh01KamokuCommonBean {
    private String combiKey;
    private String essay;
    private String essayTrustPt;
    private String essayPt;
    private String essayHissu;
    private String total;
    private String totalTrustPt;
    private String totalPt;
    private String totalHissu;
    private String interview;
    private String interviewTrustPt;
    private String interviewPt;
    private String interviewHissu;
    private String plactical;
    private String placticalTrustPt;
    private String placticalPt;
    private String placticalHissu;
    private String boki;
    private String bokiTrustPt;
    private String bokiPt;
    private String bokiHissu;
    private String info;
    private String infoTrustPt;
    private String infoPt;
    private String infoHissu;
    private String research;
    private String researchTrustPt;
    private String researchPt;
    private String researchHissu;
    private String hoka;
    private String hokaTrustPt;
    private String hokaPt;
    private String hokaHissu;
    private String hoka2TrustPt;
    private String hoka2Pt;
}

package jp.ac.kawai_juku.banzaisystem.framework.properties;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;

import org.seasar.framework.util.PropertiesUtil;

/**
 *
 * プロパティファイルの読み書きクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class PropertiesIO {

    /** プロパティファイルのファイルオブジェクト */
    private final File propsFile;

    /** ロードしたプロパティファイル */
    private Properties props;

    /** ロードしたプロパティファイルの最終更新日時 */
    private long lastModified = -1;

    /**
     * コンストラクタです。
     *
     * @param propsFile プロパティファイルのファイルオブジェクト
     */
    PropertiesIO(File propsFile) {
        this.propsFile = propsFile;
    }

    /**
     * プロパティファイルを読み込みます。
     *
     * @return プロパティファイル
     * @throws IOException IO例外
     */
    synchronized Properties load() throws IOException {

        if (lastModified == propsFile.lastModified()) {
            /* プロパティファイルに変更が無ければキャッシュを返す */
            return props;
        }

        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(propsFile));
            props = new Properties();
            PropertiesUtil.load(props, in);
        } finally {
            IOUtil.close(in);
        }

        lastModified = propsFile.lastModified();

        return props;
    }

    /**
     * プロパティファイルを保存します。
     *
     * @param props プロパティファイル
     * @throws IOException IO例外
     */
    synchronized void store(Properties props) throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(propsFile));
            props.store(out, propsFile.getName());
        } finally {
            IOUtil.close(out);
        }
        lastModified = propsFile.lastModified();
    }

    /**
     * プロパティファイルのファイルオブジェクトを返します。
     *
     * @return プロパティファイルのファイルオブジェクト
     */
    File getPropsFile() {
        return propsFile;
    }

}

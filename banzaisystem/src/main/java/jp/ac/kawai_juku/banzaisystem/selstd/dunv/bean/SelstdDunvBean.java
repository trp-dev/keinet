package jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean;

/**
 *
 * 大学選択ダイアログの大学名を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDunvBean implements Comparable<SelstdDunvBean> {

    /** 大学コード */
    private String univCd;

    /** 大学名 */
    private String univName;

    /** 大学カナ名 */
    private String univNameKana;

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 大学コードを返します。
     *
     * @return 大学コード
     */
    public String getUnivCd() {
        return univCd;
    }

    /**
     * 大学コードをセットします。
     *
     * @param univCd 大学コード
     */
    public void setUnivCd(String univCd) {
        this.univCd = univCd;
    }

    /**
     * 大学カナ名を返します。
     *
     * @return 大学カナ名
     */
    public String getUnivNameKana() {
        return univNameKana;
    }

    /**
     * 大学カナ名をセットします。
     *
     * @param univNameKana 大学カナ名
     */
    public void setUnivNameKana(String univNameKana) {
        this.univNameKana = univNameKana;
    }

    @Override
    public int hashCode() {
        return univCd.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        } else {
            return univCd.equals(((SelstdDunvBean) obj).univCd);
        }
    }

    @Override
    public int compareTo(SelstdDunvBean o) {
        return univCd.compareTo(o.univCd);
    }

}

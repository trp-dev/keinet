package jp.ac.kawai_juku.banzaisystem.mainte.stng.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.DataFolder;

/**
 *
 * コンポーネントにデータフォルダを設定するためのアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface DataFolderConfig {

    /**
     * データフォルダを設定します。
     *
     * @return データフォルダ
     */
    DataFolder value();

}

package jp.ac.kawai_juku.banzaisystem.selstd.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101Bean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101CandidateBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101SubjectBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0201Bean;

import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 対象生徒選択画面のCSV出力処理のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainCsvDao extends BaseDao {

    /**
     * 個人成績CSVのデータリストを取得します。
     *
     * @param exam 対象模試
     * @param idList 個人IDリスト
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectB0101List(ExamBean exam, List<Integer> idList,
            IterationCallback<SelstdMainB0101Bean, Object> callback) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("idCondition", DaoUtil.createIn("R.INDIVIDUALID", idList));
        jdbcManager.selectBySqlFile(SelstdMainB0101Bean.class, getSqlPath(), param).iterate(callback);
    }

    /**
     * 志望大学CSVのデータリストを取得します。
     *
     * @param exam 対象模試
     * @param idList 個人IDリスト
     * @param callback 結果を処理するコールバッククラス
     */
    public void selectB0201List(ExamBean exam, List<Integer> idList,
            IterationCallback<SelstdMainB0201Bean, Object> callback) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("idCondition", DaoUtil.createIn("B.INDIVIDUALID", idList));
        jdbcManager.selectBySqlFile(SelstdMainB0201Bean.class, getSqlPath(), param).iterate(callback);
    }

    /**
     * 科目成績リストを取得します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @return 科目成績リスト
     */
    public List<SelstdMainB0101SubjectBean> selectSubjectRecordList(ExamBean exam, Integer id) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(SelstdMainB0101SubjectBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 志望大学リストを取得します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @return 科目成績リスト
     */
    public List<SelstdMainB0101CandidateBean> selectCandidateRecordList(ExamBean exam, Integer id) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(SelstdMainB0101CandidateBean.class, getSqlPath(), param).getResultList();
    }

}

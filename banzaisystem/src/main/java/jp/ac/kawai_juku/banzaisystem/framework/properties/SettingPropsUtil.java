package jp.ac.kawai_juku.banzaisystem.framework.properties;

import java.io.File;
import java.io.IOException;

import jp.ac.kawai_juku.banzaisystem.DataFolder;

/**
 *
 * 設定ファイルに関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SettingPropsUtil {

    /**
     * コンストラクタです。
     */
    private SettingPropsUtil() {
    }

    /**
     * 大学マスタデータファイルディレクトリを返します。
     *
     * @return 大学マスタデータファイルディレクトリ
     */
    public static File getUnivDir() {
        return getDirectory(DataFolder.UNIV_FOLDER);
    }

    /**
     * 成績データファイルディレクトリを返します。
     *
     * @return 成績データファイルディレクトリ
     */
    public static File getRecordDir() {
        return getDirectory(DataFolder.RECORD_FOLDER);
    }

    /**
     * 出力ファイルディレクトリを返します。
     *
     * @return 出力ファイルディレクトリ
     * @throws IOException ディレクトリに生成に失敗したとき
     */
    public static File getOutputDir() throws IOException {
        File dir = getDirectory(DataFolder.OUTPUT_FOLDER);
        createDir(dir);
        return dir;
    }

    /**
     * ログファイルディレクトリを返します。
     *
     * @return 出力ファイルディレクトリ
     * @throws IOException ディレクトリに生成に失敗したとき
     */
    public static File getLogDir() throws IOException {
        File dir = getDirectory(DataFolder.LOG_FOLDER);
        createDir(dir);
        return dir;
    }

    /**
     * 環境設定ファイルディレクトリを返します。
     *
     * @return 環境設定ファイルディレクトリ
     */
    public static File getConfigDir() {
        return getDirectory(DataFolder.CONFIG_FOLDER);
    }

    /**
     * 指定したデータフォルダのディレクトリを返します。
     *
     * @param folder データフォルダ
     * @return ディレクトリ
     */
    public static File getDirectory(DataFolder folder) {
        SettingProps props = SettingProps.load();
        String networkFolder = props.getValue(folder.getNetworkKey());
        if (!networkFolder.isEmpty() && props.getFlagValue(SettingKey.NETWORK_SETTING)) {
            return new File(networkFolder);
        } else {
            return new File(props.getValue(folder.getLocalKey()));
        }
    }

    /**
     * ディレクトリが存在しない場合に生成します。
     *
     * @param dir ディレクトリ
     * @throws IOException ディレクトリに生成に失敗したとき
     */
    private static void createDir(File dir) throws IOException {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new IOException("ディレクトリの生成に失敗しました。" + dir);
        }
    }

}

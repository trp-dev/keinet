package jp.ac.kawai_juku.banzaisystem.result.univ.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.result.dfcl.controller.ResultDfclController;
import jp.ac.kawai_juku.banzaisystem.result.univ.service.ResultUnivService;
import jp.ac.kawai_juku.banzaisystem.result.univ.view.ResultUnivView;

/**
 *
 * 入試結果入力-大学指定(一覧)画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultUnivController extends SubController {

    /** View */
    @Resource(name = "resultUnivView")
    private ResultUnivView view;

    /** サービス */
    @Resource(name = "resultUnivService")
    private ResultUnivService service;

    @Override
    public void activatedAction() {
    }

    /**
     * 検索ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult searchResultUnivButtonAction() {
        if (!view.univNameField.getText().isEmpty()) {
            view.univListTable.setDataList(service.findUnivList(view.univNameField.getText()));
        }
        return null;
    }

    /**
     * クリアボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult clearUnivNameButtonAction() {
        view.univNameField.setText(null);
        return null;
    }

    /**
     * 受験者表示ボタン押下アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult dispExamineeButtonAction() {
        view.examineeTable.setDataList(service.findExamineeList(view.univListTable.getSelectedUnivCdList()));
        return null;
    }

    /**
     * 学部学科変更ボタン押下アクションです。
     *
     * @return 学部学科選択ダイアログ
     */
    @Execute
    public ExecuteResult changeButtonAction() {
        return forward(ResultDfclController.class, null, view.examineeTable.getExamineeBean());
    }

}

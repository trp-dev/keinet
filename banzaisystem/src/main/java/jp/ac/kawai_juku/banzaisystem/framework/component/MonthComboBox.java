package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 月コンボボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MonthComboBox extends ComboBox<Integer> {

    /** ペアの日コンボボックス */
    private ComboBox<Integer> dateComboBox;

    @Override
    public void initialize(Field field, AbstractView view) {

        super.initialize(field, view);

        /* アイテムリストを設定する */
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList(13);
        for (int m = 4; m <= 12; m++) {
            itemList.add(new ComboBoxItem<Integer>(Integer.valueOf(m)));
        }
        for (int m = 1; itemList.size() < 12; m++) {
            itemList.add(new ComboBoxItem<Integer>(Integer.valueOf(m)));
        }
        itemList.add(0, new ComboBoxItem<Integer>(null));
        setItemList(itemList);

        /* 値変更時に、日コンボボックスを初期化する */
        addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (getSelectedValue() == null) {
                        /* 未選択の場合 */
                        dateComboBox.removeAllItems();
                    } else {
                        /* 末日を取得する */
                        Calendar c = Calendar.getInstance();
                        c.set(getYear(), getSelectedValue().intValue() - 1, 1);
                        int lastDay = c.getActualMaximum(Calendar.DATE);

                        /* 変更前の値を保持しておく */
                        Integer oldValue = dateComboBox.getSelectedValue();

                        /* アイテムリストを生成してセットする */
                        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList(lastDay + 1);
                        for (int d = 1; d <= lastDay; d++) {
                            itemList.add(new ComboBoxItem<Integer>(Integer.valueOf(d)));
                        }
                        dateComboBox.setItemList(itemList);

                        /* 変更前の日が消滅していたら末日にする */
                        if (oldValue != null && oldValue > 1 && dateComboBox.getSelectedIndex() == 0) {
                            dateComboBox.setSelectedIndexSilent(dateComboBox.getItemCount() - 1);
                        }
                    }
                }
            }
        });
    }

    /**
     * ペアの日コンボボックスを返します。
     *
     * @return ペアの日コンボボックス
     */
    public ComboBox<Integer> getDateComboBox() {
        return dateComboBox;
    }

    /**
     * ペアの日コンボボックスをセットします。
     *
     * @param dateComboBox ペアの日コンボボックス
     */
    public void setDateComboBox(ComboBox<Integer> dateComboBox) {
        this.dateComboBox = dateComboBox;
    }

    /**
     * 月日コンボボックスのペアが表す日付をYYYYMMDDで返します。<br>
     * 月が未選択の場合はnullを返します。<br>
     *
     * @return YYYYMMDD
     */
    public String toYYYYMMDD() {
        if (getSelectedIndex() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(8);
        sb.append(getYear());
        sb.append(StringUtils.leftPad(getSelectedItem().getLabel(), 2, '0'));
        sb.append(StringUtils.leftPad(dateComboBox.getSelectedItem().getLabel(), 2, '0'));
        return sb.toString();
    }

    /**
     * 年を返します。
     *
     * @return 年
     */
    private int getYear() {
        SystemDto dto = SingletonS2Container.getComponent(SystemDto.class);
        int nendo = dto.getNendo().intValue();
        /* 1〜3月は翌年にする */
        return getSelectedValue().intValue() <= 3 ? nendo + 1 : nendo;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.subs.view;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.controller.ExmntnEnteController;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.controller.ExmntnFcltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.controller.ExmntnNameController;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.component.UnivSearchTabbedPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeRadio;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 大学検索画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsView extends SubView {

    /** グループ番号：入試日程(私立・短大以外) */
    public static final int GROUP_EXCEPT_PRIVATE = 1;

    /** グループ番号：入試日程(私立・短大) */
    public static final int GROUP_PRIVATE = 2;

    /** 区分-国立チェックボックス */
    @Location(x = 61, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_NATIONAL)
    @Action("univDivCheckBox")
    @Group(GROUP_EXCEPT_PRIVATE)
    public CodeCheckBox divNationalCheckBox;

    /** 区分-公立チェックボックス */
    @Location(x = 115, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_PUBLIC)
    @Action("univDivCheckBox")
    @Group(GROUP_EXCEPT_PRIVATE)
    public CodeCheckBox divPublicCheckBox;

    /** 区分-私立センターチェックボックス */
    @Location(x = 168, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_PRIVATE_C)
    @Action("univDivCheckBox")
    @Group(GROUP_PRIVATE)
    public CodeCheckBox divPrivateCenterCheckBox;

    /** 区分-私立一般チェックボックス */
    @Location(x = 285, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_PRIVATE_G)
    @Action("univDivCheckBox")
    @Group(GROUP_PRIVATE)
    public CodeCheckBox divPrivateGeneralCheckBox;

    /** 区分-短大センターチェックボックス */
    @Location(x = 373, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_JUNIOR_C)
    @Action("univDivCheckBox")
    @Group(GROUP_PRIVATE)
    public CodeCheckBox divJuniorCenterCheckBox;

    /** 区分-短大一般チェックボックス */
    @Location(x = 489, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_JUNIOR_G)
    @Action("univDivCheckBox")
    @Group(GROUP_PRIVATE)
    public CodeCheckBox divJuniorGeneralCheckBox;

    /** 区分-文科省管轄外チェックボックス */
    @Location(x = 575, y = 13)
    @CodeConfig(Code.UNIV_SEARCH_DIV_OUTSIDE)
    @Action("univDivCheckBox")
    @Group(GROUP_EXCEPT_PRIVATE)
    public CodeCheckBox divOutOfEducationCheckBox;

    /** 女子大含むラジオボタン */
    @Location(x = 61, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_FEMAIL_INCLUDE)
    @NoAction
    @Selected
    public CodeRadio femailIncludeRadio;

    /** 女子大含まないラジオボタン */
    @Location(x = 114, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_FEMAIL_NOT_INCLUDE)
    @NoAction
    public CodeRadio femailNotIncludeRadio;

    /** 女子大のみラジオボタン */
    @Location(x = 187, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_FEMAIL_ONLY)
    @NoAction
    public CodeRadio femailRadio;

    /** 夜間含むラジオボタン */
    @Location(x = 383, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_NIGHT_INCLUDE)
    @NoAction
    @Selected
    public CodeRadio nightIncludeRadio;

    /** 夜間含まないラジオボタン */
    @Location(x = 438, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_NIGHT_NOT_INCLUDE)
    @NoAction
    public CodeRadio nightNotIncludeRadio;

    /** 夜間のみラジオボタン */
    @Location(x = 509, y = 36)
    @CodeConfig(Code.UNIV_SEARCH_NIGHT_ONLY)
    @NoAction
    public CodeRadio nightRadio;

    /** 英語認定試験チェックボックス */
    /*@Location(x = 438, y = 69)
    @CodeConfig(Code.SEARCH_ENG_JOINING_EXCLUDE_ONLY)
    @Selected
    @NoAction
    public CodeCheckBox englishJoinningCheckBox;*/

    /** 検索ボタン */
    @Location(x = 534, y = 62)
    public ImageButton startSearchLargeButton;

    /** タブ */
    @Location(x = 5, y = 63)
    @TabConfig({ ExmntnNameController.class, ExmntnFcltController.class, ExmntnEnteController.class })
    public UnivSearchTabbedPane tabbedPane;

    @Override
    @InitMethod
    public void initialize() {
        super.initialize();
        startSearchLargeButton.setFocusable(true);
        startSearchLargeButton.setFocusPainted(true);
    }

}

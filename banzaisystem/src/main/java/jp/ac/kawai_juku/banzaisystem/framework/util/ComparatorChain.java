package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * Comparatorチェーンです。
 *
 *
 * @param <T> 比較されるオブジェクトの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ComparatorChain<T> implements Comparator<T> {

    /** Comparatorリスト */
    private final List<Comparator<T>> list = new ArrayList<>();

    /**
     * Comparatorをチェーンに追加します。
     *
     * @param c Comparator
     * @return 自分自身
     */
    public ComparatorChain<T> add(Comparator<T> c) {
        list.add(c);
        return this;
    }

    @Override
    public int compare(T o1, T o2) {
        for (Comparator<T> c : list) {
            int result = c.compare(o1, o2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }

}

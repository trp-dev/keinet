package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

/**
 *
 * 生徒情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivStudentBean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private String grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** カナ氏名 */
    private String nameKana;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public String getGrade() {
        return grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * カナ氏名を返します。
     *
     * @return カナ氏名
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * カナ氏名をセットします。
     *
     * @param nameKana カナ氏名
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

}

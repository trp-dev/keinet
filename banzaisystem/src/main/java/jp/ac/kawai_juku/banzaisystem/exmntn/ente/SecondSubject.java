package jp.ac.kawai_juku.banzaisystem.exmntn.ente;

import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;

/**
 *
 * 個別試験使用科目を識別するenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum SecondSubject implements SearchSubject {

    /** 英語 */
    ENGLISH("英語（リスニング受験なし）", SecondDetail.KEY_ENGLISH),

    /** リスニング */
    LISTENING("英語（リスニング受験あり）", SecondDetail.KEY_ENGLISH),

    /** 数学：チェックボックス識別用のため科目名は定義しない */
    MATH(null),

    /** 数学�T */
    MATH1("数�T", SecondDetail.KEY_MATH1),

    /** 数学�TＡ */
    MATH1A("数�TＡ", SecondDetail.KEY_MATH1, SecondDetail.KEY_MATH1A),

    /** 数学�UＡ */
    MATH2A("数�UＡ", SecondDetail.KEY_MATH1, SecondDetail.KEY_MATH1A, SecondDetail.KEY_MATH2A),

    /** 数学�UＢ */
    MATH2B("数�UＢ", SecondDetail.KEY_MATH1, SecondDetail.KEY_MATH1A, SecondDetail.KEY_MATH2A, SecondDetail.KEY_MATH2B),

    /** 数学�V */
    MATH3("数�V", SecondDetail.KEY_MATH1, SecondDetail.KEY_MATH1A, SecondDetail.KEY_MATH2A, SecondDetail.KEY_MATH2B,
            SecondDetail.KEY_MATH3),

    /** 国語：チェックボックス識別用のため科目名は定義しない */
    JAPANESE(null),

    /** 現代文 */
    COMTEMPORARY("現代文", SecondDetail.KEY_COMTEMPORARY),

    /** 現古 */
    ANCIENT("現古", SecondDetail.KEY_COMTEMPORARY, SecondDetail.KEY_ANCIENT),

    /** 現古漢 */
    CHINESE("現古漢", SecondDetail.KEY_COMTEMPORARY, SecondDetail.KEY_ANCIENT, SecondDetail.KEY_CHINESE),

    /** 物理（基礎のみ） */
    PHYSICS_BASIC("物理基礎", SecondDetail.KEY_PHYSICS_BASIC),

    /** 化学（基礎のみ） */
    CHEMISTRY_BASIC("化学基礎", SecondDetail.KEY_CHEMISTRY_BASIC),

    /** 生物（基礎のみ） */
    BIOLOGY_BASIC("生物基礎", SecondDetail.KEY_BIOLOGY_BASIC),

    /** 地学（基礎のみ） */
    EARTHSCIENCE_BASIC("地学基礎", SecondDetail.KEY_EARTHSCIENCE_BASIC),

    /** 物理 */
    PHYSICS("物理", SecondDetail.KEY_PHYSICS_UNKNOWN, SecondDetail.KEY_PHYSICS_BASIC, SecondDetail.KEY_PHYSICS),

    /** 化学 */
    CHEMISTRY("化学", SecondDetail.KEY_CHEMISTRY_UNKNOWN, SecondDetail.KEY_CHEMISTRY_BASIC, SecondDetail.KEY_CHEMISTRY),

    /** 生物 */
    BIOLOGY("生物", SecondDetail.KEY_BIOLOGY_UNKNOWN, SecondDetail.KEY_BIOLOGY_BASIC, SecondDetail.KEY_BIOLOGY),

    /** 地学 */
    EARTHSCIENCE("地学", SecondDetail.KEY_EARTHSCIENCE_UNKNOWN, SecondDetail.KEY_EARTHSCIENCE_BASIC,
            SecondDetail.KEY_EARTHSCIENCE),

    /** 日本史 */
    JHISTORY("日本史", SecondDetail.KEY_JHISTORYA, SecondDetail.KEY_JHISTORYB),

    /** 世界史 */
    WHISTORY("世界史", SecondDetail.KEY_WHISTORYA, SecondDetail.KEY_WHISTORYB),

    /** 地理 */
    GEOGRAPHY("地理", SecondDetail.KEY_GEOGRAPHYA, SecondDetail.KEY_GEOGRAPHYB),

    /** 倫理 */
    ETHIC("倫理", SecondDetail.KEY_ETHIC),

    /** 政経 */
    POLITICS("政経", SecondDetail.KEY_POLITICS),

    /** 小論文 */
    ESSAY("小論文", SecondDetail.KEY_ESSAY),

    /** 総合問題 */
    SYNTHESIS("総合問題", SecondDetail.KEY_SYNTHESIS),

    /** 面接 */
    INTERVIEW("面接", SecondDetail.KEY_INTERVIEW),

    /** 実技 */
    TECHNIQUE("実技", SecondDetail.KEY_TECHNIQUE);

    /** 科目名 */
    private final String name;

    /** 科目キー */
    private final SubjectKey[] key;

    /**
     * コンストラクタです。
     *
     * @param name 科目名
     * @param key 科目キー
     */
    SecondSubject(String name, SubjectKey... key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * 科目キーを返します。
     *
     * @return 科目キー
     */
    public SubjectKey[] getKey() {
        return key;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.util;


/**
 *
 * 個人成績画面のユーティリティクラスです。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 *
 */
public final class ExmntnMainUtil {

    /** HTML改行 */
    public static final String HTML_LINE_SEPARATOR = "<br>";

    /**
     * コンストラクタです。
     */
    private ExmntnMainUtil() {
    }

    /**
     * 文字列からHTMLタグを空文字に変換します。
     *
     * @param str 文字列
     * @return HTMLタグ変換後文字列
     */
    public static String convertHtmlTag(String str) {
        return str.replaceAll("<.+?>", "");
    }

}

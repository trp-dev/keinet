package jp.ac.kawai_juku.banzaisystem.exmntn.univ.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE_DISP;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_NONE;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXCLUDE_VOCATIONAL_SCHOOL;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.controller.ExmntnDistController;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.view.ExmntnDistView;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.controller.ExmntnDlcsController;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.service.ExmntnDlcsService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.dto.ExmntnSubsDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.component.ExmntnUnivTextLabel;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivPubSubjectHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0201Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0301Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.service.ExmntnUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivUtil;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 志望大学詳細画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnUnivController extends SubController {

    /** 志望大学個人成績画面のDTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto exmntnMainDto;

    /** 大学検索画面のDTO */
    @Resource
    private ExmntnSubsDto exmntnSubsDto;

    /** View */
    @Resource(name = "exmntnUnivView")
    private ExmntnUnivView view;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** 志望大学学力分布画面のView */
    @Resource
    private ExmntnDistView exmntnDistView;

    /** Service */
    @Resource
    private ExmntnUnivService exmntnUnivService;

    /** 取得可能資格ダイアログのService */
    @Resource
    private ExmntnDlcsService exmntnDlcsService;

    /** Helper */
    @Resource(name = "exmntnUnivHelper")
    private ExmntnUnivHelper helper;

    /** 公表科目に関するヘルパー */
    @Resource(name = "exmntnUnivPubSubjectHelper")
    private ExmntnUnivPubSubjectHelper pubSubjectHelper;

    /** 個人成績画面の成績に関するヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 画面に表示した大学詳細情報 */
    private ExmntnUnivDetailSvcOutBean univDetail;

    @Override
    @Logging(GamenId.C1)
    public void activatedAction() {
        if (systemDto.isTeacher()) {
            /* 先生でログイン */
            showDetail();
        } else {
            /* 生徒でログイン */
            if (view.candidateRankComboBox.getItemCount() > 0) {
                view.showUnivButton.setEnabled(true);
            } else {
                view.showUnivButton.setEnabled(false);
            }
            pubSubjectHelper.showPubSubject(univDetail);
        }
    }

    /**
     * 志望順位コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult candidateRankComboBoxAction() {
        showDetail();
        return null;
    }

    /**
     * 選択中の志望大学詳細を表示します。
     */
    private void showDetail() {

        /* 空欄の志望順位を削除する */
        helper.removeBlankComboBoxItem(view.candidateRankComboBox);

        /* 前の大学・次の大学のボタンの表示状態を更新 */
        helper.changeButtonStatus(view.candidateRankComboBox, view.beforeUnivButton, view.nextUnivButton);

        /* 注釈事項欄表示フラグ */
        boolean isAttentionVisible = ExmntnUnivUtil.isAttentionVisible(exmntnMainDto.getJudgeScore().getMarkExam(), systemDto);

        if (view.candidateRankComboBox.getItemCount() > 0) {
            /* 大学詳細情報を取得する */
            univDetail = exmntnUnivService.createUnivDetail(view.candidateRankComboBox.getSelectedItem().getValue(), exmntnMainDto.getJudgeScore());

            /* 大学名 */
            view.univNameLabel.setText(univDetail.getUnivJoinName());

            /* 募集人員 */
            setCapacity(univDetail.getOfferCapacity(), univDetail.getOfferCapacityKbn());

            /* 所在地 */
            view.properLocationLabel.setText(univDetail.getPrefBase());
            view.campusLocationLabel.setText(univDetail.getPrefCampus());

            /* 評価-総合 */
            view.resultTotalLabel.setRatingIcon(univDetail.getTotalRating());
            view.totalRatingLabel.setTotalRatingText(univDetail.getTotalRating());
            view.totalRatingPointLabel.setText(univDetail.getTotalRatingPoint());

            /* センタ詳細 */
            helper.setDispDetailCenter(univDetail, isAttentionVisible, exmntnMainDto.getJudgeScore().getMarkExam());

            /* 二次詳細 */
            helper.setDispDetailSecond(univDetail.getSecond(), exmntnMainDto.getJudgeScore().getWrittenExam());

            /* 大学区分が８、or、９の場合はこの大学情報を見るボタンは非表示 */
            view.showUnivButton.setEnabled(!EXCLUDE_VOCATIONAL_SCHOOL.contains(univDetail.getUnivDivCd()));
        } else {
            /* 志望大学が無い場合 */
            univDetail = null;
            for (ExmntnUnivTextLabel label : view.findComponentsByClass(ExmntnUnivTextLabel.class)) {
                label.setText(StringUtils.EMPTY);
            }
            view.resultTotalLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultCenterLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultSecondLabel.setRatingIcon(StringUtils.EMPTY);
            view.showUnivButton.setEnabled(false);
        }

        /* 公表科目を表示する */
        pubSubjectHelper.showPubSubject(univDetail);

        /* 注意事項ラベルの表示・非表示を切り替える */
        view.centerAttentionLabel.setVisible(isAttentionVisible);
    }

    /**
     * 資格取得ボタンの表示状態を更新します。
     *
     */
    public void statusUpdateAcquisitionOfQualificationButton() {
        boolean statusToEnable = true; /* ボタンを有効化する */

        /* 前提条件（※この区画はfoolproofとしてのみ存在） */
        if (statusToEnable) {
            /* コンボボックスの項目リスト内の数を取得 */
            int count = view.candidateRankComboBox.getItemCount();
            /* 項目無しの場合は「空欄の項目」が選択可能となる実装になっているため
             * 項目数は必ず count >= 1 となり下記の分岐に流れることは無いはず。
             */
            if (count == 0) {
                /* コンボボックスの項目リストに内容無し */
                statusToEnable = false; /* ボタンを無効化（グレーアウト） */
            }
        }

        /* ボタンのグレーアウト適用判定 */
        if (statusToEnable) {
            /* 現在選択されている志望大学の情報を取得 */
            ExmntnMainCandidateUnivBean selectedUnivBean = view.candidateRankComboBox.getSelectedValue();

            /* 条件１：志望順位ボックスが空の場合は資格取得ボタンを無効化 */
            if (selectedUnivBean == null) {
                /* 志望大学の情報が格納されていない */
                statusToEnable = false; /* ボタンを無効化（グレーアウト） */
            }
            /* 条件２：取得可能資格がゼロ件の場合は資格取得ボタンを無効化 */
            else {
                /* 取得可能資格の抽出条件を確保（大学・学部・学科） */
                String univCd = selectedUnivBean.getUnivCd();
                String facultyCd = selectedUnivBean.getFacultyCd();
                String deptCd = selectedUnivBean.getDeptCd();
                /* 取得可能資格の名称リストを取得 */
                List<String> nameList = exmntnDlcsService.findQualifiableCertificationList(univCd, facultyCd, deptCd);
                if (nameList.size() == 0) {
                    /* 取得可能資格の名称リストに内容無し */
                    statusToEnable = false; /* ボタンを無効化（グレーアウト） */
                }
            }
        }
    }

    /**
     * 個人成績表ボタン押下アクションです。
     *
     * @return 個人成績表印刷画面
     */
    @Execute
    public ExecuteResult printPersonalScoreButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C1).addCreator(ExmntnUnivA0301Maker.class, helper.createA0301InBean(true, true)).launch();
                return null;
            }
        };
    }

    /**
     * 大学詳細ボタン押下アクションです。
     *
     * @return 大学詳細印刷画面
     */
    @Execute
    public ExecuteResult printUnivDetailButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C1).addCreator(ExmntnUnivA0201Maker.class, helper.createA0201InBean(view.candidateRankComboBox)).launch();
                return null;
            }
        };
    }

    /**
     * 資格取得ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult acquisitionOfQualificationButtonAction() {
        return forward(ExmntnDlcsController.class);
    }

    /**
     * 学力分布ボタン(マーク模試)アクションです。
     *
     * @return 志望大学学力分布画面
     */
    @Execute
    public ExecuteResult academicDistribution1ButtonAction() {
        return activate(ExmntnDistController.class, "activatedMarkAction");
    }

    /**
     * 学力分布ボタン(記述模試)アクションです。
     *
     * @return 志望大学学力分布画面
     */
    @Execute
    public ExecuteResult academicDistribution2ButtonAction() {
        return activate(ExmntnDistController.class, "activatedWrittenAction");
    }

    /**
     * 前の大学ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult beforeUnivButtonAction() {
        view.candidateRankComboBox.setSelectedIndex(view.candidateRankComboBox.getSelectedIndex() - 1);
        return null;
    }

    /**
     * 次の大学ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult nextUnivButtonAction() {
        view.candidateRankComboBox.setSelectedIndex(view.candidateRankComboBox.getSelectedIndex() + 1);
        return null;
    }

    /**
     * 募集人員表示文字列
     *
     * @param capacity 定員
     * @param flag 区分
     */
    private void setCapacity(String capacity, String flag) {
        if (flag == null || flag.equals(CAPACITY_NONE)) {
            view.capacityLabel.setText(StringUtils.EMPTY);
        } else if (flag.equals(CAPACITY_ESTIMATE)) {
            view.capacityLabel.setText(capacity + CAPACITY_ESTIMATE_DISP);
        } else {
            view.capacityLabel.setText(capacity);
        }
    }

    /**
     * 公表科目表示ボタンアクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult pubSubjectButtonAction(ActionEvent e) {
        pubSubjectHelper.showPubSubject(univDetail);
        return null;
    }

    /**
     *  この大学の入試情報を見るボタン押下アクションです。
     *
     * @return 結果を返したコントローラ
     */
    @Execute
    public ExecuteResult showUnivButtonAction() {
        String s1 = "https://search.keinet.ne.jp/outline/department/";
        String s2 = univDetail.getUnivCd();
        StringBuffer univUrl = new StringBuffer();
        univUrl.append(s1);
        univUrl.append(s2);
        String webUnivUrl = univUrl.toString();

        ExecuteResult result = openUrl(webUnivUrl);
        return result;
    }

}

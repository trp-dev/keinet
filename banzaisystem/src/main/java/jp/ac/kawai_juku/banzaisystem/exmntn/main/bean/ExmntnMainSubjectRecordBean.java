package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;

/**
 *
 * 個人成績・志望大学画面の科目成績を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnMainSubjectRecordBean extends SubjectRecord {

    /** 科目名 */
    private String subName;

    /** 科目配点 */
    private String subAllotPnt;

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * 科目配点を返します。
     *
     * @return 科目配点
     */
    public String getSubAllotPntStr() {
        return subAllotPnt;
    }

    /**
     * 科目配点をセットします。
     *
     * @param subAllotPnt 科目配点
     */
    public void setSubAllotPnt(String subAllotPnt) {
        this.subAllotPnt = subAllotPnt;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.mtch.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.mainte.dmod.controller.MainteDmodController;
import jp.ac.kawai_juku.banzaisystem.mainte.mtch.view.MainteMtchView;

/**
 *
 * メンテナンス-個人マッチング画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMtchController extends SubController {

    /** View */
    @Resource
    private MainteMtchView mainteMtchView;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    @Override
    public void activatedAction() {
        mainteMtchView.statusTable.setExamList(systemDto.getExamList());
    }

    /**
     * 修正ダイアログ表示アクションです。
     *
     * @return 基準データ修正ダイアログ
     */
    @Execute
    public ExecuteResult showDialogAction() {
        return forward(MainteDmodController.class);
    }

}

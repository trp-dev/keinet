package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;

import javax.swing.JRadioButton;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * コード定義を利用するラジオボタンです。<br>
 * ラジオのテキストにはコード名が設定されます。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CodeRadio extends JRadioButton implements BZComponent, CodeComponent {

    /** このラジオに紐付けられたコード */
    private Code code;

    /**
     * コンストラクタです。
     */
    public CodeRadio() {
        setOpaque(false);
        setBorder(null);
        setMargin(new Insets(0, 0, 0, 0));
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        CodeConfig config = BZFieldUtil.getAnnotation(field, CodeConfig.class);
        if (config == null) {
            throw new RuntimeException("CodeConfigアノテーションが設定されていません。");
        } else {
            code = config.value();
        }

        if (field.isAnnotationPresent(Selected.class)) {
            setSelected(true);
        }

        setText(code.getName());
        setSize(getPreferredSize());

        ButtonGroupManager.getButtonGroup(view, code.getCodeType()).add(this);

        if (!field.isAnnotationPresent(NoAction.class)) {
            Action action = field.getAnnotation(Action.class);
            final ActionListener listener;
            if (action == null) {
                listener = new ControllerActionListener(view, field.getName());
            } else {
                listener = new ControllerActionListener(view, action.value());
            }
            /* ActionListenerだとsetSelectedメソッドでの値変更時にイベントが発生しないため、ItemListenerを利用する */
            addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (isSelected()) {
                        listener.actionPerformed(new ActionEvent(e.getSource(), ActionEvent.ACTION_PERFORMED, null));
                    }
                }
            });
        }
    }

    @Override
    public Code getCode() {
        return code;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.ASTERISK;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_ENG;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_JAP;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_MAT;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_SCI;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_SOC;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.MARK_AGGREGATE_SUBCD;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_C;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_D;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_E;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.JUDGE_STR_X;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2B;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDaoOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailCenterSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSecondSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.dao.ExmntnUnivDao;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamSubjectUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.beans.judge.HandledPoint;
import jp.co.fj.kawaijuku.judgement.util.JudgedUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;
import jp.co.fj.kawaijuku.judgement.util.StringUtil;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学詳細画面のサービスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivService {

	/** あと何点-評価文字 */
	private static final Map<String, String> REMAING_RATING;

	static {
		Map<String, String> map = CollectionsUtil.newHashMap();
		map.put(JUDGE_STR_A, JUDGE_STR_X);
		map.put(JUDGE_STR_B, JUDGE_STR_A);
		map.put(JUDGE_STR_C, JUDGE_STR_B);
		map.put(JUDGE_STR_D, JUDGE_STR_C);
		map.put(JUDGE_STR_E, JUDGE_STR_D);
		REMAING_RATING = Collections.unmodifiableMap(map);
	}

	/** マーク数学�Aの科目コードセット */
	private static final Set<String> MARK_MATH2_SUBCD;

	static {
		Set<String> set = CollectionsUtil.newHashSet();
		set.add(SUBCODE_CENTER_DEFAULT_MATH2);
		set.add(SUBCODE_CENTER_DEFAULT_MATH2B);
		MARK_MATH2_SUBCD = Collections.unmodifiableSet(set);
	}

	/** DAO */
	@Resource
	private ExmntnUnivDao exmntnUnivDao;

	/** システム情報DTO */
	@Resource
	private SystemDto systemDto;

	/**
	 * 大学詳細情報を生成します。
	 *
	 * @param bean 詳細
	 * @param scoreBean 成績データ
	 * @return 大学詳細情報
	 */
	public ExmntnUnivDetailSvcOutBean createUnivDetail(ExmntnMainCandidateUnivBean bean, ScoreBean scoreBean) {
		/*
		JudgedResult judge = BZJudgementExecutor.getInstance().judge(scoreBean, bean, false);
		JudgedResult judgeInclude = BZJudgementExecutor.getInstance().judge(scoreBean, bean, true);
		*/
		JudgedResult judge = BZJudgementExecutor.getInstance().judge(scoreBean, bean);
		/*JudgedResult judgeInclude = BZJudgementExecutor.getInstance().judge(scoreBean, bean);*/
		ExmntnUnivDaoOutBean daoBean = exmntnUnivDao.selectUnivDetail(bean, systemDto);

		ExmntnUnivDetailSvcOutBean detail = new ExmntnUnivDetailSvcOutBean(bean);

		/* 大学名-学部名-学科名 */
		detail.setUnivJoinName(ObjectUtils.toString(bean.getUnivName()) + " " + ObjectUtils.toString(bean.getFacultyName()) + " " + ObjectUtils.toString(bean.getDeptName()));

		/* 大学名 */
		detail.setUnivName(bean.getUnivName());

		/* 学部名 */
		detail.setFacultyName(bean.getFacultyName());

		/* 学科名 */
		detail.setDeptName(bean.getDeptName());

		/* 日程コード */
		detail.setScheduleCd(judge.getScheduleCd());

		/* 大学区分コード */
		detail.setUnivDivCd(judge.getUnivDivCd());

		/* 募集定員 */
		detail.setOfferCapacity(StringUtils.trim(daoBean.getOfferCapacity()));

		/* 募集定員区分 */
		detail.setOfferCapacityKbn(daoBean.getCapacityFlag());

		/* 出願締切 */
		if (ScheduleUtil.isAppliDeadLineEnabled(systemDto.getUnivExamDiv(), bean.getUnivCd())) {
			detail.setEntExamApplideadlineDiv(daoBean.getEntExamApplideadlineDiv());
		}

		/* 入試 */
		if (ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), bean.getUnivCd())) {
			detail.setExamDay(daoBean.getExamDay());
		}

		/* 合格発表 */
		if (ScheduleUtil.isSucAnnDateEnabled(systemDto.getUnivExamDiv(), bean.getUnivCd())) {
			detail.setSucannDate(daoBean.getSucannDate());
		}

		/* 手続締切 */
		if (ScheduleUtil.isProceDeadLineEnabled(systemDto.getUnivExamDiv(), bean.getUnivCd())) {
			detail.setProceDeadline(daoBean.getProceDeadline());
		}

		/* 本部所在地 */
		detail.setPrefBase(daoBean.getPrefBase());

		/* キャンパス所在地 */
		detail.setPrefCampus(daoBean.getPrefCampus());

		/* 総合：評価 */
		detail.setTotalRating(judge.getEvalTotal().getLetter());

		/* 総合：評価ポイント */
		detail.setTotalRatingPoint(judge.getEvalTotal().getPointStr());

		/* 共通テスト（含まない）詳細 */
		/* 共通テスト詳細 */
		detail.setCenter(createCenterDetail(scoreBean, judge, daoBean));

		/* 共通テスト（含む）詳細 */
		/* detail.setCenterInclude(createCenterDetail(scoreBean, judgeInclude, daoBean)); */

		/* 二次詳細 */
		detail.setSecond(createSecondDetail(scoreBean, judge));

		return detail;
	}

	/**
	 * センタ詳細情報を生成します。
	 *
	 * @param scoreBean 成績データ
	 * @param judge 判定結果
	 * @param daoBean {@link ExmntnUnivDaoOutBean}
	 * @return {@link ExmntnUnivDetailCenterSvcOutBean}
	 */
	private ExmntnUnivDetailCenterSvcOutBean createCenterDetail(ScoreBean scoreBean, JudgedResult judge, ExmntnUnivDaoOutBean daoBean) {

		ExamBean markExam = scoreBean.getMarkExam();

		boolean isCenter = markExam != null && ExamUtil.isCenter(markExam);

		ExmntnUnivDetailCenterSvcOutBean center = new ExmntnUnivDetailCenterSvcOutBean();

		/* センタ：枝番 */
		center.setBranchCd(judge.getDetail().getCenter().getBranchCode());

		/* センタ：評価 */
		center.setRating(judge.getCLetterJudgement());

		/* センタ：評価基準 */
		center.setLineA(judge.getCALine());
		center.setLineB(judge.getCBLine());
		center.setLineC(judge.getCCLine());
		center.setLineD(judge.getCDLine());

		/* センタ：評価得点 */
		center.setScore(judge.getC_scoreStr());

		/* センタ：あと何点 */
		center.setRemainRaiting(getRemaingRating(judge.getCLetterJudgement()));
		center.setRemainScore(getRemaingScore(center));

		/* センタ：満点 */
		center.setFullPoint(convFullPoint(judge.getDetail().getCenter().getFullPoint()));

		/* センタ：ボーダライン */
		center.setBorder(judge.getBorderPointStr());

		/* センタ：判定科目 */
		center.setSubCount(judge.getDetail().getCenter().getSubString());

		/* 第1段階選抜予想 */
		if (isCenter) {
			if (daoBean.getRejectExScore() == 0) {
				center.setPickedLine(getMessage("label.exmntn.univ.pickedLineNone"));
			} else if (daoBean.getRejectExScore() == 99999 || judge.getBean().getrFullPointNrep() == 0) {
				center.setPickedLine("--");
			} else {
				center.setPickedLine(daoBean.getRejectExScore() + "/" + judge.getBean().getrFullPointNrep());
				if (judge.getBean().getRScore() < 0) {
					center.setRejectScore(StringUtils.EMPTY);
				} else {
					center.setRejectScore(Integer.toString(judge.getBean().getRScore()));
				}
			}
		}

		/* 共通テスト 出願要件パターンコード */
		/* center.setAppReqPaternCd(judge.getBean().getUniv().getAppReqPaternCd()); */

		/* 英語資格要件 */
		/*
		if (judge.getBean().getSSHanteiFlg() == 1) {
		    if ((judge.getBean().getEngSSSutuGanHanTeiKbn() == 1) && (judge.getBean().getEngSSHKSutuGanHTKokuchi() != 1)) {
		        center.setEnglishRequirements("○");
		    } else if ((judge.getBean().getEngSSSutuGanHanTeiKbn() == 2) && (judge.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
		        center.setEnglishRequirements("△!");
		    } else if ((judge.getBean().getEngSSSutuGanHanTeiKbn() == 3) && (judge.getBean().getEngSSHKSutuGanHTKokuchi() != 1)) {
		        center.setEnglishRequirements("×");
		    } else if ((judge.getBean().getEngSSSutuGanHanTeiKbn() == 3) && (judge.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
		        center.setEnglishRequirements("×!");
		    } else if ((judge.getBean().getEngSSSutuGanHanTeiKbn() == 4) && (judge.getBean().getEngSSHKSutuGanHTKokuchi() == 1)) {
		        center.setEnglishRequirements("!");
		    } else {
		        center.setEnglishRequirements("");
		    }
		} else {
		    center.setEnglishRequirements("");
		}
		*/

		/* センタ：備考 */
		center.setNote(StringUtils.EMPTY);

		/* センタ詳細 */
		setCenter(center, markExam, judge, scoreBean.getScore().getMark(), isCenter);

		/* 注釈文 */
		center.setCommentStatement(daoBean.getCommentStatement());

		return center;
	}

	/**
	 * センタ詳細情報を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param exam 対象模試
	 * @param judge 判定結果
	 * @param markList マーク成績リスト
	 * @param isCenter センター・リサーチフラグ
	 */
	private void setCenter(ExmntnUnivDetailCenterSvcOutBean center, ExamBean exam, JudgedResult judge, List<?> markList, boolean isCenter) {
		CenterDetailHolder detail = (CenterDetailHolder) judge.getDetail().getCenter().getForJudgement();
		setCenterEng(center, judge, detail, markList);
		/* setCenterEngCefr(center, judge, detail); */
		setCenterMath(center, judge, detail, markList, isCenter);
		setCenterJap(center, judge, detail, markList);
		/* setCenterJapDesc(center, judge, detail); */
		setCenterSci(center, exam, judge, detail, markList, isCenter);
		setCenterSoc(center, judge, detail, markList, isCenter);
	}

	/**
	 * 二次詳細情報を生成します。
	 *
	 * @param scoreBean 成績データ
	 * @param judge 判定結果
	 * @return {@link ExmntnUnivDetailSecondSvcOutBean}
	 */
	private ExmntnUnivDetailSecondSvcOutBean createSecondDetail(ScoreBean scoreBean, JudgedResult judge) {

		ExmntnUnivDetailSecondSvcOutBean second = new ExmntnUnivDetailSecondSvcOutBean();

		/* 二次：枝番 */
		second.setBranchCd(judge.getDetail().getSecond().getBranchCode());

		/* 二次：評価 */
		second.setRating(judge.getSLetterJudgement());

		/* 二次：評価基準 */
		second.setLineA(judge.getSALine());
		second.setLineB(judge.getSBLine());
		second.setLineC(judge.getSCLine());
		second.setLineD(judge.getSDLine());

		/* 二次：評価偏差値 */
		second.setDev(judge.getS_scoreStr());

		/* 二次：あと何点 */
		second.setRemainRaiting(getRemaingRating(judge.getSLetterJudgement()));
		second.setRemainDeviation(getRemaingDeviation(second));

		/* 二次：満点 */
		second.setFullPoint(convFullPoint(judge.getDetail().getSecond().getFullPoint()));

		/* 二次：ボーダランク */
		if (judge.getRank() == JudgementConstants.Univ.RANK_FORCE) {
			second.setBorderRank(Univ.RANK_STR_NONE);
		} else if (judge.getRank() == JudgementConstants.Univ.RANK_NUM_NONE) {
			second.setBorderRank("--");
		} else {
			second.setBorderRank(judge.getRankLLimitsStr());
		}

		/* 二次：判定科目 */
		second.setSubCount(judge.getDetail().getSecond().getSubString());

		/* 二次：備考 */
		second.setNote(StringUtils.EMPTY);

		/* 二次詳細 */
		setSecond(second, judge, scoreBean.getScore().getMark(), scoreBean.getScore().getWrtn());

		/* 二次の判定結果有無 */
		second.setJudged(!StringUtil.isEmptyTrim(judge.getSLetterJudgement()));

		return second;
	}

	/**
	 * 二次詳細情報を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param markList マーク成績リスト
	 * @param writtenList 記述成績リスト
	 */
	private void setSecond(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, List<?> markList, List<?> writtenList) {
		SecondDetailHolder detail = (SecondDetailHolder) judge.getDetail().getSecond().getForJudgement();
		List<?> list = ObjectUtils.defaultIfNull(writtenList, markList);
		setSecondEng(second, judge, detail);
		setSecondMath(second, judge, detail);
		setSecondJap(second, judge, detail);
		setSecondSci(second, judge, detail, list);
		setSecondSoc(second, judge, detail, list);
		setSecondOther(second, judge, detail);
	}

	/**
	 * 指定した教科の大学配点を算出します。
	 *
	 * @param courseIndex 教科インデックス
	 * @param adoptedScoreList 判定採用成績リスト
	 * @return 大学配点
	 */
	private String calcAllot(int courseIndex, List adoptedScoreList) {
		BigDecimal total = null;
		for (Iterator ite = adoptedScoreList.iterator(); ite.hasNext();) {
			HandledPoint point = (HandledPoint) ite.next();
			if (point.getCourseIndex() == courseIndex && point.getAllot() > 0) {
				if (total == null) {
					total = BigDecimal.valueOf(0);
				}
				total = total.add(new BigDecimal(point.getAllot()));
			}
		}
		return total == null ? "" : Integer.toString(total.setScale(0, RoundingMode.HALF_UP).intValue());
	}

	/**
	 * センタ：英語の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 * @param markList マーク成績リスト
	 */
	private void setCenterEng(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail, List<?> markList) {

		// グループインデックスを取得
		int groupIndex = JudgedUtil.findGroupIndex(judge.getBean().getCAdoptedScore(), Detail.English.COURSE_INDEX);
		boolean writingFlg = false;
		boolean listeningFlg = false;
		ImposingSubject writing[] = { detail.getImposingSubject(CenterDetail.KEY_EWRITING), detail.getImposingSubject(CenterDetail.KEY_EWRITING_G1), detail.getImposingSubject(CenterDetail.KEY_EWRITING_G2),
				detail.getImposingSubject(CenterDetail.KEY_EWRITING_G3), detail.getImposingSubject(CenterDetail.KEY_EWRITING_G4), detail.getImposingSubject(CenterDetail.KEY_EWRITING_G5) };

		ImposingSubject listenig[] = { detail.getImposingSubject(CenterDetail.KEY_ELISTENING), detail.getImposingSubject(CenterDetail.KEY_ELISTENING_G1), detail.getImposingSubject(CenterDetail.KEY_ELISTENING_G2),
				detail.getImposingSubject(CenterDetail.KEY_ELISTENING_G3), detail.getImposingSubject(CenterDetail.KEY_ELISTENING_G4), detail.getImposingSubject(CenterDetail.KEY_ELISTENING_G5), };
		ImposingSubject writingCreate = writing[groupIndex];
		ImposingSubject listenigCreate = listenig[groupIndex];

		/* 課し科目チェック */
		for (int idx = 0; idx < writing.length; idx++) {
			if (writing[idx].isImposed()) {
				writingFlg = true;
			}
			if (listenig[idx].isImposed()) {
				listeningFlg = true;
			}
			if (writingFlg && listeningFlg) {
				break;
			}
		}

		/* 筆記配点（帳票で利用） */
		if (writingFlg) {
			center.setAllotWriting(createEnglishAllot(writingCreate, detail.getWritingAllot(groupIndex)));
		} else {
			center.setAllotWriting("");
		}

		/* リスニング配点（帳票で利用） */
		if (listeningFlg) {
			center.setAllotListening(createEnglishAllot(listenigCreate, detail.getListenigAllot(groupIndex)));
		} else {
			center.setAllotListening("");
		}

		/* 筆記＋リスニング */
		if (writingFlg || listeningFlg) {
			center.setAllotEnglish(calcAllot(Detail.English.COURSE_INDEX, judge.getBean().getCAdoptedScore()));
			double score = -1.0;
			if (!judge.getAllot1Eng().isEmpty()) {
				BZSubjectRecord[] rec = findRecords(markList, COURSE_ENG, true);
				score = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec);
			}
			center.setScoreEnglish(formatScore(score));
		} else {
			center.setAllotEnglish("");
			center.setScoreEnglish("");
		}
	}

	/**
	 * センタ：英語認定試験の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 */
	/*
	private void setCenterEngCefr(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail) {
	
	    ImposingSubject cefr = detail.getImposingSubject(CenterDetail.KEY_ENGLISH_SANKA);
	*/
	/* 英語認定試験 */
	/*
	    if (cefr.isImposed()) {
	        EngSSData eng = judge.getBean().getEngSSData();
	        if (eng == null || eng.getSankaExamSiboTokuten() == -1) {
	            center.setAllotEnglishJoining(HYPHEN);
	            center.setScoreEnglishJoining(HYPHEN);
	        } else {
	            String allot = eng.getKatenKubun() == 1 ? "+" : "";
	            allot += formatScore(eng.getSankaExamSiboHaiten());
	            String score = "1".equals(eng.getTokutenKsRiyoKubun()) ? "!" : "";
	            score += formatScore(eng.getSankaExamSiboTokuten());
	            center.setAllotEnglishJoining(allot);
	            center.setScoreEnglishJoining(score);
	        }
	    } else {
	        center.setAllotEnglishJoining(HYPHEN);
	        center.setScoreEnglishJoining(HYPHEN);
	    }
	
	}
	*/

	/**
	 * センタ：数学の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 * @param markList マーク成績リスト
	 * @param isCenter センター・リサーチフラグ
	 */
	private void setCenterMath(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail, List<?> markList, boolean isCenter) {

		BZSubjectRecord[] rec = findRecords(markList, COURSE_MAT, true);
		if (rec[0] != null && MARK_MATH2_SUBCD.contains(rec[0].getSubCd())) {
			/* 数学�Aは2番目に表示する */
			rec[1] = rec[0];
			rec[0] = null;
		}

		double score1 = -1.0;
		double score2 = -1.0;
		if (!judge.getAllot1Mat().isEmpty()) {
			List<HandledPoint> list = findAdoptedScore(judge.getBean().getCAdoptedScore(), Detail.Math.COURSE_INDEX);
			HandledPoint pair = findPairHandledPoint(list);
			if (pair != null) {
				/* ペア科目の場合はペアを分解する */
				int s1 = getPairScore(isCenter, rec[0], pair);
				int s2 = getPairScore(isCenter, rec[1], pair);
				int total = add(s1, s2);
				score1 = disassemblePair(pair, s1, total);
				score2 = disassemblePair(pair, s2, total);
			} else {
				/* ペア科目ではない場合 */
				score1 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[0]);
				score2 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[1]);
			}
		}

		/* 得点１ */
		if (isImposed(detail, CenterDetail.KEY_MATH1, CenterDetail.KEY_MATH1A, CenterDetail.KEY_MATH1_G1, CenterDetail.KEY_MATH1A_G1, CenterDetail.KEY_MATH1_G2, CenterDetail.KEY_MATH1A_G2, CenterDetail.KEY_MATH1_G3,
				CenterDetail.KEY_MATH1A_G3, CenterDetail.KEY_MATH1_G4, CenterDetail.KEY_MATH1A_G4, CenterDetail.KEY_MATH1_G5, CenterDetail.KEY_MATH1A_G5)) {
			center.setScoreMath1(formatScore(score1));
		} else {
			center.setScoreMath1("");
		}

		/* 得点２ */
		if (isImposed(detail, CenterDetail.KEY_MATH2, CenterDetail.KEY_MATH2B, CenterDetail.KEY_MATH2_G1, CenterDetail.KEY_MATH2B_G1, CenterDetail.KEY_MATH2_G2, CenterDetail.KEY_MATH2B_G2, CenterDetail.KEY_MATH2_G3,
				CenterDetail.KEY_MATH2B_G3, CenterDetail.KEY_MATH2_G4, CenterDetail.KEY_MATH2B_G4, CenterDetail.KEY_MATH2_G5, CenterDetail.KEY_MATH2B_G5)) {
			center.setScoreMath2(formatScore(score2));
		} else {
			center.setScoreMath2("");
		}

		/* 配点 */
		if ("".equals(center.getScoreMath1()) && "".equals(center.getScoreMath2())) {
			center.setAllotMath("");
		} else {
			center.setAllotMath(calcAllot(Detail.Math.COURSE_INDEX, judge.getBean().getCAdoptedScore()));
		}
	}

	/**
	 * センタ：国語の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 * @param markList マーク成績リスト
	 */
	private void setCenterJap(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail, List<?> markList) {
		if (isImposed(detail, Detail.Japanese.COURSE_INDEX)) {
			double score = -1.0;
			if (!judge.getAllot1Jap().isEmpty()) {
				BZSubjectRecord[] rec = findRecords(markList, COURSE_JAP, true);
				score = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec);
			}
			center.setAllotJap(calcAllot(Detail.Japanese.COURSE_INDEX, judge.getBean().getCAdoptedScore()));
			center.setScoreJap(formatScore(score));
		} else {
			center.setAllotJap("");
			center.setScoreJap("");
		}
	}

	/**
	 * センタ：国語(記)の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 */
	/* private void setCenterJapDesc(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail) { */
	/* ImposingSubject jpndesc = detail.getImposingSubject(CenterDetail.KEY_JAPANESE_KIJUTSU); */
	/* 国語(記) */
	/*
	center.setAllotJapDesc(HYPHEN);
	center.setScoreJapDesc(HYPHEN);
	*/
	/*
	if (jpndesc.isImposed()) {
	    HandledPoint point = getJapDesc(judge);
	    if (point == null || point.getPoint() == -1) {
	        center.setAllotJapDesc(HYPHEN);
	        center.setScoreJapDesc(HYPHEN);
	    } else {
	        String allot = point.getKatenKubun() == 1 ? "+" : "";
	        allot += formatScore(point.getAllot());
	        center.setAllotJapDesc(allot);
	        center.setScoreJapDesc(formatScore(point.getPoint()));
	    }
	} else {
	    center.setAllotJapDesc(HYPHEN);
	    center.setScoreJapDesc(HYPHEN);
	}
	}
	  */

	/**
	 * 国語(記)の配点・得点を取得します。
	 *
	 * @param judge 判定結果
	 * @return 国語(記)の配点・得点
	 */
	/*
	private HandledPoint getJapDesc(JudgedResult judge) {
	    HandledPoint point = null;
	    for (Object obj : judge.getBean().getCAdoptedScore()) {
	        if (Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU.equals(((HandledPoint) obj).getSortKey())) {
	            point = (HandledPoint) obj;
	            break;
	        }
	    }
	    return point;
	}
	*/

	/**
	 * センタ：理科の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param exam 対象模試
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 * @param markList マーク成績リスト
	 * @param isCenter センター・リサーチフラグ
	 */
	private void setCenterSci(ExmntnUnivDetailCenterSvcOutBean center, ExamBean exam, JudgedResult judge, CenterDetailHolder detail, List<?> markList, boolean isCenter) {

		ExamSubjectUtil util = exam == null ? null : new ExamSubjectUtil(systemDto, exam);

		BZSubjectRecord[] rec = findRecords(markList, COURSE_SCI, true);
		if (rec[0] != null && util.isBasic(rec[0].getSubCd())) {
			/* 基礎科目は2番目以降に表示する */
			rec[2] = rec[1];
			rec[1] = rec[0];
			rec[0] = null;
		}

		double score1 = -1.0;
		double score2 = -1.0;
		double score3 = -1.0;
		if (!judge.getAllot1Sci().isEmpty()) {
			List<HandledPoint> list = findAdoptedScore(judge.getBean().getCAdoptedScore(), Detail.Science.COURSE_INDEX);
			HandledPoint pair = findPairHandledPoint(list);
			if (pair == null) {
				/* ペア科目なし */
				score1 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[0]);
				score2 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[1]);
				score3 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[2]);
			} else if (util.isBasic(pair.getSortKey())) {
				/* ペア科目が基礎科目 */
				int s2 = getPairScore(isCenter, rec[1], pair);
				int s3 = getPairScore(isCenter, rec[2], pair);
				int total = add(s2, s3);
				score1 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[0]);
				score2 = disassemblePair(pair, s2, total);
				score3 = disassemblePair(pair, s3, total);
			} else {
				/* ペア科目が専門科目 */
				int s1 = getPairScore(isCenter, rec[0], pair);
				int s2 = getPairScore(isCenter, rec[1], pair);
				int total = add(s1, s2);
				score1 = disassemblePair(pair, s1, total);
				score2 = disassemblePair(pair, s2, total);
			}
		}

		if (isImposed(detail, Detail.Science.COURSE_INDEX)) {
			center.setAllotSci(calcAllot(Detail.Science.COURSE_INDEX, judge.getBean().getCAdoptedScore()));
			center.setScoreSci1(formatScore(score1));
			center.setScoreSci2(formatScore(score2));
			center.setScoreSci3(formatScore(score3));
		} else {
			center.setAllotSci("");
			center.setScoreSci1("");
			center.setScoreSci2("");
			center.setScoreSci3("");
		}
	}

	/**
	 * センタ：社会の配点・得点を設定します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @param judge 判定結果
	 * @param detail センタ詳細
	 * @param markList マーク成績リスト
	 * @param isCenter センター・リサーチフラグ
	 */
	private void setCenterSoc(ExmntnUnivDetailCenterSvcOutBean center, JudgedResult judge, CenterDetailHolder detail, List<?> markList, boolean isCenter) {

		BZSubjectRecord[] rec = findRecords(markList, COURSE_SOC, true);

		double score1 = -1.0;
		double score2 = -1.0;
		if (!judge.getAllot1Soc().isEmpty()) {
			List<HandledPoint> list = findAdoptedScore(judge.getBean().getCAdoptedScore(), Detail.Social.COURSE_INDEX);
			HandledPoint pair = findPairHandledPoint(list);
			if (pair != null) {
				/* ペア科目の場合はペアを分解する */
				int s1 = getPairScore(isCenter, rec[0], pair);
				int s2 = getPairScore(isCenter, rec[1], pair);
				int total = add(s1, s2);
				score1 = disassemblePair(pair, s1, total);
				score2 = disassemblePair(pair, s2, total);
			} else {
				/* ペア科目ではない場合 */
				score1 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[0]);
				score2 = findAdoptedScore(judge.getBean().getCAdoptedScore(), rec[1]);
			}
		}

		if (isImposed(detail, Detail.Social.COURSE_INDEX)) {
			center.setAllotSoc(calcAllot(Detail.Social.COURSE_INDEX, judge.getBean().getCAdoptedScore()));
			center.setScoreSoc1(formatScore(score1));
			center.setScoreSoc2(formatScore(score2));
		} else {
			center.setAllotSoc("");
			center.setScoreSoc1("");
			center.setScoreSoc2("");
		}
	}

	/**
	 * 二次：英語の配点・偏差値を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 */
	private void setSecondEng(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail) {
		if (isImposed(detail, Detail.English.COURSE_INDEX)) {
			second.setAllotEnglish(calcAllot(Detail.English.COURSE_INDEX, judge.getBean().getSAdoptedScore()));
			second.setDevEnglish(getDeviation(judge, Detail.English.COURSE_INDEX));
		} else {
			second.setAllotEnglish("");
			second.setDevEnglish("");
		}
	}

	/**
	 * 二次：数学の配点・偏差値を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 */
	private void setSecondMath(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail) {
		if (isImposed(detail, Detail.Math.COURSE_INDEX)) {
			second.setAllotMath(calcAllot(Detail.Math.COURSE_INDEX, judge.getBean().getSAdoptedScore()));
			second.setDevMath(getDeviation(judge, Detail.Math.COURSE_INDEX));
		} else {
			second.setAllotMath("");
			second.setDevMath("");
		}
	}

	/**
	 * 二次：国語の配点・偏差値を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 */
	private void setSecondJap(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail) {
		if (isImposed(detail, Detail.Japanese.COURSE_INDEX)) {
			second.setAllotJap(calcAllot(Detail.Japanese.COURSE_INDEX, judge.getBean().getSAdoptedScore()));
			second.setDevJap(getDeviation(judge, Detail.Japanese.COURSE_INDEX));
		} else {
			second.setAllotJap("");
			second.setDevJap("");
		}
	}

	/**
	 * 二次：理科の配点・偏差値を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 * @param list マーク成績リスト
	 */
	private void setSecondSci(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail, List<?> list) {
		if (isImposed(detail, Detail.Science.COURSE_INDEX)) {
			double[] score = findAdoptedDeviation(judge, list, Detail.Science.COURSE_INDEX, COURSE_SCI);
			second.setAllotSci(calcAllot(Detail.Science.COURSE_INDEX, judge.getBean().getSAdoptedScore()));
			second.setDevSci1(formatDeviation(score[0]));
			second.setDevSci2(formatDeviation(score[1]));
		} else {
			second.setAllotSci("");
			second.setDevSci1("");
			second.setDevSci2("");
		}
	}

	/**
	 * 二次：社会の配点・偏差値を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 * @param list マーク成績リスト
	 */
	private void setSecondSoc(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail, List<?> list) {
		if (isImposed(detail, Detail.Social.COURSE_INDEX)) {
			double[] score = findAdoptedDeviation(judge, list, Detail.Social.COURSE_INDEX, COURSE_SOC);
			second.setAllotSoc(calcAllot(Detail.Social.COURSE_INDEX, judge.getBean().getSAdoptedScore()));
			second.setDevSoc1(formatDeviation(score[0]));
			second.setDevSoc2(formatDeviation(score[1]));
		} else {
			second.setAllotSoc("");
			second.setDevSoc1("");
			second.setDevSoc2("");
		}
	}

	/**
	 * 二次：その他の配点を設定します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @param judge 判定結果
	 * @param detail 二次詳細
	 */
	private void setSecondOther(ExmntnUnivDetailSecondSvcOutBean second, JudgedResult judge, SecondDetailHolder detail) {
		if (isImposed(detail, Detail.Etc.COURSE_INDEX)) {
			second.setOtherAllot(judge.getOtherAllot());
		} else {
			second.setOtherAllot("");
		}
	}

	/**
	 * あと何点-評価文字を返します。
	 *
	 * @param rating 評価
	 * @return あと何点-評価文字
	 */
	private String getRemaingRating(String rating) {
		String key = StringUtils.substring(rating, 0, 1);
		return ObjectUtils.firstNonNull(REMAING_RATING.get(key), JUDGE_STR_X);
	}

	/**
	 * あと何点-評価得点を返します。
	 *
	 * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
	 * @return あと何点-評価得点
	 */
	private String getRemaingScore(ExmntnUnivDetailCenterSvcOutBean center) {

		if (!StringUtils.isNumeric(center.getScore())) {
			return ASTERISK;
		}

		String line;
		switch (center.getRemainRaiting()) {
			case JUDGE_STR_A:
				if (StringUtils.isNumeric(center.getLineA())) {
					line = center.getLineA();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_B:
				if (StringUtils.isNumeric(center.getLineB())) {
					line = center.getLineB();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_C:
				if (StringUtils.isNumeric(center.getLineC())) {
					line = center.getLineC();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_D:
				if (StringUtils.isNumeric(center.getLineD())) {
					line = center.getLineD();
					break;
				} else {
					return ASTERISK;
				}
			default:
				return ASTERISK;
		}

		return Integer.toString(Integer.parseInt(line) - Integer.parseInt(center.getScore()));
	}

	/**
	 * あと何点-偏差値を返します。
	 *
	 * @param second {@link ExmntnUnivDetailSecondSvcOutBean}
	 * @return あと何点-偏差値
	 */
	private String getRemaingDeviation(ExmntnUnivDetailSecondSvcOutBean second) {

		if (!NumberUtils.isNumber(second.getDev())) {
			return ASTERISK;
		}

		String line;
		switch (second.getRemainRaiting()) {
			case JUDGE_STR_A:
				if (NumberUtils.isNumber(second.getLineA())) {
					line = second.getLineA();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_B:
				if (NumberUtils.isNumber(second.getLineB())) {
					line = second.getLineB();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_C:
				if (NumberUtils.isNumber(second.getLineC())) {
					line = second.getLineC();
					break;
				} else {
					return ASTERISK;
				}
			case JUDGE_STR_D:
				if (NumberUtils.isNumber(second.getLineD())) {
					line = second.getLineD();
					break;
				} else {
					return ASTERISK;
				}
			default:
				return ASTERISK;
		}

		int iLine = (int) (Double.parseDouble(line) * 10);
		int iDev = (int) (Double.parseDouble(second.getDev()) * 10);

		return Double.toString((double) (iLine - iDev) / 10);
	}

	/**
	 * 満点値を文字列に変換します。
	 *
	 * @param point 満点値
	 * @return 表示用文字列
	 */
	private String convFullPoint(int point) {
		if (point > 0 && point != JudgementConstants.Detail.FULL_POINT_NA) {
			return Integer.toString(point);
		} else {
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 英語の科目配点文字列を生成します。
	 *
	 * @param subject 課し科目
	 * @param allot 配点
	 * @return 英語の科目配点文字列
	 */
	private String createEnglishAllot(ImposingSubject subject, double allot) {
		if (allot == 0 || allot == JudgementConstants.Detail.ALLOT_NA) {
			return StringUtils.EMPTY;
		} else {
			return (subject.getElective().isImposed() ? ASTERISK : StringUtils.EMPTY) + Math.round(allot);
		}
	}

	/**
	 * 判定採用成績リストからペア科目の操作得点を抽出して返します。
	 *
	 * @param list 判定採用成績リスト
	 * @return 抽出結果
	 */
	private HandledPoint findPairHandledPoint(List<HandledPoint> list) {
		for (HandledPoint point : list) {
			if (point.getPairSubCd() != null) {
				return point;
			}
		}
		return null;
	}

	/**
	 * 成績リストから指定した教科の成績を抽出して返します。
	 *
	 * @param list 成績リスト
	 * @param courseCd 教科コード
	 * @param isMark マーク模試フラグ
	 * @return 成績配列（サイズは3で固定）
	 */
	private BZSubjectRecord[] findRecords(List<?> list, String courseCd, boolean isMark) {
		BZSubjectRecord[] array = new BZSubjectRecord[3];
		if (list != null) {
			int index = 0;
			for (Object obj : list) {
				if (obj instanceof BZSubjectRecord) {
					BZSubjectRecord rec = (BZSubjectRecord) obj;
					/* マーク模試の集計科目は除外する */
					if (rec.getCourseCd().equals(courseCd) && (!isMark || !MARK_AGGREGATE_SUBCD.contains(rec.getSubCd()))) {
						array[index++] = rec;
					}
				}
			}
		}
		return array;
	}

	/**
	 * 指定した科目が課されているかを返します。
	 *
	 * @param detail 詳細データ
	 * @param keys 科目キー
	 * @return 課されているならtrue
	 */
	private boolean isImposed(CommonDetailHolder detail, SubjectKey... keys) {
		for (SubjectKey key : keys) {
			if (detail.getImposingSubject(key).isImposed()) {
				return true;
			}
		}
		return false;
	}

	/*    *//**
			* 指定した科目が課されているかを返します。
			*
			* @param detail 詳細データ
			* @param keys 科目キー
			* @return 課されているならtrue
			*//*
				private boolean isElective(CommonDetailHolder detail, SubjectKey... keys) {
				 for (SubjectKey key : keys) {
				     if (detail.getImposingSubject(key).getElective().isElective()) {
				         return true;
				     }
				 }
				 return false;
				}
				*/

	/**
	 * 指定した教科が課されているかを返します。
	 *
	 * @param detail 詳細データ
	 * @param courseIndex 教科インデックス
	 * @return 課されているならtrue
	 */
	private boolean isImposed(CommonDetailHolder detail, int courseIndex) {
		for (Object obj : detail.getImposingSubjects().entrySet()) {
			Entry<?, ?> entry = (Entry<?, ?>) obj;
			SubjectKey key = (SubjectKey) entry.getKey();
			ImposingSubject subject = (ImposingSubject) entry.getValue();
			if (subject.isImposed() && key.getCourseIndex() == courseIndex) {
				return true;
			}
		}
		return false;
	}

	/*    *//**
			* 指定した教科が課されているかを返します。
			*
			* @param detail 詳細データ
			* @param courseIndex 教科インデックス
			* @return 課されているならtrue
			*//*
				private boolean isElective(CommonDetailHolder detail, int courseIndex) {
				 for (Object obj : detail.getImposingSubjects().entrySet()) {
				     Entry<?, ?> entry = (Entry<?, ?>) obj;
				     SubjectKey key = (SubjectKey) entry.getKey();
				     ImposingSubject subject = (ImposingSubject) entry.getValue();
				     if (subject.getElective().isElective() && key.getCourseIndex() == courseIndex) {
				         return true;
				     }
				 }
				 return false;
				}
				*/

	/**
	 * 判定採用成績から指定した教科のみを抽出して返します。
	 *
	 * @param adoptedScore 判定採用成績
	 * @param courseIndex 教科インデックス
	 * @return 抽出結果
	 */
	private List<HandledPoint> findAdoptedScore(List<?> adoptedScore, int courseIndex) {
		List<HandledPoint> list = CollectionsUtil.newArrayList();
		for (Object obj : adoptedScore) {
			HandledPoint point = (HandledPoint) obj;
			if (point.getCourseIndex() == courseIndex) {
				list.add(point);
			}
		}
		return list;
	}

	/**
	 * 指定した教科の判定採用成績の合計点を求めます。
	 *
	 * @param judge 判定結果
	 * @param allotStr 配点文字列
	 * @param courseIndex 教科インデックス
	 * @return 合計点
	 */
	private double sumUpScore(JudgedResult judge, String allotStr, int courseIndex) {
		double total = -1.0;
		if (!allotStr.isEmpty()) {
			for (HandledPoint p : findAdoptedScore(judge.getBean().getCAdoptedScore(), courseIndex)) {
				total = JudgementUtil.add(total, p.getPoint());
			}
		}
		return total;
	}

	/**
	 * 指定した教科の判定採用成績の偏差値を返します。
	 *
	 * @param judge 判定結果
	 * @param courseIndex 教科インデックス
	 * @return 偏差値
	 */
	private String getDeviation(JudgedResult judge, int courseIndex) {
		for (HandledPoint p : findAdoptedScore(judge.getBean().getSAdoptedScore(), courseIndex)) {
			return formatDeviation(p.getPoint());
		}
		return StringUtils.EMPTY;
	}

	/**
	 * 判定採用成績から指定した成績を抽出して返します。
	 *
	 * @param adoptedScore 判定採用成績
	 * @param rec {@link BZSubjectRecord}
	 * @return 抽出結果
	 */
	private double findAdoptedScore(List<?> adoptedScore, BZSubjectRecord rec) {
		if (rec != null) {
			for (Object obj : adoptedScore) {
				HandledPoint point = (HandledPoint) obj;
				if (point.getSortKey().equals(rec.getSubCd())) {
					if (!"0".equals(rec.getScore()) && point.getPoint() == 0) {
						return -1.0;
					}
					return point.getPoint();
				}
			}
		}
		return -1.0;
	}

	/**
	 * 判定採用成績から指定した成績を抽出して返します。
	 *
	 * @param adoptedScore 判定採用成績
	 * @param recs {@link BZSubjectRecord}
	 * @return 抽出結果
	 */
	private double findAdoptedScore(List<?> adoptedScore, BZSubjectRecord[] recs) {
		double total = -1.0;
		if (recs != null) {
			for (BZSubjectRecord rec : recs) {
				if (rec == null)
					continue;
				for (Object obj : adoptedScore) {
					HandledPoint point = (HandledPoint) obj;
					if (point.getSortKey().equals(rec.getSubCd())) {
						if (!"0".equals(rec.getScore()) && point.getPoint() == 0) {
						} else {
							total = JudgementUtil.add(total, point.getPoint());
						}
					}
				}
			}
		}
		return total;
	}

	/**
	 * 判定採用成績から指定した教科の二次偏差値を抽出して返します。
	 *
	 * @param judge 判定結果
	 * @param list 成績リスト
	 * @param courseIndex 教科インデックス
	 * @param courseCd 教科コード
	 * @return 抽出結果
	 */
	private double[] findAdoptedDeviation(JudgedResult judge, List<?> list, int courseIndex, String courseCd) {

		/* 判定に採用された科目コードセットを作る */
		Set<String> subCd = CollectionsUtil.newHashSet();
		for (Object obj : judge.getBean().getSAdoptedScore()) {
			HandledPoint point = (HandledPoint) obj;
			if (point.getCourseIndex() == courseIndex) {
				if (point.getPairSubCd() == null) {
					subCd.add(point.getSortKey());
				} else {
					for (Object value : point.getPairSubCd()) {
						subCd.add((String) value);
					}
				}
			}
		}

		/* 判定に採用された成績を詰める */
		double[] score = { -1.0, -1.0 };
		BZSubjectRecord[] rec = findRecords(list, courseCd, false);
		for (int i = 0; i < rec.length; i++) {
			if (rec[i] != null && subCd.contains(rec[i].getSubCd())) {
				try {
					double deviation = Double.parseDouble(rec[i].getCDeviation());
					score[i < score.length ? i : score.length - 1] = deviation;
				} catch (Exception e) {
				}
			}
		}

		return score;
	}

	/**
	 * ペア科目の入力得点を返します。
	 *
	 * @param isCenter センター・リサーチフラグ
	 * @param rec {@link BZSubjectRecord}
	 * @param pair ペア科目の操作得点
	 * @return ペア科目の入力得点
	 */
	private int getPairScore(boolean isCenter, BZSubjectRecord rec, HandledPoint pair) {
		if (rec == null || !pair.getPairSubCd().contains(rec.getSubCd())) {
			return -1;
		} else {
			return Integer.parseInt(isCenter ? rec.getScore() : rec.getCvsScore());
		}
	}

	/**
	 * センタ得点を表示用にフォーマットします。
	 *
	 * @param score センタ得点
	 * @return フォーマット結果
	 */
	private String formatScore(double score) {
		return score > -1 ? Long.toString(Math.round(score)) : StringUtils.EMPTY;
	}

	/**
	 * 二次偏差値を表示用にフォーマットします。
	 *
	 * @param score 二次偏差値
	 * @return フォーマット結果
	 */
	private String formatDeviation(double score) {
		return score > -1 ? Double.toString(score) : StringUtils.EMPTY;
	}

	/**
	 * ペア得点を分解します。
	 *
	 * @param pair ペア科目の操作得点
	 * @param subScore 科目得点
	 * @param subTotal ペア科目の科目得点の合計
	 * @return 分解結果
	 */
	private double disassemblePair(HandledPoint pair, int subScore, int subTotal) {
		if (subScore < 0) {
			return -1.0;
		} else if (subScore != 0 && pair.getPoint() == 0) {
			return -1.0;
		} else if (subTotal <= 0) {
			return 0.0;
		} else {
			return JudgementUtil.round2ndPnt(pair.getPoint() * subScore / subTotal);
		}
	}

	/**
	 * 2つの正の整数を足した結果を返します。<br>
	 * 無効値の場合は加算対象としません。
	 *
	 * @param a 整数a
	 * @param b 整数b
	 * @return 計算結果
	 */
	private int add(int a, int b) {
		int total = 0;
		if (a > -1) {
			total += a;
		}
		if (b > -1) {
			total += b;
		}
		return total;
	}

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;

/**
 *
 * 出力機能ノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
abstract class OutputNode extends FunctionNode {

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    OutputNode(Object userObject) {
        super(userObject);
    }

    /**
     * 出力処理を実行します。
     *
     * @param helper {@link OutputMainHelper}
     */
    abstract void output(OutputMainHelper helper);

}

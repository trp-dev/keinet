package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.lang.reflect.Field;

import javax.swing.JTextField;
import javax.swing.border.MatteBorder;
import javax.swing.text.AbstractDocument;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NumberField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.ReadOnly;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * テキストフィールドです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class TextField extends JTextField implements BZComponent {

    /**
     * コンストラクタです。
     */
    public TextField() {
        setBorder(new MatteBorder(1, 1, 1, 1, new Color(165, 172, 178)));
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        HorizontalAlignment align = field.getAnnotation(HorizontalAlignment.class);
        if (align != null) {
            setHorizontalAlignment(align.value());
        }

        if (field.isAnnotationPresent(ReadOnly.class)) {
            setEditable(false);
            setBackground(new Color(240, 240, 240));
        }

        NumberField numberField = field.getAnnotation(NumberField.class);
        if (numberField != null) {
            /* IMEをOFFにする */
            enableInputMethods(false);
            /* チェックを行うフィルタを設定する */
            ((AbstractDocument) getDocument())
                    .setDocumentFilter(new NumberDocumentFilter(numberField.maxLength(), numberField.scale()));
        }

        MaxLength maxLength = field.getAnnotation(MaxLength.class);
        if (maxLength != null) {
            ((AbstractDocument) getDocument()).setDocumentFilter(new MaxLengthDocumentFilter(maxLength.value()));
        }
    }

}

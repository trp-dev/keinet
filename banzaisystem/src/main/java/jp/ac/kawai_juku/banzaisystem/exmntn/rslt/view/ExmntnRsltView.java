package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.component.ExmntnRsltTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

/**
 *
 * 検索結果画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class ExmntnRsltView extends SubView {

    /** 検索結果フィルタアクション名 */
    private static final String FILTER_ACTION = "filter";

    /** 前期チェックボックス */
    @Location(x = 196, y = 11)
    @CodeConfig(Code.UNIV_RESULT_UDIV_FIRST)
    @Action(FILTER_ACTION)
    public CodeCheckBox firstSemesterCheckBox;

    /** 後期チェックボックス */
    @Location(x = 249, y = 11)
    @CodeConfig(Code.UNIV_RESULT_UDIV_SECOND)
    @Action(FILTER_ACTION)
    public CodeCheckBox secondSemesterCheckBox;

    /** 中期チェックボックス */
    @Location(x = 302, y = 11)
    @CodeConfig(Code.UNIV_RESULT_UDIV_MIDDLE)
    @Action(FILTER_ACTION)
    public CodeCheckBox middleSemesterCheckBox;

    /** 別日程チェックボックス */
    @Location(x = 356, y = 11)
    @CodeConfig(Code.UNIV_RESULT_UDIV_OTHER)
    @Action(FILTER_ACTION)
    public CodeCheckBox anotherDayCheckBox;

    /** センター利用チェックボックス */
    @Location(x = 499, y = 11)
    @CodeConfig(Code.UNIV_RESULT_PDIV_CENTER)
    @Action(FILTER_ACTION)
    public CodeCheckBox centerCheckBox;

    /** 一般チェックボックス */
    @Location(x = 601, y = 11)
    @CodeConfig(Code.UNIV_RESULT_PDIV_GENERAL)
    @Action(FILTER_ACTION)
    public CodeCheckBox publicCheckBox;

    /** 検索結果テーブル */
    @Location(x = 5, y = 36)
    @Size(width = 672, height = 381)
    public ExmntnRsltTable resultTable;

    /** 該当件数ラベル */
    @Location(x = 70, y = 8)
    @Font(BZFont.RESULT_COUNT)
    public TextLabel countLabel;

    /** 備考欄ラベル */
    @Location(x = 14, y = 421)
    @Size(width = 471, height = 34)
    @Font(BZFont.UNIV_REMARKS)
    public TextLabel remarksLabel;

    /** 志望大学リストに追加するボタン */
    @Location(x = 325, y = 425)
    @Disabled
    public ImageButton addCandidateUnivButton;

    /** 印刷するボタン */
    @Location(x = 547, y = 422)
    public ImageButton printButton;

}

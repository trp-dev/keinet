package jp.ac.kawai_juku.banzaisystem.selstd.main.component;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;

/**
 *
 * 個人名のみテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdMainNameTable extends SelstdMainTable {

    /**
     * コンストラクタです。
     */
    public SelstdMainNameTable() {
        super("number", TableType.NAME);
    }

    @Override
    protected JPanel createColumnHeaderView() {

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

        panel.add(createHeaderButton("grade", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("class", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("number", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("name", CellAlignment.LEFT, true));
        panel.add(createHeaderLabel("blank", CellAlignment.LEFT, false));

        return panel;
    }

    @Override
    protected JPanel createFixedColumnHeaderView() {
        /* 固定領域はなし */
        return null;
    }

}

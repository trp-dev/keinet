package jp.ac.kawai_juku.banzaisystem.mainte.mstr.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.UNIV_FILE_NAME;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.VERSION_NUMBER;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.sql.SQLException;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClient;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClientException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.controller.MainteImptController;
import jp.ac.kawai_juku.banzaisystem.mainte.main.view.MainteMainView;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.helper.MainteMstrHelper;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet.UnivDb;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.view.MainteMstrView;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.controller.MainteStngController;
import jp.ac.kawai_juku.banzaisystem_bootstrap.BZBootStrap;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * メンテナンス-大学マスタ・プログラム最新化画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)OZEKI.Hiroshige
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMstrController extends SubController {

    /** 大学マスタファイルのファイルフィルタ */
    private final FileFilter filter = new FileFilter() {

        /** 取り込み可能な大学マスタファイル名のパターン */
        private final Pattern pattern = Pattern.compile("univ.*\\.db");

        @Override
        public String getDescription() {
            return "大学マスタファイル（univ*.db）";
        }

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || pattern.matcher(f.getName()).matches();
        }
    };

    /** View */
    @Resource(name = "mainteMstrView")
    private MainteMstrView view;

    /** メンテナンスタブ画面のView */
    @Resource
    private MainteMainView mainteMainView;

    /** Service */
    @Resource
    private MainteMstrService mainteMstrService;

    /** Helper */
    @Resource
    private MainteMstrHelper mainteMstrHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** インターネット情報：ダウンロード可能大学マスタセットのキャッシュ */
    private UnivDbSet dbSet;

    /** インターネット情報：アプリケーションバージョンのキャッシュ */
    private String appVersion = StringUtils.EMPTY;

    @Override
    @Logging(GamenId.E3)
    public void activatedAction() {
        renewalScreenInfo(false);
    }

    /**
     * 画面情報を更新ボタン押下アクションです。
     *
     * @return null（画面遷移なし）
     */
    @Execute
    public ExecuteResult renewalScreenInfoButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {

                /* HTTPクライアントを初期化する */
                UpdaterHttpClient client = new UpdaterHttpClient();

                /* 更新データ配信サイトからダウンロード可能大学マスタセットを取得する */
                dbSet = mainteMstrHelper.findDbSet(client, false);

                /* 更新データ配信サイトから最新バージョンを取得する */
                try {
                    appVersion = mainteMstrService.findAppVersion(client).getVersion();
                    /* 取得した最新バージョンを保存する */
                    SettingProps props = SettingProps.load();
                    props.setValue(SettingKey.LATEST_VERSION, appVersion);
                    props.store();
                } catch (UpdaterHttpClientException e) {
                    /* 通信エラーはユーザに通知しない */
                    appVersion = StringUtils.EMPTY;
                }

                return null;
            }

            protected void done() {
                renewalScreenInfo(false);
            }
        };
    }

    /**
     * 画面情報を更新します。
     *
     * @param checkMismatchingMasterVersionFlg 大学マスタバージョン不整合チェックフラグ
     */
    private void renewalScreenInfo(boolean checkMismatchingMasterVersionFlg) {
        initUnivMasterTable();
        initProgramArea();
        initButtonStatus();
        changeComponentsStatus();
        if (checkMismatchingMasterVersionFlg && systemDto.isMismatchingMasterVersionFlg()) {
            throw new MessageDialogException(getMessage("error.mainte.mstr.CM_E014"));
        }
    }

    /**
     * コンポーネント状態をバージョン不整合状態に合わせて変更します。
     */
    private void changeComponentsStatus() {
        boolean flag = !systemDto.isMismatchingVersion();
        mainteMainView.selectStudentButton.setEnabled(flag);
        mainteMainView.exmntnButton.setEnabled(flag);
        mainteMainView.reportButton.setEnabled(flag);
        mainteMainView.tabbedPane.setTabEnabled(MainteImptController.class, flag);
        mainteMainView.tabbedPane.setTabEnabled(MainteStngController.class, flag);
    }

    /**
     * 大学マスタテーブルを初期化します。
     */
    private void initUnivMasterTable() {
        view.univMasterTable.setDataList(mainteMstrService.createDataList(dbSet));
    }

    /**
     * プログラム欄を初期化します。
     */
    private void initProgramArea() {

        String latestVersion = appVersion;

        if (latestVersion.isEmpty()) {
            /* インターネットの情報が無ければ、前回保存していた最新バージョンを表示する */
            latestVersion = SettingProps.load().getValue(SettingKey.LATEST_VERSION);
        }

        /* 最新バージョンを設定する */
        view.latestVersionLabel.setText(latestVersion);

        /* 状況を設定する */
        if (latestVersion.isEmpty()) {
            view.statusLabel.setText(StringUtils.EMPTY);
            view.downloadUnivMstUpdatePrgButton.setEnabled(false);
        } else if (Double.parseDouble(latestVersion) > VERSION_NUMBER) {
            view.statusLabel.setText(getMessage("label.mainte.mstr.statusUpdatable"));
            view.downloadUnivMstUpdatePrgButton.setEnabled(true);
        } else {
            view.statusLabel.setText(Code.PG_UPDATE_STATUS_UPDATED.getName());
            view.downloadUnivMstUpdatePrgButton.setEnabled(false);
        }
    }

    /**
     * 大学マスタ更新・ダウンロードボタンの状態を初期化します。
     */
    private void initButtonStatus() {
        boolean b = dbSet != null && !dbSet.isEmpty();
        view.updateMasterButton.setEnabled(b);
        view.downloadUnivMstButton.setEnabled(b);
    }

    /**
     * 大学マスタを更新ボタン押下アクションです。
     *
     * @return null（画面遷移なし）
     */
    @Execute
    public ExecuteResult updateMasterButtonAction() {

        // 確認メッセージ
        String msg = isBothUpdate() ? "label.mainte.mstr.E_W004" : "label.mainte.mstr.E_W002";
        if (!showConfirmDialog(msg)) {
            return null;
        }
        final UpdaterHttpClient client = new UpdaterHttpClient();
        final UnivDb univdb = mainteMstrHelper.getUpdateTargetUnivMaster(client, view.univMasterTable.getSelectedMaster());
        /* 年度切り替えが発生する場合は確認ダイアログを表示する */
        if (univdb.getEventYear().compareTo(systemDto.getUnivYear()) > 0 && !showConfirmDialog("label.mainte.mstr.E_W005")) {
            return null;
        }

        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                // 同時更新の場合、プログラムを先に更新する。
                if (isBothUpdate()) {
                    // プログラムを更新
                    mainteMstrService.updateProgram();
                }
                // 大学マスタを更新
                mainteMstrService.updateMaster(client, univdb, isBothUpdate());
                // 同時更新の場合、再起動を行う。
                if (isBothUpdate()) {
                    System.exit(BZBootStrap.REBOOT_STATUS);
                }
                return null;
            }

            @Override
            protected void done() {
                renewalScreenInfo(true);
            }
        };
    }

    /**
     * 大学マスタをダウンロードボタン押下アクションです。
     *
     * @return null（画面遷移なし）
     */
    @Execute
    public ExecuteResult downloadUnivMstButtonAction() {

        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setSelectedFile(new File(UNIV_FILE_NAME));

        int selected = fileChooser.showSaveDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            final UpdaterHttpClient client = new UpdaterHttpClient();
            final UnivDb univDb = mainteMstrHelper.getDownloadTargetUnivMaster(client, view.univMasterTable.getSelectedMaster());
            return new TaskResult() {
                @Override
                protected ExecuteResult doTask() {
                    mainteMstrService.downloadUnivMaster(client, fileChooser.getSelectedFile(), univDb);
                    return null;
                }
            };
        }

        return null;
    }

    /**
     * 処理継続確認ダイアログを表示します。
     *
     * @param key メッセージキー
     * @return 処理継続ならtrue
     */
    private boolean showConfirmDialog(String key) {
        String[] button = { getMessage("label.mainte.mstr.dialogYes"), getMessage("label.mainte.mstr.dialogNo") };
        int result = JOptionPane.showOptionDialog(getActiveWindow(), getMessage(key), null, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

    /**
     * 大学マスタ手動取り込みボタン押下アクションです。
     *
     * @return null（画面遷移なし）
     */
    @Execute
    public ExecuteResult importButtonAction() {

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(filter);

        /* HTTPクライアントを初期化する */
        UpdaterHttpClient client = new UpdaterHttpClient();

        /* 更新データ配信サイトから最新バージョンを取得する */
        try {
            appVersion = mainteMstrService.findAppVersion(client).getVersion();
            /* 取得した最新バージョンを保存する */
            SettingProps props = SettingProps.load();
            props.setValue(SettingKey.LATEST_VERSION, appVersion);
            props.store();
        } catch (UpdaterHttpClientException e) {
            /* 通信エラーはユーザに通知しない */
            appVersion = StringUtils.EMPTY;
        }
        initProgramArea();

        int selected = fileChooser.showOpenDialog(getActiveWindow());
        if (selected == JFileChooser.APPROVE_OPTION) {
            final File file = fileChooser.getSelectedFile();
            if (file.length() == 0 || !filter.accept(file)) {
                /* 大学マスタファイルとして不正ならエラーとする */
                throw new MessageDialogException(getMessage("error.mainte.mstr.E_E002"));
            } else {
                /* 取り込む大学マスタの大学マスタ情報を取得する */
                final UnivMasterBean univMasterBean;
                try {
                    univMasterBean = mainteMstrService.getUnivMasterBean(file);
                } catch (SQLException e) {
                    throw new MessageDialogException(getMessage("error.mainte.mstr.E_E002", e));
                }
                /* 年度繰り越し処理の確認を行う */
                if (univMasterBean.getEventYear().compareTo(systemDto.getUnivYear()) > 0 && !showConfirmDialog("label.mainte.mstr.E_W005")) {
                    return null;
                }
                /* インポート処理はバックグラウンド実行する */
                return new TaskResult() {
                    @Override
                    protected ExecuteResult doTask() {
                        boolean flg = view.downloadUnivMstUpdatePrgButton.isEnabled();
                        if (flg) {
                            // プログラムを更新
                            mainteMstrService.updateProgram();
                        }
                        // 大学マスタを更新
                        mainteMstrService.importFile(file, univMasterBean, flg);
                        if (flg) {
                            // プログラムを再起動
                            System.exit(BZBootStrap.REBOOT_STATUS);
                        }
                        return null;
                    }

                    @Override
                    protected void done() {
                        renewalScreenInfo(true);
                    }
                };
            }
        }

        return null;
    }

    /**
     * プログラムを更新ボタン押下アクションです。
     *
     * @return null（画面遷移なし）
     */
    @Execute
    public ExecuteResult downloadUnivMstUpdatePrgButtonAction() {
        // 確認メッセージ
        String msg = isBothUpdate() ? "label.mainte.mstr.E_W004" : "label.mainte.mstr.E_W003";
        if (!showConfirmDialog(msg)) {
            return null;
        }
        final UpdaterHttpClient client = new UpdaterHttpClient();
        final UnivDb univdb = mainteMstrHelper.getUpdateTargetUnivMaster(client, view.univMasterTable.getSelectedMaster());
        /* 年度切り替えが発生する場合は確認ダイアログを表示する */
        if (isBothUpdate() && univdb.getEventYear().compareTo(systemDto.getUnivYear()) > 0 && !showConfirmDialog("label.mainte.mstr.E_W005")) {
            return null;
        }

        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                // プログラムを更新
                mainteMstrService.updateProgram();
                // 同時更新の場合
                if (isBothUpdate()) {
                    //大学マスタを更新
                    mainteMstrService.updateMaster(client, univdb, true);
                }
                // プログラムを再起動
                System.exit(BZBootStrap.REBOOT_STATUS);
                return null;
            }
        };
    }

    /**
     * プログラム＆大学マスタが更新可能かどうかを返却します。
     *
     * @return true:両方とも更新可能、false:それ以外
     */
    public boolean isBothUpdate() {
        final boolean isDbSet = dbSet != null && !dbSet.isEmpty();
        final boolean isDbUpdatable = view.univMasterTable.isUpdatableMaster();
        final boolean isDbAllUpd = view.univMasterTable.isAllUpdatedMaster();

        boolean isPrgUpd = view.downloadUnivMstUpdatePrgButton.isEnabled();
        boolean isDbUpd = isDbAllUpd ? isDbSet : isDbUpdatable;
        return isPrgUpd && isDbUpd;
    }

}

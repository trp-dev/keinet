package jp.ac.kawai_juku.banzaisystem.framework.excel;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.io.File;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.FormMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.maker.FormMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * Excelファイルの起動クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExcelLauncher {

    /** Excelファイル生成クラスと入力Beanのペアリスト */
    private final List<ExcelPair<?>> pairList = CollectionsUtil.newArrayList();

    /** 画面ID */
    private final GamenId gamenId;

    /** ログ出力サービス */
    private final LogService logService;

    /**
     * コンストラクタです。
     *
     * @param gamenId 画面ID
     */
    public ExcelLauncher(GamenId gamenId) {
        this.gamenId = gamenId;
        this.logService = getComponent(LogService.class);
    }

    /**
     * Excelファイル生成クラスを追加します。
     *
     * @param clazz Excelファイル生成クラス
     * @param inBean {@link FormMakerInBean}
     * @param <T> 入力Beanの型
     * @return 自分自身のインスタンス
     */
    public <T extends FormMakerInBean> ExcelLauncher addCreator(Class<? extends FormMaker<T>> clazz, T inBean) {
        pairList.add(new ExcelPair<T>(clazz, inBean));
        return this;
    }

    /**
     * Excelファイルを生成して起動します。
     */
    public void launch() {

        List<File> fileList = CollectionsUtil.newArrayList();
        List<PrintId> printIdList = CollectionsUtil.newArrayList();
        for (ExcelPair<?> pair : pairList) {
            Pair<PrintId, List<File>> result = pair.create();
            if (!result.getRight().isEmpty()) {
                printIdList.add(result.getLeft());
                fileList.addAll(result.getRight());
            }
        }

        if (fileList.isEmpty()) {
            /* 印刷対象がなければ警告ダイアログを表示する */
            throw new MessageDialogException(getMessage("error.framework.CM_E001"));
        } else {
            /* Excelを起動する */
            for (File file : fileList) {
                launch(file);
            }
            /* ログを出力する */
            logService.output(gamenId, printIdList);
        }
    }

    /**
     * ファイルをExcel起動します。
     *
     * @param file ファイル
     */
    private void launch(File file) {

        String[] cmdarray = new String[7];
        cmdarray[0] = "cmd";
        cmdarray[1] = "/c";
        cmdarray[2] = "start";
        cmdarray[3] = "";
        cmdarray[4] = "excel.exe";
        cmdarray[5] = file.getAbsolutePath();
        cmdarray[6] = "/e";

        try {
            Runtime.getRuntime().exec(cmdarray);
        } catch (Exception e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E008"), e);
        }
    }

    /** Excelファイル生成クラスと入力Beanのペアを保持するクラス */
    private static final class ExcelPair<T extends FormMakerInBean> {

        /** Excelファイル生成クラス */
        private Class<? extends FormMaker<T>> clazz;

        /** Excelファイル生成クラスの入力Bean */
        private T inBean;

        /**
         * コンストラクタです。
         *
         * @param clazz Excelファイル生成クラス
         * @param inBean Excelファイル生成クラスの入力Bean
         */
        private ExcelPair(Class<? extends FormMaker<T>> clazz, T inBean) {
            this.clazz = clazz;
            this.inBean = inBean;
        }

        /**
         * Excelファイルを生成します。
         *
         * @return 帳票IDとExcelファイルリストのペア
         */
        private Pair<PrintId, List<File>> create() {
            FormMaker<T> creator = getComponent(clazz);
            return Pair.of(creator.getPrintId(), creator.create(inBean));
        }

    }

}

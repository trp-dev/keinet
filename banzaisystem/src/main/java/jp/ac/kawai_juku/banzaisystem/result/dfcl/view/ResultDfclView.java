package jp.ac.kawai_juku.banzaisystem.result.dfcl.view;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZList;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * 学部学科選択ダイアログViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class ResultDfclView extends AbstractView {

    /** 大学名ラベル */
    @Location(x = 28, y = 25)
    @Size(width = 280, height = 17)
    @Font(BZFont.UNIV_NAME_DIALOG)
    @Foreground(r = 55, g = 88, b = 41)
    public TextLabel univNameLabel;

    /** 学部リストテーブル */
    @Location(x = 28, y = 77)
    @Size(width = 116, height = 223)
    public BZList<LabelValueBean> facultyList;

    /** 学科リストテーブル */
    @Location(x = 163, y = 77)
    @Size(width = 116, height = 223)
    public BZList<LabelValueBean> deptList;

    /** OKボタン */
    @Location(x = 70, y = 328)
    @Disabled
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 157, y = 328)
    public ImageButton cancelButton;

}

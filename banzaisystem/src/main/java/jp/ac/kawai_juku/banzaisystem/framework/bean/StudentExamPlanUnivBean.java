package jp.ac.kawai_juku.banzaisystem.framework.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 生徒用の受験予定大学を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class StudentExamPlanUnivBean extends UnivKeyBean {

    /** 地方フラグ */
    private String provincesFlg;

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public StudentExamPlanUnivBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * 地方フラグを返します。
     *
     * @return 地方フラグ
     */
    public String getProvincesFlg() {
        return provincesFlg;
    }

    /**
     * 地方フラグをセットします。
     *
     * @param provincesFlg 地方フラグ
     */
    public void setProvincesFlg(String provincesFlg) {
        this.provincesFlg = provincesFlg;
    }

}

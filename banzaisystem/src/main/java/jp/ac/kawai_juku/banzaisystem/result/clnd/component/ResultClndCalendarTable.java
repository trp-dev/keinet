package jp.ac.kawai_juku.banzaisystem.result.clnd.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.result.clnd.helper.ResultClndHelper;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;

import org.apache.commons.lang3.time.FastDateFormat;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * カレンダーテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ResultClndCalendarTable extends BZTable implements BZComponent {

    /** 外側の枠線の色 */
    private static final Color BORDER_COLOR = new Color(153, 153, 153);

    /** 日付の色：日曜日 */
    private static final Color DATE_COLOR_SUN = new Color(204, 50, 0);

    /** 日付の色：土曜日 */
    private static final Color DATE_COLOR_SAT = new Color(1, 103, 203);

    /** 日付の色：平日 */
    private static final Color DATE_COLOR_WEEKDAY = new Color(220, 137, 67);

    /** 日付の色：選択中 */
    private static final Color DATE_COLOR_ACTIVE = new Color(103, 52, 153);

    /** セルの幅 */
    private static final int CELL_WIDTH = 140;

    /** セルの高さ */
    private static final int CELL_HEIGHT = 45;

    /** 大学名パネルの幅 */
    private static final int UNIV_WIDTH = 122;

    /** 空欄セルの背景画像 */
    private final BufferedImage blankImg = ImageUtil.readImage(getClass(), "table/blank.png");

    /** 非アクティブセルの背景画像 */
    private final BufferedImage inactiveImg = ImageUtil.readImage(getClass(), "table/inactive.png");

    /** アクティブセルの背景画像 */
    private final BufferedImage activeImg = ImageUtil.readImage(getClass(), "table/active.png");

    /** moreボタン（レンダラ用） */
    private final ImageButton moreRendererButton = new ImageButton();

    /** moreボタン（エディタ用） */
    private final ImageButton moreEditorButton = new ImageButton();

    /** 大学名パネル */
    private final JPanel univPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 8, 8));

    /** 大学名ラベル */
    private final JLabel univLabel = new JLabel();

    /** 日付フォーマット */
    private final FastDateFormat format = FastDateFormat.getInstance("yyyyMMdd");

    /** 作業用カレンダー */
    private final Calendar cal = Calendar.getInstance();

    /** 合格発表日別大学リストマップ */
    private final Map<String, List<ResultUnivBean>> univMap = CollectionsUtil.newHashMap();

    /** 選択中セル */
    private final SelectedCell selectedCell = new SelectedCell();

    /**
     * コンストラクタです。
     */
    public ResultClndCalendarTable() {

        super(new BZTableModel(7));

        setFocusable(false);
        setRowHeight(CELL_HEIGHT);
        setGridColor(new Color(204, 204, 204));
        setRowSelectionAllowed(false);

        /* 「他」ボタンのロールオーバーイベントを発生させるため、マウスカーソルが乗ったボタンセルを編集状態にする */
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int row = rowAtPoint(e.getPoint());
                int column = columnAtPoint(e.getPoint());
                String value = getCellValue(row, column);
                if (univMap.containsKey(value) && (column != getEditingColumn() || row != getEditingRow())) {
                    editingCanceled(null);
                    editCellAt(row, column);
                }
            }
        });

        /* 日付選択時の動作を設定する */
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int row = rowAtPoint(e.getPoint());
                int column = columnAtPoint(e.getPoint());
                String value = getCellValue(row, column);
                List<ResultUnivBean> list = univMap.get(value);
                if (list != null && !value.equals(selectedCell.value)) {
                    selectedCell.value = value;
                    /* セルが再描画されるようにする */
                    editingCanceled(null);
                    if (selectedCell.row > -1 && selectedCell.column > -1) {
                        ((DefaultTableModel) getModel()).fireTableCellUpdated(selectedCell.row, selectedCell.column);
                    }
                    selectedCell.row = row;
                    selectedCell.column = column;
                    /* 受験者を表示する */
                    List<String> univCdList = new ArrayList<>(list.size());
                    for (ResultUnivBean bean : list) {
                        univCdList.add(bean.getUnivCd());
                    }
                    SingletonS2Container.getComponent(ResultClndHelper.class).displayExaminee(value, univCdList);
                }
            }
        });

        TableColumnModel columnModel = getColumnModel();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            TableColumn column = columnModel.getColumn(i);
            column.setPreferredWidth(CELL_WIDTH);
        }

        for (int i = 0; i < 6; i++) {
            ((DefaultTableModel) getModel()).addRow(new Object[7]);
        }

        setDefaultRenderer(Object.class, new CellRenderer());
        setDefaultEditor(Object.class, new CellEditor());

        univPanel.setBorder(BorderFactory.createLineBorder(new Color(255, 215, 55), 1));
        univPanel.setBackground(new Color(249, 241, 252));
        univPanel.setVisible(false);
        univPanel.add(univLabel);
    }

    /**
     * 指定したセルの値を返します。
     *
     * @param row 行位置
     * @param column 列位置
     * @return セルの値
     */
    private String getCellValue(int row, int column) {
        if (row < 0) {
            return null;
        }
        if (column < 0) {
            return null;
        }
        return (String) getValueAt(row, column);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        /* 外側の枠線を描画する */
        g.setColor(BORDER_COLOR);
        g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
        g.drawLine(getWidth() - 1, 0, getWidth() - 1, getHeight() - 1);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        view.addLayerComponent(univPanel);

        moreRendererButton.initialize(view, "moreButton");
        moreRendererButton.setLocation(107, 28);

        moreEditorButton.initialize(view, "moreButton");
        moreEditorButton.setLocation(107, 28);
        moreEditorButton.setRolloverIcon(moreEditorButton.getPressedIcon());
        moreEditorButton.removeMouseListener(ImageButton.CURSOR_MOUSE_LISTENER);
        for (ActionListener l : moreEditorButton.getActionListeners()) {
            moreEditorButton.removeActionListener(l);
        }
        moreEditorButton.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                JTable table = ResultClndCalendarTable.this;
                JButton button = (JButton) e.getSource();
                int x = table.getX() + button.getX() + (CELL_WIDTH + 1) * table.getEditingColumn();
                int y = table.getY() + button.getY() + button.getHeight() + CELL_HEIGHT * table.getEditingRow() + 12;
                if (x + UNIV_WIDTH > table.getX() + table.getWidth()) {
                    x -= UNIV_WIDTH - button.getWidth();
                }
                univPanel.setLocation(x, y);
                univLabel.setText(createMoreLabel((String) getValueAt(table.getEditingRow(), table.getEditingColumn())));
                univPanel.setSize(UNIV_WIDTH, univLabel.getPreferredSize().height + 18);
                univPanel.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                univPanel.setVisible(false);
            }

            /**
             * 指定した日付のmore表示用ラベルを生成します。
             *
             * @param yyyymmdd 日付
             * @return more表示用ラベル
             */
            private String createMoreLabel(String yyyymmdd) {
                StringBuilder sb = new StringBuilder();
                sb.append("<HTML>");
                for (ResultUnivBean bean : univMap.get(yyyymmdd)) {
                    sb.append(bean.getUnivName()).append("<BR>");
                }
                sb.append("</HTML>");
                return sb.toString();
            }
        });
    }

    /**
     * 合格発表日リストをセットします。
     *
     * @param sucAnnDateList 合格発表日リスト
     */
    public void setSucAnnDateList(List<ResultUnivBean> sucAnnDateList) {
        univMap.clear();
        for (ResultUnivBean bean : sucAnnDateList) {
            List<ResultUnivBean> list = univMap.get(bean.getSucAnnDate());
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(bean);
            univMap.put(bean.getSucAnnDate(), list);
        }
    }

    /**
     * 表示する年月をセットします。
     *
     * @param year 表示する年
     * @param month 表示する月（0ベース）
     */
    public void setDispMonth(int year, int month) {

        cal.set(year, month, 1);
        cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_WEEK) - 1));

        Vector<Vector<String>> dataVector = CollectionsUtil.newVector(42);
        for (int i = 0; i < 6; i++) {
            Vector<String> row = CollectionsUtil.newVector(7);
            for (int j = 0; j < 7; j++) {
                if (cal.get(Calendar.MONTH) == month) {
                    row.add(format.format(cal));
                } else {
                    row.add(null);
                }
                cal.add(Calendar.DATE, 1);
            }
            dataVector.add(row);
        }

        /* 編集中セルが再描画されるよう、編集を取り消す */
        editingCanceled(null);

        ((BZTableModel) getModel()).setDataVector(dataVector);
    }

    /** セルレンダラ */
    private final class CellRenderer implements TableCellRenderer {

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {
            return new CellPanel((String) value, moreRendererButton);
        }

    }

    /** セルエディタ */
    private final class CellEditor extends AbstractCellEditor implements TableCellEditor {

        /** セルの値 */
        private Object value;

        @Override
        public Component
                getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            this.value = value;
            return new CellPanel((String) value, moreEditorButton);
        }

        @Override
        public Object getCellEditorValue() {
            return value;
        }

        @Override
        public boolean shouldSelectCell(EventObject anEvent) {
            return false;
        }

    }

    /** セル描画パネル */
    private final class CellPanel extends JPanel {

        /** 背景画像 */
        private final BufferedImage img;

        /**
         * コンストラクタです。
         *
         * @param yyyymmdd セルの日付
         * @param moreButton moreボタン
         */
        private CellPanel(String yyyymmdd, ImageButton moreButton) {

            super(null);

            List<ResultUnivBean> univList = univMap.get(yyyymmdd);
            if (yyyymmdd == null || univList == null) {
                this.img = blankImg;
            } else if (yyyymmdd.equals(selectedCell.value)) {
                this.img = activeImg;
            } else {
                this.img = inactiveImg;
            }

            if (yyyymmdd != null) {
                addDateLabel(yyyymmdd);
                if (univList != null) {
                    addUnivLabel(univList.get(0).getUnivName(), 16);
                    if (univList.size() > 1) {
                        addUnivLabel(univList.get(1).getUnivName(), 29);
                    }
                    if (univList.size() > 2) {
                        add(moreButton);
                    }
                }
            }
        }

        /**
         * 日付ラベルを追加します。
         *
         * @param yyyymmdd 日付ラベル
         */
        private void addDateLabel(String yyyymmdd) {
            int year = Integer.parseInt(yyyymmdd.substring(0, 4));
            int month = Integer.parseInt(yyyymmdd.substring(4, 6)) - 1;
            int date = Integer.parseInt(yyyymmdd.substring(6));
            cal.set(year, month, date);

            JLabel label = new JLabel(Integer.toString(date));
            label.setFont(BZFont.CALENDAR_DATE.getFont());
            label.setLocation(5, 1);
            label.setSize(label.getPreferredSize());

            int dow = cal.get(Calendar.DAY_OF_WEEK);
            if (img == activeImg) {
                label.setForeground(DATE_COLOR_ACTIVE);
            } else if (dow == Calendar.SUNDAY) {
                label.setForeground(DATE_COLOR_SUN);
            } else if (dow == Calendar.SATURDAY) {
                label.setForeground(DATE_COLOR_SAT);
            } else {
                label.setForeground(DATE_COLOR_WEEKDAY);
            }

            add(label);
        }

        /**
         * 大学名ラベルを追加します。
         *
         * @param univName 大学名
         * @param y Y座標
         */
        private void addUnivLabel(String univName, int y) {
            JLabel label = new JLabel(univName);
            label.setFont(BZFont.DEFAULT.getFont());
            label.setLocation(5, y);
            label.setSize(label.getPreferredSize());
            add(label);
        }

        @Override
        protected void paintComponent(Graphics g) {
            g.drawImage(img, 0, 0, null);
        }

    }

    /** 選択中セルの情報を保持するクラス */
    private static final class SelectedCell {

        /** 選択中セルの行 */
        private int row = -1;

        /** 選択中セルの列 */
        private int column = -1;

        /** 選択中セルの値（YYYYMMDD） */
        private String value;

    }

}

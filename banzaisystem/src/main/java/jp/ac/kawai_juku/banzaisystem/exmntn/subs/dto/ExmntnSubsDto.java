package jp.ac.kawai_juku.banzaisystem.exmntn.subs.dto;

import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;

/**
 *
 * 大学検索画面のDTOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsDto {

    /** 検索条件Bean */
    private ExmntnSubsUnivSearchInBean searchBean;

    /**
     * 検索条件Beanを返します。
     *
     * @return 検索条件Bean
     */
    public ExmntnSubsUnivSearchInBean getSearchBean() {
        return searchBean;
    }

    /**
     * 検索条件Beanをセットします。
     *
     * @param searchBean 検索条件Bean
     */
    public void setSearchBean(ExmntnSubsUnivSearchInBean searchBean) {
        this.searchBean = searchBean;
    }

}

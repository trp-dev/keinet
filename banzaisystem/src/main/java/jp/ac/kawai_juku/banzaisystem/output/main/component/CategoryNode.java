package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * カテゴリノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class CategoryNode extends DefaultMutableTreeNode implements Node {

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    CategoryNode(Object userObject) {
        super(userObject);
    }

    @Override
    public Icon getIcon() {
        return null;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivMasterBean {

    /** 行事年度 */
    private final String eventYear;

    /** 模試区分 */
    private final String examDiv;

    /** 必要システムバージョン */
    private final String requiredVersion;

    /**
     * コンストラクタです。
     *
     * @param eventYear 行事年度
     * @param examDiv 模試区分
     * @param requiredVersion 必要システムバージョン
     */
    public UnivMasterBean(String eventYear, String examDiv, String requiredVersion) {
        this.eventYear = eventYear;
        this.examDiv = examDiv;
        this.requiredVersion = requiredVersion;
    }

    /**
     * 行事年度を返します。
     *
     * @return 行事年度
     */
    public String getEventYear() {
        return eventYear;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 必要システムバージョンを返します。
     *
     * @return 必要システムバージョン
     */
    public String getRequiredVersion() {
        return requiredVersion;
    }

    @Override
    public int hashCode() {
        return (eventYear + examDiv).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        UnivMasterBean o = (UnivMasterBean) obj;
        if (o == null) {
            return false;
        } else {
            return (eventYear + examDiv).equals(o.eventYear + o.examDiv);
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;

/**
 *
 * 志望大学・評価一覧の取り込みノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class InputB0401Node extends InputCsvNode {

    /**
     * コンストラクタです。
     */
    InputB0401Node() {
        super("志望大学・評価一覧");
    }

    @Override
    void input(OutputMainHelper helper) {
        helper.importB0401();
    }

}

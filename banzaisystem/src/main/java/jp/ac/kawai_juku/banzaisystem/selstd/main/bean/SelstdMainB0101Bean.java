package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

/**
 *
 * 個人成績CSVの出力情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainB0101Bean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private String grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** カナ氏名 */
    private String nameKana;

    /** 受験型 */
    private String examType;

    /** 文理コード */
    private String bunriCode;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public String getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * カナ氏名を返します。
     *
     * @return カナ氏名
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * カナ氏名をセットします。
     *
     * @param nameKana カナ氏名
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 受験型を返します。
     *
     * @return 受験型
     */
    public String getExamType() {
        return examType;
    }

    /**
     * 受験型をセットします。
     *
     * @param examType 受験型
     */
    public void setExamType(String examType) {
        this.examType = examType;
    }

    /**
     * 文理コードを返します。
     *
     * @return 文理コード
     */
    public String getBunriCode() {
        return bunriCode;
    }

    /**
     * 文理コードをセットします。
     *
     * @param bunriCode 文理コード
     */
    public void setBunriCode(String bunriCode) {
        this.bunriCode = bunriCode;
    }

}

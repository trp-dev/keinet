package jp.ac.kawai_juku.banzaisystem.framework.properties;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;

/**
 *
 * 設定ファイルの操作クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SettingProps extends BZProperties<SettingKey> {

    /** プロパティファイル操作クラス */
    private static final PropertiesIO IO = new PropertiesIO(new File(BZConstants.SETTING_FILE_NAME));

    /**
     * コンストラクタです。
     *
     * @param props {@link Properties}
     */
    private SettingProps(Properties props) {
        super(props);
    }

    /**
     * 設定ファイルを読み込みます。
     *
     * @return {@link SettingProps}
     */
    public static SettingProps load() {
        try {
            return new SettingProps(IO.load());
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E007"), e);
        }
    }

    /**
     * 設定ファイルを保存します。
     */
    public void store() {
        try {
            IO.store(getProperties());
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E007"), e);
        }
    }

}

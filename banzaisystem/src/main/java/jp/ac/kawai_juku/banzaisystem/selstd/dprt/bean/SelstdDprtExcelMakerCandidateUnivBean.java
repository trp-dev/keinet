package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の志望大学データBeanです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDprtExcelMakerCandidateUnivBean extends JudgementResultBean {

    /** 個人ID */
    private Integer individualId;

    /** 区分 */
    private String section;

    /** 表示順 */
    private Integer dispNumber;

    /** 大学名 */
    private String univName;

    /** 学部名 */
    private String facultyName;

    /** 学科名 */
    private String deptName;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 区分を返します。
     *
     * @return 区分
     */
    public String getSection() {
        return section;
    }

    /**
     * 区分をセットします。
     *
     * @param section 区分
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 表示順を返します。
     *
     * @return 表示順
     */
    public Integer getDispNumber() {
        return dispNumber;
    }

    /**
     * 表示順をセットします。
     *
     * @param dispNumber 表示順
     */
    public void setDispNumber(Integer dispNumber) {
        this.dispNumber = dispNumber;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

}

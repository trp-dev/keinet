package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * CSV出力機能ノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
abstract class OutputCsvNode extends OutputNode {

    /** CSVアイコン */
    private static final ImageIcon ICON;
    static {
        ICON = new ImageIcon(ImageUtil.readImage(OutputCsvNode.class, "icon/csv.png"));
    }

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    OutputCsvNode(Object userObject) {
        super(userObject);
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;

/**
 *
 * 取り込み機能ノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
abstract class InputNode extends FunctionNode {

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    InputNode(Object userObject) {
        super(userObject);
    }

    /**
     * 入力処理を実行します。
     *
     * @param helper {@link OutputMainHelper}
     */
    abstract void input(OutputMainHelper helper);

}

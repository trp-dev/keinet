package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.maker;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.bean.ExmntnPrntUnivDetailBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.service.ExmntnPrntService;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301StudentInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivExcelUtil;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Excel帳票メーカー(志望大学リスト)です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A05_01)
public class ExmntnPrntA0501Maker extends BaseExcelMaker<ExmntnUnivA0301StudentInBean> {

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル（生徒情報）：クラス番号 */
    private static final String TITLE_STUDENT_CLSNO = "クラス番号：";

    /** タイトル（生徒情報）：氏名 */
    private static final String TITLE_STUDENT_NAME = "　　氏名：";

    /** タイトル：得点 */
    private static final String TITLE_PNT = "得点";

    /** タイトル：本人得点 */
    private static final String TITLE_PNT_M = "本人得点";

    /** タイトル：偏差値 */
    private static final String TITLE_DEV = "偏差値";

    /** タイトル：本人偏差値 */
    private static final String TITLE_DEV_M = "本人偏差値";

    /** 最大大学数（1ページ目） */
    private static final int PAGE1_UNIVMAX = 20;

    /** 最大大学数（2ページ目以降） */
    private static final int PAGE2_SUB_ONES_UNIVMAX = 29;

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックス：クラス番号・氏名  */
    private static final String IDX_STUDENT = "A5";

    /** インデックス：模試名（上段） */
    private static final String IDX_EXAMINATION1 = "A6";

    /** インデックス：得点(含まない) */
    /** インデックス：得点 */
    private static final int IDX_PNT = 31;

    /** インデックス：得点(含む) */
    /* private static final int IDX_PNT_INC = 39; */

    /** インデックス：偏差値  */
    /* private static final int IDX_DEV = 43; */
    private static final int IDX_DEV = 35;

    /** インデックスrow：志望大学（1ページ目） */
    /* private static final int IDX_ROW_UNIV_PAGE1 = 20; */
    private static final int IDX_ROW_UNIV_PAGE1 = 19;

    /** インデックスrow：志望大学（2ページ目以降） */
    /* private static final int IDX_ROW_UNIV_PAGE2_SUB_ONES = 9; */
    private static final int IDX_ROW_UNIV_PAGE2_SUB_ONES = 8;

    /** インデックスrow：得点・偏差値項目切替行（1ページ目） */
    /* private static final int IDX_ROW_PNT_PAGE1 = 19; */
    private static final int IDX_ROW_PNT_PAGE1 = 18;

    /** インデックスrow：得点・偏差値項目切替行（2ページ目以降） */
    /* private static final int IDX_ROW_PNT_PAGE2_SUB_ONES = 8; */
    private static final int IDX_ROW_PNT_PAGE2_SUB_ONES = 7;

    /** インデックスcell：志望順位 */
    private static final int IDX_CELL_UNIV_WISH_RANK = 0;

    /** インデックスcell：志望大学リスト2セル項目 */
    private static final int IDX_CELL_UNIV_2CELL = 2;

    /** インデックスcell：志望大学リスト3セル項目 */
    private static final int IDX_CELL_UNIV_3CELL = 3;

    /** インデックスcell：志望大学リスト6セル項目 */
    private static final int IDX_CELL_UNIV_6CELL = 6;

    /** インデックスcell：区分 */
    private static final int IDX_CELL_UNIV_KUBUN = IDX_CELL_UNIV_WISH_RANK + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（所在地） */
    private static final int IDX_CELL_UNIV_LOCATION = IDX_CELL_UNIV_KUBUN + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（大学名） */
    private static final int IDX_CELL_UNIV_UNIVNAME = IDX_CELL_UNIV_LOCATION + IDX_CELL_UNIV_3CELL;

    /** インデックスcell：志望大学（学部名） */
    private static final int IDX_CELL_UNIV_FACULTYNAME = IDX_CELL_UNIV_UNIVNAME + IDX_CELL_UNIV_6CELL;

    /** インデックスcell：志望大学（学科名） */
    private static final int IDX_CELL_UNIV_DEPTNAME = IDX_CELL_UNIV_FACULTYNAME + IDX_CELL_UNIV_6CELL;

    /** インデックスcell：志望大学（共通テスト評価：ボーダーライン） */
    private static final int IDX_CELL_UNIV_CENTER_BORDER = IDX_CELL_UNIV_DEPTNAME + IDX_CELL_UNIV_6CELL;

    /** インデックスcell：志望大学（共通テスト評価：満点） */
    private static final int IDX_CELL_UNIV_CENTER_FULLPNT = IDX_CELL_UNIV_CENTER_BORDER + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（共通テスト評価：得点率） */
    private static final int IDX_CELL_UNIV_CENTER_SCORE_AVG = IDX_CELL_UNIV_CENTER_FULLPNT + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（共通テスト評価：本人得点） */
    private static final int IDX_CELL_UNIV_CENTER_SCORE_ME = IDX_CELL_UNIV_CENTER_SCORE_AVG + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（共通テスト評価：ボーダーライン：英語認定試験含む） */
    /* private static final int IDX_CELL_UNIV_CENTER_BORDER_ENG = IDX_CELL_UNIV_CENTER_SCORE_ME + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（共通テスト評価：満点：英語認定試験含む） */
    /* private static final int IDX_CELL_UNIV_CENTER_FULLPNT_ENG = IDX_CELL_UNIV_CENTER_BORDER_ENG + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（共通テスト評価：得点率：英語認定試験含む） */
    /* private static final int IDX_CELL_UNIV_CENTER_SCORE_AVG_ENG = IDX_CELL_UNIV_CENTER_FULLPNT_ENG + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（共通テスト評価：本人得点：英語認定試験含む） */
    /* private static final int IDX_CELL_UNIV_CENTER_SCORE_ME_ENG = IDX_CELL_UNIV_CENTER_SCORE_AVG_ENG + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（２次・一般評価：ボーダーランク） */
    /* private static final int IDX_CELL_UNIV_SECOND_RANK = IDX_CELL_UNIV_CENTER_SCORE_ME_ENG + IDX_CELL_UNIV_2CELL; */
    private static final int IDX_CELL_UNIV_SECOND_RANK = IDX_CELL_UNIV_CENTER_SCORE_ME + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（２次・一般評価：評価偏差値） */
    private static final int IDX_CELL_UNIV_SECOND_DEVIATION = IDX_CELL_UNIV_SECOND_RANK + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（２次・一般評価：満点） */
    /* private static final int IDX_CELL_UNIV_SECOND_FULLPNT = IDX_CELL_UNIV_SECOND_DEVIATION + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（共通テスト評価：含まない評価） */
    /** インデックスcell：志望大学（共通テスト評価） */
    /* private static final int IDX_CELL_UNIV_CENTER_RATING = IDX_CELL_UNIV_SECOND_FULLPNT + IDX_CELL_UNIV_2CELL; */
    private static final int IDX_CELL_UNIV_CENTER_RATING = IDX_CELL_UNIV_SECOND_DEVIATION + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（共通テスト評価：含む評価） */
    /* private static final int IDX_CELL_UNIV_CENTER_RATING_INC = IDX_CELL_UNIV_CENTER_RATING + IDX_CELL_UNIV_2CELL; */

    /** インデックスcell：志望大学（２次・一般評価：評価） */
    /* private static final int IDX_CELL_UNIV_SECOND_RATING = IDX_CELL_UNIV_CENTER_RATING_INC + IDX_CELL_UNIV_2CELL; */
    private static final int IDX_CELL_UNIV_SECOND_RATING = IDX_CELL_UNIV_CENTER_RATING + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：志望大学（総合：評価） */
    private static final int IDX_CELL_UNIV_TOTAL_RATING = IDX_CELL_UNIV_SECOND_RATING + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：配点（共通テスト） */
    private static final int IDX_CELL_PNT_CENTER = IDX_CELL_UNIV_TOTAL_RATING + IDX_CELL_UNIV_2CELL;

    /** インデックスcell：配点（二次） */
    private static final int IDX_CELL_PNT_SECOND = IDX_CELL_PNT_CENTER + IDX_CELL_UNIV_2CELL;

    /** サービス */
    @Resource
    private ExmntnPrntService exmntnPrntService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 生徒情報Bean */
    private ExmntnUnivStudentBean studentBean;

    /** 志望大学詳細リスト */
    private List<ExmntnPrntUnivDetailBean> univDetailList;

    /** 志望大学詳細リスト(英語認定含む) */
    /* private List<ExmntnPrntUnivDetailBean> univDetailListInclude; */

    /** 帳票ユーティリティ */
    private final ExmntnUnivExcelUtil excelUtil = new ExmntnUnivExcelUtil(this);

    /** マーク模試 */
    private ExamBean markExam;

    /** 記述模試 */
    private ExamBean writtenExam;

    /** 対象模試 */
    private ExamBean targetExam;

    @Override
    protected boolean prepareData(ExmntnUnivA0301StudentInBean inBean) {

        /* 出力のために入力beanを保存 */
        studentBean = inBean.getStudentBean();

        if (studentBean == null || inBean.getCandidateUnivList().isEmpty() || inBean.getScoreBean() == null) {
            /* 「生徒情報無し」「志望大学情報無し」「判定未実行」の何れかなら出力しない */
            return false;
        }

        /* 対象模試（マーク模試）を保存する */
        markExam = inBean.getScoreBean().getMarkExam();

        /* 対象模試（記述模試）を保存する */
        writtenExam = inBean.getScoreBean().getWrittenExam();

        if (markExam == null && writtenExam == null) {
            /* 入力情報がない */
            return false;
        } else {
            /* 対象模試を判定する */
            targetExam = ExamUtil.judgeTargetExam(markExam, writtenExam);
        }

        /* 帳票ユーティリティに成績データを設定する */
        excelUtil.setScoreBean(inBean.getScoreBean());

        /* 志望大学詳細リストを初期化する */
        univDetailList = exmntnPrntService.createUnivDetailList(inBean.getCandidateUnivList(), inBean.getScoreBean(), false);

        /* 志望大学詳細リストを初期化する(英語認定含む) */
        /* univDetailListInclude = exmntnPrntService.createUnivDetailList(inBean.getCandidateUnivList(), inBean.getScoreBean(), true); */

        return true;

    }

    @Override
    protected void initSheet() {

        /* 学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* クラス番号・氏名を出力する */
        outputStudent();

    }

    @Override
    protected void outputData() {

        /* 個人成績一覧を出力する */
        /* excelUtil.outputEngSubRecord(targetExam, markExam, writtenExam, IDX_EXAMINATION1, true); */
        excelUtil.outputSubRecord(targetExam, markExam, writtenExam, IDX_EXAMINATION1);

        /* 志望大学データを出力する */
        outputCandidateUniv();

    }

    /**
     * 志望大学データ一覧を出力するメソッドです。
     */
    private void outputCandidateUniv() {
        /* ページ数カウント */
        int cntPage = 1;

        /* ページ内大学数カウント */
        int cntUniv = 0;

        /* 全体大学数カウント */
        int cntAllUniv = 0;

        /* 最大大学数1ページ目 */
        int univMax1 = PAGE1_UNIVMAX;

        /* 最大大学数2ページ目 */
        int univMax2 = PAGE2_SUB_ONES_UNIVMAX;

        for (ExmntnPrntUnivDetailBean bean : univDetailList) {

            /* 英語認定試験含むデータ */
            /* ExmntnPrntUnivDetailBean beanInc = univDetailListInclude.get(cntAllUniv); */

            cntAllUniv++;

            /* 1ページ内の最大出力数を超えたかチェックする */
            if (getSheetCount() == 1) {
                /* 1ページ目の出力 */
                if (cntUniv >= univMax1) {
                    /* 改シートする */
                    breakSheet();
                    cntPage++;
                    cntUniv = 0;
                }
            } else {
                /* 2ページ目の出力 */
                if (cntUniv >= univMax2) {
                    /* これ以降のデータ出力しない */
                    break;
                }
            }

            /* outputCandidateUnivDetail(cntPage, cntUniv, cntAllUniv, bean, beanInc); */
            outputCandidateUnivDetail(cntPage, cntUniv, cntAllUniv, bean);

            cntUniv++;
        }
    }

    /**
     * 志望大学データを出力するメソッドです。
     *
     * @param cntPage ページ数カウント
     * @param cntUniv ページ内大学数カウント
     * @param cntAllUniv 全体大学数カウント
     * @param detail 大学詳細(英語認定含まない)
     */
    private void outputCandidateUnivDetail(int cntPage, int cntUniv, int cntAllUniv, ExmntnPrntUnivDetailBean detail) {

        /* 出力行 */
        int row = 0;

        /* 出力行 */
        int row2 = 0;

        /* 1ページ目と2ページ目で出力位置が違う */
        if (cntPage == 1) {
            row = IDX_ROW_UNIV_PAGE1;
            row2 = IDX_ROW_PNT_PAGE1;
        } else {
            row = IDX_ROW_UNIV_PAGE2_SUB_ONES;
            row2 = IDX_ROW_PNT_PAGE2_SUB_ONES;
        }

        if (ExamUtil.isCenter(targetExam)) {
            /* 得点・偏差値名称設定(リサーチ) */
            setCellValue(row2, IDX_PNT, TITLE_PNT_M);
            /* setCellValue(row2, IDX_PNT_INC, TITLE_PNT_M); */
            setCellValue(row2, IDX_DEV, TITLE_DEV_M);
        } else {
            /* 得点・偏差値名称設定(リサーチ以外) */
            setCellValue(row2, IDX_PNT, TITLE_PNT);
            /* setCellValue(row2, IDX_PNT_INC, TITLE_PNT); */
            setCellValue(row2, IDX_DEV, TITLE_DEV);
        }

        /* 志望順位を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_WISH_RANK, Integer.toString(cntAllUniv));

        /* 区分を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_KUBUN, detail.getSection());

        /* 所在地を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_LOCATION, detail.getPrefName());

        /* 大学名を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_UNIVNAME, detail.getUnivName());

        /* 学部名を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_FACULTYNAME, detail.getFacultyName());

        /* 学科・日程を出力する */
        if (StringUtils.isEmpty(detail.getDeptName())) {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_DEPTNAME, detail.isCautionNeeded() ? "【！】" : "");
        } else {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_DEPTNAME, (detail.isCautionNeeded() ? "【！】" : "") + detail.getDeptName());
        }

        /* 共通テスト評価：ボーダーラインを出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER, detail.getBorderPoint());

        /* 共通テスト評価：満点を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT, detail.getcFullPoint());

        /* 共通テスト評価：得点率 */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG, detail.getCenScoreRate());

        /* 共通テスト評価：本人得点 */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME, detail.getcScore());
        /* if (!ExamUtil.isCenter(targetExam)) { */
        /* 共通テスト評価：ボーダーラインを出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER, detail.getBorderPoint()); */

        /* 共通テスト評価：満点を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT, detail.getcFullPoint()); */

        /* 共通テスト評価：得点率 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG, detail.getCenScoreRate()); */

        /* 共通テスト評価：本人得点 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME, detail.getcScore()); */
        /* } else { */
        /* if (!StringUtil.isEmptyTrim(detailInc.getAppReqPaternCd())) { */
        /* 共通テスト評価：ボーダーラインを出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER, ""); */

        /* 共通テスト評価：満点を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT, ""); */

        /* 共通テスト評価：得点率 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG, ""); */

        /* 共通テスト評価：本人得点 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME, ""); */
        /* } else { */
        /* 共通テスト評価：ボーダーラインを出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER, detail.getBorderPoint()); */

        /* 共通テスト評価：満点を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT, detail.getcFullPoint()); */

        /* 共通テスト評価：得点率 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG, detail.getCenScoreRate()); */

        /* 共通テスト評価：本人得点 */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME, detail.getcScore()); */
        /* } */
        /* } */
        /* 共通テスト評価：ボーダーライン(認定試験含む) */
        /*
        if (!StringUtil.isEmptyTrim(detailInc.getAppReqPaternCd())) {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER_ENG, detailInc.getBorderPoint());
        */
        /* 共通テスト評価：満点(認定試験含む)を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT_ENG, detailInc.getcFullPoint()); */

        /* 共通テスト評価：得点率(認定試験含む) */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG_ENG, detailInc.getCenScoreRate()); */

        /* 共通テスト評価：本人得点(認定試験含む) */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME_ENG, detailInc.getcScore());*/
        /*
        } else {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_BORDER_ENG, "");
        */
        /* 共通テスト評価：満点(認定試験含む)を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_FULLPNT_ENG, ""); */

        /* 共通テスト評価：得点率(認定試験含む) */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_AVG_ENG, ""); */

        /* 共通テスト評価：本人得点(認定試験含む) */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_SCORE_ME_ENG, ""); */
        /* } */
        /* 二次評価：2次評価（ランク）を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_SECOND_RANK, detail.getSecondRank());

        /* 二次評価：二次偏差値を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_SECOND_DEVIATION, detail.getsScore());

        /* 二次評価：満点を出力する */
        /* setCellValue(row + cntUniv, IDX_CELL_UNIV_SECOND_FULLPNT, detail.getsFullPoint()); */

        /* 共通テスト評価(含まない) ：評価を出力する */
        /* 共通テスト評価 ：評価を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING, detail.getValuateExclude());
        /*
        if (!ExamUtil.isCenter(targetExam)) {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING, detail.getValuateExclude());
        } else {
            if (!StringUtil.isEmptyTrim(detailInc.getAppReqPaternCd())) {
                setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING, "");
            } else {
                setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING, detail.getValuateExclude());
            }
        }
        */

        /* 共通テスト評価(含む) * ：評価を出力する */
        /*
        if (!StringUtil.isEmptyTrim(detailInc.getAppReqPaternCd())) {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING_INC, detailInc.getValuateInclude());
        } else {
            setCellValue(row + cntUniv, IDX_CELL_UNIV_CENTER_RATING_INC, "");
        }
        */

        /* 二次評価：評価を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_SECOND_RATING, detail.getSecondRating());

        /* 総合：評価を出力する */
        setCellValue(row + cntUniv, IDX_CELL_UNIV_TOTAL_RATING, detail.getTotalRating());

        /* 配点：共通テストを出力する */
        setCellValue(row + cntUniv, IDX_CELL_PNT_CENTER, detail.getcAllotPntRate());

        /* 配点：二次を出力する */
        setCellValue(row + cntUniv, IDX_CELL_PNT_SECOND, detail.getsAllotPntRate());

    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年文字列 */
        String gradeStr = TITLE_GRADE + studentBean.getGrade();

        /* クラス文字列 */
        String classStr = TITLE_CLS + studentBean.getCls();

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeStr + classStr);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, TITLE_EXAM + targetExam);
    }

    /**
     * 生徒情報を出力するメソッドです。
     */
    private void outputStudent() {

        /* クラス番号文字列 */
        String clsnoStr = TITLE_STUDENT_CLSNO + studentBean.getClassNo();

        /* 氏名文字列 */
        String nameStr = TITLE_STUDENT_NAME + studentBean.getNameKana();

        /* 生徒情報を出力する */
        setCellValue(IDX_STUDENT, clsnoStr + nameStr);
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.output.main.bean.OutputMainB0401Bean;
import jp.ac.kawai_juku.banzaisystem.output.main.bean.OutputMainB0401CsvInBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 帳票出力・CSVファイル画面(志望大学・評価一覧)のCSV出力処理のDAOです。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class OutputMainB0401CsvDao extends BaseDao {

    /**
     * 志望大学・評価一覧CSV用のデータリストを取得します。
     *
     * @param inBean 画面からの入力情報
     * @param individualId 処理対象の生徒の個人ID
     * @return 結果リスト
     */
    public List<OutputMainB0401Bean> selectB0401List(OutputMainB0401CsvInBean inBean, Integer individualId) {
        /* SQLに組み込まれるパラメータを格納 */
        BeanMap param = new BeanMap();
        param.put("id", individualId);
        param.put("examYear", inBean.getExam().getExamYear());
        param.put("examCd", inBean.getExam().getExamCd());
        param.put("univDivFlag", inBean.isUnivDiv());
        param.put("univNameFlag", inBean.isUnivName());
        param.put("nationalFlag", inBean.isNational());
        param.put("centerFlag", inBean.isCenter());
        param.put("privateFlag", inBean.isPrivate());
        param.put("collegeFlag", inBean.isCollege());
        param.put("univCdCondition",
                inBean.getUnivCdList() == null ? null : DaoUtil.createIn("U.UNIVCD", inBean.getUnivCdList()));
        /* SQLを実行し志望大学/学部/学科のリストを取得 */
        return jdbcManager.selectBySqlFile(OutputMainB0401Bean.class, getSqlPath(), param).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.main.component;

import java.awt.FlowLayout;
import java.awt.Point;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 個人名＋成績テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdMainScoreTable extends SelstdMainTable {

    /** 科目名リスト */
    private List<String> subNameList;

    /**
     * コンストラクタです。
     */
    public SelstdMainScoreTable() {
        super("number", TableType.SCORE);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        /* この時点では初期化せず、科目名リストをセットしたタイミングで初期化する */
    }

    @Override
    protected JPanel createFixedColumnHeaderView() {

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

        panel.add(createHeaderButton("grade", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("class", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("number", CellAlignment.CENTER, true));
        panel.add(createHeaderButton("name", CellAlignment.LEFT, true));
        panel.add(createHeaderLabel("synthetic1", CellAlignment.CENTER, false));
        panel.add(createHeaderButton("synthetic1Score", CellAlignment.RIGHT, false));
        panel.add(createHeaderButton("synthetic1Dev", CellAlignment.RIGHT, true));

        return panel;
    }

    @Override
    protected JPanel createColumnHeaderView() {

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

        panel.add(createHeaderLabel("synthetic2", CellAlignment.CENTER, false));
        panel.add(createHeaderButton("synthetic2Dev", CellAlignment.RIGHT, true));

        for (String subName : subNameList) {
            /* CEFRまたは国語記述の場合、偏差値列を表示しない */
            if (BZConstants.SUB_CEFR.equals(subName) || BZConstants.SUB_JPW.equals(subName)) {
                panel.add(createHeaderButton(subName, CellAlignment.CENTER, true));
            } else {
                panel.add(createHeaderButton(subName, CellAlignment.RIGHT, false));
                panel.add(createHeaderButton(subName + "Dev", CellAlignment.RIGHT, true));
            }
        }

        return panel;
    }

    /**
     * 科目名リストをセットします。
     *
     * @param subNameList 科目名リスト
     */
    public void setSubNameList(List<String> subNameList) {
        this.subNameList = subNameList;
        /*
         * 列の数が変わるので初期化する。
         * 行選択イベントが起きるように、初期化前にデータリストをクリアする。
         */
        if (getViewport().getView() != null) {
            super.clearDataList();
        }
        Point p = getViewport().getViewPosition();
        super.initialize(null, null);
        getViewport().setViewPosition(p);
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;

/**
 *
 * 数値以外の入力を制限するドキュメントフィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class NumberDocumentFilter extends MaxLengthDocumentFilter {

    /** スケール（小数点以下の精度） */
    private final int scale;

    /**
     * コンストラクタです。
     *
     * @param maxLength 最大文字数
     * @param scale スケール（小数点以下の精度）
     */
    public NumberDocumentFilter(int maxLength, int scale) {
        super(maxLength);
        this.scale = scale;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr) throws BadLocationException {

        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
        sb.insert(offset, str);
        String afterStr = sb.toString();
        if (check(afterStr)) {
            super.insertString(fb, offset, str, attr);
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException {

        if (length == 0) {
            insertString(fb, offset, text, attrs);
        } else {
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
            sb.delete(offset, offset + length);
            if (text != null) {
                sb.insert(offset, text);
            }
            String afterStr = sb.toString();
            if (check(afterStr)) {
                super.replace(fb, offset, length, text, attrs);
            }
        }
    }

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {

        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
        sb.delete(offset, offset + length);
        String afterStr = sb.toString();
        if (check(afterStr)) {
            super.remove(fb, offset, length);
        } else {
            /* 数字削除後に小数点が変な位置に残った場合なので全体を削除する */
            super.remove(fb, 0, doc.getLength());
        }
    }

    /**
     * 文字列がNumberFieldの設定を満たしているかをチェックします。
     *
     * @param str 文字列
     * @return チェック結果
     */
    private boolean check(String str) {
        if (scale == 0) {
            /* 整数の場合 */
            return ValidateUtil.isPositiveInteger(str);
        } else {
            /* 実数の場合 */
            if (!ValidateUtil.isPositiveReal(str)) {
                return false;
            }
            int index = str.indexOf('.');
            if (index > -1 && index + 1 < str.length()) {
                return str.substring(index + 1).length() <= scale;
            }
            return true;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.component;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainCheckHelper;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.container.SingletonS2Container;

/**
 *
 * 個人成績画面の科目コンボボックスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnMainSubjectComboBox extends ComboBox<BZSubjectRecord> {

    /** エラーメッセージ */
    private String errorMessage;

    /** ペアの科目テキストフィールド */
    private ExmntnMainSubjectTextField pairTextField;

    @Override
    public void initialize(Field field, AbstractView view) {

        super.initialize(field, view);

        addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    ExmntnMainCheckHelper helper = SingletonS2Container.getComponent(ExmntnMainCheckHelper.class);
                    /* 第1,第2入れ替えボタンの状態を変更する */
                    helper.changeFirstSecondButton();
                    /* 初期値に戻すボタンを有効化する */
                    helper.enableReturnInitButton(getPairTextField().isMark());
                    /* 入力チェックを行う */
                    if (getPairTextField().isMark()) {
                        helper.checkMark();
                    } else {
                        helper.checkWritten();
                    }
                    /* エラーメッセージを表示する */
                    showErrorMessage(helper);
                }
            }
        });

        addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent focusevent) {
                showErrorMessage(SingletonS2Container.getComponent(ExmntnMainCheckHelper.class));
            }
        });
    }

    /**
     * エラーメッセージを表示します。
     *
     * @param helper {@link ExmntnMainCheckHelper}
     */
    private void showErrorMessage(ExmntnMainCheckHelper helper) {
        if (errorMessage != null) {
            if (getPairTextField().isMark()) {
                helper.showMarkErrorMessage(errorMessage);
            } else {
                helper.showWrittenErrorMessage(errorMessage);
            }
        }
    }

    /**
     * エラーメッセージを返します。
     *
     * @return エラーメッセージ
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * エラーメッセージをセットします。
     *
     * @param errorMessage エラーメッセージ
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        setBackground(errorMessage == null ? Color.WHITE : ExmntnMainCheckHelper.ERROR_COLOR);
    }

    /**
     * ペアの科目テキストフィールドを返します。
     *
     * @return ペアの科目テキストフィールド
     */
    public ExmntnMainSubjectTextField getPairTextField() {
        return pairTextField;
    }

    /**
     * ペアの科目テキストフィールドをセットします。
     *
     * @param pairTextField ペアの科目テキストフィールド
     */
    void setPairTextField(ExmntnMainSubjectTextField pairTextField) {
        this.pairTextField = pairTextField;
    }

}

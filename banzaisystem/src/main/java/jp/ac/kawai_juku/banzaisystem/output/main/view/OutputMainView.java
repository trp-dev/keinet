package jp.ac.kawai_juku.banzaisystem.output.main.view;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeRadio;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.SwitchableImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NumberField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.ReadOnly;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.output.main.component.FunctionNode;
import jp.ac.kawai_juku.banzaisystem.output.main.component.OutputMainUnivList;
import jp.ac.kawai_juku.banzaisystem.output.main.component.OutputTree;
import jp.ac.kawai_juku.banzaisystem.output.main.controller.OutputMainController;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.component.SelstdDprtClassTable;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.component.SelstdDprtClassTable.TableCellUpdatedAction;

/**
 *
 * 帳票出力・CSVファイル画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainView extends AbstractView {

    /** 出力機能コンポーネントのグループ番号 */
    public static final int GROUP_OUTPUT = 1;

    /** 入力機能コンポーネントのグループ番号 */
    public static final int GROUP_INPUT = 2;

    /** 志望大学・評価一覧機能コンポーネントのグループ番号 */
    public static final int GROUP_CANDIDATE_UNIV = 3;

    /** 大学区分指定コンポーネントのグループ番号 */
    public static final int GROUP_UNIV_DIV = 10;

    /** 大学名指定コンポーネントのグループ番号 */
    public static final int GROUP_UNIV_NAME = 11;

    /** 対象生徒選択ボタン */
    @Location(x = 281, y = 2)
    public ImageButton selectStudentButton;

    /** 個人成績・志望大学ボタン */
    @Location(x = 385, y = 2)
    public ImageButton exmntnButton;

    /** 帳票出力ボタン */
    @Location(x = 518, y = 2)
    public ImageLabel reportLabel;

    /** メンテナンスボタン */
    @Location(x = 632, y = 2)
    public ImageButton maintenanceButton;

    /** インフォメーションボタン */
    @Location(x = 736, y = 2)
    public ImageButton infoButton;

    /** インフォメーションラベル */
    @Location(x = 866, y = 2)
    public TextLabel informationLabel;

    /** 入試結果入力ボタン */
    @Location(x = 736, y = 2)
    @Invisible
    public ImageButton resultButton;

    /** ヘルプボタン */
    @Location(x = 854, y = 2)
    @Invisible
    public ImageButton helpButton;

    /** 終了ボタン */
    @Location(x = 928, y = 2)
    public ImageButton exitButton;

    /** タイトル */
    @Location(x = 313, y = 52)
    public SwitchableImagePanel titlePanel;

    /** 説明文ラベル */
    @Location(x = 319, y = 81)
    public TextLabel noteLabel;

    /** 出力イメージ */
    @Location(x = 321, y = 105)
    @Group(GROUP_OUTPUT)
    public SwitchableImagePanel outputImagePanel;

    /** 機能ツリー */
    @Location(x = 20, y = 48)
    @Size(width = 280, height = 646)
    public OutputTree outputTree;

    /** タイトル（出力条件設定） */
    @Location(x = 313, y = 359)
    @Group(GROUP_OUTPUT)
    public ImageLabel titleOutputLabel;

    /** ラベル：模試 */
    @Location(x = 334, y = 412)
    @Group(GROUP_OUTPUT)
    public ImageLabel examLabel;

    /** コンボボックス_模試 */
    @Location(x = 365, y = 408)
    @Size(width = 207, height = 20)
    @FireActionEvent
    @Group(GROUP_OUTPUT)
    public ComboBox<ExamBean> examComboBox;

    /** ラベル：学年 */
    @Location(x = 335, y = 446)
    @Foreground(r = 135, g = 82, b = 64)
    @Group(GROUP_OUTPUT)
    public ImageLabel targetGradeLabel;

    /** コンボボックス_学年 */
    @Location(x = 335, y = 465)
    @Size(width = 42, height = 20)
    @FireActionEvent
    @Group(GROUP_OUTPUT)
    public ComboBox<Integer> gradeComboBox;

    /** ラベル：年 */
    @Location(x = 379, y = 468)
    @Group(GROUP_OUTPUT)
    public TextLabel gradeLabel;

    /** ラベル：クラス */
    @Location(x = 406, y = 446)
    @Foreground(r = 135, g = 82, b = 64)
    @Group(GROUP_OUTPUT)
    public ImageLabel targetClassLabel;

    /** クラステーブル */
    @Location(x = 406, y = 465)
    @Size(width = 77, height = 121)
    @Group(GROUP_OUTPUT)
    public SelstdDprtClassTable classTable;

    /** 全選択ボタン */
    @Location(x = 405, y = 590)
    @Group(GROUP_OUTPUT)
    public ImageButton allChoiceButton;

    /** 全解除ボタン */
    @Location(x = 405, y = 609)
    @Group(GROUP_OUTPUT)
    public ImageButton allLiftButton;

    /** ラベル：番号 */
    @Location(x = 501, y = 446)
    @Foreground(r = 135, g = 82, b = 64)
    @Group(GROUP_OUTPUT)
    public ImageLabel classNoLabel;

    /** 席次番号（開始）テキストフィールド */
    @Location(x = 499, y = 465)
    @Size(width = 51, height = 19)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @NumberField(maxLength = 5)
    @Group(GROUP_OUTPUT)
    public TextField numberStartField;

    /** ラベル：〜 */
    @Location(x = 558, y = 468)
    @Group(GROUP_OUTPUT)
    public TextLabel rangeLabel;

    /** 席次番号（終了）テキストフィールド */
    @Location(x = 576, y = 465)
    @Size(width = 51, height = 19)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Group(GROUP_OUTPUT)
    @NumberField(maxLength = 5)
    public TextField numberEndField;

    /** 生徒一覧から選択ボタン */
    @Location(x = 499, y = 492)
    @Group(GROUP_OUTPUT)
    public ImageButton choiceStudentListButton;

    /** ラベル：判定評価 */
    @Location(x = 679, y = 411)
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public ImageLabel ratingLabel;

    /** A評価チェックボックス */
    @Location(x = 738, y = 411)
    @CodeConfig(Code.OUTPUT_RATING_A)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public CodeCheckBox ratingACheckBox;

    /** B評価チェックボックス */
    @Location(x = 738, y = 428)
    @CodeConfig(Code.OUTPUT_RATING_B)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public CodeCheckBox ratingBCheckBox;

    /** C評価チェックボックス */
    @Location(x = 738, y = 445)
    @CodeConfig(Code.OUTPUT_RATING_C)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public CodeCheckBox ratingCCheckBox;

    /** D評価チェックボックス */
    @Location(x = 738, y = 462)
    @CodeConfig(Code.OUTPUT_RATING_D)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public CodeCheckBox ratingDCheckBox;

    /** E評価チェックボックス */
    @Location(x = 738, y = 478)
    @CodeConfig(Code.OUTPUT_RATING_E)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public CodeCheckBox ratingECheckBox;

    /** ラベル：大学指定 */
    @Location(x = 679, y = 508)
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    public ImageLabel univChoiceLabel;

    /** 大学区分指定ラジオボタン */
    @Location(x = 684, y = 527)
    @CodeConfig(Code.OUTPUT_UNIV_CHOICE_DIV)
    @Selected
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    @Font(BZFont.BOLD)
    public CodeRadio univDivRadio;

    /** 大学名指定ラジオボタン */
    @Location(x = 834, y = 527)
    @CodeConfig(Code.OUTPUT_UNIV_CHOICE_NAME)
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV })
    @Font(BZFont.BOLD)
    public CodeRadio univNameRadio;

    /** 国公立大学チェックボックス */
    @Location(x = 691, y = 546)
    @CodeConfig(Code.OUTPUT_UNIV_DIV_NATIONAL)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_DIV })
    public CodeCheckBox nationalCheckBox;

    /** センター利用私大・短大チェックボックス */
    @Location(x = 691, y = 563)
    @CodeConfig(Code.OUTPUT_UNIV_DIV_CENTER)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_DIV })
    public CodeCheckBox centerCheckBox;

    /** 私立大学チェックボックス */
    @Location(x = 691, y = 580)
    @CodeConfig(Code.OUTPUT_UNIV_DIV_PRIVATE)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_DIV })
    public CodeCheckBox privateCheckBox;

    /** 短期大学・その他チェックボックス */
    @Location(x = 691, y = 597)
    @CodeConfig(Code.OUTPUT_UNIV_DIV_COLLEGE)
    @NoAction
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_DIV })
    public CodeCheckBox collegeCheckBox;

    /** 選択大学リスト */
    @Location(x = 849, y = 546)
    @Size(width = 125, height = 50)
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_NAME })
    public OutputMainUnivList univList;

    /** 検索ボタン */
    @Location(x = 935, y = 601)
    @Group({ GROUP_OUTPUT, GROUP_CANDIDATE_UNIV, GROUP_UNIV_NAME })
    @Disabled
    public ImageButton searchButton;

    /** 印刷するボタン */
    @Location(x = 875, y = 655)
    @Group(GROUP_OUTPUT)
    public ImageButton printButton;

    /** タイトル（CSV取り込み） */
    @Location(x = 313, y = 359)
    @Group(GROUP_INPUT)
    public ImageLabel titleInputLabel;

    /** 取り込みCSV選択ラベル */
    @Location(x = 337, y = 421)
    @Group(GROUP_INPUT)
    public TextLabel selectCsvLabel;

    /** 取り込みファイルフィールド  */
    @Location(x = 337, y = 444)
    @Size(width = 124, height = 19)
    @ReadOnly
    @Group(GROUP_INPUT)
    public TextField csvFileField;

    /** 取り込み注意文言 */
    @Location(x = 337, y = 471)
    @Group(GROUP_INPUT)
    @Foreground(r = 255, g = 0, b = 0)
    public TextLabel inputAttentionLabel;

    /** 参照ボタン */
    @Location(x = 469, y = 442)
    @Group(GROUP_INPUT)
    public ImageButton referenceButton;

    /** 大学コードラベル */
    @Location(x = 550, y = 448)
    @Group(GROUP_INPUT)
    public TextLabel univCodeLabel;

    /** 10桁ラジオボタン */
    @Location(x = 610, y = 448)
    @CodeConfig(Code.CSV_IMPORT_UNIV_CODE_10)
    @Selected
    @NoAction
    @Group({ GROUP_INPUT })
    public CodeRadio univCode10Radio;

    /** 5桁ラジオボタン */
    @Location(x = 670, y = 448)
    @CodeConfig(Code.CSV_IMPORT_UNIV_CODE_5)
    @NoAction
    @Group({ GROUP_INPUT })
    public CodeRadio univCode5Radio;

    /** 取込データラベル */
    @Location(x = 725, y = 448)
    @Group(GROUP_INPUT)
    public TextLabel importTypeLabel;

    /** 置換ラジオボタン */
    @Location(x = 790, y = 448)
    @CodeConfig(Code.CSV_IMPORT_TYPE_REPLACE)
    @Selected
    @NoAction
    @Group({ GROUP_INPUT })
    public CodeRadio importTypeReplaceRadio;

    /** 追加ラジオボタン */
    @Location(x = 840, y = 448)
    @CodeConfig(Code.CSV_IMPORT_TYPE_APPEND)
    @NoAction
    @Group({ GROUP_INPUT })
    public CodeRadio importTypeAppendRadio;

    /** 取り込むボタン */
    @Location(x = 875, y = 655)
    @Group(GROUP_INPUT)
    @Disabled
    public ImageButton retrieveButton;

    /** 選択中の機能ノード */
    public FunctionNode selectedNode;

    @Override
    public void initialize() {

        super.initialize();

        /* 席次番号の初期値を設定する */
        numberStartField.setText("0");
        numberEndField.setText("99999");

        /* 席次番号の値変更時に、印刷ボタンの状態変更アクションを呼び出すようにする */
        final OutputMainController controller = getComponent(OutputMainController.class);
        DocumentListener listener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                univList.clearUnivList();
                controller.changePrintButtonStatus();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                insertUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                insertUpdate(e);
            }
        };
        numberStartField.getDocument().addDocumentListener(listener);
        numberEndField.getDocument().addDocumentListener(listener);

        /* クラス選択時の処理を設定する */
        classTable.setUpdatedAction(new TableCellUpdatedAction() {
            @Override
            public void updated() {
                univList.clearUnivList();
                controller.changePrintButtonStatus();
            }
        });

        /* 志望大学・評価一覧機能コンポーネントを非表示にする */
        setVisibleByGroup(GROUP_CANDIDATE_UNIV, false);

        /* 入力機能コンポーネントを非表示にする */
        setVisibleByGroup(GROUP_INPUT, false);

        /* 模試コンボボックスの表示最大値を設定する */
        examComboBox.setMaximumRowCount(10);
    }

}

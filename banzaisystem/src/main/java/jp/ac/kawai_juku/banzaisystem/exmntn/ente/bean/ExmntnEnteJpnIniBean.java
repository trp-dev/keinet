package jp.ac.kawai_juku.banzaisystem.exmntn.ente.bean;

/**
 *
 * CEFRのBeanです。
 *
 *
 * <br />説明：国語(記)情報です。
 * <br />用途：検索結果の格納
 * <br />機能概要：国語(記)のレコード情報(生徒ID・トータル評価)
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
public class ExmntnEnteJpnIniBean {

    /** 生徒ID */
    private Integer studentId;

    /** 国語トータル評価 */
    private String total;

    /**
     * 生徒IDを返します。
     *
     * @return 生徒ID
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * 生徒IDをセットします。
     *
     * @param studentId 生徒ID
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * 国語トータル評価を返します。
     *
     * @return 国語トータル評価
     */
    public String gettotal() {
        return total;
    }

    /**
     * 国語トータル評価をセットします。
     *
     * @param total 国語トータル評価
     */
    public void settotal(String total) {
        this.total = total;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.JComponent;
import javax.swing.JLayer;

/**
 *
 * レイヤ表示をサポートするコンポーネントのインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface LayerSupport {

    /**
     * JLayerを返します。
     *
     * @return {@link JLayer}
     */
    JLayer<JComponent> getLayer();

}

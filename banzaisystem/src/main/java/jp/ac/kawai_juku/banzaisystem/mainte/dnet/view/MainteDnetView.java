package jp.ac.kawai_juku.banzaisystem.mainte.dnet.view;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.annotation.DataFolderConfig;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngFolderField;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngMoveButton;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngRefButton;

/**
 *
 * データフォルダ設定ダイアログのViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class MainteDnetView extends AbstractView {

    /** 初期値に戻すボタン */
    @Location(x = 260, y = 15)
    public ImageButton initFolderButton;

    /** 参照ボタン */
    @Location(x = 255, y = 51)
    @DataFolderConfig(DataFolder.UNIV_FOLDER)
    public MainteStngRefButton refUnivButton;

    /** 移動ボタン */
    @Location(x = 313, y = 51)
    @DataFolderConfig(DataFolder.UNIV_FOLDER)
    public MainteStngMoveButton moveUnivButton;

    /** 参照ボタン */
    @Location(x = 255, y = 80)
    @DataFolderConfig(DataFolder.RECORD_FOLDER)
    public MainteStngRefButton refRecordButton;

    /** 移動ボタン */
    @Location(x = 313, y = 80)
    @DataFolderConfig(DataFolder.RECORD_FOLDER)
    public MainteStngMoveButton moveRecordButton;

    /** 参照ボタン */
    @Location(x = 255, y = 109)
    @DataFolderConfig(DataFolder.OUTPUT_FOLDER)
    public MainteStngRefButton refOutputButton;

    /** 移動ボタン */
    @Location(x = 313, y = 109)
    @DataFolderConfig(DataFolder.OUTPUT_FOLDER)
    public MainteStngMoveButton moveOutputButton;

    /** 参照ボタン */
    @Location(x = 255, y = 138)
    @DataFolderConfig(DataFolder.LOG_FOLDER)
    public MainteStngRefButton refLogButton;

    /** 移動ボタン */
    @Location(x = 313, y = 138)
    @DataFolderConfig(DataFolder.LOG_FOLDER)
    public MainteStngMoveButton moveLogButton;

    /** 参照ボタン */
    @Location(x = 255, y = 167)
    @DataFolderConfig(DataFolder.CONFIG_FOLDER)
    public MainteStngRefButton refConfigButton;

    /** 移動ボタン */
    @Location(x = 313, y = 167)
    @DataFolderConfig(DataFolder.CONFIG_FOLDER)
    public MainteStngMoveButton moveConfigButton;

    /** 閉じるボタン */
    @Location(x = 155, y = 212)
    public ImageButton closeButton;

    /** 大学マスタ  */
    @Location(x = 130, y = 53)
    @Size(width = 120, height = 20)
    @DataFolderConfig(DataFolder.UNIV_FOLDER)
    public MainteStngFolderField univFolderField;

    /** 成績データ  */
    @Location(x = 130, y = 83)
    @Size(width = 120, height = 20)
    @DataFolderConfig(DataFolder.RECORD_FOLDER)
    public MainteStngFolderField recordFolderField;

    /** 出力ファイル  */
    @Location(x = 130, y = 112)
    @Size(width = 120, height = 20)
    @DataFolderConfig(DataFolder.OUTPUT_FOLDER)
    public MainteStngFolderField outputFolderField;

    /** ログファイル  */
    @Location(x = 130, y = 141)
    @Size(width = 120, height = 20)
    @DataFolderConfig(DataFolder.LOG_FOLDER)
    public MainteStngFolderField logFolderField;

    /** 環境設定ファイル  */
    @Location(x = 130, y = 170)
    @Size(width = 120, height = 20)
    @DataFolderConfig(DataFolder.CONFIG_FOLDER)
    public MainteStngFolderField configFolderField;

}

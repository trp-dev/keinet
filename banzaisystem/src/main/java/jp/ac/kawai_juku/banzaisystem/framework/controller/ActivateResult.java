package jp.ac.kawai_juku.banzaisystem.framework.controller;

import javax.swing.SwingUtilities;

import jp.ac.kawai_juku.banzaisystem.framework.component.SubViewManager;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * サブ画面をアクティブにするアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ActivateResult implements ExecuteResult {

    /** アクティブにするサブ画面のコントローラクラス */
    private final Class<? extends SubController> controllerClass;

    /** アクティブにするタイミングで起動するメソッド名 */
    private final String methodName;

    /**
     * コンストラクタです。
     *
     * @param controllerClass アクティブにするサブ画面のコントローラクラス
     * @param methodName アクティブにするタイミングで起動するメソッド名
     */
    ActivateResult(Class<? extends SubController> controllerClass, String methodName) {
        this.controllerClass = controllerClass;
        this.methodName = methodName;
    }

    @Override
    public void process(AbstractController controller) {
        AbstractView view = controller.getView();
        SubViewManager manager =
                (SubViewManager) SwingUtilities.getAncestorOfClass(SubViewManager.class, view.getView());
        manager.activate(controllerClass, methodName);
    }

}

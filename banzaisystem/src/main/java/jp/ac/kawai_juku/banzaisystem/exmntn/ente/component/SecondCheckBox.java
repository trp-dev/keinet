package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation.SecondSubjectConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * �ʎ����Ȗڃ`�F�b�N�{�b�N�X�ł��B
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SecondCheckBox extends SubjectCheckBox<SecondSubject> {

    /** �ʎ����Ȗ� */
    private SecondSubject subject;

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        subject = field.getAnnotation(SecondSubjectConfig.class).value();
    }

    @Override
    protected int getBoxHeight() {
        return 74;
    }

    @Override
    public SecondSubject getSubject() {
        return subject;
    }

}

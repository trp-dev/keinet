package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.view.ExmntnEnteView;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 評価チェックボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class EvaluationCheckBox extends CodeCheckBox {

    /** View */
    private ExmntnEnteView view;

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        this.view = (ExmntnEnteView) view;
    }

    @Override
    public void setEnabled(boolean b) {
        if (b && !view.centerCheckBox.isSelected() && !view.secondCheckBox.isSelected()
                && !view.syntheticCheckBox.isSelected()) {
            /* 評価範囲が未チェックなら有効化しない */
            return;
        }
        super.setEnabled(b);
    }

}

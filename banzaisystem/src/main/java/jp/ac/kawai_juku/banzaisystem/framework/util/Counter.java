package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.Map;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * キー別にカウントを行うクラスです。<br>
 * マルチスレッドでの同時カウントには対応していないので注意してください。
 *
 *
 * @param <T> カウンタのキーの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class Counter<T> {

    /** カウンタを保持するマップ */
    private final Map<T, Integer> map = CollectionsUtil.newHashMap();

    /**
     * コンストラクタです。
     */
    protected Counter() {
    }

    /**
     * 新しいカウンタを生成します。
     *
     * @param <T> カウンタのキーの型
     * @return カウンタ
     */
    public static <T> Counter<T> newInstance() {
        return new Counter<T>();
    }

    /**
     * 現在のカウンタに1を加算して結果を返します。
     *
     * @param key キー
     * @return 加算結果
     */
    public int add(T key) {
        return add(key, 1);
    }

    /**
     * 現在のカウンタに1を減算して結果を返します。
     *
     * @param key キー
     * @return 加算結果
     */
    public int subtract(T key) {
        return add(key, -1);
    }

    /**
     * 現在のカウンタに指定した数を加算して結果を返します。
     *
     * @param key キー
     * @param num 加算する数
     * @return 加算結果
     */
    private int add(T key, int num) {
        Integer value = map.get(key);
        if (value == null) {
            map.put(key, Integer.valueOf(num));
            return num;
        } else {
            Integer newValue = value.intValue() + num;
            map.put(key, newValue);
            return newValue.intValue();
        }
    }

    /**
     * 現在のカウンタ値を返します。
     *
     * @param key キー
     * @return 現在のカウンタ値
     */
    public int getCount(T key) {
        Integer value = map.get(key);
        return value == null ? 0 : value.intValue();
    }

}

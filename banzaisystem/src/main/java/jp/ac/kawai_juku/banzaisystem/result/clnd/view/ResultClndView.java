package jp.ac.kawai_juku.banzaisystem.result.clnd.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.result.clnd.component.ResultClndCalendarTable;
import jp.ac.kawai_juku.banzaisystem.result.univ.component.ResultUnivExamineeTable;

/**
 *
 * 入試結果入力-大学指定(カレンダー)画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultClndView extends SubView {

    /** 月ラベル */
    @Location(x = 25, y = 16)
    @Size(width = 69, height = 21)
    @Font(BZFont.CALENDAR_MONTH)
    @HorizontalAlignment(SwingConstants.CENTER)
    public TextLabel monthLabel;

    /** 前月ボタン */
    @Location(x = 109, y = 18)
    public ImageButton beforeMonthButton;

    /** 今月ボタン */
    @Location(x = 152, y = 18)
    public ImageButton currentMonthButton;

    /** 次月ボタン */
    @Location(x = 193, y = 18)
    public ImageButton nextMonthButton;

    /** コンボボックス_学年 */
    @Location(x = 451, y = 16)
    @Size(width = 42, height = 20)
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** コンボボックス_クラス */
    @Location(x = 549, y = 16)
    @Size(width = 55, height = 20)
    @FireActionEvent
    public ComboBox<String> classComboBox;

    /** 説明文 */
    @Location(x = 745, y = 14)
    public TextLabel noteLabel;

    /** カレンダー */
    @Location(x = 13, y = 59)
    @Size(width = 987, height = 270)
    public ResultClndCalendarTable calendarTable;

    /** 受験者一覧テーブル */
    @Location(x = 14, y = 361)
    @Size(width = 982, height = 231)
    public ResultUnivExamineeTable examineeTable;

    /** 保存ボタン */
    @Location(x = 758, y = 602)
    public ImageButton saveButton;

    /** CSV出力ボタン */
    @Location(x = 875, y = 603)
    public ImageButton outputCsvButton;

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;

import jp.ac.kawai_juku.banzaisystem.exmntn.input.controller.ExmntnInputController;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainCheckHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainSubjectHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainValidateHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainCandidateUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainExamPlanUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.controller.ExmntnRsltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.dto.ExmntnSubsDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.helper.ExmntnSubsHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.controller.ExmntnUnivController;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.MaxLengthDocumentFilter;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.MenuController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.dao.ScoreBeanDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.controller.SelstdDstdController;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainController extends MenuController {

    /** 空の模試コンボボックスアイテム */
    private static final ComboBoxItem<ExamBean> BLANK_EXAM = new ComboBoxItem<ExamBean>(StringUtils.EMPTY, null);

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView view;

    /** 対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    /** 成績ヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /** 検証ヘルパー */
    @Resource(name = "exmntnMainValidateHelper")
    private ExmntnMainValidateHelper validateHelper;

    /** 入力チェックヘルパー */
    @Resource(name = "exmntnMainCheckHelper")
    private ExmntnMainCheckHelper checkHelper;

    /** 科目ヘルパー */
    @Resource(name = "exmntnMainSubjectHelper")
    private ExmntnMainSubjectHelper subjectHelper;

    /** 志望大学ヘルパー */
    @Resource(name = "exmntnMainUnivHelper")
    private ExmntnMainUnivHelper univHelper;

    /** 大学検索画面のヘルパー */
    @Resource
    private ExmntnSubsHelper exmntnSubsHelper;

    /** Service */
    @Resource(name = "exmntnMainService")
    private ExmntnMainService service;

    /** 志望大学サービス */
    @Resource(name = "exmntnMainCandidateUnivService")
    private ExmntnMainCandidateUnivService univService;

    /** 受験予定大学サービス */
    @Resource(name = "exmntnMainExamPlanUnivService")
    private ExmntnMainExamPlanUnivService planService;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** 大学検索画面のDTO */
    @Resource
    private ExmntnSubsDto exmntnSubsDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** システム情報DTO */
    @Resource
    private ScoreBeanDao scoreBeanDao;

    /** 選択中の生徒リスト */
    private List<ExmntnMainKojinBean> studentList;

    /** 選択中の生徒 */
    private ExmntnMainKojinBean targetStudent;

    /** 初期表示フラグ */
    private boolean indexFlg;

    /**
     * 初期表示アクションです。
     *
     * @return 個人成績画面
     */
    @Execute
    public ExecuteResult index() {

        /* CEFR・国語記述のTab移動を無効化 */
        /*
        view.cefr1Field.setEnabled(false);
        view.cefr2Field.setEnabled(false);
        view.cefrLvl1Field.setEnabled(false);
        view.cefrLvl2Field.setEnabled(false);
        view.cefr1Field.setDisabledTextColor(Color.BLACK);
        view.cefr2Field.setDisabledTextColor(Color.BLACK);
        view.cefrLvl1Field.setDisabledTextColor(Color.BLACK);
        view.cefrLvl2Field.setDisabledTextColor(Color.BLACK);
        view.jpnWritingField.setEnabled(false);
        view.jpnWritingField.setDisabledTextColor(Color.BLACK);
        */

        if (systemDto.isTeacher()) {
            /* 初期表示フラグをONにしておく */
            indexFlg = true;
            /* コンポーネントの状態を初期化する */
            initComponents(true);
            /* 模試コンボボックスを初期化する */
            initTeacherExamComboBox();
            /* 成績変更フラグを初期化する */
            validateHelper.initScoreChangedStatus();
            /* 選択中の生徒リストを初期化する */
            initStudentList();
            /* 選択中の生徒を初期化する */
            initTargetStudent();
            /* 学年コンボボックスを初期化する */
            initGradeComboBox();
            view.infoButton.setVisible(true);
            view.informationLabel.setText(getMessage("label.exmntn.main.information"));
        } else {
            /* コンポーネントの状態を初期化する */
            initComponents(false);
            /* 模試コンボボックスを初期化する */
            initStudentExamComboBox();
            /* 検索条件を初期化する */
            initSearchCondition();
            /* 第1,第2入れ替えボタン */
            checkHelper.changeFirstSecondButton();
            /* 志望大学詳細タブをアクティブにする */
            view.tabbedPane.activate(ExmntnUnivController.class);
            // 生徒モードの場合、「インフォメーション」ボタンを非表示
            view.infoButton.setVisible(false);
            view.informationLabel.setText("");
        }

        return VIEW_RESULT;
    }

    /**
     * コンポーネントの表示状態を利用モードに合わせて初期化します。
     *
     * @param isTeacher 先生用モードフラグ
     */
    private void initComponents(boolean isTeacher) {
        view.nameField.setEditable(!isTeacher);
        view.nameField.setLocation(isTeacher ? 373 : 412, view.nameField.getY());
        view.numberLabel.setVisible(!isTeacher);
        view.gradeField.setVisible(!isTeacher);
        view.classField.setVisible(!isTeacher);
        view.numberField.setVisible(!isTeacher);
        view.selectStudentButton.setVisible(isTeacher);
        view.reportButton.setVisible(isTeacher);
        view.exmntnLabel.setVisible(isTeacher);
        view.maintenanceButton.setVisible(isTeacher);
        view.gradeComboBox.setVisible(isTeacher);
        view.classComboBox.setVisible(isTeacher);
        view.numberComboBox.setVisible(isTeacher);
        view.studentListButton.setVisible(isTeacher);
        view.nextStudentButton.setVisible(isTeacher);
        view.beforeStudentButton.setVisible(isTeacher);
        view.returnInitDataPinkButton.setVisible(isTeacher);
        view.saveRecordButton.setVisible(isTeacher);
        if (isTeacher) {
            ((AbstractDocument) view.nameField.getDocument()).setDocumentFilter(null);
        } else {
            ((AbstractDocument) view.nameField.getDocument()).setDocumentFilter(new MaxLengthDocumentFilter(14));
        }
    }

    /**
     * 選択中の生徒リストを初期化します。
     */
    private void initStudentList() {
        studentList = service.findKojinList(systemDto.getSelectedIndividualIdList());
    }

    /**
     * 選択中の生徒を初期化します。
     */
    private void initTargetStudent() {
        for (ExmntnMainKojinBean bean : studentList) {
            if (bean.getIndividualId().equals(systemDto.getTargetIndividualId())) {
                targetStudent = bean;
                return;
            }
        }
        targetStudent = null;
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        if (targetStudent == null) {
            view.gradeComboBox.setItemList(Collections.<ComboBoxItem<Integer>> emptyList());
        } else {
            List<ComboBoxItem<Integer>> gradeList = new ArrayList<>();
            for (ExmntnMainKojinBean bean : studentList) {
                if (gradeList.indexOf(new ComboBoxItem<>(bean.getGrade())) < 0) {
                    gradeList.add(new ComboBoxItem<>(bean.getGrade()));
                }
            }
            /* 選択中の生徒の学年を初期値とする */
            view.gradeComboBox.setItemList(gradeList, new ComboBoxItem<>(targetStudent.getGrade()));
        }
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                if (validateHelper.checkBeforeChangeCombo(view.gradeComboBox)) {
                    initClassComboBox();
                } else {
                    indexFlg = false;
                }
            }
        };

        if (!indexFlg) {
            /* フォーカスが確認ダイアログに移るよう、別スレッドで処理する */
            SwingUtilities.invokeLater(doRun);
            /* 初期表示フラグをONにしておく */
            indexFlg = true;
        } else {
            /* 初回アクセスの場合は同じスレッドで処理する */
            doRun.run();
        }

        return null;
    }

    /**
     * クラスコンボボックスを初期化します。
     */
    private void initClassComboBox() {
        if (targetStudent == null) {
            view.classComboBox.setItemList(Collections.<ComboBoxItem<String>> emptyList());
        } else {
            Integer grade = view.gradeComboBox.getSelectedValue();
            List<ComboBoxItem<String>> classList = new ArrayList<>();
            for (ExmntnMainKojinBean bean : studentList) {
                if (ObjectUtils.equals(bean.getGrade(), grade)) {
                    ComboBoxItem<String> item = new ComboBoxItem<>(bean.getCls());
                    if (classList.indexOf(item) < 0) {
                        classList.add(item);
                    }
                }
            }
            /* 選択中の生徒のクラスを初期値とする */
            view.classComboBox.setItemList(classList, new ComboBoxItem<String>(targetStudent.getCls()));
        }
    }

    /**
     * クラスコンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult classComboBoxAction() {

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                if (validateHelper.checkBeforeChangeCombo(view.classComboBox)) {
                    initNumberComboBox();
                } else {
                    indexFlg = false;
                }
            }
        };

        if (!indexFlg) {
            /* フォーカスが確認ダイアログに移るよう、別スレッドで処理する */
            SwingUtilities.invokeLater(doRun);
            /* 初期表示フラグをONにしておく */
            indexFlg = true;
        } else {
            /* 初回アクセスの場合は同じスレッドで処理する */
            doRun.run();
        }

        return null;
    }

    /**
     * クラス番号コンボボックスを初期化します。
     */
    @SuppressWarnings("unchecked")
    private void initNumberComboBox() {
        if (targetStudent == null) {
            view.numberComboBox.setItemList(Collections.<ComboBoxItem<ExmntnMainKojinBean>> emptyList());
        } else {
            Integer grade = view.gradeComboBox.getSelectedValue();
            String cls = view.classComboBox.getSelectedValue();
            ComboBoxItem<ExmntnMainKojinBean> selectedItem1 = null;
            ComboBoxItem<ExmntnMainKojinBean> selectedItem2 = null;
            List<ComboBoxItem<ExmntnMainKojinBean>> classNumList = CollectionsUtil.newArrayList();
            for (ExmntnMainKojinBean bean : studentList) {
                if (ObjectUtils.equals(bean.getGrade(), grade) && ObjectUtils.equals(bean.getCls(), cls)) {
                    classNumList.add(new ComboBoxItem<ExmntnMainKojinBean>(bean.getClassNo(), bean));
                    if (bean == targetStudent) {
                        /* 初期値は選択中の生徒を優先する */
                        selectedItem1 = new ComboBoxItem<>(bean);
                    }
                    if (selectedItem2 == null && ObjectUtils.equals(bean.getClassNo(), targetStudent.getClassNo())) {
                        /* 選択中の生徒が存在しない場合は、同じクラス番号の生徒を初期ととする */
                        selectedItem2 = new ComboBoxItem<>(bean);
                    }
                }
            }
            view.numberComboBox.setItemList(classNumList, ObjectUtils.firstNonNull(selectedItem1, selectedItem2));
        }
    }

    /**
     * クラス番号コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult numberComboBoxAction() {

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                if (validateHelper.checkBeforeChangeCombo(view.numberComboBox)) {

                    ExmntnMainKojinBean bean = view.numberComboBox.getSelectedValue();

                    /* 氏名をセットする */
                    view.nameField.setText(bean == null ? null : bean.getNameKana());

                    /* 前の生徒、次の生徒ボタンの状態を変更する */
                    changeBeforeNextButtonStatus(bean);

                    /* 選択中の生徒の個人IDを保存する */
                    systemDto.setTargetIndividualId(bean == null ? null : bean.getIndividualId());

                    /* 選択中の生徒を初期化する */
                    targetStudent = bean;

                    /* マーク模試の成績を初期化する */
                    scoreHelper.initMarkScore(getKojinId());

                    /* 記述模試の成績を初期化する */
                    scoreHelper.initWrittenScore(getKojinId());

                    /* 成績変更フラグを初期化する */
                    validateHelper.initScoreChangedStatus();

                    /* 志望大学リストエリアを初期化する */
                    univHelper.initCandidateUniv(scoreHelper.createDispScore(), false);

                    /* 検索条件を初期化する */
                    initSearchCondition();

                    /* 志望大学詳細タブをアクティブにする */
                    if (!view.tabbedPane.isActive(ExmntnUnivController.class)) {
                        view.tabbedPane.activate(ExmntnUnivController.class);
                    }
                }
            }
        };

        if (!indexFlg) {
            /* フォーカスが確認ダイアログに移るよう、別スレッドで処理する */
            SwingUtilities.invokeLater(doRun);
        } else {
            /* 初回アクセスの場合は同じスレッドで処理する */
            doRun.run();
        }

        /* 初期表示フラグをOFFにしておく */
        indexFlg = false;

        return null;
    }

    /**
     * 再表示アクションです。
     *
     * @return 個人成績画面
     */
    @Execute
    public ExecuteResult redisplay() {
        if (systemDto.getTargetIndividualId() == null || view.numberComboBox.getSelectedValue() == null) {
            return index();
        } else {
            view.tabbedPane.activate();
            return VIEW_RESULT;
        }
    }

    /**
     * 検索条件を初期化します。
     */
    private void initSearchCondition() {
        view.tabbedPane.setTabEnabled(ExmntnRsltController.class, false);
        exmntnSubsHelper.initSearchCondition();
    }

    /**
     * （先生用）模試コンボボックスを初期化します。
     */
    private void initTeacherExamComboBox() {
        /* コンボリスト位置 */
        int cmbListIchi = 0;
        ExamBean examdoc = null;
        List<BZSubjectRecord> recordList = null;
        List<BZSubjectRecord> recordListDoc = null;

        ExamBean exam = selstdMainView.examComboBox.getSelectedValue();
        if (exam == null) {
            view.exmntnMarkComboBox.removeAllItems();
            view.exmntnWritingComboBox.removeAllItems();
            return;
        }

        List<ComboBoxItem<ExamBean>> markList;
        List<ComboBoxItem<ExamBean>> writtenList;

        /* 対象模試が受験済かレコード取得 */
        recordList = scoreBeanDao.selectSubRecordList(getKojinId(), exam);

        if (ExamUtil.isMark(exam)) {
            /* 対象模試がマーク模試の場合 */
            markList = CollectionsUtil.newArrayList(1);
            if (recordList.size() == 0) {
                markList.add(BLANK_EXAM);
            }
            markList.add(new ComboBoxItem<ExamBean>(exam.getExamNameAbbr(), exam));
            writtenList = createDockingExamItemList(exam);
        } else {
            /* 対象模試が記述模試の場合 */
            markList = createDockingExamItemList(exam);
            writtenList = CollectionsUtil.newArrayList(1);
            if (recordList.size() == 0) {
                writtenList.add(BLANK_EXAM);
            }
            writtenList.add(new ComboBoxItem<ExamBean>(exam.getExamNameAbbr(), exam));
        }

        view.exmntnMarkComboBox.setItemList(markList, false);
        view.exmntnWritingComboBox.setItemList(writtenList, false);

        /* 対象生徒がドッキング対象模試を受験済かレコード取得 */
        if (ExamUtil.isMark(exam)) {
            for (int idx = 1; idx < writtenList.size(); idx++) {
                examdoc = writtenList.get(idx).getValue();
                cmbListIchi++;
                recordListDoc = scoreBeanDao.selectSubRecordList(getKojinId(), examdoc);
                if (recordListDoc.size() > 0) {
                    break;
                }
            }
        } else {
            for (int idx = 1; idx < markList.size(); idx++) {
                examdoc = markList.get(idx).getValue();
                cmbListIchi++;
                recordListDoc = scoreBeanDao.selectSubRecordList(getKojinId(), examdoc);
                if (recordListDoc.size() > 0) {
                    break;
                }
            }
        }
        /* ドッキング対象模試を初期設定する */
        if (ExamUtil.isMark(exam)) {
            /* 対象模試がマーク模試の場合 */
            if (view.exmntnWritingComboBox.getItemCount() > 1) {
                if (recordListDoc.size() > 0) {
                    view.exmntnWritingComboBox.setSelectedIndexSilent(cmbListIchi);
                } else {
                    view.exmntnWritingComboBox.setSelectedIndexSilent(0);
                }
            } else {
                view.exmntnWritingComboBox.setSelectedIndexSilent(0);
            }
            view.exmntnMarkComboBox.setSelectedIndexSilent(0);
        } else {
            /* 対象模試が記述模試の場合 */
            if (view.exmntnMarkComboBox.getItemCount() > 1) {
                if (recordListDoc.size() > 0) {
                    view.exmntnMarkComboBox.setSelectedIndexSilent(cmbListIchi);
                } else {
                    view.exmntnMarkComboBox.setSelectedIndexSilent(0);
                }
                /* view.inputButton.setEnabled(true); */
            } else {
                view.exmntnMarkComboBox.setSelectedIndexSilent(0);
                /* view.inputButton.setEnabled(false); */
            }
            view.exmntnWritingComboBox.setSelectedIndexSilent(0);
        }

        /* 科目リストボックスを初期化する */
        scoreHelper.initMarkSubject();
        scoreHelper.initWrittenSubject();
    }

    /**
     * （生徒用）模試コンボボックスを初期化します。
     */
    private void initStudentExamComboBox() {
        view.exmntnMarkComboBox.setItemList(createExamItemList(true));
        view.exmntnWritingComboBox.setItemList(createExamItemList(false));
    }

    /**
     * 指定した模試種類の模試アイテムリストを生成します。
     *
     * @param markFlg マーク模試を取得するならtrue
     * @return 模試アイテムリスト
     */
    private List<ComboBoxItem<ExamBean>> createExamItemList(boolean markFlg) {
        List<ComboBoxItem<ExamBean>> list = CollectionsUtil.newArrayList(6);
        list.add(BLANK_EXAM);
        for (ExamBean exam : systemDto.getExamList()) {
            boolean isMark = ExamUtil.isMark(exam);
            if ((markFlg && isMark) || (!markFlg && !isMark)) {
                list.add(new ComboBoxItem<ExamBean>(exam.getExamNameAbbr(), exam));
            }
        }
        return list;
    }

    /**
     * 対象模試とドッキング可能な模試アイテムリストを生成します。
     *
     * @param targetExam 対象模試
     * @return 模試アイテムリスト
     */
    private List<ComboBoxItem<ExamBean>> createDockingExamItemList(ExamBean targetExam) {
        List<ComboBoxItem<ExamBean>> list = CollectionsUtil.newArrayList(1);
        list.add(BLANK_EXAM);
        for (ExamBean exam : systemDto.getExamList()) {
            if (exam.getExamCd().equals(targetExam.getDockingExamCd()) || targetExam.getExamCd().equals(exam.getDockingExamCd())) {
                list.add(new ComboBoxItem<ExamBean>(exam.getExamNameAbbr(), exam));
            }
        }
        return list;
    }

    /**
     * 対象生徒選択画面で生徒選択後に呼び出されるアクションです。
     *
     * @param id 個人ID
     */
    public void dispStudent(Integer id) {

        /* 初期表示フラグをONにしておく */
        indexFlg = true;

        /* 選択中の生徒の個人IDを保存する */
        systemDto.setTargetIndividualId(id);

        /* コンボの初期値を再設定のためコンボ再作成 */
        initTeacherExamComboBox();

        /* 選択中の生徒を初期化する */
        initTargetStudent();

        /* 学年コンボボックスを初期化する */
        initGradeComboBox();
    }

    /**
     * 第1,第2入れ替えボタン(理科)押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult changeFirstSecondOfSienceButtonAction() {
        scoreHelper.changeFirstSecondButtonAction(view.science1Field, view.science2Field);
        return null;
    }

    /**
     * 第1,第2入れ替えボタン(社会)押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult changeFirstSecondOfSocialButtonAction() {
        scoreHelper.changeFirstSecondButtonAction(view.society1Field, view.society2Field);
        return null;
    }

    /**
     * 生徒一覧ボタン押下アクションです。
     *
     * @return 生徒一覧ダイアログ画面
     */
    @Execute
    public ExecuteResult studentListButtonAction() {
        if (validateHelper.checkMovable()) {
            /* 初期表示フラグをONにしておく */
            indexFlg = true;
            /* クラス番号変更イベントを起こして初期表示に戻す */
            numberComboBoxAction();
            /* 生徒一覧ダイアログを開く */
            return forward(SelstdDstdController.class);
        } else {
            return null;
        }
    }

    /**
     * 前の生徒ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult beforeStudentButtonAction() {
        if (validateHelper.checkMovable()) {
            int index = studentList.indexOf(view.numberComboBox.getSelectedValue()) - 1;
            dispStudent(studentList.get(index).getIndividualId());
        }
        return null;
    }

    /**
     * 次の生徒ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult nextStudentButtonAction() {
        if (validateHelper.checkMovable()) {
            int index = studentList.indexOf(view.numberComboBox.getSelectedValue()) + 1;
            dispStudent(studentList.get(index).getIndividualId());
        }
        return null;
    }

    /**
     * 再判定ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult reJudgeButtonAction() {

        /* 成績データを生成する */
        ScoreBean score = scoreHelper.createDispScore();

        /* 志望大学リストエリアを初期化する */
        univHelper.initCandidateUniv(score, true);

        /* 再検索する */
        return searchAgainAction(score);
    }

    /**
     * 再検索アクションです。
     *
     * @param scoreBean 成績データ
     * @return {@link ExecuteResult}
     */
    private ExecuteResult searchAgainAction(final ScoreBean scoreBean) {
        if (view.tabbedPane.isTabEnabled(ExmntnRsltController.class)) {
            /* 検索結果をクリアする */
            exmntnSubsHelper.clearSearchResult();
            if (view.tabbedPane.isActive(ExmntnRsltController.class)) {
                /* 検索結果タブがアクティブなら再検索する */
                return new TaskResult() {
                    @Override
                    protected ExecuteResult doTask() {
                        exmntnSubsHelper.search(scoreBean);
                        return null;
                    }

                    @Override
                    protected void done() {
                        view.tabbedPane.activate(ExmntnRsltController.class);
                    }
                };
            } else {
                /*
                 * 検索結果タブが非アクティブなら検索条件の成績データを上書きしておく。
                 * ここで上書きした成績は、検索結果タブがアクティブになったタイミングで実行される
                 * 再検索によって利用される。
                 */
                if (exmntnSubsDto.getSearchBean() != null) {
                    exmntnSubsDto.getSearchBean().setScoreBean(scoreBean);
                }
                return null;
            }
        } else {
            /* 検索結果タブが無効なら再検索は不要 */
            return null;
        }
    }

    /**
     * 初期値に戻すボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult returnInitDataPinkButtonAction() {

        /* 成績変更フラグを初期化する */
        validateHelper.initScoreChangedStatus();

        /* 初期表示フラグをONにしておく */
        indexFlg = true;

        /* クラス番号変更イベントを起こして初期表示に戻す */
        numberComboBoxAction();

        return null;
    }

    /**
     * 成績保存ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult saveRecordButtonAction() {

        /* 確認ダイアログを表示する */
        if (!validateHelper.showSaveConfirmDialog()) {
            return null;
        }

        /* 画面の成績データを取得する */
        ScoreBean score = scoreHelper.createDispScore();

        /* 成績を保存する */
        service.saveRecord(getKojinId(), score, dto);

        /* 成績変更フラグを初期化する */
        validateHelper.initScoreChangedStatus();

        /* 志望大学リストエリアを初期化する */
        univHelper.initCandidateUniv(score, true);

        /* 再検索する */
        return searchAgainAction(score);
    }

    /**
     * 検索結果画面で志望大学追加後の、志望大学リスト再表示アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult dispCandidateUnivListFromSerachAction() {
        univHelper.initCandidateUniv(dto.getJudgeScore(), true);
        return null;
    }

    /**
     * 記述模試：リスニングチェックボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult listeningCheckBoxAction() {
        ExamBean exam = scoreHelper.getWrittenExam();
        if (exam != null) {
            view.english2Field.setSubjectBean(subjectHelper.getWrtEng(exam, view.listeningCheckBox.isSelected()));
        }
        return null;
    }

    /**
     * 前の生徒、次の生徒ボタンの状態を変更します。
     *
     * @param bean 選択中生徒
     */
    private void changeBeforeNextButtonStatus(ExmntnMainKojinBean bean) {
        int index = studentList.indexOf(bean);
        view.beforeStudentButton.setEnabled(index > 0);
        view.nextStudentButton.setEnabled(index + 1 < studentList.size());
    }

    /**
     * 「マーク模試」コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exmntnMarkComboBoxAction() {

        /* フォーカスが確認ダイアログに移るよう、別スレッドで処理する */
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                /* 変更可能かチェックする */
                if (!validateHelper.checkBeforeChangeMark()) {
                    return;
                }

                /* マーク模試の科目と成績を初期化する */
                scoreHelper.initMarkSubject();
                scoreHelper.initMarkScore(getKojinId());

                if (systemDto.isTeacher()) {
                    /* 記述模試の科目と成績は先生用のみ初期化する */
                    scoreHelper.initWrittenSubject();
                    scoreHelper.initWrittenScore(getKojinId());
                    /* 成績変更フラグを初期化する */
                    validateHelper.initScoreChangedStatus();
                } else {
                    /* 記述模試コンボボックスを初期化する */
                    ExamBean markExam = scoreHelper.getMarkExam();
                    if (markExam != null) {
                        view.exmntnWritingComboBox.setItemList(createDockingExamItemList(markExam));
                        if (scoreHelper.getWrittenExam() == null) {
                            /* 未選択に変わったら、科目と成績をクリアする */
                            scoreHelper.initWrittenSubject();
                            scoreHelper.initWrittenScore(null);
                        }
                    } else {
                        initStudentExamComboBox();
                    }
                }

                /* 志望大学リストエリアを初期化する */
                univHelper.initCandidateUniv(scoreHelper.createDispScore(), false);

                /* 検索条件を初期化する */
                initSearchCondition();

                /* 志望大学詳細タブをアクティブにする */
                if (!view.tabbedPane.isActive(ExmntnUnivController.class)) {
                    view.tabbedPane.activate(ExmntnUnivController.class);
                }
            }
        });

        return null;
    }

    /**
     * 「記述模試」コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exmntnWritingComboBoxAction() {

        /* フォーカスが確認ダイアログに移るよう、別スレッドで処理する */
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                /* 変更可能かチェックする */
                if (!validateHelper.checkBeforeChangeWritten()) {
                    return;
                }

                /* 記述模試の科目と成績を初期化する */
                scoreHelper.initWrittenSubject();
                scoreHelper.initWrittenScore(getKojinId());

                if (systemDto.isTeacher()) {
                    /* マーク模試の科目と成績は先生用のみ初期化する */
                    scoreHelper.initMarkSubject();
                    scoreHelper.initMarkScore(getKojinId());
                    /* 成績変更フラグを初期化する */
                    validateHelper.initScoreChangedStatus();
                } else {
                    /* マーク模試コンボボックスを初期化する */
                    ExamBean writtenExam = scoreHelper.getWrittenExam();
                    if (writtenExam != null) {
                        view.exmntnMarkComboBox.setItemList(createDockingExamItemList(writtenExam));
                        if (scoreHelper.getMarkExam() == null) {
                            /* 未選択に変わったら、科目と成績をクリアする */
                            scoreHelper.initMarkSubject();
                            scoreHelper.initMarkScore(null);
                        }
                    } else {
                        initStudentExamComboBox();
                    }
                }

                /* 志望大学リストエリアを初期化する */
                univHelper.initCandidateUniv(scoreHelper.createDispScore(), false);

                /* 検索条件を初期化する */
                initSearchCondition();

                /* 志望大学詳細タブをアクティブにする */
                if (!view.tabbedPane.isActive(ExmntnUnivController.class)) {
                    view.tabbedPane.activate(ExmntnUnivController.class);
                }
            }
        });

        return null;
    }

    /**
     * 画面選択中の個人IDを取得する。
     *
     * @return 個人ID
     */
    private Integer getKojinId() {
        return systemDto.getTargetIndividualId();
    }

    /**
     * ↑ボタン押下アクションです。
     *
     * @return  なし
     */
    @Execute
    public ExecuteResult upButtonAction() {
        changeCandidateUnivRank(true);
        return null;
    }

    /**
     * ↓ボタン押下アクションです。
     *
     * @return  なし
     */
    @Execute
    public ExecuteResult downButtonAction() {
        changeCandidateUnivRank(false);
        return null;
    }

    /**
     * 志望順位を変更します。
     *
     * @param isUp 上に移動するならtrue
     */
    private void changeCandidateUnivRank(boolean isUp) {

        ExmntnMainCandidateUnivBean bean = view.candidateUnivTable.getSelectedUnivList().get(0);
        if (systemDto.isTeacher()) {
            /* 先生用 */
            univService.changeTeacherRank(getKojinId(), dto.getJudgeScore(), bean, isUp);
        } else {
            /* 生徒用 */
            univService.changeStudentRank(dto.getStudentCandidateUnivList(), bean, isUp);
        }

        /* テーブル内の順位を変更する */
        view.candidateUnivTable.changeRank(isUp);

        /* 志望順位コンボボックスを初期化する */
        univHelper.initCandidateRankComboBox(true);
    }

    /**
     * 削除ボタン押下アクションです。
     *
     * @return  なし
     */
    @Execute
    public ExecuteResult deleteButtonAction() {

        if (systemDto.isTeacher()) {
            /* 先生用 */
            univService.deleteTeacherCandidateUniv(getKojinId(), scoreHelper.getTargetExam(), view.candidateUnivTable.getSelectedUnivList());

        } else {
            /* 生徒用 */
            univService.deleteStudentCandidateUniv(view.candidateUnivTable.getSelectedUnivList());
        }

        /* 志望大学リストを初期化する */
        univHelper.initCandidateUniv(dto.getJudgeScore(), true);

        return null;
    }

    /**
     * 詳細判定ボタン押下アクションです。
     *
     * @return 志望大学詳細画面
     */
    @Execute
    public ExecuteResult detailJudgeButtonAction() {
        univHelper.showUnivDetail(view.candidateUnivTable.getSelectedUnivList().get(0));
        return null;
    }

    /**
     * スケジュール追加ボタン押下アクションです。
     *
     * @return  なし
     */
    @Execute
    public ExecuteResult addScheduleButtonAction() {

        boolean conflictFlg;
        if (systemDto.isTeacher()) {
            /* 先生用 */
            conflictFlg = planService.addTeacherExamPlanUniv(getKojinId(), view.candidateUnivTable.getSelectedUnivList());
        } else {
            /* 生徒用 */
            conflictFlg = planService.addStudentExamPlanUniv(view.candidateUnivTable.getSelectedUnivList());
        }

        /* 入試日が重複していたら警告ダイアログを表示する */
        if (conflictFlg) {
            JOptionPane.showMessageDialog(getActiveWindow(), getMessage("warning.exmntn.main.C_W004"));
        }

        /* テーブルの表示を変更する */
        view.candidateUnivTable.addSchedule();

        return null;
    }

    /* CEFR、国語記述入力ボタン押下アクションです。*/
    /**
     * 国語記述入力ボタン押下アクションです。
     *
     * @return 生徒一覧ダイアログ画面
     */
    @Execute
    public ExecuteResult inputButtonAction() {
        /* CEFR、国語記述入力ダイアログを開く */
        /* 国語記述入力ダイアログを開く */
        return forward(ExmntnInputController.class);
    }

    @Override
    @Execute
    public ExecuteResult selectStudentButtonAction() {
        return validateHelper.checkMovable() ? super.selectStudentButtonAction() : null;
    }

    @Override
    @Execute
    public ExecuteResult reportButtonAction() {
        return validateHelper.checkMovable() ? super.reportButtonAction() : null;
    }

    @Override
    @Execute
    public ExecuteResult maintenanceButtonAction() {
        return validateHelper.checkMovable() ? super.maintenanceButtonAction() : null;
    }

}

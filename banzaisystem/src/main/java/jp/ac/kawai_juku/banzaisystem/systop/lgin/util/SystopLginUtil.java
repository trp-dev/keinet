package jp.ac.kawai_juku.banzaisystem.systop.lgin.util;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * マスタ・プログラムチェック画面のユーティリティクラスです。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SystopLginUtil {

    /**
     * コンストラクタです。
     */
    private SystopLginUtil() {
    }

    /**
     * YYYYMMDDをYYYY/M/Dに変換します。
     *
     * @param yyyymmdd YYYYMMDD
     * @return YYYY/MM/DD
     */
    public static String toYMD(String yyyymmdd) {
        String yy = yyyymmdd.substring(0, 4);
        String mm = yyyymmdd.substring(4, 6);
        String dd = yyyymmdd.substring(6, 8);
        return StringUtils.stripStart(yy, "0") + "/" + StringUtils.stripStart(mm, "0") + "/"
                + StringUtils.stripStart(dd, "0");
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.util;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

/**
 *
 * 志望大学詳細画面のユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ExmntnUnivUtil {

    /**
     * コンストラクタです。
     */
    private ExmntnUnivUtil() {
    }

    /**
     * 注意事項欄を表示するかどうかを判定します。
     *
     * @param markExam マーク模試
     * @param systemDto {@link SystemDto}
     * @return 注意事項欄を表示するならtrue
     */
    public static boolean isAttentionVisible(ExamBean markExam, SystemDto systemDto) {
        return systemDto.getUnivExamDiv().equals("07") && markExam != null && ExamUtil.isCenter(markExam);
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.qlfc.controller;

import java.awt.event.ActionEvent;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.qlfc.view.ExmntnQlfcView;
import jp.ac.kawai_juku.banzaisystem.framework.component.CheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;

/**
 *
 * 大学検索（取得資格）画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnQlfcController extends SubController {

    /** View */
    @Resource(name = "exmntnQlfcView")
    private ExmntnQlfcView view;

    @Override
    @Logging(GamenId.C3_4)
    public void activatedAction() {
    }

    /**
     * 取得資格クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearObtainedLicenseButtonAction() {
        for (CheckBox c : view.findComponentsByClass(CheckBox.class)) {
            c.setSelectedSilent(false);
        }
        return null;
    }

    /**
     * 資格カテゴリチェックボックス変更アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult changeCategoryAction(ActionEvent e) {
        CodeCheckBox category = (CodeCheckBox) e.getSource();
        for (CodeComponent cc : view.findComponentsByCodeType(CodeType.CREDENTIAL)) {
            if (cc.getCode().getParent() == category.getCode()) {
                ((CodeCheckBox) cc).setSelectedSilent(category.isSelected());
            }
        }
        return null;
    }

    /**
     * 資格チェックボックス変更アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult changeCredentialAction(ActionEvent e) {
        CodeCheckBox cb = (CodeCheckBox) e.getSource();
        CodeCheckBox category = findCategoryCheckBox(cb.getCode().getParent());
        if (cb.isSelected()) {
            for (CodeComponent cc : view.findComponentsByCodeType(CodeType.CREDENTIAL)) {
                if (cc.getCode().getParent() == category.getCode() && !cc.isSelected()) {
                    return null;
                }
            }
            category.setSelectedSilent(true);
        } else {
            category.setSelectedSilent(false);
        }
        return null;
    }

    /**
     * 資格カテゴリのチェックボックスを返します。
     *
     * @param category 資格カテゴリコード
     * @return 資格カテゴリのチェックボックス
     */
    private CodeCheckBox findCategoryCheckBox(Code category) {
        for (CodeCheckBox c : view.findComponentsByClass(CodeCheckBox.class)) {
            if (c.getCode() == category) {
                return c;
            }
        }
        throw new RuntimeException("資格カテゴリのチェックボックスが見つかりませんでした。" + category);
    }

}

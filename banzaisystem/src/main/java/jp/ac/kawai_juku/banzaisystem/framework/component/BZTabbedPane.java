package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.ClassUtil;
import org.seasar.framework.util.MethodUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * タブペインコンポーネントです。<br>
 * SubViewをタブ表示する機能を提供します。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZTabbedPane extends JPanel implements BZComponent, SubViewManager {

    /** タブリスト */
    private final List<Tab> tabList = CollectionsUtil.newArrayList();

    /** タブ表示領域 */
    private final JPanel tabArea = new JPanel(null);

    /** タブパネル表示領域 */
    private final JPanel panelArea = new JPanel(null);

    /**
     * コンストラクタです。
     */
    public BZTabbedPane() {
        super(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        TabConfig config = BZFieldUtil.getAnnotation(field, TabConfig.class);
        for (Class<? extends SubController> clazz : config.value()) {
            tabList.add(new Tab(view, clazz));
        }

        tabArea.setOpaque(false);
        setOpaque(false);
        add(tabArea);
        add(panelArea);

        /* 先頭のタブを基準に大きさを調整する */
        Tab tab = tabList.get(0);
        Dimension size = tab.view.getSize();
        tabArea.setBounds(0, 0, size.width, tab.label.getHeight());
        panelArea.setBounds(0, tabArea.getHeight(), size.width, size.height);
        setSize(tabArea.getWidth(), tabArea.getHeight() + panelArea.getHeight());
    }

    /**
     * 全てのタブが非アクティブの場合は、先頭のタブをアクティブにします。<br>
     * アクティブなタブが存在する場合は、アクセスログ出力のためにそのタブを再度アクティブにします。
     */
    public void activate() {
        if (tabArea.getComponentCount() == 0) {
            activate(tabList.get(0).controller.getClass(), null);
        } else {
            for (Tab tab : tabList) {
                if (isActive(tab)) {
                    activate(tab.controller.getClass(), null);
                    break;
                }
            }
        }
    }

    /**
     * 指定したサブ画面をアクティブにします。
     *
     * @param clazz サブ画面のコントローラクラス
     */
    public void activate(Class<? extends SubController> clazz) {
        activate(clazz, null);
    }

    @Override
    public void activate(Class<? extends SubController> clazz, String methodName) {

        setTabEnabled(clazz, true);

        tabArea.removeAll();
        panelArea.removeAll();

        int left = 6;
        for (Tab tab : tabList) {
            if (clazz.isAssignableFrom(tab.controller.getClass())) {
                /* アクティブにするタブ */
                if (!tab.view.isInitialized()) {
                    /* Viewを初期化する */
                    tab.view.initialize();
                }
                tab.label.setLocation(left, 0);
                tabArea.add(tab.label);
                panelArea.add(tab.view.getView());
                if (methodName == null) {
                    tab.controller.activatedAction();
                } else {
                    MethodUtil.invoke(ClassUtil.getMethod(clazz, methodName, null), tab.controller, null);
                }
            } else {
                /* 非アクティブにするタブ */
                tab.button.setLocation(left, 0);
                tabArea.add(tab.button);
            }
            left += tab.label.getWidth() + 2;
        }

        revalidate();
        repaint();
    }

    /**
     * 指定したタブがアクティブであるかを返します。
     *
     * @param clazz {@link SubController}
     * @return アクティブならtrue
     */
    public boolean isActive(Class<? extends SubController> clazz) {
        return isActive(findTab(clazz));
    }

    /**
     * 指定したタブがアクティブであるかを返します。
     *
     * @param tab タブ
     * @return アクティブならtrue
     */
    private boolean isActive(Tab tab) {
        for (Component c : tabArea.getComponents()) {
            if (c == tab.label) {
                return true;
            }
        }
        return false;
    }

    /**
     * 指定したタブの操作可否を設定します。
     *
     * @param clazz {@link SubController}
     * @param isEnable 操作可否
     */
    public void setTabEnabled(Class<? extends SubController> clazz, boolean isEnable) {
        findTab(clazz).button.setEnabled(isEnable);
    }

    /**
     * 指定したタブの操作可否を返します。
     *
     * @param clazz {@link SubController}
     * @return 操作可否
     */
    public boolean isTabEnabled(Class<? extends SubController> clazz) {
        return findTab(clazz).button.isEnabled();
    }

    /**
     * 指定したサブ画面のタブを返します。
     *
     * @param clazz {@link SubController}
     * @return タブ
     */
    private Tab findTab(Class<? extends SubController> clazz) {
        for (Tab tab : tabList) {
            if (clazz.isAssignableFrom(tab.controller.getClass())) {
                return tab;
            }
        }
        throw new RuntimeException(String.format("指定したサブコントローラ[%s]は登録されていません。", clazz));
    }

    /**
     * タブ情報を保持するクラスです。
     */
    private final class Tab {

        /** コントローラ */
        private final SubController controller;

        /** ビュー */
        private final SubView view;

        /** タブラベル */
        private final JLabel label = new JLabel();

        /** タブボタン */
        private final JButton button = new JButton();

        /**
         * コンストラクタです。
         *
         * @param superView 上位View
         * @param clazz 画面のコントローラクラス
         */
        private Tab(AbstractView superView, Class<? extends SubController> clazz) {

            controller = SingletonS2Container.getComponent(clazz);
            view = controller.getSubView();

            BufferedImage imgOn = ImageUtil.readImage(superView.getClass(), "tab/" + controller.getId() + "_on.png");
            BufferedImage imgOff = ImageUtil.readImage(superView.getClass(), "tab/" + controller.getId() + "_off.png");

            label.setIcon(new ImageIcon(imgOn));
            label.setSize(imgOn.getWidth(), imgOn.getHeight());

            button.setBorder(null);
            button.setBounds(0, 0, imgOff.getWidth(), imgOff.getHeight());
            button.setIcon(new ImageIcon(imgOff));
            button.setFocusable(false);
            button.setFocusPainted(false);
            button.setOpaque(false);
            button.setContentAreaFilled(false);
            button.addMouseListener(ImageButton.CURSOR_MOUSE_LISTENER);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    activate(controller.getClass(), null);
                }
            });
        }
    }

}

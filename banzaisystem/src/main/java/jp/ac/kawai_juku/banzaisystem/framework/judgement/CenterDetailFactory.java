package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.data.Irregular1Center;
import jp.co.fj.kawaijuku.judgement.factory.helper.SubjectHelper;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * センター判定詳細情報生成ファクトリ<br>
 * ※既存PC版模試判定プロジェクトより移植
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class CenterDetailFactory extends DetailFactory<CenterDetail> {

    /** インデックス：科目配点 */
    private static final int IDX_SUBPNT = getSubBitIndex() + Subject.values().length;

    /** インデックス：同時選択不可区分 */
    private static final int IDX_CONCURRENT_NG = IDX_SUBPNT + Subject.values().length;

    /** インデックス：イレギュラ区分 */
    private static final int IDX_IRRDIV = IDX_CONCURRENT_NG + Subject.values().length;

    /** 科目情報 */
    private enum Subject {

        /** 英語 */
        EWRITING(CenterDetail.KEY_EWRITING),

        /** リスニング */
        ELISTENING(CenterDetail.KEY_ELISTENING),

        /** 数学�T */
        MATH1(CenterDetail.KEY_MATH1),

        /** 数学�TＡ */
        MATH1A(CenterDetail.KEY_MATH1A),

        /** 数学�U */
        MATH2(CenterDetail.KEY_MATH2),

        /** 数学�UＢ */
        MATH2B(CenterDetail.KEY_MATH2B),

        /** 現代文 */
        JPRESENT(CenterDetail.KEY_JPRESENT),

        /** 古文 */
        JCLASSICS(CenterDetail.KEY_JCLASSICS),

        /** 漢文 */
        JCHINESE(CenterDetail.KEY_JCHINESE),

        /** 物理基礎 */
        PHYSICS_BASIC(CenterDetail.KEY_PHYSICS_BASIC),

        /** 化学基礎 */
        CHEMISTRY_BASIC(CenterDetail.KEY_CHEMISTRY_BASIC),

        /** 生物基礎 */
        BIOLOGY_BASIC(CenterDetail.KEY_BIOLOGY_BASIC),

        /** 地学基礎 */
        EARTHSCIENCE_BASIC(CenterDetail.KEY_EARTHSCIENCE_BASIC),

        /** 物理 */
        PHYSICS(CenterDetail.KEY_PHYSICS),

        /** 化学 */
        CHEMISTRY(CenterDetail.KEY_CHEMISTRY),

        /** 生物 */
        BIOLOGY(CenterDetail.KEY_BIOLOGY),

        /** 地学 */
        EARTHSCIENCE(CenterDetail.KEY_EARTHSCIENCE),

        /** 日本史Ａ */
        JHISTORYA(CenterDetail.KEY_JHISTORYA),

        /** 世界史Ａ */
        WHISTORYA(CenterDetail.KEY_WHISTORYA),

        /** 地理Ａ */
        GEOGRAPHYA(CenterDetail.KEY_GEOGRAPHYA),

        /** 日本史Ｂ */
        JHISTORYB(CenterDetail.KEY_JHISTORYB),

        /** 世界史Ｂ */
        WHISTORYB(CenterDetail.KEY_WHISTORYB),

        /** 地理Ｂ */
        GEOGRAPHYB(CenterDetail.KEY_GEOGRAPHYB),

        /** 倫理 */
        ETHIC(CenterDetail.KEY_ETHIC),

        /** 政経 */
        POLITICS(CenterDetail.KEY_POLITICS),

        /** 現代社会 */
        SOCIALSTUDIES(CenterDetail.KEY_SOCIALSTUDIES),

        /** 倫政 */
        ETHICALPOLITICS(CenterDetail.KEY_ETHICALPOLITICS),

        /** 英語認定試験 */
        ENGPT(CenterDetail.KEY_ENGLISH_SANKA),

        /** 国語記述 */
        JPNDESC(CenterDetail.KEY_JAPANESE_KIJUTSU);

        /** 科目キー */
        private final SubjectKey key;

        /**
         * コンストラクタです。
         *
         * @param key 科目キー
         */
        Subject(SubjectKey key) {
            this.key = key;
        }

    }

    /**
     * コンストラクタです。
     *
     * @param values マスタ配列
     */
    CenterDetailFactory(String[] values) {
        super(values);
    }

    @Override
    protected CenterDetail createDetail() {
        return new CenterDetail();
    }

    @Override
    protected void prepareSubject(CommonDetailHolder detailHolder) {
        for (Subject sbj : Subject.values()) {
            String[] subBits = getString(getSubBitIndex() + sbj.ordinal()).split(GROUP_SEP);
            String[] subPnts = getString(IDX_SUBPNT + sbj.ordinal()).split(GROUP_SEP);
            String[] concurrentNgs = getString(IDX_CONCURRENT_NG + sbj.ordinal()).split(GROUP_SEP);
            for (int i = 0; i < subBits.length; i++) {
                String subBit = subBits[i];
                double subPnt = NumberUtils.toDouble(getArrayValue(subPnts, i), Detail.ALLOT_NA);
                String concurrentNg = getArrayValue(concurrentNgs, i);
                SubjectHelper.prepare(detailHolder, createSubject(subBit, subPnt, null, concurrentNg), sbj.key, CenterDetail.class);
            }
        }
    }

    @Override
    protected int getIrregularDivIndex() {
        return IDX_IRRDIV;
    }

    @Override
    protected Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub) {
        return new Irregular1Center(irregular, maxSelect, irrSub);
    }

}

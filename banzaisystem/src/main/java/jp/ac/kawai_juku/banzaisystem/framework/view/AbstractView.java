package jp.ac.kawai_juku.banzaisystem.framework.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ButtonGroupManager;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeRadio;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.LayerSupport;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.creator.ViewCreator;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.ClassUtil;
import org.seasar.framework.util.FieldUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * Viewクラスの基底クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class AbstractView {

    /** Logger */
    private static final Logger logger = Logger.getLogger(AbstractView.class);

    /** パネル */
    private final ImagePanel panel = new ImagePanel(getClass());

    /** レイヤ */
    private final JLayer<ImagePanel> layer = new JLayer<ImagePanel>(panel);

    /** 初期化済フラグ */
    private boolean initialized;

    /** グループ番号別のコンポーネントマップ */
    private final Map<Integer, List<JComponent>> groupMap = CollectionsUtil.newHashMap();

    /**
     * コンストラクタです。
     */
    public AbstractView() {
        Dimension size = panel.getSize();
        layer.setSize(size);
        layer.setPreferredSize(size);
        JPanel glassPane = layer.getGlassPane();
        glassPane.setLayout(null);
        glassPane.setVisible(true);
    }

    /**
     * 初期化します。
     */
    public void initialize() {
        if (!initialized) {
            /* コンポーネントを初期化する */
            initComponent();
            /* 初期化済フラグを立てる */
            initialized = true;
        } else {
            logger.warn("既に初期化されています。" + getClass().getSimpleName());
        }
    }

    /**
     * Viewに宣言されているコンポーネントを初期化します。
     */
    private void initComponent() {
        for (Field field : ClassUtil.getDeclaredFields(BZClassUtil.getOriginalClass(getClass()))) {
            if (BZComponent.class.isAssignableFrom(field.getType())) {
                BZComponent comp = (BZComponent) ClassUtil.newInstance(field.getType());
                initComponent(field, comp);
                FieldUtil.set(field, this, comp);
                JComponent jc = (JComponent) comp;
                if (comp instanceof LayerSupport) {
                    JLayer<JComponent> l = ((LayerSupport) jc).getLayer();
                    l.setBounds(l.getView().getBounds());
                    panel.add(l);
                } else {
                    panel.add(jc);
                }
                continue;
            }
        }
    }

    /**
     * 指定したクラスのコンポーネントを返します。
     *
     * @param clazz 検索するクラス
     * @param <T> コンポーネントの型
     * @return コンポーネントリスト
     */
    public final <T extends JComponent> List<T> findComponentsByClass(Class<T> clazz) {
        List<T> list = CollectionsUtil.newArrayList();
        for (Component c : panel.getComponents()) {
            Component target;
            if (c instanceof JLayer) {
                target = ((JLayer<?>) c).getView();
            } else {
                target = c;
            }
            if (clazz.isAssignableFrom(target.getClass())) {
                list.add(clazz.cast(target));
            }
        }
        return list;
    }

    /**
     * 指定したクラスのコンポーネントのうち、表示されているものを返します。
     *
     * @param clazz 検索するクラス
     * @param <T> コンポーネントの型
     * @return コンポーネントリスト
     */
    public final <T extends JComponent> List<T> findVisibleComponentsByClass(Class<T> clazz) {
        List<T> list = CollectionsUtil.newArrayList();
        for (T c : findComponentsByClass(clazz)) {
            if (((Component) c).isVisible()) {
                list.add(c);
            }
        }
        return list;
    }

    /**
     * 指定したグループ番号のコンポーネントを返します。
     *
     * @param group グループ番号
     * @return コンポーネントリスト
     */
    public final List<JComponent> findComponentsByGroup(int group) {
        List<JComponent> list = groupMap.get(group);
        if (list == null) {
            return Collections.emptyList();
        } else {
            return list;
        }
    }

    /**
     * 指定したコード種別のコンポーネントを返します。
     *
     * @param type コード種別
     * @return コンポーネントリスト
     */
    public final List<CodeComponent> findComponentsByCodeType(CodeType type) {
        List<CodeComponent> list = CollectionsUtil.newArrayList();
        for (Component c : panel.getComponents()) {
            if (c instanceof CodeComponent) {
                CodeComponent cc = (CodeComponent) c;
                if (cc.getCode().getCodeType() == type) {
                    list.add(cc);
                }
            }
        }
        return list;
    }

    /**
     * コンポーネントを初期化します。
     *
     * @param field コンポーネントのフィールド定義
     * @param comp 初期化するコンポーネント
     */
    private void initComponent(Field field, BZComponent comp) {

        JComponent jc = (JComponent) comp;

        /* Locationを設定する */
        Location loc = BZFieldUtil.getAnnotation(field, Location.class);
        jc.setLocation(loc.x(), loc.y());

        /* Sizeを設定する */
        Size size = field.getAnnotation(Size.class);
        if (size != null) {
            jc.setSize(size.width(), size.height());
        }

        /* Fontを設定する */
        Font font = field.getAnnotation(Font.class);
        if (font != null) {
            jc.setFont(font.value().getFont());
        } else {
            jc.setFont(BZFont.DEFAULT.getFont());
        }

        /* Foregroundを設定する */
        Foreground foreground = field.getAnnotation(Foreground.class);
        if (foreground != null) {
            jc.setForeground(new Color(foreground.r(), foreground.g(), foreground.b()));
        }

        /* 表示状態を設定する */
        if (field.isAnnotationPresent(Invisible.class)) {
            jc.setVisible(false);
        }

        /* 無効状態を設定する */
        if (field.isAnnotationPresent(Disabled.class)) {
            jc.setEnabled(false);
        }

        /* グループ番号別に保持する */
        Group group = field.getAnnotation(Group.class);
        if (group != null) {
            for (int key : group.value()) {
                List<JComponent> list = groupMap.get(key);
                if (list == null) {
                    list = CollectionsUtil.newArrayList();
                    groupMap.put(key, list);
                }
                list.add(jc);
            }
        }

        /* 個別の初期化処理を呼び出す */
        comp.initialize(field, this);
    }

    /**
     * ViewのID（ファンクションID＋スクリーンID）を返します。
     *
     * @return ID
     */
    public final String getId() {
        String className = BZClassUtil.getSimpleName(getClass());
        return StringUtils.removeEnd(StringUtils.uncapitalize(className), ViewCreator.NAME_SUFFIX);
    }

    /**
     * リソース用のパッケージ名（ファンクションID.スクリーンID）を返します。
     *
     * @return パッケージ名
     */
    public final String getPkg() {
        String[] array = StringUtils.splitByCharacterTypeCamelCase(getId());
        return array[0] + "." + array[1].toLowerCase();
    }

    /**
     * 初期化済フラグを返します。
     *
     * @return 初期化済ならtrue
     */
    public final boolean isInitialized() {
        return initialized;
    }

    /**
     * Viewコンポーネントを返します。
     *
     * @return Viewコンポーネント
     */
    public final JComponent getView() {
        return layer;
    }

    /**
     * Viewサイズを返します。
     *
     * @return Viewサイズ
     */
    public final Dimension getSize() {
        return panel.getSize();
    }

    /**
     * 指定したコード種別のボタングループで選択されているコードを返します。
     *
     * @param codeType コード種別
     * @return コード
     */
    public Code getSelectedCode(CodeType codeType) {
        for (Enumeration<AbstractButton> enu = ButtonGroupManager.getButtonGroup(this, codeType).getElements(); enu
                .hasMoreElements();) {
            AbstractButton button = enu.nextElement();
            if (button.isSelected()) {
                return ((CodeRadio) button).getCode();
            }

        }
        return null;
    }

    /**
     * panelを返します。
     *
     * @return panel
     */
    public ImagePanel getPanel() {
        return panel;
    }

    /**
     * ViewのFocusTraversalPolicyを設定します。
     *
     * @param policy {@link FocusTraversalPolicy}
     */
    protected final void setFocusTraversalPolicy(FocusTraversalPolicy policy) {
        panel.setFocusTraversalPolicyProvider(true);
        panel.setFocusTraversalPolicy(policy);
    }

    /**
     * レイヤ表示するコンポーネントを追加します。
     *
     * @param comp 追加するコンポーネント
     */
    public final void addLayerComponent(JComponent comp) {
        layer.getGlassPane().add(comp);
    }

    /**
     * 指定したグループ番号のコンポーネントの可視フラグを設定します。
     *
     * @param group グループ番号
     * @param flag 可視にする場合はtrue
     */
    public void setVisibleByGroup(int group, boolean flag) {
        for (JComponent comp : findComponentsByGroup(group)) {
            comp.setVisible(flag);
        }
    }

    /**
     * 指定したグループ番号のコンポーネントの有効フラグを設定します。
     *
     * @param group グループ番号
     * @param flag 有効にする場合はtrue
     */
    public void setEnabledByGroup(int group, boolean flag) {
        for (JComponent comp : findComponentsByGroup(group)) {
            comp.setEnabled(flag);
        }
    }

}

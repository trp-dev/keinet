package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dao;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 検索結果画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnRsltDao extends BaseDao {

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 志望大学リストを取得します。
     *
     * @param exam 対象模試
     * @param id 対象生徒の個人ID
     * @return 志望大学リスト
     */
    public List<UnivKeyBean> selectCandidateUnivList(ExamBean exam, Integer id) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        return jdbcManager.selectBySqlFile(UnivKeyBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 志望大学をカウントします。
     *
     * @param exam 対象模試
     * @param id 対象生徒の個人ID
     * @return 志望大学のカウント
     */
    public long countCandidateUniv(ExamBean exam, Integer id) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 全模試リストを取得します。
     *
     * @return 全模試リスト
     */
    public List<ExamBean> selectAllExamList() {
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath(), systemDto.getUnivYear()).getResultList();
    }

    /**
     * 志望大学リストを登録します。
     *
     * @param params パラメータリスト
     */
    public void insertCandidateUniv(List<BeanMap> params) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), params).execute();
    }

    /**
     * 模試単位のシステム登録志望大学数の最大値を返します。
     *
     * @param exam 対象模試（対象模試はカウント対象外）
     * @param id 対象生徒の個人ID
     * @param univKeyList カウント対象外志望大学の10ケタ大学コードリスト
     * @return 志望大学のカウント
     */
    public int selectMaxCountCandidateUniv(ExamBean exam, Integer id, List<String> univKeyList) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("id", id);
        param.put("univKeyList", univKeyList);
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getSingleResult();
    }

}

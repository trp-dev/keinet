package jp.ac.kawai_juku.banzaisystem.mainte.dnet.service;

import java.io.File;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.log4j.BZFileAppender;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;

/**
 *
 * データフォルダ設定ダイアログのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteDnetService {

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /**
     * データフォルダを移動します。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     * @param overwriteFlg 上書きフラグ
     */
    public void moveFolder(DataFolder folder, File newFolder, boolean overwriteFlg) {

        /* 設定ファイルをロードする */
        SettingProps props = SettingProps.load();

        /* データファイルを移動する */
        if (folder.getDataFileName() != null && overwriteFlg) {
            moveDataFile(folder, newFolder, props);
        }

        /* 設定を保存する */
        props.remove(folder.getNetworkKey());
        props.setValue(folder.getLocalKey(), newFolder.getAbsolutePath());
        props.store();

        /* ログを出力する */
        logService.output(GamenId.E4_d1, folder.getName(), newFolder.getAbsolutePath());
    }

    /**
     * データファイルを移動します。
     *
     * @param folder データフォルダ
     * @param newFolder 移動先データフォルダ
     * @param props {@link SettingProps}
     */
    private void moveDataFile(DataFolder folder, File newFolder, SettingProps props) {
        File currentFolder = SettingPropsUtil.getDirectory(folder);
        if (folder == DataFolder.LOG_FOLDER) {
            /* ログファイルは移動前に閉じておく */
            BZFileAppender.closeFile();
        }
        IOUtil.moveFileToDirectory(new File(currentFolder, folder.getDataFileName()), newFolder);
    }

}

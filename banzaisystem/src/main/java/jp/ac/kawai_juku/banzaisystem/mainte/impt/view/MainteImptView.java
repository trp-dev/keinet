package jp.ac.kawai_juku.banzaisystem.mainte.impt.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.component.MainteImptTable;

/**
 *
 * メンテナンス-個人成績インポート画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteImptView extends SubView {

    /** インポート状況一覧テーブル */
    @Location(x = 26, y = 76)
    @Size(width = 473, height = 492)
    public MainteImptTable mainteImportStatusTable;

    /** 個人志望手動インポートボタン */
    @Location(x = 159, y = 583)
    public ImageButton personalWishHandOpeButton;

    /** ご注意 */
    @Location(x = 519, y = 58)
    public TextLabel attentionLabel;

}

package jp.ac.kawai_juku.banzaisystem.framework.controller;

import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

/**
 *
 * サブ画面の基底コントローラクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class SubController extends AbstractController {

    /**
     * 画面がアクティブになったタイミングで呼び出されるメソッドです。
     */
    public abstract void activatedAction();

    /**
     * このコントローラに対応するSubViewを返します。
     *
     * @return {@link AbstractView}
     */
    public final SubView getSubView() {
        AbstractView view = getView();
        if (view instanceof SubView) {
            return (SubView) view;
        } else {
            throw new RuntimeException(String.format("サブ画面のビュー[%s]はSubViewを継承する必要があります。",
                    BZClassUtil.getSimpleName(view.getClass())));

        }
    }

    /**
     * 指定したサブ画面をアクティブにします。
     *
     * @param clazz アクティブにするサブ画面のコントローラ
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult activate(Class<? extends SubController> clazz) {
        return new ActivateResult(clazz, null);
    }

    /**
     * 指定したサブ画面をアクティブにします。
     *
     * @param clazz アクティブにするサブ画面のコントローラ
     * @param methodName アクティブにするタイミングで起動するメソッド名
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult activate(Class<? extends SubController> clazz, String methodName) {
        return new ActivateResult(clazz, methodName);
    }

}

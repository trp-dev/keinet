package jp.ac.kawai_juku.banzaisystem.exmntn.subs.service;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubBit;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * センタ科目検索ロジックです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class CenterSubjectSearch {

    /** 科目キーセット */
    private final Set<SubjectKey> keySet;

    /**
     * コンストラクタです。
     *
     * @param searchBean 検索条件Bean
     */
    CenterSubjectSearch(ExmntnSubsUnivSearchInBean searchBean) {

        Set<SubjectKey> set = CollectionsUtil.newHashSet();
        if (searchBean.getCenterList() != null) {
            for (CenterSubject subject : searchBean.getCenterList()) {
                for (Field field : CenterDetail.class.getDeclaredFields()) {
                    try {
                        field.setAccessible(true);
                        SubjectKey subKey = (SubjectKey) field.get(CenterDetail.class);
                        if (subject.getKey().getKeyString().equals(subKey.getKeyString())) {
                            set.add(subKey);
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
        this.keySet = Collections.unmodifiableSet(set);
    }

    /**
     * 検索を実行します。
     *
     * @param univ 大学データ
     * @return 検索結果
     */
    boolean search(UnivData univ) {
        return checkSubject(univ);
    }

    /**
     * センタ科目の課し状態をチェックします。
     *
     * @param univ 大学データ
     * @return チェック結果
     */
    private boolean checkSubject(UnivData univ) {

        if (keySet.isEmpty()) {
            /* 科目チェックなし */
            return true;
        }

        for (Object obj : univ.getDetailHash().values()) {
            UnivDetail detail = (UnivDetail) obj;
            if (checkSubject(detail.getCenter().getForDisplay())) {
                return true;
            }
        }

        return false;
    }

    /**
     * センタ科目の課し状態をチェックします。
     *
     * @param detail 大学詳細データ
     * @return チェック結果
     */
    private boolean checkSubject(CommonDetailHolder detail) {

        /* TODO：判定処理の課し科目対応で判定する科目を増加予定！ */
        /* 必須科目をチェックする */
        for (Object obj : detail.getImposingSubjects().entrySet()) {
            Entry<?, ?> entry = (Entry<?, ?>) obj;
            SubjectKey key = (SubjectKey) entry.getKey();
            ImposingSubject subject = (ImposingSubject) entry.getValue();
            SubBit subBit = subject.getRequired();
            if (subBit.isImposed() && !checkChosen(key)) {
                return false;
            }
        }

        /* 選択科目をグループ別にチェックする */
        for (int groupIndex = 0; groupIndex < detail.getGroupLength(); groupIndex++) {
            if (!checkGroup(detail, groupIndex)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 指定した科目を選択しているかをチェックします。
     *
     * @param key 科目キー
     * @return チェック結果
     */
    private boolean checkChosen(SubjectKey key) {
        return keySet.contains(key);
    }

    /**
     * グループ内科目数を満たしているかをチェックします。
     *
     * @param detail 大学詳細データ
     * @param groupIndex グループ番号
     * @return チェック結果
     */
    private boolean checkGroup(CommonDetailHolder detail, int groupIndex) {

        int[] courseCounts = countCourse(detail, groupIndex);

        int groupCount = 0;
        for (int i = 0; i < courseCounts.length; i++) {
            if (courseCounts[i] > detail.getGroupCourseCount(groupIndex, i)) {
                /*
                 * 選択した科目がグループ別教科内科目数を超えていた場合は
                 * グループ別教科内科目数だけ加算する。
                 */
                groupCount += detail.getGroupCourseCount(groupIndex, i);
            } else {
                groupCount += courseCounts[i];
            }
        }

        return groupCount >= detail.getGroupCount(groupIndex);
    }

    /**
     * グループ・教科別に選択した科目数をカウントします。
     *
     * @param detail 大学詳細データ
     * @param groupIndex グループ番号
     * @return グループ・教科別科目数
     */
    private int[] countCourse(CommonDetailHolder detail, int groupIndex) {

        int[] courseCount = new int[detail.getCourseLength()];

        Set<Pair<Integer, String>> pairSet = CollectionsUtil.newHashSet();
        for (Object obj : detail.getImposingSubjects().entrySet()) {
            Entry<?, ?> entry = (Entry<?, ?>) obj;
            SubjectKey key = (SubjectKey) entry.getKey();
            ImposingSubject subject = (ImposingSubject) entry.getValue();
            SubBit subBit = subject.getElective();
            if (!subBit.isImposed()) {
                continue;
            }
            if (subBit.getGroupIndex() != groupIndex) {
                continue;
            }
            /* ペアは後で処理する */
            if (subBit.isPair()) {
                pairSet.add(Pair.of(Integer.valueOf(key.getCourseIndex()), subBit.getBit()));
                continue;
            }
            /* 選択科目を選んでいるならカウントアップする */
            if (checkChosen(key)) {
                courseCount[key.getCourseIndex()]++;
            }
        }

        /* ビット別にペアをチェックする */
        for (Pair<Integer, String> pair : pairSet) {
            boolean isTakenAll = true;
            int takenCount = 0;
            int courseIndex = pair.getLeft().intValue();
            for (Object obj : detail.getImposingSubjects().entrySet()) {
                Entry<?, ?> entry = (Entry<?, ?>) obj;
                SubjectKey key = (SubjectKey) entry.getKey();
                ImposingSubject subject = (ImposingSubject) entry.getValue();
                SubBit subBit = subject.getElective();
                if (!subBit.isImposed()) {
                    continue;
                }
                if (subBit.getGroupIndex() != groupIndex) {
                    continue;
                }
                if (!subBit.isPair()) {
                    continue;
                }
                if (!pair.getRight().equals(subBit.getBit()) || subBit.getCourseIndex() != courseIndex) {
                    continue;
                }
                if (checkChosen(key)) {
                    takenCount++;
                } else {
                    isTakenAll = false;
                }
            }
            if (Detail.Japanese.COURSE_INDEX == courseIndex) {
                /* 国語：ペアをすべて選択していた場合のみ科目数をカウントをアップする */
                if (isTakenAll) {
                    courseCount[courseIndex]++;
                }
            } else if (takenCount > 1) {
                /* 国語以外：2科目以上選択していれば科目数をカウントをアップする */
                courseCount[courseIndex]++;
            }
        }

        return courseCount;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 画像トグルボタンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ImageToggleButton extends BaseImageToggleButton {

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        addActionListener(new ControllerActionListener(view, field.getName()));
    }

}

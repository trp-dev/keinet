package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.Icon;

/**
 *
 * ノードインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface Node {

    /**
     * ノードのアイコンを返します。
     *
     * @return アイコン
     */
    Icon getIcon();

}

package jp.ac.kawai_juku.banzaisystem.mainte.impt.service;

import java.util.List;
import java.util.Vector;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.bean.SystopLginDataStatusBean;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.service.SystopLginService;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * メンテナンス-個人成績インポート画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)OZEKI.Hiroshige
 *
 */
public class MainteImptService {

    /** マスタ・プログラムチェック画面のサービス */
    @Resource
    private SystopLginService systopLginService;

    /**
     * テーブルのデータリストを生成します。
     *
     * @return データリスト
     */
    public Vector<Vector<Object>> createDataList() {

        List<SystopLginDataStatusBean> list = systopLginService.findDataStatusList(null);

        Vector<Vector<Object>> result = CollectionsUtil.newVector(list.size());
        for (SystopLginDataStatusBean bean : list) {
            Vector<Object> row = CollectionsUtil.newVector(2);
            row.add(new CellData(bean));
            row.add(new CellData(CodeType.RECORD_IMP_STATUS.value2Code(bean.getRecordStatus())));
            result.add(row);
        }

        return result;
    }

}

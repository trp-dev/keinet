package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.controller;

import javax.annotation.Resource;
import javax.swing.SwingWorker;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dto.ExmntnRsltDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.helper.ExmntnRsltHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.maker.ExmntnRsltA0401Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

/**
 *
 * 検索結果画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)OZEKI.Hiroshige
 *
 */
public class ExmntnRsltController extends SubController {

    /** View */
    @Resource(name = "exmntnRsltView")
    private ExmntnRsltView view;

    /** Helper */
    @Resource(name = "exmntnRsltHelper")
    private ExmntnRsltHelper helper;

    /** DTO */
    @Resource(name = "exmntnRsltDto")
    private ExmntnRsltDto dto;

    @Override
    @Logging(GamenId.C4)
    public void activatedAction() {
        if (dto.getDataList() == null) {
            /* 検索結果がNULLなら再検索する */
            new SwingWorker<Object, Object>() {
                @Override
                protected ExecuteResult doInBackground() throws Exception {
                    WindowManager.getInstance().startWait();
                    helper.initDataList();
                    return null;
                }

                @Override
                protected void done() {
                    try {
                        filterAction();
                    } finally {
                        WindowManager.getInstance().stopWait();
                    }
                }
            }.execute();
        } else {
            filterAction();
        }
    }

    /**
     * 「志望大学リストに追加する」ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult addCandidateUnivButtonAction() {
        helper.addCandidateUnivButtonAction();
        return forward(ExmntnMainController.class, "dispCandidateUnivListFromSerachAction");
    }

    /**
     * 検索結果フィルタアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult filterAction() {
        helper.initResultTable();
        return null;
    }

    /**
     * 印刷ボタン押下アクションです。
     *
     * @return 検索結果印刷画面
     */
    @Execute
    public ExecuteResult printButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C4).addCreator(ExmntnRsltA0401Maker.class, helper.createInBean()).launch();
                return null;
            }
        };
    }

}

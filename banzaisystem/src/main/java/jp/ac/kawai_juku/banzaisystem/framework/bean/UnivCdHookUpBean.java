package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学コード紐付け対象を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivCdHookUpBean {

    /** 個人ID */
    private Integer id;

    /** 模試年度 */
    private String examYear;

    /** 模試コード */
    private String examCd;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 個人IDをセットします。
     *
     * @param id 個人ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

}

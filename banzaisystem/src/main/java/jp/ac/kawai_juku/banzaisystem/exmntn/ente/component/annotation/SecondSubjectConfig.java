package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;

/**
 *
 * �ʎ����Ȗڂ�ݒ肷��A�m�e�[�V�����ł��B
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface SecondSubjectConfig {

    /**
     * �ʎ����Ȗڂ�ݒ肵�܂��B
     *
     * @return �ʎ����Ȗ�
     */
    SecondSubject value();

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.component;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_OFF;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_ON;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 志望大学リスト用テーブルです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnMainTable extends TableScrollPane {

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /**
     * コンストラクタです。
     */
    public ExmntnMainTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        JPanel columnHeader = createColumnHeaderView();
        setColumnHeaderView(columnHeader);
        setViewportView(createTableView(columnHeader));
        setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, createUpperRightCorner());
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView() {
        ImagePanel panel = new ImagePanel(ExmntnMainTable.class);
        int x = 0;
        for (JComponent jc : createColumnHeaderList()) {
            if (panel.getHeight() == jc.getHeight()) {
                jc.setLocation(x, 0);
            } else {
                jc.setLocation(x, panel.getHeight() - jc.getHeight());
            }
            panel.add(jc);
            x += jc.getWidth();
        }
        return panel;
    }

    /**
     * ヘッダコンポーネントリストを生成します。
     *
     * @return ヘッダコンポーネントリスト
     */
    private List<JComponent> createColumnHeaderList() {

        List<JComponent> list = CollectionsUtil.newArrayList();
        list.add(createHeaderLabel("no", CellAlignment.CENTER));
        list.add(createHeaderLabel("univ", CellAlignment.LEFT));
        list.add(createHeaderLabel("faculty", CellAlignment.LEFT));
        list.add(createHeaderLabel("dept", CellAlignment.LEFT));
        list.add(createHeaderLabel("exclude", CellAlignment.CENTER));
        /*list.add(createHeaderLabel("include", CellAlignment.CENTER));*/
        list.add(createHeaderLabel("second", CellAlignment.CENTER));
        list.add(createHeaderLabel("total", CellAlignment.CENTER));

        return list;
    }

    /**
     * 右上のCornerを生成します。
     *
     * @return Corner
     */
    private JPanel createUpperRightCorner() {
        JPanel panel = new JPanel();
        panel.setBackground(new Color(198, 190, 227));
        return panel;
    }

    /**
     * テーブルを生成します。
     *
     * @param header ヘッダ
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final JPanel header) {

        final ReadOnlyTableModel model = new ReadOnlyTableModel(header.getComponentCount() + 1);

        final JTable table = new BZTable(model) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                /* 模試記入大学の場合、背景色を変更する */
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    ExmntnMainCandidateUnivBean bean = (ExmntnMainCandidateUnivBean) getModel().getValueAt(row, 0);
                    if (Code.CANDIDATE_UNIV_EXAM.getValue().equals(bean.getSection())) {
                        c.setBackground(Color.LIGHT_GRAY);
                    } else {
                        c.setBackground(Color.WHITE);
                    }
                }
                /* 評価文字列のフォントを設定する */
                if (column > 3) {
                    Font font = c.getFont();
                    c.setFont(new Font(BZConstants.MONOSPACED_FONT, font.getStyle(), font.getSize()));
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (Component c : header.getComponents()) {
                    x += c.getWidth();
                    g.drawLine(x - 1, 0, x - 1, getHeight() - 1);
                }
                for (int i = 1; i <= getRowCount(); i++) {
                    int y = getRowHeight() * i - 1;
                    g.drawLine(0, y, getWidth() - 1, y);
                }
            }

            @Override
            public Object getValueAt(int row, int column) {
                if (column == 0) {
                    return Integer.valueOf(row + 1);
                } else {
                    return super.getValueAt(row, column);
                }
            }
        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ExmntnMainView view = SingletonS2Container.getComponent(ExmntnMainView.class);
                /* ボタンの状態を変更する */
                int count = table.getSelectedRowCount();
                if (count == 0) {
                    view.deleteButton.setEnabled(false);
                    view.upButton.setEnabled(false);
                    view.downButton.setEnabled(false);
                    view.detailJudgeButton.setEnabled(false);
                    view.addScheduleButton.setEnabled(false);
                } else if (count == 1) {
                    view.deleteButton.setEnabled(true);
                    view.upButton.setEnabled(table.getSelectedRow() > 0);
                    view.downButton.setEnabled(table.getSelectedRow() + 1 < table.getRowCount());
                    view.detailJudgeButton.setEnabled(true);
                    view.addScheduleButton.setEnabled(true);
                } else {
                    view.deleteButton.setEnabled(true);
                    view.upButton.setEnabled(false);
                    view.downButton.setEnabled(false);
                    view.detailJudgeButton.setEnabled(true);
                    view.addScheduleButton.setEnabled(true);
                }
            }
        });

        /* ダブルクリック時の動作設定 */
        table.addMouseListener(new MouseAdapter() {

            /** ExmntnMainUnivHelper */
            private final ExmntnMainUnivHelper helper = SingletonS2Container.getComponent(ExmntnMainUnivHelper.class);

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int row = table.rowAtPoint(e.getPoint());
                    if (row >= 0) {
                        ExmntnMainCandidateUnivBean bean = (ExmntnMainCandidateUnivBean) model.getValueAt(row, 0);
                        helper.showUnivDetail(bean);
                    }
                }
            }
        });

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        Component[] components = header.getComponents();
        for (int i = 0; i < components.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(components[i].getWidth());
            CellAlignment align = ((LabelHeader) components[i]).getAlignment();
            if (align != null) {
                columnModel.getColumn(i).setCellRenderer(align.getRenderer());
            }
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 選択中の志望大学リストを返します。
     *
     * @return 選択中の志望大学リスト
     */
    public List<ExmntnMainCandidateUnivBean> getSelectedUnivList() {

        List<ExmntnMainCandidateUnivBean> list = CollectionsUtil.newArrayList();

        JTable table = getTable();
        TableModel model = table.getModel();
        int[] rowIndexArray;
        if (table.getSelectedRowCount() > 0) {
            /* 選択されている場合 */
            rowIndexArray = table.getSelectedRows();
        } else {
            /* 未選択の場合 */
            return list;
        }

        for (int rowIndex : rowIndexArray) {
            ExmntnMainCandidateUnivBean bean = (ExmntnMainCandidateUnivBean) model.getValueAt(rowIndex, 0);
            list.add(bean);
        }

        return list;
    }

    /**
     * 選択中の志望大学の志望順位を変更します。
     *
     * @param isUp 上に移動するならtrue
     */
    public void changeRank(boolean isUp) {

        /* モデルから志望大学リストを取得する */
        List<ExmntnMainCandidateUnivBean> list = getCandidateUnivList();

        /* 志望大学リスト内の順位を更新する */
        int index = getTable().getSelectedRow();
        int newIndex = index + (isUp ? -1 : 1);
        list.add(newIndex, list.remove(index));

        /* モデルに再セットする */
        setCandidateUnivList(list);

        /* 選択を復元する */
        getTable().setRowSelectionInterval(newIndex, newIndex);
    }

    /**
     * 選択中の志望大学を受験スケジュールに追加します。
     */
    public void addSchedule() {

        /* 志望大学のフラグを変更する */
        for (ExmntnMainCandidateUnivBean bean : getSelectedUnivList()) {
            bean.setPlanUnivFlg(FLG_ON);
        }

        /* モデルに再セットする */
        setCandidateUnivList(getCandidateUnivList());
    }

    /**
     * 指定した大学を受験スケジュールから削除します。
     *
     * @param keyList 大学リスト
     */
    public void deleteSchedule(List<? extends UnivKeyBean> keyList) {

        /* モデルから志望大学リストを取得する */
        List<ExmntnMainCandidateUnivBean> list = getCandidateUnivList();

        /* 志望大学のフラグを変更する */
        for (ExmntnMainCandidateUnivBean bean : list) {
            if (keyList.indexOf(bean) > -1) {
                bean.setPlanUnivFlg(FLG_OFF);
            }
        }

        /* モデルに再セットする */
        setCandidateUnivList(list);
    }

    /**
     * 志望大学リストをセットします。
     *
     * @param list 志望大学リスト
     */
    public void setCandidateUnivList(List<ExmntnMainCandidateUnivBean> list) {
        setDataList(toDataList(list));
    }

    /**
     * 志望大学リストを返します。
     *
     * @return 志望大学リスト
     */
    public List<ExmntnMainCandidateUnivBean> getCandidateUnivList() {
        TableModel model = getTable().getModel();
        List<ExmntnMainCandidateUnivBean> list = CollectionsUtil.newArrayList(model.getRowCount());
        for (int i = 0; i < model.getRowCount(); i++) {
            list.add((ExmntnMainCandidateUnivBean) model.getValueAt(i, 0));
        }
        return list;
    }

    /**
     * 志望大学リストをテーブル用データリストに変換します。
     *
     * @param list 志望大学リスト
     * @return テーブル用データリスト
     */
    private Vector<Vector<Object>> toDataList(List<ExmntnMainCandidateUnivBean> list) {
        Vector<Vector<Object>> dataList = CollectionsUtil.newVector();
        for (ExmntnMainCandidateUnivBean bean : list) {
            Vector<Object> row = CollectionsUtil.newVector();
            row.add(bean);
            row.add(StringUtils.EMPTY);
            row.add(ObjectUtils.toString(bean.getUnivName()));
            row.add(ObjectUtils.toString(bean.getFacultyName()));
            row.add(ObjectUtils.toString(bean.getDeptName()));
            row.add(ObjectUtils.toString(bean.getValuateExclude()));
            /*row.add(ObjectUtils.toString(bean.getValuateInclude()));*/
            row.add(ObjectUtils.toString(bean.getSecondRating()));
            row.add(ObjectUtils.toString(bean.getTotalRating()));
            dataList.add(row);
        }
        return dataList;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    private void setDataList(Vector<Vector<Object>> dataList) {
        ((ReadOnlyTableModel) getTable().getModel()).setDataVector(dataList);
    }

}

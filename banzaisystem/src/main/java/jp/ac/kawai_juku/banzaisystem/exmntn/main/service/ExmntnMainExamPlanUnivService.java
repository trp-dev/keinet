package jp.ac.kawai_juku.banzaisystem.exmntn.main.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXAM_PLAN_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainExamPlanUnivDaoInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dao.ExmntnMainExamPlanUnivDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.framework.bean.StudentExamPlanUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.BooleanUtils;
import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;

/**
 *
 * 個人成績・志望大学画面用の受験予定大学サービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainExamPlanUnivService {

    /** 受験予定大学の登録上限数 */
    private static final int MAX_PLAN_COUNT = EXAM_PLAN_UNIV_MAX;

    /** DAO */
    @Resource(name = "exmntnMainExamPlanUnivDao")
    private ExmntnMainExamPlanUnivDao dao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /**
     * （先生用）受験予定大学を追加します。
     *
     * @param id 個人ID
     * @param addList 追加する受験予定大学リスト
     * @return 入試日が重複していたらtrue
     */
    public boolean addTeacherExamPlanUniv(Integer id, List<? extends UnivKeyBean> addList) {

        /* 登録済みの受験予定大学リストをDBから取得する */
        List<UnivKeyBean> registedList = dao.selectExamPlanUnivList(id);

        List<ExmntnMainExamPlanUnivDaoInBean> inBeanList = new ArrayList<>(addList.size());
        for (UnivKeyBean bean : addList) {

            /* 登録済みチェックを行う */
            checkDuplication(registedList, bean);

            /* DAO入力Beanに変換する */
            ExmntnMainExamPlanUnivDaoInBean inBean = new ExmntnMainExamPlanUnivDaoInBean(bean);
            inBean.setId(id);
            inBean.setYear(systemDto.getUnivYear());
            inBean.setExamDiv(systemDto.getUnivExamDiv());
            inBeanList.add(inBean);
        }

        /* 登録上限数チェックを行う */
        checkMaxPlanCount(registedList, addList);

        /* 受験予定大学を登録する */
        dao.insertExamPlanUniv(inBeanList);

        /* 入試日が重複しているかを調べる */
        return isPlanConflict(registedList, addList);
    }

    /**
     * （生徒用）受験予定大学を追加します。
     *
     * @param addList 追加する受験予定大学リスト
     * @return 入試日が重複していたらtrue
     */
    public boolean addStudentExamPlanUniv(List<? extends UnivKeyBean> addList) {

        /* 登録済みの受験予定大学リストをDTOから取得する */
        List<StudentExamPlanUnivBean> registedList = dto.getStudentExamPlanUnivList();
        if (registedList == null) {
            registedList = new ArrayList<>(addList.size());
            dto.setStudentExamPlanUnivList(registedList);
        }

        for (UnivKeyBean bean : addList) {
            /* 登録済みチェックを行う */
            checkDuplication(registedList, bean);
        }

        /* 登録上限数チェックを行う */
        checkMaxPlanCount(registedList, addList);

        /* 入試日が重複しているかを調べる */
        boolean conflictFlg = isPlanConflict(registedList, addList);

        /* 受験予定大学を登録する */
        for (UnivKeyBean bean : addList) {
            registedList.add(new StudentExamPlanUnivBean(bean));
        }

        return conflictFlg;
    }

    /**
     * 登録済みチェックを行います。
     *
     * @param registedList 登録済みの受験予定大学リスト
     * @param bean 追加する受験予定大学
     */
    private void checkDuplication(List<? extends UnivKeyBean> registedList, UnivKeyBean bean) {
        if (registedList.indexOf(bean) > -1) {
            throw new MessageDialogException(getMessage("error.exmntn.main.C_E014"));
        }
    }

    /**
     * 登録上限数チェックを行います。
     *
     * @param registedList 登録済みの受験予定大学リスト
     * @param addList 追加する受験予定大学リスト
     */
    private void checkMaxPlanCount(List<? extends UnivKeyBean> registedList, List<? extends UnivKeyBean> addList) {
        if (registedList.size() + addList.size() > MAX_PLAN_COUNT) {
            throw new MessageDialogException(getMessage("error.exmntn.main.C_E015", MAX_PLAN_COUNT));
        }
    }

    /**
     * 入試日が重複しているかを調べます。
     *
     * @param registedList 登録済みの受験予定大学リスト
     * @param addList 追加する受験予定大学リスト
     * @return 入試日が重複していたらtrue
     */
    private boolean isPlanConflict(List<? extends UnivKeyBean> registedList, List<? extends UnivKeyBean> addList) {

        /* 登録済みの受験予定大学の入試日セットを生成する */
        final Set<String> registedSet = new HashSet<>(registedList.size() * 3);
        for (UnivKeyBean key : registedList) {
            if (ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), key.getUnivCd())) {
                dao.selectInpleDateList(key, new IterationCallback<String, Boolean>() {
                    @Override
                    public Boolean iterate(String entity, IterationContext context) {
                        registedSet.add(entity);
                        return Boolean.FALSE;
                    }
                });
            }
        }

        /* 追加する受験予定大学と入試日が重複していないか調べる */
        for (UnivKeyBean key : addList) {
            if (ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), key.getUnivCd())) {
                Boolean result = dao.selectInpleDateList(key, new IterationCallback<String, Boolean>() {
                    @Override
                    public Boolean iterate(String entity, IterationContext context) {
                        if (!registedSet.add(entity)) {
                            context.setExit(true);
                            return Boolean.TRUE;
                        }
                        return Boolean.FALSE;
                    }
                });
                if (BooleanUtils.isTrue(result)) {
                    return true;
                }
            }
        }

        return false;
    }

}

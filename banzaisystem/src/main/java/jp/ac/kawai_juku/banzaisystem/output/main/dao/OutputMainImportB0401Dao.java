package jp.ac.kawai_juku.banzaisystem.output.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 志望大学・評価一覧CSV取り込みのDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainImportB0401Dao extends BaseDao {

    /**
     * インポート可能な模試リストを取得します。
     *
     * @return 模試リスト
     */
    public List<ExamBean> selectExamList() {
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath()).getResultList();
    }

    /**
     * 指定した個人IDの学籍基本情報のレコード数を取得します。
     *
     * @param id 個人ID
     * @return 学籍基本情報のレコード数
     */
    public long countId(Integer id) {
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), id).getSingleResult().longValue();
    }

    /**
     * 指定した大学キーを持つ大学マスタ基本情報のレコード数を取得します。
     *
     * @param bean {@link UnivKeyBean}
     * @return 大学マスタ基本情報のレコード数
     */
    public long countUniv(UnivKeyBean bean) {
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), bean).getSingleResult().longValue();
    }

    /**
     * 指定した大学キーを持つ大学マスタ基本情報のレコード数を取得します。
     *
     * @param univCd 大学コード(5桁)
     * @return 大学マスタ基本情報のレコード数
     */
    public long countUniv5(String univCd) {
        BeanMap param = new BeanMap();
        param.put("univCd", univCd);
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult().longValue();
    }

    /**
     * 指定したキーを持つ志望大学リストのレコード数を取得します。
     *
     * @param id 個人ID
     * @param year 模試年度
     * @param examCd 模試コード
     * @return 志望大学リストのレコード数
     */
    public Integer countCandidateUnivRow(Integer id, String year, String examCd) {
        BeanMap param = new BeanMap();
        param.put("individualId", id);
        param.put("examYear", year);
        param.put("examCd", examCd);
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getSingleResult().intValue();
    }

    /**
     * 指定したキーを持つ志望大学リストのレコード数を取得します。
     *
     * @param id 個人ID
     * @param year 模試年度
     * @param examCd 模試コード
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @param deptCd 学科コード
     * @return 志望大学リストのレコード数
     */
    public Integer countCandidateUniv(Integer id, String year, String examCd, String univCd, String facultyCd, String deptCd) {
        BeanMap param = new BeanMap();
        param.put("individualId", id);
        param.put("examYear", year);
        param.put("examCd", examCd);
        param.put("univCd", univCd);
        param.put("facultyCd", facultyCd);
        param.put("deptCd", deptCd);
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getSingleResult().intValue();
    }

    /**
     * 指定した大学キーを持つ大学マスタ基本情報のレコード数を取得します。
     *
     * @param univCd 大学コード(5桁)
     * @return 大学コード(10桁)
     */
    public String selectUniv(String univCd) {
        BeanMap param = new BeanMap();
        param.put("univCd", univCd);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getSingleResult().toString();
    }
}

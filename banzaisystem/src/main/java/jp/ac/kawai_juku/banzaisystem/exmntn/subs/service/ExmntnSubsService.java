package jp.ac.kawai_juku.banzaisystem.exmntn.subs.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsSearchResultBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.dao.ExmntnSubsUnivSearchDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索画面のサービスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsService {

    /** ExecutorService */
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /** DAO */
    @Resource(name = "exmntnSubsUnivSearchDao")
    private ExmntnSubsUnivSearchDao dao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** View */
    @Resource(name = "exmntnSubsView")
    private ExmntnSubsView view;

    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /**
     * 大学を検索します。
     *
     * @param searchBean 検索条件Bean
     * @return 検索結果リスト
     */
    public List<Map<String, CellData>> searchUniv(ExmntnSubsUnivSearchInBean searchBean) {

        /* DBから検索する */
        List<ExmntnSubsSearchResultBean> resultList = dao.selectUnivList(searchBean);
        if (resultList.isEmpty()) {
            return Collections.emptyList();
        }

        /* 科目検索ロジックを初期化する */
        CenterSubjectSearch centerSearch = new CenterSubjectSearch(searchBean);
        SecondSubjectSearch secondSearch = new SecondSubjectSearch(searchBean);

        /* タスクを生成する */
        List<JudgementTask> tasks = CollectionsUtil.newArrayList(resultList.size());
        /*boolean noExclude = view.englishJoinningCheckBox.isSelected() && view.englishJoinningCheckBox.isVisible();*/

        for (ExmntnSubsSearchResultBean resultBean : resultList) {
            /* tasks.add(new JudgementTask(searchBean, centerSearch, secondSearch, resultBean, systemDto, false, scoreHelper)); */
            tasks.add(new JudgementTask(searchBean, centerSearch, secondSearch, resultBean, systemDto));
        }

        /* 全タスクを実行する */
        List<Map<String, CellData>> dataList = CollectionsUtil.newArrayList(resultList.size());
        try {
            for (Future<Map<String, CellData>> future : EXECUTOR.invokeAll(tasks)) {
                Map<String, CellData> map = future.get();
                if (map != null) {
                    dataList.add(map);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return dataList;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * ヘッダ部分でのソートをサポートするScrollPaneです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class SortableScrollPane extends TableScrollPane {

    /** ソートキー */
    private String sortKey;

    /** 昇順フラグ */
    private boolean isAscending;

    /** 選択中のボタンヘッダ */
    private ButtonHeader selectedButton;

    /**
     * コンストラクタです。
     *
     * @param vsbPolicy 縦スクロールバーのポリシー
     * @param hsbPolicy 横スクロールバーのポリシー
     * @param sortKey ソートキーの初期値
     */
    public SortableScrollPane(int vsbPolicy, int hsbPolicy, String sortKey) {
        super(vsbPolicy, hsbPolicy);
        this.sortKey = sortKey;
        this.isAscending = true;
    }

    /**
     * 指定したソートキーと昇順フラグで、テーブルの内容をソートします。
     *
     * @param sortKey ソートキー
     * @param isAscending 昇順フラグ
     */
    protected abstract void sort(String sortKey, boolean isAscending);

    /**
     * ヘッダボタンを生成します。
     *
     * @param name ヘッダ名
     * @param alignment アライメント設定
     * @param isDrawSeparator 区切り線描画フラグ
     * @return ボタン
     */
    protected final ButtonHeader createHeaderButton(String name, CellAlignment alignment, boolean isDrawSeparator) {
        return new ButtonHeader(name, alignment, isDrawSeparator);
    }

    /**
     * ヘッダボタンを生成します。
     *
     * @param name ヘッダ名
     * @param alignment アライメント設定
     * @return ボタン
     */
    protected final ButtonHeader createHeaderButton(String name, CellAlignment alignment) {
        return createHeaderButton(name, alignment, true);
    }

    /**
     * ソートキーを返します。
     *
     * @return ソートキー
     */
    public final String getSortKey() {
        return sortKey;
    }

    /**
     * 昇順フラグを返します。
     *
     * @return 昇順フラグ
     */
    public final boolean isAscending() {
        return isAscending;
    }

    /** ヘッダ定義インターフェース */
    protected interface Header {

        /**
         * アライメント設定を返します。
         *
         * @return アライメント設定
         */
        CellAlignment getAlignment();

        /**
         * 区切り線描画フラグを返します。
         *
         * @return 区切り線描画フラグ
         */
        boolean isDrawSeparator();

    }

    /** ヘッダボタン定義 */
    protected final class ButtonHeader extends JButton implements Header {

        /** アライメント設定 */
        private final CellAlignment alignment;

        /** 区切り線描画フラグ */
        private final boolean isDrawSeparator;

        /** デフォルトアイコン */
        private final Icon defaultIcon;

        /** 昇順アイコン */
        private final Icon ascIcon;

        /** 降順アイコン */
        private final Icon descIcon;

        /**
         * コンストラクタです。
         *
         * @param name ヘッダ名
         * @param alignment アライメント設定
         * @param isDrawSeparator 区切り線描画フラグ
         */
        private ButtonHeader(final String name, CellAlignment alignment, boolean isDrawSeparator) {

            this.alignment = alignment;
            this.isDrawSeparator = isDrawSeparator;

            /* 偏差値と得点は共通の画像を利用する */
            String imageName;
            if (name.endsWith("Dev")) {
                imageName = "dev";
            } else if (name.endsWith("Score")) {
                imageName = "score";
            } else {
                imageName = name;
            }

            Class<?> clazz = SortableScrollPane.this.getClass();
            BufferedImage defaultImg = ImageUtil.readImage(clazz, "table/" + imageName + ".png");
            BufferedImage ascImg = ImageUtil.readImage(clazz, "table/" + imageName + "_a.png");
            BufferedImage descImg = ImageUtil.readImage(clazz, "table/" + imageName + "_d.png");

            this.defaultIcon = new ImageIcon(defaultImg);
            this.ascIcon = new ImageIcon(ascImg);
            this.descIcon = new ImageIcon(descImg);

            this.setContentAreaFilled(false);
            /* ↓枠線が邪魔な場合は消す */
            this.setBorderPainted(false);

            setName(name);
            setFocusable(false);
            setBorder(null);
            setFocusPainted(false);
            setSize(defaultImg.getWidth(), defaultImg.getHeight());
            resetIcon();
            addMouseListener(ImageButton.CURSOR_MOUSE_LISTENER);
            addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (name.equals(sortKey)) {
                        /* ソートキーを変更しなかった場合はソート順を反転する */
                        isAscending = !isAscending;
                    } else {
                        /* ソートキーを変更した場合は昇順にする */
                        isAscending = true;
                    }
                    sortKey = name;
                    sort(sortKey, isAscending);
                    if (selectedButton != null) {
                        selectedButton.resetIcon();
                    }
                    resetIcon();
                }
            });
        }

        /**
         * アイコンをリセットします。
         */
        private void resetIcon() {
            if (getName().equals(sortKey)) {
                selectedButton = this;
                setIcon(isAscending ? ascIcon : descIcon);
            } else {
                setIcon(defaultIcon);
            }
        }

        @Override
        public CellAlignment getAlignment() {
            return alignment;
        }

        @Override
        public boolean isDrawSeparator() {
            return isDrawSeparator;
        }

    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.helper;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LISTENING;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WRITING;

import java.awt.Color;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectComboBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.component.ExmntnMainSubjectTextField;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.co.fj.kawaijuku.judgement.Constants.English;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.StringUtil;

/**
 *
 * 個人成績画面の入力チェックヘルパーです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainCheckHelper {

    /** エラー箇所カラー */
    public static final Color ERROR_COLOR = new Color(255, 153, 153);

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView view;

    /** 大学検索画面のView */
    @Resource
    private ExmntnSubsView exmntnSubsView;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /**
     * マーク模試の入力チェックを行います。
     */
    public void checkMark() {

        /* テキストフィールド初期化 */
        initField(true, false);

        /* コンボボックス初期化 */
        initCombo(true, false);

        /* テキスト・コンボボックスチェック */
        inputCheck(true);

        /* 第1解答科目未選択チェック */
        checkFirstAnswerSubject();

        /* 理科基礎科目チェック */
        checkSciBasicSubject();

        /* コンボボックス重複チェック */
        checkComboDupli(true);

        String errorMessage = findFirstErrorMessage(true);
        if (errorMessage == null) {
            /* エラーなし */
            initInputAreaMark(false);
        } else {
            /* エラーメッセージを表示する */
            showMarkErrorMessage(errorMessage);
        }
    }

    /**
     * 第1解答科目未選択チェックを行います。
     */
    private void checkFirstAnswerSubject() {
        ExamBean exam = view.exmntnMarkComboBox.getSelectedValue();
        if (exam != null && ExamUtil.isAns1st(exam)) {
            checkFirstAnswerSubject(view.science1Field, view.science2Field);
            checkFirstAnswerSubject(view.society1Field, view.society2Field);
        }
    }

    /**
     * 第1解答科目未選択チェックを行います。
     *
     * @param field1 第1解答テキストフィールド
     * @param field2 第2解答テキストフィールド
     */
    private void checkFirstAnswerSubject(ExmntnMainSubjectTextField field1, ExmntnMainSubjectTextField field2) {
        if (field1.getText().isEmpty() && field1.getSubjectBean() == null && isValid(field2)) {
            field1.getPairComboBox().setErrorMessage(getMessage("error.exmntn.main.C_E012"));
        }
    }

    /**
     * 理科基礎科目チェックを行います。
     */
    private void checkSciBasicSubject() {
        if (isValid(view.science1Field) && isValid(view.science2Field)) {
            boolean errorFlg = false;
            if (isValid(view.science5Field)) {
                errorFlg = true;
                view.science5Field.getPairComboBox().setErrorMessage(getMessage("error.exmntn.main.C_E013"));
            }
            if (isValid(view.science6Field)) {
                errorFlg = true;
                view.science6Field.getPairComboBox().setErrorMessage(getMessage("error.exmntn.main.C_E013"));
            }
            if (errorFlg) {
                view.science1Field.getPairComboBox().setErrorMessage(getMessage("error.exmntn.main.C_E013"));
                view.science2Field.getPairComboBox().setErrorMessage(getMessage("error.exmntn.main.C_E013"));
            }
        }
    }

    /**
     * コンポーネントに設定されている最初のエラーメッセージを返します。
     *
     * @param isMark マーク模試フラグ
     * @return コンポーネントに設定されている最初のエラーメッセージ
     */
    private String findFirstErrorMessage(boolean isMark) {
        for (ExmntnMainSubjectTextField field : view.findComponentsByClass(ExmntnMainSubjectTextField.class)) {
            if (field.isMark() == isMark) {
                if (field.getErrorMessage() != null) {
                    return field.getErrorMessage();
                } else if (field.getPairComboBox() != null && field.getPairComboBox().getErrorMessage() != null) {
                    return field.getPairComboBox().getErrorMessage();
                }
            }
        }
        return null;
    }

    /**
     * 記述模試の入力チェックを行います。
     */
    public void checkWritten() {

        /* テキストフィールド初期化 */
        initField(false, false);

        /* コンボボックス初期化 */
        initCombo(false, false);

        /* テキスト・コンボボックスチェック */
        inputCheck(false);

        /* コンボボックス重複チェック */
        checkComboDupli(false);

        /* 記述理科コンボボックス選択数上限チェック */
        checkComboWrittenSci();

        String errorMessage = findFirstErrorMessage(false);
        if (errorMessage == null) {
            /* エラーなし */
            initInputAreaWritten(false);
        } else {
            /* エラーメッセージを表示する */
            showWrittenErrorMessage(errorMessage);
        }
    }

    /**
     * 入力チェックを行います。
     *
     * @param isMark マーク模試フラグ
     */
    private void inputCheck(boolean isMark) {
        for (ExmntnMainSubjectTextField field : view.findComponentsByClass(ExmntnMainSubjectTextField.class)) {
            if (field.isMark() == isMark) {
                inputCheckDetail(field);
            }
        }
    }

    /**
     * テキストフィールド、コンボボックスの入力チェックを行います。
     *
     * @param field テキストフィールド
     */
    private void inputCheckDetail(ExmntnMainSubjectTextField field) {

        BZSubjectRecord subject = field.getSubjectBean();
        String score = field.getText();

        if (field.isMark()) {
            /* マーク模試：科目満点値を超えていないかチェックする */
            if (subject != null && !StringUtil.isEmpty(score)) {
                int fullScore = getMarkFullScore(subject.getSubCd());
                if (Integer.parseInt(score) > fullScore) {
                    field.setErrorMessage(getMessage("error.exmntn.main.C_E002", Integer.toString(fullScore)));
                }
            }
        } else {
            /* 記述模試：偏差値の範囲をチェックする */
            if (!StringUtil.isEmpty(score) && !ValidateUtil.isInRange(score, 0, 999.9)) {
                field.setErrorMessage(getMessage("error.exmntn.main.C_E006"));
            }
        }

        /* テキスト・コンボ相関チェックを行う */
        ExmntnMainSubjectComboBox combo = field.getPairComboBox();
        if (combo != null) {
            if (combo.getSelectedValue() == null && !StringUtil.isEmpty(score)) {
                combo.setErrorMessage(getMessage("error.exmntn.main.C_E003"));
            }
            if (combo.getSelectedValue() != null && StringUtil.isEmpty(score)) {
                field.setErrorMessage(getMessage(field.isMark() ? "error.exmntn.main.C_E004" : "error.exmntn.main.C_E011"));
            }
        }
    }

    /**
     * コンボボックスの重複チェックを行います。
     *
     * @param isMark マーク模試フラグ
     */
    private void checkComboDupli(boolean isMark) {
        if (isMark) {
            checkComboDupli(isMark, view.science1ComboBox, view.science2ComboBox);
            checkComboDupli(isMark, view.science5ComboBox, view.science6ComboBox);
            checkComboDupli(isMark, view.society1ComboBox, view.society2ComboBox);
        } else {
            checkComboDupli(isMark, view.science3ComboBox, view.science4ComboBox);
            checkComboDupli(isMark, view.science7ComboBox, view.science8ComboBox);
            checkComboDupli(isMark, view.society3ComboBox, view.society4ComboBox);
        }
    }

    /**
     * コンボボックスの重複チェックを行います。
     *
     * @param isMark マーク模試フラグ
     * @param combo1 第1項目
     * @param combo2 第2項目
     */
    private void checkComboDupli(boolean isMark, ExmntnMainSubjectComboBox combo1, ExmntnMainSubjectComboBox combo2) {
        BZSubjectRecord subject1 = combo1.getSelectedValue();
        BZSubjectRecord subject2 = combo2.getSelectedValue();
        if (subject1 != null && subject2 != null) {
            if (subject1.getSubCd().equals(subject2.getSubCd())) {
                String errorMessage = getMessage("error.exmntn.main.C_E005");
                combo1.setErrorMessage(errorMessage);
                combo2.setErrorMessage(errorMessage);
            }
        }
    }

    /**
     * 記述理科コンボボックス選択数上限チェックを行います。
     */
    private void checkComboWrittenSci() {
        BZSubjectRecord subject3 = view.science3ComboBox.getSelectedValue();
        BZSubjectRecord subject4 = view.science4ComboBox.getSelectedValue();
        BZSubjectRecord subject7 = view.science7ComboBox.getSelectedValue();
        BZSubjectRecord subject8 = view.science8ComboBox.getSelectedValue();
        if (countNotNull(subject3, subject4, subject7, subject8) > 2) {
            String errorMessage = getMessage("error.exmntn.main.C_E013");
            if (subject3 != null) {
                view.science3ComboBox.setErrorMessage(errorMessage);
            }
            if (subject4 != null) {
                view.science4ComboBox.setErrorMessage(errorMessage);
            }
            if (subject7 != null) {
                view.science7ComboBox.setErrorMessage(errorMessage);
            }
            if (subject8 != null) {
                view.science8ComboBox.setErrorMessage(errorMessage);
            }
        }
    }

    /**
     * NULLではないオブジェクトの数をカウントします。
     *
     * @param objects オブジェクト配列
     * @return NULLではないオブジェクトの数
     */
    private int countNotNull(Object... objects) {
        int count = 0;
        for (Object o : objects) {
            if (o != null) {
                count++;
            }
        }
        return count;
    }

    /**
     * マーク模試のエラーメッセージを表示します。
     *
     * @param errorMessage エラーメッセージ
     */
    public void showMarkErrorMessage(String errorMessage) {
        view.errorMessageChar1Label.setText(errorMessage);
        view.errorMessageChar1Label.setVisible(true);
        view.reJudgeButton.setEnabled(false);
        view.saveRecordButton.setEnabled(false);
        if (exmntnSubsView.isInitialized()) {
            exmntnSubsView.startSearchLargeButton.setEnabled(false);
        }
    }

    /**
     * 記述模試のエラーメッセージを表示します。
     *
     * @param errorMessage エラーメッセージ
     */
    public void showWrittenErrorMessage(String errorMessage) {
        view.errorMessageChar2Label.setText(errorMessage);
        view.errorMessageChar2Label.setVisible(true);
        view.reJudgeButton.setEnabled(false);
        view.saveRecordButton.setEnabled(false);
        if (exmntnSubsView.isInitialized()) {
            exmntnSubsView.startSearchLargeButton.setEnabled(false);
        }
    }

    /**
     * 入力チェックが必要であるかを判定します。
     *
     * @param isMark マーク模試フラグ
     * @return 入力チェックが必要ならtrue
     */
    public boolean needCheck(boolean isMark) {
        if (isMark) {
            return view.exmntnMarkComboBox.getSelectedValue() != null;
        } else {
            return view.exmntnWritingComboBox.getSelectedValue() != null;
        }
    }

    /**
     * マーク模試の科目満点値を返します。
     *
     * @param subCd 科目コード
     * @return 科目満点値
     */
    public int getMarkFullScore(String subCd) {
        switch (subCd) {
            case SUBCODE_CENTER_DEFAULT_WRITING:
                /* 英語ライティング */
                /* return (int) Detail.English.PERFECT_SCORE_WRITING; */
                return (int) English.PERFECT_SCORE_WRITING;
            case SUBCODE_CENTER_DEFAULT_LISTENING:
                /* 英語リスニング */
                /* return (int) Detail.English.PERFECT_SCORE_LISTENING; */
                return (int) English.PERFECT_SCORE_LISTENING;
            case SUBCODE_CENTER_DEFAULT_OLD:
                /* 古文 */
                return (int) Detail.Japanese.PERFECT_SCORE_JCLASSICS;
            case SUBCODE_CENTER_DEFAULT_CHINESE:
                /* 漢文 */
                return (int) Detail.Japanese.PERFECT_SCORE_JCHINESE;
            case SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC:
            case SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC:
            case SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC:
            case SUBCODE_CENTER_DEFAULT_EARTH_BASIC:
                /* 理科基礎 */
                return (int) Detail.Science.PERFECT_SCORE_BASIC;
            default:
                /* その他は全部100点 */
                return 100;
        }
    }

    /**
     * マーク模試の入力エリアを初期化します。
     *
     * @param clearValueFlg 値初期化フラグ
     */
    void initInputAreaMark(boolean clearValueFlg) {

        initField(true, clearValueFlg);
        initCombo(true, clearValueFlg);

        view.errorMessageChar1Label.setText(StringUtils.EMPTY);
        view.errorMessageChar1Label.setVisible(false);

        if (view.errorMessageChar2Label.isVisible()) {
            view.reJudgeButton.setEnabled(false);
            view.saveRecordButton.setEnabled(false);
            if (exmntnSubsView.isInitialized()) {
                exmntnSubsView.startSearchLargeButton.setEnabled(false);
            }
        } else {
            view.reJudgeButton.setEnabled(true);
            view.saveRecordButton.setEnabled(systemDto.getTargetIndividualId() != null);
            if (exmntnSubsView.isInitialized()) {
                exmntnSubsView.startSearchLargeButton.setEnabled(true);
            }
        }

        /* 第1,第2入れ替えボタンの表示設定を行う */
        changeFirstSecondButton();

        /* CEFR,国語記述の初期化を行う */
        initCefrJpn(clearValueFlg);

    }

    /**
     * CEFR,国語記述データをクリアします。
     *
     * @param clearValueFlg 値初期化フラグ
     */
    private void initCefrJpn(boolean clearValueFlg) {
        /* List<ExmntnMainCefrRecordBean> cefrList = null; */
        /* cefrList = CollectionsUtil.newArrayList(); */
        /* List<ExmntnMainJpnRecordBean> jpnList = null; */
        /* jpnList = systemDto.getJpnInfo(); */
        if (clearValueFlg) {
            /*TextField field = view.cefr1Field;
            field.setText(StringUtils.EMPTY);
            TextField field2 = view.cefr2Field;
            field2.setText(StringUtils.EMPTY);
            TextField field3 = view.cefrLvl1Field;
            field3.setText(StringUtils.EMPTY);
            TextField field4 = view.cefrLvl2Field;
            field4.setText(StringUtils.EMPTY);
            TextField field5 = view.jpnWritingField;
            field5.setText(StringUtils.EMPTY);
            ExmntnMainCefrRecordBean cefrdata = new ExmntnMainCefrRecordBean();
            cefrList = CollectionsUtil.newArrayList();
            cefrList.add(cefrdata);
            cefrList.get(0).setIndividualId(0);
            cefrList.get(0).setExamYear(StringUtils.EMPTY);
            cefrList.get(0).setExamDiv(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamCd1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamCd2(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamName1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamName2(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamNameAbbr1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamNameAbbr2(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevel1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevel2(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevelName1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevelName2(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevelNameAbbr1(StringUtils.EMPTY);
            cefrList.get(0).setCefrExamLevelNameAbbr2(StringUtils.EMPTY);
            cefrList.get(0).setCefrScore1(0.0);
            cefrList.get(0).setCefrScore2(0.0);
            cefrList.get(0).setCefrLevelCd1(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevelCd2(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevel1(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevel2(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevelAbbr1(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevelAbbr2(StringUtils.EMPTY);
            cefrList.get(0).setScoreCorrectFlg1(StringUtils.EMPTY);
            cefrList.get(0).setScoreCorrectFlg2(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevelCdCorrectFlg1(StringUtils.EMPTY);
            cefrList.get(0).setCefrLevelCdCorrectFlg2(StringUtils.EMPTY);
            cefrList.get(0).setExamResult1(StringUtils.EMPTY);
            cefrList.get(0).setExamResult2(StringUtils.EMPTY);
            cefrList.get(0).setExamResultCorrectFlg1(StringUtils.EMPTY);
            cefrList.get(0).setExamResultCorrectFlg2(StringUtils.EMPTY);
            cefrList.get(0).setUnknownScore1(StringUtils.EMPTY);
            cefrList.get(0).setUnknownScore2(StringUtils.EMPTY);
            cefrList.get(0).setUnknownCefr1(StringUtils.EMPTY);
            cefrList.get(0).setUnknownCefr2(StringUtils.EMPTY);
            systemDto.setCefrInfo(cefrList);
            jpnList = CollectionsUtil.newArrayList();
            ExmntnMainJpnRecordBean jpndata = new ExmntnMainJpnRecordBean();
            jpnList.add(jpndata);
            jpnList.get(0).setIndividualId(0);
            jpnList.get(0).setExamYear(StringUtils.EMPTY);
            jpnList.get(0).setExamDiv(StringUtils.EMPTY);
            jpnList.get(0).setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
            jpnList.get(0).setQuestion1(StringUtils.EMPTY);
            jpnList.get(0).setQuestion2(StringUtils.EMPTY);
            jpnList.get(0).setQuestion3(StringUtils.EMPTY);
            jpnList.get(0).setJpnTotal(StringUtils.EMPTY);
            systemDto.setJpnInfo(jpnList);
            */
        }
        return;
    }

    /**
     * 記述模試の入力エリアを初期化します。
     *
     * @param clearValueFlg 値初期化フラグ
     */
    void initInputAreaWritten(boolean clearValueFlg) {

        initField(false, clearValueFlg);
        initCombo(false, clearValueFlg);

        if (clearValueFlg) {
            /*
             * view.english2Fieldが保持している科目情報が必ず初期化されるように、
             * setSelectedSilentメソッドではなくsetSelectedメソッドを利用する。
             */
            view.listeningCheckBox.setSelected(false);
        }

        view.errorMessageChar2Label.setText(StringUtils.EMPTY);
        view.errorMessageChar2Label.setVisible(false);

        if (view.errorMessageChar1Label.isVisible()) {
            view.reJudgeButton.setEnabled(false);
            view.saveRecordButton.setEnabled(false);
            if (exmntnSubsView.isInitialized()) {
                exmntnSubsView.startSearchLargeButton.setEnabled(false);
            }
        } else {
            view.reJudgeButton.setEnabled(true);
            view.saveRecordButton.setEnabled(systemDto.getTargetIndividualId() != null);
            if (exmntnSubsView.isInitialized()) {
                exmntnSubsView.startSearchLargeButton.setEnabled(true);
            }
        }
    }

    /**
     * 第1,第2入れ替えボタンの状態を更新します。
     */
    public void changeFirstSecondButton() {

        ExamBean exam = view.exmntnMarkComboBox.getSelectedValue();
        if (exam == null) {
            view.changeFirstSecondOfSienceButton.setEnabled(false);
            view.changeFirstSecondOfSocialButton.setEnabled(false);
            return;
        }

        boolean isAns1st = ExamUtil.isAns1st(exam);

        /* 第1,第2入れ替えボタン(理科) */
        if (isAns1st && isValid(view.science1Field) && isValid(view.science2Field)) {
            view.changeFirstSecondOfSienceButton.setEnabled(true);
        } else {
            view.changeFirstSecondOfSienceButton.setEnabled(false);
        }

        /* 第1,第2入れ替えボタン(社会) */
        if (isAns1st && isValid(view.society1Field) && isValid(view.society2Field)) {
            view.changeFirstSecondOfSocialButton.setEnabled(true);
        } else {
            view.changeFirstSecondOfSocialButton.setEnabled(false);
        }
    }

    /**
     * テキストフィールドに得点の入力があるかどうかを判定します。<br>
     * 科目コンボボックスとペアの場合は、科目の選択状態も判定します。
     *
     * @param field {@link ExmntnMainSubjectTextField}
     * @return テキストフィールドに得点の入力があるならtrue
     */
    private boolean isValid(ExmntnMainSubjectTextField field) {
        return !field.getText().isEmpty() && field.getSubjectBean() != null;
    }

    /**
     * テキストフィールドを初期化します。
     *
     * @param isMark マーク模試フラグ
     * @param clearValueFlg 値初期化フラグ
     */
    private void initField(boolean isMark, boolean clearValueFlg) {
        for (ExmntnMainSubjectTextField field : view.findComponentsByClass(ExmntnMainSubjectTextField.class)) {
            if (field.isMark() == isMark) {
                field.setErrorMessage(null);
                if (clearValueFlg) {
                    field.setText(StringUtils.EMPTY);
                }
            }
        }
    }

    /**
     * コンボボックスを初期化します。
     *
     * @param isMark マーク模試フラグ
     * @param clearValueFlg 値初期化フラグ
     */
    private void initCombo(boolean isMark, boolean clearValueFlg) {
        for (ExmntnMainSubjectComboBox combo : view.findComponentsByClass(ExmntnMainSubjectComboBox.class)) {
            if (combo.getPairTextField().isMark() == isMark) {
                combo.setErrorMessage(null);
                if (clearValueFlg && combo.getItemCount() > 0) {
                    combo.setSelectedIndex(0);
                }
            }
        }
    }

    /**
     * 初期値に戻すボタンを有効化します。
     *
     * @param isMark マーク模試フラグ
     */
    public void enableReturnInitButton(boolean isMark) {

        view.returnInitDataPinkButton.setEnabled(true);

        /* 変更フラグを立てる */
        if (systemDto.getTargetIndividualId() != null) {
            if (isMark) {
                dto.setChangeMarkScore(true);
                /* dto.setChangeMarkCefrScore(true); */
                dto.setChangeMarkJpnScore(true);
            } else {
                dto.setChangeWrittenScore(true);
            }
        }
    }

}

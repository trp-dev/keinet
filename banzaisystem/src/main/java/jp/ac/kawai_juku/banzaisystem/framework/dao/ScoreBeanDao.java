package jp.ac.kawai_juku.banzaisystem.framework.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCefrRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainJpnRecordBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 判定用成績データに関するDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ScoreBeanDao extends BaseDao {

    /**
     * 対象模試の科目成績リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 科目成績リスト
     */
    public List<BZSubjectRecord> selectSubRecordListByGradeClass(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(BZSubjectRecord.class, getSqlPath(), param).getResultList();
    }

    /**
     * ドッキング対象模試の科目成績リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 科目成績リスト
     */
    public List<BZSubjectRecord> selectSubRecordListByGradeClassDoc(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(BZSubjectRecord.class, getSqlPath(), param).getResultList();
    }

    /**
     * CEFRスコアを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return CEFRスコア
     */
    public List<ExmntnMainCefrRecordBean> selectCefrRecordList(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(ExmntnMainCefrRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 国語記述スコアを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 国語記述スコア
     */
    public List<ExmntnMainJpnRecordBean> selectJpnRecordList(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(ExmntnMainJpnRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 対象模試の科目成績リストを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 科目成績リスト
     */
    public List<BZSubjectRecord> selectSubRecordList(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(BZSubjectRecord.class, getSqlPath(), param).getResultList();
    }

    /**
     * CEFRスコアを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return CEFRスコア
     */
    public ExmntnMainCefrRecordBean selectCefrRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(ExmntnMainCefrRecordBean.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 国語記述スコアを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 国語記述スコア
     */
    public ExmntnMainJpnRecordBean selectJpnRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(ExmntnMainJpnRecordBean.class, getSqlPath(), param).getSingleResult();
    }
}

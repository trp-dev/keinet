package jp.ac.kawai_juku.banzaisystem.result.univ.service;

import java.util.List;
import java.util.Vector;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.util.ExmntnRsltUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivExamineeBean;
import jp.ac.kawai_juku.banzaisystem.result.univ.dao.ResultUnivDao;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 入試結果入力-大学指定(一覧)画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultUnivService {

    /** DAO */
    @Resource
    private ResultUnivDao resultUnivDao;

    /**
     * 指定した大学名の大学一覧テーブル用の大学リストを取得します。
     *
     * @param univName 大学名
     * @return 大学リスト
     */
    public List<ResultUnivBean> findUnivList(String univName) {
        List<ResultUnivBean> list = resultUnivDao.selectUnivList(JpnStringUtil.normalize(StringUtils.substring(JpnStringUtil.hira2Hkana(univName), 0, 12)));
        for (ResultUnivBean bean : list) {
            bean.setUnivDiv(ExmntnRsltUtil.toUnivDiv(bean.getUnivCd()));
        }
        return list;
    }

    /**
     * 受験者一覧テーブル用のデータリストを取得します。
     *
     * @param univCdList 大学コードリスト
     * @return 受験者一覧テーブル用のデータリスト
     */
    public Vector<Vector<Object>> findExamineeList(List<String> univCdList) {
        return findExamineeList(univCdList, null, null, null);
    }

    /**
     * 受験者一覧テーブル用のデータリストを取得します。
     *
     * @param univCdList 大学コードリスト
     * @param sucAnnDate 合格発表日
     * @param grade 学年
     * @param cls クラス
     * @return 受験者一覧テーブル用のデータリスト
     */
    public Vector<Vector<Object>> findExamineeList(List<String> univCdList, String sucAnnDate, Integer grade, String cls) {
        Vector<Vector<Object>> list = new Vector<>();
        if (!univCdList.isEmpty()) {
            for (ResultUnivExamineeBean bean : resultUnivDao.selectExamineeList(univCdList, sucAnnDate, grade, cls)) {
                Vector<Object> row = new Vector<>(12);
                row.add(bean);
                row.add(bean.getCls());
                row.add(bean.getClassNo());
                row.add(bean.getNameKana());
                row.add(bean.getUnivCd());
                row.add(bean.getUnivName());
                row.add(bean.getFacultyName());
                row.add(bean.getDeptName());
                row.add("1");
                row.add("1");
                row.add(Boolean.FALSE);
                row.add(null);
                list.add(row);
            }
        }
        return list;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.name.view;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import jp.ac.kawai_juku.banzaisystem.exmntn.name.component.DistrictButton;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.component.PrefButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;

/**
 *
 * 大学検索（大学名称・所在地）画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnNameView extends SubView {

    /** 「大学名を入力してください。」ラベル */
    @Location(x = 55, y = 66)
    public TextLabel inputNameLabel;

    /** 入力テキスト  */
    @Location(x = 55, y = 86)
    @Size(width = 230, height = 20)
    public TextField inputField;

    /** クリアボタン */
    @Location(x = 54, y = 111)
    @Disabled
    public ImageButton clearUnivNameButton;

    /** 名称所在地クリアボタン */
    @Location(x = 497, y = 19)
    public ImageButton clearNameSeatButton;

    /** 北海道ボタン */
    @Location(x = 532, y = 66)
    @Group
    public PrefButton pref01Button;

    /** 青森ボタン */
    @Location(x = 532, y = 124)
    @Group(1)
    public PrefButton pref02Button;

    /** 秋田ボタン */
    @Location(x = 532, y = 147)
    @Group(1)
    public PrefButton pref05Button;

    /** 岩手ボタン */
    @Location(x = 578, y = 147)
    @Group(1)
    public PrefButton pref03Button;

    /** 山形ボタン */
    @Location(x = 532, y = 169)
    @Group(1)
    public PrefButton pref06Button;

    /** 宮城ボタン */
    @Location(x = 578, y = 169)
    @Group(1)
    public PrefButton pref04Button;

    /** 新潟ボタン */
    @Location(x = 487, y = 192)
    @Group(3)
    public PrefButton pref15Button;

    /** 福島ボタン */
    @Location(x = 578, y = 192)
    @Group(1)
    public PrefButton pref07Button;

    /** 群馬ボタン */
    @Group(2)
    @Location(x = 487, y = 215)
    public PrefButton pref10Button;

    /** 栃木ボタン */
    @Location(x = 532, y = 215)
    @Group(2)
    public PrefButton pref09Button;

    /** 茨城ボタン */
    @Location(x = 578, y = 215)
    @Group(2)
    public PrefButton pref08Button;

    /** 山梨ボタン */
    @Location(x = 487, y = 238)
    @Group(3)
    public PrefButton pref19Button;

    /** 埼玉ボタン */
    @Location(x = 532, y = 238)
    @Group(2)
    public PrefButton pref11Button;

    /** 千葉ボタン */
    @Location(x = 578, y = 238)
    @Group(2)
    public PrefButton pref12Button;

    /** 神奈川ボタン */
    @Location(x = 487, y = 261)
    @Group(2)
    public PrefButton pref14Button;

    /** 東京ボタン */
    @Location(x = 532, y = 261)
    @Group(2)
    public PrefButton pref13Button;

    /** 福井ボタン */
    @Location(x = 352, y = 215)
    @Group(4)
    public PrefButton pref18Button;

    /** 石川ボタン */
    @Location(x = 397, y = 215)
    @Group(4)
    public PrefButton pref17Button;

    /** 富山ボタン */
    @Location(x = 443, y = 215)
    @Group(4)
    public PrefButton pref16Button;

    /** 滋賀ボタン */
    @Location(x = 352, y = 238)
    @Group(6)
    public PrefButton pref25Button;

    /** 岐阜ボタン */
    @Location(x = 397, y = 238)
    @Group(5)
    public PrefButton pref21Button;

    /** 長野ボタン */
    @Location(x = 443, y = 238)
    @Group(3)
    public PrefButton pref20Button;

    /** 奈良ボタン */
    @Location(x = 352, y = 261)
    @Group(6)
    public PrefButton pref29Button;

    /** 愛知ボタン */
    @Location(x = 397, y = 261)
    @Group(5)
    public PrefButton pref23Button;

    /** 静岡ボタン */
    @Location(x = 443, y = 261)
    @Group(5)
    public PrefButton pref22Button;

    /** 和歌山ボタン */
    @Location(x = 352, y = 284)
    @Group(6)
    public PrefButton pref30Button;

    /** 三重ボタン */
    @Location(x = 397, y = 284)
    @Group(5)
    public PrefButton pref24Button;

    /** 京都ボタン */
    @Location(x = 307, y = 215)
    @Group(6)
    public PrefButton pref26Button;

    /** 大阪ボタン */
    @Location(x = 307, y = 238)
    @Group(6)
    public PrefButton pref27Button;

    /** 兵庫ボタン */
    @Location(x = 262, y = 215)
    @Group(6)
    public PrefButton pref28Button;

    /** 鳥取ボタン */
    @Location(x = 217, y = 215)
    @Group(7)
    public PrefButton pref31Button;

    /** 岡山ボタン */
    @Location(x = 217, y = 238)
    @Group(7)
    public PrefButton pref33Button;

    /** 島根ボタン */
    @Location(x = 172, y = 215)
    @Group(7)
    public PrefButton pref32Button;

    /** 広島ボタン */
    @Location(x = 172, y = 238)
    @Group(7)
    public PrefButton pref34Button;

    /** 山口ボタン */
    @Location(x = 127, y = 215)
    @Group(7)
    public PrefButton pref35Button;

    /** 愛媛ボタン */
    @Location(x = 172, y = 270)
    @Group(8)
    public PrefButton pref38Button;

    /** 香川ボタン */
    @Location(x = 217, y = 270)
    @Group(8)
    public PrefButton pref37Button;

    /** 高知ボタン */
    @Location(x = 172, y = 293)
    @Group(8)
    public PrefButton pref39Button;

    /** 徳島ボタン */
    @Location(x = 217, y = 293)
    @Group(8)
    public PrefButton pref36Button;

    /** 佐賀ボタン */
    @Location(x = 29, y = 197)
    @Group(9)
    public PrefButton pref41Button;

    /** 福岡ボタン */
    @Location(x = 74, y = 197)
    @Group(9)
    public PrefButton pref40Button;

    /** 長崎ボタン */
    @Location(x = 29, y = 219)
    @Group(9)
    public PrefButton pref42Button;

    /** 大分ボタン */
    @Location(x = 74, y = 219)
    @Group(9)
    public PrefButton pref44Button;

    /** 熊本ボタン */
    @Location(x = 29, y = 242)
    @Group(9)
    public PrefButton pref43Button;

    /** 宮崎ボタン */
    @Location(x = 74, y = 242)
    @Group(9)
    public PrefButton pref45Button;

    /** 鹿児島ボタン */
    @Location(x = 29, y = 265)
    @Group(9)
    public PrefButton pref46Button;

    /** 沖縄ボタン */
    @Location(x = 29, y = 293)
    @Group(9)
    public PrefButton pref47Button;

    /** 九州・沖縄ボタン */
    @Location(x = 29, y = 327)
    @Group(9)
    public DistrictButton dist10Button;

    /** 四国ボタン */
    @Location(x = 172, y = 327)
    @Group(8)
    public DistrictButton dist09Button;

    /** 中国ボタン */
    @Location(x = 217, y = 327)
    @Group(7)
    public DistrictButton dist08Button;

    /** 近畿ボタン */
    @Location(x = 307, y = 327)
    @Group(6)
    public DistrictButton dist07Button;

    /** 北陸ボタン */
    @Location(x = 352, y = 327)
    @Group(4)
    public DistrictButton dist05Button;

    /** 東海ボタン */
    @Location(x = 397, y = 327)
    @Group(5)
    public DistrictButton dist06Button;

    /** 甲信越ボタン */
    @Location(x = 442, y = 327)
    @Group(3)
    public DistrictButton dist04Button;

    /** 関東ボタン */
    @Location(x = 487, y = 327)
    @Group(2)
    public DistrictButton dist03Button;

    /** 東北ボタン */
    @Location(x = 532, y = 327)
    @Group(1)
    public DistrictButton dist02Button;

    /** 北海道ボタン */
    @Location(x = 577, y = 327)
    @Group()
    public DistrictButton dist01Button;

    @Override
    @InitMethod
    public void initialize() {

        super.initialize();

        ((AbstractDocument) inputField.getDocument()).setDocumentFilter(new DocumentFilter() {
            @Override
            public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
                super.remove(fb, offset, length);
                changeButtonStatus();
            }

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                    throws BadLocationException {
                super.replace(fb, offset, length, text, attrs);
                changeButtonStatus();
            }

            /**
             * ボタンの状態を変更します。
             */
            private void changeButtonStatus() {
                clearUnivNameButton.setEnabled(!inputField.getText().isEmpty());
            }
        });
    }

}

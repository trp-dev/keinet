package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 国語記述を保持するBeanです。
 *
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
@Getter
@Setter
@ToString
public class ExmntnMainJpnRecordBean {

    /** 個人ID */
    private Integer individualId;

    /** 模試年度 */
    private String examYear;

    /** 模試区分 */
    private String examDiv;

    /** 科目コード */
    private String subCd;

    /** 問1 */
    private String question1;

    /** 問2 */
    private String question2;

    /** 問3 */
    private String question3;

    /** 国語総合評価 */
    private String jpnTotal;

    /** 問1表示用 */
    private String dispQuestion1;

    /** 問2表示用 */
    private String dispQuestion2;

    /** 問3表示用 */
    private String dispQuestion3;

    /** 国語総合評価表示用 */
    private String dispJpnTotal;

    /** 配点 */
    private double subAllotPnt;
}

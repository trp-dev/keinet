package jp.ac.kawai_juku.banzaisystem.framework.http.response;

import java.util.List;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * アプリケーションバージョン情報の応答データBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class AppVersionResponseBean implements BaseResponseBean {

    /** バージョン */
    private String version;

    /** モジュールリスト */
    private final List<String> moduleList = CollectionsUtil.newArrayList(30);

    /**
     * コンストラクタです。
     */
    AppVersionResponseBean() {
    }

    /**
     * バージョンを返します。
     *
     * @return バージョン
     */
    public String getVersion() {
        return version;
    }

    /**
     * バージョンをセットします。
     *
     * @param version バージョン
     */
    void setVersion(String version) {
        this.version = version;
    }

    /**
     * モジュールリストを返します。
     *
     * @return モジュールリスト
     */
    public List<String> getModuleList() {
        return moduleList;
    }

}

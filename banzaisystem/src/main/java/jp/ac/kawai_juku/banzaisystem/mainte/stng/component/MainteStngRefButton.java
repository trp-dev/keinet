package jp.ac.kawai_juku.banzaisystem.mainte.stng.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.annotation.DataFolderConfig;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * メンテナンス-環境設定画面の参照ボタンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MainteStngRefButton extends ImageButton {

    /** データフォルダ */
    private DataFolder dataFolder;

    @Override
    public void initialize(Field field, AbstractView view) {
        addActionListener(new ControllerActionListener(view, StringUtils.uncapitalize(getClass().getSimpleName()
                .substring(10))));
        super.initialize(field, view);
        dataFolder = field.getAnnotation(DataFolderConfig.class).value();
    }

    /**
     * データフォルダを返します。
     *
     * @return データフォルダ
     */
    public DataFolder getDataFolder() {
        return dataFolder;
    }

}

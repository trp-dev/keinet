package jp.ac.kawai_juku.banzaisystem.mainte.impt.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 個人成績インポート画面のユーティリティクラスです。
 *
 * @author TOTEC)OZEKI.Hiroshige
 */
public final class MainteImptUtil {

    /** 状況欄の文字色：青色 */
    public static final String COLOR_BLUE = "0";

    /** 状況欄の文字色：赤色 */
    public static final String COLOR_RED = "1";

    /** 状況欄の文字色：灰色 */
    public static final String COLOR_GRAY = "2";

    /**
     * コンストラクタです。
     */
    private MainteImptUtil() {
    }

    /**
     * YYYYMMDDをYYYY/M/Dに変換します。
     *
     * @param yyyymmdd YYYYMMDD
     * @return M/D
     */
    public static String toYMD(String yyyymmdd) {
        if (StringUtils.isEmpty(yyyymmdd)) {
            return null;
        }
        String yy = yyyymmdd.substring(0, 4);
        String mm = yyyymmdd.substring(4, 6);
        String dd = yyyymmdd.substring(6, 8);
        return StringUtils.stripStart(yy, "0") + "/" + StringUtils.stripStart(mm, "0") + "/"
                + StringUtils.stripStart(dd, "0");
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.input.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.ReadOnly;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * CEFR、国語記述入力画面のViewです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Dialog
public class ExmntnInputView extends AbstractView {

    /** 認定試験名１コンボボックス */
    /*@Location(x = 127, y = 41)
    @Size(width = 140, height = 20)
    public ComboBox<EngPtBean> joiningExam1ComboBox;*/

    /** 認定試験レベル１コンボボックス */
    /*@Location(x = 127, y = 65)
    @Size(width = 140, height = 20)
    @Disabled
    public ComboBox<EngPtDetailBean> joiningExamLevel1ComboBox;*/

    /** CEFRスコア１テキスト  */
    /*@Location(x = 127, y = 89)
    @Size(width = 30, height = 20)
    @NumberField(maxLength = 4, scale = 1)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Disabled
    public TextField cefrScore1Field;*/

    /** CEFRスコアが不明１チェックボックス */
    /*@Location(x = 162, y = 93)
    @CodeConfig(Code.ENG_JOINING_INPUT_UNKNOWN_SCORE)
    @Disabled
    public CodeCheckBox unknownCefrScore1CheckBox;*/

    /** CEFRレベル１コンボボックス */
    /*@Location(x = 127, y = 113)
    @Size(width = 80, height = 20)
    @Disabled
    public ComboBox<CefrBean> cefrLevel1ComboBox;*/

    /** CEFRレベルが不明１チェックボックス */
    /*@Location(x = 212, y = 117)
    @CodeConfig(Code.ENG_JOINING_INPUT_UNKNOWN_CEFR)
    @Disabled
    public CodeCheckBox unknownCefrLevel1CheckBox;*/

    /** 英検合否１コンボボックス */
    /*@Location(x = 127, y = 137)
    @Size(width = 80, height = 20)
    @Disabled
    public ComboBox<EngExamResultBean> engExamResult1ComboBox;*/

    /** エラーメッセージ1 文字*/
    /*@Location(x = 127, y = 159)
    @Foreground(r = 255, g = 0, b = 0)
    @Font(BZFont.INPUT_SCORE_ERROR)
    @Invisible
    public ImageLabel errorMessageCefr1Label;*/

    /** 認定試験名２コンボボックス */
    /*@Location(x = 127, y = 217)
    @Size(width = 140, height = 20)
    public ComboBox<EngPtBean> joiningExam2ComboBox;*/

    /** 認定試験レベル２コンボボックス */
    /*@Location(x = 127, y = 241)
    @Size(width = 140, height = 20)
    @Disabled
    public ComboBox<EngPtDetailBean> joiningExamLevel2ComboBox;*/

    /** CEFRスコア２テキスト  */
    /*@Location(x = 127, y = 265)
    @Size(width = 30, height = 20)
    @NumberField(maxLength = 4, scale = 1)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Disabled
    public TextField cefrScore2Field;*/

    /** CEFRスコアが不明２チェックボックス */
    /*@Location(x = 162, y = 269)
    @CodeConfig(Code.ENG_JOINING_INPUT_UNKNOWN_SCORE)
    @Disabled
    public CodeCheckBox unknownCefrScore2CheckBox;*/

    /** CEFRレベル２コンボボックス */
    /*@Location(x = 127, y = 289)
    @Size(width = 80, height = 20)
    @Disabled
    public ComboBox<CefrBean> cefrLevel2ComboBox;*/

    /** CEFRレベルが不明２チェックボックス */
    /*@Location(x = 212, y = 293)
    @CodeConfig(Code.ENG_JOINING_INPUT_UNKNOWN_CEFR)
    @Disabled
    public CodeCheckBox unknownCefrLevel2CheckBox;*/

    /** 英検合否２コンボボックス */
    /*@Location(x = 127, y = 313)
    @Size(width = 80, height = 20)
    @Disabled
    public ComboBox<EngExamResultBean> engExamResult2ComboBox;*/

    /** エラーメッセージ２ 文字*/
    /*@Location(x = 127, y = 335)
    @Foreground(r = 255, g = 0, b = 0)
    @Font(BZFont.INPUT_SCORE_ERROR)
    @Invisible
    public ImageLabel errorMessageCefr2Label;*/

    /** 問１コンボボックス */
    @Location(x = 127, y = 41)
    @Size(width = 50, height = 20)
    public ComboBox<String> jpnWriting1ComboBox;

    /** 問２コンボボックス */
    @Location(x = 127, y = 65)
    @Size(width = 50, height = 20)
    public ComboBox<String> jpnWriting2ComboBox;

    /** 問３コンボボックス */
    @Location(x = 127, y = 89)
    @Size(width = 50, height = 20)
    public ComboBox<String> jpnWriting3ComboBox;

    /** 総合評価テキスト  */
    @Location(x = 127, y = 113)
    @Size(width = 30, height = 20)
    @ReadOnly
    @HorizontalAlignment(SwingConstants.LEFT)
    public TextField japaneseWField;

    /** エラーメッセージ２ 文字*/
    @Location(x = 127, y = 135)
    @Foreground(r = 255, g = 0, b = 0)
    @Font(BZFont.INPUT_SCORE_ERROR)
    @Invisible
    public ImageLabel errorMessageJpnLabel;

    /** OKボタン */
    @Location(x = 73, y = 177)
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 160, y = 177)
    public ImageButton cancelButton;

    @Override
    public void initialize() {

        super.initialize();
    }

}

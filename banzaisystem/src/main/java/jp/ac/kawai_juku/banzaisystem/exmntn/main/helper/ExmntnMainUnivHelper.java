package jp.ac.kawai_juku.banzaisystem.exmntn.main.helper;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.dist.controller.ExmntnDistController;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.view.ExmntnDistView;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainCandidateUnivService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.controller.ExmntnPrntController;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.helper.ExmntnPrntHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.controller.ExmntnUnivController;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 個人成績画面の志望大学ヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainUnivHelper {

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView view;

    /** 志望大学詳細画面のView */
    @Resource
    private ExmntnUnivView exmntnUnivView;

    /** 志望大学学力分布画面のView */
    @Resource
    private ExmntnDistView exmntnDistView;

    /** 志望大学サービス */
    @Resource(name = "exmntnMainCandidateUnivService")
    private ExmntnMainCandidateUnivService univService;

    /** 志望大学詳細画面のヘルパー */
    @Resource(name = "exmntnUnivHelper")
    private ExmntnUnivHelper univHelper;

    /** 一括出力画面のヘルパー */
    @Resource(name = "exmntnPrntHelper")
    private ExmntnPrntHelper prntHelper;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 志望大学リストエリアを初期化します。
     *
     * @param score 成績データ
     * @param holdFlg 志望大学詳細画面の志望順位コンボボックス選択値保持フラグ
     */
    public void initCandidateUniv(ScoreBean score, boolean holdFlg) {

        /* 志望大学リストを取得する*/
        List<ExmntnMainCandidateUnivBean> list;
        if (systemDto.isTeacher()) {
            list = univService.findTeacherCandidateUnivList(systemDto.getTargetIndividualId(), score);
        } else {
            list = univService.findStudentCandidateUnivList(dto.getStudentCandidateUnivList(), score);
        }

        /* 志望大学リストをテーブルに設定する */
        view.candidateUnivTable.setCandidateUnivList(list);

        /* 志望順位コンボボックスを初期化する */
        initCandidateRankComboBox(holdFlg);

    }

    /**
     * 表示中の志望順位コンボボックスがある画面について、
     * 現在の志望大学リストエリアの内容で志望順位コンボボックスを初期化します。
     *
     * @param holdFlg 志望順位コンボボックス選択値保持フラグ
     */
    public void initCandidateRankComboBox(boolean holdFlg) {

        /* 志望大学詳細画面の志望順位コンボボックスを初期化する */
        initCandidateRankComboBox(exmntnUnivView.candidateRankComboBox, holdFlg);

        /* 志望大学学力分布画面の志望順位コンボボックスを初期化する */
        if (view.tabbedPane.isActive(ExmntnDistController.class)) {
            initCandidateRankComboBox(exmntnDistView.candidateRankComboBox, holdFlg);
        }

        /* 一括出力画面の志望順位コンボボックスを初期化する */
        if (view.tabbedPane.isActive(ExmntnPrntController.class)) {
            prntHelper.initCandidateRankComboBox();
        }
    }

    /**
     * 志望順位コンボボックスを初期化します。
     *
     * @param combo 志望順位コンボボックス
     * @param holdFlg 志望順位コンボボックス選択値保持フラグ
     */
    private void initCandidateRankComboBox(ComboBox<ExmntnMainCandidateUnivBean> combo, boolean holdFlg) {
        univHelper.initCandidateRankComboBox(combo, holdFlg ? combo.getSelectedItem() : null);

    }

    /**
     * 指定した志望大学詳細を表示します。
     *
     * @param bean {@link ExmntnMainCandidateUnivBean}
     */
    public void showUnivDetail(ExmntnMainCandidateUnivBean bean) {
        exmntnUnivView.candidateRankComboBox.setSelectedItem(new ComboBoxItem<ExmntnMainCandidateUnivBean>(bean));
        if (!view.tabbedPane.isActive(ExmntnUnivController.class)) {
            view.tabbedPane.activate(ExmntnUnivController.class);
        }
    }

    /**
     * 志望大学リストの「予」アイコンを削除します。
     *
     * @param keyList 大学リスト
     */
    public void deleteSchedule(List<? extends UnivKeyBean> keyList) {
        view.candidateUnivTable.deleteSchedule(keyList);
    }

}

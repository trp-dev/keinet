package jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean;

/**
 *
 * 得点帯／偏差値帯別の人数を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistNumbersBean {

    /** 人数 */
    private int numbers;

    /** 累計 */
    private int total;

    /** 目盛り数 */
    private int scaleNum;

    /** 帯ヘッダ */
    private String distHeader;

    /** 評価基準 */
    private String standard;

    /** 強調表示フラグ */
    private boolean emphasisFlg;

    /**
     * 人数を返します。
     *
     * @return 人数
     */
    public int getNumbers() {
        return numbers;
    }

    /**
     * 人数をセットします。
     *
     * @param numbers 人数
     */
    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    /**
     * 累計を返します。
     *
     * @return 累計
     */
    public int getTotal() {
        return total;
    }

    /**
     * 累計をセットします。
     *
     * @param total 累計
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 評価基準を返します。
     *
     * @return 評価基準
     */
    public String getStandard() {
        return standard;
    }

    /**
     * 評価基準をセットします。
     *
     * @param standard 評価基準
     */
    public void setStandard(String standard) {
        this.standard = standard;
    }

    /**
     * 目盛り数を返します。
     *
     * @return 目盛り数
     */
    public int getScaleNum() {
        return scaleNum;
    }

    /**
     * 目盛り数をセットします。
     *
     * @param scaleNum 目盛り数
     */
    public void setScaleNum(int scaleNum) {
        this.scaleNum = scaleNum;
    }

    /**
     * 帯ヘッダを返します。
     *
     * @return 帯ヘッダ
     */
    public String getDistHeader() {
        return distHeader;
    }

    /**
     * 帯ヘッダをセットします。
     *
     * @param distHeader 帯ヘッダ
     */
    public void setDistHeader(String distHeader) {
        this.distHeader = distHeader;
    }

    /**
     * 強調表示フラグを返します。
     *
     * @return 強調表示フラグ
     */
    public boolean isEmphasisFlg() {
        return emphasisFlg;
    }

    /**
     * 強調表示フラグをセットします。
     *
     * @param emphasisFlg 強調表示フラグ
     */
    public void setEmphasisFlg(boolean emphasisFlg) {
        this.emphasisFlg = emphasisFlg;
    }

}

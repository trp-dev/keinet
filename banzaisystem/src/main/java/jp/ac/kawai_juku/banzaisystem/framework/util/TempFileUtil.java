package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.io.File;
import java.io.IOException;

import org.seasar.framework.exception.IORuntimeException;

/**
 *
 * 一時ファイルに関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class TempFileUtil {

    /** デフォルトの一時ディレクトリ */
    private static final File TMP_DIR = new File("temp");

    /**
     * コンストラクタです。
     */
    private TempFileUtil() {
    }

    /**
     * 一時ファイルを生成します。
     *
     * @return 一時ファイルのオブジェクト
     */
    public static File createTempFile() {
        if (!TMP_DIR.isDirectory()) {
            TMP_DIR.mkdir();
        }
        return createTempFile(TMP_DIR);
    }

    /**
     * 一時ファイルを生成します。
     *
     * @param directory 書き出し先ディレクトリ
     * @return 一時ファイルのオブジェクト
     */
    public static File createTempFile(File directory) {
        try {
            File file = File.createTempFile(TempFileUtil.class.getName(), null, directory);
            file.deleteOnExit();
            return file;
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}

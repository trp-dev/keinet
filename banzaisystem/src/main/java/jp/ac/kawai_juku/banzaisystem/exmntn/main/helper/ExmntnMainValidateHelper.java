package jp.ac.kawai_juku.banzaisystem.exmntn.main.helper;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import javax.annotation.Resource;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainService;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

/**
 *
 * 個人成績画面の検証に関するヘルパークラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainValidateHelper {

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView view;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** ScoreHelper */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /** SubjectHelper */
    @Resource(name = "exmntnMainSubjectHelper")
    private ExmntnMainSubjectHelper subjectHelper;

    /** Service */
    @Resource
    private ExmntnMainService exmntnMainService;

    /**
     * 「学年」「クラス」「クラス番号」コンボが変更可能であるかをチェックします。
     *
     * @param combo コンボボックス
     * @return 変更可能ならtrue
     */
    public boolean checkBeforeChangeCombo(ComboBox<?> combo) {
        if (!dto.isChangeMarkScore() && !dto.isChangeWrittenScore()) {
            return true;
        } else if (confirm(scoreHelper.getMarkExam(), scoreHelper.getWrittenExam())) {
            return true;
        } else {
            combo.turnBack();
            return false;
        }
    }

    /**
     * マーク模試コンボが変更可能であるかをチェックします。
     *
     * @return 変更可能ならtrue
     */
    public boolean checkBeforeChangeMark() {
        if (!systemDto.isTeacher() || (!dto.isChangeMarkScore() && !dto.isChangeWrittenScore())) {
            return true;
        } else if (confirm(view.exmntnMarkComboBox.getPreviousValue(), scoreHelper.getWrittenExam())) {
            return true;
        } else {
            view.exmntnMarkComboBox.turnBack();
            return false;
        }
    }

    /**
     * 記述模試コンボが変更可能であるかをチェックします。
     *
     * @return 変更可能ならtrue
     */
    public boolean checkBeforeChangeWritten() {
        if (!systemDto.isTeacher() || (!dto.isChangeMarkScore() && !dto.isChangeWrittenScore())) {
            return true;
        } else if (confirm(scoreHelper.getMarkExam(), view.exmntnWritingComboBox.getPreviousValue())) {
            return true;
        } else {
            view.exmntnWritingComboBox.turnBack();
            return false;
        }
    }

    /**
     * 他機能に移動可能であるかをチェックします。
     *
     * @return 移動可能ならtrue
     */
    public boolean checkMovable() {
        if (!systemDto.isTeacher() || (!dto.isChangeMarkScore() && !dto.isChangeWrittenScore())) {
            return true;
        } else {
            return confirm(scoreHelper.getMarkExam(), scoreHelper.getWrittenExam());
        }
    }

    /**
     * 処理の継続を確認します。
     *
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @return 継続OKならtrue
     */
    private boolean confirm(ExamBean markExam, ExamBean writtenExam) {

        boolean ret = true;

        if (view.returnInitDataPinkButton.isEnabled()) {
            switch (showDialog()) {
                case JOptionPane.YES_OPTION:
                    ret = saveRecord(markExam, writtenExam);
                    break;
                case JOptionPane.NO_OPTION:
                    break;
                default:
                    ret = false;
                    break;
            }
        }

        if (ret) {
            initScoreChangedStatus();
        }

        return ret;
    }

    /**
     * 成績を保存します。
     *
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @return 入力エラーがある場合はfalse
     */
    private boolean saveRecord(ExamBean markExam, ExamBean writtenExam) {
        if (view.errorMessageChar1Label.isVisible() || view.errorMessageChar2Label.isVisible()) {
            /* 入力エラーがある場合は保存しない */
            JOptionPane.showMessageDialog(WindowManager.getInstance().getActiveWindow(),
                    getMessage("warning.exmntn.main.C_W002"));
            return false;
        } else {
            exmntnMainService.saveRecord(systemDto.getTargetIndividualId(),
                    scoreHelper.createDispScore(markExam, writtenExam), dto);
            return true;
        }
    }

    /**
     * 成績保存確認ダイアログを表示します。
     *
     * @return ダイアログ選択値
     */
    private int showDialog() {
        String[] button =
                { getMessage("label.exmntn.main.C_W001.Yes"), getMessage("label.exmntn.main.C_W001.No"),
                        getMessage("label.exmntn.main.C_W001.Cancel") };
        return JOptionPane.showOptionDialog(WindowManager.getInstance().getActiveWindow(),
                getMessage("warning.exmntn.main.C_W001"), null, JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE, null, button, button[0]);
    }

    /**
     * 成績保存確認ダイアログ(保存ボタン押下時)を表示します。
     *
     * @return 保存OKならtrue
     */
    public boolean showSaveConfirmDialog() {
        String[] button = { getMessage("label.exmntn.main.C_W003.Yes"), getMessage("label.exmntn.main.C_W003.Cancel") };
        int result =
                JOptionPane.showOptionDialog(WindowManager.getInstance().getActiveWindow(),
                        getMessage("warning.exmntn.main.C_W003"), null, JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

    /**
     * 成績変更状態を初期化します。
     */
    public void initScoreChangedStatus() {
        view.returnInitDataPinkButton.setEnabled(false);
        dto.initChangeScoreState();
    }

}

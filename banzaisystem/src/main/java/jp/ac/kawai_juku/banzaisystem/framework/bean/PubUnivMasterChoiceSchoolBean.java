package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ公表用志望校を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterChoiceSchoolBean {

    /** 入試試験区分 */
    private String entExamDiv;

    /** 大学コード枝番 */
    private String uniCdBranchCd;

    /** 総合配点 */
    private int totalAllotPnt;

    /** 総合配点＿小論文なし */
    private int totalAllotPntNRep;

    /** 入試満点値 */
    private int entExamFullPnt;

    /** 入試満点値＿小論文なし */
    private String entExamFullPntNRep;

    /** 入試科目名 */
    private String subCount;

    /** 得点換算パターンコード−英語認定試験 */
    private String centerCvsScoreEngPt;

    /** 加点区分−英語認定試験 */
    private String addPtDivEngPt;

    /** 加点配点−英語認定試験 */
    private int addPtEngPt;

    /** 得点換算パターンコード−国語記述式 */
    private String centerCvsScoreKokugoDesc;

    /** 加点区分−国語記述式 */
    private String addPtDivKokugoDesc;

    /** 加点配点−国語記述式 */
    private int addPtKokugoDesc;

    /**
     * 入試試験区分を返します。
     *
     * @return 入試試験区分
     */
    public String getEntExamDiv() {
        return entExamDiv;
    }

    /**
     * 入試試験区分をセットします。
     *
     * @param entExamDiv 入試試験区分
     */
    public void setEntExamDiv(String entExamDiv) {
        this.entExamDiv = entExamDiv;
    }

    /**
     * 大学コード枝番を返します。
     *
     * @return 大学コード枝番
     */
    public String getUniCdBranchCd() {
        return uniCdBranchCd;
    }

    /**
     * 大学コード枝番をセットします。
     *
     * @param uniCdBranchCd 大学コード枝番
     */
    public void setUniCdBranchCd(String uniCdBranchCd) {
        this.uniCdBranchCd = uniCdBranchCd;
    }

    /**
     * 総合配点を返します。
     *
     * @return 総合配点
     */
    public int getTotalAllotPnt() {
        return totalAllotPnt;
    }

    /**
     * 総合配点をセットします。
     *
     * @param totalAllotPnt 総合配点
     */
    public void setTotalAllotPnt(int totalAllotPnt) {
        this.totalAllotPnt = totalAllotPnt;
    }

    /**
     * 総合配点＿小論文なしを返します。
     *
     * @return 総合配点＿小論文なし
     */
    public int getTotalAllotPntNRep() {
        return totalAllotPntNRep;
    }

    /**
     * 総合配点＿小論文なしをセットします。
     *
     * @param totalAllotPntNRep 総合配点＿小論文なし
     */
    public void setTotalAllotPntNRep(int totalAllotPntNRep) {
        this.totalAllotPntNRep = totalAllotPntNRep;
    }

    /**
     * 入試満点値を返します。
     *
     * @return 入試満点値
     */
    public int getEntExamFullPnt() {
        return entExamFullPnt;
    }

    /**
     * 入試満点値をセットします。
     *
     * @param entExamFullPnt 入試満点値
     */
    public void setEntExamFullPnt(int entExamFullPnt) {
        this.entExamFullPnt = entExamFullPnt;
    }

    /**
     * 入試満点値＿小論文なしを返します。
     *
     * @return 入試満点値＿小論文なし
     */
    public String getEntExamFullPntNRep() {
        return entExamFullPntNRep;
    }

    /**
     * 入試満点値＿小論文なしをセットします。
     *
     * @param entExamFullPntNRep 入試満点値＿小論文なし
     */
    public void setEntExamFullPntNRep(String entExamFullPntNRep) {
        this.entExamFullPntNRep = entExamFullPntNRep;
    }

    /**
     * 入試科目名を返します。
     *
     * @return 入試科目名
     */
    public String getSubCount() {
        return subCount;
    }

    /**
     * 入試科目名をセットします。
     *
     * @param subCount 入試科目名
     */
    public void setSubCount(String subCount) {
        this.subCount = subCount;
    }

    /**
     * 得点換算パターンコード−英語認定試験を返します。
     *
     * @return 得点換算パターンコード−英語認定試験
     */
    public String getCenterCvsScoreEngPt() {
        return centerCvsScoreEngPt;
    }

    /**
     * 得点換算パターンコード−英語認定試験をセットします。
     *
     * @param centerCvsScoreEngPt 得点換算パターンコード−英語認定試験
     */
    public void setCenterCvsScoreEngPt(String centerCvsScoreEngPt) {
        this.centerCvsScoreEngPt = centerCvsScoreEngPt;
    }

    /**
     * 加点区分−英語認定試験を返します。
     *
     * @return 加点区分−英語認定試験
     */
    public String getAddPtDivEngPt() {
        return addPtDivEngPt;
    }

    /**
     * 加点区分−英語認定試験をセットします。
     *
     * @param addPtDivEngPt 加点区分−英語認定試験
     */
    public void setAddPtDivEngPt(String addPtDivEngPt) {
        this.addPtDivEngPt = addPtDivEngPt;
    }

    /**
     * 加点配点−英語認定試験を返します。
     *
     * @return 加点配点−英語認定試験
     */
    public int getAddPtEngPt() {
        return addPtEngPt;
    }

    /**
     * 加点配点−英語認定試験をセットします。
     *
     * @param addPtEngPt 加点配点−英語認定試験
     */
    public void setAddPtEngPt(int addPtEngPt) {
        this.addPtEngPt = addPtEngPt;
    }

    /**
     * 得点換算パターンコード−国語記述式を返します。
     *
     * @return 得点換算パターンコード−国語記述式
     */
    public String getCenterCvsScoreKokugoDesc() {
        return centerCvsScoreKokugoDesc;
    }

    /**
     * 得点換算パターンコード−国語記述式をセットします。
     *
     * @param centerCvsScoreKokugoDesc 得点換算パターンコード−国語記述式
     */
    public void setCenterCvsScoreKokugoDesc(String centerCvsScoreKokugoDesc) {
        this.centerCvsScoreKokugoDesc = centerCvsScoreKokugoDesc;
    }

    /**
     * 加点区分−国語記述式を返します。
     *
     * @return 加点区分−国語記述式
     */
    public String getAddPtDivKokugoDesc() {
        return addPtDivKokugoDesc;
    }

    /**
     * 加点区分−国語記述式をセットします。
     *
     * @param addPtDivKokugoDesc 加点区分−国語記述式
     */
    public void setAddPtDivKokugoDesc(String addPtDivKokugoDesc) {
        this.addPtDivKokugoDesc = addPtDivKokugoDesc;
    }

    /**
     * 加点配点−国語記述式を返します。
     *
     * @return 加点配点−国語記述式
     */
    public int getAddPtKokugoDesc() {
        return addPtKokugoDesc;
    }

    /**
     * 加点配点−国語記述式をセットします。
     *
     * @param addPtKokugoDesc 加点配点−国語記述式
     */
    public void setAddPtKokugoDesc(int addPtKokugoDesc) {
        this.addPtKokugoDesc = addPtKokugoDesc;
    }

}

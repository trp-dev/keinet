package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * コンポーネントの大きさを設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Size {

    /**
     * コンポーネントの横幅を設定します。
     *
     * @return 横幅
     */
    int width();

    /**
     * コンポーネントの縦幅を設定します。
     *
     * @return 縦幅
     */
    int height();

}

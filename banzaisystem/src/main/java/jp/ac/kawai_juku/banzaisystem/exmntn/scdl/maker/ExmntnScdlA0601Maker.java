package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.ScheduleType;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlA0601InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlA0601StudentInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.util.ExmntnUnivExcelUtil;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.seasar.framework.container.SingletonS2Container;

/**
 *
 * Excel帳票メーカー(受験スケジュール)です。
 *
 *
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A06_01)
public class ExmntnScdlA0601Maker extends BaseExcelMaker<ExmntnScdlA0601InBean> {

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル（生徒情報）：クラス番号 */
    private static final String TITLE_STUDENT_CLSNO = "クラス番号：";

    /** タイトル（生徒情報）：氏名 */
    private static final String TITLE_STUDENT_NAME = "　　氏名：";

    /** 一週間の曜日 */
    private static final String[] STR_WEEK_DAY = { "日", "月", "火", "水", "木", "金", "土" };

    /** 一週間の日数（曜日） */
    private static final int CNT_WEEK_DAY = 7;

    /** 最大大学数（1ページ目） */
    private static final int PAGE1_UNIVMAX = 12;

    /** 最大大学数（2ページ目以降） */
    private static final int PAGE2_SUB_ONES_UNIVMAX = 14;

    /** インデックスrow：日付1月〜2月（1ページ目） */
    private static final int IDX_ROW_DATE_START_PAGE1_M12 = 17;

    /** インデックス：日付1月〜2月（2ページ目） */
    private static final int IDX_ROW_DATE_START_PAGE2_M12 = 7;

    /** インデックスcell：日付1月〜2月 */
    private static final int IDX_CELL_DATE_START_M12 = 15;

    /** インデックスcell：日付3月 */
    private static final int IDX_CELL_DATE_START_M3 = 66;

    /** 開始日1月〜2月 */
    private static final int IDX_DATE_START_DAY_M12 = 10;

    /** 開始日3月 */
    private static final int IDX_DATE_START_DAY_M3 = 1;

    /** インデックスrow：1大学の行数 */
    private static final int IDX_ROW_UNIV_NUM = 3;

    /** インデックスcell：受験スケジュール開始日 */
    private static final int IDX_CELL_DATE_START_EXAMDAY = 15;

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックス：クラス番号・氏名  */
    private static final String IDX_STUDENT = "A5";

    /** インデックス：模試名（上段） */
    private static final String IDX_EXAMINATION1 = "A6";

    /** インデックスcell：志望順位 */
    private static final int IDX_CELL_WISHH_RANK = 0;

    /** インデックスcell：志望大学（出願大学） */
    private static final int IDX_CELL_UNIV_APPLICATION = IDX_CELL_WISHH_RANK + 1;

    /** インデックスcell：地方受験 */
    private static final int IDX_CELL_UNIV_THIHOU = IDX_CELL_UNIV_APPLICATION + 6;

    /** インデックスcell：センター評価 */
    private static final int IDX_CELL_CENTER_RATING = IDX_CELL_UNIV_THIHOU + 2;

    /** インデックスcell：二次ー評価 */
    private static final int IDX_CELL_SECOND_RATING = IDX_CELL_CENTER_RATING + 2;

    /** インデックスcell：総合評価 */
    private static final int IDX_CELL_TOTAL_RATING = IDX_CELL_SECOND_RATING + 2;

    /** 帳票ユーティリティ */
    private ExmntnUnivExcelUtil excelUtil = new ExmntnUnivExcelUtil(this);

    /** 日付用フォーマット */
    private final FastDateFormat dateFormat = FastDateFormat.getInstance("yyyyMMdd");

    /** 日付リスト */
    private List<String> dateList;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 生徒情報リスト */
    private List<ExmntnScdlA0601StudentInBean> studentList;

    /** 生徒情報Bean */
    private ExmntnScdlA0601StudentInBean studentBean;

    /** マーク模試 */
    private ExamBean markExam;

    /** 記述模試 */
    private ExamBean writtenExam;

    /** 対象模試 */
    private ExamBean targetExam;

    /** うるう年フラグ */
    private boolean isLeapYearFlg;

    @Override
    protected boolean prepareData(ExmntnScdlA0601InBean inBean) {

        /* 出力のために入力beanを保存 */
        studentList = inBean.getStudentList();

        if (studentList.isEmpty()) {
            /* 複数生徒一括の場合：生徒が0人なら出力しない */
            return false;
        } else {
            /* initSheetメソッド用に最初の生徒で初期化しておく */
            studentBean = studentList.get(0);
        }

        if (studentBean.getStudentBean() == null || studentBean.getScoreBean() == null
                || (!studentBean.getScoreBean().isEntered() && studentBean.getScheduleList().isEmpty())) {
            /*
             * 「生徒情報無し」「判定未実行」「成績未入力かつ受験スケジュール大学無し」の何れかなら出力しない。
             * 複数生徒一括の場合は、これらの条件を満たすデータはstudentListに設定されないので、
             * このチェック処理は単数出力の場合のみ機能する。
             */
            return false;
        }

        /* 対象模試（マーク模試）を保存する */
        markExam = studentBean.getScoreBean().getMarkExam();

        /* 対象模試（記述模試）を保存する */
        writtenExam = studentBean.getScoreBean().getWrittenExam();

        if (markExam == null && writtenExam == null) {
            /* 入力情報がない */
            return false;
        } else {
            /* 対象模試を判定する */
            targetExam = ExamUtil.judgeTargetExam(markExam, writtenExam);
        }

        /* 受験スケジュール日付リストを作成する。 */
        dateList = createDateList();

        /* うるう年判定を行う */
        int year = systemDto.getNendo() + 1;
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            isLeapYearFlg = true;
        } else {
            isLeapYearFlg = false;
        }

        return true;
    }

    @Override
    protected void initSheet() {
        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();

        /* クラス番号、氏名を出力する */
        outputStudent();

        /* 曜日を出力する */
        outputDayOfWeek();

    }

    @Override
    protected void outputData() {
        for (int i = 0; i < studentList.size(); i++) {
            /* 帳票ユーティリティに成績データを設定する */
            excelUtil.setScoreBean(studentBean.getScoreBean());

            /* 個人成績一覧データを出力する */
            excelUtil.outputSubRecord(targetExam, markExam, writtenExam, IDX_EXAMINATION1);

            /* 受験スケジュールを出力する */
            outputScedule();

            /* 次の生徒が存在する場合は改シートする */
            if (i + 1 < studentList.size()) {
                /* 生徒情報を次の生徒で上書きする */
                studentBean = studentList.get(i + 1);
                /* テンプレートの位置をリセット＆改シートする */
                resetTemplateIndexAndBreakSheet();
            }
        }

    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年文字列 */
        String gradeStr = TITLE_GRADE + studentBean.getStudentBean().getGrade();

        /* クラス文字列 */
        String classStr = TITLE_CLS + studentBean.getStudentBean().getCls();

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeStr + classStr);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, TITLE_EXAM + targetExam);
    }

    /**
     * 生徒情報を出力するメソッドです。
     */
    private void outputStudent() {

        /* クラス番号文字列 */
        String clsnoStr = TITLE_STUDENT_CLSNO + studentBean.getStudentBean().getClassNo();

        /* 氏名文字列 */
        String nameStr = TITLE_STUDENT_NAME + studentBean.getStudentBean().getNameKana();

        /* 生徒情報を出力する */
        setCellValue(IDX_STUDENT, clsnoStr + nameStr);
    }

    /**
     * 受験スケジュールを出力するメソッドです。
     */
    private void outputScedule() {
        /* ページ数カウント */
        int cntPage = 1;

        /* ページ内大学数カウント */
        int cntUniv = 0;

        /* 全体大学数カウント */
        int cntAllUniv = 0;

        /* 最大大学数1ページ目 */
        int univMax1 = PAGE1_UNIVMAX;

        /* 最大大学数2ページ目 */
        int univMax2 = PAGE2_SUB_ONES_UNIVMAX;

        for (ExmntnScdlBean bean : studentBean.getScheduleList()) {

            cntAllUniv++;

            /* 1ページ内の最大出力数を超えたかチェックする */
            if (getSheetCount() == 1) {
                /* 1ページ目の出力 */
                if (cntUniv >= univMax1) {
                    /* 改シートする */
                    breakSheet();
                    cntPage++;
                    cntUniv = 0;
                }
            } else {
                /* 2ページ目の出力 */
                if (cntUniv >= univMax2) {
                    /* これ以降のデータ出力しない */
                    break;
                }
            }

            outputScheduleDetail(cntPage, cntUniv, cntAllUniv, bean);

            cntUniv++;
        }

    }

    /**
     * 曜日を出力するメソッドです。
     */
    private void outputDayOfWeek() {

        /* 出力行 */
        int row = 0;

        /* 1ページ目と2ページ目で出力位置（行）が違う */
        if (getSheetCount() == 1) {
            row = IDX_ROW_DATE_START_PAGE1_M12;
        } else {
            row = IDX_ROW_DATE_START_PAGE2_M12;
        }

        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(systemDto.getNendo() + 1, 0, IDX_DATE_START_DAY_M12, 0, 0);

        int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int j = week;

        /* 1月〜2月 */
        for (int i = 0; i < 51; i++, j++) {

            /* うるう年判定 */
            if (i == 50 && !isLeapYearFlg) {
                /* うるう年でないなら、2月29日は空白とする */
                setCellValue(row, IDX_CELL_DATE_START_M12 + i, "");
                setCellValue(row + 1, IDX_CELL_DATE_START_M12 + i, "");
                break;
            }

            if (j >= CNT_WEEK_DAY) {
                j = 0;
            }

            /* 曜日 */
            setCellValue(row + 1, IDX_CELL_DATE_START_M12 + i, STR_WEEK_DAY[j]);

        }

        Calendar cal3 = Calendar.getInstance();
        cal3.clear();
        cal3.set(systemDto.getNendo() + 1, 2, IDX_DATE_START_DAY_M3, 0, 0);

        int week3 = cal3.get(Calendar.DAY_OF_WEEK) - 1;
        int j3 = week3;

        /* 3月 */
        for (int i = 0; i < 31; i++, j3++) {

            if (j3 >= CNT_WEEK_DAY) {
                j3 = 0;
            }

            /* 曜日 */
            setCellValue(row + 1, IDX_CELL_DATE_START_M3 + i, STR_WEEK_DAY[j3]);

        }

    }

    /**
     * 受験スケジュールを出力するメソッドです。
     *
     * @param cntPage ページカウント
     * @param cntUniv 1ページの大学カウント
     * @param cntAllUniv 1生徒の全大学数
     * @param scdule 受験スケジュール情報
     */
    private void outputScheduleDetail(int cntPage, int cntUniv, int cntAllUniv, ExmntnScdlBean scdule) {

        /* 出力行 */
        int row = 0;

        /* 1ページ目と2ページ目で出力位置が違う */
        if (cntPage == 1) {
            row = 19;
        } else {
            row = 9;
        }

        /* 志望順位 */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_WISHH_RANK, scdule.getCandidateRank());

        /* 出願大学を出力する */
        /* 学部・学科はnullの場合、空文字を出力する */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_UNIV_APPLICATION,
                (scdule.isCautionNeeded() ? "【！】" : "") + scdule.getUnivName() + "\n"
                        + StringUtils.defaultString(scdule.getFacultyName()) + "\n"
                        + StringUtils.defaultString(scdule.getDeptName()));

        /* 地方受験 */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_UNIV_THIHOU,
                BZConstants.FLG_ON.equals(scdule.getProvincesFlg()) ? getMessage("label.exmntn.scdl.excel.thihou")
                        : "");

        /* センター評価：評価を出力する */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_CENTER_RATING, scdule.getCenterRating());

        /* 二次評価：評価を出力する */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_SECOND_RATING, scdule.getSecondRating());

        /* 総合：評価を出力する */
        setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_TOTAL_RATING, scdule.getTotalRating());

        /* 受験スケジュールを出力する */
        createScheduleMap(scdule.getScheduleMap(), row, cntUniv);

    }

    /**
     * 受験スケジュールを出力します。
     *
     * @param scduleMap 入試日程詳細情報
     * @param row 行
     * @param cntUniv 1ページの大学カウント
     */
    private void createScheduleMap(Map<String, ScheduleType> scduleMap, int row, int cntUniv) {

        /* 手続締切日を出力する */
        for (Map.Entry<String, ScheduleType> e : scduleMap.entrySet()) {
            if (e.getValue() == ScheduleType.PROCEDEADLINE) {
                String proceDeadLine = e.getKey();
                int number = dateList.indexOf(proceDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_25_PERCENT, IndexedColors.BLACK,
                        getMessage("label.exmntn.scdl.excel.proceDeadLine"));
            }
        }

        /* 合格発表日を出力する */
        for (Map.Entry<String, ScheduleType> e : scduleMap.entrySet()) {
            if (e.getValue() == ScheduleType.SUCANNDATE) {
                String sugannDate = e.getKey();
                int number = dateList.indexOf(sugannDate);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_40_PERCENT, IndexedColors.BLACK,
                        getMessage("label.exmntn.scdl.excel.sugannDate"));
            }
        }

        /* 願書締切日を出力する */
        for (Map.Entry<String, ScheduleType> e : scduleMap.entrySet()) {
            if (e.getValue() == ScheduleType.APPLIDEADLINE_POST) {
                /* 出願締切（消印） */
                String appliDeadLine = e.getKey();
                int number = dateList.indexOf(appliDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_50_PERCENT, IndexedColors.WHITE,
                        getMessage("label.exmntn.scdl.excel.appliDeadLinePost"));
            } else if (e.getValue() == ScheduleType.APPLIDEADLINE_NLT) {
                /* 出願締切（必着） */
                String appliDeadLine = e.getKey();
                int number = dateList.indexOf(appliDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_50_PERCENT, IndexedColors.WHITE,
                        getMessage("label.exmntn.scdl.excel.appliDeadLineNlt"));
            } else if (e.getValue() == ScheduleType.APPLIDEADLINE_DESK) {
                /* 出願締切（窓口のみ） */
                String appliDeadLine = e.getKey();
                int number = dateList.indexOf(appliDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_50_PERCENT, IndexedColors.WHITE,
                        getMessage("label.exmntn.scdl.excel.appliDeadLineDesk"));
            } else if (e.getValue() == ScheduleType.APPLIDEADLINE_NET) {
                /* 出願締切（ネットのみ） */
                String appliDeadLine = e.getKey();
                int number = dateList.indexOf(appliDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_50_PERCENT, IndexedColors.WHITE,
                        getMessage("label.exmntn.scdl.excel.appliDeadLineNet"));
            } else if (e.getValue() == ScheduleType.APPLIDEADLINE_UNK) {
                /* 出願締切（不明） */
                String appliDeadLine = e.getKey();
                int number = dateList.indexOf(appliDeadLine);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.GREY_50_PERCENT, IndexedColors.WHITE,
                        getMessage("label.exmntn.scdl.excel.appliDeadLineUnk"));
            }
        }

        /* 入試日を出力する(入試本学地方区分を考慮しない) */
        for (Map.Entry<String, ScheduleType> e : scduleMap.entrySet()) {
            if (e.getValue().isInpleDate()) {
                String inpleDate = e.getKey();
                int number = dateList.indexOf(inpleDate);
                number = number + cellDateIsLeapYear(number);
                setCellStyleValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        IndexedColors.BLACK, IndexedColors.WHITE, getMessage("label.exmntn.scdl.excel.inpleDate"));
            }
        }

        /* 日程重複を出力する */
        for (Map.Entry<String, ScheduleType> e : scduleMap.entrySet()) {
            if (e.getValue() == ScheduleType.DUPLICATION) {
                int number = dateList.indexOf(e.getKey());
                number = number + cellDateIsLeapYear(number);
                setCellValue(row + (IDX_ROW_UNIV_NUM * cntUniv), IDX_CELL_DATE_START_EXAMDAY + number,
                        getMessage("label.exmntn.scdl.excel.duplication"));
            }
        }
    }

    /**
     * セル書式を指定した文字列を出力する。
     *
     * @param rowIndex セル行番号
     * @param cellnum セル列番号
     * @param backColors 背景色（前景色）
     * @param fontColors フォント色
     * @param message 出力文字列
     */
    private void setCellStyleValue(int rowIndex, int cellnum, IndexedColors backColors, IndexedColors fontColors,
            String message) {

        HSSFCell cell = getCell(rowIndex, cellnum);

        /* 背景色を塗る */
        HSSFCellStyle style = createCellStyle();
        style.cloneStyleFrom(cell.getCellStyle());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFillForegroundColor(backColors.getIndex());
        cell.setCellStyle(style);

        /* フォントの色を変える */
        Font font = createFont();
        font.setColor(fontColors.getIndex());
        font.setFontHeightInPoints((short) 9);
        font.setFontName("ＭＳ ゴシック");
        style.setFont(font);

        /* 受験スケジュール文字列を出力する */
        cell.setCellValue(message);

    }

    /**
     * 受験スケジュールの日付リストを生成します。
     *
     * @return ヘッダ
     */

    private List<String> createDateList() {

        dateList = new ArrayList<>();

        Integer year = SingletonS2Container.getComponent(SystemDto.class).getNendo();

        Calendar cal = Calendar.getInstance();
        cal.set(year.intValue() + 1, 0, 10);

        cal.set(Calendar.MONTH, 0);
        while (cal.get(Calendar.MONTH) != 3) {
            dateList.add(dateFormat.format(cal));
            cal.add(Calendar.DATE, 1);
        }

        return dateList;
    }

    /**
     * うるう年でない場合、3月1日以降、1日プラスする。
     *
     * @param number 出力cell位置
     * @return 日数
     */
    private int cellDateIsLeapYear(int number) {
        /* うるう年でない場合、3月以降1日ズレが生じる */
        if (number > 49 && !isLeapYearFlg) {
            return 1;
        }
        return 0;
    }

}

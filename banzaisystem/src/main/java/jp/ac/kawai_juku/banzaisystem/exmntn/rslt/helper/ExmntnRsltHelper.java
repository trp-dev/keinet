package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.helper;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.bean.ExmntnRsltA0401InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dto.ExmntnRsltDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.service.ExmntnRsltService;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.dto.ExmntnSubsDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.service.ExmntnSubsService;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 検索結果画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnRsltHelper {

    /** View */
    @Resource(name = "exmntnRsltView")
    private ExmntnRsltView view;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** Service */
    @Resource(name = "exmntnRsltService")
    private ExmntnRsltService service;

    /** 大学検索画面のサービス */
    @Resource
    private ExmntnSubsService exmntnSubsService;

    /** 個人成績画面の成績に関するヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /** DTO */
    @Resource(name = "exmntnRsltDto")
    private ExmntnRsltDto dto;

    /** 大学検索画面のDTO */
    @Resource
    private ExmntnSubsDto exmntnSubsDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 検索結果データリストを初期化します。
     */
    public void initDataList() {
        dto.setDataList(exmntnSubsService.searchUniv(exmntnSubsDto.getSearchBean()));
    }

    /**
     * 検索結果テーブルを初期化します。
     */
    public void initResultTable() {
        view.resultTable.initialize(null, view);
        List<Map<String, CellData>> dataList = service.filterDataList(view, dto.getDataList());
        view.countLabel.setText(Integer.toString(dataList.size()));
        view.resultTable.setDataList(dataList);
    }

    /**
     * Excel帳票メーカー(検索結果)の入力Beanを生成します。
     *
     * @return 入力Bean
     */
    public ExmntnRsltA0401InBean createInBean() {
        if (dto.getDataList() == null) {
            /* 検索結果がNULLなら再検索する */
            initDataList();
            initResultTable();
        }
        return new ExmntnRsltA0401InBean(scoreHelper.getTargetExam(), exmntnMainView, view, exmntnSubsDto.getSearchBean());
    }

    /**
     * 「志望大学リストに追加する」ボタン押下アクションです。
     */
    public void addCandidateUnivButtonAction() {
        List<UnivKeyBean> univKeyList = view.resultTable.getSelectedUnivKeyList();
        if (systemDto.isTeacher()) {
            /* 先生用 */
            service.addTeacherCandidateUniv(scoreHelper.getTargetExam(), systemDto.getTargetIndividualId(), univKeyList);
        } else {
            /* 生徒用 */
            service.addStudentCandidateUniv(getComponent(ExmntnMainDto.class), univKeyList);
        }
        /* 選択をクリアする */
        view.resultTable.clearSelection();
    }

}

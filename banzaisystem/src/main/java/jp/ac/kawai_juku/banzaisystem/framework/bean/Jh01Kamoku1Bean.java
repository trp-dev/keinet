package jp.ac.kawai_juku.banzaisystem.framework.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * 情報誌用科目1の情報を保持するBeanです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
@Getter
@Setter
@ToString
public class Jh01Kamoku1Bean {

    /** 結合キー */
    private String combiKey;

    /** 戸籍10桁コード */
    private String univ10Cd;

    /** 枝番名称 */
    private String branchName;

}

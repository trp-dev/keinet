package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;

/**
 *
 * 一覧出力ダイアログのExcel帳票メーカーの入力Beanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDprtExcelMakerInBean implements ExcelMakerInBean {

    /** 対象模試 */
    private final ExamBean exam;

    /** 対象模試のドッキング先模試 */
    private final ExamBean dockingExam;

    /** 個人IDリスト */
    private final List<Integer> idList;

    /**
     * コンストラクタです。
     *
     * @param exam 対象模試
     * @param dockingExam 対象模試のドッキング先模試
     * @param idList 個人IDリスト
     */
    public SelstdDprtExcelMakerInBean(ExamBean exam, ExamBean dockingExam, List<Integer> idList) {
        this.exam = exam;
        this.dockingExam = dockingExam;
        this.idList = idList;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試ド
     */
    public ExamBean getExam() {
        return exam;
    }

    /**
     * 個人IDリストを返します。
     *
     * @return 個人IDリスト
     */
    public List<Integer> getIdList() {
        return idList;
    }

    /**
     * 対象模試のドッキング先模試を返します。
     *
     * @return 対象模試のドッキング先模試
     */
    public ExamBean getDockingExam() {
        return dockingExam;
    }

}

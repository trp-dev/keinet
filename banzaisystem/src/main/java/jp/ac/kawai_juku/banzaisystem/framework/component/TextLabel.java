package jp.ac.kawai_juku.banzaisystem.framework.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.lang.reflect.Field;

import javax.swing.JLabel;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.VerticalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * テキストラベルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class TextLabel extends JLabel implements BZComponent {

    /** フィールド名の接尾辞 */
    private static final String SUFFIX = "Label";

    /**
     * コンストラクタです。
     */
    public TextLabel() {
        setBorder(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        if (!field.getName().endsWith(SUFFIX)) {
            throw new RuntimeException(String.format("%sのフィールド名は[***%s]とする必要があります。[%s]", getClass().getSimpleName(),
                    SUFFIX, field.getName()));
        }

        setText(getMessage("label." + view.getPkg() + "."
                + field.getName().substring(0, field.getName().length() - SUFFIX.length())));

        if (!field.isAnnotationPresent(Size.class)) {
            setSize(getPreferredSize());
        }

        HorizontalAlignment align = field.getAnnotation(HorizontalAlignment.class);
        if (align != null) {
            setHorizontalAlignment(align.value());
        }

        VerticalAlignment valign = field.getAnnotation(VerticalAlignment.class);
        if (valign != null) {
            setVerticalAlignment(valign.value());
        }
    }

    /**
     * ラベルのテキストを差し替えるのと同時に、
     * コンポーネントサイズを推奨サイズにリサイズします。
     *
     * @param text 差し替えるテキスト
     */
    public void replace(String text) {
        setText(text);
        setSize(getPreferredSize());
    }

}

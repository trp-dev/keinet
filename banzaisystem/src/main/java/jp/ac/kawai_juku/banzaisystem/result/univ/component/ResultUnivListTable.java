package jp.ac.kawai_juku.banzaisystem.result.univ.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.SortableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * 大学一覧テーブルです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ResultUnivListTable extends SortableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** データリスト */
    private List<ResultUnivBean> dataList;

    /**
     * コンストラクタです。
     */
    public ResultUnivListTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED, "division");
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setColumnHeaderView(createColumnHeaderView());
        setViewportView(createTableView(getHeader()));
    }

    /**
     * スクロール領域のテーブルヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.add(createHeaderButton("division", CellAlignment.CENTER));
        panel.add(createHeaderButton("univName", CellAlignment.LEFT));
        panel.add(createHeaderButton("publishDay", CellAlignment.CENTER));
        return panel;
    }

    /**
     * テーブルを生成します。
     *
     * @param header ヘッダ
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final JPanel header) {

        ReadOnlyTableModel tableModel = new ReadOnlyTableModel(header.getComponentCount() + 1);

        JTable table = new BZTable(tableModel) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                c.setFont(column == 2 ? BZFont.SUCANNDATE.getFont() : BZFont.DEFAULT.getFont());
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (Component c : header.getComponents()) {
                    x += c.getWidth();
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);

        /*
         * 列の幅をヘッダの列と合わせる。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        Component[] components = header.getComponents();
        for (int i = 0; i < components.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(components[i].getWidth());
            columnModel.getColumn(i).setCellRenderer(((Header) components[i]).getAlignment().getRenderer());
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public final void setDataList(List<ResultUnivBean> dataList) {
        /* ソート用にデータリストを保持しておく */
        this.dataList = dataList;
        /* ソートする */
        sort(getSortKey(), isAscending());
    }

    @Override
    protected void sort(final String sortKey, final boolean isAscending) {
        if (dataList != null) {
            Collections.sort(dataList, new Comparator<ResultUnivBean>() {
                @Override
                public int compare(ResultUnivBean o1, ResultUnivBean o2) {

                    /* 第1ソートキーでソートする */
                    int result;
                    switch (sortKey) {
                        case "division":
                            result = ObjectUtils.compare(o1.getUnivDiv().getValue(), o2.getUnivDiv().getValue());
                            break;
                        case "univName":
                            result = ObjectUtils.compare(o1.getUnivNameKana(), o2.getUnivNameKana());
                            break;
                        case "publishDay":
                            result = ObjectUtils.compare(o1.getSucAnnDate(), o2.getSucAnnDate(), true);
                            break;
                        default:
                            throw new RuntimeException("不正なソートキーです。" + sortKey);
                    }

                    if (result != 0) {
                        return isAscending ? result : -result;
                    }

                    /* 大学コードでソートする */
                    return ObjectUtils.compare(o1.getUnivCd(), o2.getUnivCd());
                }
            });
        }
        ((BZTableModel) getTable().getModel()).setDataVector(toDataVector());
    }

    /**
     * データリストをテーブル用データに変換します。
     *
     * @return テーブル用データ
     */
    public Vector<Vector<Object>> toDataVector() {
        Vector<Vector<Object>> univList = new Vector<>();
        if (dataList != null) {
            for (ResultUnivBean bean : dataList) {
                Vector<Object> row = new Vector<>(4);
                row.add(bean.getUnivCd());
                row.add(bean.getUnivDiv().getName());
                row.add(bean.getUnivName());
                row.add(toMD(bean.getSucAnnDate()));
                univList.add(row);
            }
        }
        return univList;
    }

    /**
     * YYYYMMDDからM.D表記に変換します。
     *
     * @param yyyymmdd YYYYMMDD
     * @return M.D表記
     */
    private String toMD(String yyyymmdd) {
        if (yyyymmdd == null) {
            return StringUtils.EMPTY;
        } else {
            int m = Integer.parseInt(yyyymmdd.substring(4, 6));
            int d = Integer.parseInt(yyyymmdd.substring(6));
            StringBuilder sb = new StringBuilder(5);
            if (m < 10) {
                sb.append(' ');
            }
            sb.append(m).append(".");
            if (d < 10) {
                sb.append(' ');
            }
            sb.append(d);
            return sb.toString();
        }
    }

    /**
     * 選択中の大学コードリストを返します。
     *
     * @return 大学コードリスト
     */
    public List<String> getSelectedUnivCdList() {
        JTable table = getTable();
        TableModel model = table.getModel();
        List<String> list = new ArrayList<>();
        for (int rowIndex : table.getSelectedRows()) {
            list.add((String) model.getValueAt(rowIndex, 0));
        }
        return list;
    }

}

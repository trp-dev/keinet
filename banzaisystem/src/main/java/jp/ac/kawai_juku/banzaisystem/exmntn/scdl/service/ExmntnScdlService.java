package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_OFF;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.ScheduleType;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.dao.ExmntnScdlDao;
import jp.ac.kawai_juku.banzaisystem.framework.bean.StudentExamPlanUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;
import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.builder.InpleDateBuilder;

import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * 受験スケジュール画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnScdlService {

    /** DAO */
    @Resource(name = "exmntnScdlDao")
    private ExmntnScdlDao dao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 志望大学個人成績画面のDTO */
    @Resource
    private ExmntnMainDto exmntnMainDto;

    /**
     * [先生用] 受験予定大学一覧を取得します。
     *
     * @param id 個人ID
     * @return 受験予定大学一覧
     */
    public List<ExmntnScdlBean> findTeachderExamPlanUnivList(Integer id) {

        if (id == null) {
            return Collections.emptyList();
        }

        /* DBから受験予定大学一覧を取得する */
        List<ExmntnScdlBean> list = dao.selectTeacherExamPlanUnivList(id);

        /* 入試日程詳細を設定する */
        setScheduleDetail(list);

        /* 注意マーク表示フラグを設定する */
        setCautionNeeded(list);

        /* 判定を実行する */
        judge(list);

        return list;
    }

    /**
     * [生徒用] 受験予定大学一覧を取得します。
     *
     * @return 受験予定大学一覧
     */
    public List<ExmntnScdlBean> findStudentExamPlanUnivList() {

        final List<StudentExamPlanUnivBean> planList = exmntnMainDto.getStudentExamPlanUnivList();

        if (planList == null || planList.isEmpty()) {
            return Collections.emptyList();
        }

        /* DBから受験予定大学一覧を取得する */
        List<ExmntnScdlBean> list = dao.selectStudentExamPlanUnivList(planList);

        /* DTOに保持していた地方フラグをセットする */
        for (StudentExamPlanUnivBean bean : planList) {
            if (bean.getProvincesFlg() != null) {
                list.get(list.indexOf((Object) bean)).setProvincesFlg(bean.getProvincesFlg());
            }
        }

        /* 入試日程詳細を設定する */
        setScheduleDetail(list);

        /* 注意マーク表示フラグを設定する */
        setCautionNeeded(list);

        /* DTOに設定された順番通りにする */
        Collections.sort(list, new Comparator<ExmntnScdlBean>() {
            @Override
            public int compare(ExmntnScdlBean o1, ExmntnScdlBean o2) {
                return planList.indexOf((Object) o1) < planList.indexOf((Object) o2) ? -1 : 1;
            }
        });

        /* 志望順位を設定する */
        setCandidateRank(list);

        /* 判定を実行する */
        judge(list);

        return list;
    }

    /**
     * 入試日程詳細を設定します。
     *
     * @param list 受験予定大学一覧
     */
    private void setScheduleDetail(List<ExmntnScdlBean> list) {
        for (ExmntnScdlBean bean : list) {
            setScheduleDetail(bean, dao.selectScheduleDetailList(bean));
        }
    }

    /**
     * 注意マーク表示フラグを設定します。
     *
     * @param list 受験予定大学一覧
     */
    private void setCautionNeeded(List<ExmntnScdlBean> list) {
        for (ExmntnScdlBean bean : list) {
            bean.setCautionNeeded(ScheduleUtil.isCautionNeeded(systemDto.getUnivExamDiv(), bean.getUnivCd(), bean.getExamScheduleDetailFlag(), bean.getUnivCommentFlag()));
        }
    }

    /**
     * 入試日程詳細を設定します。
     *
     * @param bean {@link ExmntnScdlBean}
     * @param list 入試日程詳細リスト
     */
    private void setScheduleDetail(ExmntnScdlBean bean, List<ScheduleDetailBean> list) {

        if (ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), bean.getUnivCd()) && checkLocal(list)) {
            /* 入試日表示が有効かつ地方試験が存在する場合、地方受験チェックボックスが表示されるようにする */
            if (bean.getProvincesFlg() == null) {
                bean.setProvincesFlg(FLG_OFF);
            }
        } else {
            /* そうでなければ、地方フラグをNULLにする(地方受験チェックボックス非表示とする) */
            bean.setProvincesFlg(null);
        }

        /* 日程マップを設定する */
        bean.setScheduleMap(createScheduleMap(bean.getUnivCd(), list));

        /* 入試日の最小値を設定する */
        for (Entry<String, ScheduleType> entry : bean.getScheduleMap().entrySet()) {
            if (entry.getValue().isInpleDate()) {
                if (bean.getMinInpleDate() == null || bean.getMinInpleDate().compareTo(entry.getKey()) > 0) {
                    bean.setMinInpleDate(entry.getKey());
                }
            }
        }
    }

    /**
     * 「本学または地方」または「地方のみ」のデータが存在するかをチェックします。
     *
     * @param list 入試日程詳細リスト
     * @return 「本学または地方」または「地方のみ」のデータが存在するならtrue
     */
    private boolean checkLocal(List<ScheduleDetailBean> list) {
        for (ScheduleDetailBean bean : list) {
            if (Code.SCHOOLPROVENTDIV_HOME_OR_LOCAL.getValue().equals(bean.getSchoolProventDiv()) || Code.SCHOOLPROVENTDIV_LOCAL.getValue().equals(bean.getSchoolProventDiv())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 日程マップを生成します。
     *
     * @param univCd 大学コード
     * @param list 入試日程詳細リスト
     * @return 日程マップ
     */
    private Map<String, ScheduleType> createScheduleMap(String univCd, List<ScheduleDetailBean> list) {

        /* 優先順位が低いものからマップに詰める(優先順位が高いもので上書きされるようにする) */
        Map<String, ScheduleType> map = new HashMap<>();

        /* 手続締切日を設定する */
        for (ScheduleDetailBean bean : list) {
            if (bean.getProceDeadLine() != null && ScheduleUtil.isProceDeadLineEnabled(systemDto.getUnivExamDiv(), univCd)) {
                put(map, bean.getProceDeadLine(), ScheduleType.PROCEDEADLINE);
            }
        }

        /* 合格発表日を設定する */
        for (ScheduleDetailBean bean : list) {
            if (bean.getSucAnnDate() != null && ScheduleUtil.isSucAnnDateEnabled(systemDto.getUnivExamDiv(), univCd)) {
                put(map, bean.getSucAnnDate(), ScheduleType.SUCANNDATE);
            }
        }

        /* 願書締切日を設定する */
        for (ScheduleDetailBean bean : list) {
            if (bean.getEntExamAppliDeadline() != null && ScheduleUtil.isAppliDeadLineEnabled(systemDto.getUnivExamDiv(), univCd)) {
                put(map, bean.getEntExamAppliDeadline(), findScheduleTypeApplideadline(bean));
            }
        }

        /* 入試日を設定する */
        for (ScheduleDetailBean bean : list) {
            if (ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), univCd)) {
                ScheduleType type = findScheduleTypeInpleDate(bean);
                for (Object date : new InpleDateBuilder(Collections.singletonList(bean)).getDateSet()) {
                    put(map, (String) date, type);
                }
            }
        }

        return map;
    }

    /**
     * 日程マップに要素を追加します。
     *
     * @param map 日程マップ
     * @param key マップのキー：YYYYMMDD
     * @param value マップの値：{@link ScheduleType}
     */
    private void put(Map<String, ScheduleType> map, String key, ScheduleType value) {
        if (map.containsKey(key)) {
            map.put(key, ScheduleType.DUPLICATION);
        } else {
            map.put(key, value);
        }
    }

    /**
     * 入試出願締切区分に対応する {@link ScheduleType} を返します。
     *
     * @param bean {@link ScheduleDetailBean}
     * @return {@link ScheduleType}
     */
    private ScheduleType findScheduleTypeApplideadline(ScheduleDetailBean bean) {
        return (ScheduleType) CodeType.APPLIDEADLINEDIV.value2Code(bean.getEntExamAppliDeadlineDiv()).getAttribute()[0];
    }

    /**
     * 入試本学地方区分に対応する {@link ScheduleType} を返します。
     *
     * @param bean {@link ScheduleDetailBean}
     * @return {@link ScheduleType}
     */
    private ScheduleType findScheduleTypeInpleDate(ScheduleDetailBean bean) {
        return (ScheduleType) CodeType.SCHOOLPROVENTDIV.value2Code(bean.getSchoolProventDiv()).getAttribute()[0];
    }

    /**
     * 志望順位を設定します。
     *
     * @param list 受験予定大学一覧
     */
    private void setCandidateRank(List<ExmntnScdlBean> list) {
        int rank = 1;
        for (ExmntnScdlBean bean : list) {
            bean.setCandidateRank(Integer.valueOf(rank++));
        }
    }

    /**
     * 判定を実行します。
     *
     * @param list 受験予定大学一覧
     */
    private void judge(List<ExmntnScdlBean> list) {
        if (!list.isEmpty()) {
            /* BZJudgementExecutor.getInstance().judge(exmntnMainDto.getJudgeScore(), list, false); */
            BZJudgementExecutor.getInstance().judge(exmntnMainDto.getJudgeScore(), list);
        }
    }

    /**
     * [先生用] 志望順位を変更します。
     *
     * @param id 個人ID
     * @param pair 志望順位を入れ替えた受験予定大学
     */
    @Lock
    public void changeTeacherRank(Integer id, Pair<? extends UnivKeyBean, ? extends UnivKeyBean> pair) {

        /* 志望順位を入れ替える受験予定大学を取得する */
        ExmntnScdlBean leftBean = dao.selectExamPlanUnivBean(id, pair.getLeft());
        ExmntnScdlBean rightBean = dao.selectExamPlanUnivBean(id, pair.getRight());

        if (leftBean != null && rightBean != null) {
            /* 志望順位を入れ替える */
            Integer leftRank = leftBean.getCandidateRank();
            leftBean.setCandidateRank(rightBean.getCandidateRank());
            rightBean.setCandidateRank(leftRank);
            /* 志望順位を更新する */
            dao.updateExamPlanUnivRank(leftBean, rightBean);
        }
    }

    /**
     * [生徒用] 志望順位を変更します。
     *
     * @param pair 順位を入れ替えた受験予定大学
     */
    public void changeStudentRank(Pair<? extends UnivKeyBean, ? extends UnivKeyBean> pair) {

        List<StudentExamPlanUnivBean> list = exmntnMainDto.getStudentExamPlanUnivList();

        int leftIndex = list.indexOf(pair.getLeft());
        int rightIndex = list.indexOf(pair.getRight());

        int minIndex = Math.min(leftIndex, rightIndex);
        int maxIndex = Math.max(leftIndex, rightIndex);

        StudentExamPlanUnivBean minBean = list.set(minIndex, list.remove(maxIndex));
        list.add(maxIndex, minBean);
    }

    /**
     * [先生用] 受験予定大学を削除します。
     *
     * @param id 個人ID
     * @param list 削除する受験予定大学リスト
     */
    @Lock
    public void deleteTeacherExamPlanUniv(Integer id, List<ExmntnScdlBean> list) {

        /* DBから削除する */
        dao.deleteExamPlanUniv(list);

        /* DBから受験予定大学一覧を取得する */
        List<ExmntnScdlBean> planList = dao.selectTeacherExamPlanUnivList(id);

        /* 志望順位を設定する */
        setCandidateRank(planList);

        /* 志望順位を更新する */
        dao.updateExamPlanUnivRank(planList.toArray(new ExmntnScdlBean[planList.size()]));
    }

    /**
     * [生徒用] 受験予定大学を削除します。
     *
     * @param list 削除する受験予定大学リスト
     */
    public void deleteStudentExamPlanUniv(List<? extends UnivKeyBean> list) {
        if (exmntnMainDto.getStudentExamPlanUnivList() != null) {
            exmntnMainDto.getStudentExamPlanUnivList().removeAll(list);
        }
    }

    /**
     * [先生用] 地方フラグを変更します。
     *
     * @param bean {@link ExmntnScdlBean}
     */
    @Lock
    public void changeTeacherProvincesFlg(ExmntnScdlBean bean) {
        dao.updateExamPlanUnivProvincesFlg(bean);
    }

    /**
     * [生徒用] 地方フラグを変更します。
     *
     * @param bean {@link ExmntnScdlBean}
     */
    public void changeStudentProvincesFlg(ExmntnScdlBean bean) {
        List<StudentExamPlanUnivBean> list = exmntnMainDto.getStudentExamPlanUnivList();
        list.get(list.indexOf((Object) bean)).setProvincesFlg(bean.getProvincesFlg());
    }

}

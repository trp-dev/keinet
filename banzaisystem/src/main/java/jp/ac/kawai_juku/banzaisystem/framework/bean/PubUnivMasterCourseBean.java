package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ公表用教科を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterCourseBean {

    /** 入試試験区分 */
    private String entExamDiv;

    /** 大学コード枝番 */
    private String uniCdBranchCd;

    /** 教科コード */
    private String courseCd;

    /** 必須配点 */
    private double necesAllotPnt;

    /** 必須科目数 */
    private int necesSubCount;

    /** センター二次同一科目選択区分 */
    private int cenSecSubChoiceDiv;

    /**
     * 入試試験区分を返します。
     *
     * @return 入試試験区分
     */
    public String getEntExamDiv() {
        return entExamDiv;
    }

    /**
     * 入試試験区分をセットします。
     *
     * @param entExamDiv 入試試験区分
     */
    public void setEntExamDiv(String entExamDiv) {
        this.entExamDiv = entExamDiv;
    }

    /**
     * 大学コード枝番を返します。
     *
     * @return 大学コード枝番
     */
    public String getUniCdBranchCd() {
        return uniCdBranchCd;
    }

    /**
     * 大学コード枝番をセットします。
     *
     * @param uniCdBranchCd 大学コード枝番
     */
    public void setUniCdBranchCd(String uniCdBranchCd) {
        this.uniCdBranchCd = uniCdBranchCd;
    }

    /**
     * 教科コードを返します。
     *
     * @return 教科コード
     */
    public String getCourseCd() {
        return courseCd;
    }

    /**
     * 教科コードをセットします。
     *
     * @param courseCd 教科コード
     */
    public void setCourseCd(String courseCd) {
        this.courseCd = courseCd;
    }

    /**
     * 必須配点を返します。
     *
     * @return 必須配点
     */
    public double getNecesAllotPnt() {
        return necesAllotPnt;
    }

    /**
     * 必須配点をセットします。
     *
     * @param necesAllotPnt 必須配点
     */
    public void setNecesAllotPnt(double necesAllotPnt) {
        this.necesAllotPnt = necesAllotPnt;
    }

    /**
     * 必須科目数を返します。
     *
     * @return 必須科目数
     */
    public int getNecesSubCount() {
        return necesSubCount;
    }

    /**
     * 必須科目数をセットします。
     *
     * @param necesSubCount 必須科目数
     */
    public void setNecesSubCount(int necesSubCount) {
        this.necesSubCount = necesSubCount;
    }

    /**
     * センター二次同一科目選択区分を返します。
     *
     * @return センター二次同一科目選択区分
     */
    public int getCenSecSubChoiceDiv() {
        return cenSecSubChoiceDiv;
    }

    /**
     * センター二次同一科目選択区分をセットします。
     *
     * @param cenSecSubChoiceDiv センター二次同一科目選択区分
     */
    public void setCenSecSubChoiceDiv(int cenSecSubChoiceDiv) {
        this.cenSecSubChoiceDiv = cenSecSubChoiceDiv;
    }

}

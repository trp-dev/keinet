package jp.ac.kawai_juku.banzaisystem.framework.interceptor;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.service.LockService;

import org.aopalliance.intercept.MethodInvocation;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;

/**
 *
 * データファイルロックが必要なサービスメソッドのインターセプタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class LockInterceptor extends AbstractInterceptor {

    /** ロックサービス */
    @Resource
    private LockService lockService;

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        return lockService.execute(mi);
    }

}

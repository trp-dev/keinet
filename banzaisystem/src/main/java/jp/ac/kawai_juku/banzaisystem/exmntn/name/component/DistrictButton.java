package jp.ac.kawai_juku.banzaisystem.exmntn.name.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JComponent;

import jp.ac.kawai_juku.banzaisystem.framework.component.BaseImageToggleButton;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 地区画像ボタンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class DistrictButton extends BaseImageToggleButton {

    /** 地区コード */
    private String distCd;

    @Override
    public void initialize(Field field, final AbstractView view) {

        super.initialize(field, view);

        distCd = field.getName().substring(4, 6);

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (JComponent jc : view.findComponentsByGroup(getGroup())) {
                    if (jc instanceof PrefButton) {
                        ((PrefButton) jc).setSelected(isSelected());
                    }
                }
            }
        });
    }

    /**
     * 地区コードを返します。
     *
     * @return 地区コード
     */
    public String getDistCd() {
        return distCd;
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;

/**
 *
 * エクセル出力機能ノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
abstract class OutputExcelNode extends OutputNode {

    /** エクセルアイコン */
    private static final ImageIcon ICON;
    static {
        ICON = new ImageIcon(ImageUtil.readImage(OutputExcelNode.class, "icon/excel.png"));
    }

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    OutputExcelNode(Object userObject) {
        super(userObject);
    }

    @Override
    public Icon getIcon() {
        return ICON;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterChoiceSchoolBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterCourseBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSelectGpCourseBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSelectGroupBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.PubUnivMasterSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.PubUnivMasterDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.factory.FactoryHelper;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterChoiceSchoolPlus;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGpCourse;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSelectGroup;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;

/**
 *
 * 公表用大学マスタのロードサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterService {

    /** DAO */
    @Resource
    private PubUnivMasterDao pubUnivMasterDao;

    /**
     * 公表用大学マスタをロードします。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学詳細リスト
     */
    public Collection<?> load(UnivKeyBean key) {

        /* マスタをマップにロードする */
        Map<?, ?> umcsMap = createChoiceSchoolMap(key);
        Map<?, ?> umcMap = createCourseMap(key);
        Map<?, ?> umsMap = createSubjectMap(key);
        Map<?, ?> umsgMap = createSelectGroupMap(key);
        Map<?, ?> umsgcMap = createSelectGpCourseMap(key);

        /* 詳細データマップを生成する */
        Map<?, ?> detailMap;
        try {
            detailMap = FactoryHelper.createPublibCrossPattern(key.getUnivCd(), key.getFacultyCd(), key.getDeptCd(), umcsMap, umcMap, umsMap, umsgMap, umsgcMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return detailMap.values();
    }

    /**
     * 大学マスタ公表用志望校マップを生成します。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学マスタ公表用志望校マップ
     */
    private Map<String, Map<String, UnivMasterChoiceSchoolPlus>> createChoiceSchoolMap(UnivKeyBean key) {

        Map<String, Map<String, UnivMasterChoiceSchoolPlus>> map = new HashMap<>();

        for (PubUnivMasterChoiceSchoolBean bean : pubUnivMasterDao.selectPubUnivMasterChoiceSchoolList(key)) {
            UnivMasterChoiceSchoolPlus data = new UnivMasterChoiceSchoolPlus(bean.getTotalAllotPnt(), bean.getTotalAllotPntNRep(), bean.getEntExamFullPnt(), bean.getEntExamFullPntNRep(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, bean.getSubCount(), null, 9, 9, 0, "0", bean.getCenterCvsScoreEngPt(), bean.getAddPtDivEngPt(), bean.getAddPtEngPt(),
                    bean.getCenterCvsScoreKokugoDesc(), bean.getAddPtDivKokugoDesc(), bean.getAddPtKokugoDesc(), "0");
            getValue(map, bean.getUniCdBranchCd()).put(bean.getEntExamDiv(), data);
        }

        return map;
    }

    /**
     * 大学マスタ公表用教科マップを生成します。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学マスタ公表用教科マップ
     */
    private Map<String, Map<String, Map<String, UnivMasterCourse>>> createCourseMap(UnivKeyBean key) {

        Map<String, Map<String, Map<String, UnivMasterCourse>>> map = new HashMap<>();

        for (PubUnivMasterCourseBean bean : pubUnivMasterDao.selectPubUnivMasterCourseList(key)) {
            UnivMasterCourse data = new UnivMasterCourse(bean.getNecesAllotPnt(), bean.getNecesSubCount(), bean.getCenSecSubChoiceDiv());
            getValue(getValue(map, bean.getUniCdBranchCd()), bean.getEntExamDiv()).put(bean.getCourseCd(), data);
        }

        return map;
    }

    /**
     * 大学マスタ公表用科目マップを生成します。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学マスタ公表用科目マップ
     */
    private Map<String, Map<String, Map<String, UnivMasterSubject>>> createSubjectMap(UnivKeyBean key) {

        Map<String, Map<String, Map<String, UnivMasterSubject>>> map = new HashMap<>();

        for (PubUnivMasterSubjectBean bean : pubUnivMasterDao.selectPubUnivMasterSubjectList(key)) {
            UnivMasterSubject data = new UnivMasterSubject(bean.getSubBit(), bean.getSubPnt(), bean.getSubArea(), null);
            getValue(getValue(map, bean.getUniCdBranchCd()), bean.getEntExamDiv()).put(bean.getExamSubCd(), data);
        }

        return map;
    }

    /**
     * 大学マスタ公表用選択グループマップを生成します。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学マスタ公表用選択グループマップ
     */
    private Map<String, Map<String, Map<String, UnivMasterSelectGroup>>> createSelectGroupMap(UnivKeyBean key) {

        Map<String, Map<String, Map<String, UnivMasterSelectGroup>>> map = new HashMap<>();

        for (PubUnivMasterSelectGroupBean bean : pubUnivMasterDao.selectPubUnivMasterSelectGroupList(key)) {
            UnivMasterSelectGroup data = new UnivMasterSelectGroup(bean.getSelectGSubCount(), bean.getSelectGAllotPnt());
            getValue(getValue(map, bean.getUniCdBranchCd()), bean.getEntExamDiv()).put(bean.getSelectGNo(), data);
        }

        return map;
    }

    /**
     * 大学マスタ公表用選択グループ教科マップを生成します。
     *
     * @param key {@link UnivKeyBean}
     * @return 大学マスタ公表用選択グループ教科マップ
     */
    private Map<String, Map<String, Map<String, Map<String, UnivMasterSelectGpCourse>>>> createSelectGpCourseMap(UnivKeyBean key) {

        Map<String, Map<String, Map<String, Map<String, UnivMasterSelectGpCourse>>>> map = new HashMap<>();

        for (PubUnivMasterSelectGpCourseBean bean : pubUnivMasterDao.selectPubUnivMasterSelectGpCourseList(key)) {
            UnivMasterSelectGpCourse data = new UnivMasterSelectGpCourse(bean.getsGPerSubSubCount());
            getValue(getValue(getValue(map, bean.getUniCdBranchCd()), bean.getEntExamDiv()), bean.getSelectGNo()).put(bean.getCourseCd(), data);
        }

        return map;
    }

    /**
     * マップから指定したキーの値を取得します。
     *
     * @param map マップ
     * @param key キー
     * @param <V> 値の型
     * @return 値マップ
     */
    private <V> Map<String, V> getValue(Map<String, Map<String, V>> map, String key) {
        Map<String, V> child = map.get(key);
        if (child == null) {
            child = new HashMap<>();
            map.put(key, child);
        }
        return child;
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller.SelstdDunvController.SelstdDunvProcessor;

/**
 *
 * 大学選択ダイアログの入力Beanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDunvInBean {

    /** 対象模試 */
    private final ExamBean exam;

    /** 対象生徒の個人IDリスト */
    private final List<Integer> idList;

    /** 選択済大学リスト */
    private final List<SelstdDunvBean> univList;

    /** OKボタンが押された場合の処理 */
    private final SelstdDunvProcessor processor;

    /**
     * コンストラクタです。
     *
     * @param exam 対象模試
     * @param idList 対象生徒の個人IDリスト
     * @param univList 選択済大学リスト
     * @param processor {@link SelstdDunvProcessor}
     */
    public SelstdDunvInBean(ExamBean exam, List<Integer> idList, List<SelstdDunvBean> univList,
            SelstdDunvProcessor processor) {
        this.exam = exam;
        this.idList = idList;
        this.univList = univList;
        this.processor = processor;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public ExamBean getExam() {
        return exam;
    }

    /**
     * 対象生徒の個人IDリストを返します。
     *
     * @return 対象生徒の個人IDリスト
     */
    public List<Integer> getIdList() {
        return idList;
    }

    /**
     * 選択済大学リストを返します。
     *
     * @return 選択済大学リスト
     */
    public List<SelstdDunvBean> getUnivList() {
        return univList;
    }

    /**
     * OKボタンが押された場合の処理を返します。
     *
     * @return OKボタンが押された場合の処理
     */
    public SelstdDunvProcessor getProcessor() {
        return processor;
    }

}

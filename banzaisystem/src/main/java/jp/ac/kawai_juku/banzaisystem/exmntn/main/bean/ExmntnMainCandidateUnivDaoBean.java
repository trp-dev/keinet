package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 志望大学情報を保持するDAOBeanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainCandidateUnivDaoBean extends UnivKeyBean {

    /** 区分 */
    private String section;

    /** 表示順 */
    private Integer dispNumber;

    /**
     * 区分を返します。
     *
     * @return 区分
     */
    public String getSection() {
        return section;
    }

    /**
     * 区分をセットします。
     *
     * @param section 区分
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 表示順を返します。
     *
     * @return 表示順
     */
    public Integer getDispNumber() {
        return dispNumber;
    }

    /**
     * 表示順をセットします。
     *
     * @param dispNumber 表示順
     */
    public void setDispNumber(Integer dispNumber) {
        this.dispNumber = dispNumber;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.Code;

/**
 *
 * コード利用コンポーネント用の設定アノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface CodeConfig {

    /**
     * コンポーネントに紐付けるコード
     *
     * @return コード利用
    */
    Code value();

}

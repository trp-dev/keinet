package jp.ac.kawai_juku.banzaisystem.framework.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.ScoreBeanDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCandidateUnivBean;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;

/**
 *
 * 判定用成績データに関するサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ScoreBeanService {

    /** DAO */
    @Resource
    private ScoreBeanDao scoreBeanDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 成績データを生成します。
     *
     * @param candiUnivList 志望大学リスト
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 成績データ
     */
    public Map<Integer, ScoreBean> createScoreBeanList(List<SelstdMainCandidateUnivBean> candiUnivList, Integer grade, String cls, ExamBean exam) {
        ExamBean dockingExam = null;
        String docExamStr = null;
        /* CEFRスコア */
        List<EngSSScoreData> cefrData = new ArrayList<EngSSScoreData>();
        /* CEFR成績リスト */
        /* List<ExmntnMainCefrRecordBean> cefrList = null; */
        /* 国語記述リスト */
        /* List<ExmntnMainJpnRecordBean> jpnList = null; */

        /* 対象模試の科目成績リストを模試、学年、クラスで取得する */
        List<BZSubjectRecord> recordAll = scoreBeanDao.selectSubRecordListByGradeClass(grade, cls, exam);

        /* ドッキング先模試の科目成績リストを模試、学年、クラスで取得する */
        List<BZSubjectRecord> dockingRecordAll = null;

        dockingRecordAll = scoreBeanDao.selectSubRecordListByGradeClassDoc(grade, cls, exam);

        /* 判定用の成績データを生成する */
        if (ExamUtil.isMark(exam)) {
            /* 英語認定試験情報を模試、学年、クラスで取得する */
            /* cefrList = scoreBeanDao.selectCefrRecordList(grade, cls, exam); */
            /* 国語記述情報を模試、学年、クラスで取得する */
            /* jpnList = scoreBeanDao.selectJpnRecordList(grade, cls, exam); */
        } else {
            /* 英語認定試験情報を模試、学年、クラスで取得する */
            /* cefrList = scoreBeanDao.selectCefrRecordList(grade, cls, dockingExam); */
            /* 国語記述情報を模試、学年、クラスで取得する */
            /* jpnList = scoreBeanDao.selectJpnRecordList(grade, cls, dockingExam); */
        }

        Map<Integer, ScoreBean> result = new HashMap<Integer, ScoreBean>();

        for (SelstdMainCandidateUnivBean record : candiUnivList) {
            /* 個人IDごとに成績データを生成する */
            if (!result.containsKey(record.getIndividualId())) {
                /* 個人の対象模試成績データを取得する */
                List<BZSubjectRecord> recordList = recordAll.stream().filter(e -> e.getIndividualId().equals(record.getIndividualId())).collect(Collectors.toList());
                List<BZSubjectRecord> dockingRecordList = null;
                if (recordList.size() > 0) {
                    /* 個人のドッキング先模試成績データを取得する */
                    dockingRecordList = dockingRecordAll.stream().filter(e -> e.getIndividualId().equals(record.getIndividualId())).collect(Collectors.toList());
                    if (dockingRecordList.size() > 0) {
                        docExamStr = dockingRecordList.get(0).getExamCd();
                        dockingExam = ExamUtil.findExamBean(docExamStr, systemDto);
                    }
                }
                /* 模試データ、成績データを設定 */
                ExamBean markExam = dockingExam;
                ExamBean wrtnExam = exam;
                List<BZSubjectRecord> markSubject = dockingRecordList;
                List<BZSubjectRecord> wrtnSubject = recordList;
                /* 共通テスト模試の場合 */
                if (ExamUtil.isMark(exam)) {
                    markExam = exam;
                    wrtnExam = dockingExam;
                    markSubject = recordList;
                    wrtnSubject = dockingRecordList;

                } else {
                    markExam = dockingExam;
                    wrtnExam = exam;
                    markSubject = dockingRecordList;
                    wrtnSubject = recordList;
                }

                /* 個人の英語認定試験情報を取得する */
                /*
                ExmntnMainCefrRecordBean cefr = cefrList.stream().filter(e -> e.getIndividualId().equals(record.getIndividualId())).findFirst().orElse(null);
                if (cefr != null) {
                */
                /* 判定処理に渡す英語認定試験情報を作成 */
                /*
                cefrData = createCefrRecord(cefr);
                }
                */

                /* 個人の国語記述情報を取得する */
                /* ExmntnMainJpnRecordBean jpn = jpnList.stream().filter(e -> e.getIndividualId().equals(record.getIndividualId())).findFirst().orElse(null);
                if (jpn != null) {
                    /* 国語記述情報を成績データに追加
                    /* addSubjectRecordJpn(markSubject, jpn);
                }
                */
                /* 個人ごとに成績データをマップに格納 */
                result.put(record.getIndividualId(), new ScoreBean(markExam, markSubject, wrtnExam, wrtnSubject, cefrData));
            }
        }

        return result;
    }

    /**
     * 判定用の成績データを生成します。
     *
     * @param exam 対象模試
     * @param id 個人ID
     * @return {@link ScoreBean}
     */
    public ScoreBean createScoreBean(ExamBean exam, Integer id) {
        /* CEFRスコア */
        List<EngSSScoreData> cefrData = new ArrayList<EngSSScoreData>();

        /* ドッキングチェック用 */
        ExamBean dockingExam = null;

        /* CEFR成績を取得する */
        /* ExmntnMainCefrRecordBean cefr = null; */

        /* 国語記述を取得する */
        /* ExmntnMainJpnRecordBean jpnDesc = null; */

        /* 対象模試の科目成績リストを取得する */
        List<BZSubjectRecord> recordList = scoreBeanDao.selectSubRecordList(id, exam);

        /* ドッキング先模試の科目成績リストを取得する */
        List<BZSubjectRecord> dockingRecordList = null;

        dockingExam = ExamUtil.findDockingExamStudentB(exam, id, scoreBeanDao, systemDto);
        if (dockingExam != null) {
            dockingRecordList = scoreBeanDao.selectSubRecordList(id, dockingExam);
        }
        /* 判定用の成績データを生成する */
        if (ExamUtil.isMark(exam)) {
            /* マーク模試 */
            /* CEFR成績リストを取得する */
            /*
            cefr = scoreBeanDao.selectCefrRecord(id, exam);
            if (cefr == null) {
                cefr = new ExmntnMainCefrRecordBean();
            }
            cefrData = createCefrRecord(cefr);
            */
            /* 国語記述リストを取得する */
            /*
            jpnDesc = scoreBeanDao.selectJpnRecord(id, exam);
            if (jpnDesc == null) {
                jpnDesc = new ExmntnMainJpnRecordBean();
            }
            addSubjectRecordJpn(recordList, jpnDesc);
            */

            return new ScoreBean(exam, recordList, dockingExam, dockingRecordList, cefrData);
        } else {
            /* 記述模試 */
            /* CEFR成績リストを取得する */
            /*
            cefr = scoreBeanDao.selectCefrRecord(id, dockingExam);
            if (cefr == null) {
                cefr = new ExmntnMainCefrRecordBean();
            }
            cefrData = createCefrRecord(cefr);
            */
            /* 国語記述リストを取得する */
            /*
            jpnDesc = scoreBeanDao.selectJpnRecord(id, dockingExam);
            if (jpnDesc == null) {
                jpnDesc = new ExmntnMainJpnRecordBean();
            }
            addSubjectRecordJpn(recordList, jpnDesc);
            */
            return new ScoreBean(dockingExam, dockingRecordList, exam, recordList, cefrData);
        }
    }

    /**
     * CEFR成績を成績リストに追加します。
     *
     * @param cefr CEFR成績
     * @return 英語認定試験スコアデータ
     */
    /*
    private List<EngSSScoreData> createCefrRecord(ExmntnMainCefrRecordBean cefr) {
        List<EngSSScoreData> list = CollectionsUtil.newArrayList();
        */
    /* 英語認定試験１が入力されている場合 */
    /*
    if(!StringUtil.isEmpty(cefr.getCefrExamCd1()))
    
    {
        EngSSScoreData cefrInfo = new EngSSScoreData();
        cefrInfo.setEngSSCode(cefr.getCefrExamCd1());
        cefrInfo.setEngSSLevelCode(cefr.getCefrExamLevel1());
        cefrInfo.setEngScore(cefr.getCefrScore1());
        list.add(cefrInfo);
    }
    */
    /* 英語認定試験２が入力されている場合 */
    /*
    if(!StringUtil.isEmpty(cefr.getCefrExamCd2()))
    
    {
        EngSSScoreData cefrInfo2 = new EngSSScoreData();
        cefrInfo2.setEngSSCode(cefr.getCefrExamCd2());
        cefrInfo2.setEngSSLevelCode(cefr.getCefrExamLevel2());
        cefrInfo2.setEngScore(cefr.getCefrScore2());
        list.add(cefrInfo2);
    }return list;
    }
    */

    /**
     * 国語記述の成績を成績リストに追加します。
     *
     * @param list 成績リスト
     * @param jpnDesc 国語記述List
     */
    /*
    private void addSubjectRecordJpn(List<BZSubjectRecord> list, ExmntnMainJpnRecordBean jpnDesc) {
        BZSubjectRecord jpnrec = new BZSubjectRecord();
        jpnrec.setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
        jpnrec.setScore(null);
        jpnrec.setCvsScore(null);
        jpnrec.setCDeviation(null);
        jpnrec.setScope("99");
        jpnrec.setJpnHyoka1(jpnDesc.getQuestion1());
        jpnrec.setJpnHyoka2(jpnDesc.getQuestion2());
        jpnrec.setJpnHyoka3(jpnDesc.getQuestion3());
        jpnrec.setJpnHyokaAll(jpnDesc.getJpnTotal());
        jpnrec.setSubjectAllotPnt(String.valueOf(jpnDesc.getSubAllotPnt()));
        list.add(jpnrec);
    }
    */
}

package jp.ac.kawai_juku.banzaisystem.framework.csv;

import java.util.List;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * CSV行のクリエイターです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class CsvRowCreator {

    /** 列のリスト */
    private final List<String> columnList = CollectionsUtil.newArrayList(20);

    /** 列データのバッファ */
    private final StringBuilder sb = new StringBuilder(128);

    /**
     * コンストラクタです。
     */
    CsvRowCreator() {
    }

    /**
     * 1文字追加します。
     * @param c 文字
     */
    void add(char c) {
        sb.append(c);
    }

    /**
     * 列を追加します。
     */
    void addColumn() {
        columnList.add(sb.toString());
        sb.setLength(0);
    }

    /**
     * CSV行オブジェクトを生成します。
     *
     * @param rowNum 行番号
     * @return 行オブジェクト
     */
    CsvRow create(int rowNum) {
        return new CsvRow(rowNum, columnList);
    }

}

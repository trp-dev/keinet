package jp.ac.kawai_juku.banzaisystem.exmntn.univ.component;

import java.awt.Font;

import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;

/**
 *
 * 個人成績・志望大学画面のテキストラベルです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
@SuppressWarnings("serial")
public class ExmntnUnivTextLabel extends TextLabel {

    /**
     * 総合評価文字列をセットします。
     *
     * @param text 評価文字列
     */
    public void setTotalRatingText(String text) {
        super.setText(text);
        if (text != null) {
            Font font = getFont();
            setFont(new Font(font.getName(), font.getStyle(), 32));
        }
    }

    /**
     * 二次評価文字列をセットします。
     *
     * @param text 評価文字列
     */
    public void setSecondRatingText(String text) {
        super.setText(text);
        Font font = getFont();
        setFont(new Font(font.getName(), font.getStyle(), 32));
    }

    /**
     * 総合評価文字列をセットします。
     *
     * @param text 評価文字列
     */
    public void setRatingText(String text) {
        super.setText(text);
        if (text != null) {
            setFontSize(text.length(), 1, 32, 22);
        }
    }

    /**
     * 文字数によってフォントサイズを変えます。
     *
     * @param textLength 文字数
     * @param checkLength フォント切替判定文字数
     * @param firstSize フォントサイズ
     * @param secondSize フォントサイズ
     *
     */
    private void setFontSize(int textLength, int checkLength, int firstSize, int secondSize) {

        Font font = getFont();
        setFont(new Font(font.getName(), font.getStyle(), textLength > checkLength ? secondSize : firstSize));

    }
}

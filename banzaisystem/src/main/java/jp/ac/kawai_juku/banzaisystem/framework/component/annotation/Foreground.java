package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * コンポーネントのForegroundを設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Foreground {

    /**
     * Rを設定します。
     *
     * @return R
     */
    int r();

    /**
     * Gを設定します。
     *
     * @return G
     */
    int g();

    /**
     * Bを設定します。
     *
     * @return B
     */
    int b();

}

package jp.ac.kawai_juku.banzaisystem.exmntn.subs.service;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.ImposingSubject;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubBit;
import jp.co.fj.kawaijuku.judgement.beans.detail.SubjectKey;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.data.UnivData;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 二次科目検索ロジックです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class SecondSubjectSearch {

    /** 科目キーセット */
    private final Set<SubjectKey> keySet;

    /** 課さないフラグ */
    private final Boolean isNotImpose;

    /** リスニングチェックフラグ */
    private final boolean listeningFlg;

    /**
     * コンストラクタです。
     *
     * @param searchBean 検索条件Bean
     */
    SecondSubjectSearch(ExmntnSubsUnivSearchInBean searchBean) {

        this.isNotImpose = searchBean.getNotImpose();

        Set<SubjectKey> set = CollectionsUtil.newHashSet();
        boolean flg = false;
        if (this.isNotImpose != null && !this.isNotImpose.booleanValue() && searchBean.getSecondList() != null) {
            for (SecondSubject subject : searchBean.getSecondList()) {
                for (SubjectKey key : subject.getKey()) {
                    for (Field field : SecondDetail.class.getDeclaredFields()) {
                        try {
                            field.setAccessible(true);
                            SubjectKey subKey = (SubjectKey) field.get(SecondDetail.class);
                            if (key.getKeyString().equals(subKey.getKeyString())) {
                                set.add(subKey);
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                if (subject == SecondSubject.LISTENING) {
                    flg = true;
                }
            }
        }

        this.keySet = Collections.unmodifiableSet(set);
        this.listeningFlg = flg;
    }

    /**
     * 検索を実行します。
     *
     * @param univ 大学データ
     * @return 検索結果
     */
    boolean search(UnivData univ) {
        if (isNotImpose == null) {
            return true;
        } else {
            return isNotImpose.booleanValue() ? checkNotImpose(univ) : checkSubject(univ);
        }
    }

    /**
     * 二次試験が課されていないことをチェックします。
     *
     * @param univ 大学データ
     * @return 課されていないならtrue
     */
    private boolean checkNotImpose(UnivData univ) {
        for (Object obj : univ.getDetailHash().values()) {
            UnivDetail detail = (UnivDetail) obj;
            if (detail.getSecond().getForDisplay().isImposed()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 二次科目の課し状態をチェックします。
     *
     * @param univ 大学データ
     * @return チェック結果
     */
    private boolean checkSubject(UnivData univ) {

        if (keySet.isEmpty()) {
            /* 科目チェックなし */
            return true;
        }

        for (Object obj : univ.getDetailHash().values()) {
            UnivDetail detail = (UnivDetail) obj;
            if (checkSubject(detail.getSecond().getForDisplay())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 二次科目の課し状態をチェックします。
     *
     * @param detail 大学詳細データ
     * @return チェック結果
     */
    private boolean checkSubject(CommonDetailHolder detail) {

        /* 必須科目をチェックする */
        for (Object obj : detail.getImposingSubjects().entrySet()) {
            Entry<?, ?> entry = (Entry<?, ?>) obj;
            SubjectKey key = (SubjectKey) entry.getKey();
            ImposingSubject subject = (ImposingSubject) entry.getValue();
            SubBit subBit = subject.getRequired();
            if (subBit.isImposed() && !checkChosen(key, subBit.getRange())) {
                return false;
            }
        }

        /* 選択科目をグループ別にチェックする */
        for (int groupIndex = 0; groupIndex < detail.getGroupLength(); groupIndex++) {
            if (!checkGroup(detail, groupIndex)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 指定した科目を選択しているかをチェックします。
     *
     * @param key 科目キー
     * @param range 範囲区分
     * @return チェック結果
     */
    private boolean checkChosen(SubjectKey key, int range) {
        if (!keySet.contains(key)) {
            return false;
        }
        /* リスニングは範囲区分をチェックする */
        if (!listeningFlg && key.equals(SecondDetail.KEY_ENGLISH) && range >= 2 && range <= 5) {
            return false;
        }
        return true;
    }

    /**
     * グループ内科目数を満たしているかをチェックします。
     *
     * @param detail 大学詳細データ
     * @param groupIndex グループ番号
     * @return チェック結果
     */
    private boolean checkGroup(CommonDetailHolder detail, int groupIndex) {

        int[] courseCounts = countCourse(detail, groupIndex);

        int groupCount = 0;
        for (int i = 0; i < courseCounts.length; i++) {
            /* その他はグループ教科数が存在しないため条件はなしとする */
            if (i == 5) {
                groupCount += courseCounts[i];
            } else if (courseCounts[i] > detail.getGroupCourseCount(groupIndex, i)) {
                /*
                 * 選択した科目がグループ別教科内科目数を超えていた場合は
                 * グループ別教科内科目数だけ加算する。
                 */
                groupCount += detail.getGroupCourseCount(groupIndex, i);
            } else {
                groupCount += courseCounts[i];
            }
        }

        return groupCount >= detail.getGroupCount(groupIndex);
    }

    /**
     * グループ・教科別に選択した科目数をカウントします。
     *
     * @param detail 大学詳細データ
     * @param groupIndex グループ番号
     * @return グループ・教科別科目数
     */
    private int[] countCourse(CommonDetailHolder detail, int groupIndex) {

        int[] courseCount = new int[detail.getCourseLength() + 1];

        Set<Pair<Integer, String>> pairSet = CollectionsUtil.newHashSet();
        for (Object obj : detail.getImposingSubjects().entrySet()) {
            Entry<?, ?> entry = (Entry<?, ?>) obj;
            SubjectKey key = (SubjectKey) entry.getKey();
            ImposingSubject subject = (ImposingSubject) entry.getValue();
            SubBit subBit = subject.getElective();
            if (!subBit.isImposed()) {
                continue;
            }
            if (subBit.getGroupIndex() != groupIndex) {
                continue;
            }
            /* ペアは後で処理する */
            if (subBit.isPair()) {
                pairSet.add(Pair.of(Integer.valueOf(key.getCourseIndex()), subBit.getBit()));
                continue;
            }
            /* 選択科目を選んでいるならカウントアップする */
            if (checkChosen(key, subBit.getRange())) {
                courseCount[key.getCourseIndex()]++;
            }
        }

        /* ビット別にペアをチェックする */
        for (Pair<Integer, String> pair : pairSet) {
            int takenCount = 0;
            for (Object obj : detail.getImposingSubjects().entrySet()) {
                Entry<?, ?> entry = (Entry<?, ?>) obj;
                SubjectKey key = (SubjectKey) entry.getKey();
                ImposingSubject subject = (ImposingSubject) entry.getValue();
                SubBit subBit = subject.getElective();
                if (!subBit.isImposed()) {
                    continue;
                }
                if (subBit.getGroupIndex() != groupIndex) {
                    continue;
                }
                if (!subBit.isPair()) {
                    continue;
                }
                if (!pair.getRight().equals(subBit.getBit())) {
                    continue;
                }
                if (checkChosen(key, subBit.getRange())) {
                    takenCount++;
                }
            }
            /* ペアを満たしていた場合のみ科目数をカウントをアップする */
            if (takenCount >= 2) {
                courseCount[pair.getLeft().intValue()]++;
            }
        }

        return courseCount;
    }

}

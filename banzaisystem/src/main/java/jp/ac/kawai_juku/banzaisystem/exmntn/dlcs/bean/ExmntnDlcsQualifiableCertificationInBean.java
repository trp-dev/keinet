package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean;

/**
 *
 * 取得可能資格ダイアログのBeanです。
 *
 *
 * <br />説明：取得可能資格ダイアログ上に表示する資格一覧の取得に用います。
 * <br />用途：検索条件の指定
 * <br />機能概要：指定の大学-学部-学科の情報を保持します。
 * <br />関連情報：詳細設計書（個人成績・志望大学）.xls -- SQL_C1_004
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnDlcsQualifiableCertificationInBean {

    /** 大学コード */
    private String univCd;

    /** 学部コード */
    private String facultyCd;

    /** 学科コード */
    private String deptCd;

    /**
     * 大学コードを返します。
     *
     * @return 大学コード
     */
    public String getUnivCd() {
        return univCd;
    }

    /**
     * 大学コードをセットします。
     *
     * @param univCd 大学コード
     */
    public void setUnivCd(String univCd) {
        this.univCd = univCd;
    }

    /**
     * 学部コードを返します。
     *
     * @return 学部コード
     */
    public String getFacultyCd() {
        return facultyCd;
    }

    /**
     * 学部コードをセットします。
     *
     * @param facultyCd 学部コード
     */
    public void setFacultyCd(String facultyCd) {
        this.facultyCd = facultyCd;
    }

    /**
     * 学科コードを返します。
     *
     * @return 学科コード
     */
    public String getDeptCd() {
        return deptCd;
    }

    /**
     * 学科コードをセットします。
     *
     * @param deptCd 学科コード
     */
    public void setDeptCd(String deptCd) {
        this.deptCd = deptCd;
    }

}

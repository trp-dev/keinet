package jp.ac.kawai_juku.banzaisystem.exmntn.dist.controller;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_ESTIMATE_DISP;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.CAPACITY_NONE;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.text.DecimalFormat;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCandidateDetailBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCapacityBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.service.ExmntnDistService;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.view.ExmntnDistView;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0201Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0301Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 志望大学学力分布画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistController extends SubController {

    /** 定員ラベル */
    private static final String CAPACITY_LABEL = "募集人員：%s";

    /** 志願者数フォーマット */
    private static final DecimalFormat DF = new DecimalFormat(",##0");

    /** Service */
    @Resource(name = "exmntnDistService")
    private ExmntnDistService service;

    /** View */
    @Resource(name = "exmntnDistView")
    private ExmntnDistView view;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** 志望大学詳細画面のView */
    @Resource
    private ExmntnUnivView exmntnUnivView;

    /** 志望大学詳細画面のヘルパー */
    @Resource(name = "exmntnUnivHelper")
    private ExmntnUnivHelper univHelper;

    /** 志望大学個人成績画面のDTO */
    @Resource
    private ExmntnMainDto exmntnMainDto;

    @Override
    @Logging(GamenId.C2)
    public void activatedAction() {
        initExamCombo();
        initCandidateRankComboBox();
    }

    /**
     * 志望大学詳細画面でマーク模試の「学力分布」ボタンを押した場合の {@link SubController#activatedAction()} です。
     */
    @Logging(GamenId.C2)
    public void activatedMarkAction() {
        initExamCombo();
        /* マーク模試を選択する */
        if (view.examComboBox.getItemCount() > 1) {
            view.examComboBox.setSelectedIndexSilent(1);
        }
        initCandidateRankComboBox();
    }

    /**
     * 志望大学詳細画面で記述模試の「学力分布」ボタンを押した場合の {@link SubController#activatedAction()} です。
     */
    @Logging(GamenId.C1)
    public void activatedWrittenAction() {
        initExamCombo();
        /* 記述模試を選択する */
        if (view.examComboBox.getItemCount() > 1) {
            view.examComboBox.setSelectedIndexSilent(0);
        }
        initCandidateRankComboBox();
    }

    /**
     * 模試コンボボックスを初期化します。
     */
    private void initExamCombo() {
        List<ComboBoxItem<ExamBean>> itemList = CollectionsUtil.newArrayList();
        if (exmntnMainView.exmntnWritingComboBox.getSelectedValue() != null) {
            itemList.add(exmntnMainView.exmntnWritingComboBox.getSelectedItem());
        }
        if (exmntnMainView.exmntnMarkComboBox.getSelectedValue() != null) {
            itemList.add(exmntnMainView.exmntnMarkComboBox.getSelectedItem());
        }
        view.examComboBox.setItemList(itemList);
    }

    /**
     * 志望順位コンボボックスを初期化します。
     */
    public void initCandidateRankComboBox() {
        /* 志望大学詳細画面と同期する */
        view.candidateRankComboBox.copyFrom(exmntnUnivView.candidateRankComboBox);
    }

    /**
     * 志望順位コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult candidateRankComboBoxAction() {

        /* 志望大学詳細画面と同期する */
        exmntnUnivView.candidateRankComboBox.setSelectedItem(view.candidateRankComboBox.getSelectedItem());

        /* 選択中の志望大学詳細を表示する */
        showDetail();

        return null;
    }

    /**
     * 模試コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult examComboBoxAction() {
        showDetail();
        return null;
    }

    /**
     * 選択中の志望大学詳細を表示します。
     */
    private void showDetail() {

        /* 空欄の志望順位を削除する */
        univHelper.removeBlankComboBoxItem(view.candidateRankComboBox);

        /* 前の大学・次の大学のボタンの表示状態を更新 */
        univHelper.changeButtonStatus(view.candidateRankComboBox, view.beforeUnivButton, view.nextUnivButton);

        /* 対象模試を初期化する */
        ExamBean exam = view.examComboBox.getSelectedValue();

        if (view.candidateRankComboBox.getItemCount() > 0) {
            /* 志望大学がある場合 */
            ExmntnMainCandidateUnivBean univBean = view.candidateRankComboBox.getSelectedValue();

            /* 大学名を設定する */
            view.univNameLabel.replace(ObjectUtils.toString(univBean.getUnivName()) + " "
                    + ObjectUtils.toString(univBean.getFacultyName()) + " "
                    + ObjectUtils.toString(univBean.getDeptName()));

            /* 募集人員(定員)を設定する */
            view.capacityLabel.replace(createCapacityText(univBean));

            /* 学力分布情報を取得する */
            ExmntnDistBean bean = service.findDistBean(exam, exmntnMainDto.getJudgeScore(), univBean);

            /* 帯別人数グラフを初期化する */
            view.graphPanel.init(exam, bean);

            /* 平均換算得点／平均偏差値ラベルを設定する */
            setAvgScoreLabel(bean.isDeviationDistFlg() || (exam != null && ExamUtil.isWritten(exam)));

            /* 出願予定者/第1志望者ラベルを設定する */
            setFirstChoiceLabel();

            /* 志望者内訳(出願予定)を設定する */
            setCandidateDetailPlan(bean.getDetailPlan());

            /* 志望者内訳(総志望)を設定する */
            setCandidateDetailTotal(bean.getDetailTotal());
        } else {
            /* 志望大学が無い場合 */
            for (TextLabel label : view.findComponentsByClass(TextLabel.class)) {
                label.setText(StringUtils.EMPTY);
            }
            view.graphPanel.init(exam, null);
            setAvgScoreLabel(false);
            setFirstChoiceLabel();
        }
    }

    /**
     * 募集人員(定員)テキストを生成します。
     *
     * @param bean {@link UnivKeyBean}
     * @return 募集人員(定員)テキスト
     */
    private String createCapacityText(UnivKeyBean bean) {
        ExmntnDistCapacityBean capa = service.findCapacityBean(bean);
        String capacity;
        if (capa == null || capa.getCapacityFlag() == null || capa.getCapacityFlag().equals(CAPACITY_NONE)) {
            capacity = StringUtils.EMPTY;
        } else if (capa.getCapacityFlag().equals(CAPACITY_ESTIMATE)) {
            capacity = StringUtils.defaultString(capa.getOfferCapacity()) + CAPACITY_ESTIMATE_DISP;
        } else {
            capacity = StringUtils.defaultString(capa.getOfferCapacity());
        }
        return String.format(CAPACITY_LABEL, capacity);
    }

    /**
     * 平均換算得点／平均偏差値ラベルを設定します。
     *
     * @param dispDeviationFlg 偏差値帯表示フラグ
     */
    private void setAvgScoreLabel(boolean dispDeviationFlg) {
        if (dispDeviationFlg) {
            view.avgScoreLabel.setText(getMessage("label.exmntn.dist.avgDeviation"));
        } else {
            ExamBean exam = view.examComboBox.getSelectedValue();
            if (exam != null && ExamUtil.isCenter(exam)) {
                view.avgScoreLabel.setText(getMessage("label.exmntn.dist.avgPoint"));
            } else {
                view.avgScoreLabel.setText(getMessage("label.exmntn.dist.avgCvsPoint"));
            }
        }
    }

    /**
     * 出願予定者/第1志望者ラベルを設定します。
     */
    private void setFirstChoiceLabel() {
        ExamBean exam = view.examComboBox.getSelectedValue();
        String text;
        if (exam != null && ExamUtil.isCenter(exam)) {
            text = getMessage("label.exmntn.dist.applicationPlan");
        } else {
            text = getMessage("label.exmntn.dist.firstChoice");
        }
        view.numbersFirstChoiceLabel.setText(text);
        view.averageFirstChoiceLabel.setText(text);
    }

    /**
     * 志望者内訳(出願予定)を設定します。
     *
     * @param bean {@link ExmntnDistCandidateDetailBean}
     */
    private void setCandidateDetailPlan(ExmntnDistCandidateDetailBean bean) {
        setCandidateDetail(bean, view.numbersPlanALabel, view.numbersPlanSLabel, view.numbersPlanGLabel,
                view.averagePlanALabel, view.averagePlanSLabel, view.averagePlanGLabel);
    }

    /**
     * 志望者内訳(総志望)を設定します。
     *
     * @param bean {@link ExmntnDistCandidateDetailBean}
     */
    private void setCandidateDetailTotal(ExmntnDistCandidateDetailBean bean) {
        setCandidateDetail(bean, view.numbersTotalALabel, view.numbersTotalSLabel, view.numbersTotalGLabel,
                view.averageTotalALabel, view.averageTotalSLabel, view.averageTotalGLabel);
    }

    /**
     * 志望者内訳を設定します。
     *
     * @param bean {@link ExmntnDistCandidateDetailBean}
     * @param numbersA 志願者数(合計)ラベル
     * @param numbersS 志願者数(現役)ラベル
     * @param numbersG 志願者数(高卒)ラベル
     * @param averageA 平均換算得点／平均偏差値(合計)ラベル
     * @param averageS 平均換算得点／平均偏差値(現役)ラベル
     * @param averageG 平均換算得点／平均偏差値(高卒)ラベル
     */
    private void setCandidateDetail(ExmntnDistCandidateDetailBean bean, TextLabel numbersA, TextLabel numbersS,
            TextLabel numbersG, TextLabel averageA, TextLabel averageS, TextLabel averageG) {
        setCandidateDetail(numbersA, bean.getNumbersA());
        setCandidateDetail(numbersS, bean.getNumbersS());
        setCandidateDetail(numbersG, bean.getNumbersG());
        setCandidateDetail(averageA, bean.getAvgPntA());
        setCandidateDetail(averageS, bean.getAvgPntS());
        setCandidateDetail(averageG, bean.getAvgPntG());
    }

    /**
     * 志望者内訳−志望人数を設定します。
     *
     * @param label ラベル
     * @param value 値
     */
    private void setCandidateDetail(TextLabel label, Integer value) {
        label.setText(value == null ? "-" : DF.format(value.intValue()));
    }

    /**
     * 志望者内訳−平均換算得点／平均偏差値を設定します。
     *
     * @param label ラベル
     * @param value 値
     */
    private void setCandidateDetail(TextLabel label, Double value) {
        label.setText(value == null ? "-" : value.toString());
    }

    /**
     * 前の大学ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult beforeUnivButtonAction() {
        view.candidateRankComboBox.setSelectedIndex(view.candidateRankComboBox.getSelectedIndex() - 1);
        return null;
    }

    /**
     * 次の大学ボタンアクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult nextUnivButtonAction() {
        view.candidateRankComboBox.setSelectedIndex(view.candidateRankComboBox.getSelectedIndex() + 1);
        return null;
    }

    /**
     * 個人成績表ボタン押下アクションです。
     *
     * @return 個人成績表印刷画面
     */
    @Execute
    public ExecuteResult printPersonalScoreButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C2).addCreator(ExmntnUnivA0301Maker.class,
                        univHelper.createA0301InBean(true, true)).launch();
                return null;
            }
        };
    }

    /**
     * 大学詳細ボタン押下アクションです。
     *
     * @return 大学詳細印刷画面
     */
    @Execute
    public ExecuteResult printUnivDetailButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                new ExcelLauncher(GamenId.C2).addCreator(ExmntnUnivA0201Maker.class,
                        univHelper.createA0201InBean(view.candidateRankComboBox)).launch();
                return null;
            }
        };
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.ente.bean;

/**
 *
 * CEFRのBeanです。
 *
 *
 * <br />説明：CEFR情報です。
 * <br />用途：検索結果の格納
 * <br />機能概要：CEFRのレコード情報(生徒ID・認定試験コード)
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
public class ExmntnEnteCEFRIniBean {

    /** 生徒ID */
    private Integer studentId;

    /** 認定試験コード1 */
    private String examCd1;

    /** 認定試験コード2 */
    private String examCd2;

    /**
     * 生徒IDを返します。
     *
     * @return 生徒ID
     */
    public Integer getStudentId() {
        return studentId;
    }

    /**
     * 生徒IDをセットします。
     *
     * @param studentId 生徒ID
     */
    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    /**
     * 認定試験コード1を返します。
     *
     * @return 認定試験コード1
     */
    public String getExamCd1() {
        return examCd1;
    }

    /**
     * 認定試験コード1をセットします。
     *
     * @param examCd1 認定試験コード1
     */
    public void setExamCd1(String examCd1) {
        this.examCd1 = examCd1;
    }

    /**
     * 認定試験コード1を返します。
     *
     * @return 認定試験コード1
     */
    public String getExamCd2() {
        return examCd2;
    }

    /**
     * 認定試験コード2をセットします。
     *
     * @param examCd2 認定試験コード2
     */
    public void setExamCd2(String examCd2) {
        this.examCd2 = examCd2;
    }
}

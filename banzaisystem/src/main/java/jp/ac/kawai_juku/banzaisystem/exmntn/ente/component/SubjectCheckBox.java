package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.lang.reflect.Field;

import javax.swing.JCheckBox;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SearchSubject;
import jp.ac.kawai_juku.banzaisystem.framework.component.CheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 使用科目チェックボックスです。
 *
 *
 * @param <T> 使用科目の型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class SubjectCheckBox<T extends SearchSubject> extends CheckBox {

    /** フィールド名の接尾辞 */
    private static final String SUFFIX = "CheckBox";

    @Override
    public void initialize(Field field, AbstractView view) {

        if (!field.getName().endsWith(SUFFIX)) {
            throw new RuntimeException(String.format("%sのフィールド名は[***%s]とする必要があります。[%s]", getClass().getSimpleName(),
                    SUFFIX, field.getName()));
        }

        super.initialize(field, view);

        setText(getMessage("label." + view.getPkg() + "."
                + field.getName().substring(0, field.getName().length() - SUFFIX.length())));

        if (!getText().isEmpty()) {
            setVerticalTextPosition(JCheckBox.TOP);
            setHorizontalTextPosition(JCheckBox.CENTER);
            setIconTextGap(0);
            while (getPreferredSize().height < getBoxHeight()) {
                setIconTextGap(getIconTextGap() + 1);
            }
        }

        setSize(getPreferredSize());
    }

    /**
     * チェックボックスの高さを返します。
     *
     * @return チェックボックスの高さ
     */
    protected abstract int getBoxHeight();

    /**
     * 使用科目を返します。
     *
     * @return 使用科目
     */
    public abstract T getSubject();

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.plaf.LayerUI;

/**
 *
 * コンポーネント無効中にグレー表示するLayerUIです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class DisabledLayerUI extends LayerUI<JComponent> {

    @Override
    public void paint(Graphics g, JComponent c) {

        super.paint(g, c);

        if (((JLayer<?>) c).getView().isEnabled()) {
            return;
        }

        Graphics2D g2 = (Graphics2D) g.create();
        Composite urComposite = g2.getComposite();

        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .3f));
        g2.fillRect(0, 0, c.getWidth(), c.getHeight());
        g2.setComposite(urComposite);
        g2.dispose();
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        JLayer<?> jlayer = (JLayer<?>) c;
        jlayer.setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK
                | AWTEvent.MOUSE_WHEEL_EVENT_MASK | AWTEvent.KEY_EVENT_MASK);
    }

    @Override
    public void uninstallUI(JComponent c) {
        JLayer<?> jlayer = (JLayer<?>) c;
        jlayer.setLayerEventMask(0);
        super.uninstallUI(c);
    }

    @Override
    public void eventDispatched(AWTEvent e, JLayer<? extends JComponent> l) {
        if (!l.getView().isEnabled() && e instanceof InputEvent) {
            ((InputEvent) e).consume();
        }
    }

    @Override
    public void applyPropertyChange(PropertyChangeEvent pce, JLayer<? extends JComponent> l) {
        String cmd = pce.getPropertyName();
        if (cmd.equals("enabled")) {
            l.getGlassPane().setVisible(!(Boolean) pce.getNewValue());
        }
    }

}

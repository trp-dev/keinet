package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0102Maker;

/**
 *
 * �l���сE�u�]��w�ꗗ(�u�]9��w)�̃m�[�h�ł��B
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class A0102Node extends OutputExcelNode {

    /**
     * �R���X�g���N�^�ł��B
     */
    A0102Node() {
        super("�l���сE�u�]��w�ꗗ(�u�]9��w)");
    }

    @Override
    void output(OutputMainHelper helper) {
        new ExcelLauncher(GamenId.D).addCreator(
                SelstdDprtA0102Maker.class,
                new SelstdDprtExcelMakerInBean(helper.findTargetExam(), helper.findDockingExam(), helper
                        .findPrintTargetList())).launch();
    }

}

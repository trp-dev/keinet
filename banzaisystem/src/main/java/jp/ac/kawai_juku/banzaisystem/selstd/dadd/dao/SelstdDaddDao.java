package jp.ac.kawai_juku.banzaisystem.selstd.dadd.dao;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CANDIDATE_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXAM_PLAN_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.bean.SelstdDaddKojinBean;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 個人追加ダイアログのDAOです。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDaddDao extends BaseDao {

    /**
     * 学籍履歴情報内で指定の条件に該当する既存の登録情報をカウントします。
     *
     * @param nendo 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @param id 更新対象生徒の個人ID（生徒追加時はNULL）
     * @return 抽出条件に該当したレコード数
     */
    public long countRegisteredHistoryInfo(String nendo, Integer grade, String cls, String classNo, Integer id) {
        BeanMap param = new BeanMap();
        /* パラメータの識別子はSQL側の記述に一致させる。SQL：daddCountRegisteredHistoryInfo.sql */
        param.put("nendo", nendo);
        param.put("grade", grade);
        param.put("class", StringUtils.leftPad(cls, 2, '0'));
        param.put("classNo", classNo);
        param.put("id", id);
        /* 指定のパラメータでSQLを実行 */
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 個人情報を取得します。
     *
     * @param year 年度
     * @param id 個人ID
     * @return SelstdDaddKojinBean
     */
    public SelstdDaddKojinBean selectKojinBean(String year, Integer id) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("id", id);
        return jdbcManager.selectBySqlFile(SelstdDaddKojinBean.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 学籍基本情報を更新します。
     *
     * @param id 個人ID
     * @param nameKana カナ氏名
     */
    public void updateBasicInfo(Integer id, String nameKana) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("nameKana", nameKana);
        if (jdbcManager.updateBySqlFile(getSqlPath(), param).execute() != 1) {
            throw new MessageDialogException(getMessage("error.selstd.dadd.B-d3_E006"));
        }
    }

    /**
     * 学籍履歴情報を更新します。
     *
     * @param id 個人ID
     * @param nendo 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     */
    public void updateHistoryInfo(Integer id, String nendo, String grade, String cls, String classNo) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("year", nendo);
        param.put("grade", Integer.valueOf(grade));
        param.put("class", StringUtils.leftPad(cls, 2, '0'));
        param.put("classNo", StringUtils.leftPad(classNo, 5, '0'));
        /* 更新結果は学籍基本の更新時にチェックしているため、ここではチェックしない */
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報に紐付く主情報を検索します。<br>
     * 同一年度・学年・クラス・クラス番号の情報を取得します。<br>
     * カナ氏名の同一性検査は行いません。
     *
     * @param nendo 年度
     * @param subId 従情報の個人ID
     * @param grade 従情報の学年
     * @param cls 従情報のクラス
     * @param classNo 従情報のクラス番号
     * @return 主情報リスト
     */
    public List<SelstdDaddKojinBean> selectMainList(String nendo, Integer subId, Integer grade, String cls,
            String classNo) {
        BeanMap param = new BeanMap();
        param.put("year", nendo);
        param.put("subId", subId);
        param.put("grade", grade);
        param.put("class", StringUtils.leftPad(cls, 2, '0'));
        param.put("classNo", StringUtils.leftPad(classNo, 5, '0'));
        return jdbcManager.selectBySqlFile(SelstdDaddKojinBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 主情報の個表データのうち、従情報と模試が重複しているレコード数を返します。
     *
     * @param nendo 年度
     * @param mainId 主情報の個人ID
     * @param subId 従情報の個人ID
     * @return カウント結果
     */
    public long countIndividualRecord(String nendo, Integer mainId, Integer subId) {
        BeanMap param = new BeanMap();
        param.put("year", nendo);
        param.put("mainId", mainId);
        param.put("subId", subId);
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult().longValue();
    }

    /**
     * 主情報のシステム登録志望大学のうち、模試記入志望大学と重複するものを非表示にします。
     *
     * @param mainId 主情報の個人ID
     */
    public void updateCandidateUnivDispOff(Integer mainId) {
        jdbcManager.updateBySqlFile(getSqlPath(), mainId).execute();
    }

    /**
     * 従情報の志望大学データを主情報に更新します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void updateCandidateUniv(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 主情報の志望大学の表示順を振り直して取得します。
     *
     * @param mainId 主情報の個人ID
     * @return 志望大学リスト
     */
    public List<BeanMap> selectCandidateUnivList(Integer mainId) {
        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), mainId).getResultList();
    }

    /**
     * 従情報の模試記入志望大学データを主情報に更新します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void updateCandidateRating(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報の個表データを主情報に更新します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void updateIndividualRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報の成績データを主情報に更新します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void updateSubRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 指定生徒の最新受験模試（全ての模試を未受験なら最新公開模試）を取得します。
     *
     * @param id 個人ID
     * @param nendo 年度
     * @param examDiv 現在の模試区分
     * @return {@link ExamBean}
     */
    public ExamBean selectLatestExamBean(Integer id, String nendo, String examDiv) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("year", nendo);
        param.put("examDiv", examDiv);
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 指定生徒のシステム登録志望大学のうち、未受験かつ最新公開模試以外のレコードを削除します。
     *
     * @param id 個人ID
     * @param examBean {@link ExamBean}
     */
    public void deletePreviousCandidateUniv(Integer id, ExamBean examBean) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", examBean.getExamYear());
        param.put("examCd", examBean.getExamCd());
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 指定生徒の指定模試のシステム登録志望大学を未受験模試にコピーします。
     *
     * @param id 個人ID
     * @param examBean {@link ExamBean}
     */
    public void insertLatestCandidateUniv(Integer id, ExamBean examBean) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("examYear", examBean.getExamYear());
        param.put("examCd", examBean.getExamCd());
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 生徒1の未受験模試のシステム登録志望大学のうち、
     * 生徒2のシステム登録志望大学と重複するものを削除します。
     *
     * @param id1 生徒1の個人ID
     * @param id2 生徒2の個人ID
     */
    public void deleteDuplicatedCandidateUniv(Integer id1, Integer id2) {
        BeanMap param = new BeanMap();
        param.put("id1", id1);
        param.put("id2", id2);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 主従の志望大学で最大の表示順を取得します。
     *
     * @param param パラメータ（主従情報の個人ID）
     * @return 主従の志望大学で最大の表示順
     */
    public Integer selectCandidateUnivMaxDispNum(BeanMap param) {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 主従生徒が未受験である模試のシステム登録志望大学の志望順位を最大の表示順だけ加算します。<br>
     * ただし、主情報は従情報が受験している模試のみ加算します。
     *
     * @param mainId 主情報の個人ID
     * @param subId 従情報の個人ID
     * @param maxDispNum 最大の表示順
     */
    public void updateCandidateUnivDispNum(Integer mainId, Integer subId, Integer maxDispNum) {
        BeanMap param = new BeanMap();
        param.put("mainId", mainId);
        param.put("subId", subId);
        param.put("maxDispNum", maxDispNum);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 主情報のシステム登録志望大学のうち、登録上限数を超える分を削除します。
     *
     * @param mainId 主情報の個人ID
     */
    public void deleteExcessCandidateUniv(Integer mainId) {
        BeanMap param = new BeanMap();
        param.put("mainId", mainId);
        param.put("limitSize", CANDIDATE_UNIV_MAX);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報の受験予定大学のうち、主情報と重複するものを削除します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void deleteDuplicatedExamPlanUniv(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報の受験予定大学の志望順位を登録上限数だけ加算します。
     *
     * @param subId 従情報の個人ID
     */
    public void updateExamPlanUnivCandidateRank(Integer subId) {
        BeanMap param = new BeanMap();
        param.put("subId", subId);
        param.put("limitCandidateRank", EXAM_PLAN_UNIV_MAX);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 従情報の受験予定大学を主情報に更新します。
     *
     * @param param パラメータ（主従情報の個人ID）
     */
    public void updateExamPlanUniv(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 主情報の受験予定大学の志望順位を振り直して取得します。
     *
     * @param mainId 主情報の個人ID
     * @return 受験予定大学配列
     */
    public ExmntnScdlBean[] selectExamPlanUnivArray(Integer mainId) {
        List<ExmntnScdlBean> list =
                jdbcManager.selectBySqlFile(ExmntnScdlBean.class, getSqlPath(), mainId).getResultList();
        return list.toArray(new ExmntnScdlBean[list.size()]);
    }

    /**
     * 主情報の志望大学のうち、登録上限数を超える分を削除します。
     *
     * @param mainId 主情報の個人ID
     */
    public void deleteExcessExamPlanUniv(Integer mainId) {
        BeanMap param = new BeanMap();
        param.put("mainId", mainId);
        param.put("limitCandidateRank", EXAM_PLAN_UNIV_MAX);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.controller;

import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowEvent;

import javax.swing.SwingUtilities;

import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * ダイアログを閉じるアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CloseResult implements ExecuteResult {

    @Override
    public void process(AbstractController controller) {
        AbstractView view = controller.getView();
        if (!BZClassUtil.getOriginalClass(view.getClass()).isAnnotationPresent(Dialog.class)) {
            throw new RuntimeException("CloseResultはダイアログ画面のみ利用可能です。");
        }
        /* ×ボタン押下イベントを発生させる */
        Window window = SwingUtilities.getWindowAncestor(view.getView());
        Toolkit.getDefaultToolkit().getSystemEventQueue()
                .postEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.exception;

/**
 *
 * メッセージをダイアログ表示する例外です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MessageDialogException extends RuntimeException {

    /**
     * コンストラクタです。
     *
     * @param message メッセージ
     */
    public MessageDialogException(String message) {
        this(message, null);
    }

    /**
     * コンストラクタです。
     *
     * @param message メッセージ
     * @param cause 原因
     */
    public MessageDialogException(String message, Throwable cause) {
        super(message, cause);
    }

}

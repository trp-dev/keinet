package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 受験予定大学のDAO入力Beanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainExamPlanUnivDaoInBean extends UnivKeyBean {

    /** 個人ID */
    private Integer id;

    /** 年度 */
    private String year;

    /** 模試区分 */
    private String examDiv;

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public ExmntnMainExamPlanUnivDaoInBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 個人IDをセットします。
     *
     * @param id 個人ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 年度を返します。
     *
     * @return 年度
     */
    public String getYear() {
        return year;
    }

    /**
     * 年度をセットします。
     *
     * @param year 年度
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

}

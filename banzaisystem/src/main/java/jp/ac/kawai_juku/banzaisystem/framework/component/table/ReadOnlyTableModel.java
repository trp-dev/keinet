package jp.ac.kawai_juku.banzaisystem.framework.component.table;

/**
 *
 * 読み取り専用のテーブルモデルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ReadOnlyTableModel extends BZTableModel {

    /**
     * コンストラクタです。
     *
     * @param columnCount カラム数
     */
    public ReadOnlyTableModel(int columnCount) {
        super(columnCount);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

}

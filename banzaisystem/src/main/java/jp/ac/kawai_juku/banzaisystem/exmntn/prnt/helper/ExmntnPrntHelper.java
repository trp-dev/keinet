package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.helper;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.view.ExmntnPrntView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 一括出力画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnPrntHelper {

    /** View */
    @Resource(name = "exmntnPrntView")
    private ExmntnPrntView view;

    /** 志望大学詳細画面のヘルパー */
    @Resource(name = "exmntnUnivHelper")
    private ExmntnUnivHelper univHelper;

    /**
     * 志望順位コンボボックスを初期化します。
     */
    public void initCandidateRankComboBox() {
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
        int max = Math.min(univHelper.createCandidateUnivList().size(), 9);
        for (int i = 1; i <= max; i++) {
            itemList.add(new ComboBoxItem<Integer>(Integer.valueOf(i)));
        }
        view.univComboBox.setItemList(itemList);
    }

}

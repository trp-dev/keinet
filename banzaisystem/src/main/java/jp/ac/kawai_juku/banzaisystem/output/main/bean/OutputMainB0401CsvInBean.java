package jp.ac.kawai_juku.banzaisystem.output.main.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.CsvMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;

/**
 *
 * 志望大学・評価一覧のCSV帳票メーカーの入力Beanです。<br/>
 * 画面＜帳票出力・CSVファイルの志望大学・評価一覧＞上の項目の情報を格納し、
 * OutputMainB0401Maker#outputData に渡される事を前提に作成されています。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainB0401CsvInBean implements CsvMakerInBean {

    /** 対象模試 */
    private final ExamBean exam;

    /** 対象模試のドッキング先模試 */
    private final ExamBean dockingExam;

    /** 個人IDリスト */
    private final List<Integer> idList;

    /** 判定評価：A判定 */
    private final boolean ratingAFlag;

    /** 判定評価：B判定 */
    private final boolean ratingBFlag;

    /** 判定評価：C判定 */
    private final boolean ratingCFlag;

    /** 判定評価：D判定 */
    private final boolean ratingDFlag;

    /** 判定評価：E判定 */
    private final boolean ratingEFlag;

    /** 大学指定：大学区分指定 */
    private final boolean univDivFlag;

    /** 大学指定：大学名指定 */
    private final boolean univNameFlag;

    /** 大学区分指定：国公立大学 */
    private final boolean nationalFlag;

    /** 大学区分指定：センター利用私大・短大 */
    private final boolean centerFlag;

    /** 大学区分指定：私立大学 */
    private final boolean privateFlag;

    /** 大学区分指定：短期大学・その他 */
    private final boolean collegeFlag;

    /** 大学名指定：大学CDリスト */
    private final List<String> univCdList;

    /**
     * コンストラクタです。
     *
     * @param exam 対象模試
     * @param dockingExam 対象模試のドッキング先模試
     * @param idList 個人IDリスト
     * @param ratingAFlag 判定評価：A判定 の選択状態
     * @param ratingBFlag 判定評価：B判定 の選択状態
     * @param ratingCFlag 判定評価：C判定 の選択状態
     * @param ratingDFlag 判定評価：D判定 の選択状態
     * @param ratingEFlag 判定評価：E判定 の選択状態
     * @param univDivFlag 大学指定：大学区分指定 の選択状態
     * @param univNameFlag 大学指定：大学名指定 の選択状態
     * @param nationalFlag 大学区分指定：国公立大学 の選択状態
     * @param centerFlag 大学区分指定：センター利用私大・短大 の選択状態
     * @param privateFlag 大学区分指定：私立大学 の選択状態
     * @param collegeFlag 大学区分指定：短期大学・その他 の選択状態
     * @param univCdList 大学名指定：大学CDリスト
     */
    public OutputMainB0401CsvInBean(ExamBean exam, ExamBean dockingExam, List<Integer> idList, boolean ratingAFlag,
            boolean ratingBFlag, boolean ratingCFlag, boolean ratingDFlag, boolean ratingEFlag, boolean univDivFlag,
            boolean univNameFlag, boolean nationalFlag, boolean centerFlag, boolean privateFlag, boolean collegeFlag,
            List<String> univCdList) {
        this.exam = exam;
        this.dockingExam = dockingExam;
        this.idList = idList;
        this.ratingAFlag = ratingAFlag;
        this.ratingBFlag = ratingBFlag;
        this.ratingCFlag = ratingCFlag;
        this.ratingDFlag = ratingDFlag;
        this.ratingEFlag = ratingEFlag;
        this.univDivFlag = univDivFlag;
        this.univNameFlag = univNameFlag;
        this.nationalFlag = nationalFlag;
        this.centerFlag = centerFlag;
        this.privateFlag = privateFlag;
        this.collegeFlag = collegeFlag;
        this.univCdList = univCdList;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public ExamBean getExam() {
        return exam;
    }

    /**
     * 対象模試のドッキング先模試を返します。
     *
     * @return 対象模試のドッキング先模試
     */
    public ExamBean getDockingExam() {
        return dockingExam;
    }

    /**
     * 個人IDリストを返します。
     *
     * @return 個人IDリスト
     */
    public List<Integer> getIdList() {
        return idList;
    }

    /**
     * 判定評価：A判定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isRatingA() {
        return ratingAFlag;
    }

    /**
     * 判定評価：B判定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isRatingB() {
        return ratingBFlag;
    }

    /**
     * 判定評価：C判定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isRatingC() {
        return ratingCFlag;
    }

    /**
     * 判定評価：D判定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isRatingD() {
        return ratingDFlag;
    }

    /**
     * 判定評価：E判定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isRatingE() {
        return ratingEFlag;
    }

    /**
     * 大学指定：大学区分指定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isUnivDiv() {
        return univDivFlag;
    }

    /**
     * 大学指定：大学名指定 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isUnivName() {
        return univNameFlag;
    }

    /**
     * 大学区分指定：国公立大学 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isNational() {
        return nationalFlag;
    }

    /**
     * 大学区分指定：センター利用私大・短大 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isCenter() {
        return centerFlag;
    }

    /**
     * 大学区分指定：私立大学 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isPrivate() {
        return privateFlag;
    }

    /**
     * 大学区分指定：短期大学・その他 の選択状態を返します。
     *
     * @return true:選択, false:非選択
     */
    public boolean isCollege() {
        return collegeFlag;
    }

    /**
     * 大学名指定：大学CDリスト を返します。
     *
     * @return 大学CDリスト
     */
    public List<String> getUnivCdList() {
        return univCdList;
    }

}

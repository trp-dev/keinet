package jp.ac.kawai_juku.banzaisystem.exmntn.main.service;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ENGLISH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LISTENING;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONETWO;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHTWO;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WRITING;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSaveScoreDaoInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSubjectRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dao.ExmntnMainDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績・志望大学画面のサービスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnMainService {

    /** DAO */
    @Resource
    private ExmntnMainDao exmntnMainDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** 個人成績画面の成績に関するヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /**
     * 個人リストを取得します。
     *
     * @param idList 個人IDリスト
     * @return 個人リスト
     */
    public List<ExmntnMainKojinBean> findKojinList(List<Integer> idList) {
        if (idList.isEmpty()) {
            return Collections.emptyList();
        } else {
            return exmntnMainDao.selectKojinList(systemDto.getUnivYear(), idList);
        }
    }

    /**
     * 科目成績マップを返します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 科目成績リスト
     */
    public Map<String, ExmntnMainSubjectRecordBean> findSubRecordMap(Integer id, ExamBean exam) {
        Map<String, ExmntnMainSubjectRecordBean> map = CollectionsUtil.newHashMap();
        for (ExmntnMainSubjectRecordBean rec : exmntnMainDao.selectSubRecordList(id, exam)) {
            map.put(rec.getSubCd(), rec);
        }
        return map;
    }

    /**
     * CEFRのスコアを返します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return CEFRスコア
     */
    /*
    public List<ExmntnMainCefrRecordBean> findCefrRecordList(Integer id, ExamBean exam) {
        List<ExmntnMainCefrRecordBean> rslt = null;
        rslt = exmntnMainDao.selectCefrRecordList(id, exam);
        if (rslt.size() == 0) {
            ExmntnMainCefrRecordBean cefrdata = new ExmntnMainCefrRecordBean();
            rslt.add(cefrdata);
            rslt.get(0).setIndividualId(id);
            rslt.get(0).setExamYear(exam.getExamYear());
            rslt.get(0).setExamDiv(exam.getExamCd());
            rslt.get(0).setCefrExamCd1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamCd2(StringUtils.EMPTY);
            rslt.get(0).setCefrExamName1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamName2(StringUtils.EMPTY);
            rslt.get(0).setCefrExamNameAbbr1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamNameAbbr2(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevel1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevel2(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevelName1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevelName2(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevelNameAbbr1(StringUtils.EMPTY);
            rslt.get(0).setCefrExamLevelNameAbbr2(StringUtils.EMPTY);
            rslt.get(0).setCefrScore1(0.0);
            rslt.get(0).setCefrScore2(0.0);
            rslt.get(0).setCefrLevelCd1(StringUtils.EMPTY);
            rslt.get(0).setCefrLevelCd2(StringUtils.EMPTY);
            rslt.get(0).setCefrLevel1(StringUtils.EMPTY);
            rslt.get(0).setCefrLevel2(StringUtils.EMPTY);
            rslt.get(0).setCefrLevelAbbr1(StringUtils.EMPTY);
            rslt.get(0).setCefrLevelAbbr2(StringUtils.EMPTY);
            rslt.get(0).setScoreCorrectFlg1(StringUtils.EMPTY);
            rslt.get(0).setScoreCorrectFlg2(StringUtils.EMPTY);
            rslt.get(0).setCefrLevelCdCorrectFlg1(StringUtils.EMPTY);
            rslt.get(0).setCefrLevelCdCorrectFlg2(StringUtils.EMPTY);
            rslt.get(0).setExamResult1(StringUtils.EMPTY);
            rslt.get(0).setExamResult2(StringUtils.EMPTY);
            rslt.get(0).setExamResultCorrectFlg1(StringUtils.EMPTY);
            rslt.get(0).setExamResultCorrectFlg2(StringUtils.EMPTY);
            rslt.get(0).setUnknownScore1(StringUtils.EMPTY);
            rslt.get(0).setUnknownScore2(StringUtils.EMPTY);
            rslt.get(0).setUnknownCefr1(StringUtils.EMPTY);
            rslt.get(0).setUnknownCefr2(StringUtils.EMPTY);
        }
        return rslt;
    }
    */
    /**
     * 国語記述のスコアを返します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 国語記述スコア
     */

    /*
    public List<ExmntnMainJpnRecordBean> findJpnRecordList(Integer id, ExamBean exam) {
        List<ExmntnMainJpnRecordBean> rslt = null;
        rslt = exmntnMainDao.selectJpnRecordList(id, exam);
        ExmntnMainJpnRecordBean jpndata = new ExmntnMainJpnRecordBean();
        if (rslt.size() == 0) {
            rslt.add(jpndata);
            rslt.get(0).setIndividualId(id);
            rslt.get(0).setExamYear(exam.getExamYear());
            rslt.get(0).setExamDiv(exam.getExamCd());
            rslt.get(0).setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
            rslt.get(0).setQuestion1(StringUtils.EMPTY);
            rslt.get(0).setQuestion2(StringUtils.EMPTY);
            rslt.get(0).setQuestion3(StringUtils.EMPTY);
            rslt.get(0).setJpnTotal(StringUtils.EMPTY);
        }
        return rslt;
    }
    */
    /**
     * 配点を返します。
     *
     * @param subCd 科目コード
     * @param exam 対象模試
     * @return 配点
     */
    public Integer findScoringList(String subCd, ExamBean exam) {
        Integer rslt = 0;
        rslt = exmntnMainDao.selectScoringRecordList(subCd, exam);
        return rslt;
    }

    /**
     * 成績を保存します。
     *
     * @param id 個人ID
     * @param score 成績
     * @param dto {@link ExmntnMainDto}
     */
    @Lock
    public void saveRecord(int id, ScoreBean score, ExmntnMainDto dto) {

        /* マーク模試成績を保存する */
        if (score.getMarkExam() != null && dto.isChangeMarkScore()) {
            exmntnMainDao.deleteSubRecord(id, score.getMarkExam());
            if (score.getScore().getMark() != null) {
                saveRecord(id, score.getMarkExam(), score.getScore().getMark());
                registIndividualRecord(id, score.getMarkExam());
            }
        }

        /* マーク(CEFR)模試成績を保存する */
        /*
        if (score.getMarkExam() != null && dto.isChangeMarkCefrScore()) {
            exmntnMainDao.deleteCefrRecord(id, score.getMarkExam());
            if (score.getScore().getMark() != null) {
        */
        /* CEFR成績を保存する */
        /*
                saveCefrRecord();
            }
        }
        */

        /* マーク(国語記述)模試成績を保存する */
        /*
        if (score.getMarkExam() != null && dto.isChangeMarkJpnScore()) {
            exmntnMainDao.deleteJpnRecord(id, score.getMarkExam());
            if (score.getScore().getMark() != null) {
                /* 国語記述成績を保存する
                saveJpnRecord();
            }
        }
        */

        /* 記述模試成績を保存する */
        if (score.getWrittenExam() != null && dto.isChangeWrittenScore()) {
            exmntnMainDao.deleteSubRecord(id, score.getWrittenExam());
            if (score.getScore().getWrtn() != null) {
                saveRecord(id, score.getWrittenExam(), score.getScore().getWrtn());
                registIndividualRecord(id, score.getWrittenExam());
            }
        }
    }

    /**
     * 個表データを登録します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     */
    public void registIndividualRecord(int id, ExamBean exam) {
        if (exmntnMainDao.countIndividualRecord(id, exam) < 1) {
            exmntnMainDao.insertIndividualRecord(id, exam);
        }
    }

    /**
     * 成績を保存します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @param list 科目成績リスト
     */
    public void saveRecord(int id, ExamBean exam, List<?> list) {
        List<ExmntnMainSaveScoreDaoInBean> beanList = CollectionsUtil.newArrayList();
        for (Object obj : list) {
            if (obj instanceof BZSubjectRecord) {
                if (!((BZSubjectRecord) obj).getSubCd().equals(SUBCODE_CENTER_DEFAULT_JKIJUTSU)) {
                    beanList.add(createDaoInBean(id, exam, (BZSubjectRecord) obj));
                }
            }
        }
        if (!beanList.isEmpty()) {
            exmntnMainDao.insertSubRecord(beanList);
        }
    }

    /**
      * CEFR成績を保存します。
      *
      */
    /*
    public void saveCefrRecord() {
        ExmntnMainSaveCefrDaoInBean bean = new ExmntnMainSaveCefrDaoInBean();
        List<ExmntnMainCefrRecordBean> cefrList = systemDto.getCefrInfo();
        */
    /* CEFRが未設定の場合、保存しない */
    /*
    if(!dto.isChangeMarkCefrScore())
    
    {
        return;
    }
    */
    /* CEFRが未設定の場合、保存しない */
    /*
    if(cefrList.get(0).getCefrExamCd1().equals("")&&cefrList.get(0).getCefrExamCd2().equals(""))
    {
        return;
    }bean.setIndividualId(cefrList.get(0).getIndividualId());bean.setExamYear(cefrList.get(0).getExamYear());bean.setExamDiv(cefrList.get(0).getExamDiv());bean.setExamCd1(cefrList.get(0).getCefrExamCd1());bean.setCefrExamLevel1(cefrList.get(0).getCefrExamLevel1());bean.setCefrScore1(cefrList.get(0).getCefrScore1());bean.setScoreCorrectFlg1(cefrList.get(0).getScoreCorrectFlg1());bean.setCefrLevelCD1(cefrList.get(0).getCefrLevelCd1());bean.setCefrLevelCdCorrectFlg1(cefrList.get(0).getCefrLevelCdCorrectFlg1());bean.setExamResult1(cefrList.get(0).getExamResult1());bean.setExamResultCorrectFlg1(cefrList.get(0).getExamResultCorrectFlg1());bean.setUnknownScore1(cefrList.get(0).getUnknownScore1());bean.setUnknownCefr1(cefrList.get(0).getUnknownCefr1());bean.setExamCd2(cefrList.get(0).getCefrExamCd2());bean.setCefrExamLevel2(cefrList.get(0).getCefrExamLevel2());bean.setCefrScore2(cefrList.get(0).getCefrScore2());bean.setScoreCorrectFlg2(cefrList.get(0).getScoreCorrectFlg2());bean.setCefrLevelCD2(cefrList.get(0).getCefrLevelCd2());bean.setCefrLevelCdCorrectFlg2(cefrList.get(0).getCefrLevelCdCorrectFlg2());bean.setExamResult2(cefrList.get(0).getExamResult2());bean.setExamResultCorrectFlg2(cefrList.get(0).getExamResultCorrectFlg2());bean.setUnknownScore2(cefrList.get(0).getUnknownScore2());bean.setUnknownCefr2(cefrList.get(0).getUnknownCefr2());exmntnMainDao.insertCefrRecord(bean);
    }
    */

    /**
      * 国語記述成績を保存します。
      *
      */
    /*
    public void saveJpnRecord() {
        ExmntnMainSaveJpnDaoInBean bean = new ExmntnMainSaveJpnDaoInBean();
        List<ExmntnMainJpnRecordBean> jpnList = systemDto.getJpnInfo();
        /* 国語記述が未設定の場合、保存しない
        if (!dto.isChangeMarkJpnScore()) {
            return;
        }
        if (jpnList.get(0).getQuestion1().equals("") && jpnList.get(0).getQuestion2().equals("") && jpnList.get(0).getQuestion3().equals("")) {
            return;
        }
        bean.setIndividualId(jpnList.get(0).getIndividualId());
        bean.setExamYear(jpnList.get(0).getExamYear());
        bean.setExamDiv(jpnList.get(0).getExamDiv());
        bean.setSubCd(jpnList.get(0).getSubCd());
        bean.setQuestion1(jpnList.get(0).getQuestion1());
        bean.setQuestion2(jpnList.get(0).getQuestion2());
        bean.setQuestion3(jpnList.get(0).getQuestion3());
        bean.setJpnTotal(jpnList.get(0).getJpnTotal());
        exmntnMainDao.insertJpnRecord(bean);
    }
    */
    /**
     * DAO入力Beanを生成します。
     *
     * @param id 個人ID
     * @param exam 模試
     * @param rec 科目成績
     * @return {@link ExmntnMainSaveScoreDaoInBean}
     */
    private ExmntnMainSaveScoreDaoInBean createDaoInBean(int id, ExamBean exam, BZSubjectRecord rec) {
        ExmntnMainSaveScoreDaoInBean bean = new ExmntnMainSaveScoreDaoInBean();
        bean.setIndividualId(id);
        bean.setExamYear(exam.getExamYear());
        bean.setExamCd(exam.getExamCd());
        bean.setSubCd(rec.getSubCd());
        bean.setScore(rec.getScore());
        bean.setCvsScore(rec.getCvsScore());
        bean.setCDeviation(rec.getCDeviation());
        bean.setScope(rec.getScope());
        return bean;
    }

    /**
     * 科目成績リストに対して、判定前の事前処理（換算得点と偏差値の設定）を行います。
     *
     * @param exam 対象模試
     * @param list 科目成績リスト
     * @return 事前処理後の科目成績リスト
     */
    public List<BZSubjectRecord> preprocess(ExamBean exam, List<BZSubjectRecord> list) {

        if (exam == null || list == null) {
            return null;
        }

        if (!ExamUtil.isCenter(exam)) {
            /* 換算得点をセットする */
            setCvsScore(exam, list);
        }

        /* 一般私大判定用の集計科目成績を追加する */
        addAggregateScore(list);

        /* 偏差値をセットする */
        setDeviation(exam, list);

        return list;
    }

    /**
     * 科目成績に換算得点をセットします。
     *
     * @param exam 対象模試
     * @param list 科目成績リスト
     */
    private void setCvsScore(ExamBean exam, List<BZSubjectRecord> list) {

        Map<String, int[]> map = systemDto.getCvsScoreMap().get(exam.getExamCd());
        if (map == null) {
            throw new RuntimeException(String.format("センター得点換算表データがありません。[模試コード=%s]", exam.getExamCd()));
        }

        for (SubjectRecord rec : list) {
            String subCd = rec.getSubCd();
            if (subCd.compareTo("7000") < 0 && !subCd.equals(SUBCODE_CENTER_DEFAULT_JKIJUTSU)) {
                int[] table = map.get(subCd);
                if (table == null) {
                    throw new RuntimeException(String.format("センター得点換算表データがありません。[模試コード=%s][科目コード=%s]", exam.getExamCd(), subCd));
                }
                rec.setCvsScore(Integer.toString(table[Integer.parseInt(rec.getScore())]));
            }
        }
    }

    /**
     * 一般私大判定用の集計科目成績を追加します。
     *
     * @param list 科目成績リスト
     */
    private void addAggregateScore(List<BZSubjectRecord> list) {

        /* 英語筆記 */
        SubjectRecord writing = null;

        /* 英語リスニング */
        SubjectRecord listening = null;

        /* 数学�@ */
        SubjectRecord mathOne = null;

        /* 数学�A */
        SubjectRecord mathTwo = null;

        /* 現代文 */
        SubjectRecord literature = null;

        /* 古文 */
        SubjectRecord old = null;

        /* 漢文 */
        SubjectRecord chinese = null;

        for (SubjectRecord rec : list) {
            switch (rec.getSubCd()) {
                case SUBCODE_CENTER_DEFAULT_WRITING:
                    writing = rec;
                    break;
                case SUBCODE_CENTER_DEFAULT_LISTENING:
                    listening = rec;
                    break;
                case SUBCODE_CENTER_DEFAULT_LITERATURE:
                    literature = rec;
                    break;
                case SUBCODE_CENTER_DEFAULT_OLD:
                    old = rec;
                    break;
                case SUBCODE_CENTER_DEFAULT_CHINESE:
                    chinese = rec;
                    break;
                default:
                    int subCd = Integer.parseInt(rec.getSubCd());
                    if (JudgementUtil.match(subCd, "mark.subcd.math1") || JudgementUtil.match(subCd, "mark.subcd.math1A")) {
                        if (mathOne == null) {
                            mathOne = rec;
                        }
                    } else if (JudgementUtil.match(subCd, "mark.subcd.math2") || JudgementUtil.match(subCd, "mark.subcd.math2B")) {
                        if (mathTwo == null) {
                            mathTwo = rec;
                        }
                    }
                    break;
            }
        }

        /* 英語＋Ｌを追加する */
        addAggregateScore(list, SUBCODE_CENTER_DEFAULT_ENGLISH, false, writing, listening);

        /* 数学�@�Aを追加する */
        if (!addAggregateScore(list, SUBCODE_CENTER_DEFAULT_MATHONETWO, false, mathOne, mathTwo)) {
            /* 数学�@と数学�Aは数学�@�Aを追加しなかった場合のみ追加する */
            addAggregateScore(list, SUBCODE_CENTER_DEFAULT_MATHONE, false, mathOne);
            addAggregateScore(list, SUBCODE_CENTER_DEFAULT_MATHTWO, false, mathTwo);
        }

        /* 現古は漢文が無ければ追加しない */
        if (chinese != null) {
            addAggregateScore(list, SUBCODE_CENTER_DEFAULT_LITERATURE_OLD, true, literature, old);
        }

        /* 国語を追加する */
        addAggregateScore(list, SUBCODE_CENTER_DEFAULT_JAPANESE, true, literature, old, chinese);
    }

    /**
     * 一般私大判定用の集計科目成績を追加します。
     *
     * @param list 科目成績リスト
     * @param subCd 集計科目コード
     * @param isJapanese 国語教科フラグ
     * @param recs 集計対象の科目成績
     * @return 追加した場合はtrue
     */
    private boolean addAggregateScore(List<BZSubjectRecord> list, String subCd, boolean isJapanese, SubjectRecord... recs) {

        /* 得点合計 */
        int scoreTotal = 0;

        /* 換算得点合計 */
        int cvsTotal = -1;

        /* 未受験科目数 */
        int invalidCount = 0;

        ExamSubjectBean subject;
        for (SubjectRecord rec : recs) {
            if (rec == null) {
                invalidCount++;
            } else {
                scoreTotal += Integer.parseInt(rec.getScore());
                if (!StringUtils.isEmpty(rec.getCvsScore())) {
                    if (cvsTotal < 0) {
                        cvsTotal = 0;
                    }
                    cvsTotal += Integer.parseInt(rec.getCvsScore());
                }
            }
        }

        if (SUBCODE_CENTER_DEFAULT_JAPANESE.equals(subCd)) {
            /* 「国語」は全て未受験の場合のみ追加しない */
            if (invalidCount == recs.length) {
                return false;
            }
        } else if (invalidCount > 0) {
            /* 「国語」以外は未受験科目が存在する場合は追加しない */
            return false;
        }

        /* 英語＋Ｌは200点満点に換算する */
        if (SUBCODE_CENTER_DEFAULT_ENGLISH.equals(subCd)) {
            scoreTotal = (int) Math.round((double) scoreTotal);
        }

        String cvsScore = null;
        if (isJapanese && cvsTotal > -1) {
            /* 換算得点は「国語」「現古」のみ設定する */
            cvsScore = Integer.toString(cvsTotal);
        }

        if (scoreHelper.getMarkExam() == null || scoreHelper.getMarkExam().getExamCd() == null) {
            subject = null;
        } else {
            subject = systemDto.getExamSubjectMap().get(scoreHelper.getMarkExam().getExamCd()).get(subCd);
        }
        String sScore = null;
        if (subject == null || subject.getSuballotPnt() == null || subject.getSuballotPnt() == 0) {
            sScore = String.valueOf(Detail.ALLOT_NA);
        } else {
            sScore = String.valueOf(subject.getSuballotPnt());
        }
        list.add(new BZSubjectRecord(subCd, Integer.toString(scoreTotal), cvsScore, null, sScore));

        return true;
    }

    /**
     * 科目成績に偏差値をセットします。
     *
     * @param exam 対象模試
     * @param list 科目成績リスト
     */
    private void setDeviation(ExamBean exam, List<BZSubjectRecord> list) {

        Map<String, SubRecordAllBean> map = systemDto.getSubrecordAllMap().get(exam.getExamCd());
        if (map == null) {
            throw new RuntimeException(String.format("科目別成績（全国）データがありません。[模試コード=%s]", exam.getExamCd()));
        }

        for (SubjectRecord rec : list) {
            String subCd = rec.getSubCd();
            if (subCd.compareTo("7000") < 0) {
                if (subCd.compareTo("3915") == 0) {
                    rec.setCDeviation(null);
                } else {
                    SubRecordAllBean bean = map.get(subCd);
                    if (bean == null) {
                        throw new RuntimeException(String.format("科目別成績（全国）データがありません。[模試コード=%s][科目コード=%s]", exam.getExamCd(), subCd));
                    }
                    rec.setCDeviation(Double.toString(JudgementUtil.calcDeviation(Integer.parseInt(rec.getScore()), bean.getAvgPnt(), bean.getStdDeviation())));
                }
            }
        }
    }

}

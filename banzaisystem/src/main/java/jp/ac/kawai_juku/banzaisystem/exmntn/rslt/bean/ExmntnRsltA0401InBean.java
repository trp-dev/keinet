package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.bean;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;

/**
 *
 * Excel帳票メーカー(検索結果)の入力Beanです。
 *
 *
 * @author TOTEC)OZEKI.Hiroshige
 *
 */
public class ExmntnRsltA0401InBean implements ExcelMakerInBean {

    /** 対象模試 */
    private final ExamBean targetExam;

    /** 個人成績画面 */
    private final ExmntnMainView mainView;

    /** 検索結果画面 */
    private final ExmntnRsltView view;

    /** 検索条件Bean */
    private final ExmntnSubsUnivSearchInBean searchBean;

    /**
     * コンストラクタです。
     *
     * @param targetExam 対象模試
     * @param mainView 個人成績画面
     * @param view 検索結果画面
     * @param searchBean 検索条件Bean
     */
    public ExmntnRsltA0401InBean(ExamBean targetExam, ExmntnMainView mainView, ExmntnRsltView view,
            ExmntnSubsUnivSearchInBean searchBean) {
        this.targetExam = targetExam;
        this.mainView = mainView;
        this.view = view;
        this.searchBean = searchBean;
    }

    /**
     * 検索結果画面を返します。
     *
     * @return 検索結果画面
     */
    public ExmntnRsltView getView() {
        return view;
    }

    /**
     * 個人成績画面を返します。
     *
     * @return 個人成績画面
     */
    public ExmntnMainView getMainView() {
        return mainView;
    }

    /**
     * 検索条件Beanを返します。
     *
     * @return 検索条件Bean
     */
    public ExmntnSubsUnivSearchInBean getSearchBean() {
        return searchBean;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public ExamBean getTargetExam() {
        return targetExam;
    }

}

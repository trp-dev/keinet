package jp.ac.kawai_juku.banzaisystem.framework.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.REQUIRED_RECORD_VERSION;

import java.util.Collections;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.dao.UpgradeRecordDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 成績データバージョンアップサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UpgradeRecordService {

    /** バージョンアップ処理のSQLファイル名マップ */
    private static final Map<Integer, String[]> SQL_NAME_MAP;
    static {
        Map<Integer, String[]> map = CollectionsUtil.newHashMap();
        /* バージョン0→1 */
        map.put(1, new String[0]);
        /* バージョン1→2 */
        map.put(2, new String[] { "upgrade2CreateExamPlanUniv.sql", "upgrade2CreateExamPlanUnivIdx.sql" });
        SQL_NAME_MAP = Collections.unmodifiableMap(map);
    }

    /** DAO */
    @Resource
    private UpgradeRecordDao upgradeRecordDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * バージョンアップ処理を実行します。
     */
    @Lock
    public void upgrade() {
        for (int i = systemDto.getRecordInfo().getVersion(); i < REQUIRED_RECORD_VERSION; i++) {
            for (String name : SQL_NAME_MAP.get(Integer.valueOf(i + 1))) {
                upgradeRecordDao.execute("sql/upgrade/" + name);
            }
        }
        upgradeRecordDao.updateRecordInfoVersion(Integer.valueOf(REQUIRED_RECORD_VERSION));
        systemDto.getRecordInfo().setVersion(REQUIRED_RECORD_VERSION);
    }

}

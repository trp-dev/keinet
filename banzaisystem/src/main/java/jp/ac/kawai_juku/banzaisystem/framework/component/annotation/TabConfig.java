package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;

/**
 *
 * {@link jp.ac.kawai_juku.banzaisystem.framework.component.BZTabbedPane}の設定アノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface TabConfig {

    /**
     * タブ表示する画面のコントローラを設定します。
     *
     * @return タブコントローラー
     */
    Class<? extends SubController>[] value();

}

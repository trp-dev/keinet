package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.ScheduleType;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.helper.ExmntnScdlHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.view.ExmntnScdlView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.container.SingletonS2Container;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 受験スケジュールテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnScdlTable extends TableScrollPane {

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(68, 129, 254);

    /** ヘッダのボーダー色 */
    private static final Color HEADER_BORDER_COLOR = new Color(0, 51, 153);

    /** ヘッダの背景色 */
    private static final Color HEADER_BG_COLOR = new Color(132, 173, 255);

    /** 日程アイコンマップ */
    private final Map<ScheduleType, ImageIcon> scheduleIconMap;

    /** 曜日用フォーマット */
    private final FastDateFormat dowFormat = FastDateFormat.getInstance("EE");

    /** 日付用フォーマット */
    private final FastDateFormat dateFormat = FastDateFormat.getInstance("yyyyMMdd");

    /** 並べ替えコンボボックス */
    private ComboBox<Code> sortComboBox;

    /** 日付リスト */
    private List<String> dateList;

    /**
     * コンストラクタです。
     */
    public ExmntnScdlTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        setBorder(BorderFactory.createLineBorder(HEADER_BORDER_COLOR, 1));
        scheduleIconMap = createScheduleIconMap();
    }

    /**
     * 日程アイコンマップを生成します。
     *
     * @return 日程アイコンマップ
     */
    private Map<ScheduleType, ImageIcon> createScheduleIconMap() {
        Map<ScheduleType, ImageIcon> map = new HashMap<>();
        for (ScheduleType type : ScheduleType.values()) {
            map.put(type, new ImageIcon(ImageUtil.readImage(getClass(), "schedule/" + type + ".png")));
        }
        return Collections.unmodifiableMap(map);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        setColumnHeaderView(createColumnHeaderView());
        setCorner(JScrollPane.UPPER_LEFT_CORNER, createFixedColumnHeaderView());
        setViewportView(createTableView((ExmntnScdlView) view));
        setRowHeaderView(createFixedTableView(getFixedHeader()));

        /*
         * 固定領域をマウスドラッグでスクロールさせると、
         * 可変領域がスクロールせずに取り残されて行位置がずれるため、
         * 固定領域の位置変更を可変領域側に通知するようにする。
         * ※原因はSwingAPIのバグだと思われる。
         */
        getRowHeader().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JViewport leftView = (JViewport) e.getSource();
                JViewport rightView = getViewport();
                rightView.setViewPosition(new Point(rightView.getViewPosition().x, leftView.getViewPosition().y));
            }
        });

        /* 2つのテーブルのリスト選択モデルを同一にする */
        getFixedTable().setSelectionModel(getTable().getSelectionModel());
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView() {

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

        Integer year = SingletonS2Container.getComponent(SystemDto.class).getNendo();

        Calendar cal = Calendar.getInstance();
        cal.set(year.intValue() + 1, 0, 10);

        int totalWidth = 0;

        for (int i = 0; i < 3; i++) {
            int lastDay = cal.getActualMaximum(Calendar.DATE);
            int width = 18 * (i == 0 ? lastDay - 9 : lastDay);
            JLabel month = new JLabel(" " + (cal.get(Calendar.MONTH) + 1) + "月");
            month.setFont(BZFont.BOLD.getFont());
            month.setPreferredSize(new Dimension(width, 19));
            month.setOpaque(true);
            month.setForeground(HEADER_BORDER_COLOR);
            month.setBackground(HEADER_BG_COLOR);
            month.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, HEADER_BORDER_COLOR));
            panel.add(month);
            totalWidth += width;
            cal.add(Calendar.MONTH, 1);
        }

        dateList = new ArrayList<>();
        cal.set(Calendar.MONTH, 0);
        while (cal.get(Calendar.MONTH) != 3) {
            JLabel date = new JLabel(
                    "<HTML><CENTER>" + cal.get(Calendar.DATE) + "<BR>" + dowFormat.format(cal) + "</CENTER></HTML>");
            date.setHorizontalAlignment(SwingConstants.CENTER);
            date.setPreferredSize(new Dimension(18, 26));
            date.setOpaque(true);
            date.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, HEADER_BORDER_COLOR));
            date.setForeground(HEADER_BORDER_COLOR);
            date.setBackground(HEADER_BG_COLOR);
            panel.add(date);
            dateList.add(dateFormat.format(cal));
            cal.add(Calendar.DATE, 1);
        }

        panel.setPreferredSize(new Dimension(totalWidth, 45));

        return panel;
    };

    /**
     * 固定領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createFixedColumnHeaderView() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.add(createHeaderLabel("rank", CellAlignment.CENTER));
        panel.add(createHeaderLabel("univ", CellAlignment.LEFT));
        panel.add(createHeaderLabel("local", null));
        panel.add(createHeaderLabel("center", CellAlignment.CENTER));
        panel.add(createHeaderLabel("second", CellAlignment.CENTER));
        panel.add(createHeaderLabel("total", CellAlignment.CENTER));
        return panel;
    };

    /**
     * スクロール領域のテーブルを生成します。
     *
     * @param view {@link ExmntnScdlView}
     * @return テーブルを含むパネル
     */
    private JPanel createTableView(final ExmntnScdlView view) {

        final JTable table = new BZTable(new ReadOnlyTableModel(dateList.size())) {

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int i = 0; i < getColumnCount(); i++) {
                    x += 18;
                    g.drawLine(x - 1, 0, x - 1, getHeight() - 1);
                }
                for (int i = 1; i <= getRowCount(); i++) {
                    int y = getRowHeight() * i - 1;
                    g.drawLine(0, y, getWidth() - 1, y);
                }
            }

            @Override
            public Class<?> getColumnClass(int column) {
                return ImageIcon.class;
            }

        };

        table.setFocusable(false);
        table.setRowHeight(56);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.addMouseListener(new MouseListener(table));
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                /* ボタンの状態を変更する */
                int count = table.getSelectedRowCount();
                if (count == 0) {
                    view.deleteButton.setEnabled(false);
                    view.upButton.setEnabled(false);
                    view.downButton.setEnabled(false);
                    view.detailJudgeButton.setEnabled(false);
                } else if (count == 1) {
                    boolean rankFlg = sortComboBox.getSelectedValue() == Code.SCHEDULE_ORDER_RANK;
                    view.deleteButton.setEnabled(true);
                    view.upButton.setEnabled(rankFlg && table.getSelectedRow() > 0);
                    view.downButton.setEnabled(rankFlg && table.getSelectedRow() + 1 < table.getRowCount());
                    view.detailJudgeButton.setEnabled(true);
                } else {
                    view.deleteButton.setEnabled(true);
                    view.upButton.setEnabled(false);
                    view.downButton.setEnabled(false);
                    view.detailJudgeButton.setEnabled(true);
                }
            }
        });

        TableColumnModel columnModel = table.getColumnModel();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            columnModel.getColumn(i).setPreferredWidth(18);
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * 固定領域のテーブルを生成します。
     *
     * @param header ヘッダ
     * @return テーブルを含むパネル
     */
    private JPanel createFixedTableView(final JPanel header) {

        final DefaultTableModel model = new BZTableModel(header.getComponentCount() + 1) {

            /** ヘルパー */
            private final ExmntnScdlHelper helper = SingletonS2Container.getComponent(ExmntnScdlHelper.class);

            @Override
            public void fireTableCellUpdated(int row, int column) {

                super.fireTableCellUpdated(row, column);

                /* 受験予定大学の地方フラグを書き換えておく */
                boolean value = ((Boolean) getValueAt(row, column)).booleanValue();
                ExmntnScdlBean bean = (ExmntnScdlBean) getValueAt(row, 0);
                bean.setProvincesFlg(FlagUtil.toString(value));

                /* ヘルパーを呼び出す */
                helper.provincesCheckBoxAction(bean);
            }

        };

        JTable table = new BZTable(model) {

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (Component c : header.getComponents()) {
                    x += c.getWidth();
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
                for (int i = 1; i <= getRowCount(); i++) {
                    int y = getRowHeight() * i - 1;
                    g.drawLine(0, y, getWidth() - 1, y);
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                /* 地方列のみ編集可能とする */
                return column == 2 && model.getValueAt(row, 3) != null;
            }

            @Override
            public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
                /* 地方列のみ選択不可とする */
                if (columnIndex != 2 || model.getValueAt(rowIndex, 3) == null) {
                    super.changeSelection(rowIndex, columnIndex, toggle, extend);
                }
            }

            @Override
            public Class<?> getColumnClass(int column) {
                /* 地方列がチェックボックスで描画されるようにする */
                return column == 2 ? Boolean.class : super.getColumnClass(column);
            }

            @Override
            public TableCellRenderer getCellRenderer(int row, int column) {
                if (column == 2 && model.getValueAt(row, 3) == null) {
                    /* 地方がNULLの場合は、チェックボックスではなく空欄になるようにする */
                    return getDefaultRenderer(Object.class);
                } else {
                    return super.getCellRenderer(row, column);
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(56);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.addMouseListener(new MouseListener(table));

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        Component[] components = header.getComponents();
        for (int i = 0; i < components.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(components[i].getWidth());
            CellAlignment align = ((LabelHeader) components[i]).getAlignment();
            if (align != null) {
                columnModel.getColumn(i).setCellRenderer(align.getRenderer());
            }
        }

        /* 大学／学部／学科・日程列のセルレンダラをセットする */
        columnModel.getColumn(1).setCellRenderer(new NameCellRenderer());

        /* 地方列のセルエディタをセットする */
        columnModel.getColumn(2).setCellEditor(new DefaultCellEditor(new ProvincesCheckBox()));

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /** 大学／学部／学科・日程列のセルレンダラ */
    private final class NameCellRenderer implements TableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {
            return new NameCellPanel((ExmntnScdlBean) value, isSelected);
        }
    }

    /** 大学／学部／学科・日程列のセル描画パネル */
    private final class NameCellPanel extends JPanel {

        /**
         * コンストラクタです。
         *
         * @param bean {@link ExmntnScdlBean}
         * @param isSelected 選択フラグ
         */
        private NameCellPanel(ExmntnScdlBean bean, boolean isSelected) {

            super(null);

            /* 大学名ラベルを追加する */
            StringBuilder sb = new StringBuilder();

            if (bean.isCautionNeeded()) {
                sb.append("<html>").append("<span style=\"background-color:red;\" color=\"red\">!</span>")
                        .append("<span style=\"background-color:red;\" color=\"white\">!</span>")
                        .append("<span style=\"background-color:white;\" color=\"white\">!</span>");
            }

            sb.append(bean.getUnivName());

            if (bean.isCautionNeeded()) {
                sb.append("</html>");
            }

            addLabel(sb.toString(), isSelected, BZFont.DEFAULT, 8);

            if (!StringUtils.isEmpty(bean.getFacultyName())) {
                /* 学部名が存在する場合 */
                addLabel(bean.getFacultyName(), isSelected, BZFont.SCHEDULE_NAME, 24);
                addLabel(bean.getDeptName(), isSelected, BZFont.SCHEDULE_NAME, 38);
            } else {
                /* 学部名が存在しない場合 */
                addLabel(bean.getDeptName(), isSelected, BZFont.SCHEDULE_NAME, 24);
            }
        }

        /**
         * ラベルを追加します。
         *
         * @param text テキスト
         * @param isSelected 選択フラグ
         * @param font フォント
         * @param y Y座標
         */
        private void addLabel(String text, boolean isSelected, BZFont font, int y) {
            JLabel label = new JLabel(text);
            if (isSelected) {
                label.setForeground(UIManager.getColor("Table.selectionForeground"));
            }
            label.setFont(font.getFont());
            label.setLocation(5, y);
            label.setSize(label.getPreferredSize());
            add(label);
        }

    }

    /** 地方受験チェックボックス */
    private static final class ProvincesCheckBox extends JCheckBox {

        /**
         * コンストラクタです。
         */
        private ProvincesCheckBox() {
            setHorizontalAlignment(SwingConstants.CENTER);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            /* カラムの区切り線を描画する */
            g.setColor(SEPARATOR_COLOR);
            g.drawLine(getWidth() - 1, 0, getWidth() - 1, getHeight());
            g.drawLine(0, getHeight() - 1, getWidth() - 1, getHeight() - 1);
        }

    }

    /** テーブルをダブルクリックした時の処理を行うマウスリスナ */
    private final class MouseListener extends MouseAdapter {

        /** 志望大学詳細画面のヘルパー */
        private final ExmntnUnivHelper helper = SingletonS2Container.getComponent(ExmntnUnivHelper.class);

        /** テーブル */
        private final JTable table;

        /**
         * コンストラクタです。
         *
         * @param table テーブル
         */
        private MouseListener(JTable table) {
            this.table = table;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 2) {
                int row = table.rowAtPoint(e.getPoint());
                if (row >= 0) {
                    ExmntnScdlBean bean = (ExmntnScdlBean) getFixedTable().getModel().getValueAt(row, 0);
                    helper.dispSearchDetail(createCandidateUnivBean(bean));
                }
            }
        }
    };

    /**
     * 受験予定大学Beanから志望大学Beanを生成します。
     *
     * @param bean {@link ExmntnScdlBean}
     * @return {@link ExmntnMainCandidateUnivBean}
     */
    private ExmntnMainCandidateUnivBean createCandidateUnivBean(ExmntnScdlBean bean) {
        ExmntnMainCandidateUnivBean univBean = new ExmntnMainCandidateUnivBean(bean);
        univBean.setUnivName(bean.getUnivName());
        univBean.setFacultyName(bean.getFacultyName());
        univBean.setDeptName(bean.getDeptName());
        return univBean;
    }

    /**
     * 受験予定大学リストをセットします。
     *
     * @param examPlanUnivList 受験予定大学リスト
     */
    public final void setExamPlanUnivList(List<ExmntnScdlBean> examPlanUnivList) {
        sortAndDisplay(examPlanUnivList);
    }

    /**
     * 受験予定大学リストをソートしてテーブルに表示します。
     *
     * @param examPlanUnivList 受験予定大学リスト
     */
    private void sortAndDisplay(List<ExmntnScdlBean> examPlanUnivList) {
        sortExamPlanUnivList(examPlanUnivList, sortComboBox.getSelectedValue());
        ((BZTableModel) getFixedTable().getModel()).setDataVector(createFixedDataVector(examPlanUnivList));
        ((BZTableModel) getTable().getModel()).setDataVector(createDataVector(examPlanUnivList));
    }

    /**
     * 受験予定大学リストをソートします。
     *
     * @param examPlanUnivList 受験予定大学リスト
     * @param sortKey ソートキー
     */
    private void sortExamPlanUnivList(List<ExmntnScdlBean> examPlanUnivList, Code sortKey) {
        SingletonS2Container.getComponent(ExmntnScdlHelper.class).sortExamPlanUnivList(examPlanUnivList, sortKey);
    }

    /**
     * 受験予定大学リストをソートしてテーブルに表示します。
     */
    public final void sortAndDisplay() {
        sortAndDisplay(getExamPlanUnivList());
    }

    /**
     * 固定領域のデータリストを生成します。
     *
     * @param examPlanUnivList 受験予定大学リスト
     * @return 固定領域のデータリスト
     */
    private Vector<Vector<Object>> createFixedDataVector(List<ExmntnScdlBean> examPlanUnivList) {
        Vector<Vector<Object>> dataList = new Vector<>(examPlanUnivList.size());
        for (ExmntnScdlBean bean : examPlanUnivList) {
            Vector<Object> row = new Vector<>();
            row.add(bean);
            row.add(bean.getCandidateRank());
            row.add(bean);
            row.add(bean.getProvincesFlg() == null ? null
                    : Boolean.valueOf(FlagUtil.toBoolean(bean.getProvincesFlg())));
            row.add(bean.getCenterRating());
            row.add(bean.getSecondRating());
            row.add(bean.getTotalRating());
            dataList.add(row);
        }
        return dataList;
    }

    /**
     * スクロール領域のデータリストを生成します。
     *
     * @param examPlanUnivList 受験予定大学リスト
     * @return スクロール領域のデータリスト
     */
    private Vector<Vector<Object>> createDataVector(List<ExmntnScdlBean> examPlanUnivList) {
        Vector<Vector<Object>> dataList = new Vector<>(examPlanUnivList.size());
        for (ExmntnScdlBean bean : examPlanUnivList) {
            Vector<Object> row = new Vector<>(dateList.size());
            for (String date : dateList) {
                ScheduleType type = bean.getScheduleMap().get(date);
                row.add(type == null ? null : scheduleIconMap.get(type));
            }
            dataList.add(row);
        }
        return dataList;
    }

    /**
     * 並べ替えコンボボックスをセットします。
     *
     * @param sortComboBox 並べ替えコンボボックス
     */
    public void setSortComboBox(ComboBox<Code> sortComboBox) {
        this.sortComboBox = sortComboBox;
    }

    /**
     * 選択中の受験予定大学リストを返します。
     *
     * @return 選択中の受験予定大学リスト
     */
    public List<ExmntnScdlBean> getSelectedList() {

        List<ExmntnScdlBean> list = CollectionsUtil.newArrayList();

        JTable table = getFixedTable();
        if (table.getSelectedRowCount() > 0) {
            /* 選択されている場合 */
            for (int row : table.getSelectedRows()) {
                list.add((ExmntnScdlBean) table.getModel().getValueAt(row, 0));
            }
        }

        return list;
    }

    /**
     * 詳細表示する志望大学Beanを返します。
     *
     * @return 詳細表示する志望大学Bean
     */
    public ExmntnMainCandidateUnivBean getDetailJudgeBean() {
        return createCandidateUnivBean(getSelectedList().get(0));
    }

    /**
     * 受験予定大学を削除します。
     *
     * @param list 削除する受験予定大学
     */
    public void delete(List<ExmntnScdlBean> list) {

        /* 受験予定大学リストをテーブルモデルから取得する */
        List<ExmntnScdlBean> examPlanUnivList = getExamPlanUnivList();

        /* 受験予定大学リストから削除する */
        examPlanUnivList.removeAll(list);

        /* 志望順位で並べ替える */
        sortExamPlanUnivList(examPlanUnivList, Code.SCHEDULE_ORDER_RANK);

        /* 志望順位を連番にする */
        int candidateRank = 1;
        for (ExmntnScdlBean bean : examPlanUnivList) {
            bean.setCandidateRank(Integer.valueOf(candidateRank++));
        }

        /* 削除後のデータで再表示する */
        sortAndDisplay(examPlanUnivList);
    }

    /**
     * 選択中の受験予定大学の志望順位を変更します。
     *
     * @param isUp 上に移動するならtrue
     * @return 順位を入れ替えた受験予定大学
     */
    public Pair<? extends UnivKeyBean, ? extends UnivKeyBean> changeRank(boolean isUp) {

        /* 受験予定大学リストをテーブルモデルから取得する */
        List<ExmntnScdlBean> examPlanUnivList = getExamPlanUnivList();

        /* 選択中の行位置 */
        int index = getTable().getSelectedRow();

        /* 入れ替え対象の行位置 */
        int targetIndex = index + (isUp ? -1 : 1);

        /* 選択中の受験予定大学 */
        ExmntnScdlBean bean = examPlanUnivList.get(index);

        /* 入れ替え対象の受験予定大学 */
        ExmntnScdlBean targetBean = examPlanUnivList.get(targetIndex);

        /* 志望順位を入れ替える */
        Integer candidateRank = bean.getCandidateRank();
        bean.setCandidateRank(targetBean.getCandidateRank());
        targetBean.setCandidateRank(candidateRank);

        /* 入れ替え後のデータで再表示する */
        sortAndDisplay(examPlanUnivList);

        /* 選択を復元する */
        int newIndex = examPlanUnivList.indexOf(bean);
        getTable().setRowSelectionInterval(newIndex, newIndex);

        return Pair.of(bean, targetBean);
    }

    /**
     * テーブルモデルから受験予定大学リストを取得します。
     *
     * @return 受験予定大学リスト
     */
    private List<ExmntnScdlBean> getExamPlanUnivList() {
        TableModel model = getFixedTable().getModel();
        List<ExmntnScdlBean> list = CollectionsUtil.newArrayList(model.getRowCount());
        for (int i = 0; i < model.getRowCount(); i++) {
            list.add((ExmntnScdlBean) model.getValueAt(i, 0));
        }
        return list;
    }

}

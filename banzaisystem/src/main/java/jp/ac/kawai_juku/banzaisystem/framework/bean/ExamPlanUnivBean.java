package jp.ac.kawai_juku.banzaisystem.framework.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 受験予定大学データを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExamPlanUnivBean extends UnivKeyBean {

    /** 個人ID */
    private Integer id;

    /** 志望順位 */
    private Integer candidateRank;

    /** 地方フラグ */
    private String provincesFlg;

    /** 年度 */
    private String year;

    /** 模試区分 */
    private String examDiv;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 個人IDをセットします。
     *
     * @param id 個人ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 志望順位を返します。
     *
     * @return 志望順位
     */
    public Integer getCandidateRank() {
        return candidateRank;
    }

    /**
     * 志望順位をセットします。
     *
     * @param candidateRank 志望順位
     */
    public void setCandidateRank(Integer candidateRank) {
        this.candidateRank = candidateRank;
    }

    /**
     * 地方フラグを返します。
     *
     * @return 地方フラグ
     */
    public String getProvincesFlg() {
        return provincesFlg;
    }

    /**
     * 地方フラグをセットします。
     *
     * @param provincesFlg 地方フラグ
     */
    public void setProvincesFlg(String provincesFlg) {
        this.provincesFlg = provincesFlg;
    }

    /**
     * 年度を返します。
     *
     * @return 年度
     */
    public String getYear() {
        return year;
    }

    /**
     * 年度をセットします。
     *
     * @param year year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.stng.service;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.service.PasswordService;
import jp.ac.kawai_juku.banzaisystem.framework.service.UnivCdHookUpService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.dao.MainteImptImportDao;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.dao.MainteStngDao;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * メンテナンス-環境設定画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteStngService {

    /** パスワードサービス */
    @Resource
    private PasswordService passwordService;

    /** ログ出力サービス */
    @Resource
    private LogService logService;

    /** 大学コード紐付け処理サービス */
    @Resource
    private UnivCdHookUpService univCdHookUpService;

    /** DAO */
    @Resource
    private MainteStngDao mainteStngDao;

    /** 個人成績インポート処理のDAO */
    @Resource
    private MainteImptImportDao mainteImptImportDao;

    /**
     * 模試リストを取得します。
     *
     * @return 模試リスト
     */
    public List<ExamBean> findExamList() {
        return mainteStngDao.selectExamList();
    }

    /**
     * 取り込み済み成績・志望データを削除します。
     *
     * @param year 年度
     * @param examCd 模試コード
     * @param grade 学年
     * @param cls クラス
     */
    @Lock
    public void deleteRecord(String year, String examCd, Integer grade, String cls) {

        /* 個表データを削除する */
        mainteStngDao.deleteIndividualRecord(year, examCd, grade, cls);

        if (examCd != null) {
            /* 個表データが存在しない志望大学（模試記入志望大学データのみ）を削除する */
            mainteStngDao.deleteExamCandidateUniv();
            /* 非表示になっているシステムで登録した志望大学を表示する */
            mainteStngDao.updateCandidateUnivDispOn();
            /* 志望大学の表示順を振り直す */
            mainteImptImportDao.updateCandidateUnivDispNumber(mainteStngDao.selectCandidateUnivList());
        }

        /* 個表データが存在しない成績データを削除する */
        mainteStngDao.deleteSubRecord();

        /* 個表データが存在しない模試記入志望大学データを削除する */
        mainteStngDao.deleteCandidateRating();

        /* 個表データが存在しない個人成績インポート状況を削除する */
        mainteStngDao.deleteStatSubRecord();

        /* 個表データが存在しない学籍基本情報を削除する */
        mainteStngDao.deleteBasicInfo();

        /* 個表データが存在しない学籍履歴情報を削除する */
        mainteStngDao.deleteHistoryInfo();

        /* 個表データが存在しない志望大学を削除する */
        mainteStngDao.deleteCandidateUniv();

        /* 個表データが存在しない受験予定大学を削除する */
        mainteStngDao.deleteExamPlanUniv();

        /* ログを出力する */
        List<String> contents = new ArrayList<>();
        if (examCd == null) {
            contents.add("全ての模試");
        } else {
            contents.add(year);
            contents.add(examCd);
        }
        if (grade != null && grade.intValue() < 0) {
            contents.add("全");
        } else {
            contents.add(ObjectUtils.toString(grade));
        }
        if (StringUtils.EMPTY.equals(cls)) {
            contents.add("全");
        } else {
            contents.add(ObjectUtils.toString(cls));
        }
        logService.output(GamenId.E4, contents.toArray(new String[contents.size()]));
    }

    /**
     * 過年度の取り込み済み成績・志望データを削除します。
     *
     * @param year 年度
     */
    @Lock
    public void deletePastRecord(String year) {

        /* 個表データを削除する */
        mainteStngDao.deletePastIndividualRecord(year);

        /* 個表データが存在しない成績データを削除する */
        mainteStngDao.deleteSubRecord();

        /* 個表データが存在しない模試記入志望大学データを削除する */
        mainteStngDao.deleteCandidateRating();

        /* 個表データが存在しない個人成績インポート状況を削除する */
        mainteStngDao.deleteStatSubRecord();

        /* 個表データが存在しない学籍基本情報を削除する */
        mainteStngDao.deleteBasicInfo();

        /* 個表データが存在しない学籍履歴情報を削除する */
        mainteStngDao.deleteHistoryInfo();

        /* 個表データが存在しない志望大学を削除する */
        mainteStngDao.deleteCandidateUniv();

        /* 個表データが存在しない受験予定大学を削除する */
        mainteStngDao.deleteExamPlanUniv();

        /* 成績データ情報の年度・模試区分を大学マスタに合わせる */
        univCdHookUpService.updateRecordInfo();
    }

    /**
     * 教員用パスワードを変更します。
     *
     * @param newPass 新パスワード
     * @param rePass 再入力パスワード
     */
    public void changeTeacherPassword(String newPass, String rePass) {

        /* 新パスワードと再入力パスワードをチェックする */
        checkNewPassowrd(newPass, rePass);

        /* 保存する */
        passwordService.savePassword(SettingKey.TEACHER_PASSWORD, newPass);

        /* ログを出力する */
        logService.output(GamenId.E4, "先生用パスワード変更");
    }

    /**
     * 新パスワードと再入力パスワードをチェックします。
     *
     * @param newPass 新パスワード
     * @param rePass 再入力パスワード
     */
    private void checkNewPassowrd(String newPass, String rePass) {
        if (!newPass.equals(rePass)) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E003"));
        }
        if (!ValidateUtil.isAlphaNum(newPass) || newPass.length() < 4) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E004"));
        }
    }

    /**
     * 管理者用パスワードを変更します。
     *
     * @param currentPass 現パスワード
     * @param newPass 新パスワード
     * @param rePass 再入力パスワード
     */
    public void changeAdminPassword(String currentPass, String newPass, String rePass) {

        /* 現パスワードをチェックする */
        if (!passwordService.checkPassword(SettingKey.ADMIN_PASSWORD, currentPass)) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E005"));
        }

        /* 新パスワードと再入力パスワードをチェックする */
        checkNewPassowrd(newPass, rePass);

        /* 保存する */
        passwordService.savePassword(SettingKey.ADMIN_PASSWORD, newPass);

        /* ログを出力する */
        logService.output(GamenId.E4, "管理者用パスワード変更");
    }

    /**
     * 管理者用パスワードをチェックします。
     *
     * @param password パスワード
     */
    public void checkAdminPassword(String password) {
        if (!passwordService.checkPassword(SettingKey.ADMIN_PASSWORD, password)) {
            throw new MessageDialogException(getMessage("error.mainte.stng.E_E008"));
        }
    }

    /**
     * 学年リストを取得します。
     *
     * @param year 年度
     * @param exam 対象模試
     * @return 学年リスト
     */
    public List<Integer> findGradeList(String year, ExamBean exam) {
        return mainteStngDao.selectGradeList(year, exam);
    }

    /**
     * クラスリストを生成します。
     *
     * @param year 年度
     * @param exam 対象模試
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> findClassList(String year, ExamBean exam, Integer grade) {
        return mainteStngDao.selectClassList(year, exam, grade);
    }

}

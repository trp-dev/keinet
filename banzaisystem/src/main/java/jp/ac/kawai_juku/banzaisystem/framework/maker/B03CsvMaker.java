package jp.ac.kawai_juku.banzaisystem.framework.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvWriter;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 *
 * 取り込み結果CSVメーカーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class B03CsvMaker implements Closeable {

    /** 取り込み結果メッセージリスト */
    private final List<String> messages = new ArrayList<>();

    /**
     * コンストラクタです。
     */
    public B03CsvMaker() {
        messages.add(MessageManager.getMessage("label.framework.B03.completed"));
    }

    /**
     * 取り込み結果メッセージ(結果CSVの1行)を追加します。
     *
     * @param message 取り込み結果メッセージ
     */
    public void addMessage(String message) {
        messages.add(message);
    }

    /**
     * 取り込みスキップメッセージを追加します。
     *
     * @param rowNum 行番号
     */
    public void addSkipMessage(int rowNum) {
        messages.add(MessageManager.getMessage("label.framework.B03.skipped", Integer.valueOf(rowNum)));
    }

    /**
     * 取り込みエラーメッセージを追加します。
     *
     */
    public void addErrMessage() {
        messages.add(MessageManager.getMessage("label.framework.B03.err"));
    }

    @Override
    public void close() {

        File file;
        try {
            /* CSVファイルを作る */
            file = createCsvFile();

            /* メッセージをCSVに出力する */
            try (CsvWriter writer = new CsvWriter(new FileOutputStream(file))) {
                for (String message : messages) {
                    writer.addColumn(message).addRow();
                }
            }
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E004"), e);
        }

        /* Notepadで起動する */
        launch(file);
    }

    /**
     * CSVファイルオブジェクトを生成します。
     *
     * @return ファイル
     * @throws IOException IO例外
     */
    private File createCsvFile() throws IOException {
        return new File(SettingPropsUtil.getLogDir(), "B03_01_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS") + ".csv");
    }

    /**
     * ファイルをNotepadで起動します。
     *
     * @param file ファイル
     */
    private void launch(File file) {
        try {
            Runtime.getRuntime().exec(new String[] { "notepad.exe", file.getAbsolutePath() });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.log4j;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * バンザイシステムのFileAppenderです。<br>
 * 出力ファイル名を動的に変更するために定義しています。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZFileAppender extends AppenderSkeleton {

    /** ログファイルのキャラクタセット */
    private static final String CHARSET_NAME = "Windows-31J";

    /** FileAppender */
    private static final InnerFileAppender APPENDER = new InnerFileAppender();

    @Override
    public void close() {
        APPENDER.close();
    }

    @Override
    public boolean requiresLayout() {
        return APPENDER.requiresLayout();
    }

    @Override
    protected void append(LoggingEvent event) {
        APPENDER.append(event);
    }

    @Override
    public Layout getLayout() {
        return APPENDER.getLayout();
    }

    @Override
    public void setLayout(Layout layout) {
        APPENDER.setLayout(layout);
    }

    /**
     * ログファイルを閉じます。
     */
    public static void closeFile() {
        APPENDER.close();
    }

    /** FileAppenderの実体 */
    private static class InnerFileAppender extends AppenderSkeleton {

        /** ログファイル */
        private File file;

        /** Writer */
        private Writer writer;

        @Override
        public void close() {
            if (writer != null) {
                try {
                    writer.close();
                    writer = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                file = null;
            }
        }

        @Override
        public boolean requiresLayout() {
            return true;
        }

        @Override
        protected void append(LoggingEvent event) {
            try {
                File newFile = getLogFile();
                if (!newFile.equals(file)) {
                    close();
                    file = newFile;
                }
                if (writer == null) {
                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), CHARSET_NAME));
                }
                writer.write(layout.format(event));
                if (layout.ignoresThrowable()) {
                    String[] rows = event.getThrowableStrRep();
                    if (rows != null) {
                        for (String row : rows) {
                            writer.write(row);
                            writer.write(Layout.LINE_SEP);
                        }
                    }
                }
                writer.flush();
            } catch (Throwable e) {
                processError(e);
            }
        }

        /**
         * ログファイルを返します。
         *
         * @return ログファイル
         * @throws IOException ログファイルディレクトリの生成に失敗したとき
         */
        private File getLogFile() throws IOException {
            return new File(SettingPropsUtil.getLogDir(), BZConstants.LOG_FILE_NAME);
        }

        /**
         * ログ書き込み時に発生した例外を処理します。
         *
         * @param e 例外
         */
        private void processError(Throwable e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(WindowManager.getInstance().getActiveWindow(),
                    getMessage("error.framework.CM_E004"));
        }

    }

}

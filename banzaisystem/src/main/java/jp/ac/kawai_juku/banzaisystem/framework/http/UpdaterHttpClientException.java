package jp.ac.kawai_juku.banzaisystem.framework.http;

/**
 *
 * 更新機能のHTTPクライアントの例外クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class UpdaterHttpClientException extends Exception {

    /**
     * コンストラクタです。
     *
     * @param message エラーメッセージ
     */
    UpdaterHttpClientException(String message) {
        super(message);
    }

    /**
     * コンストラクタです。
     *
     * @param cause 原因
     */
    UpdaterHttpClientException(Throwable cause) {
        super(cause);
    }

}

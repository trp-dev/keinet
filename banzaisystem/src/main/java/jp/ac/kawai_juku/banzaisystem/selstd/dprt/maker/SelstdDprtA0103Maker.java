package jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.TemplateId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseExcelMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerOutputDataBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtExcelMakerService;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.util.SelstdDprtExcelUtil;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * Excel帳票メーカー(個人成績・志望一覧(志望大学上位20大学))です。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
@Template(id = TemplateId.A01_03)
public class SelstdDprtA0103Maker extends BaseExcelMaker<SelstdDprtExcelMakerInBean> {

    /** 1ページに出力する最大生徒数 */
    private static final int LISTMAX = 5;

    /** 1教科に出力する最大科目数（総合） */
    private static final int SUBTOTALMAX = 1;

    /** 1教科に出力する最大科目数（英語） */
    private static final int SUBENGMAX = 3;

    /** 1教科に出力する最大科目数（数学） */
    private static final int SUBMATMAX = 3;

    /** 1教科に出力する最大科目数（国語） */
    private static final int SUBJAPMAX = 1;

    /** 1教科に出力する最大科目数（理科） */
    private static final int SUBSCIMAX = 3;

    /** 1教科に出力する最大科目数（地歴公民） */
    private static final int SUBSOCMAX = 2;

    /** 一人に出力する最大志望大学数（1列分） */
    private static final int CANDIDATEUNIVCOLUMNMAX = 10;

    /** 一人に出力する志望大学列数 */
    private static final int CANDIDATEUNIVCOLUMN = 2;

    /** 一人に出力する最大志望大学数 */
    private static final int CANDIDATEUNIVMAX = CANDIDATEUNIVCOLUMNMAX * CANDIDATEUNIVCOLUMN;

    /** インデックス：学年・クラス */
    private static final String IDX_GRADE_CLASS = "A2";

    /** インデックス：対象模試 */
    private static final String IDX_TARGET_EXAM = "A3";

    /** インデックスrow：個人成績一覧（開始位置） */
    private static final int IDX_ROW_LIST_START = 4;

    /** インデックスrow：個人成績一覧（開始位置→科目短縮名） */
    private static final int IDX_ROW_LIST_SUBJECT = 1;

    /** インデックスrow：個人成績一覧（開始位置→科目短縮名（理科・地歴公民）） */
    private static final int IDX_ROW_LIST_SUBSCISOC = IDX_ROW_LIST_SUBJECT + 1;

    /** インデックスrow：個人成績一覧（開始位置→配点） */
    private static final int IDX_ROW_LIST_SUBALLOTPNT = IDX_ROW_LIST_SUBSCISOC + 1;

    /** インデックスrow：個人成績一覧（開始位置→得点） */
    private static final int IDX_ROW_LIST_POINT = IDX_ROW_LIST_SUBALLOTPNT + 1;

    /** インデックスrow：個人成績一覧（開始位置→換算得点） */
    private static final int IDX_ROW_LIST_CVSSCORE = IDX_ROW_LIST_POINT + 1;

    /** インデックスrow：個人成績一覧（開始位置→偏差値） */
    private static final int IDX_ROW_LIST_DEVIATION = IDX_ROW_LIST_CVSSCORE + 1;

    /** インデックスrow：個人成績一覧（１生徒当たりの行数 + 生徒間の空欄行） */
    private static final int IDX_ROW_LIST_LINE = 13 + 1;

    /** インデックスcell：個人成績一覧（生徒情報） */
    private static final int IDX_CELL_STUDENT = 0;

    /** インデックスcell：個人成績一覧（総合） */
    private static final int IDX_CELL_TOTAL = 1;

    /** インデックスcell：個人成績一覧（英語） */
    private static final int IDX_CELL_ENG = IDX_CELL_TOTAL + SUBTOTALMAX;

    /** インデックスcell：個人成績一覧（数学） */
    private static final int IDX_CELL_MAT = IDX_CELL_ENG + SUBENGMAX;

    /** インデックスcell：個人成績一覧（国語） */
    private static final int IDX_CELL_JAP = IDX_CELL_MAT + SUBMATMAX;

    /** インデックスcell：個人成績一覧（理科） */
    private static final int IDX_CELL_SCI = IDX_CELL_JAP + SUBJAPMAX;

    /** インデックスcell：個人成績一覧（地歴公民） */
    private static final int IDX_CELL_SOC = IDX_CELL_SCI + SUBSCIMAX;

    /** インデックスrow：志望大学（開始位置） */
    private static final int IDX_ROW_LIST_CANDIDATEUNIV = 7;

    /** インデックスcell：志望大学（大学名） */
    private static final int IDX_CELL_UNIVNAME = 15;

    /** インデックスcell：志望大学（大学名→学部名） */
    private static final int IDX_CELL_FACULTYNAME = 1;

    /** インデックスcell：志望大学（大学名→学科名） */
    private static final int IDX_CELL_DEPTNAME = 2;

    /** インデックスcell：志望大学（大学名→共通テスト(含まない)評価） */
    /** インデックスcell：志望大学（大学名→共通テスト評価） */
    private static final int IDX_CELL_CENTEREVAL = 3;

    /** インデックスcell：志望大学（大学名→共通テスト(含む)評価） */
    /* private static final int IDX_CELL_CENTEREVAL_INCLUDE = 4; */

    /** インデックスcell：志望大学（大学名→２次評価） */
    /* private static final int IDX_CELL_SECONDEVAL = 5; */
    private static final int IDX_CELL_SECONDEVAL = 4;

    /** インデックスcell：志望大学（大学名→総合評価） */
    /* private static final int IDX_CELL_TOTALEVAL = 6; */
    private static final int IDX_CELL_TOTALEVAL = 5;

    /** インデックスrow：志望大学（志望大学列の列数） */
    /* private static final int IDX_CELL_UNIV_COLUMN = 8; */
    private static final int IDX_CELL_UNIV_COLUMN = 7;

    /** 出力データリスト（個人成績一覧＋志望大学） */
    private List<SelstdDprtExcelMakerOutputDataBean> outputDataBeanList = new ArrayList<SelstdDprtExcelMakerOutputDataBean>();

    /** ページ毎に出力する学年 */
    private Integer pageGrade;

    /** ページ毎に出力するクラス */
    private String pageCls;

    /** 対象模試 */
    private ExamBean exam;

    /** ドッキング模試 */
    /* private ExamBean dockingexam; */

    /** Cell(中央寄用) */
    private HSSFCellStyle cellCenter;

    /** サービス */
    @Resource(name = "selstdDprtExcelMakerService")
    private SelstdDprtExcelMakerService service;

    @Override
    protected boolean prepareData(SelstdDprtExcelMakerInBean inBean) {

        /* 出力データリストを取得する*/
        if (service.prepareData(inBean, outputDataBeanList, getTemplate())) {

            /* 帳票出力用に対象模試を保存する */
            exam = inBean.getExam();
            /* dockingexam = inBean.getDockingExam(); */

            /* 学年とクラスをセットする（１シート目用） */
            pageGrade = outputDataBeanList.get(0).getGrade();
            pageCls = outputDataBeanList.get(0).getCls();

            return true;
        }

        return false;
    }

    @Override
    protected void initSheet() {

        /* 対象学年・クラスを出力する */
        outputTargetClass();

        /* 対象模試を出力する */
        outputTargetExam();
    }

    @Override
    protected void outputData() {

        /* 帳票データを出力する */
        outputExcelData();

    }

    /**
     * 学年・クラスを出力するメソッドです。
     */
    private void outputTargetClass() {

        /* 学年・クラス文字列 */
        String gradeCls = null;

        /* 学年・クラス文字列を作成する */
        gradeCls = service.createTargetClass(pageGrade, pageCls);

        /* 学年・クラスを出力する */
        setCellValue(IDX_GRADE_CLASS, gradeCls);
    }

    /**
     * 対象模試を出力するメソッドです。
     */
    private void outputTargetExam() {
        setCellValue(IDX_TARGET_EXAM, service.createTargetExam(exam.toString()));
    }

    /**
     * 帳票データを出力するメソッドです。
     */
    private void outputExcelData() {

        /* 1ページ内の人数カウント */
        int cntStudent = 0;

        /* 人数分ループする */
        for (int index = 0; index < outputDataBeanList.size(); index++) {

            SelstdDprtExcelMakerOutputDataBean bean = outputDataBeanList.get(index);

            /* 1ページ内の最大出力人数を超えたかチェックする */
            if (cntStudent >= LISTMAX || (index != 0 && (!ObjectUtils.equals(pageGrade, bean.getGrade()) || !ObjectUtils.equals(pageCls, bean.getCls())))) {

                /* 学年とクラスをセットする */
                pageGrade = bean.getGrade();
                pageCls = bean.getCls();

                /* 改シートする */
                breakSheet();
                cntStudent = 0;
            }

            /* 生徒情報出力文字を作成する */
            String strStudent = service.createStudent(bean.getGrade(), bean.getCls(), bean.getClassNo(), bean.getNameKana());

            /* 個人成績一覧データを出力する */
            outputRecordList(cntStudent, strStudent, bean.getRecordBeanList());

            /* 志望大学データを出力する */
            outputCandidateUnivList(cntStudent, bean.getCandidateUnivBeanList());

            cntStudent++;
        }
    }

    /**
     * 個人成績一覧データを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param strStudent カナ氏名
     * @param outputData 個人成績一覧データリスト（１人分）
     */
    private void outputRecordList(int cntStudent, String strStudent, List<SelstdDprtExcelMakerRecordBean> outputData) {

        /* 1教科内の科目数カウント（総合１） */
        int cntSubTotal = 0;

        /* 1教科内の科目数カウント（英語） */
        int cntSubEng = 0;

        /* 1教科内の科目数カウント（数学） */
        int cntSubMat = 0;

        /* 1教科内の科目数カウント（国語） */
        int cntSubJap = 0;

        /* 1教科内の科目数カウント（理科） */
        int cntSubSci = 0;

        /* 1教科内の科目数カウント（地歴公民） */
        int cntSubSoc = 0;

        /* 成績情報出力位置 */
        int rowRecord = IDX_ROW_LIST_START + (IDX_ROW_LIST_LINE * cntStudent);

        cellCenter = this.createCellStyle();

        /* Cellのスタイルを取得 */
        cellCenter.cloneStyleFrom((HSSFCellStyle) getCell("C8").getCellStyle());

        /* Cellのスタイルを変更 */
        cellCenter.setAlignment(CellStyle.ALIGN_CENTER);

        /* 生徒情報を出力する */
        setCellValue(rowRecord, IDX_CELL_STUDENT, strStudent);

        /* 理科・社会の教科名を出力する */
        outputCourseLabel(rowRecord);

        /* マーク模試のみ国語(科目コード:3910)の成績をセットする */
        if (ExamUtil.isMark(exam)) {
            service.setResultsMarkJapanese(exam, outputData);
        }

        /* 成績を出力する */
        for (int subcnt = 0; subcnt < outputData.size(); subcnt++) {

            /* 科目成績 */
            SelstdDprtExcelMakerRecordBean subrecord = outputData.get(subcnt);

            /* 教科 */
            String course = subrecord.getCourseCd();

            /* 成績データがある場合のみ出力する */
            if (course != null) {

                /* 総合１を出力する */
                if (course.equals(BZConstants.COURSE_TOTAL1)) {
                    /* 最大科目数まで出力する */
                    if (cntSubTotal < SUBTOTALMAX && outputRecordData(rowRecord, IDX_CELL_TOTAL + cntSubTotal, false, subrecord)) {
                        cntSubTotal++;
                    }
                }

                /* 英語を出力する */
                if (course.equals(BZConstants.COURSE_ENG)) {
                    if (cntSubEng < SUBENGMAX && outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord)) {
                        cntSubEng++;
                    }
                    /*
                    if (cntSubEng < SUBENGMAX) {
                        if (subrecord.getSubCd().equals(SUBCODE_CENTER_DEFAULT_ENGLISH)) {
                            continue;
                        }
                        boolean result;
                        if (subrecord.getSubCd().contentEquals(SUBCODE_CENTER_DEFAULT_ENNINTEI)) {
                            result = outputStrRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, subrecord);
                        } else {
                            result = outputRecordData(rowRecord, IDX_CELL_ENG + cntSubEng, false, subrecord);
                        }
                        if (result) {
                            cntSubEng++;
                        }
                    }
                    */
                }

                /* 数学を出力する */
                if (course.equals(BZConstants.COURSE_MAT)) {
                    /* 最大科目数まで出力する */
                    if (cntSubMat < SUBMATMAX && outputRecordData(rowRecord, IDX_CELL_MAT + cntSubMat, false, subrecord)) {
                        cntSubMat++;
                    }
                }

                /* 国語を出力する */
                if (course.equals(BZConstants.COURSE_JAP)) {
                    /* 最大科目数まで出力する */
                    if (cntSubJap < SUBJAPMAX && outputRecordData(rowRecord, IDX_CELL_JAP + cntSubJap, false, subrecord)) {
                        cntSubJap++;
                    }
                }

                /* 理科を出力する */
                if (course.equals(BZConstants.COURSE_SCI)) {
                    if (cntSubSci == 0 && FlagUtil.toBoolean(subrecord.getBasicFlg())) {
                        /* 1科目めが基礎科目なら、1列目は空欄にする */
                        cntSubSci++;
                    }
                    /* 最大科目数まで出力する */
                    if (cntSubSci < SUBSCIMAX && outputRecordData(rowRecord, IDX_CELL_SCI + cntSubSci, true, subrecord)) {
                        cntSubSci++;
                    }
                }

                /* 地歴公民を出力する */
                if (course.equals(BZConstants.COURSE_SOC)) {
                    /* 最大科目数まで出力する */
                    if (cntSubSoc < SUBSOCMAX && outputRecordData(rowRecord, IDX_CELL_SOC + cntSubSoc, true, subrecord)) {
                        cntSubSoc++;
                    }
                }
            }
        }
    }

    /**
     * 理科・社会の教科名を出力します。
     *
     * @param row 出力位置（行）
     */
    private void outputCourseLabel(int row) {
        int index = 0;
        for (String label : SelstdDprtExcelUtil.getCourseLabel(exam)) {
            setCellValue(row + IDX_ROW_LIST_SUBJECT, IDX_CELL_SCI + index++, label);
        }
    }

    /**
     * 成績を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param flgSciSoc 理科or地歴公民
     * @param subrecord 科目成績
     * @return 出力したらtrue
     */
    private boolean outputRecordData(int row, int cell, boolean flgSciSoc, SelstdDprtExcelMakerRecordBean subrecord) {

        /* 模試科目マスタにない科目は出力しない */
        if (subrecord.getSubAddrName() == null) {
            return false;
        }

        /* 科目短縮名を出力する */
        if (flgSciSoc) {
            /* 理科と地歴公民は他科目と出力位置が違う */
            setCellValue(row + IDX_ROW_LIST_SUBSCISOC, cell, subrecord.getSubAddrName());
        } else {
            setCellValue(row + IDX_ROW_LIST_SUBJECT, cell, subrecord.getSubAddrName());
        }

        /* 科目配点を出力する */
        setCellValue(row + IDX_ROW_LIST_SUBALLOTPNT, cell, subrecord.getSubAllotpnt());

        /* 科目得点を出力する */
        setCellValue(row + IDX_ROW_LIST_POINT, cell, subrecord.getSubScore());

        /* 科目換算得点を出力する */
        setCellValue(row + IDX_ROW_LIST_CVSSCORE, cell, subrecord.getSubCvsscore());

        /* 科目偏差値を出力する */
        setCellValue(row + IDX_ROW_LIST_DEVIATION, cell, subrecord.getSubDeviation());

        return true;
    }

    /**
     * マーク模試の国語成績を出力するメソッドです。
     *
     * @param row 出力位置（行）
     * @param cell 出力位置（列）
     * @param subrecord 科目成績
     *
     * @return 出力したらtrue
     */
    /* private boolean outputStrRecordData(int row, int cell, SelstdDprtExcelMakerRecordBean subrecord) { */

    /* 科目名に存在しない科目は出力しない */
    /* if (StringUtils.isEmpty(subrecord.getSubAddrName())) {
        return false;
    }
    */
    /* 科目短縮名を出力する */
    /*setCellValue(row + IDX_ROW_LIST_SUBJECT, cell, subrecord.getSubAddrName());*/

    /* 科目得点を出力する */
    /*
    setCellValue(row + IDX_ROW_LIST_POINT, cell, subrecord.getSubStrScore());
    setCellStyle(row + IDX_ROW_LIST_POINT, cell, cellCenter);
    
    return true;
    }
    */
    /**
     * 志望大学データを出力するメソッドです。
     *
     * @param cntStudent 1ページ内の人数カウント
     * @param outputData 志望大学データリスト（１人分）
     */
    private void outputCandidateUnivList(int cntStudent, List<SelstdDprtExcelMakerCandidateUnivBean> outputData) {

        /* 志望大学カウント（１列当たり）*/
        int cntUniv = 0;

        /* 志望大学列カウント */
        int cntClumn = 0;

        /* 志望大学出力位置（row） */
        int rowRecord = IDX_ROW_LIST_CANDIDATEUNIV + (IDX_ROW_LIST_LINE * cntStudent);

        /* ExamBean markExam; */

        /*
        if (ExamUtil.isMark(exam)) {
            markExam = exam;
        } else {
            markExam = dockingexam;
        }
        */

        /* 志望大学を出力する */
        for (int index = 0; index < outputData.size(); index++) {

            /* １列の志望大学数をオーバーしたので次の列に移動する */
            if (cntUniv >= CANDIDATEUNIVCOLUMNMAX) {
                cntUniv = 0;
                cntClumn++;
            }

            /* 志望大学の出力位置（cell）*/
            int cell = IDX_CELL_UNIVNAME + (IDX_CELL_UNIV_COLUMN * cntClumn);

            /* 志望大学 */
            SelstdDprtExcelMakerCandidateUnivBean candidateUniv = outputData.get(index);

            /* 成績データがある場合のみ２０件出力する */
            if (candidateUniv.getDispNumber() != null && index < CANDIDATEUNIVMAX) {

                /* 大学名 */
                setCellValue(rowRecord + cntUniv, cell, candidateUniv.getUnivName());

                /* 学科名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_FACULTYNAME, candidateUniv.getFacultyName());

                /* 学部名 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_DEPTNAME, candidateUniv.getDeptName());

                /* 共通テスト(含まない)評価 */
                /* 共通テスト評価 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, candidateUniv.getValuateExclude());
                /*
                if (!ExamUtil.isCenter(markExam)) {
                    setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, candidateUniv.getValuateExclude());
                } else {
                    if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                        setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, "");
                    } else {
                        setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL, candidateUniv.getValuateExclude());
                    }
                }
                */

                /* 共通テスト(含む)評価 */
                /*
                if (!StringUtil.isEmptyTrim(candidateUniv.getAppReqPaternCd())) {
                    setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL_INCLUDE, candidateUniv.getValuateInclude());
                } else {
                    setCellValue(rowRecord + cntUniv, cell + IDX_CELL_CENTEREVAL_INCLUDE, "");
                }
                */

                /* ２次評価 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_SECONDEVAL, candidateUniv.getSecondRating());

                /* 総合評価 */
                setCellValue(rowRecord + cntUniv, cell + IDX_CELL_TOTALEVAL, candidateUniv.getTotalRating());
            }

            cntUniv++;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;

/**
 *
 * ASCIIコード準拠のアルファベット文字と数字文字以外の入力を制限するドキュメントフィルタです。
 * <br />入力可能な文字種の指定は ValidateUtil クラスの関数 isAlphaNum に依存しています。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 * @see jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil#isAlphaNum(String) ValidateUtil#isAlphaNum
 *
 */
public class AlphaNumDocumentFilter extends MaxLengthDocumentFilter {

    /**
     * コンストラクタです。
     *
     * @param maxLength 最大文字数
     */
    public AlphaNumDocumentFilter(int maxLength) {
        super(maxLength);
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr) throws BadLocationException {

        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
        sb.insert(offset, str);
        String afterStr = sb.toString();
        if (check(afterStr)) {
            super.insertString(fb, offset, str, attr);
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
            throws BadLocationException {

        if (length == 0) {
            insertString(fb, offset, text, attrs);
        } else {
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
            sb.delete(offset, offset + length);
            if (text != null) {
                sb.insert(offset, text);
            }
            String afterStr = sb.toString();
            if (check(afterStr)) {
                super.replace(fb, offset, length, text, attrs);
            }
        }
    }

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {

        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
        sb.delete(offset, offset + length);
        String afterStr = sb.toString();
        if (check(afterStr)) {
            super.remove(fb, offset, length);
        }
    }

    /**
     * 文字列がAlphaNumFieldの設定を満たしているかをチェックします。
     *
     * @param str 文字列
     * @return チェック結果
     */
    private boolean check(String str) {
        /* 該当する文字 = true / 該当しない場合 = false / 空文字の場合 = true */
        /* （半角空白などの）記号文字を含む場合 = false */
        if (str.length() > 0) {
            return ValidateUtil.isAlphaNum(str);
        } else {
            /* 空文字 */
            return true;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean;

import java.util.List;

/**
 *
 * 学力分布情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistBean {

    /** 偏差値帯フラグ */
    private boolean deviationDistFlg;

    /** 志望集計パターン */
    private String patternCd;

    /** 入試満点値(小論文なし) */
    private Integer fullPnt;

    /** ボーダー */
    private String border;

    /** １目盛りあたりの人数 */
    private int scale;

    /** 帯別人数リスト */
    private List<ExmntnDistNumbersBean> numbersList;

    /** 志願者内訳(総志望) */
    private final ExmntnDistCandidateDetailBean detailTotal = new ExmntnDistCandidateDetailBean();

    /** 志願者内訳(出願予定) */
    private final ExmntnDistCandidateDetailBean detailPlan = new ExmntnDistCandidateDetailBean();

    /**
     * 偏差値帯フラグを返します。
     *
     * @return 偏差値帯フラグ
     */
    public boolean isDeviationDistFlg() {
        return deviationDistFlg;
    }

    /**
     * 偏差値帯フラグをセットします。
     *
     * @param deviationDistFlg 偏差値帯フラグ
     */
    public void setDeviationDistFlg(boolean deviationDistFlg) {
        this.deviationDistFlg = deviationDistFlg;
    }

    /**
     * 志望集計パターンを返します。
     *
     * @return 志望集計パターン
     */
    public String getPatternCd() {
        return patternCd;
    }

    /**
     * 志望集計パターンをセットします。
     *
     * @param patternCd 志望集計パターン
     */
    public void setPatternCd(String patternCd) {
        this.patternCd = patternCd;
    }

    /**
     * 入試満点値(小論文なし)を返します。
     *
     * @return 入試満点値(小論文なし)
     */
    public Integer getFullPnt() {
        return fullPnt;
    }

    /**
     * 入試満点値(小論文なし)をセットします。
     *
     * @param fullPnt 入試満点値(小論文なし)
     */
    public void setFullPnt(Integer fullPnt) {
        this.fullPnt = fullPnt;
    }

    /**
     * 志願者内訳(総志望)を返します。
     *
     * @return 志願者内訳(総志望)
     */
    public ExmntnDistCandidateDetailBean getDetailTotal() {
        return detailTotal;
    }

    /**
     * 志願者内訳(総志望)をセットします。
     *
     * @param detailTotal 志願者内訳(総志望)
     */
    public void setDetailTotal(ExmntnDistCandidateDetailBean detailTotal) {
        copy(detailTotal, this.detailTotal);
    }

    /**
     * 志願者内訳(出願予定)を返します。
     *
     * @return 志願者内訳(出願予定)
     */
    public ExmntnDistCandidateDetailBean getDetailPlan() {
        return detailPlan;
    }

    /**
     * 志願者内訳(出願予定)をセットします。
     *
     * @param detailPlan 志願者内訳(出願予定)
     */
    public void setDetailPlan(ExmntnDistCandidateDetailBean detailPlan) {
        copy(detailPlan, this.detailPlan);
    }

    /**
     * 帯別人数リストを返します。
     *
     * @return 帯別人数リスト
     */
    public List<ExmntnDistNumbersBean> getNumbersList() {
        return numbersList;
    }

    /**
     * 帯別人数リストをセットします。
     *
     * @param numbersList 帯別人数リスト
     */
    public void setNumbersList(List<ExmntnDistNumbersBean> numbersList) {
        this.numbersList = numbersList;
    }

    /**
     * ボーダーを返します。
     *
     * @return ボーダー
     */
    public String getBorder() {
        return border;
    }

    /**
     * ボーダーをセットします。
     *
     * @param border ボーダー
     */
    public void setBorder(String border) {
        this.border = border;
    }

    /**
     * １目盛りあたりの人数を返します。
     *
     * @return １目盛りあたりの人数
     */
    public int getScale() {
        return scale;
    }

    /**
     * １目盛りあたりの人数をセットします。
     *
     * @param scale １目盛りあたりの人数
     */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * 志願者内訳をコピーします。
     *
     * @param src コピー元
     * @param dest コピー先
     */
    private void copy(ExmntnDistCandidateDetailBean src, ExmntnDistCandidateDetailBean dest) {
        if (src == null) {
            dest.setNumbersA(null);
            dest.setNumbersS(null);
            dest.setNumbersG(null);
            dest.setAvgPntA(null);
            dest.setAvgPntS(null);
            dest.setAvgPntG(null);
        } else {
            dest.setNumbersA(src.getNumbersA());
            dest.setNumbersS(src.getNumbersS());
            dest.setNumbersG(src.getNumbersG());
            dest.setAvgPntA(src.getAvgPntA());
            dest.setAvgPntS(src.getAvgPntS());
            dest.setAvgPntG(src.getAvgPntG());
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.maker;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CsvId;
import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseCsvMaker;
import jp.ac.kawai_juku.banzaisystem.framework.service.ScoreBeanService;
import jp.ac.kawai_juku.banzaisystem.output.main.bean.OutputMainB0401Bean;
import jp.ac.kawai_juku.banzaisystem.output.main.bean.OutputMainB0401CsvInBean;
import jp.ac.kawai_juku.banzaisystem.output.main.dao.OutputMainB0401CsvDao;
import jp.co.fj.kawaijuku.judgement.beans.JudgedResult;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Judge;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * CSV帳票メーカー(志望大学・評価一覧)です。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class OutputMainB0401Maker extends BaseCsvMaker<OutputMainB0401CsvInBean> {

    /** CSV項目ヘッダ */
    private static final List<String> HEADER;
    static {
        List<String> list = CollectionsUtil.newArrayList(34);
        list.add("個人ID");
        list.add("学年");
        list.add("クラス");
        list.add("クラス番号");
        list.add("カナ氏名");
        list.add("模試コード");
        list.add("模試名");
        list.add("模試区分");
        list.add("志望順位");
        list.add("大学コード（10桁）");
        list.add("大学コード（5桁）");
        list.add("日程");
        list.add("大学名");
        list.add("学部名");
        list.add("学科名");
        /*list.add("共通テスト（含まない）評価得点");
        list.add("共通テスト（含まない）評価");
        list.add("共通テスト（含む）評価得点");
        list.add("共通テスト（含む）評価");
        list.add("共通テスト（含む）英語出願要件");
        list.add("共通テスト（含む）英語認定試験出願基準非対応告知");*/
        list.add("共通テスト評価得点");
        list.add("共通テスト評価");
        list.add("二次偏差値");
        list.add("二次評価");
        list.add("総合ポイント");
        list.add("総合評価");
        /*list.add("共通テスト（含まない）A評価基準");
        list.add("共通テスト（含まない）ボーダーライン");
        list.add("共通テスト（含まない）D評価基準");
        list.add("共通テスト（含まない）満点");
        list.add("共通テスト（含む）A評価基準");
        list.add("共通テスト（含む）ボーダーライン");
        list.add("共通テスト（含む）D評価基準");
        list.add("共通テスト（含む）満点");*/
        list.add("共通テストA評価基準");
        list.add("共通テストボーダーライン");
        list.add("共通テストD評価基準");
        list.add("共通テスト満点");
        list.add("二次ランク");
        HEADER = Collections.unmodifiableList(list);
    }

    /** 評価文字列の配列（ABC…の順） */
    private static final String[] JUDGE_STR_ARRAY = { Judge.JUDGE_STR_A, Judge.JUDGE_STR_B, Judge.JUDGE_STR_C, Judge.JUDGE_STR_D, Judge.JUDGE_STR_E };

    /** 英語出願要件(：なし) */
    /*private static final String JUDGE0 = "";*/
    /** 英語出願要件(○：満たしている) */
    /*private static final String JUDGE1 = "○";*/
    /** 英語出願要件(○：満たしていない) */
    /*private static final String JUDGE2 = "×";*/
    /** 英語出願要件(○：他要件があるため要確認) */
    /*private static final String JUDGE3 = "△";*/

    /** 英語認定試験出願基準非対応告知(：なし) */
    /*private static final String NOTICE0 = "";*/
    /** 英語認定試験出願基準非対応告知(！：英語認定試験の扱いが複雑なため「認定試験出願要件」に未反映部分がある場合) */
    /*private static final String NOTICE1 = "！";*/

    /** ヘッダ出力を行ったかどうかの判定用 */
    private boolean headerRowOutputFlag;

    /** 共通サービス：成績情報取得処理の提供 */
    @Resource
    private ScoreBeanService scoreBeanService;

    /** DAO */
    @Resource
    private OutputMainB0401CsvDao outputMainB0401CsvDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * コンストラクタです。
     */
    public OutputMainB0401Maker() {
        super(false);
    }

    @Override
    public PrintId getPrintId() {
        return CsvId.B04_01;
    }

    @Override
    protected void outputData(OutputMainB0401CsvInBean inBean) {
        /* 画面上で選択された生徒の個人IDリストを取得 */
        List<Integer> studentIdList = inBean.getIdList();
        /* 生徒を１人ずつ取り出して志望大学/学部/学科の情報を取得 */
        for (Integer studentId : studentIdList) {

            /* 指定の生徒の成績情報を取得 */
            ExamBean exam = inBean.getExam();
            ScoreBean scoreBean = scoreBeanService.createScoreBean(exam, studentId);

            /* 指定の生徒の志望大学/学部/学科の情報と判定結果をCSVに全て出力 */
            csvJudgementAndOutput(inBean, studentId, scoreBean);
        }
    }

    /**
     * 生徒１人分の志望大学情報と判定結果をCSVに出力します。
     *
     * @param inBean 画面上で選択された諸々の情報
     * @param individualId 生徒の個人ID
     * @param scoreBean 成績情報
     */
    private void csvJudgementAndOutput(OutputMainB0401CsvInBean inBean, Integer individualId, ScoreBean scoreBean) {

        /* 指定の生徒の志望大学/学部/学科の情報を取得（SQL発行） */
        List<OutputMainB0401Bean> candidataUnivInfoList = null;
        candidataUnivInfoList = outputMainB0401CsvDao.selectB0401List(inBean, individualId);

        /* 志望大学情報1件1件に対して個別に成績評価を行いCSVファイルに適宜出力する */
        for (OutputMainB0401Bean recordBean : candidataUnivInfoList) {

            /* 判定結果を取得（judgement.jar） */
            OutputMainB0401Bean judgedBean = applyJudgementResult(inBean.getExam(), scoreBean, recordBean);

            /* 判定結果(評価)が、CSVの出力条件に合うなら出力する */
            if (checkRating(inBean, judgedBean)) {
                /* ヘッダ行を出力（最初の１回だけ出力） */
                csvOutputHeader();
                /* データ行を出力 */
                csvOutputRecord(recordBean, judgedBean, individualId, inBean.getExam());
            }
        }
    }

    /**
     * 判定結果(評価)が、CSVの出力条件に合うかどうかを判定します。
     *
     * @param inBean 画面上で選択された諸々の情報
     * @param judgedBean {@link OutputMainB0401Bean}
     * @return CSVに出力する(出力条件に合う)ならtrue
     */
    private boolean checkRating(OutputMainB0401CsvInBean inBean, OutputMainB0401Bean judgedBean) {

        /* A判定〜E判定までの出力指定フラグを取得 */
        boolean[] ratingFlags = { inBean.isRatingA(), inBean.isRatingB(), inBean.isRatingC(), inBean.isRatingD(), inBean.isRatingE() };

        /* 判定評価のチェック状態を確認 */
        if (!ArrayUtils.contains(ratingFlags, true)) {
            /* 全てチェック無しだった場合はCSV出力する */
            return true;
        } else {
            /* 評価の値を取得 */
            String[] evals = new String[] { judgedBean.getCenterRating(), judgedBean.getSecondRating(), judgedBean.getTotalRating() };

            /* 評価値を判断してCSV出力の可否を判定 */
            for (int i = 0; i < ratingFlags.length; i++) {
                if (ratingFlags[i] && ArrayUtils.contains(evals, JUDGE_STR_ARRAY[i])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * CSVファイルにヘッダ行を出力します。<br/>
     * <br/>
     * 基底クラス BaseCsvMaker には<br/>
     * 「行が全く出力されていない場合は「対象無し」ダイアログを表示する」<br/>
     * という機能があります。<br/>
     * <br/>
     * この機能に対応するために<br/>
     * 「データ行が無い場合はヘッダ行も出力しない。」<br/>
     * という挙動をヘッダ行の出力に適用します。<br/>
     */
    private void csvOutputHeader() {
        if (!headerRowOutputFlag) {
            /* ヘッダ行の出力実績がない場合のみヘッダ行を出力 */
            addColumns(HEADER);
            addRow();
            /* ヘッダ行出力済みを記録 */
            headerRowOutputFlag = true;
        }
    }

    /**
     * CSVファイルに1行分出力します。
     *
     * @param recordBean CSV出力のための情報（DBから取得したもの）
     * @param judgedBean CSV出力のための情報（判定結果として得られたもの）
     * @param individualId 生徒の個人ID
     * @param exam 対象模試
     */
    private void csvOutputRecord(OutputMainB0401Bean recordBean, OutputMainB0401Bean judgedBean, Integer individualId, ExamBean exam) {

        /* 個人ID */
        addColumn(Integer.toString(individualId));

        /* 学年 */
        addColumn(recordBean.getGrade());

        /* クラス */
        addColumn(recordBean.getCls());

        /* クラス番号 */
        addColumn(recordBean.getClassNo());

        /* カナ氏名 */
        addColumn(recordBean.getNameKana());

        /* 模試コード */
        addColumn(exam.getExamCd());

        /* 模試名 */
        addColumn(exam.getExamName());

        /* 模試区分 */
        addColumn(systemDto.getUnivExamDiv());

        /* 志望順位 */
        addColumn(recordBean.getCandidateRank());

        /* 大学コード（10桁） */
        addColumn(recordBean.getUnivKey());

        /* 大学コード（5桁） */
        addColumn(recordBean.getChoiceCd5());

        /* 日程 */
        addColumn(recordBean.getScheduleName());

        /* 大学名 */
        addColumn(recordBean.getUnivName());

        /* 学部名 */
        addColumn(recordBean.getFacultyName());

        /* 学科名 */
        addColumn(recordBean.getDeptName());

        /* 共通テスト（含まない）評価得点 */
        /* 共通テスト評価得点 */
        addColumn(judgedBean.getCenterScore());

        /* 共通テスト（含まない）評価 */
        /* 共通テスト評価 */
        addColumn(StringUtils.trim(judgedBean.getCenterRating()));

        /* 共通テスト（含む）評価得点 */
        /*addColumn(judgedBean.getCenterIncScore());*/

        /* 共通テスト（含む）評価 */
        /*addColumn(StringUtils.trim(judgedBean.getCenterIncRating()));*/

        /* 共通テスト（含む）英語出願要件 */
        /*addColumn(judgedBean.getCenterIncJudge());*/

        /* 共通テスト（含む）英語認定試験出願基準非対応告知 */
        /*addColumn(judgedBean.getCenterIncNotice());*/

        /* 二次偏差値 */
        addColumn(judgedBean.getSecondDeviation());

        /* 二次評価 */
        addColumn(StringUtils.trim(judgedBean.getSecondRating()));

        /* 総合ポイント */
        addColumn(judgedBean.getTotalRatingPoint());

        /* 総合評価 */
        addColumn(StringUtils.trim(judgedBean.getTotalRating()));

        /* 共通テスト（含まない）A評価基準 */
        /* 共通テストA評価基準 */
        addColumn(judgedBean.getRatingALine());

        /* 共通テスト（含まない）ボーダーライン */
        /* 共通テストボーダーライン */
        addColumn(judgedBean.getBorderLine());

        /* 共通テスト（含まない）D評価基準 */
        /* 共通テストD評価基準 */
        addColumn(judgedBean.getRatingDLine());

        /* 共通テスト（含まない）満点 */
        /* 共通テスト満点 */
        addColumn(judgedBean.getCenterFullScore());

        /* 共通テスト（含む）A評価基準 */
        /*addColumn(judgedBean.getRatingIncALine());*/

        /* 共通テスト（含む）ボーダーライン */
        /*addColumn(judgedBean.getIncBorderLine());*/

        /* 共通テスト（含む）D評価基準 */
        /*addColumn(judgedBean.getRatingIncDLine());*/

        /* 共通テスト（含む）満点 */
        /*addColumn(judgedBean.getCenterIncFullScore());*/

        /* 二次ランク */
        addColumn(judgedBean.getSecondRank());

        /* 1行出力する */
        addRow();
    }

    /**
     * 志望大学情報と成績を基に判定を行い結果を返します。<br/>
     * （judgement.jarの機能を用いた判定と結果の取得を行います。）
     *
     * @param examInfo 模試情報
     * @param scoreBean 成績情報
     * @param record 志望大学・評価一覧CSVの出力情報を保持するBean
     * @return 判定結果に関する情報を保持するBean
     */
    private OutputMainB0401Bean applyJudgementResult(ExamBean examInfo, ScoreBean scoreBean, OutputMainB0401Bean record) {
        OutputMainB0401Bean resultBean = new OutputMainB0401Bean();

        /* 成績判定のための大学情報提供用Beanを作成 */
        UnivKeyBean univKey = new UnivKeyBean(record.getUnivKey());

        /*boolean isCenter = ExamUtil.isCenter(examInfo);*/

        /* 指定の大学/学部/学科に対する評価判定を実施 */
        /* 含まない値を取得 */
        /*
        JudgedResult judgeResult = BZJudgementExecutor.getInstance().judge(scoreBean, univKey, false);
        JudgedResult judgeIncResult = BZJudgementExecutor.getInstance().judge(scoreBean, univKey, true);
        */
        JudgedResult judgeResult = BZJudgementExecutor.getInstance().judge(scoreBean, univKey);
        /*JudgedResult judgeIncResult = BZJudgementExecutor.getInstance().judge(scoreBean, univKey); */
        /* 各種判定結果を格納する */
        /* 共通テストリサーチの場合は含まないの値を出力しない */
        /*if (!isCenter) {*/
        /* 列16 共通テスト（含まない）評価得点 */
        /*resultBean.setCenterScore(judgeResult.getC_scoreStr());*/
        /* 列17 共通テスト（含まない）評価 */
        /*resultBean.setCenterRating(judgeResult.getCLetterJudgement());*/
        /* 列26 共通テスト（含まない）A評価基準 */
        /*resultBean.setRatingALine(judgeResult.getCALine());*/
        /* 列27 共通テスト（含まない）ボーダーライン */
        /*resultBean.setBorderLine(convBorderLine(judgeResult));*/
        /* 列28 共通テスト（含まない）D評価基準 */
        /*resultBean.setRatingDLine(judgeResult.getCDLine());*/
        /* 列29 共通テスト（含まない）満点 */
        /*resultBean.setCenterFullScore(convFullPoint(judgeResult));
        } else {
        if (!StringUtil.isEmptyTrim(judgeIncResult.getBean().getUniv().getAppReqPaternCd())) {*/
        /* 列16 共通テスト（含まない）評価得点 */
        /*resultBean.setCenterScore("");*/
        /* 列17 共通テスト（含まない）評価 */
        /*resultBean.setCenterRating("");*/
        /* 列26 共通テスト（含まない）A評価基準 */
        /*resultBean.setRatingALine("");*/
        /* 列27 共通テスト（含まない）ボーダーライン */
        /*resultBean.setBorderLine("");*/
        /* 列28 共通テスト（含まない）D評価基準 */
        /*resultBean.setRatingDLine("");*/
        /* 列29 共通テスト（含まない）満点 */
        /*resultBean.setCenterFullScore("");
        } else {*/
        /* 列16 共通テスト（含まない）評価得点 */
        /*resultBean.setCenterScore(judgeResult.getC_scoreStr());*/
        /* 列17 共通テスト（含まない）評価 */
        /*resultBean.setCenterRating(judgeResult.getCLetterJudgement());*/
        /* 列26 共通テスト（含まない）A評価基準 */
        /*resultBean.setRatingALine(judgeResult.getCALine());*/
        /* 列27 共通テスト（含まない）ボーダーライン */
        /*resultBean.setBorderLine(convBorderLine(judgeResult));*/
        /* 列28 共通テスト（含まない）D評価基準 */
        /*resultBean.setRatingDLine(judgeResult.getCDLine());*/
        /* 列29 共通テスト（含まない）満点 */
        /*resultBean.setCenterFullScore(convFullPoint(judgeResult));
        }
        }*/

        /* 列16 共通テスト評価得点 */
        resultBean.setCenterScore(judgeResult.getC_scoreStr());
        /* 列17 共通テスト評価 */
        resultBean.setCenterRating(judgeResult.getCLetterJudgement());
        /* 列26 共通テストA評価基準 */
        resultBean.setRatingALine(judgeResult.getCALine());
        /* 列27 共通テストボーダーライン */
        resultBean.setBorderLine(convBorderLine(judgeResult));
        /* 列28 共通テストD評価基準 */
        resultBean.setRatingDLine(judgeResult.getCDLine());
        /* 列29 共通テスト満点 */
        resultBean.setCenterFullScore(convFullPoint(judgeResult));

        /* 列22 二次偏差値 */
        resultBean.setSecondDeviation(judgeResult.getS_scoreStr());

        /* 列23 二次評価 */
        resultBean.setSecondRating(judgeResult.getSLetterJudgement());

        /* 列24 総合ポイント */
        resultBean.setTotalRatingPoint(judgeResult.getEvalTotal().getPointStr());

        /* 列25 総合評価 */
        resultBean.setTotalRating(judgeResult.getEvalTotal().getLetter());

        /* 列34 二次ランク */
        resultBean.setSecondRank(convSecondRank(judgeResult));

        /*if (!StringUtil.isEmptyTrim(judgeIncResult.getBean().getUniv().getAppReqPaternCd())) {*/
        /* 列18 共通テスト（含む）評価得点 */
        /*resultBean.setCenterIncScore(judgeIncResult.getC_scoreStr());*/
        /* 列19 共通テスト（含む）評価 */
        /*resultBean.setCenterIncRating(judgeIncResult.getCLetterJudgement());*/
        /* 列20 共通テスト（含む）英語出願要件 */
        /*if (judgeIncResult.getBean().getEngSSSutuGanHanTeiKbn() == 1) {
            resultBean.setCenterIncJudge(JUDGE1);
        } else if (judgeIncResult.getBean().getEngSSSutuGanHanTeiKbn() == 2) {
            resultBean.setCenterIncJudge(JUDGE2);
        } else if (judgeIncResult.getBean().getEngSSSutuGanHanTeiKbn() == 3) {
            resultBean.setCenterIncJudge(JUDGE3);
        } else {
            resultBean.setCenterIncJudge(JUDGE0);
        }*/
        /* 列21 共通テスト（含む）英語認定試験出願基準非対応告知 */
        /*if (judgeIncResult.getBean().getEngSSHKSutuGanHTKokuchi() == 1) {
            resultBean.setCenterIncNotice(NOTICE1);
        } else {
            resultBean.setCenterIncNotice(NOTICE0);
        }*/
        /* 列30 共通テスト（含む）A評価基準 */
        /*resultBean.setRatingIncALine(judgeIncResult.getCALine());*/
        /* 列31 共通テスト（含む）ボーダーライン */
        /*resultBean.setIncBorderLine(convBorderLine(judgeIncResult));*/
        /* 列32 共通テスト（含む）D評価基準 */
        /*resultBean.setRatingIncDLine(judgeIncResult.getCDLine());*/
        /* 列33 共通テスト（含む）満点 */
        /*resultBean.setCenterIncFullScore(convFullPoint(judgeIncResult));*/
        /*} else {*/
        /* 列18 共通テスト（含む）評価得点 */
        /*resultBean.setCenterIncScore("");*/
        /* 列19 共通テスト（含む）評価 */
        /*resultBean.setCenterIncRating("");*/
        /* 列20 共通テスト（含む）英語出願要件 */
        /*resultBean.setCenterIncJudge("");*/
        /* 列21 共通テスト（含む）英語認定試験出願基準非対応告知 */
        /*resultBean.setCenterIncNotice("");*/
        /* 列30 共通テスト（含む）A評価基準 */
        /*resultBean.setRatingIncALine("");*/
        /* 列31 共通テスト（含む）ボーダーライン */
        /*resultBean.setIncBorderLine("");*/
        /* 列32 共通テスト（含む）D評価基準 */
        /*resultBean.setRatingIncDLine("");*/
        /* 列33 共通テスト（含む）満点 */
        /*resultBean.setCenterIncFullScore("");
        }*/

        return resultBean;
    }

    /**
     * ボーダーラインを文字列に変換します。
     *
     * @param judgedResult 判定結果
     * @return 表示用文字列
     * @see jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor#setJudgedResult
     */
    private String convBorderLine(JudgedResult judgedResult) {
        StringBuilder builder = new StringBuilder(32);
        /* センタボーダ */
        if (!judgedResult.getBorderPointStr().isEmpty()) {
            builder.append(Integer.valueOf(judgedResult.getBorderPointStr()));
        }
        /* センタボーダ得点率 */
        int cenScoreRate = (int) judgedResult.getCenScoreRate();
        if (cenScoreRate != JudgementConstants.Univ.CEN_SCORE_RATE_NA) {
            builder.append("(");
            builder.append(Integer.valueOf(cenScoreRate));
            builder.append("%)");
        }
        return builder.toString();
    }

    /**
     * 満点値を文字列に変換します。
     *
     * @param judgedResult 判定結果
     * @return 表示用文字列
     * @see jp.ac.kawai_juku.banzaisystem.exmntn.univ.service.ExmntnUnivService#convFullPoint
     */
    private String convFullPoint(JudgedResult judgedResult) {
        int point = judgedResult.getDetail().getCenter().getFullPoint();
        if (point > 0 && point != JudgementConstants.Detail.FULL_POINT_NA) {
            return Integer.toString(point);
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * 二次ランクを文字列に変換します。
     *
     * @param judgedResult 判定結果
     * @return 表示用文字列
     * @see jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor#setJudgedResult
     */
    private String convSecondRank(JudgedResult judgedResult) {
        /* 二次ランク */
        if (judgedResult.getRank() == JudgementConstants.Univ.RANK_FORCE) {
            return Univ.RANK_STR_NONE;
        } else if (judgedResult.getRank() == JudgementConstants.Univ.RANK_NUM_NONE) {
            return "--";
        } else {
            return judgedResult.getRankLLimitsStr();
        }
    }

}

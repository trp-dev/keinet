package jp.ac.kawai_juku.banzaisystem.selstd.dprt.service;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.COURSE_JAP;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.service.ScoreBeanService;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerOutputDataBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerOutputStudentDataBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerStudentRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.dao.SelstdDprtExcelMakerDao;

/**
 *
 * 一覧出力ダイアログのExcel帳票メーカーのサービスです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class SelstdDprtExcelMakerService {

    /** タイトル：学年 */
    private static final String TITLE_GRADE = "学年：";

    /** タイトル：クラス */
    private static final String TITLE_CLS = "　クラス名：";

    /** タイトル：対象模試 */
    private static final String TITLE_EXAM = "対象模試：";

    /** タイトル（生徒情報）：学年 */
    private static final String TITLE_STUDENT_GRADE = "学年：";

    /** タイトル（生徒情報）：クラス */
    private static final String TITLE_STUDENT_CLS = "　クラス：";

    /** タイトル（生徒情報）：クラス番号 */
    private static final String TITLE_STUDENT_CLSNO = "　クラス番号：";

    /** タイトル（生徒情報）：氏名 */
    private static final String TITLE_STUDENT_NAME = "　　氏名：";

    /** タイトル（生徒情報）：氏名(単独) */
    private static final String TITLE_STUDENT_NAME_ONLY = "氏名：";

    /** 個人ID検索用IN句のカラム名 */
    private static final String COLUMN_INDIVIDUALID = "BI.INDIVIDUALID";

    /** 個人ID検索用IN句のカラム名 */
    /* private static final String COLUMN_INDIVIDUALID_CEFR = "C1.INDIVIDUALID"; */

    /** 個人ID検索用IN句のカラム名 */
    /* private static final String COLUMN_INDIVIDUALID_JPN = "J.INDIVIDUALID"; */

    /** 判定用成績データサービス */
    @Resource
    private ScoreBeanService scoreBeanService;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** DAO */
    @Resource(name = "selstdDprtExcelMakerDao")
    private SelstdDprtExcelMakerDao dao;

    /**
     * 出力データリストを取得するメソッドです。
     *
     * @param inBean 入力Bean
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     * @param template 帳票テンプレート
     * @return 出力するデータがあるならtrue
     */
    public boolean prepareData(SelstdDprtExcelMakerInBean inBean, List<SelstdDprtExcelMakerOutputDataBean> outputDataBeanList, Template template) {

        /* 画面から対象模試または個人IDが渡されなかった場合は帳票を作成しない */
        if (inBean.getExam() == null || inBean.getIdList().isEmpty()) {
            return false;
        }

        /* 対象模試年度 */
        String examYear = inBean.getExam().getExamYear();

        /* 対象模試コード */
        String examcd = inBean.getExam().getExamCd();

        /* 個人IDを検索するIN句 */
        String individualid = DaoUtil.createIn(COLUMN_INDIVIDUALID, inBean.getIdList());
        /* String cefrindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_CEFR, inBean.getIdList()); */
        /* String jpnindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_JPN, inBean.getIdList()); */

        /* 個人成績一覧リスト */
        List<SelstdDprtExcelMakerRecordBean> recordBeanList = null;

        /* 個人成績一覧リスト（ドッキング模試用） */
        List<SelstdDprtExcelMakerRecordBean> recordDockingBeanList = null;

        /* 志望大学リスト */
        List<SelstdDprtExcelMakerCandidateUnivBean> candidateUnivBeanList = null;

        /* 個人成績一覧リストを取得する */
        /* recordBeanList = getScoreList(inBean.getExam(), individualid, template, cefrindividualid, jpnindividualid); */
        recordBeanList = getScoreList(inBean.getExam(), individualid, template, false);

        /* データが1件も取得できなかった場合は帳票を作成しない */
        if (recordBeanList.size() == 0) {
            return false;
        }

        /* 個人成績一覧リスト（ドッキング模試用）を取得する */
        /* recordDockingBeanList = getScoreList(dockingExam, individualid, template, cefrindividualid, jpnindividualid); */
        recordDockingBeanList = getScoreList(inBean.getExam(), individualid, template, true);

        /* 志望大学リストを取得する */
        candidateUnivBeanList = getCandidateUnivList(examYear, examcd, individualid);

        /* 生徒毎の成績リストと志望大学リストをセットする */
        setOutputData(recordBeanList, recordDockingBeanList, candidateUnivBeanList, outputDataBeanList);

        /* 志望大学の判定を実行する */
        judgeCandidateUniv(inBean, outputDataBeanList);

        return true;
    }

    /**
     * 出力データリストを取得するメソッドです。
     *
     * @param inBean 入力Bean
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     * @param template 帳票テンプレート
     * @return 出力するデータがあるならtrue
     */
    public boolean prepareDataStudent(SelstdDprtExcelMakerInBean inBean, List<SelstdDprtExcelMakerOutputStudentDataBean> outputDataBeanList, Template template) {

        /* 画面から対象模試または個人IDが渡されなかった場合は帳票を作成しない */
        if (inBean.getExam() == null || inBean.getIdList().isEmpty()) {
            return false;
        }

        /* 対象模試年度 */
        String examYear = inBean.getExam().getExamYear();

        /* 個人IDを検索するIN句 */
        String individualid = DaoUtil.createIn(COLUMN_INDIVIDUALID, inBean.getIdList());
        /* String cefrindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_CEFR, inBean.getIdList()); */
        /* String jpnindividualid = DaoUtil.createIn(COLUMN_INDIVIDUALID_JPN, inBean.getIdList()); */

        /* 個人成績一覧リスト */
        List<SelstdDprtExcelMakerStudentRecordBean> recordBeanList = null;

        /* 志望大学リスト */
        List<SelstdDprtExcelMakerCandidateUnivStudentBean> candidateUnivBeanList = null;

        /* 個人成績一覧リストを取得する */
        /* recordBeanList = getScoreListStudent(inBean.getExam(), individualid, template, cefrindividualid, jpnindividualid); */
        recordBeanList = getScoreListStudent(inBean.getExam(), individualid, template);

        /* データが1件も取得できなかった場合は帳票を作成しない */
        if (recordBeanList.size() == 0) {
            return false;
        }

        /* 志望大学リストを取得する */
        candidateUnivBeanList = getCandidateUnivStudentList(examYear, individualid);

        /* 生徒毎の成績リストと志望大学リストをセットする */
        setOutputStudentData(recordBeanList, candidateUnivBeanList, outputDataBeanList);

        /* 志望大学の判定を実行する */
        judgeStudentCandidateUniv(inBean, outputDataBeanList);

        return true;
    }

    /**
     * 個人成績一覧リストを取得するメソッドです。
     *
     * @param exam 対象模試
     * @param individualid 個人IDを検索するIN句
     * @param template 帳票テンプレート
     * @return 個人成績一覧リスト
     * @param docflg ドッキング対象フラグ
     */
    public List<SelstdDprtExcelMakerRecordBean> getScoreList(ExamBean exam, String individualid, Template template, Boolean docflg) {

        /* 第1解答科目模試フラグ */
        String ans1st;

        /* 模試が第1解答科目対象模試かどうか判別する */
        if (ExamUtil.isAns1st(exam)) {
            ans1st = "1";
        } else {
            ans1st = "0";
        }

        /* A01_03帳票フラグ */
        String form03Flg;

        /* 出力対象の帳票がA01_03帳票かどうか判別する */
        /* 英語認定試験延期対応にてA03帳票も英+Lを出力するよう変更 */
        form03Flg = "0";
        /*
        if (template.id() == TemplateId.A01_03) {
            form03Flg = "1";
        } else {
            form03Flg = "0";
        }
        */

        /* 個人成績一覧リストを取得する */
        List<SelstdDprtExcelMakerRecordBean> retList;
        /* List<SelstdDprtExcelCefrMakerRecordBean> cefrList; */
        /* List<SelstdDprtExcelJpnMakerRecordBean> jpnList; */
        if (!docflg) {
            retList = dao.selectExcelRecordList(ans1st, exam.getExamCd(), exam.getExamYear(), individualid, form03Flg);
        } else {
            retList = dao.selectExcelRecordListDoc(ans1st, exam.getExamCd(), exam.getExamYear(), individualid, form03Flg);
        }
        /* マーク模試の場合はCEFR情報と国語記述を読み込み */
        /* if (ExamUtil.isMark(exam)) { */
        /* CEFR情報編集 */
        /*
        cefrList = dao.selectExcelCefrRecordList(exam.getExamCd(), exam.getExamYear(), cefrindividualid);
        if (cefrList != null) {
            for (int index = 0; index < cefrList.size(); index++) {
                SelstdDprtExcelMakerRecordBean cefrListWk = new SelstdDprtExcelMakerRecordBean();
                String strWk = StringUtils.EMPTY;
                cefrListWk.setBunriCode(cefrList.get(index).getBunriCode());
                cefrListWk.setCls(cefrList.get(index).getCls());
                cefrListWk.setClassNo(cefrList.get(index).getClassNo());
                cefrListWk.setNameKana(cefrList.get(index).getNameKana());
                cefrListWk.setGrade(cefrList.get(index).getGrade());
        
                cefrListWk.setSubAddrName("CEFR");
                cefrListWk.setSubCd(SUBCODE_CENTER_DEFAULT_ENNINTEI);
                cefrListWk.setCourseCd(BZConstants.COURSE_ENG);
                cefrListWk.setIndividualId(cefrList.get(index).getIndividualId());
                */
        /* CEFRレベルの低い方(評価が高い方を値に設定) */
        /*
        if (((cefrList.get(index).getCefrLevelCd1().compareTo(cefrList.get(index).getCefrLevelCd2()) <= 0) && !((cefrList.get(index).getCefrLevelCd1() == null) || (cefrList.get(index).getCefrLevelCd1().equals(StringUtils.EMPTY))))
                        || ((cefrList.get(index).getCefrLevelCd2() == null) || (cefrList.get(index).getCefrLevelCd2().equals(StringUtils.EMPTY)))) {
                    if (cefrList.get(index).getCefrLevelCorrectFlg1().equals("1")) {
                        strWk = Constants.ASTERISK + cefrList.get(index).getCefrLevelName1();
                    } else {
                        strWk = cefrList.get(index).getCefrLevelName1();
                    }
                } else {
                    if (cefrList.get(index).getCefrLevelCorrectFlg2().equals("1")) {
                        strWk = Constants.ASTERISK + cefrList.get(index).getCefrLevelName2();
                    } else {
                        strWk = cefrList.get(index).getCefrLevelName2();
                    }
                }
                cefrListWk.setSubStrScore(strWk);
                retList.add(cefrListWk);
            }
        }
        */
        /* 国語記述編集 */
        /*
        jpnList = dao.selectExcelJpnRecordList(exam.getExamCd(), exam.getExamYear(), jpnindividualid);
        if (jpnList != null) {
            for (int index = 0; index < jpnList.size(); index++) {
                SelstdDprtExcelMakerRecordBean jpnListWk = new SelstdDprtExcelMakerRecordBean();
                String strWk = StringUtils.EMPTY;
                jpnListWk.setBunriCode(jpnList.get(index).getBunriCode());
                jpnListWk.setCls(jpnList.get(index).getCls());
                jpnListWk.setClassNo(jpnList.get(index).getClassNo());
                jpnListWk.setNameKana(jpnList.get(index).getNameKana());
                jpnListWk.setGrade(jpnList.get(index).getGrade());
        
                jpnListWk.setIndividualId(jpnList.get(index).getIndividualId());
                jpnListWk.setCourseCd(BZConstants.COURSE_JAP);
                jpnListWk.setSubAddrName("国語(記)");
                jpnListWk.setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
                strWk = jpnList.get(index).getDescRating1() + jpnList.get(index).getDescRating2() + jpnList.get(index).getDescRating3() + jpnList.get(index).getDescRatingTotal();
                jpnListWk.setSubStrScore(strWk);
                retList.add(jpnListWk);
            }
        }
        }
        */
        /* 生徒順に並び替え */
        /* return retList.stream().sorted(comparing(SelstdDprtExcelMakerRecordBean::getCls, nullsLast(naturalOrder())).thenComparing(SelstdDprtExcelMakerRecordBean::getClassNo, nullsLast(naturalOrder()))
                .thenComparing(SelstdDprtExcelMakerRecordBean::getIndividualId, nullsLast(naturalOrder()))).collect(Collectors.toList());
        */
        return retList;
    }

    /**
     * 個人成績一覧リストを取得するメソッドです。
     *
     * @param exam 対象模試
     * @param individualid 個人IDを検索するIN句
     * @param template 帳票テンプレート
     * @return 個人成績一覧リスト
     */
    public List<SelstdDprtExcelMakerStudentRecordBean> getScoreListStudent(ExamBean exam, String individualid, Template template) {

        /* 第1解答科目模試フラグ */
        String ans1st;

        /* 模試が第1解答科目対象模試かどうか判別する */
        if (ExamUtil.isAns1st(exam)) {
            ans1st = "1";
        } else {
            ans1st = "0";
        }

        /* A01_03帳票フラグ */
        String form03Flg;

        form03Flg = "0";

        /* 個人成績一覧リストを取得する */
        List<SelstdDprtExcelMakerStudentRecordBean> retList;
        /* List<SelstdDprtExcelCefrMakerStudentRecordBean> cefrList; */
        /* List<SelstdDprtExcelJpnMakerStudentRecordBean> jpnList; */
        retList = dao.selectExcelRecordStudentList(ans1st, exam.getExamYear(), individualid, form03Flg);

        /* CEFR情報編集 */
        /*
        cefrList = dao.selectExcelCefrRecordStudentList(exam.getExamYear(), cefrindividualid);
        if (cefrList != null) {
            for (int index = 0; index < cefrList.size(); index++) {
                SelstdDprtExcelMakerStudentRecordBean cefrListWk = new SelstdDprtExcelMakerStudentRecordBean();
                String strWk = StringUtils.EMPTY;
                cefrListWk.setBunriCode(StringUtils.EMPTY);
                cefrListWk.setCls(cefrList.get(index).getCls());
                cefrListWk.setClassNo(cefrList.get(index).getClassNo());
                cefrListWk.setNameKana(cefrList.get(index).getNameKana());
                cefrListWk.setGrade(cefrList.get(index).getGrade());
        
                cefrListWk.setExamCd(cefrList.get(index).getExamCd());
                cefrListWk.setExamName(StringUtils.EMPTY);
                cefrListWk.setInpleDate(StringUtils.EMPTY);
                cefrListWk.setExamType("01");
                cefrListWk.setExamOpenDate(cefrList.get(index).getExamOpenDate());
                cefrListWk.setSubAddrName("CEFR");
                cefrListWk.setSubCd(SUBCODE_CENTER_DEFAULT_ENNINTEI);
                cefrListWk.setCourseCd(BZConstants.COURSE_ENG);
                cefrListWk.setIndividualId(cefrList.get(index).getIndividualId());
                */
        /* CEFRレベルの低い方(評価が高い方を値に設定) */
        /*
        if (((cefrList.get(index).getCefrLevelCd1().compareTo(cefrList.get(index).getCefrLevelCd2()) <= 0) && !((cefrList.get(index).getCefrLevelCd1() == null) || (cefrList.get(index).getCefrLevelCd1().equals(StringUtils.EMPTY))))
                        || ((cefrList.get(index).getCefrLevelCd2() == null) || (cefrList.get(index).getCefrLevelCd2().equals(StringUtils.EMPTY)))) {
                    if (cefrList.get(index).getCefrLevelCorrectFlg1().equals("1")) {
                        strWk = Constants.ASTERISK + cefrList.get(index).getCefrLevelName1();
                    } else {
                        strWk = cefrList.get(index).getCefrLevelName1();
                    }
                } else {
                    if (cefrList.get(index).getCefrLevelCorrectFlg2().equals("1")) {
                        strWk = Constants.ASTERISK + cefrList.get(index).getCefrLevelName2();
                    } else {
                        strWk = cefrList.get(index).getCefrLevelName2();
                    }
                }
                cefrListWk.setSubStrScore(strWk);
                retList.add(cefrListWk);
            }
        }
        */
        /* 国語記述編集 */
        /*
        jpnList = dao.selectExcelJpnRecordStudentList(exam.getExamYear(), jpnindividualid);
        if (jpnList != null) {
            for (int index = 0; index < jpnList.size(); index++) {
                SelstdDprtExcelMakerStudentRecordBean jpnListWk = new SelstdDprtExcelMakerStudentRecordBean();
                String strWk = StringUtils.EMPTY;
                jpnListWk.setBunriCode(StringUtils.EMPTY);
                jpnListWk.setCls(jpnList.get(index).getCls());
                jpnListWk.setClassNo(jpnList.get(index).getClassNo());
                jpnListWk.setNameKana(jpnList.get(index).getNameKana());
                jpnListWk.setGrade(jpnList.get(index).getGrade());
        
                jpnListWk.setExamCd(jpnList.get(index).getExamCd());
                jpnListWk.setExamName(StringUtils.EMPTY);
                jpnListWk.setInpleDate(StringUtils.EMPTY);
                jpnListWk.setExamType("01");
                jpnListWk.setExamOpenDate(jpnList.get(index).getExamOpenDate());
                jpnListWk.setIndividualId(jpnList.get(index).getIndividualId());
                jpnListWk.setCourseCd(BZConstants.COURSE_JAP);
                jpnListWk.setSubAddrName("国語(記)");
                jpnListWk.setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
                strWk = jpnList.get(index).getDescRating1() + jpnList.get(index).getDescRating2() + jpnList.get(index).getDescRating3() + jpnList.get(index).getDescRatingTotal();
                jpnListWk.setSubStrScore(strWk);
                retList.add(jpnListWk);
            }
        }
        */
        /* 生徒順に並び替え */
        return retList.stream()
                .sorted(comparing(SelstdDprtExcelMakerStudentRecordBean::getCls, nullsLast(naturalOrder())).thenComparing(SelstdDprtExcelMakerStudentRecordBean::getClassNo, nullsLast(naturalOrder()))
                        .thenComparing(SelstdDprtExcelMakerStudentRecordBean::getIndividualId, nullsLast(naturalOrder())).thenComparing(SelstdDprtExcelMakerStudentRecordBean::getExamOpenDate, nullsLast(reverseOrder()))
                        .thenComparing(SelstdDprtExcelMakerStudentRecordBean::getExamCd, nullsLast(reverseOrder())))
                .collect(Collectors.toList());
    }

    /**
     * 志望大学リストを取得するメソッドです。
     *
     * @param examYear 模試年度
     * @param examcd 模試コード
     * @param individualid 個人IDを検索するIN句
     * @return 志望大学リスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivBean> getCandidateUnivList(String examYear, String examcd, String individualid) {

        /* 志望大学リストを取得する */
        return dao.selectExcelCandidateUnivList(examcd, examYear, individualid);
    }

    /**
     * 志望大学リストを取得するメソッドです。
     *
     * @param examYear 模試年度
     * @param individualid 個人IDを検索するIN句
     * @return 志望大学リスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivStudentBean> getCandidateUnivStudentList(String examYear, String individualid) {

        /* 志望大学リストを取得する */
        return dao.selectExcelCandidateUnivStudentList(examYear, individualid);
    }

    /**
     * 出力データリスト（個人成績一覧＋志望大学）をセットするメソッドです。
     *
     * @param recordBeanList 個人成績一覧リスト
     * @param recordDockingBeanList 個人成績一覧リスト（ドッキング模試用）
     * @param candidateUnivBeanList 志望大学リスト
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     */
    private void setOutputData(List<SelstdDprtExcelMakerRecordBean> recordBeanList, List<SelstdDprtExcelMakerRecordBean> recordDockingBeanList, List<SelstdDprtExcelMakerCandidateUnivBean> candidateUnivBeanList,
            List<SelstdDprtExcelMakerOutputDataBean> outputDataBeanList) {

        /* 個人成績一覧データ（１人分） */
        List<SelstdDprtExcelMakerRecordBean> outputDataRecord = new ArrayList<SelstdDprtExcelMakerRecordBean>();

        /* 志望大学データ（１人分） */
        List<SelstdDprtExcelMakerCandidateUnivBean> outputDataCandidateUniv = new ArrayList<SelstdDprtExcelMakerCandidateUnivBean>();

        /* 個人成績一覧データを取得して出力データリストにセットする */
        for (int index = 0; index < recordBeanList.size(); index++) {

            /* 個人成績一覧データ１人分のデータを取得する */
            outputDataRecord.add(recordBeanList.get(index));
            if (index != recordBeanList.size() - 1) {
                if (recordBeanList.get(index).getIndividualId().intValue() == recordBeanList.get(index + 1).getIndividualId().intValue()) {
                    continue;
                }
            }

            /* 出力データ（個人成績一覧＋志望大学）*/
            SelstdDprtExcelMakerOutputDataBean outputData = new SelstdDprtExcelMakerOutputDataBean();

            /* 出力データに個人IDをセット */
            outputData.setIndividualId(outputDataRecord.get(0).getIndividualId());

            /* 出力データに学年をセット */
            outputData.setGrade(outputDataRecord.get(0).getGrade());

            /* 出力データにクラスをセット */
            outputData.setCls(outputDataRecord.get(0).getCls());

            /* 出力データにクラス番号をセット */
            outputData.setClassNo(outputDataRecord.get(0).getClassNo());

            /* 出力データに氏名（カナ）をセット */
            outputData.setNameKana(outputDataRecord.get(0).getNameKana());

            /* 出力データに文理コードをセット */
            outputData.setBunriCode(outputDataRecord.get(0).getBunriCode());

            /* 成績データが無ければ個人成績一覧リストはセットしない */
            if (outputDataRecord.get(0).getSubCd() != null) {
                /* 出力データに個人成績一覧リストをセット */
                outputData.getRecordBeanList().addAll(outputDataRecord);
            }

            /* 出力データをリストにセット */
            outputDataBeanList.add(outputData);

            /* 個人成績一覧データをクリア */
            outputDataRecord.clear();
        }

        /* 個人成績一覧データ（ドッキング模試用）を取得して出力データリストにセットする */
        for (int index = 0; index < recordDockingBeanList.size(); index++) {

            /* 個人成績一覧データ１人分のデータを取得する */
            outputDataRecord.add(recordDockingBeanList.get(index));
            if (index != recordDockingBeanList.size() - 1) {
                if (recordDockingBeanList.get(index).getIndividualId().intValue() == recordDockingBeanList.get(index + 1).getIndividualId().intValue()) {
                    continue;
                }
            }

            /* 成績データが無ければ個人成績一覧リストはセットしない */
            if (outputDataRecord.get(0).getSubCd() != null) {

                /* 出力データリストから個人IDが一致する生徒を探す */
                for (int cntStudent = 0; cntStudent < outputDataBeanList.size(); cntStudent++) {
                    if (outputDataBeanList.get(cntStudent).getIndividualId().intValue() == outputDataRecord.get(0).getIndividualId().intValue()) {
                        /*  個人成績一覧データ（ドッキング模試用）を出力データをリストにセットする */
                        outputDataBeanList.get(cntStudent).getRecordDockingBeanList().addAll(outputDataRecord);
                    }
                }
            }

            /* 個人成績一覧データをクリア */
            outputDataRecord.clear();
        }

        /* 志望大学データを取得して出力データリストにセットする */
        for (int index = 0; index < candidateUnivBeanList.size(); index++) {

            /* 志望大学データ１人分のデータを取得する */
            outputDataCandidateUniv.add(candidateUnivBeanList.get(index));
            if (index != candidateUnivBeanList.size() - 1) {
                if (candidateUnivBeanList.get(index).getIndividualId().intValue() == candidateUnivBeanList.get(index + 1).getIndividualId().intValue()) {
                    continue;
                }
            }

            /* 志望大学データが無ければ志望大学リストはセットしない */
            if (outputDataCandidateUniv.get(0).getUnivCd() != null) {

                /* 出力データリストから個人IDが一致する生徒を探す */
                for (int cntStudent = 0; cntStudent < outputDataBeanList.size(); cntStudent++) {
                    if (outputDataBeanList.get(cntStudent).getIndividualId().intValue() == outputDataCandidateUniv.get(0).getIndividualId().intValue()) {
                        /*  志望大学データを出力データをリストにセットする */
                        outputDataBeanList.get(cntStudent).getCandidateUnivBeanList().addAll(outputDataCandidateUniv);
                    }
                }
            }

            /* 志望大学データをクリア */
            outputDataCandidateUniv.clear();
        }
    }

    /**
     * 志望大学の判定を実行するメソッドです。
     *
     * @param inBean 画面からの設定値
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     */
    private void judgeCandidateUniv(SelstdDprtExcelMakerInBean inBean, List<SelstdDprtExcelMakerOutputDataBean> outputDataBeanList) {
        /* 全生徒に対して判定を実行する */
        for (SelstdDprtExcelMakerOutputDataBean bean : outputDataBeanList) {

            /* 成績、又は志望大学が空の生徒は判定を実行しない */
            if (bean.getRecordBeanList().isEmpty() || bean.getCandidateUnivBeanList().isEmpty()) {
                continue;
            }

            /* 判定用成績データを生成する */
            ScoreBean score = scoreBeanService.createScoreBean(inBean.getExam(), bean.getIndividualId());

            /* 判定を実行する */
            SelstdDprtExcelMakerOutputDataBean bean2 = bean;
            /*
            BZJudgementExecutor.getInstance().judge(score, bean.getCandidateUnivBeanList(), false);
            BZJudgementExecutor.getInstance().judge(score, bean2.getCandidateUnivBeanList(), true);
            */
            BZJudgementExecutor.getInstance().judge(score, bean.getCandidateUnivBeanList());
            bean.getCandidateUnivBeanList().get(0).setValuateInclude(bean2.getCandidateUnivBeanList().get(0).getValuateInclude());
        }
    }

    /**
     * 出力データリスト（個人成績一覧＋志望大学）をセットするメソッドです。
     *
     * @param recordBeanList 個人成績一覧リスト
     * @param candidateUnivBeanList 志望大学リスト
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     */
    private void setOutputStudentData(List<SelstdDprtExcelMakerStudentRecordBean> recordBeanList, List<SelstdDprtExcelMakerCandidateUnivStudentBean> candidateUnivBeanList,
            List<SelstdDprtExcelMakerOutputStudentDataBean> outputDataBeanList) {

        /* 個人成績一覧データ（１人分） */
        List<SelstdDprtExcelMakerStudentRecordBean> outputDataRecord = new ArrayList<SelstdDprtExcelMakerStudentRecordBean>();

        /* 志望大学データ（１人分） */
        List<SelstdDprtExcelMakerCandidateUnivStudentBean> outputDataCandidateUniv = new ArrayList<SelstdDprtExcelMakerCandidateUnivStudentBean>();

        /* 個人成績一覧データを取得して出力データリストにセットする */
        for (int index = 0; index < recordBeanList.size(); index++) {

            /* 個人成績一覧データ１人分のデータを取得する */
            outputDataRecord.add(recordBeanList.get(index));
            if (index != recordBeanList.size() - 1) {
                if (recordBeanList.get(index).getExamCd() == null || recordBeanList.get(index + 1).getExamCd() == null) {
                    continue;
                }

                if (recordBeanList.get(index).getIndividualId().intValue() == recordBeanList.get(index + 1).getIndividualId().intValue()) {
                    if (recordBeanList.get(index).getExamCd().equals(recordBeanList.get(index + 1).getExamCd())) {
                        continue;
                    }
                }
            }

            /* 出力データ（個人成績一覧＋志望大学）*/
            SelstdDprtExcelMakerOutputStudentDataBean outputData = new SelstdDprtExcelMakerOutputStudentDataBean();

            /* 出力データに個人IDをセット */
            outputData.setIndividualId(outputDataRecord.get(0).getIndividualId());

            /* 出力データに学年をセット */
            outputData.setGrade(outputDataRecord.get(0).getGrade());

            /* 出力データにクラスをセット */
            outputData.setCls(outputDataRecord.get(0).getCls());

            /* 出力データにクラス番号をセット */
            outputData.setClassNo(outputDataRecord.get(0).getClassNo());

            /* 出力データに対象模試をセット */
            outputData.setExamCd(outputDataRecord.get(0).getExamCd());

            /* 出力データに氏名（カナ）をセット */
            outputData.setNameKana(outputDataRecord.get(0).getNameKana());

            /* 出力データに文理コードをセット */
            outputData.setBunriCode(outputDataRecord.get(0).getBunriCode());

            /* 出力データに対象模試名をセット */
            outputData.setExamName(outputDataRecord.get(0).getExamName());

            /* 出力データに対象模試月をセット */
            outputData.setInpleDate(outputDataRecord.get(0).getInpleDate());

            /* 出力データに対象模試タイプをセット */
            outputData.setExamType(outputDataRecord.get(0).getExamType());

            /* 出力データに対象模試タイプをセット */
            outputData.setExamOpenDate(outputDataRecord.get(0).getExamOpenDate());

            /* 成績データが無ければ個人成績一覧リストはセットしない */
            if (outputDataRecord.get(0).getSubCd() != null) {
                /* 出力データに個人成績一覧リストをセット */
                outputData.getRecordBeanList().addAll(outputDataRecord);
            }

            /* 出力データをリストにセット */
            outputDataBeanList.add(outputData);

            /* 個人成績一覧データをクリア */
            outputDataRecord.clear();
        }

        /* 志望大学データを取得して出力データリストにセットする */
        for (int index = 0; index < candidateUnivBeanList.size(); index++) {

            /* 志望大学データ１人分のデータを取得する */
            outputDataCandidateUniv.add(candidateUnivBeanList.get(index));
            if (index != candidateUnivBeanList.size() - 1) {

                if (candidateUnivBeanList.get(index).getIndividualId().intValue() == candidateUnivBeanList.get(index + 1).getIndividualId().intValue()) {
                    if (candidateUnivBeanList.get(index).getExamCd().equals(candidateUnivBeanList.get(index + 1).getExamCd())) {
                        continue;
                    }
                }
            }

            /* 志望大学データが無ければ志望大学リストはセットしない */
            if (outputDataCandidateUniv.get(0).getUnivCd() != null) {

                /* 出力データリストから個人IDが一致する生徒を探す */
                if (outputDataBeanList.size() > 1) {
                    for (int cntStudent = 0; cntStudent < outputDataBeanList.size(); cntStudent++) {
                        if (outputDataBeanList.get(cntStudent).getExamCd() == null) {
                            continue;
                        }
                        if (outputDataBeanList.get(cntStudent).getIndividualId().intValue() == outputDataCandidateUniv.get(0).getIndividualId().intValue()) {
                            if (outputDataBeanList.get(cntStudent).getExamCd().equals(outputDataCandidateUniv.get(0).getExamCd())) {
                                /*  志望大学データを出力データをリストにセットする */
                                outputDataBeanList.get(cntStudent).getCandidateUnivBeanList().addAll(outputDataCandidateUniv);
                            }
                        }
                    }
                } else {
                    if (outputDataBeanList.get(0).getExamCd() == null) {
                        continue;
                    }
                    if (outputDataBeanList.get(0).getExamCd().equals(outputDataCandidateUniv.get(0).getExamCd())) {
                        /*  志望大学データを出力データをリストにセットする */
                        outputDataBeanList.get(0).getCandidateUnivBeanList().addAll(outputDataCandidateUniv);
                    }
                }
            }

            /* 志望大学データをクリア */
            outputDataCandidateUniv.clear();
        }
    }

    /**
     * 志望大学の判定を実行するメソッドです。
     *
     * @param inBean 画面からの設定値
     * @param outputDataBeanList 出力データリスト（個人成績一覧＋志望大学）
     */
    private void judgeStudentCandidateUniv(SelstdDprtExcelMakerInBean inBean, List<SelstdDprtExcelMakerOutputStudentDataBean> outputDataBeanList) {
        ExamBean targetExam = null;

        /* 全生徒に対して判定を実行する */
        for (SelstdDprtExcelMakerOutputStudentDataBean bean : outputDataBeanList) {
            targetExam = null;

            /* 模試対象のExamBeanを検索 */
            for (ExamBean exam : systemDto.getExamList()) {
                if (exam.getExamCd().equals(bean.getExamCd())) {
                    targetExam = exam;
                    break;
                }
            }

            /* 成績、又は志望大学が空の生徒は判定を実行しない */
            if (bean.getRecordBeanList().isEmpty() || bean.getCandidateUnivBeanList().isEmpty()) {
                continue;
            }

            /* 判定用成績データを生成する */
            ScoreBean score = scoreBeanService.createScoreBean(targetExam, bean.getIndividualId());

            /* 判定を実行する */
            /* SelstdDprtExcelMakerOutputStudentDataBean bean2 = bean; */
            /*
            BZJudgementExecutor.getInstance().judge(score, bean.getCandidateUnivBeanList(), false);
            BZJudgementExecutor.getInstance().judge(score, bean2.getCandidateUnivBeanList(), true);
            */
            BZJudgementExecutor.getInstance().judge(score, bean.getCandidateUnivBeanList());
            /* bean.getCandidateUnivBeanList().get(0).setValuateInclude(bean2.getCandidateUnivBeanList().get(0).getValuateInclude()); */
        }
    }

    /**
     * 学年・クラス文字列を作成するメソッドです。
     *
     * @param pageGrade 学年
     * @param pageCls クラス
     * @return 学年・クラス文字列
     */
    public String createTargetClass(Integer pageGrade, String pageCls) {

        String gradeStr = TITLE_GRADE;
        String classStr = TITLE_CLS;

        /* 学年をセットする */
        if (pageGrade != null) {
            gradeStr += String.valueOf(pageGrade);
        }

        /* クラスをセットする */
        if (pageCls != null) {
            classStr += pageCls;
        }

        /* 学年・クラス文字列を返す */
        return gradeStr + classStr;
    }

    /**
     * 学年・クラス文字列を作成するメソッドです。
     *
     * @param pageName 氏名
     * @return 氏名文字列
     */
    public String createName(String pageName) {

        String nameStr = TITLE_STUDENT_NAME_ONLY;

        /* 学年をセットする */
        if (pageName != null) {
            nameStr += String.valueOf(pageName);
        }
        /* 学年・クラス文字列を返す */
        return nameStr;
    }

    /**
     * 学年・クラス・クラス番号文字列を作成するメソッドです。
     *
     * @param pageGrade 学年
     * @param pageCls クラス
     * @param pageClsNo クラス番号
     * @return 学年・クラス文字列
     */
    public String createTargetClass(Integer pageGrade, String pageCls, String pageClsNo) {

        String gradeStr = TITLE_GRADE;
        String classStr = TITLE_CLS;
        String clsnoStr = TITLE_STUDENT_CLSNO;

        /* 学年をセットする */
        if (pageGrade != null) {
            gradeStr += String.valueOf(pageGrade);
        }

        /* クラスをセットする */
        if (pageCls != null) {
            classStr += pageCls;
        }

        /* クラス番号をセットする */
        if (pageClsNo != null) {
            clsnoStr += pageClsNo;
        }

        /* 生徒情報出力用の文字列を返す */
        return gradeStr + classStr + clsnoStr;
    }

    /**
     * 対象模試文字列を作成するメソッドです。
     *
     * @param examName 対象模試
     * @return 対象模試文字列
     */
    public String createTargetExam(String examName) {

        String targetExam = TITLE_EXAM;

        /* 模試名をセットする */
        if (examName != null) {
            targetExam += examName;
        }

        /* 対象模試文字列を返す */
        return targetExam;
    }

    /**
     * 生徒情報出力用の文字列を作成するメソッドです。
     *
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @param nameKana カナ氏名
     * @return 生徒情報出力用の文字列
     */
    public String createStudent(Integer grade, String cls, String classNo, String nameKana) {

        String gradeStr = TITLE_STUDENT_GRADE;
        String classStr = TITLE_STUDENT_CLS;
        String clsnoStr = TITLE_STUDENT_CLSNO;
        String nameStr = TITLE_STUDENT_NAME;

        /* 学年をセットする */
        if (grade != null) {
            gradeStr += String.valueOf(grade.intValue());
        }

        /* クラスをセットする */
        if (cls != null) {
            classStr += cls;
        }

        /* クラス番号をセットする */
        if (classNo != null) {
            clsnoStr += classNo;
        }

        /* カナ氏名をセットする */
        if (nameKana != null) {
            nameStr += nameKana;
        }

        /* 生徒情報出力用の文字列を返す */
        return gradeStr + classStr + clsnoStr + nameStr;
    }

    /**
     * マーク模試の国語(科目コード：3910)成績をセットします。
     *
     * @param exam 対象模試
     * @param outputData 個人成績一覧データリスト（１人分）
     */
    public void setResultsMarkJapanese(ExamBean exam, List<SelstdDprtExcelMakerRecordBean> outputData) {
        SelstdDprtExcelMakerRecordBean jap = null;
        /* SelstdDprtExcelMakerRecordBean japdesc = null; */
        for (Iterator<SelstdDprtExcelMakerRecordBean> ite = outputData.iterator(); ite.hasNext();) {
            SelstdDprtExcelMakerRecordBean rec = ite.next();
            if (COURSE_JAP.equals(rec.getCourseCd())) {
                /* 国語（科目コード：3910）の得点・換算得点・科目偏差値を出力する */
                if (SUBCODE_CENTER_DEFAULT_JAPANESE.equals(rec.getSubCd())) {
                    jap = rec;
                }
                /* 国語記述（科目コード：3915）の得点・換算得点・科目偏差値を出力する */
                /*
                if (SUBCODE_CENTER_DEFAULT_JKIJUTSU.equals(rec.getSubCd())) {
                    japdesc = rec;
                }
                */
                ite.remove();
            }
        }
        /* 国語のデータ追加 */
        if (jap != null) {
            outputData.add(jap);
        }
        /* 国語記述のデータ追加 */
        /*
        if (japdesc != null) {
            outputData.add(japdesc);
        }
        */
    }

    /**
     * マーク模試の国語(科目コード：3910)成績をセットします。
     *
     * @param exam 対象模試
     * @param outputData 個人成績一覧データリスト（１人分）
     */
    public void setResultsMarkStudentJapanese(ExamBean exam, List<SelstdDprtExcelMakerStudentRecordBean> outputData) {
        SelstdDprtExcelMakerStudentRecordBean jap = null;
        /* SelstdDprtExcelMakerStudentRecordBean japdesc = null; */
        for (Iterator<SelstdDprtExcelMakerStudentRecordBean> ite = outputData.iterator(); ite.hasNext();) {
            SelstdDprtExcelMakerStudentRecordBean rec = ite.next();
            if (COURSE_JAP.equals(rec.getCourseCd())) {
                /* 国語（科目コード：3910）の得点・換算得点・科目偏差値を出力する */
                if (SUBCODE_CENTER_DEFAULT_JAPANESE.equals(rec.getSubCd())) {
                    jap = rec;
                }
                /* 国語記述（科目コード：3915）の得点・換算得点・科目偏差値を出力する */
                /*
                if (SUBCODE_CENTER_DEFAULT_JKIJUTSU.equals(rec.getSubCd())) {
                    japdesc = rec;
                }
                */
                ite.remove();
            }
        }
        /* 国語のデータ追加 */
        if (jap != null) {
            outputData.add(jap);
        }
        /* 国語記述のデータ追加 */
        /*
        if (japdesc != null) {
            outputData.add(japdesc);
        }
        */
    }

}

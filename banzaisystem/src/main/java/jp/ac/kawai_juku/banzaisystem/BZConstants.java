package jp.ac.kawai_juku.banzaisystem;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ENGLISH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JAPANESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHONETWO;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATHTWO;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score;

import org.seasar.framework.util.PropertiesUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * バンザイシステムの定数定義クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZConstants {

	/** バージョン(文字列) */
	public static final String VERSION;

	/** バージョン(数値) */
	public static final double VERSION_NUMBER;

	static {
		InputStream in = null;
		try {
			in = BZConstants.class.getResourceAsStream("/updater.properties");
			Properties props = new Properties();
			PropertiesUtil.load(props, in);
			VERSION = props.getProperty("version");
			VERSION_NUMBER = Double.parseDouble(VERSION);
		} finally {
			IOUtil.close(in);
		}
	}

	/** 大学マスタ必要バージョン */
	public static final String REQUIRED_MASTER_VERSION = "201703";

	/** 成績データ必要バージョン */
	public static final int REQUIRED_RECORD_VERSION = 2;

	/** フラグ：ON */
	public static final String FLG_ON = "1";

	/** フラグ：OFF */
	public static final String FLG_OFF = "0";

	/** データファイル名（大学マスタ） */
	public static final String UNIV_FILE_NAME = "univ.db";

	/** データファイル名（成績データ） */
	public static final String RECORD_FILE_NAME = "record.db";

	/** ログファイル名 */
	public static final String LOG_FILE_NAME = "bz_" + getHostName() + ".log";

	/** 環境設定ファイル名 */
	public static final String CONFIG_FILE_NAME = "configuration.properties";

	/** 設定ファイル名 */
	public static final String SETTING_FILE_NAME = "banzai.properties";

	/** 範囲区分：9 */
	public static final String ANSWER_NO_NA = Integer.toString(Score.ANSWER_NO_NA);

	/** 範囲区分：0 */
	public static final String ANSWER_NO_OTHER = Integer.toString(Score.ANSWER_NO_OTHER);

	/** 範囲区分：1 */
	public static final String ANSWER_NO_1 = Integer.toString(Score.ANSWER_NO_1);

	/** 範囲区分：2 */
	public static final String RANGE_SECOND_UPPER = Integer.toString(Score.RANGE_SECOND_UPPER);

	/** 範囲区分：1 */
	public static final String RANGE_SECOND_LOWER = Integer.toString(Score.RANGE_SECOND_LOWER);

	/** 教科コード（英語） */
	public static final String COURSE_ENG = "1000";

	/** 教科コード（数学） */
	public static final String COURSE_MAT = "2000";

	/** 教科コード（国語） */
	public static final String COURSE_JAP = "3000";

	/** 教科コード（理科） */
	public static final String COURSE_SCI = "4000";

	/** 教科コード（地歴公民） */
	public static final String COURSE_SOC = "5000";

	/** 教科コード（総合１） */
	public static final String COURSE_TOTAL1 = "7000";

	/** 教科コード（総合２） */
	public static final String COURSE_TOTAL2 = "8000";

	/** 模試科目マスタに存在しないマーク模試国語の科目名定義 */
	public static final Map<String, String> MARK_JAP_SUBNAME_MAP;

	static {
		Map<String, String> map = CollectionsUtil.newHashMap();
		map.put(SUBCODE_CENTER_DEFAULT_LITERATURE, "現代文");
		map.put(SUBCODE_CENTER_DEFAULT_OLD, "古文");
		map.put(SUBCODE_CENTER_DEFAULT_CHINESE, "漢文");
		MARK_JAP_SUBNAME_MAP = Collections.unmodifiableMap(map);
	}

	/** 第1解答科目対象模試（＃１〜＃３マーク、センタープレ、センター・リサーチ） */
	public static final Set<String> ANS1ST_EXAM;

	static {
		Set<String> set = CollectionsUtil.newHashSet();
		set.add(Exam.Code.MARK1);
		set.add(Exam.Code.MARK2);
		set.add(Exam.Code.MARK3);
		set.add(Exam.Code.CENTERPRE);
		set.add(Exam.Code.CENTERRESEARCH);
		ANS1ST_EXAM = Collections.unmodifiableSet(set);
	}

	/** マーク集計科目の科目コードセット */
	public static final Set<String> MARK_AGGREGATE_SUBCD;

	static {
		Set<String> set = CollectionsUtil.newHashSet();
		set.add(SUBCODE_CENTER_DEFAULT_ENGLISH);
		set.add(SUBCODE_CENTER_DEFAULT_MATHONE);
		set.add(SUBCODE_CENTER_DEFAULT_MATHTWO);
		set.add(SUBCODE_CENTER_DEFAULT_MATHONETWO);
		set.add(SUBCODE_CENTER_DEFAULT_JAPANESE);
		set.add(SUBCODE_CENTER_DEFAULT_LITERATURE_OLD);
		MARK_AGGREGATE_SUBCD = Collections.unmodifiableSet(set);
	}

	/** 文理コード：文系 */
	public static final String BUNRICD_ARTS = "1";

	/** 文理コード：理系 */
	public static final String BUNRICD_SCIENCE = "2";

	/** 模試記入志望大学の最大数 */
	public static final int EXAM_UNIV_MAX = 9;

	/** システム登録志望大学の最大数 */
	public static final int CANDIDATE_UNIV_MAX = 40;

	/** 受験予定大学数の最大数 */
	public static final int EXAM_PLAN_UNIV_MAX = 26;

	/** 定員信頼 非表示 */
	public static final String CAPACITY_NONE = "8";

	/** 定員信頼 推定 */
	public static final String CAPACITY_ESTIMATE = "9";

	/** 定員信頼 推定 */
	public static final String CAPACITY_ESTIMATE_DISP = "推";

	/** アスタリスク */
	public static final String ASTERISK = "*";

	/** 半角ハイフン */
	public static final String HYPHEN = "-";

	/** 等幅フォント：MSゴシック */
	public static final String MONOSPACED_FONT = "ＭＳ ゴシック";

	/** SUB_CEFR(科目) */
	public static final String SUB_CEFR = "cefr";

	/** 国語記述(科目) */
	public static final String SUB_JPW = "jpnDesc";

	/** 国語記述式小問評価（問１〜問３） */
	public static final String[] JPN_DESC_EVALUATION = { "", "a", "b", "c", "d" };

	/** 認定試験コード：英検 */
	public static final String ENG_EXAM_RESULT_CD = "20";

	/** 大学区分コード：この大学の入試情報を見る除外 */
	public static final List<String> EXCLUDE_VOCATIONAL_SCHOOL;

	/** 大学区分コード：二次公表科目除外 */
	public static final List<String> EXCLUDE_EXAM_SECOND;

	/** 模試コード：共通テストリサーチ */
	public static final String EXAM_RESARCH = "38";

	static {
		List<String> set = CollectionsUtil.newArrayList(2);
		set.add("08");
		set.add("09");
		EXCLUDE_VOCATIONAL_SCHOOL = set;
	}

	static {
		List<String> set = CollectionsUtil.newArrayList(4);
		set.add("04");
		set.add("05");
		set.add("06");
		set.add("07");
		EXCLUDE_EXAM_SECOND = set;
	}

	/**
	 * コンストラクタです。
	 */
	private BZConstants() {
	}

	/**
	 * ホスト名を返します。
	 *
	 * @return ホスト名
	 */
	private static String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "Unknown";
		}
	}

}

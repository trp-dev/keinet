package jp.ac.kawai_juku.banzaisystem.exmntn.input.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.input.helper.ExmntnInputHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.input.view.ExmntnInputView;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;

/**
 *
 * CEFR、国語記述入力画面のコントローラです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
public class ExmntnInputController extends AbstractController {

    /** View */
    @Resource(name = "exmntnInputView")
    private ExmntnInputView view;

    /** service */
    @Resource(name = "exmntnInputHelper")
    private ExmntnInputHelper service;

    /**
     * 初期表示アクションです。
     *
     * @return 一覧出力ダイアログ
     */
    @Logging(GamenId.C1_d2)
    @Execute
    public ExecuteResult index() {

        /* コントロールの初期化 */
        /*service.initControls();*/

        return VIEW_RESULT;
    }
    /*
    *//**
      * 認定試験名１コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult joiningExam1ComboBoxAction() {
    
      認定試験名に応じて認定試験レベルを変更
     return service.changeEngPtComboBox(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * 認定試験レベル１コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult joiningExamLevel1ComboBoxAction() {
    
      認定試験レベルに応じてスコアの活性／非活性を制御
     return service.joiningExamLevelComboBox(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * CEFR１コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult cefrLevel1ComboBoxAction() {
    
      スコアを算出して表示
     return service.chageCefrLevelComboBox(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * スコアが不明１チェックボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult unknownCefrScore1CheckBoxAction() {
    
      CEFRレベル、英検合否の活性／非活性を制御
     return service.changeUnknownCefrScoreCheckBox(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * CEFRレベルが不明１チェックボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult unknownCefrLevel1CheckBoxAction() {
    
      スコア、CEFRレベルのクリアおよび、英検合否の活性／非活性を制御
     return service.changeUnknownCefrLevelCheckBox(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * 英検合否１コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult engExamResult1ComboBoxAction() {
    
      英検合否からスコアとCEFRレベルを算出して表示
     return service.changeEngExamResult(ExmntnInputHelper.SWITCH.ENG1);
    }
    
    *//**
      * 認定試験名２コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult joiningExam2ComboBoxAction() {
    
      認定試験名に応じて認定試験レベルを変更
     return service.changeEngPtComboBox(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    *//**
      * 認定試験レベル２コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult joiningExamLevel2ComboBoxAction() {
    
      認定試験レベルに応じてスコアの活性／非活性を制御
     return service.joiningExamLevelComboBox(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    *//**
      * CEFR２コンボボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult cefrLevel2ComboBoxAction() {
    
      スコアを算出して表示
     return service.chageCefrLevelComboBox(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    *//**
      * スコアが不明２チェックボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult unknownCefrScore2CheckBoxAction() {
    
      CEFRレベル、英検合否の活性／非活性を制御
     return service.changeUnknownCefrScoreCheckBox(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    *//**
      * CEFRレベルが不明２チェックボックス変更アクションです。
      *
      * @return アクション結果
      */
    /*
    @Execute
    public ExecuteResult unknownCefrLevel2CheckBoxAction() {
    
      スコア、CEFRレベルのクリアおよび、英検合否の活性／非活性を制御
     return service.changeUnknownCefrLevelCheckBox(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    *//**
      * 英検合否２コンボボックス変更アクションです。
      *
      * @return アクション結果
      */

    /*
    @Execute
    public ExecuteResult engExamResult2ComboBoxAction() {
    
      英検合否からスコアとCEFRレベルを算出して表示
     return service.changeEngExamResult(ExmntnInputHelper.SWITCH.ENG2);
    }
    
    */
    /**
     * 問１コンボボックス変更アクションです。
     *
     * @return アクション結果
     */
    /*
    @Execute
    public ExecuteResult jpnWriting1ComboBoxAction() {
    
        /* 総合評価を設定する
        return service.changeJpnWritingComboBox();
    }
    */
    /**
     * 問２コンボボックス変更アクションです。
     *
     * @return アクション結果
     */
    /*
    @Execute
    public ExecuteResult jpnWriting2ComboBoxAction() {
    
        /* 総合評価を設定する
        return service.changeJpnWritingComboBox();
    }
    */
    /**
     * 問３コンボボックス変更アクションです。
     *
     * @return アクション結果
     */

    /*
    @Execute
    public ExecuteResult jpnWriting3ComboBoxAction() {
    
        /* 総合評価を設定する
        return service.changeJpnWritingComboBox();
    }
    */
    /**
     * OKボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {

        /* 総合評価を設定する */
        boolean ret = service.clickOkButtunAction();
        if (!ret) {
            return null;
        } else {
            return CLOSE_RESULT;
        }
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        return CLOSE_RESULT;
    }

}

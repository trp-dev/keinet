package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.JScrollPane;

/**
 *
 * バンザイシステムのScrollPaneです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZScrollPane extends JScrollPane {

    /**
     * コンストラクタです。
     *
     * @param vsbPolicy 縦スクロールバーのポリシー
     * @param hsbPolicy 横スクロールバーのポリシー
     */
    public BZScrollPane(int vsbPolicy, int hsbPolicy) {
        super(vsbPolicy, hsbPolicy);
        getVerticalScrollBar().setUnitIncrement(10);
        getHorizontalScrollBar().setUnitIncrement(18);
    }

}

package jp.ac.kawai_juku.banzaisystem.result.dunv.view;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZList;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;

/**
 *
 * 大学選択ダイアログのViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class ResultDunvView extends AbstractView {

    /** 大学名 */
    @Location(x = 124, y = 29)
    @Size(width = 139, height = 20)
    public TextField univNameField;

    /** 検索開始ボタン */
    @Location(x = 266, y = 27)
    @Disabled
    public ImageButton startButton;

    /** 大学区分コンボボックス */
    @Location(x = 29, y = 86)
    @Size(width = 84, height = 20)
    public ComboBox<Code> univDivComboBox;

    /** 大学リスト */
    @Location(x = 28, y = 108)
    @Size(width = 117, height = 224)
    public BZList<LabelValueBean> univList;

    /** 学部リスト */
    @Location(x = 161, y = 108)
    @Size(width = 117, height = 224)
    public BZList<LabelValueBean> facultyList;

    /** 学科リスト */
    @Location(x = 296, y = 108)
    @Size(width = 117, height = 224)
    public BZList<LabelValueBean> deptList;

    /** OKボタン */
    @Location(x = 138, y = 358)
    @Disabled
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 225, y = 358)
    public ImageButton cancelButton;

    @Override
    public void initialize() {

        super.initialize();

        ((AbstractDocument) univNameField.getDocument()).setDocumentFilter(new DocumentFilter() {
            @Override
            public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
                super.remove(fb, offset, length);
                changeButtonStatus();
            }

            @Override
            public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
                    throws BadLocationException {
                super.replace(fb, offset, length, text, attrs);
                changeButtonStatus();
            }

            /**
             * ボタンの状態を変更します。
             */
            private void changeButtonStatus() {
                startButton.setEnabled(!univNameField.getText().isEmpty());
            }
        });
    }

}

package jp.ac.kawai_juku.banzaisystem.result.univ.bean;

import jp.ac.kawai_juku.banzaisystem.Code;

/**
 *
 * 入試結果入力-大学指定(一覧)画面の大学情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultUnivBean {

    /** 大学コード */
    private String univCd;

    /** 大学名 */
    private String univName;

    /** 大学名カナ */
    private String univNameKana;

    /** 合格発表日 */
    private String sucAnnDate;

    /** 大学区分 */
    private Code univDiv;

    /**
     * 大学コードを返します。
     *
     * @return 大学コード
     */
    public String getUnivCd() {
        return univCd;
    }

    /**
     * 大学コードをセットします。
     *
     * @param univCd 大学コード
     */
    public void setUnivCd(String univCd) {
        this.univCd = univCd;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 合格発表日を返します。
     *
     * @return 合格発表日
     */
    public String getSucAnnDate() {
        return sucAnnDate;
    }

    /**
     * 合格発表日をセットします。
     *
     * @param sucAnnDate 合格発表日
     */
    public void setSucAnnDate(String sucAnnDate) {
        this.sucAnnDate = sucAnnDate;
    }

    /**
     * 大学区分を返します。
     *
     * @return 大学区分
     */
    public Code getUnivDiv() {
        return univDiv;
    }

    /**
     * 大学区分をセットします。
     *
     * @param univDiv 大学区分
     */
    public void setUnivDiv(Code univDiv) {
        this.univDiv = univDiv;
    }

    /**
     * 大学名カナを返します。
     *
     * @return 大学名カナ
     */
    public String getUnivNameKana() {
        return univNameKana;
    }

    /**
     * 大学名カナをセットします。
     *
     * @param univNameKana 大学名カナ
     */
    public void setUnivNameKana(String univNameKana) {
        this.univNameKana = univNameKana;
    }

}

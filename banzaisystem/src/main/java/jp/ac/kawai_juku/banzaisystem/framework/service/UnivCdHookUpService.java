package jp.ac.kawai_juku.banzaisystem.framework.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.CandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamPlanUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivCdHookUpBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.UnivCdHookUpDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.ac.kawai_juku.banzaisystem.framework.util.Counter;
import jp.ac.kawai_juku.banzaisystem.framework.util.FlagUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学コード紐付け処理のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UnivCdHookUpService {

    /** DAO */
    @Resource
    private UnivCdHookUpDao univCdHookUpDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 大学コード紐付け処理が必要であるかを判断します。
     *
     * @return 大学コード紐付け処理が必要ならtrue
     */
    public boolean needsHookUp() {
        if (systemDto.isMismatchingMasterVersionFlg()) {
            /* 大学マスタバージョンに不整合があるなら紐付けしない */
            return false;
        }
        return !systemDto.getUnivExamDiv().equals(systemDto.getRecordInfo().getExamDiv());
    }

    /**
     * 大学コード紐付け処理を実行します。
     */
    @Lock
    public void hookUp() {
        for (UnivCdHookUpBean bean : univCdHookUpDao.selectUnivCdHookUpList(systemDto)) {
            hookUpCandidateUniv(bean);
        }
        for (Integer id : univCdHookUpDao.selectUnivCdHookUpExamPlanUnivList(systemDto)) {
            hookUpExamPlanUniv(id);
        }
        updateRecordInfo();
    }

    /**
     * 成績データ情報の年度・模試区分を大学マスタに合わせます。
     */
    public void updateRecordInfo() {
        univCdHookUpDao.updateRecordInfo(systemDto);
        systemDto.getRecordInfo().setYear(systemDto.getUnivYear());
        systemDto.getRecordInfo().setExamDiv(systemDto.getUnivExamDiv());
    }

    /**
     * [志望大学] 指定した対象を紐付けます。
     *
     * @param target {@link UnivCdHookUpBean}
     */
    private void hookUpCandidateUniv(UnivCdHookUpBean target) {

        List<CandidateUnivBean> list = univCdHookUpDao.selectCandidateUnivList(target);

        /* 同じ区分で重複しているレコードを削除する */
        Set<Pair<String, String>> pairSet = CollectionsUtil.newHashSet();
        for (Iterator<CandidateUnivBean> ite = list.iterator(); ite.hasNext();) {
            CandidateUnivBean bean = ite.next();
            if (!pairSet.add(Pair.of(bean.getSection(), bean.getUnivCd() + bean.getFacultyCd() + bean.getDeptCd()))) {
                ite.remove();
            }
        }

        /* 区分2のうち、区分1と重複するレコードを非表示にする（重複していなければ表示する） */
        for (CandidateUnivBean bean : list) {
            if (bean.getSection().equals(Code.CANDIDATE_UNIV_SCREEN.getValue())) {
                bean.setDispFlg(FlagUtil.toString(!pairSet.contains(Pair.of(Code.CANDIDATE_UNIV_EXAM.getValue(),
                        bean.getUnivCd() + bean.getFacultyCd() + bean.getDeptCd()))));
            }
        }

        /* 表示フラグ別に表示順を振り直す */
        Counter<String> counter = Counter.newInstance();
        for (CandidateUnivBean bean : list) {
            bean.setDispNum(counter.add(bean.getDispFlg()));
            /* 最新年度・模試区分にしておく */
            bean.setYear(systemDto.getUnivYear());
            bean.setExamDiv(systemDto.getUnivExamDiv());
        }

        /* 登録しなおす */
        univCdHookUpDao.deleteCandidateUniv(target);
        univCdHookUpDao.insertCandidateUniv(list);
    }

    /**
     * [志望志望大学] 指定した対象を紐付けます。
     *
     * @param id 個人ID
     */
    private void hookUpExamPlanUniv(Integer id) {

        List<ExamPlanUnivBean> list = univCdHookUpDao.selectExamPlanUnivList(id);

        /* 重複しているレコードを削除する */
        Set<UnivKeyBean> keySet = new HashSet<>();
        for (Iterator<ExamPlanUnivBean> ite = list.iterator(); ite.hasNext();) {
            ExamPlanUnivBean bean = ite.next();
            if (!keySet.add(bean)) {
                ite.remove();
            }
        }

        /* 志望順位を振り直す */
        int rank = 1;
        for (ExamPlanUnivBean bean : list) {
            bean.setCandidateRank(Integer.valueOf(rank++));
            /* 最新年度・模試区分にしておく */
            bean.setYear(systemDto.getUnivYear());
            bean.setExamDiv(systemDto.getUnivExamDiv());
        }

        /* 登録しなおす */
        univCdHookUpDao.deleteExamPlanUniv(id);
        univCdHookUpDao.insertExamPlanUniv(list);
    }

}

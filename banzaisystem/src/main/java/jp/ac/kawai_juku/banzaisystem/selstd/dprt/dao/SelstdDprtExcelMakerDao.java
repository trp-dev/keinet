package jp.ac.kawai_juku.banzaisystem.selstd.dprt.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelCefrMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelCefrMakerStudentRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelJpnMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelJpnMakerStudentRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerCandidateUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerRecordBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerStudentRecordBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 個人成績・志望一覧帳票用のDAOです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 * @author TOTEC)FURUZAWA.Yusuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDprtExcelMakerDao extends BaseDao {

    /**
     * 個人成績・志望一覧帳票用データ(個人成績一覧)を取得します。
     *
     * @param ans1st 第1解答科目対象模試フラグ（1：対象、0：対象外）
     * @param examcd 模試コード
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @param form03Flg A01_03帳票フラグ（1：A01_03帳票である、0：A01_03以外の帳票）
     * @return 個人成績一覧データリスト
     */
    public List<SelstdDprtExcelMakerRecordBean> selectExcelRecordList(String ans1st, String examcd, String year, String individualid, String form03Flg) {
        BeanMap param = new BeanMap();
        param.put("ans1st", ans1st);
        param.put("examcd", examcd);
        param.put("year", year);
        param.put("individualid", individualid);
        param.put("form03flg", form03Flg);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelMakerRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(個人成績一覧)を取得します(ドッキング対象)。
     * 受験者の最新模試の成績データを取得
     * @param ans1st 第1解答科目対象模試フラグ（1：対象、0：対象外）
     * @param examcd 模試コード
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @param form03Flg A01_03帳票フラグ（1：A01_03帳票である、0：A01_03以外の帳票）
     * @return 個人成績一覧データリスト
     */
    public List<SelstdDprtExcelMakerRecordBean> selectExcelRecordListDoc(String ans1st, String examcd, String year, String individualid, String form03Flg) {
        BeanMap param = new BeanMap();
        param.put("ans1st", ans1st);
        param.put("examcd", examcd);
        param.put("year", year);
        param.put("individualid", individualid);
        param.put("form03flg", form03Flg);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelMakerRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(連続個人成績表)を取得します。
     *
     * @param ans1st 第1解答科目対象模試フラグ（1：対象、0：対象外）
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @param form03Flg A01_03帳票フラグ（1：A01_03帳票である、0：A01_03以外の帳票）
     * @return 個人成績一覧データリスト
     */
    public List<SelstdDprtExcelMakerStudentRecordBean> selectExcelRecordStudentList(String ans1st, String year, String individualid, String form03Flg) {
        BeanMap param = new BeanMap();
        param.put("ans1st", ans1st);
        param.put("year", year);
        param.put("individualid", individualid);
        param.put("form03flg", form03Flg);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelMakerStudentRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(CEFR情報)を取得します。
     *
     * @param examcd 模試コード
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return CEFR情報
     */
    public List<SelstdDprtExcelCefrMakerRecordBean> selectExcelCefrRecordList(String examcd, String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("examcd", examcd);
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelCefrMakerRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(国語記述)を取得します。
     *
     * @param examcd 模試コード
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return 国語記述
     */
    public List<SelstdDprtExcelJpnMakerRecordBean> selectExcelJpnRecordList(String examcd, String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("examcd", examcd);
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelJpnMakerRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(CEFR情報)を取得します。
     *
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return CEFR情報
     */
    public List<SelstdDprtExcelCefrMakerStudentRecordBean> selectExcelCefrRecordStudentList(String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelCefrMakerStudentRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(国語記述)を取得します。
     *
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return 国語記述
     */
    public List<SelstdDprtExcelJpnMakerStudentRecordBean> selectExcelJpnRecordStudentList(String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelJpnMakerStudentRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(志望大学)を取得します。
     *
     * @param examcd 模試コード
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return 志望大学データリスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivBean> selectExcelCandidateUnivList(String examcd, String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("examcd", examcd);
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelMakerCandidateUnivBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人成績・志望一覧帳票用データ(志望大学)を取得します。
     *
     * @param year 年度
     * @param individualid 個人ID（in句用文字列）
     * @return 志望大学データリスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivStudentBean> selectExcelCandidateUnivStudentList(String year, String individualid) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("individualid", individualid);
        return jdbcManager.selectBySqlFile(SelstdDprtExcelMakerCandidateUnivStudentBean.class, getSqlPath(), param).getResultList();
    }
}

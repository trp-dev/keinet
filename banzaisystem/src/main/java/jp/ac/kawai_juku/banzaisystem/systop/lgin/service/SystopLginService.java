package jp.ac.kawai_juku.banzaisystem.systop.lgin.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.bean.SystopLginDataStatusBean;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.dao.SystopLginDao;

/**
 *
 * マスタ・プログラムチェック画面のサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopLginService {

    /** DAO */
    @Resource
    private SystopLginDao systopLginDao;

    /**
     * データ状況リストを取得します。
     *
     * @param dbSet ダウンロード可能大学マスタセット
     * @return データ状況リスト
     */
    public List<SystopLginDataStatusBean> findDataStatusList(UnivDbSet dbSet) {
        List<SystopLginDataStatusBean> list = systopLginDao.selectDataStatusList();
        if (dbSet != null) {
            for (SystopLginDataStatusBean bean : list) {
                if (bean.getUnivStatus().equals(Code.UNIV_DL_AVAILABLE.getValue())
                        && dbSet.get(bean.getExamYear(), bean.getExamDiv()) == null) {
                    /* 取り込んでいないダウンロード不可の大学マスタを「提供予定」に変更する */
                    bean.setUnivStatus(Code.UNIV_DL_UNAVAILABLE.getValue());
                }
            }
        }
        return list;
    }

}

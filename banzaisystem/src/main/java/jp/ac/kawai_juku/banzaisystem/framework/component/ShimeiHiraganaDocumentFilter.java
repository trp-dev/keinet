package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.StyleConstants;

import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;

/**
 *
 * 半角カタカナ変換後に氏名として登録できるひらがな文字以外の入力を制限するドキュメントフィルタです。
 * <br />入力可能な文字種の指定は ValidateUtil クラスの関数 isShimeiHiragana に依存しています。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ShimeiHiraganaDocumentFilter extends DocumentFilter {

    /** 最大文字数 */
    private static final int MAX_LENGTH = 14;

    @Override
    public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr) throws BadLocationException {

        /* IM/FEPが機能している場合は AttributeSet にその旨が通知されます。 */
        /* IME経由の文字入力の場合：AttributeSet = composed text=java.text.AttributedString@5e1897 */
        /* 確定済みの文字入力の場合：AttributeSet = null */
        if (attr != null) {
            if (attr.isDefined(StyleConstants.ComposedTextAttribute)) {
                /* マルチバイト文字の入力の途中経過のものについては無視する */
                /* この入力内容がテキストボックス上での「確定前状態の文字列の表示」に使われる */
                super.insertString(fb, offset, str, attr); /* 継承元に任せる */
            } else {
                /* 他にも Bold・ALIGN_RIGHT・Underline など様々な文字修飾の情報が定義されている */
                super.insertString(fb, offset, str, attr); /* 継承元に任せる */
            }
        } else {
            /* マルチバイト文字の入力が確定したものについては内容を確認する */
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
            sb.insert(offset, str);
            String afterStr = sb.toString();
            if (check(afterStr)) {
                super.insertString(fb, offset, str, attr);
            }
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attr)
            throws BadLocationException {

        if (length == 0) {
            insertString(fb, offset, text, attr);
        } else {

            /* IM/FEPが機能している場合は AttributeSet にその旨が通知されます。 */
            /* IME経由の文字入力の場合：AttributeSet = composed text=java.text.AttributedString@5e1897 */
            /* 確定済みの文字入力の場合：AttributeSet = null */
            if (attr != null) {
                if (attr.isDefined(StyleConstants.ComposedTextAttribute)) {
                    /* マルチバイト文字の入力の途中経過のものについては無視する */
                    /* この入力内容がテキストボックス上での「確定前状態の文字列の表示」に使われる */
                    super.insertString(fb, offset, text, attr); /* 継承元に任せる */
                } else {
                    /* 他にも Bold・ALIGN_RIGHT・Underline など様々な文字修飾の情報が定義されている */
                    super.insertString(fb, offset, text, attr); /* 継承元に任せる */
                }
            } else {
                /* マルチバイト文字の入力が確定したものについては内容を確認する */
                Document doc = fb.getDocument();
                StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
                sb.delete(offset, offset + length);
                if (text != null) {
                    sb.insert(offset, text);
                }
                String afterStr = sb.toString();
                if (check(afterStr)) {
                    super.replace(fb, offset, length, text, attr);
                }
            }
        }
    }

    /**
     * 文字列が最大文字数以下かつ半角カタカナ変換後に氏名として登録できる
     * ひらがなのみで構成されていることをチェックします。
     *
     * @param str 文字列
     * @return チェック結果
     */
    private boolean check(String str) {
        return str.length() <= MAX_LENGTH && ValidateUtil.isShimeiHiragana(str);
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.dao.ScoreBeanDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.co.fj.kawaijuku.judgement.data.Examination;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 模試に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ExamUtil {

    /**
     * コンストラクタです。
     */
    private ExamUtil() {
    }

    /**
     * 模試がマーク模試であるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return マーク模試ならtrue
     */
    public static boolean isMark(ExamBean exam) {
        return Exam.Type.MARK.equals(exam.getExamTypeCd());
    }

    /**
     * 模試が記述模試であるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return 記述模試ならtrue
     */
    public static boolean isWritten(ExamBean exam) {
        return !isMark(exam);
    }

    /**
     * 模試がセンタープレであるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return センタープレならtrue
     */
    public static boolean isPre(ExamBean exam) {
        return Exam.Code.CENTERPRE.equals(exam.getExamCd());
    }

    /**
     * 模試がセンター・リサーチであるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return センター・リサーチならtrue
     */
    public static boolean isCenter(ExamBean exam) {
        return Exam.Code.CENTERRESEARCH.equals(exam.getExamCd());
    }

    /**
     * 模試が高２記述であるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return 高２記述ならtrue
     */
    public static boolean is2ndWrtn(ExamBean exam) {
        return Exam.Code.WRTN_2GRADE.equals(exam.getExamCd());
    }

    /**
     * 模試が第1解答科目対象模試であるかを判定します。
     *
     * @param exam {@link ExamBean}
     * @return 第1解答科目対象模試ならtrue
     */
    public static boolean isAns1st(ExamBean exam) {
        return BZConstants.ANS1ST_EXAM.contains(exam.getExamCd());
    }

    /**
     * 模試コンボボックス用ラベルに変換します。
     *
     * @param exam {@link ExamBean}
     * @return 模試名ラベル
     */
    public static String toLabel(ExamBean exam) {
        if (StringUtils.isEmpty(exam.getExamName())) {
            return StringUtils.EMPTY;
        } else {
            return exam.getExamName() + "（" + Integer.parseInt(exam.getInpleDate().substring(4, 6)) + "月）";
        }
    }

    /**
     * 対象模試を判断して返します。
     *
     * @param markExam マーク模試
     * @param writtenExam 記述模試
     * @return 対象模試
     */
    public static ExamBean judgeTargetExam(ExamBean markExam, ExamBean writtenExam) {
        if (markExam == null && writtenExam == null) {
            return null;
        } else if (markExam == null) {
            return writtenExam;
        } else if (writtenExam == null || isPre(markExam) || isCenter(markExam)) {
            /* センタープレとセンター・リサーチのみマークを優先する */
            return markExam;
        } else {
            return writtenExam;
        }
    }

    /**
     * 対象模試のドッキング先模試を返します。
     *
     * @param combo 模試コンボボックス
     * @return 対象模試のドッキング先模試
     */
    public static ExamBean findDockingExam(ComboBox<ExamBean> combo) {
        ExamBean targetExam = combo.getSelectedValue();
        if (targetExam != null) {
            for (int i = 0; i < combo.getItemCount(); i++) {
                ExamBean exam = combo.getItemAt(i).getValue();
                if (exam.getExamCd().equals(targetExam.getDockingExamCd()) || targetExam.getExamCd().equals(exam.getDockingExamCd())) {
                    return exam;
                }
            }
        }
        return null;
    }

    /**
     * 対象模試のドッキング先模試を返します。
     * (成績が存在するドッキング対象を返します。)
     * @param combo 模試コンボボックス
     * @param id 生徒ID
     * @param scoreBeanDao {@link ScoreBeanDao}
     * @return 対象模試のドッキング先模試
     */
    public static ExamBean findDockingExamStudent(ComboBox<ExamBean> combo, int id, ScoreBeanDao scoreBeanDao) {
        List<BZSubjectRecord> recordList = null;
        ExamBean targetExam = combo.getSelectedValue();
        if (targetExam != null) {
            for (int i = 0; i < combo.getItemCount(); i++) {
                ExamBean exam = combo.getItemAt(i).getValue();
                if (exam.getExamCd().equals(targetExam.getDockingExamCd()) || targetExam.getExamCd().equals(exam.getDockingExamCd())) {
                    recordList = scoreBeanDao.selectSubRecordList(id, exam);
                    /* 成績データチェック */
                    if (recordList.size() > 0) {
                        return exam;
                    } else {
                        /* 成績データがない場合は対象としないため次の模試を検索 */
                    }
                }
            }
        }
        return null;
    }

    /**
     * 対象模試のドッキング先模試を返します。
     * (成績が存在するドッキング対象を返します。)
     *
     * @param inBean 対象模試
     * @param id 生徒ID
     * @param scoreBeanDao スコア情報
     * @param systemDto システムDTO
     * @return 対象模試のドッキング先模試
     */
    public static ExamBean findDockingExamStudentB(ExamBean inBean, int id, ScoreBeanDao scoreBeanDao, SystemDto systemDto) {
        List<BZSubjectRecord> recordList = null;
        ExamBean targetExam = inBean;
        if (targetExam != null) {
            for (ExamBean exam : systemDto.getExamList()) {
                if (exam.getExamCd().equals(targetExam.getDockingExamCd()) || targetExam.getExamCd().equals(exam.getDockingExamCd())) {
                    recordList = scoreBeanDao.selectSubRecordList(id, exam);
                    /* 成績データチェック */
                    if (recordList.size() > 0) {
                        return exam;
                    } else {
                        /* 成績データがない場合は対象としないため次の模試を検索 */
                    }
                }
            }
        }
        return null;
    }

    /**
     * 対象模試のExamBeanを返します。
     * @param examcd 模試コード
     * @param systemDto {@link SystemDto}
     * @return 対象模試のExamBean
     */
    public static ExamBean findExamBean(String examcd, SystemDto systemDto) {
        for (ExamBean exam : systemDto.getExamList()) {
            if (exam.getExamCd().equals(examcd)) {
                return exam;
            }
        }
        return null;
    }

    /**
     * ExamBeanクラスをExaminationクラスに変換します。
     *
     * @param exam ExamBeanクラス
     * @return Examinationクラス
     */
    public static Examination toExamination(ExamBean exam) {
        if (exam != null) {
            Examination e = new Examination(exam.getExamYear(), exam.getExamCd());
            e.setExamName(exam.getExamName());
            e.setExamNameAbbr(exam.getExamNameAbbr());
            e.setExamTypeCd(exam.getExamTypeCd());
            return e;
        } else {
            return null;
        }
    }

}

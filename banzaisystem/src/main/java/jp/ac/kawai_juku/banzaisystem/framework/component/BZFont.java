package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Font;

/**
 *
 * バンザイシステムのフォント定義です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum BZFont {

    /** デフォルトのフォント定義です */
    DEFAULT("ＭＳ Ｐゴシック", Font.PLAIN, 12),

    /** デフォルトのボールド定義です */
    BOLD("ＭＳ Ｐゴシック", Font.PLAIN, 12),

    /** 成績入力画面の入力チェックエラーメッセージのフォント定義です */
    INPUT_SCORE_ERROR("ＭＳ Ｐゴシック", Font.PLAIN, 10),

    /** 帯別グラフヘッダのフォント定義です */
    DIST_GRAPH_HEADER("ＭＳ Ｐゴシック", Font.PLAIN, 10),

    /** 検索結果件数のフォント定義です */
    RESULT_COUNT("ＭＳ Ｐゴシック", Font.BOLD, 20),

    /** 評価文字列のフォント定義です */
    RATING("Meiryo UI", Font.BOLD, 32),

    /** 評価ポイントのフォント定義です */
    RATING_POINT("ＭＳ Ｐゴシック", Font.BOLD, 22),

    /** 大学名のフォント定義です */
    UNIV_NAME("ＭＳ Ｐゴシック", Font.PLAIN, 13),

    /** 志望大学詳細画面 補足メッセージのフォント定義です */
    APPEDIX_MSG("ＭＳ Ｐゴシック", Font.PLAIN, 11),

    /** 志望大学詳細画面 備考欄のフォント定義です */
    UNIV_REMARKS("ＭＳ Ｐゴシック", Font.PLAIN, 10),

    /** 中系統のフォント定義です */
    MSTEMMA("ＭＳ Ｐゴシック", Font.PLAIN, 14),

    /** 資格カテゴリのフォント定義です */
    CREDENTIAL_CATEGORY("ＭＳ Ｐゴシック", Font.PLAIN, 14),

    /** 受験スケジュールの学部名・学科名のフォント定義です */
    SCHEDULE_NAME("ＭＳ Ｐゴシック", Font.PLAIN, 11),

    /** カレンダーの年月ラベルのフォント定義です */
    CALENDAR_MONTH("ＭＳ ゴシック", Font.BOLD, 20),

    /** 学部学科選択ダイアログの大学名のフォント定義です */
    UNIV_NAME_DIALOG("ＭＳ Ｐゴシック", Font.BOLD, 17),

    /** 一括出力項目のフォント定義です */
    BATCH_OUTPUT_ITEM("ＭＳ Ｐゴシック", Font.PLAIN, 14),

    /** 入試結果入力-大学指定画面の合格発表日のフォント定義です */
    SUCANNDATE("ＭＳ ゴシック", Font.PLAIN, 12),

    /** 入試結果入力-大学指定(カレンダー)画面のカレンダーの日付のフォント定義です */
    CALENDAR_DATE("ＭＳ Ｐゴシック", Font.BOLD, 13);

    /** フォント */
    private final Font font;

    /**
     * コンストラクタです。
     *
     * @param name フォント名
     * @param style フォントスタイル
     * @param size フォントサイズ
     */
    BZFont(String name, int style, int size) {
        font = new Font(name, style, size);
    }

    /**
     * フォント定義を返します。
     *
     * @return フォント定義
     */
    public Font getFont() {
        return font;
    }

}

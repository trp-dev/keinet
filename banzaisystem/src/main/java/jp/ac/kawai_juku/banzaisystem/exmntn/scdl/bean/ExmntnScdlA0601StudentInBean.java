package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

/**
 * 帳票生成サービス（受験スケジュール）の生徒一人分の情報を保持する入力Beanです。
 *
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnScdlA0601StudentInBean implements ExcelMakerInBean {

    /** 生徒情報Bean */
    private final ExmntnUnivStudentBean studentBean;

    /** 成績データ */
    private final ScoreBean scoreBean;

    /** 受験スケジュールリスト */
    private final List<ExmntnScdlBean> scheduleList;

    /**
     * コンストラクタです。
     *
     * @param studentBean 生徒情報Bean
     * @param scoreBean 成績データ
     * @param scheduleList 受験スケジュールリスト
     */
    public ExmntnScdlA0601StudentInBean(ExmntnUnivStudentBean studentBean, ScoreBean scoreBean,
            List<ExmntnScdlBean> scheduleList) {
        this.studentBean = studentBean;
        this.scoreBean = scoreBean;
        this.scheduleList = scheduleList;
    }

    /**
     * 生徒情報Beanを返します。
     *
     * @return 生徒情報Bean
     */
    public ExmntnUnivStudentBean getStudentBean() {
        return studentBean;
    }

    /**
     * 成績データを返します。
     *
     * @return 成績データ
     */
    public ScoreBean getScoreBean() {
        return scoreBean;
    }

    /**
     * 受験スケジュールリストを返します。
     *
     * @return 受験スケジュールリスト
     */
    public List<ExmntnScdlBean> getScheduleList() {
        return scheduleList;
    }

}

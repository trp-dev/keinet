package jp.ac.kawai_juku.banzaisystem.mainte.main.controller;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.MenuController;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.controller.MainteImptController;
import jp.ac.kawai_juku.banzaisystem.mainte.main.view.MainteMainView;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.controller.MainteMstrController;

/**
 *
 * メンテナンスタブ画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMainController extends MenuController {

    /** View */
    @Resource
    private MainteMainView mainteMainView;

    /**
     * 初期表示アクションです。
     *
     * @return メンテナンス画面
     */
    @Execute
    public ExecuteResult index() {
        mainteMainView.tabbedPane.activate();
        return VIEW_RESULT;
    }

    /**
     * インポート状況確認アクションです。
     *
     * @return 個人成績インポート画面
     */
    @Execute
    public ExecuteResult checkImportSituationAction() {
        mainteMainView.tabbedPane.activate(MainteImptController.class);
        return VIEW_RESULT;
    }

    /**
     * メンテナンス-大学マスタ・プログラム最新化画面表示アクションです。
     *
     * @return メンテナンス-大学マスタ・プログラム最新化画面
     */
    @Execute
    public ExecuteResult dispMainteMstrAction() {
        mainteMainView.tabbedPane.activate(MainteMstrController.class);
        return VIEW_RESULT;
    }

    @Override
    @Execute
    public ExecuteResult exmntnButtonAction() {
        return forward(ExmntnMainController.class, "redisplay");
    }

}

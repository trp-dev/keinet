package jp.ac.kawai_juku.banzaisystem.selstd.main.component;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JPanel;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.LayerSupport;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人名＋志望大学テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdMainCandidateTable extends SelstdMainTable implements LayerSupport {

    /** 検索ボタン表示用のレイヤ */
    private final JLayer<JComponent> layer = new JLayer<JComponent>(this);

    /** 選択済大学リスト */
    private final List<SelstdDunvBean> univList = CollectionsUtil.newArrayList();

    /**
     * コンストラクタです。
     */
    public SelstdMainCandidateTable() {
        super("number", TableType.CANDIDATE);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        JPanel glassPane = layer.getGlassPane();
        glassPane.setLayout(null);
        glassPane.setVisible(true);
        ImageButton searchButton = new ImageButton();
        searchButton.initialize(view, "searchButton");
        searchButton.setLocation(516, 18);
        glassPane.add(searchButton);
    }

    @Override
    protected JPanel createColumnHeaderView() {
        ImagePanel panel = new ImagePanel(SelstdMainCandidateTable.class, "bgCandidate.png");
        int x = 0;
        for (JComponent jc : createColumnHeaderList()) {
            if (panel.getHeight() == jc.getHeight()) {
                jc.setLocation(x, 0);
            } else {
                jc.setLocation(x, panel.getHeight() - jc.getHeight());
            }
            panel.add(jc);
            x += jc.getWidth();
        }

        return panel;
    }

    /**
     * ヘッダコンポーネントリストを生成します。
     *
     * @return ヘッダコンポーネントリスト
     */
    private List<JComponent> createColumnHeaderList() {

        List<JComponent> list = CollectionsUtil.newArrayList();
        list.add(createHeaderButton("grade", CellAlignment.CENTER, true));
        list.add(createHeaderButton("class", CellAlignment.CENTER, true));
        list.add(createHeaderButton("number", CellAlignment.CENTER, true));
        list.add(createHeaderButton("name", CellAlignment.LEFT, true));
        list.add(createHeaderLabel("synthetic1", CellAlignment.CENTER, false));
        list.add(createHeaderButton("synthetic1Score", CellAlignment.RIGHT, false));
        list.add(createHeaderButton("synthetic1Dev", CellAlignment.RIGHT, true));
        list.add(createHeaderButton("wishRank", CellAlignment.CENTER, true));
        list.add(createHeaderButton("university", CellAlignment.LEFT, true));
        list.add(createHeaderLabel("faculty", CellAlignment.LEFT, true));
        list.add(createHeaderLabel("subjectSchedule", CellAlignment.LEFT, true));
        list.add(createHeaderButton("exclude", CellAlignment.CENTER, true));
        /*list.add(createHeaderButton("include", CellAlignment.CENTER, true));*/
        list.add(createHeaderButton("secondStage", CellAlignment.CENTER, true));
        list.add(createHeaderButton("synthetic", CellAlignment.CENTER, true));
        list.add(createHeaderLabel("blank2", CellAlignment.LEFT, false));

        return list;
    }

    @Override
    protected JPanel createFixedColumnHeaderView() {
        /* 固定領域はなし */
        return null;
    }

    @Override
    public JLayer<JComponent> getLayer() {
        return layer;
    }

    /**
     * 選択済大学リストを返します。
     *
     * @return 選択済大学リスト
     */
    public List<SelstdDunvBean> getUnivList() {
        return univList;
    }

    /**
     * 選択済大学リストをセットします。
     *
     * @param list 選択済大学リスト
     */
    public void setUnivList(List<SelstdDunvBean> list) {
        univList.clear();
        univList.addAll(list);
        setDataVector(getTable(), getHeader());
    }

    @Override
    protected Vector<Vector<CellData>> toDataVector(JPanel header, List<Map<String, CellData>> dataList) {

        if (!univList.isEmpty()) {
            /* 大学コードで絞り込む */
            List<Map<String, CellData>> list = CollectionsUtil.newArrayList(dataList.size());
            Set<String> univCdSet = CollectionsUtil.newHashSet(univList.size());
            for (SelstdDunvBean bean : univList) {
                univCdSet.add(bean.getUnivCd());
            }
            for (Map<String, CellData> row : dataList) {
                String univCd = (String) row.get("univCd").getDispValue();
                if (univCdSet.contains(univCd)) {
                    list.add(row);
                }
            }
            return super.toDataVector(header, list);
        }

        return super.toDataVector(header, dataList);
    }

}

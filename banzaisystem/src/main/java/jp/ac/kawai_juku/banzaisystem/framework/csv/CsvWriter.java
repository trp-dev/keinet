package jp.ac.kawai_juku.banzaisystem.framework.csv;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * CSVのライタークラスです。<br>
 * ★注意★処理の最後に {@code close()} メソッドを必ず呼び出してください。<br>
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CsvWriter implements Closeable {

    /** デフォルトのキャラクタセット */
    private static final String DEFAULT_CHARSET_NAME = "Windows-31J";

    /** 区切り文字はカンマで固定 */
    private static final char DELIMITER = ',';

    /** ダブルクオーテーション */
    private static final char QUOTE = '"';

    /** 改行コード（LF） */
    private static final char LF = '\n';

    /** 改行コード（CRLF） */
    private static final String CRLF = "\r\n";

    /** 列リスト */
    private List<String> columnList = CollectionsUtil.newArrayList(300);

    /** 出力先 */
    private final BufferedWriter out;

    /** ダブルクオートフラグ（値の内容に関わらずダブルクオートで囲う場合はtrue） */
    private boolean quoteFlg;

    /**
     * コンストラクタです。
     *
     * @param writer CSVデータの出力先
     */
    public CsvWriter(Writer writer) {
        out = new BufferedWriter(writer);
    }

    /**
     * コンストラクタです。 <br>
     * 出力データのキャラクタセットは「Windows-31J」として扱います。
     *
     * @param os CSVデータの出力先
     */
    public CsvWriter(OutputStream os) {
        this(createOutputStreamWriter(os, DEFAULT_CHARSET_NAME));
    }

    /**
     * コンストラクタです。
     *
     * @param os CSVデータの出力先
     * @param charsetName 出力データのキャラクタセット
     */
    public CsvWriter(OutputStream os, String charsetName) {
        this(createOutputStreamWriter(os, charsetName));
    }

    /**
     * 出力ストリームから指定した文字セットで出力ストリームライタを生成します。
     *
     * @param os 出力ストリーム
     * @param charsetName 文字セット
     * @return 出力ストリームライタ
     */
    private static OutputStreamWriter createOutputStreamWriter(OutputStream os, String charsetName) {
        try {
            return new OutputStreamWriter(os, charsetName);
        } catch (UnsupportedEncodingException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * ダブルクオートフラグをセットします。
     *
     * @param quoteFlg ダブルクオートフラグ
     */
    public void setQuoteFlg(boolean quoteFlg) {
        this.quoteFlg = quoteFlg;
    }

    /**
     * CSVの列を追加します。<br>
     * {@code addRow()}メソッドを呼び出すまで出力先に書き込みません。
     *
     * @param column 列の文字列
     * @return このCsvWriterのインスタンス
     */
    public CsvWriter addColumn(String column) {
        columnList.add(column);
        return this;
    }

    /**
     * CSVの列を複数追加します。
     * {@code addRow()}メソッドを呼び出すまで出力先に書き込みません。
     *
     * @param columns 列の文字列配列
     * @return このCsvWriterのインスタンス
     */
    public CsvWriter addColumns(String[] columns) {
        for (String column : columns) {
            addColumn(column);
        }
        return this;
    }

    /**
     * CSVの列を複数追加します。
     * {@code addRow()}メソッドを呼び出すまで出力先に書き込みません。
     *
     * @param columns 列の文字列リスト
     * @return このCsvWriterのインスタンス
     */
    public CsvWriter addColumns(List<String> columns) {
        columnList.addAll(columns);
        return this;
    }

    /**
     * CSVデータ1行を出力先に書き込みます。<br>
     * 書き込んだ後、addで追加された列情報はクリアします。
     *
     * @return このCsvWriterのインスタンス
     */
    public CsvWriter addRow() {

        try {
            for (int i = 0; i < columnList.size(); i++) {
                writeColumn(columnList.get(i));
                if (i + 1 < columnList.size()) {
                    out.write(DELIMITER);
                }
            }
            out.write(CRLF);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }

        columnList.clear();

        return this;
    }

    /**
     * CSV1列の値を書き出します。
     *
     * @param value 書き出す文字列
     * @throws IOException IO例外
     */
    private void writeColumn(String value) throws IOException {

        if (StringUtils.isEmpty(value)) {
            return;
        }

        if (quoteFlg || value.indexOf(DELIMITER) > -1 || value.indexOf(QUOTE) > -1 || value.indexOf(LF) > -1) {
            out.write(QUOTE);
            for (char c : value.toCharArray()) {
                if (c == QUOTE) {
                    out.write(QUOTE);
                    out.write(QUOTE);
                } else {
                    out.write(c);
                }
            }
            out.write(QUOTE);
        } else {
            out.write(value);
        }
    }

    /**
     * 出力先のWriterを閉じます。
     */
    @Override
    public void close() {
        IOUtil.close(out);
    }

}

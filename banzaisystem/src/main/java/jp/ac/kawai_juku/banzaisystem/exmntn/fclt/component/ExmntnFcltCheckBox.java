package jp.ac.kawai_juku.banzaisystem.exmntn.fclt.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.annotation.Academic;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 大学検索（学部系統）画面のチェックボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnFcltCheckBox extends CodeCheckBox {

    /** 文系フラグ */
    private boolean isAcademic;

    @Override
    public void initialize(Field field, AbstractView view) {
        super.initialize(field, view);
        isAcademic = field.isAnnotationPresent(Academic.class);
    }

    /**
     * 文系フラグを返します。
     *
     * @return 文系フラグ
     */
    public boolean isAcademic() {
        return isAcademic;
    }

}

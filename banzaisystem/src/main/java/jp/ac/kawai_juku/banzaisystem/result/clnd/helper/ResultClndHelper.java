package jp.ac.kawai_juku.banzaisystem.result.clnd.helper;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.result.clnd.view.ResultClndView;
import jp.ac.kawai_juku.banzaisystem.result.univ.service.ResultUnivService;

/**
 *
 * 入試結果入力-大学指定(カレンダー)画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultClndHelper {

    /** View */
    @Resource(name = "resultClndView")
    private ResultClndView view;

    /** 入試結果入力-大学指定(一覧)画面のサービス */
    @Resource
    private ResultUnivService resultUnivService;

    /**
     * 受験者を表示します。
     *
     * @param sucAnnDate 合格発表日
     * @param univCdList 大学コードリスト
     */
    public void displayExaminee(String sucAnnDate, List<String> univCdList) {
        view.examineeTable.setDataList(resultUnivService.findExamineeList(univCdList, sucAnnDate,
                view.gradeComboBox.getSelectedValue(), view.classComboBox.getSelectedValue()));
    }

}

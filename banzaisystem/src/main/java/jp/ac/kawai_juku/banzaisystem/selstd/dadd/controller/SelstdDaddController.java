package jp.ac.kawai_juku.banzaisystem.selstd.dadd.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import javax.annotation.Resource;
import javax.swing.SwingUtilities;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.CloseResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.bean.SelstdDaddKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.service.SelstdDaddService;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.view.SelstdDaddView;
import jp.ac.kawai_juku.banzaisystem.selstd.main.controller.SelstdMainController;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 個人編集ダイアログのコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class SelstdDaddController extends AbstractController {

    /** Service */
    @Resource
    private SelstdDaddService selstdDaddService;

    /** View */
    @Resource
    private SelstdDaddView selstdDaddView;

    /** （このダイアログから見て親となる）対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** （このダイアログから見て親となる）対象生徒選択画面のController */
    @Resource
    private SelstdMainController selstdMainController;

    /** 更新対象生徒の個人ID */
    private Integer id;

    /**
     * 生徒追加時の初期表示アクションです。
     *
     * @return 個人編集ダイアログ
     */
    @Logging(GamenId.B_d3)
    @Execute
    public ExecuteResult index() {

        /* ダイアログ上の表示要素を初期化する */
        initControls();

        /* 更新対象生徒の個人IDはNULLで初期化しておく */
        id = null;

        return VIEW_RESULT;
    }

    /**
     * 生徒更新時の初期表示アクションです。
     *
     * @param id 個人ID
     * @return 個人編集ダイアログ
     */
    @Logging(GamenId.B_d3)
    @Execute
    public ExecuteResult edit(Integer id) {

        /* 生徒更新の場合：更新対象生徒の個人情報をコンポーネントに設定する */
        setKojinInfo(selstdDaddService.findKojinBean(systemDto.getUnivYear(), id));

        /* 更新対象生徒の個人IDを保持する */
        this.id = id;

        return VIEW_RESULT;
    }

    /**
     * OKボタンアクションです。
     * <br/>関数の実行タイミング：「OKボタンがクリックされた」
     *
     * @return なし（ダイアログは本処理内で閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {

        /* 画面上の入力値を取得 */
        Integer grade = selstdDaddView.gradeComboBox.getSelectedValue();
        String cls = selstdDaddView.classField.getText();
        String classNo = selstdDaddView.numberField.getText();

        /* かな氏名（全角ひらがな）はDB登録用の文字種（半角カタカナ）に変換しておく */
        String nameKana = JpnStringUtil.hira2Hkana(selstdDaddView.nameField.getText());

        /* システム管理上の「年度」を取得 */
        String nendo = systemDto.getUnivYear();

        /* 名寄せ処理を行う */
        switch (selstdDaddService.doNameBasedAggregation(nendo, id, grade, cls, classNo, nameKana, getActiveWindow())) {
            case FAILURE:
                /* 名寄せ失敗：生徒情報を登録する */
                validateOkButton(nendo, grade, cls, classNo, nameKana);
                selstdDaddService.registKojin(nendo, id, grade.toString(), cls, classNo, nameKana);
                break;
            case ABORT:
                /* 名寄せ中断 */
                return null;
            default:
                /* 名寄せ成功 */
                break;
        }

        return new CloseResult() {
            @Override
            public void process(AbstractController controller) {

                /* ダイアログを閉じる */
                super.process(controller);

                /*
                 * 対象生徒選択画面を再表示する。
                 * ダイアログが閉じる前に対象生徒選択画面が再表示されないよう、
                 * イベントディスパッチスレッドのキューに追加する。
                 */
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        selstdMainController.index();
                    }
                });
            }
        };
    }

    /**
     * キャンセルボタンアクションです。
     * <br/>関数の実行タイミング：「キャンセルボタンがクリックされた」
     *
     * @return Viewを閉じる結果オブジェクト（→ダイアログを閉じるアクションが実行される）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        /* 処理概要：ダイアログを閉じる */
        /* ＃キャンセル時は特に何もしない */
        /* 返却物：Viewを閉じる結果オブジェクト */
        return CLOSE_RESULT;
    }

    /* ▼ 画面要素-初期化関連：ここから */

    /**
     * ダイアログ上の表示要素を初期化します。
     */
    private void initControls() {

        /* 学年コンボボックスを初期化する */
        initGradeComboBox();

        /* クラステキストフィールドを初期化する */
        selstdDaddView.classField.setText(StringUtils.EMPTY);

        /* クラス番号テキストフィールドを初期化する */
        selstdDaddView.numberField.setText(StringUtils.EMPTY);

        /* かな氏名テキストフィールドを初期化する */
        selstdDaddView.nameField.setText(StringUtils.EMPTY);
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        /* 3年を初期選択値とする */
        selstdDaddView.gradeComboBox.setSelectedItem(new ComboBoxItem<Integer>(3));
    }

    /**
     * 更新対象生徒の個人情報をコンポーネントに設定します。
     *
     * @param kojinBean {@link SelstdDaddKojinBean}
     */
    private void setKojinInfo(SelstdDaddKojinBean kojinBean) {

        /* 学年を設定する */
        if (kojinBean.getGrade() != null) {
            selstdDaddView.gradeComboBox.setSelectedItem(new ComboBoxItem<Integer>(kojinBean.getGrade()));
        }

        /* DBの学年とコンボボックスの選択値が一致しない場合は3年を選択値にする */
        if (!selstdDaddView.gradeComboBox.getSelectedValue().equals(kojinBean.getGrade())) {
            initGradeComboBox();
        }

        /* クラスを設定する */
        selstdDaddView.classField.setText(kojinBean.getCls());

        /* クラス番号を設定する */
        selstdDaddView.numberField.setText(kojinBean.getClassNo());

        /* かな氏名を設定する */
        selstdDaddView.nameField.setText(toHiragana(kojinBean.getNameKana()));
    }

    /**
     * カナ氏名（半角カタカナ）を全角ひらがなに変換します。
     *
     * @param nameKana カナ氏名（半角カタカナ）
     * @return かな氏名（全角ひらがな）
     */
    private String toHiragana(String nameKana) {
        String hiragana = JpnStringUtil.hkana2Hira(nameKana);
        if (hiragana == null) {
            return null;
        } else {
            StringBuilder buff = new StringBuilder(hiragana.length());
            for (char c : hiragana.toCharArray()) {
                if (ValidateUtil.isShimeiHiragana(new String(new char[] { c }))) {
                    /* 画面で入力できる文字のみを格納する */
                    buff.append(c);
                }
            }
            return buff.toString();
        }
    }

    /* ▲ 画面要素-初期化関連：ここまで */

    /* ▼ 検証処理：ここから */

    /**
     * OKボタンがクリックされた際の入力値の検証を行います。
     *
     * @param nendo 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @param nameKana カナ氏名（半角カタカナ）
     */
    private void validateOkButton(String nendo, Integer grade, String cls, String classNo, String nameKana) {

        /* ボタン押下時、入力値にエラーがあった場合はエラーダイアログを表示する。（【メッセージ一覧】No.2〜8参照） */
        /* エラーが複数ある場合は全てのエラーメッセージを表示する。 */

        /* 検証グループ１：単独検証 */
        /* B-d3 B_E002：同一年度・学年・クラス・クラス番号の生徒が存在する場合に、「OK」ボタンを押下 */
        if (selstdDaddService.hasRegisteredHistoryInfo(nendo, grade, cls, classNo, id)) {
            throw new MessageDialogException(getMessage("error.selstd.dadd.B-d3_E002"));
        }

        /* 検証グループ２：一括検証 */
        StringBuilder msgPack = new StringBuilder();

        /* B-d3 B_E003：クラスが未入力の場合に、「OK」ボタンを押下 */
        if (StringUtils.isEmpty(cls)) {
            if (msgPack.length() > 0) {
                msgPack.append("\n"); /* 既存のメッセージがある場合は改行 */
            }
            msgPack.append(getMessage("error.selstd.dadd.B-d3_E003")); /* 今回分のメッセージを追加 */
        }

        /* B-d3 B_E004：クラス番号が未入力の場合に、「OK」ボタンを押下 */
        if (StringUtils.isEmpty(classNo)) {
            if (msgPack.length() > 0) {
                msgPack.append("\n"); /* 既存のメッセージがある場合は改行 */
            }
            msgPack.append(getMessage("error.selstd.dadd.B-d3_E004")); /* 今回分のメッセージを追加 */
        }

        /* B-d3 B_E005：かな氏名が未入力の場合に、「OK」ボタンを押下 */
        if (StringUtils.isEmpty(nameKana)) {
            if (msgPack.length() > 0) {
                msgPack.append("\n"); /* 既存のメッセージがある場合は改行 */
            }
            msgPack.append(getMessage("error.selstd.dadd.B-d3_E005")); /* 今回分のメッセージを追加 */
        }

        /* 通知用のメッセージが格納されていたら例外を投げる */
        if (msgPack.length() > 0) {
            throw new MessageDialogException(msgPack.toString());
        }
    }

    /* ▲ 検証処理：ここまで */

}

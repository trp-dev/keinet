package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.bean;

import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsSearchResultBean;

/**
 *
 * 志望大学リスト詳細Beanです。
 *
 *
 * @author TOTEC)FURUZAWA.Yusuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnPrntUnivDetailBean extends ExmntnSubsSearchResultBean {

    /** 区分 */
    private String section;

    /** 注意マーク表示フラグ */
    private boolean isCautionNeeded;

    /**
     * 区分を返します。
     *
     * @return 区分
     */
    public String getSection() {
        return section;
    }

    /**
     * 区分をセットします。
     *
     * @param section 区分
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 注意マーク表示フラグを返します。
     *
     * @return 注意マーク表示フラグ
     */
    public boolean isCautionNeeded() {
        return isCautionNeeded;
    }

    /**
     * 注意マーク表示フラグをセットします。
     *
     * @param isCautionNeeded 注意マーク表示フラグ
     */
    public void setCautionNeeded(boolean isCautionNeeded) {
        this.isCautionNeeded = isCautionNeeded;
    }

}

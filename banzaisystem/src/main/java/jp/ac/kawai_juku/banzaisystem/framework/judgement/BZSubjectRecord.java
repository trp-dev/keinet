package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

/**
 *
 * バンザイシステムの科目成績データです。<br>
 * 教科コードを保持できるように {@link SubjectRecord} を拡張したBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZSubjectRecord extends SubjectRecord {

    /** 個人ID */
    private Integer individualId;

    /** 教科コード */
    private String courseCd;

    /** 模試コード */
    private String examcd;

    /**
     * コンストラクタです。
     */
    public BZSubjectRecord() {
        this.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
    }

    /**
     * コンストラクタです。
     *
     * @param subCd 科目コード
     * @param score 得点
     * @param cvsScore 換算得点
     * @param deviation 偏差値
     * @param scope 範囲区分
     * @param subjectAllotPnt 配点
     */
    public BZSubjectRecord(String subCd, String score, String cvsScore, String deviation, String scope, String subjectAllotPnt) {
        super(subCd, score, cvsScore, deviation, scope, subjectAllotPnt);
        if (subjectAllotPnt == null) {
            this.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
        }
    }

    /**
     * コンストラクタです。
     *
     * @param subCd 科目コード
     * @param score 得点
     * @param cvsScore 換算得点
     * @param deviation 偏差値
     * @param subjectAllotPnt 配点
     */
    public BZSubjectRecord(String subCd, String score, String cvsScore, String deviation, String subjectAllotPnt) {
        super(subCd, score, cvsScore, deviation, subjectAllotPnt);
        if (subjectAllotPnt == null) {
            this.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
        }
    }

    /**
     * コンストラクタです。
     *
     * @param subCd 科目コード
     * @param scope 範囲区分
     */
    public BZSubjectRecord(String subCd, String scope) {
        super(subCd, null, scope);
        this.setSubjectAllotPnt(String.valueOf(Detail.ALLOT_NA));
    }

    /**
     * 教科コードを返します。
     *
     * @return 教科コード
     */
    public String getCourseCd() {
        if (courseCd == null) {
            courseCd = getSubCd().substring(0, 1) + "000";
        }
        return courseCd;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examcd;
    }

    /**
     * 模試コードを設定します。
     *
     * @param examd 模試コード
     */
    public void setExamCd(String examd) {
        this.examcd = examd;
    }

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDを設定します。
     *
     * @param id 個人ID
     */
    public void setIndividualId(Integer id) {
        this.individualId = id;
    }

}

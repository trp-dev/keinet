package jp.ac.kawai_juku.banzaisystem.selstd.main.service;

import java.util.concurrent.Callable;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCandidateUnivBean;
import jp.co.fj.kawaijuku.judgement.data.UnivData;

import org.apache.commons.lang3.StringUtils;

/**
*
* 大学検索時の判定タスクです。
*
*
* @author QQ)Kurimoto
*
*/
public class JudgementTask implements Callable<SelstdMainCandidateUnivBean> {
    private final ScoreBean score;
    private final JudgementResultBean result;
    /* private final boolean noExclude; */

    /**
     * コンストラクタです。
     * @param score 成績情報
     * @param result 判定結果
     */
    public JudgementTask(ScoreBean score, JudgementResultBean result) {
        super();
        this.score = score;
        this.result = result;
    }

    @Override
    public SelstdMainCandidateUnivBean call() throws Exception {
        BZJudgementExecutor je = BZJudgementExecutor.getInstance();
        if (StringUtils.isEmpty(result.getUnivCd())) {
            return (SelstdMainCandidateUnivBean) result;
        }

        UnivData univ = je.createUniv(result);
        je.judge(score, univ, result);

        /* if (ExamUtil.isCenter(score.getMarkExam())) { */
        /* 判定用大学基本情報【含む】を作成 */
        /*    UnivData univ = je.createUniv(result, true);
            je.judge(score, univ, result);
        } else { */
        /* 判定用大学基本情報【含まない】を作成 */
        /*    UnivData univE = je.createUniv(result, false);
            je.judge(score, univE, result); */
        /* if (!noExclude) { */
        /* 判定用大学基本情報【含む】を作成 */
        /*    UnivData univI = je.createUniv(result, true);
            je.judge(score, univI, result);
        }
        */
        /* } */
        return (SelstdMainCandidateUnivBean) result;
    }

}

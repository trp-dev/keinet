package jp.ac.kawai_juku.banzaisystem.selstd.dstd.controller;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.swing.ListSelectionModel;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.output.main.view.OutputMainView;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.view.SelstdDprtView;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.dto.SelstdDstdDto;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.service.SelstdDstdService;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.view.SelstdDstdView;

/**
 *
 * 生徒一覧ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDstdController extends AbstractController {

    /** 動作モードを識別するenum */
    private enum Mode {

        /** 単一選択 */
        SINGLE {

            @Override
            protected void index(SelstdDstdView view, List<Map<String, CellData>> dataList) {
                view.studentTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                view.studentTable.setDataList(dataList);
            }

            @Override
            protected void okButtonAction(SelstdDstdView view, SelstdDstdDto dto) {
                List<Integer> list = view.studentTable.getSelectedIndividualIdList();
                if (!list.isEmpty()) {
                    getComponent(ExmntnMainController.class).dispStudent(list.get(0));
                }
            }
        },

        /** 複数選択 */
        MULTI {

            @Override
            protected void index(SelstdDstdView view, List<Map<String, CellData>> dataList) {
                view.studentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
                view.studentTable.setDataList(dataList);
            }

            @Override
            protected void okButtonAction(SelstdDstdView view, SelstdDstdDto dto) {
                dto.getSelectedIndividualIdList().clear();
                dto.getSelectedIndividualIdList().addAll(view.studentTable.getSelectedIndividualIdList());
            }
        };

        /**
         * 初期表示アクションでの処理を実装します。
         *
         * @param view ビュー
         * @param dataList データリスト
         */
        protected abstract void index(SelstdDstdView view, List<Map<String, CellData>> dataList);

        /**
         * OKボタンアクションでの処理を実装します。
         *
         * @param view ビュー
         * @param dto DTO
         */
        protected abstract void okButtonAction(SelstdDstdView view, SelstdDstdDto dto);

    }

    /** 動作モード */
    private Mode mode;

    /** View */
    @Resource
    private SelstdDstdView selstdDstdView;

    /** Service */
    @Resource
    private SelstdDstdService selstdDstdService;

    /** DTO */
    @Resource
    private SelstdDstdDto selstdDstdDto;

    /**
     * 個人成績画面用の単一選択モードアクションです。
     *
     * @return 生徒一覧ダイアログ
     */
    @Logging(GamenId.B_d4)
    @Execute
    public ExecuteResult index() {
        indexCommon(Mode.SINGLE, selstdDstdService.findKojinList());
        return VIEW_RESULT;
    }

    /**
     * 一覧出力ダイアログ用の複数選択モードアクションです。
     *
     * @return 生徒一覧ダイアログ
     */
    @Logging(GamenId.B_d4)
    @Execute
    public ExecuteResult indexMulti() {
        SelstdDprtView view = getComponent(SelstdDprtView.class);
        indexCommon(
                Mode.MULTI,
                selstdDstdService.findKojinList(view.gradeComboBox.getSelectedValue(),
                        view.classTable.getSelectedClassList(), view.numberStartField.getText(),
                        view.numberEndField.getText()));
        return VIEW_RESULT;
    }

    /**
     * 帳票出力・CSVファイル画面用の複数選択モードアクションです。
     *
     * @return 生徒一覧ダイアログ
     */
    @Logging(GamenId.B_d4)
    @Execute
    public ExecuteResult indexOutput() {
        OutputMainView view = getComponent(OutputMainView.class);
        indexCommon(
                Mode.MULTI,
                selstdDstdService.findKojinList(view.gradeComboBox.getSelectedValue(),
                        view.classTable.getSelectedClassList(), view.numberStartField.getText(),
                        view.numberEndField.getText()));
        return VIEW_RESULT;
    }

    /**
     * 指定したモードの初期表示アクションを実行します。
     *
     * @param mode モード
     * @param dataList データリスト
     */
    private void indexCommon(Mode mode, List<Map<String, CellData>> dataList) {
        this.mode = mode;
        mode.index(selstdDstdView, dataList);
    }

    /**
     * OKボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {
        mode.okButtonAction(selstdDstdView, selstdDstdDto);
        return CLOSE_RESULT;
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        selstdDstdDto.getSelectedIndividualIdList().clear();
        return CLOSE_RESULT;
    }

}

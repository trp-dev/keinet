package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;

import jp.ac.kawai_juku.banzaisystem.framework.BZMain;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.exception.ResourceNotFoundRuntimeException;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.ClassLoaderUtil;
import org.seasar.framework.util.FileInputStreamUtil;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.URLUtil;
import org.seasar.framework.util.ZipInputStreamUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 画像に関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ImageUtil {

    /** Logger */
    private static final Logger logger = Logger.getLogger(ImageUtil.class);

    /** 画像キャッシュ */
    private static final Map<String, BufferedImage> IMAGE_CACHE = CollectionsUtil.newHashMap();

    /**
     * コンストラクタです。
     */
    private ImageUtil() {
    }

    /**
     * 画像ファイルをキャッシュします。
     *
     * @param packageName 読み込み対象パッケージ名
     */
    public static void load(String packageName) {

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("画像ファイルのキャッシュ処理を開始します。[読み込み対象パッケージ=%s]", packageName));
        }

        Iterator<?> ite = ClassLoaderUtil.getResources(packageName.replace('.', '/'));
        if (ite.hasNext()) {
            URL resource = (URL) ite.next();
            String path = resource.getPath();
            if (path.startsWith("file:") && path.contains("!")) {
                /* 運用環境の場合はjarファイルから情報を得る */
                loadFromJar(path);
            } else {
                /* ローカル開発環境の場合 */
                loadFromDir(resource.getFile(), packageName);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("画像ファイルをキャッシュしました。[%sファイル]", IMAGE_CACHE.size()));
        }
    }

    /**
     * JARファイルにある画像ファイルをキャッシュに読み込みます。
     *
     * @param path 読み込み対象パス
     */
    private static void loadFromJar(String path) {
        ZipInputStream in = null;
        try {
            in = new ZipInputStream(URLUtil.openStream(URLUtil.create(StringUtils.split(path, '!')[0])));
            ZipEntry entry;
            while ((entry = ZipInputStreamUtil.getNextEntry(in)) != null) {
                if (entry.getName().startsWith("jp") && entry.getName().endsWith(".png")) {
                    IMAGE_CACHE.put(entry.getName(), readImage(in));
                }
                ZipInputStreamUtil.closeEntry(in);
            }
        } finally {
            InputStreamUtil.close(in);
        }
    }

    /**
     * ディレクトリにある画像ファイルをキャッシュに読み込みます。
     *
     * @param path 読み込み対象パス
     * @param packageName 読み込み対象パッケージ名
     */
    private static void loadFromDir(String path, String packageName) {
        File[] files = new File(path).listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    loadFromDir(file.getAbsolutePath(), packageName + "." + file.getName());
                } else if (file.getName().endsWith(".png")) {
                    InputStream in = null;
                    try {
                        in = new BufferedInputStream(FileInputStreamUtil.create(file));
                        IMAGE_CACHE.put(packageName.replace('.', '/') + "/" + file.getName(), readImage(in));
                    } finally {
                        InputStreamUtil.close(in);
                    }
                }
            }
        }
    }

    /**
     * 入力ストリームから画像ファイルを読み込みます。
     *
     * @param in 入力ストリーム
     * @return {@link BufferedImage}
     */
    private static BufferedImage readImage(InputStream in) {
        try {
            return ImageIO.read(in);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 画像ファイルを読み込みます。
     *
     * @param name 画像ファイル名(絶対位置指定)
     * @return {@link BufferedImage}
     */
    public static BufferedImage readImage(String name) {
        InputStream in = null;
        try {
            in = ImageUtil.class.getResourceAsStream(name);
            if (in == null) {
                throw new ResourceNotFoundRuntimeException(name);
            }
            in = new BufferedInputStream(in);
            return readImage(in);
        } finally {
            InputStreamUtil.close(in);
        }
    }

    /**
     * クラスパス上に存在する画像ファイルを読み込みます。
     *
     * @param clazz 読み込み位置の基準クラス
     * @param name 画像ファイル名
     * @return {@link BufferedImage}
     */
    public static BufferedImage readImage(Class<?> clazz, String name) {

        /* 基準クラスパッケージ配下のキャッシュから探す */
        String key = clazz.getPackage().getName().replace('.', '/') + "/" + name;
        BufferedImage image = IMAGE_CACHE.get(key);

        if (image == null) {
            /* frameworkパッケージ配下のキャッシュから探す */
            String commonKey = BZMain.class.getPackage().getName().replace('.', '/') + "/" + name;
            image = IMAGE_CACHE.get(commonKey);

            if (image == null) {
                /* キャッシュに無ければ直接読み込む */
                InputStream in = null;
                try {
                    in = clazz.getResourceAsStream(name);
                    if (in == null) {
                        in = BZMain.class.getResourceAsStream(name);
                        if (in == null) {
                            throw new ResourceNotFoundRuntimeException(key);
                        }
                    }
                    in = new BufferedInputStream(in);
                    image = readImage(in);
                } finally {
                    InputStreamUtil.close(in);
                }
            }
        }

        return image;
    }

}

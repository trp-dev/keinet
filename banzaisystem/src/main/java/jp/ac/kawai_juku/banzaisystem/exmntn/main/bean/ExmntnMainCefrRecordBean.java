package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * CEFRを保持するBeanです。
 *
 *
 * @author QQ)TAKAAKI.NAKAO
 *
 */
@Getter
@Setter
@ToString
public class ExmntnMainCefrRecordBean {

    /** 個人ID */
    private Integer individualId;

    /** 模試年度 */
    private String examYear;

    /** 模試区分 */
    private String examDiv;

    /** CEFR試験コード1 */
    private String cefrExamCd1;

    /** CEFR試験コード2 */
    private String cefrExamCd2;

    /** CEFR試験名1(正式名) */
    private String cefrExamName1;

    /** CEFR試験名2(正式名) */
    private String cefrExamName2;

    /** CEFR試験名1(略称) */
    private String cefrExamNameAbbr1;

    /** CEFR試験名2(略称) */
    private String cefrExamNameAbbr2;

    /** 認定試験レベルコード1 */
    private String cefrExamLevel1;

    /** 認定試験レベルコード2 */
    private String cefrExamLevel2;

    /** 認定試験レベルコード名1(正式名) */
    private String cefrExamLevelName1;

    /** 認定試験レベルコード名2(正式名) */
    private String cefrExamLevelName2;

    /** 認定試験レベルコード名1(略称) */
    private String cefrExamLevelNameAbbr1;

    /** 認定試験レベルコード名2(略称) */
    private String cefrExamLevelNameAbbr2;

    /** CEFR試験スコア1 */
    private Double cefrScore1;

    /** CEFR試験スコア2 */
    private Double cefrScore2;

    /** CEFRレベルコード1 */
    private String cefrLevelCd1;

    /** CEFRレベルコード2 */
    private String cefrLevelCd2;

    /** CEFRレベル名1(正式名) */
    private String cefrLevel1;

    /** CEFRレベル名2(正式名) */
    private String cefrLevel2;

    /** CEFRレベル名1(略称) */
    private String cefrLevelAbbr1;

    /** CEFRレベル名2(略称) */
    private String cefrLevelAbbr2;

    /** スコア補正フラグ1 */
    private String scoreCorrectFlg1;

    /** スコア補正フラグ2 */
    private String scoreCorrectFlg2;

    /** CEFRレベルコード補正フラグ1 */
    private String cefrLevelCdCorrectFlg1;

    /** CEFRレベルコード補正フラグ2 */
    private String cefrLevelCdCorrectFlg2;

    /** 合否補正フラグ1 */
    private String examResultCorrectFlg1;

    /** 合否補正フラグ2 */
    private String examResultCorrectFlg2;

    /** 合否1 */
    private String examResult1;

    /** 合否2 */
    private String examResult2;

    /** スコア不明1 */
    private String unknownScore1;

    /** スコア不明2 */
    private String unknownScore2;

    /** CEFR不明1 */
    private String unknownCefr1;

    /** CEFR不明2 */
    private String unknownCefr2;

    /** 表示用CEFR試験コード1 */
    private String dispCefrExamCd1;

    /** 表示用CEFR試験コード2 */
    private String dispCefrExamCd2;

    /** 表示用CEFR試験名1(正式名) */
    private String dispCefrExamName1;

    /** 表示用CEFR試験名2(正式名) */
    private String dispCefrExamName2;

    /** 表示用CEFR試験名1(略称) */
    private String dispCefrExamNameAbbr1;

    /** 表示用CEFR試験名2(略称) */
    private String dispCefrExamNameAbbr2;

    /** 表示用認定試験レベルコード1 */
    private String dispCefrExamLevel1;

    /** 表示用認定試験レベルコード2 */
    private String dispCefrExamLevel2;

    /** 表示用認定試験レベルコード名1(正式名) */
    private String dispCefrExamLevelName1;

    /** 表示用認定試験レベルコード名2(正式名) */
    private String dispCefrExamLevelName2;

    /** 表示用認定試験レベルコード名1(略称) */
    private String dispCefrExamLevelNameAbbr1;

    /** 表示用認定試験レベルコード名2(略称) */
    private String dispCefrExamLevelNameAbbr2;

    /** 表示用CEFR試験スコア1 */
    private Double dispCefrScore1;

    /** 表示用CEFR試験スコア2 */
    private Double dispCefrScore2;

    /** 表示用CEFRレベルコード1 */
    private String dispCefrLevelCd1;

    /** 表示用CEFRレベルコード2 */
    private String dispCefrLevelCd2;

    /** 表示用CEFRレベル名1(正式名) */
    private String dispCefrLevel1;

    /** 表示用CEFRレベル名2(正式名) */
    private String dispCefrLevel2;

    /** CEFRレベル名1(略称) */
    private String dispCefrLevelAbbr1;

    /** CEFRレベル名2(略称) */
    private String dispCefrLevelAbbr2;

    /** 表示用スコア補正フラグ1 */
    private String dispScoreCorrectFlg1;

    /** 表示用スコア補正フラグ2 */
    private String dispScoreCorrectFlg2;

    /** 表示用CEFRレベルコード補正フラグ1 */
    private String dispCefrLevelCdCorrectFlg1;

    /** 表示用CEFRレベルコード補正フラグ2 */
    private String dispCefrLevelCdCorrectFlg2;

    /** 表示用合否補正フラグ1 */
    private String dispExamResultCorrectFlg1;

    /** 表示用合否補正フラグ2 */
    private String dispExamResultCorrectFlg2;

    /** 表示用合否1 */
    private String dispExamResult1;

    /** 表示用合否2 */
    private String dispExamResult2;

    /** 表示用スコア不明1 */
    private String dispUnknownScore1;

    /** 表示用スコア不明2 */
    private String dispUnknownScore2;

    /** 表示用CEFR不明1 */
    private String dispUnknownCefr1;

    /** 表示用CEFR不明2 */
    private String dispUnknownCefr2;
}

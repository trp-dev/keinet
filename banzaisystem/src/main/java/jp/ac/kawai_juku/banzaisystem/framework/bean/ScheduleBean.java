package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 日程マスタ情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ScheduleBean {

    /** 日程コード */
    private String scheduleCd;

    /** 日程名 */
    private String scheduleName;

    /**
     * 日程コードを返します。
     *
     * @return 日程コード
     */
    public String getScheduleCd() {
        return scheduleCd;
    }

    /**
     * 日程コードをセットします。
     *
     * @param scheduleCd 日程コード
     */
    public void setScheduleCd(String scheduleCd) {
        this.scheduleCd = scheduleCd;
    }

    /**
     * 日程名を返します。
     *
     * @return 日程名
     */
    public String getScheduleName() {
        return scheduleName;
    }

    /**
     * 日程名をセットします。
     *
     * @param scheduleName 日程名
     */
    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

}

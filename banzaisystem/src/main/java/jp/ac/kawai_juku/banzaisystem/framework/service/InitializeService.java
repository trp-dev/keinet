package jp.ac.kawai_juku.banzaisystem.framework.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.REQUIRED_MASTER_VERSION;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.VERSION_NUMBER;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.bean.CvsScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.RankBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScheduleBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.InitializeDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;

import org.apache.commons.lang3.StringUtils;
import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * バンザイシステムの初期化処理サービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class InitializeService {

    /** パスワードサービス */
    @Resource
    private PasswordService passwordService;

    /** DAO */
    @Resource(name = "initializeDao")
    private InitializeDao dao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * パスワードとバージョン情報を初期化します。
     */
    public void initPasswordAndVersion() {

        SettingProps props = SettingProps.load();
        if (props.getFlagValue(SettingKey.INIT_PASSWORD)) {
            /* パスワード初期化フラグがONの場合(インストール直後)のみ初期化する */
            setPasswordHash(props, SettingKey.TEACHER_PASSWORD);
            setPasswordHash(props, SettingKey.ADMIN_PASSWORD);
            props.remove(SettingKey.INIT_PASSWORD);
        }

        /* 現バージョンを設定する */
        props.setValue(SettingKey.CURRENT_VERSION, BZConstants.VERSION);

        String latestVersion = props.getValue(SettingKey.LATEST_VERSION);
        if (!latestVersion.isEmpty() && Double.parseDouble(latestVersion) < VERSION_NUMBER) {
            /* 最新バージョン＜現バージョンなら、最新バージョンを現バージョンで上書きする */
            props.setValue(SettingKey.LATEST_VERSION, BZConstants.VERSION);
        }

        props.store();
    }

    /**
     * パスワードのハッシュ値をプロパティにセットします。
     *
     * @param props {@link SettingProps}
     * @param key キー
     */
    private void setPasswordHash(SettingProps props, SettingKey key) {
        props.setValue(key, passwordService.toHash(key, props.getValue(key)));
    }

    /**
     * メモリに保持しているマスタ情報を初期化します。
     */
    public void initMaster() {

        /* 大学マスタ情報をクリアする */
        systemDto.setUnivMaster(null);

        /* 判定用文字列マップをDTOに設定する */
        systemDto.setjStringMap(createJStringMap());

        /* ランクマップをDTOに設定する */
        systemDto.setRankMap(createRankMap());

        /* 換算得点マップをDTOに設定する */
        systemDto.setCvsScoreMap(createCvsScoreMap());

        /* 科目別成績（全国）マップをDTOに設定する */
        systemDto.setSubrecordAllMap(createSubrecordAllMap());

        /* 模試科目マスタマップをDTOに設定する */
        systemDto.setExamSubjectMap(createExamSubjectMap());

        /* 日程名マップをDTOに設定する */
        systemDto.setScheduleNameMap(createScheduleNameMap());

        /* 大学マスタインポート済の模試リストをDTOに設定する */
        systemDto.setExamList(dao.selectExamList(systemDto));

        /* 大学マスタバージョン不整合フラグを設定する */
        String masterVersion = systemDto.getUnivYear() + systemDto.getUnivExamDiv();
        systemDto.setMismatchingMasterVersionFlg(masterVersion.compareTo(REQUIRED_MASTER_VERSION) < 0 || VERSION_NUMBER < Double.parseDouble(systemDto.getUnivMaster().getRequiredVersion()));

        /* 情報誌用科目マップをDTOに設定する */
        systemDto.setUnivInfo(dao.loadUnivInfo(systemDto));

        /* CEFR情報をDTOに設定する */
        systemDto.setCefrList(dao.selectCefrList(systemDto));
        systemDto.getCefrList().stream().forEach(item -> item.setCefrName(item.getCefrName().replace("未達", "なし")));

        /* 認定試験情報をDTOに設定する */
        systemDto.setEngPtList(dao.selectEngPtList(systemDto));

        /* 認定試験詳細情報をDTOに設定する */
        systemDto.setEngPtDetailList(dao.selectEngPtDetailList(systemDto));

        /* CEFR換算用スコア情報をDTOに設定する */
        systemDto.setCefrConvScoreList(dao.selectCefrConvScoreList(systemDto));

        /* 国語記述式総合評価情報をDTOに設定する */
        systemDto.setJpnWritingTotalEvaluationList(dao.selectJpnWritingTotalEvaluationList(systemDto));

        /* 英語資格・検定試験_出願要件情報をDTOに設定する */
        systemDto.setEngPtappreqList(dao.selectEngPtappreqList(systemDto));

        /* 英語資格・検定試験_得点換算情報をDTOに設定する */
        systemDto.setEntPtscoreconvList(dao.selectEngPtscoreconvList(systemDto));

        /* 国語記述式_得点換算情報情報をDTOに設定する */
        systemDto.setKokugoDescscoreconvList(dao.selectKokugoDescscoreconvList(systemDto));
    }

    /**
     * 判定用文字列マップを生成します。
     *
     * @return 判定用文字列マップ
     */
    public Map<String, String> createJStringMap() {

        @SuppressWarnings("serial")
        final Map<String, String> map = new HashMap<String, String>(20000 * 4 / 3) {
            @Override
            public String toString() {
                /* TRACEログに大量データが出力されないように空文字列を返す */
                return StringUtils.EMPTY;
            }
        };

        dao.selectJStringList(new IterationCallback<String, Object>() {
            @Override
            public Object iterate(String entity, IterationContext context) {
                map.put(entity.substring(0, 10), entity.substring(10));
                return null;
            }
        });

        return map;
    }

    /**
     * ランクマップを生成します。
     *
     * @return ランクマップ
     */
    public Map<String, RankBean> createRankMap() {
        final Map<String, RankBean> map = CollectionsUtil.newHashMap();
        dao.selectRankBeanList(new IterationCallback<RankBean, Object>() {
            @Override
            public Object iterate(RankBean entity, IterationContext context) {
                map.put(entity.getRankCd(), entity);
                return null;
            }
        });
        return map;
    }

    /**
     * 換算得点マップを生成します。
     *
     * @return 換算得点マップ
     */
    private Map<String, Map<String, int[]>> createCvsScoreMap() {

        final Map<String, Map<String, int[]>> map = CollectionsUtil.newHashMap();

        dao.selectCvsScoreList(new IterationCallback<CvsScoreBean, Object>() {
            @Override
            public Object iterate(CvsScoreBean entity, IterationContext context) {

                int rawScore = entity.getRawScore().intValue();
                int cvsScore = entity.getCvsScore().intValue();

                Map<String, int[]> subMap = map.get(entity.getExamCd());
                if (subMap == null) {
                    subMap = CollectionsUtil.newHashMap();
                    map.put(entity.getExamCd(), subMap);
                }

                int[] table = subMap.get(entity.getSubCd());
                if (table == null) {
                    if (rawScore <= 50) {
                        table = new int[51];
                    } else if (rawScore <= 100) {
                        table = new int[101];
                    } else {
                        table = new int[201];
                    }
                    subMap.put(entity.getSubCd(), table);
                }

                table[rawScore] = cvsScore;

                return null;
            }
        });

        return map;
    }

    /**
     * 科目別成績（全国）マップを生成します。
     *
     * @return 科目別成績（全国）マップ
     */
    private Map<String, Map<String, SubRecordAllBean>> createSubrecordAllMap() {

        final Map<String, Map<String, SubRecordAllBean>> map = CollectionsUtil.newHashMap();

        dao.selectSubrecordAllList(new IterationCallback<SubRecordAllBean, Object>() {
            @Override
            public Object iterate(SubRecordAllBean entity, IterationContext context) {

                Map<String, SubRecordAllBean> subMap = map.get(entity.getExamCd());
                if (subMap == null) {
                    subMap = CollectionsUtil.newHashMap();
                    map.put(entity.getExamCd(), subMap);
                }

                subMap.put(entity.getSubCd(), entity);

                return null;
            }
        });

        return map;
    }

    /**
     * 模試科目マスタマップを生成します。
     *
     * @return 模試科目マスタマップ
     */
    public Map<String, Map<String, ExamSubjectBean>> createExamSubjectMap() {

        final Map<String, Map<String, ExamSubjectBean>> map = CollectionsUtil.newHashMap();

        dao.selectExamSubjectList(new IterationCallback<ExamSubjectBean, Object>() {
            @Override
            public Object iterate(ExamSubjectBean entity, IterationContext context) {

                Map<String, ExamSubjectBean> subMap = map.get(entity.getExamCd());
                if (subMap == null) {
                    subMap = CollectionsUtil.newHashMap();
                    map.put(entity.getExamCd(), subMap);
                }

                subMap.put(entity.getSubCd(), entity);

                return null;
            }
        });

        return map;
    }

    /**
     * 日程名マップを生成します。
     *
     * @return 日程名マップ
     */
    private Map<String, String> createScheduleNameMap() {

        final Map<String, String> map = CollectionsUtil.newHashMap();

        dao.selectScheduleList(new IterationCallback<ScheduleBean, Object>() {
            @Override
            public Object iterate(ScheduleBean entity, IterationContext context) {
                map.put(entity.getScheduleCd(), entity.getScheduleName());
                return null;
            }
        });

        return map;
    }

    /**
     * メモリに保持している成績データ情報を初期化します。
     */
    public void initRecordInfo() {
        systemDto.setRecordInfo(dao.selectRecordInfoBean());
    }

}

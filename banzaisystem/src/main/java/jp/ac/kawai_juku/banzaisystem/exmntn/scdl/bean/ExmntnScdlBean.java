package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean;

import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.ScheduleType;
import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 受験スケジュール画面のBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnScdlBean extends JudgementResultBean {

    /** 個人ID */
    private Integer id;

    /** 志望順位 */
    private Integer candidateRank;

    /** 大学名 */
    private String univName;

    /** 学部名 */
    private String facultyName;

    /** 学科名 */
    private String deptName;

    /** 地方フラグ */
    private String provincesFlg;

    /** 入試日の最小値 */
    private String minInpleDate;

    /** 日程マップ */
    private Map<String, ScheduleType> scheduleMap;

    /** 入試日程詳細フラグ */
    private String examScheduleDetailFlag;

    /** 注釈文フラグ */
    private String univCommentFlag;

    /** 注意マーク表示フラグ */
    private boolean isCautionNeeded;

    /**
     * コンストラクタです。
     */
    public ExmntnScdlBean() {
    }

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public ExmntnScdlBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 個人IDをセットします。
     *
     * @param id 個人ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 志望順位を返します。
     *
     * @return 志望順位
     */
    public Integer getCandidateRank() {
        return candidateRank;
    }

    /**
     * 志望順位をセットします。
     *
     * @param candidateRank 志望順位
     */
    public void setCandidateRank(Integer candidateRank) {
        this.candidateRank = candidateRank;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 地方フラグを返します。
     *
     * @return 地方フラグ
     */
    public String getProvincesFlg() {
        return provincesFlg;
    }

    /**
     * 地方フラグをセットします。
     *
     * @param provincesFlg 地方フラグ
     */
    public void setProvincesFlg(String provincesFlg) {
        this.provincesFlg = provincesFlg;
    }

    /**
     * 日程マップを返します。
     *
     * @return 日程マップ（キーはYYYYMMDD）
     */
    public Map<String, ScheduleType> getScheduleMap() {
        return scheduleMap;
    }

    /**
     * 日程マップをセットします。
     *
     * @param scheduleMap 日程マップ
     */
    public void setScheduleMap(Map<String, ScheduleType> scheduleMap) {
        this.scheduleMap = scheduleMap;
    }

    /**
     * 入試日の最小値を返します。
     *
     * @return 入試日の最小値
     */
    public String getMinInpleDate() {
        return minInpleDate;
    }

    /**
     * 入試日の最小値をセットします。
     *
     * @param minInpleDate 入試日の最小値
     */
    public void setMinInpleDate(String minInpleDate) {
        this.minInpleDate = minInpleDate;
    }

    /**
     * 入試日程詳細フラグを返します。
     *
     * @return 入試日程詳細フラグ
     */
    public String getExamScheduleDetailFlag() {
        return examScheduleDetailFlag;
    }

    /**
     * 入試日程詳細フラグをセットします。
     *
     * @param examScheduleDetailFlag 入試日程詳細フラグ
     */
    public void setExamScheduleDetailFlag(String examScheduleDetailFlag) {
        this.examScheduleDetailFlag = examScheduleDetailFlag;
    }

    /**
     * 注釈文フラグを返します。
     *
     * @return 注釈文フラグ
     */
    public String getUnivCommentFlag() {
        return univCommentFlag;
    }

    /**
     * 注釈文フラグをセットします。
     *
     * @param univCommentFlag 注釈文フラグ
     */
    public void setUnivCommentFlag(String univCommentFlag) {
        this.univCommentFlag = univCommentFlag;
    }

    /**
     * 注意マーク表示フラグを返します。
     *
     * @return 注意マーク表示フラグ
     */
    public boolean isCautionNeeded() {
        return isCautionNeeded;
    }

    /**
     * 注意マーク表示フラグをセットします。
     *
     * @param isCautionNeeded 注意マーク表示フラグ
     */
    public void setCautionNeeded(boolean isCautionNeeded) {
        this.isCautionNeeded = isCautionNeeded;
    }

}

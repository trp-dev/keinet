package jp.ac.kawai_juku.banzaisystem.framework.component;

import jp.ac.kawai_juku.banzaisystem.Code;

/**
 *
 * コード定義を利用するコンポーネントのインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface CodeComponent {

    /**
     * コンポーネントに紐付けられたコードを返します。
     *
     * @return コード
     */
    Code getCode();

    /**
     * コンポーネントが選択されているかを返します。
     *
     * @return コンポーネントが選択されているならtrue
     */
    boolean isSelected();

}

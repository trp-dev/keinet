package jp.ac.kawai_juku.banzaisystem.systop.lgin.dao;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.bean.SystopLginDataStatusBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * マスタ・プログラムチェック画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopLginDao extends BaseDao {

    /**
     * データ状況リストを取得します。
     *
     * @return データ状況リスト
     */
    public List<SystopLginDataStatusBean> selectDataStatusList() {
        BeanMap param = new BeanMap();
        param.put("system", getComponent(SystemDto.class));
        return jdbcManager.selectBySqlFile(SystopLginDataStatusBean.class, getSqlPath(), param).getResultList();
    }

}

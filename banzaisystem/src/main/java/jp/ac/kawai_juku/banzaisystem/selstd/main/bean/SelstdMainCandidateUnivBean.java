package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;

/**
 *
 * 対象生徒選択画面の志望大学データを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainCandidateUnivBean extends JudgementResultBean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** カナ氏名 */
    private String nameKana;

    /** 得点 */
    private Integer score;

    /** 偏差値 */
    private Double deviation;

    /** 科目名 */
    private String subName;

    /** 配点 */
    private Integer subAllotPnt;

    /** 志望順位 */
    private Integer candidateRank;

    /** 大学名 */
    private String univName;

    /** 学部名  */
    private String facultyName;

    /** 学科名  */
    private String deptName;

    /** 大学カナ名 */
    private String univNameKana;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * カナ氏名を返します。
     *
     * @return カナ氏名
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * カナ氏名をセットします。
     *
     * @param nameKana カナ氏名
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 得点を返します。
     *
     * @return 得点
     */
    public Integer getScore() {
        return score;
    }

    /**
     * 得点をセットします。
     *
     * @param score 得点
     */
    public void setScore(Integer score) {
        this.score = score;
    }

    /**
     * 偏差値を返します。
     *
     * @return 偏差値
     */
    public Double getDeviation() {
        return deviation;
    }

    /**
     * 偏差値をセットします。
     *
     * @param deviation 偏差値
     */
    public void setDeviation(Double deviation) {
        this.deviation = deviation;
    }

    /**
     * 科目名を返します。
     *
     * @return 科目名
     */
    public String getSubName() {
        return subName;
    }

    /**
     * 科目名をセットします。
     *
     * @param subName 科目名
     */
    public void setSubName(String subName) {
        this.subName = subName;
    }

    /**
     * 配点を返します。
     *
     * @return 配点
     */
    public Integer getSubAllotPnt() {
        return subAllotPnt;
    }

    /**
     * 配点をセットします。
     *
     * @param subAllotPnt 配点
     */
    public void setSubAllotPnt(Integer subAllotPnt) {
        this.subAllotPnt = subAllotPnt;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 志望順位を返します。
     *
     * @return 志望順位
     */
    public Integer getCandidateRank() {
        return candidateRank;
    }

    /**
     * 志望順位をセットします。
     *
     * @param candidateRank 志望順位
     */
    public void setCandidateRank(Integer candidateRank) {
        this.candidateRank = candidateRank;
    }

    /**
     * 大学カナ名を返します。
     *
     * @return 大学カナ名
     */
    public String getUnivNameKana() {
        return univNameKana;
    }

    /**
     * 大学カナ名をセットします。
     *
     * @param univNameKana 大学カナ名
     */
    public void setUnivNameKana(String univNameKana) {
        this.univNameKana = univNameKana;
    }

}

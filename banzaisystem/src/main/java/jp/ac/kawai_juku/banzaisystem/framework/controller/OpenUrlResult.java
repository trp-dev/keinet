package jp.ac.kawai_juku.banzaisystem.framework.controller;

import java.io.IOException;

import org.seasar.framework.exception.IORuntimeException;

/**
 *
 * URLをデフォルトブラウザで開くアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OpenUrlResult implements ExecuteResult {

    /** URL */
    private final String url;

    /**
     * コンストラクタです。
     *
     * @param url 起動するメソッド名
     */
    OpenUrlResult(String url) {
        this.url = url;
    }

    @Override
    public void process(AbstractController controller) {

        String[] cmdarray = new String[4];
        cmdarray[0] = "cmd.exe";
        cmdarray[1] = "/c";
        cmdarray[2] = "start";
        cmdarray[3] = url;

        try {
            Runtime.getRuntime().exec(cmdarray);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}

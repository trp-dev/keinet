package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * {@link jp.ac.kawai_juku.banzaisystem.framework.component.TextField}の
 * 入力値を正の数値型に制限したい場合に設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface NumberField {

    /**
     * 入力値の最大長を設定します。
     *
     * @return 最大長
     */
    int maxLength();

    /**
     * スケール（小数点以下の精度）を設定します。
     *
     * @return スケール
     */
    int scale() default 0;

}

package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.swing.SwingConstants;

/**
 *
 * VerticalAlignmentを指定するためのアノテーションです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface VerticalAlignment {

    /**
     * VerticalAlignmentを設定します。
     *
     * @return 縦位置
     */
    int value() default SwingConstants.CENTER;

}

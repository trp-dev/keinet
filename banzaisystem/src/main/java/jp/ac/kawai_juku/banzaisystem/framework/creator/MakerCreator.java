package jp.ac.kawai_juku.banzaisystem.framework.creator;

import org.seasar.framework.container.creator.ComponentCreatorImpl;
import org.seasar.framework.container.deployer.InstanceDefFactory;
import org.seasar.framework.convention.NamingConvention;

/**
 *
 * 帳票メーカークラス用の {@link org.seasar.framework.container.ComponentCreator} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MakerCreator extends ComponentCreatorImpl {

    /**
     * コンストラクタです。
     *
     * @param namingConvention 命名規約
     */
    public MakerCreator(NamingConvention namingConvention) {
        super(namingConvention);
        setNameSuffix("Maker");
        setInstanceDef(InstanceDefFactory.PROTOTYPE);
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

/**
 *
 * 大学検索画面の検索条件入力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsUnivSearchInBean {

    /** 成績データ */
    private ScoreBean scoreBean;

    /** 区分リスト */
    private List<String> divList;

    /** 女子大 */
    private Code femail;

    /** 夜間 */
    private Code night;

    /** 大学名 */
    private String univName;

    /** 県コードリスト */
    private List<String> prefCdList;

    /** 地区コードリスト */
    private List<String> distCdList;

    /** 中系統コードリスト */
    private List<String> mStemmaList;

    /** 小系統コードリスト */
    private List<String> sStemmaList;

    /** 分野コードリスト */
    private List<String> regionList;

    /** 分野コードリスト(帳票用) */
    private List<Code> printRegionList;

    /** 評価範囲：評価区分リスト */
    private List<String> raingDivList;

    /** 評価範囲：評価リスト */
    private List<String> ratingList;

    /** 日程リスト(国公立) */
    private List<String> scheduleList;

    /** 日程リスト(私立) */
    private List<String> scheduleList2;

    /** 入試日程：出願締切日（開始） */
    private String applyStart;

    /** 入試日程：出願締切日（終了） */
    private String applyEnd;

    /** 入試日程：入試日（開始） */
    private String entStart;

    /** 入試日程：入試日（終了） */
    private String entEnd;

    /** センター科目リスト */
    private List<CenterSubject> centerList;

    /** 個別試験：課さない */
    private Boolean notImpose;

    /** 二次科目リスト */
    private List<SecondSubject> secondList;

    /** 配点比リスト */
    private List<String> rateList;

    /** 資格コードリスト */
    private List<String> certCdList;

    /** 資格カテゴリリスト(帳票用) */
    private List<Code> certCategoryList;

    /**
     * 区分リストを返します。
     *
     * @return 区分リスト
     */
    public List<String> getDivList() {
        return divList;
    }

    /**
     * 女子大を返します。
     *
     * @return 女子大
     */
    public Code getFemail() {
        return femail;
    }

    /**
     * 女子大をセットします。
     *
     * @param femail 女子大
     */
    public void setFemail(Code femail) {
        this.femail = femail;
    }

    /**
     * 夜間を返します。
     *
     * @return 夜間
     */
    public Code getNight() {
        return night;
    }

    /**
     * 夜間をセットします。
     *
     * @param night 夜間
     */
    public void setNight(Code night) {
        this.night = night;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 県コードリストを返します。
     *
     * @return 県コードリスト
     */
    public List<String> getPrefCdList() {
        return prefCdList;
    }

    /**
     * 県コードリストをセットします。
     *
     * @param prefCdList 県コードリスト
     */
    public void setPrefCdList(List<String> prefCdList) {
        this.prefCdList = prefCdList;
    }

    /**
     * 日程リストを返します。
     *
     * @return 日程リスト
     */
    public List<String> getScheduleList() {
        return scheduleList;
    }

    /**
     * 日程リストをセットします。
     *
     * @param scheduleList 日程リスト
     */
    public void setScheduleList(List<String> scheduleList) {
        this.scheduleList = scheduleList;
    }

    /**
     * 日程リストを返します。
     *
     * @return 日程リスト
     */
    public List<String> getScheduleList2() {
        return scheduleList2;
    }

    /**
     * 日程リストをセットします。
     *
     * @param scheduleList2 日程リスト
     */
    public void setScheduleList2(List<String> scheduleList2) {
        this.scheduleList2 = scheduleList2;
    }

    /**
     * 出願締切日（開始）を返します。
     *
     * @return 出願締切日（開始）
     */
    public String getApplyStart() {
        return applyStart;
    }

    /**
     * 出願締切日（開始）をセットします。
     *
     * @param applyStart 出願締切日（開始）
     */
    public void setApplyStart(String applyStart) {
        this.applyStart = applyStart;
    }

    /**
     * 出願締切日（終了）を返します。
     *
     * @return 出願締切日（終了）
     */
    public String getApplyEnd() {
        return applyEnd;
    }

    /**
     * 出願締切日（終了）をセットします。
     *
     * @param applyEnd 出願締切日（終了）
     */
    public void setApplyEnd(String applyEnd) {
        this.applyEnd = applyEnd;
    }

    /**
     * 入試日（開始）を返します。
     *
     * @return 入試日（開始）
     */
    public String getEntStart() {
        return entStart;
    }

    /**
     * 入試日（開始）をセットします。
     *
     * @param entStart 入試日（開始）
     */
    public void setEntStart(String entStart) {
        this.entStart = entStart;
    }

    /**
     * 入試日（終了）を返します。
     *
     * @return 入試日（終了）
     */
    public String getEntEnd() {
        return entEnd;
    }

    /**
     * 入試日（終了）をセットします。
     *
     * @param entEnd 入試日（終了）
     */
    public void setEntEnd(String entEnd) {
        this.entEnd = entEnd;
    }

    /**
     * センター科目リストを返します。
     *
     * @return センター科目リスト
     */
    public List<CenterSubject> getCenterList() {
        return centerList;
    }

    /**
     * センター科目リストをセットします。
     *
     * @param centerList センター科目リスト
     */
    public void setCenterList(List<CenterSubject> centerList) {
        this.centerList = centerList;
    }

    /**
     * 二次科目リストを返します。
     *
     * @return 二次科目リスト
     */
    public List<SecondSubject> getSecondList() {
        return secondList;
    }

    /**
     * 二次科目リストをセットします。
     *
     * @param secondList 二次科目リスト
     */
    public void setSecondList(List<SecondSubject> secondList) {
        this.secondList = secondList;
    }

    /**
     * 配点比リストを返します。
     *
     * @return 配点比リスト
     */
    public List<String> getRateList() {
        return rateList;
    }

    /**
     * 配点比リストをセットします。
     *
     * @param rateList 配点比リスト
     */
    public void setRateList(List<String> rateList) {
        this.rateList = rateList;
    }

    /**
     * 評価範囲：評価リストを返します。
     *
     * @return 評価範囲：評価リスト
     */
    public List<String> getRatingList() {
        return ratingList;
    }

    /**
     * 評価範囲：評価リストをセットします。
     *
     * @param ratingList 評価範囲：評価リスト
     */
    public void setRatingList(List<String> ratingList) {
        this.ratingList = ratingList;
    }

    /**
     * 区分リストをセットします。
     *
     * @param divList 区分リスト
     */
    public void setDivList(List<String> divList) {
        this.divList = divList;
    }

    /**
     * 個別試験：課さないをセットします。
     *
     * @param notImpose 個別試験：課さない
     */
    public void setNotImpose(Boolean notImpose) {
        this.notImpose = notImpose;
    }

    /**
     * 中系統コードリストを返します。
     *
     * @return 中系統コードリスト
     */
    public List<String> getmStemmaList() {
        return mStemmaList;
    }

    /**
     * 中系統コードリストをセットします。
     *
     * @param mStemmaList 中系統コードリスト
     */
    public void setmStemmaList(List<String> mStemmaList) {
        this.mStemmaList = mStemmaList;
    }

    /**
     * 小系統コードリストを返します。
     *
     * @return 小系統コードリスト
     */
    public List<String> getsStemmaList() {
        return sStemmaList;
    }

    /**
     * 小系統コードリストをセットします。
     *
     * @param sStemmaList 小系統コードリスト
     */
    public void setsStemmaList(List<String> sStemmaList) {
        this.sStemmaList = sStemmaList;
    }

    /**
     * 分野コードリストを返します。
     *
     * @return 分野コードリスト
     */
    public List<String> getRegionList() {
        return regionList;
    }

    /**
     * 分野コードリストをセットします。
     *
     * @param regionList 分野コードリスト
     */
    public void setRegionList(List<String> regionList) {
        this.regionList = regionList;
    }

    /**
     * 地区コードリストを返します。
     *
     * @return 地区コードリスト
     */
    public List<String> getDistCdList() {
        return distCdList;
    }

    /**
     * 地区コードリストをセットします。
     *
     * @param distCdList 地区コードリスト
     */
    public void setDistCdList(List<String> distCdList) {
        this.distCdList = distCdList;
    }

    /**
     * 個別試験：課さないを返します。
     *
     * @return 個別試験：課さない
     */
    public Boolean getNotImpose() {
        return notImpose;
    }

    /**
     * 成績データを返します。
     *
     * @return 成績データ
     */
    public ScoreBean getScoreBean() {
        return scoreBean;
    }

    /**
     * 成績データをセットします。
     *
     * @param scoreBean 成績データ
     */
    public void setScoreBean(ScoreBean scoreBean) {
        this.scoreBean = scoreBean;
    }

    /**
     * 分野コードリスト(帳票用)を返します。
     *
     * @return 分野コードリスト(帳票用)
     */
    public List<Code> getPrintRegionList() {
        return printRegionList;
    }

    /**
     * 分野コードリスト(帳票用)をセットします。
     *
     * @param printRegionList 分野コードリスト(帳票用)
     */
    public void setPrintRegionList(List<Code> printRegionList) {
        this.printRegionList = printRegionList;
    }

    /**
     * 資格コードリストを返します。
     *
     * @return 資格コードリスト
     */
    public List<String> getCertCdList() {
        return certCdList;
    }

    /**
     * 資格コードリストをセットします。
     *
     * @param certCdList 資格コードリスト
     */
    public void setCertCdList(List<String> certCdList) {
        this.certCdList = certCdList;
    }

    /**
     * 資格カテゴリリスト(帳票用)を返します。
     *
     * @return 資格カテゴリリスト(帳票用)
     */
    public List<Code> getCertCategoryList() {
        return certCategoryList;
    }

    /**
     * 資格カテゴリリスト(帳票用)をセットします。
     *
     * @param certCategoryList 資格カテゴリリスト(帳票用)
     */
    public void setCertCategoryList(List<Code> certCategoryList) {
        this.certCategoryList = certCategoryList;
    }

    /**
     * 評価範囲：評価区分リストを返します。
     *
     * @return 評価範囲：評価区分リスト
     */
    public List<String> getRaingDivList() {
        return raingDivList;
    }

    /**
     * 評価範囲：評価区分リストをセットします。
     *
     * @param raingDivList 評価範囲：評価区分リスト
     */
    public void setRaingDivList(List<String> raingDivList) {
        this.raingDivList = raingDivList;
    }

}

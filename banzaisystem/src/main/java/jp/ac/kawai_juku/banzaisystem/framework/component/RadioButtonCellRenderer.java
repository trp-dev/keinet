package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * ラジオボタンのセルレンダラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class RadioButtonCellRenderer extends JPanel implements TableCellRenderer {

    /** ラジオボタン */
    private final JRadioButton radio = new JRadioButton();

    /**
     * コンストラクタです。
     */
    public RadioButtonCellRenderer() {
        super(new GridBagLayout());
        /* セルエディタと左右のコンポーネント位置が合うように、セルの右側のボーダとして1px確保しておく */
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 1));
        radio.setOpaque(false);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        removeAll();
        if (value != null) {
            radio.setSelected(((Boolean) value).booleanValue());
            add(radio, new GridBagConstraints());
        }
        return this;
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dstd.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.component.SelstdDstdStudentTable;

/**
 *
 * 生徒一覧ダイアログのViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Dialog
public class SelstdDstdView extends AbstractView {

    /** 生徒テーブル */
    @Location(x = 24, y = 55)
    @Size(width = 215, height = 296)
    public SelstdDstdStudentTable studentTable;

    /** OKボタン */
    @Location(x = 48, y = 377)
    @Disabled
    public ImageButton okButton;

    /** キャンセルボタン */
    @Location(x = 135, y = 377)
    public ImageButton cancelButton;

}

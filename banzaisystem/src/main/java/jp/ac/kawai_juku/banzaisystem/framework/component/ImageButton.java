package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerActionListener;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * 画像を背景に持つボタンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ImageButton extends JButton implements BZComponent {

    /** マウスオーバーでカーソルを変化させるマウスリスナ */
    public static final MouseListener CURSOR_MOUSE_LISTENER = new MouseAdapter() {

        @Override
        public void mouseEntered(MouseEvent e) {
            Cursor c = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
            Component p = (Component) e.getSource();
            p.setCursor(c);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Cursor c = Cursor.getDefaultCursor();
            Component p = (Component) e.getSource();
            p.setCursor(c);
        }

    };

    /** フィールド名の接尾辞 */
    private static final String SUFFIX = "Button";

    /**
     * コンストラクタです。
     */
    public ImageButton() {
        setBorder(null);
        setFocusable(false);
        setFocusPainted(false);
        setOpaque(false);
        setContentAreaFilled(false);
        addMouseListener(CURSOR_MOUSE_LISTENER);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        if (!field.getName().endsWith(SUFFIX)) {
            throw new RuntimeException(String.format("%sのフィールド名は[***%s]とする必要があります。[%s]", getClass().getSimpleName(),
                    SUFFIX, field.getName()));
        }
        initialize(view, field.getName());
    }

    /**
     * ボタンを初期化します。
     *
     * @param view ボタンを配置する画面のView
     * @param name ボタン名
     */
    public void initialize(AbstractView view, String name) {

        /* 背景画像を読み込む */
        String imageName = name.substring(0, name.length() - SUFFIX.length());
        BufferedImage imgOn = ImageUtil.readImage(view.getClass(), "button/" + imageName + "_on.png");
        BufferedImage imgOff = ImageUtil.readImage(view.getClass(), "button/" + imageName + "_off.png");

        setSize(imgOn.getWidth(), imgOn.getHeight());
        setIcon(new ImageIcon(imgOff));
        setPressedIcon(new ImageIcon(imgOn));

        ActionListener[] listeners = getActionListeners();

        /* ボタンクリックイベントのリスナを登録する */
        if (ArrayUtils.isEmpty(listeners)) {
            addActionListener(new ControllerActionListener(view, name));
        }

        /* ボタンクリック直後に再描画して、背景が元に戻るようにする */
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
    }

    /**
     * ボタンクリックイベントを発生させます。
     */
    public final void click() {
        fireActionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
    }

}

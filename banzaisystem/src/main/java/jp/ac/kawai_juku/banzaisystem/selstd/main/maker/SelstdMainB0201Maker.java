package jp.ac.kawai_juku.banzaisystem.selstd.main.maker;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CsvId;
import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseCsvMaker;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0201Bean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCsvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.dao.SelstdMainCsvDao;

import org.apache.commons.lang3.StringUtils;
import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * CSV帳票メーカー(志望大学)です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainB0201Maker extends BaseCsvMaker<SelstdMainCsvInBean> {

    /** CSV項目ヘッダ */
    private static final List<String> HEADER;
    static {
        List<String> list = CollectionsUtil.newArrayList(12);
        list.add("学年");
        list.add("クラス");
        list.add("クラス番号");
        list.add("カナ氏名");
        list.add("模試コード");
        list.add("模試名");
        list.add("志望順位");
        list.add("大学コード（10桁）");
        list.add("大学コード（5桁）");
        list.add("大学名");
        list.add("学部名");
        list.add("学科名");
        HEADER = Collections.unmodifiableList(list);
    }

    /** DAO */
    @Resource
    private SelstdMainCsvDao selstdMainCsvDao;

    /**
     * コンストラクタです。
     */
    public SelstdMainB0201Maker() {
        super(true);
    }

    @Override
    public PrintId getPrintId() {
        return CsvId.B02_01;
    }

    @Override
    protected void outputData(SelstdMainCsvInBean inBean) {
        if (inBean.getExam() != null && !inBean.getIdList().isEmpty()) {
            selstdMainCsvDao.selectB0201List(inBean.getExam(), inBean.getIdList(), new Callback());
        }
    }

    /** レコード1件を処理するコールバッククラス */
    private class Callback implements IterationCallback<SelstdMainB0201Bean, Object> {

        /** 前ループで処理した個人ID */
        private Integer prevIndividualId;

        /** 志望順位 */
        private int rank;

        @Override
        public Object iterate(SelstdMainB0201Bean bean, IterationContext context) {

            /* 1行目はヘッダを出力する */
            if (prevIndividualId == null) {
                addColumns(HEADER);
                addRow();
            }

            /* 前回と個人IDが変わっていたら、志望順位をリセットする */
            if (!bean.getIndividualId().equals(prevIndividualId)) {
                rank = 1;
            }

            /* 学年 */
            addColumn(bean.getGrade());

            /* クラス */
            addColumn(bean.getCls());

            /* クラス番号 */
            addColumn(bean.getClassNo());

            /* カナ氏名 */
            addColumn(bean.getNameKana());

            /* 模試コード */
            addColumn(bean.getExamCd());

            /* 模試名 */
            addColumn(bean.getExamName());

            /* 志望順位 */
            addColumn(Integer.toString(rank++));

            /* 大学コード（10桁） */
            addColumn(bean.getUnivKey());

            /* 大学コード（5桁） */
            addColumn(bean.getChoiceCd5());

            /* 大学名 */
            addColumn(StringUtils.rightPad(StringUtils.defaultString(bean.getUnivName()), 7, '　'));

            /* 学部名 */
            addColumn(StringUtils.rightPad(StringUtils.defaultString(bean.getFacultyName()), 5, '　'));

            /* 学科名 */
            addColumn(StringUtils.rightPad(StringUtils.defaultString(bean.getDeptName()), 6, '　'));

            /* 1行出力する */
            addRow();

            /* 処理した個人IDを保持しておく */
            prevIndividualId = bean.getIndividualId();

            return null;
        }

    }

}

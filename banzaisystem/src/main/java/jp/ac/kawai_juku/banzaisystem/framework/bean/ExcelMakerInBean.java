package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * Excel帳票メーカーの入力Beanインターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface ExcelMakerInBean extends FormMakerInBean {
}

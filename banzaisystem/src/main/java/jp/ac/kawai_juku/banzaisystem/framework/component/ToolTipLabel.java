package jp.ac.kawai_juku.banzaisystem.framework.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.ToolTipConfig;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * ツールチップラベルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ToolTipLabel extends JButton implements BZComponent {

    /** ツールチップパネルの背景色 */
    private static final Color PANEL_BG_COLOR = new Color(238, 249, 255);

    /** ツールチップパネルのボーダー色 */
    private static final Color PANEL_BORDER_COLOR = new Color(1, 136, 244);

    /** ツールチップパネルの幅 */
    private static final int PANEL_WIDTH = 236;

    /** ツールチップパネル */
    private JPanel panel;

    /**
     * コンストラクタです。
     */
    public ToolTipLabel() {
        setBorder(null);
        setFocusable(false);
        setOpaque(false);
        setContentAreaFilled(false);
    }

    @Override
    public void initialize(final Field field, final AbstractView view) {

        ToolTipConfig config = BZFieldUtil.getAnnotation(field, ToolTipConfig.class);
        if (config == null) {
            throw new RuntimeException("ツールチップラベルにToolTipConfigアノテーションが設定されていません。");
        }

        /* 背景画像を読み込む */
        BufferedImage img = ImageUtil.readImage(view.getClass(), "label/toolTip.png");
        setSize(img.getWidth(), img.getHeight());
        setIcon(new ImageIcon(img));

        /* ヘルプラベルを初期化する */
        ImageLabel helpLabel = new ImageLabel();
        helpLabel.initialize(view, "helpLabel");

        /* ツールチップパネルを初期化する */
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT, 3, 3);
        LineBorder border = new LineBorder(PANEL_BORDER_COLOR, 1);
        panel = new JPanel(layout);
        panel.setBorder(border);
        panel.setBackground(PANEL_BG_COLOR);
        panel.add(helpLabel);

        int height = helpLabel.getHeight() + layout.getVgap() * 2 + border.getThickness() * 2;
        BufferedImage dotImg = ImageUtil.readImage(view.getClass(), "label/helpDot.png");
        for (String key : config.value()) {
            JLabel label = new JLabel(getMessage(key));
            label.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
            label.setFont(BZFont.DEFAULT.getFont());
            label.setSize(PANEL_WIDTH - dotImg.getWidth() - layout.getHgap() * 3 - border.getThickness() * 2,
                    label.getPreferredSize().height);
            label.setPreferredSize(label.getSize());
            JLabel dotLabel = new JLabel(new ImageIcon(dotImg));
            dotLabel.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
            dotLabel.setVerticalAlignment(JLabel.TOP);
            dotLabel.setSize(dotImg.getWidth(), label.getHeight());
            dotLabel.setPreferredSize(dotLabel.getSize());
            panel.add(dotLabel);
            panel.add(label);
            height += label.getHeight() + layout.getVgap();
        }

        panel.setSize(new Dimension(PANEL_WIDTH, height));
        panel.setVisible(false);
        view.addLayerComponent(panel);

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                Point p = ToolTipLabel.this.getLocation();
                panel.setLocation(p.x + e.getX() + 12, p.y + e.getY() + 12);
                panel.setVisible(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                panel.setVisible(false);
            }
        });
    }
}

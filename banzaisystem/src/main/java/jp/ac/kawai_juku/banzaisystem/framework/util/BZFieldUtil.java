package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 *
 * {@link Field} に関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZFieldUtil {

    /**
     * コンストラクタです。
     */
    private BZFieldUtil() {
    }

    /**
     * フィールドからアノテーションを取り出します。
     *
     * @param <T> アノテーション型
     * @param field フィールド
     * @param annotationClass アノテーション
     * @return アノテーション
     */
    public static <T extends Annotation> T getAnnotation(Field field, Class<T> annotationClass) {

        T a = field.getAnnotation(annotationClass);
        if (a == null) {
            throw new RuntimeException(String.format("フィールド[%s]にアノテーション[%s]が設定されていません。", field.getName(),
                    annotationClass.getSimpleName()));
        }

        return a;
    }

}

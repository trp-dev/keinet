package jp.ac.kawai_juku.banzaisystem.framework.csv;

/**
 *
 * CSVの列位置を保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CIndex {

    /** 列位置 */
    private final int index;

    /**
     * コンストラクタです。
     *
     * @param index インデックス番号。先頭列は0です。
     */
    public CIndex(int index) {
        this.index = index;
    }

    /**
     * 列位置を返します。
     *
     * @return 列位置
     */
    public int getIndex() {
        return index;
    }

    /**
     * 指定した列数を加算した列位置を返します。
     *
     * @param add 加算する数
     * @return 列位置
     */
    public CIndex add(int add) {
        return new CIndex(index + add);
    }

    /**
     * 右隣の列位置を返します。
     *
     * @return 列位置
     */
    public CIndex right() {
        return add(1);
    }

    /**
     * 左隣の列位置を返します。
     *
     * @return 列位置
     */
    public CIndex left() {
        return add(-1);
    }

}

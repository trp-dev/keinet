package jp.ac.kawai_juku.banzaisystem.exmntn.subs.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.util.ExmntnRsltUtil;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsSearchResultBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.NullGreaterCellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.RankCellData;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.RatingCellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索時の判定タスクです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
class JudgementTask implements Callable<Map<String, CellData>> {

    /** 検索条件Bean */
    private final ExmntnSubsUnivSearchInBean searchBean;

    /** センタ科目検索ロジック */
    private final CenterSubjectSearch centerSearch;

    /** 二次科目検索ロジック */
    private final SecondSubjectSearch secondSearch;

    /** 検索結果Bean */
    private final ExmntnSubsSearchResultBean resultBean;

    /** システム情報DTO */
    private final SystemDto systemDto;

    /** 英語認定試験フラグ */
    /* private boolean noExclude; */

    /** 対象模試 */
    /* private ExmntnMainScoreHelper scoreHelper; */

    /**
     * コンストラクタです。
     *
     * @param searchBean 検索条件Bean
     * @param centerSearch センタ科目検索ロジック
     * @param secondSearch 二次科目検索ロジック
     * @param resultBean 検索結果Bean
     * @param systemDto システム情報DTO
     */
    JudgementTask(ExmntnSubsUnivSearchInBean searchBean, CenterSubjectSearch centerSearch, SecondSubjectSearch secondSearch, ExmntnSubsSearchResultBean resultBean, SystemDto systemDto) {
        this.searchBean = searchBean;
        this.centerSearch = centerSearch;
        this.secondSearch = secondSearch;
        this.resultBean = resultBean;
        this.systemDto = systemDto;
        /*
        this.noExclude = noExclude;
        this.scoreHelper = scoreHelper;
        */
    }

    @Override
    public Map<String, CellData> call() throws Exception {
        /* ExmntnSubsSearchResultBean resultBean2 = new ExmntnSubsSearchResultBean(); */
        /* 大学インスタンスを生成する */
        /* UnivData univ = BZJudgementExecutor.getInstance().createUniv(resultBean, false); */
        UnivData univ = BZJudgementExecutor.getInstance().createUniv(resultBean);
        /* 使用科目による絞込みを行う */
        if (!checkSubject(searchBean, univ)) {
            return null;
        }

        /* 判定を実行する */
        BZJudgementExecutor.getInstance().judge(searchBean.getScoreBean(), univ, resultBean);
        /* if (!noExclude) { */
        /* 必要情報をresultBeanからコピー */
        /*
        resultBean2.setDeptCd(resultBean.getDeptCd());
        resultBean2.setDeptName(resultBean.getDeptName());
        resultBean2.setEntExamApplideadLine(resultBean.getEntExamApplideadLine());
        resultBean2.setEntExamInpleDate(resultBean.getEntExamInpleDate());
        resultBean2.setScheduleCd(resultBean.getScheduleCd());
        resultBean2.setFacultyCd(resultBean.getFacultyCd());
        resultBean2.setFacultyName(resultBean.getFacultyName());
        resultBean2.setPrefCd(resultBean.getPrefCd());
        resultBean2.setPrefName(resultBean.getPrefName());
        resultBean2.setUnivCd(resultBean.getUnivCd());
        resultBean2.setUnivCommentFlag(resultBean.getUnivCommentFlag());
        resultBean2.setUnivName(resultBean.getUnivName());
        resultBean2.setUnivNameKana(resultBean.getUnivNameKana());
        UnivData univEng = BZJudgementExecutor.getInstance().createUniv(resultBean2, true);
        BZJudgementExecutor.getInstance().judge(searchBean.getScoreBean(), univEng, resultBean2);
        resultBean.setValuateInclude(resultBean2.getValuateInclude());
        } else {
        resultBean.setValuateInclude("");
        }
        */
        /* 評価範囲による絞込みを行う */
        /*
        if (scoreHelper.getMarkExam() == null || !ExamUtil.isCenter(scoreHelper.getMarkExam())) {
            return checkRating(searchBean, resultBean) ? toData(searchBean, resultBean, resultBean2) : null;
        } else {
            if (!StringUtil.isEmptyTrim(resultBean2.getAppReqPaternCd())) {
                return checkRating(searchBean, resultBean2) ? toData(searchBean, resultBean, resultBean2) : null;
            } else {
                return checkRating(searchBean, resultBean) ? toData(searchBean, resultBean, resultBean2) : null;
            }
        }
        */
        return checkRating(searchBean, resultBean) ? toData(searchBean, resultBean) : null;
    }

    /**
     * 使用科目による検索条件を満たしているかをチェックします。
     *
     * @param searchBean 検索条件Bean
     * @param univ 大学データ
     * @return チェック結果
     */
    private boolean checkSubject(ExmntnSubsUnivSearchInBean searchBean, UnivData univ) {
        return centerSearch.search(univ) && secondSearch.search(univ) && checkAllotPointRate(searchBean, univ);
    }

    /**
     * 配点比による検索条件を満たしているかをチェックします。
     *
     * @param searchBean 検索条件Bean
     * @param univ 大学データ
     * @return チェック結果
     */
    private boolean checkAllotPointRate(ExmntnSubsUnivSearchInBean searchBean, UnivData univ) {

        List<String> list = searchBean.getRateList();
        if (list == null) {
            /* 検索条件未設定なら評価しない */
            return true;
        }

        for (UnivDetail detail : extractDetailList(univ)) {
            double centerRate = calcCenterRate(detail);
            if (centerRate < 0) {
                continue;
            } else if (centerRate >= 65) {
                if (list.indexOf(Code.UNIV_SEARCH_RATE_CENTER.getValue()) > -1) {
                    return true;
                }
            } else if (centerRate <= 35) {
                if (list.indexOf(Code.UNIV_SEARCH_RATE_INDIVIDUAL.getValue()) > -1) {
                    return true;
                }
            } else if (centerRate > 35 && centerRate < 65) {
                if (list.indexOf(Code.UNIV_SEARCH_RATE_HALF.getValue()) > -1) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * センタ配点比を計算します。
     *
     * @param detail 大学詳細データ
     * @return センタ配点比
     */
    private double calcCenterRate(UnivDetail detail) {
        int cAllot = detail.getCenter().getAllotPntRateAll();
        int sAllot = detail.getSecond().getAllotPntRateAll();
        if (cAllot == Detail.ALLOT_POINT_RATE_ALL_NA && sAllot == Detail.ALLOT_POINT_RATE_ALL_NA) {
            return -1;
        } else if (cAllot == Detail.ALLOT_POINT_RATE_ALL_NA || cAllot == 0) {
            return 0;
        } else if (sAllot == Detail.ALLOT_POINT_RATE_ALL_NA || sAllot == 0) {
            return 100;
        } else {
            return (double) cAllot / (cAllot + sAllot) * 100;
        }
    }

    /**
     * 大学データから枝番別の大学詳細データリストを取り出します。
     *
     * @param univ 大学データ
     * @return 大学詳細データ
     */
    private List<UnivDetail> extractDetailList(UnivData univ) {
        List<UnivDetail> list = CollectionsUtil.newArrayList();
        for (Object obj : univ.getDetailHash().values()) {
            list.add((UnivDetail) obj);
        }
        return list;
    }

    /**
     * 評価範囲による検索条件を満たしているかをチェックします。
     *
     * @param searchBean 検索条件Bean
     * @param resultBean 判定結果Bean
     * @return チェック結果
     */
    private boolean checkRating(ExmntnSubsUnivSearchInBean searchBean, JudgementResultBean resultBean) {

        if (searchBean.getRaingDivList() == null || searchBean.getRatingList() == null) {
            return true;
        }

        for (String div : searchBean.getRaingDivList()) {
            String judgement;
            if (Code.UNIV_SEARCH_RANGE_CENTER.getValue().equals(div)) {
                /* センター */
                judgement = resultBean.getCLetterJudgement();
            } else if (Code.UNIV_SEARCH_RANGE_SECOND.getValue().equals(div)) {
                /* 二次 */
                judgement = resultBean.getSecondRating().trim();
            } else {
                /* 総合（ドッキング）*/
                judgement = resultBean.getTotalRating().trim();
            }
            if (searchBean.getRatingList().indexOf(judgement) > -1) {
                return true;
            }
        }

        return false;
    }

    /**
     * 判定結果を検索結果テーブルデータに変換します。
     *
     * @param searchBean 検索条件Bean
     * @param resultBean 判定結果(英語認定含まない)Bean
     * @return 検索結果テーブルデータ
     */
    private Map<String, CellData> toData(ExmntnSubsUnivSearchInBean searchBean, ExmntnSubsSearchResultBean resultBean) {

        Map<String, CellData> map = CollectionsUtil.newHashMap();

        /* 大学キー */
        map.put("univKey", new CellData(new UnivKeyBean(resultBean)));

        /* 区分 */
        map.put("division", ExmntnRsltUtil.toDivision(resultBean.getUnivCd()));

        /* 所在地 */
        map.put("seatOfUniv", new NullGreaterCellData(resultBean.getPrefName(), resultBean.getPrefCd()));

        /* 大学名 */
        map.put("university", new CellData(resultBean.getUnivName(), resultBean.getUnivNameKana()));

        /* 学部・学科名 */
        boolean isCautionNeeded = ScheduleUtil.isCautionNeeded(systemDto.getUnivExamDiv(), resultBean.getUnivCd(), resultBean.getExamScheduleDetailFlag(), resultBean.getUnivCommentFlag());

        StringBuilder sb = new StringBuilder();

        if (isCautionNeeded) {
            sb.append("<html>").append("<span style=\"background-color:red;\" color=\"red\">!</span>").append("<span style=\"background-color:red;\" color=\"white\">!</span>");
            if ((ObjectUtils.toString(resultBean.getFacultyName()) + ObjectUtils.toString(resultBean.getDeptName())).length() < 11) {
                /* 見辛いのでスペースに余裕があるなら余白を入れる */
                sb.append("<span style=\"background-white;\" color=\"white\">!</span>");
            }
        }

        sb.append(ObjectUtils.toString(resultBean.getFacultyName()));

        if (!StringUtils.isEmpty(resultBean.getFacultyName()) && !isCautionNeeded) {
            sb.append(" ");
        }

        sb.append(ObjectUtils.toString(resultBean.getDeptName()));

        if (isCautionNeeded) {
            sb.append("</html>");
        }

        /* ペア値（sortValue）を帳票用に設定しておく */
        map.put("subjectFacultyDays", new CellData(sb.toString(), Pair.of(resultBean.getFacultyName(), (isCautionNeeded ? "【！】" : "") + ObjectUtils.toString(resultBean.getDeptName()))));

        /* センタボーダ */
        map.put("borderOfCenter", new CellData(resultBean.getBorderPoint()));

        /* ボーダ得点率 */
        if (resultBean.getCenScoreRate() == null) {
            map.put("pointAverageOfBorder", new CellData(null));
        } else {
            map.put("pointAverageOfBorder", new CellData(resultBean.getCenScoreRate() + "%", resultBean.getCenScoreRate()));
        }

        /* 二次ランク */
        map.put("rankOfSecond", new RankCellData(resultBean.getSecondRank()));

        /* センタ得点 */
        map.put("pointOfCenter", new CellData(resultBean.getcScore()));

        /* 二次偏差値 */
        map.put("devSecond", new CellData(resultBean.getsScore()));
        map.put("devSecond2", new CellData(resultBean.getsScore()));

        /* センタ評価 */
        map.put("valuateCenter", new RatingCellData(resultBean.getCenterRating()));

        /* 二次評価 */
        map.put("valuateSecond", new RatingCellData(resultBean.getSecondRating()));

        /* 総合評価 */
        map.put("valuateSynthetic", new RatingCellData(resultBean.getTotalRating()));

        /* センタ配点比 */
        map.put("compStartPointCenter", new CellData(resultBean.getcAllotPntRate()));

        /* 二次配点比 */
        map.put("compStartPointSecond", new CellData(resultBean.getsAllotPntRate()));

        /* 出願締切日 */
        map.put("limitOfApply", toMD(resultBean.getEntExamApplideadLine(), ScheduleUtil.isAppliDeadLineEnabled(systemDto.getUnivExamDiv(), resultBean.getUnivCd())));

        /* 入試日 */
        map.put("exmntnDay", toMD(resultBean.getEntExamInpleDate(), ScheduleUtil.isInpleDateEnabled(systemDto.getUnivExamDiv(), resultBean.getUnivCd())));

        /* 日程コード */
        map.put("scheduleCd", new CellData(resultBean.getScheduleCd()));

        /* 学部名（検索結果を志望大学詳細画面に表示する際に利用） */
        map.put("facultyName", new CellData(resultBean.getFacultyName()));

        /* 学科名（検索結果を志望大学詳細画面に表示する際に利） */
        map.put("deptName", new CellData(resultBean.getDeptName()));

        /* 【含まない】ボーダ/満点(率) */
        /* 【含まない】得点 */
        /* 【含まない】評価 */
        /* 【含まない】得点率 */
        /* 【含まない】本人得点率 */
        /*
        if (scoreHelper.getMarkExam() == null || !ExamUtil.isCenter(scoreHelper.getMarkExam())) {
            map.put("borderOfExclude", new CellData(resultBean.getBorderOfExclude()));
            map.put("pointOfExclude", new CellData(resultBean.getPointOfExclude()));
            map.put("valuateExclude", new CellData(resultBean.getValuateExclude()));
            map.put("scoreRateExclude", new CellData(resultBean.getCenScoreRate()));
            map.put("pointScoreRateExclude", new CellData(resultBean.getPointExScoreRate()));
        } else {
            if (!StringUtil.isEmptyTrim(resultBeanInc.getAppReqPaternCd())) {
                map.put("borderOfExclude", new CellData(""));
                map.put("pointOfExclude", new CellData(""));
                map.put("valuateExclude", new CellData(""));
                map.put("scoreRateExclude", new CellData(""));
                map.put("pointScoreRateExclude", new CellData(""));
            } else {
                map.put("borderOfExclude", new CellData(resultBean.getBorderOfExclude()));
                map.put("pointOfExclude", new CellData(resultBean.getPointOfExclude()));
                map.put("valuateExclude", new CellData(resultBean.getValuateExclude()));
                map.put("scoreRateExclude", new CellData(resultBean.getCenScoreRate()));
                map.put("pointScoreRateExclude", new CellData(resultBean.getPointExScoreRate()));
            }
        }
        */
        map.put("borderOfExclude", new CellData(resultBean.getBorderOfExclude()));
        map.put("pointOfExclude", new CellData(resultBean.getPointOfExclude()));
        map.put("pointOfExclude2", new CellData(resultBean.getPointOfExclude()));
        map.put("valuateExclude", new CellData(resultBean.getValuateExclude()));
        map.put("scoreRateExclude", new CellData(resultBean.getCenScoreRate()));
        map.put("pointScoreRateExclude", new CellData(resultBean.getPointExScoreRate()));

        /* 【含む】ボーダ/満点(率) */
        /* 【含む】得点 */
        /* 【含む】評価 */
        /* 【含む】得点率 */
        /* 【含む】本人得点率 */
        /*
        if (scoreHelper.getMarkExam() == null || !StringUtil.isEmptyTrim(resultBeanInc.getAppReqPaternCd())) {
            map.put("borderOfInclude", new CellData(resultBeanInc.getBorderOfInclude()));
            map.put("pointOfInclude", new CellData(resultBeanInc.getPointOfInclude()));
            map.put("valuateInclude", new CellData(resultBeanInc.getValuateInclude()));
            map.put("scoreRateInclude", new CellData(resultBeanInc.getCenScoreRate()));
            map.put("pointScoreRateInclude", new CellData(resultBeanInc.getPointIncScoreRate()));
        } else {
            map.put("borderOfInclude", new CellData(""));
            map.put("pointOfInclude", new CellData(""));
            map.put("valuateInclude", new CellData(""));
            map.put("scoreRateInclude", new CellData(""));
            map.put("pointScoreRateInclude", new CellData(""));
        }
        */
        /* 二次満点値 */
        /*map.put("fullSecond", new CellData(resultBean.getsFullPoint()));*/

        return map;
    }

    /**
     * YYYYMMDDをM/Dセルデータに変換します。
     *
     * @param yyyymmdd YYYYMMDD
     * @param scheduleFlg 日程表示フラグ
     * @return {@link CellData}
     */
    private CellData toMD(String yyyymmdd, boolean scheduleFlg) {
        if (scheduleFlg) {
            return new NullGreaterCellData(ExmntnRsltUtil.toMD(yyyymmdd), yyyymmdd);
        } else {
            return new NullGreaterCellData(null);
        }
    }

}

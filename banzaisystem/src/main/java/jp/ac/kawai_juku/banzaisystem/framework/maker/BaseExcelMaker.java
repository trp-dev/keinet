package jp.ac.kawai_juku.banzaisystem.framework.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Template;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.exception.ResourceNotFoundRuntimeException;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.OutputStreamUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * Excel帳票メーカーの基底クラスです。
 *
 *
 * @param <T> 入力Beanの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseExcelMaker<T extends ExcelMakerInBean> implements FormMaker<T> {

    /** 帳票テンプレート設定 */
    private final Template template;

    /** 生成したExcelファイルリスト */
    private final List<File> fileList = CollectionsUtil.newArrayList();

    /** 帳票作成日 */
    private final String createDate;

    /** 現在のシートへの変更発生フラグ  */
    private boolean sheetModifiedFlg;

    /** 現在のワークブック */
    private HSSFWorkbook workbook;

    /** 現在のシート */
    private HSSFSheet sheet;

    /** ワークブックカウント */
    private int workbookCount;

    /** シートカウント */
    private int sheetCount;

    /** テンプレートシートカウント */
    private int templateCount;

    /** テンプレートインデックス */
    private int templateIndex;

    /**
     * コンストラクタです。
     */
    public BaseExcelMaker() {
        Template t = BZClassUtil.getOriginalClass(getClass()).getAnnotation(Template.class);
        if (t == null) {
            throw new RuntimeException("@Templateアノテーションが未設定です。");
        } else {
            this.template = t;
        }

        this.createDate = DateFormatUtils.format(System.currentTimeMillis(), "yyyy/MM/dd");
    }

    @Override
    public List<File> create(T inBean) {

        /* データを準備する */
        if (!prepareData(inBean)) {
            /* 出力するデータなし */
            return fileList;
        }

        /* ワークブックを開く */
        openWorkbook();

        /* 最初のシートを認識させる */
        breakSheet();

        /* データを出力する */
        outputData();

        /* ワークブックを保存する */
        saveWorkbook();

        return fileList;
    }

    @Override
    public PrintId getPrintId() {
        return template.id();
    }

    /**
     * 出力するデータを準備します。<br>
     * {@link #outputData()}の呼び出し前に、1回だけ呼び出されます。
     *
     * @param inBean {@link ExcelMakerInBean}
     * @return 出力するデータがあるならtrue
     */
    protected abstract boolean prepareData(T inBean);

    /**
     * シートを初期化します。<br>
     * 改シート（改ブック）が発生したタイミングでも呼び出されます。
     */
    protected abstract void initSheet();

    /**
     * データをExcelに出力します。
     */
    protected abstract void outputData();

    /**
     * ワークブックを開きます。
     */
    private void openWorkbook() {

        String name = "/excel/" + template.id() + ".xls";

        InputStream in = null;
        try {
            in = new BufferedInputStream(getTemplateResource(name));
            workbook = new HSSFWorkbook(in);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        } finally {
            InputStreamUtil.close(in);
        }

        workbookCount++;
        sheetCount = 0;
        templateCount = workbook.getNumberOfSheets();
    }

    /**
     * 帳票テンプレートのリソースを返します。
     *
     * @param name リソース名
     * @return {@link InputStream}
     */
    private InputStream getTemplateResource(String name) {
        InputStream in = BaseExcelMaker.class.getResourceAsStream(name);
        if (in == null) {
            throw new ResourceNotFoundRuntimeException(String.format("帳票テンプレート[%s]が見つかりませんでした。", name));
        }
        return in;
    }

    /**
     * 改シート処理を行います。
     */
    public final void breakSheet() {

        /* 現在のシートに変更がなければ破棄する */
        if (sheetCount > 0 && !sheetModifiedFlg) {
            destroyCurrentSheet();
        }

        /* 最大シート数になっているなら改ブックする */
        if (template.maxSheetCount() > 0 && sheetCount == template.maxSheetCount()) {
            saveWorkbook();
            openWorkbook();
        }

        /* 現在のシートを切り替える */
        sheetCount++;
        if (template.hasGraph()) {
            /* グラフ帳票 */
            sheet = workbook.getSheetAt(sheetCount - 1);
        } else {
            /* 表帳票 */
            workbook.cloneSheet(templateIndex);
            int index = workbook.getNumberOfSheets() - 1;
            workbook.setSheetName(index, String.valueOf(sheetCount));
            sheet = workbook.getSheetAt(index);
        }

        /* 次のテンプレートにする */
        if (templateIndex + 1 < templateCount) {
            templateIndex++;
        }

        /* 帳票作成日をセットする */
        setCreateDate();

        /* シートを初期化する */
        initSheet();

        sheetModifiedFlg = false;
    }

    /**
     * テンプレートの位置をリセットしてから改シートします。<br>
     * ※このメソッド呼び出し直後は、1シート目のテンプレートとなります。
     */
    public final void resetTemplateIndexAndBreakSheet() {
        templateIndex = 0;
        breakSheet();
    }

    /**
     * 現在のシートを破棄します。
     */
    private void destroyCurrentSheet() {

        /* 表のみ帳票は現在のシートを削除する */
        if (!template.hasGraph()) {
            workbook.removeSheetAt(workbook.getNumberOfSheets() - 1);
        }

        sheetCount--;
    }

    /**
     * ワークブックを保存します。
     */
    private void saveWorkbook() {

        /* 現在のシートに変更がなければ破棄する */
        if (sheetCount > 0 && !sheetModifiedFlg) {
            destroyCurrentSheet();
        }

        /* 出力するシートがなければ処理はここまで */
        if (sheetCount < 1) {
            return;
        }

        /* 不要シートを削除する */
        if (template.hasGraph()) {
            /* グラフ帳票 */
            cleanWorkbook();
        } else {
            /* 表帳票 */
            for (int i = 0; i < templateCount; i++) {
                workbook.removeSheetAt(0);
            }
        }

        if (workbook.getNumberOfSheets() > 0) {
            writeWorkbook();
        }
    }

    /**
     * 現在のワークブックから不要なシートを消す
     */
    private void cleanWorkbook() {

        /* 無効な名前の定義を消す */
        for (int i = workbook.getNumberOfNames() - 1; i >= 0; i--) {
            HSSFName name = workbook.getNameAt(i);
            try {
                if (Integer.parseInt(name.getSheetName()) > sheetCount) {
                    workbook.removeName(i);
                }
            } catch (NumberFormatException e) {
                throw new RuntimeException("シート名が数字以外で定義されています。" + name.getSheetName(), e);
            }
        }

        /* シートを消す */
        for (int i = workbook.getNumberOfSheets() - 1; i >= sheetCount; i--) {
            workbook.removeSheetAt(i);
        }
    }

    /**
     * ワークブックをファイルに書き出します。
     */
    private void writeWorkbook() {

        BufferedOutputStream out = null;
        try {
            File file = createWorkbookFile();
            /* 終了時に消えるようにする */
            file.deleteOnExit();
            out = new BufferedOutputStream(new FileOutputStream(file));
            workbook.write(out);
            fileList.add(file);
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E005"), e);
        } finally {
            OutputStreamUtil.close(out);
        }
    }

    /**
     * ワークブックのファイルオブジェクトを生成します。
     *
     * @return ファイル
     * @throws IOException 出力ディレクトリの生成に失敗したとき
     */
    private File createWorkbookFile() throws IOException {
        return new File(SettingPropsUtil.getOutputDir(), template.id() + "_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS") + ".xls");
    }

    /**
     * 現在のシートからセルを取得します。
     *
     * @param rowIndex 行位置
     * @param cellnum 列位置
     * @return {@link HSSFCell}
     */
    public final HSSFCell getCell(final int rowIndex, final int cellnum) {

        /* シート変更フラグを立てる */
        sheetModifiedFlg = true;

        HSSFRow row = sheet.getRow(rowIndex);
        if (row == null) {
            row = sheet.createRow(rowIndex);
        }

        HSSFCell cell = row.getCell(cellnum);
        if (cell == null) {
            cell = row.createCell(cellnum);
        }

        return cell;
    }

    /**
     * 「名前ボックス」の表記（A4など）で現在のシートからセルを取得します。
     *
     * @param position セル位置
     * @return {@link HSSFCell}
     */
    public final HSSFCell getCell(String position) {

        int index = 0;
        for (char c : position.toCharArray()) {
            if (Character.isDigit(c)) {
                break;
            }
            index++;
        }

        String col = position.substring(0, index);
        int rowIndex = Integer.parseInt(position.substring(index)) - 1;

        int cellnum = -1;
        for (int i = 0; i < col.length(); i++) {
            cellnum += ((char) col.charAt(i) - 64) * Math.pow(26, col.length() - i - 1);
        }

        return getCell(rowIndex, cellnum);
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param position セル位置
     * @param value セットする値
     */
    public final void setCellValue(String position, String value) {
        setCellValue(getCell(position), value);
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param rowIndex 行位置
     * @param cellnum 列位置
     * @param value セットする値
     */
    public final void setCellValue(int rowIndex, int cellnum, String value) {
        setCellValue(getCell(rowIndex, cellnum), value);
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param cell {@link HSSFCell}
     * @param value セットする値
     */
    public final void setCellValue(HSSFCell cell, String value) {
        if (value == null) {
            cell.setCellValue(StringUtils.EMPTY);
        } else {
            cell.setCellValue(value);
        }
    }

    /**
     * セルに数値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param position セル位置
     * @param value セットする値
     */
    public final void setCellValue(String position, Number value) {
        setCellValue(getCell(position), value);
    }

    /**
     * セルに数値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param rowIndex 行位置
     * @param cellnum 列位置
     * @param value セットする値
     */
    public final void setCellValue(int rowIndex, int cellnum, Number value) {
        setCellValue(getCell(rowIndex, cellnum), value);
    }

    /**
     * セルに数値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param cell {@link HSSFCell}
     * @param value セットする値
     */
    public final void setCellValue(HSSFCell cell, Number value) {
        if (value == null) {
            cell.setCellValue(StringUtils.EMPTY);
        } else {
            cell.setCellValue(value.doubleValue());
        }
    }

    /**
     * 帳票作成日をセットする
     */
    private void setCreateDate() {
        sheet.getHeader().setRight(HSSFHeader.font("ＭＳ Ｐゴシック", "標準") + createDate);
    }

    /**
     * シートカウントを返します。
     *
     * @return シートカウント
     */
    public final int getSheetCount() {
        return sheetCount;
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。<br>
     * セル内改行有効
     *
     * @param position セル位置
     * @param value セットする値
     */
    public final void setCellValueSeparator(String position, String value) {
        setCellValue(getCell(position), new HSSFRichTextString(value));
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。<br>
     * セル内改行有効
     *
     * @param cell {@link HSSFCell}
     * @param value セットする値
     */
    private void setCellValue(HSSFCell cell, HSSFRichTextString value) {
        if (value == null) {
            cell.setCellValue(StringUtils.EMPTY);
        } else {
            cell.setCellValue(value);
            HSSFCellStyle cs = cell.getCellStyle();
            cs.setWrapText(true);
            cell.setCellStyle(cs);
        }
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param rowIndex 行位置
     * @param cellnum 列位置
     * @param style セルのスタイル
     */
    public final void setCellStyle(int rowIndex, int cellnum, HSSFCellStyle style) {
        setCellStyle(style, getCell(rowIndex, cellnum));
    }

    /**
     * セルに文字列の値をセットします。<br>
     * 値が無効値の場合は、空文字列をセットします。
     *
     * @param style セルのスタイル
     * @param cell セル情報
     */
    public final void setCellStyle(HSSFCellStyle style, HSSFCell cell) {
        cell.setCellStyle(style);
    }

    /**
     * 指定したシートを削除します。
     *
     * @param index シートインデックス
     */
    public final void removeSheetAt(int index) {
        workbook.removeSheetAt(index);
    }

    /**
     * 帳票テンプレート設定を返します。
     *
     * @return 帳票テンプレート設定
     */
    public final Template getTemplate() {
        return template;
    }

    /**
     * セル設定を作成します。
     *
     * @return セル設定
     */
    public final HSSFCellStyle createCellStyle() {
        return workbook.createCellStyle();
    }

    /**
     * フォントを作成します。
     *
     * @return フォント
     */
    protected final Font createFont() {
        return workbook.createFont();
    }

}

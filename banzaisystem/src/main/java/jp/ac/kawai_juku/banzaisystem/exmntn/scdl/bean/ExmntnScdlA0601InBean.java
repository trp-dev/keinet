package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean;

import java.util.Collections;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

/**
 * Excel帳票メーカー(受験スケジュール)の入力Beanです。
 *
 *
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnScdlA0601InBean implements ExcelMakerInBean {

    /** 生徒情報リスト */
    private final List<ExmntnScdlA0601StudentInBean> studentList;

    /**
     * コンストラクタです。
     *
     * @param studentBean 生徒情報Bean
     * @param scoreBean 成績データ＋選択科目情報
     * @param scheduleList 受験スケジュールリスト
     */
    public ExmntnScdlA0601InBean(ExmntnUnivStudentBean studentBean, ScoreBean scoreBean,
            List<ExmntnScdlBean> scheduleList) {
        this.studentList =
                Collections.singletonList(new ExmntnScdlA0601StudentInBean(studentBean, scoreBean, scheduleList));
    }

    /**
     * コンストラクタです。
     *
     * @param studentList 生徒情報リスト
     */
    public ExmntnScdlA0601InBean(List<ExmntnScdlA0601StudentInBean> studentList) {
        this.studentList = studentList;
    }

    /**
     * 生徒情報リストを返します。
     *
     * @return 生徒情報リスト
     */
    public List<ExmntnScdlA0601StudentInBean> getStudentList() {
        return studentList;
    }

}

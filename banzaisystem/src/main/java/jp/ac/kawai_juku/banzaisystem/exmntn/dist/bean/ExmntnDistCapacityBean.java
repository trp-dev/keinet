package jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean;

/**
 *
 * 大志望大学学力分布画面の募集定員情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistCapacityBean {

    /** 募集定員 */
    private String offerCapacity;

    /** 募集信頼 */
    private String capacityFlag;

    /**
     * 募集定員を返します。
     *
     * @return 募集定員
     */
    public String getOfferCapacity() {
        return offerCapacity;
    }

    /**
     * 募集定員をセットします。
     *
     * @param offerCapacity 募集定員
     */
    public void setOfferCapacity(String offerCapacity) {
        this.offerCapacity = offerCapacity;
    }

    /**
     * 募集信頼を返します。
     *
     * @return 募集信頼
     */
    public String getCapacityFlag() {
        return capacityFlag;
    }

    /**
     * 募集信頼をセットします。
     *
     * @param capacityFlag 募集信頼
     */
    public void setCapacityFlag(String capacityFlag) {
        this.capacityFlag = capacityFlag;
    }

}

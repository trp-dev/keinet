package jp.ac.kawai_juku.banzaisystem.framework.creator;

import org.seasar.framework.container.ComponentCustomizer;
import org.seasar.framework.container.assembler.AutoBindingDefFactory;
import org.seasar.framework.container.creator.ComponentCreatorImpl;
import org.seasar.framework.container.deployer.InstanceDefFactory;
import org.seasar.framework.convention.NamingConvention;

/**
 *
 * Viewクラス用の {@link org.seasar.framework.container.ComponentCreator} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ViewCreator extends ComponentCreatorImpl {

    /** コンポーネント名のサフィックス */
    public static final String NAME_SUFFIX = "View";

    /**
     * コンストラクタです。
     *
     * @param namingConvention 命名規約
     */
    public ViewCreator(NamingConvention namingConvention) {
        super(namingConvention);
        setNameSuffix(NAME_SUFFIX);
        setInstanceDef(InstanceDefFactory.SINGLETON);
        setAutoBindingDef(AutoBindingDefFactory.NONE);
    }

    /**
     * {@link ComponentCustomizer}を返します。
     *
     * @return コンポーネントカスタマイザ
     */
    public ComponentCustomizer getViewCustomizer() {
        return getCustomizer();
    }

    /**
     * {@link ComponentCustomizer}を設定します。
     *
     * @param customizer
     *            コンポーネントカスタマイザ
     */
    public void setViewCustomizer(ComponentCustomizer customizer) {
        setCustomizer(customizer);
    }

}

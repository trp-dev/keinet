package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.dao;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.bean.ExmntnPrntUnivDetailBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

/**
 *
 * 志望大学リストのDAOです。
 *
 *
 * @author TOTEC)FURUZAWA.Yusuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnPrntDao extends BaseDao {

    /**
     * 大学詳細を取得します。
     *
     * @param bean {@link ExmntnMainCandidateUnivBean}
     * @return {@link ExmntnPrntUnivDetailBean}
     */
    public ExmntnPrntUnivDetailBean selectUnivDetail(ExmntnMainCandidateUnivBean bean) {
        return jdbcManager.selectBySqlFile(ExmntnPrntUnivDetailBean.class, getSqlPath(), bean).getSingleResult();
    }

}

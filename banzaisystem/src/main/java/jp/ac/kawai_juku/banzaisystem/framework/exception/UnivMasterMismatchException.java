package jp.ac.kawai_juku.banzaisystem.framework.exception;

import jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager;

/**
 *
 * メモリ上の大学マスタと、DBの大学マスタの整合性が合っていない場合の例外です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class UnivMasterMismatchException extends MessageDialogException {

    /**
     * コンストラクタです。
     */
    public UnivMasterMismatchException() {
        super(MessageManager.getMessage("error.framework.CM_E011"));
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.component;

import java.awt.Image;
import java.awt.MediaTracker;
import java.lang.reflect.Field;
import java.util.Map;

import javax.swing.ImageIcon;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Judge;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 個人成績・志望大学画面 画像ラベルです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
@SuppressWarnings("serial")
public class ExmntnUnivImageLabel extends ImageLabel {

    /** 判定アイコン種別 */
    private static final String[] ICON_TYPE = { "A", "AH", "AG", "A#", "B", "BH", "BG", "B#", "C", "CH", "CG", "C#", "D", "DH", "DG", "D#", "E", "EH", "EG", "E#", "G", "X", "Asterisk", "!", "×!", "×", "△!", "○" };

    /** 判定アイコンマップ */
    private final Map<String, ImageIcon> iconMap = CollectionsUtil.newHashMap();

    @Override
    public void initialize(Field field, AbstractView view) {

        super.initialize(field, view);

        for (String type : ICON_TYPE) {
            ImageIcon imgIcon = new ImageIcon(ImageUtil.readImage(view.getClass(), "label/" + "result" + type + ".png"));
            if (type.equals("Asterisk")) {
                iconMap.put(Judge.JUDGE_STR_X, imgIcon);
            } else {
                iconMap.put(type, imgIcon);
            }
        }
    }

    /**
     * 判定結果に応じたアイコンを設定します。
     *
     * @param strResult 判定結果文字
     */
    public void setRatingIcon(String strResult) {

        setIcon(null);

        if (strResult != null) {
            String key = strResult.trim();
            if (iconMap.containsKey(key)) {
                setIcon(iconMap.get(key));
            }
        }
    }

    /**
     * 判定結果に応じたアイコンを設定します。
     *
     * @param strResult 判定結果文字
     * @param rating 判定結果
     */
    public void setRatingIcon(String strResult, double rating) {

        setIcon(null);

        if (strResult != null) {
            String key = strResult.trim();
            if (iconMap.containsKey(key)) {
                ImageIcon icon = iconMap.get(key);
                MediaTracker tracker = new MediaTracker(this);
                /* ポイント２．getScaledInstanceで大きさを変更します。 */
                Image smallImg = icon.getImage().getScaledInstance((int) (icon.getIconWidth() * rating), -1, Image.SCALE_SMOOTH);

                /* ポイント３．MediaTrackerで処理の終了を待ちます。 */
                tracker.addImage(smallImg, 1);

                ImageIcon smallIcon = new ImageIcon(smallImg);
                setIcon(smallIcon);
            }
        }
    }

}

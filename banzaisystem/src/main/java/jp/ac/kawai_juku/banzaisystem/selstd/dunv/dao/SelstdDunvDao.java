package jp.ac.kawai_juku.banzaisystem.selstd.dunv.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 大学選択ダイアログのDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDunvDao extends BaseDao {

    /**
     * 選択可能大学リストを取得します。
     *
     * @param exam 対象模試
     * @param idList 対象生徒の個人IDリスト
     * @return 選択可能大学リスト
     */
    public List<SelstdDunvBean> selectSelectableUnivList(ExamBean exam, List<Integer> idList) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("idCondition", DaoUtil.createIn("B.INDIVIDUALID", idList));
        return jdbcManager.selectBySqlFile(SelstdDunvBean.class, getSqlPath(), param).getResultList();
    }

}

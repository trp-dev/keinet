package jp.ac.kawai_juku.banzaisystem.mainte.impt.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 個人成績インポート処理のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteImptImportDao extends BaseDao {

    /**
     * インポート可能な模試リストを取得します。
     *
     * @param univExamDiv 大学マスタの模試区分
     * @return 模試リスト
     */
    public List<ExamBean> selectExamList(String univExamDiv) {
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath(), univExamDiv).getResultList();
    }

    /**
     * 年度・学年・クラス・クラス番号が一致する生徒情報リストを取得します。
     *
     * @param year 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号
     * @return 生徒情報リスト
     */
    public List<BeanMap> selectStudentList(String year, String grade, String cls, String classNo) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("grade", StringUtils.trimToNull(grade));
        param.put("class", cls);
        param.put("classNo", StringUtils.trimToNull(classNo));
        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 学籍基本情報を登録します。
     *
     * @param kanaName カナ氏名
     * @return 個人ID
     */
    public Integer insertBasicInfo(String kanaName) {
        BeanMap param = new BeanMap();
        param.put("kanaName", StringUtils.trimToNull(kanaName));
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
        return selectIndividualId();
    }

    /**
     * 直前に登録した学籍基本情報の個人IDを取得します。
     *
     * @return 個人ID
     */
    private Integer selectIndividualId() {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath()).getSingleResult();
    }

    /**
     * 学籍履歴情報を登録します。
     *
     * @param id 個人ID
     * @param year 年度
     * @param grade 学年
     * @param cls クラス
     * @param classNo クラス番号（内部で0詰めして5桁にします）
     */
    public void insertHistoryInfo(Integer id, String year, String grade, String cls, String classNo) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("year", year);
        param.put("grade", grade.isEmpty() ? null : Integer.valueOf(grade));
        param.put("class", cls);
        param.put("classNo", StringUtils.leftPad(StringUtils.trimToNull(classNo), 5, '0'));
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 成績データを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void deleteSubRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 模試記入志望大学データを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void deleteCandidateRating(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 個表データを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void deleteIndividualRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 個表データを登録します。
     *
     * @param param パラメータ
     */
    public void insertIndividualRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 国語記述式評価データを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void deleteJpnDescEvaluationRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 国語記述式評価データを登録します。
     *
     * @param param パラメータ
     */
    public void insertJpnDescEvaluationRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * CEFRスコアデータを削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    /*public void deleteCefrScoreRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }*/

    /**
     * CEFRスコアデータを登録します。
     *
     * @param param パラメータ
     */
    /*public void insertCefrScoreRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }*/

    /**
     * 指定した大学の日程コードを取得します。
     *
     * @param exam 模試データ
     * @param univKeyBean {@link UnivKeyBean}
     * @return 日程コード
     */
    public String selectScheduleCd(ExamBean exam, UnivKeyBean univKeyBean) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        param.put("univ", univKeyBean);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 模試記入志望大学データを登録します。
     *
     * @param param パラメータ
     */
    public void insertCandidateRating(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 志望大学データ（模試記入志望大学データのみ）を削除します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @return 削除件数
     */
    public int deleteCandidateUniv(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 非表示の志望大学を表示します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void updateCandidateUnivDispOn(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 模試記入志望大学データを志望大学にコピーします。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @param univExamDiv 大学マスタの模試区分
     */
    public void insertCandidateUniv(Integer id, ExamBean exam, String univExamDiv) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        param.put("univExamDiv", univExamDiv);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 模試記入志望大学と重複している志望大学を非表示にします。
     *
     * @param id 個人ID
     * @param exam 模試データ
     */
    public void updateCandidateUnivDispOff(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 志望大学の表示順を振り直して取得します。
     *
     * @param id 個人ID
     * @param exam 模試データ
     * @return 志望大学リスト
     */
    public List<BeanMap> selectCandidateUnivList(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getResultList();
    }

    /**
     * 志望大学の表示順を更新します。
     *
     * @param list 志望大学リスト
    */
    public void updateCandidateUnivDispNumber(List<BeanMap> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * 個人成績インポート状況を削除します。
     *
     * @param exam 模試データ
     */
    public void deleteStatSubRecord(ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 個人成績インポート状況を登録します。
     *
     * @param exam 模試データ
     */
    public void insertStatSubRecord(ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 英語参加試験マスタの件数を返します。
     *
     * @param examCd 英語参加試験コード
     * @return 件数
     */
    /*public Integer selectEngptInfoCount(String examCd) {
        BeanMap param = new BeanMap();
        param.put("examCd", examCd);
        return Integer.valueOf(jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getSingleResult());
    }*/

    /**
     * 英語参加試験マスタの件数を返します。
     *
     * @param examCd 英語参加試験コード
     * @param cefrExamLevel 英語参加試験レベルコード
     * @return 件数
     */
    /*public Integer selectEngptDetailInfoCount(String examCd, String cefrExamLevel) {
        BeanMap param = new BeanMap();
        param.put("examCd", examCd);
        param.put("cefrExamLevel", cefrExamLevel);
        return Integer.valueOf(jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getSingleResult());
    }*/

    /**
     * CEFRマスタの有無を返します。
     *
     * @param cefrLevelCd 英語参加試験CEFR
     * @return 件数
     */
    /*public Integer selectCefrInfoCount(String cefrLevelCd) {
        BeanMap param = new BeanMap();
        param.put("cefrLevelCd", cefrLevelCd);
        return Integer.valueOf(jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getSingleResult());
    }*/
}

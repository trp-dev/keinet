package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;

/**
 *
 * バンザイシステムの {@link javax.swing.plaf.ComboBoxUI} です。<br>
 * Windows7でコンボボックスの背景色が変更されない問題に対応するため、
 * 独自のComboBoxUIを定義しています。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZComboBoxUI extends javax.swing.plaf.basic.BasicComboBoxUI {

    /**
     * ComponentUIを生成します。
     *
     * @param c コンポーネント
     * @return {@link ComponentUI}
     */
    public static ComponentUI createUI(JComponent c) {
        return new BZComboBoxUI();
    }

    @Override
    public void paintCurrentValueBackground(Graphics g, Rectangle bounds, boolean hasFocus) {
        Color c = g.getColor();
        if (hasFocus && !isPopupVisible(comboBox)) {
            g.setColor(UIManager.getColor("ComboBox.selectionBackground"));
        } else {
            g.setColor(comboBox.getBackground());
        }
        g.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
        g.setColor(c);
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void paintCurrentValue(Graphics g, Rectangle bounds, boolean hasFocus) {

        bounds.x += 2;
        bounds.y += 2;
        bounds.width -= 4;
        bounds.height -= 4;

        if (!comboBox.isEditable()) {
            ListCellRenderer renderer = comboBox.getRenderer();
            Component c;
            if (hasFocus && !isPopupVisible(comboBox)) {
                c = renderer.getListCellRendererComponent(listBox, comboBox.getSelectedItem(), -1, true, false);
            } else {
                c = renderer.getListCellRendererComponent(listBox, comboBox.getSelectedItem(), -1, false, false);
            }
            c.setFont(comboBox.getFont());
            if (comboBox.isEnabled()) {
                if (hasFocus && !isPopupVisible(comboBox)) {
                    c.setForeground(UIManager.getColor("ComboBox.selectionForeground"));
                    c.setBackground(UIManager.getColor("ComboBox.selectionBackground"));
                } else {
                    c.setForeground(comboBox.getForeground());
                    c.setBackground(comboBox.getBackground());
                }
            } else {

                c.setForeground(UIManager.getColor("ComboBox.disabledForeground"));
                c.setBackground(UIManager.getColor("ComboBox.disabledBackground"));
            }
            boolean shouldValidate = false;
            if (c instanceof JPanel) {
                shouldValidate = true;
            }
            currentValuePane.paintComponent(g, c, comboBox, bounds.x, bounds.y, bounds.width, bounds.height, shouldValidate);
        } else {
            super.paintCurrentValue(g, bounds, hasFocus);
        }
    }

}

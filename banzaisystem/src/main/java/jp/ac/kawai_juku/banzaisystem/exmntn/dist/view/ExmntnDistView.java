package jp.ac.kawai_juku.banzaisystem.exmntn.dist.view;

import javax.swing.SwingConstants;

import jp.ac.kawai_juku.banzaisystem.exmntn.dist.component.ExmntnDistGraphPanel;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

/**
 *
 * u]åwwÍªzæÊÌViewÅ·B
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistView extends SubView {

    /** R{{bNX_u]Ê */
    @Location(x = 62, y = 7)
    @Size(width = 43, height = 20)
    @FireActionEvent
    public ComboBox<ExmntnMainCandidateUnivBean> candidateRankComboBox;

    /** R{{bNX_}[NV[g± */
    @Location(x = 9, y = 35)
    @Size(width = 184, height = 20)
    public ComboBox<ExamBean> examComboBox;;

    /** OÌåw{^ */
    @Location(x = 118, y = 8)
    @Disabled
    public ImageButton beforeUnivButton;

    /** Ìåw{^ */
    @Location(x = 176, y = 8)
    @Disabled
    public ImageButton nextUnivButton;

    /** åw¼x */
    @Location(x = 241, y = 7)
    @Font(BZFont.UNIV_NAME)
    public TextLabel univNameLabel;

    /** åWlõx */
    @Location(x = 484, y = 8)
    public TextLabel capacityLabel;

    /** u]Ò|oè\èÒ|vx */
    @Location(x = 514, y = 113)
    @Size(width = 50, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersPlanALabel;

    /** u]Ò|oè\èÒ|»ðx */
    @Location(x = 571, y = 113)
    @Size(width = 46, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersPlanSLabel;

    /** u]Ò|oè\èÒ|²x */
    @Location(x = 624, y = 113)
    @Size(width = 42, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersPlanGLabel;

    /** u]Ò|u]|vx */
    @Location(x = 514, y = 136)
    @Size(width = 50, height = 21)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersTotalALabel;

    /** u]Ò|u]|»ðx */
    @Location(x = 571, y = 136)
    @Size(width = 46, height = 21)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersTotalSLabel;

    /** u]Ò|u]|²x */
    @Location(x = 624, y = 136)
    @Size(width = 42, height = 21)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel numbersTotalGLabel;

    /** ½Ï·Z¾_^½ÏÎ·l|oè\èÒ|vx */
    @Location(x = 514, y = 158)
    @Size(width = 50, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averagePlanALabel;

    /** ½Ï·Z¾_^½ÏÎ·l|oè\èÒ|vx */
    @Location(x = 571, y = 158)
    @Size(width = 46, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averagePlanSLabel;

    /** ½Ï·Z¾_^½ÏÎ·l|oè\èÒ|vx */
    @Location(x = 624, y = 158)
    @Size(width = 42, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averagePlanGLabel;

    /** ½Ï·Z¾_^½ÏÎ·l|u]|vx */
    @Location(x = 514, y = 181)
    @Size(width = 50, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averageTotalALabel;

    /** ½Ï·Z¾_^½ÏÎ·l|u]|vx */
    @Location(x = 571, y = 181)
    @Size(width = 46, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averageTotalSLabel;

    /** ½Ï·Z¾_^½ÏÎ·l|u]|vx */
    @Location(x = 624, y = 181)
    @Size(width = 42, height = 22)
    @HorizontalAlignment(SwingConstants.RIGHT)
    public TextLabel averageTotalGLabel;

    /** ½Ï·Z¾_^½ÏÎ·lx */
    @Location(x = 390, y = 158)
    @Size(width = 59, height = 45)
    @Foreground(r = 1, g = 50, b = 152)
    public TextLabel avgScoreLabel;

    /** u]Ò|oè\èÒ/æ1u]Òx */
    @Location(x = 450, y = 113)
    @Size(width = 69, height = 22)
    public TextLabel numbersFirstChoiceLabel;

    /** ½Ï·Z¾_^½ÏÎ·l|oè\èÒ/æ1u]Òx */
    @Location(x = 450, y = 158)
    @Size(width = 69, height = 22)
    public TextLabel averageFirstChoiceLabel;

    /** ¾_ÑÊlOtpl */
    @Location(x = 9, y = 59)
    public ExmntnDistGraphPanel graphPanel;

    /** Âl¬Ñ\{^ */
    @Location(x = 495, y = 426)
    public ImageButton printPersonalScoreButton;

    /** åwÚ×óü{^ */
    @Location(x = 589, y = 426)
    public ImageButton printUnivDetailButton;

    @Override
    public void initialize() {
        super.initialize();
        candidateRankComboBox.setMaximumRowCount(9);
    }

}

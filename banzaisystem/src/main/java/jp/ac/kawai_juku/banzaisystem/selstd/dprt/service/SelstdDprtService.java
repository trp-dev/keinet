package jp.ac.kawai_juku.banzaisystem.selstd.dprt.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.dao.SelstdDprtDao;

/**
 *
 * 一覧出力ダイアログのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDprtService {

    /** DAO */
    @Resource
    private SelstdDprtDao selstdDprtDao;

    /**
     * 学年リストを取得します。
     *
     * @param exam 対象模試
     * @return 学年リスト
     */
    public List<Integer> findGradeList(ExamBean exam) {
        return selstdDprtDao.selectGradeList(exam);
    }

    /**
     * 学年リスト(全模試)を取得します。
     *
     * @param year 対象年度
     * @return 学年リスト
     */
    public List<Integer> findGradeListAll(String year) {
        return selstdDprtDao.selectGradeListAll(year);
    }

    /**
     * クラスリストを生成します。
     *
     * @param exam 対象模試
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> findClassList(ExamBean exam, Integer grade) {
        return selstdDprtDao.selectClassList(exam, grade);
    }

    /**
     * クラスリスト(全模試)を生成します。
     *
     * @param year 対象年
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> findClassListAll(String year, Integer grade) {
        return selstdDprtDao.selectClassListAll(year, grade);
    }
}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCefrRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainJpnRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSaveCefrDaoInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSaveJpnDaoInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSaveScoreDaoInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainSubjectRecordBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 個人成績・志望大学画面のDAOです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
public class ExmntnMainDao extends BaseDao {

    /**
     * 個人リストを取得します。
     *
     * @param year 年度
     * @param idList 個人IDリスト
     * @return 個人リスト
     */
    public List<ExmntnMainKojinBean> selectKojinList(String year, List<Integer> idList) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("idCondition", DaoUtil.createIn("B.INDIVIDUALID", idList));
        return jdbcManager.selectBySqlFile(ExmntnMainKojinBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 科目成績リストを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 科目成績リスト
     */
    public List<ExmntnMainSubjectRecordBean> selectSubRecordList(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(ExmntnMainSubjectRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * CEFRスコアを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return CEFRスコア
     */
    public List<ExmntnMainCefrRecordBean> selectCefrRecordList(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(ExmntnMainCefrRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 国語記述スコアを取得します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 国語記述スコア
     */
    public List<ExmntnMainJpnRecordBean> selectJpnRecordList(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(ExmntnMainJpnRecordBean.class, getSqlPath(), param).getResultList();
    }

    /**
     *配点を取得します。
     *
     * @param subCd 科目CD
     * @param exam 対象模試
     * @return 配点
     */
    public Integer selectScoringRecordList(String subCd, ExamBean exam) {
        Long work = null;
        BeanMap param = new BeanMap();
        param.put("subCd", subCd);
        param.put("exam", exam);
        work = jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult();
        if (work == null) {
            return 0;
        } else {
            return work.intValue();
        }
    }

    /**
     * 科目成績を削除します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     */
    public void deleteSubRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * CEFR成績を削除します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     */
    public void deleteCefrRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 国語記述成績を削除します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     */
    public void deleteJpnRecord(Integer id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 科目成績を登録します。
     *
     * @param list 科目成績リスト
     */
    public void insertSubRecord(List<ExmntnMainSaveScoreDaoInBean> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * CEFR成績を登録します。
     *
     * @param bean CEFR成績リスト
     */
    public void insertCefrRecord(ExmntnMainSaveCefrDaoInBean bean) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), bean).execute();
    }

    /**
     * 国語記述成績を登録します。
     *
     * @param bean 国語記述成績リスト
     */
    public void insertJpnRecord(ExmntnMainSaveJpnDaoInBean bean) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), bean).execute();
    }

    /**
     * 個表データのレコード数をカウントします。
     *
     * @param id 個人ID
     * @param exam 対象模試
     * @return 個表データのレコード数
     */
    public long countIndividualRecord(int id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        return jdbcManager.selectBySqlFile(Long.class, getSqlPath(), param).getSingleResult().longValue();
    }

    /**
     * 個表データを登録します。
     *
     * @param id 個人ID
     * @param exam 対象模試
     */
    public void insertIndividualRecord(int id, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("exam", exam);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

}

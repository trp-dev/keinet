package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * テーブルのセルデータを保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CellData implements Comparable<CellData> {

    /** 表示用の値 */
    private final Comparable<?> dispValue;

    /** ソート用の値 */
    private final Comparable<?> sortValue;

    /**
     * コンストラクタです。
     *
     * @param dispValue 表示用の値
     * @param sortValue ソート用の値
     */
    public CellData(Comparable<?> dispValue, Comparable<?> sortValue) {
        this.dispValue = dispValue;
        this.sortValue = sortValue;
    }

    /**
     * コンストラクタです。
     *
     * @param dispValue 表示文字列
     */
    public CellData(Comparable<?> dispValue) {
        this(dispValue, dispValue);
    }

    @Override
    public String toString() {
        return ObjectUtils.toString(dispValue);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public int compareTo(CellData o) {
        return ObjectUtils.compare((Comparable) sortValue, (Comparable) o.sortValue);
    }

    /**
     * 表示用の値を返します。
     *
     * @return 表示用の値
     */
    public Comparable<?> getDispValue() {
        return dispValue;
    }

    /**
     * ソート用の値を返します。
     *
     * @return ソート用の値
     */
    public Comparable<?> getSortValue() {
        return sortValue;
    }

}

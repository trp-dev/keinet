package jp.ac.kawai_juku.banzaisystem.framework.exception;

import jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager;

/**
 *
 * メモリ上の成績データバージョンと、DBの成績データバージョンの整合性が合っていない場合の例外です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class RecordInfoMismatchException extends MessageDialogException {

    /**
     * コンストラクタです。
     */
    public RecordInfoMismatchException() {
        super(MessageManager.getMessage("error.framework.CM_E019"));
    }

}

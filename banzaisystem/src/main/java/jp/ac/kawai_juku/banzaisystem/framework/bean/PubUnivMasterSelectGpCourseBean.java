package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ公表用選択グループを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterSelectGpCourseBean {

    /** 入試試験区分 */
    private String entExamDiv;

    /** 大学コード枝番 */
    private String uniCdBranchCd;

    /** 選択グループ番号 */
    private String selectGNo;

    /** 教科コード */
    private String courseCd;

    /** 選択グループ教科別選択科目数 */
    private int sGPerSubSubCount;

    /**
     * 入試試験区分を返します。
     *
     * @return 入試試験区分
     */
    public String getEntExamDiv() {
        return entExamDiv;
    }

    /**
     * 入試試験区分をセットします。
     *
     * @param entExamDiv 入試試験区分
     */
    public void setEntExamDiv(String entExamDiv) {
        this.entExamDiv = entExamDiv;
    }

    /**
     * 大学コード枝番を返します。
     *
     * @return 大学コード枝番
     */
    public String getUniCdBranchCd() {
        return uniCdBranchCd;
    }

    /**
     * 大学コード枝番をセットします。
     *
     * @param uniCdBranchCd 大学コード枝番
     */
    public void setUniCdBranchCd(String uniCdBranchCd) {
        this.uniCdBranchCd = uniCdBranchCd;
    }

    /**
     * 選択グループ番号を返します。
     *
     * @return 選択グループ番号
     */
    public String getSelectGNo() {
        return selectGNo;
    }

    /**
     * 選択グループ番号をセットします。
     *
     * @param selectGNo 選択グループ番号
     */
    public void setSelectGNo(String selectGNo) {
        this.selectGNo = selectGNo;
    }

    /**
     * 教科コードを返します。
     *
     * @return 教科コード
     */
    public String getCourseCd() {
        return courseCd;
    }

    /**
     * 教科コードをセットします。
     *
     * @param courseCd 教科コード
     */
    public void setCourseCd(String courseCd) {
        this.courseCd = courseCd;
    }

    /**
     * 選択グループ教科別選択科目数を返します。
     *
     * @return 選択グループ教科別選択科目数
     */
    public int getsGPerSubSubCount() {
        return sGPerSubSubCount;
    }

    /**
     * 選択グループ教科別選択科目数をセットします。
     *
     * @param sGPerSubSubCount 選択グループ教科別選択科目数
     */
    public void setsGPerSubSubCount(int sGPerSubSubCount) {
        this.sGPerSubSubCount = sGPerSubSubCount;
    }

}

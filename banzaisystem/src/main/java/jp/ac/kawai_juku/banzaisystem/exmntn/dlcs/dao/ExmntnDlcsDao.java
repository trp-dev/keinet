package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean.ExmntnDlcsQualifiableCertificationInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean.ExmntnDlcsQualifiableCertificationOutBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

/**
 *
 * 取得資格ダイアログのDAOです。
 *
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnDlcsDao extends BaseDao {

    /**
     * 取得可能資格を検索します。
     *
     * @param searchBean 検索条件Bean
     * @return 検索結果リスト
     */
    public List<ExmntnDlcsQualifiableCertificationOutBean> selectQualifiableCertificationList(
            ExmntnDlcsQualifiableCertificationInBean searchBean) {
        return jdbcManager.selectBySqlFile(ExmntnDlcsQualifiableCertificationOutBean.class, getSqlPath(), searchBean)
                .getResultList();
    }

}

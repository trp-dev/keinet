package jp.ac.kawai_juku.banzaisystem.output.main.component;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * 機能ノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class FunctionNode extends DefaultMutableTreeNode implements Node {

    /**
     * コンストラクタです。
     *
     * @param userObject ノードオブジェクト
     */
    FunctionNode(Object userObject) {
        super(userObject);
    }

}

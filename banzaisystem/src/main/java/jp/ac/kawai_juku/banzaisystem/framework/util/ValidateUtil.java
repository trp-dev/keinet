package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 入力チェックに関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ValidateUtil {

    /** 半角英字のパターン */
    private static final Pattern A_PATTERN = Pattern.compile("^[a-zA-Z]*$");

    /** 半角英数字のパターン */
    private static final Pattern AN_PATTERN = Pattern.compile("^[a-zA-Z0-9]*$");

    /** 整数のパターン */
    private static final Pattern INTEGER_PATTERN = Pattern.compile("^\\-?[0-9]+$");

    /** 実数のパターン */
    private static final Pattern REAL_PATTERN = Pattern.compile("^\\-?[0-9]+\\.?[0-9]*$");

    /** 半角文字のパターン */
    private static final Pattern HAN_PATTERN = Pattern.compile("^[\\x20-\\x7E\\uFF65-\\uFF9F]*$");

    /**
     * コンストラクタです。
     */
    private ValidateUtil() {
    }

    /**
     * 文字列を整数として評価して、指定範囲内であるかをチェックします。
     *
     * @param str 文字列
     * @param min 最小値
     * @param max 最大値
     * @return 整数かつ指定範囲内ならtrue
     */
    public static boolean isInRange(String str, long min, long max) {
        if (!INTEGER_PATTERN.matcher(str).matches() || (min >= 0 && str.charAt(0) == '-')) {
            return false;
        }
        try {
            long value = Long.parseLong(str);
            return value >= min && value <= max;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 文字列を実数として評価して、指定範囲内であるかをチェックします。
     *
     * @param str 文字列
     * @param min 最小値
     * @param max 最大値
     * @return 実数かつ指定範囲内ならtrue
     */
    public static boolean isInRange(String str, double min, double max) {
        if (!REAL_PATTERN.matcher(str).matches() || (min >= 0 && str.charAt(0) == '-')) {
            return false;
        }
        try {
            double value = Double.parseDouble(str);
            return value >= min && value <= max;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 文字列が半角英字のみで構成されているかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return 半角英字のみならtrue
     */
    public static boolean isAlpha(String str) {
        return A_PATTERN.matcher(str).matches();
    }

    /**
     * 文字列が半角英数字のみで構成されているかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return 半角英数字のみならtrue
     */
    public static boolean isAlphaNum(String str) {
        return AN_PATTERN.matcher(str).matches();
    }

    /**
     * 文字列が正の整数（Integer型）に変換できるかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return 変換可能ならtrue
     */
    public static boolean isPositiveInteger(String str) {
        if (StringUtils.isEmpty(str)) {
            return true;
        }
        return isInRange(str, 0, Integer.MAX_VALUE);
    }

    /**
     * 文字列が正の実数（Double型）に変換できるかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return 変換可能ならtrue
     */
    public static boolean isPositiveReal(String str) {
        if (StringUtils.isEmpty(str)) {
            return true;
        }
        return isInRange(str, 0, Double.MAX_VALUE);
    }

    /**
     * 文字列が半角文字（半角英数字記号＋半角カタカナ）のみで構成されているかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return 半角文字のみならtrue
     */
    public static boolean isHankaku(String str) {
        return HAN_PATTERN.matcher(str).matches();
    }

    /**
     * 文字列がひらがなのみで構成されているかをチェックします。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return ひらがなのみならtrue
     */
    public static boolean isHiragana(String str) {
        for (char c : str.toCharArray()) {
            if ((c < 0x3041 || c > 0x3093) && c != 'ー') {
                return false;
            }
        }
        return true;
    }

    /**
     * 文字列が半角カタカナ変換後に氏名として登録できるひらがなのみで構成されているかをチェックします。<br>
     * {@link #isHiragana(String)}との違いは、全半角スペースを許容する点のみです。<br>
     * 空文字の場合はtrueを返すので注意してください。
     *
     * @param str 文字列
     * @return ひらがなのみならtrue
     */
    public static boolean isShimeiHiragana(String str) {
        for (char c : str.toCharArray()) {
            if ((c < 0x3041 || c > 0x3093) && c != 'ー' && c != ' ' && c != '　') {
                return false;
            }
        }
        return true;
    }

}

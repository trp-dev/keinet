package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.output.main.maker.OutputMainB0401Maker;

/**
 *
 * 志望大学・評価一覧のノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class B0401Node extends OutputCsvNode {

    /**
     * コンストラクタです。
     */
    B0401Node() {
        super("志望大学・評価一覧");
    }

    @Override
    void output(OutputMainHelper helper) {
        new ExcelLauncher(GamenId.D).addCreator(OutputMainB0401Maker.class, helper.createB0401CsvInBean()).launch();
    }

}

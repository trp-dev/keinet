package jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;

/**
 *
 * 大学検索画面の検索結果Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsSearchResultBean extends JudgementResultBean {

    /** 大学カナ名 */
    private String univNameKana;

    /** 大学名 */
    private String univName;

    /** 学部名  */
    private String facultyName;

    /** 学科名  */
    private String deptName;

    /** 県コード */
    private String prefCd;

    /** 県名 */
    private String prefName;

    /** 入試日 */
    private String entExamInpleDate;

    /** 出願締切日 */
    private String entExamApplideadLine;

    /** 入試日程詳細フラグ */
    private String examScheduleDetailFlag;

    /** 注釈文フラグ */
    private String univCommentFlag;

    /**
     * 県名を返します。
     *
     * @return 県名
     */
    public String getPrefName() {
        return prefName;
    }

    /**
     * 県名をセットします。
     *
     * @param prefName 県名
     */
    public void setPrefName(String prefName) {
        this.prefName = prefName;
    }

    /**
     * 入試日を返します。
     *
     * @return 入試日
     */
    public String getEntExamInpleDate() {
        return entExamInpleDate;
    }

    /**
     * 入試日をセットします。
     *
     * @param entExamDate 入試日
     */
    public void setEntExamInpleDate(String entExamDate) {
        this.entExamInpleDate = entExamDate;
    }

    /**
     * 出願締切日を返します。
     *
     * @return 出願締切日
     */
    public String getEntExamApplideadLine() {
        return entExamApplideadLine;
    }

    /**
     * 出願締切日をセットします。
     *
     * @param entExamApplideadLine 出願締切日
     */
    public void setEntExamApplideadLine(String entExamApplideadLine) {
        this.entExamApplideadLine = entExamApplideadLine;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 県コードを返します。
     *
     * @return 県コード
     */
    public String getPrefCd() {
        return prefCd;
    }

    /**
     * 県コードをセットします。
     *
     * @param prefCd 県コード
     */
    public void setPrefCd(String prefCd) {
        this.prefCd = prefCd;
    }

    /**
     * 大学カナ名を返します。
     *
     * @return 大学カナ名
     */
    public String getUnivNameKana() {
        return univNameKana;
    }

    /**
     * 大学カナ名をセットします。
     *
     * @param univNameKana 大学カナ名
     */
    public void setUnivNameKana(String univNameKana) {
        this.univNameKana = univNameKana;
    }

    /**
     * 入試日程詳細フラグを返します。
     *
     * @return 入試日程詳細フラグ
     */
    public String getExamScheduleDetailFlag() {
        return examScheduleDetailFlag;
    }

    /**
     * 入試日程詳細フラグをセットします。
     *
     * @param examScheduleDetailFlag 入試日程詳細フラグ
     */
    public void setExamScheduleDetailFlag(String examScheduleDetailFlag) {
        this.examScheduleDetailFlag = examScheduleDetailFlag;
    }

    /**
     * 注釈文フラグを返します。
     *
     * @return 注釈文フラグ
     */
    public String getUnivCommentFlag() {
        return univCommentFlag;
    }

    /**
     * 注釈文フラグをセットします。
     *
     * @param univCommentFlag 注釈文フラグ
     */
    public void setUnivCommentFlag(String univCommentFlag) {
        this.univCommentFlag = univCommentFlag;
    }

}

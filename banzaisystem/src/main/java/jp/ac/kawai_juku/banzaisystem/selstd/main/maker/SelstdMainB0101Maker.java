package jp.ac.kawai_juku.banzaisystem.selstd.main.maker;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.ANSWER_NO_1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHINESE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ENGLISH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_LITERATURE_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_OLD;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_SOCIAL;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ENGLISH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CsvId;
import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.ScoreBeanDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.maker.BaseCsvMaker;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101Bean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101CandidateBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainB0101SubjectBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCsvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.dao.SelstdMainCsvDao;
import jp.co.fj.kawaijuku.judgement.beans.score.EngSSScoreData;
import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.extension.jdbc.IterationCallback;
import org.seasar.extension.jdbc.IterationContext;
import org.seasar.framework.util.PropertiesUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * CSV帳票メーカー(個人成績(志望大学変更後))です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainB0101Maker extends BaseCsvMaker<SelstdMainCsvInBean> {

    /** ヘッダ科目名定義 */
    private static final Map<String, String[]> SUBNAME_MAP;
    static {
        Map<String, String[]> map = CollectionsUtil.newHashMap();
        Properties props = new Properties();
        PropertiesUtil.load(props, SelstdMainB0101Maker.class.getResourceAsStream("head3.properties"));
        addSubName(map, props, Exam.Code.MARK1, "HEADNAME1");
        addSubName(map, props, Exam.Code.MARK2, "HEADNAME1");
        addSubName(map, props, Exam.Code.MARK3, "HEADNAME1");
        addSubName(map, props, Exam.Code.CENTERPRE, "HEADNAME1");
        addSubName(map, props, Exam.Code.CENTERRESEARCH, "HEADNAME1");
        addSubName(map, props, Exam.Code.SECONDGRADE_MARK, "HEADNAME2");
        addSubName(map, props, Exam.Code.WRTN1, "HEADNAME3");
        addSubName(map, props, Exam.Code.WRTN2, "HEADNAME3");
        addSubName(map, props, Exam.Code.WRTN3, "HEADNAME3");
        addSubName(map, props, Exam.Code.WRTN_2GRADE, "HEADNAME12");
        SUBNAME_MAP = Collections.unmodifiableMap(map);
    }

    /** 科目位置定義 */
    private static final Map<String, SortedMap<Integer, String[]>> SUBPOSITION_MAP;
    static {
        Map<String, SortedMap<Integer, String[]>> map = CollectionsUtil.newHashMap();
        addSubPosition(map, Exam.Code.MARK1, "v3_subjectmaster01.properties");
        addSubPosition(map, Exam.Code.MARK2, "v3_subjectmaster01.properties");
        addSubPosition(map, Exam.Code.MARK3, "v3_subjectmaster01.properties");
        addSubPosition(map, Exam.Code.CENTERPRE, "v3_subjectmaster01.properties");
        addSubPosition(map, Exam.Code.CENTERRESEARCH, "v3_subjectmaster19.properties");
        addSubPosition(map, Exam.Code.SECONDGRADE_MARK, "v3_subjectmaster01.properties");
        addSubPosition(map, Exam.Code.WRTN1, "v3_subjectmaster02.properties");
        addSubPosition(map, Exam.Code.WRTN2, "v3_subjectmaster02.properties");
        addSubPosition(map, Exam.Code.WRTN3, "v3_subjectmaster02.properties");
        addSubPosition(map, Exam.Code.WRTN_2GRADE, "v3_subjectmaster22.properties");
        SUBPOSITION_MAP = Collections.unmodifiableMap(map);
    }

    /** マーク模試で換算得点ではなく配点を設定する科目位置 */
    private static final Set<Integer> HAITEN_SET;
    static {
        HAITEN_SET = CollectionsUtil.newHashSet();
        /* 数学12 */
        HAITEN_SET.add(5);
        /* 総合1 */
        HAITEN_SET.add(24);
        /* 総合2 */
        HAITEN_SET.add(25);
        /* 総合3 */
        HAITEN_SET.add(26);
    }

    /** 模試科目マスタに存在しないマーク模試国語の科目名定義 */
    private static final Map<String, String> MARK_JAP_SUBNAME_MAP = CollectionsUtil.newHashMap();
    static {
        MARK_JAP_SUBNAME_MAP.put(SUBCODE_CENTER_DEFAULT_LITERATURE, "現代文");
        MARK_JAP_SUBNAME_MAP.put(SUBCODE_CENTER_DEFAULT_LITERATURE_OLD, "現・古");
    }

    /** マーク模試で評価得点を出力する日程コード定義 */
    private static final String[] SCORE_SCHEDULE_CD = { Univ.SCHEDULE_MID, Univ.SCHEDULE_BFR, Univ.SCHEDULE_AFT, Univ.SCHEDULE_NEW, Univ.SCHEDULE_CNT };

    /** 学校コード */
    private static final String SCHOOL_CD = "99999";

    /** 科目レコード数 */
    private static final int SUBJECT_NUM = SUBNAME_MAP.values().iterator().next().length;

    /** 志望大学レコード数 */
    private static final int CANDIDATE_NUM = 9;

    /** DAO */
    @Resource
    private SelstdMainCsvDao selstdMainCsvDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** DAO */
    @Resource
    private ScoreBeanDao scoreBeanDao;

    /**
     * ヘッダ科目名定義を読み込んでマップに追加します。
     *
     * @param map マップ
     * @param props プロパティ情報
     * @param examCd 模試コード
     * @param key リソースキー
     */
    private static void addSubName(Map<String, String[]> map, Properties props, String examCd, String key) {
        map.put(examCd, StringUtils.split(props.getProperty(key), ','));
    }

    /**
     * 科目位置定義を読み込んでマップに追加します。
     *
     * @param map マップ
     * @param examCd 模試コード
     * @param filename リソースファイル名
     */
    private static void addSubPosition(Map<String, SortedMap<Integer, String[]>> map, String examCd, String filename) {
        SortedMap<Integer, String[]> positionMap = CollectionsUtil.newTreeMap();
        Properties props = new Properties();
        PropertiesUtil.load(props, SelstdMainB0101Maker.class.getResourceAsStream(filename));
        for (Object obj : props.keySet()) {
            String key = (String) obj;
            Integer index = Integer.parseInt(key.substring(4));
            List<String> list = CollectionsUtil.newArrayList();
            for (String subCd : StringUtils.split(props.getProperty(key), ',')) {
                if (!subCd.isEmpty()) {
                    list.add(subCd);
                }
            }
            positionMap.put(index, list.toArray(new String[list.size()]));
        }
        map.put(examCd, positionMap);
    }

    @Override
    public PrintId getPrintId() {
        return CsvId.B01_01;
    }

    @Override
    protected void outputData(SelstdMainCsvInBean inBean) {
        if (inBean.getExam() != null && !inBean.getIdList().isEmpty()) {
            selstdMainCsvDao.selectB0101List(inBean.getExam(), inBean.getIdList(), new Callback(inBean));
        }
    }

    /** レコード1件を処理するコールバッククラス */
    private final class Callback implements IterationCallback<SelstdMainB0101Bean, Object> {

        /** 入力Bean */
        private final SelstdMainCsvInBean inBean;

        /** 1行目フラグ */
        private boolean isFirst = true;

        /** 科目位置定義 */
        private final SortedMap<Integer, String[]> positionMap;

        /** 年度 */
        private final String year;

        /**
         * コンストラクタです。
         *
         * @param inBean 入力Bean
         */
        private Callback(SelstdMainCsvInBean inBean) {
            this.inBean = inBean;
            this.positionMap = SUBPOSITION_MAP.get(inBean.getExam().getExamCd());
            year = inBean.getExam().getExamYear();
        }

        @Override
        public Object iterate(SelstdMainB0101Bean bean, IterationContext context) {

            /* 1行目はヘッダを出力する */
            if (isFirst) {
                addColumns(createHeader(inBean.getExam()));
                addRow();
                isFirst = false;
            }

            /* 対象模試の科目成績リストを取得する */
            List<SelstdMainB0101SubjectBean> recordList = selstdMainCsvDao.selectSubjectRecordList(inBean.getExam(), bean.getIndividualId());

            /* 連番 */
            addColumn(bean.getIndividualId().toString());

            /* 年度 */
            addColumn(year);

            /* 模試コード */
            addColumn(inBean.getExam().getExamCd());

            /* 学校コード */
            addColumn(SCHOOL_CD);

            /* 学年 */
            addColumn(bean.getGrade());

            /* クラス */
            addColumn(bean.getCls());

            /* クラス番号 */
            addColumn(bean.getClassNo());

            /* カナ氏名 */
            addColumn(bean.getNameKana());

            /* 受験型 */
            addColumn(bean.getExamType());

            /* 文理コード */
            addColumn(bean.getBunriCode());

            /* 科目レコード */
            addSubjectRecord(recordList);

            /* 科目備考レコード */
            addSubRemarkRecord(recordList);

            /* 志望大学レコード */
            addCandidateRecord(bean.getIndividualId(), recordList);

            /* 1行出力する */
            addRow();

            return null;
        }

        /**
         * 科目レコードを追加します。
         *
         * @param recordList 科目成績リスト
         */
        private void addSubjectRecord(List<SelstdMainB0101SubjectBean> recordList) {

            boolean isMark = ExamUtil.isMark(inBean.getExam());
            boolean isResearch = ExamUtil.isCenter(inBean.getExam());

            for (Entry<Integer, String[]> entry : positionMap.entrySet()) {
                /* 指定位置に定義された科目成績を特定する */
                SelstdMainB0101SubjectBean bean = findRecord(recordList, entry.getValue());
                /* 科目コード */
                addColumn(bean == null ? null : bean.getSubCd());
                /* 科目名 */
                addColumn(bean == null ? null : StringUtils.defaultString(bean.getSubName(), MARK_JAP_SUBNAME_MAP.get(bean.getSubCd())));
                /* 配点・換算得点 */
                if (bean == null) {
                    addColumn(null);
                } else if (isMark && !isResearch && !HAITEN_SET.contains(entry.getKey())) {
                    addColumn(bean.getCvsScore());
                } else {
                    addColumn(bean.getSubjectAllotPnt());
                }
                /* 得点 */
                addColumn(bean == null ? null : bean.getScore());
                /* 偏差値 */
                addColumn(bean == null ? null : bean.getCDeviation());
                /* 学力レベル */
                addColumn(null);
            }
        }

        /**
         * 科目成績リストから指定した科目コードの成績を抽出します。
         *
         * @param recordList 科目成績リスト
         * @param subCdArray 科目コード配列
         * @return 抽出結果
         */
        private SelstdMainB0101SubjectBean findRecord(List<SelstdMainB0101SubjectBean> recordList, String[] subCdArray) {
            if (!ArrayUtils.isEmpty(subCdArray)) {
                for (SelstdMainB0101SubjectBean record : recordList) {
                    for (String subCd : subCdArray) {
                        if (record.getSubCd().startsWith(subCd)) {
                            return record;
                        }
                    }
                }
            }
            return null;
        }

        /**
         * 科目備考レコードを追加します。
         *
         * @param recordList 科目成績リスト
         */
        private void addSubRemarkRecord(List<SelstdMainB0101SubjectBean> recordList) {
            if (ExamUtil.isMark(inBean.getExam())) {
                /* マーク模試 */
                /* 英語+L偏差値 */
                addColumn(findDeviation(recordList, SUBCODE_CENTER_DEFAULT_ENGLISH));
                /* 現文換算 */
                addColumn(findCvsScore(recordList, SUBCODE_CENTER_DEFAULT_LITERATURE));
                /* 古文換算 */
                addColumn(findCvsScore(recordList, SUBCODE_CENTER_DEFAULT_OLD));
                /* 漢文換算 */
                addColumn(findCvsScore(recordList, SUBCODE_CENTER_DEFAULT_CHINESE));
                /* 第１解答理科 */
                addColumn(findSciFirstAnswerFlg(recordList));
                /* 第１解答地公 */
                addColumn(findSocFirstAnswerFlg(recordList));
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
            } else if (ExamUtil.is2ndWrtn(inBean.getExam())) {
                /* 高２記述 */
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 英リス選択 */
                addColumn(findScope(recordList, SUBCODE_SECOND_DEFAULT_ENGLISH));
                /* 物理選択 */
                addColumn("9");
                /* 化学選択 */
                addColumn("9");
                /* 生物選択 */
                addColumn("9");
                /* 地学選択 */
                addColumn("9");
            } else {
                /* ＃１〜＃３記述 */
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 未使用 */
                addColumn(null);
                /* 英リス選択 */
                addColumn(findScope(recordList, SUBCODE_SECOND_DEFAULT_ENGLISH));
                /* 物理選択 */
                addColumn("9");
                /* 化学選択 */
                addColumn("9");
                /* 生物選択 */
                addColumn("9");
                /* 地学選択 */
                addColumn("9");
            }
        }

        /**
         * 指定した科目コードの偏差値を返します。
         *
         * @param recordList 科目成績リスト
         * @param subCd 科目コード
         * @return 偏差値
         */
        private String findDeviation(List<SelstdMainB0101SubjectBean> recordList, String subCd) {
            for (SelstdMainB0101SubjectBean bean : recordList) {
                if (bean.getSubCd().equals(subCd)) {
                    return bean.getCDeviation();
                }
            }
            return null;
        }

        /**
         * 指定した科目コードの得点／換算得点を返します。
         *
         * @param recordList 科目成績リスト
         * @param subCd 科目コード
         * @return 得点／換算得点
         */
        private String findCvsScore(List<SelstdMainB0101SubjectBean> recordList, String subCd) {
            for (SelstdMainB0101SubjectBean bean : recordList) {
                if (bean.getSubCd().equals(subCd)) {
                    return ExamUtil.isCenter(inBean.getExam()) ? bean.getScore() : bean.getCvsScore();
                }
            }
            return null;
        }

        /**
         * 理科の第１解答フラグを返します。
         *
         * @param recordList 科目成績リスト
         * @return 第１解答フラグ
         */
        private String findSciFirstAnswerFlg(List<SelstdMainB0101SubjectBean> recordList) {
            String subCd = findFirstAnswerSubCd(recordList, "4");
            if (subCd == null) {
                return null;
            } else
                switch (subCd) {
                    case SUBCODE_CENTER_DEFAULT_PHYSICS:
                        /* 物理 */
                        return "0";
                    case SUBCODE_CENTER_DEFAULT_CHEMISTRY:
                        /* 化学 */
                        return "1";
                    case SUBCODE_CENTER_DEFAULT_BIOLOGY:
                        /* 生物 */
                        return "2";
                    case SUBCODE_CENTER_DEFAULT_EARTH:
                        /* 地学 */
                        return "3";
                    default:
                        return null;
                }
        }

        /**
         * 社会の第１解答フラグを返します。
         *
         * @param recordList 科目成績リスト
         * @return 第１解答フラグ
         */
        private String findSocFirstAnswerFlg(List<SelstdMainB0101SubjectBean> recordList) {
            String subCd = findFirstAnswerSubCd(recordList, "5");
            if (subCd == null) {
                return null;
            } else
                switch (subCd) {
                    case SUBCODE_CENTER_DEFAULT_WHISTORYB:
                        /* 世界史B */
                        return "0";
                    case SUBCODE_CENTER_DEFAULT_JHISTORYB:
                        /* 日本史B */
                        return "1";
                    case SUBCODE_CENTER_DEFAULT_GEOGRAPHYB:
                        /* 地理B */
                        return "2";
                    case SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS:
                        /* 倫政 */
                        return "3";
                    case SUBCODE_CENTER_DEFAULT_POLITICS:
                        /* 政治経済 */
                        return "4";
                    case SUBCODE_CENTER_DEFAULT_SOCIAL:
                        /* 現代社会 */
                        return "5";
                    case SUBCODE_CENTER_DEFAULT_ETHICS:
                        /* 倫理 */
                        return "6";
                    case SUBCODE_CENTER_DEFAULT_WHISTORYA:
                        /* 世界史A */
                        return "7";
                    case SUBCODE_CENTER_DEFAULT_JHISTORYA:
                        /* 日本史A */
                        return "8";
                    case SUBCODE_CENTER_DEFAULT_GEOGRAPHYA:
                        /* 地理A */
                        return "9";
                    default:
                        return null;
                }
        }

        /**
         * 指定した教科の第１解答科目の科目コードを返します。
         *
         * @param recordList 科目成績リスト
         * @param courseCd 教科コード（理科は4、社会は5）
         * @return 第１解答科目の科目コード
         */
        private String findFirstAnswerSubCd(List<SelstdMainB0101SubjectBean> recordList, String courseCd) {
            for (SelstdMainB0101SubjectBean bean : recordList) {
                if (bean.getSubCd().startsWith(courseCd) && bean.getScope() != null && Integer.parseInt(bean.getScope()) == ANSWER_NO_1) {
                    return bean.getSubCd();
                }
            }
            return null;
        }

        /**
         * 指定した科目コードの範囲区分を返します。
         *
         * @param recordList 科目成績リスト
         * @param subCd 科目コード
         * @return 範囲区分
         */
        private String findScope(List<SelstdMainB0101SubjectBean> recordList, String... subCd) {
            for (SelstdMainB0101SubjectBean bean : recordList) {
                if (ArrayUtils.contains(subCd, bean.getSubCd())) {
                    return StringUtils.defaultString(bean.getScope(), "9");
                }
            }
            return "9";
        }

        /**
         * 志望大学レコードを追加します。
         *
         * @param id 個人ID
         * @param recordList 科目成績リスト
         */
        private void addCandidateRecord(Integer id, List<? extends SubjectRecord> recordList) {
            ExamBean dockingExam = null;
            /* 志望大学リストを取得する */
            List<SelstdMainB0101CandidateBean> list = selstdMainCsvDao.selectCandidateRecordList(inBean.getExam(), id);
            if (list.size() > CANDIDATE_NUM) {
                /* レコード数の上限を超えていたらリサイズする */
                list = list.subList(0, CANDIDATE_NUM);
            }

            /* ドッキング先模試の科目成績リストを取得する */
            List<SelstdMainB0101SubjectBean> dockingRecordList = null;
            dockingExam = ExamUtil.findDockingExamStudentB(inBean.getExam(), id, scoreBeanDao, systemDto);
            if (dockingExam != null) {
                dockingRecordList = selstdMainCsvDao.selectSubjectRecordList(dockingExam, id);
            }

            /* 判定用の成績データを初期化する */
            ScoreBean score;

            /* 英語認定試験１ */
            List<EngSSScoreData> engList = new ArrayList<EngSSScoreData>();
            EngSSScoreData engData = new EngSSScoreData();
            engData.setEngSSCode(StringUtils.EMPTY);
            engData.setEngSSLevelCode(StringUtils.EMPTY);
            engData.setEngScore(0.0);
            engList.add(engData);
            /* 英語認定試験２ */
            engData = new EngSSScoreData();
            engData.setEngSSCode(StringUtils.EMPTY);
            engData.setEngSSLevelCode(StringUtils.EMPTY);
            engData.setEngScore(0.0);
            engList.add(engData);

            if (ExamUtil.isMark(inBean.getExam())) {
                /* マーク模試 */
                score = new ScoreBean(inBean.getExam(), recordList, dockingExam, dockingRecordList, engList);
            } else {
                /* 記述模試 */
                score = new ScoreBean(dockingExam, dockingRecordList, inBean.getExam(), recordList, engList);
            }

            /* 判定を実行する */
            BZJudgementExecutor.getInstance().judge(score, list);

            /* マーク模試フラグを初期化する */
            boolean isMark = ExamUtil.isMark(inBean.getExam());

            for (SelstdMainB0101CandidateBean bean : list) {
                /* 志望校コード5桁 */
                addColumn(bean.getChoiceCd5());
                /* 志望校コード10桁 */
                addColumn(bean.getUnivKey());
                /* 志望大学名 */
                addColumn(createCandidateUnivName(bean));
                if (isMark && ArrayUtils.contains(SCORE_SCHEDULE_CD, bean.getScheduleCd())) {
                    /* 評価得点 */
                    addColumn(ObjectUtils.toString(bean.getcScore()));
                } else {
                    /* 評価偏差値 */
                    addColumn(ObjectUtils.toString(bean.getsScore()));
                }
                /* 共通テスト評価 */
                addColumn(StringUtils.trim(bean.getCenterRating()));
                /* 二次評価 */
                addColumn(StringUtils.trim(bean.getSecondRating()));
                /* 総合評価ポイント */
                addColumn(StringUtils.trim(bean.getTotalRatingPoint()));
                /* 総合評価 */
                addColumn(StringUtils.trim(bean.getTotalRating()));
            }
        }

    }

    /**
     * 志望大学名を生成します。
     *
     * @param bean {@link SelstdMainB0101CandidateBean}
     * @return 志望大学名
     */
    private String createCandidateUnivName(SelstdMainB0101CandidateBean bean) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtils.rightPad(StringUtils.defaultString(bean.getUnivName()), 7, '　'));
        sb.append(StringUtils.rightPad(StringUtils.defaultString(bean.getFacultyName()), 5, '　'));
        sb.append(StringUtils.rightPad(StringUtils.defaultString(bean.getDeptName()), 6, '　'));
        return sb.toString();
    }

    /**
     * CSV項目ヘッダを生成します。
     *
     * @param exam 対象模試
     * @return CSV項目ヘッダ
     */
    private List<String> createHeader(ExamBean exam) {

        boolean isMark = ExamUtil.isMark(exam);
        boolean isCenter = ExamUtil.isCenter(exam);
        boolean isAns1st = ExamUtil.isAns1st(exam);
        String[] subName = SUBNAME_MAP.get(exam.getExamCd());

        List<String> list = CollectionsUtil.newArrayList(300);

        list.add("通番");
        list.add("年度");
        list.add("模試コード");
        list.add("学校コード");
        list.add("学年");
        list.add("クラス");
        list.add("クラス番号");
        list.add("カナ氏名");
        list.add("受験型");
        list.add("文理コード");

        for (int i = 1; i <= SUBJECT_NUM; i++) {
            String num = StringUtils.leftPad(Integer.toString(i), 2, '0');
            list.add("科目コード" + num);
            list.add(subName[i - 1]);
            list.add(num + (isMark && !isCenter && !HAITEN_SET.contains(i) ? "換算点" : "配点"));
            list.add(num + "得点");
            list.add(num + "偏差値");
            list.add(num + "レベル");
        }

        if (isMark) {
            list.add("英語+L偏差値");
            list.add(isCenter ? "現文得点" : "現文換算点");
            list.add(isCenter ? "古文得点" : "古文換算点");
            list.add(isCenter ? "漢文得点" : "漢文換算点");
            list.add(isAns1st ? "第１解答理科" : "未使用A");
            list.add(isAns1st ? "第１解答地公" : "未使用B");
            list.add(isAns1st ? "未使用A" : "未使用C");
            list.add(isAns1st ? "未使用B" : "未使用D");
            list.add(isAns1st ? "未使用C" : "未使用E");
        } else {
            list.add("未使用A");
            list.add("未使用B");
            list.add("未使用C");
            list.add("未使用D");
            list.add("英リス選択");
            list.add("未使用F");
            list.add("未使用G");
            list.add("未使用H");
            list.add("未使用I");
        }

        for (int i = 1; i <= CANDIDATE_NUM; i++) {
            list.add("志望大" + i + "コード5桁");
            list.add("志望大" + i + "コード10桁");
            list.add("志望大" + i + "名称");
            list.add("志望大" + i + "評価成績");
            list.add("志望大" + i + "テ評価");
            list.add("志望大" + i + "二評価");
            list.add("志望大" + i + "総評ポ");
            list.add("志望大" + i + "総評価");
        }

        return list;
    }

}

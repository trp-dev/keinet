package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;


/**
 *
 * 個別試験の基礎科目チェックボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SecondBasicCheckBox extends SecondCheckBox {

    /** 対応する専門科目のチェックボックス */
    private SecondCheckBox specialCheckBox;

    @Override
    public void setEnabled(boolean b) {
        if (b && !specialCheckBox.isSelected()) {
            /* 専門科目が未チェックなら有効化しない */
            return;
        }
        super.setEnabled(b);
    }

    /**
     * 対応する専門科目のチェックボックスを設定します。
     *
     * @param specialCheckBox 対応する専門科目のチェックボックス
     */
    public void setSpecialCheckBox(SecondCheckBox specialCheckBox) {
        this.specialCheckBox = specialCheckBox;
    }

}

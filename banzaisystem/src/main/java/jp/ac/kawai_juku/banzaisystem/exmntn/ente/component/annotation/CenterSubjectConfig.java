package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;

/**
 *
 * センター科目を設定するアノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface CenterSubjectConfig {

    /**
     * センター科目を設定します。
     *
     * @return センター科目
     */
    CenterSubject value();

}

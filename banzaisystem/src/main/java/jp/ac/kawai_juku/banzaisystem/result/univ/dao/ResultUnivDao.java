package jp.ac.kawai_juku.banzaisystem.result.univ.dao;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivBean;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivExamineeBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 入試結果入力-大学指定(一覧)画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultUnivDao extends BaseDao {

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 指定した大学名の大学リストを取得します。
     *
     * @param univName 大学名
     * @return 大学リスト
     */
    public List<ResultUnivBean> selectUnivList(String univName) {
        return jdbcManager.selectBySqlFile(ResultUnivBean.class, getSqlPath(), univName).getResultList();
    }

    /**
     * 受験者リストを取得します。
     *
     * @param univCdList 大学コードリスト
     * @param sucAnnDate 合格発表日
     * @param grade 学年
     * @param cls クラス
     * @return 受験者リスト
     */
    public List<ResultUnivExamineeBean> selectExamineeList(List<String> univCdList, String sucAnnDate, Integer grade,
            String cls) {
        BeanMap param = new BeanMap();
        param.put("year", systemDto.getUnivYear());
        param.put("univCdCondition", DaoUtil.createIn("P.UNIVCD", univCdList));
        param.put("sucAnnDate", sucAnnDate);
        param.put("grade", grade);
        param.put("class", cls);
        return jdbcManager.selectBySqlFile(ResultUnivExamineeBean.class, getSqlPath(), param).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.controller;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.helper.ExmntnPrntHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.maker.ExmntnPrntA0501Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.view.ExmntnPrntView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.controller.ExmntnRsltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.helper.ExmntnRsltHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.maker.ExmntnRsltA0401Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.helper.ExmntnScdlHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301StudentInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper.ExmntnUnivHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0201Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.maker.ExmntnUnivA0301Maker;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0104Maker;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 一括出力画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public class ExmntnPrntController extends SubController {

    /** View */
    @Resource(name = "exmntnPrntView")
    private ExmntnPrntView view;

    /** 志望大学詳細画面のView */
    @Resource
    private ExmntnUnivView exmntnUnivView;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** Helper */
    @Resource
    private ExmntnPrntHelper exmntnPrntHelper;

    /** 志望大学詳細画面のHelper */
    @Resource
    private ExmntnUnivHelper exmntnUnivHelper;

    /** 検索結果画面のHelper */
    @Resource
    private ExmntnRsltHelper exmntnRsltHelper;

    /** 受験スケジュール画面のHelper */
    @Resource
    private ExmntnScdlHelper exmntnScdlHelper;

    /** 個人成績表画面のHelper */
    @Resource
    private ExmntnMainScoreHelper exmntnMainScoreHelper;

    /** 志望大学個人成績画面のDTO */
    @Resource
    private ExmntnMainDto exmntnMainDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    @Override
    @Logging(GamenId.C6)
    public void activatedAction() {

        /* 志望順位コンボボックスを初期化する */
        exmntnPrntHelper.initCandidateRankComboBox();

        /* 「印刷する」ボタンの状態を初期化する */
        initPrintButtonStatus();

        /* 検索結果一覧チェックボックスの状態を初期化する */
        view.resultListCheckBox.setEnabled(exmntnMainView.tabbedPane.isTabEnabled(ExmntnRsltController.class));

        /* 連続個人成績表チェックボックスの状態を初期化する(生徒の場合、チェック不可) */
        view.exmntnStudentCheckBox.setEnabled(systemDto.isTeacher());
    }

    /**
     * 簡易個人表チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvPaperCheckBoxAction() {
        boolean b = view.indvPaperCheckBox.isSelected();
        view.indvPaper1CheckBox.setSelectedSilent(b);
        view.indvPaper2CheckBox.setSelectedSilent(b);
        initPrintButtonStatus();
        return null;
    }

    /**
     * 簡易個人表1面チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvPaper1CheckBoxAction() {
        if (!view.indvPaper1CheckBox.isSelected() && !view.indvPaper2CheckBox.isSelected()) {
            view.indvPaperCheckBox.setSelectedSilent(false);
        } else {
            view.indvPaperCheckBox.setSelectedSilent(true);
        }
        initPrintButtonStatus();
        return null;
    }

    /**
     * 簡易個人表2面チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvPaper2CheckBoxAction() {
        indvPaper1CheckBoxAction();
        return null;
    }

    /**
     * 志望大学リストチェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult candidateUnivListCheckBoxAction() {
        initPrintButtonStatus();
        return null;
    }

    /**
     * 志望大学詳細チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult candidateUnivDetailCheckBoxAction() {
        initPrintButtonStatus();
        return null;
    }

    /**
     * 検索結果一覧チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult resultListCheckBoxAction() {
        initPrintButtonStatus();
        return null;
    }

    /**
     * 連続個人成績表チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exmntnStudentCheckBoxAction() {
        initPrintButtonStatus();
        return null;
    }

    /**
     * 「印刷する」ボタンの状態を初期化します。
     */
    private void initPrintButtonStatus() {
        for (CodeCheckBox c : view.findComponentsByClass(CodeCheckBox.class)) {
            if (c.isSelected() && c.isEnabled()) {
                view.printButton.setEnabled(true);
                return;
            }
        }
        view.printButton.setEnabled(false);
    }

    /**
     * 印刷するボタン押下アクションです。
     *
     * @return 印刷タスク
     */
    @Execute
    public ExecuteResult printButtonAction() {
        return new TaskResult() {
            @Override
            protected ExecuteResult doTask() {
                ExcelLauncher launcher = new ExcelLauncher(GamenId.C6);
                addSimpleIndividualSheet(launcher);
                addCandidateUnivList(launcher);
                addUnivDetail(launcher);
                addSearchResult(launcher);
                addExamStudent(launcher);
                launcher.launch();
                return null;
            }
        };
    }

    /**
     * 簡易個人表をExcelLauncherに追加します。
     *
     * @param launcher {@link ExcelLauncher}
     */
    private void addSimpleIndividualSheet(ExcelLauncher launcher) {
        if (view.indvPaperCheckBox.isSelected()) {
            launcher.addCreator(ExmntnUnivA0301Maker.class, exmntnUnivHelper.createA0301InBean(view.indvPaper1CheckBox.isSelected(), view.indvPaper2CheckBox.isSelected()));
        }
    }

    /**
     * 志望大学リストをExcelLauncherに追加します。
     *
     * @param launcher {@link ExcelLauncher}
     */
    private void addCandidateUnivList(ExcelLauncher launcher) {
        if (view.candidateUnivListCheckBox.isSelected()) {
            launcher.addCreator(ExmntnPrntA0501Maker.class, new ExmntnUnivA0301StudentInBean(exmntnUnivHelper.createStudentBean(), exmntnMainDto.getJudgeScore(), exmntnUnivHelper.createCandidateUnivList(), false));
        }
    }

    /**
     * 志望大学詳細をExcelLauncherに追加します。
     *
     * @param launcher {@link ExcelLauncher}
     */
    private void addUnivDetail(ExcelLauncher launcher) {
        if (view.candidateUnivDetailCheckBox.isSelected() && view.univComboBox.getItemCount() > 0) {
            int max = view.univComboBox.getSelectedValue().intValue();
            List<Pair<String, ExmntnMainCandidateUnivBean>> list = CollectionsUtil.newArrayList(max);
            for (int i = 0; i < exmntnUnivView.candidateRankComboBox.getItemCount(); i++) {
                ComboBoxItem<ExmntnMainCandidateUnivBean> item = exmntnUnivView.candidateRankComboBox.getItemAt(i);
                if (!item.getLabel().isEmpty()) {
                    list.add(Pair.of(item.getLabel(), item.getValue()));
                    if (list.size() == max) {
                        break;
                    }
                }
            }
            launcher.addCreator(ExmntnUnivA0201Maker.class, exmntnUnivHelper.createA0201InBean(list));
        }
    }

    /**
     * 大学検索結果をExcelLauncherに追加します。
     *
     * @param launcher {@link ExcelLauncher}
     */
    private void addSearchResult(ExcelLauncher launcher) {
        if (view.resultListCheckBox.isSelected() && view.resultListCheckBox.isEnabled()) {
            launcher.addCreator(ExmntnRsltA0401Maker.class, exmntnRsltHelper.createInBean());
        }
    }

    /**
     * 連続個人成績表をExcelLauncherに追加します。
     *
     * @param launcher {@link ExcelLauncher}
     */
    private void addExamStudent(ExcelLauncher launcher) {
        if (view.exmntnStudentCheckBox.isSelected() && view.exmntnStudentCheckBox.isEnabled()) {
            ExamBean dockExam = null;
            SelstdDprtExcelMakerInBean inBean = null;
            ExmntnUnivStudentBean studentBean = exmntnUnivHelper.createStudentBean();
            List<Integer> idList = CollectionsUtil.newArrayList();

            /* 生徒ID設定 */
            idList.add(studentBean.getIndividualId());
            /* ドッキング模試対象選択 */
            if (exmntnMainScoreHelper.getMarkExam() != null && exmntnMainScoreHelper.getTargetExam().getExamCd().equals(exmntnMainScoreHelper.getMarkExam().getExamCd())) {
                dockExam = exmntnMainScoreHelper.getWrittenExam();
            } else {
                dockExam = exmntnMainScoreHelper.getMarkExam();
            }
            inBean = new SelstdDprtExcelMakerInBean(exmntnMainScoreHelper.getTargetExam(), dockExam, idList);
            if (view.exmntnStudentCheckBox.isSelected() && view.exmntnStudentCheckBox.isEnabled()) {
                launcher.addCreator(SelstdDprtA0104Maker.class, inBean);
            }
        }
    }

}

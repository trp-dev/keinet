package jp.ac.kawai_juku.banzaisystem.mainte.stng.helper;

import jp.ac.kawai_juku.banzaisystem.DataFolder;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngFolderField;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngMoveButton;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngRefButton;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

/**
 *
 * メンテナンス-環境設定画面のHelperです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Component(instance = InstanceType.OUTER)
public final class MainteStngHelper {

    /** View */
    private final AbstractView view;

    /** データフォルダ */
    private final DataFolder folder;

    /**
     * コンストラクタです。
     *
     * @param view View
     * @param folder データフォルダ
     */
    public MainteStngHelper(AbstractView view, DataFolder folder) {
        this.view = view;
        this.folder = folder;
    }

    /**
     * フォルダフィールドを返します。
     *
     * @return {@link MainteStngFolderField}
     */
    public MainteStngFolderField findFolderField() {
        for (MainteStngFolderField f : view.findComponentsByClass(MainteStngFolderField.class)) {
            if (f.getDataFolder() == folder) {
                return f;
            }
        }
        throw new RuntimeException("フォルダフィールドが見つかりませんでした。" + folder);
    }

    /**
     * 参照ボタンを返します。
     *
     * @return {@link MainteStngRefButton}
     */
    public MainteStngRefButton findRefButton() {
        for (MainteStngRefButton b : view.findComponentsByClass(MainteStngRefButton.class)) {
            if (!(b instanceof MainteStngMoveButton) && b.getDataFolder() == folder) {
                return b;
            }
        }
        throw new RuntimeException("参照ボタンが見つかりませんでした。" + folder);
    }

    /**
     * 移動ボタンを返します。
     *
     * @return {@link MainteStngMoveButton}
     */
    public MainteStngMoveButton findMoveButton() {
        for (MainteStngMoveButton b : view.findComponentsByClass(MainteStngMoveButton.class)) {
            if (b.getDataFolder() == folder) {
                return b;
            }
        }
        throw new RuntimeException("移動ボタンが見つかりませんでした。" + folder);
    }

}

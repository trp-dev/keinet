package jp.ac.kawai_juku.banzaisystem.mainte.stng.view;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.MonthComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.PasswordField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.MaxLength;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.component.MainteStngPasswordField;

/**
 *
 * メンテナンス-環境設定画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class MainteStngView extends SubView {

    /** グループ番号：先生用パスワード */
    public static final int GROUP_TPASSWORD = 1;

    /** グループ番号：管理者用パスワード */
    public static final int GROUP_APASSWORD = 2;

    /** 削除ボタン */
    @Location(x = 240, y = 115)
    public ImageButton deleteButton;

    /** 初期値に戻す */
    @Location(x = 485, y = 256)
    @Invisible
    public ImageButton initResultButton;

    /** 現在の設定を書き出すボタン */
    @Location(x = 59, y = 325)
    @Invisible
    public ImageButton putCurrentSettingButton;

    /** 参照ボタン */
    @Location(x = 423, y = 331)
    @Invisible
    public ImageButton referenceButton;

    /** 読み込みボタン */
    @Location(x = 481, y = 325)
    @Invisible
    public ImageButton loadingButton;

    /** 変更ボタン */
    @Location(x = 834, y = 88)
    @Disabled
    @Group(GROUP_TPASSWORD)
    public ImageButton changeTeacherPasswordButton;

    /** 変更ボタン */
    @Location(x = 834, y = 198)
    @Disabled
    @Group(GROUP_APASSWORD)
    public ImageButton changeAdminPasswordButton;

    /** 設定ボタン */
    @Location(x = 904, y = 309)
    public ImageButton settingButton;

    /** この環境情報を保存するボタン */
    @Location(x = 701, y = 595)
    @Invisible
    public ImageButton saveSettingInfoButton;

    /** 先生用新パスワード  */
    @Location(x = 702, y = 79)
    @Size(width = 120, height = 20)
    @NoAction
    @MaxLength(8)
    @Group(GROUP_TPASSWORD)
    public MainteStngPasswordField teachNewPassField;

    /** 先生用再パスワード  */
    @Location(x = 702, y = 103)
    @Size(width = 120, height = 20)
    @NoAction
    @MaxLength(8)
    @Group(GROUP_TPASSWORD)
    public MainteStngPasswordField teachRePassField;

    /** 管理者用現パスワード  */
    @Location(x = 702, y = 157)
    @Size(width = 120, height = 20)
    @NoAction
    @MaxLength(8)
    @Group(GROUP_APASSWORD)
    public MainteStngPasswordField adminCurrentPassField;

    /** 管理者用新パスワード  */
    @Location(x = 702, y = 189)
    @Size(width = 120, height = 20)
    @NoAction
    @MaxLength(8)
    @Group(GROUP_APASSWORD)
    public MainteStngPasswordField adminNewPassField;

    /** 管理者用再パスワード  */
    @Location(x = 702, y = 213)
    @Size(width = 120, height = 20)
    @NoAction
    @MaxLength(8)
    @Group(GROUP_APASSWORD)
    public MainteStngPasswordField adminRePassField;

    /** 別の環境設定ファイル  */
    @Location(x = 309, y = 334)
    @Size(width = 112, height = 20)
    @Invisible
    public TextField anotherConfigFileField;

    /** 管理者用パスワード  */
    @Location(x = 752, y = 313)
    @Size(width = 150, height = 20)
    @NoAction
    @MaxLength(8)
    public PasswordField adminPassField;

    /** インポート済み成績・志望データ削除-------------- */
    /** 模試コンボボックス */
    @Location(x = 39, y = 84)
    @Size(width = 207, height = 20)
    @FireActionEvent
    public ComboBox<ExamBean> examComboBox;

    /** 学年コンボボックス */
    @Location(x = 39, y = 115)
    @Size(width = 42, height = 20)
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** クラスコンボボックス */
    @Location(x = 105, y = 115)
    @Size(width = 80, height = 20)
    @NoAction
    public ComboBox<String> classComboBox;

    /** 入試結果調査機能--------------------------------- */
    /** 月コンボボックス */
    @Location(x = 397, y = 191)
    @Size(width = 50, height = 20)
    @Invisible
    public MonthComboBox startMonthComboBox;

    /** 日コンボボックス */
    @Location(x = 473, y = 191)
    @Size(width = 50, height = 20)
    @Invisible
    public ComboBox<Integer> startDayComboBox;

    /** 年度繰り越し日：月 */
    @Location(x = 96, y = 195)
    @Invisible
    public TextLabel monthRollOverLabel;

    /** 年度繰り越し日：日 */
    @Location(x = 172, y = 195)
    @Invisible
    public TextLabel dateRollOverLabel;

    /** 年度繰り越し日：説明 */
    @Location(x = 41, y = 219)
    @Invisible
    public TextLabel rollOverDescLabel;

    /** 入試結果調査機能：使用開始日 */
    @Location(x = 330, y = 195)
    @Invisible
    public TextLabel startUseDateLabel;

    /** 入試結果調査機能：月 */
    @Location(x = 452, y = 195)
    @Invisible
    public TextLabel monthStartUseLabel;

    /** 入試結果調査機能：日 */
    @Location(x = 528, y = 195)
    @Invisible
    public TextLabel dateStartUseLabel;

    @Override
    public void initialize() {

        super.initialize();

        /* 月コンボボックスにペアを設定する */
        startMonthComboBox.setDateComboBox(startDayComboBox);

        /* 別の環境設定ファイルのパスを編集不可にする */
        anotherConfigFileField.setEditable(false);

        /* 模試コンボボックスの表示最大値を設定する */
        examComboBox.setMaximumRowCount(11);
    }

}

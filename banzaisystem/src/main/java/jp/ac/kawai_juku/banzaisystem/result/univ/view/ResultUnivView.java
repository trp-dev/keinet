package jp.ac.kawai_juku.banzaisystem.result.univ.view;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.result.univ.component.ResultUnivExamineeTable;
import jp.ac.kawai_juku.banzaisystem.result.univ.component.ResultUnivListTable;

/**
 *
 * 入試結果入力-大学指定(一覧)画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultUnivView extends SubView {

    /** 大学名 */
    @Location(x = 44, y = 106)
    @Size(width = 201, height = 20)
    public TextField univNameField;

    /** 検索ボタン */
    @Location(x = 250, y = 105)
    public ImageButton searchResultUnivButton;

    /** クリアボタン */
    @Location(x = 250, y = 135)
    public ImageButton clearUnivNameButton;

    /** 大学区分コンボボックス */
    @Location(x = 426, y = 40)
    @Size(width = 71, height = 20)
    public ComboBox<Code> univDivComboBox;

    /** 大学一覧テーブル */
    @Location(x = 372, y = 61)
    @Size(width = 324, height = 142)
    public ResultUnivListTable univListTable;

    /** 説明文 */
    @Location(x = 745, y = 14)
    public TextLabel noteLabel;

    /** 受験者表示ボタン */
    @Location(x = 742, y = 103)
    public ImageButton dispExamineeButton;

    /** 受験者一覧テーブル */
    @Location(x = 15, y = 246)
    @Size(width = 982, height = 343)
    public ResultUnivExamineeTable examineeTable;

    /** 保存ボタン */
    @Location(x = 758, y = 602)
    public ImageButton saveButton;

    /** CSV出力ボタン */
    @Location(x = 875, y = 603)
    public ImageButton outputCsvButton;

}

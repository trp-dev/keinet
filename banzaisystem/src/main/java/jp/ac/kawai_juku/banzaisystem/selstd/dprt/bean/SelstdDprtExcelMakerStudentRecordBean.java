package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の個人成績一覧データBeanです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 *
 */
public class SelstdDprtExcelMakerStudentRecordBean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** 対象模試 */
    private String examCd;

    /** 氏名（カナ） */
    private String nameKana;

    /** 文理コード */
    private String bunriCode;

    /** 教科コード */
    private String courseCd;

    /** 科目コード */
    private String subCd;

    /** 科目短縮名 */
    private String subAddrName;

    /** 科目配点 */
    private Integer subAllotpnt;

    /** 科目得点 */
    private Integer subScore;

    /** 科目換算得点 */
    private Integer subCvsscore;

    /** 科目偏差値 */
    private Double subDeviation;

    /** 基礎科目フラグ */
    private String basicFlg;

    /** 科目得点(文字型) */
    private String subStrScore;

    /** 対象模試名 */
    private String examName;

    /** 対象月 */
    private String inpleDate;

    /** 対象模試タイプ */
    private String examType;

    /** 模試外部データ開放日 */
    private String examOpenDate;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 対象模試をセットします。
     *
     * @param examCd 対象模試
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 氏名（カナ）を返します。
     *
     * @return 氏名（カナ）
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 氏名（カナ）をセットします。
     *
     * @param nameKana 氏名（カナ）
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 文理コードを返します。
     *
     * @return 文理コード
     */
    public String getBunriCode() {
        return bunriCode;
    }

    /**
     * 文理コードをセットします。
     *
     * @param bunriCode 文理コード
     */
    public void setBunriCode(String bunriCode) {
        this.bunriCode = bunriCode;
    }

    /**
     * 教科コードを返します。
     *
     * @return 教科コード
     */
    public String getCourseCd() {
        return courseCd;
    }

    /**
     * 教科コードをセットします。
     *
     * @param courseCd 教科コード
     */
    public void setCourseCd(String courseCd) {
        this.courseCd = courseCd;
    }

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

    /**
     * 科目短縮名を返します。
     *
     * @return 科目短縮名
     */
    public String getSubAddrName() {
        return subAddrName;
    }

    /**
     * 科目短縮名をセットします。
     *
     * @param subAddrName 科目短縮名
     */
    public void setSubAddrName(String subAddrName) {
        this.subAddrName = subAddrName;
    }

    /**
     * 科目配点を返します。
     *
     * @return 科目配点
     */
    public Integer getSubAllotpnt() {
        return subAllotpnt;
    }

    /**
     * 科目配点をセットします。
     *
     * @param subAllotpnt 科目配点
     */
    public void setSubAllotpnt(Integer subAllotpnt) {
        this.subAllotpnt = subAllotpnt;
    }

    /**
     * 科目得点を返します。
     *
     * @return 科目得点
     */
    public Integer getSubScore() {
        return subScore;
    }

    /**
     * 科目得点をセットします。
     *
     * @param subScore 科目得点
     */
    public void setSubScore(Integer subScore) {
        this.subScore = subScore;
    }

    /**
     * 科目換算得点を返します。
     *
     * @return 科目換算得点
     */
    public Integer getSubCvsscore() {
        return subCvsscore;
    }

    /**
     * 科目換算得点をセットします。
     *
     * @param subCvsscore 科目換算得点
     */
    public void setSubCvsscore(Integer subCvsscore) {
        this.subCvsscore = subCvsscore;
    }

    /**
     * 科目偏差値を返します。
     *
     * @return 科目偏差値
     */
    public Double getSubDeviation() {
        return subDeviation;
    }

    /**
     * 科目偏差値をセットします。
     *
     * @param subDeviation 科目偏差値
     */
    public void setSubDeviation(Double subDeviation) {
        this.subDeviation = subDeviation;
    }

    /**
     * 基礎科目フラグを返します。
     *
     * @return 基礎科目フラグ
     */
    public String getBasicFlg() {
        return basicFlg;
    }

    /**
     * 基礎科目フラグをセットします。
     *
     * @param basicFlg 基礎科目フラグ
     */
    public void setBasicFlg(String basicFlg) {
        this.basicFlg = basicFlg;
    }

    /**
     * 科目得点(文字)を返します。
     *
     * @return 科目得点(文字)
     */
    public String getSubStrScore() {
        return subStrScore;
    }

    /**
     * 科目得点(文字)をセットします。
     *
     * @param subStrScore 科目得点(文字)
     */
    public void setSubStrScore(String subStrScore) {
        this.subStrScore = subStrScore;
    }

    /**
     * 対象模試名を返します。
     *
     * @return 対象模試
     */
    public String getExamName() {
        return examName;
    }

    /**
     * 対象模試名をセットします。
     *
     * @param examName 対象模試
     */
    public void setExamName(String examName) {
        this.examName = examName;
    }

    /**
     * 対象模試月を返します。
     *
     * @return 対象模試月
     */
    public String getInpleDate() {
        return inpleDate;
    }

    /**
     * 対象模試月をセットします。
     *
     * @param inpleDate 対象模試月
     */
    public void setInpleDate(String inpleDate) {
        this.inpleDate = inpleDate;
    }

    /**
     * 対象模試タイプをセットします。
     *
     * @param examType 対象模試タイプ
     */
    public void setExamType(String examType) {
        this.examType = examType;
    }

    /**
     * 対象模試タイプを返します。
     *
     * @return 対象模試タイプ
     */
    public String getExamType() {
        return examType;
    }

    /**
     * 模試外部データ開放日を返します。
     *
     * @return 模試外部データ開放日
     */
    public String getExamOpenDate() {
        return examOpenDate;
    }

    /**
     * 模試外部データ開放日をセットします。
     *
     * @param examOpenDate 模試外部データ開放日
     */
    public void setExamOpenDate(String examOpenDate) {
        this.examOpenDate = examOpenDate;
    }

}

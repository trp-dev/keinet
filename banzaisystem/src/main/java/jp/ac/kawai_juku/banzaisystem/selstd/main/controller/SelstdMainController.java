package jp.ac.kawai_juku.banzaisystem.selstd.main.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.MenuController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.main.controller.MainteMainController;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.controller.SelstdDaddController;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.controller.SelstdDprtController;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller.SelstdDunvController;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller.SelstdDunvController.SelstdDunvProcessor;
import jp.ac.kawai_juku.banzaisystem.selstd.main.annotation.SelstdMainSelectStudent;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainTable;
import jp.ac.kawai_juku.banzaisystem.selstd.main.service.SelstdMainService;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 対象生徒選択画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainController extends MenuController {

    /** View */
    @Resource(name = "selstdMainView")
    private SelstdMainView view;

    /** Service */
    @Resource(name = "selstdMainService")
    private SelstdMainService service;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 大学選択時の模試 */
    private ComboBoxItem<ExamBean> examItem;

    /** 大学選択時の学年 */
    private ComboBoxItem<Integer> gradeItem;

    /** 大学選択時のクラス */
    private ComboBoxItem<String> classItem;

    /**
     * 初期表示アクションです。
     *
     * @return 対象生徒選択画面
     */
    @Logging(GamenId.B1)
    @Execute
    public ExecuteResult index() {
        initExamComboBox();
        return VIEW_RESULT;
    }

    /**
     * 模試コンボボックスを初期化します。
     */
    private void initExamComboBox() {
        List<ComboBoxItem<ExamBean>> itemList = CollectionsUtil.newArrayList();
        for (ExamBean exam : systemDto.getExamList()) {
            itemList.add(new ComboBoxItem<ExamBean>(exam));
        }
        view.examComboBox.setItemList(itemList);
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
        int count = 0;
        for (Integer grade : service.findGradeList()) {
            if (grade != null && grade.intValue() >= 4) {
                count++;
            }
            itemList.add(new ComboBoxItem<Integer>(grade));
        }
        if (!itemList.isEmpty()) {
            /* 全学年：学年が存在する場合のみ追加する */
            itemList.add(0, new ComboBoxItem<Integer>(getMessage("label.selstd.main.allGrade"), -1));
        }
        if (count >= 2) {
            /* 4年以上：4年以上の学年が複数存在する場合のみ追加する */
            itemList.add(1, new ComboBoxItem<Integer>(getMessage("label.selstd.main.4thGradeAndAbove"), -2));
        }
        ExamBean exam = view.examComboBox.getSelectedValue();
        if (view.gradeComboBox.getItemCount() == 0 && exam != null && exam.getTargetGrade() != null) {
            /* 初回のみ模試の対象学年を初期値とする */
            view.gradeComboBox.setItemList(itemList, new ComboBoxItem<Integer>(exam.getTargetGrade()));
        } else {
            view.gradeComboBox.setItemList(itemList);
        }
    }

    /**
     * 模試コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult examComboBoxAction() {
        initGradeComboBox();
        /*
        final ExamBean exam = view.examComboBox.getSelectedValue();
        
        boolean flg = !ExamUtil.isCenter(exam) & view.univRadio.isSelected();
        
        view.englishJoinningCheckBox.setVisible(flg);
        */
        return null;
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        initClassComboBox();
        return null;
    }

    /**
     * クラスコンボボックスを初期化します。
     */
    private void initClassComboBox() {
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年が存在しない場合 */
            view.classComboBox.setItemList(Collections.<ComboBoxItem<String>> emptyList());
        } else {
            /* 学年が存在する場合 */
            List<ComboBoxItem<String>> itemList = CollectionsUtil.newArrayList();
            for (String cls : service.findClassList(grade.getValue())) {
                itemList.add(new ComboBoxItem<String>(cls));
            }
            if (!itemList.isEmpty()) {
                itemList.add(0, new ComboBoxItem<String>(getMessage("label.selstd.main.allClass"), StringUtils.EMPTY));
            }
            if (view.classComboBox.getItemCount() == 0 && !itemList.isEmpty()) {
                /* 初回のみ初期値を設定する */
                view.classComboBox.setItemList(itemList, itemList.get(1));
            } else {
                view.classComboBox.setItemList(itemList);
            }
        }
    }

    /**
     * クラスコンボボックス変更アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult classComboBoxAction() {
        if (view.nameRadio.isSelected()) {
            return nameRadioAction();
        } else if (view.scoreRadio.isSelected()) {
            return scoreRadioAction();
        } else {
            return univRadioAction();
        }
    }

    /**
     * 「個人名のみ」ラジオ選択アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult nameRadioAction() {
        initNameTable();
        /* view.englishJoinningCheckBox.setVisible(false); */
        return null;
    }

    /**
     * 「個人名＋成績」ラジオ選択アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult scoreRadioAction() {
        initScoreTable();
        /* view.englishJoinningCheckBox.setVisible(false); */
        return null;
    }

    /**
     * 「個人名＋志望大学」ラジオ選択アクションです。
     *
     * @return {@link ExecuteResult}
     */
    @Execute
    public ExecuteResult univRadioAction() {
        /* final ExamBean exam = view.examComboBox.getSelectedValue(); */

        /* view.englishJoinningCheckBox.setVisible(!ExamUtil.isCenter(exam)); */
        return initCandidateTable();
    }

    /**
     * 個人名のみテーブルを初期化します。
     */
    private void initNameTable() {

        /* 生徒更新・削除ボタンの状態が変わるよう、非表示になるテーブルをクリアする */
        view.scoreTable.clearDataList();
        view.candidateTable.clearDataList();

        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年がなければ空にする */
            view.nameTable.clearDataList();
        } else {
            view.nameTable.setDataList(service.findKojinList(grade.getValue(), view.classComboBox.getSelectedValue()));
        }

        showTable(view.nameTable);
    }

    /**
     * 個人名＋成績テーブルを初期化します。
     */
    private void initScoreTable() {

        /* 生徒更新・削除ボタンの状態が変わるよう、非表示になるテーブルをクリアする */
        view.nameTable.clearDataList();
        view.candidateTable.clearDataList();

        /* 科目名リストをセットする */
        ExamBean exam = view.examComboBox.getSelectedValue();
        view.scoreTable.setSubNameList(service.findSubNameList(exam == null ? Exam.Code.MARK1 : exam.getExamCd()));

        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (exam == null || grade == null) {
            /* 対象模試または学年がなければ空にする */
            view.scoreTable.clearDataList();
        } else {
            view.scoreTable.setDataList(service.findScoreList(grade.getValue(), view.classComboBox.getSelectedValue(), exam));
        }

        showTable(view.scoreTable);
    }

    /**
     * 個人名＋志望大学テーブルを初期化します。
     *
     * @return {@link ExecuteResult}
     */
    private ExecuteResult initCandidateTable() {

        /* 生徒更新・削除ボタンの状態が変わるよう、非表示になるテーブルをクリアする */
        view.nameTable.clearDataList();
        view.scoreTable.clearDataList();

        /* 大学選択時から「模試」「学年」「クラス」の何れかが変わっていたら、大学検索条件をクリアする */
        if (!ObjectUtils.equals(view.examComboBox.getSelectedItem(), examItem) || !ObjectUtils.equals(view.gradeComboBox.getSelectedItem(), gradeItem) || !ObjectUtils.equals(view.classComboBox.getSelectedItem(), classItem)) {
            view.candidateTable.getUnivList().clear();
        }

        final ExamBean exam = view.examComboBox.getSelectedValue();
        final ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (exam == null || grade == null) {
            /* 対象模試または学年がなければ空にする */
            view.candidateTable.clearDataList();
            showTable(view.candidateTable);
            return null;
        } else {
            return new TaskResult() {

                /** データリスト */
                private List<Map<String, CellData>> dataList;

                @Override
                protected ExecuteResult doTask() {
                    /*
                    boolean noExclude = view.englishJoinningCheckBox.isSelected() && view.englishJoinningCheckBox.isVisible();
                    dataList = service.findCandidateUnivList(grade.getValue(), view.classComboBox.getSelectedValue(), exam, ExamUtil.findDockingExam(view.examComboBox), false);
                    */
                    dataList = service.findCandidateUnivList(grade.getValue(), view.classComboBox.getSelectedValue(), exam, ExamUtil.findDockingExam(view.examComboBox));
                    return null;
                }

                @Override
                protected void done() {
                    view.candidateTable.setDataList(dataList);
                    showTable(view.candidateTable);
                }
            };
        }
    }

    /**
     * 英語認定試験チェックボックス変更アクションです。
     * @return {@link ExecuteResult}
     */
    @Execute
    public ExecuteResult englishJoinningCheckBoxAction() {

        return initCandidateTable();
    }

    /**
     * 指定したテーブルを表示します。
     *
     * @param table 表示するテーブル
     */
    private void showTable(SelstdMainTable table) {
        for (SelstdMainTable t : view.findComponentsByClass(SelstdMainTable.class)) {
            t.setVisible(t == table);
        }
    }

    /**
     * インポート状況確認ボタン押下アクションです。
     *
     * @return 個人成績インポート画面
     */
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult checkImportSituationButtonAction() {
        return forward(MainteMainController.class, "checkImportSituationAction");
    }

    /**
     * 生徒追加ボタン押下アクションです。
     *
     * @return 個人編集ダイアログ
     */
    @Execute
    public ExecuteResult addStudentButtonAction() {
        return forward(SelstdDaddController.class);
    }

    /**
     * 生徒更新ボタン押下アクションです。
     *
     * @return 個人編集ダイアログ
     */
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult editStudentButtonAction() {
        return forward(SelstdDaddController.class, "edit", systemDto.getSelectedIndividualIdList().get(0));
    }

    /**
     * 生徒削除ボタン押下アクションです。
     *
     * @return なし
     */
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult deleteStudentButtonAction() {
        if (showConfirmDialog()) {
            service.deleteStudent(systemDto.getSelectedIndividualIdList());
            return index();
        } else {
            return null;
        }
    }

    /**
     * 処理継続確認ダイアログを表示します。
     *
     * @return 処理継続ならtrue
     */
    private boolean showConfirmDialog() {
        String[] button = { getMessage("label.selstd.main.dialogYes"), getMessage("label.selstd.main.dialogNo") };
        int result = JOptionPane.showOptionDialog(getActiveWindow(), getMessage("label.selstd.main.B_W001"), null, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, button, button[0]);
        return result == JOptionPane.YES_OPTION;
    }

    /**
     * 検索ボタン押下アクションです。
     *
     * @return 大学選択ダイアログ
     */
    @Execute
    public ExecuteResult searchButtonAction() {

        /* 選択中の学年・クラスに属する個人IDリストを取得する */
        List<Integer> idList = null;
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade != null) {
            idList = service.findKojinIdList(grade.getValue(), view.classComboBox.getSelectedValue());
        }

        return forward(SelstdDunvController.class, null, new SelstdDunvInBean(view.examComboBox.getSelectedValue(), idList, view.candidateTable.getUnivList(), new SelstdDunvProcessor() {
            @Override
            public void process(List<SelstdDunvBean> list) {
                examItem = view.examComboBox.getSelectedItem();
                gradeItem = view.gradeComboBox.getSelectedItem();
                classItem = view.classComboBox.getSelectedItem();
                view.candidateTable.setUnivList(list);
            }
        }));
    }

    /**
     * 個人成績・志望大学確認ボタン押下アクションです。
     *
     * @return 個人成績画面
     */
    @Execute
    public ExecuteResult largeExmntnButtonAction() {
        return exmntnButtonAction();
    }

    /**
     * 印刷ボタン押下アクションです。
     *
     * @return 一覧出力ダイアログ
     */
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult printButtonAction() {
        return forward(SelstdDprtController.class);
    }

    @Override
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult exmntnButtonAction() {
        return super.exmntnButtonAction();
    }

    @Override
    @Execute
    @SelstdMainSelectStudent
    public ExecuteResult reportButtonAction() {
        return super.reportButtonAction();
    }

    @Override
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult maintenanceButtonAction() {
        return super.maintenanceButtonAction();
    }

    @Override
    @SelstdMainSelectStudent
    @Execute
    public ExecuteResult resultButtonAction() {
        return super.resultButtonAction();
    }

}

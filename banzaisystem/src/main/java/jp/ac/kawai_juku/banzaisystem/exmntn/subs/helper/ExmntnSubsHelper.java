package jp.ac.kawai_juku.banzaisystem.exmntn.subs.helper;

import java.awt.Point;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.swing.JCheckBox;
import javax.swing.JComponent;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.CenterCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.SecondCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.controller.ExmntnEnteController;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.service.ExmntnEnteService;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.view.ExmntnEnteView;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.component.ExmntnFcltCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.controller.ExmntnFcltController;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.view.ExmntnFcltView;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.component.DistrictButton;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.component.PrefButton;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.controller.ExmntnNameController;
import jp.ac.kawai_juku.banzaisystem.exmntn.name.view.ExmntnNameView;
import jp.ac.kawai_juku.banzaisystem.exmntn.qlfc.view.ExmntnQlfcView;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dto.ExmntnRsltDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.dto.ExmntnSubsDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.service.ExmntnSubsService;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsHelper {

    /** View */
    @Resource(name = "exmntnSubsView")
    private ExmntnSubsView view;

    /** 大学検索（大学名称・所在地）画面のView */
    @Resource
    private ExmntnNameView exmntnNameView;

    /** 大学検索（学部系統）画面のView */
    @Resource
    private ExmntnFcltView exmntnFcltView;

    /** 大学検索（取得資格）画面のView */
    @Resource
    private ExmntnQlfcView exmntnQlfcView;

    /** 大学検索（入試（科目・日程・評価））画面のView */
    @Resource
    private ExmntnEnteView exmntnEnteView;

    /** 検索結果画面のView */
    @Resource
    private ExmntnRsltView exmntnRsltView;

    /** Service */
    @Resource
    private ExmntnSubsService exmntnSubsService;

    /** DTO */
    @Resource
    private ExmntnSubsDto exmntnSubsDto;

    /** 検索結果画面のDTO */
    @Resource
    private ExmntnRsltDto exmntnRsltDto;

    /** Service */
    @Resource(name = "exmntnEnteService")
    private ExmntnEnteService service;

    /**
     * 検索条件を初期化します。
     */
    public void initSearchCondition() {
        view.tabbedPane.clear();
        for (CodeCheckBox c : view.findComponentsByClass(CodeCheckBox.class)) {
            c.setSelectedSilent(false);
        }
        view.femailIncludeRadio.setSelected(true);
        view.nightIncludeRadio.setSelected(true);
        exmntnNameView.clearNameSeatButton.click();
        exmntnFcltView.clearFacultyButton.click();
        exmntnQlfcView.clearObtainedLicenseButton.click();
        exmntnEnteView.clearValuationRangeButton.click();
        exmntnEnteView.clearExaminationScheduleButton.click();
        exmntnEnteView.returnInitialValueButton.click();
        for (JComponent c : exmntnEnteView.findComponentsByGroup(ExmntnEnteView.GROUP_TARGET)) {
            ((JCheckBox) c).setSelected(false);
        }
    }

    /**
     * 検索処理を実行します。
     *
     * @param scoreBean 成績データ
     */
    public void search(ScoreBean scoreBean) {

        /* 検索条件Beanを生成する */
        ExmntnSubsUnivSearchInBean searchBean = createSerchBean(scoreBean);

        /* 検索して結果をDTOに保存する */
        exmntnRsltDto.setDataList(exmntnSubsService.searchUniv(searchBean));
        /* 私立・短大の条件を反映 */
        List<Map<String, CellData>> dataList = service.filterDataList(exmntnEnteView, exmntnRsltDto.getDataList());
        exmntnRsltDto.setDataList(dataList);

        /* 検索条件をDTOに保存する */
        exmntnSubsDto.setSearchBean(searchBean);
    }

    /**
     * 検索結果をクリアします。
     */
    public void clearSearchResult() {
        if (exmntnRsltView.isInitialized()) {
            /* 検索結果テーブルをクリアする */
            exmntnRsltView.resultTable.setDataList(Collections.<Map<String, CellData>> emptyList());
            /* 検索結果テーブルのスクロール位置を初期化しておく */
            exmntnRsltView.resultTable.getViewport().setViewPosition(new Point(0, 0));
            /* DTOの検索結果をクリアする */
            exmntnRsltDto.setDataList(null);
            /* フィルタ条件を初期化しておく */
            for (CodeCheckBox c : exmntnRsltView.findComponentsByClass(CodeCheckBox.class)) {
                c.setSelectedSilent(false);
            }
        }
    }

    /**
     * 検索条件Beanを生成します。
     *
     * @param scoreBean 成績データ
     * @return {@link ExmntnSubsUnivSearchInBean}
     */
    public ExmntnSubsUnivSearchInBean createSerchBean(ScoreBean scoreBean) {

        ExmntnSubsUnivSearchInBean bean = new ExmntnSubsUnivSearchInBean();

        /* 成績データを設定する */
        bean.setScoreBean(scoreBean);

        /* 「区分」を検索条件に設定する */
        bean.setDivList(findCodeList(view, CodeType.UNIV_SEARCH_DIV));

        /* 「女子大」を検索条件に設定する */
        bean.setFemail(view.getSelectedCode(CodeType.UNIV_SEARCH_FEMAIL));

        /* 「夜間（夜間主を）」を検索条件に設定する */
        bean.setNight(view.getSelectedCode(CodeType.UNIV_SEARCH_NIGHT));

        /* 「大学名称・所在地」を検索条件に設定する */
        if (view.tabbedPane.findCheckBox(ExmntnNameController.class).isSelected()) {
            String univName = exmntnNameView.inputField.getText();
            if (!univName.isEmpty()) {
                /* Kei-Naviに合わせて、半角カタカナ変換後に12文字で切る(DBのサイズが12バイトまでのため) */
                bean.setUnivName(JpnStringUtil.normalize(StringUtils.substring(JpnStringUtil.hira2Hkana(univName), 0, 12)));
            }
            bean.setPrefCdList(createPrefCdList());
            bean.setDistCdList(createDistCdList());
        }

        /* 「学部系統」を検索条件に設定する */
        if (view.tabbedPane.findCheckBox(ExmntnFcltController.class).isSelected()) {
            /* 中系統リストを設定する */
            bean.setmStemmaList(findCodeList(exmntnFcltView, CodeType.M_STEMMA));
            /* 小系統リストを設定する */
            bean.setsStemmaList(findCodeList(exmntnFcltView, CodeType.S_STEMMA));
            /* 分野リストを設定する */
            bean.setRegionList(findCodeList(exmntnFcltView, CodeType.REGION));
            /* 帳票用の分野リストを設定する */
            bean.setPrintRegionList(createPrintRegionList());
        }

        /* 「入試（科目・日程・評価）」を検索条件に設定する */
        if (view.tabbedPane.findCheckBox(ExmntnEnteController.class).isSelected()) {
            /* ＜評価範囲＞を検索条件に設定する */
            if (exmntnEnteView.scopeOfAssessmentCheckBox.isSelected()) {
                bean.setRaingDivList(findCodeList(exmntnEnteView, CodeType.UNIV_SEARCH_RANGE));
                bean.setRatingList(findCodeList(exmntnEnteView, CodeType.UNIV_SEARCH_RANGE_VALUE));
            }
            /* ＜入試日程＞を検索条件に設定する */
            if (exmntnEnteView.exmntnScheduleCheckBox.isSelected()) {
                bean.setScheduleList(findCodeList(exmntnEnteView, CodeType.UNIV_SEARCH_SCHEDULE));
                bean.setScheduleList2(findCodeList(exmntnEnteView, CodeType.UNIV_SEARCH_SCHEDULE2));
            }
            /* ＜使用科目＞を検索条件に設定する */
            if (exmntnEnteView.exmntnSubjectsCheckBox.isSelected()) {
                bean.setCenterList(createCenterList());
                bean.setSecondList(createSecondList());
                bean.setNotImpose(exmntnEnteView.notImposeCheckBox.isSelected());
                bean.setRateList(findCodeList(exmntnEnteView, CodeType.UNIV_SEARCH_RATE));
            }
        }

        return bean;
    }

    /**
     * 画面で選択されている県コードリストを生成します。
     *
     * @return 県コードリスト
     */
    private List<String> createPrefCdList() {
        List<String> prefCdList = CollectionsUtil.newArrayList();
        for (PrefButton b : exmntnNameView.findComponentsByClass(PrefButton.class)) {
            if (b.isSelected()) {
                prefCdList.add(b.getPrefCd());
            }
        }
        return prefCdList.isEmpty() ? null : prefCdList;
    }

    /**
     * 画面で選択されている地区コードリストを生成します。
     *
     * @return 地区コードリスト
     */
    private List<String> createDistCdList() {
        List<String> distCdList = CollectionsUtil.newArrayList();
        for (DistrictButton b : exmntnNameView.findComponentsByClass(DistrictButton.class)) {
            if (b.isSelected()) {
                distCdList.add(b.getDistCd());
            }
        }
        return distCdList.isEmpty() ? null : distCdList;
    }

    /**
     * 画面で選択されているセンター科目リストを生成します。
     *
     * @return センター科目リスト
     */
    private List<CenterSubject> createCenterList() {
        List<CenterSubject> list = CollectionsUtil.newArrayList();
        for (CenterCheckBox c : exmntnEnteView.findComponentsByClass(CenterCheckBox.class)) {
            if (c.isSelected() && c.isVisible()) {
                list.add(c.getSubject());
            }
        }
        Collections.sort(list);
        return list.isEmpty() ? null : list;
    }

    /**
     * 画面で選択されている二次科目リストを生成します。
     *
     * @return 二次科目リスト
     */
    private List<SecondSubject> createSecondList() {
        List<SecondSubject> list = CollectionsUtil.newArrayList();
        for (SecondCheckBox s : exmntnEnteView.findComponentsByClass(SecondCheckBox.class)) {
            if (s.isSelected()) {
                list.add(s.getSubject());
            }
        }
        if (list.indexOf(SecondSubject.LISTENING) > -1) {
            /* リスニングを含む場合は英語を削除しておく */
            list.remove(SecondSubject.ENGLISH);
        }
        if (list.indexOf(SecondSubject.MATH) > -1) {
            /* 数学はコンボボックスの選択値に置き換える */
            list.remove(SecondSubject.MATH);
            list.add(exmntnEnteView.indvMathComboBox.getSelectedItem().getValue());
        }
        if (list.indexOf(SecondSubject.JAPANESE) > -1) {
            /* 国語はコンボボックスの選択値に置き換える */
            list.remove(SecondSubject.JAPANESE);
            list.add(exmntnEnteView.indvJpnComboBox.getSelectedItem().getValue());
        }
        if (list.indexOf(SecondSubject.PHYSICS_BASIC) > -1) {
            /* 物理基礎を含む場合は物理を削除しておく */
            list.remove(SecondSubject.PHYSICS);
        }
        if (list.indexOf(SecondSubject.CHEMISTRY_BASIC) > -1) {
            /* 化学基礎を含む場合は化学を削除しておく */
            list.remove(SecondSubject.CHEMISTRY);
        }
        if (list.indexOf(SecondSubject.BIOLOGY_BASIC) > -1) {
            /* 生物基礎を含む場合は生物を削除しておく */
            list.remove(SecondSubject.BIOLOGY);
        }
        if (list.indexOf(SecondSubject.EARTHSCIENCE_BASIC) > -1) {
            /* 地学基礎を含む場合は地学を削除しておく */
            list.remove(SecondSubject.EARTHSCIENCE);
        }
        Collections.sort(list);
        return list.isEmpty() ? null : list;
    }

    /**
     * 画面で選択されているコードリストを返します。
     *
     * @param view {@link AbstractView}
     * @param type コード種別
     * @return コードリスト
     */
    private List<String> findCodeList(AbstractView view, CodeType type) {
        List<String> list = CollectionsUtil.newArrayList();
        for (CodeComponent c : view.findComponentsByCodeType(type)) {
            if (c.isSelected()) {
                String value = c.getCode().getValue();
                if (list.indexOf(value) < 0) {
                    list.add(value);
                }
            }
        }
        return list.isEmpty() ? null : list;
    }

    /**
     * 分野コードリスト(帳票用)を生成します。
     *
     * @return 分野コードリスト(帳票用)
     */
    private List<Code> createPrintRegionList() {
        List<Code> list = CollectionsUtil.newArrayList();
        for (ExmntnFcltCheckBox c : exmntnFcltView.findComponentsByClass(ExmntnFcltCheckBox.class)) {
            Code code = c.getCode();
            if (c.isSelected() && list.indexOf(code) < 0) {
                CodeType type = code.getCodeType();
                if (type == CodeType.REGION || (type == CodeType.S_STEMMA && !exmntnFcltView.getRegionSStemmaSet().contains(code))) {
                    /* 分野または分野を持たない小系統を追加する */
                    list.add(code);
                }
            }
        }
        return list;
    }

    /**
     * 資格コードリスト・資格カテゴリリストを設定します。
     *
     * @param bean {@link ExmntnSubsUnivSearchInBean}
     */
    /*    private void setCertification(ExmntnSubsUnivSearchInBean bean) {
        List<String> certCdList = CollectionsUtil.newArrayList();
        List<Code> certCategoryList = CollectionsUtil.newArrayList();
        for (CodeComponent cc : exmntnQlfcView.findComponentsByCodeType(CodeType.CREDENTIAL)) {
            if (cc.isSelected()) {
                Code code = cc.getCode();
                for (Object attr : cc.getCode().getAttribute()) {
                    certCdList.add((String) attr);
                }
                if (certCategoryList.indexOf(code.getParent()) < 0) {
                     資格カテゴリは重複しないようにする
                    certCategoryList.add(code.getParent());
                }
            }
        }
        if (!certCdList.isEmpty()) {
            bean.setCertCdList(certCdList);
            bean.setCertCategoryList(certCategoryList);
        }
    }
    */
}

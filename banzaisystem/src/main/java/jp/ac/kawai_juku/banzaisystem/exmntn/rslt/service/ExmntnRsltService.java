package jp.ac.kawai_juku.banzaisystem.exmntn.rslt.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CANDIDATE_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.dao.ExmntnRsltDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.view.ExmntnRsltView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.service.annotation.Lock;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

import org.seasar.framework.beans.util.BeanMap;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 検索結果画面のサービスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
public class ExmntnRsltService {

    /** DAO */
    @Resource
    private ExmntnRsltDao exmntnRsltDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * データリストをフィルタします。
     *
     * @param view ビュー
     * @param dataList データリスト
     * @return フィルタ結果
     */
    public List<Map<String, CellData>> filterDataList(ExmntnRsltView view, List<Map<String, CellData>> dataList) {

        if (!needFilter(view, CodeType.UNIV_RESULT_UDIV) && !needFilter(view, CodeType.UNIV_RESULT_PDIV)) {
            /* フィルタ条件なし */
            return dataList;
        }

        List<Map<String, CellData>> list = CollectionsUtil.newArrayList(dataList.size());
        for (Map<String, CellData> map : dataList) {
            Code div = (Code) map.get("division").getSortValue();
            if (div == Code.UNIV_RESULT_UNIV_DIV_NATIONAL || div == Code.UNIV_RESULT_UNIV_DIV_PUBLIC) {
                /* 国公立 */
                if (!checkNationalPublic(view, map)) {
                    continue;
                }
            } else {
                /* 私立短大 */
                if (!checkPrivateJunior(view, map)) {
                    continue;
                }
            }
            list.add(map);
        }

        return list;
    }

    /**
     * 指定したコード種別でのフィルタが必要であるかを判断します。
     *
     * @param view ビュー
     * @param type コード種別
     * @return フィルタが必要ならtrue
     */
    private boolean needFilter(ExmntnRsltView view, CodeType type) {
        for (CodeComponent c : view.findComponentsByCodeType(type)) {
            if (c.isSelected()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 国公立のフィルタ条件を満たしているかチェックします。
     *
     * @param view ビュー
     * @param map 検索結果
     * @return チェック結果
     */
    private boolean checkNationalPublic(ExmntnRsltView view, Map<String, CellData> map) {
        String scheduleCd = map.get("scheduleCd").getSortValue().toString();
        if (scheduleCd.equals(Univ.SCHEDULE_BFR)) {
            return view.firstSemesterCheckBox.isSelected();
        } else if (scheduleCd.equals(Univ.SCHEDULE_AFT)) {
            return view.secondSemesterCheckBox.isSelected();
        } else if (scheduleCd.equals(Univ.SCHEDULE_MID)) {
            return view.middleSemesterCheckBox.isSelected();
        } else {
            return view.anotherDayCheckBox.isSelected();
        }
    }

    /**
     * 私立短大のフィルタ条件を満たしているかチェックします。
     *
     * @param view ビュー
     * @param map 検索結果
     * @return チェック結果
     */
    private boolean checkPrivateJunior(ExmntnRsltView view, Map<String, CellData> map) {
        String scheduleCd = map.get("scheduleCd").getSortValue().toString();
        if (scheduleCd.equals(Univ.SCHEDULE_CNT)) {
            return view.centerCheckBox.isSelected();
        } else {
            return view.publicCheckBox.isSelected();
        }
    }

    /**
     * （先生用）志望大学を追加します。
     *
     * @param exam 対象模試
     * @param id 対象生徒の個人ID
     * @param univKeyList 志望大学リスト
     */
    @Lock
    public void addTeacherCandidateUniv(ExamBean exam, Integer id, List<UnivKeyBean> univKeyList) {

        /* 対象模試の件数上限チェックを行う */
        if (exmntnRsltDao.countCandidateUniv(exam, id) + univKeyList.size() > CANDIDATE_UNIV_MAX) {
            throw new MessageDialogException(getMessage("error.exmntn.rslt.C_E010"));
        }

        /* 対象模試以外の件数上限チェックを行う */
        int max = exmntnRsltDao.selectMaxCountCandidateUniv(exam, id, toUniv10CdList(univKeyList));
        if (max + univKeyList.size() > CANDIDATE_UNIV_MAX) {
            throw new MessageDialogException(getMessage("error.exmntn.rslt.C_E016"));
        }

        /* 重複チェックを行う */
        List<UnivKeyBean> candidateUnivList = exmntnRsltDao.selectCandidateUnivList(exam, id);
        for (UnivKeyBean bean : candidateUnivList) {
            if (univKeyList.indexOf(bean) > -1) {
                throw new MessageDialogException(getMessage("error.exmntn.rslt.C_E009"));
            }
        }

        /* 全ての模試について追加する */
        List<BeanMap> params = CollectionsUtil.newArrayList();
        for (ExamBean bean : exmntnRsltDao.selectAllExamList()) {
            addParam(params, bean, id, univKeyList);
        }

        /* まとめて登録する */
        exmntnRsltDao.insertCandidateUniv(params);
    }

    /**
     * 志望大学リストを10ケタ大学コードリストに変換します。
     *
     * @param univKeyList 志望大学リスト
     * @return 10ケタ大学コードリスト
     */
    private List<String> toUniv10CdList(List<UnivKeyBean> univKeyList) {
        List<String> list = new ArrayList<>(univKeyList.size());
        for (UnivKeyBean bean : univKeyList) {
            list.add(bean.getUnivKey());
        }
        return list;
    }

    /**
     * 志望大学登録パラメータリストにパラメータを追加します。
     *
     * @param params パラメータリスト
     * @param exam 模試
     * @param id 対象生徒の個人ID
     * @param univKeyList 志望大学リスト
     */
    private void addParam(List<BeanMap> params, ExamBean exam, Integer id, List<UnivKeyBean> univKeyList) {
        for (UnivKeyBean bean : univKeyList) {
            BeanMap param = new BeanMap();
            param.put("exam", exam);
            param.put("id", id);
            param.put("univKey", bean);
            param.put("systemDto", systemDto);
            params.add(param);
        }
    }

    /**
     * （生徒用）志望大学を追加します。
     *
     * @param dto 志望大学個人成績画面のDTO
     * @param univKeyList 志望大学リスト
     */
    public void addStudentCandidateUniv(ExmntnMainDto dto, List<UnivKeyBean> univKeyList) {

        /* 志望大学リストをDTOから取得する  */
        List<UnivKeyBean> candidateUnivList = dto.getStudentCandidateUnivList();
        if (candidateUnivList == null) {
            candidateUnivList = CollectionsUtil.newArrayList(CANDIDATE_UNIV_MAX);
            dto.setStudentCandidateUnivList(candidateUnivList);
        }

        /* 件数上限チェックを行う */
        if (candidateUnivList.size() + univKeyList.size() > CANDIDATE_UNIV_MAX) {
            throw new MessageDialogException(getMessage("error.exmntn.rslt.C_E010"));
        }

        /* 重複チェックを行う */
        for (UnivKeyBean bean : candidateUnivList) {
            if (univKeyList.indexOf(bean) > -1) {
                throw new MessageDialogException(getMessage("error.exmntn.rslt.C_E009"));
            }
        }

        /* 志望大学リストに追加する */
        candidateUnivList.addAll(univKeyList);
    }

}

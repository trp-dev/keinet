package jp.ac.kawai_juku.banzaisystem.systop.lgin.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;

/**
 *
 * データ状況を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopLginDataStatusBean extends ExamBean {

    /** 大学マスタ状況 */
    private String univStatus;

    /** 個人成績状況 */
    private String recordStatus;

    /**
     * 大学マスタ状況を返します。
     *
     * @return 大学マスタ状況
     */
    public String getUnivStatus() {
        return univStatus;
    }

    /**
     * 大学マスタ状況をセットします。
     *
     * @param univStatus 大学マスタ状況
     */
    public void setUnivStatus(String univStatus) {
        this.univStatus = univStatus;
    }

    /**
     * 個人成績状況を返します。
     *
     * @return 個人成績状況
     */
    public String getRecordStatus() {
        return recordStatus;
    }

    /**
     * 個人成績状況をセットします。
     *
     * @param recordStatus 個人成績状況
     */
    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

}

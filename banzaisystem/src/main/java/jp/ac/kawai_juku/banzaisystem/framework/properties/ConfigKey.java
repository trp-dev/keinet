package jp.ac.kawai_juku.banzaisystem.framework.properties;

/**
 *
 * 環境設定ファイルのキーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum ConfigKey implements PropsKey {

    /** 空定義(※他のキーを定義した場合は削除すること) */
    EMPTY {
        @Override
        public String getDefaultValue() {
            return "";
        }
    };

}

package jp.ac.kawai_juku.banzaisystem.framework.creator;

import org.seasar.framework.container.ComponentCustomizer;
import org.seasar.framework.container.creator.ComponentCreatorImpl;
import org.seasar.framework.container.deployer.InstanceDefFactory;
import org.seasar.framework.convention.NamingConvention;

/**
 *
 * Controllerクラス用の {@link org.seasar.framework.container.ComponentCreator} です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ControllerCreator extends ComponentCreatorImpl {

    /** コンポーネント名のサフィックス */
    public static final String NAME_SUFFIX = "Controller";

    /**
     * コンストラクタです。
     *
     * @param namingConvention 命名規約
     */
    public ControllerCreator(NamingConvention namingConvention) {
        super(namingConvention);
        setNameSuffix(NAME_SUFFIX);
        setInstanceDef(InstanceDefFactory.SINGLETON);
    }

    /**
     * {@link ComponentCustomizer}を返します。
     *
     * @return コンポーネントカスタマイザ
     */
    public ComponentCustomizer getControllerCustomizer() {
        return getCustomizer();
    }

    /**
     * {@link ComponentCustomizer}を設定します。
     *
     * @param customizer
     *            コンポーネントカスタマイザ
     */
    public void setControllerCustomizer(ComponentCustomizer customizer) {
        setCustomizer(customizer);
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.main.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.JudgementResultBean;

/**
 *
 * 個人成績CSVの志望大学を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainB0101CandidateBean extends JudgementResultBean {

    /** 大学コード5桁 */
    private String choiceCd5;

    /** 大学名 */
    private String univName;

    /** 学部名 */
    private String facultyName;

    /** 学科名 */
    private String deptName;

    /**
     * 大学コード5桁を返します。
     *
     * @return 大学コード5桁
     */
    public String getChoiceCd5() {
        return choiceCd5;
    }

    /**
     * 大学コード5桁をセットします。
     *
     * @param choiceCd5 大学コード5桁
     */
    public void setChoiceCd5(String choiceCd5) {
        this.choiceCd5 = choiceCd5;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

}

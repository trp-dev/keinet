package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.view;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZFont;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Font;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Selected;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

/**
 *
 * 一括出力画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class ExmntnPrntView extends SubView {

    /** チェックボックス群========================================================== */
    /** 簡易個人表チェックボックス */
    @Location(x = 20, y = 20)
    @Font(BZFont.BATCH_OUTPUT_ITEM)
    @CodeConfig(Code.BATCH_OUTPUT_KOJIN)
    @Selected
    public CodeCheckBox indvPaperCheckBox;

    /** 簡易個人表1面チェックボックス */
    @Location(x = 25, y = 50)
    @CodeConfig(Code.BATCH_OUTPUT_KOJIN1)
    @Selected
    public CodeCheckBox indvPaper1CheckBox;

    /** 簡易個人表2面チェックボックス */
    @Location(x = 25, y = 184)
    @CodeConfig(Code.BATCH_OUTPUT_KOJIN2)
    @Selected
    public CodeCheckBox indvPaper2CheckBox;

    /** 志望大学リストチェックボックス */
    @Location(x = 241, y = 20)
    @Font(BZFont.BATCH_OUTPUT_ITEM)
    @CodeConfig(Code.BATCH_OUTPUT_CANDIDATE_LIST)
    @Selected
    public CodeCheckBox candidateUnivListCheckBox;

    /** 志望大学詳細チェックボックス */
    @Location(x = 241, y = 181)
    @Font(BZFont.BATCH_OUTPUT_ITEM)
    @CodeConfig(Code.BATCH_OUTPUT_CANDIDATE_DETAIL)
    @Selected
    public CodeCheckBox candidateUnivDetailCheckBox;

    /** 検索結果一覧チェックボックス */
    @Location(x = 462, y = 20)
    @Font(BZFont.BATCH_OUTPUT_ITEM)
    @CodeConfig(Code.BATCH_OUTPUT_SEARCH_RESULT)
    @Selected
    public CodeCheckBox resultListCheckBox;

    /** 連続個人成績表チェックボックス */
    @Location(x = 462, y = 181)
    @Font(BZFont.BATCH_OUTPUT_ITEM)
    @CodeConfig(Code.BATCH_OUTPUT_SCHEDULE)
    @Selected
    public CodeCheckBox exmntnStudentCheckBox;

    /** コンボボックス群========================================================== */
    /** 大学選択コンボボックス */
    @Location(x = 332, y = 229)
    @Size(width = 40, height = 20)
    @NoAction
    public ComboBox<Integer> univComboBox;

    /** 印刷するボタン */
    @Location(x = 547, y = 423)
    public ImageButton printButton;

    @Override
    public void initialize() {
        super.initialize();
        univComboBox.setMaximumRowCount(9);
    }

}

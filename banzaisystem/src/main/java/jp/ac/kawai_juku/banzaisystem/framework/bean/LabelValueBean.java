package jp.ac.kawai_juku.banzaisystem.framework.bean;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * ラベルと値のペアを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class LabelValueBean {

    /** ラベル */
    private String label;

    /** 値 */
    private String value;

    /**
     * ラベルを返します。
     *
     * @return ラベル
     */
    public String getLabel() {
        return label;
    }

    /**
     * ラベルをセットします。
     *
     * @param label ラベル
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * 値を返します。
     *
     * @return 値
     */
    public String getValue() {
        return value;
    }

    /**
     * 値をセットします。
     *
     * @param value 値
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return ObjectUtils.hashCode(this.value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        } else {
            return ObjectUtils.equals(this.value, ((LabelValueBean) obj).value);
        }
    }

    @Override
    public String toString() {
        return ObjectUtils.toString(this.label);
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.main.interceptor;

import static org.seasar.framework.container.SingletonS2Container.getComponent;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.selstd.main.component.SelstdMainTable;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.aopalliance.intercept.MethodInvocation;
import org.seasar.framework.aop.interceptors.AbstractInterceptor;

/**
 *
 * 対象生徒選択処理を行うインターセプタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SelstdMainSelectStudentInterceptor extends AbstractInterceptor {

    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        SelstdMainTable table =
                getComponent(SelstdMainView.class).findVisibleComponentsByClass(SelstdMainTable.class).get(0);
        SystemDto dto = getComponent(SystemDto.class);
        dto.setSelectedIndividualIdList(table.getSelectedIndividualIdList());
        return mi.proceed();
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * セルの文字列のアラインメントを設定するTableCellRendererです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
abstract class AlignedCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (c instanceof JLabel) {
            ((JLabel) c).setHorizontalAlignment(getAlignment());
        }
        return c;
    }

    /**
     * アラインメント設定を返します。
     *
     * @return アラインメント設定
     */
    abstract int getAlignment();

}

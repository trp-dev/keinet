package jp.ac.kawai_juku.banzaisystem.mainte.main.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZTabbedPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.controller.MainteImptController;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.controller.MainteMstrController;
import jp.ac.kawai_juku.banzaisystem.mainte.stng.controller.MainteStngController;

/**
 *
 * メンテナンスタブ画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMainView extends AbstractView {

    /** 対象生徒選択ボタン */
    @Location(x = 281, y = 2)
    public ImageButton selectStudentButton;

    /** 個人成績・志望大学ボタン */
    @Location(x = 385, y = 2)
    public ImageButton exmntnButton;

    /** 帳票出力ボタン */
    @Location(x = 518, y = 2)
    public ImageButton reportButton;

    /** メンテナンスラベル */
    @Location(x = 632, y = 2)
    public ImageLabel maintenanceLabel;

    /** インフォメーションボタン */
    @Location(x = 736, y = 2)
    public ImageButton infoButton;

    /** インフォメーションラベル */
    @Location(x = 866, y = 2)
    public TextLabel informationLabel;

    /** 入試結果入力ボタン */
    @Location(x = 736, y = 2)
    @Invisible
    public ImageButton resultButton;

    /** ヘルプボタン */
    @Location(x = 854, y = 2)
    @Invisible
    public ImageButton helpButton;

    /** 終了ボタン */
    @Location(x = 928, y = 2)
    public ImageButton exitButton;

    /** タブ */
    @Location(x = 4, y = 31)
    @TabConfig({ MainteMstrController.class, MainteImptController.class, MainteStngController.class })
    public BZTabbedPane tabbedPane;

}

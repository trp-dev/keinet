package jp.ac.kawai_juku.banzaisystem.exmntn.main.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainCheckHelper;
import jp.ac.kawai_juku.banzaisystem.framework.component.CheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.seasar.framework.container.SingletonS2Container;

/**
 *
 * 個人成績画面の科目チェックボックスです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 *
 */
@SuppressWarnings("serial")
public class ExmntnMainSubjectCheckBox extends CheckBox {

    /** フィールド名の接尾辞 */
    private static final String SUFFIX = "CheckBox";

    @Override
    public void initialize(Field field, AbstractView view) {

        if (!field.getName().endsWith(SUFFIX)) {
            throw new RuntimeException(String.format("%sのフィールド名は[***%s]とする必要があります。[%s]", getClass().getSimpleName(),
                    SUFFIX, field.getName()));
        }

        super.initialize(field, view);

        addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent arg0) {
                /* 初期値に戻すボタンを有効化する */
                ExmntnMainCheckHelper helper = SingletonS2Container.getComponent(ExmntnMainCheckHelper.class);
                if (helper.needCheck(false)) {
                    helper.enableReturnInitButton(false);
                }
            }
        });

        setText(getMessage("label." + view.getPkg() + "."
                + field.getName().substring(0, field.getName().length() - SUFFIX.length())));

        setSize(getPreferredSize());
    }

}

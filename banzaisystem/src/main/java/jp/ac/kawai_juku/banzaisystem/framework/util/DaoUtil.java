package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * DAOに関するユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class DaoUtil {

    /**
     * コンストラクタです。
     */
    private DaoUtil() {
    }

    /**
     * 指定したパラメータでSQLのIN句を生成します。<br>
     * {@code ["1", "2"] → IN ('1','2')}
     *
     * @param columnName カラム名
     * @param values INに指定する値のリスト
     * @return IN句
     */
    public static String createIn(String columnName, List<?> values) {
        return createInOrNotIn(columnName, values, false);
    }

    /**
     * 指定したパラメータでSQLのNOT IN句を生成します。<br>
     * {@code ["1", "2"] → NOT IN ('1','2')}
     *
     * @param columnName カラム名
     * @param values NOT INに指定する値のリスト
     * @return IN句
     */
    public static String createNotIn(String columnName, List<?> values) {
        return createInOrNotIn(columnName, values, true);
    }

    /**
     * IN もしくは NOT IN句を生成します。
     *
     * @param columnName カラム名
     * @param values 条件に設定する値のリスト
     * @param isNotIn 「NOT IN」フラグ
     * @return 生成したSQLリテラル
     */
    private static String createInOrNotIn(String columnName, List<?> values, boolean isNotIn) {

        if (values.isEmpty()) {
            return null;
        }

        boolean hasNull = false;
        StringBuilder in = new StringBuilder(16 * values.size());
        for (Object value : values) {
            if (value == null) {
                hasNull = true;
                continue;
            }
            if (in.length() > 0) {
                in.append(',');
            }
            if (value instanceof Number) {
                in.append(value);
            } else {
                in.append("'");
                in.append(value);
                in.append("'");
            }
        }

        StringBuilder sql = new StringBuilder(in.length() + 128);
        sql.append('(');

        if (in.length() > 0) {
            sql.append(columnName).append(' ').append(isNotIn ? "NOT IN" : "IN").append(" (").append(in).append(')');
        }

        if (hasNull) {
            if (in.length() > 0) {
                sql.append(isNotIn ? " AND " : " OR ");
            }
            sql.append(columnName).append(isNotIn ? " IS NOT NULL" : " IS NULL");
        }

        sql.append(')');

        return sql.toString();
    }

    /**
     * 大学リスト取得のWHERE句を生成します。
     *
     * @param univList 大学キーリスト
     * @return WHERE句
     */
    public static String createUnivWhere(List<? extends UnivKeyBean> univList) {
        StringBuffer sb = new StringBuffer();
        for (UnivKeyBean bean : univList) {
            if (sb.length() == 0) {
                sb.append("WHERE ");
            } else {
                sb.append("OR ");
            }
            sb.append("(UNIVCD = '");
            sb.append(bean.getUnivCd());
            sb.append("' AND FACULTYCD = '");
            sb.append(bean.getFacultyCd());
            sb.append("' AND DEPTCD = '");
            sb.append(bean.getDeptCd());
            sb.append("')");
        }
        return sb.toString();
    }

    /**
     * 大学マスタ情報を取得します。
     *
     * @param con DBコネクション
     * @return {@link UnivMasterBean}
     * @throws SQLException SQL例外
     */
    public static UnivMasterBean findUnivMasterBean(Connection con) throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("SELECT EVENTYEAR, EXAMDIV, REQUIRED_VERSION FROM UNIVMASTER")) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return new UnivMasterBean(rs.getString(1), rs.getString(2), rs.getString(3));
                } else {
                    throw new SQLException("大学マスタ情報が存在しません。");
                }
            }
        }
    }

    /**
     * 成績データバージョンを取得します。
     *
     * @param con DBコネクション
     * @return 成績データバージョン
     * @throws SQLException 成績データバージョンの取得に失敗した場合
     */
    public static int getRecordVersion(Connection con) throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("SELECT VERSION FROM RECORDINFO")) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    throw new SQLException("成績データ情報が存在しません。");
                }
            }
        }
    }

}

package jp.ac.kawai_juku.banzaisystem;

/**
 *
 * 入試日程詳細の日付の種類を識別するためのenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum ScheduleType {

    /** 入試願書締切日(消印有効) */
    APPLIDEADLINE_POST,

    /** 入試願書締切日(必着) */
    APPLIDEADLINE_NLT,

    /** 入試願書締切日(窓口のみ) */
    APPLIDEADLINE_DESK,

    /** 入試願書締切日(ネットのみ) */
    APPLIDEADLINE_NET,

    /** 入試願書締切日(不明) */
    APPLIDEADLINE_UNK,

    /** 入試日(本学のみ) */
    INPLEDATE_HOME,

    /** 入試日(本学または地方) */
    INPLEDATE_HOME_OR_LOCAL,

    /** 入試日(地方のみ) */
    INPLEDATE_LOCAL,

    /** 入試合格発表日 */
    SUCANNDATE,

    /** 入試手続締切日 */
    PROCEDEADLINE,

    /** 日程重複 */
    DUPLICATION;

    /** 入試日フラグ */
    private final boolean inpleDateFlg = toString().startsWith("INPLEDATE");

    /**
     * 入試日の {@link ScheduleType} であるかを返します。
     *
     * @return 入試日の {@link ScheduleType} ならtrue
     */
    public boolean isInpleDate() {
        return inpleDateFlg;
    }

}

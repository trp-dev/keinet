package jp.ac.kawai_juku.banzaisystem.result.univ.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellEditor;
import jp.ac.kawai_juku.banzaisystem.framework.component.RadioButtonCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CenterAlignedCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CodeComboCellEditor;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CodeComboCellRenderer;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivExamineeBean;

/**
 *
 * 受験者一覧テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ResultUnivExamineeTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(250, 180, 121);

    /** カラム幅定義 */
    private static final int[] COL_WIDTH = { 39, 34, 103, 62, 89, 88, 89, 115, 125, 41, 180 };

    /** カラム×コード種別対応テーブル */
    private static final CodeType[] CODETYPE_TABLE = { null, null, null, null, null, null, null, CodeType.RESULT_ENTEXAMMODE, CodeType.RESULT_RESULTDIV, CodeType.RESULT_ADMISSIONDIV, null };

    /** ボタン列インデックス */
    private static final int BUTTON_COL_INDEX = COL_WIDTH.length - 1;

    /** 学部学科変更ボタン（レンダラ用） */
    private final ImageButton changeRendererButton = new ImageButton();

    /** 削除ボタン（レンダラ用） */
    private final ImageButton deleteRendererButton = new ImageButton();

    /** 学部学科変更ボタン（エディタ用） */
    private final ImageButton changeEditorButton = new ImageButton();

    /** 削除ボタン（エディタ用） */
    private final ImageButton deleteEditorButton = new ImageButton();

    /** ボタン押下行のインデックス */
    private int buttonRowIndex;

    /**
     * コンストラクタです。
     */
    public ResultUnivExamineeTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        changeRendererButton.initialize(view, "changeButton");
        deleteRendererButton.initialize(view, "deleteButton");
        changeEditorButton.initialize(view, "changeButton");
        deleteEditorButton.initialize(view, "deleteButton");
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        final JTable table = new BZTable(new ReadOnlyTableModel(COL_WIDTH.length + 1)) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                if (c instanceof JLabel) {
                    ((JLabel) c).setBorder(column >= 2 && column != 3 ? BorderFactory.createEmptyBorder(0, 5, 0, 0) : null);
                }
                return c;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                /* コンボボックス列とボタン列のみ編集可能とする */
                return CODETYPE_TABLE[column] != null || column == BUTTON_COL_INDEX;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int i = 0; i < COL_WIDTH.length; i++) {
                    x += COL_WIDTH[i];
                    if (i == 6 || i >= 9) {
                        g.drawLine(x - 1, 0, x - 1, getHeight());
                    }
                }
            }
        };

        table.setFocusable(false);
        table.setRowHeight(30);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setRowSelectionAllowed(false);

        /* マウスカーソルの形を変えるため、マウスカーソルが乗ったボタンセルを編集状態にする */
        table.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int column = table.columnAtPoint(e.getPoint());
                if (column == BUTTON_COL_INDEX && (column != table.getEditingColumn() || row != table.getEditingRow())) {
                    table.editingCanceled(null);
                    table.editCellAt(row, column);
                }
            }
        });

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            TableColumn column = columnModel.getColumn(i);
            column.setPreferredWidth(COL_WIDTH[i]);
            CodeType codeType = CODETYPE_TABLE[i];
            if (i <= 1 || i == 3) {
                column.setCellRenderer(new CenterAlignedCellRenderer());
            } else if (i == 9) {
                column.setCellRenderer(new RadioButtonCellRenderer());
                column.setCellEditor(new RadioButtonCellEditor(SEPARATOR_COLOR));
            } else if (codeType != null) {
                column.setCellRenderer(new CodeComboCellRenderer(codeType, COL_WIDTH[i], table.getRowHeight()));
                column.setCellEditor(new CodeComboCellEditor(codeType, COL_WIDTH[i], table.getRowHeight()));
            }
        }

        columnModel.getColumn(BUTTON_COL_INDEX).setCellRenderer(new ButtonCellRenderer());
        columnModel.getColumn(BUTTON_COL_INDEX).setCellEditor(new ButtonCellEditor());

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public void setDataList(Vector<Vector<Object>> dataList) {
        /* 編集を解除しないと、編集中の行が無くなった場合にテーブルクリックでエラーになる */
        getTable().editingCanceled(null);
        ((BZTableModel) getTable().getModel()).setDataVector(dataList);
    }

    /**
     * ボタン押下行の受験者情報を返します。
     *
     * @return {@link ResultUnivExamineeBean}
     */
    public ResultUnivExamineeBean getExamineeBean() {
        return (ResultUnivExamineeBean) getTable().getModel().getValueAt(buttonRowIndex, 0);
    }

    /** ボタンセルのレンダラ */
    private final class ButtonCellRenderer extends JPanel implements TableCellRenderer {

        /**
         * コンストラクタです。
         */
        ButtonCellRenderer() {
            super(new FlowLayout(FlowLayout.LEFT, 7, 4));
            add(changeRendererButton);
            add(deleteRendererButton);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
            return this;
        }

    }

    /** ボタンセルのエディタ */
    private final class ButtonCellEditor extends AbstractCellEditor implements TableCellEditor {

        /** パネル */
        private final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 7, 4));

        /**
         * コンストラクタです。
         */
        private ButtonCellEditor() {
            panel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, SEPARATOR_COLOR));
            panel.add(changeEditorButton);
            panel.add(deleteEditorButton);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            panel.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
            buttonRowIndex = row;
            return panel;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }

        @Override
        public boolean shouldSelectCell(EventObject anEvent) {
            return false;
        }

    }

}

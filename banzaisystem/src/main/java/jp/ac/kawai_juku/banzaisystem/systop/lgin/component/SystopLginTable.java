package jp.ac.kawai_juku.banzaisystem.systop.lgin.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.lang.reflect.Field;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * 大学マスタダウンロード・個人成績インポートテーブルです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
@SuppressWarnings("serial")
public class SystopLginTable extends TableScrollPane implements BZComponent {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** 列幅定義 */
    private static final int[] COLUMN_WIDTH = { 208, 112, 111 };

    /**
     * コンストラクタです。
     */
    public SystopLginTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(null);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setViewportView(createTableView());
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        ReadOnlyTableModel tableModel = new ReadOnlyTableModel(3);

        JTable table = new BZTable(tableModel) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                if (c instanceof JLabel) {
                    JLabel label = (JLabel) c;
                    label.setBorder(column == 0 ? new EmptyBorder(0, 10, 0, 0) : null);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int width : COLUMN_WIDTH) {
                    x += width;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(22);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);
        table.setRowSelectionAllowed(false);

        /* 列の幅をヘッダの列と合わせる */
        TableColumnModel columnModel = table.getColumnModel();
        for (int i = 0; i < COLUMN_WIDTH.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(COLUMN_WIDTH[i]);
        }

        /* セルレンダラをセットする */
        columnModel.getColumn(1).setCellRenderer(new SystopLginUnivCellRenderer());
        columnModel.getColumn(2).setCellRenderer(new SystopLginRecordCellRenderer());

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

    /**
     * データリストをセットします。
     *
     * @param dataList データリスト
     */
    public final void setDataList(Vector<Vector<Object>> dataList) {
        ((BZTableModel) getTable().getModel()).setDataVector(dataList);
    }

}

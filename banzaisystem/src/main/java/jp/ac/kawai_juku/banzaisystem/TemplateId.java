package jp.ac.kawai_juku.banzaisystem;

/**
 *
 *  [ev[gIDπΗ·ιenumΕ·B
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @author TOTEC)FURUZAWA.Yusuke
 *
 */
public enum TemplateId implements PrintId {

    /** Βl¬ΡEu]κ(Βl¬Ρκ) [ */
    A01_01,

    /** Βl¬ΡEu]κ(u]εwγΚ9εw) */
    A01_02,

    /** Βl¬ΡEu]κ(u]εwγΚ20εw) */
    A01_03,

    /** Βl¬ΡEu]κ(A±Βl¬Ρ\) */
    A01_04,

    /** u]εwΪΧ */
    A02_01,

    /** ΘΥΒl\ */
    A03_01,

    /** εwυΚ */
    A04_01,

    /** u]εwXg */
    A05_01,

    /** σ±XPW[ */
    A06_01,

}

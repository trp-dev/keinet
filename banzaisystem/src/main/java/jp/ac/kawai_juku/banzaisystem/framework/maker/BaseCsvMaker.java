package jp.ac.kawai_juku.banzaisystem.framework.maker;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.CsvMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.csv.CsvWriter;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.seasar.framework.exception.IORuntimeException;

/**
 *
 * CSV帳票メーカーの基底クラスです。
 *
 *
 * @param <T> 入力Beanの型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class BaseCsvMaker<T extends CsvMakerInBean> implements FormMaker<T> {

    /** CSVファイル */
    private File file;

    /** CsvWriter */
    private CsvWriter writer;

    /** ダブルクオートフラグ */
    private final boolean quoteFlg;

    /**
     * コンストラクタです。
     *
     * @param quoteFlg ダブルクオートフラグ
     */
    public BaseCsvMaker(boolean quoteFlg) {
        this.quoteFlg = quoteFlg;
    }

    /**
     * コンストラクタです。
     */
    public BaseCsvMaker() {
        this(false);
    }

    @Override
    public List<File> create(T inBean) {
        try {
            outputData(inBean);
            if (file != null) {
                return Collections.singletonList(file);
            } else {
                return Collections.emptyList();
            }
        } catch (IORuntimeException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E005"), e);
        } finally {
            IOUtil.close(writer);
        }
    }

    /**
     * CSVの列を追加します。
     *
     * @param column 列の文字列
     */
    protected final void addColumn(String column) {
        getWriter().addColumn(column);
    }

    /**
     * CSVの列を複数追加します。
     *
     * @param columns 列の文字列配列
     */
    protected final void addColumns(String[] columns) {
        getWriter().addColumns(columns);
    }

    /**
     * CSVの列を複数追加します。
     *
     * @param columns 列の文字列リスト
     */
    protected final void addColumns(List<String> columns) {
        getWriter().addColumns(columns);
    }

    /**
     * CSVデータ1行を出力先に書き込みます。<br>
     * 書き込んだ後、addで追加された列情報はクリアします。
     */
    protected final void addRow() {
        getWriter().addRow();
    }

    /**
     * CsvWriterを返します。
     *
     * @return CsvWriter
     */
    private CsvWriter getWriter() {
        try {
            if (writer == null) {
                file = createCsvFile();
                file.deleteOnExit();
                writer = new CsvWriter(new FileOutputStream(file));
                writer.setQuoteFlg(quoteFlg);
            }
        } catch (FileNotFoundException e) {
            throw new IORuntimeException(e);
        }
        return writer;
    }

    /**
     * CSVファイルオブジェクトを生成します。
     *
     * @return ファイル
     */
    private File createCsvFile() {
        try {
            return new File(SettingPropsUtil.getOutputDir(), getPrintId() + "_"
                    + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS") + ".csv");
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * データをCSVファイルに出力します。
     *
     * @param inBean {@link CsvMakerInBean}
     */
    protected abstract void outputData(T inBean);

}

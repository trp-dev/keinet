package jp.ac.kawai_juku.banzaisystem.framework;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.lang.Thread.UncaughtExceptionHandler;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZFrame;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.exception.RecordInfoMismatchException;
import jp.ac.kawai_juku.banzaisystem.framework.exception.UnivMasterMismatchException;
import jp.ac.kawai_juku.banzaisystem.framework.service.LogService;
import jp.ac.kawai_juku.banzaisystem.framework.util.ControllerUtil;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;
import jp.ac.kawai_juku.banzaisystem.systop.strt.controller.SystopStrtController;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * バンザイシステムのメインクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZMain {

    /**
     * コンストラクタです。
     */
    private BZMain() {
    }

    /**
     * メインメソッドです。
     *
     * @param args 引数
     */
    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandlerImpl());
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                WindowManager.initialize(BZFrame.class, SystopStrtController.class);
            }
        });
    }

    /** 例外ハンドラの実装です */
    private static final class UncaughtExceptionHandlerImpl implements UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            try {
                processUncaughtException(t, e);
            } catch (Throwable ex) {
                System.err.println("例外ハンドラの処理中にエラーが発生しました。");
                System.err.println("------------------------------ 例外ハンドラ内で発生した例外 ------------------------------");
                ex.printStackTrace();
                System.err.println("------------------------------ 例外ハンドラが処理していた例外 ------------------------------");
                e.printStackTrace();
            }
        }

        /**
         * 未キャッチ例外を処理します。
         *
         * @param t スレッド
         * @param e 例外
         */
        private void processUncaughtException(Thread t, Throwable e) {
            if (SwingUtilities.isEventDispatchThread()) {
                int index = ExceptionUtils.indexOfType(e, MessageDialogException.class);
                if (index > -1) {
                    MessageDialogException mde = (MessageDialogException) ExceptionUtils.getThrowableList(e).get(index);
                    if (mde.getCause() != null) {
                        outputErrorLog("エラーが発生しました。", mde.getCause());
                    }
                    showMessageDialog(mde.getMessage());
                    if (mde instanceof UnivMasterMismatchException || mde instanceof RecordInfoMismatchException) {
                        WindowManager.getInstance().closeAllDialog();
                        ControllerUtil.invoke(SystopStrtController.class, null);
                    }
                } else {
                    String message = getMessage("error.framework.CM_E009");
                    outputErrorLog(message, e);
                    showMessageDialog(message);
                }
            } else {
                e.printStackTrace();
            }
        }

        /**
         * エラーログを出力します。
         *
         * @param message エラーメッセージ
         * @param e 例外
         */
        private void outputErrorLog(String message, Throwable e) {
            getComponent(LogService.class).error(message, e);
        }

        /**
         * メッセージダイアログを表示します。
         *
         * @param message メッセージ
         */
        private void showMessageDialog(String message) {
            JOptionPane.showMessageDialog(WindowManager.getInstance().getActiveWindow(), message);
        }

    }

}

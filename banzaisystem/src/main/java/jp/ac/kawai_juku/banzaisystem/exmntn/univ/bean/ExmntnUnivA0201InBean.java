package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;

import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * Excel帳票メーカー(志望大学詳細)の入力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)OZEKI.Hiroshige
 *
 */
public class ExmntnUnivA0201InBean implements ExcelMakerInBean {

    /** 生徒情報Bean */
    private final ExmntnUnivStudentBean studentBean;

    /** 志望順位と志望大学情報Beanのペアリスト */
    private final List<Pair<String, ExmntnMainCandidateUnivBean>> pairList;

    /** 成績データ＋選択科目情報 */
    private final ScoreBean scoreBean;

    /**
     * コンストラクタです。
     *
     * @param studentBean 生徒情報Bean
     * @param scoreBean 成績データ＋選択科目情報
     * @param pairList 志望順位と志望大学情報Beanのペアリスト
     */
    public ExmntnUnivA0201InBean(ExmntnUnivStudentBean studentBean, ScoreBean scoreBean,
            List<Pair<String, ExmntnMainCandidateUnivBean>> pairList) {
        this.studentBean = studentBean;
        this.scoreBean = scoreBean;
        this.pairList = pairList;
    }

    /**
     * 生徒情報Beanを返します。
     *
     * @return 生徒情報Bean
     */
    public ExmntnUnivStudentBean getStudentBean() {
        return studentBean;
    }

    /**
     * 成績データ＋選択科目情報を返します。
     *
     * @return 成績データ＋選択科目情報
     */
    public ScoreBean getScoreBean() {
        return scoreBean;
    }

    /**
     * 志望順位と志望大学情報Beanのペアリストを返します。
     *
     * @return 志望順位と志望大学情報Beanのペアリスト
     */
    public List<Pair<String, ExmntnMainCandidateUnivBean>> getPairList() {
        return pairList;
    }

}

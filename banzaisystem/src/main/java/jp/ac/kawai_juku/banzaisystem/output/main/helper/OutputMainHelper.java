package jp.ac.kawai_juku.banzaisystem.output.main.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;
import jp.ac.kawai_juku.banzaisystem.output.main.bean.OutputMainB0401CsvInBean;
import jp.ac.kawai_juku.banzaisystem.output.main.component.A0104Node;
import jp.ac.kawai_juku.banzaisystem.output.main.service.OutputMainImportB0401Service;
import jp.ac.kawai_juku.banzaisystem.output.main.view.OutputMainView;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.service.SelstdDprtService;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.dto.SelstdDstdDto;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.service.SelstdDstdService;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 帳票出力・CSVファイル画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class OutputMainHelper {

    /** View */
    @Resource(name = "outputMainView")
    private OutputMainView view;

    /** 志望大学・評価一覧CSV取り込みのサービス */
    @Resource
    private OutputMainImportB0401Service outputMainImportB0401Service;

    /** 一覧出力ダイアログのサービス */
    @Resource(name = "selstdDprtService")
    private SelstdDprtService service;

    /** 生徒一覧ダイアログのDTO */
    @Resource
    private SelstdDstdDto selstdDstdDto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 生徒一覧ダイアログのサービス */
    @Resource
    private SelstdDstdService selstdDstdService;

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public ExamBean findTargetExam() {
        return view.examComboBox.getSelectedValue();
    }

    /**
     * 対象模試のドッキング先模試を返します。
     *
     * @return 対象模試のドッキング先模試
     */
    public ExamBean findDockingExam() {
        return ExamUtil.findDockingExam(view.examComboBox);
    }

    /**
     * 印刷対象の個人IDリストを返します。
     *
     * @return 個人IDリスト
     */
    public List<Integer> findPrintTargetList() {
        if (!selstdDstdDto.getSelectedIndividualIdList().isEmpty()) {
            /* 生徒一覧ダイアログで選択されている場合 */
            return selstdDstdDto.getSelectedIndividualIdList();
        } else {
            /* 画面の条件で検索する */
            List<Integer> list = CollectionsUtil.newArrayList();
            for (Map<String, CellData> map : selstdDstdService.findKojinList(view.gradeComboBox.getSelectedValue(), view.classTable.getSelectedClassList(), view.numberStartField.getText(), view.numberEndField.getText())) {
                list.add((Integer) map.get("id").getSortValue());
            }
            return list;
        }
    }

    /**
     * 志望大学・評価一覧CSV出力処理の入力Beanを生成します。
     *
     * @return {@link OutputMainB0401CsvInBean}
     */
    public OutputMainB0401CsvInBean createB0401CsvInBean() {

        List<String> univCdList = null;
        if (!view.univList.getUnivList().isEmpty()) {
            univCdList = new ArrayList<>(view.univList.getUnivList().size());
            for (SelstdDunvBean bean : view.univList.getUnivList()) {
                univCdList.add(bean.getUnivCd());
            }
        }

        return new OutputMainB0401CsvInBean(findTargetExam(), findDockingExam(), findPrintTargetList(), view.ratingACheckBox.isSelected(), view.ratingBCheckBox.isSelected(), view.ratingCCheckBox.isSelected(),
                view.ratingDCheckBox.isSelected(), view.ratingECheckBox.isSelected(), view.univDivRadio.isSelected(), view.univNameRadio.isSelected(), view.nationalCheckBox.isSelected(), view.centerCheckBox.isSelected(),
                view.privateCheckBox.isSelected(), view.collegeCheckBox.isSelected(), univCdList);
    }

    /**
     * 志望大学・評価一覧CSVを取り込みます。
     */
    public void importB0401() {
        outputMainImportB0401Service.importCsv(new File(view.csvFileField.getText()));
    }

    /**
    * 学年コンボボックスを初期化します。
    */
    public void initGradeComboBox() {
        if (view.selectedNode instanceof A0104Node) {
            List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
            for (Integer grade : service.findGradeListAll(systemDto.getRecordInfo().getYear())) {
                itemList.add(new ComboBoxItem<Integer>(grade));
            }
            if (itemList.size() == 0) {
                return;
            } else {
                view.gradeComboBox.setItemList(itemList);
            }
        } else {
            ExamBean exam = null;
            if (view.examComboBox == null) {

            } else {
                exam = view.examComboBox.getSelectedValue();
            }
            if (exam == null) {
                view.gradeComboBox.setItemList(Collections.<ComboBoxItem<Integer>> emptyList());
            } else {
                List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
                for (Integer grade : service.findGradeList(exam)) {
                    itemList.add(new ComboBoxItem<Integer>(grade));
                }
                if (view.gradeComboBox.getItemCount() == 0) {
                    /* 初回のみ模試の対象学年を初期値とする */
                    view.gradeComboBox.setItemList(itemList, new ComboBoxItem<Integer>(exam.getTargetGrade()));
                } else {
                    view.gradeComboBox.setItemList(itemList);
                }
            }
        }
    }

    /**
     * クラステーブルを初期化します。
     */
    public void initClassTable() {
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (view.selectedNode instanceof A0104Node) {
            if (grade == null) {
                view.classTable.setClassListAll(grade, Collections.<String> emptyList());
            } else {
                view.classTable.setClassListAll(grade, service.findClassListAll(systemDto.getRecordInfo().getYear(), grade.getValue()));
            }
        } else {
            ComboBoxItem<ExamBean> exam = view.examComboBox.getSelectedItem();
            if (exam == null || grade == null) {
                view.classTable.setClassList(exam, grade, Collections.<String> emptyList());
            } else {
                view.classTable.setClassList(exam, grade, service.findClassList(exam.getValue(), grade.getValue()));
            }
            view.univList.clearUnivList(exam, grade);
        }
        changePrintButtonStatus();
    }

    /**
    * 印刷ボタンの状態を変更します。
    */
    public void changePrintButtonStatus() {
        /* クラス未選択か席次番号が空欄なら印刷ボタンを無効にする */
        view.printButton.setEnabled(!view.classTable.getSelectedClassList().isEmpty() && (!view.numberStartField.getText().isEmpty() || !view.numberEndField.getText().isEmpty()));
        /* 「生徒一覧から選択」で選択した生徒をクリアする */
        selstdDstdDto.getSelectedIndividualIdList().clear();
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean;

/**
 *
 * 志願者内訳情報を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistCandidateDetailBean {

    /** 志望人数−合計 */
    private Integer numbersA = Integer.valueOf(0);

    /** 志望人数−現役 */
    private Integer numbersS = Integer.valueOf(0);

    /** 志望人数−高卒 */
    private Integer numbersG = Integer.valueOf(0);

    /** 平均換算得点/平均偏差値−合計 */
    private Double avgPntA = Double.valueOf(0);

    /** 平均換算得点/平均偏差値−現役 */
    private Double avgPntS = Double.valueOf(0);

    /** 平均換算得点/平均偏差値−高卒 */
    private Double avgPntG = Double.valueOf(0);

    /**
     * 志望人数−合計を返します。
     *
     * @return 志望人数−合計
     */
    public Integer getNumbersA() {
        return numbersA;
    }

    /**
     * 志望人数−合計をセットします。
     *
     * @param numbersA 志望人数−合計
     */
    public void setNumbersA(Integer numbersA) {
        this.numbersA = numbersA;
    }

    /**
     * 志望人数−現役を返します。
     *
     * @return 志望人数−現役
     */
    public Integer getNumbersS() {
        return numbersS;
    }

    /**
     * 志望人数−現役をセットします。
     *
     * @param numbersS 志望人数−現役
     */
    public void setNumbersS(Integer numbersS) {
        this.numbersS = numbersS;
    }

    /**
     * 志望人数−高卒を返します。
     *
     * @return 志望人数−高卒
     */
    public Integer getNumbersG() {
        return numbersG;
    }

    /**
     * 志望人数−高卒をセットします。
     *
     * @param numbersG 志望人数−高卒
     */
    public void setNumbersG(Integer numbersG) {
        this.numbersG = numbersG;
    }

    /**
     * 平均換算得点/平均偏差値−合計を返します。
     *
     * @return 平均換算得点/平均偏差値−合計
     */
    public Double getAvgPntA() {
        return avgPntA;
    }

    /**
     * 平均換算得点/平均偏差値−合計をセットします。
     *
     * @param avgPntA 平均換算得点/平均偏差値−合計
     */
    public void setAvgPntA(Double avgPntA) {
        this.avgPntA = avgPntA;
    }

    /**
     * 平均換算得点/平均偏差値−現役を返します。
     *
     * @return 平均換算得点/平均偏差値−現役
     */
    public Double getAvgPntS() {
        return avgPntS;
    }

    /**
     * 平均換算得点/平均偏差値−現役をセットします。
     *
     * @param avgPntS 平均換算得点/平均偏差値−現役
     */
    public void setAvgPntS(Double avgPntS) {
        this.avgPntS = avgPntS;
    }

    /**
     * 平均換算得点/平均偏差値−高卒を返します。
     *
     * @return 平均換算得点/平均偏差値−高卒
     */
    public Double getAvgPntG() {
        return avgPntG;
    }

    /**
     * 平均換算得点/平均偏差値−高卒をセットします。
     *
     * @param avgPntG 平均換算得点/平均偏差値−高卒
     */
    public void setAvgPntG(Double avgPntG) {
        this.avgPntG = avgPntG;
    }

}

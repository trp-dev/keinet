package jp.ac.kawai_juku.banzaisystem.framework.service;

import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.ac.kawai_juku.banzaisystem.framework.util.ByteUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.MessageDigestUtil;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * パスワードサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PasswordService {

    /**
     * 指定したキーのパスワードが設定ファイルの設定値と一致しているかをチェックします。
     *
     * @param key {@link SettingKey}
     * @param password パスワード
     * @return 設定値と一致しているならtrue
     */
    public boolean checkPassword(SettingKey key, String password) {
        return SettingProps.load().getValue(key).equals(toHash(key, password));
    }

    /**
     * パスワードをチェック用のハッシュ文字列に変換します。
     *
     * @param key {@link SettingKey}
     * @param password パスワード
     * @return ハッシュ文字列
     */
    public String toHash(SettingKey key, String password) {
        return StringUtils.reverse(ByteUtil.toHexString(MessageDigestUtil.sha256(key + password)));
    }

    /**
     * 指定したキーのパスワードを設定ファイルに保存します。
     *
     * @param key {@link SettingKey}
     * @param password パスワード
     */
    public void savePassword(SettingKey key, String password) {
        SettingProps props = SettingProps.load();
        props.setValue(key, toHash(key, password));
        props.store();
    }

}

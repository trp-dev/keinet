package jp.ac.kawai_juku.banzaisystem;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * コード種別を識別するためのenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum CodeType {

    /** 対象生徒選択-表示項目 */
    SELECT_STUDENT_DISP_ITEM,

    /** 一覧出力-対象概要 */
    OUTPUT_LIST_TARGET,

    /** 一覧出力-出力範囲 */
    OUTPUT_LIST_RANGE,

    /** 志望集計パターン */
    CANDIDATE_COUNT_PTN,

    /** 検索-区分 */
    UNIV_SEARCH_DIV,

    /** 検索-女子大 */
    UNIV_SEARCH_FEMAIL,

    /** 検索-夜間 */
    UNIV_SEARCH_NIGHT,

    /** 検索-入試（科目・日程・評価） */
    UNIV_SEARCH_EXAM,

    /** 検索-評価範囲 */
    UNIV_SEARCH_RANGE,

    /** 検索-評価範囲（評価） */
    UNIV_SEARCH_RANGE_VALUE,

    /** 検索-入試日程(国公立) */
    UNIV_SEARCH_SCHEDULE,

    /** 検索-入試日程(私立) */
    UNIV_SEARCH_SCHEDULE2,

    /** 検索-使用科目（課さない） */
    UNIV_SEARCH_NOT_IMPOSED,

    /** 検索-センター個別 配点比 */
    UNIV_SEARCH_RATE,

    /** 検索結果-国公立 */
    UNIV_RESULT_UDIV,

    /** 検索結果-私立短大 */
    UNIV_RESULT_PDIV,

    /** 検索結果-大学区分 */
    UNIV_RESULT_UNIV_DIV,

    /** 志望大学テーブル-区分 */
    CANDIDATE_UNIV_SECTION,

    /** 学部系統-中系統 */
    M_STEMMA,

    /** 学部系統-小系統 */
    S_STEMMA,

    /** 学部系統-分野 */
    REGION,

    /** 資格カテゴリ */
    CREDENTIAL_CATEGORY,

    /** 資格 */
    CREDENTIAL,

    /** 受験スケジュール：並べ替え */
    SCHEDULE_ORDER,

    /** 入試本学地方区分 */
    SCHOOLPROVENTDIV,

    /** 入試出願締切区分 */
    APPLIDEADLINEDIV,

    /** 一括出力 */
    BATCH_OUTPUT,

    /** 帳票出力・CSVファイル：判定評価 */
    OUTPUT_RATING,

    /** 帳票出力・CSVファイル：大学指定 */
    OUTPUT_UNIV_CHOICE,

    /** 帳票出力・CSVファイル：大学区分 */
    OUTPUT_UNIV_DIV,

    /** プログラム更新状況 */
    PG_UPDATE_STATUS,

    /** 大学マスタダウンロード状況 */
    UNIV_DL_STATUS,

    /** 個人成績インポート状況 */
    RECORD_IMP_STATUS,

    /** メンテナンス-インターネット接続 */
    MAINTE_INET,

    /** 検索-英語認定試験 */
    SEARCH_ENG_JOINING,

    /** 入試結果入力：入試形態区分 */
    RESULT_ENTEXAMMODE,

    /** 入試結果入力：結果区分 */
    RESULT_RESULTDIV,

    /** 入試結果入力：入学区分 */
    RESULT_ADMISSIONDIV,

    /** 英語認定試験入力 */
    ENG_JOINING_INPUT,

    /** CSV取込：大学コード */
    CSV_IMPORT_UNIV_CODE,

    /** CSV取込：取込データ */
    CSV_IMPORT_TYPE;

    /** コードリスト */
    private List<Code> codeList;

    /**
     * コード種別名を返します。
     *
     * @return コード種別名
     */
    public String getName() {
        return getMessage("codeType." + toString());
    }

    /**
     * コードリストを返します。
     *
     * @return コードリスト
     */
    public List<Code> findCodeList() {
        if (codeList == null) {
            codeList = new ArrayList<>();
            for (Code code : Code.values()) {
                if (code.getCodeType() == this) {
                    codeList.add(code);
                }
            }
        }
        return codeList;
    }

    /**
     * コード値からコードに変換します。
     *
     * @param value コード値
     * @return コード
     */
    public Code value2Code(String value) {
        for (Code code : findCodeList()) {
            if (code.getValue().equals(value)) {
                return code;
            }
        }
        throw new RuntimeException(String.format("コードへの変換に失敗しました。[%s][%s]", this, value));
    }

}

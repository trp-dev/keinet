package jp.ac.kawai_juku.banzaisystem.framework.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import jp.ac.kawai_juku.banzaisystem.framework.http.response.AppVersionResponseBean;
import jp.ac.kawai_juku.banzaisystem.framework.http.response.AppVersionResponseHandler;
import jp.ac.kawai_juku.banzaisystem.framework.http.response.DbVersionResponseBean;
import jp.ac.kawai_juku.banzaisystem.framework.http.response.DbVersionResponseHandler;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingProps;
import jp.ac.kawai_juku.banzaisystem.framework.util.IOUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.TempFileUtil;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet.UnivDb;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.log.Logger;
import org.seasar.framework.util.FileOutputStreamUtil;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.URLUtil;
import org.seasar.framework.util.ZipFileUtil;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * 更新機能のHTTPクライアントです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class UpdaterHttpClient {

    /** Logger */
    private static final Logger logger = Logger.getLogger(UpdaterHttpClient.class);

    /** 更新データ配信サイトURL */
    private final URL updaterSiteUrl;

    /** 認証情報 */
    private final Authenticator authenticator;

    /** 接続タイムアウト */
    private final int connectTimeout = 10000;

    /**
     * コンストラクタです。
     */
    public UpdaterHttpClient() {
        /* 設定ファイルを読み込む */
        SettingProps props = SettingProps.load();

        /* 更新データ配信サイトURLを設定する */
        updaterSiteUrl = URLUtil.create(props.getValue(SettingKey.UPDATER_SITE_URL));

        /* 認証情報を設定する */
        authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                String proxyUser = props.getValue(SettingKey.PROXY_USER);
                if (!StringUtils.isEmpty(proxyUser) && getRequestorType() == RequestorType.PROXY) {
                    return new PasswordAuthentication(props.getValue(SettingKey.PROXY_USER), props.getValue(SettingKey.PROXY_PASSWORD).toCharArray());
                }
                return null;
            }
        };
    }

    /**
     * アプリケーションバージョン情報を取得します。
     *
     * @return {@link AppVersionResponseBean}
     * @throws UpdaterHttpClientException 処理例外
     */
    public AppVersionResponseBean appVersion() throws UpdaterHttpClientException {
        AppVersionResponseHandler handler = new AppVersionResponseHandler();
        executeXml("appVersion2.xml", handler);
        return handler.getResponseBean();
    }

    /**
     * 更新モジュールを取得します。
     *
     * @param module 更新モジュール名
     * @param file 出力先ファイル
     * @throws UpdaterHttpClientException 処理例外
     */
    public void appDownload(String module, File file) throws UpdaterHttpClientException {
        executeDownload("appDownload/" + module, file);
    }

    /**
     * 大学マスタリストを取得します。
     *
     * @return {@link DbVersionResponseBean}
     * @throws UpdaterHttpClientException 処理例外
     */
    public DbVersionResponseBean dbVersion() throws UpdaterHttpClientException {
        DbVersionResponseHandler handler = new DbVersionResponseHandler();
        executeXml("dbVersion2.xml", handler);
        return handler.getResponseBean();
    }

    /**
     * 大学マスタファイルを取得します。
     *
     * @param univDb {@link UnivDb}
     * @param file 出力先ファイル
     * @throws UpdaterHttpClientException 処理例外
     */
    public void dbDownload(UnivDb univDb, File file) throws UpdaterHttpClientException {
        File tempFile = null;
        try {
            /* 一時ファイルを生成する */
            tempFile = TempFileUtil.createTempFile();
            /* 一時ファイルにダウンロードする */
            executeDownload("dbDownload/" + univDb.getEventYear() + univDb.getExamDiv() + univDb.getVersion() + ".zip", tempFile);
            /* ZIPファイル（一時ファイル）を解凍する */
            unzip(tempFile, file);
        } catch (IORuntimeException e) {
            throw new UpdaterHttpClientException(e);
        } finally {
            if (tempFile != null) {
                tempFile.delete();
            }
        }
    }

    /**
     * ZIPファイルを解凍します。
     *
     * @param file ZIPファイル
     * @param destFile 出力先ファイル
     */
    private void unzip(File file, File destFile) {
        ZipFile zipFile = ZipFileUtil.create(file);
        try {
            unzip(zipFile, destFile);
        } finally {
            ZipFileUtil.close(zipFile);
        }
    }

    /**
     * ZipFileを解凍します。
     *
     * @param zipFile {@link ZipFile}
     * @param destFile 出力先ファイル
     */
    private void unzip(ZipFile zipFile, File destFile) {
        for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements();) {
            ZipEntry entry = e.nextElement();
            if (!entry.isDirectory()) {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = ZipFileUtil.getInputStream(zipFile, entry);
                    out = FileOutputStreamUtil.create(destFile);
                    InputStreamUtil.copy(in, out);
                    return;
                } finally {
                    IOUtil.close(in, out);
                }
            }
        }
        throw new RuntimeException("ZIPファイルに大学マスタファイルが含まれていません。");
    }

    /**
     * XMLをサーバから取得します。
     *
     * @param filename XMLファイル名
     * @param handler 応答XMLのSAXハンドラ
     * @throws UpdaterHttpClientException 処理例外
     */
    private void executeXml(String filename, DefaultHandler handler) throws UpdaterHttpClientException {
        StringBuilder buff = new StringBuilder();
        buff.append(filename);
        /* キャッシュ避けのタイムスタンプをパラメータに付加する */
        buff.append("?=");
        buff.append(new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
        HttpRequest request = HttpRequest.newBuilder(createUri(buff.toString())).GET().build();

        HttpResponse<InputStream> response = execute(request);
        try {
            parse(handler, response.body());
        } catch (IOException e) {
            throw new UpdaterHttpClientException(e);
        }
    }

    /**
     * バイナリファイルをサーバから取得します。
     *
     * @param path 取得先パス
     * @param file 出力先ファイル
     * @throws UpdaterHttpClientException 処理例外
     */
    private void executeDownload(String path, File file) throws UpdaterHttpClientException {
        HttpRequest request = HttpRequest.newBuilder(createUri(path)).GET().build();
        OutputStream out = null;
        try (InputStream in = execute(request).body()) {
            out = new FileOutputStream(file);
            IOUtils.copyLarge(in, out);
        } catch (IOException e) {
            throw new UpdaterHttpClientException(e);
        } finally {
            IOUtil.close(out);
        }
    }

    /**
     * 指定したパスのURIを生成します。
     *
     * @param path パス
     * @return URI
     */
    private URI createUri(String path) {
        try {
            return URLUtil.create(updaterSiteUrl, path).toURI();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * GETメソッドを実行します。
     *
     * @param request {@link HttpRequest}
     * @return 応答オブジェクト
     * @throws UpdaterHttpClientException 実行時エラー
     */
    private HttpResponse<InputStream> execute(HttpRequest request) throws UpdaterHttpClientException {
        HttpResponse<InputStream> response;
        ProxySelector ps;
        HttpClient httpClient = null;
        System.setProperty("java.net.useSystemProxies", "true");
        System.setProperty("jdk.http.auth.proxying.disabledSchemes", "");

        try {
            // Proxy設定を取得
            ps = findProxyDefault();
            if (ps == null) {
                // Proxyなし
                logger.info(String.format("connect to [%s] in direct", updaterSiteUrl.toString()));
                httpClient = HttpClient.newBuilder().connectTimeout(Duration.ofMillis(connectTimeout)).build();
            } else {
                // Proxyあり
                httpClient = HttpClient.newBuilder().proxy(ps).authenticator(authenticator).connectTimeout(Duration.ofMillis(connectTimeout)).build();
            }
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
        } catch (Exception e) {
            throw new UpdaterHttpClientException(e);
        }
        int statusCode = response.statusCode();
        if (statusCode == 200) {
            return response;
        }
        throw new UpdaterHttpClientException("応答ステータスが異常です。コード:[" + response.statusCode() + "]");
    }

    /**
     * banzai.propertiesのプロキシ情報を取得します。
     *
     * @return プロキシ情報
     */
    private ProxySelector findProxyApplication() {
        SettingProps props = SettingProps.load();
        /* プロキシ情報を取得 */
        String proxyHost = props.getValue(SettingKey.PROXY_HOST);
        int proxyPort;
        try {
            proxyPort = Integer.parseInt(props.getValue(SettingKey.PROXY_PORT));
        } catch (Exception e) {
            proxyPort = 0;
        }
        /* プロキシ情報が未設定の場合、スキップ */
        if (StringUtils.isEmpty(proxyHost) && proxyPort == 0) {
            return null;
        }
        InetSocketAddress a = new InetSocketAddress(proxyHost, proxyPort);
        logger.info(String.format("connect to [%s] in Proxy(settings):[%s]", updaterSiteUrl.toString(), a.toString()));

        return ProxySelector.of(a);
    }

    /**
     * システムデフォルトのプロキシ情報を取得します。
     *
     * @return プロキシ情報
     * @throws URISyntaxException URL例外
     */
    private ProxySelector findProxyDefault() throws URISyntaxException {
        ProxySelector ps = ProxySelector.getDefault();
        List<Proxy> proxies = ps.select(updaterSiteUrl.toURI());
        // プロキシ設定が取得できない場合
        if (proxies == null || proxies.size() == 0) {
            ps = findProxyApplication();
        } else {
            Proxy p = proxies.get(0);
            // プロキシ設定が直接接続の場合
            if (Proxy.Type.DIRECT.equals(p.type())) {
                // アプリケーションのプロキシ設定を取得
                ps = findProxyApplication();
            } else {
                logger.info(String.format("connect to [%s] in Proxy(system):[%s]", updaterSiteUrl.toString(), p.address()));
            }
        }
        return ps;
    }

    /**
     * 応答XMLを解析します。
     *
     * @param handler 応答XMLのSAXハンドラ
     * @param in 応答XMLの入力ストリーム
     * @throws UpdaterHttpClientException 応答XMLの解析に失敗した場合
     * @throws IOException IO例外
     */
    private void parse(DefaultHandler handler, InputStream in) throws UpdaterHttpClientException, IOException {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            parser.parse(in, handler);
        } catch (SAXException | ParserConfigurationException e) {
            throw new UpdaterHttpClientException(e);
        } finally {
            in.close();
        }
    }

}

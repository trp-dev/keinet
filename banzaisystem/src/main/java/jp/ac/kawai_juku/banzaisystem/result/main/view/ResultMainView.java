package jp.ac.kawai_juku.banzaisystem.result.main.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZTabbedPane;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.TabConfig;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.result.clnd.controller.ResultClndController;
import jp.ac.kawai_juku.banzaisystem.result.indv.controller.ResultIndvController;
import jp.ac.kawai_juku.banzaisystem.result.univ.controller.ResultUnivController;

/**
 *
 * 入試結果入力画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class ResultMainView extends AbstractView {

    /** 対象生徒選択ボタン */
    @Location(x = 281, y = 2)
    public ImageButton selectStudentButton;

    /** 個人成績・志望大学ボタン */
    @Location(x = 385, y = 2)
    public ImageButton exmntnButton;

    /** 帳票出力ボタン */
    @Location(x = 518, y = 2)
    public ImageButton reportButton;

    /** メンテナンスボタン */
    @Location(x = 632, y = 2)
    public ImageButton maintenanceButton;

    /** 入試結果入力ラベル */
    @Location(x = 736, y = 2)
    public ImageLabel resultLabel;

    /** ヘルプボタン */
    @Location(x = 854, y = 2)
    @Invisible
    public ImageButton helpButton;

    /** 終了ボタン */
    @Location(x = 928, y = 2)
    public ImageButton exitButton;

    /** タブ */
    @Location(x = 4, y = 31)
    @TabConfig({ ResultIndvController.class, ResultUnivController.class, ResultClndController.class })
    public BZTabbedPane tabbedPane;

}

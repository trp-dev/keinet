package jp.ac.kawai_juku.banzaisystem.output.main.component;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Color;
import java.awt.Component;
import java.lang.reflect.Field;

import javax.annotation.Resource;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.BZComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.output.main.view.OutputMainView;

import org.seasar.framework.container.SingletonS2Container;

/**
 *
 * 帳票出力・CSVファイル画面の機能ツリーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class OutputTree extends BZScrollPane implements BZComponent {

    /** カテゴリフォントカラー */
    private static final Color CAT_COLOR = new Color(76, 142, 19);

    /** 選択中の機能ノード */
    private FunctionNode selectedNode;

    /** Helper */
    @Resource
    private OutputMainHelper outputMainHelper;

    /**
     * コンストラクタです。
     */
    public OutputTree() {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        setBorder(null);
        setViewportView(createTree());
    }

    /**
     * ツリーを生成します。
     *
     * @return {@link JTree}
     */
    private JTree createTree() {

        DefaultMutableTreeNode root = new DefaultMutableTreeNode();
        root.add(createExcelTreeNode());
        root.add(createOutputTreeNode());
        root.add(createInputTreeNode());

        JTree tree = new JTree(root);

        /* 全部展開しておく */
        expand(tree, root);

        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        tree.setCellRenderer(new TreeCellRenderer());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(new TreeSelectionListenerImpl());
        tree.setSelectionPath(findFirstLeafPath(tree, root));

        return tree;
    }

    /**
     * 指定したノードを展開します。
     *
     * @param tree ツリー
     * @param node ノード
     * @return 展開したらtrue
     */
    private boolean expand(JTree tree, TreeNode node) {
        if (tree.getModel().isLeaf(node)) {
            tree.expandPath(new TreePath(((DefaultTreeModel) tree.getModel()).getPathToRoot(node.getParent())));
            return true;
        } else {
            for (int i = 0; i < node.getChildCount(); i++) {
                if (expand(tree, node.getChildAt(i))) {
                    break;
                }
            }
            return false;
        }
    }

    /**
     * 最初の葉ノードのパスを返します。
     *
     * @param tree ツリー
     * @param node ノード
     * @return 最初の葉ノードのパス
     */
    private TreePath findFirstLeafPath(JTree tree, TreeNode node) {
        if (tree.getModel().isLeaf(node)) {
            return new TreePath(((DefaultTreeModel) tree.getModel()).getPathToRoot(node));
        } else {
            return findFirstLeafPath(tree, node.getChildAt(0));
        }
    }

    /**
     * Excel帳票のツリーノードを生成します。
     *
     * @return {@link DefaultMutableTreeNode}
     */
    private DefaultMutableTreeNode createExcelTreeNode() {

        RootNode node = new RootNode("Excel帳票");

        CategoryNode child1 = new CategoryNode("成績速報");
        child1.add(new A0102Node());
        child1.add(new A0103Node());
        child1.add(new A0301Node());
        child1.add(new A0104Node());

        node.add(child1);

        return node;
    }

    /**
     * CSV出力のツリーノードを生成します。
     *
     * @return {@link DefaultMutableTreeNode}
     */
    private DefaultMutableTreeNode createOutputTreeNode() {

        RootNode node = new RootNode("CSV出力");

        CategoryNode child1 = new CategoryNode("成績速報");
        child1.add(new B0101Node());
        child1.add(new B0401Node());

        node.add(child1);

        return node;
    }

    /**
     * CSV取り込みのツリーノードを生成します。
     *
     * @return {@link DefaultMutableTreeNode}
     */
    private DefaultMutableTreeNode createInputTreeNode() {

        RootNode node = new RootNode("CSV取り込み");

        CategoryNode child1 = new CategoryNode("成績速報");
        child1.add(new InputB0401Node());

        node.add(child1);

        return node;
    }

    /** ツリー選択イベントリスナ */
    private final class TreeSelectionListenerImpl implements TreeSelectionListener {
        @Override
        public void valueChanged(TreeSelectionEvent e) {
            for (TreePath path : e.getPaths()) {
                if (e.isAddedPath(path) && path.getLastPathComponent() instanceof FunctionNode) {
                    OutputMainView view = SingletonS2Container.getComponent(OutputMainView.class);
                    OutputMainHelper helper = SingletonS2Container.getComponent(OutputMainHelper.class);
                    view.selectedNode = (FunctionNode) path.getLastPathComponent();
                    selectedNode = (FunctionNode) path.getLastPathComponent();
                    String className = view.selectedNode.getClass().getSimpleName();
                    /* タイトルを差し替える */
                    view.titlePanel.replace(getClass(), "title/" + className + ".png");
                    /* 説明文を差し替える */
                    view.noteLabel.replace(getMessage("label.output.main.note." + className));
                    if (view.selectedNode instanceof OutputNode) {
                        /* 出力イメージを差し替える */
                        view.outputImagePanel.replace(getClass(), "outputImage/" + className + ".png");
                    }
                    /* コンポーネントの表示状態を変更する */
                    view.setVisibleByGroup(OutputMainView.GROUP_OUTPUT, view.selectedNode instanceof OutputNode);
                    view.setVisibleByGroup(OutputMainView.GROUP_INPUT, view.selectedNode instanceof InputNode);
                    view.setVisibleByGroup(OutputMainView.GROUP_CANDIDATE_UNIV, view.selectedNode instanceof B0401Node);
                    /* 連続個人成績表の場合、模試を非表示にする */
                    if (view.selectedNode instanceof A0104Node) {
                        view.examLabel.setVisible(false);
                        view.examComboBox.setVisible(false);
                    }
                    if (view.isInitialized()) {
                        helper.initGradeComboBox();
                        helper.initClassTable();
                    }
                    return;
                }
            }
        }
    }

    /** ツリーレンダ */
    private final class TreeCellRenderer extends DefaultTreeCellRenderer {

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

            Component c = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            if (value instanceof Node) {
                /* アイコンをノードの種類で切り替える */
                setIcon(((Node) value).getIcon());
            }

            if (value instanceof CategoryNode) {
                /* カテゴリのみフォントカラーを変える */
                setForeground(CAT_COLOR);
            }

            return c;
        }

    }

    /**
     * 選択中の出力ノードの出力処理を実行します。
     *
     * @param helper {@link OutputMainHelper}
     */
    public void output(OutputMainHelper helper) {
        ((OutputNode) selectedNode).output(helper);
    }

    /**
     * 選択中の出力ノードの入力処理を実行します。
     *
     * @param helper {@link OutputMainHelper}
     */
    public void input(OutputMainHelper helper) {
        ((InputNode) selectedNode).input(helper);
    }

}

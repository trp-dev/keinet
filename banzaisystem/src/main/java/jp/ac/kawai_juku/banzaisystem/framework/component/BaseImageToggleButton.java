package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 画像トグルボタンの基底クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public abstract class BaseImageToggleButton extends JToggleButton implements BZComponent {

    /** グループ番号 */
    private int group;

    /**
     * コンストラクタです。
     */
    public BaseImageToggleButton() {
        setBorder(null);
        setFocusable(false);
        setFocusPainted(false);
        setOpaque(false);
        setContentAreaFilled(false);
        addMouseListener(ImageButton.CURSOR_MOUSE_LISTENER);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        if (!field.getName().endsWith("Button")) {
            throw new RuntimeException("ボタンのフィールド名は[***Button]とする必要があります。");
        }

        String imgName = field.getName().substring(0, field.getName().length() - 6);

        /* 背景画像を読み込む */
        BufferedImage imgOn = ImageUtil.readImage(view.getClass(), "button/" + imgName + "_on.png");
        BufferedImage imgOff = ImageUtil.readImage(view.getClass(), "button/" + imgName + "_off.png");

        setSize(imgOn.getWidth(), imgOn.getHeight());
        setIcon(new ImageIcon(imgOff));
        setPressedIcon(new ImageIcon(imgOn));
        setSelectedIcon(getPressedIcon());

        /* ボタンクリック直後に再描画して、背景が元に戻るようにする */
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });

        /* グループ番号を保持する */
        Group g = field.getAnnotation(Group.class);
        if (g != null) {
            group = g.value()[0];
        }
    }

    /**
     * グループ番号を返します。
     *
     * @return グループ番号
     */
    protected final int getGroup() {
        return group;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.stng.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * メンテナンス-環境設定画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteStngDao extends BaseDao {

    /**
     * 模試リストを取得します。
     *
     * @return 模試リスト
     */
    public List<ExamBean> selectExamList() {
        return jdbcManager.selectBySqlFile(ExamBean.class, getSqlPath()).getResultList();
    }

    /**
     * 個表データを削除します。
     *
     * @param year 年度
     * @param examCd 模試コード
     * @param grade 学年
     * @param cls クラス
     */
    public void deleteIndividualRecord(String year, String examCd, Integer grade, String cls) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("examCd", examCd);
        param.put("grade", grade);
        param.put("class", cls);
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 学年リストを取得します。
     *
     * @param year 年度
     * @param exam 対象模試
     * @return 学年リスト
     */
    public List<Integer> selectGradeList(String year, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("examCd", exam == null ? null : exam.getExamCd());
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), param).getResultList();
    }

    /**
     * クラスリストを生成します。
     *
     * @param year 年度
     * @param exam 対象模試
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> selectClassList(String year, ExamBean exam, Integer grade) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("examCd", exam == null ? null : exam.getExamCd());
        param.put("grade", grade);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個表データが存在しない志望大学（模試記入志望大学データのみ）を削除します。
     */
    public void deleteExamCandidateUniv() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 非表示になっているシステムで登録した志望大学を表示します。
     */
    public void updateCandidateUnivDispOn() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない成績データを削除します。
     */
    public void deleteSubRecord() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない試記入志望大学データを削除します。
     */
    public void deleteCandidateRating() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない個人成績インポート状況を削除します。
     */
    public void deleteStatSubRecord() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない学籍基本情報を削除します。
     */
    public void deleteBasicInfo() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない学籍履歴情報を削除します。
     */
    public void deleteHistoryInfo() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない志望大学を削除します。
     */
    public void deleteCandidateUniv() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 個表データが存在しない受験予定大学を削除します。
     */
    public void deleteExamPlanUniv() {
        jdbcManager.updateBySqlFile(getSqlPath()).execute();
    }

    /**
     * 志望大学の表示順を振り直して取得します。
     *
     * @return 志望大学リスト
     */
    public List<BeanMap> selectCandidateUnivList() {
        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath()).getResultList();
    }

    /**
     * 過年度の個表データを削除します。
     *
     * @param year 年度
     */
    public void deletePastIndividualRecord(String year) {
        jdbcManager.updateBySqlFile(getSqlPath(), year).execute();
    }

}

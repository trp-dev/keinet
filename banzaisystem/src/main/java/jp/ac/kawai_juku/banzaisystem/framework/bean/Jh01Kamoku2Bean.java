package jp.ac.kawai_juku.banzaisystem.framework.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
*
* 情報誌用科目2の情報を保持するBeanです。
*
*
 * @author QQ)Kurimoto
*
*/
@Getter
@Setter
@ToString
public class Jh01Kamoku2Bean extends Jh01KamokuCommonBean {
    private String combiKey;
    private String item7;
    private String rika1AnsFlg;
    private String society1AnsFlg;
    private String jpnDescRange;
    private String jpnDescTrustPt;
    private String jpnDescPt;
    private String jpnDescAddPt;
    private String jpnDescAddPtType;
    private String jpnDescSel;

}

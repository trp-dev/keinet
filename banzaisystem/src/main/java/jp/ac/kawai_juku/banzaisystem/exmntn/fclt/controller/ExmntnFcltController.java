package jp.ac.kawai_juku.banzaisystem.exmntn.fclt.controller;

import java.awt.event.ActionEvent;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.component.ExmntnFcltCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.fclt.view.ExmntnFcltView;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeComponent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;

/**
 *
 * 大学検索（学部系統）画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnFcltController extends SubController {

    /** View */
    @Resource(name = "exmntnFcltView")
    private ExmntnFcltView view;

    @Override
    @Logging(GamenId.C3_2)
    public void activatedAction() {
    }

    /**
     * 中系統チェックボックス変更アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult changeMStemmaAction(ActionEvent e) {
        ExmntnFcltCheckBox mb = (ExmntnFcltCheckBox) e.getSource();
        for (CodeComponent cc : view.findComponentsByCodeType(CodeType.S_STEMMA)) {
            ExmntnFcltCheckBox cb = (ExmntnFcltCheckBox) cc;
            if (cb.getCode().getParent() == mb.getCode() && cb.isAcademic() == mb.isAcademic()) {
                cb.setSelected(mb.isSelected());
            }
        }
        return null;
    }

    /**
     * 小系統チェックボックス変更アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult changeSStemmaAction(ActionEvent e) {

        ExmntnFcltCheckBox sb = (ExmntnFcltCheckBox) e.getSource();
        for (CodeComponent cc : view.findComponentsByCodeType(CodeType.REGION)) {
            ExmntnFcltCheckBox cb = (ExmntnFcltCheckBox) cc;
            if (cb.getCode().getParent() == sb.getCode() && cb.isAcademic() == sb.isAcademic()) {
                cb.setSelectedSilent(sb.isSelected());
            }
        }

        changeMStemmaStatus(sb);

        return null;
    }

    /**
     * 中系統チェックボックスの選択状態を変更します。
     *
     * @param sb 小系統チェックボックス。
     */
    private void changeMStemmaStatus(ExmntnFcltCheckBox sb) {

        boolean mFlag = false;
        if (sb.isSelected()) {
            for (CodeComponent cc : view.findComponentsByCodeType(CodeType.S_STEMMA)) {
                ExmntnFcltCheckBox cb = (ExmntnFcltCheckBox) cc;
                if (cb.getCode().getParent() == sb.getCode().getParent() && cb.isAcademic() == sb.isAcademic()
                        && !cb.isSelected()) {
                    /* 兄弟小系統に未選択が存在するなら変更しない */
                    return;
                }
            }
            mFlag = true;
        }

        for (CodeComponent cc : view.findComponentsByCodeType(CodeType.M_STEMMA)) {
            ExmntnFcltCheckBox mb = (ExmntnFcltCheckBox) cc;
            if (mb.getCode() == sb.getCode().getParent() && mb.isAcademic() == sb.isAcademic()) {
                mb.setSelectedSilent(mFlag);
                break;
            }
        }
    }

    /**
     * 分野チェックボックス変更アクションです。
     *
     * @param e {@link ActionEvent}
     * @return なし
     */
    @Execute
    public ExecuteResult changeRegionAction(ActionEvent e) {

        ExmntnFcltCheckBox rb = (ExmntnFcltCheckBox) e.getSource();

        boolean sFlag = false;
        if (rb.isSelected()) {
            for (CodeComponent cc : view.findComponentsByCodeType(CodeType.REGION)) {
                ExmntnFcltCheckBox cb = (ExmntnFcltCheckBox) cc;
                if (cb.getCode().getParent() == rb.getCode().getParent() && cb.isAcademic() == rb.isAcademic()
                        && !cb.isSelected()) {
                    /* 兄弟分野に未選択が存在するなら変更しない */
                    return null;
                }
            }
            sFlag = true;
        }

        for (CodeComponent cc : view.findComponentsByCodeType(CodeType.S_STEMMA)) {
            ExmntnFcltCheckBox sb = (ExmntnFcltCheckBox) cc;
            if (sb.getCode() == rb.getCode().getParent() && sb.isAcademic() == rb.isAcademic()) {
                sb.setSelectedSilent(sFlag);
                changeMStemmaStatus(sb);
                break;
            }
        }

        return null;
    }

    /**
     * 学部系統クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearFacultyButtonAction() {
        for (ExmntnFcltCheckBox sb : view.findComponentsByClass(ExmntnFcltCheckBox.class)) {
            sb.setSelectedSilent(false);
        }
        return null;
    }

}

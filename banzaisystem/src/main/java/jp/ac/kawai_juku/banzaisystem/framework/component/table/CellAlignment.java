package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * セル文字列のアライメント設定を識別するenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum CellAlignment {

    /** 左寄せ */
    LEFT(new DefaultTableCellRenderer()),

    /** 中央寄せ */
    CENTER(new CenterAlignedCellRenderer()),

    /** 右寄せ */
    RIGHT(new RightAlignedCellRenderer());

    /** {@link TableCellRenderer} */
    private final TableCellRenderer renderer;

    /**
     * コンストラクタです。
     *
     * @param renderer {@link TableCellRenderer}
     */
    CellAlignment(TableCellRenderer renderer) {
        this.renderer = renderer;
    }

    /**
     * TableCellRendererを返します。
     *
     * @return {@link TableCellRenderer}
     */
    public TableCellRenderer getRenderer() {
        return renderer;
    }

}

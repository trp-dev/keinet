package jp.ac.kawai_juku.banzaisystem.framework.http.response;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * 大学マスタリストの応答XMLハンドラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class DbVersionResponseHandler extends BaseResponseHandler<DbVersionResponseBean> {

    /** dbタグ終了フラグ */
    private boolean endDbFlg;

    @Override
    protected void endElement(String qName, String value) {
        if (qName.equals("db")) {
            endDbFlg = true;
        } else if (qName.equals("file")) {
            getResponseBean().getFileList().add(value);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        if (!endDbFlg) {
            fatalError(new SAXParseException("dbタグが終了していません。", null));
        }
        if (getResponseBean().getFileList().isEmpty()) {
            fatalError(new SAXParseException("fileタグが設定されていません。", null));
        }
    }

    @Override
    protected DbVersionResponseBean createResponseBean() {
        return new DbVersionResponseBean();
    }

}

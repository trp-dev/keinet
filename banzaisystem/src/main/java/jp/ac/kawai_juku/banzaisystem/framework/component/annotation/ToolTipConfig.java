package jp.ac.kawai_juku.banzaisystem.framework.component.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * {@link jp.ac.kawai_juku.banzaisystem.framework.component.ToolTipLabel}の設定アノテーションです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface ToolTipConfig {

    /**
     * ツールチップ表示するリソースキーを設定します。
     *
     * @return ツールチップ
     */
    String[] value();

}

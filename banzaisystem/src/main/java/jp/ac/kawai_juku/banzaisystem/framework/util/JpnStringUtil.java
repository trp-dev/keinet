package jp.ac.kawai_juku.banzaisystem.framework.util;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 日本語に関する文字列ユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class JpnStringUtil {

    /** 半角カタカナテーブル */
    private static final String[] HKANA_TABLE = { "ｶﾞ", "ｷﾞ", "ｸﾞ", "ｹﾞ", "ｺﾞ", "ｻﾞ", "ｼﾞ", "ｽﾞ", "ｾﾞ", "ｿﾞ", "ﾀﾞ",
            "ﾁﾞ", "ﾂﾞ", "ﾃﾞ", "ﾄﾞ", "ﾊﾞ", "ﾋﾞ", "ﾌﾞ", "ﾍﾞ", "ﾎﾞ", "ﾊﾟ", "ﾋﾟ", "ﾌﾟ", "ﾍﾟ", "ﾎﾟ", "ｳﾞ", "ﾞ", " ", "ｱ",
            "ｲ", "ｳ", "ｴ", "ｵ", "ｶ", "ｷ", "ｸ", "ｹ", "ｺ", "ｻ", "ｼ", "ｽ", "ｾ", "ｿ", "ﾀ", "ﾁ", "ﾂ", "ﾃ", "ﾄ", "ﾅ", "ﾆ",
            "ﾇ", "ﾈ", "ﾉ", "ﾊ", "ﾋ", "ﾌ", "ﾍ", "ﾎ", "ﾏ", "ﾐ", "ﾑ", "ﾒ", "ﾓ", "ﾔ", "ﾕ", "ﾖ", "ﾗ", "ﾘ", "ﾙ", "ﾚ", "ﾛ",
            "ﾜ", "ｦ", "ﾝ", "ｧ", "ｨ", "ｩ", "ｪ", "ｫ", "ｬ", "ｭ", "ｮ", "ｯ", "｡", "｢", "｣", "､", "･", "ｰ", "ﾟ", "ﾞ", "" };

    /** 半角カタカナテーブルに対応する全角カタカナテーブル */
    private static final String[] ZKANA_TABLE = { "ガ", "ギ", "グ", "ゲ", "ゴ", "ザ", "ジ", "ズ", "ゼ", "ゾ", "ダ", "ヂ", "ヅ", "デ",
            "ド", "バ", "ビ", "ブ", "ベ", "ボ", "パ", "ピ", "プ", "ペ", "ポ", "ヴ", "゛", "　", "ア", "イ", "ウ", "エ", "オ", "カ", "キ",
            "ク", "ケ", "コ", "サ", "シ", "ス", "セ", "ソ", "タ", "チ", "ツ", "テ", "ト", "ナ", "ニ", "ヌ", "ネ", "ノ", "ハ", "ヒ", "フ",
            "ヘ", "ホ", "マ", "ミ", "ム", "メ", "モ", "ヤ", "ユ", "ヨ", "ラ", "リ", "ル", "レ", "ロ", "ワ", "ヲ", "ン", "ァ", "ィ", "ゥ",
            "ェ", "ォ", "ャ", "ュ", "ョ", "ッ", "。", "「", "」", "、", "・", "ー", "゜", "゛", "" };

    /**
     * コンストラクタです。
     */
    private JpnStringUtil() {
    }

    /**
     * 文字列に含まれる半角カタカナを全角カタカナに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String hkana2Zkana(String text) {
        return StringUtils.replaceEach(text, HKANA_TABLE, ZKANA_TABLE);
    }

    /**
     * 文字列に含まれる全角カタカナを半角カタカナに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String zkana2Hkana(String text) {
        return StringUtils.replaceEach(text, ZKANA_TABLE, HKANA_TABLE);
    }

    /**
     * 文字列に含まれるひらがなを全角カタカナに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String hira2Zkana(String text) {

        if (text == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(text.length());
        for (char c : text.toCharArray()) {
            if (c >= 0x3041 && c <= 0x3093) {
                /* Unicodeひらがなのコード範囲(ぁ〜ん)の場合 */
                sb.append((char) (c + 0x0060));
            } else {
                /* 範囲外なら変換しない */
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * 文字列に含まれる全角カタカナをひらがなに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String zkana2Hira(String text) {

        if (text == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder(text.length());
        for (char c : text.toCharArray()) {
            if (c >= 0x30A1 && c <= 0x30F3) {
                /* Unicode全角カタカナのコード範囲(ァ〜ン)の場合 */
                sb.append((char) (c - 0x0060));
            } else {
                /* 範囲外なら変換しない */
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * 文字列に含まれるひらがなを半角カタカナに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String hira2Hkana(String text) {
        return zkana2Hkana(hira2Zkana(text));
    }

    /**
     * 文字列に含まれる半角カタカナをひらがなに変換します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String hkana2Hira(String text) {
        return zkana2Hira(hkana2Zkana(text));
    }

    /**
     * 半角カタカナ文字列を比較用に正規化します。<br>
     * 名寄せ（生徒氏名）および大学名検索で利用します。
     *
     * @param text 変換する文字列
     * @return 変換した文字列
     */
    public static String normalize(String text) {
        return StringUtils.replaceChars(text, "-ｧｨｩｪｫｬｭｮｯ ﾞﾟ", "ｰｱｲｳｴｵﾔﾕﾖﾂ");
    }

}

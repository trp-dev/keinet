package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 志望大学データを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CandidateUnivBean {

    /** 個人ID */
    private Integer id;

    /** 模試年度 */
    private String examYear;

    /** 模試コード */
    private String examCd;

    /** 区分 */
    private String section;

    /** 大学コード */
    private String univCd;

    /** 学部コード */
    private String facultyCd;

    /** 学科コード */
    private String deptCd;

    /** 表示フラグ */
    private String dispFlg;

    /** 表示順 */
    private Integer dispNum;

    /** 年度 */
    private String year;

    /** 模試区分 */
    private String examDiv;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getId() {
        return id;
    }

    /**
     * 個人IDをセットします。
     *
     * @param id 個人ID
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 区分を返します。
     *
     * @return 区分
     */
    public String getSection() {
        return section;
    }

    /**
     * 区分をセットします。
     *
     * @param section 区分
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * 大学コードを返します。
     *
     * @return 大学コード
     */
    public String getUnivCd() {
        return univCd;
    }

    /**
     * 大学コードをセットします。
     *
     * @param univCd 大学コード
     */
    public void setUnivCd(String univCd) {
        this.univCd = univCd;
    }

    /**
     * 学部コードを返します。
     *
     * @return 学部コード
     */
    public String getFacultyCd() {
        return facultyCd;
    }

    /**
     * 学部コードをセットします。
     *
     * @param facultyCd 学部コード
     */
    public void setFacultyCd(String facultyCd) {
        this.facultyCd = facultyCd;
    }

    /**
     * 学科コードを返します。
     *
     * @return 学科コード
     */
    public String getDeptCd() {
        return deptCd;
    }

    /**
     * 学科コードをセットします。
     *
     * @param deptCd 学科コード
     */
    public void setDeptCd(String deptCd) {
        this.deptCd = deptCd;
    }

    /**
     * 表示フラグを返します。
     *
     * @return 表示フラグ
     */
    public String getDispFlg() {
        return dispFlg;
    }

    /**
     * 表示フラグをセットします。
     *
     * @param dispFlg 表示フラグ
     */
    public void setDispFlg(String dispFlg) {
        this.dispFlg = dispFlg;
    }

    /**
     * 表示順を返します。
     *
     * @return 表示順
     */
    public Integer getDispNum() {
        return dispNum;
    }

    /**
     * 表示順をセットします。
     *
     * @param dispNum 表示順
     */
    public void setDispNum(Integer dispNum) {
        this.dispNum = dispNum;
    }

    /**
     * 年度を返します。
     *
     * @return 年度
     */
    public String getYear() {
        return year;
    }

    /**
     * 年度をセットします。
     *
     * @param year year
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 模試区分を返します。
     *
     * @return 模試区分
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試区分をセットします。
     *
     * @param examDiv 模試区分
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

}

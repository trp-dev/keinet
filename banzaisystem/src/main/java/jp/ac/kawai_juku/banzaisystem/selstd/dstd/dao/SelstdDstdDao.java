package jp.ac.kawai_juku.banzaisystem.selstd.dstd.dao;

import java.util.Collections;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.bean.SelstdDstdKojinBean;

import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 生徒一覧ダイアログのDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDstdDao extends BaseDao {

    /**
     * 個人リストを取得します。
     *
     * @param year 年度
     * @param idList 個人IDリスト
     * @return 個人リスト
     */
    public List<SelstdDstdKojinBean> selectKojinListById(String year, List<Integer> idList) {

        if (idList.isEmpty()) {
            return Collections.emptyList();
        }

        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("idCondition", DaoUtil.createIn("B.INDIVIDUALID", idList));
        return jdbcManager.selectBySqlFile(SelstdDstdKojinBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人リストを取得します。
     *
     * @param year 年度
     * @param grade 学年
     * @param classList クラスリスト
     * @param numberStart 席次番号開始
     * @param numberEnd 席次番号終了
     * @return 個人リスト
     */
    public List<SelstdDstdKojinBean> selectKojinListByClass(String year, Integer grade, List<String> classList,
            String numberStart, String numberEnd) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("grade", grade);
        param.put("classCondition", createClassCondition(classList));
        param.put("numberStart", StringUtils.defaultIfEmpty(numberStart, null));
        param.put("numberEnd", StringUtils.defaultIfEmpty(numberEnd, null));
        return jdbcManager.selectBySqlFile(SelstdDstdKojinBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * クラスによる絞込み条件を生成します。
     *
     * @param classList クラスリスト
     * @return 絞込み条件
     */
    private String createClassCondition(List<String> classList) {

        StringBuilder sql = new StringBuilder(16 * classList.size());

        for (String cls : classList) {
            if (sql.length() > 0) {
                sql.append(" OR ");
            }
            if (cls == null) {
                sql.append("H.CLASS IS NULL");
            } else {
                sql.append("H.CLASS = '").append(cls).append("'");
            }
        }

        return sql.toString();
    }

}

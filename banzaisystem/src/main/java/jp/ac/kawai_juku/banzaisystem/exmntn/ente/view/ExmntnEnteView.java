package jp.ac.kawai_juku.banzaisystem.exmntn.ente.view;

import java.util.List;

import javax.swing.JCheckBox;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.CenterSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.SecondSubject;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.CenterCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.EvaluationCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.ExmntnEnteDateComboBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.ExmntnEnteMonthComboBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.SecondBasicCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.SecondCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation.CenterSubjectConfig;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.annotation.SecondSubjectConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.BZLayoutFocusTraversalPolicy;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeCheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Disabled;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Invisible;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;

import org.seasar.framework.container.annotation.tiger.InitMethod;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学検索（入試情報（評価・日程・科目））画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnEnteView extends SubView {

    /** グループ番号：評価範囲 */
    public static final int GROUP_RANGE = 0;

    /** グループ番号：入試日程 */
    public static final int GROUP_SCHEDULE = 1;

    /** グループ番号：入試科目 */
    public static final int GROUP_SUBJECT = 2;

    /** グループ番号：二次科目 */
    public static final int GROUP_SECOND_SUBJECT = 3;

    /** グループ番号：検索対象チェックボックス */
    public static final int GROUP_TARGET = 4;

    /** グループ番号：入試日程(国公立) */
    public static final int GROUP_SCHEDULE_NATIONAL = 7;

    /** グループ番号：入試日程(私立・短大) */
    public static final int GROUP_SCHEDULE_PRIVATE = 8;

    /** 評価範囲チェックボックス */
    @Location(x = 8, y = 48)
    @CodeConfig(Code.UNIV_SEARCH_EXAM_RANGE)
    @Group(GROUP_TARGET)
    public CodeCheckBox scopeOfAssessmentCheckBox;

    /** 入試日程チェックボックス */
    @Location(x = 8, y = 66)
    @CodeConfig(Code.UNIV_SEARCH_EXAM_SCHEDULE)
    @Group(GROUP_TARGET)
    public CodeCheckBox exmntnScheduleCheckBox;

    /** 入試科目チェックボックス */
    @Location(x = 8, y = 84)
    @CodeConfig(Code.UNIV_SEARCH_EXAM_SUBJECT)
    @Group(GROUP_TARGET)
    public CodeCheckBox exmntnSubjectsCheckBox;

    /** ＜評価範囲＞-------------------------------- */
    /** センターチェックボックス */
    @Location(x = 124, y = 71)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_CENTER)
    @Group(GROUP_RANGE)
    @Disabled
    @Action("evalRangeCheckBoxAction")
    public CodeCheckBox centerCheckBox;

    /** 二次・個別チェックボックス */
    @Location(x = 227, y = 71)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_SECOND)
    @Group(GROUP_RANGE)
    @Disabled
    @Action("evalRangeCheckBoxAction")
    public CodeCheckBox secondCheckBox;

    /** 総合(ドッキング)チェックボックス */
    @Location(x = 302, y = 71)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_TOTAL)
    @Group(GROUP_RANGE)
    @Disabled
    @Action("evalRangeCheckBoxAction")
    public CodeCheckBox syntheticCheckBox;

    /** A(85%以上)チェックボックス */
    @Location(x = 124, y = 92)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_VALUE_A)
    @Group(GROUP_RANGE)
    @Disabled
    @NoAction
    public EvaluationCheckBox judgeACheckBox;

    /** B(65%以上)チェックボックス */
    @Location(x = 227, y = 92)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_VALUE_B)
    @Group(GROUP_RANGE)
    @Disabled
    @NoAction
    public EvaluationCheckBox judgeBCheckBox;

    /** C(50%以上)チェックボックス */
    @Location(x = 302, y = 92)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_VALUE_C)
    @Group(GROUP_RANGE)
    @Disabled
    @NoAction
    public EvaluationCheckBox judgeCCheckBox;

    /** D(35%)チェックボックス */
    @Location(x = 378, y = 91)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_VALUE_D)
    @Group(GROUP_RANGE)
    @Disabled
    @NoAction
    public EvaluationCheckBox judgeC35CheckBox;

    /** E(20%以下)チェックボックス */
    @Location(x = 456, y = 92)
    @CodeConfig(Code.UNIV_SEARCH_RANGE_VALUE_E)
    @Group(GROUP_RANGE)
    @Disabled
    @NoAction
    public EvaluationCheckBox judgeC20CheckBox;

    /** ＜入試日程＞---------------------------------- */
    /** 前期チェックボックス */
    @Location(x = 260, y = 154)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_FIRST)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_NATIONAL })
    @Disabled
    @NoAction
    public CodeCheckBox firstSemesterCheckBox;

    /** 後期チェックボックス */
    @Location(x = 318, y = 154)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_SECOND)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_NATIONAL })
    @Disabled
    @NoAction
    public CodeCheckBox secondSemesterCheckBox;

    /** 中期チェックボックス */
    @Location(x = 376, y = 154)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_MIDDLE)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_NATIONAL })
    @Disabled
    @NoAction
    public CodeCheckBox middleSemesterCheckBox;

    /** 別日程チェックボックス */
    @Location(x = 437, y = 154)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_OTHER)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_NATIONAL })
    @Disabled
    @NoAction
    public CodeCheckBox anotherDayCheckBox;

    /** 共通テスト利用チェックボックス */
    @Location(x = 260, y = 185)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_CENTER)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    public CodeCheckBox useCenterCheckBox;

    /** 一般チェックボックス */
    @Location(x = 376, y = 185)
    @CodeConfig(Code.UNIV_SEARCH_SCHEDULE_GENERAL)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    public CodeCheckBox publicCheckBox;

    /** ＜入試科目＞---------------------------------- */
    /** 英語チェックボックス */
    @Location(x = 128, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.ENGLISH)
    public CenterCheckBox englishCheckBox;

    /** 英語リスニングチェックボックス */
    @Location(x = 144, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.LISTENING)
    public CenterCheckBox englishListeningCheckBox;

    /** CEFRチェックボックス */
    /*@Location(x = 160, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.ENGCEFR)
    public CenterCheckBox cefrCheckBox;*/

    /** 数�Tチェックボックス */
    @Location(x = 171, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.MATH1)
    public CenterCheckBox math1CheckBox;

    /** 数�TAチェックボックス */
    @Location(x = 187, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.MATH1A)
    public CenterCheckBox math1ACheckBox;

    /** 数�Uチェックボックス */
    @Location(x = 203, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.MATH2)
    public CenterCheckBox math2CheckBox;

    /** 数�UBチェックボックス */
    @Location(x = 219, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.MATH2B)
    public CenterCheckBox math2BCheckBox;

    /** 現代文マークチェックボックス */
    @Location(x = 240, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JPRESENT)
    public CenterCheckBox japaneseCheckBox;

    /** 現代文記述チェックボックス */
    /*@Location(x = 257, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JPRESENTWRITE)
    public CenterCheckBox japaneseWCheckBox;*/

    /** 古文チェックボックス */
    @Location(x = 255, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JCLASSICS)
    public CenterCheckBox classicCheckBox;

    /** 漢文チェックボックス */
    @Location(x = 271, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JCHINESE)
    public CenterCheckBox chieneseCheckBox;

    /** 物理基礎チェックボックス */
    @Location(x = 294, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.PHYSICS_BASIC)
    public CenterCheckBox physicsBasicCheckBox;

    /** 化学基礎チェックボックス */
    @Location(x = 310, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.CHEMISTRY_BASIC)
    public CenterCheckBox chemicalBasicCheckBox;

    /** 生物基礎チェックボックス */
    @Location(x = 326, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.BIOLOGY_BASIC)
    public CenterCheckBox biologyBasicCheckBox;

    /** 地学基礎チェックボックス */
    @Location(x = 342, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.EARTHSCIENCE_BASIC)
    public CenterCheckBox earthScienceBasicCheckBox;

    /** 物理チェックボックス */
    @Location(x = 358, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.PHYSICS)
    public CenterCheckBox physicsCheckBox;

    /** 化学チェックボックス */
    @Location(x = 374, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.CHEMISTRY)
    public CenterCheckBox chemicalCheckBox;

    /** 生物チェックボックス */
    @Location(x = 390, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.BIOLOGY)
    public CenterCheckBox biologyCheckBox;

    /** 地学チェックボックス */
    @Location(x = 406, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.EARTHSCIENCE)
    public CenterCheckBox earthScienceCheckBox;

    /** 世界史Aチェックボックス421437 */
    @Location(x = 428, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.WHISTORYA)
    public CenterCheckBox worldHistoryACheckBox;

    /** 日本史Aチェックボックス */
    @Location(x = 444, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JHISTORYA)
    public CenterCheckBox japaneseHistoryACheckBox;

    /** 地理Aチェックボックス */
    @Location(x = 460, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.GEOGRAPHYA)
    public CenterCheckBox geographyACheckBox;

    /** 世界史Bチェックボックス */
    @Location(x = 476, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.WHISTORYB)
    public CenterCheckBox worldHistoryBCheckBox;

    /** 日本史Bチェックボックス */
    @Location(x = 492, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.JHISTORYB)
    public CenterCheckBox japaneseHistoryBCheckBox;

    /** 地理Bチェックボックス */
    @Location(x = 508, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.GEOGRAPHYB)
    public CenterCheckBox geographyBCheckBox;

    /** 現代社会チェックボックス */
    @Location(x = 524, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.SOCIALSTUDIES)
    public CenterCheckBox modernSocietyCheckBox;

    /** 倫理チェックボックス */
    @Location(x = 540, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.ETHIC)
    public CenterCheckBox ethicalCheckBox;

    /** 政経チェックボックス */
    @Location(x = 556, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.POLITICS)
    public CenterCheckBox politicsAndDEcnomicsCheckBox;

    /** 倫・政経チェックボックス */
    @Location(x = 572, y = 319)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @CenterSubjectConfig(CenterSubject.ETHICALPOLITICS)
    public CenterCheckBox ethicalAndPoliticsAndDEcnomicsCheckBox;

    /** 個別試験----------------------------------- */
    /** 課さないチェックボックス */
    @Location(x = 287, y = 407)
    @CodeConfig(Code.UNIV_SEARCH_NOT_IMPOSED)
    @Group(GROUP_SUBJECT)
    @Disabled
    public CodeCheckBox notImposeCheckBox;

    /** 英語リスニングチェックボックス */
    @Location(x = 138, y = 448)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.LISTENING)
    public SecondCheckBox indvEnglishListeningCheckBox;

    /** 英語チェックボックス */
    @Location(x = 138, y = 507)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.ENGLISH)
    public SecondCheckBox indvEnglishCheckBox;

    /** 数学チェックボックス */
    @Location(x = 195, y = 507)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.MATH)
    public SecondCheckBox indvMathCheckBox;

    /** 国語チェックボックス */
    @Location(x = 267, y = 507)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.JAPANESE)
    public SecondCheckBox indvJapaneseCheckBox;

    /** 物理基礎のみチェックボックス */
    @Location(x = 314, y = 488)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.PHYSICS_BASIC)
    public SecondBasicCheckBox indvPhysicsBasicCheckBox;

    /** 化学基礎のみチェックボックス */
    @Location(x = 330, y = 488)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.CHEMISTRY_BASIC)
    public SecondBasicCheckBox indvChemicalBasicCheckBox;

    /** 生物基礎のみチェックボックス */
    @Location(x = 346, y = 488)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.BIOLOGY_BASIC)
    public SecondBasicCheckBox indvBiologyBasicCheckBox;

    /** 地学基礎のみチェックボックス */
    @Location(x = 362, y = 488)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.EARTHSCIENCE_BASIC)
    public SecondBasicCheckBox indvEarthScienceBasicCheckBox;

    /** 物理チェックボックス */
    @Location(x = 314, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @SecondSubjectConfig(SecondSubject.PHYSICS)
    public SecondCheckBox indvPhysicsCheckBox;

    /** 化学チェックボックス */
    @Location(x = 330, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @SecondSubjectConfig(SecondSubject.CHEMISTRY)
    public SecondCheckBox indvChemicalCheckBox;

    /** 生物チェックボックス */
    @Location(x = 346, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @SecondSubjectConfig(SecondSubject.BIOLOGY)
    public SecondCheckBox indvBiologyCheckBox;

    /** 地学チェックボックス */
    @Location(x = 362, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @SecondSubjectConfig(SecondSubject.EARTHSCIENCE)
    public SecondCheckBox indvEarthScienceCheckBox;

    /** 世界史チェックボックス */
    @Location(x = 382, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.WHISTORY)
    public SecondCheckBox indvWorldHistoryCheckBox;

    /** 日本史チェックボックス */
    @Location(x = 398, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.JHISTORY)
    public SecondCheckBox indvJapaneseHistoryCheckBox;

    /** 地理チェックボックス */
    @Location(x = 414, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.GEOGRAPHY)
    public SecondCheckBox indvGeographyCheckBox;

    /** 倫理チェックボックス */
    @Location(x = 430, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.ETHIC)
    public SecondCheckBox indvEthicalCheckBox;

    /** 政経チェックボックス */
    @Location(x = 446, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.POLITICS)
    public SecondCheckBox indvPoliticsAndDEcnomicsCheckBox;

    /** 小論文チェックボックス */
    @Location(x = 466, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.ESSAY)
    public SecondCheckBox indvNoteCheckBox;

    /** 総合問題チェックボックス */
    @Location(x = 482, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.SYNTHESIS)
    public SecondCheckBox indvSyntheticCheckBox;

    /** 面接チェックボックス */
    @Location(x = 498, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.INTERVIEW)
    public SecondCheckBox indvInterviewCheckBox;

    /** 実技チェックボックス */
    @Location(x = 514, y = 446)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    @SecondSubjectConfig(SecondSubject.TECHNIQUE)
    public SecondCheckBox indvSkillCheckBox;

    /** センター個別配点比----------------------------------- */
    /** 65%以上チェックボックス */
    @Location(x = 121, y = 615)
    @CodeConfig(Code.UNIV_SEARCH_RATE_CENTER)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @Foreground(r = 1, g = 2, b = 253)
    public CodeCheckBox center65CheckBox;

    /** 50%チェックボックス */
    @Location(x = 293, y = 615)
    @CodeConfig(Code.UNIV_SEARCH_RATE_HALF)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @Foreground(r = 102, g = 102, b = 102)
    public CodeCheckBox center50CheckBox;

    /** 個別65%以上チェックボックス */
    @Location(x = 465, y = 615)
    @CodeConfig(Code.UNIV_SEARCH_RATE_INDIVIDUAL)
    @Group(GROUP_SUBJECT)
    @Disabled
    @NoAction
    @Foreground(r = 182, g = 60, b = 119)
    public CodeCheckBox indv65CheckBox;

    /** 出願締切日-開始月コンボボックス */
    @Location(x = 332, y = 181)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteMonthComboBox appStartMonthComboBox;

    /** 出願締切日-開始日コンボボックス */
    @Location(x = 396, y = 181)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteDateComboBox appStartDateComboBox;

    /** 出願締切日-終了月コンボボックス */
    @Location(x = 483, y = 181)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteMonthComboBox appEndMonthComboBox;

    /** 出願締切日-終了日コンボボックス */
    @Location(x = 547, y = 181)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteDateComboBox appEndDateComboBox;

    /** 入試日-開始月コンボボックス */
    @Location(x = 332, y = 205)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteMonthComboBox exmntnStartMonthComboBox;

    /** 入試日-開始日コンボボックス */
    @Location(x = 396, y = 205)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteDateComboBox exmntnStartDateComboBox;

    /** 入試日-終了月コンボボックス */
    @Location(x = 483, y = 205)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteMonthComboBox exmntnEndMonthComboBox;

    /** 入試日-終了日コンボボックス */
    @Location(x = 547, y = 205)
    @Size(width = 43, height = 20)
    @Group({ GROUP_SCHEDULE, GROUP_SCHEDULE_PRIVATE })
    @Disabled
    @NoAction
    @Invisible
    public ExmntnEnteDateComboBox exmntnEndDateComboBox;

    /** 個別試験-数学コンボボックス */
    @Location(x = 169, y = 445)
    @Size(width = 65, height = 20)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    public ComboBox<SecondSubject> indvMathComboBox;

    /** 個別試験-国語コンボボックス */
    @Location(x = 242, y = 445)
    @Size(width = 65, height = 20)
    @Group({ GROUP_SUBJECT, GROUP_SECOND_SUBJECT })
    @Disabled
    @NoAction
    public ComboBox<SecondSubject> indvJpnComboBox;

    /** 評価範囲クリアボタン */
    @Location(x = 536, y = 44)
    @Disabled
    @Group(GROUP_RANGE)
    public ImageButton clearValuationRangeButton;

    /** 入試スケジュールクリアボタン */
    @Location(x = 509, y = 125)
    @Disabled
    public ImageButton clearExaminationScheduleButton;

    /** 初期値に戻すボタン */
    @Location(x = 536, y = 253)
    @Disabled
    @Group(GROUP_SUBJECT)
    public ImageButton returnInitialValueButton;

    @Override
    @InitMethod
    public void initialize() {

        super.initialize();

        BZLayoutFocusTraversalPolicy policy = new BZLayoutFocusTraversalPolicy();
        policy.addPair(scopeOfAssessmentCheckBox, exmntnScheduleCheckBox);
        policy.addPair(exmntnScheduleCheckBox, exmntnSubjectsCheckBox);
        policy.addPair(exmntnSubjectsCheckBox, centerCheckBox);
        policy.addPair(syntheticCheckBox, judgeACheckBox);
        policy.addPair(indvEnglishListeningCheckBox, indvEnglishCheckBox);
        policy.addPair(indvEnglishCheckBox, indvMathComboBox);
        policy.addPair(indvMathComboBox, indvMathCheckBox);
        policy.addPair(indvMathCheckBox, indvJpnComboBox);
        policy.addPair(indvJpnComboBox, indvJapaneseCheckBox);
        policy.addPair(indvJapaneseCheckBox, indvPhysicsBasicCheckBox);
        policy.addPair(indvJapaneseCheckBox, indvPhysicsCheckBox);
        policy.addPair(indvPhysicsBasicCheckBox, indvPhysicsCheckBox);
        policy.addPair(indvPhysicsCheckBox, indvChemicalBasicCheckBox);
        policy.addPair(indvChemicalBasicCheckBox, indvChemicalCheckBox);
        policy.addPair(indvChemicalCheckBox, indvBiologyBasicCheckBox);
        policy.addPair(indvBiologyBasicCheckBox, indvBiologyCheckBox);
        policy.addPair(indvBiologyCheckBox, indvEarthScienceBasicCheckBox);
        policy.addPair(indvEarthScienceBasicCheckBox, indvEarthScienceCheckBox);
        policy.addPair(indvSkillCheckBox, center65CheckBox);
        setFocusTraversalPolicy(policy);

        /* 個別試験-数学コンボボックスにアイテムリストを設定する */
        List<ComboBoxItem<SecondSubject>> mathList = CollectionsUtil.newArrayList(6);
        addComboBoxItem(mathList, SecondSubject.MATH1);
        addComboBoxItem(mathList, SecondSubject.MATH1A);
        addComboBoxItem(mathList, SecondSubject.MATH2A);
        addComboBoxItem(mathList, SecondSubject.MATH2B);
        addComboBoxItem(mathList, SecondSubject.MATH3);
        indvMathComboBox.setItemList(mathList);
        indvMathComboBox.setSelectedIndex(mathList.size() - 1);

        /* 個別試験-国語コンボボックスにアイテムリストを設定する */
        List<ComboBoxItem<SecondSubject>> japaneseList = CollectionsUtil.newArrayList(6);
        addComboBoxItem(japaneseList, SecondSubject.COMTEMPORARY);
        addComboBoxItem(japaneseList, SecondSubject.ANCIENT);
        addComboBoxItem(japaneseList, SecondSubject.CHINESE);
        indvJpnComboBox.setItemList(japaneseList);
        indvJpnComboBox.setSelectedIndex(japaneseList.size() - 1);

        /* 月コンボボックスにペアを設定する */
        appStartMonthComboBox.setDateComboBox(appStartDateComboBox);
        appEndMonthComboBox.setDateComboBox(appEndDateComboBox);
        exmntnStartMonthComboBox.setDateComboBox(exmntnStartDateComboBox);
        exmntnEndMonthComboBox.setDateComboBox(exmntnEndDateComboBox);

        /* 個別試験：リスニングチェックボックスのラベル位置を調整する */
        indvEnglishListeningCheckBox.setHorizontalTextPosition(JCheckBox.CENTER);
        indvEnglishListeningCheckBox.setVerticalTextPosition(JCheckBox.BOTTOM);
        indvEnglishListeningCheckBox.setIconTextGap(4);
        indvEnglishListeningCheckBox.setSize(indvEnglishListeningCheckBox.getPreferredSize());

        /* 二次基礎科目チェックボックスに、対応する専門科目チェックボックスを設定する */
        indvPhysicsBasicCheckBox.setSpecialCheckBox(indvPhysicsCheckBox);
        indvChemicalBasicCheckBox.setSpecialCheckBox(indvChemicalCheckBox);
        indvBiologyBasicCheckBox.setSpecialCheckBox(indvBiologyCheckBox);
        indvEarthScienceBasicCheckBox.setSpecialCheckBox(indvEarthScienceCheckBox);
    }

    /**
     * 指定した科目をコンボボックスアイテムリストに追加します。
     *
     * @param list コンボボックスアイテムリスト
     * @param subject {@link SecondSubject}
     */
    private void addComboBoxItem(List<ComboBoxItem<SecondSubject>> list, SecondSubject subject) {
        list.add(new ComboBoxItem<SecondSubject>(subject.getName(), subject));
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.util;

import java.util.Comparator;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 評価値のComparatorです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class RatingComparator implements Comparator<String> {

    /** インスタンス */
    private static final RatingComparator INSTANCE = new RatingComparator();

    /**
     * コンストラクタです。
     */
    private RatingComparator() {
    }

    /**
     * インスタンスを返します。
     *
     * @return {@link RatingComparator}
     */
    public static RatingComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(String o1, String o2) {
        return toSortValue(o1).compareTo(toSortValue(o2));
    }

    /**
     * 評価値をソート用の値に変換します。
     *
     * @param rating 評価値
     * @return ソート用の値
     */
    private String toSortValue(String rating) {
        if (JudgementConstants.Judge.JUDGE_STR_X.equals(rating)) {
            return "Y";
        } else if (StringUtils.isBlank(rating)) {
            return "Z";
        } else {
            return rating;
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.lang.reflect.Field;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZFieldUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * コード定義を利用するチェックボックスです。<br>
 * チェックボックスのテキストにはコード名が設定されます。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CodeCheckBox extends CheckBox implements CodeComponent {

    /** このチェックボックスに紐付けられたコード */
    private Code code;

    @Override
    public void initialize(Field field, AbstractView view) {

        super.initialize(field, view);

        CodeConfig config = BZFieldUtil.getAnnotation(field, CodeConfig.class);
        if (config == null) {
            throw new RuntimeException("CodeConfigアノテーションが設定されていません。");
        } else {
            code = config.value();
        }

        setText(code.getName());
        setSize(getPreferredSize());
    }

    @Override
    public Code getCode() {
        return code;
    }

}

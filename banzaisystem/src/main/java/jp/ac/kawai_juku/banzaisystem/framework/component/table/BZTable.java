package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * バンザイシステムのテーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZTable extends JTable {

    /** 選択行の背景色 */
    private static final Color SELECTED_ROW_COLOR = UIManager.getColor("ComboBox.selectionBackground");

    /**
     * コンストラクタです。
     *
     * @param dm テーブルモデル
     */
    public BZTable(TableModel dm) {
        super(dm);
    }

    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);
        if (getRowSelectionAllowed()) {
            if (ArrayUtils.indexOf(getSelectedRows(), row) > -1) {
                c.setForeground(Color.WHITE);
                c.setBackground(SELECTED_ROW_COLOR);
            } else {
                c.setForeground(Color.BLACK);
                c.setBackground(Color.WHITE);
            }
        }
        return c;
    }

    @Override
    public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
        if (getRowSelectionAllowed()) {
            /* Ctrlを押さなくても、複数選択可能にする */
            super.changeSelection(rowIndex, columnIndex, true, extend);
        }
    }

    @Override
    public void createDefaultColumnsFromModel() {
        TableColumnModel cm = getColumnModel();
        while (cm.getColumnCount() > 0) {
            cm.removeColumn(cm.getColumn(0));
        }
        for (int i = 0; i < getModel().getColumnCount(); i++) {
            /*
             * デフォルトではminWidthは15px固定で定義されてしまい、
             * 15px未満をsetPreferredWidthした場合に意図した動作とならない。
             * そのため、minWidthの初期値が0pxになるようにオーバーライドして対応する。
             */
            addColumn(new TableColumn(i, 0));
        }
    }

}

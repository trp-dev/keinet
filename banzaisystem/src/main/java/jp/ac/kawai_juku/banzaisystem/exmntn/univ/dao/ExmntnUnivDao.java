package jp.ac.kawai_juku.banzaisystem.exmntn.univ.dao;

import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDaoOutBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 大学詳細DAOです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivDao extends BaseDao {

    /**
     * 大学詳細を取得します。
     *
     * @param bean {@link UnivKeyBean}
     * @param systemDto {@link SystemDto}
     * @return {@link ExmntnUnivDaoOutBean}
     */
    public ExmntnUnivDaoOutBean selectUnivDetail(UnivKeyBean bean, SystemDto systemDto) {
        BeanMap param = new BeanMap();
        param.put("univCd", bean.getUnivCd());
        param.put("facultyCd", bean.getFacultyCd());
        param.put("deptCd", bean.getDeptCd());
        param.put("eventYear", systemDto.getUnivYear());
        param.put("examDiv", systemDto.getUnivExamDiv());
        return jdbcManager.selectBySqlFile(ExmntnUnivDaoOutBean.class, getSqlPath(), param).getSingleResult();
    }

}

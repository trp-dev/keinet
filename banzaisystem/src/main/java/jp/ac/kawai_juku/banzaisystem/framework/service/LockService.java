package jp.ac.kawai_juku.banzaisystem.framework.service;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.dao.LockDao;

import org.aopalliance.intercept.MethodInvocation;

/**
 *
 *  データファイルロックが必要なサービスメソッドを実行するサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class LockService {

    /** DAO */
    @Resource
    private LockDao lockDao;

    /**
     * データファイルをロックしてからサービスメソッドを実行します。
     *
     * @param mi {@link MethodInvocation}
     * @return 実行結果
     * @throws Throwable 例外
     */
    public Object execute(MethodInvocation mi) throws Throwable {
        lockDao.updateRecordInfo4Lock();
        return mi.proceed();
    }

}

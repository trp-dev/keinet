package jp.ac.kawai_juku.banzaisystem;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Judge;

/**
 *
 * コードを識別するためのenumです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public enum Code {

    /** 対象生徒選択-表示項目：個人名のみ */
    DISP_ITEM_NAME(CodeType.SELECT_STUDENT_DISP_ITEM, "1"),

    /** 対象生徒選択-表示項目：個人名＋成績 */
    DISP_ITEM_SCORE(CodeType.SELECT_STUDENT_DISP_ITEM, "2"),

    /** 対象生徒選択-表示項目：個人名＋志望大学 */
    DISP_ITEM_UNIV(CodeType.SELECT_STUDENT_DISP_ITEM, "3"),

    /** 一覧出力-対象概要：成績一覧 */
    SCORE_LIST(CodeType.OUTPUT_LIST_TARGET, "1"),

    /** 一覧出力-対象概要：総合成績＋志望大学（上位9） */
    SCORE_PLUS_UNIV9(CodeType.OUTPUT_LIST_TARGET, "2"),

    /** 一覧出力-対象概要：総合成績＋志望大学（上位20） */
    SCORE_PLUS_UNIV20(CodeType.OUTPUT_LIST_TARGET, "3"),

    /** 一覧出力-出力範囲：画面で選択されている生徒 */
    SELECTED_STUDENT(CodeType.OUTPUT_LIST_RANGE, "1"),

    /** 一覧出力-出力範囲：改めて選択する */
    SELECT_AGAIN(CodeType.OUTPUT_LIST_RANGE, "2"),

    /** 志望集計パターン：総志望 */
    CANDIDATE_COUNT_PTN_T(CodeType.CANDIDATE_COUNT_PTN, "1"),

    /** 志望集計パターン：出願予定 */
    CANDIDATE_COUNT_PTN_P(CodeType.CANDIDATE_COUNT_PTN, "2"),

    /** 検索-区分：国立 */
    UNIV_SEARCH_DIV_NATIONAL(CodeType.UNIV_SEARCH_DIV, "1"),

    /** 検索-区分：公立 */
    UNIV_SEARCH_DIV_PUBLIC(CodeType.UNIV_SEARCH_DIV, "2"),

    /** 検索-区分：私立（センター） */
    UNIV_SEARCH_DIV_PRIVATE_C(CodeType.UNIV_SEARCH_DIV, "3"),

    /** 検索-区分：私立（一般） */
    UNIV_SEARCH_DIV_PRIVATE_G(CodeType.UNIV_SEARCH_DIV, "4"),

    /** 検索-区分：短大（センター） */
    UNIV_SEARCH_DIV_JUNIOR_C(CodeType.UNIV_SEARCH_DIV, "5"),

    /** 検索-区分：短大（一般） */
    UNIV_SEARCH_DIV_JUNIOR_G(CodeType.UNIV_SEARCH_DIV, "6"),

    /** 検索-区分：文科省所管外 */
    UNIV_SEARCH_DIV_OUTSIDE(CodeType.UNIV_SEARCH_DIV, "7"),

    /** 検索-女子大：含む */
    UNIV_SEARCH_FEMAIL_INCLUDE(CodeType.UNIV_SEARCH_FEMAIL, "1"),

    /** 検索-女子大：含まない */
    UNIV_SEARCH_FEMAIL_NOT_INCLUDE(CodeType.UNIV_SEARCH_FEMAIL, "2"),

    /** 検索-女子大：女子大のみ */
    UNIV_SEARCH_FEMAIL_ONLY(CodeType.UNIV_SEARCH_FEMAIL, "3"),

    /** 検索-夜間：含む */
    UNIV_SEARCH_NIGHT_INCLUDE(CodeType.UNIV_SEARCH_NIGHT, "1"),

    /** 検索-夜間：含まない */
    UNIV_SEARCH_NIGHT_NOT_INCLUDE(CodeType.UNIV_SEARCH_NIGHT, "2"),

    /** 検索-夜間：夜間のみ */
    UNIV_SEARCH_NIGHT_ONLY(CodeType.UNIV_SEARCH_NIGHT, "3"),

    /** 検索-入試（科目・日程・評価）：評価範囲 */
    UNIV_SEARCH_EXAM_RANGE(CodeType.UNIV_SEARCH_EXAM, "1"),

    /** 検索-入試（科目・日程・評価）：入試日程 */
    UNIV_SEARCH_EXAM_SCHEDULE(CodeType.UNIV_SEARCH_EXAM, "2"),

    /** 検索-入試（科目・日程・評価）：使用科目 */
    UNIV_SEARCH_EXAM_SUBJECT(CodeType.UNIV_SEARCH_EXAM, "3"),

    /** 検索-評価範囲：センター */
    UNIV_SEARCH_RANGE_CENTER(CodeType.UNIV_SEARCH_RANGE, "1"),

    /** 検索-評価範囲：二次・個別 */
    UNIV_SEARCH_RANGE_SECOND(CodeType.UNIV_SEARCH_RANGE, "2"),

    /** 検索-評価範囲：総合（ドッキング） */
    UNIV_SEARCH_RANGE_TOTAL(CodeType.UNIV_SEARCH_RANGE, "3"),

    /** 検索-評価範囲（評価）：A */
    UNIV_SEARCH_RANGE_VALUE_A(CodeType.UNIV_SEARCH_RANGE_VALUE, Judge.JUDGE_STR_A),

    /** 検索-評価範囲（評価）：B */
    UNIV_SEARCH_RANGE_VALUE_B(CodeType.UNIV_SEARCH_RANGE_VALUE, Judge.JUDGE_STR_B),

    /** 検索-評価範囲（評価）：C */
    UNIV_SEARCH_RANGE_VALUE_C(CodeType.UNIV_SEARCH_RANGE_VALUE, Judge.JUDGE_STR_C),

    /** 検索-評価範囲（評価）：D */
    UNIV_SEARCH_RANGE_VALUE_D(CodeType.UNIV_SEARCH_RANGE_VALUE, Judge.JUDGE_STR_D),

    /** 検索-評価範囲（評価）：E */
    UNIV_SEARCH_RANGE_VALUE_E(CodeType.UNIV_SEARCH_RANGE_VALUE, Judge.JUDGE_STR_E),

    /** 検索-入試日程：前期 */
    UNIV_SEARCH_SCHEDULE_FIRST(CodeType.UNIV_SEARCH_SCHEDULE, "1"),

    /** 検索-入試日程：後期 */
    UNIV_SEARCH_SCHEDULE_SECOND(CodeType.UNIV_SEARCH_SCHEDULE, "2"),

    /** 検索-入試日程：中期 */
    UNIV_SEARCH_SCHEDULE_MIDDLE(CodeType.UNIV_SEARCH_SCHEDULE, "3"),

    /** 検索-入試日程：別日程 */
    UNIV_SEARCH_SCHEDULE_OTHER(CodeType.UNIV_SEARCH_SCHEDULE, "4"),

    /** 検索-入試日程：センター利用 */
    UNIV_SEARCH_SCHEDULE_CENTER(CodeType.UNIV_SEARCH_SCHEDULE2, "1"),

    /** 検索-入試日程：一般 */
    UNIV_SEARCH_SCHEDULE_GENERAL(CodeType.UNIV_SEARCH_SCHEDULE2, "2"),

    /** 検索-使用科目（課さない） */
    UNIV_SEARCH_NOT_IMPOSED(CodeType.UNIV_SEARCH_NOT_IMPOSED, "1"),

    /** 検索-センター個別 配点比：センター率65％以上 */
    UNIV_SEARCH_RATE_CENTER(CodeType.UNIV_SEARCH_RATE, "1"),

    /** 検索-センター個別 配点比：センターと個別 50：50 */
    UNIV_SEARCH_RATE_HALF(CodeType.UNIV_SEARCH_RATE, "2"),

    /** 検索-センター個別 配点比：個別率65％以上 */
    UNIV_SEARCH_RATE_INDIVIDUAL(CodeType.UNIV_SEARCH_RATE, "3"),

    /** 検索結果-国公立：前期 */
    UNIV_RESULT_UDIV_FIRST(CodeType.UNIV_RESULT_UDIV, "1"),

    /** 検索結果-国公立：後期 */
    UNIV_RESULT_UDIV_SECOND(CodeType.UNIV_RESULT_UDIV, "2"),

    /** 検索結果-国公立：中期 */
    UNIV_RESULT_UDIV_MIDDLE(CodeType.UNIV_RESULT_UDIV, "3"),

    /** 検索結果-国公立：別日程 */
    UNIV_RESULT_UDIV_OTHER(CodeType.UNIV_RESULT_UDIV, "4"),

    /** 検索結果-私立短大：センター利用 */
    UNIV_RESULT_PDIV_CENTER(CodeType.UNIV_RESULT_PDIV, "1"),

    /** 検索結果-私立短大：一般 */
    UNIV_RESULT_PDIV_GENERAL(CodeType.UNIV_RESULT_PDIV, "2"),

    /** 検索結果-大学区分：国立 */
    UNIV_RESULT_UNIV_DIV_NATIONAL(CodeType.UNIV_RESULT_UNIV_DIV, "1"),

    /** 検索結果-大学区分：公立 */
    UNIV_RESULT_UNIV_DIV_PUBLIC(CodeType.UNIV_RESULT_UNIV_DIV, "2"),

    /** 検索結果-大学区分：私立 */
    UNIV_RESULT_UNIV_DIV_PRIVATE(CodeType.UNIV_RESULT_UNIV_DIV, "3"),

    /** 検索結果-大学区分：その他 */
    UNIV_RESULT_UNIV_DIV_OTHER(CodeType.UNIV_RESULT_UNIV_DIV, "4"),

    /** 志望大学テーブル-区分：模試記入志望大学データ */
    CANDIDATE_UNIV_EXAM(CodeType.CANDIDATE_UNIV_SECTION, "1"),

    /** 志望大学テーブル-区分：志望大学（画面登録） */
    CANDIDATE_UNIV_SCREEN(CodeType.CANDIDATE_UNIV_SECTION, "2"),

    /** 中系統：文・人文 */
    M_STEMMA_11(CodeType.M_STEMMA, "0011"),

    /** 中系統：社会・国際 */
    M_STEMMA_21(CodeType.M_STEMMA, "0021"),

    /** 中系統：法・政治 */
    M_STEMMA_22(CodeType.M_STEMMA, "0022"),

    /** 中系統：経済・経営・商 */
    M_STEMMA_23(CodeType.M_STEMMA, "0023"),

    /** 中系統：教員養成 */
    M_STEMMA_31(CodeType.M_STEMMA, "0031"),

    /** 中系統：総合科学課程 */
    M_STEMMA_32(CodeType.M_STEMMA, "0032"),

    /** 中系統：理 */
    M_STEMMA_41(CodeType.M_STEMMA, "0041"),

    /** 中系統：工 */
    M_STEMMA_42(CodeType.M_STEMMA, "0042"),

    /** 中系統：農 */
    M_STEMMA_43(CodeType.M_STEMMA, "0043"),

    /** 中系統：医・歯・薬・保健 */
    M_STEMMA_51(CodeType.M_STEMMA, "0051"),

    /** 中系統：生活科学 */
    M_STEMMA_61(CodeType.M_STEMMA, "0061"),

    /** 中系統：芸術・スポーツ科学 */
    M_STEMMA_62(CodeType.M_STEMMA, "0062"),

    /** 中系統：総合・環境・情報・人間 */
    M_STEMMA_71(CodeType.M_STEMMA, "0071"),

    /** 小系統：文学 */
    S_STEMMA_01(M_STEMMA_11, CodeType.S_STEMMA, "0001"),

    /** 小系統：外国語 */
    S_STEMMA_02(M_STEMMA_11, CodeType.S_STEMMA, "0002"),

    /** 小系統：哲・史・教・心 */
    S_STEMMA_03(M_STEMMA_11, CodeType.S_STEMMA, "0003"),

    /** 小系統：社会・社会福祉 */
    S_STEMMA_04(M_STEMMA_21, CodeType.S_STEMMA, "0004"),

    /** 小系統：国際 */
    S_STEMMA_05(M_STEMMA_21, CodeType.S_STEMMA, "0005"),

    /** 小系統：法・政治 */
    S_STEMMA_06(M_STEMMA_22, CodeType.S_STEMMA, "0006"),

    /** 小系統：経済 */
    S_STEMMA_07(M_STEMMA_23, CodeType.S_STEMMA, "0007"),

    /** 小系統：経営・商・会計 */
    S_STEMMA_08(M_STEMMA_23, CodeType.S_STEMMA, "0008"),

    /** 小系統：教員養成−教科 */
    S_STEMMA_09(M_STEMMA_31, CodeType.S_STEMMA, "0009"),

    /** 小系統：教員養成−実技 */
    S_STEMMA_10(M_STEMMA_31, CodeType.S_STEMMA, "0010"),

    /** 小系統：教員養成−他 */
    S_STEMMA_11(M_STEMMA_31, CodeType.S_STEMMA, "0011"),

    /** 小系統：総合科学課程 */
    S_STEMMA_12(M_STEMMA_32, CodeType.S_STEMMA, "0012"),

    /** 小系統：理 */
    S_STEMMA_13(M_STEMMA_41, CodeType.S_STEMMA, "0013"),

    /** 小系統：機械・航空 */
    S_STEMMA_14(M_STEMMA_42, CodeType.S_STEMMA, "0014"),

    /** 小系統：電気電子・情報 */
    S_STEMMA_15(M_STEMMA_42, CodeType.S_STEMMA, "0015"),

    /** 小系統：土木・建築 */
    S_STEMMA_16(M_STEMMA_42, CodeType.S_STEMMA, "0016"),

    /** 小系統：応化・応物・資 */
    S_STEMMA_17(M_STEMMA_42, CodeType.S_STEMMA, "0017"),

    /** 小系統：生物・経営工他 */
    S_STEMMA_18(M_STEMMA_42, CodeType.S_STEMMA, "0018"),

    /** 小系統：農 */
    S_STEMMA_19(M_STEMMA_43, CodeType.S_STEMMA, "0019"),

    /** 小系統：医・歯 */
    S_STEMMA_20(M_STEMMA_51, CodeType.S_STEMMA, "0020"),

    /** 小系統：薬・看護・保健 */
    S_STEMMA_21(M_STEMMA_51, CodeType.S_STEMMA, "0021"),

    /** 小系統：生活科学 */
    S_STEMMA_22(M_STEMMA_61, CodeType.S_STEMMA, "0022"),

    /** 小系統：総・環・人・情 */
    S_STEMMA_23(M_STEMMA_71, CodeType.S_STEMMA, "0023"),

    /** 小系統：芸術・スポーツ科学 */
    S_STEMMA_24(M_STEMMA_62, CodeType.S_STEMMA, "0024"),

    /** 分野：日本文 */
    REGION_0100(S_STEMMA_01, CodeType.REGION, "0100"),

    /** 分野：外国文 */
    REGION_0200(S_STEMMA_01, CodeType.REGION, "0200"),

    /** 分野：哲・倫理・宗教 */
    REGION_0400(S_STEMMA_03, CodeType.REGION, "0400"),

    /** 分野：史・地理 */
    REGION_0500(S_STEMMA_03, CodeType.REGION, "0500"),

    /** 分野：教育 */
    REGION_0600(S_STEMMA_03, CodeType.REGION, "0600"),

    /** 分野：心理 */
    REGION_0700(S_STEMMA_03, CodeType.REGION, "0700"),

    /** 分野：地域・国際 */
    REGION_0800(S_STEMMA_03, CodeType.REGION, "0800"),

    /** 分野：文化・教養 */
    REGION_0900(S_STEMMA_03, CodeType.REGION, "0900"),

    /** 分野：社会 */
    REGION_1000(S_STEMMA_04, CodeType.REGION, "1000"),

    /** 分野：社会福祉 */
    REGION_1100(S_STEMMA_04, CodeType.REGION, "1100"),

    /** 分野：国際法 */
    REGION_1200(S_STEMMA_05, CodeType.REGION, "1200"),

    /** 分野：国際経済 */
    REGION_1300(S_STEMMA_05, CodeType.REGION, "1300"),

    /** 分野：国際関係 */
    REGION_1400(S_STEMMA_05, CodeType.REGION, "1400"),

    /** 分野：法 */
    REGION_1500(S_STEMMA_06, CodeType.REGION, "1500"),

    /** 分野：政治 */
    REGION_1600(S_STEMMA_06, CodeType.REGION, "1600"),

    /** 分野：経営 */
    REGION_1800(S_STEMMA_08, CodeType.REGION, "1800"),

    /** 分野：経営情報 */
    REGION_1900(S_STEMMA_08, CodeType.REGION, "1900"),

    /** 分野：商・会計・他 */
    REGION_2000(S_STEMMA_08, CodeType.REGION, "2000"),

    /** 分野：幼稚園 */
    REGION_2300(S_STEMMA_11, CodeType.REGION, "2300"),

    /** 分野：養護教諭 */
    REGION_2400(S_STEMMA_11, CodeType.REGION, "2400"),

    /** 分野：養護学校 */
    REGION_2500(S_STEMMA_11, CodeType.REGION, "2500"),

    /** 分野：その他教員養成 */
    REGION_2600(S_STEMMA_11, CodeType.REGION, "2600"),

    /** 分野：総課-スポーツ */
    REGION_2700(S_STEMMA_12, CodeType.REGION, "2700"),

    /** 分野：総課-芸術・デザイン */
    REGION_2800(S_STEMMA_12, CodeType.REGION, "2800"),

    /** 分野：総課-情報 */
    REGION_2900(S_STEMMA_12, CodeType.REGION, "2900"),

    /** 分野：総課-国際・言語・文化 */
    REGION_3000(S_STEMMA_12, CodeType.REGION, "3000"),

    /** 分野：総課-心理・臨床 */
    REGION_3100(S_STEMMA_12, CodeType.REGION, "3100"),

    /** 分野：総課-地域・社会・生活 */
    REGION_3200(S_STEMMA_12, CodeType.REGION, "3200"),

    /** 分野：総課-自然・環境 */
    REGION_3300(S_STEMMA_12, CodeType.REGION, "3300"),

    /** 分野：数学・数理情報 */
    REGION_3400(S_STEMMA_13, CodeType.REGION, "3400"),

    /** 分野：物理 */
    REGION_3500(S_STEMMA_13, CodeType.REGION, "3500"),

    /** 分野：化学 */
    REGION_3600(S_STEMMA_13, CodeType.REGION, "3600"),

    /** 分野：生物 */
    REGION_3700(S_STEMMA_13, CodeType.REGION, "3700"),

    /** 分野：地学・他 */
    REGION_3800(S_STEMMA_13, CodeType.REGION, "3800"),

    /** 分野：機械 */
    REGION_3900(S_STEMMA_14, CodeType.REGION, "3900"),

    /** 分野：航空・宇宙 */
    REGION_4000(S_STEMMA_14, CodeType.REGION, "4000"),

    /** 分野：電気・電子 */
    REGION_4100(S_STEMMA_15, CodeType.REGION, "4100"),

    /** 分野：通信・情報 */
    REGION_4200(S_STEMMA_15, CodeType.REGION, "4200"),

    /** 分野：建築 */
    REGION_4300(S_STEMMA_16, CodeType.REGION, "4300"),

    /** 分野：土木・環境 */
    REGION_4400(S_STEMMA_16, CodeType.REGION, "4400"),

    /** 分野：応用化学 */
    REGION_4500(S_STEMMA_17, CodeType.REGION, "4500"),

    /** 分野：材料・物質工 */
    REGION_4600(S_STEMMA_17, CodeType.REGION, "4600"),

    /** 分野：応用物理 */
    REGION_4700(S_STEMMA_17, CodeType.REGION, "4700"),

    /** 分野：資源・エネルギー */
    REGION_4800(S_STEMMA_17, CodeType.REGION, "4800"),

    /** 分野：生物工 */
    REGION_4900(S_STEMMA_18, CodeType.REGION, "4900"),

    /** 分野：経営工・管理工 */
    REGION_5000(S_STEMMA_18, CodeType.REGION, "5000"),

    /** 分野：船舶・海洋 */
    REGION_5100(S_STEMMA_18, CodeType.REGION, "5100"),

    /** 分野：デザイン工・他 */
    REGION_5200(S_STEMMA_18, CodeType.REGION, "5200"),

    /** 分野：生物生産・応用生命 */
    REGION_5300(S_STEMMA_19, CodeType.REGION, "5300"),

    /** 分野：環境科学 */
    REGION_5400(S_STEMMA_19, CodeType.REGION, "5400"),

    /** 分野：経済システム */
    REGION_5500(S_STEMMA_19, CodeType.REGION, "5500"),

    /** 分野：獣医 */
    REGION_5600(S_STEMMA_19, CodeType.REGION, "5600"),

    /** 分野：酪農・畜産 */
    REGION_5700(S_STEMMA_19, CodeType.REGION, "5700"),

    /** 分野：水産 */
    REGION_5800(S_STEMMA_19, CodeType.REGION, "5800"),

    /** 分野：医 */
    REGION_5900(S_STEMMA_20, CodeType.REGION, "5900"),

    /** 分野：歯 */
    REGION_6000(S_STEMMA_20, CodeType.REGION, "6000"),

    /** 分野：薬 */
    REGION_6100(S_STEMMA_21, CodeType.REGION, "6100"),

    /** 分野：看護 */
    REGION_6200(S_STEMMA_21, CodeType.REGION, "6200"),

    /** 分野：医療技術 */
    REGION_6300(S_STEMMA_21, CodeType.REGION, "6300"),

    /** 分野：保健・福祉 */
    REGION_6400(S_STEMMA_21, CodeType.REGION, "6400"),

    /** 分野：食物・栄養 */
    REGION_6500(S_STEMMA_22, CodeType.REGION, "6500"),

    /** 分野：被服 */
    REGION_6600(S_STEMMA_22, CodeType.REGION, "6600"),

    /** 分野：児童 */
    REGION_6700(S_STEMMA_22, CodeType.REGION, "6700"),

    /** 分野：住居 */
    REGION_6800(S_STEMMA_22, CodeType.REGION, "6800"),

    /** 分野：生活科学 */
    REGION_6900(S_STEMMA_22, CodeType.REGION, "6900"),

    /** 分野：総合 */
    REGION_7000(S_STEMMA_23, CodeType.REGION, "7000"),

    /** 分野：環境 */
    REGION_7100(S_STEMMA_23, CodeType.REGION, "7100"),

    /** 分野：情報 */
    REGION_7200(S_STEMMA_23, CodeType.REGION, "7300"),

    /** 分野：人間 */
    REGION_7300(S_STEMMA_23, CodeType.REGION, "7200"),

    /** 分野：美術 */
    REGION_7400(S_STEMMA_24, CodeType.REGION, "7400"),

    /** 分野：音楽 */
    REGION_7500(S_STEMMA_24, CodeType.REGION, "7500"),

    /** 分野：デザイン・その他芸術 */
    REGION_7600(S_STEMMA_24, CodeType.REGION, "7600"),

    /** 分野：芸術理論 */
    REGION_7700(S_STEMMA_24, CodeType.REGION, "7700"),

    /** 分野：スポーツ・健康 */
    REGION_7800(S_STEMMA_24, CodeType.REGION, "7800"),

    /** 資格カテゴリ：教員 */
    CREDENTIAL_CATEGORY1(CodeType.CREDENTIAL_CATEGORY, "1"),

    /** 資格カテゴリ：社会(教育)・福祉・心理 */
    CREDENTIAL_CATEGORY2(CodeType.CREDENTIAL_CATEGORY, "2"),

    /** 資格カテゴリ：医療・健康 */
    CREDENTIAL_CATEGORY3(CodeType.CREDENTIAL_CATEGORY, "3"),

    /** 資格カテゴリ：土木・建築・技術 */
    CREDENTIAL_CATEGORY4(CodeType.CREDENTIAL_CATEGORY, "4"),

    /** 資格カテゴリ：バイオ・食品・生活科学 */
    CREDENTIAL_CATEGORY5(CodeType.CREDENTIAL_CATEGORY, "5"),

    /** 資格：保育士 */
    CREDENTIAL_43(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "43", "2300"),

    /** 資格：幼稚園教諭 */
    CREDENTIAL_01(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "01", "1101", "1102"),

    /** 資格：小学校教諭 */
    CREDENTIAL_02(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "02", "1151", "1152"),

    /** 資格：中学校教諭(国語) */
    CREDENTIAL_03(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "03", "1201", "1202"),

    /** 資格：中学校教諭(社会) */
    CREDENTIAL_04(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "04", "1211", "1212"),

    /** 資格：中学校教諭(数学) */
    CREDENTIAL_05(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "05", "1221", "1222"),

    /** 資格：中学校教諭(理科) */
    CREDENTIAL_06(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "06", "1231", "1232"),

    /** 資格：中学校教諭(音楽) */
    CREDENTIAL_07(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "07", "1241", "1242"),

    /** 資格：中学校教諭(美術) */
    CREDENTIAL_08(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "08", "1251", "1252"),

    /** 資格：中学校教諭(保健体育) */
    CREDENTIAL_09(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "09", "1261", "1262"),

    /** 資格：中学校教諭(保健のみ) */
    CREDENTIAL_10(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "10", "1351", "1352"),

    /** 資格：中学校教諭(家庭) */
    CREDENTIAL_11(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "11", "1271", "1272"),

    /** 資格：中学校教諭(英語) */
    CREDENTIAL_12(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "12", "1281", "1282"),

    /** 資格：中学校教諭(技術) */
    CREDENTIAL_13(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "13", "1291", "1292"),

    /** 資格：高等学校教諭(国語) */
    CREDENTIAL_14(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "14", "1401"),

    /** 資格：高等学校教諭(地理歴史) */
    CREDENTIAL_15(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "15", "1411"),

    /** 資格：高等学校教諭(公民) */
    CREDENTIAL_16(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "16", "1421"),

    /** 資格：高等学校教諭(数学) */
    CREDENTIAL_17(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "17", "1431"),

    /** 資格：高等学校教諭(理科) */
    CREDENTIAL_18(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "18", "1441"),

    /** 資格：高等学校教諭(音楽) */
    CREDENTIAL_19(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "19", "1451"),

    /** 資格：高等学校教諭(美術) */
    CREDENTIAL_20(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "20", "1461"),

    /** 資格：高等学校教諭(保健体育) */
    CREDENTIAL_21(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "21", "1471"),

    /** 資格：高等学校教諭(保健のみ) */
    CREDENTIAL_22(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "22", "1481"),

    /** 資格：高等学校教諭(家庭) */
    CREDENTIAL_23(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "23", "1491"),

    /** 資格：高等学校教諭(英語) */
    CREDENTIAL_24(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "24", "1501"),

    /** 資格：高等学校教諭(書道) */
    CREDENTIAL_25(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "25", "1511"),

    /** 資格：高等学校教諭(工業) */
    CREDENTIAL_26(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "26", "1521"),

    /** 資格：高等学校教諭(工芸) */
    CREDENTIAL_27(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "27", "1531"),

    /** 資格：高等学校教諭(看護) */
    CREDENTIAL_28(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "28", "1601"),

    /** 資格：高等学校教諭(情報) */
    CREDENTIAL_29(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "29", "1611"),

    /** 資格：高等学校教諭(農業) */
    CREDENTIAL_30(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "30", "1621"),

    /** 資格：高等学校教諭(水産) */
    CREDENTIAL_31(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "31", "1641"),

    /** 資格：高等学校教諭(福祉) */
    CREDENTIAL_32(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "32", "1651"),

    /** 資格：高等学校教諭(商船) */
    CREDENTIAL_33(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "33", "1661"),

    /** 資格：高等学校教諭(商業) */
    CREDENTIAL_34(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "34", "1671"),

    /** 資格：特別支援学校教諭 */
    CREDENTIAL_35(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "35", "1801", "1802"),

    /** 資格：養護教諭 */
    CREDENTIAL_36(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "36", "1861", "1862"),

    /** 資格：学校図書館司書教諭 */
    CREDENTIAL_37(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "37", "1871"),

    /** 資格：栄養教諭 */
    CREDENTIAL_38(CREDENTIAL_CATEGORY1, CodeType.CREDENTIAL, "38", "1881", "1882"),

    /** 資格：社会調査士 */
    CREDENTIAL_39(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "39", "2100"),

    /** 資格：司書 */
    CREDENTIAL_40(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "40", "2200"),

    /** 資格：社会教育主事 */
    CREDENTIAL_41(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "41", "2210"),

    /** 資格：学芸員 */
    CREDENTIAL_42(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "42", "2220"),

    /** 資格：社会福祉士 */
    CREDENTIAL_44(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "44", "2310"),

    /** 資格：介護福祉士 */
    CREDENTIAL_45(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "45", "2320"),

    /** 資格：社会福祉主事 */
    CREDENTIAL_46(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "46", "2330"),

    /** 資格：児童福祉司 */
    CREDENTIAL_47(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "47", "2340"),

    /** 資格：精神保健福祉士 */
    CREDENTIAL_48(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "48", "2350"),

    /** 資格：認定心理士 */
    CREDENTIAL_49(CREDENTIAL_CATEGORY2, CodeType.CREDENTIAL, "49", "2500"),

    /** 資格：医師 */
    CREDENTIAL_50(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "50", "4100"),

    /** 資格：歯科医師 */
    CREDENTIAL_51(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "51", "4110"),

    /** 資格：獣医師 */
    CREDENTIAL_52(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "52", "4120"),

    /** 資格：薬剤師 */
    CREDENTIAL_53(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "53", "4130"),

    /** 資格：看護師 */
    CREDENTIAL_54(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "54", "4150"),

    /** 資格：保健師 */
    CREDENTIAL_55(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "55", "4160"),

    /** 資格：助産師 */
    CREDENTIAL_56(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "56", "4170"),

    /** 資格：理学療法士 */
    CREDENTIAL_57(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "57", "4200"),

    /** 資格：作業療法士 */
    CREDENTIAL_58(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "58", "4210"),

    /** 資格：言語聴覚士 */
    CREDENTIAL_59(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "59", "4220"),

    /** 資格：視能訓練士 */
    CREDENTIAL_60(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "60", "4230"),

    /** 資格：臨床検査技師 */
    CREDENTIAL_61(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "61", "4250"),

    /** 資格：診療放射線技師 */
    CREDENTIAL_62(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "62", "4260"),

    /** 資格：細胞検査士 */
    CREDENTIAL_63(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "63", "4270"),

    /** 資格：臨床工学技士 */
    CREDENTIAL_64(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "64", "4280"),

    /** 資格：歯科衛生士 */
    CREDENTIAL_65(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "65", "4290"),

    /** 資格：歯科技工士 */
    CREDENTIAL_66(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "66", "4300"),

    /** 資格：はり師・きゅう師 */
    CREDENTIAL_67(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "67", "4400"),

    /** 資格：救急救命士 */
    CREDENTIAL_68(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "68", "4450"),

    /** 資格：健康運動実践指導者 */
    CREDENTIAL_69(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "69", "4500"),

    /** 資格：柔道整復師 */
    CREDENTIAL_82(CREDENTIAL_CATEGORY3, CodeType.CREDENTIAL, "82", "4410"),

    /** 資格：測量士 */
    CREDENTIAL_70(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "70", "6150"),

    /** 資格：測量士補 */
    CREDENTIAL_71(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "71", "6100"),

    /** 資格：一級建築士 */
    CREDENTIAL_72(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "72", "6110"),

    /** 資格：二級建築士 */
    CREDENTIAL_73(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "73", "6120"),

    /** 資格：木造建築士 */
    CREDENTIAL_74(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "74", "6130"),

    /** 資格：技術士補 */
    CREDENTIAL_75(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "75", "6200"),

    /** 資格：操縦士 */
    CREDENTIAL_83(CREDENTIAL_CATEGORY4, CodeType.CREDENTIAL, "83", "6300"),

    /** 資格：バイオ技術者認定試験 */
    CREDENTIAL_76(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "76", "7100"),

    /** 資格：食品衛生監視員 */
    CREDENTIAL_77(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "77", "7300"),

    /** 資格：管理栄養士 */
    CREDENTIAL_78(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "78", "7500"),

    /** 資格：栄養士 */
    CREDENTIAL_79(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "79", "7510"),

    /** 資格：フードスペシャリスト */
    CREDENTIAL_80(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "80", "7520"),

    /** 資格：衣料管理士 */
    CREDENTIAL_81(CREDENTIAL_CATEGORY5, CodeType.CREDENTIAL, "81", "7531", "7532"),

    /** 受験スケジュール：並べ替え：志望順位 */
    SCHEDULE_ORDER_RANK(CodeType.SCHEDULE_ORDER, "1"),

    /** 受験スケジュール：並べ替え：評価(センター) */
    SCHEDULE_ORDER_CENTER(CodeType.SCHEDULE_ORDER, "2"),

    /** 受験スケジュール：並べ替え：評価(二次) */
    SCHEDULE_ORDER_SECOND(CodeType.SCHEDULE_ORDER, "3"),

    /** 受験スケジュール：並べ替え：評価(総合) */
    SCHEDULE_ORDER_TOTAL(CodeType.SCHEDULE_ORDER, "4"),

    /** 受験スケジュール：並べ替え：入試日 */
    SCHEDULE_ORDER_DATE(CodeType.SCHEDULE_ORDER, "5"),

    /** 入試本学地方区分：本学のみ */
    SCHOOLPROVENTDIV_HOME(CodeType.SCHOOLPROVENTDIV, "1", ScheduleType.INPLEDATE_HOME),

    /** 入試本学地方区分：本学または地方 */
    SCHOOLPROVENTDIV_HOME_OR_LOCAL(CodeType.SCHOOLPROVENTDIV, "2", ScheduleType.INPLEDATE_HOME_OR_LOCAL),

    /** 入試本学地方区分：地方のみ */
    SCHOOLPROVENTDIV_LOCAL(CodeType.SCHOOLPROVENTDIV, "3", ScheduleType.INPLEDATE_LOCAL),

    /** 入試出願締切区分：消印有効 */
    APPLIDEADLINEDIV_POST(CodeType.APPLIDEADLINEDIV, "1", ScheduleType.APPLIDEADLINE_POST),

    /** 入試出願締切区分：必着 */
    APPLIDEADLINEDIV_NLT(CodeType.APPLIDEADLINEDIV, "2", ScheduleType.APPLIDEADLINE_NLT),

    /** 入試出願締切区分：窓口のみ */
    APPLIDEADLINEDIV_DESK(CodeType.APPLIDEADLINEDIV, "3", ScheduleType.APPLIDEADLINE_DESK),

    /** 入試出願締切区分：ネットのみ */
    APPLIDEADLINEDIV_NET(CodeType.APPLIDEADLINEDIV, "4", ScheduleType.APPLIDEADLINE_NET),

    /** 入試出願締切区分：不明 */
    APPLIDEADLINEDIV_UNK(CodeType.APPLIDEADLINEDIV, "9", ScheduleType.APPLIDEADLINE_UNK),

    /** 一括出力：簡易個人表 */
    BATCH_OUTPUT_KOJIN(CodeType.BATCH_OUTPUT, "10"),

    /** 一括出力：簡易個人表-第1面 */
    BATCH_OUTPUT_KOJIN1(CodeType.BATCH_OUTPUT, "11"),

    /** 一括出力：簡易個人表-第2面 */
    BATCH_OUTPUT_KOJIN2(CodeType.BATCH_OUTPUT, "12"),

    /** 一括出力：志望大学リスト */
    BATCH_OUTPUT_CANDIDATE_LIST(CodeType.BATCH_OUTPUT, "20"),

    /** 一括出力：志望大学詳細 */
    BATCH_OUTPUT_CANDIDATE_DETAIL(CodeType.BATCH_OUTPUT, "30"),

    /** 一括出力：検索結果一覧 */
    BATCH_OUTPUT_SEARCH_RESULT(CodeType.BATCH_OUTPUT, "40"),

    /** 一括出力：受験スケジュール */
    BATCH_OUTPUT_SCHEDULE(CodeType.BATCH_OUTPUT, "50"),

    /** 帳票出力・CSVファイル：判定評価：A */
    OUTPUT_RATING_A(CodeType.OUTPUT_RATING, "1"),

    /** 帳票出力・CSVファイル：判定評価：B */
    OUTPUT_RATING_B(CodeType.OUTPUT_RATING, "2"),

    /** 帳票出力・CSVファイル：判定評価：C */
    OUTPUT_RATING_C(CodeType.OUTPUT_RATING, "3"),

    /** 帳票出力・CSVファイル：判定評価：D */
    OUTPUT_RATING_D(CodeType.OUTPUT_RATING, "4"),

    /** 帳票出力・CSVファイル：判定評価：E */
    OUTPUT_RATING_E(CodeType.OUTPUT_RATING, "5"),

    /** 帳票出力・CSVファイル：大学指定：大学区分 */
    OUTPUT_UNIV_CHOICE_DIV(CodeType.OUTPUT_UNIV_CHOICE, "1"),

    /** 帳票出力・CSVファイル：大学指定：大学名 */
    OUTPUT_UNIV_CHOICE_NAME(CodeType.OUTPUT_UNIV_CHOICE, "2"),

    /** 帳票出力・CSVファイル：大学指定：国公立大学 */
    OUTPUT_UNIV_DIV_NATIONAL(CodeType.OUTPUT_UNIV_DIV, "1"),

    /** 帳票出力・CSVファイル：大学指定：センター利用私大・短大 */
    OUTPUT_UNIV_DIV_CENTER(CodeType.OUTPUT_UNIV_DIV, "2"),

    /** 帳票出力・CSVファイル：大学指定：私立大学 */
    OUTPUT_UNIV_DIV_PRIVATE(CodeType.OUTPUT_UNIV_DIV, "3"),

    /** 帳票出力・CSVファイル：大学指定：短期大学・その他 */
    OUTPUT_UNIV_DIV_COLLEGE(CodeType.OUTPUT_UNIV_DIV, "4"),

    /** プログラム更新状況：更新可能 */
    PG_UPDATE_STATUS_UPDATABLE(CodeType.PG_UPDATE_STATUS, "1"),

    /** プログラム更新状況：更新済 */
    PG_UPDATE_STATUS_UPDATED(CodeType.PG_UPDATE_STATUS, "2"),

    /** 大学マスタダウンロード状況：提供予定 */
    UNIV_DL_UNAVAILABLE(CodeType.UNIV_DL_STATUS, "1"),

    /** 大学マスタダウンロード状況：更新可 */
    UNIV_DL_AVAILABLE(CodeType.UNIV_DL_STATUS, "2"),

    /** 大学マスタダウンロード状況：更新済 */
    UNIV_DL_DONE(CodeType.UNIV_DL_STATUS, "3"),

    /** 個人成績インポート状況：提供予定 */
    RECORD_IMP_UNAVAILABLE(CodeType.RECORD_IMP_STATUS, "1"),

    /** 個人成績インポート状況：取り込み可 */
    RECORD_IMP_AVAILABLE(CodeType.RECORD_IMP_STATUS, "2"),

    /** 個人成績インポート状況：取り込み済 */
    RECORD_IMP_DONE(CodeType.RECORD_IMP_STATUS, "3"),

    /** メンテナンス-インターネット接続：可能にする */
    MAINTE_INET_ENABLE(CodeType.MAINTE_INET, "1"),

    /** 検索-英語認定試験：含まないのみ */
    SEARCH_ENG_JOINING_EXCLUDE_ONLY(CodeType.SEARCH_ENG_JOINING, "1"),

    /** 入試結果入力：入試形態区分：一般 */
    ENTEXAMMODE001(CodeType.RESULT_ENTEXAMMODE, "1"),

    /** 入試結果入力：結果区分：合格 */
    RESULTDIV001(CodeType.RESULT_RESULTDIV, "1"),

    /** 入試結果入力：入学区分：入学 */
    ADMISSIONDIV001(CodeType.RESULT_ADMISSIONDIV, "1"),

    /** 英語認定試験入力：スコアが不明 */
    ENG_JOINING_INPUT_UNKNOWN_SCORE(CodeType.ENG_JOINING_INPUT, "1"),

    /** 英語認定試験入力：CEFRが不明 */
    ENG_JOINING_INPUT_UNKNOWN_CEFR(CodeType.ENG_JOINING_INPUT, "2"),

    /** CSV取込：大学コード：10桁 */
    CSV_IMPORT_UNIV_CODE_10(CodeType.CSV_IMPORT_UNIV_CODE, "1"),

    /** CSV取込：大学コード：5桁 */
    CSV_IMPORT_UNIV_CODE_5(CodeType.CSV_IMPORT_UNIV_CODE, "2"),

    /** CSV取込：取込データ：置換 */
    CSV_IMPORT_TYPE_REPLACE(CodeType.CSV_IMPORT_TYPE, "1"),

    /** CSV取込：取込データ：追加 */
    CSV_IMPORT_TYPE_APPEND(CodeType.CSV_IMPORT_TYPE, "2");

    /** 親コード */
    private final Code parent;

    /** コード種別 */
    private final CodeType codeType;

    /** コード値 */
    private final String value;

    /** コード属性値 */
    private final Object[] attribute;

    /**
     * コンストラクタです。
     *
     * @param parent 親コード
     * @param codeType コード種別
     * @param value コード値
     * @param attribute コード属性値
     */
    Code(Code parent, CodeType codeType, String value, Object... attribute) {
        this.parent = parent;
        this.codeType = codeType;
        this.value = value;
        this.attribute = attribute;
    }

    /**
     * コンストラクタです。
     *
     * @param codeType コード種別
     * @param value コード値
     * @param attribute コード属性値
     */
    Code(CodeType codeType, String value, Object... attribute) {
        this(null, codeType, value, attribute);
    }

    /**
     * コード名を返します。
     *
     * @return コード名
     */
    public String getName() {
        return getMessage("code." + codeType + "." + value);
    }

    /**
     * コード別名を返します。
     *
     * @return コード別名
     */
    public String getAliasName() {
        return getMessage("code." + codeType + "." + value + ".alias");
    }

    /**
     * 親コードを返します。
     *
     * @return 親コード
     */
    public Code getParent() {
        return parent;
    }

    /**
     * コード種別を返します。
     *
     * @return コード種別
     */
    public CodeType getCodeType() {
        return codeType;
    }

    /**
     * コード値を返します。
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * コード属性値を返します。
     *
     * @return コード属性値
     */
    public Object[] getAttribute() {
        return attribute;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.subs.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsSearchResultBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.bean.ExmntnSubsUnivSearchInBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

/**
 *
 * 大学検索DAOです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnSubsUnivSearchDao extends BaseDao {

    /**
     * 大学を検索します。
     *
     * @param searchBean 検索条件Bean
     * @return 検索結果リスト
     */
    public List<ExmntnSubsSearchResultBean> selectUnivList(ExmntnSubsUnivSearchInBean searchBean) {
        return jdbcManager.selectBySqlFile(ExmntnSubsSearchResultBean.class, getSqlPath(), searchBean).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.systop.lgin.component;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.util.SystopLginUtil;

/**
 *
 * 個人成績セルのセルレンダラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class SystopLginRecordCellRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {

        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        CellData cell = (CellData) table.getValueAt(row, column);
        Code code = (Code) cell.getSortValue();

        JLabel label = (JLabel) c;
        label.setHorizontalAlignment(SwingConstants.CENTER);

        if (code == Code.RECORD_IMP_UNAVAILABLE) {
            CellData examCell = (CellData) table.getValueAt(row, 0);
            ExamBean exam = (ExamBean) examCell.getSortValue();
            label.setForeground(Color.BLUE);
            label.setText(SystopLginUtil.toYMD(exam.getOutDataOpenDate()) + code.getName());
        } else if (code == Code.RECORD_IMP_AVAILABLE) {
            label.setForeground(Color.RED);
            label.setText(code.getName());
        } else {
            label.setForeground(Color.GRAY);
            label.setText(code.getName());
        }

        return c;
    }

}

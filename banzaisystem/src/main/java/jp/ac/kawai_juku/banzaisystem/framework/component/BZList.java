package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Action;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.listener.ControllerListSelectionListener;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * リストコンポーネントです。
 *
 *
 * @param <T> リストのデータ型
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class BZList<T extends LabelValueBean> extends BZScrollPane implements BZComponent {

    /** リストモデル */
    private final DefaultListModel<T> model = new DefaultListModel<>();

    /** リスト */
    private final JList<T> list = new JList<>(model);

    /**
     * コンストラクタです。
     */
    public BZList() {
        super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        list.setFocusable(false);

        if (!field.isAnnotationPresent(NoAction.class)) {
            Action action = field.getAnnotation(Action.class);
            if (action == null) {
                list.addListSelectionListener(new ControllerListSelectionListener(view, field.getName()));
            } else {
                list.addListSelectionListener(new ControllerListSelectionListener(view, action.value()));
            }
        }

        setViewportView(list);
    }

    /**
     * リストをセットします。
     *
     * @param list リスト
     */
    public void setList(List<T> list) {
        model.clear();
        for (T bean : list) {
            model.addElement(bean);
        }
    }

    /**
     * リストをクリアします。
     */
    public void clear() {
        model.clear();
    }

    /**
     * 選択リストを返します。
     *
     * @return 選択リスト
     */
    public List<T> getSelectedList() {
        return list.getSelectedValuesList();
    }

}

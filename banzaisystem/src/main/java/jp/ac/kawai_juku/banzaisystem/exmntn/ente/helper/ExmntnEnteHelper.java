package jp.ac.kawai_juku.banzaisystem.exmntn.ente.helper;

import javax.annotation.Resource;
import javax.swing.JCheckBox;
import javax.swing.JComponent;

import jp.ac.kawai_juku.banzaisystem.exmntn.ente.view.ExmntnEnteView;
import jp.ac.kawai_juku.banzaisystem.exmntn.subs.view.ExmntnSubsView;

/**
 *
 * 大学検索（入試情報（評価・日程・科目））画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnEnteHelper {

    /** View */
    @Resource(name = "exmntnEnteView")
    private ExmntnEnteView view;

    /** 大学検索画面のView */
    @Resource
    private ExmntnSubsView exmntnSubsView;

    /**
     * 入試日程コンポーネントの有効・無効状態を更新します。
     */
    public void renewScheduleStatus() {

        /* 区分「国立」「公立」「文科省所管外」がチェックされているか調べる */
        boolean nationalFlg = false;
        for (JComponent c : exmntnSubsView.findComponentsByGroup(ExmntnSubsView.GROUP_EXCEPT_PRIVATE)) {
            if (((JCheckBox) c).isSelected()) {
                nationalFlg = true;
                break;
            }
        }

        /* 区分「私立」「短大」がチェックされているか調べる */
        boolean privateFlg = false;
        for (JComponent c : exmntnSubsView.findComponentsByGroup(ExmntnSubsView.GROUP_PRIVATE)) {
            if (((JCheckBox) c).isSelected()) {
                privateFlg = true;
                break;
            }
        }

        /* 入試日程・方式チェックボックスの選択フラグ */
        boolean scheduleFlg = view.exmntnScheduleCheckBox.isSelected();

        view.setEnabledByGroup(ExmntnEnteView.GROUP_SCHEDULE_NATIONAL, scheduleFlg && (nationalFlg || !privateFlg));
        view.setEnabledByGroup(ExmntnEnteView.GROUP_SCHEDULE_PRIVATE, scheduleFlg && (!nationalFlg || privateFlg));
        view.clearExaminationScheduleButton.setEnabled(scheduleFlg);
    }

}

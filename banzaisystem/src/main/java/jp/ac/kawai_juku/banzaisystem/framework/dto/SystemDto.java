package jp.ac.kawai_juku.banzaisystem.framework.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCefrRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainJpnRecordBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamSubjectBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.RankBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.RecordInfoBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrBean;
import jp.co.fj.kawaijuku.judgement.beans.CefrConvScoreBean;
import jp.co.fj.kawaijuku.judgement.beans.EngCertJpnDesc;
import jp.co.fj.kawaijuku.judgement.beans.EngPtBean;
import jp.co.fj.kawaijuku.judgement.beans.EngPtDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.JpnDescTotalEvaluationBean;
import jp.co.fj.kawaijuku.judgement.beans.SubRecordAllBean;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuAppInfo;
import jp.co.fj.kawaijuku.judgement.data.EngSikakuScoreInfo;
import jp.co.fj.kawaijuku.judgement.data.JpnKijutuScoreInfo;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * システム全体で持ちまわる情報を保持するDTOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings({ "rawtypes" })
public class SystemDto implements EngCertJpnDesc {

    /** 先生フラグ */
    private boolean isTeacher;

    /** 大学マスタ情報 */
    private UnivMasterBean univMaster;

    /** システム年度 */
    private Integer nendo;

    /** 大学マスタインポート済の模試リスト */
    private List<ExamBean> examList;

    /** 選択中生徒の個人IDリスト */
    private List<Integer> selectedIndividualIdList;

    /** 対象生徒の個人ID */
    private Integer targetIndividualId;

    /** 判定用文字列マップ */
    private Map<String, String> jStringMap;

    /** ランクマップ */
    private Map<String, RankBean> rankMap;

    /** 換算得点マップ */
    private Map<String, Map<String, int[]>> cvsScoreMap;

    /** 科目別成績（全国）マップ */
    private Map<String, Map<String, SubRecordAllBean>> subrecordAllMap;

    /** 模試科目マスタマップ */
    private Map<String, Map<String, ExamSubjectBean>> examSubjectMap;

    /** 日程名マップ */
    private Map<String, String> scheduleNameMap;

    /** 成績データ情報 */
    private RecordInfoBean recordInfo;

    /** 成績データバージョン不整合フラグ */
    private boolean mismatchingRecordVersionFlg;

    /** 大学マスタバージョン不整合フラグ */
    private boolean mismatchingMasterVersionFlg;

    /** CEFR情報 */
    @Getter
    @Setter
    private List<CefrBean> cefrList;

    /** 認定試験情報 */
    @Getter
    @Setter
    private List<EngPtBean> engPtList;

    /** 認定試験詳細情報 */
    @Getter
    @Setter
    private List<EngPtDetailBean> engPtDetailList;

    /** CEFR換算用スコア情報 */
    @Getter
    @Setter
    private List<CefrConvScoreBean> cefrConvScoreList;

    /** 国語記述式総合評価情報 */
    @Getter
    @Setter
    private List<JpnDescTotalEvaluationBean> jpnWritingTotalEvaluationList;

    /** 英語資格・検定試験_出願要件情報 */
    @Getter
    @Setter
    private List<EngSikakuAppInfo> engPtappreqList;

    /** 英語資格・検定試験_得点換算情報 */
    @Getter
    @Setter
    private List<EngSikakuScoreInfo> entPtscoreconvList;

    /** 国語記述式_得点換算情報情報 */
    @Getter
    @Setter
    private List<JpnKijutuScoreInfo> kokugoDescscoreconvList;

    /** CEFR情報 */
    @Getter
    @Setter
    private List<ExmntnMainCefrRecordBean> cefrInfo;

    /** CEFR情報(変更値) */
    @Getter
    @Setter
    private List<ExmntnMainCefrRecordBean> cefrChangeInfo;

    /** 国語記述情報 */
    @Getter
    @Setter
    private List<ExmntnMainJpnRecordBean> jpnInfo;

    /** 情報誌用科目データマップ */
    private Map univInfo;

    /**
     * 先生フラグを返します。
     *
     * @return 先生フラグ
     */
    public boolean isTeacher() {
        return isTeacher;
    }

    /**
     * 先生フラグをセットします。
     *
     * @param isTeacher 先生フラグ
     */
    public void setTeacher(boolean isTeacher) {
        this.isTeacher = isTeacher;
    }

    /**
     * 大学マスタ最新年度を返します。
     *
     * @return 大学マスタ最新年度
     */
    public String getUnivYear() {
        return univMaster.getEventYear();
    }

    /**
     * 大学マスタ最新模試区分を返します。
     *
     * @return 大学マスタ最新模試区分
     */
    public String getUnivExamDiv() {
        return univMaster.getExamDiv();
    }

    /**
     * 選択中生徒の個人IDリストを返します。
     *
     * @return 選択中生徒の個人IDリスト
     */
    public List<Integer> getSelectedIndividualIdList() {
        return selectedIndividualIdList;
    }

    /**
     * 選択中生徒の個人IDリストをセットします。
     *
     * @param selectedIndividualIdList 選択中生徒の個人IDリスト
     */
    public void setSelectedIndividualIdList(List<Integer> selectedIndividualIdList) {
        this.selectedIndividualIdList = selectedIndividualIdList;
        if (!selectedIndividualIdList.isEmpty()) {
            setTargetIndividualId(selectedIndividualIdList.get(0));
        } else {
            setTargetIndividualId(null);
        }
    }

    /**
     * 選択中生徒の個人IDリストをクリアします。
     */
    public void clearSelectedIndividualIdList() {
        setSelectedIndividualIdList(Collections.<Integer> emptyList());
    }

    /**
     * 対象生徒の個人IDを返します。
     *
     * @return 対象生徒の個人ID
     */
    public Integer getTargetIndividualId() {
        return targetIndividualId;
    }

    /**
     * 対象生徒の個人IDをセットします。
     *
     * @param targetIndividualId 対象生徒の個人ID
     */
    public void setTargetIndividualId(Integer targetIndividualId) {
        this.targetIndividualId = targetIndividualId;
    }

    /**
     * システム年度を返します。<br>
     * 大学マスタ最新年度と同じ値です。
     *
     * @return システム年度
     */
    public Integer getNendo() {
        return nendo;
    }

    /**
     * 大学マスタインポート済の模試リストを返します。
     *
     * @return 大学マスタインポート済の模試リスト
     */
    public List<ExamBean> getExamList() {
        return examList;
    }

    /**
     * 大学マスタインポート済の模試リストをセットします。
     *
     * @param examList 大学マスタインポート済の模試リスト
     */
    public void setExamList(List<ExamBean> examList) {
        this.examList = examList;
    }

    /**
     * 判定用文字列マップを返します。
     *
     * @return 判定用文字列マップ
     */
    public Map<String, String> getjStringMap() {
        return jStringMap;
    }

    /**
     * 判定用文字列マップをセットします。
     *
     * @param jStringMap 判定用文字列マップ
     */
    public void setjStringMap(Map<String, String> jStringMap) {
        this.jStringMap = jStringMap;
    }

    /**
     * 大学マスタ情報をセットします。
     *
     * @param univMaster 大学マスタ情報
     */
    public void setUnivMaster(UnivMasterBean univMaster) {
        this.univMaster = univMaster;
        if (univMaster != null) {
            nendo = Integer.valueOf(univMaster.getEventYear());
        } else {
            nendo = null;
        }
    }

    /**
     * 大学マスタ情報を返します。
     *
     * @return 大学マスタ情報
     */
    public UnivMasterBean getUnivMaster() {
        return univMaster;
    }

    /**
     * 換算得点マップを返します。
     *
     * @return 換算得点マップ
     */
    public Map<String, Map<String, int[]>> getCvsScoreMap() {
        return cvsScoreMap;
    }

    /**
     * 換算得点マップをセットします。
     *
     * @param cvsScoreMap 換算得点マップ
     */
    public void setCvsScoreMap(Map<String, Map<String, int[]>> cvsScoreMap) {
        this.cvsScoreMap = cvsScoreMap;
    }

    /**
     * 科目別成績（全国）マップを返します。
     *
     * @return 科目別成績（全国）マップ
     */
    public Map<String, Map<String, SubRecordAllBean>> getSubrecordAllMap() {
        return subrecordAllMap;
    }

    /**
     * 科目別成績（全国）マップをセットします。
     *
     * @param subrecordAllMap 科目別成績（全国）マップ
     */
    public void setSubrecordAllMap(Map<String, Map<String, SubRecordAllBean>> subrecordAllMap) {
        this.subrecordAllMap = subrecordAllMap;
    }

    /**
     * ランクマップを返します。
     *
     * @return ランクマップ
     */
    public Map<String, RankBean> getRankMap() {
        return rankMap;
    }

    /**
     * ランクマップをセットします。
     *
     * @param rankMap ランクマップ
     */
    public void setRankMap(Map<String, RankBean> rankMap) {
        this.rankMap = rankMap;
    }

    /**
     * 成績データ情報を返します。
     *
     * @return 成績データ情報
     */
    public RecordInfoBean getRecordInfo() {
        return recordInfo;
    }

    /**
     * 成績データ情報をセットします。
     *
     * @param recordInfo 成績データ情報
     */
    public void setRecordInfo(RecordInfoBean recordInfo) {
        this.recordInfo = recordInfo;
    }

    /**
     * 成績データバージョンまたは大学マスタバージョンに不整合があるかを返します。
     *
     * @return 成績データバージョンまたは大学マスタバージョンに不整合があるならtrue
     */
    public boolean isMismatchingVersion() {
        return isMismatchingRecordVersionFlg() || isMismatchingMasterVersionFlg();
    }

    /**
     * 成績データバージョン不整合フラグを返します。
     *
     * @return 成績データバージョン不整合フラグ
     */
    public boolean isMismatchingRecordVersionFlg() {
        return mismatchingRecordVersionFlg;
    }

    /**
     * 成績データバージョン不整合フラグをセットします。
     *
     * @param mismatchingRecordVersionFlg 成績データバージョン不整合フラグ
     */
    public void setMismatchingRecordVersionFlg(boolean mismatchingRecordVersionFlg) {
        this.mismatchingRecordVersionFlg = mismatchingRecordVersionFlg;
    }

    /**
     * 大学マスタバージョン不整合フラグを返します。
     *
     * @return 大学マスタバージョン不整合フラグ
     */
    public boolean isMismatchingMasterVersionFlg() {
        return mismatchingMasterVersionFlg;
    }

    /**
     * 大学マスタバージョン不整合フラグをセットします。
     *
     * @param mismatchingMasterVersionFlg 大学マスタバージョン不整合フラグ
     */
    public void setMismatchingMasterVersionFlg(boolean mismatchingMasterVersionFlg) {
        this.mismatchingMasterVersionFlg = mismatchingMasterVersionFlg;
    }

    /**
     * 模試科目マスタマップを返します。
     *
     * @return 模試科目マスタマップ
     */
    public Map<String, Map<String, ExamSubjectBean>> getExamSubjectMap() {
        return examSubjectMap;
    }

    /**
     * 模試科目マスタマップをセットします。
     *
     * @param examSubjectMap 模試科目マスタマップ
     */
    public void setExamSubjectMap(Map<String, Map<String, ExamSubjectBean>> examSubjectMap) {
        this.examSubjectMap = examSubjectMap;
    }

    /**
     * 日程名マップを返します。
     *
     * @return 日程名マップ
     */
    public Map<String, String> getScheduleNameMap() {
        return scheduleNameMap;
    }

    /**
     * 日程名マップをセットします。
     *
     * @param scheduleNameMap 日程名マップ
     */
    public void setScheduleNameMap(Map<String, String> scheduleNameMap) {
        this.scheduleNameMap = scheduleNameMap;
    }

    /**
     * 情報誌用科目データマップを返します。
     *
     * @return 情報誌用科目データマップ
     */
    public Map getUnivInfo() {
        return univInfo;
    }

    /**
     * 情報誌用科目データマップを設定します。
     *
     * @param univInfo 情報誌用科目データマップ
     */
    public void setUnivInfo(Map univInfo) {
        this.univInfo = univInfo;
    }

    @Override
    public List<JpnDescTotalEvaluationBean> getJpnDescList() {
        return jpnWritingTotalEvaluationList;
    }

    /*@Override
    public List<EngPtBean> getEngCertExamList() {
        return engPtList;
    }*/

    /*@Override
    public List<EngPtDetailBean> getEngCertExamDetailList() {
        return engPtDetailList;
    }*/

}

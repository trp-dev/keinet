package jp.ac.kawai_juku.banzaisystem.exmntn.name.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.JComponent;

import jp.ac.kawai_juku.banzaisystem.framework.component.BaseImageToggleButton;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 都道府県画像ボタンです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class PrefButton extends BaseImageToggleButton {

    /** 県コード */
    private String prefCd;

    @Override
    public void initialize(Field field, final AbstractView view) {

        super.initialize(field, view);

        prefCd = field.getName().substring(4, 6);

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                boolean isAllSelected = true;

                for (JComponent jc : view.findComponentsByGroup(getGroup())) {
                    if (jc instanceof PrefButton) {
                        if (!((PrefButton) jc).isSelected()) {
                            isAllSelected = false;
                        }
                    }
                }

                for (JComponent jc : view.findComponentsByGroup(getGroup())) {
                    if (jc instanceof DistrictButton) {
                        ((DistrictButton) jc).setSelected(isAllSelected);
                        break;
                    }
                }
            }
        });
    }

    /**
     * 県コードを返します。
     *
     * @return 県コード
     */
    public String getPrefCd() {
        return prefCd;
    }

}

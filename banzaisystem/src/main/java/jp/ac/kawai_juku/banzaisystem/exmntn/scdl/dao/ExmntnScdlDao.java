package jp.ac.kawai_juku.banzaisystem.exmntn.scdl.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;
import jp.co.fj.kawaijuku.judgement.beans.ScheduleDetailBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 受験スケジュール画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnScdlDao extends BaseDao {

    /**
     * [先生用] 受験予定大学一覧を取得します。
     *
     * @param id 個人ID
     * @return 受験予定大学一覧
     */
    public List<ExmntnScdlBean> selectTeacherExamPlanUnivList(Integer id) {
        return jdbcManager.selectBySqlFile(ExmntnScdlBean.class, getSqlPath(), id).getResultList();
    }

    /**
     * 受験予定大学を取得します。
     *
     * @param id 個人ID
     * @param key {@link UnivKeyBean}
     * @return 受験予定大学
     */
    public ExmntnScdlBean selectExamPlanUnivBean(Integer id, UnivKeyBean key) {
        BeanMap param = new BeanMap();
        param.put("id", id);
        param.put("univCd", key.getUnivCd());
        param.put("facultyCd", key.getFacultyCd());
        param.put("deptCd", key.getDeptCd());
        return jdbcManager.selectBySqlFile(ExmntnScdlBean.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * [生徒用] 受験予定大学一覧を取得します。
     *
     * @param univList 大学キーリスト
     * @return 受験予定大学一覧
     */
    public List<ExmntnScdlBean> selectStudentExamPlanUnivList(List<? extends UnivKeyBean> univList) {
        BeanMap param = new BeanMap();
        param.put("where", DaoUtil.createUnivWhere(univList));
        return jdbcManager.selectBySqlFile(ExmntnScdlBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 受験予定大学の志望順位を更新します。
     *
     * @param beans {@link ExmntnScdlBean}
     */
    public void updateExamPlanUnivRank(ExmntnScdlBean... beans) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), beans).execute();
    }

    /**
     * 受験予定大学の地方フラグを更新します。
     *
     * @param bean {@link ExmntnScdlBean}
     */
    public void updateExamPlanUnivProvincesFlg(ExmntnScdlBean bean) {
        jdbcManager.updateBySqlFile(getSqlPath(), bean).execute();
    }

    /**
     * 受験予定大学を削除します。
     *
     * @param list 削除する受験予定大学
     */
    public void deleteExamPlanUniv(List<ExmntnScdlBean> list) {
        jdbcManager.updateBatchBySqlFile(getSqlPath(), list).execute();
    }

    /**
     * 入試日程詳細リストを取得します。
     *
     * @param key {@link UnivKeyBean}
     * @return 入試日程詳細リスト
     */
    public List<ScheduleDetailBean> selectScheduleDetailList(UnivKeyBean key) {
        return jdbcManager.selectBySqlFile(ScheduleDetailBean.class, getSqlPath(), key).getResultList();
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.dao;

import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;

/**
 *
 * データファイルロック処理のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class LockDao extends BaseDao {

    /**
     * 成績データ情報をダミー更新してロックを取得します。
     */
    public void updateRecordInfo4Lock() {
        if (jdbcManager.updateBySqlFile(getSqlPath()).execute() < 0) {
            throw new RuntimeException("成績データ情報の更新に失敗しました。");
        }
    }

}

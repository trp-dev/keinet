package jp.ac.kawai_juku.banzaisystem.selstd.dunv.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.ValidateUtil;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.bean.SelstdDunvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.service.SelstdDunvService;
import jp.ac.kawai_juku.banzaisystem.selstd.dunv.view.SelstdDunvView;

/**
 *
 * 大学選択ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class SelstdDunvController extends AbstractController {

    /** View */
    @Resource(name = "selstdDunvView")
    private SelstdDunvView view;

    /** Service */
    @Resource
    private SelstdDunvService selstdDunvService;

    /** OKボタンが押された場合の処理 */
    private SelstdDunvProcessor processor;

    /**
     * 対象生徒選択画面から遷移した場合の初期表示アクションです。
     *
     * @param bean {@link SelstdDunvInBean}
     * @return 大学選択ダイアログ
     */
    @Logging(GamenId.B_d2)
    @Execute
    public ExecuteResult index(SelstdDunvInBean bean) {

        /* 選択可能大学リストを取得する */
        List<SelstdDunvBean> list = selstdDunvService.findSelectableUnivList(bean.getExam(), bean.getIdList());

        /* 選択可能大学リストを設定する */
        view.selectableTable.setUnivList(list);

        if (!bean.getUnivList().isEmpty()) {
            /* 選択済大学を選択可能大学リストから削除する */
            view.selectableTable.removeUnivList(bean.getUnivList());
        }

        /* 選択済大学リストを設定する */
        view.selectedTable.setUnivList(bean.getUnivList());

        /* 大学名称検索条件をクリアする */
        view.univNameField.setText(null);

        /* OKボタンが押された場合の処理を保持する */
        processor = bean.getProcessor();

        return VIEW_RESULT;
    }

    /**
     * 検索開始ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult startSearchSmallButtonAction() {

        /* 大学名をチェックする */
        if (!ValidateUtil.isHiragana(view.univNameField.getText())) {
            throw new MessageDialogException(getMessage("label.selstd.dunv.B_E001"));
        }

        view.selectableTable.setUnivNameCondition(JpnStringUtil.normalize(JpnStringUtil.hira2Hkana(view.univNameField
                .getText())));

        return null;
    }

    /**
     * 追加矢印ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult addAllowButtonAction() {
        List<SelstdDunvBean> list = view.selectableTable.getSelectedUnivList();
        view.selectableTable.removeUnivList(list);
        view.selectedTable.addUnivList(list);
        return null;
    }

    /**
     * 削除矢印ボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult removeAllowButtonAction() {
        List<SelstdDunvBean> list = view.selectedTable.getSelectedUnivList();
        view.selectedTable.removeUnivList(list);
        view.selectableTable.addUnivList(list);
        return null;
    }

    /**
     * OKボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {
        processor.process(view.selectedTable.getUnivList());
        return CLOSE_RESULT;
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        return CLOSE_RESULT;
    }

    /** 大学選択ダイアログでOKボタンが押された場合の処理を定義するインターフェース */
    public interface SelstdDunvProcessor {

        /**
         * OKボタンがを押された場合の処理を行います。
         *
         * @param list 選択された大学リスト
         */
        void process(List<SelstdDunvBean> list);

    }

}

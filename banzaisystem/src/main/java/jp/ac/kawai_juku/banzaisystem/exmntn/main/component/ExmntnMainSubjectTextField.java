package jp.ac.kawai_juku.banzaisystem.exmntn.main.component;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.lang.reflect.Field;

import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.seasar.framework.container.SingletonS2Container;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.annotation.MarkTextField;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainCheckHelper;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

/**
 *
 * 個人成績画面の科目テキストフィールドです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnMainSubjectTextField extends TextField {

    /** エラーメッセージ */
    private String errorMessage;

    /** マーク模試コンポーネントフラグ */
    private boolean isMark;

    /** 科目情報 */
    private BZSubjectRecord subjectBean;

    /** ペアの科目コンボボックス */
    private ExmntnMainSubjectComboBox pairComboBox;

    /** 得点(記述のみ。2016年度以降は記述模試であってもDBに得点が入らなくなるので不要となる) */
    private String score;

    /**
     * コンストラクタです。
     */
    public ExmntnMainSubjectTextField() {
        setHorizontalAlignment(SwingConstants.RIGHT);
    }

    @Override
    public void initialize(Field field, AbstractView view) {

        super.initialize(field, view);

        isMark = field.isAnnotationPresent(MarkTextField.class);

        getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                changedUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                ExmntnMainCheckHelper helper = SingletonS2Container.getComponent(ExmntnMainCheckHelper.class);
                /* 模試が未選択の場合は処理しない */
                if (!helper.needCheck(isMark)) {
                    return;
                }
                /* 第1,第2入れ替えボタンの状態を変更する */
                helper.changeFirstSecondButton();
                /* 初期値に戻すボタンを有効化する */
                helper.enableReturnInitButton(isMark);
                /* 入力チェックを行う */
                if (isMark) {
                    helper.checkMark();
                } else {
                    helper.checkWritten();
                }
                /* エラーメッセージを表示する */
                showErrorMessage(helper);
            }
        });

        addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent focusevent) {
                showErrorMessage(SingletonS2Container.getComponent(ExmntnMainCheckHelper.class));
            }
        });
    }

    /**
     * エラーメッセージを表示します。
     *
     * @param helper {@link ExmntnMainCheckHelper}
     */
    private void showErrorMessage(ExmntnMainCheckHelper helper) {
        if (errorMessage != null) {
            if (isMark) {
                helper.showMarkErrorMessage(errorMessage);
            } else {
                helper.showWrittenErrorMessage(errorMessage);
            }
        }
    }

    /**
     * エラーメッセージを返します。
     *
     * @return エラーメッセージ
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * エラーメッセージをセットします。
     *
     * @param errorMessage エラーメッセージ
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        setBackground(errorMessage == null ? Color.WHITE : ExmntnMainCheckHelper.ERROR_COLOR);
    }

    /**
     * マーク模試コンポーネントフラグを返します。
     *
     * @return マーク模試コンポーネントフラグ
     */
    public boolean isMark() {
        return isMark;
    }

    /**
     * 科目情報を返します。
     *
     * @return 科目情報
     */
    public BZSubjectRecord getSubjectBean() {
        if (pairComboBox == null) {
            return subjectBean;
        } else {
            return pairComboBox.getSelectedValue();
        }
    }

    /**
     * 科目情報をセットします。
     *
     * @param subjectBean 科目情報
     */
    public void setSubjectBean(BZSubjectRecord subjectBean) {
        this.subjectBean = subjectBean;
    }

    /**
     * ペアの科目コンボボックスを返します。
     *
     * @return ペアの科目コンボボックス
     */
    public ExmntnMainSubjectComboBox getPairComboBox() {
        return pairComboBox;
    }

    /**
     * ペアの科目コンボボックスをセットします。
     *
     * @param pairComboBox ペアの科目コンボボックス
     */
    public void setPairComboBox(ExmntnMainSubjectComboBox pairComboBox) {
        this.pairComboBox = pairComboBox;
        this.pairComboBox.setPairTextField(this);
    }

    /**
     * 得点を返します。
     *
     * @return 得点
     */
    public String getScore() {
        return score;
    }

    /**
     * 得点をセットします。
     *
     * @param score 得点
     */
    public void setScore(String score) {
        this.score = score;
    }

}

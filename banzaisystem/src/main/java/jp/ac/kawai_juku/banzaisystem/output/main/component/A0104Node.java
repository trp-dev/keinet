package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean.SelstdDprtExcelMakerInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.maker.SelstdDprtA0104Maker;

/**
 *
 * 連続個人成績表のノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class A0104Node extends OutputExcelNode {

    /**
     * コンストラクタです。
     */
    A0104Node() {
        super("連続個人成績表");
    }

    @Override
    void output(OutputMainHelper helper) {
        new ExcelLauncher(GamenId.D).addCreator(SelstdDprtA0104Maker.class, new SelstdDprtExcelMakerInBean(helper.findTargetExam(), helper.findDockingExam(), helper.findPrintTargetList())).launch();
    }
}

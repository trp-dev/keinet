package jp.ac.kawai_juku.banzaisystem.systop.lgin.controller;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import javax.annotation.Resource;
import javax.swing.JOptionPane;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExitController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ViewResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingKey;
import jp.ac.kawai_juku.banzaisystem.framework.service.PasswordService;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.helper.MainteImptHelper;
import jp.ac.kawai_juku.banzaisystem.mainte.main.controller.MainteMainController;
import jp.ac.kawai_juku.banzaisystem.selstd.main.controller.SelstdMainController;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.service.SystopLginService;
import jp.ac.kawai_juku.banzaisystem.systop.lgin.view.SystopLginView;
import jp.ac.kawai_juku.banzaisystem.systop.strt.controller.SystopStrtController;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * マスタ・プログラムチェック画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SystopLginController extends ExitController {

    /** View */
    @Resource
    private SystopLginView systopLginView;

    /** サービス */
    @Resource
    private SystopLginService systopLginService;

    /** パスワードサービス */
    @Resource
    private PasswordService passwordService;

    /** メンテナンス-個人成績インポート画面のヘルパー */
    @Resource
    private MainteImptHelper mainteImptHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 初期表示アクションです。
     *
     * @return マスタ・プログラムチェック画面
     */
    @Logging(GamenId.A2)
    @Execute
    public ExecuteResult index() {

        /* パスワードをクリアする */
        clearButtonAction();

        /* 成績データバージョンまたは大学マスタバージョンに不整合があるなら戻れなくする */
        if (systemDto.isMismatchingVersion()) {
            systopLginView.backButton.setEnabled(false);
        }

        return new ViewResult() {
            @Override
            public void process(AbstractController controller) {
                super.process(controller);
                systopLginView.passwordField.requestFocusInWindow();
                if (systemDto.isMismatchingRecordVersionFlg()) {
                    throw new MessageDialogException(getMessage("error.systop.lgin.CM_E016"));
                }
            }
        };
    }

    /**
     * 戻るボタン押下アクションです。
     *
     * @return 起動画面
     */
    @Execute
    public ExecuteResult backButtonAction() {
        return forward(SystopStrtController.class, "redisplay");
    }

    /**
     * パスワードフィールドEnterアクションです。
     *
     * @return {@link #okButtonAction()}
     */
    @Execute
    public ExecuteResult passwordFieldAction() {
        /* OKボタンを押したときと同じ処理を行う */
        return okButtonAction();
    }

    /**
     * OKボタン押下アクションです。
     *
     * @return 対象生徒選択画面
     */
    @Execute
    public ExecuteResult okButtonAction() {
        if (passwordService
                .checkPassword(SettingKey.TEACHER_PASSWORD, systopLginView.passwordField.getStringPassword())) {
            if (systemDto.isMismatchingVersion()) {
                if (systemDto.isMismatchingMasterVersionFlg()) {
                    JOptionPane.showMessageDialog(getActiveWindow(), getMessage("error.systop.lgin.CM_E013"));
                }
                if (systemDto.isMismatchingRecordVersionFlg()) {
                    JOptionPane.showMessageDialog(getActiveWindow(), getMessage("error.systop.lgin.CM_E017"));
                }
                return forward(MainteMainController.class, "dispMainteMstrAction");
            } else {
                return forward(SelstdMainController.class);
            }
        } else {
            throw new MessageDialogException(getMessage("error.systop.lgin.A2_E001"));
        }
    }

    /**
     * クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearButtonAction() {
        systopLginView.passwordField.setText(StringUtils.EMPTY);
        return null;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import jp.ac.kawai_juku.banzaisystem.framework.util.RatingComparator;

/**
 *
 * 評価値セルデータを保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class RatingCellData extends CellData {

    /**
     * コンストラクタです。
     *
     * @param rating 評価値
     */
    public RatingCellData(String rating) {
        super(rating);
    }

    @Override
    public int compareTo(CellData o) {
        return RatingComparator.getInstance().compare((String) getSortValue(), (String) o.getSortValue());
    }

}

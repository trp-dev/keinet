package jp.ac.kawai_juku.banzaisystem.result.dunv.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;
import jp.ac.kawai_juku.banzaisystem.result.dunv.dao.ResultDunvDao;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 大学選択ダイアログのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultDunvService {

    /** DAO */
    @Resource(name = "resultDunvDao")
    private ResultDunvDao dao;

    /**
     * 大学リストを取得します。
     *
     * @param univName 大学名
     * @return 大学リスト
     */
    public List<LabelValueBean> findUnivList(String univName) {
        return dao.selectUnivList(JpnStringUtil.normalize(StringUtils.substring(JpnStringUtil.hira2Hkana(univName), 0,
                12)));
    }

    /**
     * 学部リストを取得します。
     *
     * @param univCd 大学コード
     * @return 学部リスト
     */
    public List<LabelValueBean> findFacultyList(String univCd) {
        return dao.selectFacultyList(univCd);
    }

    /**
     * 学科リストを取得します。
     *
     * @param univCd 大学コード
     * @param facultyCd 学部コード
     * @return 学科リスト
     */
    public List<LabelValueBean> findDeptList(String univCd, String facultyCd) {
        return dao.selectDeptList(univCd, facultyCd);
    }

}

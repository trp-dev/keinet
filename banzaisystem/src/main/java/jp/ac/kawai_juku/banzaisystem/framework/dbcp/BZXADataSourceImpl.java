package jp.ac.kawai_juku.banzaisystem.framework.dbcp;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.XAConnection;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.bean.UnivMasterBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.exception.RecordInfoMismatchException;
import jp.ac.kawai_juku.banzaisystem.framework.exception.UnivMasterMismatchException;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.DaoUtil;

import org.seasar.extension.dbcp.impl.XADataSourceImpl;

/**
 *
 * バンザイシステムのXADataSource実装です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZXADataSourceImpl extends XADataSourceImpl {

    /**
     * 大学マスタDBファイルのパスを返します。
     *
     * @return ファイルパス
     */
    public String getUnivPath() {
        File file = new File(SettingPropsUtil.getUnivDir(), BZConstants.UNIV_FILE_NAME);
        checkFile(file, true);
        return file.getAbsolutePath();
    }

    /**
     * 成績データDBファイルのパスを返します。
     *
     * @return ファイルパス
     */
    public String getRecordPath() {
        File file = new File(SettingPropsUtil.getRecordDir(), BZConstants.RECORD_FILE_NAME);
        checkFile(file, false);
        return file.getAbsolutePath();
    }

    /**
     * DBファイルをチェックします。
     *
     * @param file DBファイル
     * @param isReadonly 読み込み専用フラグ
     */
    private void checkFile(File file, boolean isReadonly) {
        if (!file.isFile()) {
            throw new MessageDialogException(getMessage("error.framework.CM_E003"), new FileNotFoundException(
                    "DBファイルが見つかりませんでした。" + file.toString()));
        }
        if (!file.canRead()) {
            throw new MessageDialogException(getMessage("error.framework.CM_E003"), new IOException(
                    "DBファイルの読み込み権限がありません。" + file.toString()));
        }
        if (!isReadonly && !file.canWrite()) {
            throw new MessageDialogException(getMessage("error.framework.CM_E003"), new IOException(
                    "DBファイルの書き込み権限がありません。" + file.toString()));
        }
    }

    @Override
    public XAConnection getXAConnection(String user, String password) throws SQLException {

        setURL("jdbc:sqlite:" + getUnivPath());
        XAConnection con = super.getXAConnection(user, password);

        try {
            SystemDto systemDto = getComponent(SystemDto.class);
            if (systemDto.isTeacher()) {
                /* 先生用のみ成績データDBをアタッチする */
                attach(con.getConnection());
                /* 成績データバージョンの整合性をチェックする */
                if (systemDto.getRecordInfo() != null
                        && systemDto.getRecordInfo().getVersion() != DaoUtil.getRecordVersion(con.getConnection())) {
                    systemDto.setRecordInfo(null);
                    throw new RecordInfoMismatchException();
                }
            }

            UnivMasterBean univMaster = DaoUtil.findUnivMasterBean(con.getConnection());
            if (systemDto.getUnivMaster() == null) {
                systemDto.setUnivMaster(univMaster);
            } else if (!systemDto.getUnivMaster().equals(univMaster)) {
                throw new UnivMasterMismatchException();
            }
        } catch (Exception e) {
            con.close();
            throw e;
        }

        return con;
    }

    /**
     * 成績データDBファイルをアタッチします。
     *
     * @param con DBコネクション
     * @throws SQLException SQL例外
     */
    private void attach(Connection con) throws SQLException {
        try (PreparedStatement ps = con.prepareStatement("ATTACH ? AS REC")) {
            ps.setString(1, getRecordPath());
            ps.execute();
        }
    }

}

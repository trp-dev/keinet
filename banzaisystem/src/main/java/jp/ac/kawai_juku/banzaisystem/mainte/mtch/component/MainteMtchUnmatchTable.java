package jp.ac.kawai_juku.banzaisystem.mainte.mtch.component;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImagePanel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.BZTable;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.ReadOnlyTableModel;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.TableScrollPane;
import jp.ac.kawai_juku.banzaisystem.framework.util.ImageUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * 基準ファイルアンマッチ状況テーブルです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class MainteMtchUnmatchTable extends TableScrollPane {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** カラム区切り線の色 */
    private static final Color SEPARATOR_COLOR = new Color(204, 204, 204);

    /** カラム幅定義 */
    private static final int[] COL_WIDTH = { 68, 65, 68, 203, 24 };

    /**
     * コンストラクタです。
     */
    public MainteMtchUnmatchTable() {
        super(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        setBorder(new BevelBorder(BevelBorder.LOWERED));
    }

    @Override
    public void initialize(Field field, AbstractView view) {
        BufferedImage img = ImageUtil.readImage(getClass(), "table/upper_right_corner.png");
        JLabel label = new JLabel(new ImageIcon(img));
        label.setSize(img.getWidth(), img.getHeight());
        setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, label);
        setColumnHeaderView(createColumnHeaderView());
        setViewportView(createTableView());
    }

    /**
     * スクロール領域のヘッダを生成します。
     *
     * @return ヘッダ
     */
    private JPanel createColumnHeaderView() {
        ImagePanel panel = new ImagePanel(getClass(), "table/unmatch.png");
        return panel;
    }

    /**
     * テーブルを生成します。
     *
     * @return テーブルを含むパネル
     */
    private JPanel createTableView() {

        JTable table = new BZTable(new ReadOnlyTableModel(COL_WIDTH.length + 1)) {

            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                if (ArrayUtils.indexOf(getSelectedRows(), row) < 0) {
                    c.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
                }
                return c;
            }

            @Override
            protected void paintComponent(Graphics g) {

                super.paintComponent(g);

                /* カラムの区切り線を描画する */
                int x = 0;
                g.setColor(SEPARATOR_COLOR);
                for (int width : COL_WIDTH) {
                    x += width;
                    g.drawLine(x - 1, 0, x - 1, getHeight());
                }
            }

        };

        table.setFocusable(false);
        table.setRowHeight(20);
        table.setIntercellSpacing(new Dimension(0, 0));
        table.setShowHorizontalLines(false);
        table.setShowVerticalLines(false);

        /*
         * 列の幅をヘッダの列と合わせ、アライメントを設定する。
         * また、先頭列は隠しデータ用のため、非表示にする。
         */
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.removeColumn(columnModel.getColumn(0));
        for (int i = 0; i < COL_WIDTH.length; i++) {
            columnModel.getColumn(i).setPreferredWidth(COL_WIDTH[i]);
        }

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.add(table);

        return panel;
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

import jp.co.fj.kawaijuku.judgement.data.SubjectRecord;

/**
 *
 * 成績保存用のDAO入力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 */
@SuppressWarnings("serial")
public class ExmntnMainSaveScoreDaoInBean extends SubjectRecord {

    /** 個人ID */
    private Integer individualId;

    /** 模試年度 */
    private String examYear;

    /** 模試コード */
    private String examCd;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

}

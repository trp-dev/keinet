package jp.ac.kawai_juku.banzaisystem.exmntn.prnt.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.bean.ExmntnPrntUnivDetailBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.prnt.dao.ExmntnPrntDao;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.util.ExmntnRsltUtil;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ScoreBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZJudgementExecutor;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 志望大学リストのサービスです。
 *
 *
 * @author TOTEC)FURUZAWA.Yusuke
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnPrntService {

    /** DAO */
    @Resource
    private ExmntnPrntDao exmntnPrntDao;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 志望大学詳細リストを生成します。
     *
     * @param candidateUnivList 志望大学情報リスト
     * @param scoreBean 成績データ
     * @param engFlg 英語認定試験含むフラグ
     * @return 志望大学詳細リスト
     */
    public List<ExmntnPrntUnivDetailBean> createUnivDetailList(List<ExmntnMainCandidateUnivBean> candidateUnivList, ScoreBean scoreBean, boolean engFlg) {

        List<ExmntnPrntUnivDetailBean> list = CollectionsUtil.newArrayList(candidateUnivList.size());

        for (ExmntnMainCandidateUnivBean bean : candidateUnivList) {
            ExmntnPrntUnivDetailBean detail = exmntnPrntDao.selectUnivDetail(bean);
            /* 区分を設定する */
            detail.setSection(ExmntnRsltUtil.toDivision(bean.getUnivCd()).toString());
            /* 注意マーク表示フラグを設定する */
            detail.setCautionNeeded(ScheduleUtil.isCautionNeeded(systemDto.getUnivExamDiv(), detail.getUnivCd(), detail.getExamScheduleDetailFlag(), detail.getUnivCommentFlag()));
            list.add(detail);
        }

        /* 判定を実行する */
        /*
        if (!engFlg) {
            BZJudgementExecutor.getInstance().judge(scoreBean, list, false);
        } else {
            BZJudgementExecutor.getInstance().judge(scoreBean, list, true);
        }
        */
        BZJudgementExecutor.getInstance().judge(scoreBean, list);
        return list;
    }

}

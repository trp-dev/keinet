package jp.ac.kawai_juku.banzaisystem.framework.controller;

import jp.ac.kawai_juku.banzaisystem.framework.util.ControllerUtil;

/**
 *
 * コントローラを起動するアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ControllerResult implements ExecuteResult {

    /** 起動するコントローラのクラス */
    private final Class<? extends AbstractController> controllerClass;

    /** 起動するメソッド名 */
    private final String methodName;

    /** 起動するメソッドのパラメータ */
    private final Object[] params;

    /**
     * コンストラクタです。
     *
     * @param controllerClass 起動するコントローラのクラス
     * @param methodName 起動するメソッド名
     * @param params 起動するメソッドのパラメータ
     */
    ControllerResult(Class<? extends AbstractController> controllerClass, String methodName, Object[] params) {
        this.controllerClass = controllerClass;
        this.methodName = methodName;
        this.params = params;
    }

    @Override
    public void process(AbstractController controller) {
        ControllerUtil.invoke(controllerClass, methodName, params);
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dprt.view;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.CodeRadio;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.CodeConfig;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Group;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.HorizontalAlignment;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NoAction;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.NumberField;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.component.SelstdDprtClassTable;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.component.SelstdDprtClassTable.TableCellUpdatedAction;
import jp.ac.kawai_juku.banzaisystem.selstd.dprt.controller.SelstdDprtController;

/**
 *
 * 一覧出力ダイアログのViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
@Dialog
public class SelstdDprtView extends AbstractView {

    /** コンボボックス_模試 */
    @Location(x = 85, y = 20)
    @Size(width = 207, height = 20)
    @FireActionEvent
    public ComboBox<ExamBean> examComboBox;

    /** 「成績一覧」ラジオ */
    @Location(x = 29, y = 84)
    @CodeConfig(Code.SCORE_LIST)
    @NoAction
    public CodeRadio scoreListRadio;

    /** 「総合成績＋志望大学（上位9）」ラジオ */
    @Location(x = 29, y = 106)
    @CodeConfig(Code.SCORE_PLUS_UNIV9)
    @NoAction
    public CodeRadio scorePlusUniv9Radio;

    /** 「総合成績＋志望大学（上位20）」ラジオ */
    @Location(x = 29, y = 128)
    @CodeConfig(Code.SCORE_PLUS_UNIV20)
    @NoAction
    public CodeRadio scorePlusUniv20Radio;

    /** 「画面で選択されている生徒」ラジオ */
    @Location(x = 29, y = 196)
    @CodeConfig(Code.SELECTED_STUDENT)
    public CodeRadio selectedStudentRadio;

    /** 「改めて選択する」ラジオ */
    @Location(x = 29, y = 218)
    @CodeConfig(Code.SELECT_AGAIN)
    public CodeRadio selectAgain20Radio;

    /** コンボボックス_学年 */
    @Location(x = 29, y = 240)
    @Size(width = 42, height = 20)
    @Group
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** クラステーブル */
    @Location(x = 27, y = 290)
    @Size(width = 77, height = 131)
    @Group
    public SelstdDprtClassTable classTable;

    /** 全選択ボタン */
    @Location(x = 26, y = 425)
    @Group
    public ImageButton allChoiceButton;

    /** 全解除ボタン */
    @Location(x = 26, y = 444)
    @Group
    public ImageButton allLiftButton;

    /** 席次番号（開始）テキストフィールド */
    @Location(x = 136, y = 290)
    @Size(width = 51, height = 19)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Group
    @NumberField(maxLength = 5)
    public TextField numberStartField;

    /** 席次番号（終了）テキストフィールド */
    @Location(x = 213, y = 290)
    @Size(width = 51, height = 19)
    @HorizontalAlignment(SwingConstants.RIGHT)
    @Group
    @NumberField(maxLength = 5)
    public TextField numberEndField;

    /** 生徒一覧から選択ボタン */
    @Location(x = 136, y = 316)
    @Group
    public ImageButton choiceStudentListButton;

    /** 印刷ボタン */
    @Location(x = 95, y = 478)
    public ImageButton printButton;

    @Override
    public void initialize() {

        super.initialize();

        /* 席次番号の値変更時に、印刷ボタンの状態変更アクションを呼び出すようにする */
        final SelstdDprtController controller = getComponent(SelstdDprtController.class);
        DocumentListener listener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                controller.changePrintButtonStatus();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                insertUpdate(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                insertUpdate(e);
            }
        };
        numberStartField.getDocument().addDocumentListener(listener);
        numberEndField.getDocument().addDocumentListener(listener);

        /* クラス選択時の処理を設定する */
        classTable.setUpdatedAction(new TableCellUpdatedAction() {
            @Override
            public void updated() {
                controller.changePrintButtonStatus();
            }
        });

        /* 模試コンボボックスの表示最大値を設定する */
        examComboBox.setMaximumRowCount(10);
    }

}

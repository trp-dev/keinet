package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellEditor;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.CodeType;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;

/**
 *
 * コードを利用するコンボボックスセルのエディタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class CodeComboCellEditor extends AbstractCellEditor implements TableCellEditor {

    /** 偶数行の背景色 */
    private static final Color EVEN_ROW_COLOR = new Color(246, 246, 246);

    /** パネル */
    private final JPanel panel;

    /** コンボボックス */
    private final JComboBox<ComboBoxItem<String>> combo = new JComboBox<ComboBoxItem<String>>();

    /**
     * コンストラクタです。
     *
     * @param codeType コード種別
     * @param width セル幅
     * @param height セル高さ
     * @param borderColor カラムの区切り線の色
     */
    public CodeComboCellEditor(CodeType codeType, int width, int height, Color borderColor) {
        panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 9, (int) Math.floor((double) (height - 20) / 2.0)));
        combo.setBorder(new MatteBorder(1, 1, 1, 1, new Color(165, 172, 178)));
        combo.setPreferredSize(new Dimension(width - 18, 20));
        combo.setRequestFocusEnabled(false);
        combo.addItem(new ComboBoxItem<String>(null));
        for (Code code : codeType.findCodeList()) {
            combo.addItem(new ComboBoxItem<String>(code.getName(), code.getValue()));
        }
        if (borderColor != null) {
            panel.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, borderColor));
        }
        panel.add(combo);
    }

    /**
     * コンストラクタです。
     *
     * @param codeType コード種別
     * @param width セル幅
     * @param height セル高さ
     */
    public CodeComboCellEditor(CodeType codeType, int width, int height) {
        this(codeType, width, height, null);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (isSelected) {
            panel.setBackground(table.getSelectionBackground());
        } else {
            panel.setBackground(row % 2 > 0 ? EVEN_ROW_COLOR : Color.WHITE);
        }
        combo.setSelectedIndex(0);
        if (value != null) {
            for (int i = 0; i < combo.getItemCount(); i++) {
                if (value.equals(combo.getItemAt(i).getValue())) {
                    combo.setSelectedIndex(i);
                    break;
                }
            }
        }
        return panel;
    }

    @Override
    public Object getCellEditorValue() {
        return ((ComboBoxItem<?>) combo.getSelectedItem()).getValue();
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return false;
    }

}

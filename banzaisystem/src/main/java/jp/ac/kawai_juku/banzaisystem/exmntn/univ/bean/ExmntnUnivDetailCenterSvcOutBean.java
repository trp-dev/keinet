package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

/**
 *
 * 志望大学詳細 センター試験情報Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivDetailCenterSvcOutBean {

    /** 採用枝番 */
    private String branchCd;

    /** 評価 */
    private String rating;

    /** 評価得点 */
    private String score;

    /** 英語資格要件 */
    private String englishRequirements;

    /** 出願要件パターンコード */
    private String appReqPaternCd;

    /** 満点 */
    private String fullPoint;

    /** ボーダライン */
    private String border;

    /** 評価基準-A */
    private String lineA;

    /** 評価基準-B */
    private String lineB;

    /** 評価基準-C */
    private String lineC;

    /** 評価基準-D */
    private String lineD;

    /** 判定科目 */
    private String subCount;

    /** 配点-英語(筆記＋リスニング) */
    private String allotEnglish;

    /** 配点-英語(筆記) */
    private String allotWriting;

    /** 配点-英語(リスニング) */
    private String allotListening;

    /** 配点-英語認定試験 */
    private String allotEnglishJoining;

    /** 配点-教科(数学) */
    private String allotMath;

    /** 配点-教科(国語) */
    private String allotJap;

    /** 配点-教科(国語記述) */
    private String allotJapDesc;

    /** 配点-教科(理科) */
    private String allotSci;

    /** 配点-教科(社会) */
    private String allotSoc;

    /** 得点-英語(筆記＋リスニング) */
    private String scoreEnglish;

    /** 得点-英語認定試験 */
    private String scoreEnglishJoining;

    /** 得点-数学1 */
    private String scoreMath1;

    /** 得点-数学2 */
    private String scoreMath2;

    /** 得点-国語 */
    private String scoreJap;

    /** 得点-国語記述 */
    private String scoreJapDesc;

    /** 得点-理科1 */
    private String scoreSci1;

    /** 得点-理科2 */
    private String scoreSci2;

    /** 得点-理科3 */
    private String scoreSci3;

    /** 得点-社会1 */
    private String scoreSoc1;

    /** 得点-社会2 */
    private String scoreSoc2;

    /** 備考 */
    private String note;

    /** あと何点-判定 */
    private String remainRaiting;

    /** あと何点-得点 */
    private String remainScore;

    /** 第1段階選抜予想 */
    private String pickedLine;

    /** 本人得点(足切評価得点) */
    private String rejectScore;

    /** 注釈文 */
    private String commentStatement;

    /** ５桁コード */
    private String choiceCd5;

    /**
     * 評価を返します。
     *
     * @return 評価
     */
    public String getRating() {
        return rating;
    }

    /**
     * 評価をセットします。
     *
     * @param rating 評価
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    /**
     * 評価得点を返します。
     *
     * @return 評価得点
     */
    public String getScore() {
        return score;
    }

    /**
     * 評価得点をセットします。
     *
     * @param score 評価得点
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * 満点を返します。
     *
     * @return 満点
     */
    public String getFullPoint() {
        return fullPoint;
    }

    /**
     * 満点をセットします。
     *
     * @param fullPoint 満点
     */
    public void setFullPoint(String fullPoint) {
        this.fullPoint = fullPoint;
    }

    /**
     * ボーダラインを返します。
     *
     * @return ボーダライン
     */
    public String getBorder() {
        return border;
    }

    /**
     * ボーダラインをセットします。
     *
     * @param border ボーダライン
     */
    public void setBorder(String border) {
        this.border = border;
    }

    /**
     * 評価基準-Aを返します。
     *
     * @return 評価基準-A
     */
    public String getLineA() {
        return lineA;
    }

    /**
     * 評価基準-Aをセットします。
     *
     * @param lineA 評価基準-A
     */
    public void setLineA(String lineA) {
        this.lineA = lineA;
    }

    /**
     * 評価基準-Bを返します。
     *
     * @return 評価基準-B
     */
    public String getLineB() {
        return lineB;
    }

    /**
     * 評価基準-Bをセットします。
     *
     * @param lineB 評価基準-B
     */
    public void setLineB(String lineB) {
        this.lineB = lineB;
    }

    /**
     * 評価基準-Cを返します。
     *
     * @return 評価基準-C
     */
    public String getLineC() {
        return lineC;
    }

    /**
     * 評価基準-Cをセットします。
     *
     * @param lineC 評価基準-C
     */
    public void setLineC(String lineC) {
        this.lineC = lineC;
    }

    /**
     * 評価基準-Dを返します。
     *
     * @return 評価基準-D
     */
    public String getLineD() {
        return lineD;
    }

    /**
     * 評価基準-Dをセットします。
     *
     * @param lineD 評価基準-D
     */
    public void setLineD(String lineD) {
        this.lineD = lineD;
    }

    /**
     * 配点-英語(筆記＋リスニング)を返します。
     *
     * @return 配点-英語(筆記＋リスニング)
     */
    public String getAllotEnglish() {
        return allotEnglish;
    }

    /**
     * 配点-英語(筆記＋リスニング)をセットします。
     *
     * @param allotEnglish 配点-英語(筆記＋リスニング)
     */
    public void setAllotEnglish(String allotEnglish) {
        this.allotEnglish = allotEnglish;
    }

    /**
     * 配点-英語(リスニング)を返します。
     *
     * @return 配点-英語(リスニング)
     */
    public String getAllotListening() {
        return allotListening;
    }

    /**
     * 配点-英語(リスニング)をセットします。
     *
     * @param allotListening 配点-英語(リスニング)
     */
    public void setAllotListening(String allotListening) {
        this.allotListening = allotListening;
    }

    /**
     * 配点-教科(数学)を返します。
     *
     * @return 配点-教科(数学)
     */
    public String getAllotMath() {
        return allotMath;
    }

    /**
     * 配点-教科(数学)をセットします。
     *
     * @param allotMath 配点-教科(数学)
     */
    public void setAllotMath(String allotMath) {
        this.allotMath = allotMath;
    }

    /**
     * 配点-教科(国語)を返します。
     *
     * @return 配点-教科(国語)
     */
    public String getAllotJap() {
        return allotJap;
    }

    /**
     * 配点-教科(国語)をセットします。
     *
     * @param allotJap 配点-教科(国語)
     */
    public void setAllotJap(String allotJap) {
        this.allotJap = allotJap;
    }

    /**
     * 配点-教科(国語記述)を返します。
     *
     * @return 配点-教科(国語記述)
     */
    public String getAllotJapDesc() {
        return allotJapDesc;
    }

    /**
     * 配点-教科(国語記述)をセットします。
     *
     * @param allotJapDesc 配点-教科(国語記述)
     */
    public void setAllotJapDesc(String allotJapDesc) {
        this.allotJapDesc = allotJapDesc;
    }

    /**
     * 配点-教科(理科)を返します。
     *
     * @return 配点-教科(理科)
     */
    public String getAllotSci() {
        return allotSci;
    }

    /**
     * 配点-教科(理科)をセットします。
     *
     * @param allotSci 配点-教科(理科)
     */
    public void setAllotSci(String allotSci) {
        this.allotSci = allotSci;
    }

    /**
     * 配点-教科(社会)を返します。
     *
     * @return 配点-教科(社会)
     */
    public String getAllotSoc() {
        return allotSoc;
    }

    /**
     * 配点-教科(社会)をセットします。
     *
     * @param allotSoc 配点-教科(社会)
     */
    public void setAllotSoc(String allotSoc) {
        this.allotSoc = allotSoc;
    }

    /**
     * 得点-英語(筆記＋リスニング)を返します。
     *
     * @return 得点-英語(筆記＋リスニング)
     */
    public String getScoreEnglish() {
        return scoreEnglish;
    }

    /**
     * 得点-英語(筆記＋リスニング)をセットします。
     *
     * @param scoreEnglish 得点-英語(筆記＋リスニング)
     */
    public void setScoreEnglish(String scoreEnglish) {
        this.scoreEnglish = scoreEnglish;
    }

    /**
     * 得点-数学1を返します。
     *
     * @return 得点-数学1
     */
    public String getScoreMath1() {
        return scoreMath1;
    }

    /**
     * 得点-数学1をセットします。
     *
     * @param scoreMath1 得点-数学1
     */
    public void setScoreMath1(String scoreMath1) {
        this.scoreMath1 = scoreMath1;
    }

    /**
     * 得点-数学2を返します。
     *
     * @return 得点-数学2
     */
    public String getScoreMath2() {
        return scoreMath2;
    }

    /**
     * 得点-数学2をセットします。
     *
     * @param scoreMath2 得点-数学2
     */
    public void setScoreMath2(String scoreMath2) {
        this.scoreMath2 = scoreMath2;
    }

    /**
     * 得点-国語を返します。
     *
     * @return 得点-国語
     */
    public String getScoreJap() {
        return scoreJap;
    }

    /**
     * 得点-国語をセットします。
     *
     * @param scoreJap 得点-国語
     */
    public void setScoreJap(String scoreJap) {
        this.scoreJap = scoreJap;
    }

    /**
     * 得点-国語記述を返します。
     *
     * @return 得点-国語記述
     */
    public String getScoreJapDesc() {
        return scoreJapDesc;
    }

    /**
     * 得点-国語記述をセットします。
     *
     * @param scoreJapDesc 得点-国語記述
     */
    public void setScoreJapDesc(String scoreJapDesc) {
        this.scoreJapDesc = scoreJapDesc;
    }

    /**
     * 得点-理科1を返します。
     *
     * @return 得点-理科1
     */
    public String getScoreSci1() {
        return scoreSci1;
    }

    /**
     * 得点-理科1をセットします。
     *
     * @param scoreSci1 得点-理科1
     */
    public void setScoreSci1(String scoreSci1) {
        this.scoreSci1 = scoreSci1;
    }

    /**
     * 得点-理科2を返します。
     *
     * @return 得点-理科2
     */
    public String getScoreSci2() {
        return scoreSci2;
    }

    /**
     * 得点-理科2をセットします。
     *
     * @param scoreSci2 得点-理科2
     */
    public void setScoreSci2(String scoreSci2) {
        this.scoreSci2 = scoreSci2;
    }

    /**
     * 得点-理科3を返します。
     *
     * @return 得点-理科3
     */
    public String getScoreSci3() {
        return scoreSci3;
    }

    /**
     * 得点-理科3をセットします。
     *
     * @param scoreSci3 得点-理科3
     */
    public void setScoreSci3(String scoreSci3) {
        this.scoreSci3 = scoreSci3;
    }

    /**
     * 得点-社会1を返します。
     *
     * @return 得点-社会1
     */
    public String getScoreSoc1() {
        return scoreSoc1;
    }

    /**
     * 得点-社会1をセットします。
     *
     * @param scoreSoc1 得点-社会1
     */
    public void setScoreSoc1(String scoreSoc1) {
        this.scoreSoc1 = scoreSoc1;
    }

    /**
     * 得点-社会2を返します。
     *
     * @return 得点-社会2
     */
    public String getScoreSoc2() {
        return scoreSoc2;
    }

    /**
     * 得点-社会2をセットします。
     *
     * @param scoreSoc2 得点-社会2
     */
    public void setScoreSoc2(String scoreSoc2) {
        this.scoreSoc2 = scoreSoc2;
    }

    /**
     * 備考を返します。
     *
     * @return 備考
     */
    public String getNote() {
        return note;
    }

    /**
     * 備考をセットします。
     *
     * @param note 備考
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * あと何点-判定を返します。
     *
     * @return あと何点-判定
     */
    public String getRemainRaiting() {
        return remainRaiting;
    }

    /**
     * あと何点-判定をセットします。
     *
     * @param remainRaiting あと何点-判定
     */
    public void setRemainRaiting(String remainRaiting) {
        this.remainRaiting = remainRaiting;
    }

    /**
     * あと何点-得点を返します。
     *
     * @return あと何点-得点
     */
    public String getRemainScore() {
        return remainScore;
    }

    /**
     * あと何点-得点をセットします。
     *
     * @param remainScore あと何点-得点
     */
    public void setRemainScore(String remainScore) {
        this.remainScore = remainScore;
    }

    /**
     * 判定科目を返します。
     *
     * @return 判定科目
     */
    public String getSubCount() {
        return subCount;
    }

    /**
     * 判定科目をセットします。
     *
     * @param subCount 判定科目
     */
    public void setSubCount(String subCount) {
        this.subCount = subCount;
    }

    /**
     * 配点-英語(筆記)を返します。
     *
     * @return 配点-英語(筆記)
     */
    public String getAllotWriting() {
        return allotWriting;
    }

    /**
     * 配点-英語(筆記)をセットします。
     *
     * @param allotWriting 配点-英語(筆記)
     */
    public void setAllotWriting(String allotWriting) {
        this.allotWriting = allotWriting;
    }

    /**
     * 第1段階選抜予想を返します。
     *
     * @return 第1段階選抜予想
     */
    public String getPickedLine() {
        return pickedLine;
    }

    /**
     * 第1段階選抜予想をセットします。
     *
     * @param pickedLine 第1段階選抜予想
     */
    public void setPickedLine(String pickedLine) {
        this.pickedLine = pickedLine;
    }

    /**
     * 本人得点(足切評価得点)を返します。
     *
     * @return 本人得点(足切評価得点)
     */
    public String getRejectScore() {
        return rejectScore;
    }

    /**
     * 本人得点(足切評価得点)をセットします。
     *
     * @param rejectScore 本人得点(足切評価得点)
     */
    public void setRejectScore(String rejectScore) {
        this.rejectScore = rejectScore;
    }

    /**
     * 採用枝番を返します。
     *
     * @return 採用枝番
     */
    public String getBranchCd() {
        return branchCd;
    }

    /**
     * 採用枝番をセットします。
     *
     * @param branchCd 採用枝番
     */
    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    /**
     * 注釈文を返します。
     *
     * @return 注釈文
     */
    public String getCommentStatement() {
        return commentStatement;
    }

    /**
     * 注釈文をセットします。
     *
     * @param commentStatement 注釈文
     */
    public void setCommentStatement(String commentStatement) {
        this.commentStatement = commentStatement;
    }

    /**
     * 英語資格要件を返します。
     *
     * @return 英語資格要件
     */
    public String getEnglishRequirements() {
        return englishRequirements;
    }

    /**
     * 英語資格要件を設定します。
     *
     * @param englishRequirements 英語資格要件
     */
    public void setEnglishRequirements(String englishRequirements) {
        this.englishRequirements = englishRequirements;
    }

    /**
     * 出願要件パターンコードを返します。
     *
     * @return 出願要件パターンコード
     */
    public String getAppReqPaternCd() {
        return appReqPaternCd;
    }

    /**
     * 出願要件パターンコードを設定します。
     *
     * @param appReqPaternCd 出願要件パターンコード
     */
    public void setAppReqPaternCd(String appReqPaternCd) {
        this.appReqPaternCd = appReqPaternCd;
    }

    /**
     * 配点-英語認定試験を返します。
     *
     * @return 配点-英語認定試験
     */
    public String getAllotEnglishJoining() {
        return allotEnglishJoining;
    }

    /**
     * 配点-英語認定試験を設定します。
     *
     * @param allotEnglishJoining 配点-英語認定試験
     */
    public void setAllotEnglishJoining(String allotEnglishJoining) {
        this.allotEnglishJoining = allotEnglishJoining;
    }

    /**
     * 得点-英語認定試験を返します。
     *
     * @return 得点-英語認定試験
     */
    public String getScoreEnglishJoining() {
        return scoreEnglishJoining;
    }

    /**
     * 得点-英語認定試験を設定します。
     *
     * @param scoreEnglishJoining 得点-英語認定試験
     */
    public void setScoreEnglishJoining(String scoreEnglishJoining) {
        this.scoreEnglishJoining = scoreEnglishJoining;
    }

    /**
     * ５桁コードを返す。
     *
     * @return 大学区分コード
     */
    public String getChoiceCd5() {
        return choiceCd5;
    }

    /**
     * ５桁コードをセットします。
     *
     * @param choiceCd5 大学区分コード
     */
    public void setChoiceCd5(String choiceCd5) {
        this.choiceCd5 = choiceCd5;

    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean;

import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

/**
 *
 * 個人成績・志望大学 画面表示用の出力Beanです。
 *
 *
 * @author TOTEC)TANAKA.Tomokazu
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivDetailSvcOutBean extends UnivKeyBean {

    /** 大学名-学部名-学科名 */
    private String univJoinName;

    /** 大学名 */
    private String univName;

    /** 学部名 */
    private String facultyName;

    /** 学科名 */
    private String deptName;

    /** 日程コード */
    private String scheduleCd;

    /** 募集定員 */
    private String offerCapacity;

    /** 募集定員区分 */
    private String offerCapacityKbn;

    /** 出願締切 */
    private String entExamApplideadlineDiv;

    /** 入試 */
    private String examDay;

    /** 合格発表 */
    private String sucannDate;

    /** 手続締切 */
    private String proceDeadline;

    /** 本部所在地 */
    private String prefBase;

    /** キャンパス所在地 */
    private String prefCampus;

    /** 総合：評価 */
    private String totalRating;

    /** 総合：評価ポイント  */
    private String totalRatingPoint;

    /** 共通テスト（含まない）詳細 */
    private ExmntnUnivDetailCenterSvcOutBean center;

    /** 共通テスト（含む）詳細 */
    private ExmntnUnivDetailCenterSvcOutBean centerInclude;

    /** 二次詳細 */
    private ExmntnUnivDetailSecondSvcOutBean second;

    /** 大学区分コード */
    private String univDivCd;

    /**
     * コンストラクタです。
     *
     * @param key {@link UnivKeyBean}
     */
    public ExmntnUnivDetailSvcOutBean(UnivKeyBean key) {
        super(key);
    }

    /**
     * 大学名-学部名-学科名を返します。
     *
     * @return 大学名-学部名-学科名
     */
    public String getUnivJoinName() {
        return univJoinName;
    }

    /**
     * 大学名-学部名-学科名をセットします。
     *
     * @param univJoinName 大学名-学部名-学科名
     */
    public void setUnivJoinName(String univJoinName) {
        this.univJoinName = univJoinName;
    }

    /**
     * 大学名を返します。
     *
     * @return 大学名
     */
    public String getUnivName() {
        return univName;
    }

    /**
     * 大学名をセットします。
     *
     * @param univName 大学名
     */
    public void setUnivName(String univName) {
        this.univName = univName;
    }

    /**
     * 学部名を返します。
     *
     * @return 学部名
     */
    public String getFacultyName() {
        return facultyName;
    }

    /**
     * 学部名をセットします。
     *
     * @param facultyName 学部名
     */
    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    /**
     * 学科名を返します。
     *
     * @return 学科名
     */
    public String getDeptName() {
        return deptName;
    }

    /**
     * 学科名をセットします。
     *
     * @param deptName 学科名
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 募集定員を返します。
     *
     * @return 募集定員
     */
    public String getOfferCapacity() {
        return offerCapacity;
    }

    /**
     * 募集定員をセットします。
     *
     * @param offerCapacity 募集定員
     */
    public void setOfferCapacity(String offerCapacity) {
        this.offerCapacity = offerCapacity;
    }

    /**
     * 募集定員区分を返します。
     *
     * @return 募集定員区分
     */
    public String getOfferCapacityKbn() {
        return offerCapacityKbn;
    }

    /**
     * 募集定員区分をセットします。
     *
     * @param offerCapacityKbn 募集定員区分
     */
    public void setOfferCapacityKbn(String offerCapacityKbn) {
        this.offerCapacityKbn = offerCapacityKbn;
    }

    /**
     * 出願締切を返します。
     *
     * @return 出願締切
     */
    public String getEntExamApplideadlineDiv() {
        return entExamApplideadlineDiv;
    }

    /**
     * 出願締切をセットします。
     *
     * @param entExamApplideadlineDiv 出願締切
     */
    public void setEntExamApplideadlineDiv(String entExamApplideadlineDiv) {
        this.entExamApplideadlineDiv = entExamApplideadlineDiv;
    }

    /**
     * 入試を返します。
     *
     * @return 入試
     */
    public String getExamDay() {
        return examDay;
    }

    /**
     * 入試をセットします。
     *
     * @param examDay 入試
     */
    public void setExamDay(String examDay) {
        this.examDay = examDay;
    }

    /**
     * 合格発表を返します。
     *
     * @return 合格発表
     */
    public String getSucannDate() {
        return sucannDate;
    }

    /**
     * 合格発表をセットします。
     *
     * @param sucannDate 合格発表
     */
    public void setSucannDate(String sucannDate) {
        this.sucannDate = sucannDate;
    }

    /**
     * 手続締切を返します。
     *
     * @return 手続締切
     */
    public String getProceDeadline() {
        return proceDeadline;
    }

    /**
     * 手続締切をセットします。
     *
     * @param proceDeadline 手続締切
     */
    public void setProceDeadline(String proceDeadline) {
        this.proceDeadline = proceDeadline;
    }

    /**
     * 本部所在地を返します。
     *
     * @return 本部所在地
     */
    public String getPrefBase() {
        return prefBase;
    }

    /**
     * 本部所在地をセットします。
     *
     * @param prefBase 本部所在地
     */
    public void setPrefBase(String prefBase) {
        this.prefBase = prefBase;
    }

    /**
     * キャンパス所在地を返します。
     *
     * @return キャンパス所在地
     */
    public String getPrefCampus() {
        return prefCampus;
    }

    /**
     * キャンパス所在地をセットします。
     *
     * @param prefCampus キャンパス所在地
     */
    public void setPrefCampus(String prefCampus) {
        this.prefCampus = prefCampus;
    }

    /**
     * 総合：評価を返します。
     *
     * @return 総合：評価
     */
    public String getTotalRating() {
        return totalRating;
    }

    /**
     * 総合：評価をセットします。
     *
     * @param totalRating 総合：評価
     */
    public void setTotalRating(String totalRating) {
        this.totalRating = totalRating;
    }

    /**
     * 総合：評価ポイントを返します。
     *
     * @return 総合：評価ポイント
     */
    public String getTotalRatingPoint() {
        return totalRatingPoint;
    }

    /**
     * 総合：評価ポイントをセットします。
     *
     * @param totalRatingPoint 総合：評価ポイント
     */
    public void setTotalRatingPoint(String totalRatingPoint) {
        this.totalRatingPoint = totalRatingPoint;
    }

    /**
     * 共通テスト（含まない）詳細を返します。
     *
     * @return 共通テスト（含まない）詳細
     */
    public ExmntnUnivDetailCenterSvcOutBean getCenter() {
        return center;
    }

    /**
     * 共通テスト（含まない）詳細をセットします。
     *
     * @param center 共通テスト（含まない）詳細
     */
    public void setCenter(ExmntnUnivDetailCenterSvcOutBean center) {
        this.center = center;
    }

    /**
     * 共通テスト（含む）詳細を返します。
     *
     * @return 共通テスト（含む）詳細
     */
    public ExmntnUnivDetailCenterSvcOutBean getCenterInclude() {
        return centerInclude;
    }

    /**
     * 共通テスト（含む）詳細をセットします。
     *
     * @param centerInclude 共通テスト（含む）詳細
     */
    public void setCenterInclude(ExmntnUnivDetailCenterSvcOutBean centerInclude) {
        this.centerInclude = centerInclude;
    }

    /**
     * 二次詳細を返します。
     *
     * @return 二次詳細
     */
    public ExmntnUnivDetailSecondSvcOutBean getSecond() {
        return second;
    }

    /**
     * 二次詳細をセットします。
     *
     * @param second 二次詳細
     */
    public void setSecond(ExmntnUnivDetailSecondSvcOutBean second) {
        this.second = second;
    }

    /**
     * 日程コードを返します。
     *
     * @return 日程コード
     */
    public String getScheduleCd() {
        return scheduleCd;
    }

    /**
     * 日程コードをセットします。
     *
     * @param scheduleCd 日程コード
     */
    public void setScheduleCd(String scheduleCd) {
        this.scheduleCd = scheduleCd;
    }

    /**
     * 大学区分コードを返す。
     *
     * @return 大学区分コード
     */
    public String getUnivDivCd() {
        return univDivCd;
    }

    /**
     * 大学区分コードをセットします。
     *
     * @param univDivCd 大学区分コード
     */
    public void setUnivDivCd(String univDivCd) {
        this.univDivCd = univDivCd;

    }
}

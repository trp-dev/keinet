package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCsvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.maker.SelstdMainB0201Maker;

/**
 *
 * 志望大学のノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class B0201Node extends OutputCsvNode {

    /**
     * コンストラクタです。
     */
    B0201Node() {
        super("個人志望大学");
    }

    @Override
    void output(OutputMainHelper helper) {
        new ExcelLauncher(GamenId.D).addCreator(SelstdMainB0201Maker.class, new SelstdMainCsvInBean(helper.findTargetExam(), helper.findPrintTargetList())).launch();
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dprt.util;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.util.ExamUtil;

/**
 *
 * A01帳票用のユーティリティクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class SelstdDprtExcelUtil {

    /** 理科・社会の教科名定義（マーク） */
    private static final String[] COURSE_LABEL_MARK = { "理第1", "理2/基1", "理基2", "地公第1", "地公第2" };

    /** 理科・社会の教科名定義（＃１〜＃３記述） */
    private static final String[] COURSE_LABEL_WRTN = { "理1", "理2/基1", "基2", "地公1", "地公2" };

    /** 理科・社会の教科名定義（高2記述模試） */
    private static final String[] COURSE_LABEL_2NDWRTN = { "理1", "理2/基1", "基2", "地公1", "地公2" };

    /**
     * コンストラクタです。
     */
    private SelstdDprtExcelUtil() {
    }

    /**
     * 対象模試の理科・社会の教科名定義を返します。
     *
     * @param exam 対象模試
     * @return 教科名定義
     */
    public static String[] getCourseLabel(ExamBean exam) {
        if (ExamUtil.isMark(exam)) {
            return COURSE_LABEL_MARK;
        } else if (ExamUtil.is2ndWrtn(exam)) {
            return COURSE_LABEL_2NDWRTN;
        } else {
            return COURSE_LABEL_WRTN;
        }
    }

}

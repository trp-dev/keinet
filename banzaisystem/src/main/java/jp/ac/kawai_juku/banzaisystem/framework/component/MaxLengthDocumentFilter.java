package jp.ac.kawai_juku.banzaisystem.framework.component;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.StyleConstants;

/**
 *
 * 最大文字数以上の入力を制限するドキュメントフィルタです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MaxLengthDocumentFilter extends DocumentFilter {

    /** 最大文字数 */
    private final int maxLength;

    /**
     * コンストラクタです。
     *
     * @param maxLength 最大文字数
     */
    public MaxLengthDocumentFilter(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr) throws BadLocationException {

        if (attr != null && attr.isDefined(StyleConstants.ComposedTextAttribute)) {
            /* IME変換中の場合はチェックしない */
            super.insertString(fb, offset, str, attr);
        } else {
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
            sb.insert(offset, str);
            String afterStr = sb.toString();
            if (afterStr.length() <= maxLength) {
                super.insertString(fb, offset, str, attr);
            }
        }
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attr)
            throws BadLocationException {

        if (length == 0) {
            insertString(fb, offset, text, attr);
        } else if (attr != null && attr.isDefined(StyleConstants.ComposedTextAttribute)) {
            /* IME変換中の場合はチェックしない */
            super.replace(fb, offset, length, text, attr);
        } else {
            Document doc = fb.getDocument();
            StringBuilder sb = new StringBuilder(doc.getText(0, doc.getLength()));
            sb.delete(offset, offset + length);
            if (text != null) {
                sb.insert(offset, text);
            }
            String afterStr = sb.toString();
            if (afterStr.length() <= maxLength) {
                super.replace(fb, offset, length, text, attr);
            }
        }
    }

}

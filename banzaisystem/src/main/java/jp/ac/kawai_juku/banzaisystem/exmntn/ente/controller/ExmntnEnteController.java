package jp.ac.kawai_juku.banzaisystem.exmntn.ente.controller;

import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH1A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_MATH2B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_SOCIAL;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYA;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_CENTER_DEFAULT_WHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_EARTH_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_ETHICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_GEOGRAPHYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP2;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JAP3;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_JHISTORYB;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH1A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2A;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH2B;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_MATH3;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_POLITICS;
import static jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score.SUBCODE_SECOND_DEFAULT_WHISTORYB;

import javax.annotation.Resource;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.EvaluationCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.SecondBasicCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.component.SecondCheckBox;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.helper.ExmntnEnteHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.service.ExmntnEnteService;
import jp.ac.kawai_juku.banzaisystem.exmntn.ente.view.ExmntnEnteView;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.framework.component.CheckBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.annotation.Logging;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.judgement.BZSubjectRecord;
import jp.ac.kawai_juku.banzaisystem.selstd.main.view.SelstdMainView;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * 大学検索（入試情報（評価・日程・科目））画面のコントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnEnteController extends SubController {

    /** View */
    @Resource(name = "exmntnEnteView")
    private ExmntnEnteView view;

    /** 個人成績画面のView */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView mainView;

    /** Helper */
    @Resource(name = "exmntnEnteHelper")
    private ExmntnEnteHelper helper;

    /** 個人成績表画面のHelper */
    @Resource
    private ExmntnMainScoreHelper exmntnMainScoreHelper;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 大学検索サービス */
    @Resource
    private ExmntnEnteService exmntnEnteService;

    /** 対象生徒選択画面のView */
    @Resource
    private SelstdMainView selstdMainView;

    @Override
    @Logging(GamenId.C3_3)
    public void activatedAction() {
    }

    /**
     * 画面選択中の個人IDを取得する。
     *
     * @return 個人ID
     */
    public Integer getKojinId() {
        return systemDto.getTargetIndividualId();
    }

    /**
     * 評価範囲クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearValuationRangeButtonAction() {
        clearCheckBox(ExmntnEnteView.GROUP_RANGE);
        return null;
    }

    /**
     * 入試日程・方式クリアボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult clearExaminationScheduleButtonAction() {
        clearCheckBox(ExmntnEnteView.GROUP_SCHEDULE);
        for (JComponent jc : view.findComponentsByGroup(ExmntnEnteView.GROUP_SCHEDULE)) {
            if (jc instanceof JComboBox<?>) {
                JComboBox<?> combo = (JComboBox<?>) jc;
                if (combo.getItemCount() > 0) {
                    combo.setSelectedIndex(0);
                }
            }
        }
        return null;
    }

    /**
     * 初期値に戻すボタン押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult returnInitialValueButtonAction() {

        clearCheckBox(ExmntnEnteView.GROUP_SUBJECT);

        initSecondComboBox();

        if (systemDto.isTeacher()) {
            initCenterCheckBox();
            initSecondCheckBox();
        }

        notImposeCheckBoxAction();

        return null;
    }

    /**
     * 指定したグループのチェックボックスをクリアします。
     *
     * @param group グループ番号
     */
    private void clearCheckBox(int group) {
        for (JComponent jc : view.findComponentsByGroup(group)) {
            if (jc instanceof CheckBox) {
                ((CheckBox) jc).setSelected(false);
            }
        }
    }

    /**
     * 入試科目：コンボボックス初期値を設定します。
     *
     */
    private void initSecondComboBox() {
        view.indvMathComboBox.setSelectedIndex(view.indvMathComboBox.getItemCount() - 1);
        view.indvJpnComboBox.setSelectedIndex(view.indvJpnComboBox.getItemCount() - 1);
    }

    /**
     * 評価範囲チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult scopeOfAssessmentCheckBoxAction() {
        setEnabled(ExmntnEnteView.GROUP_RANGE, view.scopeOfAssessmentCheckBox.isSelected());
        return null;
    }

    /**
     * 入試日程・方式チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exmntnScheduleCheckBoxAction() {
        helper.renewScheduleStatus();
        return null;
    }

    /**
     * 評価範囲：入試試験区分チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult evalRangeCheckBoxActionAction() {
        boolean b = view.centerCheckBox.isSelected() || view.secondCheckBox.isSelected() || view.syntheticCheckBox.isSelected();
        for (EvaluationCheckBox c : view.findComponentsByClass(EvaluationCheckBox.class)) {
            c.setEnabled(b);
        }
        return null;
    }

    /**
     * 入試科目チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult exmntnSubjectsCheckBoxAction() {
        setEnabled(ExmntnEnteView.GROUP_SUBJECT, view.exmntnSubjectsCheckBox.isSelected());
        if (view.exmntnSubjectsCheckBox.isSelected()) {
            notImposeCheckBoxAction();
        }
        return null;
    }

    /**
     * 課さないチェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult notImposeCheckBoxAction() {
        setEnabled(ExmntnEnteView.GROUP_SECOND_SUBJECT, view.exmntnSubjectsCheckBox.isSelected() && !view.notImposeCheckBox.isSelected());
        return null;
    }

    /**
     * 二次物理チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvPhysicsCheckBoxAction() {
        secondScienceCheckBoxAction(view.indvPhysicsCheckBox, view.indvPhysicsBasicCheckBox);
        return null;
    }

    /**
     * 二次化学チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvChemicalCheckBoxAction() {
        secondScienceCheckBoxAction(view.indvChemicalCheckBox, view.indvChemicalBasicCheckBox);
        return null;
    }

    /**
     * 二次生物チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvBiologyCheckBoxAction() {
        secondScienceCheckBoxAction(view.indvBiologyCheckBox, view.indvBiologyBasicCheckBox);
        return null;
    }

    /**
     * 二次地学チェックボックス押下アクションです。
     *
     * @return なし
     */
    @Execute
    public ExecuteResult indvEarthScienceCheckBoxAction() {
        secondScienceCheckBoxAction(view.indvEarthScienceCheckBox, view.indvEarthScienceBasicCheckBox);
        return null;
    }

    /**
     * 二次理科チェックボックス押下アクションです。
     *
     * @param specialCheckBox 専門科目チェックボックス
     * @param basicCheckBox 基礎科目チェックボックス
     */
    private void secondScienceCheckBoxAction(SecondCheckBox specialCheckBox, SecondBasicCheckBox basicCheckBox) {
        if (specialCheckBox.isSelected()) {
            basicCheckBox.setEnabled(true);
        } else {
            basicCheckBox.setEnabled(false);
            basicCheckBox.setSelected(false);
        }
    }

    /**
     * 指定したグループの有効フラグをセットします。
     *
     * @param group グループ番号
     * @param enabled 有効フラグ
     */
    private void setEnabled(int group, boolean enabled) {
        for (JComponent jc : view.findComponentsByGroup(group)) {
            /* if (group == 2 && jc.getX() == 257 && jc.getY() == 319) {
            if (jc.equals(view.japaneseWCheckBox)) {
                continue;
            } */
            jc.setEnabled(enabled);

        }
    }

    /**
     * センター試験：チェックボックス初期設定
     *
     */
    private void initCenterCheckBox() {

        /* マーク模試選択 */
        if (StringUtils.isNotEmpty(mainView.exmntnMarkComboBox.getSelectedLabel())) {

            /* 英語 */
            if (StringUtils.isNotEmpty(mainView.english1Field.getText())) {
                view.englishCheckBox.setSelectedSilent(true);
            }

            /* 英語リスニング */
            if (StringUtils.isNotEmpty(mainView.listeningField.getText())) {
                view.englishListeningCheckBox.setSelectedSilent(true);
            }

            /* CEFR */
            /*
            ExamBean exam = selstdMainView.examComboBox.getSelectedValue();
            ExmntnEnteCEFRIniBean cefr = exmntnEnteService.iniCefr(getKojinId(), systemDto.getUnivYear(), exmntnMainScoreHelper.getMarkExam().getExamCd());
            if (cefr == null) {
                view.cefrCheckBox.setSelectedSilent(false);
            } else {
                if (cefr.getExamCd1() == null && cefr.getExamCd2() == null) {
                    view.cefrCheckBox.setSelectedSilent(false);
                } else {
                    view.cefrCheckBox.setSelectedSilent(true);
                }
            }
            */

            /* 数学�T・�TA */
            BZSubjectRecord math1Bean = mainView.math1ComboBox.getSelectedValue();
            if (math1Bean != null && math1Bean.getSubCd() != null) {
                String subCd = math1Bean.getSubCd();
                if (subCd.equals(SUBCODE_CENTER_DEFAULT_MATH1)) {
                    view.math1CheckBox.setSelectedSilent(true);
                } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_MATH1A)) {
                    view.math1ACheckBox.setSelectedSilent(true);
                }
            }

            /* 数学�U・�UB */
            BZSubjectRecord math2Bean = mainView.math2ComboBox.getSelectedValue();
            if (math2Bean != null && math2Bean.getSubCd() != null) {
                String subCd = math2Bean.getSubCd();
                if (subCd.equals(SUBCODE_CENTER_DEFAULT_MATH2)) {
                    view.math2CheckBox.setSelectedSilent(true);
                } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_MATH2B)) {
                    view.math2BCheckBox.setSelectedSilent(true);
                }
            }

            /* 現代文 */
            if (StringUtils.isNotEmpty(mainView.contemporaryField.getText())) {
                view.japaneseCheckBox.setSelectedSilent(true);
            }

            /* 現代ｂ記 */
            /*
            ExmntnEnteJpnIniBean jpnB = exmntnEnteService.iniJpnWrite(getKojinId(), systemDto.getUnivYear(), exmntnMainScoreHelper.getMarkExam().getExamCd());
            if (jpnB == null) {
                view.japaneseWCheckBox.setSelectedSilent(false);
            } else {
                if (jpnB.gettotal() == null) {
                    view.japaneseWCheckBox.setSelectedSilent(false);
                } else {
                    view.japaneseWCheckBox.setSelectedSilent(true);
                }
            }
            */
            /*view.japaneseWCheckBox.setSelectedSilent(false);*/

            /* 古文 */
            if (StringUtils.isNotEmpty(mainView.classicsField.getText())) {
                view.classicCheckBox.setSelectedSilent(true);
            }

            /* 漢文 */
            if (StringUtils.isNotEmpty(mainView.chineseField.getText())) {
                view.chieneseCheckBox.setSelectedSilent(true);
            }

            /* 理科専門（上段） */
            setCenterScienceCheckBox(mainView.science1ComboBox.getSelectedValue());

            /* 理科専門（下段） */
            setCenterScienceCheckBox(mainView.science2ComboBox.getSelectedValue());

            /* 理科基礎（上段） */
            setCenterScienceBasicCheckBox(mainView.science5ComboBox.getSelectedValue());

            /* 理科基礎（下段） */
            setCenterScienceBasicCheckBox(mainView.science6ComboBox.getSelectedValue());

            /* 社会（上段） */
            setCenterSocietyCheckBox(mainView.society1ComboBox.getSelectedValue());

            /* 社会（下段） */
            setCenterSocietyCheckBox(mainView.society2ComboBox.getSelectedValue());
        }
    }

    /**
     * センター試験：理科専門チェックボックス設定
     *
     * @param sciBean 選択科目情報
     */
    private void setCenterScienceCheckBox(BZSubjectRecord sciBean) {

        if (sciBean != null && sciBean.getSubCd() != null) {

            String subCd = sciBean.getSubCd();

            if (subCd.equals(SUBCODE_CENTER_DEFAULT_PHYSICS)) {
                view.physicsCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_CHEMISTRY)) {
                view.chemicalCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_BIOLOGY)) {
                view.biologyCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_EARTH)) {
                view.earthScienceCheckBox.setSelectedSilent(true);
            }
        }
    }

    /**
     * センター試験：理科基礎チェックボックス設定
     *
     * @param sciBean 選択科目情報
     */
    private void setCenterScienceBasicCheckBox(BZSubjectRecord sciBean) {

        if (sciBean != null && sciBean.getSubCd() != null) {

            String subCd = sciBean.getSubCd();

            if (subCd.equals(SUBCODE_CENTER_DEFAULT_PHYSICS_BASIC)) {
                view.physicsBasicCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_CHEMISTRY_BASIC)) {
                view.chemicalBasicCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_BIOLOGY_BASIC)) {
                view.biologyBasicCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_EARTH_BASIC)) {
                view.earthScienceBasicCheckBox.setSelectedSilent(true);
            }
        }
    }

    /**
     * センター試験：社会チェックボックス設定
     *
     * @param socBean 選択科目情報
     */
    private void setCenterSocietyCheckBox(BZSubjectRecord socBean) {

        if (socBean != null && socBean.getSubCd() != null) {

            String subCd = socBean.getSubCd();

            if (subCd.equals(SUBCODE_CENTER_DEFAULT_JHISTORYA)) {
                view.japaneseHistoryACheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_WHISTORYA)) {
                view.worldHistoryACheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_GEOGRAPHYA)) {
                view.geographyACheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_JHISTORYB)) {
                view.japaneseHistoryBCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_WHISTORYB)) {
                view.worldHistoryBCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_GEOGRAPHYB)) {
                view.geographyBCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_SOCIAL)) {
                view.modernSocietyCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_POLITICS)) {
                view.politicsAndDEcnomicsCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_ETHICS)) {
                view.ethicalCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_CENTER_DEFAULT_ETHICALPOLITICS)) {
                view.ethicalAndPoliticsAndDEcnomicsCheckBox.setSelectedSilent(true);
            }
        }
    }

    /**
     * 個別試験：チェックボックス初期設定
     *
     */
    private void initSecondCheckBox() {

        /* 記述模試選択 */
        if (StringUtils.isNotEmpty(mainView.exmntnWritingComboBox.getSelectedLabel())) {

            /* 英語 */
            if (StringUtils.isNotEmpty(mainView.english2Field.getText())) {

                /* リスニング */
                if (mainView.listeningCheckBox.isSelected()) {
                    view.indvEnglishListeningCheckBox.setSelectedSilent(true);
                }

                view.indvEnglishCheckBox.setSelectedSilent(true);

            }

            /* 数学 */
            BZSubjectRecord math3Bean = mainView.math3ComboBox.getSelectedValue();
            if (math3Bean != null && math3Bean.getSubCd() != null) {

                String subCd = math3Bean.getSubCd();

                if (subCd.equals(SUBCODE_SECOND_DEFAULT_MATH1)) {
                    view.indvMathComboBox.setSelectedIndex(0);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_MATH1A)) {
                    view.indvMathComboBox.setSelectedIndex(1);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_MATH2A)) {
                    view.indvMathComboBox.setSelectedIndex(2);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_MATH2B)) {
                    view.indvMathComboBox.setSelectedIndex(3);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_MATH3)) {
                    view.indvMathComboBox.setSelectedIndex(4);
                }

                if (StringUtils.isNotEmpty(subCd)) {
                    view.indvMathCheckBox.setSelectedSilent(true);
                }

            } else {
                view.indvMathComboBox.setSelectedIndex(view.indvMathComboBox.getItemCount() - 1);
            }

            /* 国語 */
            BZSubjectRecord japBean = mainView.japaneseComboBox.getSelectedValue();
            if (japBean != null && japBean.getSubCd() != null) {

                String subCd = japBean.getSubCd();

                if (subCd.equals(SUBCODE_SECOND_DEFAULT_JAP1)) {
                    view.indvJpnComboBox.setSelectedIndex(0);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_JAP2)) {
                    view.indvJpnComboBox.setSelectedIndex(1);
                } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_JAP3)) {
                    view.indvJpnComboBox.setSelectedIndex(2);
                }

                if (StringUtils.isNotEmpty(subCd)) {
                    view.indvJapaneseCheckBox.setSelectedSilent(true);
                }

            } else {
                view.indvJpnComboBox.setSelectedIndex(view.indvJpnComboBox.getItemCount() - 1);
            }

            /* 理科専門（上段） */
            setSecondScienceCheckBox(mainView.science3ComboBox.getSelectedValue());

            /* 理科専門（下段） */
            setSecondScienceCheckBox(mainView.science4ComboBox.getSelectedValue());

            /* 理科基礎（上段） */
            setSecondScienceCheckBox(mainView.science7ComboBox.getSelectedValue());

            /* 理科基礎（下段） */
            setSecondScienceCheckBox(mainView.science8ComboBox.getSelectedValue());

            /* 社会（上段） */
            setSecondSocietyCheckBox(mainView.society3ComboBox.getSelectedValue());

            /* 社会（下段） */
            setSecondSocietyCheckBox(mainView.society4ComboBox.getSelectedValue());

        }
    }

    /**
     * 個別試験：理科チェックボックス設定
     *
     * @param sciBean 選択科目情報
     */
    private void setSecondScienceCheckBox(BZSubjectRecord sciBean) {

        if (sciBean != null && sciBean.getSubCd() != null) {

            String subCd = sciBean.getSubCd();

            /* #１〜＃３記述 */
            if (subCd.equals(SUBCODE_SECOND_DEFAULT_PHYSICS)) {
                /* 物理 */
                view.indvPhysicsCheckBox.setSelectedSilent(true);
                view.indvPhysicsBasicCheckBox.setSelectedSilent(false);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_CHEMISTRY)) {
                /* 化学 */
                view.indvChemicalCheckBox.setSelectedSilent(true);
                view.indvChemicalBasicCheckBox.setSelectedSilent(false);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_BIOLOGY)) {
                /* 生物 */
                view.indvBiologyCheckBox.setSelectedSilent(true);
                view.indvBiologyBasicCheckBox.setSelectedSilent(false);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_EARTH)) {
                /* 地学 */
                view.indvEarthScienceCheckBox.setSelectedSilent(true);
                view.indvEarthScienceBasicCheckBox.setSelectedSilent(false);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_PHYSICS_BASIC)) {
                /* 物理基礎 */
                if (!view.indvPhysicsCheckBox.isSelected()) {
                    view.indvPhysicsCheckBox.setSelectedSilent(true);
                    view.indvPhysicsBasicCheckBox.setSelectedSilent(true);
                }
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_CHEMISTRY_BASIC)) {
                /* 化学基礎 */
                if (!view.indvChemicalCheckBox.isSelected()) {
                    view.indvChemicalCheckBox.setSelectedSilent(true);
                    view.indvChemicalBasicCheckBox.setSelectedSilent(true);
                }
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_BIOLOGY_BASIC)) {
                /* 生物基礎 */
                if (!view.indvBiologyCheckBox.isSelected()) {
                    view.indvBiologyCheckBox.setSelectedSilent(true);
                    view.indvBiologyBasicCheckBox.setSelectedSilent(true);
                }
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_EARTH_BASIC)) {
                /* 地学基礎 */
                if (!view.indvEarthScienceCheckBox.isSelected()) {
                    view.indvEarthScienceCheckBox.setSelectedSilent(true);
                    view.indvEarthScienceBasicCheckBox.setSelectedSilent(true);
                }
            }

        }
    }

    /**
     * 個別試験：社会チェックボックス設定
     *
     * @param socBean 選択科目情報
     */
    private void setSecondSocietyCheckBox(BZSubjectRecord socBean) {

        if (socBean != null && socBean.getSubCd() != null) {

            String subCd = socBean.getSubCd();

            if (subCd.equals(SUBCODE_SECOND_DEFAULT_WHISTORYB)) {
                /* 世界史 */
                view.indvWorldHistoryCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_JHISTORYB)) {
                /* 日本史 */
                view.indvJapaneseHistoryCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_GEOGRAPHYB)) {
                /* 地理 */
                view.indvGeographyCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_ETHICS)) {
                /* 倫理 */
                view.indvEthicalCheckBox.setSelectedSilent(true);
            } else if (subCd.equals(SUBCODE_SECOND_DEFAULT_POLITICS)) {
                /* 政経 */
                view.indvPoliticsAndDEcnomicsCheckBox.setSelectedSilent(true);
            }
        }
    }

}

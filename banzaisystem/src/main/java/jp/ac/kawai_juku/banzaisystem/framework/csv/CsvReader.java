package jp.ac.kawai_juku.banzaisystem.framework.csv;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.seasar.framework.exception.IORuntimeException;

/**
 *
 * CSVのリーダークラスです。<br>
 * ★注意★処理の最後に {@link #close()} メソッドを必ず呼び出してください。<br>
 * CSVの解析処理（アルゴリズム）は、以下のライブラリの公開仕様に従っています。<br>
 * {@link http:&#47;&#47;csvparser.sourceforge.jp/}
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CsvReader implements Closeable {

    /** デフォルトのキャラクタセット */
    private static final String DEFAULT_CHARSET_NAME = "Windows-31J";

    /** 区切り文字はカンマで固定 */
    private static final char DELIMITER = ',';

    /** ダブルクオーテーション */
    private static final char QUOTE = '"';

    /** 改行コード */
    private static final char LF = '\n';

    /** 入力ソースのReader */
    private final InnerReader in;

    /** 行番号 */
    private int rowNum;

    /**
     * 処理状態を表すenumです。
     * executeメソッドで状態別の処理を実行します。
     */
    private enum State {

        /** 初期状態です */
        NEW {

            /**
             * {@inheritDoc}
             */
            @Override
            State execute(char c, CsvRowCreator creator) {
                if (c == DELIMITER) {
                    creator.addColumn();
                    return DELIMITED;
                } else if (c == QUOTE) {
                    return QUOTED;
                } else if (c == LF) {
                    /* 空のリストを返すためにaddColumnは呼ばない */
                    return NEW;
                } else {
                    creator.add(c);
                    return VALUE;
                }
            }
        },

        /** 直前が値の場合の処理です */
        VALUE {

            /**
             * {@inheritDoc}
             */
            @Override
            State execute(char c, CsvRowCreator creator) {
                if (c == DELIMITER) {
                    creator.addColumn();
                    return DELIMITED;
                } else if (c == QUOTE) {
                    creator.add(c);
                    return VALUE;
                } else if (c == LF) {
                    creator.addColumn();
                    return NEW;
                } else {
                    creator.add(c);
                    return VALUE;
                }
            }
        },

        /** 直前が分割文字の場合の処理です */
        DELIMITED {

            /**
             * {@inheritDoc}
             */
            @Override
            State execute(char c, CsvRowCreator creator) {
                if (c == DELIMITER) {
                    creator.addColumn();
                    return DELIMITED;
                } else if (c == QUOTE) {
                    return QUOTED;
                } else if (c == LF) {
                    creator.addColumn();
                    return NEW;
                } else {
                    creator.add(c);
                    return VALUE;
                }
            }
        },

        /** ダブルクオートで囲まれている場合の処理です */
        QUOTED {

            /**
             * {@inheritDoc}
             */
            @Override
            State execute(char c, CsvRowCreator creator) {
                if (c == DELIMITER) {
                    creator.add(c);
                    return QUOTED;
                } else if (c == QUOTE) {
                    return ESCAPED;
                } else if (c == LF) {
                    creator.add(c);
                    return QUOTED;
                } else {
                    creator.add(c);
                    return QUOTED;
                }
            }
        },

        /** ダブルクオートが連続した場合のエスケープ処理です */
        ESCAPED {

            /**
             * {@inheritDoc}
             */
            @Override
            State execute(char c, CsvRowCreator creator) {
                if (c == DELIMITER) {
                    creator.addColumn();
                    return DELIMITED;
                } else if (c == QUOTE) {
                    creator.add(c);
                    return QUOTED;
                } else if (c == LF) {
                    creator.addColumn();
                    return NEW;
                } else {
                    creator.add(c);
                    return VALUE;
                }
            }
        };

        /**
         * CSVの1文字を処理します。
         * @param c 処理する1文字
         * @param creator CSVの論理的な1行分を生成するクラス
         * @return 新しい状態
         */
        abstract State execute(char c, CsvRowCreator creator);
    }

    /**
     * コンストラクタです。 <br>
     * 入力ソースのキャラクタセットは「Windows-31J」として扱います。
     *
     * @param is CSVデータの入力ソース
     */
    public CsvReader(InputStream is) {
        this(is, DEFAULT_CHARSET_NAME);
    }

    /**
     * コンストラクタです。
     *
     * @param is CSVデータの入力ソース
     * @param charsetName 入力ソースのキャラクタセット
     */
    public CsvReader(InputStream is, String charsetName) {
        in = new InnerReader(is, charsetName);
    }

    /**
     * ReaderからCSVの1行を読み込みます。<br>
     * 改行のみの行はサイズ0のリストを返します。
     *
     * @return CSV行オブジェクト
     * @throws IOException 入力ソースからの読み出しに失敗した場合
     */
    public CsvRow readLine() throws IOException {

        /* 状態 */
        State state = State.NEW;

        /* 行オブジェクトのクリエイター */
        CsvRowCreator creator = null;

        int c;
        while ((c = in.read()) > -1) {

            /* ループ処理の初回でクリエイター初期化 */
            if (creator == null) {
                creator = new CsvRowCreator();
            }

            /* 状態別処理 */
            state = state.execute((char) c, creator);

            /* NEWに戻ったら抜ける */
            if (state == State.NEW) {
                break;
            }
        }

        /* ループに入っていなければ終端 */
        if (creator == null) {
            return null;
        }

        /* NEWに戻る前に入力ソース終端に達した場合はバッファの残りを詰める */
        if (state != State.NEW) {
            creator.addColumn();
        }

        return creator.create(rowNum);
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    /** 内部処理用のReader */
    private final class InnerReader implements Closeable {

        /** BufferedReader */
        private final BufferedReader reader;

        /** 行 */
        private String line;

        /** 読み取り位置 */
        private int pos;

        /**
         * コンストラクタです。
         *
         * @param is CSVデータの入力ソース
         * @param charsetName 入力ソースのキャラクタセット
         */
        private InnerReader(InputStream is, String charsetName) {
            try {
                reader = new BufferedReader(new InputStreamReader(is, charsetName));
            } catch (UnsupportedEncodingException e) {
                throw new IORuntimeException(e);
            }
        }

        /**
         * 入力ソースから1文字読み込みます。
         *
         * @return 文字
         * @throws IOException IO例外
         */
        private int read() throws IOException {
            if (line == null) {
                line = reader.readLine();
                pos = 0;
                rowNum++;
            }
            if (line == null) {
                return -1;
            }
            if (pos == line.length()) {
                line = null;
                return LF;
            }
            return line.charAt(pos++);
        }

        @Override
        public void close() throws IOException {
            reader.close();
        }

    }

}

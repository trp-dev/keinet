package jp.ac.kawai_juku.banzaisystem.framework.controller;

/**
 *
 * アクションメソッドの結果インターフェースです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public interface ExecuteResult {

    /**
     * アクションメソッドの結果を処理します。
     *
     * @param controller 結果を返したコントローラ
     */
    void process(AbstractController controller);

}

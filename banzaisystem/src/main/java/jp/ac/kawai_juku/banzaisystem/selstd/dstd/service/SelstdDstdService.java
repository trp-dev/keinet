package jp.ac.kawai_juku.banzaisystem.selstd.dstd.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.bean.SelstdDstdKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dstd.dao.SelstdDstdDao;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 生徒一覧ダイアログのサービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdDstdService {

    /** DAO */
    @Resource
    private SelstdDstdDao selstdDstdDao;

    /** システム情報 */
    @Resource
    private SystemDto systemDto;

    /**
     * 対象生徒選択画面で選択中の個人リストを取得します。
     *
     * @return 個人リスト
     */
    public List<Map<String, CellData>> findKojinList() {
        return createResultList(selstdDstdDao.selectKojinListById(systemDto.getUnivYear(),
                systemDto.getSelectedIndividualIdList()));
    }

    /**
     * 指定した条件の個人リストを取得します。
     *
     * @param grade 学年
     * @param classList クラスリスト
     * @param numberStart 席次番号開始
     * @param numberEnd 席次番号終了
     * @return 個人リスト
     */
    public List<Map<String, CellData>> findKojinList(Integer grade, List<String> classList, String numberStart,
            String numberEnd) {
        if (classList.isEmpty() || (numberStart.isEmpty() && numberEnd.isEmpty())) {
            return Collections.emptyList();
        } else {
            return createResultList(selstdDstdDao.selectKojinListByClass(systemDto.getUnivYear(), grade, classList,
                    numberStart, numberEnd));
        }
    }

    /**
     * 結果リストを生成します。
     *
     * @param list 個人情報リスト
     * @return 結果リスト
     */
    private List<Map<String, CellData>> createResultList(List<SelstdDstdKojinBean> list) {

        List<Map<String, CellData>> result = CollectionsUtil.newArrayList(list.size());

        for (SelstdDstdKojinBean bean : list) {
            Map<String, CellData> map = CollectionsUtil.newHashMap();
            map.put("id", new CellData(bean.getIndividualId()));
            map.put("class", new CellData(bean.getCls()));
            map.put("number", new CellData(bean.getClassNo()));
            map.put("name", new CellData(bean.getNameKana()));
            result.add(map);
        }

        return result;
    }

}

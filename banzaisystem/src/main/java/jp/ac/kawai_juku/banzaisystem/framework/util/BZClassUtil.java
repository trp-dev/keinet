package jp.ac.kawai_juku.banzaisystem.framework.util;

import org.seasar.framework.aop.javassist.AspectWeaver;

/**
 *
 * クラスに関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZClassUtil {

    /**
     * コンストラクタです。
     */
    private BZClassUtil() {
    }

    /**
     * クラスがAOPによってエンハンスされている場合は、エンハンス前のクラスを返します。
     *
     * @param paramClass クラス
     * @return エンハンス前のクラス
     */
    public static Class<?> getOriginalClass(Class<?> paramClass) {
        Class<?> clazz = paramClass;
        while (clazz.getName().indexOf(AspectWeaver.SUFFIX_ENHANCED_CLASS) > -1) {
            clazz = clazz.getSuperclass();
        }
        return clazz;
    }

    /**
     * クラスの単純名を返します。<br>
     * クラスがAOPによってエンハンスされている場合は、エンハンス前の単純名を返します。
     *
     * @param paramClass クラス
     * @return クラスの単純名
     */
    public static String getSimpleName(Class<?> paramClass) {
        return getOriginalClass(paramClass).getSimpleName();
    }

}

package jp.ac.kawai_juku.banzaisystem.output.main.component;

import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.framework.excel.ExcelLauncher;
import jp.ac.kawai_juku.banzaisystem.output.main.helper.OutputMainHelper;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCsvInBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.maker.SelstdMainB0101Maker;

/**
 *
 * 個人成績のノードです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
class B0101Node extends OutputCsvNode {

    /**
     * コンストラクタです。
     */
    B0101Node() {
        super("個人成績");
    }

    @Override
    void output(OutputMainHelper helper) {
        new ExcelLauncher(GamenId.D).addCreator(SelstdMainB0101Maker.class, new SelstdMainCsvInBean(helper.findTargetExam(), helper.findPrintTargetList())).launch();
    }

}

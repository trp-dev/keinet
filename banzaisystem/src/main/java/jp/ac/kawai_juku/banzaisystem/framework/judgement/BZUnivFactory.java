package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.ac.kawai_juku.banzaisystem.framework.bean.RankBean;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.beans.detail.CenterDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.UnivDetail;
import jp.co.fj.kawaijuku.judgement.data.UnivData;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 大学インスタンスのファクトリクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZUnivFactory {

    /** ブロックの区切り文字 */
    public static final char BLOCK_SEP = '#';

    /** 値の区切り文字 */
    public static final char VALUE_SEP = ';';

    /**
     * 大学インスタンスを生成します。
     *
     * @param systemDto システム情報DTO
     * @param univKeyBean {@link UnivKeyBean}
     *
     * @return 大学インスタンス
     */
    public UnivData createUniv(SystemDto systemDto, UnivKeyBean univKeyBean) {
        String jString = systemDto.getjStringMap().get(univKeyBean.getUnivKey());
        if (jString != null) {
            return createUniv(univKeyBean, jString, systemDto.getRankMap(), systemDto);
        } else {
            throw new RuntimeException("判定用大学マスタが見つかりませんでした。" + univKeyBean);
        }
    }

    /**
     * 大学インスタンスを生成します。
     *
     * @param univKeyBean {@link UnivKeyBean}
     * @param jString 判定用文字列
     * @param rankMap ランクマップ
     * @param systemDto システムDTO
     * @return 大学インスタンス
     */
    public UnivData createUniv(UnivKeyBean univKeyBean, String jString, Map<String, RankBean> rankMap, SystemDto systemDto) {

        /* TODO:banzan-master改修済でない場合、判定処理の最新化はしてはダメなので注意！！！(Jstringに選択グループが追加される予定) */
        String[] blocks = StringUtils.split(jString, BLOCK_SEP);

        UnivData univ = createUnivData(blocks, univKeyBean, rankMap);
        univ.setDetailHash(createDetailHash(blocks, univKeyBean));
        univ.setRejectHash(createRejectHash(blocks, univKeyBean));

        univ.setEngAppReqInfo(systemDto.getEngPtappreqList());
        univ.setEngScoreConversion(systemDto.getEntPtscoreconvList());
        univ.setJpnScoreConversion(systemDto.getKokugoDescscoreconvList());

        /* 参加試験判定フラグ */
        /*
        if (engFlg) {
            univ.setSSHanteiFlg(1);
        } else {
            univ.setSSHanteiFlg(0);
        }
        */
        univ.setSSHanteiFlg(0);
        return univ;
    }

    /**
     * ブロック文字列を値配列に変換します。
     *
     * @param block ブロック
     * @return 値配列
     */
    private String[] block2Values(String block) {
        return StringUtils.splitPreserveAllTokens(block, VALUE_SEP);
    }

    /**
     * 大学基本情報を生成します。
     *
     * @param blocks ブロック配列
     * @param key {@link UnivKeyBean}
     * @param rankMap ランクマップ
     * @return {@link UnivData}
     */
    private UnivData createUnivData(String[] blocks, UnivKeyBean key, Map<String, RankBean> rankMap) {

        String[] values = block2Values(blocks[0]);

        UnivData univ = new UnivData(key.getUnivCd(), key.getFacultyCd(), key.getDeptCd(), values[0], values[1], NumberUtils.toInt(values[10], JudgementConstants.Univ.CERTAIN_POINT_NA), //
                NumberUtils.toInt(values[11], JudgementConstants.Univ.BORDER_POINT_NA), //
                NumberUtils.toInt(values[12], JudgementConstants.Univ.WARNING_POINT_NA), //
                NumberUtils.toInt(values[13], JudgementConstants.Univ.CEN_SCORE_RATE_NA), //
                values[6], Integer.parseInt(values[7]), Integer.parseInt(values[8]));

        /* ランク情報を設定する */
        RankBean rankBean = rankMap.get(values[6]);
        if (rankBean != null) {
            univ.setRankName(rankBean.getRankName());
            univ.setRankHLimits(rankBean.getRankHLimits());
            univ.setRankLLimits(rankBean.getRankLLimits());
        }

        /* ボーダ入力区分 */
        univ.setBorderInputKbn(NumberUtils.toInt(values[9], 1));
        /* ボーダーポイント（参加試験あり） */
        univ.setBorderPointSS(NumberUtils.toInt(values[3], JudgementConstants.Univ.BORDER_POINT_NA));
        /* 濃厚ポイント（参加試験あり） */
        univ.setCertainPointSS(NumberUtils.toInt(values[2], JudgementConstants.Univ.CERTAIN_POINT_NA));
        /* 注意ポイント（参加試験あり） */
        univ.setWarningPointSS(NumberUtils.toInt(values[4], JudgementConstants.Univ.WARNING_POINT_NA));
        /* 出願要件パターンコード */
        univ.setAppReqPaternCd(values[14]);
        /* 英語参加試験ボーダー利用フラグ */
        univ.setIsSSUse(NumberUtils.toInt(values[15], 1));
        /* 英語参加試験代表枝番 */
        univ.setEngSsDaihyoEdaban(values[16]);

        return univ;
    }

    /**
     * 大学詳細マップを生成します。
     *
     * @param blocks ブロック配列
     * @param key {@link UnivKeyBean}
     * @return 大学詳細マップ
     */
    private Map<String, UnivDetail> createDetailHash(String[] blocks, UnivKeyBean key) {

        /*
         * jp.co.fj.kawaijuku.judgement.factory.FactoryHelperを参考にして実装した。
         */

        List<CenterDetail> centerList = createCenterDetailList(blocks);
        List<SecondDetail> secondList = createSecondDetailList(blocks);

        /* センタを基準にペアを作成する */
        Map<String, UnivDetail> map = new TreeMap<String, UnivDetail>();
        for (CenterDetail center : centerList) {
            for (SecondDetail second : secondList) {
                String branchCd = toStringBranchCd(map.size() + 1);
                UnivDetail detail = new UnivDetail(key.getUnivCd(), key.getFacultyCd(), key.getDeptCd(), branchCd);
                detail.setCenter(center);
                detail.setSecond(second);
                map.put(branchCd, detail);
            }
        }

        return map;
    }

    /**
     * 足切判定用の大学詳細マップを生成します。
     *
     * @param blocks ブロック配列
     * @param key {@link UnivKeyBean}
     * @return 足切判定用の大学詳細マップ
     */
    private Map<String, UnivDetail> createRejectHash(String[] blocks, UnivKeyBean key) {

        List<CenterDetail> rejectList = createRejectDetailList(blocks);
        if (rejectList.isEmpty()) {
            return null;
        }

        /* センタを基準にペアを作成する */
        Map<String, UnivDetail> map = new TreeMap<String, UnivDetail>();
        for (CenterDetail reject : rejectList) {
            String branchCd = toStringBranchCd(map.size() + 1);
            UnivDetail detail = new UnivDetail(key.getUnivCd(), key.getFacultyCd(), key.getDeptCd(), branchCd);
            detail.setCenter(reject);
            detail.setSecond(new SecondDetail());
            map.put(branchCd, detail);
        }

        return map;
    }

    /**
     * センター詳細情報リストを生成します。
     *
     * @param blocks ブロック配列
     * @return センター詳細情報リスト
     */
    private List<CenterDetail> createCenterDetailList(String[] blocks) {

        List<CenterDetail> list = CollectionsUtil.newArrayList();

        for (int i = 1; i < blocks.length; i++) {
            if (blocks[i].startsWith("1")) {
                list.add(new CenterDetailFactory(block2Values(blocks[i])).create());
            }
        }

        if (list.isEmpty()) {
            /* センター情報が存在しない場合は、空のインスタンスを入れておく */
            list.add(new CenterDetail());
        }

        return list;
    }

    /**
     * 二次詳細情報リストを生成します。
     *
     * @param blocks ブロック配列
     * @return 二次詳細情報リスト
     */
    private List<SecondDetail> createSecondDetailList(String[] blocks) {

        List<SecondDetail> list = CollectionsUtil.newArrayList();

        for (int i = 1; i < blocks.length; i++) {
            if (blocks[i].startsWith("2")) {
                list.add(new SecondDetailFactory(block2Values(blocks[i])).create());
            }
        }

        if (list.isEmpty()) {
            /* 二次情報が存在しない場合は、空のインスタンスを入れておく */
            list.add(new SecondDetail());
        }

        return list;
    }

    /**
     * 足切詳細情報リストを生成します。
     *
     * @param blocks ブロック配列
     * @return センター詳細情報リスト
     */
    private List<CenterDetail> createRejectDetailList(String[] blocks) {

        List<CenterDetail> list = CollectionsUtil.newArrayList();

        for (int i = 1; i < blocks.length; i++) {
            if (blocks[i].startsWith("3")) {
                list.add(new CenterDetailFactory(block2Values(blocks[i])).create());
            }
        }

        return list;
    }

    /**
     * 数値の枝番を文字列に変換します。
     *
     * @param branchCd 数値の枝番
     * @return 文字列の枝番
     */
    private String toStringBranchCd(int branchCd) {
        return StringUtils.leftPad(Integer.toString(branchCd), JudgementConstants.Detail.BRANCHCD_LENGTH, '0');
    }

}

package jp.ac.kawai_juku.banzaisystem.exmntn.univ.helper;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.AbstractButton;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0201InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivA0301InBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailCenterSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSecondSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivDetailSvcOutBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.bean.ExmntnUnivStudentBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.controller.ExmntnUnivController;
import jp.ac.kawai_juku.banzaisystem.exmntn.univ.view.ExmntnUnivView;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.StringUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 志望大学詳細画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnUnivHelper {

    /** DTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto dto;

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** View */
    @Resource(name = "exmntnUnivView")
    private ExmntnUnivView view;

    /** 個人成績画面のView */
    @Resource
    private ExmntnMainView exmntnMainView;

    /** 個人成績画面の成績に関するヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /**
     * 志望順位コンボボックスを初期化します。
     *
     * @param combo 志望順位コンボボックス
     * @param selectedItem 復元する選択値
     */
    public void initCandidateRankComboBox(ComboBox<ExmntnMainCandidateUnivBean> combo, ComboBoxItem<ExmntnMainCandidateUnivBean> selectedItem) {
        List<ComboBoxItem<ExmntnMainCandidateUnivBean>> list = CollectionsUtil.newArrayList();
        int rank = 1;
        for (ExmntnMainCandidateUnivBean bean : createCandidateUnivList()) {
            list.add(new ComboBoxItem<ExmntnMainCandidateUnivBean>(Integer.toString(rank++), bean));
        }
        combo.setItemList(list, selectedItem);
    }

    /**
     * Excel帳票メーカー(簡易個人表)の入力Beanを生成します。
     *
     * @param output1stFlg 第1面出力フラグ
     * @param output2ndFlg 第2面出力フラグ
     * @return {@link ExmntnUnivA0301InBean}
     */
    public ExmntnUnivA0301InBean createA0301InBean(boolean output1stFlg, boolean output2ndFlg) {

        /* 志望大学情報リストを生成する */
        List<ExmntnMainCandidateUnivBean> candidateUnivList = createCandidateUnivList();

        /* 上位9位までとする */
        if (candidateUnivList.size() > 9) {
            candidateUnivList = candidateUnivList.subList(0, 9);
        }

        return new ExmntnUnivA0301InBean(createStudentBean(), dto.getJudgeScore(), candidateUnivList, output1stFlg, output2ndFlg);
    }

    /**
     * 志望大学情報リストを生成します。
     *
     * @return 志望大学情報リスト
     */
    public List<ExmntnMainCandidateUnivBean> createCandidateUnivList() {
        return exmntnMainView.candidateUnivTable.getCandidateUnivList();
    }

    /**
     * Excel帳票メーカー(志望大学詳細)の入力Beanを生成します。
     *
     * @param combo 志望順位コンボボックス
     * @return {@link ExmntnUnivA0201InBean}
     */
    public ExmntnUnivA0201InBean createA0201InBean(ComboBox<ExmntnMainCandidateUnivBean> combo) {
        ComboBoxItem<ExmntnMainCandidateUnivBean> item = combo.getSelectedItem();
        List<Pair<String, ExmntnMainCandidateUnivBean>> list;
        if (item != null) {
            list = Collections.singletonList(Pair.of(item.getLabel(), item.getValue()));
        } else {
            list = Collections.emptyList();
        }
        return createA0201InBean(list);
    }

    /**
     * Excel帳票メーカー(志望大学詳細)の入力Beanを生成します。
     *
     * @param list 志望順位と志望大学情報Beanのペアリスト
     * @return {@link ExmntnUnivA0201InBean}
     */
    public ExmntnUnivA0201InBean createA0201InBean(List<Pair<String, ExmntnMainCandidateUnivBean>> list) {
        return new ExmntnUnivA0201InBean(createStudentBean(), dto.getJudgeScore(), list);
    }

    /**
     * 生徒情報Beanを生成します。
     *
     * @return {@link ExmntnUnivStudentBean}
     */
    public ExmntnUnivStudentBean createStudentBean() {

        ExmntnUnivStudentBean studentBean;
        if (systemDto.isTeacher()) {
            ExmntnMainKojinBean bean = exmntnMainView.numberComboBox.getSelectedValue();
            if (bean == null) {
                return null;
            } else {
                studentBean = new ExmntnUnivStudentBean();
                studentBean.setIndividualId(bean.getIndividualId());
                studentBean.setGrade(ObjectUtils.toString(bean.getGrade()));
                studentBean.setCls(StringUtils.defaultString(bean.getCls()));
                studentBean.setClassNo(StringUtils.defaultString(bean.getClassNo()));
            }
        } else {
            studentBean = new ExmntnUnivStudentBean();
            studentBean.setGrade(exmntnMainView.gradeField.getText());
            studentBean.setCls(exmntnMainView.classField.getText());
            studentBean.setClassNo(exmntnMainView.numberField.getText());
        }

        studentBean.setNameKana(exmntnMainView.nameField.getText());

        return studentBean;
    }

    /**
     * 前の大学、次の大学ボタンの状態を変更します。
     *
     * @param combo 志望順位コンボボックス
     * @param beforeButton 前の大学ボタン
     * @param nextButton 次の大学ボタン
     */
    public void changeButtonStatus(ComboBox<?> combo, AbstractButton beforeButton, AbstractButton nextButton) {
        if (combo.getItemCount() <= 1) {
            beforeButton.setEnabled(false);
            nextButton.setEnabled(false);
        } else if (combo.getSelectedIndex() == 0) {
            beforeButton.setEnabled(false);
            nextButton.setEnabled(true);
        } else if (combo.getSelectedIndex() == combo.getItemCount() - 1) {
            beforeButton.setEnabled(true);
            nextButton.setEnabled(false);
        } else {
            beforeButton.setEnabled(true);
            nextButton.setEnabled(true);
        }
    }

    /**
     * 志望順位コンボの空欄アイテム（検索結果の志望大学）を削除します。
     *
     * @param combo 志望順位コンボボックス
     */
    public void removeBlankComboBoxItem(ComboBox<?> combo) {
        if (combo.getItemCount() > 0) {
            if (combo.getSelectedIndex() > 0 && StringUtil.isBlank(combo.getItemAt(0).getLabel())) {
                combo.removeItemAt(0);
            }
        }
    }

    /**
     * 検索結果／受験スケジュール画面からの詳細画面表示処理です。
     *
     * @param bean {@link ExmntnMainCandidateUnivBean}
     */
    public void dispSearchDetail(ExmntnMainCandidateUnivBean bean) {

        for (int i = 0; i < view.candidateRankComboBox.getItemCount(); i++) {
            ComboBoxItem<ExmntnMainCandidateUnivBean> item = view.candidateRankComboBox.getItemAt(i);
            if (!StringUtil.isEmpty(item.getLabel()) && item.getValue().equals(bean)) {
                /* 選択した検索結果が志望大学に存在する場合 */
                view.candidateRankComboBox.setSelectedIndex(i);
                if (!exmntnMainView.tabbedPane.isActive(ExmntnUnivController.class)) {
                    exmntnMainView.tabbedPane.activate(ExmntnUnivController.class);
                }
                return;
            }
        }

        int itemCnt = view.candidateRankComboBox.getItemCount();

        List<ComboBoxItem<ExmntnMainCandidateUnivBean>> itemList = CollectionsUtil.newArrayList(itemCnt + 1);

        for (int i = 0; i < itemCnt; i++) {
            ComboBoxItem<ExmntnMainCandidateUnivBean> item = view.candidateRankComboBox.getItemAt(i);
            if (!StringUtil.isEmpty(item.getLabel())) {
                itemList.add(item);
            }
        }

        itemList.add(0, new ComboBoxItem<ExmntnMainCandidateUnivBean>(StringUtils.EMPTY, bean));

        view.candidateRankComboBox.setItemList(itemList, false);

        exmntnMainView.tabbedPane.activate(ExmntnUnivController.class);
    }

    /**
     * センタ詳細を表示します。
     *
     * @param univDetail 大学詳細情報
     * @param isAttentionVisible 注意事項欄表示フラグ
     * @param exam 模試情報
     */
    public void setDispDetailCenter(ExmntnUnivDetailSvcOutBean univDetail, boolean isAttentionVisible, ExamBean exam) {

        ExmntnUnivDetailCenterSvcOutBean center = univDetail.getCenter();
        /* ExmntnUnivDetailCenterSvcOutBean include = univDetail.getCenterInclude(); */

        if (exam == null) {
            /* マークがNullの場合初期化 */
            /*
            view.getPanel().setImage(view.getClass(), "bgCenter.png");
            view.resultCenterLabel.setLocation(511, 86);
            view.centerRatingLabel.setSecondRatingText(include.getRating());
            view.centerRatingLabel.setVisible(true);
            view.centerRatingLabel.setLocation(530, 85);
            view.centerRatingLabel.setSize(120, view.centerRatingLabel.getSize().height);
            */

            /*view.centerIncludeEnglishRequirementsLabel.setText(StringUtils.EMPTY);*/
            view.centerRatingLabel.setRatingText(StringUtils.EMPTY);
            /*
            view.resultCenterIncludeLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultCenterIncludeEngLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultCenterLabel.setRatingIcon(include.getRating());
            */
            view.centerScoreLabel.setText(StringUtils.EMPTY);
            /*
            view.centerFullScoreLabel.setText(include.getFullPoint());
            view.centerBorderLabel.setText(include.getBorder());
            view.centerALineLabel.setText(include.getLineA());
            view.centerBLineLabel.setText(include.getLineB());
            view.centerCLineLabel.setText(include.getLineC());
            view.centerDLineLabel.setText(include.getLineD());
            */
            view.centerRatingLabel.setText(StringUtils.EMPTY);
            view.centerRemainScoreLabel.setText(StringUtils.EMPTY);
            /*
            view.centerIncludeEnglishRequirementsLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRatingLabel.setRatingText(StringUtils.EMPTY);
            view.resultCenterIncludeLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultCenterIncludeEngLabel.setRatingIcon(StringUtils.EMPTY);
            view.centerIncludeScoreLabel.setText(StringUtils.EMPTY);
            view.centerIncludeFullScoreLabel.setText(StringUtils.EMPTY);
            view.centerIncludeBorderLabel.setText(StringUtils.EMPTY);
            view.centerIncludeALineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeBLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeCLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeDLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRemainRaitingLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRemainScoreLabel.setText(StringUtils.EMPTY);
            */
            view.centerScoreEnglishLabel.setText(StringUtils.EMPTY);
            /*view.centerScoreEnglishJoiningLabel.setText(StringUtils.EMPTY);*/
            view.centerScoreMathILabel.setText(StringUtils.EMPTY);
            view.centerScoreMathIILabel.setText(StringUtils.EMPTY);
            view.centerScoreLanguageLabel.setText(StringUtils.EMPTY);
            /*view.centerScoreLanguageDescLabel.setText(StringUtils.EMPTY);*/
            view.centerScoreScienceILabel.setText(StringUtils.EMPTY);
            view.centerScoreScienceIILabel.setText(StringUtils.EMPTY);
            view.centerScoreScienceIIILabel.setText(StringUtils.EMPTY);
            view.centerScoreSocietyILabel.setText(StringUtils.EMPTY);
            view.centerScoreSocietyIILabel.setText(StringUtils.EMPTY);
            view.centerDescriptionLabel.setText(StringUtils.EMPTY);
            view.centerSecondSelectedLabel.setText(StringUtils.EMPTY);
            view.centerFirstSelectedLabel.setText(StringUtils.EMPTY);
            view.centerFirstSelectedScoreLabel.setText(StringUtils.EMPTY);
            view.centerRemainRaitingLabel.setText(BZConstants.ASTERISK);
            view.centerRemainScoreLabel.setText(BZConstants.ASTERISK);
            view.centerFirstSelectedScoreLabel.setText(StringUtils.EMPTY);
            view.centerCommentStatementLabel.setText(StringUtils.EMPTY);
            /* return; */
        }
        /* boolean isCenter = ExamUtil.isCenter(exam); */

        /*if (isCenter && !jp.co.fj.kawaijuku.judgement.util.StringUtil.isEmptyTrim(include.getAppReqPaternCd())) {
            view.getPanel().setImage(view.getClass(), "bgCenter.png");
            view.resultCenterLabel.setLocation(511, 86);
            view.centerRatingLabel.setSecondRatingText(include.getRating());
            view.centerRatingLabel.setVisible(isCenter);
            view.centerRatingLabel.setLocation(530, 85);
            view.centerRatingLabel.setSize(120, view.centerRatingLabel.getSize().height);
        } else {
            view.getPanel().setImage(view.getClass(), "bg.png");
            view.resultCenterLabel.setLocation(492, 86);
            view.centerRatingLabel.setLocation(538, 85);
            view.centerRatingLabel.setSize(50, view.centerRatingLabel.getSize().height);
        }*/
        /* 共通テスト（含む）評価の表示／非表示を切り替え */
        /*view.centerIncludeEnglishRequirementsLabel.setVisible(!isCenter);
        view.resultCenterIncludeLabel.setVisible(!isCenter);
        view.resultCenterIncludeEngLabel.setVisible(isCenter);
        view.centerIncludeRatingLabel.setVisible(!isCenter);
        view.centerIncludeScoreLabel.setVisible(!isCenter);
        view.centerIncludeRemainRaitingLabel.setVisible(!isCenter);
        view.centerIncludeRemainScoreLabel.setVisible(!isCenter);
        view.centerIncludeFullScoreLabel.setVisible(!isCenter);
        view.centerIncludeBorderLabel.setVisible(!isCenter);
        view.centerIncludeALineLabel.setVisible(!isCenter);
        view.centerIncludeBLineLabel.setVisible(!isCenter);
        view.centerIncludeCLineLabel.setVisible(!isCenter);
        view.centerIncludeDLineLabel.setVisible(!isCenter);*/

        /* 共通テスト評価 */
        /*        if (isCenter && !jp.co.fj.kawaijuku.judgement.util.StringUtil.isEmptyTrim(include.getAppReqPaternCd())) {
            view.resultCenterLabel.setRatingIcon(include.getRating());
            view.centerRatingLabel.setTotalRatingText(center.getRating());
            view.centerScoreLabel.setText(include.getScore());
            view.centerFullScoreLabel.setText(include.getFullPoint());
            view.centerBorderLabel.setText(include.getBorder());
            view.centerALineLabel.setText(include.getLineA());
            view.centerBLineLabel.setText(include.getLineB());
            view.centerCLineLabel.setText(include.getLineC());
            view.centerDLineLabel.setText(include.getLineD());
        } else {*/
        view.resultCenterLabel.setRatingIcon(center.getRating());
        view.centerRatingLabel.setTotalRatingText(center.getRating());
        view.centerScoreLabel.setText(center.getScore());
        view.centerFullScoreLabel.setText(center.getFullPoint());
        view.centerBorderLabel.setText(center.getBorder());
        view.centerALineLabel.setText(center.getLineA());
        view.centerBLineLabel.setText(center.getLineB());
        view.centerCLineLabel.setText(center.getLineC());
        view.centerDLineLabel.setText(center.getLineD());
        /*}*/

        /* 共通テスト（含む）評価 */
        /*if (!jp.co.fj.kawaijuku.judgement.util.StringUtil.isEmptyTrim(include.getAppReqPaternCd())) {
            view.centerIncludeEnglishRequirementsLabel.setText(getMessage("label.exmntn.univ.englishRequirements", include.getEnglishRequirements()));
            view.centerIncludeRatingLabel.setRatingText(include.getRating());
            view.resultCenterIncludeLabel.setRatingIcon(include.getRating());
            view.resultCenterIncludeEngLabel.setRatingIcon(include.getEnglishRequirements());
            view.centerIncludeScoreLabel.setText(include.getScore());
            view.centerIncludeFullScoreLabel.setText(include.getFullPoint());
            view.centerIncludeBorderLabel.setText(include.getBorder());
            view.centerIncludeALineLabel.setText(include.getLineA());
            view.centerIncludeBLineLabel.setText(include.getLineB());
            view.centerIncludeCLineLabel.setText(include.getLineC());
            view.centerIncludeDLineLabel.setText(include.getLineD());
            view.centerIncludeRemainRaitingLabel.setText(include.getRemainRaiting());
            view.centerIncludeRemainScoreLabel.setText(getMessage("label.exmntn.univ.remainScoreLabel", include.getRemainScore()));
        } else {
            view.centerIncludeEnglishRequirementsLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRatingLabel.setRatingText(StringUtils.EMPTY);
            view.resultCenterIncludeLabel.setRatingIcon(StringUtils.EMPTY);
            view.resultCenterIncludeEngLabel.setRatingIcon(StringUtils.EMPTY);
            view.centerIncludeScoreLabel.setText(StringUtils.EMPTY);
            view.centerIncludeFullScoreLabel.setText(StringUtils.EMPTY);
            view.centerIncludeBorderLabel.setText(StringUtils.EMPTY);
            view.centerIncludeALineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeBLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeCLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeDLineLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRemainRaitingLabel.setText(StringUtils.EMPTY);
            view.centerIncludeRemainScoreLabel.setText(StringUtils.EMPTY);
        }*/

        /* 得点 */
        view.centerScoreEnglishLabel.setText(center.getScoreEnglish());
        /*view.centerScoreEnglishJoiningLabel.setText(center.getScoreEnglishJoining());*/
        view.centerScoreMathILabel.setText(center.getScoreMath1());
        view.centerScoreMathIILabel.setText(center.getScoreMath2());
        view.centerScoreLanguageLabel.setText(center.getScoreJap());
        /*view.centerScoreLanguageDescLabel.setText(center.getScoreJapDesc());*/
        view.centerScoreScienceILabel.setText(center.getScoreSci1());
        view.centerScoreScienceIILabel.setText(center.getScoreSci2());
        view.centerScoreScienceIIILabel.setText(center.getScoreSci3());
        view.centerScoreSocietyILabel.setText(center.getScoreSoc1());
        view.centerScoreSocietyIILabel.setText(center.getScoreSoc2());

        /* 備考 */
        view.centerDescriptionLabel.setText("<html>" + center.getNote() + "</html>");

        /* #1評価版 無効 */
        view.centerSecondSelectedLabel.setText(StringUtils.EMPTY);
        /* #1評価版 無効 */
        view.centerFirstSelectedLabel.setText(StringUtils.EMPTY);
        /* #1評価版 無効 */
        view.centerFirstSelectedScoreLabel.setText(StringUtils.EMPTY);

        /* あと何点-判定 */
        view.centerRemainRaitingLabel.setText(getMessage("label.exmntn.univ.remainRating", center.getRemainRaiting()));
        /* あと何点-得点 */
        view.centerRemainScoreLabel.setText(getMessage("label.exmntn.univ.remainScore", center.getRemainScore()));

        /* 第1段階選抜予想 */
        view.centerFirstSelectedScoreLabel.setText(createCenterFirstSelectedScoreText(center));

        /* 注釈文 */
        if (isAttentionVisible) {
            view.centerCommentStatementLabel.setText(String.format("<html>%s</html>", center.getCommentStatement() == null ? StringUtils.EMPTY : center.getCommentStatement()));
        } else {
            view.centerCommentStatementLabel.setText(StringUtils.EMPTY);
        }
    }

    /**
     * 「第1段階選抜予想」テキストを生成します。
     *
     * @param center {@link ExmntnUnivDetailCenterSvcOutBean}
     * @return 「第1段階選抜予想」テキスト
     */
    private String createCenterFirstSelectedScoreText(ExmntnUnivDetailCenterSvcOutBean center) {
        StringBuilder sb = new StringBuilder();
        if (center.getPickedLine() != null) {
            sb.append(center.getPickedLine());
            if (center.getRejectScore() != null) {
                sb.append(getMessage("label.exmntn.univ.selfScore", center.getRejectScore()));
            }
        }
        return sb.toString();
    }

    /**
     * 二次詳細を表示します。
     *
     * @param second 詳細情報
     * @param exam 模試情報
     */
    public void setDispDetailSecond(ExmntnUnivDetailSecondSvcOutBean second, ExamBean exam) {

        /* 記述模試が未選択もしくは、二次の判定が行われてない場合 */
        if (exam == null && !second.isJudged()) {
            view.resultSecondLabel.setRatingIcon(StringUtils.EMPTY);
            view.secondRatingLabel.setTotalRatingText(StringUtils.EMPTY);
            view.secondDeviationEnglishLabel.setText(StringUtils.EMPTY);
            view.secondDeviationMathLabel.setText(StringUtils.EMPTY);
            view.secondDeviationLanguageLabel.setText(StringUtils.EMPTY);
            view.secondDeviationScienceILabel.setText(StringUtils.EMPTY);
            view.secondDeviationScienceIILabel.setText(StringUtils.EMPTY);
            view.secondDeviationSocietyILabel.setText(StringUtils.EMPTY);
            view.secondDeviationSocietyIILabel.setText(StringUtils.EMPTY);
            view.secondDescriptionLabel.setText(StringUtils.EMPTY);
            view.secondScoreLabel.setText(StringUtils.EMPTY);
            view.secondFullScoreLabel.setText(second.getFullPoint());
            view.secondBorderLabel.setText(second.getBorderRank());
            view.secondALineLabel.setText(second.getLineA());
            view.secondBLineLabel.setText(second.getLineB());
            view.secondCLineLabel.setText(second.getLineC());
            view.secondDLineLabel.setText(second.getLineD());
            view.secondRemainRaitingLabel.setText(getMessage("label.exmntn.univ.remainRating", BZConstants.ASTERISK));
            view.secondRemainScoreLabel.setText(String.format(getMessage("label.exmntn.univ.remainDeviation"), BZConstants.ASTERISK));
            return;
        }
        view.resultSecondLabel.setRatingIcon(second.getRating());
        view.secondRatingLabel.setTotalRatingText(second.getRating());

        /* 偏差値 */
        view.secondDeviationEnglishLabel.setText(second.getDevEnglish());
        view.secondDeviationMathLabel.setText(second.getDevMath());
        view.secondDeviationLanguageLabel.setText(second.getDevJap());
        view.secondDeviationScienceILabel.setText(second.getDevSci1());
        view.secondDeviationScienceIILabel.setText(second.getDevSci2());
        view.secondDeviationSocietyILabel.setText(second.getDevSoc1());
        view.secondDeviationSocietyIILabel.setText(second.getDevSoc2());

        /* 備考 */
        view.secondDescriptionLabel.setText("<html>" + second.getNote() + "</html>");

        /* 評価 */
        view.secondScoreLabel.setText(second.getDev());
        view.secondFullScoreLabel.setText(second.getFullPoint());
        view.secondBorderLabel.setText(second.getBorderRank());
        view.secondALineLabel.setText(second.getLineA());
        view.secondBLineLabel.setText(second.getLineB());
        view.secondCLineLabel.setText(second.getLineC());
        view.secondDLineLabel.setText(second.getLineD());

        /* あと何点-判定 */
        view.secondRemainRaitingLabel.setText(getMessage("label.exmntn.univ.remainRating", second.getRemainRaiting()));
        /* あと何点-偏差値 */
        /* 偏差値の . がリソースキーと判定されてしまうため、getMessageの置換パラメータとして使えない */
        view.secondRemainScoreLabel.setText(String.format(getMessage("label.exmntn.univ.remainDeviation"), second.getRemainDeviation()));
    }

}

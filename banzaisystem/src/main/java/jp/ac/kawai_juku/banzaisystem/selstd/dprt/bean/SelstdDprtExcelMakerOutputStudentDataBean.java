package jp.ac.kawai_juku.banzaisystem.selstd.dprt.bean;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 個人成績・志望一覧(個人成績一覧)帳票の出力データ（個人成績一覧＋志望大学）Beanです。
 *
 *
 * @author TOTEC)SHIMIZU.Masami
 *
 */
public class SelstdDprtExcelMakerOutputStudentDataBean {

    /** 個人ID */
    private Integer individualId;

    /** 学年 */
    private Integer grade;

    /** クラス */
    private String cls;

    /** クラス番号 */
    private String classNo;

    /** 対象模試 */
    private String examCd;

    /** 氏名（カナ） */
    private String nameKana;

    /** 文理コード */
    private String bunriCode;

    /** 対象模試名 */
    private String examName;

    /** 対象月 */
    private String inpleDate;

    /** 対象模試タイプ */
    private String examType;

    /** 模試外部データ開放日 */
    private String examOpenDate;

    /** 個人成績一覧リスト */
    private final List<SelstdDprtExcelMakerStudentRecordBean> recordBeanList;

    /** 個人成績一覧リスト（ドッキング模試用） */
    private final List<SelstdDprtExcelMakerStudentRecordBean> recordDockingBeanList;

    /** 志望大学リスト */
    private final List<SelstdDprtExcelMakerCandidateUnivStudentBean> candidateUnivBeanList;

    /**
     * コンストラクタです。
     */
    public SelstdDprtExcelMakerOutputStudentDataBean() {
        recordBeanList = new ArrayList<SelstdDprtExcelMakerStudentRecordBean>();
        recordDockingBeanList = new ArrayList<SelstdDprtExcelMakerStudentRecordBean>();
        candidateUnivBeanList = new ArrayList<SelstdDprtExcelMakerCandidateUnivStudentBean>();
    }

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 学年を返します。
     *
     * @return 学年
     */
    public Integer getGrade() {
        return grade;
    }

    /**
     * 学年をセットします。
     *
     * @param grade 学年
     */
    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    /**
     * クラスを返します。
     *
     * @return クラス
     */
    public String getCls() {
        return cls;
    }

    /**
     * クラスをセットします。
     *
     * @param cls クラス
     */
    public void setCls(String cls) {
        this.cls = cls;
    }

    /**
     * クラス番号を返します。
     *
     * @return クラス番号
     */
    public String getClassNo() {
        return classNo;
    }

    /**
     * クラス番号をセットします。
     *
     * @param classNo クラス番号
     */
    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 対象模試をセットします。
     *
     * @param examCd 対象模試
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 氏名（カナ）を返します。
     *
     * @return 氏名（カナ）
     */
    public String getNameKana() {
        return nameKana;
    }

    /**
     * 氏名（カナ）をセットします。
     *
     * @param nameKana 氏名（カナ）
     */
    public void setNameKana(String nameKana) {
        this.nameKana = nameKana;
    }

    /**
     * 文理コードを返します。
     *
     * @return 文理コード
     */
    public String getBunriCode() {
        return bunriCode;
    }

    /**
     * 文理コードをセットします。
     *
     * @param bunriCode 文理コード
     */
    public void setBunriCode(String bunriCode) {
        this.bunriCode = bunriCode;
    }

    /**
     * 対象模試を返します。
     *
     * @return 対象模試
     */
    public String getExamName() {
        return examName;
    }

    /**
     * 対象模試をセットします。
     *
     * @param examName 対象模試
     */
    public void setExamName(String examName) {
        this.examName = examName;
    }

    /**
     * 対象月を返します。
     *
     * @return 対象月
     */
    public String getInpleDate() {
        return inpleDate;
    }

    /**
     * 対象月をセットします。
     *
     * @param inpleDate 対象月
     */
    public void setInpleDate(String inpleDate) {
        this.inpleDate = inpleDate;
    }

    /**
     * 対象模試タイプを返します。
     *
     * @return 対象模試タイプ
     */
    public String getExamType() {
        return examType;
    }

    /**
     * 対象模試タイプをセットします。
     *
     * @param examType 対象模試タイプ
     */
    public void setExamType(String examType) {
        this.examType = examType;
    }

    /**
     * 模試外部データ開放日を返します。
     *
     * @return 模試外部データ開放日
     */
    public String getOpenDate() {
        return examOpenDate;
    }

    /**
     * 模試外部データ開放日タイプをセットします。
     *
     * @param examOpenDate 模試外部データ開放日
     */
    public void setExamOpenDate(String examOpenDate) {
        this.examOpenDate = examOpenDate;
    }

    /**
     * 個人成績一覧リストを返します。
     *
     * @return 個人成績一覧リスト
     */
    public List<SelstdDprtExcelMakerStudentRecordBean> getRecordBeanList() {
        return recordBeanList;
    }

    /**
     * 個人成績一覧リスト（ドッキング模試用）を返します。
     *
     * @return 個人成績一覧リスト（ドッキング模試用）
     */
    public List<SelstdDprtExcelMakerStudentRecordBean> getRecordDockingBeanList() {
        return recordDockingBeanList;
    }

    /**
     * 志望大学リストを返します。
     *
     * @return 志望大学リスト
     */
    public List<SelstdDprtExcelMakerCandidateUnivStudentBean> getCandidateUnivBeanList() {
        return candidateUnivBeanList;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import org.apache.commons.lang3.ObjectUtils;

/**
 *
 * ソート時にNULL値を非NULL値よりも大きい値として扱うセルデータです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class NullGreaterCellData extends CellData {

    /**
     * コンストラクタです。
     *
     * @param dispValue 表示用の値
     * @param sortValue ソート用の値
     */
    public NullGreaterCellData(Comparable<?> dispValue, Comparable<?> sortValue) {
        super(dispValue, sortValue);
    }

    /**
     * コンストラクタです。
     *
     * @param dispValue 表示用の値
     */
    public NullGreaterCellData(Comparable<?> dispValue) {
        super(dispValue);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public int compareTo(CellData o) {
        return ObjectUtils.compare((Comparable) getSortValue(), (Comparable) o.getSortValue(), true);
    }

}

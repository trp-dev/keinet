package jp.ac.kawai_juku.banzaisystem.framework.view;

/**
 *
 * サブ画面の基底Viewクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class SubView extends AbstractView {
}

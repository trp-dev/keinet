package jp.ac.kawai_juku.banzaisystem.exmntn.ente.component;

import jp.ac.kawai_juku.banzaisystem.framework.component.MonthComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.util.ScheduleUtil;

/**
 *
 * 入試日程：月コンボボックスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class ExmntnEnteMonthComboBox extends MonthComboBox {

    @Override
    public void setEnabled(boolean b) {
        if (b && !ScheduleUtil.isScheduleSearchEnabled()) {
            /* 日程表示が無効なら有効化しない */
            return;
        }
        super.setEnabled(b);
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.service;

import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.CsvId;
import jp.ac.kawai_juku.banzaisystem.GamenId;
import jp.ac.kawai_juku.banzaisystem.PrintId;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

import org.seasar.framework.log.Logger;

/**
 *
 * ログ出力サービスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class LogService {

    /** Logger */
    private static final Logger logger = Logger.getLogger(LogService.class);

    /** ログ出力内容のセパレータ */
    private static final char SEPARATOR = ',';

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /**
     * 指定した画面IDのログを出力します。
     *
     * @param id 画面ID
     * @param contents 出力内容
     */
    public void output(GamenId id, String... contents) {

        StringBuilder sb = new StringBuilder();

        /* 画面IDを追記する */
        sb.append(id.toLogId());

        /* モードを追記する */
        appendMode(id, sb);

        /* 出力内容を追記する */
        for (String content : contents) {
            sb.append(SEPARATOR).append(content);
        }

        logger.info(sb.toString());
    }

    /**
     * 帳票出力ログを出力します。
     *
     * @param id 画面ID
     * @param printIdList 帳票IDリスト
     */
    public void output(GamenId id, List<PrintId> printIdList) {

        StringBuilder sb = new StringBuilder();

        /* 帳票IDを追記する */
        for (PrintId printId : printIdList) {
            if (sb.length() > 0) {
                sb.append(SEPARATOR);
            }
            sb.append(printId);
        }

        /* モードを追記する */
        if (id != null) {
            appendMode(id, sb);
        }

        logger.info(sb.toString());
    }

    /**
     * 指定したCSVファイルIDのログを出力します。
     *
     * @param id CSVファイルID
     * @param contents 出力内容
     */
    public void output(CsvId id, String... contents) {

        StringBuilder sb = new StringBuilder();

        /* CSVファイルIDを追記する */
        sb.append(id.toString());

        /* 出力内容を追記する */
        for (String content : contents) {
            sb.append(SEPARATOR).append(content);
        }

        logger.info(sb.toString());
    }

    /**
     * ログバッファにモードを追記します。
     *
     * @param id 画面ID
     * @param sb ログバッファ
     */
    private void appendMode(GamenId id, StringBuilder sb) {
        /* 個人成績・志望大学機能のみモードを出力する */
        if (id.toLogId().startsWith("C")) {
            sb.append(SEPARATOR).append(systemDto.isTeacher() ? "先生用" : "生徒用");
        }
    }

    /**
     * エラーログを出力します。
     *
     * @param message エラーメッセージ
     * @param e 例外
     */
    public void error(String message, Throwable e) {
        logger.error(message, e);
    }

}

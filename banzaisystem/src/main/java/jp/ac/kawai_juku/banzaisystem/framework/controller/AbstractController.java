package jp.ac.kawai_juku.banzaisystem.framework.controller;

import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.awt.Window;

import jp.ac.kawai_juku.banzaisystem.framework.creator.ControllerCreator;
import jp.ac.kawai_juku.banzaisystem.framework.creator.ViewCreator;
import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * コントローラクラスの基底クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class AbstractController {

    /** Viewを表示する結果オブジェクト */
    protected static final ExecuteResult VIEW_RESULT = new ViewResult();

    /** ダイアログを閉じる結果オブジェクト */
    protected static final ExecuteResult CLOSE_RESULT = new CloseResult();

    /**
     * このコントローラのID（ファンクションID＋スクリーンID）を返します。
     *
     * @return ID
     */
    public final String getId() {
        String className = BZClassUtil.getSimpleName(getClass());
        return StringUtils.removeEnd(StringUtils.uncapitalize(className), ControllerCreator.NAME_SUFFIX);
    }

    /**
     * このコントローラに対応するViewを返します。
     *
     * @return {@link AbstractView}
     */
    public final AbstractView getView() {
        return getComponent(getId() + ViewCreator.NAME_SUFFIX);
    }

    /**
     * 指定したコントローラに転送します。
     *
     * @param clazz 転送先コントローラ
     * @param methodName 転送先メソッド名
     * @param params 転送先メソッドのパラメータ
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult forward(Class<? extends AbstractController> clazz, String methodName,
            Object... params) {
        return new ControllerResult(clazz, methodName, params);
    }

    /**
     * 指定したコントローラのindexメソッドへ転送します。
     *
     * @param clazz 転送先コントローラ
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult forward(Class<? extends AbstractController> clazz) {
        return forward(clazz, null);
    }

    /**
     * 指定したURLをデフォルトブラウザで開きます。
     *
     * @param url URL
     * @return {@link ExecuteResult}
     */
    protected final ExecuteResult openUrl(String url) {
        return new OpenUrlResult(url);
    }

    /**
     * アクティブなウィンドウを返します。
     *
     * @return {@link Window}
     */
    protected final Window getActiveWindow() {
        return WindowManager.getInstance().getActiveWindow();
    }

}

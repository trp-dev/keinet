package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * 大学マスタ公表用科目を保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class PubUnivMasterSubjectBean {

    /** 入試試験区分 */
    private String entExamDiv;

    /** 大学コード枝番 */
    private String uniCdBranchCd;

    /** 受験科目コード */
    private String examSubCd;

    /** 教科コード */
    private String courseCd;

    /** 科目ビット */
    private String subBit;

    /** 科目配点 */
    private double subPnt;

    /** 科目範囲 */
    private String subArea;

    /**
     * 入試試験区分を返します。
     *
     * @return 入試試験区分
     */
    public String getEntExamDiv() {
        return entExamDiv;
    }

    /**
     * 入試試験区分をセットします。
     *
     * @param entExamDiv 入試試験区分
     */
    public void setEntExamDiv(String entExamDiv) {
        this.entExamDiv = entExamDiv;
    }

    /**
     * 大学コード枝番を返します。
     *
     * @return 大学コード枝番
     */
    public String getUniCdBranchCd() {
        return uniCdBranchCd;
    }

    /**
     * 大学コード枝番をセットします。
     *
     * @param uniCdBranchCd 大学コード枝番
     */
    public void setUniCdBranchCd(String uniCdBranchCd) {
        this.uniCdBranchCd = uniCdBranchCd;
    }

    /**
     * 受験科目コードを返します。
     *
     * @return 受験科目コード
     */
    public String getExamSubCd() {
        return examSubCd;
    }

    /**
     * 受験科目コードをセットします。
     *
     * @param examSubCd 受験科目コード
     */
    public void setExamSubCd(String examSubCd) {
        this.examSubCd = examSubCd;
    }

    /**
     * 教科コードを返します。
     *
     * @return 教科コード
     */
    public String getCourseCd() {
        return courseCd;
    }

    /**
     * 教科コードをセットします。
     *
     * @param courseCd 教科コード
     */
    public void setCourseCd(String courseCd) {
        this.courseCd = courseCd;
    }

    /**
     * 科目ビットを返します。
     *
     * @return 科目ビット
     */
    public String getSubBit() {
        return subBit;
    }

    /**
     * 科目ビットをセットします。
     *
     * @param subBit 科目ビット
     */
    public void setSubBit(String subBit) {
        this.subBit = subBit;
    }

    /**
     * 科目配点を返します。
     *
     * @return 科目配点
     */
    public double getSubPnt() {
        return subPnt;
    }

    /**
     * 科目配点をセットします。
     *
     * @param subPnt 科目配点
     */
    public void setSubPnt(double subPnt) {
        this.subPnt = subPnt;
    }

    /**
     * 科目範囲を返します。
     *
     * @return 科目範囲
     */
    public String getSubArea() {
        return subArea;
    }

    /**
     * 科目範囲をセットします。
     *
     * @param subArea 科目範囲
     */
    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

}

package jp.ac.kawai_juku.banzaisystem.mainte.impt.bean;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;

/**
 * 個人成績インポート情報を保持するBeanです。
 *
 * @author TOTEC)OZEKI.Hiroshige
 */
public class MainteImportBean extends ExamBean {

    /** インポート日時 */
    private String importtime;

    /**
     * インポート日時を返します。
     *
     * @return インポート日時
     */
    public String getImporttime() {
        return importtime;
    }

    /**
     * インポート日時をセットします。
     *
     * @param importtime インポート日時
     */
    public void setImporttime(String importtime) {
        this.importtime = importtime;
    }

}

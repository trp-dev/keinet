package jp.ac.kawai_juku.banzaisystem.mainte.mtch.view;

import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Foreground;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.mainte.mtch.component.MainteMtchCandidateTable;
import jp.ac.kawai_juku.banzaisystem.mainte.mtch.component.MainteMtchStatusTable;
import jp.ac.kawai_juku.banzaisystem.mainte.mtch.component.MainteMtchUnmatchTable;

/**
 *
 * メンテナンス-個人マッチング画面のViewです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteMtchView extends SubView {

    /** インポート済みラベル */
    @Location(x = 200, y = 36)
    public ImageLabel finishedImportLabel;

    /** 再インポートボタン */
    @Location(x = 336, y = 36)
    public ImageButton reImportButton;

    /** エクスポートボタン */
    @Location(x = 474, y = 36)
    public ImageButton exportButton;

    /** 基準ファイルアンマッチ状況テーブル */
    @Location(x = 28, y = 155)
    @Size(width = 449, height = 176)
    public MainteMtchUnmatchTable unmatchTable;

    /** 主従結合ボタン */
    @Location(x = 484, y = 216)
    public ImageButton masterSlaveUnionButton;

    /** 結合候補者テーブル */
    @Location(x = 534, y = 155)
    @Size(width = 449, height = 176)
    public MainteMtchCandidateTable candidateTable;

    /** 生徒名を…ラベル */
    @Location(x = 513, y = 397)
    @Foreground(r = 205, g = 112, b = 35)
    public TextLabel studentNameLabel;

    /** 全模試結合状況参照テーブル */
    @Location(x = 28, y = 414)
    @Size(width = 955, height = 195)
    public MainteMtchStatusTable statusTable;

}

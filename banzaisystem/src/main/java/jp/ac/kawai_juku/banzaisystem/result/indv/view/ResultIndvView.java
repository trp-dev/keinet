package jp.ac.kawai_juku.banzaisystem.result.indv.view;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBox;
import jp.ac.kawai_juku.banzaisystem.framework.component.ImageButton;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextField;
import jp.ac.kawai_juku.banzaisystem.framework.component.TextLabel;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.FireActionEvent;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Location;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.ReadOnly;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Size;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.result.indv.component.ResultIndvTable;

/**
 *
 * 入試結果入力-個人指定画面画面のViewです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultIndvView extends SubView {

    /** コンボボックス_学年 */
    @Location(x = 24, y = 43)
    @Size(width = 42, height = 20)
    @FireActionEvent
    public ComboBox<Integer> gradeComboBox;

    /** コンボボックス_クラス */
    @Location(x = 119, y = 43)
    @Size(width = 47, height = 20)
    @FireActionEvent
    public ComboBox<String> classComboBox;

    /** コンボボックス_番号 */
    @Location(x = 172, y = 43)
    @Size(width = 65, height = 20)
    @FireActionEvent
    public ComboBox<ExmntnMainKojinBean> numberComboBox;

    /** 氏名テキスト  */
    @Location(x = 242, y = 43)
    @Size(width = 139, height = 20)
    @ReadOnly
    public TextField nameField;

    /** 生徒一覧ボタン */
    @Location(x = 385, y = 41)
    public ImageButton studentListButton;

    /** 前の生徒ボタン */
    @Location(x = 470, y = 45)
    public ImageButton beforeStudentButton;

    /** 次の生徒ボタン */
    @Location(x = 530, y = 45)
    public ImageButton nextStudentButton;

    /** 説明文 */
    @Location(x = 745, y = 14)
    public TextLabel noteLabel;

    /** 一覧テーブル */
    @Location(x = 16, y = 107)
    @Size(width = 717, height = 528)
    public ResultIndvTable listTable;

    /** 大学追加ボタン */
    @Location(x = 749, y = 125)
    public ImageButton addUnivButton;

    /** 大学削除ボタン */
    @Location(x = 749, y = 159)
    public ImageButton deleteUnivButton;

    /** CSV出力ボタン */
    @Location(x = 874, y = 564)
    public ImageButton outputCsvButton;

    /** 保存ボタン */
    @Location(x = 875, y = 602)
    public ImageButton saveButton;

}

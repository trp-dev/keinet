package jp.ac.kawai_juku.banzaisystem.framework.controller;

import jp.ac.kawai_juku.banzaisystem.framework.util.BZClassUtil;
import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;
import jp.ac.kawai_juku.banzaisystem.framework.view.SubView;
import jp.ac.kawai_juku.banzaisystem.framework.view.annotation.Dialog;
import jp.ac.kawai_juku.banzaisystem.framework.window.WindowManager;

/**
 *
 * Viewを表示するアクション結果です。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ViewResult implements ExecuteResult {

    /**
     * コンストラクタです。
     */
    public ViewResult() {
    }

    @Override
    public void process(AbstractController controller) {
        AbstractView view = controller.getView();
        if (view instanceof SubView) {
            throw new RuntimeException(
                    String.format("サブビュー[%s]は直接表示できません。", BZClassUtil.getSimpleName(view.getClass())));
        } else {
            Class<?> clazz = BZClassUtil.getOriginalClass(view.getClass());
            WindowManager manager = WindowManager.getInstance();
            if (clazz.isAnnotationPresent(Dialog.class)) {
                manager.displayDialog(view.getView());
            } else {
                manager.displayFrame(view.getView());
            }
        }
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.controller;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.controller.ExmntnMainController;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.mainte.main.controller.MainteMainController;
import jp.ac.kawai_juku.banzaisystem.output.main.controller.OutputMainController;
import jp.ac.kawai_juku.banzaisystem.result.main.controller.ResultMainController;
import jp.ac.kawai_juku.banzaisystem.selstd.main.controller.SelstdMainController;

/**
 *
 * 画面上部に「対象生徒選択」等のボタンメニューがある画面の抽象コントローラです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public abstract class MenuController extends ExitController {

    /**
     * 対象生徒選択ボタン押下アクションです。
     *
     * @return 対象生徒選択画面
     */
    @Execute
    public ExecuteResult selectStudentButtonAction() {
        return forward(SelstdMainController.class);
    }

    /**
     * 個人成績・志望大学ボタン押下アクションです。
     *
     * @return 個人成績画面
     */
    @Execute
    public ExecuteResult exmntnButtonAction() {
        return forward(ExmntnMainController.class);
    }

    /**
     * 帳票出力・CSVボタン押下アクションです。
     *
     * @return 帳票出力・CSV画面
     */
    @Execute
    public ExecuteResult reportButtonAction() {
        return forward(OutputMainController.class);
    }

    /**
     * メンテナンスボタン押下アクションです。
     *
     * @return メンテナンス画面
     */
    @Execute
    public ExecuteResult maintenanceButtonAction() {
        return forward(MainteMainController.class);
    }

    /**
     * インフォメーションボタン押下アクションです。
     *
     * @return メンテナンス画面
     */
    @Execute
    public ExecuteResult infoButtonAction() {
        ExecuteResult result = openUrl("https://navi.keinet.ne.jp/public/info/");
        return result;
    }

    /**
     * 入試結果入力ボタン押下アクションです。
     *
     * @return 入試結果入力画面
     */
    @Execute
    public ExecuteResult resultButtonAction() {
        return forward(ResultMainController.class);
    }

}

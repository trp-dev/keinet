package jp.ac.kawai_juku.banzaisystem.result.indv.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.bean.ExmntnScdlBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.scdl.service.ExmntnScdlService;
import jp.ac.kawai_juku.banzaisystem.framework.component.ComboBoxItem;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.component.table.CellData;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.SubController;
import jp.ac.kawai_juku.banzaisystem.result.dunv.controller.ResultDunvController;
import jp.ac.kawai_juku.banzaisystem.result.indv.view.ResultIndvView;
import jp.ac.kawai_juku.banzaisystem.selstd.main.service.SelstdMainService;

import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * 入試結果入力-個人指定画面のコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultIndvController extends SubController {

    /** View */
    @Resource(name = "resultIndvView")
    private ResultIndvView view;

    /** 対象生徒選択画面のサービス */
    @Resource
    private SelstdMainService selstdMainService;

    /** 受験スケジュール画面のサービス */
    @Resource
    private ExmntnScdlService exmntnScdlService;

    @Override
    public void activatedAction() {
        /* 学年コンボボックスを初期化する */
        initGradeComboBox();
    }

    /**
     * 学年コンボボックスを初期化します。
     */
    private void initGradeComboBox() {
        List<ComboBoxItem<Integer>> itemList = CollectionsUtil.newArrayList();
        for (Integer grade : selstdMainService.findGradeList()) {
            itemList.add(new ComboBoxItem<Integer>(grade));
        }
        view.gradeComboBox.setItemList(itemList);
    }

    /**
     * 学年コンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult gradeComboBoxAction() {
        initClassComboBox();
        return null;
    }

    /**
     * クラスコンボボックスを初期化します。
     */
    private void initClassComboBox() {
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年が存在しない場合 */
            view.classComboBox.setItemList(Collections.<ComboBoxItem<String>> emptyList());
        } else {
            /* 学年が存在する場合 */
            List<ComboBoxItem<String>> itemList = CollectionsUtil.newArrayList();
            for (String cls : selstdMainService.findClassList(grade.getValue())) {
                itemList.add(new ComboBoxItem<String>(cls));
            }
            view.classComboBox.setItemList(itemList);
        }
    }

    /**
     * クラスコンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult classComboBoxAction() {
        initNumberComboBox();
        return null;
    }

    /**
     * クラス番号コンボボックスを初期化します。
     */
    private void initNumberComboBox() {
        ComboBoxItem<Integer> grade = view.gradeComboBox.getSelectedItem();
        if (grade == null) {
            /* 学年が存在しない場合 */
            view.numberComboBox.setItemList(Collections.<ComboBoxItem<ExmntnMainKojinBean>> emptyList());
        } else {
            /* 学年が存在する場合 */
            List<ComboBoxItem<ExmntnMainKojinBean>> itemList = CollectionsUtil.newArrayList();
            for (Map<String, CellData> map : selstdMainService.findKojinList(grade.getValue(), view.classComboBox.getSelectedValue())) {
                ExmntnMainKojinBean bean = new ExmntnMainKojinBean();
                bean.setIndividualId((Integer) map.get("id").getSortValue());
                bean.setGrade((Integer) map.get("grade").getSortValue());
                bean.setCls((String) map.get("class").getSortValue());
                bean.setClassNo((String) map.get("number").getSortValue());
                bean.setNameKana((String) map.get("name").getSortValue());
                itemList.add(new ComboBoxItem<ExmntnMainKojinBean>(bean.getClassNo(), bean));
            }
            view.numberComboBox.setItemList(itemList);
        }
    }

    /**
     * クラス番号コンボボックス変更アクションです。
     *
     * @return なし（画面遷移しない）
     */
    @Execute
    public ExecuteResult numberComboBoxAction() {

        ExmntnMainKojinBean studentBean = view.numberComboBox.getSelectedValue();
        if (studentBean != null) {
            view.nameField.setText(studentBean.getNameKana());

            /*
             * テーブルの動作確認用に、受験スケジュール画面のサービスを呼び出している。
             * 本来は、この画面のサービスクラスを用意して入試形態・結果・入学を合わせたデータを取得すべき。
             */
            List<ExmntnScdlBean> list = exmntnScdlService.findTeachderExamPlanUnivList(studentBean.getIndividualId());

            Vector<Vector<Object>> dataList = new Vector<>(list.size());
            for (ExmntnScdlBean bean : list) {
                Vector<Object> row = CollectionsUtil.newVector();
                row.add(bean);
                row.add(bean.getCandidateRank());
                row.add(bean.getUnivCd());
                row.add(bean.getUnivName());
                row.add(bean.getFacultyName());
                row.add(bean.getDeptName());
                row.add("1");
                row.add("1");
                row.add(Boolean.FALSE);
                dataList.add(row);
            }
            view.listTable.setDataList(dataList);
        } else {
            view.nameField.setText(null);
            view.listTable.setDataList(new Vector<Vector<Object>>());
        }

        return null;
    }

    /**
     * 大学追加ボタン押下アクションです。
     *
     * @return 大学追加ダイアログ
     */
    @Execute
    public ExecuteResult addUnivButtonAction() {
        return forward(ResultDunvController.class);
    }

}

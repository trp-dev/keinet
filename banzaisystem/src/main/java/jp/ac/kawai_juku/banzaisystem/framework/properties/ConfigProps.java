package jp.ac.kawai_juku.banzaisystem.framework.properties;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import jp.ac.kawai_juku.banzaisystem.BZConstants;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;

/**
 *
 * 環境設定ファイルの操作クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ConfigProps extends BZProperties<ConfigKey> {

    /** プロパティファイル操作クラス */
    private static PropertiesIO io;

    /**
     * コンストラクタです。
     *
     * @param props {@link Properties}
     */
    private ConfigProps(Properties props) {
        super(props);
    }

    /**
     * 環境設定ファイルを読み込みます。
     *
     * @return {@link ConfigProps}
     */
    public static ConfigProps load() {
        /* TODO 必要になったらロード処理を呼び出す
        try {
            return new ConfigProps(getPropertiesIO().load());
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E006"), e);
        }
        */
        return new ConfigProps(new Properties());
    }

    /**
     * 環境設定ファイルを保存します。
     */
    public void store() {
        try {
            getPropertiesIO().store(getProperties());
        } catch (IOException e) {
            throw new MessageDialogException(getMessage("error.framework.CM_E006"), e);
        }
    }

    /**
     * プロパティファイル操作クラスを返します。
     *
     * @return {@link PropertiesIO}
     */
    private static PropertiesIO getPropertiesIO() {
        /*
         * PropertiesIOをstatic定数で宣言してしまうと、S2コンテナ初期化時に
         * 設定ファイルが存在しない場合にエラーになるため、
         * 必要になったタイミングで初期化する。
         */
        File propsFile = new File(SettingPropsUtil.getConfigDir(), BZConstants.CONFIG_FILE_NAME);
        if (io == null || !propsFile.equals(io.getPropsFile())) {
            io = new PropertiesIO(propsFile);
        }
        return io;
    }

}

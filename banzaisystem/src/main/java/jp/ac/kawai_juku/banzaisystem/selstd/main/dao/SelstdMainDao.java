package jp.ac.kawai_juku.banzaisystem.selstd.main.dao;

import java.util.List;

import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainCandidateUnivBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.main.bean.SelstdMainScoreBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Score;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 対象生徒選択画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class SelstdMainDao extends BaseDao {

    /**
     * 学年リストを取得します。
     *
     * @param year 年度
     * @return 学年リスト
     */
    public List<Integer> selectGradeList(String year) {
        return jdbcManager.selectBySqlFile(Integer.class, getSqlPath(), year).getResultList();
    }

    /**
     * クラスリストを取得します。
     *
     * @param year 年度
     * @param grade 学年
     * @return クラスリスト
     */
    public List<String> selectClassList(String year, Integer grade) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("grade", grade);
        return jdbcManager.selectBySqlFile(String.class, getSqlPath(), param).getResultList();
    }

    /**
     * 個人リストを取得します。
     *
     * @param year 年度
     * @param grade 学年
     * @param cls クラス
     * @return 個人リスト
     */
    public List<SelstdMainKojinBean> selectKojinList(String year, Integer grade, String cls) {
        BeanMap param = new BeanMap();
        param.put("year", year);
        param.put("grade", grade);
        param.put("class", cls);
        return jdbcManager.selectBySqlFile(SelstdMainKojinBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 成績リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 個人リスト
     */
    public List<SelstdMainScoreBean> selectScoreList(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("jpnDesc", Score.SUBCODE_CENTER_DEFAULT_JKIJUTSU);
        return jdbcManager.selectBySqlFile(SelstdMainScoreBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 志望大学リストを取得します。
     *
     * @param grade 学年
     * @param cls クラス
     * @param exam 対象模試
     * @return 志望大学リスト
     */
    public List<SelstdMainCandidateUnivBean> selectCandidateUnivList(Integer grade, String cls, ExamBean exam) {
        BeanMap param = new BeanMap();
        param.put("grade", grade);
        param.put("class", cls);
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        return jdbcManager.selectBySqlFile(SelstdMainCandidateUnivBean.class, getSqlPath(), param).getResultList();
    }

    /**
     * 受験予定大学を削除します。
     *
     * @param param パラメータ
     */
    public void deleteExamPlanUniv(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 志望大学を削除します。
     *
     * @param param パラメータ
     */
    public void deleteCandidateUniv(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 模試記入志望大学データを削除します。
     *
     * @param param パラメータ
     */
    public void deleteCandidateRating(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 個表データを削除します。
     *
     * @param param パラメータ
     */
    public void deleteIndividualRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 成績データを削除します。
     *
     * @param param パラメータ
     */
    public void deleteSubRecord(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 学籍履歴情報を削除します。
     *
     * @param param パラメータ
     */
    public void deleteHistoryInfo(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

    /**
     * 学籍基本情報を削除します。
     *
     * @param param パラメータ
     */
    public void deleteBasicInfo(BeanMap param) {
        jdbcManager.updateBySqlFile(getSqlPath(), param).execute();
    }

}

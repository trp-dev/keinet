package jp.ac.kawai_juku.banzaisystem.exmntn.dlcs.bean;

/**
 *
 * 取得可能資格ダイアログのBeanです。
 *
 *
 * <br />説明：取得可能資格ダイアログ上に表示する資格一覧の1レコード分の情報です。
 * <br />用途：検索結果の格納
 * <br />機能概要：指定の大学-学部-学科に紐付く取得可能な資格の検索結果を1資格分保持します。
 * <br />関連情報：詳細設計書（個人成績・志望大学）.xls -- SQL_C1_004
 *
 * @author TOTEC)OOMURA.Masafumi
 *
 */
public class ExmntnDlcsQualifiableCertificationOutBean {

    /** 資格コード */
    private String certCd;

    /** 資格名 */
    private String certName;

    /**
     * 資格コードを返します。
     *
     * @return 資格コード
     */
    public String getCertCd() {
        return certCd;
    }

    /**
     * 資格コードをセットします。
     *
     * @param certCd 資格コード
     */
    public void setCertCd(String certCd) {
        this.certCd = certCd;
    }

    /**
     * 資格名を返します。
     *
     * @return 資格名
     */
    public String getCertName() {
        return certName;
    }

    /**
     * 資格名をセットします。
     *
     * @param certName 資格名
     */
    public void setCertName(String certName) {
        this.certName = certName;
    }

}

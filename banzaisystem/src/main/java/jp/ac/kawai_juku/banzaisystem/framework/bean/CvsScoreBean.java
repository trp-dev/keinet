package jp.ac.kawai_juku.banzaisystem.framework.bean;

/**
 *
 * センター得点換算表のデータを保持するBeanです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CvsScoreBean {

    /** 模試コード */
    private String examCd;

    /** 科目コード */
    private String subCd;

    /** 素点 */
    private Integer rawScore;

    /** 換算得点 */
    private Integer cvsScore;

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamCd() {
        return examCd;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examCd 模試コード
     */
    public void setExamCd(String examCd) {
        this.examCd = examCd;
    }

    /**
     * 素点を返します。
     *
     * @return 素点
     */
    public Integer getRawScore() {
        return rawScore;
    }

    /**
     * 素点をセットします。
     *
     * @param rawScore 素点
     */
    public void setRawScore(Integer rawScore) {
        this.rawScore = rawScore;
    }

    /**
     * 換算得点を返します。
     *
     * @return 換算得点
     */
    public Integer getCvsScore() {
        return cvsScore;
    }

    /**
     * 換算得点をセットします。
     *
     * @param cvsScore 換算得点
     */
    public void setCvsScore(Integer cvsScore) {
        this.cvsScore = cvsScore;
    }

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

}

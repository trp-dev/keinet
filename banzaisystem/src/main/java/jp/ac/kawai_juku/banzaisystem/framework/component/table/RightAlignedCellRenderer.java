package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import javax.swing.JLabel;

/**
 *
 * セルの文字列を右寄せにするTableCellRendererです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@SuppressWarnings("serial")
public class RightAlignedCellRenderer extends AlignedCellRenderer {

    @Override
    int getAlignment() {
        return JLabel.RIGHT;
    }

}

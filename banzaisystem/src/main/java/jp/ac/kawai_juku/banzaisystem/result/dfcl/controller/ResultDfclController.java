package jp.ac.kawai_juku.banzaisystem.result.dfcl.controller;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.bean.LabelValueBean;
import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.result.dfcl.view.ResultDfclView;
import jp.ac.kawai_juku.banzaisystem.result.dunv.service.ResultDunvService;
import jp.ac.kawai_juku.banzaisystem.result.univ.bean.ResultUnivExamineeBean;

/**
 *
 * 学部学科選択ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ResultDfclController extends AbstractController {

    /** View */
    @Resource(name = "resultDfclView")
    private ResultDfclView view;

    /** 大学追加ダイアログのサービス */
    @Resource
    private ResultDunvService resultDunvService;

    /** 表示中の情報 */
    private ResultUnivExamineeBean examineeBean;

    /**
     * 初期表示アクションです。
     *
     * @param examineeBean {@link ResultUnivExamineeBean}
     * @return 学部学科選択ダイアログ
     */
    @Execute
    public ExecuteResult index(ResultUnivExamineeBean examineeBean) {
        view.univNameLabel.setText(examineeBean.getUnivName());
        view.facultyList.setList(resultDunvService.findFacultyList(examineeBean.getUnivCd()));
        this.examineeBean = examineeBean;
        return VIEW_RESULT;
    }

    /**
     * 学部リストアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult facultyListAction() {
        List<LabelValueBean> facultyList = view.facultyList.getSelectedList();
        if (facultyList.isEmpty()) {
            view.deptList.setList(Collections.<LabelValueBean> emptyList());
        } else {
            view.deptList.setList(resultDunvService.findDeptList(examineeBean.getUnivCd(), facultyList.get(0).getValue()));
        }
        return null;
    }

    /**
     * 学科リストアクションです。
     *
     * @return なし(画面遷移しない)
     */
    @Execute
    public ExecuteResult deptListAction() {
        view.okButton.setEnabled(!view.deptList.getSelectedList().isEmpty());
        return null;
    }

    /**
     * OKボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {
        return CLOSE_RESULT;
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        return CLOSE_RESULT;
    }

}

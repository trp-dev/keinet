package jp.ac.kawai_juku.banzaisystem.framework.component.table;

import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Univ;

/**
 *
 * 二次ランクセルデータを保持するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class RankCellData extends CellData {

    /**
     * コンストラクタです。
     *
     * @param rating 評価値
     */
    public RankCellData(String rating) {
        super(rating, Univ.RANK_STR_NONE.equals(rating) ? "00.0" : rating);
    }

}

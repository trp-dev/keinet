package jp.ac.kawai_juku.banzaisystem.exmntn.input.helper;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.exmntn.input.view.ExmntnInputView;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainCefrRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.bean.ExmntnMainJpnRecordBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.dto.ExmntnMainDto;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.helper.ExmntnMainScoreHelper;
import jp.ac.kawai_juku.banzaisystem.exmntn.main.view.ExmntnMainView;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;

/**
 *
 * CEFR、国語記述入力画面のヘルパーです。
 *
 *
 * @author QQ)Kurimoto
 *
 */
public class ExmntnInputHelper {

    /**
     * 認定試験スイッチ
     *
     */
    public enum SWITCH {
        /**
         * 認定試験１
         */
        ENG1,

        /**
         * 認定試験２
         */
        ENG2;
    }

    /** View */
    @Resource(name = "exmntnMainView")
    private ExmntnMainView mainview;

    /** 成績ヘルパー */
    @Resource(name = "exmntnMainScoreHelper")
    private ExmntnMainScoreHelper scoreHelper;

    /** 変更イベント処理中フラグ */
    /*private boolean changingFlg;*/

    /** システム情報DTO */
    @Resource
    private SystemDto systemDto;

    /** 志望大学個人成績画面のDTO */
    @Resource(name = "exmntnMainDto")
    private ExmntnMainDto mainDto;

    /** View */
    @Resource(name = "exmntnInputView")
    private ExmntnInputView view;

    /* 記述式総合評価マスタ */
    /* private List<JpnDescTotalEvaluationBean> jpnDescTotalEval; */

    /* 国語記述問１評価 */
    /* private List<String> jpnDescQ1; */
    /* 国語記述問２評価 */
    /* private List<String> jpnDescQ2; */
    /* 国語記述問３評価 */
    /* private List<String> jpnDescQ3; */

    /**
     * コントロールの初期化を行います。
     *
     */
    public void initControls() {

        if (systemDto.getCefrInfo() == null) {
            ExmntnMainCefrRecordBean cefrdata = new ExmntnMainCefrRecordBean();
            List<ExmntnMainCefrRecordBean> cefrInfo = new ArrayList<ExmntnMainCefrRecordBean>();
            cefrInfo.add(cefrdata);
            systemDto.setCefrInfo(cefrInfo);
        }

        if (systemDto.getJpnInfo() == null) {
            ExmntnMainJpnRecordBean jpndata = new ExmntnMainJpnRecordBean();
            List<ExmntnMainJpnRecordBean> jpnInfo = new ArrayList<ExmntnMainJpnRecordBean>();
            jpnInfo.add(jpndata);
            systemDto.setJpnInfo(jpnInfo);
        }

        /* 認定試験名１〜２のアイテムリストを設定する */
        /*setCombobox(view.joiningExam1ComboBox, systemDto.getEngPtList());
        setCombobox(view.joiningExam2ComboBox, systemDto.getEngPtList());*/

        /* 認定試験レベル１〜２のアイテムリストを設定する */
        /*setCombobox(view.joiningExamLevel1ComboBox, filterJoininExamDetailList(SWITCH.ENG1));
        setCombobox(view.joiningExamLevel2ComboBox, filterJoininExamDetailList(SWITCH.ENG2));*/

        /* スコアのフォーカスロストイベントを設定する */
        /*addListenerCefrScore(view.cefrScore1Field, SWITCH.ENG1);
        addListenerCefrScore(view.cefrScore2Field, SWITCH.ENG2);*/

        /* CEFRレベル１〜２のアイテムリストを設定する */
        /*setCombobox(view.cefrLevel1ComboBox, systemDto.getCefrList());
        setCombobox(view.cefrLevel2ComboBox, systemDto.getCefrList());*/

        /* 英検合否１〜２のアイテムリストを設定する */
        /*setCombobox(view.engExamResult1ComboBox, Constants.ENG_EXAM_RESULT_LIST);
        setCombobox(view.engExamResult2ComboBox, Constants.ENG_EXAM_RESULT_LIST);*/

        /* 国語記述入力1〜3のアイテムリストを設定する */
        /*
        jpnDescQ1 = new ArrayList<String>();
        jpnDescQ2 = new ArrayList<String>();
        jpnDescQ3 = new ArrayList<String>();
        this.jpnDescTotalEval = systemDto.getJpnWritingTotalEvaluationList();
        
        jpnDescQ1.add("");
        jpnDescQ2.add("");
        jpnDescQ3.add("");
        */
        /* 問１評価のユニーク値を取得 */
        /*
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ1Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ1.add(item);
        }
        */
        /* 問２評価のユニーク値を取得 */
        /*
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ2Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ2.add(item);
        }
        */
        /* 問３評価のユニーク値を取得 */
        /*
        for (String item : jpnDescTotalEval.stream().map(i -> i.getQ3Evaluation()).distinct().sorted().collect(Collectors.toList())) {
            jpnDescQ3.add(item);
        }
        
        setCombobox(view.jpnWriting1ComboBox, jpnDescQ1.toArray(new String[jpnDescQ1.size()]));
        setCombobox(view.jpnWriting2ComboBox, jpnDescQ2.toArray(new String[jpnDescQ2.size()]));
        setCombobox(view.jpnWriting3ComboBox, jpnDescQ3.toArray(new String[jpnDescQ3.size()]));
        */

        /* エラーメッセージを初期化 */
        /*initErrorMessageLabel(view.errorMessageCefr1Label);
        initErrorMessageLabel(view.errorMessageCefr2Label);*/
        /*initErrorMessageLabel(view.errorMessageJpnLabel);*/

        /* データ設定 */
        /*List<ExmntnMainCefrRecordBean> cefrList = systemDto.getCefrInfo();*/
        /*List<ExmntnMainJpnRecordBean> jpnList = systemDto.getJpnInfo();*/

        /* 英語認定試験１ */
        /*setEngDispData(ExmntnInputHelper.SWITCH.ENG1, null);*/
        /* 認定試験名 */
        /*ComboBox<EngPtBean> exam1 = getEngPtField(ExmntnInputHelper.SWITCH.ENG1);
        for (int i = 1; i < exam1.getItemCount(); i++) {
            if (exam1.getItemAt(i).getValue().getEngPtCd().equals(cefrList.get(0).getCefrExamCd1())) {
                exam1.setSelectedIndex(i);
                break;
            }
        }*/

        /* 認定試験レベル */
        /*ComboBox<EngPtDetailBean> detail1 = getEngPtLevelField(ExmntnInputHelper.SWITCH.ENG1);
        for (int i = 1; i < detail1.getItemCount(); i++) {
            if (detail1.getItemAt(i).getValue().getEngPtLevelCd().equals(cefrList.get(0).getCefrExamLevel1())) {
                detail1.setSelectedIndex(i);
                break;
            }
        }*/

        /* スコア不明 */
        /*CodeCheckBox unknownScore1CheckBox = getUnknownCefrScoreField(ExmntnInputHelper.SWITCH.ENG1);
        if (Constants.FLG_ON.equals(cefrList.get(0).getUnknownScore1())) {
            unknownScore1CheckBox.setSelected(true);
        } else {
            unknownScore1CheckBox.setSelected(false);
        }*/

        /* CEFR不明 */
        /*CodeCheckBox unknownCefr1CheckBox = getUnknownCefrLevelField(ExmntnInputHelper.SWITCH.ENG1);
        if (Constants.FLG_ON.equals(cefrList.get(0).getUnknownCefr1())) {
            unknownCefr1CheckBox.setSelected(true);
        } else {
            unknownCefr1CheckBox.setSelected(false);
        }*/

        /* 英検合否コンボボックス */
        /*ComboBox<EngExamResultBean> engExamResult1 = getEngExamResultFiled(ExmntnInputHelper.SWITCH.ENG1);
        for (int i = 1; i < engExamResult1.getItemCount(); i++) {
            if (engExamResult1.getItemAt(i).getValue().getFlg().equals(cefrList.get(0).getExamResult1())) {
                engExamResult1.setSelectedIndex(i);
                break;
            }
        }*/

        /* CEFRレベルコンボボックス */
        /*ComboBox<CefrBean> cefrLevel1 = getCefrLevelField(ExmntnInputHelper.SWITCH.ENG1);
        for (int i = 1; i < cefrLevel1.getItemCount(); i++) {
            if (cefrLevel1.getItemAt(i).getValue().getCefrCd().equals(cefrList.get(0).getCefrLevelCd1())) {
                cefrLevel1.setSelectedIndex(i);
                break;
            }
        }*/

        /* スコア */
        /*TextField cefrScore1 = getCefrScoreField(ExmntnInputHelper.SWITCH.ENG1);
        if (cefrList.get(0).getCefrScore1() != null && cefrList.get(0).getCefrScore1() != 0) {
             0以外の場合設定する
            cefrScore1.setText(getStringScore(cefrList.get(0).getCefrScore1(), ExmntnInputHelper.SWITCH.ENG1));
        } else {
            cefrScore1.setText("");
        }*/

        /*systemDto.getCefrInfo().get(0).setDispCefrExamCd1(cefrList.get(0).getCefrExamCd1());
        systemDto.getCefrInfo().get(0).setDispCefrExamName1(cefrList.get(0).getCefrExamName1());
        systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr1(cefrList.get(0).getCefrExamNameAbbr1());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevel1(cefrList.get(0).getCefrExamLevel1());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevelName1(cefrList.get(0).getCefrExamLevelName1());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr1(cefrList.get(0).getCefrExamLevelNameAbbr1());
        systemDto.getCefrInfo().get(0).setDispUnknownScore1(cefrList.get(0).getUnknownScore1());
        systemDto.getCefrInfo().get(0).setDispUnknownCefr1(cefrList.get(0).getUnknownCefr1());
        systemDto.getCefrInfo().get(0).setDispExamResult1(cefrList.get(0).getExamResult1());
        systemDto.getCefrInfo().get(0).setDispCefrLevelCd1(cefrList.get(0).getCefrLevelCd1());
        systemDto.getCefrInfo().get(0).setDispCefrLevel1(cefrList.get(0).getCefrLevel1());
        systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr1(cefrList.get(0).getCefrLevelAbbr1());
        systemDto.getCefrInfo().get(0).setDispCefrScore1(cefrList.get(0).getCefrScore1());
        if ("".equals(cefrList.get(0).getScoreCorrectFlg1())) {
            systemDto.getCefrInfo().get(0).setScoreCorrectFlg1(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg1(cefrList.get(0).getScoreCorrectFlg1());
        if ("".equals(cefrList.get(0).getCefrLevelCdCorrectFlg1())) {
            systemDto.getCefrInfo().get(0).setCefrLevelCdCorrectFlg1(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg1(cefrList.get(0).getCefrLevelCdCorrectFlg1());
        if ("".equals(cefrList.get(0).getExamResultCorrectFlg1())) {
            systemDto.getCefrInfo().get(0).setExamResultCorrectFlg1(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg1(cefrList.get(0).getExamResultCorrectFlg1());*/

        /*if (exam1.getSelectedIndex() == 0) {
             認定試験未選択の場合は、各項目を非活性にする
            detail1.setEnabled(false);
            unknownScore1CheckBox.setEnabled(false);
            unknownCefr1CheckBox.setEnabled(false);
            engExamResult1.setEnabled(false);
            cefrLevel1.setEnabled(false);
            cefrScore1.setEnabled(false);
        }*/

        /* 英語認定試験２ */
        /*setEngDispData(ExmntnInputHelper.SWITCH.ENG2, null);*/
        /* 認定試験名 */
        /*ComboBox<EngPtBean> exam2 = getEngPtField(ExmntnInputHelper.SWITCH.ENG2);
        for (int i = 1; i < exam2.getItemCount(); i++) {
            if (exam2.getItemAt(i).getValue().getEngPtCd().equals(cefrList.get(0).getCefrExamCd2())) {
                exam2.setSelectedIndex(i);
                break;
            }
        }*/

        /* 認定試験レベル */
        /*ComboBox<EngPtDetailBean> detail2 = getEngPtLevelField(ExmntnInputHelper.SWITCH.ENG2);
        for (int i = 1; i < detail2.getItemCount(); i++) {
            if (detail2.getItemAt(i).getValue().getEngPtLevelCd().equals(cefrList.get(0).getCefrExamLevel2())) {
                detail2.setSelectedIndex(i);
                break;
            }
        }*/

        /* スコア不明 */
        /*CodeCheckBox unknownScore2CheckBox = getUnknownCefrScoreField(ExmntnInputHelper.SWITCH.ENG2);
        if (Constants.FLG_ON.equals(cefrList.get(0).getUnknownScore2())) {
            unknownScore2CheckBox.setSelected(true);
        } else {
            unknownScore2CheckBox.setSelected(false);
        }*/

        /* CEFR不明 */
        /*CodeCheckBox unknownCefr2CheckBox = getUnknownCefrLevelField(ExmntnInputHelper.SWITCH.ENG2);
        if (Constants.FLG_ON.equals(cefrList.get(0).getUnknownCefr2())) {
            unknownCefr2CheckBox.setSelected(true);
        } else {
            unknownCefr2CheckBox.setSelected(false);
        }*/

        /* 英検合否コンボボックス */
        /*ComboBox<EngExamResultBean> engExamResult2 = getEngExamResultFiled(ExmntnInputHelper.SWITCH.ENG2);
        for (int i = 1; i < engExamResult2.getItemCount(); i++) {
            if (engExamResult2.getItemAt(i).getValue().getFlg().equals(cefrList.get(0).getExamResult2())) {
                engExamResult2.setSelectedIndex(i);
                break;
            }
        }*/

        /* CEFRレベルコンボボックス */
        /*ComboBox<CefrBean> cefrLevel2 = getCefrLevelField(ExmntnInputHelper.SWITCH.ENG2);
        for (int i = 1; i < cefrLevel2.getItemCount(); i++) {
            if (cefrLevel2.getItemAt(i).getValue().getCefrCd().equals(cefrList.get(0).getCefrLevelCd2())) {
                cefrLevel2.setSelectedIndex(i);
                break;
            }
        }*/

        /* スコア */
        /*TextField cefrScore2 = getCefrScoreField(ExmntnInputHelper.SWITCH.ENG2);
        if (cefrList.get(0).getCefrScore2() != null && cefrList.get(0).getCefrScore2() != 0) {
             0以外の場合設定する
            cefrScore2.setText(getStringScore(cefrList.get(0).getCefrScore2(), ExmntnInputHelper.SWITCH.ENG2));
        } else {
            cefrScore2.setText("");
        }*/

        /*systemDto.getCefrInfo().get(0).setDispCefrExamCd2(cefrList.get(0).getCefrExamCd2());
        systemDto.getCefrInfo().get(0).setDispCefrExamName2(cefrList.get(0).getCefrExamName2());
        systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr2(cefrList.get(0).getCefrExamNameAbbr2());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevel2(cefrList.get(0).getCefrExamLevel2());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevelName2(cefrList.get(0).getCefrExamLevelName2());
        systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr2(cefrList.get(0).getCefrExamLevelNameAbbr2());
        systemDto.getCefrInfo().get(0).setDispUnknownScore2(cefrList.get(0).getUnknownScore2());
        systemDto.getCefrInfo().get(0).setDispUnknownCefr2(cefrList.get(0).getUnknownCefr2());
        systemDto.getCefrInfo().get(0).setDispExamResult2(cefrList.get(0).getExamResult2());
        systemDto.getCefrInfo().get(0).setDispCefrLevelCd2(cefrList.get(0).getCefrLevelCd2());
        systemDto.getCefrInfo().get(0).setDispCefrLevel2(cefrList.get(0).getCefrLevel2());
        systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr2(cefrList.get(0).getCefrLevelAbbr2());
        systemDto.getCefrInfo().get(0).setDispCefrScore2(cefrList.get(0).getCefrScore2());
        if ("".equals(cefrList.get(0).getScoreCorrectFlg2())) {
            systemDto.getCefrInfo().get(0).setScoreCorrectFlg2(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg2(cefrList.get(0).getScoreCorrectFlg2());
        if ("".equals(cefrList.get(0).getCefrLevelCdCorrectFlg2())) {
            systemDto.getCefrInfo().get(0).setCefrLevelCdCorrectFlg2(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg2(cefrList.get(0).getCefrLevelCdCorrectFlg2());
        if ("".equals(cefrList.get(0).getExamResultCorrectFlg2())) {
            systemDto.getCefrInfo().get(0).setExamResultCorrectFlg2(Constants.FLG_OFF);
        }
        systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg2(cefrList.get(0).getExamResultCorrectFlg2());
        
        if (exam2.getSelectedIndex() == 0) {
             認定試験未選択の場合は、各項目を非活性にする
            detail2.setEnabled(false);
            unknownScore2CheckBox.setEnabled(false);
            unknownCefr2CheckBox.setEnabled(false);
            engExamResult2.setEnabled(false);
            cefrLevel2.setEnabled(false);
            cefrScore2.setEnabled(false);
        }*/

        /* 国語記述 */
        /* 問１ */
        /*
        ComboBox<String> jpn1 = view.jpnWriting1ComboBox;
        for (int i = 1; i < jpn1.getItemCount(); i++) {
            if (jpn1.getItemAt(i).getValue().equals(jpnList.get(0).getQuestion1())) {
                jpn1.setSelectedIndex(i);
                break;
            }
        }
        */
        /* 問２ */
        /*
        ComboBox<String> jpn2 = view.jpnWriting2ComboBox;
        for (int i = 1; i < jpn2.getItemCount(); i++) {
            if (jpn2.getItemAt(i).getValue().equals(jpnList.get(0).getQuestion2())) {
                jpn2.setSelectedIndex(i);
                break;
            }
        }
        */
        /* 問３ */
        /*
        ComboBox<String> jpn3 = view.jpnWriting3ComboBox;
        for (int i = 1; i < jpn3.getItemCount(); i++) {
            if (jpn3.getItemAt(i).getValue().equals(jpnList.get(0).getQuestion3())) {
                jpn3.setSelectedIndex(i);
                break;
            }
        }
        */
        /* 総合 */
        /*
        TextField jpnW = view.japaneseWField;
        jpnW.setText(jpnList.get(0).getJpnTotal());
        */
    }

    /**
     * エラーメッセージを初期化します。
     *
     * @param label エラーメッセージラベル
     */
    /*
    private void initErrorMessageLabel(ImageLabel label) {
        label.setText(StringUtils.EMPTY);
        label.setVisible(false);
        label.setIconTextGap(-label.getWidth() + 4);
    
    }
    */
    /**
     * スコアにフォーカスロストイベントを設定する。
     *
     * @param text スコアテキスト
     * @param sw 認定試験スイッチ(1 or 2)
     */
    /*private void addListenerCefrScore(TextField text, SWITCH sw) {
        text.addFocusListener(new FocusAdapter() {
            public void focusLost(FocusEvent e) {
                setCefrLevel(sw);
            }
        });
    }*/

    /**
     * コンボボックスを設定します。
     *
     * @param combobox コンボボックス
     * @param items コンボアイテム
     */
    /*
    private void setCombobox(ComboBox<String> combobox, String[] items) {
        combobox.removeAllItems();
        List<ComboBoxItem<String>> itemList = CollectionsUtil.newArrayList(items.length);
        for (String item : items) {
            itemList.add(new ComboBoxItem<String>(item));
        }
        combobox.setItemList(itemList);
    }
    */

    /**
     * コンボボックスを設定します。
     *
     * @param <T> コンボボックスBean
     * @param combobox コンボボックス
     * @param items コンボアイテム
     */
    /*private <T> void setCombobox(ComboBox<T> combobox, List<T> items) {
        combobox.removeAllItems();
        List<ComboBoxItem<T>> itemList = CollectionsUtil.newArrayList(items.size() + 1);
        itemList.add(new ComboBoxItem<T>(StringUtils.EMPTY, null));
        for (T item : items) {
            String name = ((ComboBoxItems) item).getName();
            itemList.add(new ComboBoxItem<T>(name, item));
        }
        combobox.setItemList(itemList);
    }*/

    /**
     * CEFRレベルを算出して表示します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     */
    /*private void setCefrLevel(SWITCH sw) {
    
        EngCertJpnDesc data = systemDto;
        InputEngPtBean input = new InputEngPtBean();
    
        setEngDispData(sw, null);
         認定試験名コンボボックス
        EngPtBean exam = getEngPtField(sw).getSelectedValue();
         認定試験レベルコンボボックス
        EngPtDetailBean detail = getEngPtLevelField(sw).getSelectedValue();
         スコアテキストボックス
        TextField cefrScore = getCefrScoreField(sw);
         CEFRレベルコンボボックス
        ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
         英検合否コンボボックス
        ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
    
        input.setEngPtCd(exam.getEngPtCd());
        if (detail != null) {
            input.setEngPtLevelCd(detail.getEngPtLevelCd());
        }
        input.setScore(cefrScore.getText());
        changingFlg = true;
    
         エラーメッセージの初期化
        if (sw == ExmntnInputHelper.SWITCH.ENG1) {
            view.errorMessageCefr1Label.setText("");
            view.errorMessageCefr1Label.setVisible(false);
        } else {
            view.errorMessageCefr2Label.setText("");
            view.errorMessageCefr2Label.setVisible(false);
        }
    
         CEFRのクリア
        cefrLevel.setSelectedIndex(0);
         表示用CEFRレベルコード
        systemDto.getCefrInfo().get(0).setDispCefrLevelCd1("");
         表示用CEFRレベル名
        systemDto.getCefrInfo().get(0).setDispCefrLevel1("");
    
         入力チェック
        int validResult = ExamUtil.validateEngCert(data, input);
        input.setErrorCode(validResult);
        if (validResult == 0) {
             スコア刻みチェック
            if (Double.parseDouble(cefrScore.getText()) % exam.getScoreTick() != 0) {
                if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                    view.errorMessageCefr1Label.setText(getMessage("error.exmntn.main.C_E017", exam.getScoreTick()));
                    view.errorMessageCefr1Label.setVisible(true);
                } else {
                    view.errorMessageCefr2Label.setText(getMessage("error.exmntn.main.C_E017", exam.getScoreTick()));
                    view.errorMessageCefr2Label.setVisible(true);
                }
            } else {
                 認定試験情報を設定
                ExamUtil.createEngCert(data, input);
                setCefrLevel(cefrLevel, input.getCefrLevelCd());
                setResult(engExamResult, input.getDispResult());
                setEngDispData(sw, input);
            }
        } else if (validResult == 2) {
            if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                view.errorMessageCefr1Label.setText(getMessage("error.exmntn.main.C_E016"));
                view.errorMessageCefr1Label.setVisible(true);
            } else {
                view.errorMessageCefr2Label.setText(getMessage("error.exmntn.main.C_E016"));
                view.errorMessageCefr2Label.setVisible(true);
            }
        }
        changingFlg = false;
    }*/

    /**
     * 選択された認定試験名に該当する認定試験レベル情報を抽出します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return List<EngPtDetailBean> 認定試験詳細情報
     */
    /*private List<EngPtDetailBean> filterJoininExamDetailList(SWITCH sw) {
         選択した認定試験名
        EngPtBean exam = getEngPtField(sw).getSelectedValue();
    
        List<EngPtDetailBean> result = CollectionsUtil.newArrayList();
        if (exam != null) {
            result = systemDto.getEngPtDetailList().stream().filter(e -> e.getEngPtCd().equals(exam.getEngPtCd())).collect(Collectors.toList());
        }
        setEngDispData(sw, null);
    
        return result;
    }*/

    /**
     * 認定試験名コンボボックス変更アクションです。
     * 認定試験名に応じて認定試験レベルを変更します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult changeEngPtComboBox(SWITCH sw) {
         認定試験名コンボボックス
        ComboBox<EngPtBean> joiningExam = getEngPtField(sw);
         認定試験レベルコンボボックス
        ComboBox<EngPtDetailBean> joiningExamLevel = getEngPtLevelField(sw);
         スコアテキストボックス
        TextField cefrScore = getCefrScoreField(sw);
         スコアが不明チェックボックス
        CodeCheckBox unknownCefrScore = getUnknownCefrScoreField(sw);
         CEFRレベルコンボボックス
        ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
         CEFRレベルが不明チェックボックス
        CodeCheckBox unknownCefrLevel = getUnknownCefrLevelField(sw);
         英検合否コンボボックス
        ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
    
        changingFlg = true;
    
         エラーメッセージの初期化
        if (sw == ExmntnInputHelper.SWITCH.ENG1) {
            view.errorMessageCefr1Label.setText("");
            view.errorMessageCefr1Label.setVisible(false);
        } else {
            view.errorMessageCefr2Label.setText("");
            view.errorMessageCefr2Label.setVisible(false);
        }
    
         認定試験レベルを再設定
        boolean isLevel = isLevel(sw);
        if (isLevel) {
            setCombobox(joiningExamLevel, filterJoininExamDetailList(sw));
        } else {
            EngPtBean exam = joiningExam.getSelectedValue();
            if (exam != null) {
                setSelectableCefrLevel(exam, joiningExamLevel, cefrLevel);
            }
        }
    
         値を初期化
        joiningExamLevel.setSelectedIndex(0);
        cefrScore.setText("");
        unknownCefrScore.setSelected(false);
        cefrLevel.setSelectedIndex(0);
        unknownCefrLevel.setSelected(false);
        engExamResult.setSelectedIndex(0);
    
         活性／非活性を設定
        if (joiningExam.getSelectedIndex() == 0) {
            joiningExamLevel.setEnabled(false);
            cefrScore.setEnabled(false);
            unknownCefrScore.setEnabled(false);
        } else {
            joiningExamLevel.setEnabled(isLevel);
            cefrScore.setEnabled(!isLevel);
            unknownCefrScore.setEnabled(!isLevel);
        }
        cefrLevel.setEnabled(false);
        unknownCefrLevel.setEnabled(false);
        engExamResult.setEnabled(false);
        setEngDispData(sw, null);
        changingFlg = false;
    
        return null;
    }*/

    /**
     * 認定試験レベルコンボボックス変更アクションです。
     * CEFRレベルの活性／非活性を制御します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult joiningExamLevelComboBox(SWITCH sw) {
        if (!changingFlg) {
             認定試験名コンボボックス
            EngPtBean exam = getEngPtField(sw).getSelectedValue();
             認定試験レベルコンボボックス
            ComboBox<EngPtDetailBean> joiningExamLevel = getEngPtLevelField(sw);
             スコアテキストボックス
            TextField cefrScore = getCefrScoreField(sw);
             スコアが不明チェックボックス
            CodeCheckBox unknownCefrScore = getUnknownCefrScoreField(sw);
             CEFRレベルコンボボックス
            ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
             CEFRレベルが不明チェックボックス
            CodeCheckBox unknownCefrLevel = getUnknownCefrLevelField(sw);
             英検合否コンボボックス
            ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
    
            changingFlg = true;
    
             エラーメッセージの初期化
            if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                view.errorMessageCefr1Label.setText("");
                view.errorMessageCefr1Label.setVisible(false);
            } else {
                view.errorMessageCefr2Label.setText("");
                view.errorMessageCefr2Label.setVisible(false);
            }
    
             値を初期化
            cefrScore.setText("");
            unknownCefrScore.setSelected(false);
            setSelectableCefrLevel(exam, joiningExamLevel, cefrLevel);
            cefrLevel.setSelectedIndex(0);
            unknownCefrLevel.setSelected(false);
            engExamResult.setSelectedIndex(0);
    
            boolean enabled = joiningExamLevel.getSelectedIndex() != 0;
    
             活性／非活性を設定
            cefrScore.setEnabled(enabled);
            unknownCefrScore.setEnabled(enabled);
            cefrLevel.setEnabled(false);
            unknownCefrLevel.setEnabled(false);
            engExamResult.setEnabled(false);
            setEngDispData(sw, null);
            changingFlg = false;
        }
    
        return null;
    }*/

    /**
     * 選択された認定試験レベルで設定可能なCEFRレベルを設定します。
     *
     * @param exam 認定試験情報
     * @param joiningExamLevel 認定試験レベルコンボボックス
     * @param cefrLevel CEFRレベルコンボボックス
     */
    /*private void setSelectableCefrLevel(EngPtBean exam, ComboBox<EngPtDetailBean> joiningExamLevel, ComboBox<CefrBean> cefrLevel) {
        EngPtDetailBean lvl;
        if (joiningExamLevel.getSelectedValue() != null) {
            lvl = joiningExamLevel.getSelectedValue();
        } else {
            lvl = systemDto.getEngPtDetailList().stream().filter(e -> e.getEngPtCd().equals(exam.getEngPtCd())).findFirst().orElse(null);
        }
         選択された認定試験レベルで設定可能なCEFRレベルを抽出
        List<CefrBean> cefrList = systemDto.getCefrList().stream()
                .filter(e -> (lvl.getCefrLevelUnder().compareTo(e.getCefrCd()) >= 0 && lvl.getCefrLevelUpper().compareTo(e.getCefrCd()) <= 0) || Constants.NON_LEVEL_CEFR_CD.contentEquals(e.getCefrCd())).collect(Collectors.toList());
        setCombobox(cefrLevel, cefrList);
    }*/

    /**
     * スコアが不明チェックボックス変更アクションです。
     * CEFRレベル、英検合否の活性／非活性を制御します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult changeUnknownCefrScoreCheckBox(SWITCH sw) {
        if (!changingFlg) {
             スコアテキストボックス
            TextField cefrScore = getCefrScoreField(sw);
             スコアが不明チェックボックス
            CodeCheckBox unknownCefrScore = getUnknownCefrScoreField(sw);
             CEFRレベルコンボボックス
            ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
             CEFRレベルが不明チェックボックス
            CodeCheckBox unknownCefrLevel = getUnknownCefrLevelField(sw);
             英検合否コンボボックス
            ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
             英検かどうか
            boolean enabled = isEiken(sw);
    
            changingFlg = true;
    
             エラーメッセージの初期化
            if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                view.errorMessageCefr1Label.setText("");
                view.errorMessageCefr1Label.setVisible(false);
            } else {
                view.errorMessageCefr2Label.setText("");
                view.errorMessageCefr2Label.setVisible(false);
            }
    
             値を初期化
            cefrScore.setText("");
            cefrLevel.setSelectedIndex(0);
            unknownCefrLevel.setSelected(false);
            engExamResult.setSelectedIndex(0);
    
             活性／非活性を設定
            cefrScore.setEnabled(!unknownCefrScore.isSelected());
            cefrLevel.setEnabled(unknownCefrScore.isSelected());
            unknownCefrLevel.setEnabled(unknownCefrScore.isSelected() && enabled);
            enabled = unknownCefrLevel.isSelected() && isGouhi(sw);
            engExamResult.setEnabled(enabled);
            setEngDispData(sw, null);
            changingFlg = false;
        }
        return null;
    }*/

    /**
     * CEFRレベルコンボボックス変更アクションです。
     * スコアを算出して表示します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult chageCefrLevelComboBox(SWITCH sw) {
        if (!changingFlg) {
            setEngDispData(sw, null);
            EngCertJpnDesc data = systemDto;
            InputEngPtBean input = new InputEngPtBean();
    
             認定試験名コンボボックス
            EngPtBean exam = getEngPtField(sw).getSelectedValue();
             認定試験レベルコンボボックス
            EngPtDetailBean detail = getEngPtLevelField(sw).getSelectedValue();
             スコアテキストボックス
            TextField cefrScore = getCefrScoreField(sw);
             英検合否コンボボックス
            ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
             CEFRレベルコンボボックス
            CefrBean cefrLevel = getCefrLevelField(sw).getSelectedValue();
    
            changingFlg = true;
            if (cefrLevel == null) {
                 未選択の場合は、初期化
                cefrScore.setText("");
                setResult(engExamResult, null);
            } else {
    
                input.setEngPtCd(exam.getEngPtCd());
                if (detail != null) {
                    input.setEngPtLevelCd(detail.getEngPtLevelCd());
                }
                input.setCefrLevelCd(cefrLevel.getCefrCd());
    
                 認定試験情報を設定
                ExamUtil.createEngCert(data, input);
                cefrScore.setText(input.getScore());
                setResult(engExamResult, input.getDispResult());
                setEngDispData(sw, input);
            }
            changingFlg = false;
        }
        return null;
    }*/

    /**
     * CEFRレベルが不明チェックボックス変更アクションです。
     * スコア、CEFRレベルのクリアおよび、英検合否の活性／非活性を制御します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult changeUnknownCefrLevelCheckBox(SWITCH sw) {
        if (!changingFlg) {
             スコアテキストボックス
            TextField cefrScore = getCefrScoreField(sw);
             CEFRレベルコンボボックス
            ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
             CEFRレベルが不明チェックボックス
            CodeCheckBox unknownCefrLevel = getUnknownCefrLevelField(sw);
             英検合否コンボボックス
            ComboBox<EngExamResultBean> engExamResult = getEngExamResultFiled(sw);
    
            changingFlg = true;
             値を初期化
            cefrScore.setText("");
            cefrLevel.setSelectedIndex(0);
            engExamResult.setSelectedIndex(0);
    
             活性／非活性を設定
            cefrLevel.setEnabled(!unknownCefrLevel.isSelected());
            boolean enabled = unknownCefrLevel.isSelected() && isGouhi(sw);
            engExamResult.setEnabled(enabled);
            setEngDispData(sw, null);
            changingFlg = false;
        }
        return null;
    }*/

    /**
     * 合否コンボボックス変更アクションです。
     * 英検合否からスコアとCEFRレベルを算出して表示します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return なし（アクション結果）
     */
    /*public ExecuteResult changeEngExamResult(SWITCH sw) {
        if (!changingFlg) {
            setEngDispData(sw, null);
            EngCertJpnDesc data = systemDto;
            InputEngPtBean input = new InputEngPtBean();
    
             認定試験名コンボボックス
            EngPtBean exam = getEngPtField(sw).getSelectedValue();
             認定試験レベルコンボボックス
            EngPtDetailBean detail = getEngPtLevelField(sw).getSelectedValue();
             スコアテキストボックス
            TextField cefrScore = getCefrScoreField(sw);
             CEFRレベルコンボボックス
            ComboBox<CefrBean> cefrLevel = getCefrLevelField(sw);
             英検合否コンボボックス
            EngExamResultBean item = getEngExamResultFiled(sw).getSelectedValue();
    
            changingFlg = true;
            if (item == null) {
                 未選択の場合は初期化する
                cefrScore.setText("");
                setCefrLevel(cefrLevel, null);
            } else {
                input.setEngPtCd(exam.getEngPtCd());
                input.setEngPtLevelCd(detail.getEngPtLevelCd());
                input.setResult(item.getFlg());
    
                 認定試験情報を設定
                ExamUtil.createEngCert(data, input);
                cefrScore.setText(input.getScore());
                setCefrLevel(cefrLevel, input.getCefrLevelCd());
                setEngDispData(sw, input);
            }
            changingFlg = false;
        }
        return null;
    }*/

    /**
     * OKボタンクリック処理
     *
     * @return false：チェックエラー有 true：チェックエラー無
     */
    public boolean clickOkButtunAction() {
        /*
        if (view.errorMessageJpnLabel.isVisible()) {
            /* エラーがある場合は処理しない
            return false;
        }
        */
        /* 英語変更有無判定 */
        /*boolean isChange = getCefrChange();
        mainDto.setChangeMarkCefrScore(isChange);
         呼び出し元画面の初期に戻すボタンの活性状態変更
        if (isChange) {
            ExmntnMainCheckHelper helper = SingletonS2Container.getComponent(ExmntnMainCheckHelper.class);
            helper.enableReturnInitButton(true);
        }*/

        /* 戻り値の値の再設定 */
        /*         英語認定試験１
         表示用CEFR試験コード
        systemDto.getCefrInfo().get(0).setCefrExamCd1(systemDto.getCefrInfo().get(0).getDispCefrExamCd1());
         表示用CEFR試験名
        systemDto.getCefrInfo().get(0).setCefrExamName1(systemDto.getCefrInfo().get(0).getDispCefrExamName1());
         表示用CEFR試験名(略称)
        systemDto.getCefrInfo().get(0).setCefrExamNameAbbr1(systemDto.getCefrInfo().get(0).getDispCefrExamNameAbbr1());
         表示用認定試験レベルコード
        systemDto.getCefrInfo().get(0).setCefrExamLevel1(systemDto.getCefrInfo().get(0).getDispCefrExamLevel1());
         表示用認定試験レベルコード名
        systemDto.getCefrInfo().get(0).setCefrExamLevelName1(systemDto.getCefrInfo().get(0).getDispCefrExamLevelName1());
         表示用認定試験レベルコード名(略称)
        systemDto.getCefrInfo().get(0).setCefrExamLevelNameAbbr1(systemDto.getCefrInfo().get(0).getDispCefrExamLevelNameAbbr1());
         表示用CEFR試験スコア
        systemDto.getCefrInfo().get(0).setCefrScore1(systemDto.getCefrInfo().get(0).getDispCefrScore1());
         表示用CEFRレベルコード
        systemDto.getCefrInfo().get(0).setCefrLevelCd1(systemDto.getCefrInfo().get(0).getDispCefrLevelCd1());
         表示用CEFRレベル名
        systemDto.getCefrInfo().get(0).setCefrLevel1(systemDto.getCefrInfo().get(0).getDispCefrLevel1());
         表示用CEFRレベル名(略称)
        systemDto.getCefrInfo().get(0).setCefrLevelAbbr1(systemDto.getCefrInfo().get(0).getDispCefrLevelAbbr1());
         表示用合否
        systemDto.getCefrInfo().get(0).setExamResult1(systemDto.getCefrInfo().get(0).getDispExamResult1());
         表示用スコア不明
        systemDto.getCefrInfo().get(0).setUnknownScore1(systemDto.getCefrInfo().get(0).getDispUnknownScore1());
         表示用CEFR不明
        systemDto.getCefrInfo().get(0).setUnknownCefr1(systemDto.getCefrInfo().get(0).getDispUnknownCefr1());
         表示用スコア補正フラグ
        systemDto.getCefrInfo().get(0).setScoreCorrectFlg1(systemDto.getCefrInfo().get(0).getDispScoreCorrectFlg1());
         表示用CEFRレベルコード補正フラグ
        systemDto.getCefrInfo().get(0).setCefrLevelCdCorrectFlg1(systemDto.getCefrInfo().get(0).getDispCefrLevelCdCorrectFlg1());
         表示用合否補正フラグ
        systemDto.getCefrInfo().get(0).setExamResultCorrectFlg1(systemDto.getCefrInfo().get(0).getDispExamResultCorrectFlg1());
         英語認定試験２
         表示用CEFR試験コード
        systemDto.getCefrInfo().get(0).setCefrExamCd2(systemDto.getCefrInfo().get(0).getDispCefrExamCd2());
         表示用CEFR試験名
        systemDto.getCefrInfo().get(0).setCefrExamName2(systemDto.getCefrInfo().get(0).getDispCefrExamName2());
         表示用CEFR試験名(略称)
        systemDto.getCefrInfo().get(0).setCefrExamNameAbbr2(systemDto.getCefrInfo().get(0).getDispCefrExamNameAbbr2());
         表示用認定試験レベルコード
        systemDto.getCefrInfo().get(0).setCefrExamLevel2(systemDto.getCefrInfo().get(0).getDispCefrExamLevel2());
         表示用認定試験レベルコード名
        systemDto.getCefrInfo().get(0).setCefrExamLevelName2(systemDto.getCefrInfo().get(0).getDispCefrExamLevelName2());
         表示用認定試験レベルコード名(略称)
        systemDto.getCefrInfo().get(0).setCefrExamLevelNameAbbr2(systemDto.getCefrInfo().get(0).getDispCefrExamLevelNameAbbr2());
         表示用CEFR試験スコア
        systemDto.getCefrInfo().get(0).setCefrScore2(systemDto.getCefrInfo().get(0).getDispCefrScore2());
         表示用CEFRレベルコード
        systemDto.getCefrInfo().get(0).setCefrLevelCd2(systemDto.getCefrInfo().get(0).getDispCefrLevelCd2());
         表示用CEFRレベル名
        systemDto.getCefrInfo().get(0).setCefrLevel2(systemDto.getCefrInfo().get(0).getDispCefrLevel2());
         表示用CEFRレベル名(略称)
        systemDto.getCefrInfo().get(0).setCefrLevelAbbr2(systemDto.getCefrInfo().get(0).getDispCefrLevelAbbr2());
         表示用合否
        systemDto.getCefrInfo().get(0).setExamResult2(systemDto.getCefrInfo().get(0).getDispExamResult2());
         表示用スコア不明
        systemDto.getCefrInfo().get(0).setUnknownScore2(systemDto.getCefrInfo().get(0).getDispUnknownScore2());
         表示用CEFR不明
        systemDto.getCefrInfo().get(0).setUnknownCefr2(systemDto.getCefrInfo().get(0).getDispUnknownCefr2());
         表示用スコア補正フラグ
        systemDto.getCefrInfo().get(0).setScoreCorrectFlg2(systemDto.getCefrInfo().get(0).getDispScoreCorrectFlg2());
         表示用CEFRレベルコード補正フラグ
        systemDto.getCefrInfo().get(0).setCefrLevelCdCorrectFlg2(systemDto.getCefrInfo().get(0).getDispCefrLevelCdCorrectFlg2());
         表示用合否補正フラグ
        systemDto.getCefrInfo().get(0).setExamResultCorrectFlg2(systemDto.getCefrInfo().get(0).getDispExamResultCorrectFlg2());*/

        /* 国語記述 */
        /*
        systemDto.getJpnInfo().get(0).setSubCd(SUBCODE_CENTER_DEFAULT_JKIJUTSU);
        if (view.jpnWriting1ComboBox.getSelectedValue() == null || view.jpnWriting2ComboBox.getSelectedValue() == null || view.jpnWriting3ComboBox.getSelectedValue() == null || "".equals(view.japaneseWField.getText())) {
            /* 問１
            systemDto.getJpnInfo().get(0).setQuestion1("");
            /* 問２
            systemDto.getJpnInfo().get(0).setQuestion2("");
            /* 問３
            systemDto.getJpnInfo().get(0).setQuestion3("");
            /* 総合
            systemDto.getJpnInfo().get(0).setJpnTotal("");
        } else {
            /* 問１
            systemDto.getJpnInfo().get(0).setQuestion1(view.jpnWriting1ComboBox.getSelectedValue());
            /* 問２
            systemDto.getJpnInfo().get(0).setQuestion2(view.jpnWriting2ComboBox.getSelectedValue());
            /* 問３
            systemDto.getJpnInfo().get(0).setQuestion3(view.jpnWriting3ComboBox.getSelectedValue());
            /* 総合
            systemDto.getJpnInfo().get(0).setJpnTotal(view.japaneseWField.getText());
        }
        */
        /* 再描画処理 */
        /*List<ExmntnMainCefrRecordBean> cefrList = systemDto.getCefrInfo();*/
        /*List<ExmntnMainJpnRecordBean> jpnList = systemDto.getJpnInfo();*/
        /* 国語変更有無判定 */
        /*
        if (!mainview.jpnWritingField.getText().equals(getMarkJpnScore(jpnList))) {
            mainDto.setChangeMarkJpnScore(true);
            ExmntnMainCheckHelper helper = SingletonS2Container.getComponent(ExmntnMainCheckHelper.class);
            helper.enableReturnInitButton(true);
        }
        */
        /* データ設定 */
        /*scoreHelper.setMarkCefrScore(cefrList, mainview.cefr1Field);
        scoreHelper.setMarkCefrScore(cefrList, mainview.cefr2Field);
        scoreHelper.setMarkCefrScore(cefrList, mainview.cefrLvl1Field);
        scoreHelper.setMarkCefrScore(cefrList, mainview.cefrLvl2Field);*/

        /* 現代文(記) */
        /*scoreHelper.setMarkJpnScore(jpnList, mainview.jpnWritingField); */

        return true;
    }

    /**
     * CEFRの変更有無を戻します。
     *
     * @return true:変更有り、false:変更無し
     */
    /*private boolean getCefrChange() {
         変更有無判定
        List<ExmntnMainCefrRecordBean> cefrList = systemDto.getCefrInfo();
        if (cefrList.get(0).getCefrExamCd1() == null) {
            return true;
        }
         英語認定試験１
        if (!cefrList.get(0).getCefrExamCd1().equals(cefrList.get(0).getDispCefrExamCd1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamName1().equals(cefrList.get(0).getDispCefrExamName1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamNameAbbr1().equals(cefrList.get(0).getDispCefrExamNameAbbr1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevel1().equals(cefrList.get(0).getDispCefrExamLevel1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevelName1().equals(cefrList.get(0).getDispCefrExamLevelName1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevelNameAbbr1().equals(cefrList.get(0).getDispCefrExamLevelNameAbbr1())) {
            return true;
        }
        if (!cefrList.get(0).getUnknownScore1().equals(cefrList.get(0).getDispUnknownScore1())) {
            return true;
        }
        if (!cefrList.get(0).getUnknownCefr1().equals(cefrList.get(0).getDispUnknownCefr1())) {
            return true;
        }
        if (!cefrList.get(0).getExamResult1().equals(cefrList.get(0).getDispExamResult1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelCd1().equals(cefrList.get(0).getDispCefrLevelCd1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevel1().equals(cefrList.get(0).getDispCefrLevel1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelAbbr1().equals(cefrList.get(0).getDispCefrLevelAbbr1())) {
            return true;
        }
        if (!ObjectUtils.defaultIfNull(cefrList.get(0).getCefrScore1(), "").equals(ObjectUtils.defaultIfNull(cefrList.get(0).getDispCefrScore1(), ""))) {
            return true;
        }
        if (!cefrList.get(0).getScoreCorrectFlg1().equals(cefrList.get(0).getDispScoreCorrectFlg1())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelCdCorrectFlg1().equals(cefrList.get(0).getDispCefrLevelCdCorrectFlg1())) {
            return true;
        }
        if (!cefrList.get(0).getExamResultCorrectFlg1().equals(cefrList.get(0).getDispExamResultCorrectFlg1())) {
            return true;
        }
    
         英語認定試験２
        if (!cefrList.get(0).getCefrExamCd2().equals(cefrList.get(0).getDispCefrExamCd2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamName2().equals(cefrList.get(0).getDispCefrExamName2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamNameAbbr2().equals(cefrList.get(0).getDispCefrExamNameAbbr2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevel2().equals(cefrList.get(0).getDispCefrExamLevel2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevelName2().equals(cefrList.get(0).getDispCefrExamLevelName2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrExamLevelNameAbbr2().equals(cefrList.get(0).getDispCefrExamLevelNameAbbr2())) {
            return true;
        }
        if (!cefrList.get(0).getUnknownScore2().equals(cefrList.get(0).getDispUnknownScore2())) {
            return true;
        }
        if (!cefrList.get(0).getUnknownCefr2().equals(cefrList.get(0).getDispUnknownCefr2())) {
            return true;
        }
        if (!cefrList.get(0).getExamResult2().equals(cefrList.get(0).getDispExamResult2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelCd2().equals(cefrList.get(0).getDispCefrLevelCd2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevel2().equals(cefrList.get(0).getDispCefrLevel2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelAbbr2().equals(cefrList.get(0).getDispCefrLevelAbbr2())) {
            return true;
        }
        if (!ObjectUtils.defaultIfNull(cefrList.get(0).getCefrScore2(), "").equals(ObjectUtils.defaultIfNull(cefrList.get(0).getDispCefrScore2(), ""))) {
            return true;
        }
        if (!cefrList.get(0).getScoreCorrectFlg2().equals(cefrList.get(0).getDispScoreCorrectFlg2())) {
            return true;
        }
        if (!cefrList.get(0).getCefrLevelCdCorrectFlg2().equals(cefrList.get(0).getDispCefrLevelCdCorrectFlg2())) {
            return true;
        }
        if (!cefrList.get(0).getExamResultCorrectFlg2().equals(cefrList.get(0).getDispExamResultCorrectFlg2())) {
            return true;
        }
        return false;
    }*/

    /**
     * 国語記述成績を編集します。
     *
     * @param jpnList 国語記述成績リスト
     * @return 編集結果
     */
    /*
    private String getMarkJpnScore(List<ExmntnMainJpnRecordBean> jpnList) {
        if (jpnList == null) {
            return "";
        }
        return jpnList.get(0).getQuestion1() + jpnList.get(0).getQuestion2() + jpnList.get(0).getQuestion3() + jpnList.get(0).getJpnTotal();
    }
    */

    /**
     * 指定されたCEFRをコンボボックスに設定します。
     *
     * @param cefrLevel CEFRレベルコンボボックス
     * @param cefr CEFR値
     */
    /*private void setCefrLevel(ComboBox<CefrBean> cefrLevel, String cefr) {
        if (cefr == null) {
            cefrLevel.setSelectedIndex(0);
        } else {
            for (int i = 1; i < cefrLevel.getItemCount(); i++) {
                if (cefrLevel.getItemAt(i).getValue().getCefrCd().equals(cefr)) {
                    cefrLevel.setSelectedIndex(i);
                    break;
                }
            }
        }
    }*/

    /**
     * 指定された合否をコンボボックスに設定します。
     *
     * @param engExamResult 合否コンボボックス
     * @param result 合否値
     */
    /*private void setResult(ComboBox<EngExamResultBean> engExamResult, String result) {
        if (result == null) {
            engExamResult.setSelectedIndex(0);
        } else {
            for (int i = 1; i < engExamResult.getItemCount(); i++) {
                if (engExamResult.getItemAt(i).getValue().getName().equals(result)) {
                    engExamResult.setSelectedIndex(i);
                    break;
                }
            }
        }
    }*/

    /**
     * 指定されたCEFRをコンボボックスに設定します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @param input 入力値
     */
    /*private void setEngDispData(SWITCH sw, InputEngPtBean input) {
    
        if (input == null) {
            if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                 英語認定試験１
                 表示用CEFR試験コード
                systemDto.getCefrInfo().get(0).setDispCefrExamCd1("");
                 表示用CEFR試験名
                systemDto.getCefrInfo().get(0).setDispCefrExamName1("");
                 表示用CEFR試験名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr1("");
                 表示用認定試験レベルコード
                systemDto.getCefrInfo().get(0).setDispCefrExamLevel1("");
                 表示用認定試験レベルコード名
                systemDto.getCefrInfo().get(0).setDispCefrExamLevelName1("");
                 表示用認定試験レベルコード名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr1("");
                 表示用CEFR試験スコア
                systemDto.getCefrInfo().get(0).setDispCefrScore1(null);
                 表示用CEFRレベルコード
                systemDto.getCefrInfo().get(0).setDispCefrLevelCd1("");
                 表示用CEFRレベル名
                systemDto.getCefrInfo().get(0).setDispCefrLevel1("");
                 表示用CEFRレベル名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr1("");
                 表示用合否
                systemDto.getCefrInfo().get(0).setDispExamResult1("");
                 表示用スコア不明
                systemDto.getCefrInfo().get(0).setDispUnknownScore1(Constants.FLG_OFF);
                 表示用CEFR不明
                systemDto.getCefrInfo().get(0).setDispUnknownCefr1(Constants.FLG_OFF);
                 表示用スコア補正フラグ
                systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg1(Constants.FLG_OFF);
                 表示用CEFRレベルコード補正フラグ
                systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg1(Constants.FLG_OFF);
                 表示用合否補正フラグ
                systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg1(Constants.FLG_OFF);
            } else {
                 英語認定試験２
                 表示用CEFR試験コード
                systemDto.getCefrInfo().get(0).setDispCefrExamCd2("");
                 表示用CEFR試験名
                systemDto.getCefrInfo().get(0).setDispCefrExamName2("");
                 表示用CEFR試験名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr2("");
                 表示用認定試験レベルコード
                systemDto.getCefrInfo().get(0).setDispCefrExamLevel2("");
                 表示用認定試験レベルコード名
                systemDto.getCefrInfo().get(0).setDispCefrExamLevelName2("");
                 表示用認定試験レベルコード名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr2("");
                 表示用CEFR試験スコア
                systemDto.getCefrInfo().get(0).setDispCefrScore2(null);
                 表示用CEFRレベルコード
                systemDto.getCefrInfo().get(0).setDispCefrLevelCd2("");
                 表示用CEFRレベル名
                systemDto.getCefrInfo().get(0).setDispCefrLevel2("");
                 表示用CEFRレベル名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr2("");
                 表示用合否
                systemDto.getCefrInfo().get(0).setDispExamResult2("");
                 表示用スコア不明
                systemDto.getCefrInfo().get(0).setDispUnknownScore2(Constants.FLG_OFF);
                 表示用CEFR不明
                systemDto.getCefrInfo().get(0).setDispUnknownCefr2(Constants.FLG_OFF);
                 表示用スコア補正フラグ
                systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg2(Constants.FLG_OFF);
                 表示用CEFRレベルコード補正フラグ
                systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg2(Constants.FLG_OFF);
                 表示用合否補正フラグ
                systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg2(Constants.FLG_OFF);
            }
        } else {
             認定試験名コンボボックス
            EngPtBean exam = getEngPtField(sw).getSelectedValue();
             認定試験レベルコンボボックス
            EngPtDetailBean detail = getEngPtLevelField(sw).getSelectedValue();
             CEFRレベルコンボボックス
            CefrBean cefrLevel = getCefrLevelField(sw).getSelectedValue();
             スコア不明
            CodeCheckBox unknownScoreCheckBox = getUnknownCefrScoreField(sw);
             CEFR不明
            CodeCheckBox unknownCefrCheckBox = getUnknownCefrLevelField(sw);
    
            if (sw == ExmntnInputHelper.SWITCH.ENG1) {
                 英語認定試験１
                 表示用CEFR試験コード
                systemDto.getCefrInfo().get(0).setDispCefrExamCd1(input.getEngPtCd());
                 表示用CEFR試験名
                systemDto.getCefrInfo().get(0).setDispCefrExamName1(exam.getEngPtName());
                 表示用CEFR試験名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr1(exam.getEngPtAbbr());
                if (detail == null) {
                     表示用認定試験レベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevel1("");
                     表示用認定試験レベルコード名
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelName1("");
                     表示用認定試験レベルコード名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr1("");
                } else {
                     表示用認定試験レベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevel1(input.getEngPtLevelCd());
                     表示用認定試験レベルコード名
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelName1(detail.getEngPtLevelName());
                     表示用認定試験レベルコード名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr1(detail.getEngPtLevelAbbr());
                }
                 表示用CEFR試験スコア
                if (input.getScore() == null || Constants.DBL_HYPHEN.equals(input.getScore())) {
                    systemDto.getCefrInfo().get(0).setDispCefrScore1(null);
                } else {
                    systemDto.getCefrInfo().get(0).setDispCefrScore1(Double.valueOf(input.getScore()));
                }
                if (cefrLevel == null) {
                     表示用CEFRレベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrLevelCd1("");
                     表示用CEFRレベル名
                    systemDto.getCefrInfo().get(0).setDispCefrLevel1("");
                     表示用CEFRレベル名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr1("");
                } else {
                     表示用CEFRレベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrLevelCd1(input.getCefrLevelCd());
                     表示用CEFRレベル名
                    systemDto.getCefrInfo().get(0).setDispCefrLevel1(cefrLevel.getCefrName());
                     表示用CEFRレベル名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr1(cefrLevel.getCefrAbbr());
                }
                 表示用合否
                if (input.getResult() == null) {
                    systemDto.getCefrInfo().get(0).setDispExamResult1("");
                } else {
                    systemDto.getCefrInfo().get(0).setDispExamResult1(input.getResult());
                }
                 表示用スコア不明
                if (unknownScoreCheckBox.isSelected()) {
                    systemDto.getCefrInfo().get(0).setDispUnknownScore1(Constants.FLG_ON);
                } else {
                    systemDto.getCefrInfo().get(0).setDispUnknownScore1(Constants.FLG_OFF);
                }
                 表示用CEFR不明
                if (unknownCefrCheckBox.isSelected()) {
                    systemDto.getCefrInfo().get(0).setDispUnknownCefr1(Constants.FLG_ON);
                } else {
                    systemDto.getCefrInfo().get(0).setDispUnknownCefr1(Constants.FLG_OFF);
                }
                 表示用スコア補正フラグ
                systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg1(input.getScoreCorrectFlg());
                 表示用CEFRレベルコード補正フラグ
                systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg1(input.getCefrLevelCdCorrectFlg());
                 表示用合否補正フラグ
                systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg1(input.getResultCorrectFlg());
            } else {
                 英語認定試験２
                 表示用CEFR試験コード
                systemDto.getCefrInfo().get(0).setDispCefrExamCd2(input.getEngPtCd());
                 表示用CEFR試験名
                systemDto.getCefrInfo().get(0).setDispCefrExamName2(exam.getEngPtName());
                 表示用CEFR試験名(略称)
                systemDto.getCefrInfo().get(0).setDispCefrExamNameAbbr2(exam.getEngPtAbbr());
                if (detail == null) {
                     表示用認定試験レベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevel2("");
                     表示用認定試験レベルコード名
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelName2("");
                     表示用認定試験レベルコード名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr2("");
                } else {
                     表示用認定試験レベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevel2(input.getEngPtLevelCd());
                     表示用認定試験レベルコード名
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelName2(detail.getEngPtLevelName());
                     表示用認定試験レベルコード名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrExamLevelNameAbbr2(detail.getEngPtLevelAbbr());
                }
                 表示用CEFR試験スコア
                if (input.getScore() == null || Constants.DBL_HYPHEN.equals(input.getScore())) {
                    systemDto.getCefrInfo().get(0).setDispCefrScore2(null);
                } else {
                    systemDto.getCefrInfo().get(0).setDispCefrScore2(Double.valueOf(input.getScore()));
                }
                if (cefrLevel == null) {
                     表示用CEFRレベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrLevelCd2("");
                     表示用CEFRレベル名
                    systemDto.getCefrInfo().get(0).setDispCefrLevel2("");
                     表示用CEFRレベル名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr2("");
                } else {
                     表示用CEFRレベルコード
                    systemDto.getCefrInfo().get(0).setDispCefrLevelCd2(input.getCefrLevelCd());
                     表示用CEFRレベル名
                    systemDto.getCefrInfo().get(0).setDispCefrLevel2(cefrLevel.getCefrName());
                     表示用CEFRレベル名(略称)
                    systemDto.getCefrInfo().get(0).setDispCefrLevelAbbr2(cefrLevel.getCefrAbbr());
                }
                 表示用合否
                if (input.getResult() == null) {
                    systemDto.getCefrInfo().get(0).setDispExamResult2("");
                } else {
                    systemDto.getCefrInfo().get(0).setDispExamResult2(input.getResult());
                }
                 表示用スコア不明
                if (unknownScoreCheckBox.isSelected()) {
                    systemDto.getCefrInfo().get(0).setDispUnknownScore2(Constants.FLG_ON);
                } else {
                    systemDto.getCefrInfo().get(0).setDispUnknownScore2(Constants.FLG_OFF);
                }
                 表示用CEFR不明
                if (unknownCefrCheckBox.isSelected()) {
                    systemDto.getCefrInfo().get(0).setDispUnknownCefr2(Constants.FLG_ON);
                } else {
                    systemDto.getCefrInfo().get(0).setDispUnknownCefr2(Constants.FLG_OFF);
                }
                 表示用スコア補正フラグ
                systemDto.getCefrInfo().get(0).setDispScoreCorrectFlg2(input.getScoreCorrectFlg());
                 表示用CEFRレベルコード補正フラグ
                systemDto.getCefrInfo().get(0).setDispCefrLevelCdCorrectFlg2(input.getCefrLevelCdCorrectFlg());
                 表示用合否補正フラグ
                systemDto.getCefrInfo().get(0).setDispExamResultCorrectFlg2(input.getResultCorrectFlg());
            }
        }
    }*/

    /**
     * 問１〜問３の値から総合評価を求める。
     *
     * @return なし（アクション結果）
     */
    /*
    public ExecuteResult changeJpnWritingComboBox() {
    
        String q1 = view.jpnWriting1ComboBox.getSelectedValue();
        String q2 = view.jpnWriting2ComboBox.getSelectedValue();
        String q3 = view.jpnWriting3ComboBox.getSelectedValue();
        /* 問１〜問３の評価から総合評価を求める
        JpnDescTotalEvaluationBean eval = systemDto.getJpnWritingTotalEvaluationList().stream().filter(e -> e.getQ1Evaluation().equals(q1) && e.getQ2Evaluation().equals(q2) && e.getQ3Evaluation().equals(q3)).findFirst().orElse(null);
        if (eval == null) {
            view.japaneseWField.setText("");
        } else {
            view.japaneseWField.setText(eval.getTotalEvaluation());
        }
        return null;
    }
    */
    /**
     * スコアを文字列に変換する。
     *
     * @param value スコア（double型）
     * @param sw 認定試験スイッチ(1 or 2)
     * @return スコア（String型）
     */
    /*private String getStringScore(double value, SWITCH sw) {
        EngPtBean data = getEngPtField(sw).getSelectedValue();
        EngPtBean exam = systemDto.getEngCertExamList().stream().filter(e -> e.getEngPtCd().equals(data.getEngPtCd())).findFirst().orElse(null);
        if (Constants.FLG_ON.equals(exam.getScoreDecFlg())) {
            return String.valueOf(value);
        } else {
            return String.valueOf((long) value);
        }
    }*/

    /**
     * 認定試験名コンボボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return 認定試験レベルコンボボックス
     */
    /*private ComboBox<EngPtBean> getEngPtField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.joiningExam1ComboBox : view.joiningExam2ComboBox;
    }*/

    /**
     * 認定試験レベルコンボボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return 認定試験レベルコンボボックス
     */
    /*private ComboBox<EngPtDetailBean> getEngPtLevelField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.joiningExamLevel1ComboBox : view.joiningExamLevel2ComboBox;
    }*/

    /**
     * スコアテキストボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return スコアテキストボックス
     */
    /*private TextField getCefrScoreField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.cefrScore1Field : view.cefrScore2Field;
    }*/

    /**
     * スコアが不明チェックボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return スコアが不明チェックボックス
     */
    /*private CodeCheckBox getUnknownCefrScoreField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.unknownCefrScore1CheckBox : view.unknownCefrScore2CheckBox;
    }*/

    /**
     * CEFRレベルコンボボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return CEFRレベルコンボボックス
     */
    /*private ComboBox<CefrBean> getCefrLevelField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.cefrLevel1ComboBox : view.cefrLevel2ComboBox;
    }*/

    /**
     * CEFRレベルが不明チェックボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return CEFRレベルが不明チェックボックス
     */
    /*private CodeCheckBox getUnknownCefrLevelField(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.unknownCefrLevel1CheckBox : view.unknownCefrLevel2CheckBox;
    }*/

    /**
     * 英検合否コンボボックスを取得します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return 英検合否コンボボックス
     */
    /*private ComboBox<EngExamResultBean> getEngExamResultFiled(SWITCH sw) {
        return SWITCH.ENG1.equals(sw) ? view.engExamResult1ComboBox : view.engExamResult2ComboBox;
    }*/

    /**
     * 参加試験名が「英検」かどうかを返却します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return boolean 英検かどうか(true:英検、false:英検以外)
     */
    /*private boolean isEiken(SWITCH sw) {
         認定試験名コンボボックス
        EngPtBean item = getEngPtField(sw).getSelectedValue();
        return item != null && BZConstants.ENG_EXAM_RESULT_CD.equals(item.getEngPtCd());
    }*/

    /**
     * 認定試験が合否判定有無かどうかを返却します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return boolean (true:合否あり、false:合否なし)
     */
    /*private boolean isGouhi(SWITCH sw) {
         認定試験名コンボボックス
        EngPtBean item = getEngPtField(sw).getSelectedValue();
        return BZConstants.FLG_ON.equals(item.getResultFlg());
    }*/

    /**
     * 選択した認定試験のレベル管理有無を返却します。
     *
     * @param sw 認定試験スイッチ(1 or 2)
     * @return true:レベル管理有り、false:レベル管理無し
     */
    /*private boolean isLevel(SWITCH sw) {
         認定試験名コンボボックス
        EngPtBean item = getEngPtField(sw).getSelectedValue();
        boolean result = false;
        if (item != null) {
            result = BZConstants.FLG_ON.equals(item.getLevelFlg());
        }
        return result;
    }*/
}

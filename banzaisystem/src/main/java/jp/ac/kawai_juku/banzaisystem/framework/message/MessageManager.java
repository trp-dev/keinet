package jp.ac.kawai_juku.banzaisystem.framework.message;

import java.io.File;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.seasar.framework.util.ClassLoaderUtil;
import org.seasar.framework.util.InputStreamUtil;
import org.seasar.framework.util.URLUtil;
import org.seasar.framework.util.ZipInputStreamUtil;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * メッセージ管理クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class MessageManager {

    /** Singletonインスタンス */
    private static final Message INSTANCE = new Message();

    /**
     * コンストラクタです。
     */
    private MessageManager() {
    }

    /**
     * メッセージを取得します。
     *
     * @param key メッセージキー
     * @param values 置換パラメータ
     * @return メッセージ
     */
    public static String getMessage(String key, Object... values) {
        return INSTANCE.getMessage(key, values);
    }

    /**
     * メッセージ取得処理の実装クラスです。
     */
    private static final class Message {

        /** メッセージ置換パターン */
        private final Pattern pattern = Pattern.compile("\\{[^0-9].+?\\}");

        /** メッセージキャッシュ */
        private final Map<String, String> messageCache = CollectionsUtil.newConcurrentHashMap();

        /** フォーマットキャッシュ */
        private final Map<String, MessageFormat> fotmatCache = CollectionsUtil.newConcurrentHashMap();

        /** リソース名の配列 */
        private final String[] resourceNames;

        /**
         * コンストラクタです。
         */
        private Message() {

            Set<String> pkgSet = CollectionsUtil.newHashSet();

            for (Iterator<?> ite = ClassLoaderUtil.getResources("jp/ac/kawai_juku"); ite.hasNext();) {
                URL resource = (URL) ite.next();
                String path = resource.getPath();
                if (path.startsWith("file:") && path.contains("!")) {
                    /* 運用環境の場合はjarファイルから情報を得る */
                    ZipInputStream in = null;
                    try {
                        in = new ZipInputStream(URLUtil.openStream(URLUtil.create(StringUtils.split(path, '!')[0])));
                        ZipEntry entry;
                        while ((entry = ZipInputStreamUtil.getNextEntry(in)) != null) {
                            if (entry.getName().startsWith("jp") && entry.getName().endsWith("Message.properties")) {
                                pkgSet.add(new File(entry.getName()).getParent().replace('\\', '.'));
                            }
                            ZipInputStreamUtil.closeEntry(in);
                        }
                    } finally {
                        InputStreamUtil.close(in);
                    }
                } else {
                    /* ローカル開発環境の場合 */
                    for (File file : FileUtils.listFiles(new File(path),
                            FileFilterUtils.suffixFileFilter("Message.properties"), FileFilterUtils.trueFileFilter())) {
                        String pkg = file.getParent().replace('\\', '.');
                        pkgSet.add(pkg.substring(pkg.indexOf("jp.ac.kawai_juku")));
                    }
                }
            }

            List<String> list = CollectionsUtil.newArrayList(pkgSet.size());
            list.add("commonMessage");
            list.add("codeMessage");

            for (String pkg : pkgSet) {
                String name = pkg + pkg.substring(pkg.lastIndexOf('.')) + "Message";
                list.add(name);
            }

            resourceNames = list.toArray(new String[list.size()]);
        }

        /**
         * メッセージを取得します。
         *
         * @param key メッセージキー
         * @param values 置換パラメータ
         * @return メッセージ
         */
        private String getMessage(String key, Object... values) {

            String message = getBundleMessage(key);

            if (values == null || values.length == 0) {
                return message;
            }

            Object[] args = new Object[values.length];
            for (int i = 0; i < values.length; i++) {
                if (values[i] instanceof String) {
                    args[i] = getBundleMessage((String) values[i]);
                } else {
                    args[i] = values[i];
                }
            }

            MessageFormat format = fotmatCache.get(message);
            if (format == null) {
                format = new MessageFormat(escape(message));
                fotmatCache.put(message, format);
            }

            return format.format(args);
        }

        /**
         * メッセージバンドルからメッセージを取得します。
         *
         * @param key メッセージキー
         * @return メッセージ
         */
        private String getBundleMessage(String key) {

            /* キーとして不正ならそのまま返す */
            if (key.indexOf('.') < 0) {
                return key;
            }

            String message = messageCache.get(key);
            if (message == null) {
                for (String resourceName : resourceNames) {
                    ResourceBundle bundle = ResourceBundle.getBundle(resourceName);
                    if (bundle.containsKey(key)) {
                        message = bundle.getString(key);
                        break;
                    }
                }
                if (message == null) {
                    throw new RuntimeException(String.format("メッセージ[%s]が未登録です。", key));
                }
                message = replaceItems(message);
            }

            return message;
        }

        /**
         * 置換対象項目を置換します。
         *
         * @param message 置換対象メッセージ
         * @return 置換後メッセージ
         */
        private String replaceItems(String message) {

            if (StringUtils.isEmpty(message) || message.indexOf('{') < 0) {
                return message;
            }

            StringBuffer sb = new StringBuffer(64);

            Matcher matcher = pattern.matcher(message);
            while (matcher.find()) {
                String matchStr = matcher.group();
                matchStr = matchStr.substring(1, matchStr.length() - 1);
                String replaced = getBundleMessage(matchStr);
                if (!matchStr.equals(replaced) || matchStr.indexOf('.') < 0) {
                    matcher.appendReplacement(sb, replaced);
                }
            }
            matcher.appendTail(sb);

            return sb.toString();
        }

        /**
         * MessageFormat用に文字列をエスケープします。
         *
         * @param string エスケープする文字列
         * @return エスケープした文字列
         */
        private String escape(String string) {

            if (StringUtils.isEmpty(string) || string.indexOf('\'') < 0) {
                return string;
            }

            StringBuilder sb = new StringBuilder(string.length() + 16);

            for (char c : string.toCharArray()) {
                if (c == '\'') {
                    sb.append('\'');
                }
                sb.append(c);
            }

            return sb.toString();
        }

    }

}

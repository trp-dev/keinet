package jp.ac.kawai_juku.banzaisystem.exmntn.main.bean;

/**
 *
 * 国語記述成績保存用のDAO入力Beanです。
 *
 *
 * @author QQ)Nakao.Takaaki
 * @author QQ)Nakao.Takaaki
 */
public class ExmntnMainSaveJpnDaoInBean {

    /** 個人ID */
    private Integer individualId;

    /** 模試年度 */
    private String examYear;

    /** 模試コード */
    private String examDiv;

    /** 科目コード */
    private String subCd;

    /** 国語（記）評価問1 */
    private String question1;

    /** 国語（記）評価問2 */
    private String question2;

    /** 国語（記）評価問3 */
    private String question3;

    /** 国語（記）総合評価 */
    private String jpnTotal;

    /**
     * 個人IDを返します。
     *
     * @return 個人ID
     */
    public Integer getIndividualId() {
        return individualId;
    }

    /**
     * 個人IDをセットします。
     *
     * @param individualId 個人ID
     */
    public void setIndividualId(Integer individualId) {
        this.individualId = individualId;
    }

    /**
     * 模試年度を返します。
     *
     * @return 模試年度
     */
    public String getExamYear() {
        return examYear;
    }

    /**
     * 模試年度をセットします。
     *
     * @param examYear 模試年度
     */
    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    /**
     * 模試コードを返します。
     *
     * @return 模試コード
     */
    public String getExamDiv() {
        return examDiv;
    }

    /**
     * 模試コードをセットします。
     *
     * @param examDiv 模試コード
     */
    public void setExamDiv(String examDiv) {
        this.examDiv = examDiv;
    }

    /**
     * 科目コードを返します。
     *
     * @return 科目コード
     */
    public String getSubCd() {
        return subCd;
    }

    /**
     * 科目コードをセットします。
     *
     * @param subCd 科目コード
     */
    public void setSubCd(String subCd) {
        this.subCd = subCd;
    }

    /**
     * 国語（記）評価問1を返します。
     *
     * @return 国語（記）評価問1
     */
    public String getQuestion1() {
        return question1;
    }

    /**
     * 国語（記）評価問1をセットします。
     *
     * @param question1 国語（記）評価問1
     */
    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    /**
     * 国語（記）評価問2を返します。
     *
     * @return 国語（記）評価問2
     */
    public String getQuestion2() {
        return question2;
    }

    /**
     * 国語（記）評価問2をセットします。
     *
     * @param question2 国語（記）評価問2
     */
    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    /**
     * 国語（記）評価問3を返します。
     *
     * @return 国語（記）評価問3
     */
    public String getQuestion3() {
        return question3;
    }

    /**
     * 国語（記）評価問3をセットします。
     *
     * @param question3 国語（記）評価問3
     */
    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    /**
     * 国語（記）総合評価を返します。
     *
     * @return 国語（記）総合評価
     */
    public String getJpnTotal() {
        return jpnTotal;
    }

    /**
     * 国語（記）総合評価をセットします。
     *
     * @param jpnTotal 国語（記）総合評価
     */
    public void setJpnTotal(String jpnTotal) {
        this.jpnTotal = jpnTotal;
    }

}

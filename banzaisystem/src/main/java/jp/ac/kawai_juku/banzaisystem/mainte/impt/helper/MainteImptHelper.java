package jp.ac.kawai_juku.banzaisystem.mainte.impt.helper;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;

import java.awt.Window;
import java.io.File;

import javax.annotation.Resource;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import jp.ac.kawai_juku.banzaisystem.framework.component.InputFileChooser;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;
import jp.ac.kawai_juku.banzaisystem.framework.controller.TaskResult;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.maker.B03CsvMaker;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.service.MainteImptImportService;

/**
 *
 * メンテナンス-個人成績インポート画面のヘルパーです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class MainteImptHelper {

    /** 個人成績ファイルのファイルフィルタ */
    private final FileFilter filter = new FileNameExtensionFilter("個人成績ファイル（*.csv;*.txt）", "csv", "txt");

    /** 個人成績インポートサービス */
    @Resource
    private MainteImptImportService mainteImptImportService;

    /**
     * 個人成績手動取り込みボタン押下アクションです。
     *
     * @param window アクティブなウィンドウ
     * @param doRun インポート後の処理
     * @return インポートタスク
     */
    public ExecuteResult importCsvAction(Window window, final Runnable doRun) {

        JFileChooser fileChooser = new InputFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(filter);

        int selected = fileChooser.showOpenDialog(window);
        if (selected == JFileChooser.APPROVE_OPTION) {
            final File file = fileChooser.getSelectedFile();
            if (file.length() == 0 || !filter.accept(file)) {
                /* ファイルサイズが0または不正な拡張子の場合はエラーとする */
                throw new MessageDialogException(getMessage("error.mainte.impt.E_E001"));
            } else {
                /* インポート処理はバックグラウンド実行する */
                return new TaskResult() {
                    @Override
                    protected ExecuteResult doTask() {
                        try (B03CsvMaker maker = new B03CsvMaker()) {
                            mainteImptImportService.importCsv(file, maker);
                        }
                        return null;
                    }

                    @Override
                    protected void done() {
                        doRun.run();
                    }
                };
            }
        }

        return null;
    }

}

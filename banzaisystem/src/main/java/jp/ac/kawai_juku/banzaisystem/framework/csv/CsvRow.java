package jp.ac.kawai_juku.banzaisystem.framework.csv;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * CSVの論理的な1行を表現するクラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class CsvRow {

    /** 行番号 */
    private final int rowNum;

    /** 列のリスト */
    private final List<String> columnList;

    /**
     * コンストラクタです。
     *
     * @param rowNum 行番号
     * @param columnList 列リスト
     */
    CsvRow(int rowNum, List<String> columnList) {
        this.rowNum = rowNum;
        this.columnList = columnList;
    }

    /**
     * 行番号を返します。
     *
     * @return 行番号
     */
    public int getRowNum() {
        return rowNum;
    }

    /**
     * この行が空行であるかを返します。
     *
     * @return 空行ならtrue
     */
    public boolean isEmptyRow() {
        return columnList.isEmpty();
    }

    /**
     * 指定した列定義の列文字列を取得します。
     *
     * @param index CSV列インデックス定義
     * @return 列の文字列
     */
    public String get(CIndex index) {
        if (columnList.size() > index.getIndex()) {
            return columnList.get(index.getIndex());
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * 指定した列定義の列文字列を設定します。
     *
     * @param index CSV列インデックス定義
     * @param value 設定する値
     */
    public void set(CIndex index, String value) {
        if (columnList.size() > index.getIndex()) {
            columnList.set(index.getIndex(), value);
        }
    }

    /**
     * 列の数を返します。
     *
     * @return 列の数
     */
    public int getColumnSize() {
        return columnList.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return columnList.toString();
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.judgement;

import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetail;
import jp.co.fj.kawaijuku.judgement.beans.detail.CommonDetailHolder;
import jp.co.fj.kawaijuku.judgement.beans.detail.SecondDetailHolder;
import jp.co.fj.kawaijuku.judgement.data.Irregular1;
import jp.co.fj.kawaijuku.judgement.factory.UnivMasterSubject;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Detail;
import jp.co.fj.kawaijuku.judgement.util.JudgementUtil;

import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * 判定詳細情報生成ファクトリ<br>
 * ※既存PC版模試判定プロジェクトより移植
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 * @param <T> {@link CommonDetail}
 *
 */
abstract class DetailFactory<T extends CommonDetail> {

    /** インデックス：入試試験区分 */
    private static final int IDX_ENTEXAMDIV = 0;

    /** インデックス：大学コード枝番 */
    private static final int IDX_BRANCHCD = IDX_ENTEXAMDIV + 1;

    /** インデックス：優先枝番 */
    private static final int IDX_PRIORITYBRANCHCD = IDX_BRANCHCD + 1;

    /** インデックス：理科同一科目選択不可 */
    private static final int IDX_SAMECHOICE_NG_SC = IDX_PRIORITYBRANCHCD + 1;

    /** インデックス：判定科目名 */
    private static final int IDX_SUBCOUNT = IDX_SAMECHOICE_NG_SC + 1;

    /** インデックス：総合配点 */
    private static final int IDX_TOTALALLOTPNT = IDX_SUBCOUNT + 1;

    /** インデックス：総合配点＿小論文なし */
    private static final int IDX_TOTALALLOTPNT_NREP = IDX_TOTALALLOTPNT + 1;

    /** インデックス：入試満点値 */
    private static final int IDX_ENTEXAMFULLPNT = IDX_TOTALALLOTPNT_NREP + 1;

    /** インデックス：入試満点値＿小論文なし */
    private static final int IDX_ENTEXAMFULLPNT_NREP = IDX_ENTEXAMFULLPNT + 1;

    /** インデックス：入試必須科目数 */
    private static final int IDX_NECESSUBCOUNT = IDX_ENTEXAMFULLPNT_NREP + 1;

    /** インデックス：入試選択科目数 */
    private static final int IDX_CHOICESUBCOUNT = IDX_NECESSUBCOUNT + 11;

    /** インデックス：グループ別科目別科目数 */
    private static final int IDX_GROUPSUBCOUNT = IDX_CHOICESUBCOUNT + 10;

    /** インデックス：同一科目選択区分 */
    private static final int IDX_CEN_SECSUBCHOICEDIV = IDX_GROUPSUBCOUNT + 25;

    /** インデックス：第一解答科目フラグ */
    private static final int IDX_FIRSTANS_FLG = IDX_CEN_SECSUBCHOICEDIV + 5;

    /** インデックス：得点換算パターンコード−英語参加試験 */
    private static final int IDX_PATTERNCODEEIGOSS = IDX_FIRSTANS_FLG + 5;

    /** インデックス：加点区分−英語参加試験 */
    private static final int IDX_ENGKATENKUBUN = IDX_PATTERNCODEEIGOSS + 1;

    /** インデックス：加点配点−英語参加試験 */
    private static final int IDX_ENGKATENHAITEN = IDX_ENGKATENKUBUN + 1;

    /** インデックス：得点換算パターンコード−国語記述式 */
    private static final int IDX_TKPATTERNCODEKOKUGOKJS = IDX_ENGKATENHAITEN + 1;

    /** インデックス：加点区分−国語記述式 */
    private static final int IDX_JPNKATENKUBUN = IDX_TKPATTERNCODEKOKUGOKJS + 1;

    /** インデックス：加点配点−国語記述式 */
    private static final int IDX_JPNKATENHAITEN = IDX_JPNKATENKUBUN + 1;

    /** インデックス：最善枝番ソート用得点換算利用区分 */
    private static final int IDX_BESTBRANCHNOSORTTKKANSANKBN = IDX_JPNKATENHAITEN + 1;

    /** インデックス：科目ビット */
    private static final int IDX_SUBBIT = IDX_BESTBRANCHNOSORTTKKANSANKBN + 1;

    /** 選択グループ区切り文字 */
    protected static final String GROUP_SEP = ":";

    /** マスタ配列 */
    private final String[] values;

    /**
     * コンストラクタです。
     *
     * @param values マスタ配列
     */
    DetailFactory(String[] values) {
        this.values = values;
    }

    /**
     * 科目ビットインデックスを返します。
     *
     *
     * @return 科目ビットインデックス
     */
    protected static final int getSubBitIndex() {
        return IDX_SUBBIT;
    }

    /**
     * 詳細データインスタンスを生成します。
     *
     * @return {@link CommonDetail}
     */
    protected abstract T createDetail();

    /**
     * 科目情報を設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     */
    protected abstract void prepareSubject(CommonDetailHolder detailHolder);

    /**
     * イレギュラ区分のインデックスを返します。
     *
     * @return イレギュラ区分のインデックス
     */
    protected abstract int getIrregularDivIndex();

    /**
     * 判定詳細情報を生成します。
     *
     * @return {@link CommonDetail}
     */
    T create() {
        T detail = createDetail();
        CommonDetailHolder detailHolder = detail.getForJudgement();
        prepare(detailHolder);
        prepare(detail);
        return detail;
    }

    /**
     * 大学詳細データホルダを設定します。
     *
     * @param detailHolder {@link CommonDetailHolder}
     */
    private void prepare(CommonDetailHolder detailHolder) {

        /* 必須教科科目数・教科配点 */
        for (int courseIndex = 0; courseIndex < 5; courseIndex++) {
            int subCountIndex = IDX_NECESSUBCOUNT + courseIndex;
            detailHolder.setRequiredCourse(courseIndex, getInt(subCountIndex), getAllotPnt(subCountIndex + 5));
        }

        /* 必須教科配点（その他） */
        if (detailHolder instanceof SecondDetailHolder) {
            ((SecondDetailHolder) detailHolder).setOtherAllot(getAllotPnt(IDX_CHOICESUBCOUNT - 1));
        }

        /* グループ科目数・グループ配点 */
        for (int groupIndex = 0; groupIndex < 5; groupIndex++) {
            int subCountIndex = IDX_CHOICESUBCOUNT + groupIndex;
            detailHolder.setElectiveGroup(groupIndex, getInt(subCountIndex), getAllotPnt(subCountIndex + 5));
        }

        /* グループ別科目別科目数 */
        for (int courseIndex = 0; courseIndex < 5; courseIndex++) {
            for (int groupIndex = 0; groupIndex < 5; groupIndex++) {
                detailHolder.setElectiveGroupCourse(groupIndex, courseIndex, getInt(IDX_GROUPSUBCOUNT + courseIndex * 5 + groupIndex));
            }
        }

        /* 第一解答科目フラグ */
        for (int courseIndex = 0; courseIndex < 5; courseIndex++) {
            detailHolder.setFirstAnsFlg(courseIndex, getInt(IDX_FIRSTANS_FLG + courseIndex));
        }

        /* 理科同一科目選択不可 */
        detailHolder.setSamechoiceNgSc(getString(IDX_SAMECHOICE_NG_SC));

        /* 課し科目 */
        prepareSubject(detailHolder);

        /* 得点換算パターンコード−英語認定試験 */
        detailHolder.setTkPatternCodeEigoSs(getString(IDX_PATTERNCODEEIGOSS));

        /* 加点区分−英語参加試験 */
        detailHolder.setEngKatenKubun(getString(IDX_ENGKATENKUBUN));

        /* 加点配点−英語参加試験 */
        detailHolder.setEngKatenHaiten(getAllotPnt(IDX_ENGKATENHAITEN));

        /* 得点換算パターンコード−国語記述式 */
        detailHolder.setTkPatternCodeKokugoKjs(getString(IDX_TKPATTERNCODEKOKUGOKJS));

        /* 加点区分−国語記述式 */
        detailHolder.setJpnKatenKubun(getString(IDX_JPNKATENKUBUN));

        /* 加点配点−国語記述式 */
        detailHolder.setJpnKatenHaiten(getAllotPnt(IDX_JPNKATENHAITEN));

        /* 最善枝番ソート用得点換算利用区分 */
        detailHolder.setBestBranchNoSorttkKansanKbn(getAllotPnt(IDX_BESTBRANCHNOSORTTKKANSANKBN));

    }

    /**
     * 大学詳細データを設定します。
     *
     * @param detail {@link CommonDetail}
     */
    private void prepare(CommonDetail detail) {

        String branchCd = getString(IDX_BRANCHCD);

        /* 枝番 */
        detail.setBranchCode(branchCd);

        /* 優先枝番 */
        detail.setPriorityBranchCd(getInt(IDX_PRIORITYBRANCHCD));

        /* 判定科目名 */
        detail.setSubString(getString(IDX_SUBCOUNT));

        /* 総合配点 */
        detail.setAllotPntRateAll(JudgementUtil.check9(getInt(IDX_TOTALALLOTPNT)));

        /* 総合配点＿小論文なし */
        detail.setAllotPntRate(JudgementUtil.check9(getInt(IDX_TOTALALLOTPNT_NREP)));

        /* 入試満点値 */
        detail.setFullPnt(JudgementUtil.check9(getInt(IDX_ENTEXAMFULLPNT)));

        /* 入試満点値＿小論文なし */
        detail.setFullPointNrep(JudgementUtil.check9(getInt(IDX_ENTEXAMFULLPNT_NREP)));

        /* センター二次同一科目選択区分 */
        for (int courseIndex = 0; courseIndex < 5; courseIndex++) {
            if (getInt(IDX_CEN_SECSUBCHOICEDIV + courseIndex) == 1) {
                detail.setUnadoptables(courseIndex);
            }
        }

        /* イレギュラ�@ */
        if (getInt(getIrregularDivIndex()) == 1) {
            detail.setUnivIrregular1(createIrregular1(createIrr1SubPnt(), createIrr1MaxSelect(), createIrr1SubCount()));
        }

        /* セットアップ処理を呼び出す */
        detail.setup();
    }

    /**
     * イレギュラ情報�@を生成します。
     *
     * @param irregular 配点
     * @param maxSelect 教科別最大科目数
     * @param irrSub 科目数
     * @return {@link Irregular1}
     */
    protected abstract Irregular1 createIrregular1(double[] irregular, int[][] maxSelect, int[] irrSub);

    /**
     * イレギュラ配点配列を生成します。
     *
     * @return イレギュラ配点配列
     */
    private double[] createIrr1SubPnt() {
        int start = getIrregularDivIndex() + 1;
        double[] array = new double[6];
        for (int i = 0; i < array.length; i++) {
            array[i] = getAllotPnt(start + i);
        }
        return array;
    }

    /**
     * イレギュラ最大選択科目数配列を生成します。
     *
     * @return イレギュラ最大選択科目数配列
     */
    private int[][] createIrr1MaxSelect() {
        int start = getIrregularDivIndex() + 6 + 1;
        int[][] array = new int[6][5];
        for (int groupIndex = 0; groupIndex < array.length; groupIndex++) {
            for (int courseIndex = 0; courseIndex < array[groupIndex].length; courseIndex++) {
                array[groupIndex][courseIndex] = getInt(start + groupIndex + courseIndex * array.length);
            }
        }
        return array;
    }

    /**
     * イレギュラ科目数配列を生成します。
     *
     * @return イレギュラ科目数配列
     */
    private int[] createIrr1SubCount() {
        int start = getIrregularDivIndex() + 6 + 30 + 1;
        int[] array = new int[6];
        for (int i = 0; i < array.length; i++) {
            array[i] = getInt(start + i);
        }
        return array;
    }

    /**
     * 指定したインデックスのマスタ値（文字列）を返します。
     *
     * @param index インデックス
     * @return マスタ値（文字列）
     */
    protected final String getString(int index) {
        return values[index];
    }

    /**
     * 指定したインデックスのマスタ値（int）を返します。
     *
     * @param index インデックス
     * @return マスタ値（int）
     */
    protected final int getInt(int index) {
        return NumberUtils.toInt(getString(index));
    }

    /**
     * 指定したインデックスの配点値（double）を返します。
     *
     * @param index インデックス
     * @return 配点値（double）
     */
    protected final double getAllotPnt(int index) {
        /* 空文字列は無効配点に変換する */
        return NumberUtils.toDouble(getString(index), Detail.ALLOT_NA);
    }

    /**
     * 科目データを生成します。
     *
     * @param subBit 科目ビット
     * @param subPnt 科目配点
     * @param subArea 範囲区分
     * @param concurrentNg 同時選択不可区分
     * @return {@link UnivMasterSubject}
     */
    protected final UnivMasterSubject createSubject(String subBit, double subPnt, String subArea, String concurrentNg) {
        return new UnivMasterSubject(subBit, subPnt, subArea, concurrentNg);
    }

    /**
     * 配列から指定されたインデックスの値を取得します。
     *
     * @param array 配列
     * @param index インデックス
     * @return 値
     */
    protected final String getArrayValue(String[] array, int index) {
        return array.length - 1 < index ? null : array[index];
    }
}

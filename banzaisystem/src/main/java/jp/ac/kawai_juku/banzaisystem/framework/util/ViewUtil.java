package jp.ac.kawai_juku.banzaisystem.framework.util;

/**
 *
 * ビューに関するユーティリティです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ViewUtil {

    /**
     * コンストラクタです。
     */
    private ViewUtil() {
    }

}

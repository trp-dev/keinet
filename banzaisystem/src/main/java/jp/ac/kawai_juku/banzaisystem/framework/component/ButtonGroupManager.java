package jp.ac.kawai_juku.banzaisystem.framework.component;

import java.util.Map;

import javax.swing.ButtonGroup;

import jp.ac.kawai_juku.banzaisystem.framework.view.AbstractView;

import org.apache.commons.lang3.tuple.Pair;
import org.seasar.framework.util.tiger.CollectionsUtil;

/**
 *
 * ボタングループ管理クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class ButtonGroupManager {

    /** ボタングループマップ */
    private static final Map<Pair<AbstractView, Object>, ButtonGroup> GROUP_MAP = CollectionsUtil.newHashMap();

    /**
     * コンストラクタです。
     */
    private ButtonGroupManager() {
    }

    /**
     * 指定したキーのボタングループを返します。
     *
     * @param view ビュー
     * @param key キー
     * @return {@link ButtonGroup}
     */
    public static ButtonGroup getButtonGroup(AbstractView view, Object key) {
        Pair<AbstractView, Object> mapKey = Pair.of(view, key);
        ButtonGroup group = GROUP_MAP.get(mapKey);
        if (group == null) {
            group = new ButtonGroup();
            GROUP_MAP.put(mapKey, group);
        }
        return group;
    }

}

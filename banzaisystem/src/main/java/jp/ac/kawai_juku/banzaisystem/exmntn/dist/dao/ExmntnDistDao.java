package jp.ac.kawai_juku.banzaisystem.exmntn.dist.dao;

import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCandidateDetailBean;
import jp.ac.kawai_juku.banzaisystem.exmntn.dist.bean.ExmntnDistCapacityBean;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.ac.kawai_juku.banzaisystem.framework.dao.base.BaseDao;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.framework.beans.util.BeanMap;

/**
 *
 * 志望大学学力分布画面のDAOです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class ExmntnDistDao extends BaseDao {

    /**
     * 募集定員情報を取得します。
     *
     * @param univKeyBean {@link UnivKeyBean}
     * @return {@link ExmntnDistCapacityBean}
     */
    public ExmntnDistCapacityBean selectCapacityBean(UnivKeyBean univKeyBean) {
        return jdbcManager.selectBySqlFile(ExmntnDistCapacityBean.class, getSqlPath(), univKeyBean).getSingleResult();
    }

    /**
     * 志望大学学力分布を取得します。
     *
     * @param exam 対象模試
     * @param univKeyBean {@link UnivKeyBean}
     * @return {@link ExmntnDistCandidateDetailBean}
     */
    public BeanMap selectUnivDistRecord(ExamBean exam, UnivKeyBean univKeyBean) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("univCd", univKeyBean.getUnivCd());
        param.put("facultyCd", univKeyBean.getFacultyCd());
        param.put("deptCd", univKeyBean.getDeptCd());
        return jdbcManager.selectBySqlFile(BeanMap.class, getSqlPath(), param).getSingleResult();
    }

    /**
     * 志願者内訳を取得します。
     *
     * @param exam 対象模試
     * @param univKeyBean {@link UnivKeyBean}
     * @param pattern 志望集計パターン
     * @return {@link ExmntnDistCandidateDetailBean}
     */
    public ExmntnDistCandidateDetailBean selectCandidateDetail(ExamBean exam, UnivKeyBean univKeyBean, Code pattern) {
        BeanMap param = new BeanMap();
        param.put("examYear", exam.getExamYear());
        param.put("examCd", exam.getExamCd());
        param.put("univCd", univKeyBean.getUnivCd());
        param.put("facultyCd", univKeyBean.getFacultyCd());
        param.put("deptCd", univKeyBean.getDeptCd());
        param.put("patternCd", pattern.getValue());
        return jdbcManager.selectBySqlFile(ExmntnDistCandidateDetailBean.class, getSqlPath(), param).getSingleResult();
    }

}

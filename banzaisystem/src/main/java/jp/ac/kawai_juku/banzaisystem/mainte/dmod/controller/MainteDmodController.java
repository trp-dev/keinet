package jp.ac.kawai_juku.banzaisystem.mainte.dmod.controller;

import jp.ac.kawai_juku.banzaisystem.framework.component.annotation.Execute;
import jp.ac.kawai_juku.banzaisystem.framework.controller.AbstractController;
import jp.ac.kawai_juku.banzaisystem.framework.controller.ExecuteResult;

/**
 *
 * 基準データ修正ダイアログのコントローラです。
 *
 *
 * @author TOTEC)Morita.Yuuichirou
 *
 */
public class MainteDmodController extends AbstractController {

    /**
     * 初期表示アクションです。
     *
     * @return 基準データ修正ダイアログ
     */
    @Execute
    public ExecuteResult index() {
        return VIEW_RESULT;
    }

    /**
     * OKボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult okButtonAction() {
        return CLOSE_RESULT;
    }

    /**
     * キャンセルボタンアクションです。
     *
     * @return なし（ダイアログを閉じる）
     */
    @Execute
    public ExecuteResult cancelButtonAction() {
        return CLOSE_RESULT;
    }

}

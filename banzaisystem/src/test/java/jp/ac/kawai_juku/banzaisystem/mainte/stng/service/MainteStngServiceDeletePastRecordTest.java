package jp.ac.kawai_juku.banzaisystem.mainte.stng.service;

import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;

import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link MainteStngService} の過年度データ削除処理のテストクラス
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/MainteStngServiceDeletePastRecordTest を作業ディレクトリとすること。</li>
 * <li>2015年度の01区分以降の大学マスタを使うこと。</li>
 * <li>2014年度と2015年度のデータが登録された成績データファイルを使うこと。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class MainteStngServiceDeletePastRecordTest extends BZTeacherTest {

    @Resource
    private MainteStngService service;

    /**
     * {@link MainteStngService#deletePastRecord()} のためのテスト・メソッド。
     */
    public void testDeletePastRecord() {

        /* 2014年度の個人IDセットを作る */
        Set<String> idSet = createIdSet();

        /* 過年度データを削除する */
        service.deletePastRecord(systemDto.getUnivYear());

        /* 学籍基本情報をチェックする */
        checkBasicInfo(idSet);

        /* 学籍履歴情報をチェックする */
        checkHistoryInfo();

        /* 成績データをチェックする */
        checkRecord("SUBRECORD_I");

        /* 模試記入志望大学をチェックする */
        checkRecord("CANDIDATERATING");

        /* 志望大学をチェックする */
        checkRecord("CANDIDATEUNIV");

        /* 個表データをチェックする */
        checkRecord("INDIVIDUALRECORD");

        /* 個人成績インポート状況をチェックする */
        checkRecord("STAT_SUBRECORD_I");

        /* 受験予定大学をチェックする */
        checkExamPlanUniv(idSet);
    }

    /**
     * 2014年度の個人IDセットを作ります。
     *
     * @return 2014年度の個人IDセット
     */
    private Set<String> createIdSet() {
        Set<String> idSet = new HashSet<>();
        DataTable table = da.readDbByTable("HISTORYINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            if ("2014".equals(row.getValue("YEAR"))) {
                idSet.add(row.getValue("INDIVIDUALID").toString());
            }
        }
        return idSet;
    }

    /**
     * 学籍基本情報をチェックします。
     *
     * @param idSet 2014年度の個人IDセット
     */
    private void checkBasicInfo(Set<String> idSet) {
        DataTable table = da.readDbByTable("BASICINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!idSet.contains(row.getValue("INDIVIDUALID").toString()));
        }
    }

    /**
     * 学籍履歴情報をチェックします。
     */
    private void checkHistoryInfo() {
        DataTable table = da.readDbByTable("HISTORYINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!"2014".equals(row.getValue("YEAR")));
        }
    }

    /**
     * 成績データ・模試記入志望大学・志望大学・個表データ・個人成績インポート状況をチェックします。
     *
     * @param tabelName テーブル名
     */
    private void checkRecord(String tabelName) {
        DataTable table = da.readDbByTable(tabelName);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!"2014".equals(row.getValue("EXAMYEAR")));
        }
    }

    /**
     * 受験予定大学をチェックします。
     *
     * @param idSet 2014年度の個人IDセット
     */
    private void checkExamPlanUniv(Set<String> idSet) {
        DataTable table = da.readDbByTable("EXAMPLANUNIV");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!idSet.contains(row.getValue("INDIVIDUALID").toString()) && !"2014".equals(row.getValue("YEAR")));
        }
    }

}

package jp.ac.kawai_juku.banzaisystem;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.UNIV_FILE_NAME;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.unit.DataAccessor;
import org.seasar.framework.unit.TestContext;
import org.seasar.framework.unit.annotation.PostBindFields;

/**
 *
 * バンザイシステムの生徒用モード用のテスト基本クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZStudentTest {

    /** 大学マスタファイルフォルダ */
    protected static final File DB_DIR = new File("db");

    /** 大学マスタファイル */
    protected static final File UNIV_FILE = new File(DB_DIR, UNIV_FILE_NAME);

    /** DataAccessor */
    @Resource
    protected DataAccessor da;

    /** TestContext */
    @Resource
    protected TestContext tx;

    /** システム情報DTO */
    @Resource
    protected SystemDto systemDto;

    /**
     * ログファイルを全て削除します。
     */
    @BeforeClass
    public static void deleteLog() {
        try {
            FileUtils.cleanDirectory(SettingPropsUtil.getLogDir());
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

    /**
     * 初期化します。
     */
    @PostBindFields
    public void initialize() {
        /* マスタ情報を初期化する */
        getComponent(InitializeService.class).initMaster();
    }

}

package jp.ac.kawai_juku.banzaisystem.selstd.dadd.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.CANDIDATE_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.EXAM_PLAN_UNIV_MAX;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_OFF;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.FLG_ON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;
import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager;
import jp.ac.kawai_juku.banzaisystem.framework.properties.SettingPropsUtil;
import jp.ac.kawai_juku.banzaisystem.framework.util.Counter;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.bean.SelstdDaddKojinBean;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.dao.SelstdDaddDao;
import jp.ac.kawai_juku.banzaisystem.selstd.dadd.service.SelstdDaddService.AggregationResult;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants.Exam;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.exception.IORuntimeException;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link SelstdDaddService} の名寄せ処理のテストクラス
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/SelstdDaddServiceAggregationTest を作業ディレクトリとすること。</li>
 * <li>名寄せ処理確認用データが登録された成績データファイルを使うこと。</li>
 * <li>成績データの模試区分に合う大学マスタを使うこと。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class SelstdDaddServiceAggregationTest extends BZTeacherTest {

    /** 主情報の個人ID */
    private static final BigDecimal MAIN_ID = BigDecimal.valueOf(1);

    /** 従情報の個人ID */
    private static final BigDecimal SUB_ID = BigDecimal.valueOf(3);

    /** 従情報と同じ模試成績を持つ個人ID */
    private static final BigDecimal CONFRICT_ID = BigDecimal.valueOf(2);

    /** 模試コード公開順定義 */
    private static final String[] EXAM_CD_ORDER = { Exam.Code.MARK1, Exam.Code.WRTN1, Exam.Code.MARK2, Exam.Code.WRTN2,
            Exam.Code.MARK3, Exam.Code.WRTN3, Exam.Code.CENTERPRE, Exam.Code.CENTERRESEARCH,
            Exam.Code.SECONDGRADE_MARK, Exam.Code.WRTN_2GRADE };

    @Resource
    private SelstdDaddDao dao;

    @Resource
    private SelstdDaddService service;

    /**
     * {@link SelstdDaddDao#selectLatestExamBean()} のためのテスト・メソッド。
     */
    public void testSelectLatestExamBean() {

        String nendo = systemDto.getUnivYear();
        String examDiv = systemDto.getUnivExamDiv();

        /* 存在しない個人IDを与え、全ての模試区分で最新模試コードが返ることを確認する */
        assertEquals(Exam.Code.MARK1, dao.selectLatestExamBean(0, nendo, "01").getExamCd());
        assertEquals(Exam.Code.WRTN1, dao.selectLatestExamBean(0, nendo, "02").getExamCd());
        assertEquals(Exam.Code.MARK2, dao.selectLatestExamBean(0, nendo, "03").getExamCd());
        assertEquals(Exam.Code.WRTN2, dao.selectLatestExamBean(0, nendo, "04").getExamCd());
        assertEquals(Exam.Code.WRTN3, dao.selectLatestExamBean(0, nendo, "05").getExamCd());
        assertEquals(Exam.Code.CENTERPRE, dao.selectLatestExamBean(0, nendo, "06").getExamCd());
        assertEquals(Exam.Code.CENTERRESEARCH, dao.selectLatestExamBean(0, nendo, "07").getExamCd());
        assertEquals(Exam.Code.WRTN_2GRADE, dao.selectLatestExamBean(0, nendo, "08").getExamCd());

        /* 特定生徒の最新受験模試をチェックする */
        List<DataRow> beforeList = getRowList("CANDIDATEUNIV");
        assertEquals(getLatestExamCd(beforeList, MAIN_ID), dao.selectLatestExamBean(MAIN_ID.intValue(), nendo, examDiv)
                .getExamCd());
        assertEquals(getLatestExamCd(beforeList, SUB_ID), dao.selectLatestExamBean(SUB_ID.intValue(), nendo, examDiv)
                .getExamCd());
        assertEquals(getLatestExamCd(beforeList, CONFRICT_ID),
                dao.selectLatestExamBean(CONFRICT_ID.intValue(), nendo, examDiv).getExamCd());
    }

    /**
     * 指定生徒の最新受験模試コードを返します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     * @param id 個人ID
     * @return 最新受験模試コード
     */
    private String getLatestExamCd(List<DataRow> beforeList, BigDecimal id) {
        return getLatestExamCd(createExamCdSet(beforeList, id));
    }

    /**
     * 模試コードセットの中で最新の模試コードを返します。
     *
     * @param examCdSet 模試コードセット
     * @return 最新の模試コード
     */
    private String getLatestExamCd(Set<String> examCdSet) {
        int index = -1;
        for (String examCd : examCdSet) {
            index = Math.max(index, ArrayUtils.indexOf(EXAM_CD_ORDER, examCd));
        }
        return EXAM_CD_ORDER[index];
    }

    /**
     * {@link SelstdDaddService#doNameBasedAggregation()} のためのテスト・メソッド。
     */
    public void testDoNameBasedAggregation() {

        /* 主情報を取得する */
        SelstdDaddKojinBean mainBean = dao.selectKojinBean(systemDto.getUnivYear(), MAIN_ID.intValue());
        Integer grade = mainBean.getGrade();
        String cls = StringUtils.stripStart(mainBean.getCls(), "0");
        String classNo = StringUtils.stripStart(mainBean.getClassNo(), "0");
        String nameKana = mainBean.getNameKana() + " ﾞﾟ";

        /* 従情報を取得する */
        SelstdDaddKojinBean subBean = dao.selectKojinBean(systemDto.getUnivYear(), SUB_ID.intValue());

        /* 従情報と同じ模試成績を持つ生徒情報を取得する */
        SelstdDaddKojinBean conBean = dao.selectKojinBean(systemDto.getUnivYear(), CONFRICT_ID.intValue());

        /* 新規登録の場合は名寄せしない */
        assertEquals(AggregationResult.FAILURE,
                service.doNameBasedAggregation(systemDto.getUnivYear(), null, grade, cls, classNo, nameKana, null));

        /* クラスが未入力の場合は名寄せしない */
        assertEquals(AggregationResult.FAILURE, service.doNameBasedAggregation(systemDto.getUnivYear(),
                SUB_ID.intValue(), grade, "", classNo, nameKana, null));

        /* クラス番号が未入力の場合は名寄せしない */
        assertEquals(AggregationResult.FAILURE, service.doNameBasedAggregation(systemDto.getUnivYear(),
                SUB_ID.intValue(), grade, cls, "", nameKana, null));

        /* カナ氏名が未入力の場合は名寄せしない */
        assertEquals(AggregationResult.FAILURE, service.doNameBasedAggregation(systemDto.getUnivYear(),
                SUB_ID.intValue(), grade, cls, classNo, "", null));

        /* カナ氏名が不一致の場合は名寄せしない */
        assertEquals(AggregationResult.FAILURE, service.doNameBasedAggregation(systemDto.getUnivYear(),
                SUB_ID.intValue(), grade, cls, classNo, nameKana + "ｱ", null));

        /* 同じ模試の成績が存在する場合はエラーダイアログ表示 */
        try {
            service.doNameBasedAggregation(systemDto.getUnivYear(), SUB_ID.intValue(), conBean.getGrade(),
                    conBean.getCls(), conBean.getClassNo(), conBean.getNameKana(), null);
            fail();
        } catch (MessageDialogException e) {
            assertEquals(MessageManager.getMessage("error.selstd.dadd.B-d3_E007"), e.getMessage());
        }

        /* 名寄せ前の個表データを取得する */
        List<DataRow> individualRecordList = getRowList("INDIVIDUALRECORD");

        /* 名寄せ前の科目別成績を取得する */
        List<DataRow> subRecordList = getRowList("SUBRECORD_I");

        /* 名寄せ前の模試記入志望大学を取得する */
        List<DataRow> candidateRatingList = getRowList("CANDIDATERATING");

        /* 名寄せ前の受験予定大学を取得する */
        List<DataRow> examPlanUnivList = getRowList("EXAMPLANUNIV");

        /* 名寄せ前の志望大学を取得する */
        DataTable candidateUnivTable = da.readDbByTable("CANDIDATEUNIV");

        /* 名寄せ実行 */
        assertEquals(AggregationResult.SUCCESS, service.doNameBasedAggregation(systemDto.getUnivYear(),
                SUB_ID.intValue(), grade, cls, classNo, nameKana, null));

        /* 結果チェック：学籍基本情報 */
        checkIndividualId(da.readDbByTable("BASICINFO"));

        /* 結果チェック：学籍履歴情報 */
        checkIndividualId(da.readDbByTable("HISTORYINFO"));

        /* 結果チェック：個表データ */
        checkRecordList("INDIVIDUALRECORD", individualRecordList);

        /* 結果チェック：科目別成績 */
        checkRecordList("SUBRECORD_I", subRecordList);

        /* 結果チェック：模試記入志望大学 */
        checkRecordList("CANDIDATERATING", candidateRatingList);

        /* 結果チェック：受験予定大学 */
        checkExamPlanUniv(examPlanUnivList);

        /* 結果チェック：志望大学 */
        checkCandidateUniv(candidateUnivTable);

        /* ログチェック */
        checkLog(mainBean, subBean);
    }

    /**
     * 行リストを取得します。
     *
     * @param tableName テーブル名
     * @return 行リスト
     */
    private List<DataRow> getRowList(String tableName) {
        return toRowList(da.readDbByTable(tableName));
    }

    /**
     * {@link DataTable} を行リストに変換します。
     *
     * @param table {@link DataTable}
     * @return 行リスト
     */
    private List<DataRow> toRowList(DataTable table) {
        List<DataRow> list = new ArrayList<>();
        for (int i = 0; i < table.getRowSize(); i++) {
            list.add(table.getRow(i));
        }
        return list;
    }

    /**
     * レコードに含まれる個人IDをチェックします。
     *
     * @param table {@link DataTable}
     */
    private void checkIndividualId(DataTable table) {

        Set<BigDecimal> idSet = new HashSet<>();
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            idSet.add(toIndividualId(row));
        }

        assertTrue(idSet.size() >= 2);
        assertTrue(idSet.contains(MAIN_ID));
        assertTrue(idSet.contains(CONFRICT_ID));
        assertTrue(!idSet.contains(SUB_ID));
    }

    /**
     * 従情報の受験予定大学が主情報に書き換わっているかチェックします。<br>
     * ※SQLを使わずに、Javaロジックで名寄せした結果と一致することを確認。
     *
     * @param beforeList 名寄せ前の受験予定大学
     */
    private void checkExamPlanUniv(List<DataRow> beforeList) {

        /* 主情報の大学キーセットを生成する */
        Set<String> mainKeySet = new HashSet<>();
        for (DataRow row : beforeList) {
            if (MAIN_ID.equals(toIndividualId(row))) {
                mainKeySet.add(toUnivKey(row));
            }
        }

        for (Iterator<DataRow> ite = beforeList.iterator(); ite.hasNext();) {
            DataRow row = ite.next();
            if (SUB_ID.equals(toIndividualId(row))) {
                if (mainKeySet.contains(toUnivKey(row))) {
                    /* 主情報と重複する従情報を削除する */
                    ite.remove();
                } else {
                    /* 従情報の志望順位を登録上限数だけ加算しておく */
                    row.setValue("CANDIDATERANK",
                            ((BigDecimal) row.getValue("CANDIDATERANK")).add(BigDecimal.valueOf(EXAM_PLAN_UNIV_MAX)));
                    /* 個人IDを主情報に差し替えておく */
                    row.setValue("INDIVIDUALID", MAIN_ID);
                }
            }
        }

        /* 志望順位順にする */
        SortedMap<BigDecimal, DataRow> beforeMap = new TreeMap<>();
        for (DataRow row : beforeList) {
            BigDecimal id = toIndividualId(row);
            if (MAIN_ID.equals(id)) {
                beforeMap.put((BigDecimal) row.getValue("CANDIDATERANK"), row);
            }
        }

        /* 志望順位を連番にする */
        int rank = 1;
        for (DataRow row : beforeMap.values()) {
            row.setValue("CANDIDATERANK", BigDecimal.valueOf(rank++));
        }

        /* 登録上限を超えるものを削除する */
        while (beforeMap.values().size() > EXAM_PLAN_UNIV_MAX) {
            beforeList.remove(beforeMap.remove(beforeMap.lastKey()));
        }

        List<DataRow> afterList = getRowList("EXAMPLANUNIV");
        for (DataRow row : beforeList) {
            assertTrue(afterList.remove(row));
        }
        assertTrue(afterList.isEmpty());
    }

    /**
     * 指定したテーブルのレコードをチェックします。
     *
     * @param tableName テーブル名
     * @param beforeList 名寄せ前の行リスト
     */
    private void checkRecordList(String tableName, List<DataRow> beforeList) {
        List<DataRow> afterList = getRowList(tableName);
        assertTrue(!afterList.isEmpty());
        for (DataRow row : beforeList) {
            /* 名寄せ前の従情報は主情報に変わっているはずなので、個人IDを主情報に差し替え */
            if (SUB_ID.equals(toIndividualId(row))) {
                row.setValue("INDIVIDUALID", MAIN_ID);
            }
            assertTrue(afterList.remove(row));
        }
        assertTrue(afterList.isEmpty());
    }

    /**
     * 従情報の志望大学が主情報に書き換わっているかチェックします。<br>
     * ※SQLを使わずに、Javaロジックで名寄せした結果と一致することを確認。
     *
     * @param beforeTable 名寄せ前の志望大学テーブル
     */
    private void checkCandidateUniv(DataTable beforeTable) {

        List<DataRow> beforeList = toRowList(beforeTable);

        /* 受験模試コードセットを作成する */
        Set<String> mainExamCdSet = createExamCdSet(beforeList, MAIN_ID);
        Set<String> subExamCdSet = createExamCdSet(beforeList, SUB_ID);

        /* 準備処理を行う（名寄せ処理「�@�B」と「�A�C」に相当） */
        prepareCandidateUniv(beforeTable, beforeList, MAIN_ID, mainExamCdSet);
        prepareCandidateUniv(beforeTable, beforeList, SUB_ID, subExamCdSet);

        /* 重複を削除する（名寄せ処理�Dと�Eに相当） */
        deleteDuplicatedCandidateUniv(beforeList, SUB_ID, MAIN_ID, subExamCdSet);
        deleteDuplicatedCandidateUniv(beforeList, MAIN_ID, SUB_ID, mainExamCdSet);

        /* 主従情報が未受験である模試のシステム登録志望大学の志望順位を加算する（名寄せ処理�Fに相当） */
        addCandidateUnivDispNum(beforeList, subExamCdSet);

        /* 主生徒のシステム登録志望大学のうち、模試記入志望大学と重複するものを非表示にする（名寄せ処理�Hに相当） */
        hideDuplicatedCandidateUniv(beforeList);

        /* 登録上限を超える主生徒のシステム登録志望大学を削除する（名寄せ処理�Iに相当） */
        deleteExcessCandidateUniv(beforeList);

        /* 主生徒の志望大学の志望順位を連番に振り直す（名寄せ処理�Jに相当） */
        reNumberCandidateUnivDispNum(beforeList);

        List<DataRow> afterList = getRowList("CANDIDATEUNIV");
        for (DataRow row : beforeList) {
            assertTrue(afterList.remove(row));
        }
        assertTrue(afterList.isEmpty());
    }

    /**
     * 指定生徒の受験模試コードセットを生成します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     * @param id 個人ID
     * @return 受験模試コードセット
     */
    private Set<String> createExamCdSet(List<DataRow> beforeList, BigDecimal id) {
        Set<String> examCdSet = new HashSet<>();
        for (DataRow row : beforeList) {
            if (id.equals(toIndividualId(row)) && Code.CANDIDATE_UNIV_EXAM.getValue().equals(row.getValue("SECTION"))) {
                examCdSet.add(row.getValue("EXAMCD").toString());
            }
        }
        return examCdSet;
    }

    /**
     * 指定生徒の志望大学に対して、名寄せ前の準備処理を行います。
     *
     * @param beforeTable 名寄せ前の志望大学テーブル
     * @param beforeList 名寄せ前の志望大学リスト
     * @param id 個人ID
     * @param examCdSet 受験模試コードセット
     */
    private void prepareCandidateUniv(DataTable beforeTable, List<DataRow> beforeList, BigDecimal id,
            Set<String> examCdSet) {

        /* 指定生徒が未受験である模試のシステム登録志望大学を削除する */
        Set<String> copyExamCdSet = new HashSet<>();
        for (Iterator<DataRow> ite = beforeList.iterator(); ite.hasNext();) {
            DataRow row = ite.next();
            if (id.equals(toIndividualId(row))) {
                String examCd = row.getValue("EXAMCD").toString();
                if (!examCdSet.contains(examCd)) {
                    copyExamCdSet.add(examCd);
                    ite.remove();
                }
            }
        }

        /* 最新受験模試コードを判断する */
        String latestExamCd;
        if (examCdSet.isEmpty()) {
            /*
             * 全ての模試を未受験なら、最新公開模試とする。
             * ※DAO呼び出し結果の妥当性は テストメソッド（testSelectLatestExamBean）で担保している。
             */
            latestExamCd = dao.selectLatestExamBean(0, systemDto.getUnivYear(), systemDto.getUnivExamDiv()).getExamCd();
        } else {
            latestExamCd = getLatestExamCd(examCdSet);
        }

        /* 指定生徒の最新受験模試の模試記入志望大学マップを作成する */
        Map<String, BigDecimal> examUnivMap = new HashMap<>();
        for (DataRow row : beforeList) {
            if (id.equals(toIndividualId(row)) && latestExamCd.equals(row.getValue("EXAMCD"))
                    && Code.CANDIDATE_UNIV_EXAM.getValue().equals(row.getValue("SECTION"))) {
                examUnivMap.put(toUnivKey(row), (BigDecimal) row.getValue("DISP_NUMBER"));
            }
        }

        /* 指定生徒の最新受験模試のシステム登録志望大学リストを作成する */
        List<DataRow> systemUnivList = new ArrayList<>();
        for (DataRow row : beforeList) {
            if (id.equals(toIndividualId(row)) && latestExamCd.equals(row.getValue("EXAMCD"))
                    && Code.CANDIDATE_UNIV_SCREEN.getValue().equals(row.getValue("SECTION"))) {
                systemUnivList.add(row);
            }
        }

        /* 指定生徒の最新受験模試のシステム登録志望大学を未受験・未公開模試にコピーする */
        for (String examCd : copyExamCdSet) {
            for (DataRow row : systemUnivList) {
                DataRow newRow = beforeTable.addRow();
                newRow.copyFrom(row);
                if (FLG_OFF.equals(newRow.getValue("DISP_FLG"))) {
                    /* 非表示レコードは表示しておく */
                    newRow.setValue("DISP_NUMBER", examUnivMap.get(toUnivKey(row)));
                    newRow.setValue("DISP_FLG", FLG_ON);
                }
                newRow.setValue("EXAMCD", examCd);
                beforeList.add(newRow);
            }
        }
    }

    /**
     * 生徒1が未受験である模試のシステム登録志望大学のうち、
     * 生徒2のシステム登録志望大学と重複するものを削除します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     * @param id1 生徒1の個人ID
     * @param id2 生徒2の個人ID
     * @param examCdSet1 生徒1の受験模試コードセット
     */
    private void deleteDuplicatedCandidateUniv(List<DataRow> beforeList, BigDecimal id1, BigDecimal id2,
            Set<String> examCdSet1) {

        /* 生徒2のシステム登録大学セットを生成する */
        Set<String> systemUnivSet = createUnivSet(beforeList, id2, Code.CANDIDATE_UNIV_SCREEN);

        /*
         * 生徒1が未受験である模試のシステム登録志望大学のうち、
         * 生徒2のシステム登録志望大学と重複するものを削除する。
         */
        for (Iterator<DataRow> ite = beforeList.iterator(); ite.hasNext();) {
            DataRow row = ite.next();
            if (id1.equals(toIndividualId(row)) && !examCdSet1.contains(row.getValue("EXAMCD"))
                    && Code.CANDIDATE_UNIV_SCREEN.getValue().equals(row.getValue("SECTION"))
                    && systemUnivSet.contains(row.getValue("EXAMCD") + toUnivKey(row))) {
                ite.remove();
            }
        }
    }

    /**
     * 指定生徒の指定区分の志望大学セットを生成します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     * @param section 区分
     * @param id 個人ID
     * @return 志望大学セット
     */
    private Set<String> createUnivSet(List<DataRow> beforeList, BigDecimal id, Code section) {
        Set<String> univSet = new HashSet<>();
        for (DataRow row : beforeList) {
            if (id.equals(toIndividualId(row)) && section.getValue().equals(row.getValue("SECTION"))) {
                univSet.add(row.getValue("EXAMCD") + toUnivKey(row));
            }
        }
        return univSet;
    }

    /**
     * 主従情報が未受験である模試のシステム登録志望大学の志望順位を加算します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     * @param subExamCdSet 従情報の受験模試コードセット
     */
    private void addCandidateUnivDispNum(List<DataRow> beforeList, Set<String> subExamCdSet) {
        for (DataRow row : beforeList) {
            BigDecimal id = toIndividualId(row);
            String examCd = row.getValue("EXAMCD").toString();
            if (MAIN_ID.equals(id)) {
                /* 主情報は従情報が未受験の模試を加算しない */
                if (!subExamCdSet.contains(examCd)) {
                    continue;
                }
            } else if (SUB_ID.equals(id)) {
                /* 従情報は個人IDを主情報に差し替えておく（名寄せ処理�Gに相当） */
                row.setValue("INDIVIDUALID", MAIN_ID);
                if (subExamCdSet.contains(examCd)) {
                    continue;
                }
            } else {
                continue;
            }
            row.setValue("DISP_NUMBER", ((BigDecimal) row.getValue("DISP_NUMBER")).add(BigDecimal.valueOf(10000)));
        }
    }

    /**
     * 主生徒のシステム登録志望大学のうち、模試記入志望大学と重複するものを非表示にします。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     */
    private void hideDuplicatedCandidateUniv(List<DataRow> beforeList) {
        Set<String> examUnivSet = createUnivSet(beforeList, MAIN_ID, Code.CANDIDATE_UNIV_EXAM);
        for (DataRow row : beforeList) {
            if (MAIN_ID.equals(toIndividualId(row))
                    && Code.CANDIDATE_UNIV_SCREEN.getValue().equals(row.getValue("SECTION"))
                    && examUnivSet.contains(row.getValue("EXAMCD") + toUnivKey(row))) {
                row.setValue("DISP_FLG", FLG_OFF);
            }
        }
    }

    /**
     * 登録上限を超える主生徒のシステム登録志望大学を削除します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     */
    private void deleteExcessCandidateUniv(List<DataRow> beforeList) {
        Map<String, SortedMap<BigDecimal, DataRow>> map = new HashMap<>();
        Counter<String> invisibleCounter = Counter.newInstance();
        for (DataRow row : beforeList) {
            String examCd = row.getValue("EXAMCD").toString();
            if (MAIN_ID.equals(toIndividualId(row))
                    && Code.CANDIDATE_UNIV_SCREEN.getValue().equals(row.getValue("SECTION"))) {
                if (FLG_ON.equals(row.getValue("DISP_FLG"))) {
                    SortedMap<BigDecimal, DataRow> examMap = map.get(examCd);
                    if (examMap == null) {
                        examMap = new TreeMap<>();
                        map.put(row.getValue("EXAMCD").toString(), examMap);
                    }
                    examMap.put((BigDecimal) row.getValue("DISP_NUMBER"), row);
                } else {
                    invisibleCounter.add(examCd);
                }
            }
        }
        for (Entry<String, SortedMap<BigDecimal, DataRow>> entry : map.entrySet()) {
            SortedMap<BigDecimal, DataRow> examMap = entry.getValue();
            while (examMap.values().size() > CANDIDATE_UNIV_MAX - invisibleCounter.getCount(entry.getKey())) {
                beforeList.remove(examMap.remove(examMap.lastKey()));
            }
        }
    }

    /**
     * 主生徒の志望大学の志望順位を連番に振り直します。
     *
     * @param beforeList 名寄せ前の志望大学リスト
     */
    private void reNumberCandidateUnivDispNum(List<DataRow> beforeList) {
        Map<String, Map<String, SortedMap<BigDecimal, DataRow>>> map = new HashMap<>();
        for (DataRow row : beforeList) {
            if (MAIN_ID.equals(toIndividualId(row))) {
                Map<String, SortedMap<BigDecimal, DataRow>> examMap = map.get(row.getValue("EXAMCD"));
                if (examMap == null) {
                    examMap = new HashMap<>();
                    map.put(row.getValue("EXAMCD").toString(), examMap);
                }
                SortedMap<BigDecimal, DataRow> dispFlgMap = examMap.get(row.getValue("DISP_FLG"));
                if (dispFlgMap == null) {
                    dispFlgMap = new TreeMap<>();
                    examMap.put(row.getValue("DISP_FLG").toString(), dispFlgMap);
                }
                dispFlgMap.put((BigDecimal) row.getValue("DISP_NUMBER"), row);
            }
        }
        for (Map<String, SortedMap<BigDecimal, DataRow>> examMap : map.values()) {
            for (SortedMap<BigDecimal, DataRow> dispFlgMap : examMap.values()) {
                int dispNum = 1;
                for (DataRow row : dispFlgMap.values()) {
                    row.setValue("DISP_NUMBER", BigDecimal.valueOf(dispNum++));
                }
            }
        }
    }

    /**
     * 行データに含まれる個人IDを返します。
     *
     * @param row {@link DataRow}
     * @return 個人ID
     */
    private BigDecimal toIndividualId(DataRow row) {
        return (BigDecimal) row.getValue("INDIVIDUALID");
    }

    /**
     * 行データに含まれる大学キー（大学コード＋学部コード＋学科コード）を返します。
     *
     * @param row {@link DataRow}
     * @return 大学キー
     */
    private String toUnivKey(DataRow row) {
        return row.getValue("UNIVCD").toString() + row.getValue("FACULTYCD") + row.getValue("DEPTCD");
    }

    /**
     * ログの出力内容をチェックします。
     *
     * @param subBean 主情報
     * @param mainBean 従情報
     */
    private void checkLog(SelstdDaddKojinBean mainBean, SelstdDaddKojinBean subBean) {
        try {
            for (File file : FileUtils.listFiles(SettingPropsUtil.getLogDir(), new String[] { "log" }, false)) {
                assertTrue(FileUtils
                        .readLines(file)
                        .get(0)
                        .endsWith(
                                ",B-d3,統合," + mainBean.getId() + "," + mainBean.getGrade() + "," + mainBean.getCls()
                                        + "," + mainBean.getClassNo() + "," + subBean.getId() + ","
                                        + subBean.getGrade() + "," + subBean.getCls() + "," + subBean.getClassNo()));
            }
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }

}

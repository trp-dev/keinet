package jp.ac.kawai_juku.banzaisystem.selstd.dadd.service;

import static jp.ac.kawai_juku.banzaisystem.framework.message.MessageManager.getMessage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;
import jp.ac.kawai_juku.banzaisystem.framework.exception.MessageDialogException;
import jp.ac.kawai_juku.banzaisystem.framework.util.JpnStringUtil;

import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link SelstdDaddService} のテストクラス
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/SelstdDaddServiceTest を作業ディレクトリとすること。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class SelstdDaddServiceTest extends BZTeacherTest {

    @Resource
    private SelstdDaddService service;

    /**
     * {@link SelstdDaddService#registKojin()} のためのテスト・メソッド。
     */
    public void testRegistKojin() {

        /* 生徒追加のテスト */
        service.registKojin(systemDto.getUnivYear(), null, "3", "1", "1", JpnStringUtil.hira2Hkana("てすと　たろう"));

        DataTable b = da.readDbByTable("BASICINFO");
        assertEquals(1, b.getRowSize());

        DataRow br = b.getRow(0);
        assertEquals(BigDecimal.valueOf(1), br.getValue("INDIVIDUALID"));
        assertEquals("ﾃｽﾄ ﾀﾛｳ", br.getValue("NAME_KANA"));

        DataTable h = da.readDbByTable("HISTORYINFO");
        assertEquals(1, h.getRowSize());

        DataRow hr = h.getRow(0);
        assertEquals(BigDecimal.valueOf(1), hr.getValue("INDIVIDUALID"));
        assertEquals(BigDecimal.valueOf(3), hr.getValue("GRADE"));
        assertEquals("01", hr.getValue("CLASS"));
        assertEquals("00001", hr.getValue("CLASS_NO"));

        /* 生徒編集のテスト */
        service.registKojin(systemDto.getUnivYear(), 1, "2", "2", "2", JpnStringUtil.hira2Hkana("てすと　じろう"));

        b = da.readDbByTable("BASICINFO");
        assertEquals(1, b.getRowSize());

        br = b.getRow(0);
        assertEquals(BigDecimal.valueOf(1), br.getValue("INDIVIDUALID"));
        assertEquals("ﾃｽﾄ ｼﾞﾛｳ", br.getValue("NAME_KANA"));

        h = da.readDbByTable("HISTORYINFO");
        assertEquals(1, h.getRowSize());

        hr = h.getRow(0);
        assertEquals(BigDecimal.valueOf(1), hr.getValue("INDIVIDUALID"));
        assertEquals(BigDecimal.valueOf(2), hr.getValue("GRADE"));
        assertEquals("02", hr.getValue("CLASS"));
        assertEquals("00002", hr.getValue("CLASS_NO"));

        /* 存在しない個人IDで編集した場合 */
        try {
            service.registKojin(systemDto.getUnivYear(), 0, "2", "2", "2", JpnStringUtil.hira2Hkana("てすと　じろう"));
            fail();
        } catch (MessageDialogException e) {
            assertEquals(getMessage("error.selstd.dadd.B-d3_E006"), e.getMessage());
        }
    }

}

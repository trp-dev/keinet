package jp.ac.kawai_juku.banzaisystem.selstd.main.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;

import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link SelstdMainService} のテストクラス
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/SelstdMainServiceTest を作業ディレクトリとすること。</li>
 * <li>2015年度の02区分以降の大学マスタを使うこと。</li>
 * <li>2015年度＃１マークと＃１記述の成績データCSV（record.csv）が作業ディレクトリに配置されていること。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class SelstdMainServiceTest extends BZTeacherTest {

    @Resource
    private SelstdMainService service;

    /**
     * テストパターン１〜７：単一生徒選択
     */
    public void testDeleteStudent1To7() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 個人IDキューを生成する */
        Queue<String> idQueue = createIdQueue();

        /* 生徒を削除する */
        service.deleteStudent(Collections.singletonList(Integer.valueOf(idQueue.remove())));

        /* 結果チェック：学籍基本情報 */
        check1To7(da.readDbByTable("BASICINFO"), idQueue);

        /* 結果チェック：学籍履歴情報 */
        check1To7(da.readDbByTable("HISTORYINFO"), idQueue);

        /* 結果チェック：成績データテーブル */
        check1To7(da.readDbByTable("SUBRECORD_I"), idQueue);

        /* 結果チェック：模試記入志望大学データテーブル */
        check1To7(da.readDbByTable("CANDIDATERATING"), idQueue);

        /* 結果チェック：志望大学テーブル */
        check1To7(da.readDbByTable("CANDIDATEUNIV"), idQueue);

        /* 結果チェック：個表データテーブル */
        check1To7(da.readDbByTable("INDIVIDUALRECORD"), idQueue);

        /* 結果チェック：受験予定大学テーブル */
        check1To7(da.readDbByTable("EXAMPLANUNIV"), idQueue);

        /* 結果チェック：個人成績インポート状況テーブル */
        assertEquals(2, da.readDbByTable("STAT_SUBRECORD_I").getRowSize());
    }

    /**
     * テストパターン１〜７のチェック処理です。
     *
     * @param table {@link DataTable}
     * @param idQueue 削除していない個人IDのキュー
     */
    private void check1To7(DataTable table, Queue<String> idQueue) {
        assertTrue(table.getRowSize() > 0);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(idQueue.contains(row.getValue("INDIVIDUALID").toString()));
        }
    }

    /**
     * テストパターン７〜１４：単一生徒選択
     */
    public void testDeleteStudent7To14() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 個人IDキューを生成する */
        Queue<String> idQueue = createIdQueue();

        /* 生徒を削除する */
        List<Integer> idList = new ArrayList<>(2);
        idList.add(Integer.valueOf(idQueue.remove()));
        idList.add(Integer.valueOf(idQueue.remove()));
        service.deleteStudent(idList);

        /* 結果チェック：学籍基本情報 */
        check7To14(da.readDbByTable("BASICINFO"), idQueue);

        /* 結果チェック：学籍履歴情報 */
        check7To14(da.readDbByTable("HISTORYINFO"), idQueue);

        /* 結果チェック：成績データテーブル */
        check7To14(da.readDbByTable("SUBRECORD_I"), idQueue);

        /* 結果チェック：模試記入志望大学データテーブ */
        check7To14(da.readDbByTable("CANDIDATERATING"), idQueue);

        /* 結果チェック：志望大学テーブル */
        check7To14(da.readDbByTable("CANDIDATEUNIV"), idQueue);

        /* 結果チェック：個表データテーブル */
        check7To14(da.readDbByTable("INDIVIDUALRECORD"), idQueue);

        /* 結果チェック：受験予定大学テーブル */
        check7To14(da.readDbByTable("EXAMPLANUNIV"), idQueue);

        /* 結果チェック：個人成績インポート状況テーブル */
        assertEquals(2, da.readDbByTable("STAT_SUBRECORD_I").getRowSize());
    }

    /**
     * テストパターン７〜１４のチェック処理です。
     *
     * @param table {@link DataTable}
     * @param idQueue 削除していない個人IDのキュー
     */
    private void check7To14(DataTable table, Queue<String> idQueue) {
        check1To7(table, idQueue);
    }

    /**
     * テストパターン１５：単一生徒選択
     */
    public void testDeleteStudent15() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 個人IDキューを生成する */
        Queue<String> idQueue = createIdQueue();

        /* 生徒を削除する */
        List<Integer> idList = new ArrayList<>(idQueue.size());
        while (!idQueue.isEmpty()) {
            idList.add(Integer.valueOf(idQueue.remove()));
        }
        service.deleteStudent(idList);

        /* 結果チェック：学籍基本情報 */
        check15(da.readDbByTable("BASICINFO"));

        /* 結果チェック：学籍履歴情報 */
        check15(da.readDbByTable("HISTORYINFO"));

        /* 結果チェック：成績データテーブル */
        check15(da.readDbByTable("SUBRECORD_I"));

        /* 結果チェック：模試記入志望大学データテーブ */
        check15(da.readDbByTable("CANDIDATERATING"));

        /* 結果チェック：志望大学テーブル */
        check15(da.readDbByTable("CANDIDATEUNIV"));

        /* 結果チェック：個表データテーブル */
        check15(da.readDbByTable("INDIVIDUALRECORD"));

        /* 結果チェック：受験予定大学テーブル */
        check15(da.readDbByTable("EXAMPLANUNIV"));

        /* 結果チェック：個人成績インポート状況テーブル */
        check15(da.readDbByTable("STAT_SUBRECORD_I"));
    }

    /**
     * テストパターン１５のチェック処理です。
     *
     * @param table {@link DataTable}
     */
    private void check15(DataTable table) {
        assertEquals(0, table.getRowSize());
    }

    /**
     * 追加テストパターン１：3000人の削除
     */
    public void testDeleteStudentExt1() {
        List<Integer> idList = new ArrayList<Integer>();
        for (int i = 1; i <= 3000; i++) {
            idList.add(Integer.valueOf(i));
        }
        service.deleteStudent(idList);
    }

    /**
     * ＃１マークと＃1記述の成績を取り込みます。
     */
    private void importCsv() {

        /* 個人成績CSVを取り込む */
        importCsv("record.csv");

        /* 受験予定大学を登録する */
        registExamPlanUniv();
    }

    /**
     * 個人IDキューを生成します。
     *
     * @return 個人IDキュー
     */
    private Queue<String> createIdQueue() {
        Queue<String> idQueue = new LinkedList<>();
        DataTable table = da.readDbByTable("HISTORYINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            idQueue.add(row.getValue("INDIVIDUALID").toString());
        }
        /* 存在しない個人IDを入れておく */
        idQueue.add("0");
        return idQueue;
    }

}

package jp.ac.kawai_juku.banzaisystem.framework.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.runner.RunWith;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link JpnStringUtil} のテストクラス
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class JpnStringUtilTest {

    /**
     * {@link JpnStringUtil#hkana2Hira(java.lang.String)} のためのテスト・メソッド。
     */
    public void testHkana2Hira() {

        /* NULLのテスト */
        assertNull(JpnStringUtil.hkana2Hira(null));

        /* 1文字のテスト */
        assertEquals("あ", JpnStringUtil.hkana2Hira("ｱ"));
        assertEquals("い", JpnStringUtil.hkana2Hira("ｲ"));
        assertEquals("う", JpnStringUtil.hkana2Hira("ｳ"));
        assertEquals("え", JpnStringUtil.hkana2Hira("ｴ"));
        assertEquals("お", JpnStringUtil.hkana2Hira("ｵ"));
        assertEquals("か", JpnStringUtil.hkana2Hira("ｶ"));
        assertEquals("き", JpnStringUtil.hkana2Hira("ｷ"));
        assertEquals("く", JpnStringUtil.hkana2Hira("ｸ"));
        assertEquals("け", JpnStringUtil.hkana2Hira("ｹ"));
        assertEquals("こ", JpnStringUtil.hkana2Hira("ｺ"));
        assertEquals("さ", JpnStringUtil.hkana2Hira("ｻ"));
        assertEquals("し", JpnStringUtil.hkana2Hira("ｼ"));
        assertEquals("す", JpnStringUtil.hkana2Hira("ｽ"));
        assertEquals("せ", JpnStringUtil.hkana2Hira("ｾ"));
        assertEquals("そ", JpnStringUtil.hkana2Hira("ｿ"));
        assertEquals("た", JpnStringUtil.hkana2Hira("ﾀ"));
        assertEquals("ち", JpnStringUtil.hkana2Hira("ﾁ"));
        assertEquals("つ", JpnStringUtil.hkana2Hira("ﾂ"));
        assertEquals("て", JpnStringUtil.hkana2Hira("ﾃ"));
        assertEquals("と", JpnStringUtil.hkana2Hira("ﾄ"));
        assertEquals("な", JpnStringUtil.hkana2Hira("ﾅ"));
        assertEquals("に", JpnStringUtil.hkana2Hira("ﾆ"));
        assertEquals("ぬ", JpnStringUtil.hkana2Hira("ﾇ"));
        assertEquals("ね", JpnStringUtil.hkana2Hira("ﾈ"));
        assertEquals("の", JpnStringUtil.hkana2Hira("ﾉ"));
        assertEquals("は", JpnStringUtil.hkana2Hira("ﾊ"));
        assertEquals("ひ", JpnStringUtil.hkana2Hira("ﾋ"));
        assertEquals("ふ", JpnStringUtil.hkana2Hira("ﾌ"));
        assertEquals("へ", JpnStringUtil.hkana2Hira("ﾍ"));
        assertEquals("ほ", JpnStringUtil.hkana2Hira("ﾎ"));
        assertEquals("ま", JpnStringUtil.hkana2Hira("ﾏ"));
        assertEquals("み", JpnStringUtil.hkana2Hira("ﾐ"));
        assertEquals("む", JpnStringUtil.hkana2Hira("ﾑ"));
        assertEquals("め", JpnStringUtil.hkana2Hira("ﾒ"));
        assertEquals("も", JpnStringUtil.hkana2Hira("ﾓ"));
        assertEquals("や", JpnStringUtil.hkana2Hira("ﾔ"));
        assertEquals("ゆ", JpnStringUtil.hkana2Hira("ﾕ"));
        assertEquals("よ", JpnStringUtil.hkana2Hira("ﾖ"));
        assertEquals("ら", JpnStringUtil.hkana2Hira("ﾗ"));
        assertEquals("り", JpnStringUtil.hkana2Hira("ﾘ"));
        assertEquals("る", JpnStringUtil.hkana2Hira("ﾙ"));
        assertEquals("れ", JpnStringUtil.hkana2Hira("ﾚ"));
        assertEquals("ろ", JpnStringUtil.hkana2Hira("ﾛ"));
        assertEquals("わ", JpnStringUtil.hkana2Hira("ﾜ"));
        assertEquals("を", JpnStringUtil.hkana2Hira("ｦ"));
        assertEquals("ん", JpnStringUtil.hkana2Hira("ﾝ"));

        /* 濁点・半濁点・促音 */
        assertEquals("が", JpnStringUtil.hkana2Hira("ｶﾞ"));
        assertEquals("ぎ", JpnStringUtil.hkana2Hira("ｷﾞ"));
        assertEquals("ぐ", JpnStringUtil.hkana2Hira("ｸﾞ"));
        assertEquals("げ", JpnStringUtil.hkana2Hira("ｹﾞ"));
        assertEquals("ご", JpnStringUtil.hkana2Hira("ｺﾞ"));
        assertEquals("ざ", JpnStringUtil.hkana2Hira("ｻﾞ"));
        assertEquals("じ", JpnStringUtil.hkana2Hira("ｼﾞ"));
        assertEquals("ず", JpnStringUtil.hkana2Hira("ｽﾞ"));
        assertEquals("ぜ", JpnStringUtil.hkana2Hira("ｾﾞ"));
        assertEquals("ぞ", JpnStringUtil.hkana2Hira("ｿﾞ"));
        assertEquals("だ", JpnStringUtil.hkana2Hira("ﾀﾞ"));
        assertEquals("ぢ", JpnStringUtil.hkana2Hira("ﾁﾞ"));
        assertEquals("づ", JpnStringUtil.hkana2Hira("ﾂﾞ"));
        assertEquals("で", JpnStringUtil.hkana2Hira("ﾃﾞ"));
        assertEquals("ど", JpnStringUtil.hkana2Hira("ﾄﾞ"));
        assertEquals("ば", JpnStringUtil.hkana2Hira("ﾊﾞ"));
        assertEquals("び", JpnStringUtil.hkana2Hira("ﾋﾞ"));
        assertEquals("ぶ", JpnStringUtil.hkana2Hira("ﾌﾞ"));
        assertEquals("べ", JpnStringUtil.hkana2Hira("ﾍﾞ"));
        assertEquals("ぼ", JpnStringUtil.hkana2Hira("ﾎﾞ"));
        assertEquals("ぱ", JpnStringUtil.hkana2Hira("ﾊﾟ"));
        assertEquals("ぴ", JpnStringUtil.hkana2Hira("ﾋﾟ"));
        assertEquals("ぷ", JpnStringUtil.hkana2Hira("ﾌﾟ"));
        assertEquals("ぺ", JpnStringUtil.hkana2Hira("ﾍﾟ"));
        assertEquals("ぽ", JpnStringUtil.hkana2Hira("ﾎﾟ"));
        assertEquals("ぁ", JpnStringUtil.hkana2Hira("ｧ"));
        assertEquals("ぃ", JpnStringUtil.hkana2Hira("ｨ"));
        assertEquals("ぅ", JpnStringUtil.hkana2Hira("ｩ"));
        assertEquals("ぇ", JpnStringUtil.hkana2Hira("ｪ"));
        assertEquals("ぉ", JpnStringUtil.hkana2Hira("ｫ"));
        assertEquals("ゃ", JpnStringUtil.hkana2Hira("ｬ"));
        assertEquals("ゅ", JpnStringUtil.hkana2Hira("ｭ"));
        assertEquals("ょ", JpnStringUtil.hkana2Hira("ｮ"));
        assertEquals("っ", JpnStringUtil.hkana2Hira("ｯ"));

        /* 複数文字 */
        assertEquals("あいうえお", JpnStringUtil.hkana2Hira("ｱｲｳｴｵ"));
        assertEquals("がぎぐげご", JpnStringUtil.hkana2Hira("ｶﾞｷﾞｸﾞｹﾞｺﾞ"));
        assertEquals("ぱぴぷぺぽ", JpnStringUtil.hkana2Hira("ﾊﾟﾋﾟﾌﾟﾍﾟﾎﾟ"));
        assertEquals("しゃしゅしょ", JpnStringUtil.hkana2Hira("ｼｬｼｭｼｮ"));
        assertEquals("りっぱ", JpnStringUtil.hkana2Hira("ﾘｯﾊﾟ"));
        assertEquals("のーとる", JpnStringUtil.hkana2Hira("ﾉｰﾄﾙ"));

        /* 範囲外テスト */
        assertEquals("A", JpnStringUtil.hkana2Hira("A"));
        assertEquals("試験", JpnStringUtil.hkana2Hira("試験"));
    }

}

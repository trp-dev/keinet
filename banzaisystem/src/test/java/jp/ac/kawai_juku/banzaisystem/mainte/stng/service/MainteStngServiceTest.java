package jp.ac.kawai_juku.banzaisystem.mainte.stng.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;
import jp.ac.kawai_juku.banzaisystem.Code;
import jp.ac.kawai_juku.banzaisystem.exmntn.rslt.service.ExmntnRsltService;
import jp.ac.kawai_juku.banzaisystem.framework.bean.ExamBean;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;
import jp.co.fj.kawaijuku.judgement.util.JudgementConstants;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.unit.Seasar2;

/**
 *
 * {@link MainteStngService} のテストクラス。
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/MainteStngServiceTest を作業ディレクトリとすること。</li>
 * <li>2015年度の02区分以降の大学マスタを使うこと。</li>
 * <li>2015年度＃１マークの成績データCSV（record01.csv）が作業ディレクトリに配置されていること。</li>
 * <li>2015年度＃１記述の成績データCSV（record05.csv）が作業ディレクトリに配置されていること。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class MainteStngServiceTest extends BZTeacherTest {

    @Resource
    private MainteStngService mainteStngService;

    /**
     * テストパターン１：模試を選択、学年に「全」を選択して削除実行
     */
    public void testDeleteRecord1() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* ＃１記述削除実行 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), JudgementConstants.Exam.Code.WRTN1, -1, "");

        /* 結果チェック：個表データ */
        check1(da.readDbByTable("INDIVIDUALRECORD"));

        /* 結果チェック：成績データ */
        check1(da.readDbByTable("SUBRECORD_I"));

        /* 結果チェック：模試記入志望大学データ */
        check1(da.readDbByTable("CANDIDATERATING"));

        /* 結果チェック：志望大学 */
        checkCandidateUniv1(da.readDbByTable("CANDIDATEUNIV"));
    }

    /**
     * テストパターン１のチェック処理です。
     *
     * @param table {@link DataTable}
     */
    private void check1(DataTable table) {
        assertTrue(table.getRowSize() > 0);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!JudgementConstants.Exam.Code.WRTN1.equals(row.getValue("EXAMCD")));
        }
    }

    /**
     * テストパターン１の志望大学チェック処理です。
     *
     * @param table {@link DataTable}
     */
    private void checkCandidateUniv1(DataTable table) {
        assertTrue(table.getRowSize() > 0);
        /* ＃１記述の志望大学（システム登録）の表示順が連番になっているかチェックする */
        Map<Pair<String, String>, Set<BigDecimal>> map = new HashMap<>();
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            if (JudgementConstants.Exam.Code.WRTN1.equals(row.getValue("EXAMCD"))) {
                addDispNumber(map, row);
            }
        }
        checkSequence(map.values());
    }

    /**
     * 生徒・表示フラグ別の表示順保持マップに値を追加します。
     *
     * @param map 生徒・表示フラグ別の表示順保持マップ
     * @param row 志望大学データ
     */
    private void addDispNumber(Map<Pair<String, String>, Set<BigDecimal>> map, DataRow row) {
        assertEquals(Code.CANDIDATE_UNIV_SCREEN.getValue(), row.getValue("SECTION").toString());
        Pair<String, String> key =
                Pair.of(row.getValue("INDIVIDUALID").toString(), row.getValue("DISP_FLG").toString());
        Set<BigDecimal> set = map.get(key);
        if (set == null) {
            set = new HashSet<>();
            map.put(key, set);
        }
        set.add((BigDecimal) row.getValue("DISP_NUMBER"));
    }

    /**
     * 表示順が連番であるかをチェックします。
     *
     * @param setCollection 表示順セットのコレクション
     */
    private void checkSequence(Collection<Set<BigDecimal>> setCollection) {
        for (Set<BigDecimal> set : setCollection) {
            for (int i = 0; i < set.size(); i++) {
                BigDecimal number = BigDecimal.valueOf(i + 1);
                assertTrue(number + " -> [" + set + "]", set.contains(number));
            }
        }
    }

    /**
     * テストパターン２：模試を選択、学年に「全」以外を選択して削除実行
     */
    public void testDeleteRecord2() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 3年生以外の個人IDセットを生成する */
        Set<String> idSet = createGradeIdSet();

        /* ＃１マーク削除実行 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), JudgementConstants.Exam.Code.MARK1, Integer.valueOf(3),
                "");

        /* 結果チェック：個表データ */
        check2(da.readDbByTable("INDIVIDUALRECORD"), idSet);

        /* 結果チェック：成績データ */
        check2(da.readDbByTable("SUBRECORD_I"), idSet);

        /* 結果チェック：模試記入志望大学データ */
        check2(da.readDbByTable("CANDIDATERATING"), idSet);

        /* 結果チェック：志望大学 */
        checkCandidateUniv2(da.readDbByTable("CANDIDATEUNIV"), idSet);
    }

    /**
     * テストパターン２のチェック処理です。
     *
     * @param table {@link DataTable}
     * @param idSet 3年生以外の個人IDセット
     */
    private void check2(DataTable table, Set<String> idSet) {
        assertTrue(table.getRowSize() > 0);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(!JudgementConstants.Exam.Code.MARK1.equals(row.getValue("EXAMCD"))
                    || idSet.contains(row.getValue("INDIVIDUALID").toString()));
        }
    }

    /**
     * テストパターン２の志望大学チェック処理です。
     *
     * @param table {@link DataTable}
     * @param idSet 3年生以外の個人IDセット
     */
    private void checkCandidateUniv2(DataTable table, Set<String> idSet) {
        assertTrue(table.getRowSize() > 0);
        /* ＃１マークの志望大学（システム登録）の表示順が連番になっているかチェックする */
        Map<Pair<String, String>, Set<BigDecimal>> map = new HashMap<>();
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            if (JudgementConstants.Exam.Code.MARK1.equals(row.getValue("EXAMCD"))
                    && !idSet.contains(row.getValue("INDIVIDUALID").toString())) {
                addDispNumber(map, row);
            }
        }
        checkSequence(map.values());
    }

    /**
     * テストパターン３：「全ての模試」を選択、学年に「全」を選択して削除実行
     */
    public void testDeleteRecord3() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 削除実行 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), null, -1, "");

        /* 結果チェック：個表データ */
        check3(da.readDbByTable("INDIVIDUALRECORD"));

        /* 結果チェック：成績データ */
        check3(da.readDbByTable("SUBRECORD_I"));

        /* 結果チェック：模試記入志望大学データ */
        check3(da.readDbByTable("CANDIDATERATING"));

        /* 結果チェック：志望大学 */
        check3(da.readDbByTable("CANDIDATEUNIV"));
    }

    /**
     * テストパターン３のチェック処理です。
     *
     * @param table {@link DataTable}
     */
    private void check3(DataTable table) {
        assertEquals(0, table.getRowSize());
    }

    /**
     * テストパターン４：「全ての模試」を選択、学年に「全」以外を選択して削除実行
     */
    public void testDeleteRecord4() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 3年生以外の個人IDセットを生成する */
        Set<String> idSet = createGradeIdSet();

        /* 削除実行 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), null, Integer.valueOf(3), "");

        /* 結果チェック：個表データ */
        check4(da.readDbByTable("INDIVIDUALRECORD"), idSet);

        /* 結果チェック：成績データ */
        check4(da.readDbByTable("SUBRECORD_I"), idSet);

        /* 結果チェック：模試記入志望大学データ */
        check4(da.readDbByTable("CANDIDATERATING"), idSet);

        /* 結果チェック：志望大学 */
        check4(da.readDbByTable("CANDIDATEUNIV"), idSet);
    }

    /**
     * テストパターン４のチェック処理です。
     *
     * @param table {@link DataTable}
     * @param idSet 3年生以外の個人IDセット
     */
    private void check4(DataTable table, Set<String> idSet) {
        assertTrue(table.getRowSize() > 0);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(idSet.contains(row.getValue("INDIVIDUALID").toString()));
        }
    }

    /**
     * テストパターン５：
     * 成績・志望大学データ削除後、何れかの模試の成績・志望大学（模試で記入した志望大学）データが存在する生徒
     */
    public void testDeleteRecord5() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 3年生以外の個人IDセットを生成する */
        Set<String> idSet = createGradeIdSet();

        /* 3年生の＃１マークを削除 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), JudgementConstants.Exam.Code.MARK1, Integer.valueOf(3),
                "");

        /* 結果チェック：学籍基本情報 */
        assertEquals(4, da.readDbByTable("BASICINFO").getRowSize());

        /* 結果チェック：学籍履歴情報 */
        assertEquals(4, da.readDbByTable("HISTORYINFO").getRowSize());

        /* 結果チェック：志望大学 */
        checkCandidateUniv2(da.readDbByTable("CANDIDATEUNIV"), idSet);

        /* 結果チェック：受験予定大学 */
        assertEquals(8, da.readDbByTable("EXAMPLANUNIV").getRowSize());
    }

    /**
     * テストパターン６〜１０：
     * 成績・志望大学データ削除後、何れの模試の成績・志望大学（模試で記入した志望大学）データも存在しない生徒
     */
    public void testDeleteRecord6To10() {

        /* テストパターン３を実行 */
        testDeleteRecord3();

        /* 結果チェック：学籍基本情報 */
        assertEquals(0, da.readDbByTable("BASICINFO").getRowSize());

        /* 結果チェック：学籍履歴情報 */
        assertEquals(0, da.readDbByTable("HISTORYINFO").getRowSize());

        /* 結果チェック：志望大学 */
        assertEquals(0, da.readDbByTable("CANDIDATEUNIV").getRowSize());

        /* 結果チェック：受験予定大学 */
        assertEquals(0, da.readDbByTable("EXAMPLANUNIV").getRowSize());

        /* 結果チェック：個人成績インポート状況 */
        assertEquals(0, da.readDbByTable("STAT_SUBRECORD_I").getRowSize());
    }

    /**
     * 追加テストパターン１：「全ての模試」を選択、学年に「全」、クラスに「0A」を選択して削除実行
     */
    public void testDeleteRecordExt1() {

        /* ＃１マークと＃1記述の成績を取り込む */
        importCsv();

        /* 0Aクラス以外の個人IDセットを生成する */
        Set<String> idSet = createClassIdSet();

        /* 削除実行 */
        mainteStngService.deleteRecord(systemDto.getUnivYear(), null, -1, "0A");

        /* 結果チェック：個表データ */
        checkExt1(da.readDbByTable("INDIVIDUALRECORD"), idSet);

        /* 結果チェック：成績データ */
        checkExt1(da.readDbByTable("SUBRECORD_I"), idSet);

        /* 結果チェック：模試記入志望大学データ */
        checkExt1(da.readDbByTable("CANDIDATERATING"), idSet);

        /* 結果チェック：志望大学 */
        checkExt1(da.readDbByTable("CANDIDATEUNIV"), idSet);
    }

    /**
     * 追加テストパターン１のチェック処理です。
     *
     * @param table {@link DataTable}
     * @param idSet 0Aクラス以外の個人IDセット
     */
    private void checkExt1(DataTable table, Set<String> idSet) {
        assertTrue(table.getRowSize() > 0);
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            assertTrue(idSet.contains(row.getValue("INDIVIDUALID").toString()));
        }
    }

    /**
     * ＃１マークと＃1記述の成績を取り込みます。
     */
    private void importCsv() {

        /* ＃１マークの個人成績CSVを取り込む */
        importCsv("record01.csv");

        /* ＃１記述の志望大学を登録する（システムで登録した志望大学データを作る） */
        ExamBean exam = new ExamBean();
        exam.setExamYear(systemDto.getUnivYear());
        exam.setExamCd(JudgementConstants.Exam.Code.WRTN1);
        List<UnivKeyBean> univKeyList = createUnivKeyList();
        ExmntnRsltService univService = getComponent(ExmntnRsltService.class);
        DataTable table = da.readDbByTable("BASICINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            Integer id = Integer.valueOf(((BigDecimal) row.getValue("INDIVIDUALID")).intValue());
            univService.addTeacherCandidateUniv(exam, id, univKeyList);
        }

        /* ＃１記述の個人成績CSVを取り込む */
        importCsv("record05.csv");

        /* 受験予定大学を登録する */
        registExamPlanUniv();
    }

    /**
     * 志望大学キーリスト（生徒別上位3位まで）を生成します。
     *
     * @return 志望大学キーリスト
     */
    private List<UnivKeyBean> createUnivKeyList() {
        List<UnivKeyBean> list = new ArrayList<>();
        DataTable table = da.readDbByTable("CANDIDATEUNIV", "DISP_NUMBER <= 3");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            list.add(new UnivKeyBean(row.getValue("UNIVCD").toString(), row.getValue("FACULTYCD").toString(), row
                    .getValue("DEPTCD").toString()));
        }
        return list;
    }

    /**
     * 3年生以外の個人IDセットを生成します。
     *
     * @return 3年生以外の個人IDセット
     */
    private Set<String> createGradeIdSet() {
        Set<String> idSet = new HashSet<>();
        DataTable table = da.readDbByTable("HISTORYINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            if (!"3".equals(row.getValue("GRADE").toString())) {
                idSet.add(row.getValue("INDIVIDUALID").toString());
            }
        }
        return idSet;
    }

    /**
     * 0Aクラス以外の個人IDセットを生成します。
     *
     * @return 0Aクラス以外の個人IDセット
     */
    private Set<String> createClassIdSet() {
        Set<String> idSet = new HashSet<>();
        DataTable table = da.readDbByTable("HISTORYINFO");
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            if (!"0A".equals(row.getValue("CLASS").toString())) {
                idSet.add(row.getValue("INDIVIDUALID").toString());
            }
        }
        return idSet;
    }

}

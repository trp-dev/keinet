package jp.ac.kawai_juku.banzaisystem.mainte.mstr.service;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.RECORD_FILE_NAME;
import static jp.ac.kawai_juku.banzaisystem.BZConstants.UNIV_FILE_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Iterator;

import javax.annotation.Resource;

import jp.ac.kawai_juku.banzaisystem.BZTeacherTest;
import jp.ac.kawai_juku.banzaisystem.framework.http.UpdaterHttpClient;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.helper.MainteMstrHelper;
import jp.ac.kawai_juku.banzaisystem.mainte.mstr.service.MainteMstrService.UnivDbSet.UnivDb;

import org.apache.commons.io.FileUtils;
import org.junit.runner.RunWith;
import org.seasar.extension.dataset.DataRow;
import org.seasar.framework.unit.Seasar2;
import org.seasar.framework.unit.annotation.PostBindFields;
import org.seasar.framework.util.FileUtil;

/**
 *
 * {@link MainteMstrService} のテストクラス。
 *
 * <p>【実行条件】</p>
 * <ul>
 * <li>banzaisystem/src/test_work/MainteMstrServiceTest を作業ディレクトリとすること。</li>
 * <li>更新データ配信サイトに2015年度01区分と02区分の大学マスタファイルが配置されていること。</li>
 * <li>大学マスタファイルフォルダに2015年度01区分（univ.db.201501）と<br>
 * 2014年度08区分（univ.db.201408）の大学マスタファイルが配置されていること。</li>
 * <li>大学マスタファイルフォルダに2015年度（record.db.2015）と<br>
 * 2014年度（record.db.2014）のデータ登録済み成績データファイルが配置されていること。</li>
 * </ul>
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
@RunWith(Seasar2.class)
public class MainteMstrServiceTest extends BZTeacherTest {

    /** 大学マスタファイル(2015年度01区分) */
    private static final File UNIV_2015 = new File(DB_DIR, UNIV_FILE_NAME + ".201501");

    /** 大学マスタファイル(2014年度08区分) */
    private static final File UNIV_2014 = new File(DB_DIR, UNIV_FILE_NAME + ".201408");

    /** 成績データファイル(2015年度) */
    private static final File REC_2015 = new File(DB_DIR, RECORD_FILE_NAME + ".2015");

    /** 成績データファイル(2014年度) */
    private static final File REC_2014 = new File(DB_DIR, RECORD_FILE_NAME + ".2014");

    /** 削除チェック処理対象テーブル */
    private static final String[] REC_TABLES = { "BASICINFO", "HISTORYINFO", "SUBRECORD_I", "CANDIDATERATING", "CANDIDATEUNIV", "INDIVIDUALRECORD", "STAT_SUBRECORD_I", "EXAMPLANUNIV" };

    /** UpdaterHttpClient */
    private final UpdaterHttpClient client = new UpdaterHttpClient();

    @Resource
    private MainteMstrService mainteMstrService;

    @Resource
    private MainteMstrHelper mainteMstrHelper;

    @PostBindFields
    @Override
    public void initialize() {

        /* 初期化処理前に大学マスタファイルと成績データファイルのコピーを行う */
        if (tx.getTestMethodName().endsWith("2014")) {
            FileUtil.copy(UNIV_2014, UNIV_FILE);
            FileUtil.copy(REC_2014, REC_FILE);
        } else {
            FileUtil.copy(UNIV_2015, UNIV_FILE);
            FileUtil.copy(REC_2015, REC_FILE);
        }

        super.initialize();
    }

    /**
     * テストケースの解放メソッドです。
     */
    public void preUnbindFields() {

        /* 大学マスタファイルを削除する */
        UNIV_FILE.delete();

        /* 成績データファイルを削除する */
        REC_FILE.delete();
    }

    /**
     * テストパターン１、３〜５：2015年度01区分から2015年度02区分への更新
     */
    public void testUpdateMaster2015() {

        /* 更新処理対象の大学マスタ情報を取得する */
        UnivDb univDb = mainteMstrHelper.getUpdateTargetUnivMaster(client, new String[] { "2015", "02" });

        /* 大学マスタを更新する */
        mainteMstrService.updateMaster(client, univDb, false);

        /* 更新結果をチェックする */
        check2015();
    }

    /**
     * 2015年度01区分から2015年度02区分へ更新した場合のチェック処理です。
     */
    private void check2015() {

        /* 模試年度・模試区分を確認する */
        assertEquals("2015", systemDto.getUnivYear());
        assertEquals("02", systemDto.getUnivExamDiv());

        /* バックアップファイルが削除されていないことを確認する */
        assertTrue(backupExists());

        /* レコード数を確認する */
        for (String table : REC_TABLES) {
            assertTrue(da.readDbByTable(table).getRowSize() > 0);
        }

        /* 成績データ情報を確認する */
        checkRecordInfo();
    }

    /**
     * 大学マスタファイルのバックアップファイルが存在するかを調べます。
     *
     * @return 大学マスタファイルのバックアップファイルが存在するならtrue
     */
    private boolean backupExists() {
        for (Iterator<File> ite = FileUtils.iterateFiles(DB_DIR, new String[] { "db" }, false); ite.hasNext();) {
            File file = ite.next();
            if (file.getName().startsWith("univ_")) {
                return true;
            }
        }
        return false;
    }

    /**
     * 成績データ情報を確認します。
     */
    private void checkRecordInfo() {
        DataRow row = da.readDbByTable("RECORDINFO").getRow(0);
        assertEquals(systemDto.getUnivYear(), row.getValue("YEAR"));
        assertEquals(systemDto.getUnivExamDiv(), row.getValue("EXAMDIV"));
    }

    /**
     * テストパターン２、６〜１５：2014年度08区分から2015年度02区分への更新
     */
    public void testUpdateMaster2014() {

        /* 更新処理対象の大学マスタ情報を取得する */
        UnivDb univDb = mainteMstrHelper.getUpdateTargetUnivMaster(client, null);

        /* 大学マスタを更新する */
        mainteMstrService.updateMaster(client, univDb, false);

        /* 更新結果をチェックする */
        check2014();
    }

    /**
     * 2015年度01区分から2015年度02区分へ更新した場合のチェック処理です。
     */
    private void check2014() {

        /* 模試年度・模試区分を確認する */
        assertEquals("2015", systemDto.getUnivYear());
        assertEquals("02", systemDto.getUnivExamDiv());

        /* バックアップファイルが削除されていることを確認する */
        assertFalse(backupExists());

        /* レコード数を確認する */
        for (String table : REC_TABLES) {
            assertEquals(0, da.readDbByTable(table).getRowSize());
        }

        /* 成績データ情報を確認する */
        checkRecordInfo();
    }

    /**
     * テストパターン１６、１８〜２０：2015年度01区分から2015年度02区分への更新
     */
    public void testImportFile2015() {

        /* 更新処理対象の大学マスタ情報を取得する */
        UnivDb univDb = mainteMstrHelper.getDownloadTargetUnivMaster(client, new String[] { "2015", "02" });

        /* 大学マスタファイルをダウンロードして取り込む */
        importFile(univDb);

        /* 更新結果をチェックする */
        check2015();
    }

    /**
     * 大学マスタファイルをダウンロードして取り込みます。
     *
     * @param univDb {@link UnivDb}
     */
    private void importFile(UnivDb univDb) {
        File file = null;
        try {
            /* ダウンロード先の一時ファイルを生成する */
            file = File.createTempFile(getClass().getSimpleName(), null);

            /* 大学マスタをダウンロードする */
            mainteMstrService.downloadUnivMaster(client, file, univDb);

            /* ダウンロードした大学マスタを取り込む */
            mainteMstrService.importFile(file, mainteMstrService.getUnivMasterBean(file), false);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (file != null) {
                file.delete();
            }
        }
    }

    /**
     * テストパターン１７、２１〜３０：2014年度08区分から2015年度02区分への更新
     */
    public void testImportFile2014() {

        /* 更新処理対象の大学マスタ情報を取得する */
        UnivDb univDb = mainteMstrHelper.getDownloadTargetUnivMaster(client, null);

        /* 大学マスタファイルをダウンロードして取り込む */
        importFile(univDb);

        /* 更新結果をチェックする */
        check2014();
    }

}

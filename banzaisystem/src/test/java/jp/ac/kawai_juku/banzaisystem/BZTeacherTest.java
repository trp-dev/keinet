package jp.ac.kawai_juku.banzaisystem;

import static jp.ac.kawai_juku.banzaisystem.BZConstants.RECORD_FILE_NAME;
import static org.junit.Assert.assertTrue;
import static org.seasar.framework.container.SingletonS2Container.getComponent;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jp.ac.kawai_juku.banzaisystem.exmntn.main.service.ExmntnMainExamPlanUnivService;
import jp.ac.kawai_juku.banzaisystem.framework.dto.SystemDto;
import jp.ac.kawai_juku.banzaisystem.framework.service.InitializeService;
import jp.ac.kawai_juku.banzaisystem.mainte.impt.service.MainteImptImportService;
import jp.co.fj.kawaijuku.judgement.beans.UnivKeyBean;

import org.seasar.extension.dataset.DataRow;
import org.seasar.extension.dataset.DataTable;
import org.seasar.framework.unit.annotation.PostBindFields;

/**
 *
 * バンザイシステムの先生用モード用のテスト基本クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public class BZTeacherTest extends BZStudentTest {

    /** 成績データファイル */
    protected static final File REC_FILE = new File(DB_DIR, RECORD_FILE_NAME);

    @PostBindFields
    @Override
    public void initialize() {

        super.initialize();

        /* 先生用フラグを立てる */
        getComponent(SystemDto.class).setTeacher(true);

        /* 成績データ情報を初期化する */
        getComponent(InitializeService.class).initRecordInfo();
    }

    /**
     * 個人成績CSVを取り込みます。
     *
     * @param file 個人成績CSVファイル
     */
    protected void importCsv(String file) {
        MainteImptImportService importService = getComponent(MainteImptImportService.class);
        importService.importCsv(new File(file), null);
        assertTrue(da.readDbByTable("BASICINFO").getRowSize() > 0);
        assertTrue(da.readDbByTable("HISTORYINFO").getRowSize() > 0);
        assertTrue(da.readDbByTable("SUBRECORD_I").getRowSize() > 0);
        assertTrue(da.readDbByTable("CANDIDATERATING").getRowSize() > 0);
        assertTrue(da.readDbByTable("CANDIDATEUNIV").getRowSize() > 0);
        assertTrue(da.readDbByTable("INDIVIDUALRECORD").getRowSize() > 0);
        assertTrue(da.readDbByTable("STAT_SUBRECORD_I").getRowSize() > 0);
    }

    /**
     * 全生徒の受験予定大学を登録します。（東大文�Tと理�T）
     */
    protected void registExamPlanUniv() {
        ExmntnMainExamPlanUnivService planService = getComponent(ExmntnMainExamPlanUnivService.class);
        DataTable table = da.readDbByTable("BASICINFO");
        List<UnivKeyBean> addList = new ArrayList<>(2);
        addList.add(new UnivKeyBean("1120130001"));
        addList.add(new UnivKeyBean("1120410001"));
        for (int i = 0; i < table.getRowSize(); i++) {
            DataRow row = table.getRow(i);
            BigDecimal id = (BigDecimal) row.getValue("INDIVIDUALID");
            planService.addTeacherExamPlanUniv(Integer.valueOf(id.intValue()), addList);
        }
    }

}

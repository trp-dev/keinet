package jp.ac.kawai_juku.banzaisystem_bootstrap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * バンザイシステムの起動クラスです。
 *
 *
 * @author TOTEC)KAWAI.Yoshimoto
 *
 */
public final class BZBootStrap {

    /** 再起動時のプロセスステータス */
    public static final int REBOOT_STATUS = 1048576;

    /**
     * コンストラクタです。
     */
    private BZBootStrap() {
    }

    /**
     * メインメソッドです。
     *
     * @param args 引数
     * @throws IOException IO例外
     */
    public static void main(String[] args) throws IOException {
        boot();
    }

    /**
     * バンザイシステムを起動します。
     *
     * @throws IOException IO例外
     */
    private static void boot() throws IOException {

        String javaHome = System.getProperty("java.home");

        String javaCmd = javaHome + File.separator + "bin" + File.separator + "javaw.exe";

        ProcessBuilder builder = new ProcessBuilder(javaCmd, "-Xmx512m", "-Xms256m", "-cp", createClassPath(javaHome), "jp.ac.kawai_juku.banzaisystem.framework.BZMain");
        builder.redirectErrorStream(true);

        Process p = builder.start();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream(), "Windows-31J"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }

        if (p.exitValue() == REBOOT_STATUS) {
            boot();
        } else {
            System.exit(p.exitValue());
        }
    }

    /**
     * クラスパスを生成します。
     *
     * @param javaHome システムプロパティ[java.home]の値
     * @return クラスパス
     */
    private static String createClassPath(String javaHome) {

        File lib = new File("lib");
        if (!lib.isDirectory()) {
            throw new RuntimeException("ライブラリフォルダが見つかりませんでした。" + lib);
        }

        StringBuilder sb = new StringBuilder();

        /* プロキシ設定取得用のjarを追加する */
        sb.append(javaHome).append(File.separator).append("lib").append(File.separator).append("deploy.jar;");
        sb.append(javaHome).append(File.separator).append("lib").append(File.separator).append("plugin.jar;");

        for (File file : findSubFolders(lib)) {
            addClassPath(sb, file);
        }

        addClassPath(sb, lib);

        return sb.toString();
    }

    /**
     * ライブラリフォルダのサブフォルダリストを返します。
     *
     * @param lib ライブラリフォルダ
     * @return ライブラリフォルダのサブフォルダリスト
     */
    private static List<File> findSubFolders(File lib) {

        File[] folders = lib.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.isFile()) {
                    return false;
                }
                try {
                    Double.parseDouble(pathname.getName());
                    return true;
                } catch (NumberFormatException e) {
                    return false;
                }
            }
        });

        if (folders == null) {
            throw new RuntimeException("フォルダが存在しません。" + lib);
        }

        List<File> list = Arrays.asList(folders);

        Collections.sort(list, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                Double d1 = Double.valueOf(o1.getName());
                Double d2 = Double.valueOf(o2.getName());
                return d1.compareTo(d2);
            }
        });

        Collections.reverse(list);

        return list;
    }

    /**
     * 指定したフォルダをクラスパスに追加します。
     *
     * @param sb 文字列バッファ
     * @param dir フォルダ
     */
    private static void addClassPath(StringBuilder sb, File dir) {
        String path = dir.getPath();
        sb.append(path);
        if (!path.endsWith(File.separator)) {
            sb.append(File.separator);
        }
        sb.append("*;");
    }

}

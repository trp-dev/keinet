#!/bin/sh

# 異常終了時はデータファイル及びログファイル名に日時を付けて移動する

# 日時の取得
DAY=`date +%Y%m%d%H%M%S`

# 対象ファイルをエラーフォルダに移動
mv ${DATASRCDIR}/${DATAFILENAME} ${ERRDIR}/${DAY}_${DATAFILENAME}

# ログファイルをエラーフォルダに移動
if [ -f ${LOGFILEDIR}/${LOGFILENAME} ]
then
cp -p  ${LOGFILEDIR}/${LOGFILENAME} ${ERRDIR}/${DAY}_${LOGFILENAME}
fi

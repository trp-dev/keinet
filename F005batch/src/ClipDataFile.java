/*
 * 作成日: 2004/12/15
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * センターリサーチ
 * FDデータファイルから各学校のファイルへ切り出すクラス
 *
 * @author Yoshimoto KAWAI - Totec
 * @version 1.0
 */
public class ClipDataFile {

    // プロパティファイル
	private static final String PROPERTIES = "ClipDataFile.properties";

	// 出力先ディレクトリ
	private static final String FILE_OUTPUT_DIR;
	// 出力先ディレクトリ（センターリサーチ）
	private static final String CR_FILE_OUTPUT_DIR;
	// 入力データファイル
	private static final String INPUT_DATA_DIR;
	// 入力文字コードセット
	private static final String INPUT_CHARSET;
	// 出力文字コードセット
	private static final String OUTPUT_CHARSET;
	// 改行コード
	private static final String LINE_FEED;
	// 受験学力測定テスト - 調査回答１ファイル名
	private static final String TYOUSA_ONE;
	// 受験学力測定テスト - 調査回答２ファイル名
	private static final String TYOUSA_TWO;
	
	// ログインスタンス 
	private static final Logger logger = Logger.getLogger(ClipDataFile.class);
	
	// 設定を読み込む
	static {
		InputStream in = ClipDataFile.class.getResourceAsStream(PROPERTIES);

		if (in == null) {
			throw new IllegalArgumentException("properties file not found.");
		}

		Properties props = new Properties();

		try {
			props.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}

		FILE_OUTPUT_DIR = (String) props.get("FileOutputDir");
		CR_FILE_OUTPUT_DIR = (String) props.get("CRFileOutputDir");
		INPUT_DATA_DIR = (String) props.get("InputDataDir");
		INPUT_CHARSET = (String) props.get("InputCharset");
		OUTPUT_CHARSET = (String) props.get("OutputCharset");
		LINE_FEED = (String) props.get("LineFeed");
		TYOUSA_ONE = (String) props.get("TyousaOne");
		TYOUSA_TWO = (String) props.get("TyousaTwo");
		
		DOMConfigurator.configure((String) props.get("Log4jConfig"));
	}
	
	/**
	 * コンストラクタ
	 */
	private ClipDataFile() {
	}
	
    /**
     * メインメソッド
     * @param args
     */
    public static void main(String[] args) {
        logger.info("バッチ処理を開始します。");

        try {
            ClipDataFile clip = new ClipDataFile();
            
            File dir = new File(INPUT_DATA_DIR);
            if (dir.isDirectory()) {
                File[] file = dir.listFiles();
                for (int i=0; file!=null && i<file.length; i++) {
                    if (file[i].isDirectory()) continue;
                    logger.info("ファイル処理開始: " + file[i].getPath());
                    clip.execute(file[i]);
                }
            }
        } catch (Exception e) {
            logger.fatal("バッチ処理中に例外が発生しました。", e);
            System.exit(1);
        }

        logger.info("バッチ処理を終了します。");
        System.exit(0);
    }
   
    /**
     * 実行部
     * @param file 入力ファイル
     * @throws Exception
     */
    public void execute(File file) throws Exception {
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            reader =
                new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(file),
                        INPUT_CHARSET
                    )
               	);
            
            int count = 0; // カウンタ
            String dir = null; // 出力ディレクトリ
            String subject = null; // 項目名の行
            String line = null;
            while ((line = reader.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");
                
                if (st.countTokens() < 4)
                	throw new Exception("データ項目数が不足しています。"
                			+ st.countTokens() + "：" + file);

                // 連番が数字でなければ項目名を保持して次へ
                String number = st.nextToken();
                try {
                	Integer.parseInt(number);
                } catch (NumberFormatException e) {
                	subject = line;
                	continue;
                }
                
                // ファイル切替処理
                if (subject != null) {
                    if (writer != null) writer.close();

                    String examYear = st.nextToken(); // 模試年度
                    String examCd = st.nextToken(); // 模試コード
                    String bundleCd = st.nextToken(); // 一括コード
                    if (bundleCd.length() == 4) bundleCd = "0" + bundleCd;

					// 受験学力測定テスト対応
					//「調査回答１」
					if (TYOUSA_ONE.equals(file.getName())) {
						examCd = examCd + "_1";
					//「調査回答２」
					} else if (TYOUSA_TWO.equals(file.getName())) {
						examCd = examCd + "_2";
					}
					
                    // 出力ディレクトリを作る
                    if (dir == null) {
                        if ("38".equals(examCd)) dir = CR_FILE_OUTPUT_DIR;
                        else dir = FILE_OUTPUT_DIR;

                        // ファイル削除処理
                        // ２年前は無条件ですべて消す
                        this.deleteAll(dir + File.separator
                        		+ String.valueOf(Integer.parseInt(examYear) - 2));
                        // １年前の同じ模試を消す
                        this.deleteAll(dir + File.separator
                        		+ String.valueOf(Integer.parseInt(examYear) - 1)
								+ File.separator + examCd);
                        
                        // 作ります
                        dir += File.separator + examYear + File.separator + examCd;
                        new File(dir).mkdirs();
                    }
                    
                    // writer
                    writer =
                        new BufferedWriter(
	                        new OutputStreamWriter(
                                new FileOutputStream(dir + File.separator + bundleCd + ".csv"),
	                            OUTPUT_CHARSET
                            )
                     	);
                    
                    // 切り替えました
                    writer.write(subject + LINE_FEED);
                    subject = null;
                }
                
                writer.write(line + LINE_FEED);
                
                if (++count % 10000 == 0) logger.info(count + "レコード");
            }
            logger.info("ファイル処理終了: " + count + "レコード");

		} finally {
		    if (reader != null) reader.close();
		    if (writer != null) writer.close();
		}       
    }
    
    /**
     * 指定されたパス以下のファイルを全て消す
     * @param path
     */
    private void deleteAll(String path) {
    	File current = new File(path);
    	if (current.isDirectory()) {
    		File[] list = current.listFiles();
    		for (int i=0; i<list.length; i++) {
    			if (list[i].isDirectory()) this.deleteAll(list[i].getPath());
    			else list[i].delete();
    		}
    	}
    	current.delete();
    }
    
}

package jp.ac.kawaijuku.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.ac.kawaijuku.config.AppConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnivMasterService {

    @Autowired
    private AppConfig config;

    private Logger logger = LogManager.getLogger(UnivMasterService.class);

    private final List<String> nitteiList = Arrays.asList("C", "D", "E", "N", "1");

    private final String lf = "\r\n";

    private final String charset = "MS932";

    private Map<String, Integer> swap = new HashMap<String, Integer>();

    /**
     * application.propertiesに定義された大学マスタファイルを加工します。
     *  ・univmaster_basic
     *  ・univmaster_choiceschool
     *  ・univmaster_course
     *  ・univmaster_selectgpcourse
     *  ・univmaster_selectgroup
     *  ・univmaster_subject
     *
     */
    public void execute() {
        String input = config.getProp("input");
        String output = config.getProp("output");
        input += input.endsWith("/") ? "" : "/";
        output += output.endsWith("/") ? "" : "/";
        logger.info("input:  [{}]", input);
        logger.info("output: [{}]", output);

        // 大学マスタ基本情報
        modifyUnivMasterBasic(input, output);
        // 大学マスタ模試用志望校
        modifyUnivMasterChoiceSchool(input, output);
        // 大学マスタ模試用科目
        modifyUnivMasterSubject(input, output);
        // 大学マスタ模試用教科
        modifyUnivMasterCourse(input, output);
        // 大学マスタ模試用選択グループ
        modifyUnivMasterSelectGroup(input, output);
        // 大学マスタ模試用選択グループ教科
        modifyUnivMasterSelectGpCourse(input, output);
    }

    /**
     * インターネットバンザイ用に大学マスタ基本情報を加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterBasic(String input, String output) {
        String file = config.getProp("univmaster_basic");
        int count = 0;
        try (BufferedWriter bw1 = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);
                BufferedWriter bw2 = Files.newBufferedWriter(Paths.get(output + file + "_cut_4_border"), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 大学区分[2]
                String univDiv = substring(line, 7, 2);
                // 学部コード[2]
                String facultyCd = substring(line, 13, 2);
                // 学科コード[4]
                String deptCd = substring(line, 15, 4);
                // 大学グループ区分[1]
                String nittei = substring(line, 29, 1);
                // 学科通番[4]
                String deptSerialNo = substring(line, 34, 4);
                // 受験校本部県コード[2]
                String prefCdHo = substring(line, 44, 2);
                // 大学カナ名[12]
                String univNameK = substring(line, 153, 12);
                // 学科[10]
                String deptNameK = substring(line, 173, 10);
                // 大学夜間部区分[1]
                String nightDiv = substring(line, 192, 1);
                // 入試ランク[2]
                String entExamRank = substring(line, 206, 2);
                // 日程方式[1]
                String scheduleSys = substring(line, 280, 1);
                // リサーチ使用区分[1]
                String researchUseDiv = substring(line, 426, 1);

                // 難易ランクが空のときは"99"に書き換え
                if ("  ".equals(entExamRank)) {
                    entExamRank = "99";
                }

                // リサーチ使用区分がONで、かつ、日程が[前中後期・センター私大]のみ出力
                if ("1".equals(researchUseDiv) && nitteiList.contains(nittei)) {
                    String out1 = substring(line, 0, 206).concat(entExamRank).concat(substring(line, 208)).concat(univDiv).concat(prefCdHo).concat(univNameK).concat(nightDiv).concat(facultyCd).concat(nittei).concat(deptSerialNo)
                            .concat(scheduleSys).concat(deptNameK).concat(deptCd);
                    String out2 = substring(line, 0, 206).concat(entExamRank.concat(substring(line, 208)));
                    bw1.write(out1 + lf);
                    bw2.write(out2 + lf);
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * インターネットバンザイ用に大学マスタ模試用志望校を加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterChoiceSchool(String input, String output) {
        String file = config.getProp("univmaster_choiceschool");
        int count = 0;
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 推薦用区分[1]
                String admissionDiv = substring(line, 17, 1);
                // 来年用区分[1]
                String nextYearDiv = substring(line, 18, 1);

                // 推薦用区分が[0:通常用]かつ、来年用区分が[0:本年]、[9:旧課程]の場合のみ出力
                if ("0".equals(admissionDiv) && ("0".equals(nextYearDiv)) || "9".equals(nextYearDiv)) {
                    bw.write(line + lf);
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * インターネットバンザイ用に大学マスタ模試用科目を加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterSubject(String input, String output) {
        String file = config.getProp("univmaster_subject");
        int count = 0;
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 推薦用区分[1]
                String admissionDiv = substring(line, 17, 1);
                // 来年用区分[1]
                String nextYearDiv = substring(line, 18, 1);
                // 教科コード[1]
                String courseCd = substring(line, 27, 1);
                // 科目ビット[2]
                String subBit = substring(line, 28, 2);
                // 科目配点[5]
                String subPnt = substring(line, 30, 5);
                // 科目範囲[2]
                String subArea = substring(line, 35, 2);

                // 推薦用区分が[0:通常用]かつ、来年用区分が[0:本年]、[9:旧課程]の場合のみ出力
                if ("0".equals(admissionDiv) && ("0".equals(nextYearDiv)) || "9".equals(nextYearDiv)) {
                    // 科目範囲の書き換え
                    if ("4".equals(courseCd) && "03".equals(subArea))
                        subArea = "04";
                    if ("5".equals(courseCd) && "04".equals(subArea))
                        subArea = "03";

                    bw.write(substring(line, 0, 35) + subArea + substring(line, 37, 1) + lf);

                    if ("H ".equals(subBit)) {
                        String key = substring(line, 0, 23) + courseCd;
                        Integer pnt = 0;
                        if (swap.containsKey(key)) {
                            pnt = swap.get(key);
                        }
                        pnt += Integer.valueOf(subPnt);
                        swap.put(key, pnt);
                    }
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * インターネットバンザイ用に大学マスタ模試用教科を加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterCourse(String input, String output) {
        String file = config.getProp("univmaster_course");
        int count = 0;
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 推薦用区分[1]
                String admissionDiv = substring(line, 17, 1);
                // 来年用区分[1]
                String nextYearDiv = substring(line, 18, 1);
                // 必須配点[5]
                String necesAllotPnt = substring(line, 24, 5);

                // 推薦用区分が[0:通常用]かつ、来年用区分が[0:本年]、[9:旧課程]の場合のみ出力
                if ("0".equals(admissionDiv) && ("0".equals(nextYearDiv)) || "9".equals(nextYearDiv)) {
                    count++;
                    // 必須教科配点が設定されている場合
                    if (!"99999".equals(necesAllotPnt)) {
                        bw.write(line + lf);
                        continue;
                    }
                    String key = substring(line, 0, 24);
                    // 必須教科配点が未設定の場合、科目配点の合計を設定
                    if (swap.containsKey(key)) {
                        bw.write(key + String.format("%05d", swap.get(key)) + substring(line, 29) + lf);
                    } else {
                        bw.write(line + lf);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * インターネットバンザイ用に大学マスタ模試用選択グループを加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterSelectGroup(String input, String output) {
        String file = config.getProp("univmaster_selectgroup");
        int count = 0;
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 推薦用区分[1]
                String admissionDiv = substring(line, 17, 1);
                // 来年用区分[1]
                String nextYearDiv = substring(line, 18, 1);

                // 推薦用区分が[0:通常用]かつ、来年用区分が[0:本年]、[9:旧課程]の場合のみ出力
                if ("0".equals(admissionDiv) && ("0".equals(nextYearDiv)) || "9".equals(nextYearDiv)) {
                    count++;
                    bw.write(line + lf);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * インターネットバンザイ用に大学マスタ模試用選択グループ教科を加工します。
     *
     * @param input
     * @param output
     */
    private void modifyUnivMasterSelectGpCourse(String input, String output) {
        String file = config.getProp("univmaster_selectgpcourse");
        int count = 0;
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get(output + file), Charset.forName(charset), StandardOpenOption.CREATE);) {
            // 全行読み込み
            List<String> lines = Files.readAllLines(Paths.get(input + file), Charset.forName(charset));
            for (String line : lines) {
                // 推薦用区分[1]
                String admissionDiv = substring(line, 17, 1);
                // 来年用区分[1]
                String nextYearDiv = substring(line, 18, 1);

                // 推薦用区分が[0:通常用]かつ、来年用区分が[0:本年]、[9:旧課程]の場合のみ出力
                if ("0".equals(admissionDiv) && ("0".equals(nextYearDiv)) || "9".equals(nextYearDiv)) {
                    count++;
                    bw.write(line + lf);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            logger.info("{}: [ {} ]件", file, count);
        }
    }

    /**
     * 指定された位置からバイト数分の文字を切り出します。
     *
     * @param source
     * @param startIndex
     * @param bytesCount
     * @return 切り出された文字
     * @throws UnsupportedEncodingException
     */
    protected String substring(String source, int startIndex, int bytesCount) throws UnsupportedEncodingException {
        if (source == null)
            return source;
        byte[] bytes = source.getBytes(charset);
        if (bytes.length < bytesCount)
            return source;
        String result = new String(bytes, startIndex, bytesCount, charset);
        String last = result.substring(result.length() - 1);
        String lazt = new String(last.getBytes(charset), charset);
        if (!last.equals(lazt)) {
            result = result.substring(startIndex, result.length() - 1);
        }
        return result;
    }

    /**
     * 指定された位置から文字を切り出します。
     *
     * @param source
     * @param startIndex
     * @return 切り出された文字
     * @throws UnsupportedEncodingException
     */
    protected String substring(String source, int startIndex) throws UnsupportedEncodingException {
        if (source == null)
            return source;
        byte[] bytes = source.getBytes(charset);
        return substring(source, startIndex, bytes.length - startIndex);
    }
}

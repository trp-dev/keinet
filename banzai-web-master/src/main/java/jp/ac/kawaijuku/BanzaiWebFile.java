package jp.ac.kawaijuku;

import jp.ac.kawaijuku.service.UnivMasterService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BanzaiWebFile implements CommandLineRunner {

    private Logger logger = LogManager.getLogger(BanzaiWebFile.class);

    @Autowired
    private UnivMasterService svc;

    /**
     * メイン処理
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(BanzaiWebFile.class);
        app.run(args);
    }

    @Override
    public void run(String... args) {
        logger.info("App Start");
        // 大学マスタファイルの加工処理
        svc.execute();
        logger.info("App End");
    }
}

package jp.ac.kawaijuku.aspect;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(1)
public class ControllerAspect extends BaseAspect {

    /**
     * <pre>
     * ポイントカット。
     * </pre>
     */
    @Pointcut("execution(public * jp.ac.kawaijuku.controller..*(..))")
    public void pointCut() {
    }

    /**
     * ハンドリングする処理の前に実行されるメソッド
     * @param jp
     * @throws Exception
     */
    @Before("pointCut()")
    public void beforeHandler(JoinPoint jp) throws Exception {
        Logger logger = getTargetLogger(jp);
        setStartTime();
        String info = "";
        if (logger.isDebugEnabled()) {
            if (jp != null && jp.getArgs() != null && jp.getArgs().length > 0) {
                info = "| args=" + Arrays.asList(jp.getArgs());
            }
        }
        logger.info("{} START {}", jp.getSignature().getName(),  info);
    }

    /**
     * ハンドリングする処理の後に実行されるメソッド
     * @param jp
     * @param r
     * @throws Exception
     */
    @AfterReturning(value = "pointCut()", returning = "r")
    public void afterHandler(JoinPoint jp, Object r) throws Exception {
      Logger logger = getTargetLogger(jp);
      long time = System.currentTimeMillis() - getStartTime();

      logger.info("{} END {}ms", jp.getSignature().getName(), time);
    }
}

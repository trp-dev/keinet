package jp.ac.kawaijuku.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

public abstract class BaseAspect {

    /** スレッドローカルストレージ : 処理開始時間スタック */
    protected static ThreadLocal<Stack<Long>> startTimeStack = new ThreadLocal<>();

    /**
     * <pre>
     * ジョインポイントの情報からロガーを生成して返すメソッド。
     * </pre>
     *
     * @param jp
     *            ジョインポイント
     * @return ロガー
     */
    protected Logger getTargetLogger(JoinPoint jp) {
        // ターゲットクラスを取りだす
        Class<?> targetClass = jp.getTarget().getClass();
        Logger logger = LogManager.getLogger(targetClass);
        return logger;
    }

    /**
     * <pre>
     * ジョインポイントに特定のアノテーションが付与されている場合にそのアノテーションを取得する。
     * アノテーションが付与されていない場合はnullを返す。
     * </pre>
     *
     * @param <A>
     *            アノテーション
     * @param jp
     *            ジョインポイント
     * @param annotationClass
     *            アノテーションクラス
     * @return アノテーション(付与されていなければnull)
     * @throws Exception
     */
    protected <A extends Annotation> A getAnnotation(JoinPoint jp, Class<A> annotationClass) throws Exception {

        // メソッドシグネチャからメソッドを取り出す
        MethodSignature signature = (MethodSignature) jp.getSignature();
        Method method = signature.getMethod();

        Class<?>[] parameterTypes = method.getParameterTypes();
        Method implMethod = null;
        A annotation = null;

        // 実装メソッドを取得
        try {
            implMethod = jp.getTarget().getClass().getMethod(method.getName(), parameterTypes);
            // 実装メソッドからアノテーションを取得
            annotation = implMethod.getAnnotation(annotationClass);
        } catch (NoSuchMethodException | SecurityException e) {
            throw e;
        }
        if (annotation == null) {
            // 実装メソッドにアノテーションが無ければ、インターフェイスからアノテーションを確認
            annotation = method.getAnnotation(annotationClass);
        }
        return annotation;
    }

    /**
     * <pre>
     * 処理開始日時をセットするメソッド。
     * </pre>
     */
    protected void setStartTime() {
        Stack<Long> stack = startTimeStack.get();
        if (stack == null) {
            stack = new Stack<Long>();
        }
        stack.push(System.currentTimeMillis());
        startTimeStack.set(stack);
    }

    /**
     * <pre>
     * 処理開始日時を取得するメソッド。
     * </pre>
     * @return 処理開始日時
     */
    protected long getStartTime() {
        Stack<Long> stack = startTimeStack.get();
        if (stack == null || stack.isEmpty()) {
            return 0;
        }
        return stack.pop();
    }

}
